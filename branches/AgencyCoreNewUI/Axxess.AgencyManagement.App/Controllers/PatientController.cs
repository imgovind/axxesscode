﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;
    using ViewData;
    using Workflows;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;


    using Axxess.OasisC.Domain;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Exports;

    using Axxess.Log.Enums;
    using Axxess.Log.Common;

    using Telerik.Web.Mvc;

    using Axxess.AgencyManagement.App.iTextExtension;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using System.Web.Script.Serialization;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.App.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PatientController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IUserService userService;
        private readonly IMessageService messageService;
        private readonly IPatientService patientService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IPhysicianRepository physicianRepository;

        public PatientController(IAgencyManagementDataProvider agencyManagementDataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService, IMessageService messageService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.messageService = messageService;
            this.assessmentService = assessmentService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region PatientController Actions

        #region CRUD
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid? id) {
            if (id.HasValue) return PartialView(referralRepository.Get(Current.AgencyId, (Guid)id));
            else return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Create([Bind] Patient patient) {
            Check.Argument.IsNotNull(patient, "patient");
            var rules = new List<Validation>();
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be saved" };
            if (patient != null) {
                if (patient.PatientIdNumber.IsNotNullOrEmpty()) {
                    bool patientIdCheck = patientRepository.IsPatientIdExist(Current.AgencyId, patient.PatientIdNumber);
                    rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                }
                if (patient.MedicareNumber.IsNotNullOrEmpty()) {
                    bool medicareNumberCheck = patientRepository.IsMedicareExist(Current.AgencyId, patient.MedicareNumber);
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty()) {
                    bool medicaidNumberCheck = patientRepository.IsMedicaidExist(Current.AgencyId, patient.MedicaidNumber);
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                if ( patient.PrimaryInsurance >= 1000) rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                if ( patient.SecondaryInsurance >= 1000) rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                if ( patient.TertiaryInsurance >= 1000) rules.Add(new Validation(() => patient.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (patient.IsValid && entityValidator.IsValid) {
                    patient.AgencyId = Current.AgencyId;
                    patient.Id = Guid.NewGuid();
                    patient.Encode(); // setting string arrays to one field
                    if (patient.Status == (int)PatientStatus.Active) {
                        var workflow = new CreatePatientWorkflow(patient);
                        if (workflow.IsCommitted) {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Patient was created successfully.";
                        } else {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = workflow.Message;
                        }
                    } else {
                        var workflow = new PendingPatientWorkFlow(patient);
                        if (workflow.IsCommitted) {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Patient was created successfully.";
                        } else viewData.isSuccessful = false;
                    }
                    if (viewData.isSuccessful && !patient.ReferralId.IsEmpty()) {
                        if (referralRepository.SetStatus(Current.AgencyId, patient.ReferralId, ReferralStatus.Admitted)) Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, patient.ReferralId.ToString(), LogType.Referral, LogAction.ReferralAdmitted, string.Empty);
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = patient.ValidationMessage + "\n" + entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid id) {
            return Json(patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(Guid id)
        {
            return PartialView(patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List() {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            ViewData["BranchId"] = Guid.Empty;
            ViewData["Status"] = 1;
            var patientList = patientRepository.All(Current.AgencyId, Guid.Empty, 1);
            return View("List", patientList);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(Guid branch, int status, string sort)
        {
            ViewData["BranchId"] = branch;
            ViewData["Status"] = status;
            var parameters = sort.Split('-');
            if (parameters.Length >= 2) {
                ViewData["SortColumn"] = parameters[0];
                ViewData["SortDirection"] = parameters[1].ToUpperCase();
            }
            return View("ListContent", patientRepository.All(Current.AgencyId, branch, status));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GridAll(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId);
            else if (Current.IsClinicianOrHHA) patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GridAllMedicare(Guid branchId, byte statusId, int paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) patientList = patientRepository.GetPatientSelectionMedicare(Current.AgencyId, branchId, statusId, paymentSourceId);
            else if (Current.IsClinicianOrHHA) patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GridAllInsurance(Guid branchId, byte statusId, int paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) patientList = patientRepository.GetPatientSelectionAllInsurance(Current.AgencyId, branchId, statusId, paymentSourceId);
            else if (Current.IsClinicianOrHHA) patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id) {
            return PartialView("~/Views/Patient/Edit.ascx", patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Patient patient) {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be edited" };
            if (patient != null && !patient.Id.IsEmpty()) {
                var rules = new List<Validation>();
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
                rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
                rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
                rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
                rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
                if (patient.Status == (int)PatientStatus.Discharged) {
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DischargeDate.ToString()), "Patient Discharge date is required.  <br/>"));
                    rules.Add(new Validation(() => !patient.DischargeDate.ToString().IsValidDate(), "Patient Discharge date is not in valid format.  <br/>"));
                }
                if (patient.PatientIdNumber.IsNotNullOrEmpty()) {
                    bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
                    rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                }
                if (patient.MedicareNumber.IsNotNullOrEmpty()) {
                    bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty()) {
                    bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                if (patient.SSN.IsNotNullOrEmpty()) {
                    bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
                    rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
                }
                if (patient.PrimaryInsurance >= 1000)  rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                if ( patient.SecondaryInsurance >= 1000)  rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                if ( patient.TertiaryInsurance >= 1000) rules.Add(new Validation(() => patient.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid) {
                    patient.AgencyId = Current.AgencyId;
                    patient.Encode();// setting string arrays to one field
                    if (patientService.EditPatient(patient)) {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Data successfully edited";
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the data.";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid id) {
            Check.Argument.IsNotEmpty(id, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeletePatient(id)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Patient could not be restored." };
            if (!id.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(id, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsDeprecated = false;
                    if (patientRepository.Update(patient))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientRestored, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was restored successfully.";
                    }
                    else viewData.errorMessage = "The patient restore failed. Please try again.";
                }
                else viewData.errorMessage = "The patient information could not be found. Please try again.";
            }
            else viewData.errorMessage = "The patient identifier provided is not valid. Please try again.";
            return Json(viewData);
        }
        #endregion

        #region Authorization
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationNew(Guid? patientId)
        {
            if (patientId.HasValue) return PartialView("Authorization/New", (Guid)patientId);
            else return PartialView("Authorization/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AuthorizationCreate([Bind]Authorization authorization) {
            Check.Argument.IsNotNull(authorization, "authorization");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New authorization could not be saved." };
            if (authorization.IsValid) {
                authorization.UserId = Current.UserId;
                authorization.AgencyId = Current.AgencyId;
                authorization.Id = Guid.NewGuid();
                if (patientRepository.AddAuthorization(authorization)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was saved successfully.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = authorization.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationList(Guid patientId)
        {
            return PartialView("Authorization/List", patientRepository.Get(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationGrid(Guid patientId)
        {
            IList<Authorization> authorizations = patientRepository.GetAuthorizations(Current.AgencyId, patientId);
            return View(new GridModel(authorizations));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult AuthorizationPrint(Guid id, Guid patientId)
        {
            Authorization auth = patientRepository.GetAuthorization(Current.AgencyId, patientId, id);
            auth.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            auth.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            return FileGenerator.Pdf<AuthorizationPdf>(new AuthorizationPdf(auth), "Authorization");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AuthorizationPrintPreview(Guid id, Guid patientId)
        {
            var auth = patientRepository.GetAuthorization(Current.AgencyId, patientId, id);
            auth.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            auth.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            return PartialView("Authorization/Print", auth);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationEdit(Guid id, Guid patientId) {
            var auth = patientRepository.GetAuthorization(Current.AgencyId, patientId, id);
            if (auth != null) {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null) auth.DisplayName = patient.DisplayName;
            }
            return PartialView("Authorization/Edit", auth);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AuthorizationUpdate([Bind]Authorization authorization) {
            Check.Argument.IsNotNull(authorization, "authorization");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Edit authorization could not be saved." };
            if (authorization.IsValid) {
                authorization.AgencyId = Current.AgencyId;
                if (patientRepository.EditAuthorization(authorization)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was saved successfully.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = authorization.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AuthorizationDelete(Guid id, Guid patientId) {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Delete authorization is not Successful." };
            if (!id.IsEmpty() && !patientId.IsEmpty()) {
                if (patientRepository.DeleteAuthorization(Current.AgencyId, patientId, id)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, id.ToString(), LogType.Authorization, LogAction.AuthorizationDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was deleted successfully.";
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Communication Note
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteNew(Guid? patientId) {
            if (patientId.HasValue) return PartialView("CommunicationNote/New", (Guid)patientId);
            else return PartialView("CommunicationNote/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteCreate([Bind]CommunicationNote communicationNote) {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved" };
            if (communicationNote.IsValid) {
                if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) {
                    var rules = new List<Validation>();
                    rules.Add(new Validation(() => string.IsNullOrEmpty(communicationNote.SignatureText), "User Signature can't be empty."));
                    rules.Add(new Validation(() => communicationNote.SignatureText.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, communicationNote.SignatureText) : false, "User Signature is not correct."));
                    rules.Add(new Validation(() => !(communicationNote.SignatureDate > DateTime.MinValue), "Signature date is not valid."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid) {
                        communicationNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) communicationNote.Status = (int)ScheduleStatus.NoteCompleted;
                        if (!communicationNote.PhysicianId.IsEmpty()) {
                            var physician = physicianRepository.Get(communicationNote.PhysicianId, Current.AgencyId);
                            if (physician != null) communicationNote.PhysicianData = physician.ToXml();
                        }
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                        return Json(viewData);
                    }
                } else communicationNote.SignatureText = string.Empty;
                communicationNote.Id = Guid.NewGuid();
                communicationNote.UserId = Current.UserId;
                communicationNote.AgencyId = Current.AgencyId;
                if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0) communicationNote.Recipients = communicationNote.RecipientArray.ToXml();
                var newScheduleEvent = new ScheduleEvent {
                    AgencyId = Current.AgencyId,
                    EventId = communicationNote.Id,
                    UserId = communicationNote.UserId,
                    EpisodeId = communicationNote.EpisodeId,
                    PatientId = communicationNote.PatientId,
                    Status = communicationNote.Status,
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = communicationNote.Created,
                    VisitDate = communicationNote.Created,
                    DisciplineTask = (int)DisciplineTasks.CommunicationNote
                };
                if (patientRepository.AddCommunicationNote(communicationNote)) {
                    if (patientRepository.UpdateScheduleEventNew(newScheduleEvent)) {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, Actions.Add, DisciplineTasks.CommunicationNote);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Communication note successfully saved";
                        if (communicationNote.SendAsMessage == true && (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature || communicationNote.Status == (int)ScheduleStatus.NoteCompleted)) {
                            if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0) {
                                var message = new Message {
                                    Type = MessageType.User,
                                    AgencyId = Current.AgencyId,
                                    Body = communicationNote.Text,
                                    Subject = "Communication Note Message",
                                    PatientId = communicationNote.PatientId,
                                    Recipients = communicationNote.RecipientArray
                                };
                                if (messageService.SendMessage(message, null)) viewData.errorMessage = "Communication note successfully saved and sent to receipients";
                                else {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Communication note saved but notification could not be sent to the recipients";
                                }
                            }
                        }
                    } else {
                        patientRepository.RemoveModel<CommunicationNote>(communicationNote.Id);
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error saving the communication note.";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the communication note.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteList(Guid? patientId) {
            return PartialView("CommunicationNote/List", patientService.GetCommunicationNotes(patientId.HasValue ? (Guid)patientId : Guid.Empty, Guid.Empty, 1, patientId.HasValue ? DateTime.MinValue : DateTime.Now.AddDays(-60), patientId.HasValue ? DateTime.MaxValue : DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteGrid(Guid? patientId, Guid? branchId, int? status, DateTime? startDate, DateTime? endDate, string sort) {
            return PartialView("CommunicationNote/ListContent", patientService.GetCommunicationNotes(patientId.HasValue ? (Guid)patientId : Guid.Empty, branchId.HasValue ? (Guid)branchId : Guid.Empty,status.HasValue? (int)status:0, startDate.HasValue ? (DateTime)startDate : DateTime.MinValue, endDate.HasValue ? (DateTime)endDate : DateTime.MaxValue));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CommunicationNoteGet(Guid id, Guid patientId) {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "Id");
            var communicationNote = patientRepository.GetCommunicationNote(id, patientId, Current.AgencyId);
            if (communicationNote != null && !communicationNote.PhysicianId.IsEmpty()) {
                AgencyPhysician physician = null;
                if ((communicationNote.Status == (int)ScheduleStatus.NoteCompleted || communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !communicationNote.PhysicianId.IsEmpty() && communicationNote.PhysicianData.IsNotNullOrEmpty()) {
                    physician = communicationNote.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null) communicationNote.PhysicianName = physician.DisplayName;
                    else {
                        physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                        communicationNote.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                    }
                } else {
                    physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                    communicationNote.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                }
            }
            return Json(communicationNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNotePrintPreview(Guid id, Guid patientId) {
            return PartialView("CommunicationNote/PrintPreview", patientService.GetCommunicationNotePrint(id, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CommunicationNotePrint(Guid? id, Guid? patientId)
        {
            if (id.HasValue && patientId.HasValue)
            {
                return FileGenerator.Pdf<CommunicationNotePdf>(new CommunicationNotePdf(patientService.GetCommunicationNotePrint((Guid)id, (Guid)patientId)), "CommunicationNote");
            }
            else
            {
                return FileGenerator.Pdf<CommunicationNotePdf>(new CommunicationNotePdf(patientService.GetCommunicationNotePrint()), "CommunicationNote");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteEdit(Guid id, Guid patientId) {
            Check.Argument.IsNotEmpty(id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var communicationNote = patientRepository.GetCommunicationNote(id, patientId, Current.AgencyId);
            if (communicationNote != null) {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null) communicationNote.DisplayName = patient.DisplayName;
                communicationNote.RecipientArray = communicationNote.Recipients.IsNotNullOrEmpty() ? communicationNote.Recipients.ToObject<List<Guid>>() : new List<Guid>();
                communicationNote.SignatureDate = DateTime.Now;
                if (!communicationNote.EpisodeId.IsEmpty()) {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId);
                    if (episode != null) {
                        communicationNote.EpisodeEndDate = episode.EndDateFormatted;
                        communicationNote.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
            } else communicationNote = new CommunicationNote();
            return PartialView("CommunicationNote/Edit", communicationNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteUpdate(CommunicationNote communicationNote) {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved" };
            if (communicationNote.IsValid) {
                if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) {
                    var rules = new List<Validation>();
                    rules.Add(new Validation(() => string.IsNullOrEmpty(communicationNote.SignatureText), "User Signature can't be empty."));
                    rules.Add(new Validation(() => communicationNote.SignatureText.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, communicationNote.SignatureText) : false, "User Signature is not correct."));
                    rules.Add(new Validation(() => !(communicationNote.SignatureDate > DateTime.MinValue), "Signature date is not valid."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid) {
                        communicationNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) communicationNote.Status = (int)ScheduleStatus.NoteCompleted;
                        if (!communicationNote.PhysicianId.IsEmpty()) {
                            var physician = physicianRepository.Get(communicationNote.PhysicianId, Current.AgencyId);
                            if (physician != null) communicationNote.PhysicianData = physician.ToXml();
                        }
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                        return Json(viewData);
                    }
                } else communicationNote.SignatureText = string.Empty;
                communicationNote.AgencyId = Current.AgencyId;
                if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0) communicationNote.Recipients = communicationNote.RecipientArray.ToXml();
                var evnt = patientRepository.GetScheduleEventNew(Current.AgencyId, communicationNote.PatientId, communicationNote.EpisodeId, communicationNote.Id);
                if (evnt != null) {
                    var oldStatus = evnt.Status;
                    var oldReason = evnt.ReturnReason;
                    var oldVisitDate = evnt.VisitDate;
                    var oldDiscipline = evnt.Discipline;
                    evnt.VisitDate = communicationNote.Created;
                    evnt.Discipline = Disciplines.ReportsAndNotes.ToString();
                    evnt.Status = communicationNote.Status;
                    evnt.ReturnReason = string.Empty;
                    if (patientRepository.UpdateScheduleEventNew(evnt)) {
                        if (patientRepository.EditCommunicationNote(communicationNote)) {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Communication note successfully saved";
                            if (Enum.IsDefined(typeof(ScheduleStatus), evnt.Status)) Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status, DisciplineTasks.CommunicationNote, string.Empty);
                            if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature || communicationNote.Status == (int)ScheduleStatus.NoteCompleted) {
                                if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0) {
                                    var message = new Message {
                                        Type = MessageType.User,
                                        AgencyId = Current.AgencyId,
                                        Body = communicationNote.Text,
                                        Subject = "Communication Note Message",
                                        PatientId = communicationNote.PatientId,
                                        Recipients = communicationNote.RecipientArray
                                    };
                                    if (messageService.SendMessage(message, null)) viewData.errorMessage = "Communication note successfully saved and sent to recipients";
                                    else {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "Communication note saved but notification could not be sent to the recipients";
                                    }
                                }
                            }
                        } else {
                            evnt.VisitDate = oldVisitDate;
                            evnt.Discipline = oldDiscipline;
                            evnt.Status = oldStatus;
                            evnt.ReturnReason = oldReason;
                            patientRepository.UpdateScheduleEventNew(evnt);
                            viewData.isSuccessful = false;
                        }
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in Saving the data.";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Scheduled Task is not found. Try again.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteDelete(Guid id, Guid patientId) {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeleteCommunicationNote(id, patientId)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in Deleting the data.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNoteProcess(Guid id, Guid patientId, string action, string reason) {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your communication note could not be saved." };
            if (!id.IsEmpty() && !patientId.IsEmpty()) {
                if (action == ButtonAction.Approve.ToString()) {
                    if (patientService.ProcessCommunicationNotes(action, patientId, id, reason)) {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your communication note has been successfully approved.";
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your communication note could not be approved.";
                    }
                } else if (action == ButtonAction.Return.ToString()) {
                    if (patientService.ProcessCommunicationNotes(action, patientId, id, reason)) {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your communication note has been successfully returned.";
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your communication note could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region FaceToFaceEncounter
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceEncounterNew() {
            return PartialView("FaceToFaceEncounter/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceEncounterCreate([Bind]FaceToFaceEncounter faceToFaceEncounter) {
            Check.Argument.IsNotNull(faceToFaceEncounter, "faceToFaceEncounter");
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Face-to-face Encounter could not be saved" };
            if (faceToFaceEncounter.IsValid) {
                faceToFaceEncounter.Id = Guid.NewGuid();
                if (patientService.CreateFaceToFaceEncounter(faceToFaceEncounter)) {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Face-to-face Encounter successfully saved";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Face-to-face Encounter.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = faceToFaceEncounter.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceEncounterPrintPreview(Guid id, Guid patientId) {
            var note = patientService.GetFaceToFacePrint(patientId, id);
            var xml = new FaceToFaceEncounterXml(note);
            note.PrintViewJson = xml.GetJson();
            return View("FaceToFaceEncounter/Print", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult FaceToFaceEncounterPrint(Guid? id, Guid? patientId)
        {
            if (id.HasValue && patientId.HasValue)
            {
                return FileGenerator.Pdf<FaceToFaceEncounterPdf>(new FaceToFaceEncounterPdf(patientService.GetFaceToFacePrint((Guid)patientId, (Guid)id)), "FaceToFaceEncounter");
            }
            else
            {
                return FileGenerator.Pdf<FaceToFaceEncounterPdf>(new FaceToFaceEncounterPdf(patientService.GetFaceToFacePrint()), "FaceToFaceEncounter");
            }
        }
        #endregion

        #region Order
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderNew(Guid? patientId) {
            if (patientId.HasValue) return PartialView("Order/New", (Guid)patientId);
            else return PartialView("Order/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult OrderCreate(PhysicianOrder order) {
            Check.Argument.IsNotNull(order, "order");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New order could not be saved." };
            if (order.IsValid) {
                order.Id = Guid.NewGuid();
                order.UserId = Current.UserId;
                order.AgencyId = Current.AgencyId;
                order.OrderNumber = patientRepository.GetNextOrderNumber();
                order.Created = DateTime.Now;
                if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview) {
                    if (order.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, order.SignatureText)) {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return Json(viewData);
                    } else {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) order.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        if (!order.PhysicianId.IsEmpty()) {
                            var physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null) order.PhysicianData = physician.ToXml();
                        }
                    }
                } else order.SignatureText = string.Empty;
                var newScheduleEvent = new ScheduleEvent {
                    AgencyId = Current.AgencyId,
                    EventId = order.Id,
                    UserId = order.UserId,
                    PatientId = order.PatientId,
                    EpisodeId = order.EpisodeId,
                    Status = order.Status,
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = order.OrderDate,
                    VisitDate = order.OrderDate,
                    DisciplineTask = (int)DisciplineTasks.PhysicianOrder,
                    IsOrderForNextEpisode = order.IsOrderForNextEpisode
                };
                if (patientRepository.AddOrder(order)) {
                    if (patientRepository.UpdateScheduleEventNew(newScheduleEvent)) {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, order.EpisodeId, order.PatientId, order.Id, Actions.Add, (DisciplineTasks)newScheduleEvent.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Order has been saved successfully.";
                        if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) viewData.errorMessage = "Order has been completed successfully.";
                    } else {
                        patientRepository.RemoveModel<PhysicianOrder>(order.Id);
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Order could not be saved! Please try again.";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderHistory(Guid patientId)
        {
            return PartialView("Order/History", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderHistoryGrid(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(patientService.GetPatientOrders(patientId, startDate, endDate)));
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrderPrintPreview(Guid id, Guid patientId) {
            return PartialView("Order/Print", patientService.GetOrderPrint(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrderPrint(Guid? id, Guid? patientId)
        {
            if (id.HasValue && patientId.HasValue)
            {
                return FileGenerator.Pdf<PhysicianOrderPdf>(new PhysicianOrderPdf(patientService.GetOrderPrint((Guid)patientId, (Guid)id)), "Order");
            }
            else
            {
                return FileGenerator.Pdf<PhysicianOrderPdf>(new PhysicianOrderPdf(patientService.GetOrderPrint()), "Order");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderEdit(Guid id, Guid patientId)
        {
            var order = new PhysicianOrder();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null) {
                order = patientRepository.GetOrderOnly(id, patientId, Current.AgencyId);
                if (order != null) {
                    order.DisplayName = patient.DisplayName;
                    if (!order.EpisodeId.IsEmpty()) {
                        var episode = patientRepository.GetEpisodeById(Current.AgencyId, order.EpisodeId, order.PatientId);
                        if (episode != null) {
                            order.EpisodeEndDate = episode.EndDateFormatted;
                            order.EpisodeStartDate = episode.StartDateFormatted;

                        }
                    }
                }
            }
            return PartialView("Order/Edit", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderUpdate(PhysicianOrder order) {
            Check.Argument.IsNotNull(order, "order");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated." };
            if (order.IsValid) {
                order.AgencyId = Current.AgencyId;
                if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview) {
                    if (order.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, order.SignatureText)) {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return Json(viewData);
                    } else {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) order.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        if (!order.PhysicianId.IsEmpty()) {
                            var physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null) order.PhysicianData = physician.ToXml();
                        }
                    }
                } else order.SignatureText = string.Empty;
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
                if (scheduleEvent != null) {
                    var oldStatus = scheduleEvent.Status;
                    var oldReason = scheduleEvent.ReturnReason;
                    var oldIsOrderForNextEpisode = scheduleEvent.IsOrderForNextEpisode;
                    var oldVisitDate = scheduleEvent.VisitDate;

                    scheduleEvent.VisitDate = order.OrderDate;
                    scheduleEvent.Status = order.Status;
                    scheduleEvent.ReturnReason = string.Empty;
                    scheduleEvent.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) {
                        if (patientRepository.UpdateOrder(order)) {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Order has been updated successfully.";
                            if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                            {
                                viewData.errorMessage = "Order has been completed and pending QA Review.";
                            }
                            if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.Edit, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
                            }
                        }
                        else
                        {
                            scheduleEvent.VisitDate = oldVisitDate;
                            scheduleEvent.Status = oldStatus;
                            scheduleEvent.ReturnReason = oldReason;
                            scheduleEvent.IsOrderForNextEpisode = oldIsOrderForNextEpisode;
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Order could not be saved! Please try again.";
                        }
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Order could not be saved! Please try again.";
                    }
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult OrderUpdateStatus(Guid id, Guid episodeId, Guid patientId, string orderType, string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Order could not been updated." };
            if (orderType == "PhysicianOrder")
            {
                if (patientService.ProcessPhysicianOrder(id, episodeId, patientId, actionType, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            else if (orderType == "PlanofCare" || orderType == "PlanofCareStandAlone")
            {
                if (assessmentService.UpdatePlanofCareStatus(id, episodeId, patientId, actionType, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteOrder(Guid id, Guid patientId) {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be deleted." };
            if (!id.IsEmpty() && !patientId.IsEmpty()) {
                if (patientService.DeletePhysicianOrder(id, patientId)) {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Order has been deleted successfully.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be deleted! Please try again.";
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Other Views
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedList()
        {
            return View("Deleted/Main", patientRepository.AllDeleted(Current.AgencyId, Guid.Empty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedGrid(Guid branchId)
        {
            return View("Deleted/Grid", patientRepository.AllDeleted(Current.AgencyId, branchId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonAdmissionList()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult NonAdmissionGrid()
        {
            var nonAdmitList = patientService.GetNonAdmits();
            return View(new GridModel(nonAdmitList));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTasksList(Guid id)
        {
            return PartialView("DeletedTasksList", patientRepository.GetPatientOnly(id, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTasksGrid(Guid id)
        {
            return View(new GridModel(patientRepository.GetDeletedItemsNew(Current.AgencyId, id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummaryList(Guid id)
        {
            return PartialView("SixtyDaySummaryList", patientRepository.GetPatientOnly(id, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummaryGrid(Guid id)
        {
            return View(new GridModel(patientService.GetSixtyDaySummary((id))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VitalSignsList(Guid id)
        {
            ViewData["SortColumn"] = "VisitDate";
            ViewData["SortDirection"] = "ASC";
            ViewData["PatientId"] = id;
            var patient = patientRepository.GetPatientOnly(id, Current.AgencyId);
            if (patient != null) ViewData["DisplayName"] = patient.DisplayName;
            return PartialView("VitalSigns/Main", id.IsEmpty() ? new List<VitalSign>() : patientService.GetPatientVitalSigns(id, DateTime.Now.AddDays(-60), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VitalSignsGrid(Guid PatientId, DateTime StartDate, DateTime EndDate, string sort)
        {
            if (sort.IsNotNullOrEmpty())
            {
                var paramArray = sort.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            ViewData["PatientId"] = PatientId;
            return PartialView("VitalSigns/Content", PatientId.IsEmpty() ? new List<VitalSign>() : patientService.GetPatientVitalSigns(PatientId, StartDate, EndDate));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Eligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender) {
            return PartialView(patientService.VerifyEligibility(medicareNumber, lastName, firstName, dob, gender));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Info(Guid? id) {
            if (id.HasValue) return PartialView(patientRepository.Get((Guid)id, Current.AgencyId));
            else return PartialView(new Patient());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoPopup(Guid id) {
            return PartialView("Popup", patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Map(Guid id) {
            return View(patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatusEdit(Guid id)
        {
            return PartialView(patientRepository.GetPatientOnly(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatusUpdate([Bind] PendingPatient patient)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The patient status could not be changed. Please try again." };
            if (!patient.Id.IsEmpty())
            {
                patient.AgencyId = Current.AgencyId;
                if (patient.Status == (int)PatientStatus.Active)
                {
                    if (patientService.ActivatePatient(patient.Id))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully activated.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Discharged)
                {
                    if (patientService.DischargePatient(patient.Id, patient.DateOfDischarge, patient.Comments))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully discharged.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Pending)
                {
                    if (patientService.SetPatientPending(patient.Id))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to pending successfully.";
                    }
                }
                if (patient.Status == (int)PatientStatus.NonAdmission)
                {
                    if (patientService.NonAdmitPatient(patient))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to non-admit successfully.";
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Charts
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Charts()
        {
            var viewData = new PatientCenterViewData();
            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsOfficeManager || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) viewData.Count = patientRepository.GetPatientStatusCount(Current.AgencyId, (int)PatientStatus.Active);
            else if (Current.IsClinicianOrHHA) viewData.Count = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).Count;
            else viewData.Count = 0;
            return PartialView("Charts/Layout", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChartsContent(Guid patientId)
        {
            var viewData = new PatientCenterViewData();
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("Charts/Content", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChartsInfo(Guid patientId)
        {
            return PartialView("Charts/Info", patientRepository.Get(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChartsActivitiesGrid(Guid patientId, string discipline, string dateRangeId, string rangeStartDate, string rangeEndDate)
        {
            var dateRange = new DateRange { Id = dateRangeId };
            if (dateRangeId == "DateRange") {
                dateRange.EndDate = rangeEndDate.IsDate() ? rangeEndDate.ToDateTime() : DateTime.Now;
                dateRange.StartDate = rangeStartDate.IsDate() ? rangeStartDate.ToDateTime() : DateTime.Now.AddDays(-60);
            } else dateRange = dateService.GetDateRange(dateRangeId, patientId);
            return View(new GridModel(patientService.GetScheduledEventsNew(patientId, discipline, dateRange)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChartsPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId);
            else if (Current.IsClinicianOrHHA) patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }
        #endregion

        #region Order Management
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetOrder(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return Json(patientRepository.GetOrderOnly(Id, patientId, Current.AgencyId));
        }
        #endregion

        #region Profile
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ProfilePrintPreview(Guid id)
        {
            return View("Profile", patientService.GetProfile(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ProfilePrint(Guid id)
        {
            return FileGenerator.Pdf<PatientProfilePdf>(new PatientProfilePdf(patientService.GetProfile(id)), "PatientProfile");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TriageClassificationPrintPreview(Guid id)
        {
            return View("TriageClassification", patientService.GetProfile(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult TriageClassificationPrint(Guid id)
        {
            return FileGenerator.Pdf<TriageClassificationPdf>(new TriageClassificationPdf(patientService.GetProfile(id)), "TriageClassification");
        }
        #endregion

        #region Photo
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhotoEdit(Guid id)
        {
            return PartialView("Photo/Edit", patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhotoUpdate([Bind] Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient photo could not be saved." };
            if (patientService.IsValidImage(Request.Files) && patientService.AddPhoto(id, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient photo successfully updated";
            }
            else viewData.errorMessage = "File uploaded is not a valid image.";
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PhotoDelete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient photo could not be saved." };
            var patient = patientRepository.GetPatientOnly(id, Current.AgencyId);
            if (patient != null && patientService.UpdatePatientForPhotoRemove(patient))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient photo successfully removed.";
            }
            return Json(viewData);
        }
        #endregion

        #region Emergency Contact
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContactNew(Guid patientId)
        {
            return PartialView("EmergencyContact/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContactCreate(Guid patientId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(patientId, "PatientId");
            var viewData = Validate<JsonViewData>(
                new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. "),
                new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required. "),
                new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format. "),
                new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required. ")
            );
            if (viewData.isSuccessful)
            {
                if (patientService.NewEmergencyContact(emergencyContact, patientId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully added";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the data.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContactGet(Guid id, Guid patientId)
        {
            return Json(patientRepository.GetEmergencyContact(patientId, id));
        }

        [GridAction]
        public ActionResult EmergencyContactGrid(Guid patientId)
        {
            return Json(new GridModel { Data = patientRepository.GetEmergencyContacts(patientId) });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContactEdit(Guid id, Guid patientId)
        {
            return PartialView("EmergencyContact/Edit", patientRepository.GetEmergencyContact(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContactUpdate(Guid id, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            var viewData = Validate<JsonViewData>(
                new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. "),
                new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required. "),
                new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format. "),
                new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required. ")
            );
            if (viewData.isSuccessful)
            {
                if (patientService.EditEmergencyContact(emergencyContact))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Emergency contact successfully updated.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Emergency contact could not be updated. Please try again.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmergencyContactDelete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeleteEmergencyContact(id, patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }
        #endregion

        #region Physician
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianAdd(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your data is not added. Try Again." };
            if (!physicianRepository.DoesPhysicianExist(patientId, id))
            {
                if (patientService.LinkPhysician(patientId, id, false))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your data is successfully added.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician already exists.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianGet(Guid id, Guid patientId)
        {
            return Json(physicianRepository.GetByPatientId(id, patientId, Current.AgencyId));
        }

        [GridAction]
        public ActionResult PhysicianGrid(Guid patientId)
        {
            return Json(new GridModel(physicianRepository.GetPatientPhysicians(patientId, Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianSetPrimary(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "PatientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your physician could not be set as primary." };
            var result = physicianRepository.SetPrimary(patientId, id);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "This physician is now the primary physician";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your physician is not successfully set primary.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianDelete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your data is not deleted. Try Again." };
            if (patientService.UnlinkPhysician(patientId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in deleting the data.";
            }
            return Json(viewData);
        }
        #endregion

        #region Medication
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationNew(Guid profileId)
        {
            return PartialView("Medication/New", profileId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationCreate(Guid profileId, [Bind] Medication medication, string medicationType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new medication could not be added to the medication profile." };
            if (medication != null)
            {
                if (patientService.AddMedication(profileId, medication, medicationType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The new medication was added to the medication profile successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationProfile(Guid patientId)
        {
            var viewData = new MedicationProfileViewData();
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                viewData.MedicationProfile = medicationProfile;
                viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
                viewData.Allergies = patientService.GetAllergies(patientId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null) viewData.PhysicianId = physician.Id;
                    }
                    if (medicationProfile.PharmacyName.IsNotNullOrEmpty()) viewData.PharmacyName = medicationProfile.PharmacyName;
                    else viewData.PharmacyName = viewData.Patient.PharmacyName;
                    if (medicationProfile.PharmacyPhone.IsNotNullOrEmpty()) viewData.PharmacyPhone = medicationProfile.PharmacyPhone;
                    else viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                    if (currentEpisode != null)
                    {
                        viewData.EpisodeId = currentEpisode.Id;
                        viewData.StartDate = currentEpisode.StartDate;
                        viewData.EndDate = currentEpisode.EndDate;
                        if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                        {
                            var assessment = patientService.GetEpisodeAssessment(currentEpisode.Id, patientId);
                            if (assessment != null)
                            {
                                var diagnosis = assessmentService.Diagnosis(assessment);
                                if (diagnosis != null)
                                {
                                    diagnosis.Merge<string, Question>(assessmentService.Allergies(assessment));
                                    viewData.Questions = diagnosis;
                                }
                                else viewData.Questions = assessmentService.Allergies(assessment);
                            }
                        }
                    }
                }
            }
            return PartialView("Medication/Profile", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationProfileGrid(Guid profileId)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(profileId, Current.AgencyId);
            return View("Medication/List", medicationProfile);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationProfilePrintPreview(Guid patientId)
        {
            return PartialView("Medication/ProfilePrint", patientService.GetMedicationProfilePrint(patientId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult MedicationProfilePrint(Guid patientId)
        {
            return FileGenerator.Pdf<MedicationProfilePdf>(new MedicationProfilePdf(patientService.GetMedicationProfilePrint(patientId)), "MedProfile");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationInteractions(Guid profileId)
        {
            return View("Medication/Interactions", patientRepository.GetMedicationProfile(profileId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MedicationInteractionsPrint(Guid patientId, List<string> drugsSelected)
        {
            return FileGenerator.Pdf<DrugDrugInteractionsPdf>(new DrugDrugInteractionsPdf(patientService.GetDrugDrugInteractionsPrint(patientId, drugsSelected)), "DrugDrugInteractions");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationEdit(Guid id, Guid profileId)
        {
            var medication = new Medication();
            var medicationProfile = patientRepository.GetMedicationProfile(profileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medication = medicationProfile.Medication.ToObject<List<Medication>>().SingleOrDefault(m => m.Id == id);
                medication.ProfileId = medicationProfile.Id;
            }
            return PartialView("Medication/Edit", medication);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationUpdate([Bind] Medication medication, string medicationType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The medication could not be updated. Please try again." };
            if (medication != null)
            {
                if (patientService.UpdateMedication(medication.ProfileId, medication, medicationType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The medication was updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationDiscontinue(Guid id, Guid profileId)
        {
            var medication = new Medication();
            var medicationProfile = patientRepository.GetMedicationProfile(profileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medication = medicationProfile.Medication.ToObject<List<Medication>>().SingleOrDefault(m => m.Id == id);
                medication.ProfileId = medicationProfile.Id;
            }
            return PartialView("Medication/Discontinue", medication);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationUpdateStatus(Guid id, Guid profileId, string medicationCategory, DateTime dischargeDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The medication could not be updated. Please try again." };
            if (patientService.UpdateMedicationStatus(profileId, id, medicationCategory, dischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The medication was updated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationDelete(Guid id, Guid profileId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Medication could not be deleted from the Medication Profile." };
            if (patientService.DeleteMedication(profileId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Medication was deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationLogs(Guid patientId)
        {
            return PartialView("ActivityLogs", patientService.GetMedicationLogs(LogDomain.Patient, LogType.Patient, patientId));
        }
        #endregion

        #region Medication Snapshot
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotNew(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var viewData = new MedicationProfileViewData();
            if (medProfile != null)
            {
                viewData.MedicationProfile = medProfile;
                viewData.Allergies = patientService.GetAllergies(patientId);
                viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null) viewData.PhysicianId = physician.Id;
                    }
                    viewData.PharmacyName = viewData.Patient.PharmacyName;
                    viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                }
                var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                if (currentEpisode != null)
                {
                    viewData.EpisodeId = currentEpisode.Id;
                    if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                    {
                        var assessment = patientService.GetEpisodeAssessment(currentEpisode.Id, patientId);
                        if (assessment != null)
                        {
                            var diagnosis = assessmentService.Diagnosis(assessment);
                            if (diagnosis != null && diagnosis.Count > 0) viewData.Questions = diagnosis;
                        }
                    }
                }
            }
            return PartialView("Medication/Snapshot/New", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotCreate(MedicationProfileHistory medicationProfileHistory)
        {
            Check.Argument.IsNotNull(medicationProfileHistory, "medicationProfileHistory");
            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Medication Profile Snapshot could not be created. Try Again." };
            viewData = Validate<JsonViewData>(
                new Validation(() => string.IsNullOrEmpty(medicationProfileHistory.Signature), "The signature field is empty."),
                new Validation(() => !userService.IsSignatureCorrect(Current.UserId, medicationProfileHistory.Signature), "The signature provided is not correct."),
                new Validation(() => !medicationProfileHistory.SignedDate.IsValid(), "The signature date is not valid.")
            );
            if (viewData.isSuccessful)
            {
                if (patientService.SignMedicationHistory(medicationProfileHistory))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Medication Profile Snapshot was created successfully.";
                }
                else viewData.isSuccessful = false;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult MedicationSnapshotPrint(Guid id)
        {
            return FileGenerator.Pdf<MedicationProfilePdf>(new MedicationProfilePdf(patientService.GetMedicationSnapshotPrint(id)), "MedProfile");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationSnapshotPrintPreview(Guid id)
        {
            return PartialView("Medication/ProfilePrint", patientService.GetMedicationSnapshotPrint(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotList(Guid patientId)
        {
            return PartialView("Medication/Snapshot/List", patientRepository.Get(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotGrid(Guid patientId)
        {
            return View(new GridModel(patientService.GetMedicationHistoryForPatient(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotEdit(Guid id)
        {
            return PartialView("Medication/Snapshot/Edit", patientRepository.GetMedicationProfileHistory(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotUpdate(Guid id, DateTime signedDate)
        {
            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Medication Profile Snapshot could not be created. Try Again." };
            var medicationProfileSnapShot = patientRepository.GetMedicationProfileHistory(id, Current.AgencyId);
            if (medicationProfileSnapShot != null)
            {
                medicationProfileSnapShot.SignedDate = signedDate;
                if (patientRepository.UpdateMedicationProfileHistory(medicationProfileSnapShot))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Medication Profile Snapshot was updated successfully.";
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfileSnapShot.PatientId, medicationProfileSnapShot.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationHistoryUpdated, string.Empty);
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotDelete(Guid Id)
        {
            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Medication Profile Snapshot could not be deleted." };
            var medicationProfileSnapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (medicationProfileSnapShot != null)
            {
                medicationProfileSnapShot.IsDeprecated = true;
                if (patientRepository.UpdateMedicationProfileHistory(medicationProfileSnapShot))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Medication Profile Snapshot was successfully deleted.";
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfileSnapShot.PatientId, medicationProfileSnapShot.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationHistoryDeleted, string.Empty);
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Allergy
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllergyNew(Guid profileId)
        {
            return PartialView("Allergy/New", profileId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AllergyCreate([Bind] Allergy allergy)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new allergy could not be added to the allergy profile." };
            if (allergy != null)
            {
                if (patientService.AddAllergy(allergy))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The new allergy was added to the allergy profile successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllergyProfile(Guid patientId)
        {
            var viewData = new AllergyProfileViewData();
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                viewData.AllergyProfile = allergyProfile;
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null) viewData.PhysicianId = physician.Id;
                    }
                }
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("Allergy/Profile", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllergyProfilePrintPreview(Guid id)
        {
            return View("Allergy/ProfilePrint", patientService.GetAllergyProfilePrint(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult AllergyProfilePrint(Guid id)
        {
            return FileGenerator.Pdf<AllergyProfilePdf>(new AllergyProfilePdf(patientService.GetAllergyProfilePrint(id)), "AllergyProfile");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllergyProfileGrid(Guid profileId)
        {
            return PartialView("Allergy/List", patientRepository.GetAllergyProfile(profileId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllergyEdit(Guid id, Guid profileId)
        {
            var allergy = new Allergy();
            var allergyProfile = patientRepository.GetAllergyProfile(profileId, Current.AgencyId);
            if (allergyProfile != null)
            {
                allergy = allergyProfile.Allergies.ToObject<List<Allergy>>().SingleOrDefault(a => a.Id == id);
                allergy.ProfileId = allergyProfile.Id;
            }
            return PartialView("Allergy/Edit", allergy);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AllergyUpdate([Bind] Allergy allergy)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy could not be updated. Please try again." };
            if (allergy != null)
            {
                if (patientService.UpdateAllergy(allergy))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Allergy was updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AllergyUpdateStatus(Guid id, Guid profileId, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy status could not be updated." };
            if (patientService.UpdateAllergy(profileId, id, isDeprecated))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Allergy was updated successfully.";
            }
            return Json(viewData);
        }
        #endregion

        #region Pending Admissions
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmitNew(Guid id, NonAdmitTypes type)
        {
            Check.Argument.IsNotNull(id, "id");
            PendingPatient pending = null;
            if (type == NonAdmitTypes.Patient)
            {
                var patient = patientRepository.Get(id, Current.AgencyId);
                if (patient != null)
                {
                    pending = new PendingPatient
                    {
                        Id = patient.Id,
                        DisplayName = patient.DisplayName,
                        PatientIdNumber = patient.PatientIdNumber,
                        StartofCareDate = patient.StartofCareDate,
                        ReferralDate = patient.ReferralDate,
                        CaseManagerId = patient.CaseManagerId,
                        PrimaryInsurance = patient.PrimaryInsurance,
                        SecondaryInsurance = patient.SecondaryInsurance,
                        TertiaryInsurance = patient.TertiaryInsurance,
                        Type = NonAdmitTypes.Patient,
                        Payer = patient.Payer,
                        UserId = patient.UserId
                    };
                }
            }
            else
            {
                var referral = referralRepository.Get(Current.AgencyId, id);
                if (referral != null)
                {
                    pending = new PendingPatient
                    {
                        Id = referral.Id,
                        DisplayName = referral.DisplayName,
                        PatientIdNumber = string.Empty,
                        StartofCareDate = DateTime.Today,
                        ReferralDate = referral.ReferralDate,
                        PrimaryInsurance = 0,
                        SecondaryInsurance = 0,
                        TertiaryInsurance = 0,
                        Type = NonAdmitTypes.Referral,
                        UserId = referral.UserId
                    };
                }
            }
            return PartialView("PendingAdmission/Admit", pending);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AdmitCreate([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            patient.AgencyId = Current.AgencyId;
            if (patient.IsValid)
            {
                if (patientService.AdmitPatient(patient))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Patient has been admitted successfully.";
                }
            }
            else viewData.errorMessage = patient.ValidationMessage;
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonAdmitNew(Guid id)
        {
            return PartialView("PendingAdmission/NonAdmit", patientRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NonAdmitCreate([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Non-Admission of patient could not be saved." };
            patient.AgencyId = Current.AgencyId;
            if (patientService.NonAdmitPatient(patient))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient non-admission successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingAdmissionList()
        {
            return PartialView("PendingAdmission/List");
        }

        [GridAction]
        public ActionResult PendingAdmissionGrid()
        {
            var patientList = patientService.GetPendingPatients();
            return View(new GridModel(patientList));
        }
        #endregion

        #region Hospitalization
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationList()
        {
            return PartialView("Hospitalization/List");
        }

        [GridAction]
        public ActionResult HospitalizationGrid()
        {
            var patientList = patientRepository.GetHospitalizedPatients(Current.AgencyId);
            if (patientList != null && patientList.Count > 0)
            {
                patientList.ForEach(d =>
                {
                    d.User = !d.UserId.IsEmpty() ? UserEngine.GetName(d.UserId, Current.AgencyId) : string.Empty;
                });
            }
            return View(new GridModel(patientList));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLogs(Guid patientId)
        {
            var viewData = new HospitalizationViewData();
            viewData.Logs = patientService.GetHospitalizationLogs(Current.AgencyId, patientId);
            viewData.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            return View("Hospitalization/Logs", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationNew(Guid? patientId)
        {
            return PartialView("Hospitalization/New", patientId.HasValue ? (Guid)patientId : Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HospitalizationCreate(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new hospitalization log could not be added." };
            if (patientService.AddHospitalizationLog(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The new hospitalization log was added successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationEdit(Guid id, Guid patientId)
        {
            return PartialView("Hospitalization/Edit", patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HospitalizationUpdate(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Hospitalization Log could not be updated. Please try again." };
            if (patientService.UpdateHospitalizationLog(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Hospitalization Log was updated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult HospitalizationPrint(Guid id, Guid patientId)
        {
            return FileGenerator.Pdf<HospitalizationLogPdf>(new HospitalizationLogPdf(patientService.GetHospitalizationLog(patientId, id)), "HospitalizationLog");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HospitalizationPrintPreview(Guid id, Guid patientId)
        {
            var log = patientService.GetHospitalizationLog(patientId, id);
            var xml = new HospitalizationLogXml(log);
            log.PrintViewJson = xml.GetJson();
            return PartialView("Hospitalization/Print", log);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HospitalizationDelete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Hospitalization Log could not be deleted." };
            var transferLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, id);
            if (transferLog != null)
            {
                transferLog.IsDeprecated = true;
                if (patientRepository.UpdateHospitalizationLog(transferLog))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospitalization Log was updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLogsGrid(Guid patientId)
        {
            return PartialView("Hospitalization/Grid", patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId));
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetNote(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new NoteViewData { isSuccessful = false };
            var patientNote = patientRepository.GetNote(patientId);
            if (patientNote != null)
            {
                viewData.Id = patientNote.Id;
                viewData.Note = patientNote.Note;
                viewData.PatientId = patientNote.PatientId;
                viewData.isSuccessful = true;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Note(Guid patientId, string patientNote)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Note Is Not Saved. Try Again." };
            var guid = patientRepository.Note(patientId, patientNote);
            if (!guid.IsEmpty())
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Note Successfully Saved.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DateRange(string dateRangeId, Guid patientId)
        {
            return Json(dateService.GetDateRange(dateRangeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Readmit(Guid patientId)
        {
            return PartialView(patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientReadmit(Guid PatientId, DateTime ReadmissionDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient Re-admission is unsuccessful." };
            if (patientService.ActivatePatient(PatientId, ReadmissionDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient Re-admission is successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(int status, Guid branchId)
        {
            return Json(patientRepository.Find(status, branchId, Current.AgencyId).OrderBy(s => s.DisplayName.ToUpperCase()).Select(p => new { Id = p.Id, Name = p.DisplayName }).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeOrdersView(Guid episodeId, Guid patientId)
        {
            return PartialView("EpisodeOrders", patientRepository.GetPatientEpisodeFluent(Current.AgencyId, episodeId, patientId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeOrders(Guid episodeId, Guid patientId)
        {
            return View(new GridModel(patientService.GetEpisodeOrders(episodeId, patientId))); 
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityReports(Guid patientId)
        {
            return View(new GridModel(patientService.GetMedicareEligibilityLists(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityList(Guid patientId)
        {
            return PartialView("EligibilityList", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibility(Guid medicareEligibilityId, Guid patientId)
        {
            var result = new PatientEligibility();
            var medicareEligibility = patientRepository.GetMedicareEligibility(Current.AgencyId, patientId, medicareEligibilityId);
            if (medicareEligibility != null && medicareEligibility.Result.IsNotNullOrEmpty())
            {
                var javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Deserialize<PatientEligibility>(medicareEligibility.Result);
                string npi = result != null && result.Episode != null && result.Episode.reference_id.IsNotNullOrEmpty() ? result.Episode.reference_id : null;
                if (npi != null)
                {
                    var OtherAgencyData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(npi);
                    if (OtherAgencyData != null)
                    {
                        result.Other_Agency_Data.name = OtherAgencyData.ProviderOrganizationName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderOrganizationName : string.Empty;
                        result.Other_Agency_Data.address1 = OtherAgencyData.ProviderFirstLineBusinessPracticeLocationAddress.IsNotNullOrEmpty() ? OtherAgencyData.ProviderFirstLineBusinessPracticeLocationAddress : string.Empty;
                        result.Other_Agency_Data.address2 = OtherAgencyData.ProviderSecondLineBusinessPracticeLocationAddress.IsNotNullOrEmpty() ? OtherAgencyData.ProviderSecondLineBusinessPracticeLocationAddress : string.Empty;
                        result.Other_Agency_Data.city = OtherAgencyData.ProviderBusinessPracticeLocationAddressCityName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressCityName : string.Empty;
                        result.Other_Agency_Data.state = OtherAgencyData.ProviderBusinessMailingAddressStateName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessMailingAddressStateName : string.Empty;
                        result.Other_Agency_Data.zip = OtherAgencyData.ProviderBusinessMailingAddressPostalCode.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessMailingAddressPostalCode : string.Empty;
                        result.Other_Agency_Data.phone = OtherAgencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber : string.Empty;
                        result.Other_Agency_Data.fax = OtherAgencyData.ProviderBusinessPracticeLocationAddressFaxNumber.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressFaxNumber : string.Empty;
                    }
                }
            }
            return PartialView("Eligibility", result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MedicareEligibilityPdf([Bind]PatientEligibility Eligibility)
        {
            return FileGenerator.Pdf<MedicareEligibilityReportPdf>(new MedicareEligibilityReportPdf(Eligibility, agencyRepository.GetWithBranches(Current.AgencyId), null), "MedicareEligibility");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MedicareEligibilityReportPdf(Guid patientId, Guid mcareEligibilityId)
        {
            PatientEligibility pEligibility = null;
            var eligibility = patientRepository.GetMedicareEligibility(Current.AgencyId, patientId, mcareEligibilityId);
            if (eligibility != null && eligibility.Result.IsNotNullOrEmpty())
            {
                pEligibility = eligibility.Result.FromJson<PatientEligibility>();
            }
            return FileGenerator.Pdf<MedicareEligibilityReportPdf>(new MedicareEligibilityReportPdf(pEligibility, agencyRepository.GetWithBranches(Current.AgencyId), patientRepository.GetPatientOnly(patientId, Current.AgencyId)), "MedicareEligibility");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientLogs(Guid patientId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Patient, LogType.Patient, patientId, patientId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NextStep()
        {
            return PartialView("NextStep");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedDates(Guid patientId)
        {
            return PartialView("ManagedDates", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionPeriod(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                ViewData["DisplayName"] = patient.DisplayName;
                ViewData["AdmissionId"] = patient.AdmissionId;
                ViewData["PatientId"] = patient.Id;
                ViewData["PatientStatus"] = patient.Status;
            }
            else
            {
                ViewData["DisplayName"] = string.Empty;
                ViewData["AdmissionId"] = Guid.Empty;
                ViewData["PatientId"] = Guid.Empty;
                ViewData["PatientStatus"] = 0;
            }
            return PartialView("PatientAdmissionPeriod", patientRepository.PatientAdmissonPeriods(Current.AgencyId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionPeriodContent(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                ViewData["AdmissionId"] = patient.AdmissionId;
                ViewData["PatientStatus"] = patient.Status;
            }
            else
            {
                ViewData["AdmissionId"] = Guid.Empty;
                ViewData["PatientStatus"] = 0;
            }
            return PartialView("PatientAdmissionPeriodContent", patientRepository.PatientAdmissonPeriods(Current.AgencyId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeletePatientAdmission(Guid patientId, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be deleted." };
            if (!patientId.IsEmpty() && !Id.IsEmpty())
            {
                var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                if (admission != null)
                {
                    admission.IsDeprecated = true;
                    admission.IsActive = false;
                    if (patientRepository.UpdatePatientAdmissionDateModal(admission))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodDeleted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient admission period is deleted ";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The patient admission period is not deleted ";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The patient admission period not found to delete. Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The patient admission period infomation is not right. Try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceInfoContent(Guid PatientId, string InsuranceId, string Action, string InsuranceType)
        {
            var info = new PatientInsuranceInfoViewData();
            if (InsuranceId.IsNotNullOrEmpty() && InsuranceId.IsInteger())
            {
                info = patientService.PatientInsuranceInfo(PatientId, InsuranceId.ToInteger(), InsuranceType);
                if (info != null)
                {
                    info.ActionType = Action;
                }
            }
            return PartialView("InsuranceInfoContent", info);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionPatientInfo(Guid patientId, Guid Id, string Type)
        {
            var admissionPatient = new Patient();
            ViewData["IsDischarge"] = false;
            if (!patientId.IsEmpty() && Type.IsNotNullOrEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    if (Type.IsEqual("edit") && !Id.IsEmpty())
                    {
                        var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                        if (admission != null && admission.PatientData.IsNotNullOrEmpty())
                        {
                            admissionPatient = admission.PatientData.ToObject<Patient>() ?? new Patient();
                            admissionPatient.AdmissionId = admission.Id;
                            admissionPatient.Id = admission.PatientId;
                            admissionPatient.StartofCareDate = admission.StartOfCareDate;
                            admissionPatient.DischargeDate = admission.DischargedDate;
                            ViewData["IsDischarge"] = !patient.AdmissionId.IsEmpty() && (patient.AdmissionId != patient.AdmissionId || (patient.AdmissionId == patient.AdmissionId && patient.Status == (int)PatientStatus.Discharged));
                        }
                    }
                    else if (Type.IsEqual("new"))
                    {
                        admissionPatient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                        admissionPatient.AdmissionId = Guid.Empty;
                    }
                }
            }
            ViewData["Type"] = Type;
            return PartialView("AdmissionPatientInfo", admissionPatient);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionPatientEdit([Bind] Patient patient)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient Admission information could not be edited" };
            if (patient != null && !patient.Id.IsEmpty() && !patient.AdmissionId.IsEmpty())
            {
                var currentPatientData = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
                if (currentPatientData != null)
                {
                    var rules = new List<Validation>();
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
                    rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
                    rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
                    rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
                    rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
                    rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
                  
                    if (patient.PatientIdNumber.IsNotNullOrEmpty())
                    {
                        bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
                        rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                    }
                    if (patient.MedicareNumber.IsNotNullOrEmpty())
                    {
                        bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                        rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                    }
                    if (patient.MedicaidNumber.IsNotNullOrEmpty())
                    {
                        bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                        rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                    }
                    if (patient.SSN.IsNotNullOrEmpty())
                    {
                        bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
                        rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
                    }
                    if ( patient.PrimaryInsurance >= 1000)
                    {
                        rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                    }
                    if (patient.SecondaryInsurance >= 1000)
                    {
                        rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                    }

                    if ( patient.TertiaryInsurance >= 1000)
                    {
                        rules.Add(new Validation(() => patient.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                    }
                    if (!currentPatientData.AdmissionId.IsEmpty() &&  (currentPatientData.AdmissionId != patient.AdmissionId || ( currentPatientData.AdmissionId == patient.AdmissionId && currentPatientData.Status==(int)PatientStatus.Discharged)))
                    {
                        rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DischargeDate.ToString()), "Patient discharge date is required.  <br/>"));
                        rules.Add(new Validation(() => !patient.DischargeDate.ToString().IsValidDate(), "Patient discharge date is not in valid format.  <br/>"));
                        rules.Add(new Validation(() => patient.StartofCareDate.Date > patient.DischargeDate.Date, "Patient discharge date has to be greater than Start of care date .  <br/>"));
                    }
                    rules.Add(new Validation(() => !patientService.IsValidAdmissionPeriod(patient.AdmissionId, patient.Id, patient.StartofCareDate, patient.DischargeDate), "Admission period (SOC and DC date range)  is not in the valid date range."));

                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {

                        patient.AgencyId = Current.AgencyId;
                        patient.Encode();// setting string arrays to one field
                        if (patientRepository.PatientAdmissionEdit(patient))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your data successfully edited.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in editing the data.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Patient data not found. Try again.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AdmissionPatientNew([Bind] Patient patient)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient Admission information could not be edited" };
            if (patient != null && !patient.Id.IsEmpty())
            {
                var rules = new List<Validation>();

                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
                rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of Birth  for the patient is not in the valid range.  <br/>"));
                rules.Add(new Validation(() => patient.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
                if (patient.PatientIdNumber.IsNotNullOrEmpty())
                {
                    bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
                    rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                }
                if (patient.MedicareNumber.IsNotNullOrEmpty())
                {
                    bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty())
                {
                    bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                if (patient.SSN.IsNotNullOrEmpty())
                {
                    bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
                    rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
                }
                rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
                rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DischargeDate.ToString()), "Patient discharge date is required.  <br/>"));
                rules.Add(new Validation(() => !patient.DischargeDate.ToString().IsValidDate(), "Patient discharge date is not in valid format.  <br/>"));

                if (patient.Ethnicities.IsNullOrEmpty())
                {
                    rules.Add(new Validation(() => patient.EthnicRaces.Count == 0, "Patient Race/Ethnicity is required."));
                }

                if (patient.PaymentSource.IsNullOrEmpty())
                {
                    rules.Add(new Validation(() => patient.PaymentSources.Count == 0, "Patient Payment Source is required."));
                }
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
                rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
               
            
                if ( patient.PrimaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (patient.SecondaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }

                if ( patient.TertiaryInsurance >= 1000)
                {
                    rules.Add(new Validation(() => patient.TertiaryHealthPlanId.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
                rules.Add(new Validation(() => !patientService.IsValidAdmissionPeriod(patient.Id, patient.StartofCareDate, patient.DischargeDate), "Admission period (SOC and DC date range)  is not in the valid date range."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {

                    patient.AgencyId = Current.AgencyId;
                    patient.Encode();// setting string arrays to one field
                    if (patientRepository.PatientAdmissionAdd(patient))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Data successfully added";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the data.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkPatientAdmissionCurrent(Guid patientId, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient admission could not be set active." };
            if (!patientId.IsEmpty() && !Id.IsEmpty())
            {
                if (patientService.MarkPatientAdmissionCurrent(patientId, Id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The patient admission period is set active. ";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Patient admission could not be set active. ";
                }
            }

            return Json(viewData);
        }

        #endregion
    }
}
