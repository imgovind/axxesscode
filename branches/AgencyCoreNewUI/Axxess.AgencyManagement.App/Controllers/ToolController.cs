﻿namespace Axxess.AgencyManagement.App.Controllers {
    using System;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;
    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ToolController : BaseController {
        public ToolController() { }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Browser() {
            System.Web.HttpBrowserCapabilitiesBase browser = HttpContext.Request.Browser;
            string browserInfo = "<strong>Browser Capabilities</strong><br />"
                + "Type = " + browser.Type + "<br />"
                + "Name = " + browser.Browser + "<br />"
                + "Version = " + browser.Version + "<br />"
                + "Major Version = " + browser.MajorVersion + "<br />"
                + "Minor Version = " + browser.MinorVersion + "<br />"
                + "Platform = " + browser.Platform + "<br />"
                + "Is Beta = " + browser.Beta + "<br />"
                + "Is Crawler = " + browser.Crawler + "<br />"
                + "Is AOL = " + browser.AOL + "<br />"
                + "Is Win16 = " + browser.Win16 + "<br />"
                + "Is Win32 = " + browser.Win32 + "<br />"
                + "Supports Frames = " + browser.Frames + "<br />"
                + "Supports Tables = " + browser.Tables + "<br />"
                + "Supports Cookies = " + browser.Cookies + "<br />"
                + "Supports VBScript = " + browser.VBScript + "<br />"
                + "Supports JavaScript = " + browser.EcmaScriptVersion.ToString() + "<br />"
                + "Supports Java Applets = " + browser.JavaApplets + "<br />"
                + "Supports ActiveX Controls = " + browser.ActiveXControls + "<br />"
                + "Supports JavaScript Version = " + browser["JavaScriptVersion"] + "<br />";
            return PartialView("Data", browserInfo);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Smoke() {
            return PartialView("Data", Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Ping() {
            return PartialView("Ping");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Who() {
            var who = string.Format("{0} ({1})", Environment.MachineName, Request.ServerVariables["LOCAL_ADDR"]); 
            return PartialView("Data", who);
        }
    }
}