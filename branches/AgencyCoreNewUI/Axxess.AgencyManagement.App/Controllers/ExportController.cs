﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;
    using Exports;
    using Enums;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Enums;

    using Axxess.OasisC.Repositories;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.App.Domain;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IReportService reportService;
        private readonly IPayrollService payrollService;

        public ExportController(IAgencyManagementDataProvider agencyManagementDataProvider, IOasisCDataProvider oasisCDataProvider, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService, IPatientService patientService, IReportService reportService, IPayrollService payrollService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.reportService = reportService;
            this.payrollService = payrollService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Contact()
        {
            var contacts = agencyRepository.GetContacts(Current.AgencyId);
            var export = new ContactExporter(contacts);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Contacts.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Hospital()
        {
            var hospitals = agencyRepository.GetHospitals(Current.AgencyId);
            var export = new HosptialExporter(hospitals);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Hospitals.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Physician()
        {
            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId);
            var export = new PhysicianExporter(physicians);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Physicians.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Referral()
        {
            IEnumerable<Referral> referrals = null;
            if (Current.IsCommunityLiason || Current.IsInRole(AgencyRoles.ExternalReferralSource)) referrals = referralRepository.GetAllByCreatedUser(Current.AgencyId, Current.UserId, ReferralStatus.Pending);
            else referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.Pending);
            var export = new ReferralExporter(referrals.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Referrals.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Patient(Guid branch, int status)
        {
            var patients = patientRepository.All(Current.AgencyId, branch, status); // patientRepository.GetAllByAgencyId(Current.AgencyId);
            var export = new PatientExporter(patients.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Patients.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult DeletedPatients(Guid BranchId)
        {
            var patients = patientRepository.AllDeleted(Current.AgencyId, BranchId); 
            var export = new PatientExporter(patients.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Patients.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UserActive()
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Active);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Users.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UserInactive()
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Inactive);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Users.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CahpsReport(Guid BranchId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var export = new CahpsExporter(BranchId, sampleMonth, sampleYear, paymentSources);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserSchedule()
        {
            var export = new UserScheduleExporter(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ScheduleList.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult QA(Guid branch, int status)
        {
            var export = new QAScheduleExporter(agencyService.GetQASchedule(branch, status).OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "QAScheduleList.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueue()
        {
            var export = new PrintQueueExporter(agencyService.GetPrintQueue().OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "PrintQueue.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ExportedOasis(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var assessments = assessmentService.GetAssessmentByStatus(branchId, ScheduleStatus.OasisExported, status, startDate, endDate);
            var export = new ExportedOasis(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Exported Oasis.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OasisExport(Guid branchId, List<int> paymentSources)
        {
            var assessments = assessmentService.GetAssessmentByStatus(branchId, ScheduleStatus.OasisCompletedExportReady, paymentSources);
            var export = new OasisExporter(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "OASIS To Export.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Authorization(Guid patientId)
        {
            var authorizations = patientRepository.GetAuthorizations(Current.AgencyId, patientId);
            var export = new AuthorizationsExporter(authorizations);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Authorizations.xls");
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public FileResult VitalSigns(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var vitalSigns = patientService.GetPatientVitalSigns(PatientId, StartDate, EndDate);
            var export = new VitalSignExporter(vitalSigns.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Patient Vital Signs_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OrdersHistory(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var orders = agencyService.GetProcessedOrders(branchId, startDate, endDate, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature });
            var export = new OrderExport(orders.ToList(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Orders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PatientOrdersHistory(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var orders = patientService.GetPatientOrders(patientId, startDate, endDate);
            var export = new PatientOrderExport(orders.ToList(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Patient Orders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var orders = agencyService.GetOrdersPendingSignature(branchId, startDate, endDate);
            var export = new PendingSignatureOrderExport(orders.ToList(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersPendingPhysicianSignature_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OrdersToBeSent(Guid branchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            var orders = agencyService.GetOrdersToBeSent(branchId, sendAutomatically, startDate, endDate);
            var export = new OrdersToBeSentExporter(orders.ToList(), sendAutomatically, startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersToBeSent{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult RecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var recertEvents = agencyService.GetRecertsPastDue(branchId, insuranceId, startDate, endDate, false, 0);
            var export = new PastDueRecertExporter(recertEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Past Due Recerts.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult RecertsUpcoming(Guid branchId, int insuranceId)
        {
            var recertEvents = agencyService.GetRecertsUpcoming(branchId, insuranceId, DateTime.Now, DateTime.Now.AddDays(24), false, 0);
            var export = new UpcomingRecertExporter(recertEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Upcoming Recerts.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Template()
        {
            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            var export = new TemplateExporter(templates.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Templates.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Incident()
        {
            var incidents = agencyService.GetIncidents(Current.AgencyId);
            var export = new IncidentExporter(incidents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Incident.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Infection()
        {
            var infections = agencyService.GetInfections(Current.AgencyId);
            var export = new InfectionExporter(infections.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Infections.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CommunicationNote(Guid? patientId, Guid? branchId, int status, DateTime? startDate, DateTime? endDate)
        {
            var communicationNotes = patientService.GetCommunicationNotes(patientId.HasValue ? (Guid)patientId : Guid.Empty, branchId.HasValue ? (Guid)branchId : Guid.Empty, status, startDate.HasValue ? (DateTime)startDate : DateTime.MinValue, endDate.HasValue ? (DateTime)endDate : DateTime.MaxValue);
            var export = new CommunicationNoteExporter(communicationNotes.ToList(), startDate.HasValue ? (DateTime)startDate : DateTime.MinValue, endDate.HasValue ? (DateTime)endDate : DateTime.MaxValue);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "CommunicationNotes.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ScheduleDeviations(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var scheduleEvents = reportService.GetScheduleDeviation(branchId, startDate, endDate);
            var export = new ScheduleDeviationExporter(scheduleEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ScheduleDeviations.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PayrollSummary(DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var payroll = payrollService.GetSummary(StartDate, EndDate, payrollStatus);
            var export = new PayrollSummaryExport(payroll.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollSummary_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PayrollDetail(Guid UserId, DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            detail.StartDate = StartDate;
            detail.EndDate = EndDate;
            detail.PayrollStatus = payrollStatus;
            if (!UserId.IsEmpty())
            {
                detail.Id = UserId;
                detail.Name = UserEngine.GetName(UserId, Current.AgencyId);
                detail.Visits = payrollService.GetVisits(UserId, StartDate, EndDate, payrollStatus);
            }
            var export = new PayrollDetailExport(detail);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetail_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PayrollDetails(DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var details = payrollService.GetDetails(StartDate, EndDate, payrollStatus);
            var export = new PayrollDetailsExport(details, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetail_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult SubmittedBatchClaims(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            var claims = billingRepository.ClaimDatas(Current.AgencyId, StartDate, EndDate, ClaimType);
            var export = new SubmittedBatchClaimsExport(claims.ToList(), StartDate, EndDate, ClaimType);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("SubmittedBatchClaims_{0}.xls", DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSEpisodeInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsEpisodeExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSVisitInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsVisitExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSPaymentInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsPaymentExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSChargeInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsChargeExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult PatientsAndVisitsByAgeReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new PatientsAndVisitsByAgeExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult DischargesByReasonReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new DischargesByReasonExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult VisitsByPrimaryPaymentSourceReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new VisitsByPrimaryPaymentSourceExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult VisitsByStaffTypeReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new VisitsByStaffTypeExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult AdmissionsByReferralSourceReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new AdmissionsByReferralSourceExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult PatientsVisitsByPrincipalDiagnosisReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new PatientsVisitsByPrincipalDiagnosisExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLog(Guid? patientId)
        {
            if (patientId.HasValue)
            {
                var logs = patientRepository.GetHospitalizationLogs((Guid)patientId, Current.AgencyId);
                var patient = patientRepository.Get((Guid)patientId, Current.AgencyId);
                var export = new PatientHospitalizationLogExporter(logs, patient.DisplayName);
                return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("{0}_HospitalizationLog_{1}.xls", patient.DisplayName, DateTime.Now.Ticks));
            }
            else
            {
                var patientList = patientRepository.GetHospitalizedPatients(Current.AgencyId);
                if (patientList != null && patientList.Count > 0)
                {
                    patientList.ForEach(d =>
                    {
                        d.User = !d.UserId.IsEmpty() ? UserEngine.GetName(d.UserId, Current.AgencyId) : string.Empty;
                    });
                }
                var export = new HospitalizationLogExporter(patientList);
                return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("HospitalizationLog_{0}.xls", DateTime.Now.Ticks));
            }
        }
        #endregion
    }
}
