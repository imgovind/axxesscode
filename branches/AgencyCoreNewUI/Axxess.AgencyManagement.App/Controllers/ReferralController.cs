﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.iTextExtension;
    
    using Telerik.Web.Mvc;
    using Axxess.Log.Enums;
    using Axxess.Log.Common;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReferralController : BaseController
    {
        #region Private Members/Constructor

        private readonly IReferralService referralService;
        private readonly IReferralRepository referralRepository;
        private readonly IPatientService patientService;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;

        public ReferralController(IAgencyManagementDataProvider agencyManagementDataProvider, IReferralService referralService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.referralService = referralService;
            this.patientService = patientService;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region ReferralController Actions

        #region CRUD
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New() {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Create([Bind]Referral referral) {
            Check.Argument.IsNotNull(referral, "referral");
            var viewData = new JsonViewData();
            if (referral.IsValid) {
                if (!referralService.AddReferral(referral)) {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the referral.";
                } else {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Referral was created successfully";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = referral.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Get(Guid id) {
            return Json(referralRepository.Get(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List() {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid() {
            return View(new GridModel(referralService.GetPending(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id) {
            return PartialView(referralRepository.Get(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Referral referral) {
            Check.Argument.IsNotNull(referral, "referral");
            var viewData = Validate<JsonViewData>(
                new Validation(() => string.IsNullOrEmpty(referral.FirstName), ". Patient first name is required.<br/>"),
                new Validation(() => string.IsNullOrEmpty(referral.LastName), ". Patient last name is required. <br>"),
                new Validation(() => string.IsNullOrEmpty(referral.DOB.ToString()), ". Patient date of birth is required.<br/>"),
                new Validation(() => !referral.DOB.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range.<br/>"),
                new Validation(() => string.IsNullOrEmpty(referral.Gender), ". Patient gender has to be selected.<br/>"),
                new Validation(() => (referral.EmailAddress == null ? !string.IsNullOrEmpty(referral.EmailAddress) : !referral.EmailAddress.IsEmail()), ". Patient e-mail is not in a valid  format.<br/>"),
                new Validation(() => string.IsNullOrEmpty(referral.AddressLine1), ". Patient address line is required.<br/>"),
                new Validation(() => string.IsNullOrEmpty(referral.AddressCity), ". Patient city is required.<br/>"),
                new Validation(() => string.IsNullOrEmpty(referral.AddressStateCode), ". Patient state is required.<br/>"),
                new Validation(() => string.IsNullOrEmpty(referral.AddressZipCode), ". Patient zip is required.<br/>")
            );
            if (viewData.isSuccessful) {
                referral.AgencyId = Current.AgencyId;
                if (referralRepository.Edit(referral)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your referral has been updated.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Could not update the referral. Please try again.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid id) {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this Referral. Please try again." };
            if (referralRepository.Delete(Current.AgencyId, id)) {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.Referral, LogAction.ReferralDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Referral has been deleted.";
            }
            return Json(viewData);
        }
        #endregion

        #region Physician
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianAdd(Guid id, Guid referralId) {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician was not added. Try Again." };
            var Referral = referralRepository.Get(Current.AgencyId, referralId);
            if (Referral != null && Referral.Physicians.IsNotNullOrEmpty()) {
                var Physicians = Referral.Physicians.ToObject<List<Physician>>();
                Referral.AgencyPhysicians = new List<Guid>();
                foreach (var Physician in Physicians) Referral.AgencyPhysicians.Add(Physician.Id);
                Referral.AgencyPhysicians.Add(id);
                if (referralRepository.Edit(Referral)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, Referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician has been successfully added.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error occurred while adding the physician.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult PhysicianGrid(Guid referralId) {
            var Referral = referralRepository.Get(Current.AgencyId, referralId);
            if (Referral != null && Referral.Physicians.IsNotNullOrEmpty()) {
                var Physicians = Referral.Physicians.ToObject<List<Physician>>();
                IList<AgencyPhysician> ReturnValue = new List<AgencyPhysician>();
                foreach (var Physician in Physicians) {
                    var AgencyPhysician = physicianRepository.Get(Physician.Id, Current.AgencyId);
                    AgencyPhysician.Primary = Physician.IsPrimary;
                    ReturnValue.Add(AgencyPhysician);
                }
                return Json(new GridModel(ReturnValue));
            }
            else return Json(string.Empty);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianDelete(Guid id, Guid referralId) {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician was not deleted. Try Again." };
            var Referral = referralRepository.Get(Current.AgencyId, referralId);
            if (Referral != null && Referral.Physicians.IsNotNullOrEmpty()) {
                var Physicians = Referral.Physicians.ToObject<List<Physician>>();
                for (int i = 0; i < Physicians.Count(); i++) if (Physicians[i].Id.Equals(id)) Physicians.RemoveAt(i--);
                Referral.AgencyPhysicians = new List<Guid>();
                if (Physicians.Count() > 0) foreach (var Physician in Physicians) Referral.AgencyPhysicians.Add(Physician.Id);
                else Referral.AgencyPhysicians.Add(Guid.Empty);
                if (referralRepository.Edit(Referral)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, Referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician has been successfully deleted.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the physician.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianSetPrimary(Guid id, Guid referralId) {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician was not set as primary. Try Again." };
            var Referral = referralRepository.Get(Current.AgencyId, referralId);
            if (Referral != null && Referral.Physicians.IsNotNullOrEmpty()) {
                var Physicians = Referral.Physicians.ToObject<List<Physician>>();
                for (int i = 0; i < Physicians.Count(); i++) if (Physicians[i].Id.Equals(id)) Physicians.RemoveAt(i--);
                Referral.AgencyPhysicians = new List<Guid>();
                Referral.AgencyPhysicians.Add(id);
                foreach (var Physician in Physicians) Referral.AgencyPhysicians.Add(Physician.Id);
                if (referralRepository.Edit(Referral)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, Referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician was successfully set as primary.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in setting physician as primary.";
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Helpers
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonAdmit(Guid id)
        {
            return PartialView("NonAdmit", referralRepository.Get(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NonAdmitComplete([Bind] PendingPatient referral)
        {
            Check.Argument.IsNotNull(referral, "PendingReferral");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Non-Admission of referral could not be saved." };
            referral.AgencyId = Current.AgencyId;
            if (referralRepository.NonAdmitReferral(referral))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralNonAdmitted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Referral non-admission successful.";
            }
            return Json(viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ReferralPdf(Guid id)
        {
            return FileGenerator.Pdf<ReferralPdf>(new ReferralPdf(referralRepository.Get(Current.AgencyId, id), agencyRepository.GetWithBranches(Current.AgencyId)), "Referral");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Logs(Guid id)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.Referral, Current.AgencyId, id.ToString()));
        }
        #endregion

        #endregion
    }
}
