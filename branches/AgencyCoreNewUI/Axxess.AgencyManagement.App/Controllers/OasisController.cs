﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;


    using Services;
    using ViewData;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;


    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.LookUp.Domain;
    using Axxess.OasisC.Enums;
    using Axxess.AgencyManagement.App.Enums;
    using Axxess.AgencyManagement.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OasisController : BaseController
    {
        #region Constructor / Member

        private readonly IUserService userService;
        private readonly IDateService dateService;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientService patientService;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly ICachedDataRepository cachedDataRepository;
        private readonly IAssessmentRepository oasisAssessmentRepository;

        public OasisController(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IAssessmentService assessmentService, IPatientService patientService, IUserService userService, IDateService dateService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(oasisDataProvider, "oasisDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.assessmentService = assessmentService;
            this.dateService = dateService;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.cachedDataRepository = oasisDataProvider.CachedDataRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.oasisAssessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region Export
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Export()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Export");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExportGrid(Guid branchId, List<int> paymentSources)
        {
            return View(new GridModel(assessmentService.GetAssessmentByStatus(branchId, ScheduleStatus.OasisCompletedExportReady, paymentSources)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Exported()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Exported");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExportedGrid(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(assessmentService.GetAssessmentByStatus(branchId, ScheduleStatus.OasisExported, status, startDate, endDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExportedReopen(Guid id, Guid episodeId, Guid patientId, string assessmentType)
        {
            var viewData = new AssessmentExport();
            viewData.AssessmentId = id;
            viewData.EpisodeId = episodeId;
            viewData.PatientId = patientId;
            viewData.AssessmentType = assessmentType;
            return PartialView("Reopen", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Mark(List<string> oasisSelected, string statusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The selected OASIS Assessments status could not be changed." };
            if (oasisSelected != null && oasisSelected.Count > 0)
            {
                if (statusType.IsNotNullOrEmpty() && (statusType.IsEqual("Exported") || statusType.IsEqual("CompletedNotExported")))
                {
                    if (assessmentService.MarkAsExportedORCompleted(oasisSelected, statusType.IsEqual("Exported") ? (int)ScheduleStatus.OasisExported : (int)ScheduleStatus.OasisCompletedNotExported))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The selected OASIS Assessments have been  marked as " + (statusType.IsEqual("Exported") ? "Exported." : "Completed ( Not Exported ).");
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected OASIS status change is not successfully . Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Select OASIS you want to change the status.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Generate(Guid branchId, List<string> oasisSelected)
        {
            var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (agencyLocation != null && !agencyLocation.IsLocationStandAlone)
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                    agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                    agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                    agencyLocation.Name = agency.Name;
                    agencyLocation.ContactPersonFirstName = agency.ContactPersonFirstName;
                    agencyLocation.ContactPersonLastName = agency.ContactPersonLastName;
                    agencyLocation.ContactPersonPhone = agency.ContactPersonPhone;
                    agencyLocation.ContactPersonEmail = agency.ContactPersonEmail;
                }
            }

            string generateOasisHeader = assessmentService.OasisHeader(agencyLocation);
            var generateJsonOasis = string.Empty;
            int count = 0;
            var hl = generateOasisHeader.Length;
            if (oasisSelected != null && oasisSelected.Count > 0)
            {
                oasisSelected.ForEach(o =>
                {
                    string[] data = o.Split('|');
                    var assessment = oasisAssessmentRepository.Get(data[0].ToGuid(), Current.AgencyId);
                    if (assessment != null && assessment.SubmissionFormat != null)
                    {
                        generateJsonOasis += assessment.SubmissionFormat + "\r\n";
                        count++;
                    }
                });
            }
            var bl = generateJsonOasis.Length;
            string generateOasisFooter = assessmentService.OasisFooter(count + 2);
            var fl = generateOasisFooter.Length;
            string allString = generateOasisHeader + generateJsonOasis + generateOasisFooter;
            return FileGenerator.PlainText(allString, "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Inactivate(Guid id, string type)
        {
            return Json(assessmentService.ValidateInactivate(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult GenerateCancel(Guid id, string type)
        {
            var assessment = oasisAssessmentRepository.Get(id, Current.AgencyId);
            var agencyLocation = new AgencyLocation();
            if (assessment != null)
            {
                var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    agencyLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                    if (agencyLocation != null && !agencyLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }
                }
            }
            string generateOasisHeadre = assessmentService.OasisHeader(agencyLocation);
            var generateJsonOasis = string.Empty;
            if (assessment != null && assessment.CancellationFormat.IsNotNullOrEmpty()) generateJsonOasis = assessment.CancellationFormat + "\r\n";
            string generateOasisFooter = assessmentService.OasisFooter(3);
            string allString = generateOasisHeadre + generateJsonOasis + generateOasisFooter;
            return FileGenerator.PlainText(allString, "OASIS");
        }
        #endregion

        #region OasisController Actions

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Assessment(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new OasisViewData();
            var oasisAssessment = assessmentService.SaveAssessment(formCollection, Request.Files);
            if (oasisAssessment != null)
            {
                if (oasisAssessment.ValidationError.IsNullOrEmpty())
                {
                    viewData.Assessment = oasisAssessment;
                    viewData.assessmentId = oasisAssessment.Id;
                    viewData.isSuccessful = true;
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = oasisAssessment.ValidationError;
                }
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return Json(assessmentService.GetAssessment(Id).ToDictionary());
        }

        #region OASIS
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OASISView(Guid Id, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            var assessment = assessmentService.GetAssessmentWithScheduleType(Id);
            return PartialView(string.Format("Assessments/{0}", assessment != null && assessment.Type != null ? assessment.Type.ToString() : string.Empty), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OASISBlank(string AssessmentType, string Discipline)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType), Discipline), "OASIS");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OASISPrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            var assessment = assessmentService.GetAssessmentPrint(episodeId, patientId, eventId);
            return View(string.Format("Assessments/{0}Print", assessment != null && assessment.Type != null ? assessment.Type.ToString() : string.Empty), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OASISPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<OasisPdf>(new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId)), "OASIS");
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Category(Guid Id, Guid PatientId, string AssessmentType, string Category)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            var assessment = assessmentService.GetAssessmentWithScheduleType(Id);
            var cat = Category.IsNotNullOrEmpty() ? Category.Split('_').Last() : string.Empty;
            if (assessment != null)
            {
                if (cat.IsNotNullOrEmpty() && cat.IsEqual("Medications"))
                {
                    var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                    if (medicationProfile != null) assessment.MedicationProfile = medicationProfile.ToXml();
                }
                if (cat.IsNotNullOrEmpty() && cat.IsEqual("PatientHistory")) assessment.DiagnosisDataJson = assessmentService.GetDiagnosisData(assessment);
            }
            return PartialView("Assessments/Tabs/" + cat, assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "mooCode")]
        public JsonResult Guide(string mooCode)
        {
            if (mooCode.IsNullOrEmpty())
            {
                return Json(new OasisGuide());
            }
            else
            {
                var result = cachedDataRepository.GetOasisGuide(mooCode);
                return Json(result != null ? result : new OasisGuide());
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanOfCareMedication(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return View("485/Medication", patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId) ?? new MedicationProfile());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanOfCarePrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("485/Print", assessmentService.GetPlanOfCarePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PlanOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return FileGenerator.Pdf<PlanOfCarePdf>(new PlanOfCarePdf(assessmentService.GetPlanOfCarePrint(episodeId, patientId, eventId)), "HCFA-485");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new OasisPlanOfCareJson { isSuccessful = false, errorMessage = "No Plan of Care (485) found for this episode." };
            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (scheduleEvent != null)
            {
                var assessment = patientService.GetEpisodeAssessment(episodeId, patientId, scheduleEvent.EventDate);
                if (assessment != null)
                {
                    var pocScheduleEvent = assessmentService.GetPlanofCareScheduleEvent(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    if (pocScheduleEvent != null)
                    {
                        viewData.isSuccessful = true;
                        viewData.url = "/Oasis/PlanOfCarePdf";
                        viewData.episodeId = pocScheduleEvent.EpisodeId;
                        viewData.patientId = pocScheduleEvent.PatientId;
                        viewData.eventId = pocScheduleEvent.EventId;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return View("485/New", patientService.GetPatientAndAgencyInfo(episodeId, patientId, eventId) ?? new PlanofCareViewData());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanofCareContent(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var assessment = oasisAssessmentRepository.Get(planofCare.AssessmentId, Current.AgencyId);
                if (assessment != null)
                {
                    planofCare.Questions = assessmentService.Get485FromAssessment(assessment);
                }
            }
            return PartialView("485/LocatorQuestions", planofCare);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var agency = agencyRepository.GetWithBranches(planofCare.AgencyId);
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
                planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
                if (planofCare.Data.IsNotNullOrEmpty())
                {
                    planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                }

                var episodeRange = assessmentService.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
                if (episodeRange != null)
                {
                    planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                    planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                }

                if (planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
                {
                    var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                    {
                        planofCare.PhysicianId = oldPhysician.Id;
                    }
                }

                return View("485/Edit", planofCare);
            }
            return View("485/Edit", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Save485(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The 485 Plan of Care could not be saved." };
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();

                if (statusId == (int)ScheduleStatus.OrderSaved)
                {
                    if (assessmentService.UpdatePlanofCare(formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The 485 Plan of Care has been saved.";
                    }
                }
                else if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                    }
                    else if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                    }
                    else
                    {
                        if (assessmentService.UpdatePlanofCare(formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The 485 Plan of Care has been completed.";
                        }
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SavePlanofCareStandAlone(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The 485 Plan of Care could not be saved." };
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();

                if (statusId == (int)ScheduleStatus.OrderSaved)
                {
                    if (assessmentService.UpdatePlanofCareStandAlone(formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The 485 Plan of Care has been saved.";
                    }
                }
                else if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                    }
                    else if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                    }
                    else
                    {
                        if (assessmentService.UpdatePlanofCareStandAlone(formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The 485 Plan of Care has been completed.";
                        }
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Validate(Guid Id, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return View("~/Views/Oasis/Validation.aspx", assessmentService.Validate(Id, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AuditPdf(Guid id, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");

            return FileGenerator.Pdf<OasisAuditPdf>(new OasisAuditPdf(assessmentService.Audit(id, patientId, episodeId)), "OASISLogicalCheck");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Submit(Guid id, Guid patientId, Guid episodeId,  string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your assessment was not submitted." };
            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, id);
            if (scheduleEvent != null)
            {
                if (actionType == ButtonAction.Submit.ToString())
                {
                    if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisCompletedPendingReview).ToString(), string.Empty))
                    {
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            if (DisciplineTaskFactory.AllDischargingOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask))//(assessmentType == (int)DisciplineTasks.OASISCDischargeOT || assessmentType == (int)DisciplineTasks.OASISCDischarge.ToString() || assessmentType == DisciplineTasks.OASISCDischargePT.ToString() || assessmentType == "DischargeFromAgency") || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString() || assessmentType == "OASISCTransferDischarge") || (assessmentType == DisciplineTasks.OASISCDeath.ToString() || assessmentType == DisciplineTasks.OASISCDeathOT.ToString() || assessmentType == DisciplineTasks.OASISCDeathPT.ToString() || assessmentType == "OASISCDeath")
                            {
                                var assessment = oasisAssessmentRepository.Get(id, Current.AgencyId);
                                if (assessment != null)
                                {
                                    var assessmentData = assessment.ToDictionary();
                                    var date = DateTime.MinValue;
                                    var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                    if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                                    var eventDateSchedule = scheduleEvent.EventDate;
                                    if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                    var visitDateSchedule = scheduleEvent.VisitDate;
                                    if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                    if (date.Date > DateTime.MinValue.Date) patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to discharge OASIS.");
                                }
                                else
                                {
                                    var date = DateTime.MinValue;
                                    var eventDateSchedule = scheduleEvent.EventDate;
                                    if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                    var visitDateSchedule = scheduleEvent.VisitDate;
                                    if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                    if (date.Date > DateTime.MinValue.Date) patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to Discharge OASIS.");
                                }
                            }
                        }
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Assessment was submitted successfully.";
                    }
                }
                else if (actionType == ButtonAction.Approve.ToString())
                {
                    if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), string.Empty))
                    {
                        if (DisciplineTaskFactory.AllDischargingOASISDisciplineTasks().Contains(scheduleEvent.DisciplineTask))//(assessmentType == (int)DisciplineTasks.OASISCDischargeOT || assessmentType == (int)DisciplineTasks.OASISCDischarge.ToString() || assessmentType == DisciplineTasks.OASISCDischargePT.ToString() || assessmentType == "DischargeFromAgency") || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString() || assessmentType == "OASISCTransferDischarge") || (assessmentType == DisciplineTasks.OASISCDeath.ToString() || assessmentType == DisciplineTasks.OASISCDeathOT.ToString() || assessmentType == DisciplineTasks.OASISCDeathPT.ToString() || assessmentType == "OASISCDeath")
                        {
                            var assessment = oasisAssessmentRepository.Get(id, Current.AgencyId);
                            if (assessment != null)
                            {
                                var assessmentData = assessment.ToDictionary();
                                var date = DateTime.MinValue;
                                var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                                var eventDateSchedule = scheduleEvent.EventDate;
                                if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                var visitDateSchedule = scheduleEvent.VisitDate;
                                if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                if (date.Date > DateTime.MinValue.Date) patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to discharge OASIS.");
                            }
                            else
                            {
                                var date = DateTime.MinValue;
                                var eventDateSchedule = scheduleEvent.EventDate;
                                if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                var visitDateSchedule = scheduleEvent.VisitDate;
                                if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                if (date.Date > DateTime.MinValue.Date) patientService.DischargePatient(patientId, episodeId, date, "Patient discharged due to Discharge OASIS.");
                            }
                        }
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Assessment has been approved.";
                    }
                }
                else if (actionType == ButtonAction.Return.ToString())
                {
                    if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString(), reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Assessment has been returned.";
                    }
                }
                else if (actionType == ButtonAction.Exported.ToString())
                {
                    if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisExported).ToString(), string.Empty))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Assessment has been exported.";
                    }
                }
                else if (actionType == ButtonAction.ReOpen.ToString())
                {
                    if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisReopened).ToString(), reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Assessment has been reopened.";
                    }
                }
                else if (actionType == ButtonAction.CompletedNotExported.ToString())
                {
                    if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisCompletedNotExported).ToString(), string.Empty))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The Assessment has been completed ( not exported ).";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SubmitOnly(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Assessment could not be submitted." };
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                string type = keys.Contains("OasisValidationType") && formCollection["OasisValidationType"].IsNotNullOrEmpty() ? formCollection["OasisValidationType"] : string.Empty;
                if (type.IsNotNullOrEmpty())
                {
                    var Id = keys.Contains(string.Format("{0}_Id", type)) && formCollection[string.Format("{0}_Id", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_Id", type)].IsGuid() ? formCollection.Get(string.Format("{0}_Id", type)).ToGuid() : Guid.Empty;
                    var episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) && formCollection[string.Format("{0}_EpisodeId", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_EpisodeId", type)].IsGuid() ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
                    var patientId = keys.Contains(string.Format("{0}_PatientId", type)) && formCollection[string.Format("{0}_PatientId", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_PatientId", type)].IsGuid() ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;

                    if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                        rules.Add(new Validation(() => !keys.Contains(string.Format("{0}_ValidationClinician", type)) || (keys.Contains(string.Format("{0}_ValidationClinician", type)) && string.IsNullOrEmpty(formCollection[string.Format("{0}_ValidationClinician", type)])), "User Signature can't be empty.\n"));
                        rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationClinician", type)) && formCollection[string.Format("{0}_ValidationClinician", type)].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[string.Format("{0}_ValidationClinician", type)]) : false, "User Signature is not correct.\n"));

                        rules.Add(new Validation(() => !keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) || (keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) && string.IsNullOrEmpty(formCollection[string.Format("{0}_ValidationSignatureDate", type)])), "Signature date can't be empty.\n"));
                        rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsValidDate() ? false : true, "Signature date is not valid.\n"));
                        rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsValidDate() ? !(formCollection[string.Format("{0}_ValidationSignatureDate", type)].ToDateTime() >= episode.StartDate && formCollection[string.Format("{0}_ValidationSignatureDate", type)].ToDateTime() <= DateTime.Now) : true, "Signature date is not the in valid range.\n"));
                        if (type == AssessmentType.StartOfCare.ToString() || type == AssessmentType.ResumptionOfCare.ToString() || type == AssessmentType.Recertification.ToString() || type == AssessmentType.FollowUp.ToString())
                        {
                            rules.Add(new Validation(() => !keys.Contains(type + "_TimeIn") || (keys.Contains(type + "_TimeIn") && string.IsNullOrEmpty(formCollection[type + "_TimeIn"])), "Time-In can't be empty. \n"));
                            rules.Add(new Validation(() => !keys.Contains(type + "_TimeOut") || (keys.Contains(type + "_TimeOut") && string.IsNullOrEmpty(formCollection[type + "_TimeOut"])), "Time-Out can't be empty. \n"));
                        }

                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            if (assessmentService.UpdateAssessmentStatusForSubmit(Id, patientId, episodeId, ((int)ScheduleStatus.OasisCompletedPendingReview).ToString(), string.Format("Electronically Signed by: {0}", Current.UserFullName), formCollection[string.Format("{0}_ValidationSignatureDate", type)].ToDateTime(), formCollection[type + "_TimeIn"], formCollection[type + "_TimeOut"]))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Your Assessment submitted successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (assessmentService.DeleteWoundCareAsset(episodeId, patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType)
        {
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");

            assessmentService.AddSupply(episodeId, patientId, eventId,supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.UpdateSupply(episodeId, patientId, eventId,supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.DeleteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BlankMasterCalendar(Guid episodeId, Guid patientId, string assessmentType)
        {
            var episode = patientRepository.GetEpisodeOnlyWithPreviousAndAfter(Current.AgencyId, episodeId, patientId);
            var modelData = new EpisodeDateViewData();
            if (episode != null)
            {
                if (assessmentType.IsEqual("Recertification"))
                {
                    if (episode.NextEpisode != null && episode.EndDate.AddDays(1).Date == episode.NextEpisode.StartDate.Date)
                    {
                        modelData.StartDate = episode.NextEpisode.StartDate;
                        modelData.EndDate = episode.NextEpisode.EndDate;
                    }
                    else
                    {
                        modelData.StartDate = episode.EndDate.AddDays(1);
                        modelData.EndDate = modelData.StartDate.AddDays(59);
                    }
                }
                else
                {
                    modelData.EndDate = episode.EndDate;
                    modelData.StartDate = episode.StartDate;
                }
            }
            return PartialView("BlankMasterCalendar", modelData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisSignature(Guid Id, Guid PatientId, Guid EpisodeId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return PartialView("NonOasisSignature", assessmentService.GetAssessment(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadPrevious(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(assessmentId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotEmpty(previousAssessmentId, "previousAssessmentId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Previous Assessment data could not be saved." };
            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, assessmentId);
            if (scheduleEvent != null)
            {
                var oldStatus = scheduleEvent.Status;
                scheduleEvent.Status = (int)ScheduleStatus.OasisSaved;
                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                {
                    if (oasisAssessmentRepository.UsePreviousAssessment(Current.AgencyId, episodeId, patientId, assessmentId, previousAssessmentId, previousAssessmentType))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Assessment was successfully loaded from the previous assessment.";
                    }
                    else
                    {
                        scheduleEvent.Status = oldStatus;
                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Correction(Guid Id, Guid PatientId, Guid EpisodeId, string Type, int CorrectionNumber)
        {
            var export = new OasisExport();
            export.AssessmentId = Id;
            export.PatientId = PatientId;
            export.EpisodeId = EpisodeId;
            export.CorrectionNumber = CorrectionNumber;
            export.AssessmentType = Type;
            return PartialView(export);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CorrectionChange(Guid Id, Guid PatientId, Guid EpisodeId, string Type, int CorrectionNumber)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Correction Number Is Not Updated." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty() && !EpisodeId.IsEmpty())
            {
                if (assessmentService.UpdateAssessmentCorrectionNumber(Id, PatientId, EpisodeId, CorrectionNumber))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Correction Number Is Updated.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult OasisProfilePdf(Guid Id, String type)
        {
            return FileGenerator.Pdf<OasisProfilePdf>(new OasisProfilePdf(oasisAssessmentRepository.Get(Id, Current.AgencyId), agencyRepository.GetWithBranches(Current.AgencyId)), "OASISProfile");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OasisProfilePrint(Guid Id, String type) {
            var note = oasisAssessmentRepository.Get(Id,Current.AgencyId);
            note.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            var xml = new OasisProfileXml(note);
            note.PrintViewJson = xml.GetJson();
            return View("Profile", note);
        }

        #endregion
    }
}
