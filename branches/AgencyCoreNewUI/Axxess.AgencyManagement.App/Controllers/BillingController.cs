﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Xml;
    using Axxess.AgencyManagement.App.Enums;
    using Axxess.AgencyManagement.App.iTextExtension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Exports;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Domain;
    using Axxess.Log.Common;
    using Axxess.Log.Enums;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;
    using Axxess.OasisC.Repositories;
    using Services;
    using Telerik.Web.Mvc;
    using ViewData;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IAgencyService agencyService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public BillingController(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, ILookUpDataProvider lookUpDataProvider, IPatientService patientService, IBillingService billingService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            this.referrralRepository = dataProvider.ReferralRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.patientService = patientService;
            this.billingService = billingService;
            this.agencyService = agencyService;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Authorization(Guid authorizationId, string claimTypeIdentifier)
        {
            var viewData = patientRepository.GetAuthorization(Current.AgencyId, authorizationId);
            if (viewData != null) viewData.ClaimTypeIdentifier = claimTypeIdentifier;
            return PartialView("Authorization", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceInfo(Guid patientId, int insuranceId, DateTime startDate, DateTime endDate, string claimTypeIdentifier)
        {
            var viewData = billingService.InsuranceWithAuthorization(patientId, insuranceId, startDate, endDate);
            if (viewData != null)
            {
                if (viewData.Authorization == null) viewData.Authorization = new Authorization();
                viewData.ClaimTypeIdentifier = viewData.Authorization.ClaimTypeIdentifier = claimTypeIdentifier;
            }
            return PartialView("InsuranceInfo", viewData);
        }

        #region Medicare Eligibility
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibility()
        {
            return PartialView("MedicareEligibility/Layout");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityContent(Guid patientId)
        {
            return PartialView("MedicareEligibility/Content", patientService.GetMedicareEligibilityLists(patientId));
        }
        #endregion

        #region Claim
        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ClaimPrint(Guid branchId, int insuranceId, string sortType, string claimType)
        {
            return FileGenerator.Pdf<BillingClaimsPdf>(new BillingClaimsPdf(billingService.GetClaimsPrint(branchId, insuranceId, sortType, claimType)), "Claims");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimPrintPreview(Guid branchId, int insuranceId, string sortType, string claimType)
        {
            var viewData = billingService.GetClaimsPrint(branchId, insuranceId, sortType, claimType);
            if (viewData.ClaimType.IsEqual("rap")) return PartialView("Claim/RAP/GridPrint", viewData);
            else return PartialView("Claim/Final/GridPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimExport(Guid branchId, int insuranceId, string sortType, string claimType)
        {
            var export = new ClaimsExport(billingService.GetClaimsPrint(branchId, insuranceId, sortType, claimType));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ClaimSummary.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimGenerate(int ansiId)
        {
            var claimData = billingRepository.GetClaimData(Current.AgencyId, ansiId);
            string generateJsonClaim = claimData != null ? claimData.Data : string.Empty;
            return FileGenerator.PlainText(generateJsonClaim, "billing");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimCreateANSI(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance, string Type)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateDirect(ClaimSelected, Type, ClaimCommandType.download, out claimDataOut, out billExchage, BranchId, PrimaryInsurance) && claimDataOut != null) return Json(new { isSuccessful = true, Id = claimDataOut.Id });
            else if (billExchage != null) return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
            else return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error in processing of the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimSubmitDirectly(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) are not processed. Try again." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateDirect(ClaimSelected, Type, ClaimCommandType.direct, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) are processed successfully.";
            }
            else if (billExchage != null) viewData.errorMessage = billExchage.Message;
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimUpdateStatus(List<Guid> ClaimSelected, string Type, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your status update is not Successful." };
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("rap"))
                {
                    if (billingService.UpdateRapStatus(ClaimSelected, StatusType))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your RAP status update is successful.";
                    }
                }
                else if (Type.IsEqual("final"))
                {
                    if (billingService.UpdateFinalStatus(ClaimSelected, StatusType))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Final (EOE) status update is  successful.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSummary(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance, string Type)
        {
            return PartialView("Claim/Summary", billingService.ClaimToGenerate(ClaimSelected, BranchId, PrimaryInsurance, Type));
        }
        #endregion

        #region Claim RAP
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRAPCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var bill = new Bill();
            bill.ClaimType = "RAP";
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
                    {
                        bill.Claims = billingService.AllUnProcessedRaps(agencyMainBranch.Id, payorType);
                        bill.IsMedicareHMO = false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.Insurance = payorType;
                        return PartialView("Claim/Center", bill);
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            bill.Claims = billingService.AllUnProcessedRaps(agencyMainBranch.Id, agencyMedicareInsurance.Id);
                            bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                            bill.BranchId = agencyMainBranch.Id;
                            bill.Insurance = agencyMedicareInsurance.Id;
                            return PartialView("Claim/Center", bill);
                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        bill.Claims = billingService.AllUnProcessedRaps(agencyMainBranch.Id, agencyMedicareInsurance.Id);
                        bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.Insurance = agencyMedicareInsurance.Id;
                        return PartialView("Claim/Center", bill);
                    }
                }
            }
            return PartialView("Claim/Center", bill);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRAPGrid(Guid branchId, int insuranceId)
        {
            return PartialView("Claim/RAP/Grid", billingService.AllUnProcessedRaps(branchId, insuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRAP(Guid episodeId, Guid patientId)
        {
            return PartialView("Claim/RAP/Verify", billingService.GetRap(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ClaimRAPPrint(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<BillingRapPdf>(new BillingRapPdf(billingService.GetRapPrint(episodeId, patientId)), "Rap");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimRAPPrintPreview(Guid episodeId, Guid patientId)
        {
            return PartialView("Claim/RAP/Print", billingService.GetRapPrint(episodeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimRAPVerify(Rap claim, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The RAP could not be verified." };
            var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
            if (patient != null)
            {
                if (claim != null)
                {
                    if (formCollection != null)
                    {
                        var keys = formCollection.AllKeys;
                        if (keys != null && keys.Length > 0)
                        {
                            claim.FirstBillableVisitDateFormat = keys.Contains("FirstBillableVisitDateFormatInput") ? formCollection["FirstBillableVisitDateFormatInput"] : string.Empty;
                            if (keys.Contains("Ub04Locator81"))
                            {
                                var locatorList = formCollection["Ub04Locator81"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3")) locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                    });
                                }
                                claim.Ub04Locator81cca = locators.ToXml();
                            }
                        }
                    }
                    var agencyInsurance = new AgencyInsurance();
                    if (claim.PrimaryInsuranceId >= 1000) agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    else if (claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000) agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                    claim.Insurance = agencyInsurance.ToXml();
                    if (claim.IsValid)
                    {
                        if (billingRepository.VerifyRap(Current.AgencyId, claim))
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Rap, LogAction.RAPVerified, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The RAP was verified successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = claim.ValidationMessage;
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Patient dosen't exist.Try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRAPSnapshotGrid(Guid id)
        {
            var raps = billingRepository.GetRapSnapShots(Current.AgencyId, id);
            return PartialView("Claim/RAP/Snapshot/Grid", raps != null && raps.Count > 0 ? raps : new List<RapSnapShot>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRAPSnapshotEdit(long batchId, Guid id)
        {
            return PartialView("Claim/RAP/Snapshot/Edit", billingRepository.GetRAPSnapshot(Current.AgencyId, id, batchId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimRAPSnapshotUpdate(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The RAP snapshot could not be updated." };
            if (billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "RAP"))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The RAP snapshot was updated successfully.";
            }
            return Json(viewData);
        }
        #endregion

        #region Claim Final
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var bill = new Bill();
            bill.ClaimType = "Final";
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
                    {
                        bill.Claims = billingService.AllUnProcessedFinals(agencyMainBranch.Id, payorType);
                        bill.IsMedicareHMO = false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.Insurance = payorType;
                        return PartialView("Claim/Center", bill);
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            bill.Claims = billingService.AllUnProcessedFinals(agencyMainBranch.Id, agencyMedicareInsurance.Id);
                            bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                            bill.BranchId = agencyMainBranch.Id;
                            bill.Insurance = agencyMedicareInsurance.Id;
                            return PartialView("Claim/Center", bill);
                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        bill.Claims = billingService.AllUnProcessedFinals(agencyMainBranch.Id, agencyMedicareInsurance.Id);
                        bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.Insurance = agencyMedicareInsurance.Id;
                        return PartialView("Claim/Center", bill);
                    }
                }
            }
            return PartialView("Claim/Center", bill);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalGrid(Guid branchId, int insuranceId)
        {
            return PartialView("Claim/Final/Grid", billingService.AllUnProcessedFinals(branchId, insuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinal(Guid episodeId, Guid patientId)
        {
            if (episodeId.IsEmpty() || patientId.IsEmpty()) return PartialView("Claim/Final/Verify", new Final());
            return PartialView("Claim/Final/Verify", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ClaimFinalPrint(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<BillingFinalPdf>(new BillingFinalPdf(billingService.GetFinalPrint(episodeId, patientId)), "Final");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimFinalPrintPreview(Guid episodeId, Guid patientId)
        {
            return PartialView("Claim/Final/Print", billingService.GetFinalPrint(episodeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimFinalSuppliesPdf(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<BillingSuppliesPdf>(new BillingSuppliesPdf(billingService.GetFinalWithSupplies(episodeId, patientId)), "FinalSupplies");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalCategory(Guid episodeId, Guid patientId, string category)
        {
            category = category.Substring(category.IndexOf("_") + 1);
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        claim.EpisodeStartDate = episode.StartDate;
                        claim.EpisodeEndDate = episode.EndDate;
                        var notVerifiedVisits = patientRepository.GetScheduledEventsOnly(Current.AgencyId, episode.PatientId, episode.Id, episode.StartDate, episode.EndDate);//  episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.VisitDate.IsValidDate() && s.VisitDate.ToDateTime().Date >= episode.StartDate.Date && s.VisitDate.ToDateTime().Date <= episode.EndDate.Date && s.DisciplineTask > 0).OrderBy(s => s.VisitDate.ToDateTime().Date).ToList();
                        if (notVerifiedVisits != null && notVerifiedVisits.Count > 0)
                        {
                            claim.AgencyLocationId = patient.AgencyLocationId;
                            var agencyInsurance = new AgencyInsurance();
                            claim.BillVisitDatas = billingService.BillableVisitsData(patient.AgencyLocationId, notVerifiedVisits, claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, billingService.FinalToCharegRates(claim, out agencyInsurance), false);
                        }
                    }
                }

            }
            return PartialView("Claim/Final/" + category, claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalVerifyDemographics(Final claim, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final Basic Info is not verified." };
            var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
            if (patient != null)
            {
                if (claim != null)
                {
                    if (formCollection != null)
                    {
                        var keys = formCollection.AllKeys;
                        if (keys != null && keys.Length > 0)
                        {
                            claim.FirstBillableVisitDateFormat = keys.Contains("FirstBillableVisitDateFormatInput") ? formCollection["FirstBillableVisitDateFormatInput"] : string.Empty;
                            if (keys.Contains("Ub04Locator81"))
                            {
                                var locatorList = formCollection["Ub04Locator81"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3")) locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                    });
                                }
                                claim.Ub04Locator81cca = locators.ToXml();
                            }
                        }
                    }
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        var agencyInsurance = new AgencyInsurance();
                        if (claim.PrimaryInsuranceId >= 1000) agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        else if (claim.PrimaryInsuranceId < 1000) agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                        claim.Insurance = agencyInsurance.ToXml();
                    }
                    if (claim.IsValid)
                    {
                        if (billingRepository.VerifyInfo(Current.AgencyId, claim))
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Final, LogAction.FinalDemographicsVerified, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Final Basic Info is successfully verified.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = claim.ValidationMessage;
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Patient dosen't exist.Try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalVerifyVisits(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final is not verified." };
            if (billingService.VisitVerify(Id, episodeId, patientId, Visit))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalVisitVerified, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final Visit is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalVerifySupplies(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final supply is not verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() && formCollection["Id"].IsGuid() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var episodeId = keys.Contains("episodeId") && formCollection["episodeId"].IsNotNullOrEmpty() && formCollection["episodeId"].IsGuid() ? formCollection["episodeId"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() && formCollection["patientId"].IsGuid() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                var IsSupplyNotBillable = keys.Contains("IsSupplyNotBillable") && formCollection["IsSupplyNotBillable"].IsNotNullOrEmpty() && formCollection["IsSupplyNotBillable"].IsBoolean() ? formCollection["IsSupplyNotBillable"].ToBoolean() : true;
                if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.VisitSupply(Id, episodeId, patientId, IsSupplyNotBillable))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final supply is successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalComplete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Final (EOE) could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.FinalComplete(id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Final(EOE) completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSnapshotGrid(Guid id)
        {
            return PartialView("Claim/Final/Snapshot/Grid", billingRepository.GetFinalSnapShots(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSnapshotEdit(int batchId, Guid id)
        {
            return PartialView("Claim/Final/Snapshot/Edit", billingRepository.GetFinalSnapshot(Current.AgencyId, id, batchId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimFinalSnapshotUpdate(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Final snapshot could not be updated." };
            if (billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "Final"))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Final snapshot was updated successfully.";
            }
            return Json(viewData);
        }
        #endregion

        #region Claim Final Supply
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyNew(Guid finalId)
        {
            return PartialView("Claim/Final/Supply/New", finalId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyCreate([Bind]Supply supply)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply update is unsuccessful." };
            var claim = billingRepository.GetFinal(Current.AgencyId, supply.FinalId);
            if (claim != null)
            {
                var suppliesToEdit = new List<Supply>();
                if (claim.Supply.IsNotNullOrEmpty()) suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
                if (supply != null)
                {
                    supply.UniqueIdentifier = Guid.NewGuid();
                    supply.IsBillable = true;
                    suppliesToEdit.Add(supply);
                    claim.Supply = suppliesToEdit.ToXml();
                    claim.SupplyTotal = billingService.MedicareSupplyTotal(claim);
                    if (billingRepository.UpdateFinal(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Supply is successfuly added.";
                    }
                }
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyBillableGrid(Guid finalId, Guid patientId)
        {
            var supplies = new List<Supply>();
            if (!finalId.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetFinal(Current.AgencyId, finalId);
                if (claim != null && claim.Supply.IsNotNullOrEmpty()) supplies = claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable == true && !s.IsDeprecated).ToList() ?? new List<Supply>();
            }
            return View(new GridModel(supplies));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyNonBillableGrid(Guid finalId, Guid patientId)
        {
            var supplies = new List<Supply>();
            if (!finalId.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetFinal(Current.AgencyId, finalId);
                if (claim != null && claim.Supply.IsNotNullOrEmpty()) supplies = claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable == false && !s.IsDeprecated).ToList() ?? new List<Supply>();
            }
            return View(new GridModel(supplies));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyChangeStatus(List<Guid> id, Guid finalId, bool isBillable)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply bill status change is unsuccessful." };
            if (!finalId.IsEmpty())
            {
                if (id != null && id.Count > 0)
                {
                    var claim = billingRepository.GetFinal(Current.AgencyId, finalId);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
                        if (suppliesToEdit != null && suppliesToEdit.Count > 0)
                        {
                            suppliesToEdit.ForEach(s =>
                            {
                                if (!s.UniqueIdentifier.IsEmpty() && id.Contains(s.UniqueIdentifier)) s.IsBillable = isBillable;
                            });
                            claim.Supply = suppliesToEdit.ToXml();
                            if (billingRepository.UpdateFinal(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supply bill status is successfuly updated.";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyEdit(Guid id, Guid finalId)
        {
            var claim = billingRepository.GetFinal(Current.AgencyId, finalId);
            var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            var supplyToEdit = suppliesToEdit.FirstOrDefault(s => s.UniqueIdentifier == id);
            supplyToEdit.FinalId = finalId;
            return PartialView("Claim/Final/Supply/Edit", supplyToEdit);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyUpdate([Bind]Supply supply)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply update is unsuccessful." };
            var suppliesEdited = new List<Supply>();
            var claim = billingRepository.GetFinal(Current.AgencyId, supply.FinalId);
            if (claim != null && claim.Supply.IsNotNullOrEmpty())
            {
                var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
                suppliesEdited = suppliesToEdit.Where(s => s.IsBillable == supply.IsBillable && !s.IsDeprecated).ToList() ?? new List<Supply>();
                var supplyToEdit = suppliesToEdit.FirstOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                if (supplyToEdit != null)
                {
                    supplyToEdit.Code = supply.Code;
                    supplyToEdit.Date = supply.Date;
                    supplyToEdit.Quantity = supply.Quantity;
                    supplyToEdit.UnitCost = supply.UnitCost;
                    claim.Supply = suppliesToEdit.ToXml();
                    claim.SupplyTotal = billingService.MedicareSupplyTotal(claim);
                    if (billingRepository.UpdateFinal(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Supply is successfuly updated.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimFinalSupplyDelete(List<Guid> id, Guid finalId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supplies are not deleted." };
            if (!finalId.IsEmpty())
            {
                if (id != null && id.Count > 0)
                {
                    var claim = billingRepository.GetFinal(Current.AgencyId, finalId);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
                        suppliesToEdit.ForEach(s =>
                        {
                            if (!s.UniqueIdentifier.IsEmpty() && id.Contains(s.UniqueIdentifier)) s.IsDeprecated = true;
                        });
                        claim.Supply = suppliesToEdit.ToXml();
                        claim.SupplyTotal = billingService.MedicareSupplyTotal(claim);
                        if (billingRepository.UpdateFinal(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Selected Supplies have been deleted successfully.";
                        }
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Claim Managed
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimManagedCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType != 3).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        return PartialView("Claim/Managed/Center", billingService.ManagedBill(agencyMainBranch.Id, agencyMedicareInsurance.Id, (int)ManagedClaimStatus.ClaimCreated));
                    }
                }
            }
            return PartialView("Claim/Managed/Center", new ManagedBillViewData { Bills = new List<ManagedBill>() });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimManagedGrid(Guid branchId, int insuranceId)
        {
            return PartialView("Claim/Managed/Grid", billingService.ManagedBill(branchId, insuranceId, (int)ManagedClaimStatus.ClaimCreated));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimManaged(Guid id, Guid patientId)
        {
            if (id.IsEmpty() || patientId.IsEmpty()) return PartialView("Claim/Managed/Verify", new Final());
            return PartialView("Claim/Managed/Verify", billingRepository.GetManagedClaim(Current.AgencyId, patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimManagedCategory(Guid id, Guid patientId, string category)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, id);
            /*
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    claim.Visits = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, claim.EpisodeStartDate, claim.EpisodeEndDate, new int[] { }, new int[] { }, false);
                    if (claim.Visits != null && claim.Visits.Count > 0)
                    {
                        claim.AgencyLocationId = patient.AgencyLocationId;
                        var agencyInsurance = new AgencyInsurance();
                        claim.BillVisitDatas = billingService.BillableVisitsData(patient.AgencyLocationId, claim.Visits, ClaimType.MAN, billingService.ManagedToCharegRates(claim, out agencyInsurance), true);
                    }
                }
            }
            */
            return PartialView("Claim/Managed/" + category.Substring(category.IndexOf("_") + 1), claim);
        }
        #endregion

        #region Claim Pending
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimPending()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var selectionLists = new FilterViewData();
            if (agency != null)
            {
                var agencyBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyBranch != null && !agencyBranch.Id.IsEmpty())
                {
                    selectionLists.Branches = agencyService.Branchs(agencyBranch.Id.ToString(), false);
                    selectionLists.SelecetdBranch = agencyBranch.Id;
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                    {
                        selectionLists.Insurances = agencyService.Insurances(payorType, false, true);
                        selectionLists.SelecetdInsurance = agency.Payor;
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id, false, true);
                            selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                        }
                    }
                }
                else
                {
                    var agencyBranches = agencyRepository.GetBranches(Current.AgencyId).FirstOrDefault();
                    if (agencyBranches != null)
                    {
                        selectionLists.Branches = agencyService.Branchs(agencyBranches.Id.ToString(), false);
                        selectionLists.SelecetdBranch = agencyBranches.Id;
                        int payorType;
                        if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                        {
                            selectionLists.Insurances = agencyService.Insurances(payorType, false, true);
                            selectionLists.SelecetdInsurance = agency.Payor;
                        }
                        else
                        {
                            var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                            if (agencyMedicareInsurance != null)
                            {
                                selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id, false, true);
                                selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                            }
                        }
                    }
                }
            }
            return PartialView("Claim/Pending/Main", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimPendingRAPGrid(Guid branchId, string insuranceId)
        {
            return PartialView("Claim/Pending/Grid", billingService.PendingClaimRaps(branchId, insuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimPendingFinalGrid(Guid branchId, string insuranceId)
        {
            return PartialView("Claim/Pending/Grid", billingService.PendingClaimFinals(branchId, insuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimPendingEdit(Guid id, string type)
        {
            return PartialView("Claim/Pending/Edit", billingService.PendingClaim(id, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimPendingUpdate(Guid Id, string Type, DateTime PaymentDate, int Status, double PaymentAmount)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Pending claim updates failed." };
            if (Type == "RAP")
            {
                if (billingService.UpdateRapClaimStatus(Id, PaymentDate, PaymentAmount, Status))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Pending claim updated successfully.";
                }
            }
            else
            {
                if (billingService.UpdateFinalClaimStatus(Id, PaymentDate, PaymentAmount, Status))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Pending claim updated successfully.";
                }
            }
            return Json(viewData);
        }
        #endregion

        #region History Medicare
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryMedicare()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var selectionLists = new FilterViewData();
            if (agency != null)
            {
                int payorType;
                if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                {
                    selectionLists.Insurances = agencyService.Insurances(payorType, false, true);
                    selectionLists.SelecetdInsurance = agency.Payor;
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id, false, true);
                        selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                    }
                }
            }
            return PartialView("History/Medicare/Layout", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryMedicareContent(Guid patientId, Guid? claimId, string claimType)
        {
            var viewData = new BillingHistoryViewData();
            if (claimId.HasValue) viewData.ClaimInfo = billingService.GetClaimSnapShotInfo(patientId, (Guid)claimId, claimType);
            else
            {
                viewData.ClaimInfo = new ClaimInfoSnapShotViewData();
                viewData.ClaimInfo.PatientId = patientId;
            }
            return PartialView("History/Medicare/Content", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryMedicareInfo(Guid patientId, Guid claimId, string claimType)
        {
            return PartialView("History/Medicare/Info", billingService.GetClaimSnapShotInfo(patientId, claimId, claimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryMedicareActivity(Guid patientId)
        {
            return PartialView("History/Medicare/Activity", patientId);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryMedicareActivityGrid(Guid patientId)
        {
            return View(new GridModel(billingService.Activity(patientId, 0)));
        }
        #endregion

        #region Remittance
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceList()
        {
            return PartialView("Remittance/List/Main", billingRepository.GetRemittances(Current.AgencyId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceListGrid(DateTime startDate, DateTime endDate)
        {
            return PartialView("Remittance/List/Grid", billingRepository.GetRemittances(Current.AgencyId, startDate, endDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult RemittanceListPrint(string startDate, string endDate)
        {
            if (startDate.IsValidPHPDate() && endDate.IsValidPHPDate()) return FileGenerator.Pdf<RemittancesPdf>(new RemittancesPdf(new RemittanceListViewData(billingRepository.GetRemittances(Current.AgencyId, startDate.ToDateTimePHP(), endDate.ToDateTimePHP()).ToList<RemittanceLean>(), agencyRepository.Get(Current.AgencyId))), "Remittances");
            else return FileGenerator.Pdf<RemittancesPdf>(new RemittancesPdf(new RemittanceListViewData(billingRepository.GetRemittances(Current.AgencyId, DateTime.Now.AddDays(-59), DateTime.Now).ToList<RemittanceLean>(), agencyRepository.Get(Current.AgencyId))), "Remittances");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RemittanceListPrintPreview(string startDate, string endDate)
        {
            if (startDate.IsValidPHPDate() && endDate.IsValidPHPDate()) return PartialView("Remittance/List/Print", new RemittanceListViewData(billingRepository.GetRemittances(Current.AgencyId, startDate.ToDateTimePHP(), endDate.ToDateTimePHP()).ToList<RemittanceLean>(), agencyRepository.Get(Current.AgencyId)));
            else return PartialView("Remittance/List/Print", new RemittanceListViewData(billingRepository.GetRemittances(Current.AgencyId, DateTime.Now.AddDays(-59), DateTime.Now).ToList<RemittanceLean>(), agencyRepository.Get(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetail(Guid id)
        {
            var remittance = new Remittance();
            try
            {
                remittance = billingRepository.GetRemittanceWithClaims(Current.AgencyId, id);
                return PartialView("Remittance/Detail/Main", remittance);
            }
            catch (Exception ex)
            {
                return PartialView("Remittance/Detail/Main", remittance);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetailGrid(Guid id)
        {
            var remittance = new Remittance();
            try
            {
                return PartialView("Remittance/Detail/Grid", billingRepository.GetRemittanceWithClaims(Current.AgencyId, id));
            }
            catch (Exception ex)
            {
                return PartialView("Remittance/Detail/Grid", remittance);
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult RemittanceDetailPrint(Guid id)
        {
            var remittance = billingRepository.GetRemittance(Current.AgencyId, id);
            if (remittance != null && remittance.Data.IsNotNullOrEmpty()) remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
            remittance.Agency = agencyRepository.Get(Current.AgencyId);
            return FileGenerator.Pdf<RemittancePdf>(new RemittancePdf(remittance), "RemittanceDetail");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RemittanceDetailPrintPreview(Guid id)
        {
            var remittance = billingRepository.GetRemittance(Current.AgencyId, id);
            if (remittance != null && remittance.Data.IsNotNullOrEmpty()) remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
            remittance.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Remittance/Detail/Print", remittance);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RemittanceDetailUpload()
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance upload is unsuccessful. Try again." };
            var file = Request.Files.Get(0);
            if (file != null)
            {
                if (file.ContentType != "Text/Plain")
                {
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        if (billingService.AddRemittanceUpload(file))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The upload is successful.";
                        }
                        else viewData.errorMessage = "The remittance upload is unsuccessful. Try again.";
                    }
                    else viewData.errorMessage = "The upload file is empty.";
                }
                else viewData.errorMessage = "The upload is not on the right format.";
            }
            else viewData.errorMessage = "There is no file uploaded.";
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetailDelete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance could not be deleted. Please try again." };
            if (billingRepository.DeleteRemittance(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The remittance has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RemittanceDetailPost(Guid id, List<string> episodes)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "There is a problem posting the remittance." };
            if (!id.IsEmpty())
            {
                if (episodes != null && episodes.Count > 0)
                {
                    if (billingService.PostRemittance(id, episodes))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "There remittance posted successfully.";
                    }
                }
                else viewData.errorMessage = "The remittance Id is not found. Try again.";
            }
            else viewData.errorMessage = "The remittance Id is not found. Try again.";
            return Json(viewData);
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Unprocessed()
        {
            return Json(billingService.GetAllUnProcessedBill(true,5));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleManagedANSI(Guid Id)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    ClaimData claimDataOut = null;
                    BillExchange billExchage;
                    var ids = new List<Guid>();
                    ids.Add(Id);
                    if (billingService.GenerateManaged(ids, ClaimCommandType.download, out claimDataOut, out billExchage, patient.AgencyLocationId, claim.PrimaryInsuranceId))
                    {
                        if (claimDataOut != null)
                        {
                            return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                        }
                    }
                    else
                    {
                        if (billExchage != null)
                        {
                            return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                        }
                    }

                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error in processing of the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateManagedANSI(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateManaged(ManagedClaimSelected, ClaimCommandType.download, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                if (claimDataOut != null)
                {
                    return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                }
            }
            else
            {
                if (billExchage != null)
                {
                    return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error in processing of the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitManagedClaimDirectly(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) are not processed. Try again." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateManaged(ManagedClaimSelected, ClaimCommandType.direct, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) are processed successfully.";
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UB04Pdf(Guid patientId, Guid Id, string type)
        {
            return FileGenerator.Pdf<UB04Pdf>(new UB04Pdf(billingService.GetUBOFourInfo(patientId, Id, type), this.billingService), "UB04");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult HCFA1500Pdf(Guid patientId, Guid Id)
        {
            return FileGenerator.Pdf<HCFA1500Pdf>(new HCFA1500Pdf(billingService.GetHCFA1500Info(patientId, Id), this.billingService), "HCFA1500");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitManagedClaims(List<Guid> ManagedClaimSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your status update is not Successful." };
            if (billingService.UpdateManagedClaimStatus(ManagedClaimSelected, StatusType))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your status update is successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedCharts()
        {
            var selectionLists = new FilterViewData();
            selectionLists.Insurances = agencyService.Insurances(0, true, true);
            selectionLists.SelecetdInsurance = "0";
            return PartialView("Managed/Center/Layout", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedChartsContent(Guid patientId)
        {
            return PartialView("Managed/Center/Content", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSnapShotClaimInfo(Guid patientId, Guid claimId)
        {
            return PartialView("Managed/Center/Info", billingService.GetManagedClaimSnapShotInfo(patientId, claimId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SnapShotClaims(Guid Id, string Type)
        {
            return View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSnapShotClaim(Guid Id, long BatchId, string Type, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, PaymentAmount, PaymentDate, Status, Type);
            return View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsActivity(Guid patientId, int insuranceId)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<ManagedClaimLean>()));
            }
            return View(new GridModel(billingRepository.GetManagedClaimsPerPatient(Current.AgencyId, patientId, insuranceId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(Guid patientId, Guid id, string type)
        {
            return PartialView("Update", billingService.GetClaimViewData(patientId, id, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveUpdate(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                // var primaryInsuranceId = keys.Contains("PrimaryInsuranceId") && formCollection["PrimaryInsuranceId"].IsNotNullOrEmpty() && formCollection["PrimaryInsuranceId"].IsInteger() ? formCollection["PrimaryInsuranceId"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;

                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() ? !formCollection["PaymentAmount"].IsDouble() : false, "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? !formCollection["PaymentDateValue"].IsValidDate() : false, "Payment date is not a right format."));

                if (!Id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                            var paymentDate = formCollection["PaymentDateValue"].IsDate() ? formCollection["PaymentDateValue"].ToDateTime() : DateTime.MinValue;
                            if (billingService.UpdateProccesedClaimStatus(patient, Id, type, paymentDate, paymentAmount, paymentDate, claimStatus, comment))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The claim updated successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message; ;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The patient information don't exist.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimData(Guid patientId, Guid claimId, string claimType)
        {
            var viewData = new BillingHistoryViewData();
            viewData.ClaimInfo = billingService.GetClaimSnapShotInfo(patientId, claimId, claimType);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateClaim(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var episodeId = keys.Contains("EpisodeId") && formCollection["EpisodeId"].IsNotNullOrEmpty() ? formCollection["EpisodeId"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                if (!episodeId.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient dosn't exist."));
                    rules.Add(new Validation(() => !patientRepository.IsEpisodeExist(Current.AgencyId, episodeId), "Episode dosn't exist for this claim."));
                    rules.Add(new Validation(() => billingService.IsEpisodeHasClaim(episodeId, patientId, type), "Episode already has claim."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.AddClaim(patientId, episodeId, type))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please verify the information provided.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewClaim(Guid patientId, string type)
        {
            var newClaimData = new NewClaimViewData();
            newClaimData.EpisodeData = billingRepository.GetEpisodeNeedsClaim(Current.AgencyId, patientId, type);
            newClaimData.Type = type;
            newClaimData.PatientId = patientId;
            return PartialView("NewClaim", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaim(Guid patientId)
        {
            var newClaimData = new NewManagedClaimViewData();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    newClaimData.Insurances = agencyService.Insurances(patient.PrimaryInsurance, false, false);
                    newClaimData.SelecetdInsurance = patient.PrimaryInsurance;
                    newClaimData.PatientId = patientId;
                }
            }
            return PartialView("Managed/NewManagedClaim", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateManagedClaim(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var insuranceId = keys.Contains("InsuranceId") && formCollection["InsuranceId"].IsNotNullOrEmpty() && formCollection["InsuranceId"].IsInteger() ? formCollection["InsuranceId"].ToInteger() : -1;
                var startDate = keys.Contains("StartDate") && formCollection["StartDate"].IsNotNullOrEmpty() && formCollection["StartDate"].IsValidDate() ? formCollection["StartDate"].ToDateTime() : DateTime.MinValue;
                var endDate = keys.Contains("EndDate") && formCollection["EndDate"].IsNotNullOrEmpty() && formCollection["EndDate"].IsValidDate() ? formCollection["EndDate"].ToDateTime() : DateTime.MinValue;
                if (!patientId.IsEmpty())
                {
                    rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient dosn't exist."));
                    rules.Add(new Validation(() => startDate.Date <= DateTime.MinValue.Date, "Claim start Date don't have a correct date value."));
                    rules.Add(new Validation(() => endDate.Date <= DateTime.MinValue.Date, "Claim end Date don't have a correct date value."));
                    rules.Add(new Validation(() => startDate.Date > endDate.Date, "Claim start date must be less than claim end date."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.AddManagedClaim(patientId, startDate, endDate, insuranceId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim added successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please verify the information provided.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEpisodes(Guid managedClaimId, Guid patientId)
        {
            return PartialView("Managed/MultipleEpisodes", billingService.GetManagedClaimEpisodes(patientId, managedClaimId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedCareSupplyBillable(Guid Id, Guid patientId)
        {
            var supplies = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null && claim.Supply.IsNotNullOrEmpty())
                {
                    supplies = claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable == true && !s.IsDeprecated).ToList() ?? new List<Supply>();
                    if (!claim.IsSupplyVerified)
                    {
                        supplies.ForEach(s =>
                        {
                            var supply = agencyRepository.GetSupply(Current.AgencyId, s.UniqueIdentifier);
                            if (supply != null)
                            {
                                s.Code = supply.Code;
                                s.UnitCost = supply.UnitCost;
                                s.RevenueCode = supply.RevenueCode;
                            }
                        });
                    }
                }
            }
            return View(new GridModel(supplies));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedCareSupplyUnBillable(Guid Id, Guid patientId)
        {
            var supplies = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null && claim.Supply.IsNotNullOrEmpty())
                {
                    supplies = claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable == false && !s.IsDeprecated).ToList() ?? new List<Supply>();
                    if (!claim.IsSupplyVerified)
                    {
                        supplies.ForEach(s =>
                        {
                            var supply = agencyRepository.GetSupply(Current.AgencyId, s.UniqueIdentifier);
                            if (supply != null)
                            {
                                s.Code = supply.Code;
                                s.UnitCost = supply.UnitCost;
                                s.RevenueCode = supply.RevenueCode;
                            }
                        });
                    }
                }
            }
            return View(new GridModel(supplies));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeSupplyBillStatus(Guid Id, Guid PatientId, List<Guid> UniqueIdentifier, bool IsBillable)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply bill status change is unsuccessful." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (UniqueIdentifier != null && UniqueIdentifier.Count > 0)
                {
                    var claim = billingRepository.GetManagedClaim(Current.AgencyId, PatientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();

                        if (suppliesToEdit != null && suppliesToEdit.Count > 0)
                        {
                            suppliesToEdit.ForEach(s =>
                            {
                                if (!s.UniqueIdentifier.IsEmpty() && UniqueIdentifier.Contains(s.UniqueIdentifier))
                                {
                                    s.IsBillable = IsBillable;
                                }
                            });
                            claim.Supply = suppliesToEdit.ToXml();
                            if (billingRepository.UpdateManagedClaimModel(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supply bill status is successfuly updated.";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedCareSupplyBillableUpdate(Guid Id, Guid patientId, Supply suppy, bool IsBillable)
        {
            var suppliesEdited = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null && claim.Supply.IsNotNullOrEmpty())
                {
                    var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
                    suppliesEdited = suppliesToEdit.Where(s => s.IsBillable == IsBillable && !s.IsDeprecated).ToList() ?? new List<Supply>();
                    var supplyToEdit = suppliesToEdit.FirstOrDefault(s => s.UniqueIdentifier == suppy.UniqueIdentifier);
                    if (supplyToEdit != null)
                    {
                        supplyToEdit.RevenueCode = suppy.RevenueCode;
                        supplyToEdit.Code = suppy.Code;
                        supplyToEdit.Date = suppy.Date;
                        supplyToEdit.Quantity = suppy.Quantity;
                        supplyToEdit.UnitCost = suppy.UnitCost;
                        claim.Supply = suppliesToEdit.ToXml();
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            suppliesEdited = suppliesToEdit.Where(s => s.IsBillable == IsBillable && !s.IsDeprecated).ToList() ?? new List<Supply>();
                        }
                    }
                }
            }
            return View(new GridModel(suppliesEdited));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedCareSupplyBillableAdd(Guid Id, Guid patientId, Supply supply)
        {
            var suppliesEdited = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null)
                {
                    var suppliesToEdit = new List<Supply>();
                    if (claim.Supply.IsNotNullOrEmpty())
                    {
                        suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
                    }
                    suppliesEdited = suppliesToEdit.Where(s => s.IsBillable == true).ToList() ?? new List<Supply>();
                    if (supply != null)
                    {
                        if (supply.UniqueIdentifier.IsEmpty())
                        {
                            supply.UniqueIdentifier = Guid.NewGuid();
                        }
                        supply.IsBillable = true;
                        suppliesToEdit.Add(supply);
                        claim.Supply = suppliesToEdit.ToXml();
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            suppliesEdited = suppliesToEdit.Where(s => s.IsBillable == true).ToList() ?? new List<Supply>();
                        }
                    }
                }
            }
            return View(new GridModel(suppliesEdited));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedCareSuppliesDelete(Guid Id, Guid PatientId, List<Guid> UniqueIdentifier)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supplies are not deleted." };

            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (UniqueIdentifier != null && UniqueIdentifier.Count > 0)
                {
                    var claim = billingRepository.GetManagedClaim(Current.AgencyId, PatientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        var suppliesToEdit = claim.Supply.ToObject<List<Supply>>().ToList() ?? new List<Supply>();

                        suppliesToEdit.ForEach(s =>
                        {
                            if (!s.UniqueIdentifier.IsEmpty() && UniqueIdentifier.Contains(s.UniqueIdentifier))
                            {
                                s.IsDeprecated = true;
                            }
                        });
                        claim.Supply = suppliesToEdit.ToXml();
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supplies are successfuly deleted.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInfoVerify(ManagedClaim claim, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim Basic Info is  not verified." };
            var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
            if (patient != null)
            {
                if (claim.IsValid)
                {
                    if (formCollection != null)
                    {
                        var keys = formCollection.AllKeys;
                        if (keys != null && keys.Length > 0)
                        {
                            if (keys.Contains("Ub04Locator81"))
                            {
                                var locatorList = formCollection["Ub04Locator81"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                        }
                                    });
                                }
                                claim.Ub04Locator81cca = locators.ToXml();
                            }
                        }
                    }
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        var agencyInsurance = new AgencyInsurance();
                        if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                        else if (claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                        }
                        claim.Insurance = agencyInsurance.ToXml();
                    }

                    if (billingRepository.ManagedVerifyInfo(claim, Current.AgencyId))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedDemographicsVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Managed Claim Basic Info is successfully verified.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = claim.ValidationMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Patient dosen't exist.Try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ManagedClaimAssessmentData(string input)
        {
            var managedClaimEpisodeData = new ManagedClaimEpisodeData();
            if (input.IsNotNullOrEmpty())
            {
                var inputArray = input.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                if (inputArray != null && inputArray.Length > 0 && input.Length > 0)
                {
                    managedClaimEpisodeData = billingService.GetEpisodeAssessmentData(inputArray[0].ToGuid(), inputArray[1].ToGuid());
                }
            }
            return Json(managedClaimEpisodeData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim visit is not verified." };
            if (billingService.ManagedVisitVerify(Id, patientId, Visit))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedVisitVerified, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Managed Claim Visit is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSupplyVerify(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim supply is not verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.ManagedVisitSupply(Id, patientId))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Managed Claim supply is successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSummary(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            return PartialView("Managed/ClaimSummary", billingService.ManagedClaimToGenerate(ManagedClaimSelected, BranchId, PrimaryInsurance));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ManagedUB04Pdf(Guid patientId, Guid Id)
        {
            return FileGenerator.Pdf<ManagedUB04Pdf>(new ManagedUB04Pdf(billingService.GetManagedUBOFourInfo(patientId, Id), billingService), "UB04");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaim(Guid patientId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be deleted. Please try again." };
            var rules = new List<Validation>();
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingRepository.DeleteManagedClaim(Current.AgencyId, patientId, id))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, id.ToString(), LogType.ManagedClaim, LogAction.ManagedDeleted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in deleting the claim. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the claim. Please try again";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaim(Guid patientId, Guid id)
        {
            return PartialView("Managed/Update", billingService.GetManagedClaimInfo(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimStatus(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;
               
                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() ? !formCollection["PaymentAmount"].IsDouble() : false, "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? !formCollection["PaymentDateValue"].IsValidDate() : false, "Payment date is not a right format."));
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                            var paymentDate = formCollection["PaymentDateValue"].ToDateTime();
                            if (billingService.UpdateProccesedManagedClaimStatus(patient, Id, paymentDate, paymentAmount, paymentDate, claimStatus, comment))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The claim updated successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message; ;
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedComplete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Managed Claim could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.ManagedComplete(id, patientId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Managed Claim completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteClaim(Guid patientId, Guid id, string type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be deleted. Please try again." };
            var rules = new List<Validation>();
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                rules.Add(new Validation(() => !billingService.IsEpisodeHasClaim(id, patientId, type), "This claim does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (patientService.DeleteClaim(patientId, id, type))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in deleting the claim. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the claim. Please try again";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePending(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim update was not successful." };
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var rapIds = keys.Contains("RapId") && formCollection["RapId"].IsNotNullOrEmpty() ? formCollection["RapId"].ToArray() : null;
                var finalIds = keys.Contains("FinalId") && formCollection["FinalId"].IsNotNullOrEmpty() ? formCollection["FinalId"].ToArray() : null;
                if ((rapIds != null && rapIds.Length > 0) || (finalIds != null && finalIds.Length > 0))
                {
                    if (rapIds != null && rapIds.Length > 0)
                    {
                        foreach (var id in rapIds)
                        {
                            rules.Add(new Validation(() => keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
                            rules.Add(new Validation(() => keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
                        }
                    }
                    if (finalIds != null && finalIds.Length > 0)
                    {
                        foreach (var id in finalIds)
                        {
                            rules.Add(new Validation(() => keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
                            rules.Add(new Validation(() => keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
                        }
                    }
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (billingService.UpdatePendingClaimStatus(formCollection, rapIds, finalIds))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The claim update was not successful.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimLogs(string type, Guid claimId, Guid patientId)
        {
            if (type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() || type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() || type.ToUpperCase() == LogType.ManagedClaim.ToString().ToUpperCase())
            {
                return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Patient, type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() ? LogType.Rap : (type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() ? LogType.Final : LogType.ManagedClaim), patientId, claimId.ToString()));
            }
            return PartialView("ActivityLogs", new List<AppAudit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedList()
        {
            return PartialView("SubmittedList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSubmittedList(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            return View(new GridModel(billingRepository.ClaimDatas(Current.AgencyId, StartDate, EndDate, ClaimType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimDetail(int Id)
        {
            return PartialView("SubmittedClaimsDetail", billingService.GetSubmittedBatchClaims(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimResponse(int Id)
        {
            var claimData = billingRepository.GetClaimData(Current.AgencyId, Id);
            return PartialView("ClaimResponse", claimData != null ? claimData.Response.Replace("\r\n", "<br />") : string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRemittance(Guid Id, string Type)
        {
            var claimInfos = new List<PaymentInformation>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("rap"))
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, Id);
                    if (rap != null && rap.Remittance.IsNotNullOrEmpty())
                    {
                        return PartialView("ClaimRemittance", rap.Remittance.ToObject<List<PaymentInformation>>());
                    }
                }
                else if (Type.IsEqual("final"))
                {
                    var final = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                    if (final != null && final.Remittance.IsNotNullOrEmpty())
                    {
                        return PartialView("ClaimRemittance", final.Remittance.ToObject<List<PaymentInformation>>());
                    }
                }
            }
            return PartialView("ClaimRemittance", claimInfos);
        }

        #endregion
    }
}
