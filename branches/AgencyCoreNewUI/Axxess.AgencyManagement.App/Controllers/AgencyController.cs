﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Text;
    using System.Web.Mvc;
    using System.Linq;
    using System.Collections.Generic;

    using Telerik.Web.Mvc;

    using Enums;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.iTextExtension;

    using Axxess.Membership.Repositories;

    using Axxess.Log.Enums;
    using Axxess.Log.Common;
    
    using Axxess.LookUp.Repositories;
    

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly IPhysicianService physicianService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly ILookupRepository lookupRepository;

        public AgencyController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider coreDataProvider, ILookUpDataProvider lookUpDataProvider, IAgencyService agencyService, IUserService userService, IPatientService patientService, IPhysicianService physicianService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "dataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.physicianService = physicianService;
            this.loginRepository = coreDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Agency Actions
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Info()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Signature()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoContent(Guid branchId)
        {
            var viewData = new AgencyLocation();
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            if (agency != null)
            {
                viewData = agency.GetBranch(branchId);
            }
            return PartialView("InfoContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInfo([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();
            var existingLocation = agencyRepository.FindLocation(Current.AgencyId, location.Id);
            if (existingLocation != null)
            {
                if (existingLocation.IsLocationStandAlone)
                {
                    existingLocation.Name = location.Name;
                    existingLocation.AddressLine1 = location.AddressLine1;
                    existingLocation.AddressLine2 = location.AddressLine2;
                    existingLocation.AddressCity = location.AddressCity;
                    existingLocation.AddressStateCode = location.AddressStateCode;
                    existingLocation.AddressZipCode = location.AddressZipCode;
                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }
                }
                else
                {
                    existingLocation.Name = location.Name;
                    existingLocation.AddressLine1 = location.AddressLine1;
                    existingLocation.AddressLine2 = location.AddressLine2;
                    existingLocation.AddressCity = location.AddressCity;
                    existingLocation.AddressStateCode = location.AddressStateCode;
                    existingLocation.AddressZipCode = location.AddressZipCode;
                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }
                }

                if (!agencyRepository.UpdateLocation(existingLocation))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Location could not be updated.";
                }
                else
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was updated successfully.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Location could not be found.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckSignature(string signature)
        {
            Check.Argument.IsNotEmpty(signature, "signature");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your signature does not match the one on file." };
            if (userService.IsSignatureCorrect(Current.UserId, signature))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Signature verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AfterHoursSupport()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueue()
        {
            ViewData["GroupName"] = "EventDate";
            return PartialView("PrintQueue/Main", agencyService.GetPrintQueue());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueueGrid(string groupName)
        {
            ViewData["GroupName"] = groupName.IsNotNullOrEmpty() ? groupName : "EventDate";
            return PartialView("PrintQueue/List", agencyService.GetPrintQueue());
        }

        [GridAction]
        public ActionResult Users()
        {
            return View(new GridModel(userRepository.GetAgencyUsers(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Blankforms()
        {
            return PartialView();
        }
        #endregion

        #region Contact
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactNew() {
            return PartialView("Contact/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ContactCreate([Bind] AgencyContact contact) {
            Check.Argument.IsNotNull(contact, "contact");
            var viewData = new JsonViewData();
            if (contact.IsValid) {
                if (!agencyService.CreateContact(contact)) {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the contact.";
                } else {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Contact was saved successfully";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactList() {
            return PartialView("Contact/List");
        }

        [GridAction]
        public ActionResult ContactGrid() {
            return View(new GridModel(agencyRepository.GetContacts(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactEdit(Guid id) {
            return PartialView("Contact/Edit", agencyRepository.FindContact(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactUpdate([Bind] AgencyContact contact) {
            Check.Argument.IsNotNull(contact, "contact");
            var viewData = new JsonViewData();
            if (contact.IsValid) {
                if (agencyRepository.FindContact(Current.AgencyId, contact.Id) != null) {
                    contact.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditContact(contact)) {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the contact.";
                    } else {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Contact was edited successfully";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected contact don't exist.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ContactDelete(Guid id) {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Contact could not be deleted. Please try again." };
            if (agencyRepository.DeleteContact(Current.AgencyId, id)) {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyContact, LogAction.AgencyContactDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Contact has been deleted.";
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Contact could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactLogs(Guid id) {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyContact, Current.AgencyId, id.ToString()));
        }
        #endregion

        #region Hospital
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalNew() {
            return PartialView("Hospital/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalCreate([Bind] AgencyHospital hospital) {
            Check.Argument.IsNotNull(hospital, "hospital");
            var viewData = new JsonViewData();
            if (hospital.IsValid) {
                hospital.AgencyId = Current.AgencyId;
                hospital.Id = Guid.NewGuid();
                if (!agencyRepository.AddHospital(hospital)) {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the hospital.";
                } else {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was saved successfully";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalList() {
            return PartialView("Hospital/List");
        }

        [GridAction]
        public ActionResult HospitalGrid() {
            return View(new GridModel(agencyRepository.GetHospitals(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalEdit(Guid id) {
            return PartialView("Hospital/Edit", agencyRepository.FindHospital(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalUpdate([Bind] AgencyHospital hospital) {
            Check.Argument.IsNotNull(hospital, "hospital");
            var viewData = new JsonViewData();
            if (hospital.IsValid) {
                if (agencyRepository.FindHospital(Current.AgencyId, hospital.Id) != null) {
                    hospital.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditHospital(hospital)) {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the hospital.";
                    } else {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Hospital was edited successfully";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected Hospial don't exist.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HospitalDelete(Guid id) {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this hospital. Please try again." };
            if (agencyRepository.FindHospital(Current.AgencyId, id) != null) {
                if (agencyRepository.DeleteHospital(Current.AgencyId, id)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was successfully deleted.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the hospital.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected Hospital don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalLogs(Guid id) {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyHospital, Current.AgencyId, id.ToString()));
        }
        #endregion

        #region Physician
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianNew() {
            return PartialView("Physician/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianCreate([Bind] AgencyPhysician agencyPhysician) {
            Check.Argument.IsNotNull(agencyPhysician, "agencyPhysician");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be added" };
            if (agencyPhysician.PhysicianAccess && agencyPhysician.EmailAddress.IsNullOrEmpty()) {
                viewData.errorMessage = "E-mail Address required for Physician Access";
                return Json(viewData);
            }
            if (agencyPhysician.IsValid) {
                agencyPhysician.AgencyId = Current.AgencyId;
                if (!physicianService.CreatePhysician(agencyPhysician)) {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the data.";
                } else {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully Saved";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = agencyPhysician.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PhysicianGet(Guid? id) {
            if (id.HasValue) return Json(physicianRepository.Get((Guid)id, Current.AgencyId));
            else return Json(physicianRepository.GetAgencyPhysicians(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianList() {
            return PartialView("Physician/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianGrid() {
            return View(new GridModel(physicianRepository.GetAgencyPhysicians(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianEdit(Guid id) {
            return PartialView("Physician/Edit", physicianRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianUpdate([Bind] AgencyPhysician physician)
        {
            Check.Argument.IsNotNull(physician, "physician");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be updated." };
            if (physician.PhysicianAccess && physician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "E-mail Address required for Physician Access";
                return Json(viewData);
            }
            if (physician.IsValid)
            {
                if (physicianService.UpdatePhysician(physician))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Physician has been successfully edited";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Physician data could not be saved.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = physician.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PhysicianDelete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be deleted. Please try again." };
            if (physicianRepository.Delete(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLogs(Guid id) {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyPhysician, Current.AgencyId, id.ToString()));
        }
        #endregion

        #region Physician License
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseNew(Guid physicianId)
        {
            return PartialView("Physician/License/New", physicianId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseCreate([Bind] PhysicianLicense physicianLicense)
        {
            Check.Argument.IsNotNull(physicianLicense, "physicianLicense");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician License could not be added" };
            if (physicianService.AddLicense(physicianLicense))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data successfully saved";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseList()
        {
            return PartialView("Physician/License/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseGrid(Guid physicianId)
        {
            return View(new GridModel(physicianRepository.GetLicenses(physicianId, Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseEdit(Guid id, Guid physicianId)
        {
            return PartialView("Physician/License/Edit", physicianRepository.GetLicense(physicianId, Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseUpdate([Bind] PhysicianLicense physicianLicense)
        {
            Check.Argument.IsNotNull(physicianLicense, "physicianLicense");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician License could not be updated" };
            if (physicianService.UpdateLicense(physicianLicense))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data successfully saved";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianLicenseDelete(Guid id, Guid physicianId)
        {
            Check.Argument.IsNotNull(id, "id");
            Check.Argument.IsNotNull(physicianId, "physicianId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician License could not be deleted" };
            if (physicianService.DeleteLicense(id, physicianId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data successfully deleted";
            }
            return Json(viewData);
        }
        #endregion

        #region Location
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationNew()
        {
            return PartialView("Location/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationCreate([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");
            var viewData = new JsonViewData();
            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationList()
        {
            return PartialView("Location/List");
        }

        [GridAction]
        public ActionResult LocationGrid()
        {
            return View(new GridModel(agencyRepository.GetBranches(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationEdit(Guid id)
        {
            return PartialView("Location/Edit", agencyRepository.FindLocation(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationUpdate([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");
            var viewData = new JsonViewData();
            if (location.IsValid)
            {
                if (agencyRepository.FindLocation(Current.AgencyId, location.Id) != null)
                {
                    location.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditLocation(location))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the location.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Location was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected location don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationLogs(Guid id)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyLocation, Current.AgencyId, id.ToString()));
        }
        #endregion

        #region Insurance
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceNew()
        {
            return PartialView("Insurance/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceCreate([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            var rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                if (disciplineList != null && disciplineList.Length > 0)
                {
                    disciplineList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Charge") && formCollection[l + "_Charge"].IsDouble())
                        {
                            visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"].ToDouble(), ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                            rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                        }
                    });
                }
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid) insurance.Charge = visitRatesList.ToXml();
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            if (keys.Contains("Ub04Locator81cca"))
            {
                var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                var locators = new List<Locator>();
                if (locatorList != null && locatorList.Length > 0)
                {
                    locatorList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3")) locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                    });
                }
                insurance.Ub04Locator81cca = locators.ToXml();
            }
            if (insurance.IsValid)
            {
                insurance.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddInsurance(insurance))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the insurance.";
                }
                else
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceAdded, string.Empty);
                    InsuranceEngine.Instance.Refresh(Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Insurance was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceGet()
        {
            return Json(agencyService.GetInsurances());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceList()
        {
            return PartialView("Insurance/List");
        }

        [GridAction]
        public ActionResult InsuranceGrid()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var insurances = new List<InsuranceLean>();
            if (agency != null)
            {
                int payor;
                if (int.TryParse(agency.Payor, out payor))
                {
                    var insurance = lookupRepository.GetInsurance(payor);
                    if (insurance != null)
                    {
                        insurances.Add(new InsuranceLean { Name = "Medicare", PayorType = 3, PayorId = "", InvoiceType = 1, IsTradtionalMedicare = true });
                    }
                }
                var data = agencyRepository.GetLeanInsurances(Current.AgencyId, new int[] { });
                if (data != null && data.Count > 0)
                {
                    insurances.AddRange(data.OrderBy(i => i.Name));
                }
            }
            return View(new GridModel(insurances));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceEdit(int id)
        {
            return PartialView("Insurance/Edit", agencyRepository.FindInsurance(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceUpdate([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                disciplineList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Charge"))
                    {
                        visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"].ToDouble(), ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                    }
                });
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid) insurance.Charge = visitRatesList.ToXml();
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            if (keys.Contains("Ub04Locator81cca"))
            {
                var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                var locators = new List<Locator>();
                if (locatorList != null && locatorList.Length > 0)
                {
                    locatorList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3")) locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                    });
                }
                insurance.Ub04Locator81cca = locators.ToXml();
            }
            if (insurance.IsValid)
            {
                if (agencyRepository.FindInsurance(Current.AgencyId, insurance.Id) != null)
                {
                    insurance.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditInsurance(insurance))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the insurance.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceUpdated, string.Empty);
                        InsuranceEngine.Instance.Refresh(Current.AgencyId);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceDelete(int id)
        {
            Check.Argument.IsNotNegativeOrZero(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance. Please try again." };
            if (agencyRepository.FindInsurance(Current.AgencyId, id) != null)
            {
                if (agencyRepository.DeleteInsurance(Current.AgencyId, id))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceDeleted, string.Empty);
                    InsuranceEngine.Instance.Refresh(Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The insurance was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the insurance.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected insurance don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceLogs(int id)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyInsurance, Current.AgencyId, id.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceSelectList(Guid branchId) 
        {
            var list = new List<object>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances.ForEach(i =>
                {
                    list.Add(new { Name = i.Name, Id = i.Id });
                });
            }
            return Json(list);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceMedicareSelectList(Guid branchId) 
        {
            var list = new List<object>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id });
                    }
                }
            }
            return Json(list);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceMedicareWithHMOSelectList(Guid branchId) 
        {
            var list = new List<object>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id, Selected = true });
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id, Selected = true });
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null) list.Add(new { Name = standardInsurance.Name, Id = standardInsurance.Id, Selected = true });
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                if (i.PayorType == 2) list.Add(new { Name = i.Name, Id = i.Id, Selected = false });
            });
            return Json(list);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsurancePatientSelectList(Guid branchId) 
        {
            var selectList = new StringBuilder();
            selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>-- Select Insurance --</option>", "0", 0);
            selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
            selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null) selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", standardInsurance.Id.ToString(), 0, standardInsurance.Name);
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null) selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", standardInsurance.Id.ToString(), 0, standardInsurance.Name);
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null) selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", standardInsurance.Id.ToString(), 0, standardInsurance.Name);
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(insurance =>
            {
                selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", insurance.Id.ToString(), 1, insurance.Name);
            });
            return Json(selectList.ToString());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceDuplicateVisitRates(int id, int replacedId) 
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error in editing the insurance visit information." };
            var insurance = agencyRepository.FindInsurance(Current.AgencyId, id);
            if (insurance != null)
            {
                var selectedInsurance = agencyRepository.FindInsurance(Current.AgencyId, replacedId);
                if (selectedInsurance != null)
                {
                    if (selectedInsurance.BillData.IsNotNullOrEmpty())
                    {
                        insurance.BillData = selectedInsurance.BillData;
                        if (!agencyRepository.EditInsuranceModal(insurance))
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in editing the insurance visit information.";
                        }
                        else
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceVisitInfoOverWrite, string.Empty);
                            InsuranceEngine.Instance.Refresh(Current.AgencyId);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Insurance was edited successfully";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Selected insurance bill information is empty.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The insurance to be updated don't exist.";
            }
            return Json(viewData);
        }
        #endregion

        #region Insurance BillData
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceBillDataNew(int insuranceId)
        {
            return PartialView("Insurance/BillData/New", insuranceId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceBillDataCreate(int insuranceId, ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this insurance rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            if (chargeRate.IsValid)
            {
                if (insurance != null)
                {
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Insurance rate already exist.";
                            }
                            else
                            {
                                rates.Add(chargeRate);
                                insurance.BillData = rates.ToXml();
                                if (agencyRepository.EditInsuranceModal(insurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate saved successfully";
                                }
                            }
                        }
                        else
                        {
                            rates = new List<ChargeRate>();
                            rates.Add(chargeRate);
                            insurance.BillData = rates.ToXml();
                            if (agencyRepository.EditInsuranceModal(insurance))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance rate saved successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<ChargeRate>();
                        rates.Add(chargeRate);
                        insurance.BillData = rates.ToXml();
                        if (agencyRepository.EditInsuranceModal(insurance))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Insurance rate saved successfully";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = chargeRate.ValidationMessage;
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult InsuranceBillDataGrid(int insuranceId)
        {
            var billDatas = new List<ChargeRate>();
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                billDatas = insurance.BillData.ToObject<List<ChargeRate>>();
            }
            return View(new GridModel(billDatas));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceBillDataEdit(int id, int insuranceId) {
            var chargeRate = new ChargeRate();
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                chargeRate = insurance.BillData.ToObject<List<ChargeRate>>().FirstOrDefault(r => r.Id == id);
                if (chargeRate != null) chargeRate.InsuranceId = insurance.Id;
            }
            return PartialView("Insurance/BillData/Edit", chargeRate);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceBillDataUpdate(ChargeRate chargeRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to add this insurance rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(chargeRate.InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                if (chargeRate.IsValid)
                {
                    if (insurance.BillData.IsNotNullOrEmpty())
                    {
                        var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (rate != null)
                            {
                                rate.PreferredDescription = chargeRate.PreferredDescription;
                                rate.Code = chargeRate.Code;
                                rate.RevenueCode = chargeRate.RevenueCode;
                                rate.Charge = chargeRate.Charge;
                                rate.Modifier = chargeRate.Modifier;
                                rate.Modifier2 = chargeRate.Modifier2;
                                rate.Modifier3 = chargeRate.Modifier3;
                                rate.Modifier4 = chargeRate.Modifier4;
                                rate.ChargeType = chargeRate.ChargeType;
                                if (rate.ChargeType == ((int)BillUnitType.Per15Min).ToString())
                                {
                                    if (chargeRate.IsTimeLimit)
                                    {
                                        rate.IsTimeLimit = chargeRate.IsTimeLimit;
                                        rate.TimeLimitHour = chargeRate.TimeLimitHour;
                                        rate.TimeLimitMin = chargeRate.TimeLimitMin;
                                        rate.SecondDescription = chargeRate.SecondDescription;
                                        rate.SecondCode = chargeRate.SecondCode;
                                        rate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                        rate.SecondModifier = chargeRate.SecondModifier;
                                        rate.SecondModifier2 = chargeRate.SecondModifier2;
                                        rate.SecondModifier3 = chargeRate.SecondModifier3;
                                        rate.SecondModifier4 = chargeRate.SecondModifier4;
                                    }
                                }
                                else if (rate.ChargeType == ((int)BillUnitType.PerVisit).ToString()) rate.Unit = chargeRate.Unit;
                                insurance.BillData = rates.ToXml();
                                if (agencyRepository.EditInsuranceModal(insurance))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Insurance rate updated successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = chargeRate.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceBillDataDelete(int insuranceId, int id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            if (insurance != null)
            {
                if (insurance.BillData.IsNotNullOrEmpty())
                {
                    var rates = insurance.BillData.ToObject<List<ChargeRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == id);
                        if (removed > 0)
                        {
                            insurance.BillData = rates.ToXml();
                            if (agencyRepository.EditInsuranceModal(insurance))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Insurance visit rate deleted successfully";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Insurance VisitRate
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceVisitRatesEdit()
        {
            return PartialView("Insurance/VisitRate", agencyRepository.GetMainLocation(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceVisitRatesContent(Guid branchId)
        {
            var location=agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (location != null)
            {
                if (!location.IsLocationStandAlone)
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        int payor;
                        if (int.TryParse(agency.Payor, out payor))
                        {
                            var insurance = lookupRepository.GetInsurance(payor);
                            if (insurance != null)
                            {
                                location.InsuranceName = insurance.Name;
                                location.SubmitterId = agency.SubmitterId;
                                location.SubmitterName = agency.SubmitterName;
                                location.SubmitterPhone = agency.SubmitterPhone;
                                location.ContactPersonFirstName = agency.ContactPersonFirstName;
                                location.ContactPersonLastName = agency.ContactPersonLastName;
                                location.ContactPersonPhone = agency.ContactPersonPhone;
                            }
                        }
                    }
                }
                else
                {
                    int payor;
                    if (int.TryParse(location.Payor, out payor))
                    {
                        var insurance = lookupRepository.GetInsurance(payor);
                        if (insurance != null)
                        {
                            location.InsuranceName = insurance.Name;
                            location.SubmitterId = location.SubmitterId;
                        }
                    }
                }
            }
            return PartialView("Insurance/VisitRateContent",location );
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsuranceVisitRatesUpdate(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData();
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0 && keys.Contains("AgencyLocationId") && formCollection["AgencyLocationId"].IsNotNullOrEmpty())
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, formCollection["AgencyLocationId"].ToGuid());
                if (agencyLocation != null)
                {
                    if (keys.Contains("Ub04Locator81"))
                    {
                        var locatorList = formCollection["Ub04Locator81"].ToArray();
                        var locators = new List<Locator>();
                        if (locatorList != null && locatorList.Length > 0)
                        {
                            locatorList.ForEach(l =>
                            {
                                if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3")) locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                            });
                        }
                        agencyLocation.Ub04Locator81cca = locators.ToXml();
                    }
                    if (keys.Contains("RateDiscipline"))
                    {
                        var disciplineList = formCollection["RateDiscipline"].ToArray();
                        var visitRatesList = new List<CostRate>();
                        disciplineList.ForEach(l =>
                        {
                            if (keys.Contains(l + "_PerUnit"))
                            {
                                visitRatesList.Add(new CostRate { RateDiscipline = l, PerUnit = formCollection[l + "_PerUnit"] });
                                rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_PerUnit"]) ? !formCollection[l + "_PerUnit"].IsDouble() : false, "Wrong entry"));
                            }
                        });
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid) agencyLocation.Cost = visitRatesList.ToXml();
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "One of the cost rate is not in correct format";
                            return Json(viewData);
                        }
                        if (agencyRepository.EditBranchCost(agencyLocation))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Branch cost rates was successfully edited.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Branch cost rates could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Could not update the branch cost rates.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected branch don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected branch don't exist.";
            }
            return Json(viewData);
        }
        #endregion

        #region Infection
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionNew(Guid? patientId)
        {
            if (patientId.HasValue) return PartialView("Infection/New", (Guid)patientId);
            else return PartialView("Infection/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionCreate([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");
            var viewData = new JsonViewData();
            if (infection.IsValid)
            {
                infection.Id = Guid.NewGuid();
                infection.UserId = Current.UserId;
                infection.AgencyId = Current.AgencyId;
                if (infection.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (infection.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, infection.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this infection report.";
                        return Json(viewData);
                    }
                    else infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                }
                else infection.SignatureText = string.Empty;
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = infection.Id,
                    AgencyId = Current.AgencyId,
                    UserId = infection.UserId,
                    PatientId = infection.PatientId,
                    EpisodeId = infection.EpisodeId,
                    Status = infection.Status,
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = infection.InfectionDate,
                    VisitDate = infection.InfectionDate,
                    DisciplineTask = (int)DisciplineTasks.InfectionReport
                };
                if (agencyRepository.AddInfection(infection))
                {
                    if (patientRepository.AddScheduleEvent(newScheduleEvent))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, infection.EpisodeId, infection.PatientId, infection.Id, Actions.Add, DisciplineTasks.InfectionReport);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Infection was saved successfully";
                    }
                    else
                    {
                        agencyRepository.RemoveInfection(infection.Id);
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in Saving the Infection.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the infection.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionList()
        {
            return PartialView("Infection/List");
        }

        [GridAction]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult InfectionGrid()
        {
            return View(new GridModel(agencyService.GetInfections(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionEdit(Guid Id)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, Id);
            if (infection != null)
            {
                var patient = patientRepository.GetPatientOnly(infection.PatientId, infection.AgencyId);
                if (patient != null) infection.PatientName = patient.DisplayName;
                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
            }
            else infection = new Infection();
            return PartialView("Infection/Edit", infection);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionUpdate([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");
            var viewData = new JsonViewData();
            if (infection.IsValid)
            {
                infection.AgencyId = Current.AgencyId;
                infection.UserId = infection.UserId.IsEmpty() ? Current.UserId : infection.UserId;
                if (infection.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (infection.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, infection.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this infection report.";
                        return Json(viewData);
                    }
                    else
                    {
                        infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) infection.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    }
                }
                else infection.SignatureText = string.Empty;
                var evnt = patientRepository.GetScheduleEventNew(Current.AgencyId, infection.PatientId, infection.EpisodeId, infection.Id);
                if (evnt != null)
                {
                    var oldVisitDate = evnt.VisitDate;
                    var oldStatus = evnt.Status;
                    var oldReason = evnt.ReturnReason;

                    evnt.VisitDate = infection.InfectionDate;
                    evnt.Status = infection.Status;
                    evnt.ReturnReason = string.Empty;
                    if (patientRepository.UpdateScheduleEventNew(evnt))
                    {
                        if (agencyRepository.UpdateInfection(infection))
                        {
                            if (Enum.IsDefined(typeof(ScheduleStatus), evnt.Status)) Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status, DisciplineTasks.InfectionReport, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Infection was updated successfully";

                        }
                        else
                        {
                            evnt.VisitDate = oldVisitDate;
                            evnt.Status = oldStatus;
                            evnt.ReturnReason = oldReason;
                            patientRepository.UpdateScheduleEventNew(evnt);
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Infection could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in updating the data. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in updating the data. Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionPrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Infection/PrintPreview", agencyService.GetInfectionReportPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult InfectionPrint(Guid? episodeId, Guid? patientId, Guid? eventId) 
        {
            if (episodeId.HasValue && patientId.HasValue && eventId.HasValue)
            {
                return FileGenerator.Pdf<InfectionReportPdf>(new InfectionReportPdf(agencyService.GetInfectionReportPrint((Guid)episodeId, (Guid)patientId, (Guid)eventId)), "Infection");
            }
            else
            {
                return FileGenerator.Pdf<InfectionReportPdf>(new InfectionReportPdf(agencyService.GetInfectionReportPrint()), "Infection");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfectionProcess(Guid id, Guid patientId, string action, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Infection Report could not be saved." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (action == ButtonAction.Approve.ToString())
                {
                    if (agencyService.ProcessInfections(action, patientId, id, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Infection Report has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Infection Report could not be approved.";
                    }
                }
                else if (action == ButtonAction.Return.ToString())
                {
                    if (agencyService.ProcessInfections(action, patientId, id, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Infection Report has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Infection Report could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Incident
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IncidentNew(Guid? patientId)
        {
            if (patientId.HasValue) return PartialView("Incident/New", (Guid)patientId);
            else return PartialView("Incident/New");
        }

        public ActionResult IncidentCreate([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");
            var viewData = new JsonViewData();
            if (incident.IsValid) {
                incident.Id = Guid.NewGuid();
                incident.UserId = Current.UserId;
                incident.AgencyId = Current.AgencyId;
                if (incident.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (incident.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, incident.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this incident/accident report.";
                        return Json(viewData);
                    }
                    else incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                }
                else incident.SignatureText = string.Empty;
                var newScheduleEvent = new ScheduleEvent
                {
                    AgencyId=Current.AgencyId,
                    EventId = incident.Id,
                    UserId = incident.UserId,
                    PatientId = incident.PatientId,
                    EpisodeId=incident.EpisodeId,
                    Status = incident.Status,
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = incident.IncidentDate,
                    VisitDate = incident.IncidentDate,
                    DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
                };
                if (agencyRepository.AddIncident(incident))
                {
                    if (patientRepository.AddScheduleEvent(newScheduleEvent))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,incident.EpisodeId, incident.PatientId, incident.Id, Actions.Add, DisciplineTasks.IncidentAccidentReport);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Incident / Accident was saved successfully";
                    }
                    else
                    {
                        agencyRepository.RemoveIncidentReport(incident.Id);
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the incident / accident.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the Incident / Accident.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IncidentList()
        {
            return PartialView("Incident/List");
        }

        [GridAction]
        public ActionResult IncidentGrid()
        {
            return View(new GridModel(agencyService.GetIncidents(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IncidentEdit(Guid Id)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, Id);
            if (incident != null)
            {
                var patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);
                if (patient != null) incident.PatientName = patient.DisplayName;
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;

                    }
                }
            }
            else incident = new Incident();
            return PartialView("Incident/Edit", incident);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IncidentUpdate([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");
            var viewData = new JsonViewData();
            if (incident.IsValid)
            {
                incident.AgencyId = Current.AgencyId;
                incident.UserId = incident.UserId.IsEmpty() ? Current.UserId : incident.UserId;
                if (incident.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (incident.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, incident.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this incident report.";
                        return Json(viewData);
                    }
                    else
                    {
                        incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) incident.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    }
                }
                else incident.SignatureText = string.Empty;
                var evnt = patientRepository.GetScheduleEventNew(Current.AgencyId, incident.PatientId, incident.EpisodeId, incident.Id);
                if (evnt != null)
                {
                    var oldVisitDate = evnt.VisitDate;
                    var oldStatus = evnt.Status;
                    var oldReason = evnt.ReturnReason;

                    evnt.VisitDate = incident.IncidentDate;
                    evnt.Status = incident.Status;
                    evnt.ReturnReason = string.Empty;
                    if (patientRepository.UpdateScheduleEventNew(evnt))
                    {
                        if (agencyRepository.UpdateIncident(incident))
                        {
                            if (Enum.IsDefined(typeof(ScheduleStatus), evnt.Status))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status, DisciplineTasks.IncidentAccidentReport, string.Empty);
                            }
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Incident was updated successfully";
                        }
                        else
                        {
                            evnt.VisitDate = oldVisitDate;
                            evnt.Status = oldStatus;
                            evnt.ReturnReason = oldReason;
                            agencyRepository.UpdateIncident(incident);
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Incident could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in updating the data. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in updating the data. Try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IncidentPrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Incident/PrintPreview", agencyService.GetIncidentReportPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult IncidentPrint(Guid? episodeId, Guid? patientId, Guid? eventId)
        {
            if (episodeId.HasValue && patientId.HasValue && eventId.HasValue)
            {
                return FileGenerator.Pdf<IncidentAccidentReportPdf>(new IncidentAccidentReportPdf(agencyService.GetIncidentReportPrint((Guid)episodeId, (Guid)patientId, (Guid)eventId)), "Incident");
            }
            else
            {
                return FileGenerator.Pdf<IncidentAccidentReportPdf>(new IncidentAccidentReportPdf(agencyService.GetIncidentReportPrint()), "Incident");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IncidentProcess(Guid id, Guid patientId, string action, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Incident/Accident Report could not be saved." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (action == ButtonAction.Approve.ToString())
                {
                    if (agencyService.ProcessIncidents(action, patientId, id, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Incident/Accident Report has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Incident/Accident Report could not be approved.";
                    }
                }
                else if (action == ButtonAction.Return.ToString())
                {
                    if (agencyService.ProcessIncidents(action, patientId, id, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Incident/Accident Report has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Incident/Accident Report could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Template
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateNew()
        {
            return PartialView("Template/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TemplateCreate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");
            var viewData = new JsonViewData();
            if (template.IsValid)
            {
                template.AgencyId = Current.AgencyId;
                template.Id = Guid.NewGuid();
                if (!agencyRepository.AddTemplate(template))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the template.";
                }
                else
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Template was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateList()
        {
            return PartialView("Template/List");
        }

        [GridAction]
        public ActionResult TemplateGrid()
        {
            return View(new GridModel(agencyRepository.GetTemplates(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TemplateGet(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var template = agencyRepository.GetTemplate(Current.AgencyId, id);
            if (template != null) return Json(template);
            return Json(new AgencyTemplate());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateEdit(Guid id)
        {
            return PartialView("Template/Edit", agencyRepository.GetTemplate(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateUpdate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");

            var viewData = new JsonViewData();

            if (template.IsValid)
            {
                var existingTemplate = agencyRepository.GetTemplate(Current.AgencyId, template.Id);
                if (existingTemplate != null)
                {
                    existingTemplate.Text = template.Text;
                    existingTemplate.Title = template.Title;
                    if (!agencyRepository.UpdateTemplate(existingTemplate))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the template.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Template was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected template don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TemplateDelete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Template could not be deleted. Please try again." };
            if (agencyRepository.DeleteTemplate(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Template has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Template could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateLogs(Guid id)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyTemplate, Current.AgencyId, id.ToString()));
        }
        #endregion

        #region Supply
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyNew()
        {
            return PartialView("Supply/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SupplyCreate([Bind] AgencySupply supply)
        {
            Check.Argument.IsNotNull(supply, "supply");
            var viewData = new JsonViewData();
            if (supply.IsValid)
            {
                supply.Id = Guid.NewGuid();
                supply.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddSupply(supply))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the supply.";
                }
                else
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, supply.Id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Supply was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = supply.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SupplySearch(string term, int limit)
        {
            var supplies = agencyRepository.GetSupplies(Current.AgencyId, term, limit).Select(p => new { p.Description, p.Code, p.Id, p.RevenueCode, p.UnitCost }).ToList();
            return Json(supplies);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyList()
        {
            return PartialView("Supply/List");
        }

        [GridAction]
        public ActionResult SupplyGrid()
        {
            return View(new GridModel(agencyRepository.GetSupplies(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SupplyGet(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var supply = agencyRepository.GetSupply(Current.AgencyId, id);
            if (supply != null) return Json(supply);
            return Json(new AgencySupply());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyEdit(Guid id)
        {
            return PartialView("Supply/Edit", agencyRepository.GetSupply(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyUpdate([Bind] AgencySupply supply)
        {
            Check.Argument.IsNotNull(supply, "supply");
            var viewData = new JsonViewData();
            if (supply.IsValid)
            {
                var existingSupply = agencyRepository.GetSupply(Current.AgencyId, supply.Id);
                if (existingSupply != null)
                {
                    existingSupply.Code = supply.Code;
                    existingSupply.Description = supply.Description;
                    existingSupply.RevenueCode = supply.RevenueCode;
                    existingSupply.UnitCost = supply.UnitCost;
                    if (!agencyRepository.UpdateSupply(existingSupply))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the supply.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, supply.Id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Supply was updated successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected supply does not exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = supply.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SupplyDelete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply could not be deleted. Please try again." };
            if (agencyRepository.DeleteSupply(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencySupply, LogAction.AgencySupplyDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Supply has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Supply could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyLogs(Guid id)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencySupply, Current.AgencyId, id.ToString()));
        }
        #endregion

        #region QA Center
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult QA()
        {
            ViewData["GroupName"] = "EventDate";
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("QA/Main", agencyService.GetQASchedule(location != null ? location.Id : Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult QAGrid(Guid branch, string group, int status, string sort)
        {
            ViewData["GroupName"] = group;
            if (sort.IsNotNullOrEmpty())
            {
                var parameters = sort.Split('-');
                if (parameters.Length >= 2)
                {
                    ViewData["SortColumn"] = parameters[0];
                    ViewData["SortDirection"] = parameters[1].ToUpperCase();
                }
            }
            return PartialView("QA/List", agencyService.GetQASchedule(branch, status));
        }
        #endregion

        #region Orders
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersToBeSentList()
        {
            return PartialView("Order/ToBeSentList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersToBeSentGrid(Guid branchId, bool sendAutomatically, string startDate, string endDate)
        {
            return View(new GridModel(agencyService.GetOrdersToBeSent(branchId, sendAutomatically, startDate.ToDateTime(), endDate.ToDateTime())));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistoryList()
        {
            return PartialView("Order/HistoryList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistoryGrid(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var status = new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
            return View(new GridModel(agencyService.GetProcessedOrders(branchId, startDate, endDate, status)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult OrdersSend(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Orders could not be marked as sent to Physician" };
            if (formCollection.Count > 0)
            {
                if (agencyService.MarkOrdersAsSent(formCollection))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Orders have been marked as sent to Physician";
                }
            }
            else viewData.errorMessage = "No Orders were selected";
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersPendingSignatureReceive(Guid id, Guid patientId, string type)
        {
            return PartialView("Order/Receive", agencyService.GetOrder(id, patientId, type));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult OrdersReceive(Guid Id, Guid PatientId, OrderType Type, DateTime ReceivedDate, DateTime PhysicianSignatureDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Orders could not be received" };
            if (agencyService.MarkOrderAsReturned(Id, PatientId, Type, ReceivedDate, PhysicianSignatureDate)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Orders has been received";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistoryEdit(Guid id, Guid patientId, string type)
        {
            return PartialView("Order/HistoryEdit", agencyService.GetOrder(id, patientId, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult OrdersHistoryUpdate(Guid Id, Guid PatientId, OrderType Type, DateTime ReceivedDate, DateTime SendDate, DateTime PhysicianSignatureDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to update order. Please try again." };
            if (agencyService.UpdateOrderDates(Id, PatientId, Type, ReceivedDate, SendDate, PhysicianSignatureDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Order successfully updated";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersPendingSignatureList()
        {
            return PartialView("Order/PendingSignatureList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersPendingSignatureGrid(Guid branchId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(agencyService.GetOrdersPendingSignature(branchId, startDate, endDate)));
        }
        #endregion

        #region Recerts
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RecertsPastDueList()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Recerts/PastDue", agency != null ? agency.Payor : "0");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RecertsUpcomingList()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Recerts/Upcoming", agency != null ? agency.Payor : "0");
        }

        [GridAction]
        public ActionResult RecertsPastDueGrid(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(agencyService.GetRecertsPastDue(branchId, insuranceId, startDate, endDate, false, 0)));
        }

        public JsonResult RecertsPastDueWidget()
        {
            return Json(agencyService.GetRecertsPastDueWidget());
        }

        [GridAction]
        public ActionResult RecertsUpcomingGrid(Guid branchId, int insuranceId)
        {
            return View(new GridModel(agencyService.GetRecertsUpcoming(branchId, insuranceId, DateTime.Now, DateTime.Now.AddDays(24), false, 0)));
        }

        public JsonResult RecertsUpcomingWidget()
        {
            return Json(agencyService.GetRecertsUpcomingWidget());
        }
        #endregion
    }
}
