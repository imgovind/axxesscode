﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;

    public class UserVisit
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public Guid UserId { get; set; }
        public string EpisodeNotes { get; set; }
        public string StatusComment { get; set; }
        public string VisitNotes { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public string TaskName { get; set; }
        public string VisitDate { get; set; }
        public string VisitRate { get; set; }
        public string Surcharge { get; set; }
        public string StatusName { get; set; }
        public string PatientName { get; set; }
        public string UserDisplayName { get; set; }
    }
}
