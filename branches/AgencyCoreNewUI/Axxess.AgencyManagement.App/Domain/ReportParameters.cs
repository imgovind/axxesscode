﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;

    public class ReportParameters
    {
        public int StatusId { get; set; }
        public Guid AddressBranchCode { get; set; }
        public Guid AgencyPhysicianId { get; set; }
        public string AddressStateCode { get; set; }
    }
}
