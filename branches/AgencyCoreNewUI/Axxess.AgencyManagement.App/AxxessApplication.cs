﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Security;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using System.Diagnostics;
    using System.Web.SessionState;

    public class AxxessApplication : HttpApplication
    {
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        public override void Init()
        {
            base.Init();
            //this.AuthenticateRequest += new EventHandler(AxxessApplication_AuthenticateRequest);
            this.AcquireRequestState += new EventHandler(AxxessApplication_AcquireRequestState);
        }

        protected void AxxessApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Context.User != null)
            {
                IMembershipService membershipService = Container.Resolve<IMembershipService>();
                IFormsAuthenticationService formAuthService = Container.Resolve<IFormsAuthenticationService>();

                var userName = Context.User.Identity.Name;
                AxxessPrincipal principal = membershipService.Get(userName);

                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;
                }

                if (!Current.CanContinue)
                {
                    membershipService.LogOff(userName);
                    formAuthService.SignOut();
                    formAuthService.RedirectToLogin();
                }
            }
        }

        protected void AxxessApplication_AcquireRequestState(object sender, EventArgs e)
        {
            if (Context.Handler is MvcHandler 
               && Context.User != null 
               && Context.User.Identity.IsAuthenticated)
            {
                IMembershipService membershipService = Container.Resolve<IMembershipService>();
                IFormsAuthenticationService formAuthService = Container.Resolve<IFormsAuthenticationService>();

                var userName = Context.User.Identity.Name;
                AxxessPrincipal principal = membershipService.Get(userName);

                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;

                    if (principal.IsImpersonated())
                    {
                        return;
                    }
                    else
                    {
                        if (AppSettings.IsSingleUserMode)
                        {
                            if (authenticationAgent.Verify(principal.ToSingleUser()))
                            {
                                if (Current.CanContinue)
                                {
                                    return;
                                }
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                membershipService.LogOff(userName);
                formAuthService.SignOut();
                formAuthService.RedirectToLogin();
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new MobileCapableWebFormViewEngine());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                Logger.Exception(exception);
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (context.Request.IsAuthenticated && custom == "Agency")
            {
                return Current.AgencyId.ToString();
            }
            else
            {
                return base.GetVaryByCustomString(context, custom);
            }
        }
    }
}
