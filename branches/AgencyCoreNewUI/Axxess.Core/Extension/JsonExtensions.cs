﻿namespace Axxess.Core.Extension
{
    using System;
    using System.IO;
    using System.Text;
    using System.Diagnostics;
    using System.Runtime.Serialization.Json;

    using Axxess.Core.Infrastructure;

    public static class JsonExtensions
    {
        [DebuggerStepThrough]
        public static T FromJson<T>(this string json)
        {
            T obj = Activator.CreateInstance<T>();
            if (json.IsNotNullOrEmpty())
            {
                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                    obj = (T)serializer.ReadObject(ms);
                }
            }
            return obj;
        }

        [DebuggerStepThrough]
        public static string ToJson<T>(this T instance)
        {
            Check.Argument.IsNotNull(instance, "instance");

            return JsonSerializer.Serialize(instance);
        }
    }
}
