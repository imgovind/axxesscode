﻿namespace Axxess.Core.Extension
{
    using System;
    using System.IO;
    using System.Diagnostics;

    using Axxess.Core.Infrastructure;

    public static class StreamExtensions
    {
        [DebuggerStepThrough]
        public static void CopyTo(this Stream input, Stream output)
        {
            if (input != null && input.Length > 0 && output != null)
            {
                var buffer = new byte[32768];
                while (true)
                {
                    int read = input.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                        return;
                    output.Write(buffer, 0, read);
                }
            }
        }
    }
}
