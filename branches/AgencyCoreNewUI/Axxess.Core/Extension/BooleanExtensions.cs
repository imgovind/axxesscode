﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Extension
{
    public static class BooleanExtensions
    {
        public static int ToInteger(this bool value)
        {
            return value ? 1 : 0;
        }

        public static string ToChecked(this bool @checked)
        {
            return @checked ? "checked='checked'" : string.Empty;
        }

        public static string ToSelected(this bool selected)
        {
            return selected ? " selected='selected'" : string.Empty;
        }

    }
}
