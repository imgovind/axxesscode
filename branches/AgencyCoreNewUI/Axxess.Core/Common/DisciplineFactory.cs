﻿namespace Axxess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Enums;

    public static class DisciplineFactory
    {

        public static List<Disciplines> MainDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.Nursing);
            list.AddRange(TherapyDisciplines());
            list.Add(Disciplines.HHA);
            list.Add(Disciplines.MSW);
            return list;
        }

        public static List<Disciplines> SkilledCareDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.Nursing);
            list.AddRange(TherapyDisciplines());
            return list;
        }

        public static List<Disciplines> NonBillableDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.ReportsAndNotes);
            list.Add(Disciplines.Orders);
            list.Add(Disciplines.Claim);
            return list;
        }

        public static List<Disciplines> TherapyDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.PT);
            list.Add(Disciplines.OT);
            list.Add(Disciplines.ST);
            return list;
        }

    }
}
