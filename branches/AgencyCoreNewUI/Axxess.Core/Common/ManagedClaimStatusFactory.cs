﻿namespace Axxess.Core
{
    using System.Collections.Generic;
    using Axxess.Core.Enums;

    public static class ManagedClaimStatusFactory
    {
        public static List<int> UnProcessed()
        {
            var list = new List<int>();
            list.Add((int)ManagedClaimStatus.ClaimCreated);
            list.Add((int)ManagedClaimStatus.ClaimReOpen);
            return list;
        }

    }
}
