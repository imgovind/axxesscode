﻿namespace Axxess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public static class ScheduleStatusFatory
    {

        public static List<int> NoteCompleted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteCompleted);
            list.Add((int)ScheduleStatus.NoteSubmittedWithSignature);
            return list;
        }

        public static List<int> OnlyEvalNoteCompleted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.EvalSentToPhysician);
            list.Add((int)ScheduleStatus.EvalToBeSentToPhysician);
            list.Add((int)ScheduleStatus.EvalReturnedWPhysicianSignature);
            list.Add((int)ScheduleStatus.EvalSentToPhysicianElectronically);
            return list;
        }

        public static List<int> EvalOrderNoteAfterQA()
        {
            var list = new List<int>();
            list.AddRange(OnlyEvalNoteCompleted());
            list.Add((int)ScheduleStatus.OrderSavedByPhysician);
            return list;
        }

        public static List<int> EvalPendingPhysicianSignature()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.EvalSentToPhysician);
            list.Add((int)ScheduleStatus.EvalSentToPhysicianElectronically);
            return list;
        }

        public static List<int> PhysicianOrdersPendingSignature()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderSentToPhysicianElectronically);
            list.Add((int)ScheduleStatus.OrderSentToPhysician);
            return list;
        }

        public static List<int> OrdersPendingPhysicianSignature()
        {
            var list = new List<int>();
            list.AddRange(EvalPendingPhysicianSignature());
            list.AddRange(PhysicianOrdersPendingSignature());
            return list;
        }

        public static List<int> EvalNoteCompleted()
        {
            var list = new List<int>();
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(NoteCompleted());
            return list;
        }

        public static List<int> AllNoteNotYetStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.CommonNotStarted);
            list.Add((int)ScheduleStatus.OrderNotYetStarted);
            list.Add((int)ScheduleStatus.OrderNotYetDue);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.NoteNotYetDue);
            list.Add((int)ScheduleStatus.NoteNotStarted);
            list.Add((int)ScheduleStatus.ReportAndNotesCreated);
            return list;
        }

        public static List<int> NursingNoteNotYetStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteNotYetDue);
            list.Add((int)ScheduleStatus.NoteNotStarted);
            return list;
        }

        public static List<int> OrdersAfterQA()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderSentToPhysician);
            list.Add((int)ScheduleStatus.OrderToBeSentToPhysician);
            list.Add((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            list.Add((int)ScheduleStatus.OrderSentToPhysicianElectronically);
            list.Add((int)ScheduleStatus.OrderSavedByPhysician);
            return list;
        }

        public static List<int> OrdersCompleted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            list.AddRange(OrdersAfterQA());
            return list;
        }

        public static List<int> NoteMissedVisit()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteMissedVisit);
            return list;
        }

        public static List<int> OASISAfterQA()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisCompletedExportReady);
            list.Add((int)ScheduleStatus.OasisCompletedNotExported);
            list.Add((int)ScheduleStatus.OasisExported);
            return list;
        }

        public static List<int> OASISCompleted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisCompletedPendingReview);
            list.AddRange(OASISAfterQA());
            return list;
        }

        public static List<int> OpenOASISStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            list.Add((int)ScheduleStatus.OasisReopened);
            list.Add((int)ScheduleStatus.OasisSaved);
            return list;
        }

        public static List<int> ReportAndNotesCompleted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.ReportAndNotesCompleted);
            list.Add((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
            return list;
        }

        public static List<int> OnAndAfterQAStatus()
        {
            var list = new List<int>();
            list.AddRange(OrdersCompleted());
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(NoteCompleted());
            list.AddRange(NoteMissedVisit());
            list.AddRange(OASISCompleted());
            list.AddRange(ReportAndNotesCompleted());
            return list;
        }

        public static List<int> CaseManagerStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderSubmittedPendingReview);
            list.Add((int)ScheduleStatus.OasisCompletedPendingReview);
            list.Add((int)ScheduleStatus.NoteSubmittedWithSignature);
            list.Add((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
            return list;
        }

        public static List<int> AfterQAStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteCompleted);
            list.AddRange(OrdersAfterQA());
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(OASISAfterQA());
            list.Add((int)ScheduleStatus.ReportAndNotesCompleted);
            return list;
        }

        public static List<int> NoteNotStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteNotStarted);
            list.Add((int)ScheduleStatus.NoteNotYetDue);
            return list;
        }

        public static List<int> OASISNotStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            return list;
        }

        public static List<int> OrdersNotStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderNotYetDue);
            list.Add((int)ScheduleStatus.OrderNotYetStarted);
            return list;
        }

        public static List<int> AllNotStarted()
        {
            var list = new List<int>();
            list.AddRange(NoteNotStarted());
            list.AddRange(OASISNotStarted());
            list.AddRange(OrdersNotStarted());
            list.Add((int)ScheduleStatus.ReportAndNotesCreated);
            return list;
        }

        public static List<int> OASISAndNurseNotesAfterQA()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteCompleted);
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(OASISAfterQA());
            return list;
        }

        public static List<int> BillStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteReturned);
            list.Add((int)ScheduleStatus.NoteReopened);
            list.Add((int)ScheduleStatus.NoteSubmittedWithSignature);

            list.Add((int)ScheduleStatus.OasisReopened);
            list.Add((int)ScheduleStatus.OasisCompletedPendingReview);
            list.Add((int)ScheduleStatus.OasisReturnedForClinicianReview);
            list.AddRange(OASISAndNurseNotesAfterQA());
            return list;
        }
    }
}
