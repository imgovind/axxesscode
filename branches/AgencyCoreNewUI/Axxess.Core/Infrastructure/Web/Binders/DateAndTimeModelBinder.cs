﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using System.Web.Mvc;

namespace Axxess.Core.Infrastructure
{
    public class DateAndTimeModelBinder : IModelBinder
    {
        public string Date { get; set; }
        public string Time { get; set; }

        #region IModelBinder Members

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException("bindingContext");
            }
            bindingContext.FallbackToEmptyPrefix = true;
            DateTime? dateTimeAttempt = TryGet<DateTime>(bindingContext, bindingContext.ModelName);
            if (dateTimeAttempt != null)
            {
                return dateTimeAttempt.Value;
            }
            //Get the separate date and time.
            DateTime? dateAttempt = TryGet<DateTime>(bindingContext, this.Date);
            DateTime? timeAttempt = TryGet<DateTime>(bindingContext, this.Time);
            if (dateAttempt != null && timeAttempt != null)
            {
                return new DateTime(dateAttempt.Value.Year,
                    dateAttempt.Value.Month,
                    dateAttempt.Value.Day,
                    timeAttempt.Value.Hour,
                    timeAttempt.Value.Minute,
                    timeAttempt.Value.Second);
            }
            return dateAttempt ?? timeAttempt;
        }

        private Nullable<T> TryGet<T>(ModelBindingContext bindingContext, string key) where T : struct
        {
            if (!key.IsNotNullOrEmpty())
            {
                return null;
            }
            ValueProviderResult result = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + "." + key);
            if (result == null && bindingContext.FallbackToEmptyPrefix == true)
            {
                result = bindingContext.ValueProvider.GetValue(key);
            }

            //This checks the value provider for the value based on how it would be bound coming from JSON
            if (result == null)
            {
                result = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + "[" + key + "]");
                if (result == null && bindingContext.FallbackToEmptyPrefix == true)
                {
                    result = bindingContext.ValueProvider.GetValue("[" + key + "]");
                }
            }

            if (result == null)
            {
                return null;
            }
            else
            {
                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, result);
            }
            try
            {
                return (Nullable<T>)result.ConvertTo(typeof(T));
            }
            catch(Exception e)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, e);
                return null;
            }
        }

        

        #endregion
    }
}
