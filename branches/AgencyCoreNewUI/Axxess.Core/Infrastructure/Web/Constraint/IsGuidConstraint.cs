﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;
    using System.Web.Routing;

    using Extension;

    public class IsGuid : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            Guid result;

            return values[parameterName].ToString().GuidTryParse(out result);
        }
    }
}
