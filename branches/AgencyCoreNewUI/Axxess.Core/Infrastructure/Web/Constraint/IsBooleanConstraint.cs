﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;
    using System.Web.Routing;

    using Extension;

    public class IsBoolean : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            bool result;

            return values[parameterName].ToString().BooleanTryParse(out result);
        }
    }
}
