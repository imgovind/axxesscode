﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.IO;
namespace Axxess.Core.Infrastructure
{
    public class GhostScript : IDisposable
    {
        #region Constants
        const String GHOST = "gsdll64.dll";
        #endregion
        #region Event Handling
        public class ProcessingStartedEventArgs : EventArgs
        {
            private int _PageCount;
            private ProcessingStartedEventArgs() { }
            public ProcessingStartedEventArgs(int pageCount)
            {
                _PageCount = pageCount;
            }
            public int PageCount
            {
                get
                {
                    return _PageCount;
                }
            }
        }
        public class ProcessingPageEventArgs : EventArgs
        {
            private int _Page;
            private int _PageCount;
            private ProcessingPageEventArgs() { }
            public ProcessingPageEventArgs(int page, int pageCount)
            {
                _Page = page;
                _PageCount = pageCount;
            }
            public int Page
            {
                get
                {
                    return _Page;
                }
            }
            public int PageCount
            {
                get
                {
                    return _PageCount;
                }
            }
        }
        public class ProcessingCompletedEventArgs : EventArgs
        {
            private int _PageCount;
            private List<string> _OuputFilenames;
            private ProcessingCompletedEventArgs() { }
            public ProcessingCompletedEventArgs(int pageCount, List<string> ouputFilenames)
            {
                _PageCount = pageCount;
                _OuputFilenames = ouputFilenames;
            }
            public int PageCount
            {
                get
                {
                    return _PageCount;
                }
            }
            public List<string> OuputFilenames
            {
                get
                {
                    return _OuputFilenames;
                }
            }
        }
        public delegate int StdioMessageEventHandler(IntPtr handle, IntPtr pointer, int count);
        public delegate void StdioCallbackMessageHandler(string message);
        public delegate void InMessageEventHandler(string message);
        public delegate void OutMessageEventHandler(string message);
        public delegate void ErrorMessageEventHandler(string message);
        public delegate void ProcessingStartedEventHandler(ProcessingStartedEventArgs e);
        public delegate void ProcessingPageEventHandler(ProcessingPageEventArgs e);
        public delegate void ProcessingCompletedEventHandler(ProcessingCompletedEventArgs e);
        public event StdioCallbackMessageHandler OnStdInCallbackMessage;
        public event StdioCallbackMessageHandler OnStdOutCallbackMessage;
        public event StdioCallbackMessageHandler OnStdErrCallbackMessage;
        public event ProcessingStartedEventHandler OnProcessingStarted;
        public event ProcessingPageEventHandler OnProcessingPage;
        public event ProcessingCompletedEventHandler OnProcessingCompleted;
        private int RaiseStdInCallbackMessageEvent(IntPtr handle, IntPtr pointer, int count)
        {
            if (OnStdInCallbackMessage != null)
            {
                string message = Marshal.PtrToStringAnsi(pointer);
                OnStdInCallbackMessage(message);
            }
            return count;
        }
        private int RaiseStdOutCallbackMessageEvent(IntPtr handle, IntPtr pointer, int count)
        {
            if (OnStdOutCallbackMessage != null)
            {
                OnStdInCallbackMessage(Marshal.PtrToStringAnsi(pointer));
            }
            if (OnProcessingStarted != null || OnProcessingPage != null)
            {
                string message = Marshal.PtrToStringAnsi(pointer).Trim();
                if (_PageCount <= 0)
                {
                    if (message.StartsWith("Processing"))
                    {
                        try
                        {
                            _PageCount = Int32.Parse(message.Substring(0, message.IndexOf("\n")).Replace("Processing pages 1 through ", string.Empty).Replace(".", string.Empty).Trim());
                        }
                        catch
                        {
                        }
                        RaiseProcessingStartedEvent(new ProcessingStartedEventArgs(_PageCount));
                    }
                }
                if (message.StartsWith("Page"))
                {
                    int page = 0;
                    try
                    {
                        page = Int32.Parse(message.Substring(0, message.IndexOf("\n")).Replace("Page ", string.Empty).Trim());
                    }
                    catch
                    {
                    }
                    RaiseProcessingPageEvent(new ProcessingPageEventArgs(page, _PageCount));
                }
            }
            return count;
        }
        private int RaiseStdErrCallbackMessageEvent(IntPtr handle, IntPtr pointer, int count)
        {
            string message = Marshal.PtrToStringAnsi(pointer);
            if (message.StartsWith("%%SeparationName:"))
            {
                string separationName = message.Replace("%%SeparationName:", string.Empty).Trim();
                if (!_SpotColorSeparationNames.Contains(separationName)) _SpotColorSeparationNames.Add(separationName);
            }
            if (OnStdErrCallbackMessage != null) OnStdErrCallbackMessage(message);
            return count;
        }
        private void RaiseProcessingStartedEvent(ProcessingStartedEventArgs processingStartedEventArgs)
        {
            if (OnProcessingStarted != null)
            {
                OnProcessingStarted(processingStartedEventArgs);
            }
        }
        private void RaiseProcessingPageEvent(ProcessingPageEventArgs processingPageEventArgs)
        {
            if (OnProcessingPage != null) OnProcessingPage(processingPageEventArgs);
        }
        private void RaiseProcessingCompletedEvent(ProcessingCompletedEventArgs processingCompletedEventArgs)
        {
            if (OnProcessingCompleted != null) OnProcessingCompleted(processingCompletedEventArgs);
        }
        #endregion
        #region Enumerations
        public enum SupportedFormats
        {
            ps, eps, epsf, pdf
        }
        public enum OutputDevice
        {
            png16m, pnggray, png256, png16, pngmono, pngalpha,
            jpeg, jpeggray,
            pbm, pbmraw, pgm, pgmraw, pgnm, pgnmraw, pnm, pnmraw, ppm, ppmraw, pkm, pkmraw, pksm, pksmraw,
            tiffgray, tiff12nc, tiff24nc, tiff32nc, tiffsep, tiffcrle, tiffg3, tiffg32d, tiffg4, tifflzw, tiffpack,
            faxg3, faxg32d, faxg4,
            bmpmono, bmpgray, bmpsep1, bmpsep8, bmp16, bmp256, bmp16m, bmp32b,
            pcxmono, pcxgray, pcx16, pcx256, pcx24b, pcxcmyk,
            psdcmyk, psdrgb,
            pdfwrite, pswrite, epswrite, pxlmono, pxlcolor
        }
        public enum GraphicsAlphaBits
        {
            NotSet = 0,
            Low = 1,
            Medium = 2,
            Optimum = 4
        }
        public enum ReturnCode : int
        {
            // Postscript level 1 errors
            e_unknownerror = -1,
            e_dictfull = -2,
            e_dictstackoverflow = -3,
            e_dictstackunderflow = -4,
            e_execstackoverflow = -5,
            e_interrupt = -6,
            e_invalidaccess = -7,
            e_invalidexit = -8,
            e_invalidfileaccess = -9,
            e_invalidfont = -10,
            e_invalidrestore = -11,
            e_ioerror = -12,
            e_limitcheck = -13,
            e_nocurrentpoint = -14,
            e_rangecheck = -15,
            e_stackoverflow = -16,
            e_setackunderflow = -17,
            e_syntaxerror = -18,
            e_timeout = -19,
            e_typecheck = -20,
            e_undefined = -21,
            e_undefinedfilename = -22,
            e_undefinedresult = -23,
            e_unmatchedmark = -24,
            e_VMerror = -25,
            // Additional level 2 and DPS errors
            e_configurationerror = -26,
            e_invalidcontext = -27,
            e_undefinedresource = -28,
            e_unregistered = -29,
            // Pseudo-errors used by ghostscript internally
            e_invalidid = -30,                                  // invalidid is for the NeXT DPS extension.
            e_fatal = -100,                                     // Internal code for a fatal error. gs_interpret also returns this for a .quit with a positive exit code.
            e_Quit = -101,                                      // Internal code for the .quit operator. The real quit code is an integer on the operand stack. gs_interpret returns this only for a .quit with a zero exit code.
            e_InterpreterExit = -102,                           // Internal code for a normal exit from the interpreter. Do not use outside of interp.c.
            e_RemapColor = -103,                                // Internal code that indicates that a procedure has been stored in the remap_proc of the graphics state, and should be called before retrying the current token.  This is used for color remapping involving a call back into the interpreter -- inelegant, but effective.
            e_ExecStackUnderflow = -104,                        // Internal code to indicate we have underflowed the top block of the e-stack.
            e_VMreclaim = -105,                                 // Internal code for the vmreclaim operator with a positive operand. We need to handle this as an error because otherwise the interpreter won't reload enough of its state when the operator returns.
            e_NeedInput = -106,                                 // Internal code for requesting more input from run_string.
            e_NeedStdin = -107,                                 // Internal code for stdin callout.
            e_NeedStdout = -108,                                // Internal code for stdout callout.
            e_NeedStderr = -109,                                // Internal code for stderr callout.
            e_Info = -110,                                      // Internal code for a normal exit when usage info is displayed. This allows Window versions of Ghostscript to pause until the message can be read.
            // Custom Errors
            FileTypeNotSupportedByInterpreter = -10000,         // Custom GhostScript Error: Input file type is not supported by the interpreter.
            UnableToLoadGhostScriptDll = -10001,                // Custom GhostScript Error: Unable to load GhostScript DLL (gsdll32.dll)
            GhostScriptDllNotFound = -10002                     // Custom GhostScript Error: GhostScript DLL not found in the specified Library Path
        }
        #endregion
        #region Unmanaged Dynamic-Link Library (DLL) Imports
        [DllImport(GHOST, EntryPoint = "gsapi_revision", CharSet = CharSet.Ansi)]
        private static extern int gsapi_revision([In, Out] gsapi_revision_t revision, int len);
        [DllImport(GHOST, EntryPoint = "gsapi_new_instance", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int gsapi_new_instance(ref IntPtr pInstance, out IntPtr pCaller);
        [DllImport(GHOST, EntryPoint = "gsapi_init_with_args", CharSet = CharSet.Ansi)]
        private static extern int gsapi_init_with_args(IntPtr pInstance, int argc, [In, Out] String[] argv);
        [DllImport(GHOST, EntryPoint = "gsapi_exit", CharSet = CharSet.Ansi)]
        private static extern int gsapi_exit(IntPtr pInstance);
        [DllImport(GHOST, EntryPoint = "gsapi_delete_instance", CharSet = CharSet.Ansi)]
        private static extern int gsapi_delete_instance(IntPtr pInstance);
        [DllImport(GHOST, EntryPoint = "gsapi_run_file", CharSet = CharSet.Ansi)]
        private static extern int gsapi_run_file(IntPtr pInstance, string strFilename, int nErrors, int nExitCode);
        [DllImport(GHOST, EntryPoint = "gsapi_set_stdio", CharSet = CharSet.Ansi)]
        private static extern int gsapi_set_stdio(IntPtr pInstance, StdioMessageEventHandler gsdll_stdin, StdioMessageEventHandler gsdll_stdout, StdioMessageEventHandler gsdll_stderr);
        #endregion
        #region Private Property Members
        private int _Handle = 0;
        private gsapi_revision_t _VersionInfo = null;
        private int _PageCount = 0;
        private List<string> _SpotColorSeparationNames = new List<string>();
        #endregion
        #region Initialization
        public GhostScript()
        {
            if (File.Exists(GHOST))
            {
                InitializePrivateMembers();
            }
        }
        private void InitializePrivateMembers()
        {
            _PageCount = 0;
            _SpotColorSeparationNames.Clear();
        }
        #endregion
        #region Public Property Members
        public gsapi_revision_t VersionInfo
        {
            get
            {
                return _VersionInfo;
            }
        }
        public List<string> SpotColorSeparationNames
        {
            get
            {
                return _SpotColorSeparationNames;
            }
        }
        #endregion
        #region Public Methods
        [FileIOPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]
        public List<String> Convert(OutputDevice OutputDevice, String PdfFile, int Resolution)
        {
            InitializePrivateMembers();
            String TempFolder = System.IO.Path.GetTempPath();
            List<String> OutputFilenames = new List<String>();
            String Filename = Path.GetFileNameWithoutExtension(PdfFile.Replace("%", "_"));
            String OutputFilenameTemplate = Path.Combine(TempFolder, String.Concat(Filename, "%01d.", GetOutputDeviceFileExtension(OutputDevice)));
            String TemporaryInputFilename = Path.Combine(TempFolder, String.Concat(Guid.NewGuid().ToString("N"), Path.GetExtension(PdfFile)));
            System.IO.File.Copy(PdfFile, TemporaryInputFilename, true);
            String[] CommandParameters = new String[] { "convert", "-dSAFER", "-dBATCH", "-dNOPAUSE", "-dCOLORSCREEN", "-dDOINTERPOLATE", "-dTextAlphaBits=4",
                "-dGraphicsAlphaBits=4", "-dUseCIEColor", GetOutputDeviceParameter(OutputDevice), GetResolutionParameter(Resolution > 0 ? Resolution : 100),
                "-sOutputFile=" + OutputFilenameTemplate, TemporaryInputFilename };
            try
            {
                CallGSDll(CommandParameters);
                String OutputFileName = Path.GetFileNameWithoutExtension(OutputFilenameTemplate.Replace("%01d", string.Empty));
                String OutputFile = Path.Combine(TempFolder, OutputFileName);
                String OutputFileExtension = Path.GetExtension(OutputFilenameTemplate);
                int i = 1;
                String OutputFileNameCheck = String.Concat(OutputFile, i, OutputFileExtension);
                while (System.IO.File.Exists(OutputFileNameCheck))
                {
                    OutputFilenames.Add(String.Concat(OutputFileName, i));
                    i++;
                    OutputFileNameCheck = String.Concat(OutputFile, i, OutputFileExtension);
                }
                RaiseProcessingCompletedEvent(new ProcessingCompletedEventArgs(_PageCount, OutputFilenames));
            }
            finally
            {
                if (System.IO.File.Exists(TemporaryInputFilename))
                {
                    try
                    {
                        System.IO.File.Delete(TemporaryInputFilename);
                    }
                    catch { }
                }
                if (System.IO.File.Exists(PdfFile))
                {
                    try
                    {
                        System.IO.File.Delete(PdfFile);
                    }
                    catch { }
                }
            }
            return OutputFilenames;
        }
        public void CallGSDll(string[] commandParameters)
        {
            int errorCode = (int)ReturnCode.e_unknownerror;
            IntPtr ghostScriptPtr = IntPtr.Zero;
            IntPtr callerPtr = IntPtr.Zero;
            errorCode = gsapi_new_instance(ref ghostScriptPtr, out callerPtr);
            errorCode = gsapi_set_stdio(ghostScriptPtr, new StdioMessageEventHandler(RaiseStdInCallbackMessageEvent), new StdioMessageEventHandler(RaiseStdOutCallbackMessageEvent), new StdioMessageEventHandler(RaiseStdErrCallbackMessageEvent));
            if (errorCode >= 0)
            {
                try
                {
                    errorCode = gsapi_init_with_args(ghostScriptPtr, commandParameters.Length, commandParameters);
                    gsapi_exit(ghostScriptPtr);
                }
                finally
                {
                    gsapi_delete_instance(ghostScriptPtr);
                }
            }
            errorCode = (errorCode == (int)ReturnCode.e_Quit ? 0 : errorCode);
            if (errorCode < 0)
            {
                StringBuilder commands = new StringBuilder();
                foreach (string command in commandParameters) commands.Append(String.Concat(command, " "));
                throw (new GhostScriptException(errorCode, String.Concat("Command Arguments=[" + commands.ToString().Trim() + "] ", GetGSErrorMessage(errorCode), " (", errorCode.ToString(), ")")));
            }
        }
        #endregion
        #region Helper Functions
        public static string GetOutputDeviceParameter(OutputDevice outputDevice)
        {
            if (Enum.IsDefined(typeof(OutputDevice), outputDevice)) return String.Concat("-sDEVICE=", outputDevice);
            else return "-sDEVICE=unknown";
        }
        public static string GetResolutionParameter(int resolution)
        {
            return String.Concat("-r", resolution.ToString());
        }
        public static string GetOutputDeviceFileExtension(OutputDevice outputDevice)
        {
            switch (outputDevice)
            {
                case OutputDevice.png16m:
                case OutputDevice.pnggray:
                case OutputDevice.png256:
                case OutputDevice.png16:
                case OutputDevice.pngmono:
                case OutputDevice.pngalpha:
                    return "png";
                case OutputDevice.jpeg:
                case OutputDevice.jpeggray:
                    return "jpg";
                case OutputDevice.pbm:
                case OutputDevice.pbmraw:
                case OutputDevice.pgm:
                case OutputDevice.pgmraw:
                case OutputDevice.pgnm:
                case OutputDevice.pgnmraw:
                case OutputDevice.pnm:
                case OutputDevice.pnmraw:
                case OutputDevice.ppm:
                case OutputDevice.ppmraw:
                case OutputDevice.pkm:
                case OutputDevice.pkmraw:
                case OutputDevice.pksm:
                case OutputDevice.pksmraw:
                    return "pnm";
                case OutputDevice.tiffgray:
                case OutputDevice.tiff12nc:
                case OutputDevice.tiff24nc:
                case OutputDevice.tiff32nc:
                case OutputDevice.tiffsep:
                case OutputDevice.tiffcrle:
                case OutputDevice.tiffg3:
                case OutputDevice.tiffg32d:
                case OutputDevice.tiffg4:
                case OutputDevice.tifflzw:
                case OutputDevice.tiffpack:
                    return "tif";
                case OutputDevice.faxg3:
                case OutputDevice.faxg32d:
                case OutputDevice.faxg4:
                    return "raw";
                case OutputDevice.bmpgray:
                case OutputDevice.bmpsep1:
                case OutputDevice.bmpsep8:
                case OutputDevice.bmp16:
                case OutputDevice.bmp256:
                case OutputDevice.bmp16m:
                case OutputDevice.bmp32b:
                    return "bmp";
                case OutputDevice.pcxmono:
                case OutputDevice.pcxgray:
                case OutputDevice.pcx16:
                case OutputDevice.pcx256:
                case OutputDevice.pcx24b:
                case OutputDevice.pcxcmyk:
                    return "pcx";
                case OutputDevice.psdcmyk:
                case OutputDevice.psdrgb:
                    return "psd";
                case OutputDevice.pdfwrite:
                    return "pdf";
                case OutputDevice.pswrite:
                    return "ps";
                case OutputDevice.epswrite:
                    return "eps";
                case OutputDevice.pxlmono:
                case OutputDevice.pxlcolor:
                    return "pxl";
                default:
                    return string.Empty;
            }
        }
        #endregion
        #region Error Handling
        public class GhostScriptException : System.Exception
        {
            private int _ErrorCode = -1;
            private DateTime _TimeStamp = DateTime.MinValue;
            public GhostScriptException(int errorCode, string message)
                : base(message)
            {
                _ErrorCode = errorCode;
                _TimeStamp = DateTime.Now;
            }
            public int ErrorCode
            {
                get { return _ErrorCode; }
            }
            public DateTime TimeStamp
            {
                get { return _TimeStamp; }
            }
        }
        private static string GetGSErrorMessage(int returnCode)
        {
            switch (returnCode)
            {
                case (int)ReturnCode.e_unknownerror: return "Unknown error";
                case (int)ReturnCode.e_dictfull: return "level 1 error: e_dictfull";
                case (int)ReturnCode.e_dictstackoverflow: return "level 1 error: e_dictstackoverflow";
                case (int)ReturnCode.e_dictstackunderflow: return "level 1 error: e_dictstackunderflow";
                case (int)ReturnCode.e_execstackoverflow: return "level 1 error: e_execstackoverflow";
                case (int)ReturnCode.e_interrupt: return "level 1 error: e_interrupt";
                case (int)ReturnCode.e_invalidaccess: return "level 1 error: e_invalidaccess";
                case (int)ReturnCode.e_invalidexit: return "level 1 error: e_invalidexit";
                case (int)ReturnCode.e_invalidfileaccess: return "level 1 error: e_invalidfileaccess";
                case (int)ReturnCode.e_invalidfont: return "level 1 error: e_invalidfont";
                case (int)ReturnCode.e_invalidrestore: return "level 1 error: e_invalidrestore";
                case (int)ReturnCode.e_ioerror: return "level 1 error: e_ioerror";
                case (int)ReturnCode.e_limitcheck: return "level 1 error: e_limitcheck";
                case (int)ReturnCode.e_nocurrentpoint: return "level 1 error: e_nocurrentpoint";
                case (int)ReturnCode.e_rangecheck: return "level 1 error: e_rangecheck error";
                case (int)ReturnCode.e_stackoverflow: return "level 1 error: e_stackoverflow";
                case (int)ReturnCode.e_setackunderflow: return "level 1 error: e_setackunderflow";
                case (int)ReturnCode.e_syntaxerror: return "level 1 error: e_syntaxerror";
                case (int)ReturnCode.e_timeout: return "level 1 error: e_timeout";
                case (int)ReturnCode.e_typecheck: return "level 1 error: e_typecheck";
                case (int)ReturnCode.e_undefined: return "level 1 error: e_undefined";
                case (int)ReturnCode.e_undefinedfilename: return "level 1 error: e_undefinedfilename";
                case (int)ReturnCode.e_undefinedresult: return "level 1 error: e_undefinedresult";
                case (int)ReturnCode.e_unmatchedmark: return "level 1 error: e_unmatchedmark";
                case (int)ReturnCode.e_VMerror: return "level 1 error: e_VMerror error";
                case (int)ReturnCode.e_configurationerror: return "Level 2 error: e_configurationerror";
                case (int)ReturnCode.e_invalidcontext: return "Level 2 error: e_invalidcontext";
                case (int)ReturnCode.e_undefinedresource: return "Level 2: e_undefinedresource";
                case (int)ReturnCode.e_unregistered: return "Level 2: e_unregistered";
                case (int)ReturnCode.e_invalidid: return "Level 2: e_invalidid";
                case (int)ReturnCode.e_fatal: return "Internal GhostScript code: e_fatal";
                case (int)ReturnCode.e_Quit: return "Internal GhostScript code: e_Quit";
                case (int)ReturnCode.e_InterpreterExit: return "Internal GhostScript code: e_InterpreterExit";
                case (int)ReturnCode.e_RemapColor: return "Internal GhostScript code: e_RemapColor";
                case (int)ReturnCode.e_ExecStackUnderflow: return "Internal GhostScript code: e_ExecStackUnderflow";
                case (int)ReturnCode.e_VMreclaim: return "Internal GhostScript code: e_VMreclaim";
                case (int)ReturnCode.e_NeedInput: return "Internal GhostScript code: e_NeedInput";
                case (int)ReturnCode.e_NeedStdin: return "Internal GhostScript code: e_NeedStdin";
                case (int)ReturnCode.e_NeedStdout: return "Internal GhostScript code: e_NeedStdout";
                case (int)ReturnCode.e_NeedStderr: return "Internal GhostScript code: e_NeedStderr";
                case (int)ReturnCode.e_Info: return "Internal GhostScript code: e_Info";
                case (int)ReturnCode.FileTypeNotSupportedByInterpreter: return "GhostScript Wrapper Class error: File type not supported by the ghostscript interpreter.";
                case (int)ReturnCode.UnableToLoadGhostScriptDll: return "GhostScript Wrapper Class error: Unable to load GhostScript DLL (gsdll32.dll).";
                case (int)ReturnCode.GhostScriptDllNotFound: return "GhostScript Wrapper Class error: GhostScript DLL (gsdll32.dll) not found in specified library path.";
                default: return "Unknown error.";
            }
        }
        #endregion
        #region Revision Class
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class gsapi_revision_t
        {
            public string Product;
            public string Copyright;
            public int Revision;
            public int RevisionDate;
        }
        #endregion
        #region IDisposable Members
        public void Dispose()
        {
        }
        #endregion
        #region Finalization
        ~GhostScript()
        {
            Dispose();
        }
        #endregion
    }
}