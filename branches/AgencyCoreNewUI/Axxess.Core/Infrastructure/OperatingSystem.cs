﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using Extension;

    public static class OperatingSystem
    {
        public static bool Is64Bit
        {
            get
            {
                if (Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE").IsEqual("x86") && 
                    Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432").IsNullOrEmpty())
                {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }
}
