﻿namespace Axxess.Core.Infrastructure
{
    using System;

    using Axxess.Core.Extension;

    public static class CoreSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static bool UseSSL
        {
            get
            {
                return configuration.AppSettings["UseSSL"].ToBoolean();
            }
        }

        public static bool CompressHtmlResponse
        {
            get
            {
                return configuration.AppSettings["CompressHtmlResponse"].ToBoolean();
            }
        }

        public static double CachingIntervalInMinutes
        {
            get
            {
                return configuration.AppSettings["CachingIntervalInMinutes"].ToDouble();
            }
        }

        public static double MembaseSessionTimeoutInMinutes
        {
            get
            {
                return configuration.AppSettings["MembaseSessionTimeoutInMinutes"].ToDouble();
            }
        }

        public static string[] MemcacheServerUriArray
        {
            get
            {
                return configuration.AppSettings["MemcacheServerUris"].Split(';');
            }
        }

        public static string NoReplyEmail
        {
            get
            {
                return configuration.AppSettings["NoReplyEmail"];
            }
        }

        public static string NoReplyDisplayName
        {
            get
            {
                return configuration.AppSettings["NoReplyDisplayName"];
            }
        }

        public static string BugTrackingEmail
        {
            get
            {
                return configuration.AppSettings["BugTrackingEmail"];
            }
        }

        public static string RedmineHost
        {
            get
            {
                return configuration.AppSettings["RedmineHost"];
            }
        }

        public static string RedmineAPIKey
        {
            get
            {
                return configuration.AppSettings["RedmineAPIKey"];
            }
        }

        public static bool EnableSSLMail
        {
            get
            {
                return configuration.AppSettings["EnableSSLMail"].ToBoolean();
            }
        }

        public static string MailTemplateDirectory
        {
            get
            {
                return configuration.AppSettings["TemplateDirectory"];
            }
        }

        public static string MailAttachmentDirectory
        {
            get
            {
                return configuration.AppSettings["AttachmentDirectory"];
            }
        }

        public static string ExcelDirectory
        {
            get
            {
                return configuration.AppSettings["ExcelDirectory"];
            }
        }

        public static string CryptoPassPhrase
        {
            get
            {
                return configuration.AppSettings["CryptoPassPhrase"];
            }
        }

        public static string CryptoSaltValue
        {
            get
            {
                return configuration.AppSettings["CryptoSaltValue"];
            }
        }

        public static string CryptoHashAlgorithm
        {
            get
            {
                return configuration.AppSettings["CryptoHashAlgorithm"];
            }
        }

        public static string CryptoInitVector
        {
            get
            {
                return configuration.AppSettings["CryptoInitVector"];
            }
        }

        public static int CryptoPasswordIterations
        {
            get
            {
                return configuration.AppSettings["CryptoPasswordIterations"].ToInteger();
            }
        }


        

        public static int CryptoKeySize
        {
            get
            {
                return configuration.AppSettings["CryptoKeySize"].ToInteger();
            }
        }


        public static int CurrentClusterId
        {
            get
            {
                return configuration.AppSettings["CurrentClusterId"].ToInteger();
            }
        }

        

        public static string[] NoEncryptPaths
        {
            get
            {
                return configuration.AppSettings["NoEncryptPaths"].Split(new string[] {";"}, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static string[] NonSecurePaths
        {
            get
            {
                return configuration.AppSettings["NonSecurePaths"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static string[] NoEncryptFiles
        {
            get
            {
                return configuration.AppSettings["NoEncryptFiles"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            }
        }

        public static string[] ValidDomains
        {
            get
            {
                try
                {
                    return configuration.AppSettings["ValidDomains"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                }
                catch (Exception ex)
                {
                    return new string[] { };
                }
            }
        }

        public static bool MobileTestEnvironment
        {
            get
            {
                return configuration.AppSettings["MobileTestEnvironment"] == "true" ? true : false;
            }
        }

        public static bool MobileRedirect
        {
            get
            {
                return configuration.AppSettings["MobileRedirect"] == "true" ? true : false;
            }
        }

        public static int MaxFileSizeInBytes
        {
            get
            {
                return configuration.AppSettings["MaxFileSizeInBytes"].ToInteger();
            }
        }

        public static string ESignAPI
        {
            get
            {
                return configuration.AppSettings["ESignAPI"].ToString();
            }
        }

        public static string DevESignAPI
        {
            get
            {
                return configuration.AppSettings["DevESignAPI"].ToString();
            }
        }

        public static string AgencyUpgrade
        {
            get
            {
                return configuration.AppSettings["AgencyUpgrade"].ToString();
            }
        }
    
    }
}
