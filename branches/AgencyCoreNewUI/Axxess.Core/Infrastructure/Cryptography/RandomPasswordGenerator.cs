﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;

    using Extension;

    public class RandomPasswordGenerator
    {
        // Define default password length.
        private static int DEFAULT_PASSWORD_LENGTH = 8;

        //No characters that are confusing: i, I, l, L, o, O, 0, 1, u, v

        public static string PASSWORD_CHARS_ALPHA = "abcdefghjkmnpqrstwxyz";
        public static string PASSWORD_CHARS_ALPHA_UPPER = "ABCDEFGHJKMNPQRSTWXYZ";
        public static string PASSWORD_CHARS_NUMERIC = "23456789";
        public static string PASSWORD_CHARS_SPECIAL = "!@#$%&*?";
        public static string PASSWORD_CHARS_ALPHANUMERIC = PASSWORD_CHARS_ALPHA + PASSWORD_CHARS_NUMERIC;
        public static string PASSWORD_CHARS_ALL = PASSWORD_CHARS_ALPHANUMERIC + PASSWORD_CHARS_SPECIAL;

        /// <summary>
        /// Generates the password.
        /// </summary>
        /// <returns></returns>
        public static string Generate()
        {
            int passwordLength = DEFAULT_PASSWORD_LENGTH;

            var random = GetRandom();
            var password = new char[passwordLength];

            password[0] = PASSWORD_CHARS_SPECIAL[random.Next(PASSWORD_CHARS_SPECIAL.Length)];
            password[1] = PASSWORD_CHARS_NUMERIC[random.Next(PASSWORD_CHARS_NUMERIC.Length)];
            password[2] = PASSWORD_CHARS_ALPHA_UPPER[random.Next(PASSWORD_CHARS_ALPHA_UPPER.Length)];

            for (int i = 3; i < passwordLength; i++)
            {
                password[i] = PASSWORD_CHARS_ALPHA[random.Next(PASSWORD_CHARS_ALPHA.Length)];
            }

            return new string(password.OrderBy(s => (random.Next(2) % 2) == 0).ToArray());
        }

        /// <summary>
        /// Gets a random object with a real random seed
        /// </summary>
        /// <returns></returns>
        private static Random GetRandom()
        {
            // Use a 4-byte array to fill it with random bytes and convert it then
            // to an integer value.
            byte[] randomBytes = new byte[4];

            // Generate 4 random bytes.
            new RNGCryptoServiceProvider().GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = (randomBytes[0] & 0x7f) << 24 |
                        randomBytes[1] << 16 |
                        randomBytes[2] << 8 |
                        randomBytes[3];

            // Now, this is real randomization.
            return new Random(seed);
        }


    }
}