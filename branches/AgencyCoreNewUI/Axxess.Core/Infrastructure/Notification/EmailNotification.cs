﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Net.Mail;
    using System.Threading;

    using Extension;
    using Axxess.Core.Infrastructure.Notification;
    using System.ComponentModel;
    using System.Collections.Generic;

    public class EmailNotification : INotification
    {
        const int MAX_RETRY_COUNT = 5;
        private bool _isAlreadyRun;
        private MailMessage _mailMessage;
        private Queue<MailMessage> MessageQueue = new Queue<MailMessage>();
        private int CurrentRetryCount { get; set; }

        public event SendEmailCompletedEventHandler SendEmailCompleted;

        public delegate void SendEmailCompletedEventHandler(object sender, SendEmailCompletedEventArgs e);

        #region INotification Members

        public void Send(string fromAddress, string toAddress, string subject, string body)
        {
            using (MailMessage mail = CreateMessage(fromAddress, toAddress, subject, body))
            {
                SendMail(mail);
            }
        }

        public void Send(string fromAddress, string toAddress, string subject, string body, string[] attachments)
        {
            using (MailMessage mail = CreateMessage(fromAddress, toAddress, subject, body, attachments))
            {
                SendMail(mail);
            }
        }

        private bool SendMail(MailMessage mail)
        {
            try
            {
                if (_isAlreadyRun)
                {
                    MessageQueue.Enqueue(mail);
                }
                else
                {
                    SmtpClient smtp = new SmtpClient
                    {
                        EnableSsl = CoreSettings.EnableSSLMail
                    };
                    smtp.SendCompleted += SmtpClientSendCompleted;
                    _mailMessage = mail;
                    SendMessage(smtp);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void SendMessage(SmtpClient client)
        {
            try
            {
                var id = _mailMessage.Headers.Get("Message-ID");
                client.SendAsync(_mailMessage, id.ToGuid());
            }
            catch (Exception exception)
            {
                ProcessingComplete(client);
            }
        }

        private void SmtpClientSendCompleted(object sender, AsyncCompletedEventArgs args)
        {
            var client = (SmtpClient)sender;

            if (args.Error == null || CurrentRetryCount >= MAX_RETRY_COUNT)
            {
                ProcessingComplete(client);
            }
            else
            {
                CurrentRetryCount++;
                SendMessage(client);
            }
        }

        private void ProcessingComplete(SmtpClient client)
        {
            CurrentRetryCount = 0;
            OnSendCompleted(new SendEmailCompletedEventArgs(null, false, null, CurrentRetryCount));
            if (MessageQueue.Count > 0)
            {
                _mailMessage = MessageQueue.Dequeue();
                client.Send(_mailMessage);
            }
            else
            {
                if (client != null)
                {
                    client.SendCompleted -= SmtpClientSendCompleted;
                }
                _isAlreadyRun = false;
            }
        }

        protected virtual void OnSendCompleted(SendEmailCompletedEventArgs args)
        {
            var handler = SendEmailCompleted;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region Static Methods

        private static MailMessage CreateMessage(string fromAddress, string toAddress, string subject, string body)
        {
            return CreateMessage(fromAddress, toAddress, subject, body, null);
        }

        private static MailMessage CreateMessage(string fromAddress, string toAddress, string subject, string body, string[] attachments)
        {
            var message = new MailMessage
            {
                Sender = new MailAddress(CoreSettings.NoReplyEmail),
                From = new MailAddress(fromAddress, CoreSettings.NoReplyDisplayName),
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            };
            message.Headers.Add("Message-ID", Guid.NewGuid().ToString());
            if (attachments != null && attachments.Length > 0)
            {
                var attachmentDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CoreSettings.MailAttachmentDirectory);
                attachments.ForEach(attachmentName =>
                {
                    string filePath = Path.Combine(attachmentDirectory, attachmentName);
                    var fileStream = new FileStream(filePath, FileMode.Open);
                    Attachment attachment = new Attachment(fileStream, attachmentName);
                    message.Attachments.Add(attachment);
                });
            }

            string[] tos = toAddress.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string to in tos)
            {
                message.To.Add(new MailAddress(to));
            }

            return message;
        }

        #endregion
    }
}
