﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.Core.Infrastructure.Notification
{
    public class SendEmailCompletedEventArgs : AsyncCompletedEventArgs
    {
        public int RetryCount { get; set; }

        public SendEmailCompletedEventArgs(Exception error, bool cancelled, object userState, int retryCount) : base(error, cancelled, userState)
        {
            RetryCount = retryCount;
        }
    }
}
