﻿namespace Axxess.Core.Infrastructure
{
    public interface INotification
    {
        void Send(string fromAddress, string toAddress, string subject, string body);
        void Send(string fromAddress, string toAddress, string subject, string body, string[] attachments);
        //void SendAsync(string fromAddress, string toAddress, string subject, string body);
    }
}
