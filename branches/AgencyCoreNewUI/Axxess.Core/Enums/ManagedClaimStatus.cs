﻿namespace Axxess.Core.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Infrastructure;

    public enum ManagedClaimStatus
    {
        [GroupDescription("Created", "Claims")]
        ClaimCreated = 3000,
        [GroupDescription("ReOpenend", "Claims")]
        ClaimReOpen = 3001,
        [GroupDescription("Submitted", "Claims")]
        ClaimSubmitted = 3005,
        [GroupDescription("Rejected", "Claims")]
        ClaimRejected = 3010,
        [GroupDescription("Payment Pending", "Claims")]
        ClaimPaymentPending = 3015,
        [GroupDescription("Accepted/Processing", "Claims")]
        ClaimAccepted = 3020,
        [GroupDescription("Returned", "Claims")]
        ClaimWithErrors = 3025,
        [GroupDescription("Paid", "Claims")]
        ClaimPaidClaim = 3030,
        [GroupDescription("Cancelled", "Claims")]
        ClaimCancelledClaim = 3035,
    }
}
