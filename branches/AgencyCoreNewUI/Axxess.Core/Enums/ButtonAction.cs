﻿namespace Axxess.Core.Enums
{

    public enum ButtonAction
    {
        Complete = 1,
        Approve = 2,
        Submit = 3,
        Save = 4,
        Return = 5,
        Print = 6,
        Exported = 7,
        ReOpen = 8,
        CompletedNotExported = 9,

        Cancelled=10,
        Rejected=11,
        Accepted=12,
        PaymentPending=13,
        Error=14,
        Paid=15

    }
}
