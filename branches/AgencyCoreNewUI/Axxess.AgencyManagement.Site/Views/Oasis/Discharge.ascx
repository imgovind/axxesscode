﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="oasisAssWindowContainer">
    <div id="dischargeTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
        <ul>
            <li><a href="#clinicalRecord_discharge">Clinical Record Items</a></li>
            <li><a href="#riskassessment_discharge">Risk Assessment</a></li>
            <li><a href="#sensorystatus_discharge">Sensory Status</a></li>
            <li><a href="#pain_discharge">Pain</a></li>
            <li><a href="#integumentarystatus_discharge">Integumentary Status</a></li>
            <li><a href="#respiratorystatus_discharge">Respiratory Status</a></li>
            <li><a href="#cardiacstatus_discharge">Cardiac Status</a></li>
            <li><a href="#eliminationstatus_discharge">Elimination Status</a></li>
            <li><a href="#behaviourialstatus_discharge">Neuro/Behaviourial Status</a></li>
            <li><a href="#adl_discharge">ADL/IADLs</a></li>
            <li><a href="#medications_discharge">Medications</a></li>
            <li><a href="#caremanagement_discharge">Care Management</a></li>
            <li><a href="#emergentcare_discharge">Emergent Care</a></li>
            <li><a href="#dischargeOnlyAdd_discharge">Discharge</a></li>
        </ul>
        <div style="width: 179px;">
            <input id="dischargeValidation" type="button" value="Validate" onclick="Discharge.Validate('<%=Model.Id%>'); " /></div>
        <div id="clinicalRecord_discharge" class="general abs">
            <% Html.RenderPartial("~/Views/Oasis/Discharge/NewDemographics.ascx", Model); %>
        </div>
        <div id="riskassessment_discharge" class="general abs">
        </div>
        <div id="sensorystatus_discharge" class="general abs">
        </div>
        <div id="pain_discharge" class="general abs">
        </div>
        <div id="integumentarystatus_discharge" class="general abs">
        </div>
        <div id="respiratorystatus_discharge" class="general abs">
        </div>
        <div id="cardiacstatus_discharge" class="general abs">
        </div>
        <div id="eliminationstatus_discharge" class="general abs">
        </div>
        <div id="behaviourialstatus_discharge" class="general abs">
        </div>
        <div id="adl_discharge" class="general abs">
        </div>
        <div id="medications_discharge" class="general abs">
        </div>
        <div id="caremanagement_discharge" class="general abs">
        </div>
        <div id="emergentcare_discharge" class="general abs">
        </div>
        <div id="dischargeOnlyAdd_discharge" class="general abs">
        </div>
    </div>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Discharge.js"))
       .OnDocumentReady(() =>
        {%>
Discharge.Init();
<%}); 
%>