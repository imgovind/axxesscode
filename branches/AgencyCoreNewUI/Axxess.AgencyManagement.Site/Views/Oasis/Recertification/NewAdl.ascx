﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationAdlForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <%string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="4">
                Activities Permitted (locator #18.B)
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input type="hidden" name="Recertification_485ActivitiesPermitted" value="" />
                        <input name="Recertification_485ActivitiesPermitted" value="1" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Complete bed rest </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="3" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Up as tolerated </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="5" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Exercise prescribed </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="7" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("7")  ){ %>checked="checked"<% }%>'" />Independent
                        at home</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="9" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        Cane </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="B" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("B")  ){ %>checked="checked"<% }%>'" />
                        Walker </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="2" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Bed rest with BRP </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="4" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Transfer bed-chair </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="6" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("6")  ){ %>checked="checked"<% }%>'" />Partial
                        weight bearing</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="8" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("8")  ){ %>checked="checked"<% }%>'" />
                        Crutches </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_485ActivitiesPermitted" value="A" type="checkbox" '<% if( activitiesPermitted!=null && activitiesPermitted.Contains("A")  ){ %>checked="checked"<% }%>'" />
                        Wheelchair </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Other (specify):
                        <%=Html.TextBox("Recertification_485ActivitiesPermittedOther", data.ContainsKey("485ActivitiesPermittedOther") ? data["485ActivitiesPermittedOther"].Answer : "", new { @id = "Recertification_485ActivitiesPermittedOther", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <%string[] genericMusculoskeletal = data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer != "" ? data["GenericMusculoskeletal"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Musculoskeletal
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input type="hidden" name="Recertification_GenericMusculoskeletal" value="" />
                        <input name="Recertification_GenericMusculoskeletal" value="1" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericMusculoskeletal" value="2" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Weakness </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericMusculoskeletal" value="3" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Ambulation Difficulty </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericMusculoskeletal" value="4" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Limited Mobility/ROM </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLimitedMobilityLocation", data.ContainsKey("GenericLimitedMobilityLocation") ? data["GenericLimitedMobilityLocation"].Answer : "", new { @id = "Recertification_GenericLimitedMobilityLocation", @size = "15", @maxlength = "15" })%>
                        <i>(location)</i> </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericMusculoskeletal" value="5" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Joint Pain/Stiffness </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericJointPainLocation", data.ContainsKey("GenericJointPainLocation") ? data["GenericJointPainLocation"].Answer : "", new { @id = "Recertification_GenericJointPainLocation", @size = "15", @maxlength = "15" })%>
                        <i>(location)</i> </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericMusculoskeletal" value="6" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("6")  ){ %>checked="checked"<% }%>'" />
                        Poor Balance </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericMusculoskeletal" value="7" type="checkbox" '<% if( genericMusculoskeletal!=null && genericMusculoskeletal.Contains("7")  ){ %>checked="checked"<% }%>'" />
                        Grip Strength </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp;</li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericGripStrengthEqual", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericGripStrengthEqual", "1", data.ContainsKey("GenericGripStrengthEqual") && data["GenericGripStrengthEqual"].Answer == "1" ? true : false, new { @id = "" })%>
                        Equal
                        <br />
                        <%=Html.RadioButton("Recertification_GenericGripStrengthEqual", "2", data.ContainsKey("GenericGripStrengthEqual") && data["GenericGripStrengthEqual"].Answer == "2" ? true : false, new { @id = "" })%>
                        Unequal
                        <br />
                        <%=Html.TextBox("Recertification_GenericGripStrengthDesc", data.ContainsKey("GenericGripStrengthDesc") ? data["GenericGripStrengthDesc"].Answer : "", new { @id = "Recertification_GenericGripStrengthDesc", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
            </td>
            <td>
                <%string[] genericBoundType = data.ContainsKey("GenericBoundType") && data["GenericBoundType"].Answer != "" ? data["GenericBoundType"].Answer.Split(',') : null; %>
                <ul>
                    <li>
                        <input type="hidden" name="Recertification_GenericBoundType" value="" />
                        <input name="Recertification_GenericBoundType" value="1" type="checkbox" '<% if( genericBoundType!=null && genericBoundType.Contains("1")){ %>checked="checked"<% }%>'" />
                        Bedbound </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericBoundType" value="2" type="checkbox" '<% if( genericBoundType!=null && genericBoundType.Contains("2")){ %>checked="checked"<% }%>'" />
                        Chairbound </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericBoundType" value="3" type="checkbox" '<% if( genericBoundType!=null && genericBoundType.Contains("3")){ %>checked="checked"<% }%>'" />
                        Contracture: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericContractureLocation", data.ContainsKey("GenericContractureLocation") ? data["GenericContractureLocation"].Answer : "", new { @id = "Recertification_GenericContractureLocation", @size = "15", @maxlength = "15" })%>
                        <i>(location)</i> </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericBoundType" value="4" type="checkbox" '<% if( genericBoundType!=null && genericBoundType.Contains("4")){ %>checked="checked"<% }%>'" />
                        Paralysis: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericParalysisLocation", data.ContainsKey("GenericParalysisLocation") ? data["GenericParalysisLocation"].Answer : "", new { @id = "Recertification_GenericParalysisLocation", @size = "15", @maxlength = "15" })%>
                        <i>(location)</i> </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericParalysisDominant", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericParalysisDominant", "1", data.ContainsKey("GenericParalysisDominant") && data["GenericParalysisDominant"].Answer == "1" ? true : false, new { @id = "" })%>
                        Dominant
                        <br />
                        <%=Html.RadioButton("Recertification_GenericParalysisDominant", "2", data.ContainsKey("GenericParalysisDominant") && data["GenericParalysisDominant"].Answer == "2" ? true : false, new { @id = "" })%>
                        Nondominant
                        <br />
                    </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericBoundType" value="5" type="checkbox" '<% if( genericBoundType!=null && genericBoundType.Contains("5")){ %>checked="checked"<% }%>'" />
                        Assistive Device: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericAssistiveDeviceType", data.ContainsKey("GenericAssistiveDeviceType") ? data["GenericAssistiveDeviceType"].Answer : "", new { @id = "Recertification_GenericAssistiveDeviceType", @size = "15", @maxlength = "15" })%>
                        <i>(type)</i> </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul>
                    <li>Comments:<br />
                        <%=Html.TextArea("Recertification_GenericMusculoskeletalComments", data.ContainsKey("GenericMusculoskeletalComments") ? data["GenericMusculoskeletalComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericMusculoskeletalComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids)
                including undergarments, pullovers, front-opening shirts and blouses, managing zippers,
                buttons, and snaps:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1810CurrentAbilityToDressUpper", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1810CurrentAbilityToDressUpper", "00", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to get clothes out of closets and drawers, put them on and remove them from
                the upper body without assistance.<br />
                <%=Html.RadioButton("Recertification_M1810CurrentAbilityToDressUpper", "01", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to dress upper body without assistance if clothing is laid out or handed
                to the patient.<br />
                <%=Html.RadioButton("Recertification_M1810CurrentAbilityToDressUpper", "02", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient put on upper body clothing.<br />
                <%=Html.RadioButton("Recertification_M1810CurrentAbilityToDressUpper", "03", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to dress the upper body.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids)
                including undergarments, slacks, socks or nylons, shoes:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1820CurrentAbilityToDressLower", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1820CurrentAbilityToDressLower", "00", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to obtain, put on, and remove clothing and shoes without assistance.<br />
                <%=Html.RadioButton("Recertification_M1820CurrentAbilityToDressLower", "01", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to dress lower body without assistance if clothing and shoes are laid out
                or handed to the patient.<br />
                <%=Html.RadioButton("Recertification_M1820CurrentAbilityToDressLower", "02", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient put on undergarments, slacks, socks or nylons, and
                shoes.<br />
                <%=Html.RadioButton("Recertification_M1820CurrentAbilityToDressLower", "03", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to dress lower body.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing
                face, washing hands, and shampooing hair).
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1830CurrentAbilityToBatheEntireBody", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "00", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to bathe self in shower or tub independently, including getting in and out
                of tub/shower.<br />
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "01", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - With the use of devices, is able to bathe self in shower or tub independently,
                including getting in and out of the tub/shower.<br />
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "02", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to bathe in shower or tub with the intermittent assistance of another person:<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) for intermittent supervision or encouragement
                or reminders, OR
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) to get in and out of the shower or
                tub, OR<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) for washing difficult to reach areas.<br />
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "03", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to participate in bathing self in shower or tub, but requires presence of
                another person throughout the bath for assistance or supervision.<br />
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "04", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to use the shower or tub, but able to bathe self independently with or
                without the use of devices at the sink, in chair, or on commode.<br />
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "05", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Unable to use the shower or tub, but able to participate in bathing self in bed,
                at the sink, in bedside chair, or on commode, with the assistance or supervision
                of another person throughout the bath.<br />
                <%=Html.RadioButton("Recertification_M1830CurrentAbilityToBatheEntireBody", "06", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
                - Unable to participate effectively in bathing and is bathed totally by another
                person.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside
                commode safely and transfer on and off toilet/commode.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1840ToiletTransferring", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1840ToiletTransferring", "00", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to get to and from the toilet and transfer independently with or without
                a device.<br />
                <%=Html.RadioButton("Recertification_M1840ToiletTransferring", "01", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - When reminded, assisted, or supervised by another person, able to get to and from
                the toilet and transfer.<br />
                <%=Html.RadioButton("Recertification_M1840ToiletTransferring", "02", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Unable to get to and from the toilet but is able to use a bedside commode (with
                or without assistance).<br />
                <%=Html.RadioButton("Recertification_M1840ToiletTransferring", "03", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal
                independently.<br />
                <%=Html.RadioButton("Recertification_M1840ToiletTransferring", "04", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Is totally dependent in toileting.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1850) Transferring: Current ability to move safely from bed to chair, or ability
                to turn and position self in bed if patient is bedfast.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1850Transferring", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1850Transferring", "00", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently transfer.<br />
                <%=Html.RadioButton("Recertification_M1850Transferring", "01", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to transfer with minimal human assistance or with use of an assistive device.<br />
                <%=Html.RadioButton("Recertification_M1850Transferring", "02", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to bear weight and pivot during the transfer process but unable to transfer
                self.<br />
                <%=Html.RadioButton("Recertification_M1850Transferring", "03", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Unable to transfer self and is unable to bear weight or pivot when transferred
                by another person.<br />
                <%=Html.RadioButton("Recertification_M1850Transferring", "04", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Bedfast, unable to transfer but is able to turn and position self in bed.<br />
                <%=Html.RadioButton("Recertification_M1850Transferring", "05", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Bedfast, unable to transfer and is unable to turn and position self.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing
                position, or use a wheelchair, once in a seated position, on a variety of surfaces.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1860AmbulationLocomotion", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "00", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently walk on even and uneven surfaces and negotiate stairs with
                or without railings (i.e., needs no human assistance or assistive device).<br />
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "01", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able
                to independently walk on even and uneven surfaces and negotiate stairs with or without
                railings.<br />
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "02", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Requires use of a two-handed device (e.g., walker or crutches) to walk alone on
                a level surface and/or requires human supervision or assistance to negotiate stairs
                or steps or uneven surfaces.<br />
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "03", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to walk only with the supervision or assistance of another person at all
                times.<br />
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "04", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Chairfast, unable to ambulate but is able to wheel self independently.<br />
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "05", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Chairfast, unable to ambulate and is unable to wheel self.<br />
                <%=Html.RadioButton("Recertification_M1860AmbulationLocomotion", "06", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
                - Bedfast, unable to ambulate or be up in a chair.
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <%string[] nursingInterventions = data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer != "" ? data["485NursingInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485NursingInterventions" value=" " />
                <input name="Recertification_485NursingInterventions" value="1" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Physical therapy
                <%=Html.TextBox("Recertification_485PhysicalTherapyFreq", data.ContainsKey("485PhysicalTherapyFreq") ? data["485PhysicalTherapyFreq"].Answer : "", new { @id = "Recertification_485PhysicalTherapyFreq", @size = "10", @maxlength = "20" })%>
                (freq) to evaluate week of
                <%=Html.TextBox("Recertification_485PhysicalTherapyDate", data.ContainsKey("485PhysicalTherapyDate") ? data["485PhysicalTherapyDate"].Answer : "", new { @id = "Recertification_485PhysicalTherapyDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485NursingInterventions" value="2" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Occupational therapy
                <%=Html.TextBox("Recertification_485OccupationalTherapyFreq", data.ContainsKey("485OccupationalTherapyFreq") ? data["485OccupationalTherapyFreq"].Answer : "", new { @id = "Recertification_485OccupationalTherapyFreq", @size = "10", @maxlength = "20" })%>
                (freq) to evaluate week of
                <%=Html.TextBox("Recertification_485OccupationalTherapyDate", data.ContainsKey("485OccupationalTherapyDate") ? data["485OccupationalTherapyDate"].Answer : "", new { @id = "Recertification_485OccupationalTherapyDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485NursingInterventions" value="3" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Home Health Aide (freq)
                <%=Html.TextBox("Recertification_485HomeHealthAideFreq", data.ContainsKey("485HomeHealthAideFreq") ? data["485HomeHealthAideFreq"].Answer : "", new { @id = "Recertification_485HomeHealthAideFreq", @size = "10", @maxlength = "10" })%>
                for assistance with ADLs/IADLs
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485NursingInterventions" value="4" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess for patient adherence to appropriate activity levels
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485NursingInterventions" value="5" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient's compliance with home exercise program
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485NursingInterventions" value="6" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructRomExcercisePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructRomExcercisePerson") && data["485InstructRomExcercisePerson"].Answer != "" ? data["485InstructRomExcercisePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructRomExcercisePerson", instructRomExcercisePerson)%>
                on proper ROM exercises and body alignment techniques
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485NursingInterventions" value="7" type="checkbox" '<% if( nursingInterventions!=null && nursingInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to perform circulatory checks and cast care every visit
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var aDLOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485ADLOrderTemplates") && data["485ADLOrderTemplates"].Answer != "" ? data["485ADLOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485ADLOrderTemplates", aDLOrderTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485ADLComments", data.ContainsKey("485ADLComments") ? data["485ADLComments"].Answer : "", 5, 70, new { @id = "Recertification_485ADLComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] nursingGoals = data.ContainsKey("485NursingGoals") && data["485NursingGoals"].Answer != "" ? data["485NursingGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485NursingGoals" id="Recertification_485EstablishHomeExercisePT" />
                <input name="Recertification_485NursingGoals" value="1" type="checkbox" '<% if( nursingGoals!=null && nursingGoals.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Home exercise program will be established by physical therapist
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NursingGoals" value="2" type="checkbox" '<% if( nursingGoals!=null && nursingGoals.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Home exercise program will be established by occupational therapist
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NursingGoals" value="3" type="checkbox" '<% if( nursingGoals!=null && nursingGoals.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's mobility will be improved with assistance of physical therapist
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NursingGoals" value="4" type="checkbox" '<% if( nursingGoals!=null && nursingGoals.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var demonstrateROMExcercisePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DemonstrateROMExcercisePerson") && data["485DemonstrateROMExcercisePerson"].Answer != "" ? data["485DemonstrateROMExcercisePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485DemonstrateROMExcercisePerson", demonstrateROMExcercisePerson)%>
                &nbsp; will demonstrate proper ROM exercise and body alignment techniques
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NursingGoals" value="5" type="checkbox" '<% if( nursingGoals!=null && nursingGoals.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will remain free from impaired circulation related to cast or other orthotic
                device
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485NursingGoals" value="6" type="checkbox" '<% if( nursingGoals!=null && nursingGoals.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's ADL/IADL needs will be met with assistance of home health aide
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var aDLGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485ADLGoalTemplates") && data["485ADLGoalTemplates"].Answer != "" ? data["485ADLGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485ADLGoalTemplates", aDLGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485ADLGoalComments", data.ContainsKey("485ADLGoalComments") ? data["485ADLGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485ADLGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="3">
                Fall Assessment
            </th>
        </tr>
        <tr>
            <td>
                <i>One point is assessed for each <strong>yes</strong> selection</i>
            </td>
            <td width="15px">
                Yes
            </td>
            <td width="15px">
                No
            </td>
        </tr>
        <tr>
            <td>
                <strong>Age 65+</strong>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericAge65Plus", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericAge65Plus", "1", data.ContainsKey("GenericAge65Plus") && data["GenericAge65Plus"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericAge65Plus", "0", data.ContainsKey("GenericAge65Plus") && data["GenericAge65Plus"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Diagnosis (3 or more co-existing)</strong><br />
                <i>Assess for hypotension.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericHypotensionDiagnosis", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericHypotensionDiagnosis", "1", data.ContainsKey("GenericHypotensionDiagnosis") && data["GenericHypotensionDiagnosis"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericHypotensionDiagnosis", "0", data.ContainsKey("GenericHypotensionDiagnosis") && data["GenericHypotensionDiagnosis"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Prior history of falls within 3 months</strong><br />
                <i>Fall definition: "An unintentional change in position resulting in coming to
                rest on the ground or at a lower level."
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericPriorFalls", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericPriorFalls", "1", data.ContainsKey("GenericPriorFalls") && data["GenericPriorFalls"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericPriorFalls", "0", data.ContainsKey("GenericPriorFalls") && data["GenericPriorFalls"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Incontinence</strong><br />
                <i>Inability to make it to the bathroom or commode in timely manner. Includes frequency,
                    urgency, and/or nocturia.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericFallIncontinence", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericFallIncontinence", "1", data.ContainsKey("GenericFallIncontinence") && data["GenericFallIncontinence"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericFallIncontinence", "0", data.ContainsKey("GenericFallIncontinence") && data["GenericFallIncontinence"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Visual impairment</strong><br />
                <i>Includes macular degeneration, diabetic retinopathies, visual field loss, age related
                    changes, decline in visual acuity, accommodation, glare tolerance, depth perception,
                    and night vision or not wearing prescribed glasses or having the correct prescription.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericVisualImpairment", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericVisualImpairment", "1", data.ContainsKey("GenericVisualImpairment") && data["GenericVisualImpairment"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericVisualImpairment", "0", data.ContainsKey("GenericVisualImpairment") && data["GenericVisualImpairment"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Impaired functional mobility</strong><br />
                <i>May include patients who need help with IADLs or ADLs or have gait or transfer problems,
                    arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination
                    or improper use of assistive devices.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericImpairedFunctionalMobility", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericImpairedFunctionalMobility", "1", data.ContainsKey("GenericImpairedFunctionalMobility") && data["GenericImpairedFunctionalMobility"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericImpairedFunctionalMobility", "0", data.ContainsKey("GenericImpairedFunctionalMobility") && data["GenericImpairedFunctionalMobility"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Environmental hazards</strong><br />
                <i>May include poor illumination, equipment tubing, inappropriate footwear, pets, hard
                    to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and
                    exits.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericEnvHazards", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericEnvHazards", "1", data.ContainsKey("GenericEnvHazards") && data["GenericEnvHazards"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericEnvHazards", "0", data.ContainsKey("GenericEnvHazards") && data["GenericEnvHazards"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Poly Pharmacy (4 or more prescriptions)</strong><br />
                <i>Drugs highly associated with fall risk include but are not limited to, sedatives,
                    anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids,
                    anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericPolyPharmacy", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericPolyPharmacy", "1", data.ContainsKey("GenericPolyPharmacy") && data["GenericPolyPharmacy"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericPolyPharmacy", "0", data.ContainsKey("GenericPolyPharmacy") && data["GenericPolyPharmacy"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Pain affecting level of function</strong><br />
                <i>Pain often affects an individual's desire or ability to move or pain can be a factor
                    in depression or compliance with safety recommendations.</i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericPainAffectingFunction", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericPainAffectingFunction", "1", data.ContainsKey("GenericPainAffectingFunction") && data["GenericPainAffectingFunction"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericPainAffectingFunction", "0", data.ContainsKey("GenericPainAffectingFunction") && data["GenericPainAffectingFunction"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Cognitive impairment</strong><br />
                <i>Could include patients with dementia, Alzheimer's or stroke patients or patients
                    who are confused, use poor judgment, have decreased comprehension, impulsivity,
                    memory deficits. Consider patient's ability to adhere to the plan of care. </i>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.Hidden("Recertification_GenericCognitiveImpairment", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericCognitiveImpairment", "1", data.ContainsKey("GenericCognitiveImpairment") && data["GenericCognitiveImpairment"].Answer == "1" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericCognitiveImpairment", "0", data.ContainsKey("GenericCognitiveImpairment") && data["GenericCognitiveImpairment"].Answer == "0" ? true : false, new { @id = "" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <div class="bold" align="right">
                    Total:</div>
            </td>
            <td colspan="2">
                <div id="box3">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                A score of 4 or more is considered at risk for falling
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] instructInterventions = data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer != "" ? data["485InstructInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485InstructInterventions" value=" " />
                <input name="Recertification_485InstructInterventions" value="1" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to wear proper footwear when ambulating
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="2" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to use prescribed assistive device when ambulating
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="3" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to change positions slowly
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="4" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructRemoveRugsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructRemoveRugsPerson") && data["485InstructRemoveRugsPerson"].Answer != "" ? data["485InstructRemoveRugsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructRemoveRugsPerson", instructRemoveRugsPerson)%>
                to remove throw rugs or use double-sided tape to secure rug in place
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="5" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructRemoveClutterPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructRemoveClutterPerson") && data["485InstructRemoveClutterPerson"].Answer != "" ? data["485InstructRemoveClutterPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructRemoveClutterPerson", instructRemoveClutterPerson)%>
                to remove clutter from patient's path such as clothes, books, shoes, electrical
                cords, or other items that may cause patient to trip
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="6" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructContactForDizzinessPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructContactForDizzinessPerson") && data["485InstructContactForDizzinessPerson"].Answer != "" ? data["485InstructContactForDizzinessPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructContactForDizzinessPerson", instructContactForDizzinessPerson)%>
                to contact agency for increased dizziness or problems with balance
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="7" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess date of patient's last eye exam
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="8" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to have annual eye exams
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="9" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("9")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to use non-skid mats in tub/shower
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="10" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("10")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructAdequateLightingPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructAdequateLightingPerson") && data["485InstructAdequateLightingPerson"].Answer != "" ? data["485InstructAdequateLightingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructAdequateLightingPerson", instructAdequateLightingPerson)%>
                on importance of adequate lighting in patient area
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructInterventions" value="11" type="checkbox" '<% if( instructInterventions!=null && instructInterventions.Contains("11")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructContactForFallPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructContactForFallPerson") && data["485InstructContactForFallPerson"].Answer != "" ? data["485InstructContactForFallPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructContactForFallPerson", instructContactForFallPerson)%>
                to contact agency to report any fall with or without minor injury and to call 911
                for fall resulting in serious injury or causing severe pain or immobility
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var iADLOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485IADLOrderTemplates") && data["485IADLOrderTemplates"].Answer != "" ? data["485IADLOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485IADLOrderTemplates", iADLOrderTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485IADLComments", data.ContainsKey("485IADLComments") ? data["485IADLComments"].Answer : "", 5, 70, new { @id = "Recertification_485IADLComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] instructGoals = data.ContainsKey("485InstructGoals") && data["485InstructGoals"].Answer != "" ? data["485InstructGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485InstructGoals" value=" " />
                <input name="Recertification_485InstructGoals" value="1" type="checkbox" '<% if( instructGoals!=null && instructGoals.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The patient will be free from falls during the certification period
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructGoals" value="2" type="checkbox" '<% if( instructGoals!=null && instructGoals.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The patient will be free from injury during the certification period
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructGoals" value="3" type="checkbox" '<% if( instructGoals!=null && instructGoals.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeAnnualEyeExamPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeAnnualEyeExamPerson") && data["485VerbalizeAnnualEyeExamPerson"].Answer != "" ? data["485VerbalizeAnnualEyeExamPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeAnnualEyeExamPerson", verbalizeAnnualEyeExamPerson)%>
                will verbalize understanding of need for annual eye examination by:
                <%=Html.TextBox("Recertification_485VerbalizeAnnualEyeExamDate", data.ContainsKey("485VerbalizeAnnualEyeExamDate") ? data["485VerbalizeAnnualEyeExamDate"].Answer : "", new { @id = "Recertification_485VerbalizeAnnualEyeExamDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructGoals" value="4" type="checkbox" '<% if( instructGoals!=null && instructGoals.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var removeClutterFromPathPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485RemoveClutterFromPathPerson") && data["485RemoveClutterFromPathPerson"].Answer != "" ? data["485RemoveClutterFromPathPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485RemoveClutterFromPathPerson", removeClutterFromPathPerson)%>
                will remove all clutter from patient's path, such as clothes, books, shoes, electrical
                cords, and other items, that may cause patient to trip by:
                <%=Html.TextBox("Recertification_485RemoveClutterFromPathDate", data.ContainsKey("485RemoveClutterFromPathDate") ? data["485RemoveClutterFromPathDate"].Answer : "", new { @id = "Recertification_485RemoveClutterFromPathDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485InstructGoals" value="5" type="checkbox" '<% if( instructGoals!=null && instructGoals.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var removeThrowRugsAndSecurePerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485RemoveThrowRugsAndSecurePerson") && data["485RemoveThrowRugsAndSecurePerson"].Answer != "" ? data["485RemoveThrowRugsAndSecurePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485RemoveThrowRugsAndSecurePerson", removeThrowRugsAndSecurePerson)%>
                will remove throw rugs or secure them with double-sided tape by:
                <%=Html.TextBox("Recertification_485RemoveThrowRugsAndSecureDate", data.ContainsKey("485RemoveThrowRugsAndSecureDate") ? data["485RemoveThrowRugsAndSecureDate"].Answer : "", new { @id = "Recertification_485RemoveThrowRugsAndSecureDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var iADLGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485IADLGoalTemplates") && data["485IADLGoalTemplates"].Answer != "" ? data["485IADLGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485IADLGoalTemplates", iADLGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485IADLGoalComments", data.ContainsKey("485IADLGoalComments") ? data["485IADLGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485IADLGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <%string[] dME = data.ContainsKey("485DME") && data["485DME"].Answer != "" ? data["485DME"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="5">
                DME (locator #14)
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="Recertification_485DME" value="" />
                <input name="Recertification_485DME" value="1" type="checkbox" '<% if( dME!=null && dME.Contains("1")){ %>checked="checked"<% }%>'" />
                Bedside Commode
            </td>
            <td>
                <input name="Recertification_485DME" value="2" type="checkbox" '<% if( dME!=null && dME.Contains("2")){ %>checked="checked"<% }%>'" />
                Cane
            </td>
            <td>
                <input name="Recertification_485DME" value="3" type="checkbox" '<% if( dME!=null && dME.Contains("3")){ %>checked="checked"<% }%>'" />
                Elevated Toilet Seat
            </td>
            <td>
                <input name="Recertification_485DME" value="4" type="checkbox" '<% if( dME!=null && dME.Contains("4")){ %>checked="checked"<% }%>'" />
                Grab Bars
            </td>
            <td>
                <input name="Recertification_485DME" value="5" type="checkbox" '<% if( dME!=null && dME.Contains("5")){ %>checked="checked"<% }%>'" />
                Hospital Bed
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485DME" value="6" type="checkbox" '<% if( dME!=null && dME.Contains("6")){ %>checked="checked"<% }%>'" />
                Nebulizer
            </td>
            <td>
                <input name="Recertification_485DME" value="7" type="checkbox" '<% if( dME!=null && dME.Contains("7")){ %>checked="checked"<% }%>'" />
                Oxygen
            </td>
            <td>
                <input name="Recertification_485DME" value="8" type="checkbox" '<% if( dME!=null && dME.Contains("8")){ %>checked="checked"<% }%>'" />
                Tub/Shower Bench
            </td>
            <td>
                <input name="Recertification_485DME" value="9" type="checkbox" '<% if( dME!=null && dME.Contains("9")){ %>checked="checked"<% }%>'" />
                Walker
            </td>
            <td>
                <input name="Recertification_485DME" value="10" type="checkbox" '<% if( dME!=null && dME.Contains("10")){ %>checked="checked"<% }%>'" />
                Wheelchair
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <ul>
                    <li>Other:
                        <br />
                        <%=Html.TextArea("Recertification_485DMEComments", data.ContainsKey("485DMEComments") ? data["485DMEComments"].Answer : "", 5, 70, new { @id = "Recertification_485DMEComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] supplies = data.ContainsKey("485Supplies") && data["485Supplies"].Answer != "" ? data["485Supplies"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="5">
                Supplies
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="Recertification_485Supplies" value="" />
                <input name="Recertification_485Supplies" value="1" type="checkbox"  '<% if( supplies!=null && supplies.Contains("1")){ %>checked="checked"<% }%>'" />
                ABDs
            </td>
            <td>
                <input name="Recertification_485Supplies" value="2" type="checkbox" '<% if( supplies!=null && supplies.Contains("2")){ %>checked="checked"<% }%>'" />
                Ace Wrap
            </td>
            <td>
                <input name="Recertification_485Supplies" value="3" type="checkbox" '<% if( supplies!=null && supplies.Contains("3")){ %>checked="checked"<% }%>'" />
                Alcohol Pads
            </td>
            <td>
                <input name="Recertification_485Supplies" value="4" type="checkbox" '<% if( supplies!=null && supplies.Contains("4")){ %>checked="checked"<% }%>'" />
                Chux/Underpads
            </td>
            <td>
                <input name="Recertification_485Supplies" value="5" type="checkbox" '<% if( supplies!=null && supplies.Contains("5")){ %>checked="checked"<% }%>'" />
                Diabetic Supplies
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485Supplies" value="6" type="checkbox" '<% if( supplies!=null && supplies.Contains("6")){ %>checked="checked"<% }%>'" />
                Drainage Bag
            </td>
            <td>
                <input name="Recertification_485Supplies" value="7" type="checkbox" '<% if( supplies!=null && supplies.Contains("7")){ %>checked="checked"<% }%>'" />
                Dressing Supplies
            </td>
            <td>
                <input name="Recertification_485Supplies" value="8" type="checkbox" '<% if( supplies!=null && supplies.Contains("8")){ %>checked="checked"<% }%>'" />
                Duoderm
            </td>
            <td>
                <input name="Recertification_485Supplies" value="9" type="checkbox" '<% if( supplies!=null && supplies.Contains("9")){ %>checked="checked"<% }%>'" />
                Exam Gloves
            </td>
            <td>
                <input name="Recertification_485Supplies" value="10" type="checkbox" '<% if( supplies!=null && supplies.Contains("10")){ %>checked="checked"<% }%>'" />
                Foley Catheter
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485Supplies" value="11" type="checkbox" '<% if( supplies!=null && supplies.Contains("11")){ %>checked="checked"<% }%>'" />
                Gauze Pads
            </td>
            <td>
                <input name="Recertification_485Supplies" value="12" type="checkbox" '<% if( supplies!=null && supplies.Contains("12")){ %>checked="checked"<% }%>'" />
                Insertion Kit
            </td>
            <td>
                <input name="Recertification_485Supplies" value="13" type="checkbox" '<% if( supplies!=null && supplies.Contains("13")){ %>checked="checked"<% }%>'" />
                Irrigation Set
            </td>
            <td>
                <input name="Recertification_485Supplies" value="14" type="checkbox" '<% if( supplies!=null && supplies.Contains("14")){ %>checked="checked"<% }%>'" />
                Irrigation Solution
            </td>
            <td>
                <input name="Recertification_485Supplies" value="15" type="checkbox" '<% if( supplies!=null && supplies.Contains("15")){ %>checked="checked"<% }%>'" />
                Kerlix Rolls
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485Supplies" value="16" type="checkbox" '<% if( supplies!=null && supplies.Contains("16")){ %>checked="checked"<% }%>'" />
                Leg Bag
            </td>
            <td>
                <input name="Recertification_485Supplies" value="17" type="checkbox" '<% if( supplies!=null && supplies.Contains("17")){ %>checked="checked"<% }%>'" />
                Needles
            </td>
            <td>
                <input name="Recertification_485Supplies" value="18" type="checkbox" '<% if( supplies!=null && supplies.Contains("18")){ %>checked="checked"<% }%>'" />
                NG Tube
            </td>
            <td>
                <input name="Recertification_485Supplies" value="19" type="checkbox" '<% if( supplies!=null && supplies.Contains("19")){ %>checked="checked"<% }%>'" />
                Probe Covers
            </td>
            <td>
                <input name="Recertification_485Supplies" value="20" type="checkbox" '<% if( supplies!=null && supplies.Contains("20")){ %>checked="checked"<% }%>'" />
                Sharps Container
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485Supplies" value="21" type="checkbox" '<% if( supplies!=null && supplies.Contains("21")){ %>checked="checked"<% }%>'" />
                Sterile Gloves
            </td>
            <td>
                <input name="Recertification_485Supplies" value="22" type="checkbox" '<% if( supplies!=null && supplies.Contains("22")){ %>checked="checked"<% }%>'" />
                Syringe
            </td>
            <td>
                <input name="Recertification_485Supplies" value="23" type="checkbox" '<% if( supplies!=null && supplies.Contains("23")){ %>checked="checked"<% }%>'" />
                Tape
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <ul>
                    <li>Other:
                        <br />
                        <%=Html.TextArea("Recertification_485SuppliesComment", data.ContainsKey("485SuppliesComment") ? data["485SuppliesComment"].Answer : "", 5, 70, new { @id = "Recertification_485SuppliesComment", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="5">
                DME Provider
            </th>
        </tr>
        <tr>
            <td colspan="5" class="bold">
                Information for company (other than home health agency) that provides supplies/DME:
                <ul class="columns">
                    <li class="spacer">Name: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDMEProviderName", data.ContainsKey("GenericDMEProviderName") ? data["GenericDMEProviderName"].Answer : "", new { @id = "Recertification_GenericDMEProviderName", @size = "40", @maxlength = "40" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Address: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDMEProviderAddress", data.ContainsKey("GenericDMEProviderAddress") ? data["GenericDMEProviderAddress"].Answer : "", new { @id = "Recertification_GenericDMEProviderAddress", @size = "40", @maxlength = "40" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Phone Number: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDMEProviderPhone", data.ContainsKey("GenericDMEProviderPhone") ? data["GenericDMEProviderPhone"].Answer : "", new { @id = "Recertification_GenericDMEProviderPhone", @size = "15", @maxlength = "15" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Supplies/DME Provided: </li>
                    <li>
                        <%=Html.TextArea("Recertification_GenericDMESuppliesProvided", data.ContainsKey("GenericDMESuppliesProvided") ? data["GenericDMESuppliesProvided"].Answer : "", 5, 70, new { @id = "Recertification_GenericDMESuppliesProvided", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
