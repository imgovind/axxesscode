﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Model == Guid.Empty ? Html.BeginForm("NewPhysician", "Patient", FormMethod.Post, new { @id = "newPhysicianForm" }) : Html.BeginForm("NewPhysicianContact", "Patient", FormMethod.Post, new { @id = "newPhysicianContactForm" }))%>
<%  { %>
<%=Html.Hidden("PatientId", Model, new { @id ="txtNew_PhysicianContact_PatientID" })%>
<div id="newPhysicianContactValidaton" class="marginBreak" style="display: none">
</div>
<div class="margin">
    <div class="rowTable">
        <table border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        <label>
                            Select from the list below:</label>
                        <select style="width: 150px; float: left" class="Physicians input_wrapper" tabindex="17"
                            name="AgencyPhysicians" id="txtNew_PhysicianContact_PhysicianDropDown">
                            <option value="0" selected="selected">** Select Physician **</option>
                        </select>
                    </th>
                    <th>
                        <label>
                            Search by NPI Number:</label>
                        <input type="text" name="txtNew_PhysicianContact_NpiNumber" id="txtNew_PhysicianContact_NpiNumber"
                            maxlength="10" />
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <div class="rowBreak">
                            <div class="contentDivider">
                                <div class="patientfieldset">
                                    <div class="fix">
                                        <div class="row">
                                            <label for="FirstName">
                                                First Name:&nbsp;&nbsp;&nbsp;</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("FirstName", "", new { @id = "txtNew_PhysicianContact_FirstName", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "48" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="LastName">
                                                Last Name:&nbsp;&nbsp;&nbsp;</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("LastName", "", new { @id = "txtNew_PhysicianContact_LastName", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "49" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="PhoneHome">
                                                Primary Phone:</label>
                                            <div class="inputs">
                                                <span class="input_wrappermultible">
                                                    <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                                        margin: 0px;" name="PhoneWorkArray" id="PhysicianContactPrimaryPhoneArray1"
                                                        maxlength="3" size="3" tabindex="50" /></span> - <span class="input_wrappermultible">
                                                            <input type="text" class="autotext digits required" style="width: 49px; padding: 0px;
                                                                margin: 0px;" name="PhoneWorkArray" id="PhysicianContactPrimaryPhoneArray2"
                                                                maxlength="3" size="3" tabindex="51" /></span> - <span class="input_wrappermultible">
                                                                    <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                                                        margin: 0px;" name="PhoneWorkArray" id="PhysicianContactPrimaryPhoneArray3"
                                                                        maxlength="4" size="5" tabindex="52" /></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="PhoneMobile">
                                                Fax:</label>
                                            <div class="inputs">
                                                <span class="input_wrappermultible">
                                                    <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                        name="FaxNumberArray" id="PhysicianContactFaxArray1" maxlength="3"
                                                        size="3" tabindex="53" /></span> - <span class="input_wrappermultible">
                                                            <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                                                name="FaxNumberArray" id="PhysicianContactFaxArray2" maxlength="3"
                                                                size="3" tabindex="54" /></span> - <span class="input_wrappermultible">
                                                                    <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                                        name="FaxNumberArray" id="PhysicianContactFaxArray3" maxlength="4"
                                                                        size="5" tabindex="55" /></span>
                                            </div>
                                        </div>
                                        <div class="row" style="vertical-align: middle;">
                                            <label for="FirstName">
                                                Email:&nbsp;&nbsp;&nbsp;</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("EmailAddress", "", new { @id = "txtNew_PhysicianContact_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "56" })%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contentDivider">
                                <div class="patientfieldset">
                                    <div class="fix">
                                        <div class="row">
                                            <label for="Relationship">
                                                NPI No:</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("NPI", "", new { @id = "txtNew_PhysicianContact_NPINo", @class = "text input_wrapper digits", @maxlength = "10", @tabindex = "57" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="AddressLine1">
                                                Address Line 1:</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("AddressLine1", "", new { @id = "txtNew_PhysicianContact_AddressLine1", @class = "text input_wrapper required", @tabindex = "58" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="AddressLine2">
                                                Address Line 2:</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("AddressLine2", "", new { @id = "txtNew_PhysicianContact_AddressLine2", @class = "text input_wrapper", tabindex = "59" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="AddressCity">
                                                City:</label>
                                            <div class="inputs">
                                                <%=Html.TextBox("AddressCity", "", new { @id = "txtNew_PhysicianContact_AddressCity", @class = "text input_wrapper required", @tabindex = "60" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="AddressStateCode">
                                                State, Zip Code :</label>
                                           
                                            <div class="inputs">
                                               <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "0", new { @id = "txtNew_PhysicianContact_AddressStateCode", @class = "input_wrapper required", @tabindex = "", @style = "width: 119px;" })%>
                                                &nbsp;
                                                <%=Html.TextBox("AddressZipCode", "", new { @id = "txtNew_PhysicianContact_AddressZipCode", @class = "text digits isValidUSZip required", @tabindex = "62", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "4", @maxlength = "5" })%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="SetPrimary">
                                                Set Primary:</label>
                                            <div class="inputs">
                                                <%=Html.CheckBox("Primary", new { @id = "txtNew_PhysicianContact_SetPrimary" })%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="buttons">
        <ul>
            <li>
                <input name="" type="submit" value="Add" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input name="" type="reset" value="Reset" /></li>
        </ul>
    </div>
</div>
<%} %>
