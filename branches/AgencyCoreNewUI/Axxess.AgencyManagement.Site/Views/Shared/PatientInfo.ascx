﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="patientInfo" class="patientInfo">
    <div class="row">
        <label for="ReferrerFirstName">
            Patient ID#:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <span id="patientId" class="input_wrapper blank"></span>
        </div>
    </div>
    <div class="row">
        <label for="ReferrerFirstName">
            Patient Name:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <span id="patientName" class="input_wrapper blank"></span>
        </div>
    </div>
    <div class="row">
        <label>
            Address:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientAddress1" class="input_wrapper blank"></span></b>
            <br />
            <b><span id="patientAddress2" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            &nbsp;</label>
        <div class="inputs">
            <b><span id="patientCityStateZip" class="input_wrapper blank"></span></b>&nbsp;&nbsp;[<span
                class="input_wrapper blank"><a href="javascript:void(0);" onclick="JQD.open_window('#patientmap');">Map</a>&nbsp;|&nbsp;<a
                    href="javascript:void(0);">Direction</a>&nbsp;</span>]
        </div>
    </div>
    <div class="row">
        <label>
            Primary Phone:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientHomePhone" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Alternate Phone:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientAltPhone" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label for="patientSocDate">
            Start of Care Date:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <span id="patientSocDate" class="input_wrapper blank"></span>
        </div>
    </div>
    <div class="row">
        <label for="patientGender">
            Gender:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <span id="patientGender" class="input_wrapper blank"></span>
        </div>
    </div>
    <div class="row">
        <label for="patientBirthDate">
            Birth Date:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <span id="patientBirthDate" class="input_wrapper blank"></span>
        </div>
    </div>
    <div class="row">
        <label>
            Medicare #:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientMedicareNumber" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Medicaid #:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientMedicaidNumber" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="underLine" style="background:#FFFFFF;">
    </div>
    <div class="row">
        <label>
            Physician Name:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientPhysicianName" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Physician Phone:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientPhysicianPhone" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Physician Email:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientPhysicianEmail" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="underLine" style="background:#FFFFFF;">
    </div>
    <div class="row">
        <label>
            Emergency Name:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientEmergencyContactName" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Emergency Phone:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientEmergencyContactPhone" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Emergency Email:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientEmergencyContactEmail" class="input_wrapper blank"></span></b>
        </div>
    </div>
    <div class="row">
        <label>
            Branch Location:&nbsp;&nbsp;&nbsp;</label>
        <div class="inputs">
            <b><span id="patientBranch" class="input_wrapper blank"></span></b>
        </div>
    </div>
</div>
