﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("CommunicationNote", "Patient", FormMethod.Post, new { @id = "newCommunicationNoteForm" }))%>
<%  { %>
<%=Html.Hidden("PatientId", Model.Id, new { @id = "txtNew_CommunicationNote_PatientID" })%>
<div id="newCommunicationNoteValidaton" class="marginBreak " style="display: none">
</div>
<div class="row">
    <div class="contentDivider">
        <div class="row">
            <label for="FirstName">
                &nbsp;&nbsp;&nbsp; Patient Name:&nbsp;&nbsp;&nbsp;</label>
            <span name="CommunicationNotePatient" id="txtNew_CommunicationNote_Patient">
                <%=Model.DisplayName %></span>
        </div>
    </div>
    <div class="contentDivider">
        <div class="row">
            <label for="FirstName">
                &nbsp;&nbsp;&nbsp;Date:&nbsp;&nbsp;&nbsp;</label>
            <%= Html.Telerik().DatePicker()
                                        .Name("Created")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new { @id = "txtNew_CommunicationNote_Date" })
                                                                         
            %>
        </div>
    </div>
</div>
<div class="row">
    <label for="FirstName">
        &nbsp;&nbsp;&nbsp; Communication Text&nbsp;&nbsp;&nbsp;</label>
    <div class="marginBreakNotes">
        <%=Html.TextArea("Text", new { @id = "txtNew_CommunicationNote_text", @class = "notesTextArea required", @style = "height:auto;" })%>
    </div>
</div>
<div class="row">
    <div class="buttons buttonfix">
        <ul>
            <li>
                <input name="" type="submit" value="Save" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
        </ul>
    </div>
</div>
<%} %>
