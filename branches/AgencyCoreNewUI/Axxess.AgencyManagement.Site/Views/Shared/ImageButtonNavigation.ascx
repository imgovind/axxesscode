﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="abs" id="bar_QB">
    <ul id="qbmenu">
        <li>
            <img src="/Images/icons/home.png" alt="home" /><a href="javascript:void(0);" onclick="JQD.open_window('#window_computer');">Home</a></li>
        <li>
            <img src="/Images/icons/messages.png" alt="messages" /><a href="javascript:void(0);"
                onclick="JQD.open_window('#rap');">Messages<span style="color: Red;">&nbsp;(23)</span></a></li>
        <li>
            <img src="/Images/icons/patient.png" alt="patient" /><a href="javascript:void(0);"
                onclick="JQD.open_window('#patient_window');">Patients</a></li>
        <li>
            <img src="/Images/icons/billing.gif" alt="billing" /><a href="javascript:void(0);"
                onclick="Billing.loadBillingCenter();">Billing</a></li>
        <li>
            <img src="/Images/icons/schedule.png" alt="schedule" /><a href="javascript:void(0);"
                onclick="JQD.open_window('#schedule_window');">Schedules</a></li>
        <li>
            <img src="/Images/icons/finance.png" alt="finance" /><a href="javascript:void(0);"
                onclick="JQD.open_window('#rap');">Administration</a></li>
        <li>
            <img src="/Images/icons/report.png" alt="reports" /><a href="javascript:void(0);"
                onclick="JQD.open_window('#report');">Reports</a></li>
        <li>
            <img src="/Images/icons/employee.png" alt="users" /><a href="javascript:void(0);"
                onclick="JQD.open_window('#employee_window');">Users</a></li>
        <li>
            <img src="/Images/icons/exit.png" alt="logout" /><a href="/logout">Logout</a></li>
    </ul>
</div>
