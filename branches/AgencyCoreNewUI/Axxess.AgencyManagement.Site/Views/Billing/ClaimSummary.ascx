﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% using (Html.BeginForm("Generate", "Billing", FormMethod.Post, new { @id = "generateBilling" }))%>
<%  { %>
<%if ((Model.Finals != null && Model.Finals.Count > 0) || (Model.Raps != null && Model.Raps.Count > 0))
  {%>
<div class="row">
    <input type="submit" class="mine-button" value="Generate Claim and Save" />
</div>
<%}
  else
  {%>
<div class="row">
    <b>Select the claim you want to generate.</b>
</div>
<%} %>
<%
    if (Model != null)
    { %>
<%
    if (Model.Raps != null && Model.Raps.Count > 0)
    {%>
<div class="row">
    <table class="claim">
        <thead>
            <tr>
                <th>
                    Patient Name
                </th>
                <th>
                    Medicare No
                </th>
                <th>
                    Episode
                </th>
                <th>
                    Claim Amount
                </th>
            </tr>
        </thead>
        <tbody>
            <%
                int j = 1;%>
            <% foreach (var rap in Model.Raps)
               {%>
            <% if (j % 2 == 0)
               {%>
            <tr class="even">
                <%}
               else
               { %>
                <tr class="odd">
                    <%} %>
                    <td>
                        <%=Html.Hidden("rapSelected", rap.Id) %>
                        <%=j %>
                        .&nbsp;&nbsp;&nbsp;
                        <%=rap.DisplayName + "(" + rap.PatientIdNumber + ")"%>
                    </td>
                    <td>
                        <%=rap.MedicareNumber%>
                    </td>
                    <td>
                        <% if (rap.EpisodeStartDate != null)
                           {%>
                        <%=rap.EpisodeStartDate.ToShortDateString()%>
                        <%} %>
                        <% if (rap.EpisodeEndDate != null)
                           {%>
                        -<%=rap.EpisodeEndDate.ToShortDateString()%>
                        <%} %>
                    </td>
                    <td>
                    </td>
                </tr>
                <% j++;
               } %>
        </tbody>
        <tfoot>
            <tr>
            </tr>
        </tfoot>
    </table>
</div>
<%} %>
<% if (Model.Finals != null && Model.Finals.Count > 0)
   {%>
<div class="row">
    <table class="claim">
        <thead>
            <tr>
                <th>
                    Patient Name
                </th>
                <th>
                    Medicare No
                </th>
                <th>
                    Episode Date
                </th>
                <th>
                    Claim Amount
                </th>
            </tr>
        </thead>
        <tbody>
            <% int j = 1;%>
            <% foreach (var final in Model.Finals)
               {%>
            <% if (j % 2 == 0)
               {%>
            <tr class="even">
                <%}
               else
               { %>
                <tr class="odd">
                    <%} %>
                    <td>
                        <%=Html.Hidden("finalSelected", final.Id) %>
                        <%=j %>
                        .&nbsp;&nbsp;&nbsp;
                        <%=final.DisplayName + "(" + final.PatientIdNumber + ")"%>
                    </td>
                    <td>
                        <%=final.MedicareNumber%>
                    </td>
                    <td>
                        <% if (final.EpisodeStartDate != null)
                           {%>
                        <%=final.EpisodeStartDate.ToShortDateString()%>
                        <%} %>
                        <% if (final.EpisodeEndDate != null)
                           {%>
                        -<%=final.EpisodeEndDate.ToShortDateString()%>
                        <%} %>
                    </td>
                    <td>
                    </td>
                </tr>
                <% j++;
               } %>
        </tbody>
        <tfoot>
            <tr>
            </tr>
        </tfoot>
    </table>
</div>
<% }%>
<% } %>
<%if ((Model.Finals != null && Model.Finals.Count > 0) || (Model.Raps != null && Model.Raps.Count > 0))
  {%>
<div class="row">
    <input type="button" class="mine-button" value="Update the claim status" onclick="Billing.UpdateStatus('#generateBilling');"/>
</div>
<%}%>
<%} %>
