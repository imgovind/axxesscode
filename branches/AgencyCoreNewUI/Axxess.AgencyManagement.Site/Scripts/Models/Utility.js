﻿var U =
{
    showDialog: function(popupBox, height, width) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                border: 'none',
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            }
        });
    },
    postUrl: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: input,
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    block: function() {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: 'Loading...',
            css: {
                color: '#fff',
                opacity: .6,
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                top: ($(window).height() - 40) / 2 + 'px',
                left: ($(window).width() - 100) / 2 + 'px'
            }
        });
    },
    unBlock: function(selector) {
        $.unblockUI();
    }
};

jQuery.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }
