﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AxxessScript
    {
        private static string input = Path.Combine(App.Root, "Files\\Hebron.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Hebron_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;

                                            patientData.FirstName = dataRow.GetValue(0);
                                            patientData.LastName = dataRow.GetValue(1);
                                            patientData.Gender = dataRow.GetValue(2).IsEqual("f") ? "Female" : "Male";
                                            patientData.PatientIdNumber = dataRow.GetValue(3);
                                            patientData.MedicareNumber = dataRow.GetValue(4);
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            }
                                            patientData.SSN = dataRow.GetValue(6).Replace(" ", "");
                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                            }

                                            if (dataRow.GetValue(9).ToLower().Contains("married"))
                                            {
                                                patientData.MaritalStatus = "Married";
                                            }
                                            else if (dataRow.GetValue(9).ToLower().Contains("single"))
                                            {
                                                patientData.MaritalStatus = "Single";
                                            }
                                            else if (dataRow.GetValue(9).ToLower().Contains("widowed"))
                                            {
                                                patientData.MaritalStatus = "Widowed";
                                            }
                                            else if (dataRow.GetValue(9).ToLower().Contains("divorced"))
                                            {
                                                patientData.MaritalStatus = "Divorced";
                                            }
                                            else
                                            {
                                                patientData.MaritalStatus = "Unknown";
                                            }

                                            patientData.AddressLine1 = dataRow.GetValue(10);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(11);
                                            patientData.AddressStateCode = dataRow.GetValue(12).ToUpper().Replace(".", "");
                                            patientData.AddressZipCode = dataRow.GetValue(13);
                                            patientData.PhoneHome = dataRow.GetValue(14).ToPhoneDB();
                                            patientData.PrimaryInsurance = "1";
                                            //patientData.Comments = string.Format("Episode Range: {0}. ", dataRow.GetValue(8));

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId.ToGuid(),
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(22);
                                                            var physician = Database.GetPhysician(npi, agencyId.ToGuid());
                                                            if (physician == null)
                                                            {
                                                                exists = false;
                                                                var info = Database.GetNpiData(npi);
                                                                physician = new AgencyPhysician
                                                                {
                                                                    Id = Guid.NewGuid(),
                                                                    AgencyId = agencyId.ToGuid(),
                                                                    NPI = npi,
                                                                    LoginId = Guid.Empty,
                                                                    AddressLine1 = dataRow.GetValue(16),
                                                                    AddressCity = dataRow.GetValue(17),
                                                                    AddressStateCode = dataRow.GetValue(18).Replace(".", ""),
                                                                    AddressZipCode = dataRow.GetValue(19),
                                                                    PhoneWork = dataRow.GetValue(20).ToPhoneDB(),
                                                                    FaxNumber = dataRow.GetValue(21).ToPhoneDB(),
                                                                    FirstName = info != null ? info.ProviderFirstName : string.Empty,
                                                                    LastName = info != null ? info.ProviderLastName : string.Empty
                                                                };
                                                                var physicianNameArray = dataRow.GetValue(15).Replace("DR.", "").Replace("DR", "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                                {
                                                                    physician.LastName = physicianNameArray[1].Trim();
                                                                    physician.FirstName = physicianNameArray[0].Trim();
                                                                }
                                                                //else if (physicianNameArray != null && physicianNameArray.Length == 2)
                                                                //{
                                                                //    physician.FirstName = info.ProviderFirstName;
                                                                //    physician.LastName = physicianNameArray[1].Trim();
                                                                //}

                                                                Database.Add(physician);
                                                            }

                                                            if (physician != null)
                                                            {
                                                                var patientPhysician = new PatientPhysician
                                                                {
                                                                    IsPrimary = true,
                                                                    PatientId = patientData.Id,
                                                                    PhysicianId = physician.Id
                                                                };

                                                                if (Database.Add(patientPhysician))
                                                                {
                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
