﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    public static class CaliforniaHHAScript
    {
        private static Sheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\CaliHHAs.txt");
        private static string output = Path.Combine(App.Root, string.Format("Files\\CaliHHA_{0}.xls", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            Initialize();
            using (var fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    int i = 1;
                    while (streamReader.Peek() >= 0)
                    {
                        var line = streamReader.ReadLine();
                        if (line.IsNotNullOrEmpty() 
                            && !line.IsEqual(" ") 
                            && line.Contains("@")
                            && !line.StartsWith("NOTE: * -"))
                        {
                             var lineArray = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                             if (lineArray != null && lineArray.Length > 0)
                             {
                                 lineArray.ForEach(word =>
                                 {
                                     if (word.Length > 3 && word.Contains("@"))
                                     {
                                         Row excelRow = sheet.CreateRow(i);
                                         excelRow.CreateCell(0).SetCellValue(word);
                                         i++;
                                     }
                                 });
                             }
                        }
                    }
                }
                Write();
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("California HHA List");

            Font headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            sheet.AutoSizeColumn(0);

            using (FileStream fileStream = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }

    public class CaliAgency
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
    }
}
