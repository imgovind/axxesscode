﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

   public static class PatinetAddressScript
    {
       public static int Run(Guid agencyId)
       {
           var patients = Database.GetPatients(agencyId);
           int i = 0;
           if (patients != null && patients.Count > 0)
           {
               patients.ForEach(p =>
               {
                   if (p.AddressLine1 != null && p.AddressLine1 != "")
                   {
                       p.AddressLine1 = Regex.Replace(p.AddressLine1, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase);
                       i++;
                       Database.UpdatePatient(p);
                   }
               });

           }
           return i;
       }
    }
}
