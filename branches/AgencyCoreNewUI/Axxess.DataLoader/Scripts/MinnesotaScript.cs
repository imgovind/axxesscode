﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    public static class MinnesotaScript
    {
        private static Sheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\Minnesota.txt");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Minnesota_{0}.xls", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            Initialize();
            using (var fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    int i = 1;
                    Row excelRow = null;
                    while (streamReader.Peek() >= 0)
                    {
                        var line = streamReader.ReadLine();
                        if (line.IsNotNullOrEmpty())
                        {
                            if (line.ToLowerCase().Contains("name:"))
                            {
                                var startIndex = line.ToLowerCase().IndexOf(":");
                                var name = line.ToLowerCase().Substring(startIndex + 1, (line.ToLowerCase().Length - 1) - startIndex);
                                excelRow = sheet.CreateRow(i);
                                excelRow.CreateCell(0).SetCellValue(name.Trim().Replace("<br>", "").Replace("</b>", ""));
                            }
                            else if (line.ToLowerCase().Contains("fax:"))
                            {
                                var startIndex = line.ToLowerCase().LastIndexOf(":");
                                if (line.ToLowerCase().Length >= startIndex + 12)
                                {
                                    var faxNumber = line.ToLowerCase().Substring(startIndex + 1, (line.ToLowerCase().Length - 1) - startIndex);
                                    excelRow.CreateCell(1).SetCellValue(faxNumber.Trim().Replace("<br>", "").Replace("</b>", "").ToPhoneDB());
                                    i++;
                                }
                            }
                        }
                    }
                }
                Write();
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("Minnesota HHA List");

            Font headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(1).SetCellValue("Fax Number");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            sheet.AutoSizeColumn(0);

            using (FileStream fileStream = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
