﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AxxessScript4
    {
        private static string input = Path.Combine(App.Root, "Files\\HaveAHeartHHA2.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\HaveAHeartHHA2_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            var name = dataRow.GetValue(0);
                                            if (name.IsNotNullOrEmpty())
                                            {
                                                var firstAndLast = name.Split(',');
                                                if (firstAndLast != null && firstAndLast.Length > 1)
                                                {
                                                   
                                                    patientData.LastName = firstAndLast[0];
                                                    var firstName = firstAndLast[1];
                                                    if (firstName.IsNotNullOrEmpty())
                                                    {
                                                        var firstAndMI = firstName.Trim().Split(' ');
                                                        if (firstAndMI != null)
                                                        {
                                                            if (firstAndMI.Length > 1)
                                                            {
                                                                patientData.FirstName = firstAndMI[0];
                                                                patientData.MiddleInitial = firstAndMI[1];
                                                            }
                                                            else if (firstAndMI.Length == 1)
                                                            {
                                                                patientData.FirstName = firstAndMI[0];
                                                            }
                                                        }
                                                        else
                                                        {
                                                            patientData.FirstName = firstName;
                                                        }
                                                    }

                                                   
                                                }
                                            }
                                           
                                            if (dataRow.GetValue(1).IsNotNullOrEmpty() && dataRow.GetValue(1).Length >= 1)
                                            {
                                                patientData.Gender = dataRow.GetValue(1).ToUpper().StartsWith("F") ? "Female" : dataRow.GetValue(1).ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                            }
                                            patientData.PatientIdNumber = dataRow.GetValue(2);
                                            patientData.MedicareNumber = dataRow.GetValue(3);
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = dataRow.GetValue(4).IsValidDate() ? dataRow.GetValue(4).ToDateTime() : DateTime.MinValue;  //DateTime.FromOADate(double.Parse(dataRow.GetValue(4)));
                                            }
                                            patientData.SSN = dataRow.GetValue(5).Replace(" ", "");
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = dataRow.GetValue(6).IsValidDate() ? dataRow.GetValue(6).ToDateTime() : DateTime.MinValue;//DateTime.FromOADate(double.Parse(dataRow.GetValue(6)));
                                            }
                                            var maritalStatus=dataRow.GetValue(8);
                                            if (maritalStatus.IsNotNullOrEmpty())
                                            {
                                                if (maritalStatus.ToLower().Contains("m"))
                                                {
                                                    patientData.MaritalStatus = "Married";
                                                }
                                                else if (maritalStatus.ToLower().Contains("s"))
                                                {
                                                    patientData.MaritalStatus = "Single";
                                                }
                                                else if (maritalStatus.ToLower().Contains("w"))
                                                {
                                                    patientData.MaritalStatus = "Widowed";
                                                }
                                                else if (maritalStatus.ToLower().Contains("d"))
                                                {
                                                    patientData.MaritalStatus = "Divorced";
                                                }
                                                else
                                                {
                                                    patientData.MaritalStatus = "Unknown";
                                                }
                                            }
                                            else
                                            {
                                                patientData.MaritalStatus = "Unknown";
                                            }
                                           

                                            patientData.AddressLine1 = dataRow.GetValue(9);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(10);
                                            patientData.AddressStateCode = dataRow.GetValue(11).IsNotNullOrEmpty() ? dataRow.GetValue(11).ToUpper().Replace(".", "") : string.Empty;
                                            patientData.AddressZipCode = dataRow.GetValue(12);
                                            patientData.PhoneHome = dataRow.GetValue(13).IsNotNullOrEmpty() ? dataRow.GetValue(13).Replace("-", "").ToPhoneDB() : string.Empty;
                                            patientData.PrimaryInsurance = "1";

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(25);
                                                            var physician = Database.GetPhysician(npi, agencyId);
                                                            if (physician == null)
                                                            {
                                                                exists = false;
                                                                var info = Database.GetNpiData(npi);
                                                                if (info != null)
                                                                {
                                                                    physician = new AgencyPhysician
                                                                    {
                                                                        Id = Guid.NewGuid(),
                                                                        AgencyId = agencyId,
                                                                        NPI = npi,
                                                                        LoginId = Guid.Empty,
                                                                        FirstName = dataRow.GetValue(14).IsNotNullOrEmpty()?dataRow.GetValue(14):info.ProviderFirstName,
                                                                        LastName = dataRow.GetValue(15).IsNotNullOrEmpty()?dataRow.GetValue(15):info.ProviderLastName,
                                                                        AddressLine1 = dataRow.GetValue(17),
                                                                        AddressLine2 = dataRow.GetValue(18),
                                                                        AddressCity = dataRow.GetValue(19),
                                                                        AddressStateCode = dataRow.GetValue(20).IsNotNullOrEmpty() ? dataRow.GetValue(20).ToUpperCase().Replace(".", "") : string.Empty,
                                                                        AddressZipCode = dataRow.GetValue(21),
                                                                        PhoneWork = dataRow.GetValue(23).ToPhoneDB(),
                                                                        FaxNumber = dataRow.GetValue(24).ToPhoneDB(),
                                                                        Credentials = dataRow.GetValue(16)
                                                                    };
                                                                }

                                                                Database.Add(physician);
                                                            }

                                                            if (physician != null)
                                                            {
                                                                var patientPhysician = new PatientPhysician
                                                                {
                                                                    IsPrimary = true,
                                                                    PatientId = patientData.Id,
                                                                    PhysicianId = physician.Id
                                                                };

                                                                if (Database.Add(patientPhysician))
                                                                {
                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}