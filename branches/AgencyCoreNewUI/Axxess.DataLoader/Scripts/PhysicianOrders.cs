﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public static class PhysicianOrders
    {
        public static void Run()
        {
            var i = 1;
            var orders = Database.GetOrders();
            if (orders != null && orders.Count > 0)
            {
                Console.WriteLine("Order List Count: {0}", orders.Count);
                orders.ForEach(order =>
                {
                    if (order.SignatureText.IsNotNullOrEmpty() && !order.SignatureText.StartsWith("Electronically Signed"))
                    {
                        Console.WriteLine("{0}) {1}", i, order.SignatureText);
                        var user = Database.GetUser(order.UserId, order.AgencyId);
                        if (user != null && user.DisplayName.IsNotNullOrEmpty())
                        {
                            var signature = string.Format("Electronically Signed by: {0}", user.DisplayName.Replace(" [deleted]", ""));
                            order.SignatureText = signature;
                            if (Database.Update<PhysicianOrder>(order))
                            {
                                Console.WriteLine("{0}) {1}", i, signature);
                                i++;
                            }
                        }
                        else
                        {
                            Thread.Sleep(2000);
                        }
                    }
                });
            }
        }
    }
}
