﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    public static class SponsorScript
    {
        private static Sheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\SponsorExhibitorMergd.txt");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Sponsors_{0}.xls", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            Initialize();

            using (var fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    int i = 1;
                    int dataCounter = 0;
                    int blankSpaceCounter = 0;
                    Row excelRow = null;
                    Sponsor sponsor = null;
                    while (streamReader.Peek() >= 0)
                    {
                        var line = streamReader.ReadLine();
                        if (line.IsNotNullOrEmpty() && !line.IsEqual(" "))
                        {
                            dataCounter++;
                            if (dataCounter == 1)
                            {
                                if (sponsor != null)
                                {
                                    i++;
                                }

                                sponsor = new Sponsor { };
                                sponsor.Name = line;
                                excelRow = sheet.CreateRow(i);
                                excelRow.CreateCell(0).SetCellValue(line);
                            }
                            else if (dataCounter == 2)
                            {
                                sponsor.Contact = line;
                                excelRow.CreateCell(1).SetCellValue(line);
                            }
                            else if (dataCounter == 3)
                            {
                                sponsor.AddressLine1 = line;
                                excelRow.CreateCell(2).SetCellValue(line);
                            }
                            else if (dataCounter == 4)
                            {
                                sponsor.AddressLine2 = line;
                                excelRow.CreateCell(3).SetCellValue(line);
                            }
                            else if (dataCounter == 5)
                            {
                                sponsor.Phone = line;
                                excelRow.CreateCell(4).SetCellValue(line);
                            }
                            else if (dataCounter == 6)
                            {
                                sponsor.EmailAddress = line;
                                excelRow.CreateCell(5).SetCellValue(line);
                            }
                            blankSpaceCounter = 0;
                        }
                        else
                        {
                            blankSpaceCounter++;
                            if (blankSpaceCounter >= 2)
                            {
                                dataCounter = 0;
                            }
                        }
                    }
                }
                Write();
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("Sponsor List");

            Font headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Company Name");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(1).SetCellValue("Contact Name");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(2).SetCellValue("Address Line 1");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(3).SetCellValue("Address Line 2");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(4).SetCellValue("Phone Number");
            headerRow.RowStyle = headerStyle;

            headerRow.CreateCell(5).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);

            using (FileStream fileStream = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }

    public class Sponsor
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
    }
}
