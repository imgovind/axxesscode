﻿using System;
using System.IO;
using System.Data;

using Excel;

using Axxess.Core.Extension;

using Axxess.AgencyManagement.Enums;
using Axxess.AgencyManagement.Domain;

namespace Axxess.DataLoader.Domain
{
    public class VestaScript
    {
        private static string input = Path.Combine(App.Root, "Files\\AbilityHomeCare.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AbilityHomeCare_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            patientData.LastName = dataRow.GetValue(1);
                                            patientData.FirstName = dataRow.GetValue(2);
                                            patientData.SSN = dataRow.GetValue(3);
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(4)));
                                            }

                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.Gender = dataRow.GetValue(5).ToUpper().StartsWith("F") ? "Female" : dataRow.GetValue(5).ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                            }
                                            patientData.PhoneHome = dataRow.GetValue(6).IsNotNullOrEmpty() ? dataRow.GetValue(6).Replace("-", "").ToPhoneDB() : string.Empty;
                                            patientData.AddressLine1 = dataRow.GetValue(7);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(8);
                                            patientData.AddressStateCode = dataRow.GetValue(9).IsNotNullOrEmpty() ? dataRow.GetValue(9).ToUpper().Replace(".", "") : string.Empty;
                                            patientData.AddressZipCode = dataRow.GetValue(10);
                                            patientData.PrimaryInsurance = "1";

                                            var status = dataRow.GetValue(14);
                                            if (status.IsNotNullOrEmpty())
                                            {
                                                if (status.Trim().IsEqual("ACTIVE"))
                                                {
                                                    patientData.Status = 1;
                                                }
                                                else if (status.Trim().IsEqual("TERMINATED"))
                                                {
                                                    patientData.Status = 2;
                                                }
                                                else
                                                {
                                                    patientData.Status = 1;
                                                }
                                            }
                                            else
                                            {
                                                patientData.Status = 1;
                                            }


                                            if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                            {
                                                var statusDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(15)));
                                                if (patientData.Status == 1)
                                                {
                                                    patientData.StartofCareDate = statusDate;
                                                }
                                                else if (patientData.Status == 2)
                                                {
                                                    patientData.DischargeDate = statusDate;
                                                }
                                            }

                                            patientData.MedicareNumber = dataRow.GetValue(16);

                                            patientData.MaritalStatus = "Unknown";

                                            patientData.Comments = string.Format("Physician Name : {0} , Physician Phone : {1} , Physician Address: {2}", dataRow.GetValue(11), dataRow.GetValue(12), dataRow.GetValue(13));
                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
