﻿using System;
using System.IO;
using System.Data;

using Excel;

using Axxess.Core.Extension;

using Axxess.AgencyManagement.Enums;
using Axxess.AgencyManagement.Domain;

namespace Axxess.DataLoader.Domain
{
    partial class VestaPhysicianScript
    {
        private static string input = Path.Combine(App.Root, "Files\\AbilityHomeCarePhysician.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AbilityHomeCarePhysician_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var exists = true;
                                            var npi = dataRow.GetValue(11);
                                            if (npi.IsNotNullOrEmpty())
                                            {
                                                var physician = Database.GetPhysician(npi, agencyId);
                                                if (physician == null)
                                                {
                                                    exists = false;
                                                    var info = Database.GetNpiData(npi);
                                                    if (info != null)
                                                    {
                                                        physician = new AgencyPhysician
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            NPI = npi,
                                                            LoginId = Guid.Empty,
                                                            FirstName = dataRow.GetValue(9).IsNotNullOrEmpty() ? dataRow.GetValue(9) : info.ProviderFirstName,
                                                            LastName = dataRow.GetValue(8).IsNotNullOrEmpty() ? dataRow.GetValue(8) : info.ProviderLastName,
                                                            AddressLine1 = dataRow.GetValue(1),
                                                            AddressLine2 = "",
                                                            AddressCity = dataRow.GetValue(2),
                                                            AddressStateCode = dataRow.GetValue(3).IsNotNullOrEmpty() ? dataRow.GetValue(3).ToUpperCase().Replace(".", "") : string.Empty,
                                                            AddressZipCode = dataRow.GetValue(4),
                                                            PhoneWork = dataRow.GetValue(5).ToPhoneDB(),
                                                            FaxNumber = dataRow.GetValue(7).ToPhoneDB(),
                                                            Credentials = dataRow.GetValue(10)

                                                        };
                                                        if (Database.Add(physician))
                                                        {
                                                            Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, "");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("{0}) {1} {2}", i, dataRow.GetValue(8) + "," + dataRow.GetValue(9), "Physicain  NPI not found.");
                                                    }

                                                }
                                                else
                                                {
                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, "ALREADY EXISTS");
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} {2}", i, dataRow.GetValue(8) + "," + dataRow.GetValue(9), "NPI is empty.");
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
