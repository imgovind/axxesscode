﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class HealthCareFirstScript
    {
        private static string input = Path.Combine(App.Root, "Files\\marshall.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\marshall_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.PatientIdNumber = dataRow.GetValue(2);
                                            patientData.Gender = "";
                                            
                                            var nameArray = dataRow.GetValue(6).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                patientData.LastName = nameArray[0].Trim();
                                                var firstNameArray = nameArray[1].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (firstNameArray != null && firstNameArray.Length == 2)
                                                {
                                                    patientData.FirstName = firstNameArray[0].Trim();
                                                    patientData.MiddleInitial = firstNameArray[1].Trim();
                                                }
                                                else
                                                {
                                                    patientData.FirstName = nameArray[1].Trim();
                                                }
                                            }
                                            if (dataRow.GetValue(12).IsNotNullOrEmpty() && dataRow.GetValue(12).IsDate())
                                            {
                                                patientData.StartofCareDate = dataRow.GetValue(12).ToDateTime();
                                            }

                                            if (dataRow.GetValue(13).IsNotNullOrEmpty())
                                            {
                                                var addressArray = dataRow.GetValue(13).Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                if (addressArray != null && addressArray.Length > 1)
                                                {
                                                    patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                    patientData.AddressLine2 = "";

                                                    var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (locationArray != null && locationArray.Length > 2)
                                                    {
                                                        locationArray = locationArray.Reverse();
                                                        patientData.AddressZipCode = locationArray[0];
                                                        patientData.AddressStateCode = "TX";

                                                        if (locationArray.Length > 3)
                                                        {
                                                            int cityNameCount = 2;
                                                            var city = new string[locationArray.Length - cityNameCount];

                                                            do
                                                            {
                                                                city[cityNameCount - 2] = locationArray[cityNameCount];
                                                                cityNameCount++;
                                                            } while (cityNameCount < locationArray.Length);

                                                            if (city.Length > 0)
                                                            {
                                                                city = city.Reverse();
                                                                city.ForEach(name =>
                                                                {
                                                                    patientData.AddressCity += string.Format("{0} ", name.Replace(",", ""));
                                                                });
                                                                patientData.AddressCity = patientData.AddressCity.Trim();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            patientData.AddressCity = locationArray[2].Replace(",", "");
                                                        }
                                                    }
                                                }
                                            }
                                            if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                            {
                                                patientData.PhoneHome = dataRow.GetValue(19).ToPhoneDB();
                                            }
                                            if (dataRow.GetValue(21).IsNotNullOrEmpty())
                                            {
                                                patientData.MedicareNumber = dataRow.GetValue(21);
                                            }
                                            if (dataRow.GetValue(22).IsNotNullOrEmpty() && dataRow.GetValue(22).IsDate())
                                            {
                                                patientData.DOB = dataRow.GetValue(22).ToDateTime();
                                            }

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };


                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
