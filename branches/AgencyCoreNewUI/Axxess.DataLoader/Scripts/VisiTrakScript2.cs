﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using HtmlAgilityPack;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class VisiTrakScript2
    {
        private static string input = Path.Combine(App.Root, "Files\\Raphacare.HTM");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Raphacare_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.Load(input);

                    int i = 1;
                    int counter = 1;
                    Patient patientData = null;
                    htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                    {
                        if (htmlNode.Name.IsEqual("html"))
                        {
                            htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                            {
                                if (bodyNode.Name.IsEqual("body"))
                                {
                                    bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                    {
                                        if (tableNode.DoesHtmlRowHaveDataSecond())
                                        {
                                            var tbody = tableNode.SelectSingleNode("tbody");
                                            if (tbody != null)
                                            {
                                                int dataCounter = 1;
                                                if (counter % 3 == 1)
                                                {
                                                    // Table 1
                                                    patientData = new Patient();
                                                    patientData.Id = Guid.NewGuid();
                                                    patientData.AgencyId = agencyId;
                                                    patientData.AgencyLocationId = locationId;
                                                    patientData.Status = 1;
                                                    patientData.Ethnicities = string.Empty;
                                                    patientData.MaritalStatus = string.Empty;
                                                    patientData.IsDeprecated = false;
                                                    patientData.IsHospitalized = false;
                                                    patientData.Status = 1;
                                                    patientData.Gender = "";

                                                    tbody.ChildNodes.ToList().ForEach(tRowNode =>
                                                    {
                                                        if (tRowNode.Name.IsEqual("tr"))
                                                        {
                                                            tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                            {
                                                                if (dataNode.Name.IsEqual("td"))
                                                                {
                                                                    dataNode.SetVisiTrakPatient(1, dataCounter, patientData);
                                                                    dataCounter++;
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                else
                                                {
                                                    if (counter % 3 == 0)
                                                    {
                                                        // Table 3
                                                        tbody.ChildNodes.ToList().ForEach(tRowNode =>
                                                        {
                                                            if (tRowNode.Name.IsEqual("tr"))
                                                            {
                                                                tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                                {
                                                                    if (dataNode.Name.IsEqual("td"))
                                                                    {
                                                                        dataNode.SetVisiTrakPatient(3, dataCounter, patientData);
                                                                        dataCounter++;
                                                                    }
                                                                });
                                                            }
                                                        });

                                                        patientData.Comments += patientData.Comments.Trim();

                                                        patientData.Created = DateTime.Now;
                                                        patientData.Modified = DateTime.Now;

                                                        var medicationProfile = new MedicationProfile
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            PatientId = patientData.Id,
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            Medication = "<ArrayOfMedication />"
                                                        };

                                                        var allergyProfile = new AllergyProfile
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            PatientId = patientData.Id,
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            Allergies = "<ArrayOfAllergy />"
                                                        };

                                                        if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                        {
                                                            var admissionPeriod = new PatientAdmissionDate
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = agencyId,
                                                                Created = DateTime.Now,
                                                                DischargedDate = DateTime.MinValue,
                                                                IsActive = true,
                                                                IsDeprecated = false,
                                                                Modified = DateTime.Now,
                                                                PatientData = patientData.ToXml().Replace("'", ""),
                                                                PatientId = patientData.Id,
                                                                Reason = string.Empty,
                                                                StartOfCareDate = patientData.StartofCareDate,
                                                                Status = patientData.Status
                                                            };
                                                            if (Database.Add(admissionPeriod))
                                                            {
                                                                var patient = Database.GetPatient(patientData.Id, agencyId);
                                                                if (patient != null)
                                                                {
                                                                    patient.AdmissionId = admissionPeriod.Id;
                                                                    if (Database.Update(patient))
                                                                    {
                                                                        Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        i++;
                                                    }
                                                    else
                                                    {
                                                        // Table 2
                                                        tbody.ChildNodes.ToList().ForEach(tRowNode =>
                                                        {
                                                            if (tRowNode.Name.IsEqual("tr"))
                                                            {
                                                                tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                                {
                                                                    if (dataNode.Name.IsEqual("td"))
                                                                    {
                                                                        dataNode.SetVisiTrakPatient(2, dataCounter, patientData);
                                                                        dataCounter++;
                                                                    }
                                                                });
                                                            }
                                                        });

                                                        Console.WriteLine("Table {0}", counter);
                                                    }
                                                }
                                                counter++;
                                            }
                                        }
                                    });

                                }
                            });
                        }
                    });
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
