﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Kent.Boogaart.KBCsv;

    public static class AxxessScript5
    {
        private static string input = Path.Combine(App.Root, "Files\\CommunityHHL1.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\CommunityHHL1_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        //using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        using (var excelReader = new CsvReader(fileStream))
                        {
                            if (excelReader != null)
                            {
                                // excelReader.IsFirstRowAsColumnNames = true;
                                excelReader.ReadHeaderRecord();
                                // DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                //if (dataRows != null && dataRows.cou > 0)
                                //{
                                var i = 1;
                                foreach (var dataRow in excelReader.DataRecords)
                                {
                                    //if (!dataRow.IsEmpty())
                                    //{
                                    var patientData = new Patient();
                                    patientData.Id = Guid.NewGuid();
                                    patientData.AgencyId = agencyId;
                                    patientData.AgencyLocationId = locationId;
                                    patientData.Ethnicities = string.Empty;
                                    patientData.MaritalStatus = string.Empty;
                                    patientData.IsDeprecated = false;
                                    patientData.IsHospitalized = false;
                                    patientData.Status = 1;
                                    patientData.FirstName = dataRow.GetValue(0);
                                    patientData.LastName = dataRow.GetValue(1);
                                    if (dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(2).Length >= 1)
                                    {
                                        patientData.Gender = dataRow.GetValue(2).ToUpper().StartsWith("F") ? "Female" : dataRow.GetValue(2).ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                    }
                                    patientData.PatientIdNumber = dataRow.GetValue(3);
                                    patientData.MedicareNumber = dataRow.GetValue(4);
                                    patientData.MedicaidNumber = dataRow.GetValue(24);
                                    if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                    {
                                        patientData.DOB = dataRow.GetValue(5).IsValidDate() ? dataRow.GetValue(5).ToDateTime() : DateTime.MinValue; //DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                    }
                                    patientData.SSN = dataRow.GetValue(6).Replace(" ", "");
                                    if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                    {
                                        patientData.StartofCareDate = dataRow.GetValue(7).IsValidDate() ? dataRow.GetValue(7).ToDateTime() : DateTime.MinValue;// DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                    }

                                    var maritalStatus = dataRow.GetValue(9);
                                    if (maritalStatus.IsNotNullOrEmpty())
                                    {
                                        maritalStatus = maritalStatus.ToUpperCase();
                                        if (maritalStatus.StartsWith("M"))
                                        {
                                            patientData.MaritalStatus = "Married";
                                        }
                                        else if (maritalStatus.StartsWith("S"))
                                        {
                                            patientData.MaritalStatus = "Single";
                                        }
                                        else if (maritalStatus.StartsWith("W"))
                                        {
                                            patientData.MaritalStatus = "Widowed";
                                        }
                                        else if (maritalStatus.StartsWith("D"))
                                        {
                                            patientData.MaritalStatus = "Divorced";
                                        }
                                        else
                                        {
                                            patientData.MaritalStatus = "Unknown";
                                        }
                                    }
                                    else
                                    {
                                        patientData.MaritalStatus = "Unknown";
                                    }

                                    patientData.AddressLine1 = dataRow.GetValue(10);
                                    patientData.AddressCity = dataRow.GetValue(11);
                                    patientData.AddressStateCode = dataRow.GetValue(12).ToUpper().Replace(".", "");
                                    patientData.AddressZipCode = dataRow.GetValue(13);
                                    patientData.PhoneHome = dataRow.GetValue(14).IsNotNullOrEmpty() ? dataRow.GetValue(14).Replace(")", "").Replace("(", "").Replace(".", "").Replace("-", "").Replace(" ", "").ToPhoneDB() : string.Empty;
                                    patientData.PrimaryInsurance = "1";
                                    patientData.Comments = string.Format("Episode Start Date: {0}", dataRow.GetValue(8).IsValidDate() ? dataRow.GetValue(8).ToDateTime().ToString("MM/dd/yyyy") : string.Empty);//dataRow.GetValue(8).IsNotNullOrEmpty() ? DateTime.FromOADate(double.Parse(dataRow.GetValue(8))).ToString("MM/dd/yyyy") : string.Empty
                                    patientData.Created = DateTime.Now;
                                    patientData.Modified = DateTime.Now;

                                    var medicationProfile = new MedicationProfile
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Medication = "<ArrayOfMedication />"
                                    };

                                    var allergyProfile = new AllergyProfile
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Allergies = "<ArrayOfAllergy />"
                                    };

                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                    {
                                        var admissionPeriod = new PatientAdmissionDate
                                        {
                                            Id = Guid.NewGuid(),
                                            AgencyId = agencyId,
                                            Created = DateTime.Now,
                                            DischargedDate = DateTime.MinValue,
                                            IsActive = true,
                                            IsDeprecated = false,
                                            Modified = DateTime.Now,
                                            PatientData = patientData.ToXml().Replace("'", ""),
                                            PatientId = patientData.Id,
                                            Reason = string.Empty,
                                            StartOfCareDate = patientData.StartofCareDate,
                                            Status = patientData.Status
                                        };
                                        if (Database.Add(admissionPeriod))
                                        {
                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                            if (patient != null)
                                            {
                                                patient.AdmissionId = admissionPeriod.Id;
                                                if (Database.Update(patient))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                    var exists = true;
                                                    var npi = dataRow.GetValue(23);
                                                    var physician = Database.GetPhysician(npi, agencyId);
                                                    if (physician == null)
                                                    {
                                                        exists = false;
                                                        var info = Database.GetNpiData(npi);
                                                        if (info != null)
                                                        {
                                                            physician = new AgencyPhysician
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = agencyId,
                                                                NPI = npi,
                                                                LoginId = Guid.Empty,
                                                                LastName = dataRow.GetValue(15).IsNotNullOrEmpty() ? dataRow.GetValue(15) : info.ProviderLastName,
                                                                FirstName = dataRow.GetValue(16).IsNotNullOrEmpty() ? dataRow.GetValue(16) : info.ProviderFirstName,
                                                                AddressLine1 = dataRow.GetValue(17),
                                                                AddressCity = dataRow.GetValue(18),
                                                                AddressStateCode = dataRow.GetValue(19).IsNotNullOrEmpty() ? dataRow.GetValue(19).Replace(".", "") : string.Empty,
                                                                AddressZipCode = dataRow.GetValue(20),
                                                                PhoneWork = dataRow.GetValue(21).IsNotNullOrEmpty() ? dataRow.GetValue(21).Replace(" ", "").Replace(")", "").Replace("(", "").Replace(".", "").Replace("-", "").ToPhoneDB() : string.Empty,
                                                                FaxNumber = dataRow.GetValue(22).IsNotNullOrEmpty() ? dataRow.GetValue(22).Replace(" ", "").Replace(")", "").Replace("(", "").Replace(".", "").Replace("-", "").ToPhoneDB() : string.Empty

                                                                //Credentials = dataRow.GetValue(16)
                                                            };
                                                            //var physicianNameArray = dataRow.GetValue(15).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                            //if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                            //{
                                                            //    if (physicianNameArray.Length == 3)
                                                            //    {
                                                            //        physician.LastName = physicianNameArray[2].IsNotNullOrEmpty() ? physicianNameArray[2].Trim() : info.ProviderLastName;
                                                            //        physician.MiddleName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderMiddleName;
                                                            //        physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderLastName;
                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderLastName;
                                                            //        physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderLastName;
                                                            //    }

                                                            //}
                                                        }

                                                        Database.Add(physician);
                                                    }

                                                    if (physician != null)
                                                    {
                                                        var patientPhysician = new PatientPhysician
                                                        {
                                                            IsPrimary = true,
                                                            PatientId = patientData.Id,
                                                            PhysicianId = physician.Id
                                                        };

                                                        if (Database.Add(patientPhysician))
                                                        {
                                                            Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    i++;
                                    //  }
                                }
                                //}
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}