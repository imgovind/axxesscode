﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.OasisC.Domain;

    public static class OasisSubmissionIdScript
    {
        public static void Run(Guid agencyId, List<string> wrongFacilityIds, string newFacilityId)
        {
            var socList = Database.GetSocByAgencyId(agencyId);
            if (socList != null && socList.Count > 0)
            {
                Console.WriteLine("SOC List Count: {0}", socList.Count);
                socList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<StartOfCareAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }
            var rocList = Database.GetRocByAgencyId(agencyId);
            if (rocList != null && rocList.Count > 0)
            {
                Console.WriteLine("ROC List Count: {0}", rocList.Count);
                rocList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<ResumptionofCareAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }
            var recertList = Database.GetRecertByAgencyId(agencyId);
            if (recertList != null && recertList.Count > 0)
            {
                Console.WriteLine("Recert List Count: {0}", recertList.Count);
                recertList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<RecertificationAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }
            var followupList = Database.GetFollowupByAgencyId(agencyId);
            if (followupList != null && followupList.Count > 0)
            {
                Console.WriteLine("FollowUp List Count: {0}", followupList.Count);
                followupList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<FollowUpAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }
            var deathList = Database.GetDeathatHomeByAgencyId(agencyId);
            if (deathList != null && deathList.Count > 0)
            {
                Console.WriteLine("Death List Count: {0}", deathList.Count);
                deathList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<DeathAtHomeAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }

            var dischargeList = Database.GetDischargeByAgencyId(agencyId);
            if (dischargeList != null && dischargeList.Count > 0)
            {
                Console.WriteLine("Discharge List Count: {0}", dischargeList.Count);
                dischargeList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<DischargeFromAgencyAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }

            var transferList = Database.GetTransferByAgencyId(agencyId);
            if (transferList != null && transferList.Count > 0)
            {
                Console.WriteLine("Transfer List Count: {0}", transferList.Count);
                transferList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<TransferDischargeAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }

            var transferNotList = Database.GetTransferNotByAgencyId(agencyId);
            if (transferNotList != null && transferNotList.Count > 0)
            {
                Console.WriteLine("TransferNot List Count: {0}", transferNotList.Count);
                transferNotList.ForEach(a =>
                {
                    var result = ReplaceWrongFacilityId(a.SubmissionFormat, wrongFacilityIds, newFacilityId);

                    if (!result.IsEqual(a.SubmissionFormat))
                    {
                        a.SubmissionFormat = result;
                        Database.UpdateForOasisC<TransferNotDischargedAssessment>(a);
                    }
                    else
                    {
                        Console.WriteLine("Nothing to change");
                    }
                });
            }
        }

        private static string ReplaceWrongFacilityId(string submissionFormat, List<string> wrongFacilityIds, string newFacilityId)
        {
            var result = submissionFormat;
            if (submissionFormat.IsNotNullOrEmpty() && submissionFormat.Length == 1446)
            {
                wrongFacilityIds.ForEach(wrongId =>
                {
                    if (submissionFormat.Contains(wrongId))
                    {
                        Console.WriteLine("Wrong Id: {0}", wrongId);
                        if (wrongId.Length == newFacilityId.Length)
                        {
                            result = submissionFormat.Replace(wrongId, newFacilityId);
                        }
                        else if (wrongId.Length > newFacilityId.Length)
                        {
                            var diff = wrongId.Length - newFacilityId.Length;
                            var total = newFacilityId.Length + diff;
                            result = submissionFormat.Replace(wrongId, newFacilityId.PadRight(total, ' '));
                        }
                        else if (wrongId.Length < newFacilityId.Length)
                        {
                            var diff = newFacilityId.Length - wrongId.Length;
                            var total = wrongId.Length + diff;
                            result = submissionFormat.Replace(wrongId.PadRight(total, ' '), newFacilityId);
                        }

                        Console.WriteLine("Result Length == SubmissionFormat : {0}", result.Length == submissionFormat.Length);
                        Console.WriteLine("Result Lenght: {0}", result.Length);
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("This Wrong Id not Found.");
                        Console.WriteLine();
                    }
                });
            }
            return result;
        }
    }
}
