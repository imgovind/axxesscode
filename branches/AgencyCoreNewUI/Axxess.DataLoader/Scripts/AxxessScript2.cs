﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AxxessScript2
    {
        private static string input = Path.Combine(App.Root, "Files\\SASHHA.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\SASHHA_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.FirstName = dataRow.GetValue(0);
                                            patientData.LastName = dataRow.GetValue(1);
                                            if (dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(2).Length >= 1)
                                            {
                                                patientData.Gender = dataRow.GetValue(2).ToUpper().StartsWith("F") ? "Female" : dataRow.GetValue(2).ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                            }
                                            patientData.PatientIdNumber = dataRow.GetValue(3);
                                            patientData.MedicareNumber = dataRow.GetValue(4);

                                            if (dataRow.GetValue(5).IsNotNullOrEmpty() && dataRow.GetValue(5).IsDouble())
                                            {
                                                patientData.DischargeDate = DateTime.FromOADate(dataRow.GetValue(5).ToDouble()); // patientData.DischargeDate = dataRow.GetValue(5).Trim().IsValidDate() ? dataRow.GetValue(5).Trim().ToDateTime() : DateTime.MinValue; 
                                            }
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty() && dataRow.GetValue(6).IsDouble())
                                            {
                                                patientData.DOB = DateTime.FromOADate(dataRow.GetValue(6).ToDouble()); //patientData.DOB = dataRow.GetValue(6).Trim().IsValidDate() ? dataRow.GetValue(6).Trim().ToDateTime() : DateTime.MinValue;
                                            }
                                           
                                            patientData.SSN = dataRow.GetValue(7).Replace(" ", "");

                                            if (dataRow.GetValue(8).IsNotNullOrEmpty() && dataRow.GetValue(8).IsDouble())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(dataRow.GetValue(8).ToDouble()); //patientData.StartofCareDate = dataRow.GetValue(8).Trim().IsValidDate() ? dataRow.GetValue(8).Trim().ToDateTime() : DateTime.MinValue;
                                            }
                                            var martialStatus = dataRow.GetValue(10);
                                            if (martialStatus.IsNotNullOrEmpty())
                                            {
                                                if (martialStatus.StartsWith("M",StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    patientData.MaritalStatus = "Married";
                                                }
                                                else if (martialStatus.StartsWith("S", StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    patientData.MaritalStatus = "Single";
                                                }
                                                else if (martialStatus.StartsWith("W", StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    patientData.MaritalStatus = "Widowed";
                                                }
                                                else if (martialStatus.StartsWith("D", StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    patientData.MaritalStatus = "Divorced";
                                                }
                                                else
                                                {
                                                    patientData.MaritalStatus = "Unknown";
                                                }
                                            }
                                            else
                                            {
                                                patientData.MaritalStatus = "Unknown";
                                            }
                                            patientData.AddressLine1 = dataRow.GetValue(11);
                                            patientData.AddressLine2 = "";
                                            patientData.AddressCity = dataRow.GetValue(12);
                                            patientData.AddressStateCode = "TX";// dataRow.GetValue(13).ToUpper().Replace(".", "");
                                            patientData.AddressZipCode = dataRow.GetValue(14);
                                            patientData.PhoneHome = dataRow.GetValue(15).ToPhoneDB();
                                            patientData.PrimaryInsurance = "1";
                                            if (dataRow.GetValue(9).IsNotNullOrEmpty() && dataRow.GetValue(9).IsDouble())
                                            {
                                                patientData.Comments = string.Format("Episode Start Date: {0}. ", patientData.StartofCareDate = DateTime.FromOADate(dataRow.GetValue(8).ToDouble()));//dataRow.GetValue(9).IsNotNullOrEmpty() && dataRow.GetValue(9).Trim().IsValidDate() ? dataRow.GetValue(9).Trim().ToDateTime().ToString("MM/dd/yyyy") : string.Empty);
                                            }

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                            var exists = true;
                                                            var npi = dataRow.GetValue(24);
                                                            var physician = Database.GetPhysician(npi, agencyId);
                                                            if (physician == null)
                                                            {
                                                                exists = false;
                                                                var info = Database.GetNpiData(npi);
                                                                if (info != null)
                                                                {
                                                                    physician = new AgencyPhysician
                                                                    {
                                                                        Id = Guid.NewGuid(),
                                                                        AgencyId = agencyId,
                                                                        NPI = npi,
                                                                        LoginId = Guid.Empty,
                                                                        AddressLine1 = dataRow.GetValue(18),
                                                                        AddressCity = dataRow.GetValue(19),
                                                                        AddressStateCode = "TX",// dataRow.GetValue(20).Replace(".", ""),
                                                                        AddressZipCode = dataRow.GetValue(21),
                                                                        PhoneWork = dataRow.GetValue(22).ToPhoneDB(),
                                                                        FaxNumber = dataRow.GetValue(23).ToPhoneDB(),
                                                                        Credentials = dataRow.GetValue(17)
                                                                    };
                                                                    var physicianNameArray = dataRow.GetValue(16).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                    if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                                    {
                                                                        physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim() : info.ProviderLastName;
                                                                        physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                    }
                                                                }

                                                                Database.Add(physician);
                                                            }

                                                            if (physician != null)
                                                            {
                                                                var patientPhysician = new PatientPhysician
                                                                {
                                                                    IsPrimary = true,
                                                                    PatientId = patientData.Id,
                                                                    PhysicianId = physician.Id
                                                                };

                                                                if (Database.Add(patientPhysician))
                                                                {
                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
