﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    public class StateWageIndex
    {
        public string CBSA { get; set; }
        public string State { get; set; }
        public double WageIndex { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
            .AppendFormat("State: {0}", this.State).AppendLine()
            .AppendFormat("CBSA: {0}", this.CBSA).AppendLine()
            .AppendFormat("Wage Index: {0}", this.WageIndex)
            .AppendLine().AppendLine().ToString();
        }
    }
}
