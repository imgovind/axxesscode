﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;

    using Axxess.AgencyManagement.Domain;

    using Axxess.OasisC.Domain;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public static class Database
    {
       
        private static readonly SimpleRepository agencyManagementDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository oasisDatabase = new SimpleRepository("OasisCConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository lookupDatabase = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);

        internal static List<Agency> GetAgencies()
        {
            return agencyManagementDatabase.All<Agency>().ToList();
        }

        internal static Agency GetAgency(Guid agencyId)
        {
            return agencyManagementDatabase.Single<Agency>(a => a.Id == agencyId);
        }

        internal static AgencyLocation GetAgencyLocation(Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyLocation>(a => a.AgencyId == agencyId);
        }

        internal static List<PhysicianOrder> GetOrders()
        {
            return agencyManagementDatabase.All<PhysicianOrder>().ToList();
        }

        internal static Patient GetPatient(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static PatientAdmissionDate GetPatientAdminPeriod(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<PatientAdmissionDate>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static User GetUser(Guid userId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<User>(p => p.Id == userId && p.AgencyId == agencyId);
        }

        internal static List<User> GetUsers(Guid agencyId)
        {
            return agencyManagementDatabase.Find<User>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Rap> GetRaps(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Rap>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Final> GetFinals(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Final>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Patient> GetPatients(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Patient>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<AllergyProfile> GetAllergyProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AllergyProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<MedicationProfile> GetMedicationProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<MedicationProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PhysicianOrder> GetPhysicianOrders(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PhysicianOrder>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId)
        {
            return agencyManagementDatabase.Find<FaceToFaceEncounter>(p => p.AgencyId == agencyId).ToList();
        }

        internal static bool UpdatePatient(Patient patient)
        {
            if (patient != null)
            {
                agencyManagementDatabase.Update<Patient>(patient);
                return true;
            }
            return false;
        }

        internal static AgencyPhysician GetPhysician(string npi, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.NPI == npi && p.AgencyId == agencyId);
        }

        internal static List<AgencyPhysician> GetPhysicians(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyPhysician>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientEpisode> GetEpisodes(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientEpisode>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientAdmissionDate>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientVisitNote>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId, DateTime startTime, DateTime endTime)
        {
            return agencyManagementDatabase.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.Modified >= startTime && p.Modified <= endTime).OrderByDescending(p => p.Modified).ToList();
        }

        internal static Npi GetNpiData(string npi)
        {
            return lookupDatabase.Single<Npi>(p => p.Id == npi);
        }

        internal static List<StartOfCareAssessment> GetSocByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<StartOfCareAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<DeathAtHomeAssessment> GetDeathatHomeByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<DeathAtHomeAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<DischargeFromAgencyAssessment> GetDischargeByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<FollowUpAssessment> GetFollowupByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<FollowUpAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<RecertificationAssessment> GetRecertByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<RecertificationAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<ResumptionofCareAssessment> GetRocByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<ResumptionofCareAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<TransferDischargeAssessment> GetTransferByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<TransferDischargeAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<TransferNotDischargedAssessment> GetTransferNotByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<PlanofCare> GetPlanofCareByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<PlanofCare>(a => a.AgencyId == agencyId).ToList();
        }

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool AddForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForOasisC<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                oasisDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<CBSACode> GetCbsaCodes()
        {
            return lookupDatabase.All<CBSACode>().ToList();
        }

        internal static List<User> GetUsers()
        {
            return agencyManagementDatabase.All<User>().ToList();
        }

        internal static List<PatientEpisode> GetPatientEpisodes()
        {
            return agencyManagementDatabase.All<PatientEpisode>().ToList();
        }

        internal static void AddMany<T>(IEnumerable<T> items) where T : class, new()
        {
                agencyManagementDatabase.AddMany<T>(items);
        }
    }
}
