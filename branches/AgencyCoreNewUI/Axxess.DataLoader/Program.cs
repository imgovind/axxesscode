﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using StructureMap;

    using Axxess.Api;
    using Axxess.DataLoader.Domain;

    class Program
    {
        static void Main(string[] args)
        {
            ColoradoScript.Run();
            //ArizonaScript.Run();
            //MinnesotaScript.Run();
            //CaliforniaHHAScript.Run();
            //SponsorScript.Run();
            //var stopWatch = new Stopwatch();
            //stopWatch.Start();
            //CradleMedPointScript.Run(new Guid("5d50ae56-5e62-46cf-9b7f-40f5ca8202b3"), new Guid("4bab3fe9-51dc-4e90-9262-32575e505349"));
            //KinnserScript.Run(new Guid("ae7d7a23-5403-43a4-ba85-09128db82d94"), new Guid("7ff5f829-09e2-4cb9-bc7a-acefa8bf3ee4"));
            //KinnserCsvScript.Run(new Guid("37d54330-f914-451a-937d-ebe3b1d19bf4"), new Guid("12c407d0-b0e5-4c9c-aab0-386c36e4057d"));
            //AxxessScript.Run("985ed2d1-648c-44c1-902e-d7b9d2383bee", "4dada53a-f2e9-4b50-b144-ee8bf59edced");
            //MedicationProfileScript.Run(new Guid("6d80ae1f-410d-408f-b342-051e956b037f"));
            //GenericExcelScript.Run(new Guid("8bf20512-359a-4fe5-9438-966dfaa5a34c"), new Guid("8885e4f3-36ce-4a26-a08a-0b691e89ccbe"));
            //SynergyScript.Run(new Guid("855d3109-d693-4458-9ed7-ca44612502c2"), new Guid("cce3cbb8-2ddd-47a0-8828-5ee0852bfada"));
            //VisiTrakScript.Run(new Guid("e91737a9-d5ad-4428-a862-5452e2c72cda"), new Guid("dcabe675-af20-4d45-a6ad-e10ad8977fff"));
            //VisiTrakScript2.Run(new Guid("e91737a9-d5ad-4428-a862-5452e2c72cda"), new Guid("dcabe675-af20-4d45-a6ad-e10ad8977fff"));
            //VisiTrakPhysicianScript.Run(new Guid("e91737a9-d5ad-4428-a862-5452e2c72cda"));
            //SynergyTwoScript.Run(new Guid("86dcea98-c812-4e92-9ed0-7e1a72fe7a94"), new Guid("5149e056-21c7-4e5f-8209-cb16b870e774"));
            //SynergyPhysicianScript.Run(new Guid("42d3a029-b1cb-4608-b788-6255f6cea23d"));
            //HealthMedXPhysicianScript.Run(new Guid("abc2fbb2-807e-4dd0-987f-608e5d75558a"));
            //HealthCareFirstScript.Run(new Guid("e279f258-4622-47ed-9b3d-fb10b7b2f150"), new Guid("db30ed8f-5d92-49db-8029-d1f537ed38c1"));
            //Icd9Script.Run(true);
            //Icd9Script.Run(false);
            //GenericExcelTwoScript.Run(new Guid("4a318889-50b7-4ba2-90d2-bb259a663411"), new Guid("e4556fd3-e9e9-42e3-a801-06ddd9da77ee"));
            //WageIndexScript.Run();
            //CradleCsvScript.Run("32a40f1a-7eb9-4e9d-9759-5099468fe503", "dafec74c-2829-461e-b1e0-bd0824801508");
            //OasisSubmissionIdScript.Run(new Guid("0b175677-cefc-4c2d-8e94-7ee6c11bb2ab"), new List<string> { "HHA0006661" }, "HH630011067");
            //OasisSubmissionBranchIdScript.Run(new Guid("7d69982e-edd1-4ecc-a698-3440a3eb8e78"), 'P');
            //HHCenterScript.Run(new Guid("9d5afae5-6127-4c58-b457-c5185273ad4a"), new Guid("044ec64d-118e-43df-85f1-9c772a004c86"));
            //MjsOneScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"), new Guid("e8460caf-8a63-42b2-89e7-058bf9b8be15"));
            //MsjOnePhysicianScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"));
            //IgeaHomeHealthIntake.Run("5ccbf159-dc60-4b5a-a6aa-ade0ca60a1be", "80404746-3140-46c9-9ea8-4c68ee2bdb47");
            //PermissionScript.Run();
            //AgencySupplies.Run();
            //PhysicianOrders.Run();
            //AgencyMergeScript.Run("4da8bb1c-0b75-415c-862d-c36ff673619d", new List<string> { "949dfac1-8bdb-4097-868b-a7729cc991b4", "c65edc26-12be-43d1-b71c-5074d16213e8" });
            //stopWatch.Stop();
            //Console.WriteLine("Script ran for {0} hours {1} minutes and {2} seconds", stopWatch.Elapsed.Hours, stopWatch.Elapsed.Minutes, stopWatch.Elapsed.Seconds);
            //AxxessScript4.Run(new Guid("91ddde23-b364-4c96-bb4b-84a25b8fa1de"), new Guid("77df1f10-3e78-4c94-a490-a1bbc91ad977"));
            //int i = PatinetAddressScript.Run(new Guid("0bd5ffc9-61c1-495a-9df5-7b93885ab30d"));
           // AxxessScript3.Run(new Guid("2f59e587-a25d-4f0b-b34d-c620f92a0e0e"), new Guid("4067d4bc-c56b-4491-ab65-38fd7461b75e"));
            //AxxessScript2.Run(new Guid("b122a9d3-4a60-4f2d-877a-d24a2e2e9862"), new Guid("d71e2197-798e-4edd-8d78-5b6b3b19efa0"));
           // SchduleLoadScript.Run(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //SchduleLoadScript.Run(new Guid("d0307ff6-9d69-435a-a411-f737b1980bfb"));
            //SchduleLoadScript.Run(new Guid("86493a0e-0fc0-4260-998b-442dd17f1a6e"));
            
            Console.WriteLine("Script Complete");
            Console.ReadLine();
        }
    }
}
