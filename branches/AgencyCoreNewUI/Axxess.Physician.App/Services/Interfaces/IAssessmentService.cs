﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Collections.Generic;
    using Axxess.Physician.App.ViewData;

    using Axxess.OasisC.Domain;
    
    public interface IAssessmentService
    {
        PlanofCare GetPlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        IDictionary<string, Question> GetAllergies(Guid assessmentId, Guid agencyId);
        IDictionary<string, Question> GetDiagnoses(Guid assessmentId, Guid agencyId);
        bool UpdatePlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason);
        VisitNoteViewData GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
    }
}
