﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Domain;

    public interface IPhysicianService
    {
        List<Order> GetOrders();
        List<Order> GetOrdersCompleted();
        List<FaceToFaceEncounter> GetFaceToFaceEncounters();
        bool UpdatePhysicianOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string action, string reason);
        bool UpdateFaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string action, string reason);
        bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceOrder);
        bool UpdateEvalOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid evalId, string action, string reason);
    }
}
