﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement;

    using Axxess.Log.Common;
    using Axxess.Log.Enums;

    public class PhysicianService : IPhysicianService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPlanofCareRepository planofCareRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagmentDataProvider, IOasisCDataProvider oasisCDataProvider)
        {
            Check.Argument.IsNotNull(oasisCDataProvider, "oasisCDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.planofCareRepository = oasisCDataProvider.PlanofCareRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
        }
        
        #endregion

        #region IPhysicianService Members

        public List<FaceToFaceEncounter>GetFaceToFaceEncounters()
        {
            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncountersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSavedByPhysician);
            faceToFaceEncounter.ForEach(ffe =>
            {
                ffe.Patient = patientRepository.GetPatientOnly(ffe.PatientId, ffe.AgencyId);
                ffe.DisplayName = ffe.Patient != null ? ffe.Patient.DisplayName : string.Empty;
                ffe.Agency = AgencyEngine.Get(ffe.AgencyId);
                ffe.PrintUrl = Url.Print(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true);
            });
            return faceToFaceEncounter;
        }

        public List<Order> GetOrders()
        {
            var orders = new List<Order>();
            var physicianOrders = patientRepository.GetPhysicianOrdersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSentToPhysicianElectronically);
            physicianOrders.ForEach(po =>
            {
                var patient = patientRepository.GetPatientOnly(po.PatientId, po.AgencyId);
                orders.Add(new Order
                {
                    Id = po.Id,
                    Status = po.Status,
                    AgencyId = po.AgencyId,
                    Number = po.OrderNumber,
                    Type = OrderType.PhysicianOrder,
                    PrintUrl = Url.Print(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, false),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    AgencyName = AgencyEngine.Get(po.AgencyId).Name,
                    SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = po.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareOrders = planofCareRepository.GetByPhysicianId(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSentToPhysicianElectronically);
            planofCareOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, false),
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianId(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSentToPhysicianElectronically);
            planofCareStandAloneOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485StandAlone,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, false),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            //var evalOrders = patientRepository.GetEvalOrdersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.EvalSentToPhysicianElectronically);
            //evalOrders.ForEach(eval =>
            //{
            //    var patient = patientRepository.GetPatientOnly(eval.PatientId, eval.AgencyId);
            //    orders.Add(new Order
            //    {
            //        Id = eval.Id,
            //        Type = GetOrderType(eval.NoteType),
            //        //Text = GetDisciplineType(eval.NoteType).GetDescription(),
            //        Number = eval.OrderNumber,
            //        Status = eval.Status,
            //        AgencyId = eval.AgencyId,
            //        AgencyName = AgencyEngine.Get(eval.AgencyId).Name,
            //        PrintUrl = Url.Print(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, OrderType.HCFA485StandAlone, false),
            //        PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
            //        SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
            //        CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
            //        ReceivedDate = DateTime.Today
            //    });
            //});

            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetOrdersCompleted()
        {
            var orders = new List<Order>();

            var physicianOrders = patientRepository.GetPhysicianOrdersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            physicianOrders.ForEach(po =>
            {
                var patient = patientRepository.GetPatientOnly(po.PatientId, po.AgencyId);
                orders.Add(new Order
                {
                    Id = po.Id,
                    Status = po.Status,
                    AgencyId = po.AgencyId,
                    Number = po.OrderNumber,
                    Type = OrderType.PhysicianOrder,
                    PrintUrl = Url.View(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, true),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    AgencyName = AgencyEngine.Get(po.AgencyId).Name,
                    SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = po.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareOrders = planofCareRepository.GetByPhysicianId(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            planofCareOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, true),
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianId(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            planofCareStandAloneOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485StandAlone,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, true),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var facetofaceOrders = patientRepository.GetFaceToFaceEncountersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            facetofaceOrders.ForEach(ffe =>
            {
                var patient = patientRepository.GetPatientOnly(ffe.PatientId, ffe.AgencyId);
                orders.Add(new Order
                {
                    Id = ffe.Id,
                    Type = OrderType.FaceToFaceEncounter,
                    Number = ffe.OrderNumber,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    Status = ffe.Status,
                    AgencyId = ffe.AgencyId,
                    AgencyName = AgencyEngine.Get(ffe.AgencyId).Name,
                    PrintUrl = Url.View(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true),
                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                    ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                    SendDate = ffe.RequestDate
                });
            });

            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public bool UpdatePhysicianOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            bool isActionSet = false;
            var description = string.Empty;
            var action = new Actions();
            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(agencyId, patientId, episodeId, orderId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    var oldReason = scheduleEvent.ReturnReason;
                    var physicianOrder = patientRepository.GetOrderOnly(orderId, patientId, agencyId);
                    if (physicianOrder != null)
                    {
                        if (actionType == "Approve")
                        {
                            physicianOrder.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            physicianOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            physicianOrder.ReceivedDate = DateTime.Now;

                            scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            scheduleEvent.ReturnReason = string.Empty;

                            description = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            action = Actions.PhysicianApproved;
                            isActionSet = true;
                        }
                        else if (actionType == "Return")
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            physicianOrder.Modified = DateTime.Now;

                            scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedForClinicianReview;
                            scheduleEvent.ReturnReason = reason;

                            description = string.Format("Returned for review by: {0}", Current.DisplayName);
                            action = Actions.PhysicianReturned;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (patientRepository.UpdateOrder(physicianOrder))
                                {

                                    Auditor.Log(agencyId, Guid.Empty, Current.DisplayName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, action, DisciplineTasks.PhysicianOrder, description);
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleEvent.ReturnReason = oldReason;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var isActionSet = false;
            var description = string.Empty;
            var action = new Actions();
            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(agencyId, patientId, episodeId, orderId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    var oldReason = scheduleEvent.ReturnReason;

                    var facetoFace = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId);
                    if (facetoFace != null)
                    {
                        if (actionType == "Approve")
                        {
                            facetoFace.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            facetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            facetoFace.ReceivedDate = DateTime.Now;

                            scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            scheduleEvent.ReturnReason = string.Empty;

                            description = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            action = Actions.PhysicianApproved;
                            isActionSet = true;
                        }
                        else if (actionType == "Return")
                        {
                            facetoFace.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            facetoFace.Modified = DateTime.Now;

                            scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedForClinicianReview;
                            scheduleEvent.ReturnReason = reason;

                            description = string.Format("Returned for review by: {0}", Current.DisplayName);
                            action = Actions.PhysicianReturned;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (patientRepository.UpdateFaceToFaceEncounter(facetoFace))
                                {

                                    Auditor.Log(agencyId, Guid.Empty, Current.DisplayName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, action, DisciplineTasks.FaceToFaceEncounter, description);
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleEvent.ReturnReason = oldReason;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter order)
        {
            var result = false;
            var description = string.Empty;
            var existingFacetoFace = patientRepository.GetFaceToFaceEncounter(order.Id, order.PatientId, order.AgencyId);
            if (existingFacetoFace != null)
            {
                existingFacetoFace.Status = order.Status;
                existingFacetoFace.Modified = DateTime.Now;
                existingFacetoFace.Services = order.Services;
                existingFacetoFace.ServicesOther = order.ServicesOther;
                existingFacetoFace.Certification = order.Certification;
                existingFacetoFace.EncounterDate = order.EncounterDate;
                existingFacetoFace.MedicalReason = order.MedicalReason;
                existingFacetoFace.ClinicalFinding = order.ClinicalFinding;

                if (order.ServicesArray != null && order.ServicesArray.Count > 0)
                {
                    existingFacetoFace.Services = order.ServicesArray.ToArray().AddColons();
                }

                if (existingFacetoFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                {
                    existingFacetoFace.ReceivedDate = DateTime.Now;
                    existingFacetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                }
                else
                {
                    existingFacetoFace.SignatureText = string.Empty;
                }


                var scheduleEvent = patientRepository.GetScheduleEventNew(existingFacetoFace.AgencyId, existingFacetoFace.PatientId, existingFacetoFace.EpisodeId, existingFacetoFace.Id);
                if (scheduleEvent != null)
                {

                    var oldStatus = scheduleEvent.Status;
                    scheduleEvent.Status = existingFacetoFace.Status;
                    if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        if (patientRepository.UpdateFaceToFaceEncounter(existingFacetoFace))
                        {
                            Auditor.Log(existingFacetoFace.AgencyId, Guid.Empty,Current.DisplayName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.PhysicianEdit, DisciplineTasks.FaceToFaceEncounter, string.Empty);
                            result = true;
                        }
                        else
                        {
                            scheduleEvent.Status = oldStatus;
                            patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            result = false;
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateEvalOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid evalId, string actionType, string reason)
        {
            var result = false;
            bool isActionSet = false;
            var description = string.Empty;
            var action = new Actions();
            if (!evalId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(agencyId, patientId, episodeId, evalId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    var oldReason = scheduleEvent.ReturnReason;

                    var evalOrder = patientRepository.GetVisitNote(agencyId, patientId, evalId);
                    if (evalOrder != null)
                    {
                        if (actionType == "Approve")
                        {
                            evalOrder.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                            evalOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            evalOrder.ReceivedDate = DateTime.Now;
                            scheduleEvent.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                            scheduleEvent.ReturnReason = string.Empty;
                            description = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            action = Actions.PhysicianApproved;
                            isActionSet = true;
                        }
                        else if (actionType == "Return")
                        {
                            evalOrder.Status = ((int)ScheduleStatus.EvalReturnedByPhysician);
                            evalOrder.Modified = DateTime.Now;

                            scheduleEvent.Status = (int)ScheduleStatus.EvalReturnedByPhysician;
                            scheduleEvent.ReturnReason = reason;
                            description = string.Format("Returned for review by: {0}", Current.DisplayName);
                            action = Actions.PhysicianReturned;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (patientRepository.UpdateVisitNote(evalOrder))
                                {
                                    if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                    {
                                        Auditor.Log(agencyId, Guid.Empty, Current.DisplayName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, action,(DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleEvent.ReturnReason = oldReason;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        #endregion
    }
}
