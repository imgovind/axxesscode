﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Log.Common;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Physician.App.ViewData;
    using Axxess.Physician.App.Extensions;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;
    using Axxess.Log.Enums;

    public class AssessmentService : IAssessmentService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IOasisCDataProvider oasisDataProvider;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;

        public AssessmentService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(oasisDataProvider, "dataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.oasisDataProvider = oasisDataProvider;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region IAssessmentService Members

        public VisitNoteViewData GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = new VisitNoteViewData();

            note.Agency = agencyRepository.GetWithBranches(agencyId);
            var patientvisitNote = patientRepository.GetVisitNote(agencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var selectedEpisodeId = note.Questions != null && note.Questions.ContainsKey("SelectedEpisodeId") && note.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? note.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;

                var episode = patientRepository.GetEpisodeById(agencyId, selectedEpisodeId, patientId);
                if (episode != null)
                {
                    var scheduledEvent = patientRepository.GetScheduleEventNew(agencyId, patientId, episodeId, eventId);
                    if (scheduledEvent != null)
                    {

                        note.SignatureText = patientvisitNote.SignatureText;
                        note.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                        if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                        {
                            note.DisciplineTask = scheduledEvent.DisciplineTask;
                            var assessment = GetEpisodeAssessment(agencyId, episodeId, patientId, scheduledEvent.EventDate);
                            if (assessment != null)
                            {
                                assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                note.Questions = assessment.ToNotesQuestionDictionary();
                            }
                        }
                        else
                        {
                            note.Questions = patientvisitNote.ToDictionary();
                        }
                        if (!selectedEpisodeId.IsEmpty())
                        {
                            note.EndDate = episode.EndDate;
                            note.StartDate = episode.StartDate;

                        }
                        else
                        {

                            note.EndDate = episode.EndDate;
                            note.StartDate = episode.StartDate;
                            note.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");


                        }
                        note.PatientId = patientvisitNote.PatientId;
                        note.EpisodeId = patientvisitNote.EpisodeId;
                        note.EventId = patientvisitNote.Id;
                        note.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                        note.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    }
                    else
                    {
                        note.Questions = new Dictionary<string, NotesQuestion>();
                    }
                    var patient = patientRepository.Get(patientId, agencyId);
                    note.Patient = patient;
                    if (patient != null)
                    {
                        if (note.Questions != null && note.Questions.ContainsKey("Physician") && note.Questions["Physician"].Answer.IsNotNullOrEmpty() && note.Questions["Physician"].Answer.IsGuid())
                        {
                            var physician = physicianRepository.Get(note.Questions["Physician"].Answer.ToGuid(), agencyId);
                            if (physician != null)
                            {
                                note.PhysicianId = physician.Id;
                                note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                            }
                        }
                        else
                        {
                            if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                            {
                                var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                                if (physician != null)
                                {
                                    note.PhysicianId = physician.Id;
                                    note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                }
                            }
                        }
                    }
                }
                else
                {
                    note.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                note.Questions = new Dictionary<string, NotesQuestion>();
            }
            return note;
        }

        public Assessment GetEpisodeAssessment(Guid agencyId, Guid episodeId, Guid patientId, DateTime eventDate)
        {
            Assessment assessment = null;
            var episode = patientRepository.GetEpisodeOnly(agencyId, episodeId, patientId);
            if (episode != null)
            {
                var oasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.ToString().ToLowerInvariant().Contains("resumptionofcare") || d.ToString().ToLowerInvariant().Contains("startofcare")).Select(d => (int)d).ToArray();
                var assessmentEvent = patientRepository.GetLastScheduledEvents(agencyId, patientId, episodeId, episode.StartDate, episode.EndDate, episode.StartDate, eventDate, oasis);
                if (assessmentEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentEvent.EventId,agencyId);
                }
                else
                {
                    var previousEpisode = patientRepository.GetLastEpisodeByEndDate(agencyId, patientId, episode.StartDate.AddDays(-1));
                    if (previousEpisode != null)
                    {
                        var previousOasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.ToString().ToLowerInvariant().Contains("recertification") || d.ToString().ToLowerInvariant().Contains("resumptionofcare")).Select(d => (int)d).ToArray();
                        var previousAssessmentEvent = patientRepository.GetLastScheduledEvents(agencyId, patientId, episodeId, previousEpisode.StartDate, previousEpisode.EndDate, previousEpisode.EndDate.AddDays(-5), previousEpisode.EndDate, previousOasis);
                        if (previousAssessmentEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(previousAssessmentEvent.EventId,agencyId);
                        }

                    }
                }
            }
            return assessment;
        }

        private Assessment GetAssessment(Guid assessmentId, Guid agencyId)
        {
            return oasisDataProvider.OasisAssessmentRepository.Get(assessmentId,agencyId);
        }

        public PlanofCare GetPlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid planofcareId)
        {
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(agencyId, planofcareId);

            if (planofCare == null)
            {
                var standAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, episodeId, patientId, planofcareId);
                if (standAlone != null)
                {
                    planofCare = standAlone.ToPlanofCare();
                }
            }

            return planofCare;
        }

        public IDictionary<string, Question> GetAllergies(Guid assessmentId, Guid agencyId)
        {
            var allergies = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                {
                    allergies.Add("485Allergies", questions["485Allergies"]);
                }
                if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                {
                    allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
                }
            }
            return allergies;
        }

        public IDictionary<string, Question> GetDiagnoses(Guid assessmentId,Guid agencyId)
        {
            var diagnosis = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

        public bool UpdatePlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason)
        {
            var result = false;
            bool isActionSet = false;
            var description = string.Empty;
            var action = new Actions();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(agencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    var oldReason = scheduleEvent.ReturnReason;
                    var planofCareStandAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, episodeId, patientId, eventId);
                    if (planofCareStandAlone != null)
                    {
                        if (actionType == ButtonAction.Approve.ToString())
                        {
                            planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            planofCareStandAlone.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            planofCareStandAlone.ReceivedDate = DateTime.Now;

                            scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            scheduleEvent.ReturnReason = string.Empty;

                            description = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            action = Actions.PhysicianApproved;
                            isActionSet = true;
                        }
                        else if (actionType == ButtonAction.Return.ToString())
                        {
                            planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);

                            scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedForClinicianReview;
                            scheduleEvent.ReturnReason = reason;

                            description = string.Format("Returned for review by: {0}", Current.DisplayName);
                            action = Actions.PhysicianReturned;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                                {
                                    Auditor.Log(agencyId, Guid.Empty,Current.DisplayName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, action, DisciplineTasks.HCFA485StandAlone, description);
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleEvent.ReturnReason = oldReason;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    result = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        var planofCare = oasisDataProvider.PlanofCareRepository.Get(agencyId, episodeId, patientId, eventId);
                        if (planofCare != null)
                        {
                            if (actionType == ButtonAction.Approve.ToString())
                            {
                                planofCare.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                planofCare.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                planofCare.ReceivedDate = DateTime.Now;

                                scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                                scheduleEvent.ReturnReason = string.Empty;

                                description = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                                action = Actions.PhysicianApproved;
                                isActionSet = true;
                            }
                            else if (actionType == ButtonAction.Return.ToString())
                            {
                                planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);

                                scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedForClinicianReview;
                                scheduleEvent.ReturnReason = reason;

                                description = string.Format("Returned for review by: {0}", Current.DisplayName);
                                action = Actions.PhysicianReturned;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                                    {

                                        Auditor.Log(agencyId, Guid.Empty, Current.DisplayName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, action, DisciplineTasks.HCFA485, description);
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }
                                }
                            }
                        }
                    }

                }
            }

            return result;
        }

        #endregion
    }
}
