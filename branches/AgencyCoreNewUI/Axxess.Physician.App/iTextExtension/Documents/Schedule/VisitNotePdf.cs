﻿namespace Axxess.Physician.App.iTextExtension
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.Physician.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.Physician.App.iTextExtension.XmlParsing;
    using Axxess.Physician.App.Enums;
    abstract class VisitNotePdf : AxxessPdf {
        private VisitNoteXml xml;
        protected String DocType;
        public VisitNotePdf(VisitNoteViewData data) : base() {
            PdfDoc type;
            switch (data.Type) {
                case "MSWAssessment": case "MSWDischarge": case "MSWEvaluationAssessment":
                    type = PdfDocs.MSWEval;
                    this.Init(data, type);
                    break;
                case "PTEvaluation": case "PTReEvaluation": case "PTMaintenance":
                    type = PdfDocs.PTEval;
                    this.Init(data, type);
                    break;
                case "OTEvaluation": case "OTReEvaluation": case "OTDischarge": case "OTMaintenance":
                    type = PdfDocs.OTEval;
                    this.Init(data, type);
                    break;
                case "STEvaluation": case "STReEvaluation": case "STMaintenance":
                    type = PdfDocs.STEval;
                    this.Init(data, type);
                    break;
            }
        }
        public VisitNotePdf(VisitNoteViewData data, PdfDoc type) : base() {
            this.Init(data, type);
        }
        private void Init(VisitNoteViewData data, PdfDoc type) {
            this.xml = new VisitNoteXml(data, type);
            this.SetType(type);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.Content(this.xml));
            this.SetMargins(this.Margins(data));
            this.SetFields(this.FieldMap(data));
        }
        protected virtual IElement[] Content(VisitNoteXml xml) {
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            return content;
        }
        protected virtual List<Dictionary<String, String>> FieldMap(VisitNoteViewData data) {
            return new List<Dictionary<string, string>>();
        }
        protected virtual float[] Margins(VisitNoteViewData data) {
            return new float[] { 0, 0, 0, 0 };
        }
    }
}