﻿namespace Axxess.Physician.App.iTextExtension {
    using System;
    public class PdfDocs {
        public static PdfDoc PlanOfCare485          = new PdfDoc(AppSettings.PlanOfCare485Pdf);
        public static PdfDoc PlanOfCare487          = new PdfDoc(AppSettings.PlanOfCare487Pdf);
        public static PdfDoc PhysicianOrder         = new PdfDoc(AppSettings.PhysicianOrderPdf);
        public static PdfDoc PhysFaceToFace         = new PdfDoc(AppSettings.PhysFaceToFacePdf, AppSettings.PhysFaceToFaceXml);
        public static PdfDoc MSWEval                = new PdfDoc(AppSettings.MSWEvalPdf, AppSettings.MSWEvalXml);
        public static PdfDoc OTEval                 = new PdfDoc(AppSettings.OTEvalPdf, AppSettings.OTEvalXml);
        public static PdfDoc PTEval                 = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml);
        public static PdfDoc STEval                 = new PdfDoc(AppSettings.STEvalPdf, AppSettings.STEvalXml);
    }
}
