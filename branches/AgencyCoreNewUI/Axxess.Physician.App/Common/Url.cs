﻿namespace Axxess.Physician.App
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Extensions;

    public static class Url
    {
        public static string Print(Guid orderId, Guid patientId, Guid episodeId, Guid agencyId, OrderType type, bool useIcon)
        {
            string printUrl = string.Empty;
            string linkText = type.GetDescription();
            if (useIcon) linkText = "<span class=\"img icon print\"></span>";

            switch (type)
            {
                case OrderType.PhysicianOrder:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/Physician/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PhysicianPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.FaceToFaceEncounter:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/FaceToFace/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/FaceToFacePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PlanofCare/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PlanofCarePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
            }
            return printUrl;
        }

        public static string View(Guid orderId, Guid patientId, Guid episodeId, Guid agencyId, OrderType type, bool useIcon)
        {
            string printUrl = string.Empty;
            string linkText = type.GetDescription();
            if (useIcon) linkText = "<span class=\"img icon print\"></span>";

            switch (type)
            {
                case OrderType.PhysicianOrder:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/Physician/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PhysicianPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.FaceToFaceEncounter:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/FaceToFace/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/FaceToFacePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PlanofCare/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PlanofCarePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
            }
            return printUrl;
        }
    }
}
