﻿namespace Axxess.Physician.App.Controllers
{
    using System.Web.Mvc;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Security;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using MvcReCaptcha;
    using Axxess.Core;

    [Compress]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountController : BaseController
    {
        #region Constructor

        public AccountController()
            : this(null, null)
        {
        }

        public AccountController(IFormsAuthenticationService formsAuthentication, IPhysicianMembershipService membershipService)
        {
            this.MembershipService = membershipService;
            this.AuthenticationService = formsAuthentication;
        }

        #endregion

        #region Properties

        public IPhysicianMembershipService MembershipService
        {
            get;
            private set;
        }

        public IFormsAuthenticationService AuthenticationService
        {
            get;
            private set;
        }

        #endregion

        #region AccountController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Activate()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["loginid"] != null)
            {
                var loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
                var login = loginRepository.Find(HttpContext.Request.QueryString["loginid"].ToGuid());
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.Name = login.DisplayName;
                    account.EmailAddress = login.EmailAddress;
                }
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Activate([Bind] Account account)
        {
            var viewData = Validate<AccountViewData>(
                            new Validation(() => string.IsNullOrEmpty(account.Password), "You must specify a password. <br/>"),
                            new Validation(() => account.Password.Length < 8, "The minimum password length is 8 characters.")
                            );

            if (viewData.isSuccessful)
            {
                if (this.MembershipService.Activate(account))
                {
                    this.AuthenticationService.SignIn(account.UserName, false);
                    viewData.redirectUrl = "/";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Account Activation failed. Please try again.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AppLogOn()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }

            if (HttpContext.Request.QueryString["linkid"] != null)
            {
                var linkId = HttpContext.Request.QueryString["linkid"].ToGuid();

                if (MembershipService.Impersonate(linkId))
                {
                    this.AuthenticationService.SignIn(Current.User.Name, false);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOn()
        {
            var logon = new Logon();

            if ((HttpContext.Request.Cookies.Count > 0) && (HttpContext.Request.Cookies[PhysicianAppSettings.RememberMeCookie] != null))
            {
                var cookie = HttpContext.Request.Cookies[PhysicianAppSettings.RememberMeCookie];
                logon.UserName = Crypto.Decrypt(cookie.Value);
            }

            return View(logon);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public JsonResult LogOn([Bind] Logon logon)
        {
            var viewData = Validate<AccountViewData>(
                             new Validation(() => string.IsNullOrEmpty(logon.UserName.Trim()), "You must specify a username. <br/>"),
                             new Validation(() => string.IsNullOrEmpty(logon.Password.Trim()), "You must specify a password.  <br/>"),
                             new Validation(() => !logon.UserName.Trim().IsEmail(), "E-mail Address is not valid.<br/>")
                        );

            if (viewData.isSuccessful)
            {
                LoginAttemptType loginAttempt = MembershipService.Validate(logon.UserName.Trim(), logon.Password.Trim());

                switch (loginAttempt)
                {
                    case LoginAttemptType.Success:
                        this.AuthenticationService.SignIn(logon.UserName.Trim(), logon.RememberMe);
                        viewData.redirectUrl = "/";
                        break;
                    case LoginAttemptType.TooManyAttempts:
                        viewData.isLocked = true;
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "You exceeded the maximum number of login attempts. Please note that your online access to Support will be disabled for 5 minutes.";
                        break;
                    default:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Username or Password combination failed.";
                        break;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOff()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }

            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotPassword()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }
            return View();
        }

        [CaptchaValidator]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ForgotPassword(string EmailAddress, bool captchaValid)
        {
            var viewData = Validate<JsonViewData>(
                            new Validation(() => string.IsNullOrEmpty(EmailAddress.Trim()), "You must specify your E-mail Address. <br/>"),
                            new Validation(() => !EmailAddress.IsEmail(), "The E-mail Address provided appears to be invalid. <br/>"),
                            new Validation(() => !(captchaValid), "The Captcha text you entered didn't match the security check. Please try again. <br/>")
                       );

            if (viewData.isSuccessful)
            {
                var resetAttempt = this.MembershipService.Validate(EmailAddress.Trim());

                if (resetAttempt == ResetAttemptType.Success)
                {
                    if (this.MembershipService.ResetPassword(EmailAddress.Trim()))
                    {
                        viewData.errorMessage = "A link to reset your password has been sent to your e-mail address. <br />Please check your e-mail to complete the login process. <a href=\"/login\">Login</a>";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = ProcessResetAttempt(resetAttempt);
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Reset()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["loginid"] != null && HttpContext.Request.QueryString["type"] != null)
            {
                var loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
                var login = loginRepository.Find(HttpContext.Request.QueryString["loginid"].ToGuid());
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.Name = login.DisplayName;
                    account.EmailAddress = login.EmailAddress;
                    account.ResetType = HttpContext.Request.QueryString["type"].IsEqual("password") ? Change.Password : Change.Signature;
                }
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "Exceptions result in password not being changed.")]
        public JsonResult Reset([Bind] Account account)
        {
            Check.Argument.IsNotNull(account, "account");

            var viewData = new AccountViewData();

            if (account.ResetType == Change.Password)
            {
                viewData = Validate<AccountViewData>(
                           new Validation(() => string.IsNullOrEmpty(account.Password), "You must specify a password. <br/>"),
                           new Validation(() => account.Password.Length < 8, "The minimum password length is 8 characters.")
                        );

                if (viewData.isSuccessful)
                {
                    if (this.MembershipService.UpdatePassword(account))
                    {
                        LoginMonitor.Instance.Remove(account.EmailAddress);
                        viewData.errorMessage = "Your password has been successfully changed. <a href=\"/login\">Login</a>";
                        viewData.redirectUrl = "/";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Change password attempt failed.";
                    }
                }
            }

            return Json(viewData);
        }

        #endregion

        #region Private Methods

        private string ProcessResetAttempt(ResetAttemptType resetAttempt)
        {
            var message = string.Empty;
            switch (resetAttempt)
            {
                case ResetAttemptType.Locked:
                    message = "This user has been locked. Please contact your Agency/Company's Administrator.";
                    break;
                case ResetAttemptType.Deactivated:
                    message = "This user is deactivated. Please contact your Agency/Company's Administrator.";
                    break;
                default:
                    message = "Reset failed. Please try again.";
                    break;
            }
            return message;
        }

        #endregion
    }

}
