﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;

    [Compress]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PingController : BaseController
    {
        public PingController()
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Index()
        {
            var viewData = new JsonViewData();
            if (HttpContext.User != null && HttpContext.User.Identity.IsAuthenticated) viewData.isSuccessful = true;
            return Json(viewData);
        }
    }
}
