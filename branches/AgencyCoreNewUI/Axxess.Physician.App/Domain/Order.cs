﻿namespace Axxess.Physician.App.Domain
{
    using System;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Enums;
    
    [KnownType(typeof(Order))]
    public class Order
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string PatientName { get; set; }
        public string PhysicianName { get; set; }
        public OrderType Type { get; set; }
        public long Number { get; set; }
        public string SentDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ReceivedDate { get; set; }
        public string CreatedDate { get; set; }
        public string PrintUrl { get; set; }
        [DataType(DataType.Date)]
        public DateTime SendDate { get; set; }
        public int Status { get; set; }
        public string StatusName
        {
            get
            {
                ScheduleStatus status = this.Status > 0 && Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status.ToString()) : ScheduleStatus.NoStatus;
                if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.CreatedDate.IsValidDate() && this.CreatedDate.ToDateTime() < DateTime.Today)
                {
                    return ScheduleStatus.CommonNotStarted.GetDescription();
                }
                else
                {
                    return status.GetDescription();
                }
            }
        }

        public string AgencyName { get; set; }
        public string TypeDescription
        {
            get
            {
                return this.Type.GetDescription();
            }
        }
        public string ReceivedDateFormatted { get { return this.ReceivedDate > DateTime.MinValue ? this.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string SendDateFormatted { get { return this.SendDate > DateTime.MinValue ? this.SendDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string OrderDate
        {
            get
            {
                return CreatedDate.IsNotNullOrEmpty() ? "<span class='float-right'>" + CreatedDate.Split('/')[2] + "</span><span class='float-right'>/</span><span class='float-right'>" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[1] + "</span>" : "";
            }
        }

    }
}
