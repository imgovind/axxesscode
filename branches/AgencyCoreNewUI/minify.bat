@ECHO OFF

GOTO %1

:all
ECHO Compressing All JavaScript Files...

:acore
ECHO Compressing AgencyCore Files...
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Agency\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Agency\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Agency\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Billing\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Billing\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Billing\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Home\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Home\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Home\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Message\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Message\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Message\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Oasis\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Oasis\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Oasis\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Patient\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Patient\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Patient\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Payroll\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Payroll\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Payroll\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\PDSchedule\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\PDSchedule\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\PDSchedule\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Plugins\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Plugins\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Plugins\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Print\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Print\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Print\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Referral\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Referral\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Referral\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Report\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Report\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Report\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\Schedule\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\Schedule\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\Schedule\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\System\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\System\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\System\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.WebSite\Scripts\User\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.WebSite\Scripts\User\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.WebSite\Scripts\User\min\%%f
IF "%1"=="acore" GOTO end

:support
ECHO Compressing Axxess Support Files...
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.SupportSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.SupportSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.SupportSite\Scripts\Modules\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.AgencyManagement.SupportSite\Scripts\System\*.js') DO java -jar compiler.jar --js Axxess.AgencyManagement.SupportSite\Scripts\System\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.AgencyManagement.SupportSite\Scripts\System\min\%%f
REM FOR /f %%f IN ('dir /b .\AgencyCoreProduction\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCoreProduction\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCoreProduction\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\min\%%f
IF "%1"=="support" GOTO end

:md
ECHO Compressing Axxess MD Files...
FOR /f %%f IN ('dir /b .\Axxess.Physician.WebSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js Axxess.Physician.WebSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.Physician.WebSite\Scripts\Modules\min\%%f
FOR /f %%f IN ('dir /b .\Axxess.Physician.WebSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js Axxess.Physician.WebSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file Axxess.Physician.WebSite\Scripts\Plugins\Custom\min\%%f

:end
del ~compile.tmp
ECHO Complete
pause