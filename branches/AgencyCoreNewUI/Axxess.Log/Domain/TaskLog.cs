﻿namespace Axxess.Log.Domain
{
    using System;
    using System.Xml.Serialization;

    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Log.Enums;

    public class TaskLog
    {
        public Guid UserId { get; set; }
        public DateTime Date { get; set; }
        public Guid AgencyId { get; set; }
        public int Status { get; set; }
        public string Action { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
       
        [XmlIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status != (int)ScheduleStatus.NoStatus)
                {
                    if (Enum.IsDefined(typeof(ScheduleStatus), this.Status))
                    {
                        return ((ScheduleStatus)this.Status).GetDescription();
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    if (this.Action.IsNotNullOrEmpty() && Enum.IsDefined(typeof(Actions), this.Action))
                    {
                        var action = (Actions)Enum.Parse(typeof(Actions), this.Action);
                        if (action == Actions.Reassigned)
                        {
                            return action.GetDescription() + " " + this.Description;
                        }
                        else if (action == Actions.EditAssessment)
                        {
                            return action.GetDescription() + " (" + this.Description + " )";
                        }
                        else
                        {
                            return action.GetDescription();
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
        }

    }
}
