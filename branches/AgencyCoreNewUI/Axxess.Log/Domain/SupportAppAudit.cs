﻿namespace Axxess.Log.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Log.Enums;

    public class SupportAppAudit
    {
        public int Id { get; set; }
        public string EntityId { get; set; }
        public Guid DomainId { get; set; }
        public Guid UserId { get; set; }
        public string LogDomain { get; set; }
        public string LogType { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

        [SubSonicIgnore]
        public string ActionDescription { get { return Enum.IsDefined(typeof(LogAction), this.Action) ? ((LogAction)Enum.Parse(typeof(LogAction), this.Action)).GetDescription() + (this.Description.IsNotNullOrEmpty() ? string.Format(" ({0})", this.Description) : string.Empty) : (this.Description.IsNotNullOrEmpty() ? this.Description : string.Empty); } }
    }
}
