﻿namespace Axxess.Log.Enums
{
    using System.ComponentModel;

    public enum Actions
    {
        [Description("Created")]
        Add,
        [Description("Updated")]
        Edit,
        [Description("Updated Task Detail")]
        EditDetail,
        [Description("Deleted")]
        Deleted,
        [Description("Approved")]
        Approved,
        [Description("Returned")]
        Returned,
        [Description("Physician Approved")]
        PhysicianApproved,
        [Description("Physician Returned for review")]
        PhysicianReturned,
        [Description("Physician Updated Order")]
        PhysicianEdit,
        [Description("Status changed")]
        StatusChange,
        [Description("Loaded Previous Note")]
        LoadPreviousNote,
        [Description("Restored")]
        Restored,
        [Description("Reassigned")]
        Reassigned,
        [Description("Saved")]
        Saved,
        [Description("Saved")]
        EditAssessment,
        [Description("Printed")]
        Printed,
        [Description("Marked as Paid")]
        MarkedPaid,
        [Description("Marked as Unpaid")]
        MarkedUnpaid

    }
}
