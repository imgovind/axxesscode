﻿namespace Axxess.Log.Common
{
    using System;
    using System.Collections.Generic;


    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.Log.Repositories;

    public static class Auditor
    {
        private static readonly ILogRepository logRepository = Container.Resolve<ILogDataProvider>().LogRepository;

        public static bool Log(Guid agencyId, Guid userId, string userName, Guid episodeId, Guid patientId, Guid entityId, Actions action, DisciplineTasks disciplineTask)
        {
            return Log( agencyId,  userId,  userName,episodeId, patientId, entityId, action, ScheduleStatus.NoStatus, disciplineTask, string.Empty);
        }

        public static bool Log(Guid agencyId, Guid userId, string userName, Guid episodeId, Guid patientId, Guid entityId, Actions action, DisciplineTasks disciplineTask, string description)
        {
            return Log( agencyId,  userId,  userName,episodeId, patientId, entityId, action, ScheduleStatus.NoStatus, disciplineTask, description);
        }

        public static bool Log(Guid agencyId, Guid userId, string userName, Guid episodeId, Guid patientId, Guid entityId, Actions action, ScheduleStatus status, DisciplineTasks disciplineTask, string description)
        {
            var log = new TaskLog
            {
                UserId = userId,
                Description = description,
                AgencyId = agencyId,
                UserName = userName,
                Action = action.ToString(),
                Status = (int)status,
                Date = DateTime.Now
            };

            return SaveAudit(agencyId, episodeId, patientId, entityId, disciplineTask, log);
        }

        //public static IList<string> Trail(Guid patientId, Guid entityId, Disciplines disciplineTask)
        //{
        //    var trail = new List<string>();
        //    var audit = logRepository.GetTaskAudit(Current.AgencyId, patientId, entityId, (int)disciplineTask);

        //    if (audit != null)
        //    {
        //        var logs = audit.Log.ToObject<List<TaskLog>>();
        //        logs.ForEach(l =>
        //        {
        //            trail.Add(l.ToString());
        //        });
        //    }

        //    return trail;
        //}

        public static bool AddGeneralLog(Guid agencyId, Guid userId, LogDomain domain, Guid domainId, string entityId, LogType logType, LogAction logAction, string description)
        {
            var audit = new AppAudit { AgencyId = agencyId, LogDomain = domain.ToString(), DomainId = domainId, EntityId = entityId, UserId = userId, LogType = logType.ToString(), Action = logAction.ToString(), Date = DateTime.Now, Description = description };
            return logRepository.AddGeneralAudit(audit);
        }

        private static bool SaveAudit(Guid agencyId, Guid episodeId, Guid patientId, Guid entityId, DisciplineTasks disciplineTask, TaskLog log)
        {
            var result = false;
            var audit = logRepository.GetTaskAudit(agencyId, patientId, entityId, (int)disciplineTask);

            if (audit == null)
            {
                audit = new TaskAudit
                {
                    EntityId = entityId,
                    EpisodeId = episodeId,
                    PatientId = patientId,
                    AgencyId = agencyId,
                    Log = new List<TaskLog> { log }.ToXml(),
                    DisciplineTaskId = (int)disciplineTask
                };

                result = logRepository.AddTaskAudit(audit);
            }
            else
            {
                var logs = audit.Log.ToObject<List<TaskLog>>();
                logs.Add(log);
                audit.Log = logs.ToXml();
                audit.EpisodeId = episodeId;
                result = logRepository.UpdateTaskAudit(audit);
            }

            return result;
        }

        public static bool AddSupportLog(Guid userId, LogDomain domain, Guid domainId, string entityId, LogType logType, LogAction logAction, string description)
        {
            var audit = new SupportAppAudit { UserId = userId, LogDomain = domain.ToString(), DomainId = domainId, EntityId = entityId, LogType = logType.ToString(), Action = logAction.ToString(), Date = DateTime.Now, Description = description };
            return logRepository.AddSupportAudit(audit);
        }

    }
}
