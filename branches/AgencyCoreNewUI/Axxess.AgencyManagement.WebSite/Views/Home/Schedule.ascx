﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>Task</th>
				<th>Date</th>
			</tr>
        </thead>
	    <tbody class="faux-grid-content"></tbody>
    </table>
</div>
<div class="widget-more">
    <a status="Link to My Scheduled Tasks Window">More &#187;</a>
</div>
