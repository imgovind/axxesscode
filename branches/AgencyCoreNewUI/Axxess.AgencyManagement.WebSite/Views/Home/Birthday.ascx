﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
                <th>Birth Day</th>
                <th>Age</th>
                <th>Name</th>
                <th>Home Phone</th>
            </tr>
        </thead>
        <tbody class="faux-grid-content"></tbody>
    </table>
</div>
<div class="widget-more">
    <a status="Link to Report Center Window, Patient Birthday Report">More &#187;</a>
</div>
