﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Axxess.Core.Infrastructure" %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Dashboard | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <ul id="widget-column-1" class="widgets">
        <li class="widget stationary" id="custom-message-widget">
            <div class="widget-head">
                <h5>Hello,&#160;<%= Current.DisplayName %>!</h5>
            </div>
            <div class="widget-content"></div>
        </li>
        <li class="widget stationary" id="news-widget">
            <div class="widget-head">
                <h5>News/Updates</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/NewsFeed.ascx"); %>    </div>
        </li>
<%  if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
        <li class="widget" id="past-due-recert-widget">
            <div class="widget-head">
                <h5>Past Due Recerts</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/RecertsPastDue.ascx"); %></div>
        </li>
<%  } %>
    </ul>
    <ul id="widget-column-2" class="widgets">
        <li class="widget" id="local-widget">
            <div class="widget-head">
                <h5>Local</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/Local.ascx"); %></div>
        </li>
<%  if (!Current.IsCommunityLiason) { %>
        <li class="widget" id="user-schedule-widget">
            <div class="widget-head">
                <h5>My Scheduled Tasks</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/Schedule.ascx"); %></div>
        </li>
<%  } %>
<%  if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
        <li class="widget" id="upcoming-recert-widget">
            <div class="widget-head">
                <h5>Upcoming Recerts</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/RecertsUpcoming.ascx"); %></div>
        </li>
<%  } %>
    </ul>
    <ul id="widget-column-3" class="widgets">
        <li class="widget" id="message-widget">
            <div class="widget-head">
                <h5>Messages</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/Messages.ascx"); %></div>
        </li>
<%  if (Current.HasRight(Permissions.AccessPatientReports)) { %>
        <li class="widget" id="birthday-widget">
            <div class="widget-head">
                <h5>Patient Birthdays</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/Birthday.ascx"); %></div>
        </li>
<%  } %>
<%  if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
        <li class="widget" id="outstanding-claim-widget">
            <div class="widget-head">
                <h5>Outstanding Claims</h5>
            </div>
            <div class="widget-content"><% Html.RenderPartial("~/Views/Home/Claims.ascx"); %></div>
        </li>
<%  } %>
    </ul>
</div>