﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="digital-clock"></div>
<input type="hidden" id="weather-zip" value="<%= Html.ZipCode() %>" />
<div id="weather-image"></div>
<div id="weather-info">
    <div class="city"></div>
    <div class="temp"></div>
    <div class="desc"></div>
    <div class="wind"></div>
    <div class="range"></div>
    <div class="link"><a target="_blank">Full forecast</a></div>
</div>