﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="winmenu">
    <ul>
        <li><a status="Link to Message Center Window">Inbox</a></li>
        <li><a status="Link to New Message Window">Compose Mail</a></li>
        <li><a status="Delete Selected Messages">Delete</a></li>
    </ul>
</div>
<ul class="widget-message-list"></ul>
<div class="widget-more">
    <a status="Link to Message Center Window">More &#187;</a>
</div>