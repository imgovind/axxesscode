﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>TOB</th>
				<th>Episode</th>
			</tr>
        </thead>
	    <tbody class="faux-grid-content"></tbody>
    </table>
</div>
<div class="widget-more additional">
    <a status="Link to Create RAP Claims Window">More RAPs &#187;</a>
</div>
<div class="widget-more">
    <a status="Link to Create Final Claims Window">More Finals&#187;</a>
</div>