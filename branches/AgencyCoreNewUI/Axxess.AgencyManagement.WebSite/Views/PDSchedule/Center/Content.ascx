﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PDCalendarViewData>" %>
<div id="PDScheduleCenter_TabStrip" class="wrapper main blue pdschedule">
    <div class="fl button"><a class="current-patient-schedule" >New Schedule</a></div>
    <ul class="fr">
	    <li class="ac">
	        <a href="#PDScheduleCenter_Monthly" action="Monthly">
	            <span>Monthly</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#PDScheduleCenter_Weekly" action="Weekly">
	            <span>Weekly</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#PDScheduleCenter_Daily" action="Daily">
	            <span>Daily</span>
	        </a>
	    </li>
    </ul>
    <div id="PDScheduleCenter_Monthly" class="tab-content"> <% Html.RenderPartial("Center/Monthly", Model); %></div>
    <div id="PDScheduleCenter_Weekly" class="tab-content"></div>
    <div id="PDScheduleCenter_Daily" class="tab-content"></div>
</div>
