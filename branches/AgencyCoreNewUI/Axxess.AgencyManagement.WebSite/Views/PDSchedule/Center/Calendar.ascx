﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  if (Model != null) { %>
    <%  if (Model.Episode != null) { %>
        <%  var scheduleEvents = Model.Episode.ScheduleEvents ?? new List<ScheduleEvent>(); %>
        <%  DateTime[] startdate = new DateTime[3], enddate = new DateTime[3], currentdate = new DateTime[3]; %>
        <%  startdate[0] = DateUtilities.GetStartOfMonth(Model.Episode.StartDate.Month, Model.Episode.StartDate.Year); %>
        <%  enddate[0] = DateUtilities.GetEndOfMonth(Model.Episode.StartDate.Month, Model.Episode.StartDate.Year); %>
        <%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
        <%  startdate[1] = enddate[0].AddDays(1); %>
        <%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
        <%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
        <%  startdate[2] = enddate[1].AddDays(1); %>
        <%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
        <%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
        <%= Html.Hidden("SchedulePatientID", Model.Episode.PatientId, new { @id = "SchedulePatientID" })%>
        <%= Html.Hidden("ScheduleEpisodeID", Model.Episode.Id, new { @id = "ScheduleEpisodeID" })%>
<div class="trical">
        <%  for (int c = 0; c < 3; c++) { %>
            <%  if (startdate[c].Date <= Model.Episode.EndDate.Date) { %>
    <div class="cal">
        <table>
            <thead>
                <tr>
                    <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td>
                </tr>
                <tr>
                    <th>Su</th>
                    <th>Mo</th>
                    <th>Tu</th>
                    <th>We</th>
                    <th>Th</th>
                    <th>Fr</th>
                    <th>Sa</th>
                </tr>
            </thead>
            <tbody>
                <%  var maxWeek = DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); %>
                <%  for (int i = 0; i <= maxWeek; i++) { %>
                <tr>
                    <%  string tooltip = ""; %>
                    <%  int addedDate = (i) * 7; %>
                    <%  if (currentdate[c].AddDays(7 + addedDate).Date >= Model.Episode.StartDate.Date && currentdate[c].AddDays(addedDate) <= enddate[c] && currentdate[c].Date.AddDays(addedDate) <= Model.Episode.EndDate.Date) for (int j = 0; j <= 6; j++) { %>
                        <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
                        <%  if (specificDate < Model.Episode.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate.Date > Model.Episode.EndDate.Date) { %>
                        <td class="inactive"></td>
                        <%  } else { %>
                            <%  var events = scheduleEvents.FindAll(e => e.EventDate.Date == specificDate.Date); %>
                            <%  var count = events.Count; %>
                            <%  if (count > 1) { %>
                                <%  var allevents = "<br />"; %>
                                <%  events.ForEach(e => { allevents += string.Format("{0} - <em>{1}</em><br />", e.DisciplineTaskName, e.UserDisplayName); }); %>
                                <%  tooltip = specificDate.ToShortDateString() + allevents; %>
                    <td class="multi" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');">
                        <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                    </td>
                            <% } else if (count == 1) { %>
                                <%  var evnt = events.First(); %>
                                <%  var missed = (evnt.IsMissedVisit) ? "missed" : ""; %>
                                <%  var status = evnt.Status > 0 ? (evnt.Status) : 1000; %>
                                <%  tooltip = specificDate.ToShortDateString() + string.Format("<br />{0} - <em>{1}</em>", evnt.DisciplineTaskName, evnt.UserName.IsNotNullOrEmpty() || evnt.UserId.IsEmpty() ? evnt.UserName : UserEngine.GetName(evnt.UserId, Current.AgencyId)); %>
                    <td class="status<%= status %> scheduled <%= missed %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');">
                        <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                    </td>
                            <%  } else { %>
                                <%  tooltip = ""; %>
                    <td onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');">
                        <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                    </td>
                            <%  } %>
                        <%  } %>
                    <%  } %>
                </tr>
                <%  } %>
            </tbody>
        </table>
    </div>
            <%  } %>
        <%  } %>
</div>
    <%  } else { %>
        <%  if (!Model.IsDischarged) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(
        U.MessageWarn("No Episode", "There are no episodes found for this patient.  Please add one before schedueling.")).append(
        $("<div/>").Buttons([{ Text: "Add New Episode", Click: function() { Schedule.Episode.NewModal("<%= Model.PatientId %>") } }]))
</script>
        <%  } else { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(U.MessageError("No Episode", "There are no episodes found for this discharged patient."))
</script>
        <%  } %>
    <%  } %>
<%  } %>