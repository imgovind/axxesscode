﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  Html.Telerik().Grid<PatientSelection>().Name("PDScheduleSelectionGrid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name");
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding
        .Ajax().Select("CenterPatientGrid", "PDSchedule", new { branchId = Guid.Empty, statusId = 1, paymentSourceId = 0, name = string.Empty })
    ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
        .OnDataBound("PDSchedule.Center.PatientSelector.Init")
        .OnRowSelected("PDSchedule.Center.PatientSelector.Select")
    ).Render(); %>