﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<ul>
	<li><a href="#ScheduleCenter_NursingTab" discipline="Nursing">Nursing</a></li>
	<li><a href="#ScheduleCenter_HHATab" discipline="HHA">HHA</a></li>
	<li><a href="#ScheduleCenter_MSWTab" discipline="MSW">MSW/Other</a></li>
	<li><a href="#ScheduleCenter_TherapyTab" discipline="Therapy">Therapy</a></li>
	<li><a href="#ScheduleCenter_OrdersTab" discipline="Orders">Orders/Care Plans</a></li>
	<li><a href="#ScheduleCenter_OutlierTab" discipline="Multiple">Daily/Outlier</a></li>
</ul>
<%  string[] tab = { "Nursing", "HHA", "MSW", "Therapy", "Orders" }; %>
<%  for (int i = 0; i < tab.Length; i++) { %>
<div id="ScheduleCenter_<%= tab[i] %>Tab" class="schedule-tab-content">
    <table class="add-schedule ma">
        <thead>
            <tr class="strong">
                <th>Task</th>
                <th>User</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
    <%  for (int j = 0; j < 5; j++) { %>
            <tr class="<%= j == 0 ? "visible" : string.Empty %>">
                <td><%= Html.DisciplineTaskList(tab[i], "ScheduleCenter_" + tab[i] + "Task" + j.ToString())%></td>
                <td><%= Html.Users("ScheduleCenter_" + tab[i] + "User" + j.ToString(), string.Empty, new { })%></td>
                <td>
                    <input type="text" class="date-picker" name="ScheduleCenter_<%= tab[i] %>Date<%= j.ToString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" />
                    <a class="addrem fr ecks">&#0215;</a>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <div class="buttons fr">
        <ul>
            <li><a class="save-schedule">Save</a></li>
            <li><a class="cancel-schedule">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>
<div id="ScheduleCenter_OutlierTab" class="schedule-tab-content">
    <table class="add-schedule ma">
        <thead>
            <tr class="strong">
                <th>Task</th>
                <th>User</th>
                <th>Date Range</th>
            </tr>
        </thead>
        <tbody>
            <tr class="visible">
                <td><%= Html.MultipleDisciplineTasks("ScheduleCenter_OutlierTask")%></td>
                <td><%= Html.Users("ScheduleCenter_OutlierUser", "", new { })%></td>
                <td>
                    <input type="text" class="date-picker" name="ScheduleCenter_OutlierDateFrom" mindate="<%= Model.StartDate.ToShortDateString() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" />
                    to
                    <input type="text" class="date-picker" name="ScheduleCenter_OutlierDateTo" mindate="<%= Model.StartDate.ToShortDateString() %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" />
                </td>
            </tr>
        </tbody>
    </table>
    <div class="buttons fr">
        <ul>
            <li><a class="save-schedule">Save</a></li>
            <li><a class="cancel-schedule">Cancel</a></li>
        </ul>
    </div>
</div>