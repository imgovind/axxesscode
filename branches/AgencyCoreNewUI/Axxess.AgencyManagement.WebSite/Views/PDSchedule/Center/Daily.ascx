﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PDCalendarViewData>" %>
    <%  if (Model!= null ) { %>
        <%  var scheduleEvents = Model.Schedules ?? new List<PDScheduleEvent>(); %>
        <% var startdate =Model.StartDate; %>
        <% var enddate = Model.StartDate; %>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "PDScheduleDaily_PatientId" })%>
         <%= Html.Hidden("StartDate", Model.StartDate, new { @class = "current-view-startDate" })%>
        <div class="trical">
          <div class="calhead">
             <table>
                <thead>
                    <tr>
                        <td class="caltitle" colspan="2">
                             <span class="button  fl">
                                <a class="navigate previous-day" date="<%= startdate.AddDays(-1) %>" ><span class="largefont">&#8617;</span>Previous </a>
                            </span>
                        </td>
                        <td colspan="4" class="caltitle">
                                <%= string.Format("{0:ddd} {0:MMMM} {0:dd} {0:yyyy}", startdate)%>
                        </td>
                        <td class="caltitle" colspan="2">
                             <span class="button  fr">
                                <a class="navigate next-day" date="<%= enddate.AddDays(1) %>" >Next<span class="largefont">&#8618;</span> </a>
                            </span>
                        </td>
                    </tr>
                </thead>
             </table>
          </div>
          <div class="pdcal wc-scrollable-grid"  > 
              <table class="wc-time-slots"> 
                    <tbody>
                             <tr> 
                                <td width="60px!important" class="wc-grid-timeslot-header"></td>                
                                <td >                
                                    <div class="wc-time-slot-wrapper">                
                                         <div class="wc-time-slots">
                                              <%  for (int i = 0; i < 96; i++)
                                                  { if((i+1)%4==0){ %>
                                                  <div class="wc-time-slot wc-hour-end"  ></div>
                                              <%} else{%><div class="wc-time-slot"  ></div><%}} %>
                              
                                         </div>
                                   </div>
                               </td>
                             </tr>
                             <tr>
                                   <td class="wc-grid-timeslot-header">
                                          <%  var date = DateTime.Today.Add(new TimeSpan(0,0,0)); %>
                                          <%  var timeSpan = TimeSpan.FromMinutes(60); %>
                                           <%  for (int i = 1; i <= 24; i++){%>
                                                 <div class="wc-hour-header wc-business-hours"><div class="wc-time-header-cell" ><%= date.ToString("hh")%><span class="wc-am-pm"><%= date.ToString("tt")%></span></div></div>
                                             <%date=date.Add(timeSpan) ;} %>
                                   </td>
                                      <td class="wc-day-column day-1"><div class="wc-day-column-inner ui-droppable" style="height: 100%; -moz-user-select: none;"></div></td>
                            </tr>
                          </tbody>
              </table>
          </div>
        </div>
        <%  } else { %>
            <%  if (!Model.IsDischarged) { %>
        <script type="text/javascript">
            $("#PDScheduleMainResult").html(
            U.MessageWarn("No Episode", "There are no episodes found for this patient.  Please add one before schedueling.")).append(
            $("<div/>").Buttons([{ Text: "Add New Episode", Click: function() { Schedule.Episode.NewModal("<%= Model.PatientId %>") } }]))
        </script>
            <%  } else { %>
        <script type="text/javascript">
            $("#PDScheduleMainResult").html(U.MessageError("No Episode", "There are no episodes found for this discharged patient."))
        </script>
            <%  } %>
        <%  } %>
