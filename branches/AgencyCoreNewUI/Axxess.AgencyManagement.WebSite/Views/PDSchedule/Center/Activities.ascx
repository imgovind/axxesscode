﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleActivityArgument>" %>
<% Html.Telerik().Grid<ScheduleEvent>().Name("ScheduleActivityGrid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task").Width(23);
       columns.Bound(s => s.EventDateSortable).Title("Scheduled Date").Width(10);
       columns.Bound(s => s.UserDisplayName).Title("Assigned To").Width(17);
       columns.Bound(s => s.StatusName).Title("Status").Width(14);
       columns.Bound(s => s.Icons).Title(" ").ClientTemplate("<#=Icons#>").Width(8).HtmlAttributes(new { @class = "ac" }).Sortable(false);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(20).Sortable(false);
       columns.Bound(s => s.IsComplete).Visible(false);
   }).ClientEvents(c => c.OnRowDataBound("Schedule.Center.ActivityRowBind")).DataBinding(dataBinding => dataBinding.Ajax().Select("CenterActivitiesGrid", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline })).Sortable().Scrollable().Footer(false).Render(); %>