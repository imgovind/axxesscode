﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PDCalendarViewData>" %>
    <%  if (Model!= null ) { %>
        <%  var scheduleEvents = Model.Schedules ?? new List<PDScheduleEvent>(); %>
        <% var startdate = Model.StartDate; %>
        <% var enddate =  DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year); ; %>
        <% var currentdate = startdate.AddDays(-(int)startdate.DayOfWeek); %>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "PDScheduleMonthly_PatientID" })%>
        <%= Html.Hidden("StartDate", Model.StartDate, new { @class = "current-view-startDate" })%>
        <div class="trical">
          <div class="calhead">
             <table>
                <thead>
                        <tr>
                            <td class="caltitle" colspan="2">
                                     <span class="button  fl">
                                        <a class="navigate previous-month" date="<%= startdate.AddDays(-1) %>" ><span class="largefont">&#8617;</span>Previous </a>
                                    </span>
                            </td>
                            <td colspan="3" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate)%></td>
                            <td class="caltitle" colspan="2">
                                     <span class="button  fr">
                                        <a class="navigate next-month" date="<%= enddate.AddDays(1) %>" >Next<span class="largefont">&#8618;</span> </a>
                                    </span>
                            </td>
                        </tr>
                        <tr>
                            <th>Sun</th>
                            <th>Mon</th>
                            <th>Tue</th>
                            <th>Wed</th>
                            <th>Thu</th>
                            <th>Fri</th>
                            <th>Sat</th>
                        </tr>
                    </thead>
             </table>
          </div>
            <div class="pdcal">
                <table>
                   
                    <tbody>
                        <%  var maxWeek = DateUtilities.Weeks(startdate.Month, startdate.Year); %>
                        <%  for (int i = 0; i <= maxWeek; i++) { %>
                        <tr>
                            <%  string tooltip = ""; %>
                            <%  int addedDate = (i) * 7; %>
                            <%  if ( currentdate.AddDays(addedDate) <= enddate ) for (int j = 0; j <= 6; j++) { %>
                                <%  var specificDate = currentdate.AddDays(j + addedDate); %>
                                <%  if (specificDate < startdate || specificDate > enddate ) { %>
                                <td class="inactive"></td>
                                <%  } else { %>
                                    <%  var events = scheduleEvents.FindAll(e => e.STimeIn.Date == specificDate.Date); %>
                                    <%  var count = events.Count; %>
                                    <%  if (count > 1) { %>
                                        <%  var allevents = "<br />"; %>
                                        <%  events.ForEach(e => { allevents += string.Format("{0} - <em>{1}</em><br />", e.DisciplineTaskName, e.UserDisplayName); }); %>
                                        <%  tooltip = specificDate.ToShortDateString() + allevents; %>
                            <td class="multi" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');">
                                <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                            </td>
                                    <% } else if (count == 1) { %>
                                        <%  var evnt = events.First(); %>
                                        <%  var missed = (evnt.IsMissedVisit) ? "missed" : ""; %>
                                        <%  var status = evnt.Status > 0 ? (evnt.Status) : 1000; %>
                                        <%  tooltip = specificDate.ToShortDateString() + string.Format("<br />{0} - <em>{1}</em>", evnt.DisciplineTaskName, evnt.UserDisplayName.IsNotNullOrEmpty() || evnt.UserId.IsEmpty() ? evnt.UserDisplayName : UserEngine.GetName(evnt.UserId, Current.AgencyId)); %>
                            <td class="status<%= status %> scheduled <%= missed %>" onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');">
                                <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                            </td>
                                    <%  } else { %>
                                        <%  tooltip = ""; %>
                            <td onmouseover="$(this).addClass('active');" onmouseout="$(this).removeClass('active');">
                                <%= string.Format("<div class=\"datelabel\"{0}><a date=\"{2}\">{1}</a></div>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day, specificDate.ToShortDateString())%>
                            </td>
                                    <%  } %>
                                <%  } %>
                            <%  } %>
                        </tr>
                        <%  } %>
                    </tbody>
                </table>
            </div>
        </div>
            <%  } else { %>
                <%  if (!Model.IsDischarged) { %>
        <script type="text/javascript">
            $("#PDScheduleMainResult").html(
                U.MessageWarn("No Episode", "There are no episodes found for this patient.  Please add one before schedueling.")).append(
                $("<div/>").Buttons([{ Text: "Add New Episode", Click: function() { Schedule.Episode.NewModal("<%= Model.PatientId %>") } }]))
        </script>
                <%  } else { %>
        <script type="text/javascript">
            $("#PDScheduleMainResult").html(U.MessageError("No Episode", "There are no episodes found for this discharged patient."))
        </script>
                <%  } %>
            <%  } %>
