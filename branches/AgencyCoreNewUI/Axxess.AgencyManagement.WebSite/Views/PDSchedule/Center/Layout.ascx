﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">PD Schedule Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><%  Html.RenderPartial("Center/PatientFilters"); %></div>
        <div class="bottom new-patient"><%  Html.RenderPartial("Center/PatientSelector"); %></div>
    </div>
    <div id="PDScheduleMainResult" class="ui-layout-center"></div>
</div>
<%--<%  if (Model == null || Model.Count == 0) { %>
<script type="text/javascript">
    $("#PDScheduleMainResult").html(U.MessageWarn("No Patient Found", "There are no patients to be gound which match your search criteria.  Please try a different search."));
</script>
<%  } %>--%>