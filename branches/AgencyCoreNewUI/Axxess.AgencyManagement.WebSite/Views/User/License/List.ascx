﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%= Html.Telerik().Grid<License>().Name("EditUser_LicensesGrid").Columns(columns => {
        columns.Bound(l => l.LicenseType).ReadOnly().Sortable(false);
        columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Issue Date").Sortable(false);
        columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
        columns.Bound(l => l.AssetUrl).Title("Attachment").ReadOnly().Sortable(false);
        columns.Bound(l => l.Id).ClientTemplate("<a class=\"link\" onclick=\"User.License.Edit('<#=Id#>','" + Model.Id + "');return false\">Edit</a> | <a class=\"link\" onclick=\"User.License.Delete('<#=Id#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Width(135);
    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("LicenseGrid", "User", new { userId = Model.Id })).Sortable().Footer(false)%>
<h1 class="blue">No Licenses Found</h1>