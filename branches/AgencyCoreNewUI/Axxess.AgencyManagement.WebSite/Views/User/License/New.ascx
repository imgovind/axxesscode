﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("License/Create", "User", FormMethod.Post, new { @id = "NewUserLicense_Form" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "NewUserLicense_UserId" })%>
    <fieldset>
        <legend>New License</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewtUserLicense_LicenseType" class="fl strong">License Type</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", string.Empty, new { @id = "NewUserLicense_LicenseType", @class = "required notzero" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewUserLicense_LicenseTypeMore">
                    <label for="NewUserLicense_OtherLicenseType"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", "", new { @id = "NewUserLicense_OtherLicenseType", @maxlength = "25" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUserLicense_InitiationDate" class="fl strong">Initiation Date</label>
                <div class="fr"><%= Html.TextBox("InitiationDate", string.Empty, new { @id = "NewUserLicense_InitiationDate", @class = "date-picker required" })%></div>
            </div>
            <div class="row">
                <label for="NewUserLicense_ExpirationDate" class="fl strong">Expiration Date</label>
                <div class="fr"><%= Html.TextBox("ExpirationDate", string.Empty, new { @id = "NewUserLicense_ExpirationDate", @class = "date-picker required" })%></div>
            </div>
            <div class="row">
                <label for="NewUserLicense_Attachment" class="fl strong">Attachment</label>
                <div class="fr"><input type="file" name="Attachment" id="NewUserLicense_Attachment" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>