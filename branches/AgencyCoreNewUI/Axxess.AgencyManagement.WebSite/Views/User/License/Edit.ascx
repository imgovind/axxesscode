﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<License>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("License/Update", "User", FormMethod.Post, new { @id = "EditUserLicense_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUserLicense_Id" }) %>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditUserLicense_UserId" }) %>
    <fieldset>
        <legend>Edit License</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditUserLicense_LicenseType" class="fl strong">License Type</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", Model.LicenseType, new { @id = "EditUserLicense_LicenseType", @class = "required notzero" })%></div>
                <div class="clr"></div>
                <div class="fr" id="EditUserLicense_LicenseTypeMore">
                    <label for="EditUserLicense_OtherLicenseType"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", Model.OtherLicenseType, new { @id = "EditUserLicense_OtherLicenseType", @maxlength = "25" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUserLicense_InitiationDate" class="fl strong">Initiation Date</label>
                <div class="fr"><%= Html.TextBox("InitiationDate", Model.InitiationDateFormatted, new { @id = "EditUserLicense_InitiationDate", @class = "date-picker required" })%></div>
            </div>
            <div class="row">
                <label for="EditUserLicense_ExpirationDate" class="fl strong">Expiration Date</label>
                <div class="fr"><%= Html.TextBox("ExpirationDate", Model.ExpirationDateFormatted, new { @id = "EditUserLicense_ExpirationDate", @class = "date-picker required" })%></div>
            </div>
            <%--
            <div class="row">
    <%  if (Model.AssetId.IsEmpty()) { %>
                <label for="EditUserLicense_Attachment" class="fl strong">Attachment</label>
                <div class="fr"><input type="file" name="Attachment" id="EditUserLicense_Attachment" /></div>
    <%  } else { %>
                <label for="EditUserLicense_Attachment" class="fl strong">Attachment</label>
                <div class="fr">
                    <%= Model.AssetUrl %>
                    <a class="fr addrem" id="EditUserLicense_RemoveAttachment" guid="<%= Model.AssetId %>">&#0215;</a></li>
                </div>
    <%  } %>
            </div>
            --%>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>