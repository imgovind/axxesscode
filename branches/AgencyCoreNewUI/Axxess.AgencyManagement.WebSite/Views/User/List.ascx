﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%  var activeAction = string.Empty; %>
<%  var inactiveAction = string.Empty; %>
<%  var visible = false; %>
<%  if (Current.HasRight(Permissions.ManageUsers)) { %>
    <%  visible = true; %>
    <%  activeAction = "<a class=\"link\" onclick=\"User.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"User.Deactivate('<#=Id#>',$(this).closest('.window').find('.t-grid'));return false\">Deactivate</a> | <a class=\"link\" onclick=\"User.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>"; %>
    <%  inactiveAction = "<a class=\"link\" onclick=\"User.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"User.Activate('<#=Id#>',$(this).closest('.window').find('.t-grid'));return false\">Activate</a> | <a class=\"link\" onclick=\"User.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>"; %>
<%  } %>
<span class="wintitle">List Users | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="grid-stack top">
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-user">New User</a></li>
        </ul>
        <ul class="fr">
            <li><a class="export active">Excel Export (Active)</a></li>
        </ul>
    </div>
    <h1>Active Users</h1>
    <%= Html.Telerik().Grid<User>().Name("List_User").HtmlAttributes(new { @class = "top-gap bottom-bar" }).Columns(columns => {
            columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Width(20);
            columns.Bound(u => u.DisplayTitle).Title("Title").Sortable(true).Width(15);
            columns.Bound(u => u.EmailAddress).ClientTemplate("<a class=\"link\" href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(20).Sortable(true);
            columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(10);
            columns.Bound(u => u.MobilePhone).Title("Mobile").Sortable(false).Width(10);
            columns.Bound(u => u.EmploymentType).Sortable(true).Width(10);
            columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(8);
            columns.Bound(u => u.Comments).ClientTemplate("<a class=\"tooltip\" tooltip=\"<#=Comments#>\"></a>").Title("").Sortable(false).Width(3);
            columns.Bound(u => u.Id).Width(15).Sortable(false).ClientTemplate(activeAction).Visible(visible).Title("Action");
        }).ClientEvents(c => c.OnRowDataBound("User.ListUserRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "User", new { status = (int)UserStatus.Active })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
    <div class="grid-stack bottom">
    <div class="buttons">
        <ul class="fr">
            <li><a class="export inactive">Excel Export (Inactive)</a></li>
        </ul>
    </div>
    <h1>Inactive Users</h1>
    <%= Html.Telerik().Grid<User>().Name("List_UserInactive").HtmlAttributes(new { @class = "top-gap bottom-bar" }).Columns(columns => {
            columns.Bound(u => u.DisplayName).Title("Name").Sortable(true).Width(20);
            columns.Bound(u => u.TitleType).Title("Title").Sortable(true).Width(15);
            columns.Bound(u => u.EmailAddress).ClientTemplate("<a class=\"link\" href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(20).Sortable(true);
            columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(10);
            columns.Bound(u => u.MobilePhone).Title("Mobile").Sortable(false).Width(10);
            columns.Bound(u => u.EmploymentType).Sortable(true).Width(10);
            columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(8);
            columns.Bound(u => u.Comments).ClientTemplate("<a class=\"tooltip\" tooltip=\"<#=Comments#>\"></a>").Title("").Sortable(false).Width(3);
            columns.Bound(u => u.Id).Width(15).Sortable(false).ClientTemplate(inactiveAction).Visible(visible).Title("Action");
        }).ClientEvents(c => c.OnRowDataBound("User.ListUserRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "User", new { status = (int)UserStatus.Inactive })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
    </div>
</div>