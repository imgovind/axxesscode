﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
    <fieldset>
        <legend>Licenses</legend>
        <div class="wide column">
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-user-license" status="New User License">New User License</a></div>
            </div>
            <div class="row">
                <% Html.RenderPartial("License/List", Model); %>
            </div>
        </div>
    </fieldset>
</div>