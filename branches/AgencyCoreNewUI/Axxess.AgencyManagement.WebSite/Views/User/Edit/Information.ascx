﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "User", FormMethod.Post, new { @id = "EditUser_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUser_Id" }) %>
    <fieldset>
        <legend>User Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditUser_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditUser_FirstName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditUser_LastName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_Suffix" class="fl strong">Suffix</label>
                <div class="fr"><%= Html.TextBox("Suffix", Model.Suffix, new { @id = "EditUser_Suffix", @maxlength = "50", @class = "" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_Credentials" class="fl strong">Credentials</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", Model.Credentials, new { @id = "EditUser_Credentials", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="EditUser_CredentialsMore">
                    <label for="EditUser_OtherCredentials"><em>(Specify)</em></label>
                    <%= Html.TextBox("CredentialsOther", Model.CredentialsOther, new { @id = "EditUser_OtherCredentials", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUser_TitleType" class="fl strong">Title</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", Model.TitleType, new { @id = "EditUser_TitleType", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="EditUser_TitleTypeMore">
                    <label for="EditUser_OtherTitleType"><em>(Specify)</em></label>
                    <%= Html.TextBox("TitleTypeOther", Model.TitleTypeOther, new { @id = "EditUser_OtherTitleType", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Employment Type</label>
                <div class="fr radio">
                    <%= Html.RadioButton("EmploymentType", "Employee", new { @id = "EditUser_EmploymentType_E", @class = "required" })%>
                    <label for="EditUser_EmploymentType_E" class="fixed">Employee</label>
                    <%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "EditUser_EmploymentType_C", @class = "required" })%>
                    <label for="EditUser_EmploymentType_C" class="fixed">Contractor</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditUser_CustomId" class="fl strong">Agency Custom Employee Id</label>
                <div class="fr"> <%= Html.TextBox("CustomId", Model.CustomId, new { @id = "EditUser_CustomId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_LocationId" class="fl strong">Agency Branch</label>
                <div class="fr"> <%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "EditUser_LocationId" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "EditUser_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "EditUser_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "EditUser_AddressCity", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditUser_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "EditUser_AddressStateCode", @class = "state" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "EditUser_AddressZipCode", @class = "numeric zip", @size = "5", @maxlength = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUser_HomePhoneArray1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length >= 3 ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "EditUser_HomePhoneArray1", @class = "numeric phone-short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length >= 6 ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "EditUser_HomePhoneArray2", @class = "numeric phone-short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length >= 10 ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "EditUser_HomePhoneArray3", @class = "numeric phone-long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditUser_MobilePhoneArray1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneMobile.Length >= 3 ? Model.Profile.PhoneMobile.Substring(0, 3) : "", new { @id = "EditUser_MobilePhoneArray1", @class = "numeric digits phone-short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneMobile.Length >= 6 ? Model.Profile.PhoneMobile.Substring(3, 3) : "", new { @id = "EditUser_MobilePhoneArray2", @class = "numeric digits phone-short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneMobile.Length >= 10 ? Model.Profile.PhoneMobile.Substring(6, 4) : "", new { @id = "EditUser_MobilePhoneArray3", @class = "numeric digits phone-long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <div class="wide column">
            <div class="row">
                <%  string[] roles = Model.Roles.IsNotNullOrEmpty() ? Model.Roles.Split(';') : new string[] {};  %>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_1' type='checkbox' value='1' name='AgencyRoleList' class='required' {0} />", roles.Contains("1").ToChecked())%>
                        <label for="EditUser_Role_1">Administrator</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_2' type='checkbox' value='2' name='AgencyRoleList' class='required' {0} />", roles.Contains("2").ToChecked())%>
                        <label for="EditUser_Role_2">Director of Nursing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_3' type='checkbox' value='3' name='AgencyRoleList' class='required' {0} />", roles.Contains("3").ToChecked())%>
                        <label for="EditUser_Role_3">Case Manager</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_4' type='checkbox' value='4' name='AgencyRoleList' class='required' {0} />", roles.Contains("4").ToChecked())%>
                        <label for="EditUser_Role_4">Nursing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_5' type='checkbox' value='5' name='AgencyRoleList' class='required' {0} />", roles.Contains("5").ToChecked())%>
                        <label for="EditUser_Role_5">Clerk (non-clinical)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_6' type='checkbox' value='6' name='AgencyRoleList' class='required' {0} />", roles.Contains("6").ToChecked())%>
                        <label for="EditUser_Role_6">Physical Therapist</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_7' type='checkbox' value='7' name='AgencyRoleList' class='required' {0} />", roles.Contains("7").ToChecked())%>
                        <label for="EditUser_Role_7">Occupational Therapist</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_8' type='checkbox' value='8' name='AgencyRoleList' class='required' {0} />", roles.Contains("8").ToChecked())%>
                        <label for="EditUser_Role_8">Speech Therapist</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_9' type='checkbox' value='9' name='AgencyRoleList' class='required' {0} />", roles.Contains("9").ToChecked())%>
                        <label for="EditUser_Role_9">Medical Social Worker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_10' type='checkbox' value='10' name='AgencyRoleList' class='required' {0} />", roles.Contains("10").ToChecked())%>
                        <label for="EditUser_Role_10">Home Health Aide</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_11' type='checkbox' value='11' name='AgencyRoleList' class='required' {0} />", roles.Contains("11").ToChecked())%>
                        <label for="EditUser_Role_11">Scheduler</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_12' type='checkbox' value='12' name='AgencyRoleList' class='required' {0} />", roles.Contains("12").ToChecked())%>
                        <label for="EditUser_Role_12">Biller</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_13' type='checkbox' value='13' name='AgencyRoleList' class='required' {0} />", roles.Contains("13").ToChecked())%>
                        <label for="EditUser_Role_13">Quality Assurance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_14' type='checkbox' value='14' name='AgencyRoleList' class='required' {0} />", roles.Contains("14").ToChecked())%>
                        <label for="EditUser_Role_14">Physician</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_15' type='checkbox' value='15' name='AgencyRoleList' class='required' {0} />", roles.Contains("15").ToChecked())%>
                        <label for="EditUser_Role_15">Office Manager</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_16' type='checkbox' value='16' name='AgencyRoleList' class='required' {0} />", roles.Contains("16").ToChecked())%>
                        <label for="EditUser_Role_16">Community Liason Officer/Marketer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_17' type='checkbox' value='17' name='AgencyRoleList' class='required' {0} />", roles.Contains("17").ToChecked())%>
                        <label for="EditUser_Role_17">External Referral Source</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditUser_Role_18' type='checkbox' value='18' name='AgencyRoleList' class='required' {0} />", roles.Contains("18").ToChecked())%>
                        <label for="EditUser_Role_18">Driver/Transportation</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Access &#38; Restrictions</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("AllowWeekendAccess", Model.AllowWeekendAccess, new { @id = "EditUser_AllowWeekendAccess" })%>
                        <label for="EditUser_AllowWeekendAccess" class="strong">Allow Weekend Access</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="EditUser_EarliestLoginTime" class="fl strong">Earliest Login Time</label>
                <div class="fr"><input value="<%= Model.EarliestLoginTime %>" type="text" size="10" id="EditUser_EarliestLoginTime" name="EarliestLoginTime" /></div>
            </div>
            <div class="row">
                <label for="EditUser_AutomaticLogoutTime" class="fl strong">Automatic Logout Time</label>
                <div class="fr"><input value="<%= Model.AutomaticLogoutTime %>" type="text" size="10" id="EditUser_AutomaticLogoutTime" name="AutomaticLogoutTime" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="EditUser_Comments" name="Comments" maxcharacters="500" class="taller"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>