﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdatePermissions", "User", FormMethod.Post, new { @id = "EditUserPermissions_Form" })) { %>
    <%= Html.Hidden("UserId", Model.Id, new { @id = "EditUser_UserId" }) %>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="EditUser_AllPermissions" type="checkbox" class="strong" value="" />
                        <label for="EditUser_AllPermissions">Select all Permissions</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
             <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
             <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
             <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
             <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
         </div>
         <div class="column">
             <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
             <%= Html.PermissionList("QA", Model.PermissionsArray)%>
             <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
             <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
         </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>