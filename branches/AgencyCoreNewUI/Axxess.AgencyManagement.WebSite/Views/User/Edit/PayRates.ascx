﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
    <fieldset>
        <legend>Pay Rates</legend>
        <div class="wide column">
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-pay-rate" status="New Pay Rate">New Pay Rate</a></div>
            </div>
            <div class="row">
                <% Html.RenderPartial("PayRate/List", Model); %>
            </div>
        </div>
    </fieldset>
</div>