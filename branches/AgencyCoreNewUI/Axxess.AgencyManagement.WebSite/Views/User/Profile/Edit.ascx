﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<span class="wintitle">Edit Profile | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Profile/Update", "User", FormMethod.Post, new { @id = "UserProfileEdit_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "UserProfileEdit_Id" })%>
    <fieldset class="half float-left">
        <legend>Login Password</legend>
        <div class="column">
            <div class="row">
                <label for="UserProfileEdit_CurrentPassword" class="fl strong">Current Password</label>
                <div class="fr"><%= Html.Password("PasswordChanger.CurrentPassword", string.Empty, new { @id = "UserProfileEdit_CurrentPassword", @maxlength = "15" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_NewPassword" class="fl strong">New Password</label>
                <div class="fr"><%= Html.Password("PasswordChanger.NewPassword", string.Empty, new { @id = "UserProfileEdit_NewPassword", @maxlength = "15" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_NewPasswordConfirm" class="fl strong">Confirm New Password</label>
                <div class="fr"><%= Html.Password("PasswordChanger.NewPasswordConfirm", string.Empty, new { @id = "UserProfileEdit_NewPasswordConfirm", @maxlength = "15" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="UserProfileEdit_CurrentSignature" class="fl strong">Current Signature</label>
                <div class="fr"><%= Html.Password("SignatureChanger.CurrentSignature", string.Empty, new { @id = "UserProfileEdit_CurrentSignature", @maxlength = "15" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_NewSignature" class="fl strong">New Signature</label>
                <div class="fr"><%= Html.Password("SignatureChanger.NewSignature", string.Empty, new { @id = "UserProfileEdit_NewSignature", @maxlength = "15" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_NewSignatureConfirm" class="fl strong">Confirm New Signature</label>
                <div class="fr"><%= Html.Password("SignatureChanger.NewSignatureConfirm", string.Empty, new { @id = "UserProfileEdit_NewSignatureConfirm", @maxlength = "15" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row">
                <label for="UserProfileEdit_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "UserProfileEdit_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "UserProfileEdit_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "UserProfileEdit_AddressCity", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_AddressStateCode" class="fl strong">State, Zip Code</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "UserProfileEdit_AddressStateCode", @class = "state", @status = "State" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "UserProfileEdit_AddressZipCode", @class = "numeric zip", @maxlength = "9", @status = "Zip Code" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="UserProfileEdit_PhonePrimary1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(0, 3) : string.Empty, new { @id = "UserProfileEdit_PhonePrimary1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(3, 3) : string.Empty, new { @id = "UserProfileEdit_PhonePrimary2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(6, 4) : string.Empty, new { @id = "UserProfileEdit_PhonePrimary3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="UserProfileEdit_PhoneAlternate1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(0, 3) : string.Empty, new { @id = "UserProfileEdit_PhoneAlternate1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(3, 3) : string.Empty, new { @id = "UserProfileEdit_PhoneAlternate2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(6, 4) : string.Empty, new { @id = "UserProfileEdit_PhoneAlternate3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Update Profile</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>