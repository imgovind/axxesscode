﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div id="EditUser_Tabs" class="tabs vertical-tabs vertical-tabs-left">
    <ul class="vertical-tab-list strong">
        <li><a href="#EditUser_Information">User Information</a></li>
        <li><a href="#EditUser_Licenses">Licenses</a></li>
        <li><a href="#EditUser_Permissions">Permissions</a></li>
        <li><a href="#EditUser_PayRates">Pay Rates</a></li>
    </ul>
    <div id="EditUser_Information" class="general"><% Html.RenderPartial("Edit/Information", Model); %></div>
    <div id="EditUser_Licenses" class="general"></div>
    <div id="EditUser_Permissions" class="general"></div>
    <div id="EditUser_PayRates" class="general"></div>
</div>