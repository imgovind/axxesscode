﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<span class="wintitle">My Monthly Calendar | <%= Current.AgencyName.Clean() %></span>
<%  if (Model != null) { %>
    <%  if (Model.UserEvents != null) { %>
        <%  var userEvents = Model.UserEvents; %>
        <%  var startdate = Model.FromDate; %>
        <%  var enddate = Model.ToDate; %>
        <%  var currentdate = startdate.AddDays(-(int)startdate.DayOfWeek); %>
<div class="wrapper main">
    <span class="button top left">
        <a status="<%= string.Format("Navigate to {0:MMMM} {0:yyyy} Calendar", startdate.AddMonths(-1)) %>" month="<%= Model.FromDate.AddDays(-1).Month%>" year="<%=Model.FromDate.AddDays(-1).Year %>"><span class="largefont">&#8617;</span><%= string.Format("{0:MMMM} {0:yyyy}", startdate.AddMonths(-1))%></a>
    </span>
    <span class="button top right">
        <a status="<%= string.Format("Navigate to {0:MMMM} {0:yyyy} Calendar", startdate.AddMonths(1)) %>" month="<%= Model.ToDate.AddDays(1).Month%>" year="<%=Model.ToDate.AddDays(1).Year %>"><%= string.Format("{0:MMMM} {0:yyyy}", startdate.AddMonths(1))%><span class="largefont">&#8618;</span></a>
    </span>
    <div class="ac">
        <h1><%= Current.UserFullName %></h1>
        <div class="buttons inline">
            <ul>
                <li><a status="Refresh Calendar Data" month="<%= Model.FromDate.Month %>" year="<%= Model.FromDate.Year %>">Refresh</a></li>
                <li><a class="print" status="Print Calendar" date="<%= startdate %>">Print</a></li>
            </ul>
        </div>
        <br />
        <div class="cal big">
            <table>
                <thead>
                    <tr>
                        <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate)%></td>
                    </tr>
                    <tr>
                        <th>Sunday</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                    </tr>
                </thead>
                <tbody>
        <%  var weekNumber = DateUtilities.Weeks(startdate.Month, startdate.Year); %>
        <%  for (int i = 0; i <= weekNumber; i++) { %>
                    <tr>
            <%  string tooltip = ""; %>
            <%  int addedDate = (i) * 7; %>
            <%  for (int j = 0; j <= 6; j++) { %>
                <%  var specificDate = currentdate.AddDays(j + addedDate); %>
                <%  if (specificDate.Date < startdate.Date || specificDate.Date > enddate.Date) { %>
                        <td class="inactive ui-droppable"></td>
                <%  } else { %>
                    <%  var events = userEvents.FindAll(e => e.VisitDate.IsValid() && e.VisitDate.Date == specificDate.Date); %>
                    <%  var count = events.Count; %>
                    <%  if (count > 1) { %>
                        <%  var allevents = "<br />"; %>
                        <%  events.ForEach(e => { allevents += string.Format("{0}- <em>{1}</em><br />", e.TaskName, e.PatientName.Clean()); }); %>
                        <td class="multi ui-droppable" date="<%= specificDate %>">
                        <%  tooltip = specificDate.ToShortDateString() + allevents; %>
                    <%  } else if (count == 1) { %>
                        <%  var evnt = events.First(); %>
                        <%  var missed = (evnt.IsMissedVisit) ? "missed" : string.Empty; %>
                        <%  var status = evnt.Status != null ? evnt.Status : 1000; %>
                        <td class="status<%= status %> scheduled <%= missed %> ui-droppable" date="<%= specificDate %>">
                        <%  tooltip = specificDate.ToShortDateString() + string.Format("<br />{0}- <em>{1}</em>", evnt.TaskName, evnt.PatientName.Clean()); %>
                    <%  } else { %>
                        <td class="ui-droppable" date="<%= specificDate %>">
                        <%  tooltip = ""; %>
                    <%  } %>
                    <%  var dayOnClick = Current.HasRight(Permissions.ScheduleVisits) ? string.Format("Schedule.Add('{0}');", specificDate.ToShortDateString()) : string.Empty; %>
                            <%= string.Format("<div class=\"datelabel\" {0}>{1}</div></td>", tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day)%>
                <%  } %>
            <%  } %>
                    </tr>
        <%  } %>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="ac">
    <fieldset class="legend">
        <ul>
            <li>
                <div class="scheduled">&#160;</div>
                Scheduled
            </li>
            <li>
                <div class="completed">&#160;</div>
                Completed
            </li>
            <li>
                <div class="multi">&#160;</div>
                Multiple
            </li>
        </ul>
    </fieldset>
</div>
<div class="clear"></div>
<%  Html.RenderPartial("Calendar/List", Model); %>
    <%  } %>
<%  } %>