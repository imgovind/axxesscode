﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserCalendarViewData>" %>
<%= Html.Telerik().Grid(Model.UserEvents).Name("List_UserCalander_Schedule").HtmlAttributes(new { @class = "position-relative args" }).Columns(columns => {
        columns.Bound(v => v.PatientName);
        columns.Bound(v => v.Url).Template(v => v.Url).ClientTemplate("<#=Url#>").Title("Task").Width(250);
        columns.Bound(v => v.VisitDate).Format("{0:MM/dd/yyyy}").Title("Date").Width(100);
        columns.Bound(v => v.StatusName).Width(200).Title("Status");
        columns.Bound(v => v.StatusComment).Title(" ").Width(30).Template(v => v.StatusComment.IsNotNullOrEmpty() ? string.Format("<a class=\"sticky-note-icon red\" tooltip=\"{0}\"></a>", v.StatusComment) : string.Empty);
        columns.Bound(v => v.VisitNotes).Title(" ").Width(30).Template(v => v.VisitNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"sticky-note-icon\" tooltip=\"{0}\"></a>", v.VisitNotes) : string.Empty);
        columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).Template(v => v.EpisodeNotes.IsNotNullOrEmpty() ? string.Format("<a class=\"sticky-note-icon blue\" tooltip=\"{0}\"></a>", v.EpisodeNotes) : string.Empty);
        columns.Bound(v => v.Id).Template(v => v.IsMissedVisitReady ? string.Format("<a onclick=\"Schedule.MissedVisit('{0}','{1}','{2}');return false\">Missed Visit Form</a>", v.EpisodeId, v.PatientId, v.Id) : string.Empty).Title(" ").Width(150);
    }).ClientEvents(c => c.OnRowDataBound("U.StickyNoteInit")).Footer(false).Sortable(sorting =>
        sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
            if (sortName == "PatientName") {
                if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
            } else if (sortName == "VisitDate") {
                if (sortDirection == "ASC") order.Add(o => o.VisitDate).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.VisitDate).Descending();
            } else if (sortName == "TaskName") {
                if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
            }
        })
    ) %>