﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Reset <%= Current.DisplayName %>&#8217;s Signature | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <fieldset>
        <legend>Reset Signature</legend>
        <div class="wide column">
            <div class="row">
                Click on the button below to reset your signature. An e-mail with instructions on how to reset your signature
                will be sent to <%= Model %>.
            </div>
            <div id="resetSignatureMessage" class="error-message"></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="reset" status="Send Reset Instructions to my E-mail">Reset Signature</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>



