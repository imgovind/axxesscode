﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("PayRate/Create", "User", FormMethod.Post, new { @id = "NewPayRate_Form" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "NewPayRate_UserId" })%>
    <fieldset>
        <legend>New Rate</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewPayRate_Task" class="fl strong">Task</label>
                <div class="fr"><%= Html.UserDisciplineTask("Id", 0, Model, true, new { @id = "NewPayRate_Task", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewPayRate_VisitRate" class="fl strong">Visit Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", string.Empty, new { @id = "NewPayRate_VisitRate", @class = "decimal" })%></div>
            </div>
             <div class="row">
                <label for="New_UserRate_MileageRate" class="fl strong">Mileage Rate</label>
                <div class="fr"><%= Html.TextBox("MileageRate", string.Empty, new { @id = "NewPayRate_MileageRate", @class = "decimal" })%></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save &#38; Exit</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
