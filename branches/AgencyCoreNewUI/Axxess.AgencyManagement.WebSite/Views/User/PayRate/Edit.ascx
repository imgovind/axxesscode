﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("PayRate/Update", "User", FormMethod.Post, new { @id = "EditPayRate_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditPayRate_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditPayRate_UserId" })%>
    <fieldset>
        <legend>Edit Rate</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditPayRate_Task" class="fl strong">Task</label>
                <div class="fr"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label for="EditPayRate_VisitRate" class="fl strong">Visit Rate</label>
                <div class="fr"><%= Html.TextBox("Rate", Model.Rate, new { @id = "EditPayRate_VisitRate", @class = "decimal" })%></div>
            </div>
            <div class="row">
                <label for="EditPayRate_MileageRate" class="fl strong">Mileage Rate</label>
                <div class="fr"><%= Html.TextBox("MileageRate", Model.MileageRate, new { @id = "EditPayRate_MileageRate", @class = "decimal" })%></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>