﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%= Html.Telerik().Grid<UserRate>().Name("EditUser_PayRateGrid").Columns(columns => {
        columns.Bound(e => e.DisciplineTaskName).Title("Task").Sortable(true).Width(5);
        columns.Bound(e => e.Rate).Format("${0:#0.00}").Title("Visit Rate").Width(1).Sortable(true);
        columns.Bound(e => e.MileageRate).Format("${0:#0.00}").Title("Mileage Rate").Width(1).Sortable(true);
        columns.Bound(e => e.Id).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"User.PayRate.Edit('<#=Id#>','" + Model.Id + "');return false\">Edit</a> | <a class=\"link\" onclick=\"User.PayRate.Delete('<#=Id#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Width(2);
    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("PayRateGrid", "User", new { userId = Model.Id })).Sortable().Footer(false)%>
<h1 class="blue">No Pay Rates Found</h1>