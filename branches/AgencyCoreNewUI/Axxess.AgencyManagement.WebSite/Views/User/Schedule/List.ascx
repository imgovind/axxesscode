﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<%= Html.Hidden("Sort", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]), new { @id = "UserSchedule_Sort" }) %>
<%= Html.Telerik().Grid<UserVisit>().Name("User_Schedule").HtmlAttributes(new { @class = "bottom-bar args"}).Columns(columns => {
        columns.Bound(v => v.PatientName).Width("18%");
        columns.Bound(v => v.TaskName).ClientTemplate("<#=Url#>").Title("Task").Width("20%");
        columns.Bound(v => v.VisitDate).Title("Date").Width("10%");
        columns.Bound(v => v.StatusName).Width("25%").Title("Status");
        columns.Bound(v => v.StatusComment).Title(" ").Width("3%").ClientTemplate("<a class=\"sticky-note-icon red\" tooltip=\"<#=StatusComment#>\"></a>");
        columns.Bound(v => v.VisitNotes).Title(" ").Width("3%").ClientTemplate("<a class=\"sticky-note-icon\" tooltip=\"<#=VisitNotes#>\"></a>");
        columns.Bound(v => v.EpisodeNotes).Title(" ").Width("3%").ClientTemplate("<a class=\"sticky-note-icon blue\" tooltip=\"<#=EpisodeNotes#>\"></a>");
        columns.Bound(v => v.Id).ClientTemplate("<a onclick=\"UserInterface.ShowMissedVisitModal('<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');return false\" class=\"link\">Missed Visit Form</a>").Title(" ").Width("15%");
    }).Groupable(settings => settings.Groups(groups => {
        var data = ViewData["UserScheduleGroupName"].ToString();
        if (data == "PatientName") groups.Add(s => s.PatientName);
        else if (data == "VisitDate") groups.Add(s => s.VisitDate);
        else if (data == "TaskName") groups.Add(s => s.TaskName);
        else groups.Add(s => s.VisitDate);
    })).ClientEvents(c => c.OnDataBound("ToolTip.InitNote")).DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleGrid", "User")).Scrollable().Sortable()%>