﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName %>&#8217;s Schedule and Tasks | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a status="Refresh Grid" class="grid-refresh">Refresh</a></li></ul>
        <br />
        <ul><li><a status="Export Grid as Spreadsheet (Excel Format)" class="excel-export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls">
        <div class="buttons">
            <ul>
                <li><a status="Organize Grid by Patient" class="sort">Group By Patient</a></li>
                <li><a status="Organize Grid by Date" class="sort">Group By Date</a></li>
                <li><a status="Organize Grid by Type of Task" class="sort">Group By Task</a></li>
            </ul>
        </div>
        <div class="ac">
            <em>Note: This list shows you items/tasks dated 3 months into the past and 2 weeks into the future. To find older items, look in the Patient's Chart or Schedule Center.</em>
        </div>
    </fieldset>
    <div class="clr"></div>
    <div class="content"><% Html.RenderPartial("Schedule/List", Model); %></div>
</div>