﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<bool>" %>
<span class="wintitle">New User | <%= Current.AgencyName %></span>
<div id="NewUser_Content" class="wrapper main">
<%  using (Html.BeginForm("Create", "User", FormMethod.Post, new { @id = "NewUser_Form" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewUser_EmailAddress" class="fl strong">E-mail Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewUser_EmailAddress", @class = "email required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", "", new { @id = "NewUser_FirstName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", "", new { @id = "NewUser_LastName", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_Credentials" class="fl strong">Credentials</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", "", new { @id = "NewUser_Credentials", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewUser_CredentialsMore">
                    <label for="NewUser_OtherCredentials"><em>(Specify)</em></label>
                    <%= Html.TextBox("CredentialsOther", "", new { @id = "NewUser_OtherCredentials", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_TitleType" class="fl strong">Title</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", "", new { @id = "NewUser_TitleType", @class = "required" })%></div>
                <div class="clr"></div>
                <div class="fr" id="NewUser_TitleTypeMore">
                    <label for="NewUser_OtherTitleType"><em>(Specify)</em></label>
                    <%= Html.TextBox("TitleTypeOther", "", new { @id = "NewUser_OtherTitleType", @maxlength = "20" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Employment Type</label>
                <div class="fr radio">
                    <%= Html.RadioButton("EmploymentType", "Employee", new { @id = "NewUser_EmploymentType_E", @class = "required" })%>
                    <label for="NewUser_EmploymentType_E" class="fixed">Employee</label>
                    <%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "NewUser_EmploymentType_C", @class = "required" })%>
                    <label for="NewUser_EmploymentType_C" class="fixed">Contractor</label>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_DOB" class="fl strong">DOB</label>
                <div class="fr"><input type="text" class="date-picker" name="DOB"  id="NewUser_DOB" /></div>
            </div>
            <div class="row">
                <label for="NewUser_SSN" class="fl strong">SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", "", new { @id = "NewUser_SSN", @class = "numeric ssn", @maxlength = "9" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewUser_CustomId" class="fl strong">Agency Custom Employee Id</label>
                <div class="fr"><%= Html.TextBox("CustomId", "", new { @id = "NewUser_CustomId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_LocationId" class="fl strong">Agency Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "NewUser_LocationId" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine1", "", new { @id = "NewUser_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressLine2", "", new { @id = "NewUser_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", "", new { @id = "NewUser_AddressCity", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewUser_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", "", new { @id = "NewUser_AddressStateCode", @class = "state" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", "", new { @id = "NewUser_AddressZipCode", @class = "numeric zip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_HomePhoneArray1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "NewUser_HomePhoneArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "NewUser_HomePhoneArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "NewUser_HomePhoneArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_MobilePhoneArray1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "NewUser_MobilePhoneArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "NewUser_MobilePhoneArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "NewUser_MobilePhoneArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_FaxPhoneArray1" class="fl strong">Fax Phone</label>
                <div class="fr">
                    <%= Html.TextBox("FaxPhoneArray", "", new { @id = "NewUser_FaxPhoneArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxPhoneArray", "", new { @id = "NewUser_FaxPhoneArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxPhoneArray", "", new { @id = "NewUser_FaxPhoneArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="NewUser_Role_1" type="checkbox" class="required" name="AgencyRoleList" value="1" />
                        <label for="NewUser_Role_1">Administrator</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_2" type="checkbox" class="required" name="AgencyRoleList" value="2" />
                        <label for="NewUser_Role_2">Director of Nursing</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_3" type="checkbox" class="required" name="AgencyRoleList" value="3" />
                        <label for="NewUser_Role_3">Case Manager</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_4" type="checkbox" class="required" name="AgencyRoleList" value="4" />
                        <label for="NewUser_Role_4">Nursing</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_5" type="checkbox" class="required" name="AgencyRoleList" value="5" />
                        <label for="NewUser_Role_5">Clerk (non-clinical)</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_6" type="checkbox" class="required" name="AgencyRoleList" value="6" />
                        <label for="NewUser_Role_6">Physical Therapist</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_7" type="checkbox" class="required" name="AgencyRoleList" value="7" />
                        <label for="NewUser_Role_7">Occupational Therapist</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_8" type="checkbox" class="required" name="AgencyRoleList" value="8" />
                        <label for="NewUser_Role_8">Speech Therapist</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_9" type="checkbox" class="required" name="AgencyRoleList" value="9" />
                        <label for="NewUser_Role_9">Medical Social Worker</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_10" type="checkbox" class="required" name="AgencyRoleList" value="10" />
                        <label for="NewUser_Role_10">Home Health Aide</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_11" type="checkbox" class="required" name="AgencyRoleList" value="11" />
                        <label for="NewUser_Role_11">Scheduler</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_12" type="checkbox" class="required" name="AgencyRoleList" value="12" />
                        <label for="NewUser_Role_12">Biller</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_13" type="checkbox" class="required" name="AgencyRoleList" value="13" />
                        <label for="NewUser_Role_13">Quality Assurance</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_14" type="checkbox" class="required" name="AgencyRoleList" value="14" />
                        <label for="NewUser_Role_14">Physician</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_15" type="checkbox" class="required" name="AgencyRoleList" value="15" />
                        <label for="NewUser_Role_15">Office Manager</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_16" type="checkbox" class="required" name="AgencyRoleList" value="16" />
                        <label for="NewUser_Role_16">Community Liason Officer/Marketer</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_17" type="checkbox" class="required" name="AgencyRoleList" value="17" />
                        <label for="NewUser_Role_17">External Referral Source</label>
                    </div>
                    <div class="option">
                        <input id="NewUser_Role_18" type="checkbox" class="required" name="AgencyRoleList" value="18" />
                        <label for="NewUser_Role_18">Driver/Transportation</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="NewUser_AllPermissions" type="checkbox" class="radio float-left" value="" />
                        <label for="NewUser_AllPermissions" class="radio">Select all Permissions</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
             <%= Html.PermissionList("Clerical", new List<string>())%>
             <%= Html.PermissionList("Clinical", new List<string>())%>
             <%= Html.PermissionList("OASIS", new List<string>())%>
             <%= Html.PermissionList("Schedule Management", new List<string>())%>
         </div>
         <div class="column">
             <%= Html.PermissionList("Reporting", new List<string>())%>
             <%= Html.PermissionList("QA", new List<string>())%>
             <%= Html.PermissionList("Billing", new List<string>())%>
             <%= Html.PermissionList("Administration", new List<string>())%>
         </div>
    </fieldset>
    <fieldset>
        <legend>Access &#38; Restrictions</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("AllowWeekendAccess", true, new { @id="NewUser_AllowWeekendAccess" })%>
                        <label for="NewUser_AllowWeekendAccess" class="strong">Allow Weekend Access</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="NewUser_EarliestLoginTime" class="fl strong">Earliest Login Time</label>
                <div class="fr"><input type="text" size="10" id="NewUser_EarliestLoginTime" name="EarliestLoginTime" class="time-picker" /></div>
            </div>
            <div class="row">
                <label for="NewUser_AutomaticLogoutTime" class="fl strong">Automatic Logout Time</label>
                <div class="fr"><input type="text" size="10" id="NewUser_AutomaticLogoutTime" name="AutomaticLogoutTime" class="time-picker" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="NewUser_Comments" name="Comments" maxcharacters="500" class="taller"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add User</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<%  if (Model) { %>
<script type="text/javascript">
    $("#NewUser_Content").html(U.MessageError("Maximum User Accounts Reached", "You have already created the amount of user accounts to reach the maximum number of accounts allowed for this agency. Please contact Axxess about upgrading your account."));
</script>
<%  } %>