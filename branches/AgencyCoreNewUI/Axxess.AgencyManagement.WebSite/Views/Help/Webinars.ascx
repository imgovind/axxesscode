﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<div class="wrapper main">
    <div class="trical">
    <%  DateTime[] startdate = new DateTime[3], enddate = new DateTime[3], currentdate = new DateTime[3]; %>
    <%  int advanceNotification = 14; %>
    <%  startdate[0] = DateUtilities.GetStartOfMonth(DateTime.Now.AddDays(advanceNotification).Month, DateTime.Now.AddDays(advanceNotification).Year); %>
    <%  enddate[0] = DateUtilities.GetEndOfMonth(DateTime.Now.AddDays(advanceNotification).Month, DateTime.Now.AddDays(advanceNotification).Year); %>
    <%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
    <%  startdate[1] = enddate[0].AddDays(1); %>
    <%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
    <%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
    <%  startdate[2] = enddate[1].AddDays(1); %>
    <%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
    <%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
    <%  for (int c = 0; c < 3; c++) { %>
        <div class="cal">
            <table>
                <thead>
                    <tr>
                        <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td>
                    </tr>
                    <tr>
                        <th>Su</th>
                        <th>Mo</th>
                        <th>Tu</th>
                        <th>We</th>
                        <th>Th</th>
                        <th>Fr</th>
                        <th>Sa</th>
                    </tr>
                </thead>
                <tbody>
        <%  var maxWeek = DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); %>
        <%  for (int i = 0; i <= maxWeek; i++) { %>
                       <tr>
            <%  string tooltip = string.Empty; %>
            <%  string dayOnClick = string.Empty; %>
            <%  int addedDate = (i) * 7; %>
            <%  for (int j = 0; j <= 6; j++) { %>
                <%  if (j == 2) tooltip = "Introduction to Agencycore"; %>
                <%  if (j == 3) tooltip = "Training for Patient Care Staff"; %>
                <%  if (j == 4) tooltip = "Medicare Billing Training"; %>
                <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
                <%  if (specificDate <= DateTime.Now.AddDays(advanceNotification)) { %>
                            <td class="inactive"></td>
                <%  } else { %>
                            <td class="<%= j > 1 && j < 5 ? "scheduled" : string.Empty %>">
                                <div class="datelabel" onclick="<%= dayOnClick + (tooltip.IsNotNullOrEmpty() ? "\" tooltip=\"" + tooltip : string.Empty) %>"><a><%= specificDate.Day %></a></div>
                            </td>
                <%  } %>
            <%  } %>
                    </tr>
        <%  } %>
                </tbody>
            </table>
        </div>
    <%  } %>
    </div>
    <div class="clear"></div>
    <fieldset>
        <legend>Webinars</legend>
        <div class="column">
            <div class="row">
                <label for="Webinar_FirstName" class="float-left">First Name</label>
                <div class="fr"><%= Html.TextBox("Webinar_FisrtName", Model.FirstName) %></div>
            </div>
            <div class="row">
                <label for="Webinar_FirstName" class="float-left">Last Name</label>
                <div class="fr"><%= Html.TextBox("Webinar_FisrtName", Model.LastName) %></div>
            </div>
            <div class="row">
                <label for="Webinar_FirstName" class="float-left">Agency Name</label>
                <div class="fr"><%= Html.TextBox("Webinar_FisrtName", Model.AgencyName) %></div>
            </div>
            <div class="row">
                <label for="Webinar_FirstName" class="float-left">User Name</label>
                <div class="fr"><%= Html.TextBox("Webinar_FisrtName", Model.UserName) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Webinar_FirstName" class="float-left">Email Address</label>
                <div class="fr"><%= Html.TextBox("Webinar_FisrtName", Model.EmailAddress) %></div>
            </div>
            <div class="row">
                <label for="Edit_Profile_AddressCity" class="float-left">City</label>
                <div class="fr"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "Edit_Profile_AddressCity", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Profile_AddressStateCode" class="float-left">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "Edit_Profile_AddressStateCode", @class = "state" })%>
                    <%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "Edit_Profile_AddressZipCode", @class = "text numeric input_wrapper zip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Profile_PhonePrimary1" class="float-left">Phone</label>
                <div class="fr"><%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Profile_PhonePrimary1", @class = "digits phone-short", @maxlength = "3" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Profile_PhonePrimary2", @class = "digits phone-short", @maxlength = "3" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Profile_PhonePrimary3", @class = "digits phone-long", @maxlength = "4" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="">Enroll</a></li>
            <li><a href="javascript:void(0)" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $.get("https://api.citrixonline.com/oauth/access_token?grant_type=authorization_code&code=LTczZWY0YTQ0OjEzNDViMWMzYWFmOjc3MjY=&client_id=e8d2daa035e9d59d75eddf69189a4e94");

    $(".datelabel").each(function() {
        if ($(this).attr("tooltip") != undefined) {
            $(this).tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                bodyHandler: function() { return $(this).attr("tooltip") }
            })
        }
    });
</script>
