﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCareViewData>" %>
<%  var editData = Model.EditData; %>
<%  var displayData = Model.DisplayData; %>
<%  var episodeId = displayData.ContainsKey("EpisodeId") && displayData["EpisodeId"].IsNotNullOrEmpty() ? displayData["EpisodeId"] : ""; %>
<%  var patientId = displayData.ContainsKey("PatientId") && displayData["PatientId"].IsNotNullOrEmpty() ? displayData["PatientId"].ToGuid() : Guid.Empty; %>
<%  var episodeAssociatedId = editData.AnswerOrDefault("EpisodeAssociated", episodeId); %>
<%  string[] dME = editData.AnswerArray("485DME"); %>
<%  string[] safetyMeasure = editData.AnswerArray("485SafetyMeasures"); %>
<%  string[] functionLimitations = editData.AnswerArray("485FunctionLimitations"); %>
<%  string[] activitiesPermitted = editData.AnswerArray("485ActivitiesPermitted"); %>
<%  string[] mentalStatus = editData.AnswerArray("485MentalStatus"); %>
<span class="wintitle">Plan of Treatment/Care | <%= displayData.ContainsKey("PatientName") && displayData["PatientName"].IsNotNullOrEmpty() ? displayData["PatientName"] : string.Empty %></span>
<div class="wrapper main">
<% using (Html.BeginForm("SavePlanofCareStandAlone", "Oasis", FormMethod.Post, new { @id = "New485_Form" })) { %>
    <%= Html.Hidden("Id", displayData.ContainsKey("Id") && displayData["Id"].IsNotNullOrEmpty() ? displayData["Id"] : "", new { @id = "New_485_Id" })%>
    <%= Html.Hidden("PatientId", patientId, new { @id = "New_485_PatientId" })%>
    <%= Html.Hidden("EpisodeId", episodeId, new { @id = "New_485_EpisodeId" })%>
    <fieldset>
        <legend>Patient<span class="light"> (Verify information)</span></legend>
        <div class="column">
            <div class="row">
                <label>Name</label>
                <div class="fr"><%= displayData.ContainsKey("PatientName") && displayData["PatientName"].IsNotNullOrEmpty() ? displayData["PatientName"] : "" %></div>
            </div>
            <div class="row">
                <label>HI Claim No</label>
                <div class="fr"><%= displayData.ContainsKey("PatientMedicareNumber") && displayData["PatientMedicareNumber"].IsNotNullOrEmpty() ? displayData["PatientMedicareNumber"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Gender</label>
                <div class="fr"><%= displayData.ContainsKey("PatientGender") && displayData["PatientGender"].IsNotNullOrEmpty() ? displayData["PatientGender"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Date of Birth</label>
                <div class="fr"><%= displayData.ContainsKey("PatientDoB") && displayData["PatientDoB"].IsNotNullOrEmpty() ? displayData["PatientDoB"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Start of Care Date</label>
                <div class="fr"><%= displayData.ContainsKey("PatientSoC") && displayData["PatientSoC"].IsNotNullOrEmpty() ? displayData["PatientSoC"] : string.Empty %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label>Medical Record No</label>
                <div class="fr"><%= displayData.ContainsKey("PatientIdNumber") && displayData["PatientIdNumber"].IsNotNullOrEmpty() ? displayData["PatientIdNumber"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Certifcation Period</label>
                <div class="fr"><%= Html.PatientEpisodes("GenericEpisodeAssociated", episodeAssociatedId, patientId, "-- Select Episode --", new { @id = "New_485_EpisodeList", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label>Address</label>
                <div class="fr"><%= displayData.ContainsKey("PatientAddressFirstRow") && displayData["PatientAddressFirstRow"].IsNotNullOrEmpty() ? displayData["PatientAddressFirstRow"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>&#160;</label>
                <div class="fr"><%= displayData.ContainsKey("PatientAddressSecondRow") && displayData["PatientAddressSecondRow"].IsNotNullOrEmpty() ? displayData["PatientAddressSecondRow"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Phone</label>
                <div class="fr"><%= displayData.ContainsKey("PatientPhone") && displayData["PatientPhone"].IsNotNullOrEmpty() ? displayData["PatientPhone"] : string.Empty %></div>
            </div>
        </div>
    <%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="UserInterface.ShowEditPatient('<%= displayData.ContainsKey("PatientId") && displayData["PatientId"].IsNotNullOrEmpty() ? displayData["PatientId"] : "" %>'); ">Edit Patient</a></li>
            </ul>
        </div>
    <%  } %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half float-left">
        <legend>Agency<span class="light"> (Verify information)</span></legend>
        <div class="column">
            <div class="row">
                <label>Name</label>
                <div class="fr"><%= displayData.ContainsKey("AgencyName") && displayData["AgencyName"].IsNotNullOrEmpty() ? displayData["AgencyName"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Provider No</label>
                <div class="fr"><%= displayData.ContainsKey("AgencyMedicareProviderNumber") && displayData["AgencyMedicareProviderNumber"].IsNotNullOrEmpty() ? displayData["AgencyMedicareProviderNumber"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Address</label>
                <div class="fr"><%= displayData.ContainsKey("AgencyAddressFirstRow") && displayData["AgencyAddressFirstRow"].IsNotNullOrEmpty() ? displayData["AgencyAddressFirstRow"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>&#160;</label>
                <div class="fr"><%= displayData.ContainsKey("AgencyAddressSecondRow") && displayData["AgencyAddressSecondRow"].IsNotNullOrEmpty() ? displayData["AgencyAddressSecondRow"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Phone</label>
                <div class="fr"><%= displayData.ContainsKey("AgencyPhone") && displayData["AgencyPhone"].IsNotNullOrEmpty() ? displayData["AgencyPhone"] : string.Empty %></div>
            </div>
            <div class="row">
                <label>Fax</label>
                <div class="fr"><%= displayData.ContainsKey("AgencyFax") && displayData["AgencyFax"].IsNotNullOrEmpty() ? displayData["AgencyFax"] : string.Empty %></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Physician</legend>
        <div class="column">
            <div class="row">
                <label for="PhysicianId" class="float-left">Select Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", displayData.ContainsKey("PhysicianId") && displayData["PhysicianId"].IsNotNullOrEmpty() ? displayData["PhysicianId"] : string.Empty, new { @id = "New_485_PhysicianId", @class = "physician-picker requiredphysician" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Medications (Locator #10): Dose/Frequency/Route (N)ew (C)hanged</legend>
        <div class="wide column">
            <div class="buttons float-left"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowMedicationModal('<%= displayData.ContainsKey("PatientId") && displayData["PatientId"].IsNotNullOrEmpty() ? displayData["PatientId"] : "" %>', 'true');">Add/Edit Medications</a></li></ul></div>
            <div class="row"><%= Html.TextArea("485Medications", editData.ContainsKey("485Medications") && editData["485Medications"].Answer.IsNotNullOrEmpty() ? editData["485Medications"].Answer : "", new { @id = "New_485_Medications", @style = "height: 180px; font-size: 16px;" })%></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Principal Diagnosis (Locator #11)</legend>
        <div class="wide column">
            <div class="row">
                <table class="form layout-auto align-center">
                    <thead>
                        <tr>
                            <th>Principal Diagnosis</th>
                            <th>ICD-9-C M Code</th>
                            <th>O/E</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><%= Html.TextBox("M1020PrimaryDiagnosis", editData.ContainsKey("M1020PrimaryDiagnosis") && editData["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? editData["M1020PrimaryDiagnosis"].Answer : "", new { @id = "New_485_PrincipalDiagnosis", @class = "diagnosis", @maxlength = "200" })%></td>
                            <td><%= Html.TextBox("M1020ICD9M", editData.ContainsKey("M1020ICD9M") && editData["M1020ICD9M"].Answer.IsNotNullOrEmpty() ? editData["M1020ICD9M"].Answer : "", new { @id = "New_485_PrincipalDiagnosisICD9", @class = "icd", @maxlength = "7" })%></td>
                            <td>
                                <%  var OE = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                        new SelectListItem { Text = "Onset", Value = "2" }
                                    }, "Value", "Text", editData.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? editData["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer : "0"); %>
                                <%= Html.DropDownList("485ExacerbationOrOnsetPrimaryDiagnosis", OE, new { @id = "New_485_485ExacerbationOrOnsetPrimaryDiagnosis", @title = "(M1020) Primary Diagnosis, Onset/Exacerbation", @class = "oe" })%>
                            </td>
                            <td><input type="text" class="date-picker" name="M1020PrimaryDiagnosisDate" value="<%= editData.ContainsKey("M1020PrimaryDiagnosisDate") && editData["M1020PrimaryDiagnosisDate"].Answer.IsNotNullOrEmpty() ? editData["M1020PrimaryDiagnosisDate"].Answer : string.Empty %>" id="New_485_PrimaryDiagnosisDate" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Surgical Procedure (Locator #12)</legend>
        <div class="wide column">
            <div class="row">
                <table class="form layout-auto align-center">
                    <thead>
                        <tr>
                            <th>Surgical Procedure</th>
                            <th>Code</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><%= Html.TextBox("485SurgicalProcedureDescription1", editData.ContainsKey("485SurgicalProcedureDescription1") && editData["485SurgicalProcedureDescription1"].Answer.IsNotNullOrEmpty() ? editData["485SurgicalProcedureDescription1"].Answer : "", new { @class = "procedureDiagnosis", @id = "New_485_485SurgicalProcedureDescription1", @maxlength = "200" })%></td>
                            <td><%= Html.TextBox("485SurgicalProcedureCode1", editData.ContainsKey("485SurgicalProcedureCode1") && editData["485SurgicalProcedureCode1"].Answer.IsNotNullOrEmpty() ? editData["485SurgicalProcedureCode1"].Answer : "", new { @class = "procedureICD pad", @id = "New_485_485SurgicalProcedureCode1", @maxlength = "7" })%></td>
                            <td><input type="text" class="date-picker" name="485SurgicalProcedureCode1Date" value="<%= editData.ContainsKey("485SurgicalProcedureCode1Date") && editData["485SurgicalProcedureCode1Date"].Answer.IsNotNullOrEmpty() ? editData["485SurgicalProcedureCode1Date"].Answer : string.Empty %>" id="New_485_SurgicalProcedureCode1Date" /></td>
                        </tr>
                        <tr>
                            <td><%= Html.TextBox("485SurgicalProcedureDescription2", editData.ContainsKey("485SurgicalProcedureDescription2") && editData["485SurgicalProcedureDescription2"].Answer.IsNotNullOrEmpty() ? editData["485SurgicalProcedureDescription2"].Answer : "", new { @class = "procedureDiagnosis", @id = "New_485_SurgicalProcedureDescription2", @maxlength = "200" })%></td>
                            <td><%= Html.TextBox("485SurgicalProcedureCode2", editData.ContainsKey("485SurgicalProcedureCode2") && editData["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty() ? editData["485SurgicalProcedureCode2"].Answer : "", new { @class = "procedureICD pad", @id = "New_485_SurgicalProcedureCode2", @maxlength = "7" })%></td>
                            <td><input type="text" class="date-picker" name="485SurgicalProcedureCode2Date" value="<%= editData.ContainsKey("485SurgicalProcedureCode2Date") && editData["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? editData["485SurgicalProcedureCode2Date"].Answer : string.Empty %>" id="485SurgicalProcedureCode2Date" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Other Pertinent Diagnosis (Locator #13)</legend>
        <div class="wide column">
            <div class="row">
                <table class="form layout-auto align-center">
                    <thead>
                        <tr>
                            <th>Other Pertinent Diagnosis</th>
                            <th>ICD-9-C M Code</th>
                            <th>O/E</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
    <%  for (int i = 1; i <= 14; i++) { %>
                        <tr>
                            <td><%= Html.TextBox("M1022PrimaryDiagnosis" + i, editData.ContainsKey("M1022PrimaryDiagnosis" + i) && editData["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? editData["M1022PrimaryDiagnosis" + i].Answer : "", new { @id = "New_485_OtherDiagnosis" + i, @class = "diagnosis", @maxlength = "200" })%></td>
                            <td><%= Html.TextBox("M1022ICD9M" + i, editData.ContainsKey("M1022ICD9M" + i) && editData["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? editData["M1022ICD9M" + i].Answer : "", new { @id = "New_485_OtherDiagnosisICD9" + i, @class = "icd", @maxlength = "7" })%></td>
                            <td>
                                <%  var OEOther = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                        new SelectListItem { Text = "Onset", Value = "2" }
                                    }, "Value", "Text", editData.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? editData["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer : "0"); %>
                                <%= Html.DropDownList("485ExacerbationOrOnsetPrimaryDiagnosis" + i, OEOther, new { @id = "New_485_485ExacerbationOrOnsetPrimaryDiagnosis" + i, @title = "Diagnosis, Onset/Exacerbation", @class = "oe" })%>
                            </td>
                            <td><input type="text" class="date-picker" name="M1022PrimaryDiagnosis<%= i %>Date" value="<%= editData.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") && editData["M1022PrimaryDiagnosis" + i + "Date"].Answer.IsNotNullOrEmpty() ? editData["M1022PrimaryDiagnosis" + i + "Date"].Answer : string.Empty %>" id="New_485_OtherDiagnosisDate<%= i %>" /></td>
                        </tr>
    <%  } %>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME and Supplies (Locator #14)</legend>
        <input type="hidden" name="485DME" value="" />
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <%= string.Format("<input id='485DME1' class='radio float-left' name='485DME' value='1' type='checkbox' {0} />", dME != null && dME.Contains("1") ? "checked='checked'" : "") %>
                        <label for="485DME1">Bedside Commode</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485DME2' class='radio float-left' name='485DME' value='2' type='checkbox' {0} />", dME != null && dME.Contains("2") ? "checked='checked'" : "") %>
                        <label for="485DME2">Cane</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485DME3' class='radio float-left' name='485DME' value='3' type='checkbox' {0} />", dME != null && dME.Contains("3") ? "checked='checked'" : "") %>
                        <label for="485DME3">Elevated Toilet Seat</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485DME4' class='radio float-left' name='485DME' value='4' type='checkbox' {0} />", dME != null && dME.Contains("4") ? "checked='checked'" : "") %>
                        <label for="485DME4">Grab Bars</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485DME5' class='radio float-left' name='485DME' value='5' type='checkbox' {0} />", dME != null && dME.Contains("5") ? "checked='checked'" : "") %>
                        <label for="485DME5">Hospital Bed</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485DME6' class='radio float-left' name='485DME' value='6' type='checkbox' {0} />", dME != null && dME.Contains("6") ? "checked='checked'" : "") %>
                        <label for="485DME6">Nebulizer</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485DME7' class='radio float-left' name='485DME' value='7' type='checkbox' {0} />", dME != null && dME.Contains("7") ? "checked='checked'" : "") %>
                        <label for="485DME7">Oxygen</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485DME8' class='radio float-left' name='485DME' value='8' type='checkbox' {0} />", dME != null && dME.Contains("8") ? "checked='checked'" : "") %>
                        <label for="485DME8">Tub/Shower Bench</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485DME9' class='radio float-left' name='485DME' value='9' type='checkbox' {0} />", dME != null && dME.Contains("9") ? "checked='checked'" : "") %>
                        <label for="485DME9">Walker</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485DME10' class='radio float-left' name='485DME' value='10' type='checkbox' {0} />", dME != null && dME.Contains("10") ? "checked='checked'" : "") %>
                        <label for="485DME10">Wheelchair</label>
                    </td>
                    <td colspan="2">
                        <label for="485DMEComments">Other</label>
                        <%= Html.TextBox("485DMEComments", editData.ContainsKey("485DMEComments") ? editData["485DMEComments"].Answer : "", new { @id = "485DMEComments", @maxlength = "40" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Safety Measures (Locator #15)</legend>
        <input type="hidden" name="485SafetyMeasures" value="" />
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures1' name='485SafetyMeasures' value='1' {0} />", safetyMeasure != null && safetyMeasure.Contains("1") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures1">Anticoagulant Precautions</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures2' name='485SafetyMeasures' value='2' {0} />", safetyMeasure != null && safetyMeasure.Contains("2") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures2">Emergency Plan Developed</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures3' name='485SafetyMeasures' value='3' {0} />", safetyMeasure != null && safetyMeasure.Contains("3") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures3">Fall Precautions</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures4' name='485SafetyMeasures' value='4' {0} />", safetyMeasure != null && safetyMeasure.Contains("4") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures4">Keep Pathway Clear</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures5' name='485SafetyMeasures' value='5' {0} />", safetyMeasure != null && safetyMeasure.Contains("5") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures5">Keep Side Rails Up</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures6' name='485SafetyMeasures' value='6' {0} />", safetyMeasure != null && safetyMeasure.Contains("6") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures6">Neutropenic Precautions</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures7' name='485SafetyMeasures' value='7' {0} />", safetyMeasure != null && safetyMeasure.Contains("7") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures7">O2 Precautions</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures8' name='485SafetyMeasures' value='8' {0} />", safetyMeasure != null && safetyMeasure.Contains("8") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures8">Proper Position During Meals</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures9' name='485SafetyMeasures' value='9' {0} />", safetyMeasure != null && safetyMeasure.Contains("9") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures9">Safety in ADLs</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures10' name='485SafetyMeasures' value='10' {0} />", safetyMeasure != null && safetyMeasure.Contains("10") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures10">Seizure Precautions</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures11' name='485SafetyMeasures' value='11' {0} />", safetyMeasure != null && safetyMeasure.Contains("11") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures11">Sharps Safety</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures12' name='485SafetyMeasures' value='12' {0} />", safetyMeasure != null && safetyMeasure.Contains("12") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures12">Slow Position Change</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures13' name='485SafetyMeasures' value='13' {0} />", safetyMeasure != null && safetyMeasure.Contains("13") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures13">Standard Precautions/ Infection Control</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures14' name='485SafetyMeasures' value='14' {0} />", safetyMeasure != null && safetyMeasure.Contains("14") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures14">Support During Transfer and Ambulation</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures15' name='485SafetyMeasures' value='15' {0} />", safetyMeasure != null && safetyMeasure.Contains("15") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures15">Use of Assistive Devices</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures16' name='485SafetyMeasures' value='16' {0} />", safetyMeasure != null && safetyMeasure.Contains("16") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures16">Instructed on safe utilities management</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures17' name='485SafetyMeasures' value='17' {0} />", safetyMeasure != null && safetyMeasure.Contains("17") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures17">Instructed on mobility safety</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures18' name='485SafetyMeasures' value='18' {0} />", safetyMeasure != null && safetyMeasure.Contains("18") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures18">Instructed on DME &#38; electrical safety</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures19' name='485SafetyMeasures' value='19' {0} />", safetyMeasure != null && safetyMeasure.Contains("19") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures19">Instructed on sharps container</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures20' name='485SafetyMeasures' value='20' {0} />", safetyMeasure != null && safetyMeasure.Contains("20") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures20">Instructed on medical gas</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures21' name='485SafetyMeasures' value='21' {0} />", safetyMeasure != null && safetyMeasure.Contains("21") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures21">Instructed on disaster/ emergency plan</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures22' name='485SafetyMeasures' value='22' {0} />", safetyMeasure != null && safetyMeasure.Contains("22") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures22">Instructed on safety measures</label>
                    </td>
                    <td>
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='485SafetyMeasures23' name='485SafetyMeasures' value='23' {0} />", safetyMeasure != null && safetyMeasure.Contains("23") ? "checked='checked'" : "") %>
                        <label for="485SafetyMeasures23">Instructed on proper handling of biohazard waste</label>
                    </td>
                    <td>
                        <label for="485SafetyMeasuresComments">Other (Specify)</label>
                        <%= Html.TextBox("485OtherSafetyMeasures", editData.ContainsKey("485OtherSafetyMeasures") ? editData["485OtherSafetyMeasures"].Answer : "", new { @id = "485OtherSafetyMeasures", @maxlength = "40" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Nutritional Req. (Locator #16)</legend>
        <div class="wide column">
            <div class="row">
                <%= Html.Templates("485NutritionTemplates", new { @class = "Templates", @template = "#485NutritionalReqs" })%>
                <br />
                <%= Html.TextArea("485NutritionalReqs", editData.ContainsKey("485NutritionalReqs") && editData["485NutritionalReqs"].Answer.IsNotNullOrEmpty() ? editData["485NutritionalReqs"].Answer : "", new { @id = "485NutritionalReqs", @style = "height: 120px; font-size: 16px;" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Allergies (Locator #17)</legend>
        <div class="column">
            <div class="row">
                <%= Html.RadioButton("485Allergies", "Yes", editData.ContainsKey("485Allergies") && editData["485Allergies"].Answer == "Yes", new { @id = "SOC_Allergies_Specify" }) %>
                <label for="SOC_Allergies_Specify">Allergic to</label>
                <%= Html.TextArea("485AllergiesDescription", editData.ContainsKey("485AllergiesDescription") ? editData["485AllergiesDescription"].Answer : "", new { @id = "", @style = "font-size: 16px;" })%>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <%= Html.Hidden("485Allergies"," ", new { @id = "" }) %>
                <%= Html.RadioButton("485Allergies", "No", editData.ContainsKey("485Allergies") && editData["485Allergies"].Answer == "No", new { @id = "SOC_Allergies_NKA" }) %>
                <label for="SOC_Allergies_NKA">NKA (Food/Drugs/Latex)</label>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Functional Limitations (Locator #18.A)</legend>
        <input name="485FunctionLimitations" value=" " type="hidden" />
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations1' name='485FunctionLimitations' value='1' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "" )%>
                        <label for="485FunctionLimitations1">Amputation</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations5' name='485FunctionLimitations' value='5' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5").ToChecked()) %>
                        <label for="485FunctionLimitations5">Paralysis</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations9' name='485FunctionLimitations' value='9' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9").ToChecked()) %>
                        <label for="485FunctionLimitations9">Legally Blind</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations2' name='485FunctionLimitations' value='2' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2").ToChecked()) %>
                        <label for="485FunctionLimitations2">Bowel/Bladder Incontinence</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations6' name='485FunctionLimitations' value='6' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6").ToChecked()) %>
                        <label for="485FunctionLimitations6">Endurance</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitationsA' name='485FunctionLimitations' value='A' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A").ToChecked()) %>
                        <label for="485FunctionLimitationsA">Dyspnea with Minimal Exertion</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations3' name='485FunctionLimitations' value='3' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3").ToChecked()) %>
                        <label for="485FunctionLimitations3">Contracture</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations7' name='485FunctionLimitations' value='7' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7").ToChecked()) %>
                        <label for="485FunctionLimitations7">Ambulation</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations4' name='485FunctionLimitations' value='4' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4").ToChecked()) %>
                        <label for="485FunctionLimitations4">Hearing</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485FunctionLimitations8' name='485FunctionLimitations' value='8' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8").ToChecked()) %>
                        <label for="485FunctionLimitations8">Speech</label>
                    </td>
                    <td colspan="2">
                        <%= string.Format("<input id='485FunctionLimitationsB' name='485FunctionLimitations' value='B' class='radio float-left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B").ToChecked()) %>
                        <label for="485FunctionLimitationsB" class="radio more">Other (Specify)</label>
                        <%= Html.TextBox("485FunctionLimitationsOther", editData.ContainsKey("485FunctionLimitationsOther") ? editData["485FunctionLimitationsOther"].Answer : "", new { @id = "485FunctionLimitationsOther", @maxlength = "40" }) %>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
	    <legend>Activities Permitted (Locator #18.B)</legend>
	    <input type="hidden" name="485ActivitiesPermitted" value="" />
	    <div class="column">
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted1' name='485ActivitiesPermitted' value='1' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted1">Complete bed rest</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted3' name='485ActivitiesPermitted' value='3' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted3">Up as tolerated</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted5' name='485ActivitiesPermitted' value='5' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted5">Exercise prescribed</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted7' name='485ActivitiesPermitted' value='7' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted7">Independent at home</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted9' name='485ActivitiesPermitted' value='9' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("9") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted9">Cane</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermittedB' name='485ActivitiesPermitted' value='B' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("B") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermittedB">Walker</label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted2' name='485ActivitiesPermitted' value='2' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted2">Bed rest with BRP</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted4' name='485ActivitiesPermitted' value='4' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted4">Transfer bed-chair</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted6' name='485ActivitiesPermitted' value='6' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted6">Partial weight bearing</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermitted8' name='485ActivitiesPermitted' value='8' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermitted8">Crutches</label>
            </div>
            <div class="row">
                <%= string.Format("<input id='485ActivitiesPermittedA' name='485ActivitiesPermitted' value='A' class='radio float-left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("A") ? "checked='checked'" : "" ) %>
                <label for="485ActivitiesPermittedA">Wheelchair</label>
            </div>
            <div class="row">
                <label for="485ActivitiesPermittedOther">Other (specify)</label>
                <%= Html.TextBox("485ActivitiesPermittedOther", editData.ContainsKey("485ActivitiesPermittedOther") ? editData["485ActivitiesPermittedOther"].Answer : "", new { @id = "485ActivitiesPermittedOther", @maxlength = "40" }) %>
            </div>
	    </div>
    </fieldset>
    <fieldset>
        <legend>Mental Status (Locator #19)</legend>
        <input type="hidden" name="485MentalStatus" value=" " />
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <%= string.Format("<input id='485MentalStatus1' class='radio float-left' name='485MentalStatus' value='1' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus1">Oriented</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485MentalStatus2' class='radio float-left' name='485MentalStatus' value='2' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus2">Comatose</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485MentalStatus3' class='radio float-left' name='485MentalStatus' value='3' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus3">Forgetful</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485MentalStatus4' class='radio float-left' name='485MentalStatus' value='4' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus4">Depressed</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485MentalStatus5' class='radio float-left' name='485MentalStatus' value='5' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus5">Disoriented</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= string.Format("<input id='485MentalStatus6' class='radio float-left' name='485MentalStatus' value='6' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus6">Lethargic</label>
                    </td>
                    <td>
                        <%= string.Format("<input id='485MentalStatus7' class='radio float-left' name='485MentalStatus' value='7' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus7">Agitated</label>
                    </td>
                    <td colspan="3">
                        <%= string.Format("<input id='485MentalStatus8' class='radio float-left' name='485MentalStatus' value='8' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                        <label for="485MentalStatus8" class="radio more fixed">Other (specify)</label>
                        <%= Html.TextBox("485MentalStatusOther", editData.ContainsKey("485MentalStatusOther") ? editData["485MentalStatusOther"].Answer : "", new { @id = "485MentalStatusOther", @maxlength = "40" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Prognosis (Locator #20)</legend>
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <%= Html.RadioButton("485Prognosis", "Guarded", editData.ContainsKey("485Prognosis") && editData["485Prognosis"].Answer == "Guarded", new { @id = "485PrognosisGuarded", @class = "radio float-left" } )%>
                        <label for="485PrognosisGuarded">Guarded</label>
                    </td>
                    <td>
                        <%= Html.RadioButton("485Prognosis", "Poor", editData.ContainsKey("485Prognosis") && editData["485Prognosis"].Answer == "Poor", new { @id = "485PrognosisPoor", @class = "radio float-left" })%>
                        <label for="485PrognosisPoor">Poor</label>
                    </td>
                    <td>
                        <%= Html.RadioButton("485Prognosis", "Fair", editData.ContainsKey("485Prognosis") && editData["485Prognosis"].Answer == "Fair", new { @id = "485PrognosisFair", @class = "radio float-left" })%>
                        <label for="485PrognosisFair">Fair</label>
                    </td>
                    <td>
                        <%= Html.RadioButton("485Prognosis", "Good", editData.ContainsKey("485Prognosis") && editData["485Prognosis"].Answer == "Good", new { @id = "485PrognosisGood", @class = "radio float-left" })%>
                        <label for="485PrognosisGood">Good</label>
                    </td>
                    <td>
                        <%= Html.RadioButton("485Prognosis", "Excellent", editData.ContainsKey("485Prognosis") && editData["485Prognosis"].Answer == "Excellent", new { @id = "485PrognosisExcellent", @class = "radio float-left" })%>
                        <label for="485PrognosisExcellent">Excellent</label>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Orders for Discipline and Treatments (Specify Amount/Frequency/Duration) (#21)</legend>
        <div class="wide column">
            <div class="row">
                <%= Html.Templates("485InterventionsTemplates", new { @class = "Templates", @template = "#Edit_485_Interventions" })%>
                <br />
                <%= Html.TextArea("485Interventions", editData.ContainsKey("485Interventions") && editData["485Interventions"].Answer.IsNotNullOrEmpty() ? editData["485Interventions"].Answer : "", new { @id = "Edit_485_Interventions", @style = "height: 180px;font-size: 16px;" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Goals/Rehabilitation Potential/Discharge Plans (#22)</legend>
        <div class="wide column">
            <div class="row">
                <%= Html.Templates("485GoalsTemplates", new { @class = "Templates", @template = "#Edit_485_Goals" })%>
                <br />
                <%= Html.TextArea("485Goals", editData.ContainsKey("485Goals") && editData["485Goals"].Answer.IsNotNullOrEmpty() ? editData["485Goals"].Answer : "", new { @id = "Edit_485_Goals", @cols = "100", @style = "height: 180px;font-size: 16px;" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_485_ClinicianSignature" class="bigtext float-left">Staff Signature</label>
                <div class="fr"><%= Html.Password("SignatureText","", new { @id = "New_485_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_485_SignatureDate" class="bigtext float-left">Date</label>
                <div class="fr"><input type="text" class="date-picker" name="SignatureDate" id="New_485_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_485_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_485_Status').val('110');PlanOfCare.Submit($(this),'newplanofcare');">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_485_Status').val('110');PlanOfCare.Submit($(this),'newplanofcare');">Save &#38; Close</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_485_Status').val('115');PlanOfCare.Submit($(this),'newplanofcare');">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newplanofcare');">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>