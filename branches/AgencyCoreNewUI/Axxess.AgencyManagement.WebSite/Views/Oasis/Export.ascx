﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">OASIS Export | <%= Current.AgencyName %></span>
<%  var pageName = "OASISToExport"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul>
            <li><a status="Refresh Grid" class="grid-refresh">Refresh</a></li>
        </ul>
        <br />
        <ul>
            <li><a status="Export Grid as Spreadsheet (Excel Format)" class="export">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter">
            <div class="narrow checkgroup">
                <div class="option select-all">
                    <input type="checkbox" id="<%= pageName %>_SelectAll" class="select-all"/>
                    <label for="<%= pageName %>_SelectAll">Select All</label>
                </div>
            </div>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_BranchCode" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id =  pageName +"_BranchCode", @status = "Select Branch to Filter Results" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_PaymentSources" class="strong">Payment Source</label>
            <select class="multiselect" multiple="multiple" name="PaymentSources">
                <option value="0">None; no charge for current services</option>
                <option value="1" selected="selected">Medicare (traditional fee-for-service)</option>
                <option value="2">Medicare (HMO/ managed care)</option>
                <option value="3">Medicaid (traditional fee-for-service)</option>
                <option value="4">Medicaid (HMO/ managed care)</option>
                <option value="5">Workers' compensation</option>
                <option value="6">Title programs (e.g., Title III,V, or XX)</option>
                <option value="7">Other government (e.g.,CHAMPUS,VA,etc)</option>
                <option value="8">Private insurance</option>
                <option value="9">Private HMO/ managed care</option>
                <option value="10">Self-pay</option>
                <option value="11">Unknown</option>
                <option value="12">Other</option>
            </select>
        </div>
    </fieldset>
    <%  Html.Telerik().Grid<AssessmentExport>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "bottom-gap args" }).Columns(columns => {
            columns.Bound(o => o.AssessmentId).Template(t => string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", t.Identifier)).ClientTemplate("<input name='OasisSelected' type='checkbox' value='<#= Identifier #>'/>").Title("").Width(50).HtmlAttributes(new { @class = "ac" }).Sortable(false);
            columns.Bound(o => o.PatientName).Title("Patient Name");
            columns.Bound(o => o.AssessmentName).Title("Assessment Type").Sortable(true);
            columns.Bound(o => o.AssessmentDateFormatted).Title("Assessment Date").Width(120).Sortable(true);
            columns.Bound(o => o.EpisodeRange).Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.CorrectionNumberFormat).Title("Correction #").Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ExportGrid", "Oasis", new { BranchId = ViewData["BranchId"], PaymentSources = "1" })).Footer(false).Scrollable().Sortable().Render(); %>
    <div class="buttons abs-bottom wrapper">
        <ul>
            <li><a class="generate-oasis">Generate OASIS File</a></li>
            <li><a class="mark-exported">Mark Selected as Exported</a></li>
            <li><a class="mark-completed">Mark Selected as Completed (Not Exported)</a></li>
        </ul>
    </div>
</div>