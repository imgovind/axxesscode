﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ValidationInfoViewData>" %>
<%  var dictonary = new Dictionary<string, string>() {
        { AssessmentType.StartOfCare.ToString(), "StartOfCare" },
        { AssessmentType.ResumptionOfCare.ToString(), "ResumptionOfCare" },
        { AssessmentType.Recertification.ToString(), "Recertification" },
        { AssessmentType.FollowUp.ToString(), "FollowUp" },
        { AssessmentType.TransferInPatientNotDischarged.ToString(), "TransferInPatientNotDischarged" },
        { AssessmentType.TransferInPatientDischarged.ToString(), "TransferInPatientDischarged" },
        { AssessmentType.DischargeFromAgencyDeath.ToString(), "DischargeFromAgencyDeath" },
        { AssessmentType.DischargeFromAgency.ToString(), "DischargeFromAgency" }
    }; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>OASIS-C Validation</title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
                .Add("jquery-ui-1.7.1.custom.css")
                .Add("desktop.css")
                .Add("validation.css")
                .Add("telerik.common.css")
                .Add("telerik.office2007.css")
                .Add("Site.css")
                .Combined(true)
                .Compress(true)
                .CacheDurationInDays(1)
                .Version(Current.AssemblyVersion)) %>
        <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
                // jQuery
                .Add("jquery-1.7.2.min.js")
                // jQuery UI Plugins
                .Add("Plugins/jQueryUI/jquery.ui.core.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.widget.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.mouse.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.position.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.autocomplete.min.js")
                .Add("Plugins/jQueryUI/jquery.ui.datepicker.min.js")
                // Other Plugins
                .Add("Plugins/Other/form.min.js")
                .Add("Plugins/Other/validate.min.js")
                .Add("Plugins/Other/jgrowl.min.js")
                // Custom Plugins
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : string.Empty) + "AutoComplete.js")
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Datepicker.js")
                .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "TimePicker.js")
                // Modules            
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Oasis.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "OasisValidation.js")
                .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
                .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
    </head>
    <body>
        <div class="wrapper main">
<%  if (Model.ValidationErrors != null) { %>
            <div class="title">
                You have
                <strong><%= Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count()%> error<%= Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() != 1 ? "s" : string.Empty %></strong>
                and
                <strong><%= Model.ValidationErrors.Where(e => e.ErrorType == "WARNING").Count() %> warning<%= Model.ValidationErrors.Where(e => e.ErrorType == "WARNING").Count() != 1 ? "s" : string.Empty %></strong>
                .
            </div>
    <%  foreach (var data in Model.ValidationErrors) { %>
            <a class="error-anchor" href="javascript:void(0)" onclick="window.parent.Oasis.GotoQuestion('<%= data.ErrorDup.Substring(0, 5) %>','<%= dictonary[Model.AssessmentType] %>');window.parent.UserInterface.CloseModal()" class="<%= data.ErrorType == "FATAL" ? "red" : "" %>">
                <div>
                    <%= data.ErrorType == "ERROR" || data.ErrorType == "FATAL" ? "<span class='img icon error'></span>" : "<span class='img icon warning'></span>" %>
                    <span><%= data.ErrorDup %></span>
                    <span class="description"><%= data.Description %></span>
                </div>
            </a>
    <%  } %>
<%  } %>
<%  if (Model.ValidationErrors != null && Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() == 0) { %>
    <%  if (Current.HasRight(Permissions.ViewHHRGCalculations)) { %>
            <div class="hipps">
                <span>
                    <label>HIPPS Code</label>
                    <strong><%= Model.HIPPSCODE %></strong>
                </span>
                <span>
                    <label>OASIS Claim Matching Key</label>
                    <strong><%= Model.HIPPSKEY %></strong>
                </span>
                <span>
                    <label>HHRG Code</label>
                    <strong><%= Model.HHRG %></strong>
                </span>
            </div>
            <div class="hipps">
                <span>
                    <label>Episode Payment Rate</label>
                    <strong><%= Model.StandardPaymentRate != 0 ? string.Format("${0}", Math.Round(Model.StandardPaymentRate,2).ToString()):"" %></strong>
                </span>
            </div>
    <%  } %>
    <%  using (Html.BeginForm("SubmitOnly", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.AssessmentType + "Validation_Form" })) { %>
        <%= Html.Hidden(Model.AssessmentType + "_PatientId", Model.PatientId)%>
        <%= Html.Hidden(Model.AssessmentType + "_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden(Model.AssessmentType + "_Id", Model.AssessmentId)%>
        <%= Html.Hidden("OasisValidationType", Model.AssessmentType)%>
        <%= Html.Hidden("oasisPageName", dictonary[Model.AssessmentType])%>
            <input type="hidden" id="oasisval_submit"  onclick="$(this).closest('form').submit();"  />
            <div class="oasissignature">
                <table>
                    <tbody>
            <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() || Model.AssessmentType == AssessmentType.Recertification.ToString() || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() || Model.AssessmentType == AssessmentType.FollowUp.ToString()) { %>
                        <tr>
                            <td>
                                <label for="<%= Model.AssessmentType%>_TimeIn" class="float-left bigtext">Time In</label>
                                <div class="fr"><%= Html.TextBox(Model.AssessmentType + "_TimeIn", Model.TimeIn, new { @id = Model.AssessmentType + "_TimeIn", @class = "time-picker required" }) %></div>
                            </td>
                            <td></td>
                            <td>
                                <label for="<%= Model.AssessmentType%>_TimeOut" class="float-left bigtext">Time Out</label>
                                <div class="fr"><%= Html.TextBox(Model.AssessmentType + "_TimeOut", Model.TimeOut, new { @id = Model.AssessmentType + "_TimeOut", @class = "time-picker required" }) %></div>
                            </td>
                        </tr>
            <%  } %>
                        <tr>
                            <td>
                                <label for="<%= Model.AssessmentType%>_ValidationClinician" class="float-left bigtext">Clinician Signature</label>
                                <div class="fr"><%= Html.Password(Model.AssessmentType + "_ValidationClinician", "", new { @id = Model.AssessmentType + "_ValidationClinician", @class = "required" }) %></div>
                            </td>
                            <td></td>
                            <td>
                                <label for="<%= Model.AssessmentType%>_ValidationSignatureDate" class="float-left bigtext">Date</label>
                                <div class="fr"><%= String.Format("<input type='text' id='{0}_ValidationSignatureDate' name='{0}_ValidationSignatureDate' class='date-picker required' mindate='{1}' maxdate='{2}' />", Model.AssessmentType, Model.EpisodeStartDate.ToShortDateString(), Model.EpisodeEndDate.ToShortDateString()) %></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
    <%  } %>
<%  } %>
        </div>
        <script type="text/javascript">
            Oasis.SignatureSubmit($("#oasisValidationForm"));
<%  if (Model.ValidationErrors != null && Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() == 0) { %>
            $("#printbutton", window.parent.document).parent().html(
                $("<a/>", { "href": "javascript:void(0)", "text": "Finish", "onclick": "$('#printview').contents().find('#oasisval_submit').click()" })
            ).next().find("a").text("Cancel");
<%  } %>
            window.parent.Acore.OnLoad($(".main"));
        </script>
    </body>
</html>