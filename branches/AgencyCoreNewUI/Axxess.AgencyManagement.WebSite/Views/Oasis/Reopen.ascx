﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentExport>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Submit", "Oasis", FormMethod.Post, new { @id = "OasisReopen_Form" })) { %>
    <%= Html.Hidden("Id", Model.AssessmentId, new { @id = "OasisReopen_Id" })%>  
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "OasisReopen_Id" })%>  
    <%= Html.Hidden("episodeId", Model.EpisodeId, new { @id = "OasisReopen_Id" })%>  
    <%= Html.Hidden("assessmentType", Model.AssessmentType, new { @id = "OasisReopen_Id" })%>  
    <%= Html.Hidden("actionType", "ReOpen", new { @id = "OasisReopen_Id" })%>  
    <fieldset>
        <legend>Reopen OASIS</legend>
        <div class="wide column">
            <div class="row">
                <p>
                    If this OASIS assessment was already accepted by CMS (Center for Medicare &#38; Medicaid Services), you will have to re-submit
                    this OASIS assessment to CMS if changes are made to OASIS items.
                </p>
                <div class="ac strong">Are you sure you want to reopen the assessment?</div>
            </div>
            <div class="row">
                <label for="OasisReopen_Reason" class="strong">Reason</label>
                <div class="ac"><textarea id="OasisReopen_Reason" name="reason" maxcharacters="500"></textarea></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Reopen</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>