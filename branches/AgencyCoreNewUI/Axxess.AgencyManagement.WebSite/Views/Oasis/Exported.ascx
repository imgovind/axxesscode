﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Exported OASIS Assessments | <%= Current.AgencyName %></span>
<%  var visible = Current.HasRight(Permissions.ReopenDocuments); %>
<%  var pageName = "OASISExported"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul> 
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul> 
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_Status" class="strong">Status</label>
            <select id="<%= pageName %>_Status" name="StatusId">
                <option value="0">All</option>
                <option value="1" selected="selected">Active</option>
                <option value="2">Discharged</option>
            </select>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" />
            <label for="<%= pageName %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
        </div>
    </fieldset>
    <%  Html.Telerik().Grid<AssessmentExport>().Name(pageName+"_Grid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.AssessmentName).Title("Assessment").Sortable(true);
            columns.Bound(o => o.AssessmentDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Assessment Date").Sortable(true);
            columns.Bound(o => o.EpisodeRange).Format("{0:MM/dd/yyyy}").Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.ExportedDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Exported Date").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.AssessmentId).Title("Cancel").Width(110).Template(o =>{%><%= string.Format("<a class=\"link\" onclick=\"OASIS.Exported.GenerateCancel('{0}','{1}');return false\" >Generate Cancel</a>", o.AssessmentId,  o.AssessmentType)%><%}).ClientTemplate("<a class=\"link\" onclick=\"OASIS.Exported.GenerateCancel('<#= AssessmentId#>','<#= AssessmentType#>');return false\">Generate Cancel</a>").Sortable(false).Visible(visible).Sortable(false);
            columns.Bound(o => o.AssessmentId).Title("Action").Width(60).Template(o =>{%><%= string.Format("<a class=\"link\" onclick=\"OASIS.Exported.Reopen('{0}','{1}','{2}','{3}','{4}');return false\">Reopen</a>", o.AssessmentId, o.PatientId, o.EpisodeId, o.AssessmentType, "ReOpen")%><%}).ClientTemplate("<a class=\"link\" onclick=\"OASIS.Exported.Reopen('<#= AssessmentId#>','<#= PatientId#>','<#= EpisodeId#>','<#= AssessmentType#>','ReOpen');return false\">Reopen</a>").Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ExportedGrid", "Oasis", new { branchId = ViewData["BranchId"], status = 1, startDate = DateTime.Now.AddDays(-59), endDate = DateTime.Now })).Scrollable().Sortable().Footer(false).Render(); %>
</div>

