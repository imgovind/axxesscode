﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">OASIS-C Death at Home | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><a href="#<%= Model.TypeName %>_Demographics" tooltip="M0010 &#8211; M0100">Clinical Record Items</a></li>
        <li><a href="#<%= Model.TypeName %>_TransferDischargeDeath" tooltip="M0903 &#8211; M0906">Death</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_TransferDischargeDeath" class="general loading"></div>
</div>