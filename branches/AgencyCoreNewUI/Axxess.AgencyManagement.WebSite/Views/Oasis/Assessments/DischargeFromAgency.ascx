﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">OASIS-C Discharge from Agency | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><a href="#<%= Model.TypeName %>_Demographics" tooltip="M0010 &#8211; M0150">Clinical Record Items</a></li>
        <li><a href="#<%= Model.TypeName %>_RiskAssessment" tooltip="M1040 &#8211; M1050">Risk Assessment</a></li>
        <li><a href="#<%= Model.TypeName %>_Sensory" tooltip="M1230">Sensory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Pain" tooltip="M1242">Pain</a></li>
        <li><a href="#<%= Model.TypeName %>_Integumentary" tooltip="M1306 &#8211; M1350">Integumentary Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Respiratory" tooltip="M1400 &#8211; M1410">Respiratory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Cardiac" tooltip="M1500 &#8211; M1510">Cardiac Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Elimination" tooltip="M1600 &#8211; M1620">Elimination Status</a></li>
        <li><a href="#<%= Model.TypeName %>_NeuroBehavioral" tooltip="M1700 &#8211; M1750">Neuro/Behaviourial Status</a></li>
        <li><a href="#<%= Model.TypeName %>_AdlIadl" tooltip="M1800 &#8211; M1890">ADL/IADLs</a></li>
        <li><a href="#<%= Model.TypeName %>_Medications" tooltip="M2004 &#8211; M2030">Medications</a></li>
        <li><a href="#<%= Model.TypeName %>_CareManagement" tooltip="M2100 &#8211; M2110">Care Management</a></li>
        <li><a href="#<%= Model.TypeName %>_EmergentCare" tooltip="M2300 &#8211; M2310">Emergent Care</a></li>
        <li><a href="#<%= Model.TypeName %>_TransferDischargeDeath" tooltip="M0903 &#8211; M0906<br />M2400 &#8211; M2420">Discharge</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_RiskAssessment" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Sensory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Pain" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Integumentary" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Respiratory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Cardiac" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Elimination" class="general loading"></div>
    <div id="<%= Model.TypeName %>_NeuroBehavioral" class="general loading"></div>
    <div id="<%= Model.TypeName %>_AdlIadl" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Medications" class="general loading"></div>
    <div id="<%= Model.TypeName %>_CareManagement" class="general loading"></div>
    <div id="<%= Model.TypeName %>_EmergentCare" class="general loading"></div>
    <div id="<%= Model.TypeName %>_TransferDischargeDeath" class="general loading"></div>
</div>