<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "Nutrition_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Nutrition")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset>
        <legend>Nutrition</legend>
        <% string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNutrition", "", new { @id = Model.TypeName + "_GenericNutritionHidden" })%>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Within Normal Limits' id='{0}_GenericNutrition1' name='{0}_GenericNutrition' value='1' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition1">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Dysphagia' id='{0}_GenericNutrition2' name='{0}_GenericNutrition' value='2' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition2">Dysphagia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Appetite' id='{0}_GenericNutrition3' name='{0}_GenericNutrition' value='3' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition3">Appetite</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Weight' id='{0}_GenericNutrition4' name='{0}_GenericNutrition' value='4' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition4" class="fixed short">Weight</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition4More" class="fr">
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionWeightGainLoss", " ", new { @id = Model.TypeName + "_GenericNutritionWeightGainLossHidden" })%>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionWeightGainLoss", "Loss", data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss"), new { @id = Model.TypeName + "_GenericNutritionWeightLoss", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Weight Loss" })%>
                            <label for="GenericNutritionWeightLoss" class="fixed short">Loss</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionWeightGainLoss", "Gain", data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain"), new { @id = Model.TypeName + "_GenericNutritionWeightGain", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Weight Gain" })%>
                            <label for="GenericNutritionWeightGain" class="fixed short">Gain</label>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Diet' id='{0}_GenericNutrition5' name='{0}_GenericNutrition' value='5' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition5" class="fixed short">Diet</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition5More" class="fr">
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionDietAdequate", "", new { @id = Model.TypeName + "_GenericNutritionDietAdequateHidden" })%>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionDietAdequate", "Adequate", data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate"), new { @id = Model.TypeName + "_GenericNutritionDietAdequate", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Adequate Diet" })%>
                            <label for="<%= Model.TypeName %>_GenericNutritionDietAdequate" class="fixed short">Adequate</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericNutritionDietAdequate", "Inadequate", data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate"), new { @id = Model.TypeName + "_GenericNutritionDietInadequate", @class = "no_float radio deselectable", @title = "(Optional) Nutrition, Inadequate Diet" })%>
                            <label for="<%= Model.TypeName %>_GenericNutritionDietInadequate" class="fixed short">Inadequate</label>
                            <%  string[] genericNutritionDiet = data.AnswerArray("GenericNutritionDiet"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionDiet", "", new { @id = Model.TypeName + "_GenericNutritionDietHidden" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='Diet Type' id='{0}_GenericNutrition9' name='{0}_GenericNutrition' value='9' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("9").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition9" class="fixed short">Diet Type</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition9More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_GenericNutritionDietType", data.AnswerOrEmptyString("GenericNutritionDietType"), new { @id = Model.TypeName + "_GenericNutritionDietType", @maxlength = "100", @title = " Diet Type " })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding' id='{0}_GenericNutrition6' name='{0}_GenericNutrition' value='6' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("6").ToChecked()) %>
                        <label class="radio" for="<%= Model.TypeName %>_GenericNutrition6" class="fixed short">Enteral Feeding</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericNutrition6More" class="fr">
                            <%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericNutritionEnteralFeeding", "", new { @id = Model.TypeName + "_GenericNutritionEnteralFeedingHidden" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding, NG' id='{0}_GenericNutritionEnteralFeeding1' name='{0}_GenericNutritionEnteralFeeding' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding1" class="inline-radio fixed">NG</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding, PEG' id='{0}_GenericNutritionEnteralFeeding2' name='{0}_GenericNutritionEnteralFeeding' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("2").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding2" class="inline-radio fixed">PEG</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(Optional) Nutrition, Enteral Feeding, Dobhoff' id='{0}_GenericNutritionEnteralFeeding3' name='{0}_GenericNutritionEnteralFeeding' value='3' type='checkbox' {1} />", Model.TypeName, genericNutritionEnteralFeeding.Contains("3").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_GenericNutritionEnteralFeeding3" class="inline-radio fixed">Dobhoff</label>
                            </div>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Tube Placement Checked' id='{0}_GenericNutrition7' name='{0}_GenericNutrition' value='7' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition7">Tube Placement Checked</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nutrition, Residual Checked' id='{0}_GenericNutrition8' name='{0}_GenericNutrition' value='8' type='checkbox' {1} />", Model.TypeName, genericNutrition.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutrition8" class="fixed short">Residual Checked</label>
                        <div id="<%= Model.TypeName %>_GenericNutrition8More" class="fr">
                            <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount">Amount</label>
                            <%= Html.TextBox(Model.TypeName + "_GenericNutritionResidualCheckedAmount", data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount"), new { @id = Model.TypeName + "_GenericNutritionResidualCheckedAmount", @class = "vitals numeric", @maxlength = "5", @title = "(Optional) Nutrition, Residual Checked Amount" })%>
                            <label for="<%= Model.TypeName %>_GenericNutritionResidualCheckedAmount">ml</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNutritionComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericNutritionComments", data.AnswerOrEmptyString("GenericNutritionComments"), 10, 50, new { @id = Model.TypeName + "_GenericNutritionCommentsComments", @title = "(Optional) Nutrition Comments" })%>
            </div>
        </div>
    </fieldset>
    <fieldset id="<%= Model.TypeName %>_NutritionalHealthScreen">
        <legend>Nutritional Health Screen</legend>
        <%  string[] genericNutritionalHealth = data.AnswerArray("GenericNutritionalHealth"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNutritionalHealth", "", new { @id = Model.TypeName + "_GenericNutritionalHealthHidden" })%>
        <div class="wide column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth1' name='{0}_GenericNutritionalHealth' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth1" class="fr">15</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth1">Without reason, has lost more than 10 lbs, in the last 3 months</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth2' name='{0}_GenericNutritionalHealth' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth2" class="fr">10</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth2">Has an illness or condition that made pt change the type and/or amount of food eaten</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth3' name='{0}_GenericNutritionalHealth' value='3' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth3" class="fr">10</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth3">Has open decubitus, ulcer, burn or wound</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth4' name='{0}_GenericNutritionalHealth' value='4' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth4" class="fr">10</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth4">Eats fewer than 2 meals a day</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth5' name='{0}_GenericNutritionalHealth' value='5' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth5" class="fr">10</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth5">Has a tooth/mouth problem that makes it hard to eat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth6' name='{0}_GenericNutritionalHealth' value='6' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth6" class="fr">10</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth6">Has 3 or more drinks of beer, liquor or wine almost every day</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth7' name='{0}_GenericNutritionalHealth' value='7' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth7" class="fr">10</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth7">Does not always have enough money to buy foods needed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth8' name='{0}_GenericNutritionalHealth' value='8' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth8" class="fr">5</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth8">Eats few fruits or vegetables, or milk products</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth9' name='{0}_GenericNutritionalHealth' value='9' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("9").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth9" class="fr">5</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth9">Eats alone most of the time</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth10' name='{0}_GenericNutritionalHealth' value='10' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("10").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth10" class="fr">5</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth10">Takes 3 or more prescribed or OTC medications a day</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth11' name='{0}_GenericNutritionalHealth' value='11' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("11").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth11" class="fr">5</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth11">Is not always physically able to cook and/or feed self and has no caregiver to assist</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionalHealth12' name='{0}_GenericNutritionalHealth' value='12' type='checkbox' {1} />", Model.TypeName, genericNutritionalHealth.Contains("12").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth12" class="fr">5</label>
                        <label for="<%= Model.TypeName %>_GenericNutritionalHealth12">Frequently has diarrhea or constipation</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <%  string[] genericGoodNutritionScore = data.AnswerArray("GenericGoodNutritionScore"); %>
                <div class="float-left pad strong">Total:<br /><input type="text" class="mi" readonly="readonly" value="0" id="<%= Model.TypeName %>_GenericGoodNutritionScore" name="<%= Model.TypeName %>_GenericGoodNutritionScore" /></div>
                <div>
                    <label id="<%= Model.TypeName %>_GoodNutritionalStatus">Good Nutritional Status (Score 0 &#8211; 25)</label>
                    <label id="<%= Model.TypeName %>_ModerateNutritionalRisk">Moderate Nutritional Risk (Score 26 &#8211; 55)</label>
                    <label id="<%= Model.TypeName %>_HighNutritionalRisk">High Nutritional Risk (Score 56 &#8211; 100)</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNutritionalStatusComments" class="strong">Nutritional Status Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericNutritionalStatusComments", data.AnswerOrEmptyString("GenericNutritionalStatusComments"),10, 50, new { @id = Model.TypeName + "_GenericNutritionalStatusComments" })%>
            </div>
            <div class="row">
                <%  string[] genericNutritionDiffect = data.AnswerArray("GenericNutritionDiffect"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericNutritionDiffect", "", new { @id = Model.TypeName + "_GenericNutritionDiffectHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionDiffect1' name='{0}_GenericNutritionDiffect' value='1' type='checkbox' {1} />", Model.TypeName, genericNutritionDiffect.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionDiffect1">Non-compliant with prescribed diet</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericNutritionDiffect2' name='{0}_GenericNutritionDiffect' value='2' type='checkbox' {1} />", Model.TypeName, genericNutritionDiffect.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericNutritionDiffect2">Over/under weight by 10%</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMealsPreparedBy" class="strong">Meals prepared by</label>
                <%= Html.TextArea(Model.TypeName + "_GenericMealsPreparedBy", data.AnswerOrEmptyString("GenericMealsPreparedBy"), 10, 50, new { @id = Model.TypeName + "_GenericMealsPreparedBy" })%>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Enter Physician&#8217;s Orders or Diet Requirements (Locator #16)</legend>
        <%  string[] nutritionalReqs = data.AnswerArray("485NutritionalReqs"); %>
        <%= Html.Hidden(Model.TypeName + "_485NutritionalReqs", "", new { @id = Model.TypeName + "_485NutritionalReqsHidden" })%>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Regular' id='{0}_485NutritionalReqs1' name='{0}_485NutritionalReqs' value='1' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs1">Regular</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Mechanical Soft' id='{0}_485NutritionalReqs2' name='{0}_485NutritionalReqs' value='2' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs2">Mechanical Soft</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Heart Healthy' id='{0}_485NutritionalReqs3' name='{0}_485NutritionalReqs' value='3' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs3">Heart Healthy</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Low Cholesterol' id='{0}_485NutritionalReqs4' name='{0}_485NutritionalReqs' value='4' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs4">Low Cholesterol</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Low Fat' id='{0}_485NutritionalReqs5' name='{0}_485NutritionalReqs' value='5' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs5">Low Fat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Sodium' id='{0}_485NutritionalReqs6' name='{0}_485NutritionalReqs' value='6' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("6").ToChecked()) %>
                        <span>
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsSodiumAmount", data.AnswerOrDefault("485NutritionalReqsSodiumAmount", "Low"), new { @id = Model.TypeName + "_485NutritionalReqsSodiumAmount", @class = "vitals", @maxlength = "10", @title = "(485 Locator 16) Nutritional Requirements, Sodium Amount" })%>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs6">Sodium</label>
                        </span>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, No Added Salt' id='{0}_485NutritionalReqs7' name='{0}_485NutritionalReqs' value='7' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs7">No Added Salt</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Calorie ADA Diet' id='{0}_485NutritionalReqs8' name='{0}_485NutritionalReqs' value='8' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("8").ToChecked()) %>
                        <span>
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsCalorieADADietAmount", data.AnswerOrDefault("485NutritionalReqsCalorieADADietAmount", "Low"), new { @id = Model.TypeName + "_485NutritionalReqsCalorieADADietAmount", @class = "vitals", @maxlength = "10", @title = "(485 Locator 16) Nutritional Requirements, Calorie ADA Diet Amount" })%>
                            <label for="<%= Model.TypeName %>_485NutritionalReqs8">Calorie ADA Diet</label>
                        </span>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, No Concentrated Sweets' id='{0}_485NutritionalReqs9' name='{0}_485NutritionalReqs' value='9' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("9").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs9">No Concentrated Sweets</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Coumadin Diet' id='{0}_485NutritionalReqs10' name='{0}_485NutritionalReqs' value='10' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("10").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs10">Coumadin Diet</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Renal Diet' id='{0}_485NutritionalReqs11' name='{0}_485NutritionalReqs' value='11' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("11").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs11">Renal Diet</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Other' id='{0}_485NutritionalReqs12' name='{0}_485NutritionalReqs' value='12' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("12").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs12" class="fixed short">Other</label>
                        <div id="<%= Model.TypeName %>_485NutritionalReqs12More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsPhyDietOtherName", data.AnswerOrEmptyString("485NutritionalReqsPhyDietOtherName"), new { @id = Model.TypeName + "_485NutritionalReqsPhyDietOtherName", @maxlength = "20", @title = "(485 Locator 16) Nutritional Requirements, Specify Other" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Supplement' id='{0}_485NutritionalReqs13' name='{0}_485NutritionalReqs' value='13' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("13").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs13" class="fixed short">Supplement</label>
                        <div id="<%= Model.TypeName %>_485NutritionalReqs13More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsSupplementType", data.AnswerOrEmptyString("485NutritionalReqsSupplementType"), new { @id = Model.TypeName + "_485NutritionalReqsSupplementType", @maxlength = "20", @title = "(485 Locator 16) Nutritional Requirements, Supplement Type" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Fluid Restriction' id='{0}_485NutritionalReqs14' name='{0}_485NutritionalReqs' value='14' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("14").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs14" class="fixed short">Fluid Restriction</label>
                        <div id="<%= Model.TypeName %>_485NutritionalReqs14More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsFluidResAmount", data.AnswerOrEmptyString("485NutritionalReqsFluidResAmount"), new { @id = Model.TypeName + "_485NutritionalReqsFluidResAmount", @class = "st numeric", @maxlength = "5", @title = "(485 Locator 16) Nutritional Requirements, Fluid Restriction, Amount" })%>
                            <label for="<%= Model.TypeName %>_485NutritionalReqsFluidResAmount">ml/24 hours</label>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, TPN' id='{0}_485NutritionalReqs16' name='{0}_485NutritionalReqs' value='16' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("16").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs16">TPN</label>
                        <div id="<%= Model.TypeName %>_485NutritionalReqs16More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsTPNAmount", data.AnswerOrEmptyString("485NutritionalReqsTPNAmount"), new { @id = Model.TypeName + "_485NutritionalReqsTPNAmount", @class = "vitals", @maxlength = "15", @title = "(485 Locator 16) Nutritional Requirements, TPN, Amount" })%>
                            <label for="<%= Model.TypeName %>_485NutritionalReqsTPNAmount">@ml/hr</label>
                            <label for="<%= Model.TypeName %>_485NutritionalReqsTPNVia">via</label>
                            <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsTPNVia", data.AnswerOrEmptyString("485NutritionalReqsTPNVia"), new { @id = Model.TypeName + "_485NutritionalReqsTPNVia", @class = "oe", @maxlength = "15", @title = "(485 Locator 16) Nutritional Requirements, TPN, Via" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition' id='{0}_485NutritionalReqs15' name='{0}_485NutritionalReqs' value='15' type='checkbox' {1} />", Model.TypeName, nutritionalReqs.Contains("15").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485NutritionalReqs15">Enteral Nutrition</label>
                        <div id="<%= Model.TypeName %>_485NutritionalReqs15More">
                            <div class="margin">
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralDesc">Formula</label>
                                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsEnteralDesc", data.AnswerOrEmptyString("485NutritionalReqsEnteralDesc"), new { @id = Model.TypeName + "_485NutritionalReqsEnteralDesc", @class = "st", @maxlength = "15", @title = "(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Formula" })%></div>
                                <div class="clear"></div>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralAmount">Amount</label>
                                <div class="fr">
                                    <%= Html.TextBox(Model.TypeName + "_485NutritionalReqsEnteralAmount", data.AnswerOrEmptyString("485NutritionalReqsEnteralAmount"), new { @id = Model.TypeName + "_485NutritionalReqsEnteralAmount", @class = "vitals numeric", @maxlength = "5", @title = "(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Amount" })%>
                                    <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralAmount">ml/hr</label>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div>Per:</div>
                            <%  string[] nutritionalReqsEnteral = data.AnswerArray("485NutritionalReqsEnteral"); %>
                            <%= Html.Hidden(Model.TypeName + "_485NutritionalReqsEnteral", "", new { @id = Model.TypeName + "_485NutritionalReqsEnteralHidden" })%>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per PEG' class='no_float' id='{0}_485NutritionalReqsEnteralPer1' name='{0}_485NutritionalReqsEnteral' value='1' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("1").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer1" class="fixed short">PEG</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per NG' class='no_float' id='{0}_485NutritionalReqsEnteralPer2' name='{0}_485NutritionalReqsEnteral' value='2' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("2").ToChecked())%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer2" class="fixed short">NG</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per Dobhoff' class='no_float' id='{0}_485NutritionalReqsEnteralPer3' name='{0}_485NutritionalReqsEnteral' value='3' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("3").ToChecked())%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer3" class="fixed short">Dobhoff</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Per Other' class='no_float' id='{0}_485NutritionalReqsEnteralPer4' name='{0}_485NutritionalReqsEnteral' value='4' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("4").ToChecked())%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralPer4" class="fixed short">Other</label>
                            </div>
                            <div id="<%= Model.TypeName %>_485NutritionalReqsEnteralPer4More" class="fr"><%= Html.TextBox(Model.TypeName + "_485NutritionalReqsEnteralOtherName", data.AnswerOrEmptyString("485NutritionalReqsEnteralOtherName"), new { @id = Model.TypeName + "_485NutritionalReqsEnteralOtherName", @class = "mediumWidth", @maxlength = "20" })%></div>
                            <div class="clear"></div>
                            <div>Via:</div>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Via Pump' class='no_float' id='{0}_485NutritionalReqsEnteralVia1' name='{0}_485NutritionalReqsEnteral' value='5' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("5").ToChecked())%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralVia1" class="fixed short">Pump</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Via Gravity' class='no_float' id='{0}_485NutritionalReqsEnteralVia2' name='{0}_485NutritionalReqsEnteral' value='6' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("6").ToChecked())%>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralVia2" class="fixed short">Gravity</label>
                            </div>
                            <div class="float-left">
                                <%= string.Format("<input title='(485 Locator 16) Nutritional Requirements, Enteral Nutrition, Via Bolus' class='no_float' id='{0}_485NutritionalReqsEnteralVia3' name='{0}_485NutritionalReqsEnteral' value='7' type='checkbox' {1} />", Model.TypeName, nutritionalReqsEnteral.Contains("7").ToChecked()) %>
                                <label for="<%= Model.TypeName %>_485NutritionalReqsEnteralVia3" class="fixed short">Bolus</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Nutrition.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>