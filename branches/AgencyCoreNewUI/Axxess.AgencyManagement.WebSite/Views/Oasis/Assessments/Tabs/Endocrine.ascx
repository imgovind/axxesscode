<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "Endocrine_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Endocrine")%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset>
        <legend>Endocrine</legend>
        <%= Html.Hidden(Model.TypeName + "_GenericEndocrineWithinNormalLimits", "", new { @id = Model.TypeName + "_GenericEndocrineWithinNormalLimitsHidden" })%>
        <div class="column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine, Within Normal Limits' id='{0}_GenericEndocrineWithinNormalLimits' name='{0}_GenericEndocrineWithinNormalLimits' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("GenericEndocrineWithinNormalLimits").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericEndocrineWithinNormalLimits">WNL (Within Normal Limits)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Is patient diabetic?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericPatientDiabetic", "", new { @id = Model.TypeName + "_GenericPatientDiabeticHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericPatientDiabetic", "1", data.AnswerOrEmptyString("GenericPatientDiabetic").Equals("1"), new { @id = Model.TypeName + "_GenericPatientDiabetic1", @title = "(Optional) Diabetic, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericPatientDiabetic1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericPatientDiabetic", "0", data.AnswerOrEmptyString("GenericPatientDiabetic").Equals("0"), new { @id = Model.TypeName + "_GenericPatientDiabetic0", @title = "(Optional) Diabetic, No" })%>
                    <label for="<%= Model.TypeName %>_GenericPatientDiabetic0" class="fixed short">No</label>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row diabetic">
                <label class="strong">Diabetic Management</label>
                <%  string[] diabeticCareDiabeticManagement = data.AnswerArray("GenericDiabeticCareDiabeticManagement"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDiabeticCareDiabeticManagement", "", new { @id = Model.TypeName + "_GenericDiabeticCareDiabeticManagementHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Diabetic Management, Diet' id='{0}_GenericDiabeticCareDiabeticManagement1' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='1' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement1">Diet</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Diabetic Management, Oral Hypoglycemic' id='{0}_GenericDiabeticCareDiabeticManagement2' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='2' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement2">Oral Hypoglycemic</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Diabetic Management, Exercise' id='{0}_GenericDiabeticCareDiabeticManagement3' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='3' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement3">Exercise</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Diabetic Management, Insulin' id='{0}_GenericDiabeticCareDiabeticManagement4' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='4' {1} />", Model.TypeName, diabeticCareDiabeticManagement.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareDiabeticManagement4">Insulin</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <hr />
        <div class="column">
            <div class="row">
                <label class="float-left">Insulin dependent?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericInsulinDependent", "", new { @id = Model.TypeName + "_GenericInsulinDependentHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericInsulinDependent", "1", data.AnswerOrEmptyString("GenericInsulinDependent").Equals("1"), new { @id = Model.TypeName + "_GenericInsulinDependent1", @class = "radio deselectable", @title = "(Optional) Insulin Dependent, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericInsulinDependent1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericInsulinDependent", "0", data.AnswerOrEmptyString("GenericInsulinDependent").Equals("0"), new { @id = Model.TypeName + "_GenericInsulinDependent0", @class = "radio deselectable", @title = "(Optional) Insulin Dependent, No" })%>
                    <label for="<%= Model.TypeName %>_GenericInsulinDependent0" class="fixed short">No</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div id="<%= Model.TypeName %>_GenericInsulinDependentMore" class="row">
                <label for="<%= Model.TypeName %>_GenericInsulinDependencyDuration" class="float-left">How long?</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericInsulinDependencyDuration", data.AnswerOrEmptyString("GenericInsulinDependencyDuration"), new { @id = Model.TypeName + "_GenericInsulinDependencyDuration", @class = "st", @maxlength = "10", @title = "(Optional) Insulin Dependent, How Long" })%></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label class="strong">Insulin Administered by</label>
                <%  string[] diabeticCareInsulinAdministeredby = data.AnswerArray("GenericDiabeticCareInsulinAdministeredby"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericDiabeticCareInsulinAdministeredby", "", new { @id = Model.TypeName + "_GenericDiabeticCareInsulinAdministeredbyHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Insulin Administered By, Not Applicable' id='{0}_GenericDiabeticCareInsulinAdministeredby1' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='1' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby1">N/A</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Insulin Administered by Patient' id='{0}_GenericDiabeticCareInsulinAdministeredby2' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='2' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby2">Patient</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Insulin Administered by Caregiver' id='{0}_GenericDiabeticCareInsulinAdministeredby3' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='3' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby3">Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Insulin Administered by Skilled Nurse' id='{0}_GenericDiabeticCareInsulinAdministeredby4' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='4' {1} />", Model.TypeName, diabeticCareInsulinAdministeredby.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericDiabeticCareInsulinAdministeredby4">SN</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <hr />
        <div class="column">
           <div class="row">
                <label class="float-left">Is patient independent with glucometer use?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericGlucometerUseIndependent", "", new { @id = Model.TypeName + "_GenericGlucometerUseIndependentHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericGlucometerUseIndependent", "1", data.AnswerOrEmptyString("GenericGlucometerUseIndependent").Equals("1"), new { @id = Model.TypeName + "_GenericGlucometerUseIndependent1", @class = "radio deselectable", @title = "(Optional) Patient Independent with Glucometer, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericGlucometerUseIndependent1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericGlucometerUseIndependent", "0", data.AnswerOrEmptyString("GenericGlucometerUseIndependent").Equals("0"), new { @id = Model.TypeName + "_GenericGlucometerUseIndependent0", @class = "radio deselectable", @title = "(Optional) Patient Independent with Glucometer, No" })%>
                    <label for="<%= Model.TypeName %>_GenericGlucometerUseIndependent0" class="fixed short">No</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Is caregiver independent with glucometer use?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericCareGiverGlucometerUse", "", new { @id = Model.TypeName + "_GenericCareGiverGlucometerUseHidden" })%>
                <div class="fr">
                    <div class="float-left">
                        <%= Html.RadioButton(Model.TypeName + "_GenericCareGiverGlucometerUse", "1", data.AnswerOrEmptyString("GenericCareGiverGlucometerUse").Equals("1"), new { @id = Model.TypeName + "_GenericCareGiverGlucometerUse1", @class = "radio deselectable", @title = "(Optional) Caregiver Independent with Glucometer, Yes" })%>
                        <label for="<%= Model.TypeName %>_GenericCareGiverGlucometerUse1" class="fixed short">Yes</label>
                    </div>
                    <div class="float-left">
                        <%= Html.RadioButton(Model.TypeName + "_GenericCareGiverGlucometerUse", "0", data.AnswerOrEmptyString("GenericCareGiverGlucometerUse").Equals("0"), new { @id = Model.TypeName + "_GenericCareGiverGlucometerUse0", @class = "radio deselectable", @title = "(Optional) Caregiver Independent with Glucometer, No" })%>
                        <label for="<%= Model.TypeName %>_GenericCareGiverGlucometerUse0" class="fixed short">No</label>
                    </div>
                    <div class="float-left">
                        <%= Html.RadioButton(Model.TypeName + "_GenericCareGiverGlucometerUse", "2", data.AnswerOrEmptyString("GenericCareGiverGlucometerUse").Equals("2"), new { @id = Model.TypeName + "_GenericCareGiverGlucometerUse2", @class = "radio deselectable", @title = "(Optional) Caregiver Independent with Glucometer, Not Applicable" })%>
                        <label for="<%= Model.TypeName %>_GenericCareGiverGlucometerUse2" class="fixed short">N/A, no caregiver</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <hr />
        <div class="wide column">
            <div class="row">
                <div class="strong">Does patient have any of the following?</div>
                <%  string[] patientEdocrineProblem = data.AnswerArray("GenericPatientEdocrineProblem"); %>
                <%= Html.Hidden(Model.TypeName + "GenericPatientEdocrineProblem", new { @id = Model.TypeName + "GenericPatientEdocrineProblemHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine Patient Conditions, Polyuria' id='{0}_GenericPatientEdocrineProblem1' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='1' {1} />", Model.TypeName, patientEdocrineProblem.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem1" class="fixed inline-radio">Polyuria</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine Patient Conditions, Polydipsia' id='{0}_GenericPatientEdocrineProblem2' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='2' {1} />", Model.TypeName, patientEdocrineProblem.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem2" class="fixed inline-radio">Polydipsia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine Patient Conditions, Polyphagia' id='{0}_GenericPatientEdocrineProblem3' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='3' {1} />", Model.TypeName, patientEdocrineProblem.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem3" class="fixed inline-radio">Polyphagia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine Patient Conditions, Neuropathy' id='{0}_GenericPatientEdocrineProblem4' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='4' {1} />", Model.TypeName, patientEdocrineProblem.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem4" class="fixed inline-radio">Neuropathy</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine Patient Conditions, Radiculopathy' id='{0}_GenericPatientEdocrineProblem5' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='5' {1} />", Model.TypeName, patientEdocrineProblem.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem5" class="fixed inline-radio">Radiculopathy</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Endocrine Patient Conditions, Thyroid Problems' id='{0}_GenericPatientEdocrineProblem6' type='checkbox' name='{0}_GenericPatientEdocrineProblem' value='6' {1} />", Model.TypeName, patientEdocrineProblem.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericPatientEdocrineProblem6" class="fixed inline-radio">Thyroid problems</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <hr />
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBloodSugarLevelText" class="float-left">Blood Sugar</label>
                <div class="fr">
                    <%= Html.TextBox(Model.TypeName + "_GenericBloodSugarLevelText", data.AnswerOrEmptyString("GenericBloodSugarLevelText"), new { @id = Model.TypeName + "_GenericBloodSugarLevelText", @class = "st numeric", @maxlength = "5", @title = "(Optional) Blood Sugar Level" })%>
                    <%= Html.Hidden(Model.TypeName + "_GenericBloodSugarLevel", "", new { @id = Model.TypeName + "_GenericBloodSugarLevelHidden" })%>
                    <label>mg/dl</label>
                </div>
                <div class="clear"></div>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericBloodSugarLevel", "Random", data.AnswerOrEmptyString("GenericBloodSugarLevel").Equals("Random"), new { @id = Model.TypeName + "_GenericBloodSugarLevelRandom", @class = "radio deselectable", @title = "(Optional) Blood Sugar Test, Random" })%>
                    <label for="<%= Model.TypeName %>_GenericBloodSugarLevelRandom" class="fixed short">Random</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericBloodSugarLevel", "Fasting", data.AnswerOrEmptyString("GenericBloodSugarLevel").Equals("Fasting"), new { @id = Model.TypeName + "_GenericBloodSugarLevelFasting", @class = "radio deselectable", @title = "(Optional) Blood Sugar Test, Fasting" })%>
                    <label for="<%= Model.TypeName %>_GenericBloodSugarLevelFasting" class="fixed short">Fasting</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBloodSugarCheckedBy" class="float-left">Blood sugar checked by</label>
                <div class="fr">
                    <%  var diabeticCarePerformedby = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Patient", Value = "1" },
                            new SelectListItem { Text = "SN", Value = "2" },
                            new SelectListItem { Text = "Caregiver", Value = "3" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarCheckedBy", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericBloodSugarCheckedBy", diabeticCarePerformedby, new { @id = Model.TypeName + "_GenericBloodSugarCheckedBy", @title = "(Optional) Blood Sugar Checked By" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericBloodSugarSiteText" class="float-left">Site</label>
                <div class="fr">
                    <%= Html.TextBox(Model.TypeName + "_GenericBloodSugarSiteText", data.AnswerOrEmptyString("GenericBloodSugarSiteText"), new { @id = Model.TypeName + "_GenericBloodSugarSiteText", @maxlength = "", @title = "(Optional) Blood Sugar Test Site" })%>
                    <%= Html.Hidden(Model.TypeName + "_GenericBloodSugarSite") %>
                </div>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericBloodSugarSite", "Left", data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Left"), new { @id = Model.TypeName + "_GenericBloodSugarSiteLeft", @class = "radio deselectable", @title = "(Optional) Blood Sugar Test Site, Left" })%>
                    <label for="<%= Model.TypeName %>_GenericBloodSugarLevelRandom" class="fixed short">Left</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericBloodSugarSite", "Right", data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Right"), new { @id = Model.TypeName + "_GenericBloodSugarSiteRight", @class = "radio deselectable", @title = "(Optional) Blood Sugar Test Site, Right" })%>
                    <label for="<%= Model.TypeName %>_GenericBloodSugarLevelFasting" class="fixed short">Right</label> 
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <hr />
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericEndocrineComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericEndocrineComments", data.AnswerOrEmptyString("GenericEndocrineComments"), 5, 70, new { @id = Model.TypeName + "_GenericEndocrineComments", @title = "(Optional) Endocrine Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Endocrine.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>