<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "Respiratory_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Respiratory")%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Respiratory</legend>
        <%  string[] respiratoryCondition = data.AnswerArray("GenericRespiratoryCondition"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericRespiratoryCondition", "", new { @id = Model.TypeName + "_GenericRespiratoryConditionHidden" })%>
        <div class="wide column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Within Normal Limits' id='{0}_GenericRespiratoryCondition1' name='{0}_GenericRespiratoryCondition' value='1' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition1">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds' id='{0}_GenericRespiratoryCondition2' name='{0}_GenericRespiratoryCondition' value='2' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition2">Lung Sounds</label>
                        <div class="clear"></div>
                        <div id="<%= Model.TypeName %>_GenericRespiratoryCondition2More">
                            <%  string[] respiratorySounds = data.AnswerArray("GenericRespiratorySounds"); %>
                            <%= Html.Hidden(Model.TypeName + "_GenericRespiratorySounds", "", new { @id = Model.TypeName + "_GenericRespiratorySoundsHidden" })%>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, CTA' id='{0}_GenericRespiratorySounds1' name='{0}_GenericRespiratorySounds' value='1' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("1").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds1">CTA</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds1More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsCTAText", data.AnswerOrEmptyString("GenericLungSoundsCTAText"), new { @id = Model.TypeName + "_GenericLungSoundsCTAText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, CTA" }) %>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Rales' id='{0}_GenericRespiratorySounds2' name='{0}_GenericRespiratorySounds' value='2' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("2").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds2">Rales</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds2More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsRalesText", data.AnswerOrEmptyString("GenericLungSoundsRalesText"), new { @id = Model.TypeName + "_GenericLungSoundsRalesText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Rales" })%>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Rhonchi' id='{0}_GenericRespiratorySounds3' name='{0}_GenericRespiratorySounds' value='3' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("3").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds3">Rhonchi</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds3More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsRhonchiText", data.AnswerOrEmptyString("GenericLungSoundsRhonchiText"), new { @id = Model.TypeName + "_GenericLungSoundsRhonchiText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Rhonchi" })%>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Wheezes' id='{0}_GenericRespiratorySounds4' name='{0}_GenericRespiratorySounds' value='4' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("4").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds4">Wheezes</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds4More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsWheezesText", data.AnswerOrEmptyString("GenericLungSoundsWheezesText"), new { @id = Model.TypeName + "_GenericLungSoundsWheezesText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Wheezes" })%>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Crackles' id='{0}_GenericRespiratorySounds5' name='{0}_GenericRespiratorySounds' value='5' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("5").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds5">Crackles</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds5More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsCracklesText", data.AnswerOrEmptyString("GenericLungSoundsCracklesText"), new { @id = Model.TypeName + "_GenericLungSoundsCracklesText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Crackles" })%>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Diminished' id='{0}_GenericRespiratorySounds6' name='{0}_GenericRespiratorySounds' value='6' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("6").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds6">Diminished</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds6More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsDiminishedText", data.AnswerOrEmptyString("GenericLungSoundsDiminishedText"), new { @id = Model.TypeName + "_GenericLungSoundsDiminishedText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Diminished" })%>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Absent' id='{0}_GenericRespiratorySounds7' name='{0}_GenericRespiratorySounds' value='7' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("7").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds7">Absent</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds7More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsAbsentText", data.AnswerOrEmptyString("GenericLungSoundsAbsentText"), new { @id = Model.TypeName + "_GenericLungSoundsAbsentText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Absent" })%>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="float-left">
                                                <%= string.Format("<input title='(Optional) Respiratory, Lung Sounds, Stridor' id='{0}_GenericRespiratorySounds8' name='{0}_GenericRespiratorySounds' value='8' type='checkbox' {1} />", Model.TypeName, respiratorySounds.Contains("8").ToChecked()) %>
                                                <label for="<%= Model.TypeName %>_GenericRespiratorySounds8">Stridor</label>
                                            </div>
                                            <div id="<%= Model.TypeName %>_GenericRespiratorySounds8More" class="fr">
                                                <%= Html.TextBox(Model.TypeName + "_GenericLungSoundsStridorText", data.AnswerOrEmptyString("GenericLungSoundsStridorText"), new { @id = Model.TypeName + "_GenericLungSoundsStridorText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Lung Sounds, Stridor" })%>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Cough' id='{0}_GenericRespiratoryCondition3' name='{0}_GenericRespiratoryCondition' value='3' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition3">Cough</label>
                        <div id="<%= Model.TypeName %>_GenericRespiratoryCondition3More" class="fr">
                            <%  var coughList = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "N/A", Value = "1" },
                                    new SelectListItem { Text = "Productive", Value = "1" },
                                    new SelectListItem { Text = "Nonproductive", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericCoughList", "0")); %>
                            <%= Html.DropDownList(Model.TypeName + "_GenericCoughList", coughList, new { @title = "(Optional) Respiratory, Cough, Productive" })%>
                            <label for="<%= Model.TypeName %>_GenericCoughDescribe">Describe</label>
                            <%= Html.TextBox(Model.TypeName + "_GenericCoughDescribe", data.AnswerOrEmptyString("GenericCoughDescribe"), new { @id = Model.TypeName + "_GenericCoughDescribe", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Cough, Describe" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Oxygen' id='{0}_GenericRespiratoryCondition4' name='{0}_GenericRespiratoryCondition' value='4' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition4">O2</label>
                        <div id="<%= Model.TypeName %>_GenericRespiratoryCondition4More" class="fr">
                            <label for="<%= Model.TypeName %>_Generic02AtText">At</label>
                            <%= Html.TextBox(Model.TypeName + "_Generic02AtText", data.AnswerOrEmptyString("Generic02AtText"), new { @id = Model.TypeName + "_Generic02AtText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Oxygen At" }) %>
                            <label for="<%= Model.TypeName %>_GenericLPMVia">LPM via</label>
                            <%  var via = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Nasal Cannula", Value = "1" },
                                    new SelectListItem { Text = "Mask", Value = "2" },
                                    new SelectListItem { Text = "Trach", Value = "3" },
                                    new SelectListItem { Text = "Other", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericLPMVia", "0"));%>
                            <%= Html.DropDownList(Model.TypeName + "_GenericLPMVia", via, new { @id = Model.TypeName + "_GenericLPMVia", @title = "(Optional) Respiratory, Oxygen Via" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Oxygen Saturation' id='{0}_GenericRespiratoryCondition5' name='{0}_GenericRespiratoryCondition' value='5' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition5">O2 Sat</label>
                        <div id="<%= Model.TypeName %>_GenericRespiratoryCondition5More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_Generic02SatText", data.AnswerOrEmptyString("Generic02SatText"), new { @id = Model.TypeName + "_Generic02SatText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Oxygen Saturation Level" })%>
                            <label>On</label>
                            <%  var satList = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Room Air", Value = "Room Air" },
                                    new SelectListItem { Text = "02", Value = "02" }
                                }, "Value", "Text", data.AnswerOrDefault("Generic02SatList", "0")); %>
                            <%= Html.DropDownList(Model.TypeName + "_Generic02SatList", satList, new { @title = "(Optional) Respiratory, Oxygen Saturation, Room Air/O2" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Nebulizer' id='{0}_GenericRespiratoryCondition6' name='{0}_GenericRespiratoryCondition' value='6' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition6">Nebulizer</label>
                        <div id="<%= Model.TypeName %>_GenericRespiratoryCondition6More" class="fr">
                            <%= Html.TextBox(Model.TypeName + "_GenericNebulizerText", data.AnswerOrEmptyString("GenericNebulizerText"), new { @id = Model.TypeName + "_GenericNebulizerText", @class = "st", @maxlength = "20", @title = "(Optional) Respiratory, Nebulizer" }) %>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Respiratory, Tracheostomy' id='{0}_GenericRespiratoryCondition7' name='{0}_GenericRespiratoryCondition' value='7' type='checkbox' {1} />", Model.TypeName, respiratoryCondition.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericRespiratoryCondition7">Tracheostomy</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericRespiratoryComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericRespiratoryComments", data.AnswerOrEmptyString("GenericRespiratoryComments"), 5, 70, new { @id = Model.TypeName + "_GenericRespiratoryComments", @title = "(Optional) Respiratory Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() != 11 && Model.AssessmentTypeNum.ToInteger() != 14) { %>
    <fieldset class="oasis">
        <legend>Dyspneic</legend>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M1400">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1400" class="green" onclick="Oasis.ToolTip('M1400')">(M1400)</a>
                    When is the patient dyspneic or noticeably Short of Breath?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1400PatientDyspneic", "", new { @id = Model.TypeName + "_M1400PatientDyspneicHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1400PatientDyspneic", "00", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("00"), new { @id = Model.TypeName + "_M1400PatientDyspneic0", @title = "(OASIS M1400) Dyspneic or Short of Breath, Not Short of Breath" })%>
                        <label for="<%= Model.TypeName %>_M1400PatientDyspneic0"><span class="float-left">0 &#8211;</span><span class="normal margin">Patient is not short of breath</span></label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1400PatientDyspneic", "01", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("01"), new { @id = Model.TypeName + "_M1400PatientDyspneic1", @title = "(OASIS M1400) Dyspneic or Short of Breath, Walking More Than 20 Feet/Climbing Stairs" })%>
                        <label for="<%= Model.TypeName %>_M1400PatientDyspneic1"><span class="float-left">1 &#8211;</span><span class="normal margin">When walking more than 20 feet, climbing stairs</span></label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1400PatientDyspneic", "02", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("02"), new { @id = Model.TypeName + "_M1400PatientDyspneic2", @title = "(OASIS M1400) Dyspneic or Short of Breath, Moderate Exertion" })%>
                        <label for="<%= Model.TypeName %>_M1400PatientDyspneic2"><span class="float-left">2 &#8211;</span><span class="normal margin">With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span></label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1400PatientDyspneic", "03", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("03"), new { @id = Model.TypeName + "_M1400PatientDyspneic3", @title = "(OASIS M1400) Dyspneic or Short of Breath, Minimal Exertion" })%>
                        <label for="<%= Model.TypeName %>_M1400PatientDyspneic3"><span class="float-left">3 &#8211;</span><span class="normal margin">With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span></label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1400PatientDyspneic", "04", data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("04"), new { @id = Model.TypeName + "_M1400PatientDyspneic4", @title = "(OASIS M1400) Dyspneic or Short of Breath, At Rest" })%>
                        <label for="<%= Model.TypeName %>_M1400PatientDyspneic4"><span class="float-left">4 &#8211;</span><span class="normal margin">At rest (during day or night)</span></label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1400')" title="More Information about M1400">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Treatments</legend>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M1410">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1410" class="green" onclick="Oasis.ToolTip('M1410')">(M1410)</a>
                    Respiratory Treatments utilized at home:
                    <em>(Mark all that apply)</em>
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsOxygen", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsOxygenHidden" })%>
                        <%= string.Format("<input title='(OASIS M1410) Respiratory Treatments, Oxygen' id='{0}_M1410HomeRespiratoryTreatmentsOxygen' class='M1410' name='{0}_M1410HomeRespiratoryTreatmentsOxygen' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsOxygen").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1410HomeRespiratoryTreatmentsOxygen">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Oxygen (intermittent or continuous)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsVentilator", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsVentilatorHidden" })%>
                        <%= string.Format("<input title='(OASIS M1410) Respiratory Treatments, Ventilator' id='{0}_M1410HomeRespiratoryTreatmentsVentilator' class='M1410' name='{0}_M1410HomeRespiratoryTreatmentsVentilator' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsVentilator").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1410HomeRespiratoryTreatmentsVentilator">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Ventilator (continually or at night)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsContinuous", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsContinuousHidden" })%>
                        <%= string.Format("<input title='(OASIS M1410) Respiratory Treatments, Positive Airway Pressure' id='{0}_M1410HomeRespiratoryTreatmentsContinuous' class='M1410' name='{0}_M1410HomeRespiratoryTreatmentsContinuous' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsContinuous").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1410HomeRespiratoryTreatmentsContinuous">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Continuous/Bi-level positive airway pressure</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1410HomeRespiratoryTreatmentsNone", "", new { @id = Model.TypeName + "_M1410HomeRespiratoryTreatmentsNoneHidden" })%>
                        <%= string.Format("<input title='(OASIS M1410) Respiratory Treatments, None of the Above' id='{0}_M1410HomeRespiratoryTreatmentsNone' class='M1410' name='{0}_M1410HomeRespiratoryTreatmentsNone' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsNone").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1410HomeRespiratoryTreatmentsNone">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1410')" title="More Information about M1410">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Respiratory.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>