<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main"> 
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "Integumentary_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>  
    <%= Html.Hidden("categoryType", "Integumentary")%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset id="<%= Model.TypeName %>_BradenScale">
        <legend>Braden Scale</legend>
        <table class="braden fixed">
            <thead>
                <tr>
                    <th>Risk Factor</th>
                    <th colspan="4">Score/Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>
                        <h4>Sensory Perception</h4>
                        <p>Ability to respond meaningfully to pressure-related discomfort</p>
                        <%= Html.Hidden(Model.TypeName + "_485BradenScaleSensory", data.AnswerOrEmptyString("485BradenScaleSensory"), new { @id = Model.TypeName + "_485BradenScaleSensoryHidden" }) %>
                    </th>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleSensory").Equals("1") ? "active" : string.Empty %>" value="1">
                        <h4><input type="radio" />1. Completely Limited &#8212;</h4>
                        <p>Unresponsive (does not moan, flinch, or grasp) to painful stimuli, due to diminished level of consciousness or sedation</p>
                        <h5>or</h5>
                        <p>limited ability to feel pain over most of body.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleSensory").Equals("2") ? "active" : string.Empty %>" value="2">
                        <h4><input type="radio" />2. Very Limited &#8212;</h4>
                        <p>Responds only to painful stimuli. Cannot communicate discomfort except by moaning or restlessness</p>
                        <h5>or</h5>
                        <p>has a sensory impairment which limits the ability to feel pain or discomfort over &frac12; of body.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleSensory").Equals("3") ? "active" : string.Empty %>" value="3">
                        <h4><input type="radio" />3. Slightly Limited &#8212;</h4>
                        <p>Responds to verbal commands, but cannot always communicate discomfort or the need to be turned</p>
                        <h5>or</h5>
                        <p>has some sensory impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleSensory").Equals("4") ? "active" : string.Empty %>" value="4">
                        <h4><input type="radio" />4. No Impairment &#8212;</h4>
                        <p>Responds to verbal commands. Has no sensory deficit which would limit ability to feel or voice pain or discomfort.</p>
                    </td>
                </tr>
                <tr>
                    <th class="total" colspan="5"><hr /></th>
                </tr>
                <tr>
                    <th>
                        <h4>Moisture</h4>
                        <p>Degree to which skin is exposed to moisture</p>
                        <%= Html.Hidden(Model.TypeName + "_485BradenScaleMoisture", data.AnswerOrEmptyString("485BradenScaleMoisture"), new { @id = Model.TypeName + "_485BradenScaleMoistureHidden" })%>
                    </th>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("1") ? "active" : string.Empty %>" value="1">
                        <h4><input type="radio" />1. Constantly Moist &#8212;</h4>
                        <p>Skin is kept moist almost constantly by perspiration, urine, etc. Dampness is detected every time patient is moved or turned.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("2") ? "active" : string.Empty %>" value="2">
                        <h4><input type="radio" />2. Often Moist &#8212;</h4>
                        <p>Skin is often, but not always moist. Linen must be changed as often as 3 times in 24 hours.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("3") ? "active" : string.Empty %>" value="3">
                        <h4><input type="radio" />3. Occasionally Moist &#8212;</h4>
                        <p>Skin is occasionally moist, requiring an extra linen change approximately once a day.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMoisture").Equals("4") ? "active" : string.Empty %>" value="4">
                        <h4><input type="radio" />4. Rarely Moist &#8212;</h4>
                        <p>Skin is usually dry; Linen only requires changing at routine intervals.</p>
                    </td>
                </tr>
                <tr>
                    <th class="total" colspan="5"><hr /></th>
                </tr>
                <tr>
                    <th>
                        <h4>Activity</h4>
                        <p>Degree of physical activity</p>
                        <%= Html.Hidden(Model.TypeName + "_485BradenScaleActivity", data.AnswerOrEmptyString("485BradenScaleActivity"), new { @id = Model.TypeName + "_485BradenScaleActivityHidden" })%>
                    </th>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleActivity").Equals("1") ? "active" : string.Empty %>" value="1">
                        <h4><input type="radio" />1. Bedfast &#8212;</h4>
                        <p>Confined to bed.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleActivity").Equals("2") ? "active" : string.Empty %>" value="2">
                        <h4><input type="radio" />2. Chairfast &#8212;</h4>
                        <p>Ability to walk severely limited or non-existent. Cannot bear own weight and/or must be assisted into chair or wheelchair.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleActivity").Equals("3") ? "active" : string.Empty %>" value="3">
                        <h4><input type="radio" />3. Walks Occasionally &#8212;</h4>
                        <p>Walks occasionally during day, but for very short distances, with or without assistance. Spends majority of day in bed or chair.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleActivity").Equals("4") ? "active" : string.Empty %>" value="4">
                        <h4><input type="radio" />4. Walks Frequently &#8212;</h4>
                        <p>Walks outside bedroom twice a day and inside room at least once every two hours during waking hours.</p>
                    </td>
                </tr>
                <tr>
                    <th class="total" colspan="5"><hr /></th>
                </tr>
                <tr>
                    <th>
                        <h4>Mobility</h4>
                        <p>Ability to change and control body position</p>
                        <%= Html.Hidden(Model.TypeName + "_485BradenScaleMobility", data.AnswerOrEmptyString("485BradenScaleMobility"), new { @id = Model.TypeName + "_485BradenScaleMobilityHidden" })%>
                    </th>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMobility").Equals("1") ? "active" : string.Empty %>" value="1">
                        <h4><input type="radio" />1. Completely Immobile &#8212;</h4>
                        <p>Does not make even slight changes in body or extremity position without assistance.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMobility").Equals("2") ? "active" : string.Empty %>" value="2">
                        <h4><input type="radio" />2. Very Limited &#8212;</h4>
                        <p>Makes occasional slight changes in body or extremity position but unable to make frequent or significant changes independently.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMobility").Equals("3") ? "active" : string.Empty %>" value="3">
                        <h4><input type="radio" />3. Slightly Limited &#8212;</h4>
                        <p>Makes frequent though slight changes in body or extremity position independently.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleMobility").Equals("4") ? "active" : string.Empty %>" value="4">
                        <h4><input type="radio" />4. No Limitation &#8212;</h4>
                        <p>Makes major and frequent changes in position without assistance.</p>
                    </td>
                </tr>
                <tr>
                    <th class="total" colspan="5"><hr /></th>
                </tr>
                <tr>
                    <th>
                        <h4>Nutrition</h4>
                        <p>Usual food intake pattern</p>
                        <%= Html.Hidden(Model.TypeName + "_485BradenScaleNutrition", data.AnswerOrEmptyString("485BradenScaleNutrition"), new { @id = Model.TypeName + "_485BradenScaleNutritionHidden" })%>
                    </th>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("1") ? "active" : string.Empty %>" value="1">
                        <h4><input type="radio" />1. Very Poor &#8212;</h4>
                        <p>Never eats a complete meal. Rarely eats more than &#8531; of any food offered. Eats 2 servings or less of protein (meat or dairy products) per day. Takes fluids poorly. Does not take a liquid dietary supplement</p>
                        <h5>or</h5>
                        <p>is <a href="javascript:void(0)" title="Nothing by Mouth">NPO</a> and/or maintained on clear liquids or <a href="javascript:void(0)" title="Intravenously">IV</a> for more than 5 days.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("2") ? "active" : string.Empty %>" value="2">
                        <h4><input type="radio" />2. Probably Inadequate &#8212;</h4>
                        <p>Rarely eats a complete meal and generally eats only about &frac12; of any food offered. Protein intake includes only 3 servings of meat or dairy products per day. Occasionally will take a dietary supplement</p>
                        <h5>or</h5>
                        <p>receives less than optimum amount of liquid diet or tube feeding.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("3") ? "active" : string.Empty %>" value="3">
                        <h4><input type="radio" />3. Adequate &#8212;</h4>
                        <p>Eats over half of most meals. Eats a total of 4 servings of protein (meat, dairy products) per day. Occasionally will refuse a meal, but will usually take a supplement when offered</p>
                        <h5>or</h5>
                        <p>is on a tube feeding or <a href="javascript:void(0)" title="Total Parenteral Nutrition">TPN</a> regimen which probably meets most of nutritional needs.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleNutrition").Equals("4") ? "active" : string.Empty %>" value="4">
                        <h4><input type="radio" />4. Excellent &#8212;</h4>
                        <p>Eats most of every meal. Never refuses a meal. Usually eats a total of 4 or more servings of meat and dairy products. Occasionally eats between meals. Does not require supplementation.</p>
                    </td>
                </tr>
                <tr>
                    <th class="total" colspan="5"><hr class="eighty" /></th>
                </tr>
                <tr>
                    <th>
                        <h4>Friction &#38; Shear</h4>
                        <%= Html.Hidden(Model.TypeName + "_485BradenScaleFriction", data.AnswerOrEmptyString("485BradenScaleFriction"), new { @id = Model.TypeName + "_485BradenScaleFrictionHidden" })%>
                    </th>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleFriction").Equals("1") ? "active" : string.Empty %>" value="1">
                        <h4><input type="radio" />1. Problem &#8212;</h4>
                        <p>Requires moderate to maximum assistance in moving. Complete lifting without sliding against sheets is impossible. Frequently slides down in bed or chair, requiring frequent repositioning with maximum assistance. Spasticity, contractures or agitation leads to almost constant friction.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleFriction").Equals("2") ? "active" : string.Empty %>" value="2">
                        <h4><input type="radio" />2. Potential Problem &#8212;</h4>
                        <p>Moves feebly or requires minimum assistance. During a move skin probably slides to some extent against sheets, chair, restraints or other devices. Maintains relatively good position in chair or bed most of the time but occasionally slides down.</p>
                    </td>
                    <td class="<%= data.AnswerOrEmptyString("485BradenScaleFriction").Equals("3") ? "active" : string.Empty %>" value="3">
                        <h4><input type="radio" />3. No Apparent Problem &#8212;</h4>
                        <p>Moves in bed and in chair independently and has sufficient muscle strength to lift up completely during move. Maintains good position in bed or chair.</p>
                    </td>
                    <th class="total">
                        <label for="<%= Model.TypeName %>_485BradenScaleTotal" class="strong">Total</label>
                        <%= Html.TextBox(Model.TypeName + "_485BradenScaleTotal", data.AnswerOrEmptyString("485BradenScaleTotal"), new { @id = Model.TypeName + "_485BradenScaleTotal", @class = "mi", @readonly="readonly"}) %>
                        <ul class="braden_score">
                            <li>19 or above: Not at Risk;</li>
                            <li>15-18: At risk;</li>
                            <li>13-14: Moderate risk;</li>
                            <li>10-12: High risk;</li>
                            <li>9 or below: Very high risk</li>
                        </ul>
                    </th>
                </tr>
            </tbody>
        </table>
        <em class="fr">Copyright. Barbara Braden and Nancy Bergstrom, 1988. Reprinted with permission. All Rights Reserved.</em>
    </fieldset>
    <fieldset>
        <legend>Integumentary Status</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Skin Turgor</label>
                <div class="fr">
                    <%= Html.Hidden(Model.TypeName + "_GenericSkinTurgor", "", new { @id = Model.TypeName + "_GenericSkinTurgorHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_GenericSkinTurgor", "Good", data.AnswerOrEmptyString("GenericSkinTurgor").Equals("Good"), new { @id = Model.TypeName + "_GenericSkinTurgorGood", @class = "radio deselectable", @title = "(Optional) Skin Turgor, Good" })%>
                    <label for="<%= Model.TypeName %>_GenericSkinTurgorGood" class="fixed short">Good</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericSkinTurgor", "Fair", data.AnswerOrEmptyString("GenericSkinTurgor").Equals("Fair"), new { @id = Model.TypeName + "_GenericSkinTurgorFair", @class = "radio deselectable", @title = "(Optional) Skin Turgor, Fair" })%>
                    <label for="<%= Model.TypeName %>_GenericSkinTurgorFair" class="fixed short">Fair</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericSkinTurgor", "Poor", data.AnswerOrEmptyString("GenericSkinTurgor").Equals("Poor"), new { @id = Model.TypeName + "_GenericSkinTurgorPoor", @class = "radio deselectable", @title = "(Optional) Skin Turgor, Poor" })%>
                    <label for="<%= Model.TypeName %>_GenericSkinTurgorPoor" class="fixed short">Poor</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Skin Temperature</label>
                <div class="fr">
                    <%  var skinTemp = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Warm", Value = "1" },
                            new SelectListItem { Text = "Cool", Value = "2" },
                            new SelectListItem { Text = "Clammy", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericSkinTemp", "0")); %>
                    <%= Html.DropDownList(Model.TypeName + "_GenericSkinTemp", skinTemp, new { @id = Model.TypeName + "_GenericSkinTemp", @title = "(Optional) Skin Temperature" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label class="strong">Skin Color</label>
                <%  string[] skinColor = data.AnswerArray("GenericSkinColor"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericSkinColor", "", new { @id = Model.TypeName + "_GenericSkinColorHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Color, Pink' id='{0}_GenericSkinColor1' name='{0}_GenericSkinColor' value='1' type='checkbox' {1} />", Model.TypeName, skinColor.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinColor1">Pink/WNL</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Color, Pallor' id='{0}_GenericSkinColor2' name='{0}_GenericSkinColor' value='2' type='checkbox' {1} />", Model.TypeName, skinColor.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinColor2">Pallor</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Color, Jaundice' id='{0}_GenericSkinColor3' name='{0}_GenericSkinColor' value='3' type='checkbox' {1} />", Model.TypeName, skinColor.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinColor3">Jaundice</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Color, Cyanotic' id='{0}_GenericSkinColor4' name='{0}_GenericSkinColor' value='4' type='checkbox' {1} />", Model.TypeName, skinColor.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinColor4">Cyanotic</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Color, Other' id='{0}_GenericSkinColor5' name='{0}_GenericSkinColor' value='5' type='checkbox' {1} />", Model.TypeName, skinColor.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinColor5">Other</label>
                        <div id="<%= Model.TypeName %>_GenericSkinColor5More">
                            <label for="<%= Model.TypeName %>_GenericSkinColorOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericSkinColorOther", data.AnswerOrEmptyString("GenericSkinColorOther"), new { @id = Model.TypeName + "_GenericSkinColorOther", @class = "st", @maxlength = "20", @title = "(Optional) Skin Color, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong">Condition</label>
                <%  string[] skinCondition = data.AnswerArray("GenericSkinCondition"); %>
                <%= Html.Hidden(Model.TypeName + "_GenericSkinCondition", "", new { @id = Model.TypeName + "_GenericSkinConditionHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Condition, Dry' id='{0}_GenericSkinCondition1' name='{0}_GenericSkinCondition' value='1' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinCondition1">Dry</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Condition, Wound' id='{0}_GenericSkinCondition2' name='{0}_GenericSkinCondition' value='2' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinCondition2">Wound</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Condition, Ulcer' id='{0}_GenericSkinCondition3' name='{0}_GenericSkinCondition' value='3' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinCondition3">Ulcer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Condition, Incision' id='{0}_GenericSkinCondition4' name='{0}_GenericSkinCondition' value='4' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinCondition4">Incision</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Condition, Rash' id='{0}_GenericSkinCondition5' name='{0}_GenericSkinCondition' value='5' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinCondition5">Rash</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Skin Condition, Other' id='{0}_GenericSkinCondition6' name='{0}_GenericSkinCondition' value='6' type='checkbox' {1} />", Model.TypeName, skinCondition.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_GenericSkinCondition6">Other</label>
                        <div id="<%= Model.TypeName %>_GenericSkinCondition6More">
                            <label for="<%= Model.TypeName %>_GenericSkinConditionOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericSkinConditionOther", data.AnswerOrEmptyString("GenericSkinConditionOther"), new { @id = Model.TypeName + "_GenericSkinConditionOther", @class = "st", @maxlength = "20", @title = "(Optional) Skin Condition, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label class="float-left">Instructed on measures to control infections?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericInstructedControlInfections", "", new { @id = Model.TypeName + "_GenericInstructedControlInfectionsHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericInstructedControlInfections", "1", data.AnswerOrEmptyString("GenericInstructedControlInfections").Equals("1"), new { @id = Model.TypeName + "_GenericInstructedControlInfectionsYes", @class = "radio deselectable", @title = "(Optional) Instructed on Infection Control, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericInstructedControlInfectionsYes" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericInstructedControlInfections", "0", data.AnswerOrEmptyString("GenericInstructedControlInfections").Equals("0"), new { @id = Model.TypeName + "_GenericInstructedControlInfectionsNo", @class = "radio deselectable", @title = "(Optional) Instructed on Infection Control, No" })%>
                    <label for="<%= Model.TypeName %>_GenericInstructedControlInfectionsNo" class="fixed short">No</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Nail Condition</label>
                <%= Html.Hidden(Model.TypeName + "_GenericNails", "", new { @id = Model.TypeName + "_GenericNailsHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericNails", "Good", data.AnswerOrEmptyString("GenericNails").Equals("Good"), new { @id = Model.TypeName + "_GenericNailsGood", @title = "(Optional) Nail Condition, Good" }) %>
                    <label for="<%= Model.TypeName %>_GenericNailsGood" class="fixed short">Good</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericNails", "Problem", data.AnswerOrEmptyString("GenericNails").Equals("Problem"), new { @id = Model.TypeName + "_GenericNailsProblem", @title = "(Optional) Nail Condition, Problem" })%>
                    <label for="<%= Model.TypeName %>_GenericNailsProblem" class="fixed short">Problems</label>
                </div>
                <div class="clear"></div>
                <div id="<%= Model.TypeName %>_GenericNailsProblemMore" class="fr">
                    <label for="<%= Model.TypeName %>_GenericNailsProblemOther"><em>(Specify)</em></label>
                    <%= Html.TextBox(Model.TypeName + "_GenericNailsProblemOther", data.AnswerOrEmptyString("GenericNailsProblemOther"), new { @id = Model.TypeName + "_GenericNailsProblemOther", @maxlength = "20", @title = "(Optional) Nail Condition, Specify Problem" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Is patient using pressure-relieving device(s)?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericPressureRelievingDevice", "", new { @id = Model.TypeName + "_GenericPressureRelievingDeviceHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericPressureRelievingDevice", "1", data.AnswerOrEmptyString("GenericPressureRelievingDevice").Equals("1"), new { @id = Model.TypeName + "_GenericPressureRelievingDeviceYes", @title = "(Optional) Pressure-Relieving Device, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericPressureRelievingDeviceYes" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericPressureRelievingDevice", "0", data.AnswerOrEmptyString("GenericPressureRelievingDevice").Equals("0"), new { @id = Model.TypeName + "_GenericPressureRelievingDeviceNo", @title = "(Optional) Pressure-Relieving Device, No" })%>
                    <label for="<%= Model.TypeName %>_GenericPressureRelievingDeviceNo" class="fixed short">No</label>
                </div>
                <div class="clear"></div>
                <div id="<%= Model.TypeName %>_GenericPressureRelievingDeviceMore" class="fr">
                    <label for="<%= Model.TypeName %>_GenericPressureRelievingDeviceType">Type</label>
                    <%  var pressureRelievingDeviceType = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Low Air Mattress", Value = "1" },
                            new SelectListItem { Text = "Gel Cushion", Value = "2" },
                            new SelectListItem { Text = "Egg Crate", Value = "3" },
                            new SelectListItem { Text = "Other (Specify)", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericPressureRelievingDeviceType", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericPressureRelievingDeviceType", pressureRelievingDeviceType, new { @id = Model.TypeName + "_GenericPressureRelievingDeviceType", @class = "loc", @title = "(Optional) Pressure-Relieving Device" })%>
                    <%= Html.TextBox(Model.TypeName + "_GenericPressureRelievingDeviceTypeOther", data.AnswerOrEmptyString("GenericPressureRelievingDeviceTypeOther"), new { @id = Model.TypeName + "_GenericPressureRelievingDeviceTypeOther", @class = "loc", @maxlength = "20", @title = "(Optional) Pressure-Relieving Device, Specify Other" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIntegumentaryStatusComments" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericIntegumentaryStatusComments", data.AnswerOrEmptyString("GenericIntegumentaryStatusComments"), 5, 70, new { @id = Model.TypeName + "_GenericIntegumentaryStatusComments", @title = "(Optional) Integumentary Comments" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>IV Access</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Does patient have IV access?</label>
                <%= Html.Hidden(Model.TypeName + "_GenericIVAccess", "", new { @id = Model.TypeName + "_GenericIVAccessHidden" })%>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericIVAccess", "1", data.AnswerOrEmptyString("GenericIVAccess").Equals("1"), new { @id = Model.TypeName + "_GenericIVAccess1", @title = "(Optional) IV Access, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericIVAccess1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericIVAccess", "0", data.AnswerOrEmptyString("GenericIVAccess").Equals("0"), new { @id = Model.TypeName + "_GenericIVAccess0", @title = "(Optional) IV Access, No" })%>
                    <label for="<%= Model.TypeName %>_GenericIVAccess0" class="fixed short">No</label>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="<%= Model.TypeName %>_IVAccess column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIVAccessDate" class="float-left">Date of Insertion</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericIVAccessDate" value="<%= data.AnswerOrEmptyString("GenericIVAccessDate") %>" id="<%= Model.TypeName %>_GenericIVAccessDate" title="(Optional) IV Insertion Date" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIVAccessType" class="float-left">IV Access Type</label>
                <div class="fr">
                    <%  var IVIVAccess = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Saline Lock", Value = "1" },
                            new SelectListItem { Text = "PICC Line", Value = "2" },
                            new SelectListItem { Text = "Central Line", Value = "3" },
                            new SelectListItem { Text = "Port-A-Cath", Value = "4" },
                            new SelectListItem { Text = "Med-A-Port", Value = "5" },
                            new SelectListItem { Text = "Other", Value = "6" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVAccessType", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIVAccessType", IVIVAccess, new { @id = Model.TypeName + "_GenericIVAccessType", @title = "(Optional) IV Access Type" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIVLocation" class="float-left">IV Location</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_GenericIVLocation", data.AnswerOrEmptyString("GenericIVLocation"), new { @id = Model.TypeName + "_GenericIVLocation", @maxlength = "25", @title = "(Optional) IV Location" })%></div>
            </div>
            <div class="row">
                <label for="GenericIVConditionOfIVSite" class="float-left">Condition of IV Site</label>
                <div class="fr">
                    <%  var IVConditionOfIVSite = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "WNL", Value = "1" },
                            new SelectListItem { Text = "Phlebitis", Value = "2" },
                            new SelectListItem { Text = "Redness", Value = "3" },
                            new SelectListItem { Text = "Swelling", Value = "4" },
                            new SelectListItem { Text = "Pallor ", Value = "5" },
                            new SelectListItem { Text = "Warmth ", Value = "6" },
                            new SelectListItem { Text = "Bleeding ", Value = "7" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfIVSite", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIVConditionOfIVSite", IVConditionOfIVSite, new { @id = Model.TypeName + "_GenericIVConditionOfIVSite", @title = "(Optional) IV Site Condition" })%>
                </div>
            </div>
        </div>
        <div class="<%= Model.TypeName %>_IVAccess column">
            <div class="row">
                <label for="GenericIVSiteDressing" class="float-left">IV site Dressing Changed performed on this visit</label>
                <div class="fr">
                    <%  var IVSiteDressing = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "N/A", Value = "1" },
                            new SelectListItem { Text = "Yes", Value = "2" },
                            new SelectListItem { Text = "No", Value = "3" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressing", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIVSiteDressing", IVSiteDressing, new { @id = Model.TypeName + "_GenericIVSiteDressing", @title = "(Optional) IV Dressing Change Performed this Visit" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIVAccessDressingChange" class="float-left">Date of Last Dressing Change</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericIVAccessDateOfLastDressingChange" value="<%= data.AnswerOrEmptyString("GenericIVAccessDateOfLastDressingChange") %>" id="<%= Model.TypeName %>_GenericIVAccessDateOfLastDressingChange" title="(Optional) Date of Last IV Dressing Change" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIVConditionOfDressing" class="float-left">Condition of Dressing</label>
                <div class="fr">
                    <%  var IVConditionOfDressing = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Dry & Intact/WNL", Value = "1" },
                            new SelectListItem { Text = "Bloody", Value = "2" },
                            new SelectListItem { Text = "Soiled", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfDressing", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIVConditionOfDressing", IVConditionOfDressing, new { @id = Model.TypeName + "_GenericIVConditionOfDressing", @title = "(Optional) Condition of IV Dressing" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Flush</label>
                <div class="fr">
                    <%= Html.RadioButton(Model.TypeName + "_GenericFlush", "1", data.AnswerOrEmptyString("GenericFlush").Equals("1"), new { @id = Model.TypeName + "_GenericFlush1", @class = " radio", @title = "(Optional) IV Flush, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericFlush1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericFlush", "0", data.AnswerOrEmptyString("GenericFlush").Equals("0"), new { @id = Model.TypeName + "_GenericFlush2", @class = " radio", @title = "(Optional) IV Flush, No" })%>
                    <label for="<%= Model.TypeName %>_GenericFlush2" class="fixed short">No</label>
                </div>
                <div class="clear"></div>
                <div class="fr" id="<%= Model.TypeName %>_GenericFlushMore">
                    <label for="<%= Model.TypeName %>_GenericIVAccessFlushed">flushed with</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericIVAccessFlushed", data.AnswerOrEmptyString("GenericIVAccessFlushed"), new { @class = "st numeric", @id = Model.TypeName + "_GenericIVAccessFlushed", @maxlength = "5", @title = "(Optional) IV Flush Amount (ml)" })%>
                    <label for="<%= Model.TypeName %>_GenericIVAccessFlushed">/ml of</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericIVSiteDressingUnit", data.AnswerOrEmptyString("GenericIVSiteDressingUnit"), new { @id = Model.TypeName + "_GenericIVSiteDressingUnit", @class = "IVSiteDressingUnit", @maxlength = "25", @title = "(Optional) IV Flush Type" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <% Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Intravenous.ascx", Model); %>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() != 11 && Model.AssessmentTypeNum.ToInteger() != 14) { %>
    <fieldset class="oasis">
        <legend>Ulcers</legend>
        <div class="wide column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row" id="<%= Model.TypeName %>_M1300">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1300" class="green" onclick="Oasis.ToolTip('M1300')">(M1300)</a>
                    Pressure Ulcer Assessment: Was this patient assessed for Risk of Developing Pressure Ulcers?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1300PressureUlcerAssessment", "", new { @id = Model.TypeName + "_M1300PressureUlcerAssessmentHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1300PressureUlcerAssessment", "00", data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("00"), new { @id = Model.TypeName + "_M1300PressureUlcerAssessment0", @title = "(OASIS M1300) Pressure Ulcer Assessment, Not Conducted" })%>
                        <label for="<%= Model.TypeName %>_M1300PressureUlcerAssessment0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No assessment conducted</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1300PressureUlcerAssessment", "01", data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("01"), new { @id = Model.TypeName + "_M1300PressureUlcerAssessment1", @title = "(OASIS M1300) Pressure Ulcer Assessment, Non-standardized Tool" })%>
                        <label for="<%= Model.TypeName %>_M1300PressureUlcerAssessment1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, based on an evaluation of clinical factors, e.g., mobility, incontinence, nutrition, etc., without use of standardized tool</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1300PressureUlcerAssessment", "02", data.AnswerOrEmptyString("M1300PressureUlcerAssessment").Equals("02"), new { @id = Model.TypeName + "_M1300PressureUlcerAssessment2", @title = "(OASIS M1300) Pressure Ulcer Assessment, Standardized Tool" })%>
                        <label for="<%= Model.TypeName %>_M1300PressureUlcerAssessment2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Yes, using a standardized tool, e.g., Braden, Norton, other</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1300')" title="More Information about M1300">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1302">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1302" class="green" onclick="Oasis.ToolTip('M1302')">(M1302)</a>
                    Does this patient have a Risk of Developing Pressure Ulcers?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1302RiskDevelopingPressureUlcers", "", new { @id = Model.TypeName + "_M1302RiskDevelopingPressureUlcersHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1302RiskDevelopingPressureUlcers", "0", data.AnswerOrEmptyString("M1302RiskDevelopingPressureUlcers").Equals("0"), new { @id = Model.TypeName + "_M1302RiskDevelopingPressureUlcers0", @title = "(OASIS M1302) Risk of Developing Pressure Ulcers, No" })%>
                        <label for="<%= Model.TypeName %>_M1302RiskDevelopingPressureUlcers0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1302RiskDevelopingPressureUlcers", "1", data.AnswerOrEmptyString("M1302RiskDevelopingPressureUlcers").Equals("1"), new { @id = Model.TypeName + "_M1302RiskDevelopingPressureUlcers1", @title = "(OASIS M1302) Risk of Developing Pressure Ulcers, Yes" })%>
                        <label for="<%= Model.TypeName %>_M1302RiskDevelopingPressureUlcers1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1302')" title="More Information about M1302">?</div>
                </div>
            </div>
            <%  } %>
            <div class="row" id="<%= Model.TypeName %>_M1306">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1306" class="green" onclick="Oasis.ToolTip('M1306')">(M1306)</a>
                    Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &#8220;unstageable&#8221;?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1306UnhealedPressureUlcers", "", new { @id = Model.TypeName + "_M1306UnhealedPressureUlcersHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1306UnhealedPressureUlcers", "0", data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("0"), new { @id = Model.TypeName + "_M1306UnhealedPressureUlcers0", @title = "(OASIS M1306) Unhealed Pressure Ulcer at Stage II or Higher, No" })%>
                        <label for="<%= Model.TypeName %>_M1306UnhealedPressureUlcers0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1306UnhealedPressureUlcers", "1", data.AnswerOrEmptyString("M1306UnhealedPressureUlcers").Equals("1"), new { @id = Model.TypeName + "_M1306UnhealedPressureUlcers1", @title = "(OASIS M1306) Unhealed Pressure Ulcer at Stage II or Higher, Yes" })%>
                        <label for="<%= Model.TypeName %>_M1306UnhealedPressureUlcers1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1306')" title="More Information about M1306">?</div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
            <div class="row <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1307">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1307')">(M1307)</a>
                    The Oldest Non-epithelialized Stage II Pressure Ulcer that is present at discharge
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcerDate", "", new { @id = Model.TypeName + "_M1307NonEpithelializedStageTwoUlcerDateHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer", "01", data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("01"), new { @id = Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer01" })%>
                        <label for="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcer01">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Was present at the most recent SOC/ROC assessment</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer", "02", data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("02"), new { @id = Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer02" })%>
                        <label for="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcer02">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Developed since the most recent SOC/ROC assessment: record date pressure ulcer first identified:</span>
                        </label>
                        <div id="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcer02More">
                            <input type="text" class="date-picker" name="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcerDate" value="<%= data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcerDate") %>" />
                        </div>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1307NonEpithelializedStageTwoUlcer", "NA", data.AnswerOrEmptyString("M1307NonEpithelializedStageTwoUlcer").Equals("NA"), new { @id = Model.TypeName + "_M1307NonEpithelializedStageTwoUlcerNA" })%>
                        <label for="<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcerNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No non-epithelialized Stage II pressure ulcers are present at discharge</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1307')">?</div>
                </div>
            </div>
            <%  } %>
            <div class="row <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1308">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1308" class="green" onclick="Oasis.ToolTip('M1308')">(M1308)</a>
                    Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage: (Enter &#8220;0&#8221; if none; excludes Stage I pressure ulcers)
                </label>
                <table class="form">
                    <thead>
                        <tr>
                            <th colspan="2">Stage description &#8211; unhealed pressure ulcers</td>
                            <th>Number Currently Present</td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <th>Number of those listed in Column 1 that were present on admission (most recent SOC/ ROC)</td>
                            <%  } %>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <span class="float-left">a.</span>
                                <span>Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.</span>
                            </td>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageTwoUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerCurrent", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Present Number Stage II Ulcers" })%></td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageTwoUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageTwoUlcerAdmission", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Number Stage II Ulcers at Admission" })%></td>
                            <%  } %>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="float-left">b.</span>
                                <span>Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.</span>
                            </td>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageThreeUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Present Number Stage III Ulcers" })%></td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageThreeUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageThreeUlcerAdmission", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Number Stage III Ulcers at Admission" })%></td>
                            <%  } %>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="float-left">c.</span>
                                <span>Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.</span>
                            </td>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageFourUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageFourUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageFourUlcerCurrent", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Present Number Stage IV Ulcers" })%></td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedStageIVUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedStageIVUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedStageIVUlcerAdmission", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Number Stage IV Ulcers at Admission" })%></td>
                            <%  } %>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="float-left">d.1</span>
                                <span>Unstageable: Known or likely but unstageable due to non-removable dressing or device</span>
                            </td>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Present Number Unstageable Ulcers (Due to Non-removable Dressing/Device)" })%></td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Number Unstageable Ulcers at Admission (Due to Non-removable Dressing/Device)" })%></td>
                            <%  } %>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="float-left">d.2</span>
                                <span>Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.</span>
                            </td>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Present Number Unstageable Ulcers (Due to Coverage of Wound)" })%></td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Number Unstageable Ulcers at Admission (Due to Coverage of Wound)" })%></td>
                            <%  } %>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span class="float-left">d.3</span>
                                <span>Unstageable: Suspected deep tissue injury in evolution.</span>
                            </td>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Present Number Unstageable Ulcers (Suspected Deep Tissue Injury)" })%></td>
                            <%  if (Model.AssessmentTypeNum.ToInteger() > 3) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", data.AnswerOrEmptyString("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"), new { @id = Model.TypeName + "_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", @class = "sn numeric", @maxlength = "5", @title = "(OASIS M1308) Number Unstageable Ulcers at Admission (Suspected Deep Tissue Injury)" })%></td>
                            <%  } %>
                        </tr>
                    </tbody>
                </table>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1308')" title="More Information about M1308">?</div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1310">
                <label class="strong">Directions for M1310, M1312, and M1314</label>
                If the patient has one or more unhealed (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or IV pressure ulcer with the largest surface dimension (length x width) and record in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.
                <label for="<%= Model.TypeName %>_M1310PressureUlcerLength" class="float-left">
                    <a href="javascript:void(0)" title="More Information about M1310" class="green" onclick="Oasis.ToolTip('M1310')">(M1310)</a>
                    Pressure Ulcer Length: Longest length &#8220;head-to-toe&#8221;
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M1310PressureUlcerLength", data.AnswerOrEmptyString("M1310PressureUlcerLength"), new { @id = Model.TypeName + "_M1310PressureUlcerLength", @maxlength = "2", @class = "sn numeric", @title = "(OASIS M1310) Pressure Ulcer Length" })%>
                    <label class="strong">.</label>
                    <%= Html.TextBox(Model.TypeName + "_M1310PressureUlcerLengthDecimal", data.AnswerOrEmptyString("M1310PressureUlcerLengthDecimal"), new { @id = Model.TypeName + "_M1310PressureUlcerLengthDecimal", @maxlength = "1", @class = "sn numeric", @title = "(OASIS M1310) Pressure Ulcer Length" })%>
                    <label class="strong">cm</label>
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1310')" title="More Information about M1310">?</div>
                </div>
            </div>
            <div class="row <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1312">
                <label for="<%= Model.TypeName %>_M1312PressureUlcerWidth" class="float-left">
                    <a href="javascript:void(0)" title="More Information about M1312" class="green" onclick="Oasis.ToolTip('M1312')">(M1312)</a>
                    Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular to the length
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M1312PressureUlcerWidth", data.AnswerOrEmptyString("M1312PressureUlcerWidth"), new { @id = Model.TypeName + "_M1312PressureUlcerWidth", @maxlength = "2", @class = "sn numeric", @title = "(OASIS M1312) Pressure Ulcer Width" })%>
                    <label class="strong">.</label>
                    <%= Html.TextBox(Model.TypeName + "_M1312PressureUlcerWidthDecimal", data.AnswerOrEmptyString("M1312PressureUlcerWidthDecimal"), new { @id = Model.TypeName + "_M1312PressureUlcerWidthDecimal", @maxlength = "1", @class = "sn numeric", @title = "(OASIS M1312) Pressure Ulcer Width" })%>
                    <label class="strong">cm</label>
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1312')" title="More Information about M1312">?</div>
                </div>
            </div>
            <div class="row <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1314">
                <label for="<%= Model.TypeName %>_M1314PressureUlcerDepth" class="float-left">
                    <a href="javascript:void(0)" title="More Information about M1314" class="green" onclick="Oasis.ToolTip('M1314')">(M1314)</a>
                    Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface to the deepest area
                </label>
                <div class="fr oasis">
                    <%= Html.TextBox(Model.TypeName + "_M1314PressureUlcerDepth", data.AnswerOrEmptyString("M1314PressureUlcerDepth"), new { @id = Model.TypeName + "_M1314PressureUlcerDepth", @maxlength = "2", @class = "sn numeric", @title = "(OASIS M1314) Pressure Ulcer Depth" })%>
                    <label class="strong">.</label>
                    <%= Html.TextBox(Model.TypeName + "_M1314PressureUlcerDepthDecimal", data.AnswerOrEmptyString("M1314PressureUlcerDepthDecimal"), new { @id = Model.TypeName + "_M1314PressureUlcerDepthDecimal", @maxlength = "1", @class = "sn numeric", @title = "(OASIS M1314) Pressure Ulcer Depth" })%>
                    <label class="strong">cm</label>
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1314')" title="More Information about M1314">?</div>
                </div>
            </div>
            <div class="row <%= Model.TypeName %>_M1306_00" id="<%= Model.TypeName %>_M1320">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1320" class="green" onclick="Oasis.ToolTip('M1320')">(M1320)</a>
                    Status of Most Problematic (Observable) Pressure Ulcer
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "", new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatusHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "00", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("00"), new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatus0", @title = "(OASIS M1320) Status of Most Problematic Pressure Ulcer, Newly Epithelialized" })%>
                        <label for="<%= Model.TypeName %>_M1320MostProblematicPressureUlcerStatus0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Newly epithelialized</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "01", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("01"), new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatus1", @title = "(OASIS M1320) Status of Most Problematic Pressure Ulcer, Fully Granulating" })%>
                        <label for="<%= Model.TypeName %>_M1320MostProblematicPressureUlcerStatus1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Fully granulating</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "02", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("02"), new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatus2", @title = "(OASIS M1320) Status of Most Problematic Pressure Ulcer, Early/Partial Granulation" })%>
                        <label for="<%= Model.TypeName %>_M1320MostProblematicPressureUlcerStatus2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Early/partial granulation</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "03", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("03"), new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatus3", @title = "(OASIS M1320) Status of Most Problematic Pressure Ulcer, Not Healing" })%>
                        <label for="<%= Model.TypeName %>_M1320MostProblematicPressureUlcerStatus3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Not healing</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1320MostProblematicPressureUlcerStatus", "NA", data.AnswerOrEmptyString("M1320MostProblematicPressureUlcerStatus").Equals("NA"), new { @id = Model.TypeName + "_M1320MostProblematicPressureUlcerStatusNA", @title = "(OASIS M1320) Status of Most Problematic Pressure Ulcer, Not Applicable" })%>
                        <label for="<%= Model.TypeName %>_M1320MostProblematicPressureUlcerStatusNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No observable pressure ulcer</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1320')" title="More Information about M1320">?</div>
                </div>
            </div>
            <%  } %>
            <div class="row" id="<%= Model.TypeName %>_M1322">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1322" class="green" onclick="Oasis.ToolTip('M1322')">(M1322)</a>
                    Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "", new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcerHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "00", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("00"), new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcer0", @title = "(OASIS M1322) Current Number of Stage I Pressure Ulcers, None" })%>
                        <label for="<%= Model.TypeName %>_M1322CurrentNumberStageIUlcer0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Zero</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "01", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("01"), new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcer1", @title = "(OASIS M1322) Current Number of Stage I Pressure Ulcers, One" })%>
                        <label for="<%= Model.TypeName %>_M1322CurrentNumberStageIUlcer1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">One</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "02", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("02"), new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcer2", @title = "(OASIS M1322) Current Number of Stage I Pressure Ulcers, Two" })%>
                        <label for="<%= Model.TypeName %>_M1322CurrentNumberStageIUlcer2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Two</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "03", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("03"), new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcer3", @title = "(OASIS M1322) Current Number of Stage I Pressure Ulcers, Three" })%>
                        <label for="<%= Model.TypeName %>_M1322CurrentNumberStageIUlcer3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Three</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1322CurrentNumberStageIUlcer", "04", data.AnswerOrEmptyString("M1322CurrentNumberStageIUlcer").Equals("04"), new { @id = Model.TypeName + "_M1322CurrentNumberStageIUlcer4", @title = "(OASIS M1322) Current Number of Stage I Pressure Ulcers, Four or More" })%>
                        <label for="<%= Model.TypeName %>_M1322CurrentNumberStageIUlcer4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Four or more</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1322')" title="More Information about M1322">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1324">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1324" class="green" onclick="Oasis.ToolTip('M1324')">(M1324)</a>
                    Stage of Most Problematic Unhealed (Observable) Pressure Ulcer
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1324MostProblematicUnhealedStage", "", new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStageHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1324MostProblematicUnhealedStage", "01", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("01"), new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStage1", @title = "(OASIS M1324) Stage of Most Problematic Unhealed Pressure Ulcer, Stage I" })%>
                        <label for="<%= Model.TypeName %>_M1324MostProblematicUnhealedStage1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Stage I</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1324MostProblematicUnhealedStage", "02", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("02"), new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStage2", @title = "(OASIS M1324) Stage of Most Problematic Unhealed Pressure Ulcer, Stage II" })%>
                        <label for="<%= Model.TypeName %>_M1324MostProblematicUnhealedStage2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Stage II</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1324MostProblematicUnhealedStage", "03", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("03"), new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStage3", @title = "(OASIS M1324) Stage of Most Problematic Unhealed Pressure Ulcer, Stage III" })%>
                        <label for="<%= Model.TypeName %>_M1324MostProblematicUnhealedStage3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Stage III</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1324MostProblematicUnhealedStage", "04", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("04"), new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStage4", @title = "(OASIS M1324) Stage of Most Problematic Unhealed Pressure Ulcer, Stage IV" })%>
                        <label for="<%= Model.TypeName %>_M1324MostProblematicUnhealedStage4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Stage IV</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1324MostProblematicUnhealedStage", "NA", data.AnswerOrEmptyString("M1324MostProblematicUnhealedStage").Equals("NA"), new { @id = Model.TypeName + "_M1324MostProblematicUnhealedStageNA", @title = "(OASIS M1324) Stage of Most Problematic Unhealed Pressure Ulcer, Not Applicable" })%>
                        <label for="<%= Model.TypeName %>_M1324MostProblematicUnhealedStageNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No observable pressure ulcer or unhealed pressure ulcer</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1324')" title="More Information about M1324">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1330">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1330" class="green" onclick="Oasis.ToolTip('M1330')">(M1330)</a>
                    Does this patient have a Stasis Ulcer?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1330StasisUlcer", "", new { @id = Model.TypeName + "_M1330StasisUlcerHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1330StasisUlcer", "00", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("00"), new { @id = Model.TypeName + "_M1330StasisUlcer0", @title = "(OASIS M1330) Stasis Ulcer, No" })%>
                        <label for="<%= Model.TypeName %>_M1330StasisUlcer0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1330StasisUlcer", "01", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("01"), new { @id = Model.TypeName + "_M1330StasisUlcer1", @title = "(OASIS M1330) Stasis Ulcer, Observable and Unobservable" })%>
                        <label for="<%= Model.TypeName %>_M1330StasisUlcer1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, patient has BOTH observable and unobservable stasis ulcers</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1330StasisUlcer", "02", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("02"), new { @id = Model.TypeName + "_M1330StasisUlcer2", @title = "(OASIS M1330) Stasis Ulcer, Observable" })%>
                        <label for="<%= Model.TypeName %>_M1330StasisUlcer2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Yes, patient has observable stasis ulcers ONLY</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1330StasisUlcer", "03", data.AnswerOrEmptyString("M1330StasisUlcer").Equals("03"), new { @id = Model.TypeName + "_M1330StasisUlcer3", @title = "(OASIS M1330) Stasis Ulcer, Unobservable" })%>
                        <label for="<%= Model.TypeName %>_M1330StasisUlcer3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">
                                Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing)
                                <em>[Go to M1340]</em>
                            </span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1330')" title="More Information about M1330">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1332">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1332" class="green" onclick="Oasis.ToolTip('M1332')">(M1332)</a>
                    Current Number of (Observable) Stasis Ulcer(s)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "", new { @id = Model.TypeName + "_M1332CurrentNumberStasisUlcerHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "01", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("01"), new { @id = Model.TypeName + "_M1332CurrentNumberStasisUlcer1", @title = "(OASIS M1332) Number of Stasis Ulcers, One" })%>
                        <label for="<%= Model.TypeName %>_M1332CurrentNumberStasisUlcer1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">One</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "02", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("02"), new { @id = Model.TypeName + "_M1332CurrentNumberStasisUlcer2", @title = "(OASIS M1332) Number of Stasis Ulcers, Two" })%>
                        <label for="<%= Model.TypeName %>_M1332CurrentNumberStasisUlcer2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Two</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "03", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("03"), new { @id = Model.TypeName + "_M1332CurrentNumberStasisUlcer3", @title = "(OASIS M1332) Number of Stasis Ulcers, Three" })%>
                        <label for="<%= Model.TypeName %>_M1332CurrentNumberStasisUlcer3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Three</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1332CurrentNumberStasisUlcer", "04", data.AnswerOrEmptyString("M1332CurrentNumberStasisUlcer").Equals("04"), new { @id = Model.TypeName + "_M1332CurrentNumberStasisUlcer4", @title = "(OASIS M1332) Number of Stasis Ulcers, Four or More" })%>
                        <label for="<%= Model.TypeName %>_M1332CurrentNumberStasisUlcer4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Four or more</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1332')" title="More Information about M1332">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1334">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1334" class="green" onclick="Oasis.ToolTip('M1334')">(M1334)</a>
                    Status of Most Problematic (Observable) Stasis Ulcer
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1334StasisUlcerStatus", "", new { @id = Model.TypeName + "_M1334StasisUlcerStatusHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1334StasisUlcerStatus", "00", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("00"), new { @id = Model.TypeName + "_M1334StasisUlcerStatus0", @title = "(OASIS M1334) Status of Most Problematic Stasis Ulcer, Newly Epithelialized" })%>
                        <label for="<%= Model.TypeName %>_M1334StasisUlcerStatus0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Newly epithelialized</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1334StasisUlcerStatus", "01", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("01"), new { @id = Model.TypeName + "_M1334StasisUlcerStatus1", @title = "(OASIS M1334) Status of Most Problematic Stasis Ulcer, Fully Granulating" })%>
                        <label for="<%= Model.TypeName %>_M1334StasisUlcerStatus1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Fully granulating</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1334StasisUlcerStatus", "02", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("02"), new { @id = Model.TypeName + "_M1334StasisUlcerStatus2", @title = "(OASIS M1334) Status of Most Problematic Stasis Ulcer, Early/Partial Granulation" })%>
                        <label for="<%= Model.TypeName %>_M1334StasisUlcerStatus2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Early/partial granulation</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1334StasisUlcerStatus", "03", data.AnswerOrEmptyString("M1334StasisUlcerStatus").Equals("03"), new { @id = Model.TypeName + "_M1334StasisUlcerStatus3", @title = "(OASIS M1334) Status of Most Problematic Stasis Ulcer, Not Healing" })%>
                        <label for="<%= Model.TypeName %>_M1334StasisUlcerStatus3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Not healing</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1334')" title="More Information about M1334">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Surgical Wounds</legend>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M1340">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1340" class="green" onclick="Oasis.ToolTip('M1340')">(M1340)</a>
                    Does this patient have a Surgical Wound?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1340SurgicalWound", "", new { @id = Model.TypeName + "_M1340SurgicalWoundHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1340SurgicalWound", "00", data.AnswerOrEmptyString("M1340SurgicalWound").Equals("00"), new { @id = Model.TypeName + "_M1340SurgicalWound0", @title = "(OASIS M1340) Surgical Wound, No" })%>
                        <label for="<%= Model.TypeName %>_M1340SurgicalWound0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1340SurgicalWound", "01", data.AnswerOrEmptyString("M1340SurgicalWound").Equals("01"), new { @id = Model.TypeName + "_M1340SurgicalWound1", @title = "(OASIS M1340) Surgical Wound, Observable" })%>
                        <label for="<%= Model.TypeName %>_M1340SurgicalWound1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, patient has at least one (observable) surgical wound</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1340SurgicalWound", "02", data.AnswerOrEmptyString("M1340SurgicalWound").Equals("02"), new { @id = Model.TypeName + "_M1340SurgicalWound2", @title = "(OASIS M1340) Surgical Wound, Known but Not Observable" })%>
                        <label for="<%= Model.TypeName %>_M1340SurgicalWound2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Surgical wound known but not observable due to non-removable dressing</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1340')" title="More Information about M1340">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1342">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1342" class="green" onclick="Oasis.ToolTip('M1342')">(M1342)</a>
                    Status of Most Problematic (Observable) Surgical Wound
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1342SurgicalWoundStatus", "", new { @id = Model.TypeName + "_M1342SurgicalWoundStatusHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1342SurgicalWoundStatus", "00", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("00"), new { @id = Model.TypeName + "_M1342SurgicalWoundStatus0", @title = "(OASIS M1342) Status of Most Problematic Surgical Wound, Newly Epithelialized" })%>
                        <label for="<%= Model.TypeName %>_M1342SurgicalWoundStatus0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Newly epithelialized</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1342SurgicalWoundStatus", "01", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("01"), new { @id = Model.TypeName + "_M1342SurgicalWoundStatus1", @title = "(OASIS M1342) Status of Most Problematic Surgical Wound, Fully Granulating" })%>
                        <label for="<%= Model.TypeName %>_M1342SurgicalWoundStatus1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Fully granulating</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1342SurgicalWoundStatus", "02", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("02"), new { @id = Model.TypeName + "_M1342SurgicalWoundStatus2", @title = "(OASIS M1342) Status of Most Problematic Surgical Wound, Early/Partial Granulation" })%>
                        <label for="<%= Model.TypeName %>_M1342SurgicalWoundStatus2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Early/partial granulation</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1342SurgicalWoundStatus", "03", data.AnswerOrEmptyString("M1342SurgicalWoundStatus").Equals("03"), new { @id = Model.TypeName + "_M1342SurgicalWoundStatus3", @title = "(OASIS M1342) Status of Most Problematic Surgical Wound, Not Healing" })%>
                        <label for="<%= Model.TypeName %>_M1342SurgicalWoundStatus3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Not healing</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1342')" title="More Information about M1342">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Open Wounds</legend>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M1350">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1350" class="green" onclick="Oasis.ToolTip('M1350')">(M1350)</a>
                    Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1350SkinLesionOpenWound", "", new { @id = Model.TypeName + "_M1350SkinLesionOpenWoundHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1350SkinLesionOpenWound", "0", data.AnswerOrEmptyString("M1350SkinLesionOpenWound").Equals("0"), new { @id = Model.TypeName + "_M1350SkinLesionOpenWound0", @title = "(OASIS M1350) Skin Lesion or Open Wound, No" })%>
                        <label for="<%= Model.TypeName %>_M1350SkinLesionOpenWound0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1350SkinLesionOpenWound", "1", data.AnswerOrEmptyString("M1350SkinLesionOpenWound").Equals("1"), new { @id = Model.TypeName + "_M1350SkinLesionOpenWound1", @title = "(OASIS M1350) Skin Lesion or Open Wound, Yes" })%>
                        <label for="<%= Model.TypeName %>_M1350SkinLesionOpenWound1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1350')" title="More Information about M1350">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Wound Graph</legend>
        <div class="wide column">
            <div class="row align-center">
                <img alt="Body Outline" src="/Images/body_outline.gif" />
                <table class="form bordergrid old-wound-care">
                    <tbody>
                        <tr>
                            <th></th>
                            <th class="textCenter bold">Wound One</th>
                            <th class="textCenter bold">Wound Two</th>
                            <th class="textCenter bold">Wound Three</th>
                            <th class="textCenter bold">Wound Four</th>
                            <th class="textCenter bold">Wound Five</th>
                        </tr>
                        <tr>
                            <td><label class="strong">Upload File</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
        <%  if (data.AnswerOrEmptyString("GenericUploadFile" + i.ToString()).Equals(Guid.Empty.ToString()) || data.AnswerOrEmptyString("GenericUploadFile" + i.ToString()).Equals(string.Empty)) { %>
                                <input type="file" name="<%= Model.TypeName %>_GenericUploadFile<%= i.ToString() %>" value="Upload" title="(Optional) Photo of Wound" class="uploadWidth" size="1" />
        <%  } else { %>
                                <%= Html.Hidden(Model.TypeName + "_GenericUploadFile" + i.ToString(), data.AnswerOrEmptyString("GenericUploadFile" + i.ToString()).ToGuid()) %>
                                <%= Html.Asset(data.AnswerOrEmptyString("GenericUploadFile" + i.ToString()).ToGuid())%> |
                                <a href="javascript:void(0)" onclick="Oasis.DeleteAsset($(this),'<%= Model.TypeName %>','GenericUploadFile<%= i.ToString() %>','<%= data.AnswerOrEmptyString("GenericUploadFile" + i.ToString()) %>')">Delete</a>
        <%  } %>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Location</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_GenericLocation" + i.ToString(), data.AnswerOrEmptyString("GenericLocation" + i.ToString()), new { @id = Model.TypeName + "_GenericLocation" + i.ToString(), @class = "fill", @maxlength = "30", @title = "(Optional) Wound Location" })%></td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Onset Date</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td><input type="text" class="date-picker fill" name="<%= Model.TypeName %>_GenericOnsetDate<%= i.ToString() %>" value="<%= data.AnswerOrEmptyString("GenericOnsetDate" + i.ToString()) %>" id="<%= Model.TypeName %>_GenericOnsetDate<%= i.ToString() %>" title="(Optional) Wound Onset Date" /></td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Wound Type</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td><%= Html.TextBox(Model.TypeName + "_GenericWoundType" + i.ToString(), data.AnswerOrEmptyString("GenericWoundType" + i.ToString()), new { @id = Model.TypeName + "_GenericWoundType" + i.ToString(), @class = "WoundType fill", @maxlength = "30", @title = "(Optional) Wound Type" })%></td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Pressure Ulcer Stage</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var pressureUlcerStage = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "I", Value = "I" },
                                        new SelectListItem { Text = "II", Value = "II" },
                                        new SelectListItem { Text = "III", Value = "III" },
                                        new SelectListItem { Text = "IV", Value = "IV" },
                                        new SelectListItem { Text = "Unstageable", Value = "Unstageable" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericPressureUlcerStage" + i.ToString(), "0")); %>
                                <%= Html.DropDownList(Model.TypeName + "_GenericPressureUlcerStage" + i.ToString(), pressureUlcerStage, new { @id = Model.TypeName + "_GenericPressureUlcerStage" + i.ToString(), @class = "fill", @title = "(Optional) Pressure Ulcer Stage" })%>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Measurements:(cm)</li>
                                    <li class="align-right woundPad">Length:</li>
                                    <li class="align-right woundPad">Width:</li>
                                    <li class="align-right woundPad">Depth:</li>
                                </ul>
                            </td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&#160;</li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericMeasurementLength" + i.ToString(), data.AnswerOrEmptyString("GenericMeasurementLength" + i.ToString()), new { @id = Model.TypeName + "_GenericMeasurementLength" + i.ToString(), @class = "st woundPad floatnum", @maxlength = "5", @title = "(Optional) Wound Measurements, Length" })%></li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericMeasurementWidth" + i.ToString(), data.AnswerOrEmptyString("GenericMeasurementWidth" + i.ToString()), new { @id = Model.TypeName + "_GenericMeasurementWidth" + i.ToString(), @class = "st woundPad floatnum", @maxlength = "5", @title = "(Optional) Wound Measurements, Width" })%></li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericMeasurementDepth" + i.ToString(), data.AnswerOrEmptyString("GenericMeasurementDepth" + i.ToString()), new { @id = Model.TypeName + "_GenericMeasurementDepth" + i.ToString(), @class = "st woundPad floatnum", @maxlength = "5", @title = "(Optional) Wound Measurements, Depth" })%></li>
                                </ul>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Wound Bed:</li>
                                    <li class="align-right woundPad">Granulation %:</li>
                                    <li class="align-right woundPad">Slough %:</li>
                                    <li class="fr woundPad">Eschar%:</li>
                                </ul>
                            </td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&#160;</li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericWoundBedGranulation" + i.ToString(), data.AnswerOrEmptyString("GenericWoundBedGranulation" + i.ToString()), new { @id = Model.TypeName + "_GenericWoundBedGranulation" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5", @title = "(Optional) Wound Bed, Granulation" })%></li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericWoundBedSlough" + i.ToString(), data.AnswerOrEmptyString("GenericWoundBedSlough" + i.ToString()), new { @id = Model.TypeName + "_GenericWoundBedSlough" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5", @title = "(Optional) Wound Bed, Slough" })%></li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericWoundBedEschar" + i.ToString(), data.AnswerOrEmptyString("GenericWoundBedEschar" + i.ToString()), new { @id = Model.TypeName + "_GenericWoundBedEschar" + i.ToString(), @class = "st woundPad numeric", @maxlength = "5", @title = "(Optional) Wound Bed, Eschar" })%></li>
                                </ul>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Surrounding Tissue</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var surroundingTissue = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Pink", Value = "Pink" },
                                        new SelectListItem { Text = "Dry", Value = "Dry" },
                                        new SelectListItem { Text = "Pale", Value = "Pale" },
                                        new SelectListItem { Text = "Moist", Value = "Moist" },
                                        new SelectListItem { Text = "Excoriated", Value = "Excoriated" },
                                        new SelectListItem { Text = "Calloused", Value = "Calloused" },
                                        new SelectListItem { Text = "Normal", Value = "Normal" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericSurroundingTissue" + i.ToString(), "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericSurroundingTissue" + i.ToString(), surroundingTissue, new { @id = Model.TypeName + "_GenericSurroundingTissue" + i.ToString(), @class = "fill", @title = "(Optional) Wound Surrounding Tissue" })%>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Drainage</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var drainage = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Serous", Value = "Serous" },
                                        new SelectListItem { Text = "Serosanguineous", Value = "Serosanguineous" },
                                        new SelectListItem { Text = "Purulent", Value = "Purulent" },
                                        new SelectListItem { Text = "None", Value = "None" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericDrainage" + i.ToString(), "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericDrainage" + i.ToString(), drainage, new { @id = Model.TypeName + "_GenericDrainage" + i.ToString(), @class = "fill", @title = "(Optional) Wound Drainage" })%>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Drainage Amount</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var drainageAmount = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Minimal", Value = "Minimal" },
                                        new SelectListItem { Text = "Moderate", Value = "Moderate" },
                                        new SelectListItem { Text = "Heavy", Value = "Heavy" },
                                        new SelectListItem { Text = "None", Value = "None" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericDrainageAmount" + i.ToString(), "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericDrainageAmount" + i.ToString(), drainageAmount, new { @id = Model.TypeName + "_GenericDrainageAmount" + i.ToString(), @class = "fill", @title = "(Optional) Wound Drainage Amount" })%>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td><label class="strong">Odor</label></td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <%  var odor = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "Yes", Value = "Yes" },
                                        new SelectListItem { Text = "No", Value = "No" }
                                    }, "Value", "Text", data.AnswerOrDefault("GenericOdor" + i.ToString(), "0"));%>
                                <%= Html.DropDownList(Model.TypeName + "_GenericOdor" + i.ToString(), odor, new { @id = Model.TypeName + "_GenericOdor" + i.ToString(), @class = "fill", @title = "(Optional) Wound Odor" })%>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Tunneling:</li>
                                    <li class="align-right woundPad">Length:</li>
                                    <li class="align-right woundPad">Time:</li>
                                </ul>
                            </td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&#160;</li>
                                    <li>
                                        <%= Html.TextBox(Model.TypeName + "_GenericTunnelingLength" + i.ToString(), data.AnswerOrEmptyString("GenericTunnelingLength" + i.ToString()), new { @id = Model.TypeName + "_GenericTunnelingLength" + i.ToString(), @class = "vitals woundPad floatnum", @maxlength = "5", @title = "(Optional) Tunneling Length" })%>
                                        <label>cm</label>
                                    </li>
                                    <li>
                                        <%  var tunnelingTime = new SelectList(new[] {
                                                new SelectListItem { Text = "", Value = "" },
                                                new SelectListItem { Text = "1", Value = "1" },
                                                new SelectListItem { Text = "2", Value = "2" },
                                                new SelectListItem { Text = "3", Value = "3" },
                                                new SelectListItem { Text = "4", Value = "4" },
                                                new SelectListItem { Text = "5", Value = "5" },
                                                new SelectListItem { Text = "6", Value = "6" },
                                                new SelectListItem { Text = "7", Value = "7" },
                                                new SelectListItem { Text = "8", Value = "8" },
                                                new SelectListItem { Text = "9", Value = "9" },
                                                new SelectListItem { Text = "10", Value = "10" },
                                                new SelectListItem { Text = "11", Value = "11" },
                                                new SelectListItem { Text = "12", Value = "12" }
                                            }, "Value", "Text", data.AnswerOrEmptyString("GenericTunnelingTime" + i.ToString()));%>
                                        <%= Html.DropDownList(Model.TypeName + "_GenericTunnelingTime" + i.ToString(), tunnelingTime, new { @id = Model.TypeName + "_GenericTunnelingTime" + i.ToString(), @class = "vitals", @title = "(Optional) Tunneling Time" })%>
                                        <label>o&#8217;clock</label>
                                    </li>
                                </ul>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Undermining:</li>
                                    <li class="align-right woundPad">Length:</li>
                                    <li class="align-right woundPad">Time:</li>
                                </ul>
                            </td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&#160;</li>
                                    <li>
                                        <%= Html.TextBox(Model.TypeName + "_GenericUnderminingLength" + i.ToString(), data.AnswerOrEmptyString("GenericUnderminingLength" + i.ToString()), new { @id = Model.TypeName + "_GenericUnderminingLength" + i.ToString(), @class = "vitals woundPad floatnum", @maxlength = "5", @title = "(Optional) Undermining Length" })%>
                                        <label>cm</label>
                                    </li>
                                    <li>
                                        <%  var underminingTimeFrom = new SelectList(new[] {
                                                new SelectListItem { Text = "", Value = "" },
                                                new SelectListItem { Text = "1", Value = "1" },
                                                new SelectListItem { Text = "2", Value = "2" },
                                                new SelectListItem { Text = "3", Value = "3" },
                                                new SelectListItem { Text = "4", Value = "4" },
                                                new SelectListItem { Text = "5", Value = "5" },
                                                new SelectListItem { Text = "6", Value = "6" },
                                                new SelectListItem { Text = "7", Value = "7" },
                                                new SelectListItem { Text = "8", Value = "8" },
                                                new SelectListItem { Text = "9", Value = "9" },
                                                new SelectListItem { Text = "10", Value = "10" },
                                                new SelectListItem { Text = "11", Value = "11" },
                                                new SelectListItem { Text = "12", Value = "12" }
                                            }, "Value", "Text", data.AnswerOrEmptyString("GenericUnderminingTimeFrom" + i.ToString()));%>
                                        <%= Html.DropDownList(Model.TypeName + "_GenericUnderminingTimeFrom" + i.ToString(), underminingTimeFrom, new { @id = Model.TypeName + "_GenericUnderminingTimeFrom" + i.ToString(), @class = "vitals", @title = "(Optional) Undermining Time Start" })%>
                                        <label>to</label>
                                        <%  var underminingTimeTo = new SelectList(new[] {
                                                new SelectListItem { Text = "", Value = "" },
                                                new SelectListItem { Text = "1", Value = "1" },
                                                new SelectListItem { Text = "2", Value = "2" },
                                                new SelectListItem { Text = "3", Value = "3" },
                                                new SelectListItem { Text = "4", Value = "4" },
                                                new SelectListItem { Text = "5", Value = "5" },
                                                new SelectListItem { Text = "6", Value = "6" },
                                                new SelectListItem { Text = "7", Value = "7" },
                                                new SelectListItem { Text = "8", Value = "8" },
                                                new SelectListItem { Text = "9", Value = "9" },
                                                new SelectListItem { Text = "10", Value = "10" },
                                                new SelectListItem { Text = "11", Value = "11" },
                                                new SelectListItem { Text = "12", Value = "12" }
                                            }, "Value", "Text", data.AnswerOrEmptyString("GenericUnderminingTimeTo" + i.ToString()));%>
                                        <%= Html.DropDownList(Model.TypeName + "_GenericUnderminingTimeTo" + i.ToString(), underminingTimeTo, new { @id = Model.TypeName + "_GenericUnderminingTimeTo" + i.ToString(), @class = "vitals", @title = "(Optional) Undermining Time End" })%>
                                        <label>o&#8217;clock</label>
                                    </li>
                                </ul>
                            </td>
    <%  } %>
                        </tr>
                        <tr>
                            <td>
                                <ul class="fixedheight">
                                    <li class="strong">Device:</li>
                                    <li class="align-right woundPad">Type:</li>
                                    <li class="align-right woundPad">Setting:</li>
                                </ul>
                            </td>
    <%  for (int i = 1; i < 6; i++) { %>
                            <td>
                                <ul class="fixedheight">
                                    <li>&#160;</li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericDeviceType" + i.ToString(), data.AnswerOrEmptyString("GenericDeviceType" + i.ToString()), new { @id = Model.TypeName + "_GenericDeviceType" + i.ToString(), @class = "fill woundPad DeviceType", @maxlength = "30", @title = "(Optional) Device Type " })%></li>
                                    <li><%= Html.TextBox(Model.TypeName + "_GenericDeviceSetting" + i.ToString(), data.AnswerOrEmptyString("GenericDeviceSetting" + i.ToString()), new { @id = Model.TypeName + "_GenericDeviceSetting" + i.ToString(), @class = "fill woundPad", @maxlength = "30", @title = "(Optional) Device Setting " })%></li>
                                </ul>
                            </td>
    <%  } %>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <label class="strong">Treatment Performed</label>
                <%= Html.TextArea(Model.TypeName + "_GenericTreatmentPerformed", data.AnswerOrEmptyString("GenericTreatmentPerformed"), 8, 20, new { @class = "fill", @id = Model.TypeName + "_GenericTreatmentPerformed", @title = "(Optional) Treatment Performed" })%>
            </div>
            <div class="row">
                <label class="strong">Narrative</label>
                <%= Html.TextArea(Model.TypeName + "_GenericNarrative", data.AnswerOrEmptyString("GenericNarrative"), 8, 20, new { @class = "fill", @id = Model.TypeName + "_GenericNarrative", @title = "(Optional) Narrative" })%>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Integumentary.ascx", Model); %>
    <%  } %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.oasis-tip").remove();
<%  } %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    Oasis.BradenScale("<%= Model.TypeName %>");
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericSkinColor5"),
        $("#<%= Model.TypeName %>_GenericSkinColor5More"));
    U.ShowIfChecked(
        $("#<%= Model.TypeName %>_GenericSkinCondition6"),
        $("#<%= Model.TypeName %>_GenericSkinCondition6More"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericNails", "Problem",
        $("#<%= Model.TypeName %>_GenericNailsProblemMore"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericPressureRelievingDevice", "1",
        $("#<%= Model.TypeName %>_GenericPressureRelievingDeviceMore"));
    U.ShowIfSelectEquals(
        $("#<%= Model.TypeName %>_GenericPressureRelievingDeviceType"), "4",
        $("#<%= Model.TypeName %>_GenericPressureRelievingDeviceTypeOther"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericIVAccess", "1",
        $(".<%= Model.TypeName %>_IVAccess"));
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_GenericFlush", "1",
        $("#<%= Model.TypeName %>_GenericFlushMore"));
<%  } %>
<%  if (Model.AssessmentTypeNum != "11" && Model.AssessmentTypeNum != "14") { %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    U.HideIfRadioEquals(
        "<%= Model.TypeName %>_M1300PressureUlcerAssessment", "00",
        $("#<%= Model.TypeName %>_M1302"));
    <%  } %>
    U.HideIfRadioEquals(
        "<%= Model.TypeName %>_M1306UnhealedPressureUlcers", "0",
        $(".<%= Model.TypeName %>_M1306_00"));
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 8) { %>
    U.ShowIfRadioEquals(
        "<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcer", "02",
        $("#<%= Model.TypeName %>_M1307NonEpithelializedStageTwoUlcer02More"));
    <%  } %>
    U.HideIfRadioEquals(
        "<%= Model.TypeName %>_M1330StasisUlcer", "00|03",
        $("#<%= Model.TypeName %>_M1332"));
    U.HideIfRadioEquals(
        "<%= Model.TypeName %>_M1330StasisUlcer", "00|03",
        $("#<%= Model.TypeName %>_M1334"));
    U.HideIfRadioEquals(
        "<%= Model.TypeName %>_M1340SurgicalWound", "00|02",
       $("#<%= Model.TypeName %>_M1342"));
    $("#<%= Model.TypeName %>_M1308NumberNonEpithelializedStageThreeUlcerCurrent,#<%= Model.TypeName %>_M1308NumberNonEpithelializedStageFourUlcerCurrent,#<%= Model.TypeName %>_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").keyup(function() {
	    var stage3 = $("#<%= Model.TypeName %>_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(),
	        stage4 = $("#<%= Model.TypeName %>_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(),
	        cvrg = $("#<%= Model.TypeName %>_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val();
	    if (parseInt(stage3) > 0 || parseInt(stage4) > 0 || parseInt(cvrg) > 0) $("#<%= Model.TypeName %>_M1310,#<%= Model.TypeName %>_M1312,#<%= Model.TypeName %>_M1314").show();
	    else $("#<%= Model.TypeName %>_M1310,#<%= Model.TypeName %>_M1312,#<%= Model.TypeName %>_M1314").each(function() { $("input", this).val("") }).hide();
    });
    $("input[name=<%= Model.TypeName %>_M1306UnhealedPressureUlcers]").change(function() {
        $("#<%= Model.TypeName %>_M1308NumberNonEpithelializedStageThreeUlcerCurrent").keyup()
    });
    $("input[name=<%= Model.TypeName %>_M1306UnhealedPressureUlcers]").change();
<%  } %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    $(".WoundType").Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"] });
    $(".DeviceType").Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
    $(".IVSiteDressingUnit").Autocomplete({ source: ["Normal Saline", "Heparin"] });
<%  } %>
</script>