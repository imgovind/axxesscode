<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "Pain_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Pain")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Pain Scale</legend>
        <div class="wide column align-center">
            <div class="row">
                <img src="/Images/painscale.png" alt="Pain Scale Image" /><br />
                <em>From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532" target="_blank">Wong&#8217;s essentials of pediatric nursing</a>, ed. 8, St. Louis, 2009, Mosby. Used with permission. Copyright Mosby.</em>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPainOnSetDate" class="float-left">Onset Date</label>
                <div class="fr">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_GenericPainOnSetDate" value="<%= data.AnswerOrEmptyString("GenericPainOnSetDate") %>" id="<%= Model.TypeName %>_GenericPainOnSetDate" title="(Optional) Onset Date" />
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericIntensityOfPain" class="float-left">Pain Intensity</label>
                <div class="fr">
                    <%  var painIntensity = new SelectList(new[] {
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "5 = Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericIntensityOfPain", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericIntensityOfPain", painIntensity, new { @id = Model.TypeName + "_GenericIntensityOfPain", @title = "(Optional) Pain Intensity" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericLocationOfPain" class="float-left">Primary Site</label>
                <div class="fr">
                    <%= Html.TextBox(Model.TypeName + "_GenericLocationOfPain", data.AnswerOrEmptyString("GenericLocationOfPain"), new { @id = Model.TypeName + "_GenericLocationOfPain", @maxlength = "80", @title = "(Optional) Pain Location" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDurationOfPain" class="float-left">Duration</label>
                <div class="fr">
                    <%  var duration = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Continuous", Value = "1" },
                            new SelectListItem { Text = "Intermittent", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericDurationOfPain", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericDurationOfPain", duration, new { @id = Model.TypeName + "_GenericDurationOfPain", @title = "(Optional) Pain Duration" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericQualityOfPain" class="float-left">Description</label>
                <div class="fr">
                    <%  var painDescription = new SelectList(new[] { 
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Aching", Value = "1" },
                            new SelectListItem { Text = "Throbbing", Value = "2" },
                            new SelectListItem { Text = "Burning", Value = "3" },
                            new SelectListItem { Text = "Sharp", Value = "4" },
                            new SelectListItem { Text = "Tender", Value = "5" },
                            new SelectListItem { Text = "Other", Value = "6" }
                        } , "Value", "Text", data.AnswerOrDefault("GenericQualityOfPain", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericQualityOfPain", painDescription, new { @id = Model.TypeName + "_GenericQualityOfPain", @title = "(Optional) Pain Description" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedicationEffectiveness" class="strong">Current Pain Management Effectiveness</label>
                <div class="fr">
                    <%  var currentPainManagementEffectiveness = new SelectList(new[] {
                            new SelectListItem { Text = "N/A", Value = "0" },
                            new SelectListItem { Text = "Effective", Value = "1" },
                            new SelectListItem { Text = "Not Effective", Value = "2" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericMedicationEffectiveness", "0"));%>
                    <%= Html.DropDownList(Model.TypeName + "_GenericMedicationEffectiveness", currentPainManagementEffectiveness, new { @id = Model.TypeName + "_GenericMedicationEffectiveness", @title = "(Optional) Pain Management Effectiveness" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWhatMakesPainBetter" class="strong">What makes pain better</label>
                <%= Html.TextArea(Model.TypeName + "_GenericWhatMakesPainBetter", data.AnswerOrEmptyString("GenericWhatMakesPainBetter"), 2, 70, new { @id = Model.TypeName + "_GenericWhatMakesPainBetter", @title = "(Optional) Pain Improvement" })%>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWhatMakesPainWorse" class="strong">What makes pain worse</label>
                <%= Html.TextArea(Model.TypeName + "_GenericWhatMakesPainWorse", data.AnswerOrEmptyString("GenericWhatMakesPainWorse"), 2, 70, new { @id = Model.TypeName + "_GenericWhatMakesPainWorse", @title = "(Optional) Pain Exacerbation" })%>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPatientPainGoal" class="strong">Patient&#8217;s pain goal</label>
                <%= Html.TextArea(Model.TypeName + "_GenericPatientPainGoal", data.AnswerOrEmptyString("GenericPatientPainGoal"), 2, 70, new { @id = Model.TypeName + "_GenericPatientPainGoal", @title = "(Optional) Pain Goal" })%>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPatientPainComment" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_GenericPatientPainComment", data.AnswerOrEmptyString("GenericPatientPainComment"), 2, 70, new { @id = Model.TypeName + "_GenericPatientPainComment", @title = "(Optional) Pain Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Pain Assessment</legend>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M1240">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1240" class="green" onclick="Oasis.ToolTip('M1240')">(M1240)</a>
                    Has this patient had a formal Pain Assessment using a standardized pain assessment tool (appropriate to the patient&#8217;s ability to communicate the severity of pain)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1240FormalPainAssessment", "", new { @id = Model.TypeName + "_M1240FormalPainAssessmentHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1240FormalPainAssessment", "00", data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("00"), new { @id = Model.TypeName + "_M1240FormalPainAssessment00", @title = "(OASIS M1240) Formal Pain Assessment, No Formal Assessment" }) %>
                        <label for="<%= Model.TypeName %>_M1240FormalPainAssessment00">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No standardized assessment conducted</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1240FormalPainAssessment", "01", data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("01"), new { @id = Model.TypeName + "_M1240FormalPainAssessment01", @title = "(OASIS M1240) Formal Pain Assessment, Does Not Indicate Severe Pain" })%>
                        <label for="<%= Model.TypeName %>_M1240FormalPainAssessment01">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, and it does not indicate severe pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1240FormalPainAssessment", "02", data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("02"), new { @id = Model.TypeName + "_M1240FormalPainAssessment02", @title = "(OASIS M1240) Formal Pain Assessment, Indicating Severe Pain" })%>
                        <label for="<%= Model.TypeName %>_M1240FormalPainAssessment02">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Yes, and it indicates severe pain</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1240')" title="More Information about M1240">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum != "11" && Model.AssessmentTypeNum != "14") { %>
    <fieldset class="oasis">
        <legend>Pain Frequency</legend>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M1242">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1242" class="green" onclick="Oasis.ToolTip('M1242')">(M1242)</a>
                    Frequency of Pain Interfering with patient&#8217;s activity or movement
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1242PainInterferingFrequency", "", new { @id = Model.TypeName + "_M1242PainInterferingFrequencyHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1242PainInterferingFrequency", "00", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("00"), new { @id = Model.TypeName + "_M1242PainInterferingFrequency00", @title = "(OASIS M1242) Frequency of Pain Interfering with Activity, No Pain" })%>
                        <label for="<%= Model.TypeName %>_M1242PainInterferingFrequency00">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Patient has no pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1242PainInterferingFrequency", "01", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("01"), new { @id = Model.TypeName + "_M1242PainInterferingFrequency01", @title = "(OASIS M1242) Frequency of Pain Interfering with Activity, Pain does not Interfere with Activity" })%>
                        <label for="<%= Model.TypeName %>_M1242PainInterferingFrequency01">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient has pain that does not interfere with activity or movement</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1242PainInterferingFrequency", "02", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("02"), new { @id = Model.TypeName + "_M1242PainInterferingFrequency02", @title = "(OASIS M1242) Frequency of Pain Interfering with Activity, Less Often than Daily" })%>
                        <label for="<%= Model.TypeName %>_M1242PainInterferingFrequency02">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Less often than daily</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1242PainInterferingFrequency", "03", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("03"), new { @id = Model.TypeName + "_M1242PainInterferingFrequency03", @title = "(OASIS M1242) Frequency of Pain Interfering with Activity, Daily" })%>
                        <label for="<%= Model.TypeName %>_M1242PainInterferingFrequency03">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Daily, but not constantly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1242PainInterferingFrequency", "04", data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("04"), new { @id = Model.TypeName + "_M1242PainInterferingFrequency04", @title = "(OASIS M1242) Frequency of Pain, All of the Time" })%>
                        <label for="<%= Model.TypeName %>_M1242PainInterferingFrequency04">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">All of the time</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M1242')" title="More Information about M1242">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Pain.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="fr">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>