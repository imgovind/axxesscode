<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<%  var DiabeticCareDiabeticManagement = data.AnswerArray("GenericDiabeticCareDiabeticManagement"); %>
<%  var DiabeticCareInsulinAdministeredby = data.AnswerArray("GenericDiabeticCareInsulinAdministeredby"); %>
<%  var PatientEdocrineProblem = data.AnswerArray("GenericPatientEdocrineProblem"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(2,
            printview.checkbox("WNL (Within Normal Limits)",<%= data.AnswerOrEmptyString("GenericEndocrineWithinNormalLimits").Equals("1").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.span("Is patient diabetic?",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericPatientDiabetic").Equals("1").ToString().ToLower() %>) +
                    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericPatientDiabetic").Equals("0").ToString().ToLower() %>)))) +
        printview.col(5,
            printview.span("Diabetic Management:",true) +
            printview.checkbox("Diet",<%= DiabeticCareDiabeticManagement.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Oral Hypoglycemic",<%= DiabeticCareDiabeticManagement.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Exercise",<%= DiabeticCareDiabeticManagement.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Insulin",<%= DiabeticCareDiabeticManagement.Contains("4").ToString().ToLower() %>)) +
        printview.col(2,
            printview.col(2,
                printview.span("Insulin dependent?",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericInsulinDependent").Equals("1").ToString().ToLower() %>) +
                    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericInsulinDependent").Equals("0").ToString().ToLower() %>))) +
            printview.col(2,
                printview.span("How long?") +
                printview.span("<%= data.AnswerOrEmptyString("GenericInsulinDependencyDuration").Clean() %>",false,1)) +
            printview.span("Insulin Administered by:",true) +
            printview.col(2,
                printview.col(2,
                    printview.checkbox("N/A",<%= DiabeticCareInsulinAdministeredby.Contains("1").ToString().ToLower() %>) +
                    printview.checkbox("Patient",<%= DiabeticCareInsulinAdministeredby.Contains("2").ToString().ToLower() %>)) +
                printview.col(2,
                    printview.checkbox("Caregiver",<%= DiabeticCareInsulinAdministeredby.Contains("3").ToString().ToLower() %>) +
                    printview.checkbox("SN",<%= DiabeticCareInsulinAdministeredby.Contains("4").ToString().ToLower() %>))) +
            printview.span("Is patient independent with glucometer use?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericGlucometerUseIndependent").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericGlucometerUseIndependent").Equals("0").ToString().ToLower() %>)) +
            printview.span("Is caregiver independent with glucometer use?",true) +
            printview.col(3,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCareGiverGlucometerUse").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCareGiverGlucometerUse").Equals("0").ToString().ToLower() %>) +
                printview.checkbox("N/A, no caregiver",<%= data.AnswerOrEmptyString("GenericCareGiverGlucometerUse").Equals("2").ToString().ToLower() %>))) +
        printview.span("Does patient have any of the following?",true) +
        printview.col(6,
            printview.checkbox("Polyuria",<%= PatientEdocrineProblem.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Polydipsia",<%= PatientEdocrineProblem.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Polyphagia",<%= PatientEdocrineProblem.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Neuropathy",<%= PatientEdocrineProblem.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Radiculopathy",<%= PatientEdocrineProblem.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Thyroid problems",<%= PatientEdocrineProblem.Contains("6").ToString().ToLower() %>)) +
        printview.col(2,
            printview.col(2,
                printview.span("<strong>Blood Sugar:</strong> <%= data.AnswerOrEmptyString("GenericBloodSugarLevelText").Clean() %> mg/dl") +
                printview.col(2,
                    printview.checkbox("Random",<%= data.AnswerOrEmptyString("GenericBloodSugarLevel").Equals("Random").ToString().ToLower() %>) +
                    printview.checkbox("Fasting",<%= data.AnswerOrEmptyString("GenericBloodSugarLevel").Equals("Fasting").ToString().ToLower() %>))) +
            printview.col(2,
                printview.span("Blood sugar checked by:",true) +
                printview.span("<%= (data.AnswerOrEmptyString("GenericBloodSugarCheckedBy").Equals("1") ? "Patient" : "") + (data.AnswerOrEmptyString("GenericBloodSugarCheckedBy").Equals("2") ? "SN" : "") + (data.AnswerOrEmptyString("GenericBloodSugarCheckedBy").Equals("3") ? "Caregiver" : "").Clean() %>",false,1)) +
            printview.col(2,
                printview.span("Site:",true) +
                printview.col(2,
                    printview.checkbox("Left",<%= data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Left").ToString().ToLower() %>) +
                    printview.checkbox("Right",<%= data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Right").ToString().ToLower() %>))) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBloodSugarSiteText").Clean() %>")) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericEndocrineComments").Clean() %>",false,2),
        "Endocrine");
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Endocrine.ascx", Model); %>
<%  } %>
</script>