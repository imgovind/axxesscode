<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var FunctionLimitations = data.AnswerArray("485FunctionLimitations"); %>
<%  var AdvancedDirectivesIntent = data.AnswerArray("485AdvancedDirectivesIntent"); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(5,
            printview.checkbox("Guarded",<%= data.AnswerOrEmptyString("485Prognosis").Equals("Guarded").ToString().ToLower() %>) +
            printview.checkbox("Poor",<%= data.AnswerOrEmptyString("485Prognosis").Equals("Poor").ToString().ToLower() %>) +
            printview.checkbox("Fair",<%= data.AnswerOrEmptyString("485Prognosis").Equals("Fair").ToString().ToLower() %>) +
            printview.checkbox("Good",<%= data.AnswerOrEmptyString("485Prognosis").Equals("Good").ToString().ToLower() %>) +
            printview.checkbox("Excellent",<%= data.AnswerOrEmptyString("485Prognosis").Equals("Excellent").ToString().ToLower() %>)),"Prognosis");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Amputation",<%= FunctionLimitations.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Paralysis",<%= FunctionLimitations.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Legally Blind",<%= FunctionLimitations.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Bowel/Bladder Incontinence",<%= FunctionLimitations.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Endurance",<%= FunctionLimitations.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Dyspnea with Minimal Exertion",<%= FunctionLimitations.Contains("A").ToString().ToLower() %>) +
            printview.checkbox("Contracture",<%= FunctionLimitations.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Ambulation",<%= FunctionLimitations.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Hearing",<%= FunctionLimitations.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Speech",<%= FunctionLimitations.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Other (Specify)",<%= FunctionLimitations.Contains("B").ToString().ToLower() %>)) +
        printview.span("<%= FunctionLimitations.Contains("B") ? data.AnswerOrEmptyString("485FunctionLimitationsOther") : string.Empty %>",false,2),
        "Functional Limitations");
    printview.addsection(
        printview.col(2,
            printview.span("Are there any advanced directives?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("485AdvancedDirectives").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("485AdvancedDirectives").Equals("No").ToString().ToLower() %>))) +
    <%  if (!data.AnswerOrEmptyString("485AdvancedDirectives").Equals("No")) { %>
        printview.col(5,
            printview.span("Intent:",true) +
            printview.checkbox("DNR",<%= AdvancedDirectivesIntent.Contains("DNR").ToString().ToLower() %>) +
            printview.checkbox("Living Will",<%= AdvancedDirectivesIntent.Contains("Living Will").ToString().ToLower() %>) +
            printview.checkbox("Med. Power of Attorney",<%= AdvancedDirectivesIntent.Contains("Medical Power of Attorney").ToString().ToLower() %>) +
            printview.checkbox("Other <%= AdvancedDirectivesIntent.Contains("Other") ? data.AnswerOrEmptyString("485AdvancedDirectivesIntentOther") : string.Empty %>",<%= AdvancedDirectivesIntent.Contains("Other").ToString().ToLower() %>)) +
        printview.col(2,
            printview.span("Copy on file at agency?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("485AdvancedDirectivesCopyOnFile").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("485AdvancedDirectivesCopyOnFile").Equals("No").ToString().ToLower() %>)) +
            printview.span("Patient was provided written and verbal information?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("485AdvancedDirectivesWrittenAndVerbal").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("485AdvancedDirectivesWrittenAndVerbal").Equals("No").ToString().ToLower() %>)) +
            printview.span("Is the Patient DNR (Do Not Resuscitate)?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericPatientDNR").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericPatientDNR").Equals("No").ToString().ToLower() %>))) +
        printview.span("Comments",true) +
        printview.span("<%= data.AnswerOrEmptyString("485AdvancedDirectivesComment").Clean() %>",false,2) +
    <%  } %>
        "","Advanced Directives");
<%  } %>
</script>