<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<%  var Supplies = data.AnswerArray("485Supplies"); %>
<%  var Dme = data.AnswerArray("485DME"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(6,
            printview.checkbox("ABDs",<%= Supplies.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Ace Wrap",<%= Supplies.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Alcohol Pads",<%= Supplies.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Chux/Underpads",<%= Supplies.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Diabetic Supplies",<%= Supplies.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Drainage Bag",<%= Supplies.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Dressing Supplies",<%= Supplies.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Duoderm",<%= Supplies.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Exam Gloves",<%= Supplies.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Foley Catheter",<%= Supplies.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Gauze Pads",<%= Supplies.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Insertion Kit",<%= Supplies.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Irrigation Set",<%= Supplies.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Irrigation Solution",<%= Supplies.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Kerlix Rolls",<%= Supplies.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Leg Bag",<%= Supplies.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Needles",<%= Supplies.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("NG Tube",<%= Supplies.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Probe Covers",<%= Supplies.Contains("19").ToString().ToLower() %>) +
            printview.checkbox("Sharps Container",<%= Supplies.Contains("20").ToString().ToLower() %>) +
            printview.checkbox("Sterile Gloves",<%= Supplies.Contains("21").ToString().ToLower() %>) +
            printview.checkbox("Syringe",<%= Supplies.Contains("22").ToString().ToLower() %>) +
            printview.checkbox("Tape",<%= Supplies.Contains("23").ToString().ToLower() %>)) +
        printview.span("Other:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485SuppliesComment").Clean() %>",false,2),
        "Supplies");
    printview.addsection(
        printview.col(5,
            printview.checkbox("Bedside Commode",<%= Dme.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Cane",<%= Dme.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Elevated Toilet Seat",<%= Dme.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Grab Bars",<%= Dme.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Hospital Bed",<%= Dme.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Nebulizer",<%= Dme.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Oxygen",<%= Dme.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Tub/Shower Bench",<%= Dme.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Walker",<%= Dme.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Wheelchair",<%= Dme.Contains("10").ToString().ToLower() %>)) +
        printview.span("Other:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485DMEComments").Clean() %>",false,2),
        "DME");
    printview.addsection(
        printview.col(4,
            printview.span("Name:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEProviderName").Clean() %>",false,1) +
            printview.span("Phone Number:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEProviderPhone").Clean() %>",false,1)) +
        printview.span("DME/Supplies Provided:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericDMESuppliesProvided").Clean() %>",false,2),
        "DME Provider");
<%  } %>
</script>