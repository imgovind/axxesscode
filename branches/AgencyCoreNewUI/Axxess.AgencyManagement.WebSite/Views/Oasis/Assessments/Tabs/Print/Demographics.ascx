<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("<%= isOasis ? "(M0020) " : string.Empty %>Patient ID Number:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0020PatientIdNumber").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0040) " : string.Empty %>Patient Name:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0040LastName").Clean() %> <%= data.AnswerOrEmptyString("M0040Suffix").Clean() %>, <%= data.AnswerOrEmptyString("M0040FirstName") %> <%= data.AnswerOrEmptyString("M0040MI") %>",false,1) +
            printview.span("<%= isOasis ? "(M0050) " : string.Empty %>State:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0050PatientState").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0060) " : string.Empty %>Zip:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0060PatientZipCode").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0069) " : string.Empty %>Gender:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0069Gender").Equals("1") ? "Male" : string.Empty %><%= data.AnswerOrEmptyString("M0069Gender").Equals("2") ? "Female" : string.Empty %>",false,1) +
            printview.span("<%= isOasis ? "(M0064) " : string.Empty %>Social Security Number:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0064PatientSSNUnknown").Equals("1") ? "UK &#8211; Unknown" : data.AnswerOrEmptyString("M0064PatientSSN").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0063) " : string.Empty %>Medicare Number:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0063PatientMedicareNumberUnknown").Equals("1") ? "NA &#8211; No Medicare" : data.AnswerOrEmptyString("M0063PatientMedicareNumber").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0065) " : string.Empty %>Medicaid Number:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0065PatientMedicaidNumberUnknown").Equals("1") ? "NA &#8211; No Medicaid" : data.AnswerOrEmptyString("M0065PatientMedicaidNumber").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0066) " : string.Empty %>Birth Date:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0066PatientDoB").Clean() %>",false,1) +
<%  if (Model.AssessmentTypeNum.ToInteger() != 8) { %>
            printview.span("<%= isOasis ? "(M0140) " : string.Empty %>Race/Ethnicity (Below):",true)) +
        printview.col(3,
            printview.checkbox("1 &#8211; American Indian or Alaska Native",<%= data.AnswerOrEmptyString("M0140RaceAMorAN").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Asian",<%= data.AnswerOrEmptyString("M0140RaceAsia").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Black or Affrican American",<%= data.AnswerOrEmptyString("M0140RaceBalck").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Hispanic or Latino",<%= data.AnswerOrEmptyString("M0140RaceHispanicOrLatino").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; Native Hawaiian or Pacific Islander",<%= data.AnswerOrEmptyString("M0140RaceNHOrPI").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &#8211; White",<%= data.AnswerOrEmptyString("M0140RaceWhite").Equals("1").ToString().ToLower() %>)),
<%  } else { %>
        ""),
<%  } %>
        "Patient Information");
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("<%= isOasis ? "(M0030) " : string.Empty %>Start of Care Date:",true) +
                printview.span("<%= data.AnswerOrEmptyString("M0030SocDate").Clean() %>",false,1)) +
            printview.col(2,
                printview.span("Episode Start Date:",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericEpisodeStartDate").Clean() %>",false,1)) +
            printview.col(2,
                printview.span("<%= isOasis ? "(M0032) " : string.Empty %>Resumption of Care:",true) +
                printview.span("<%= data.AnswerOrEmptyString("M0032ROCDateNotApplicable").Equals("1") ? "NA &#8211; Not Applicable" : data.AnswerOrEmptyString("M0032ROCDate").Clean() %>",false,1)) +
            printview.col(2,
                printview.span("<%= isOasis ? "(M0090) " : string.Empty %>Date Completed:",true) +
                printview.span("<%= data.AnswerOrEmptyString("M0090AssessmentCompleted").Clean() %>",false,1)) +
            printview.span("<strong><%= isOasis ? "(M0080) " : string.Empty %>Discipline of Person Completing Assessment:</strong> <%= data.AnswerOrEmptyString("M0080DisciplinePerson").Equals("01") ? "RN" : string.Empty %><%= data.AnswerOrEmptyString("M0080DisciplinePerson").Equals("02") ? "PT" : string.Empty %><%= data.AnswerOrEmptyString("M0080DisciplinePerson").Equals("03") ? "SLP/ST" : string.Empty %><%= data.AnswerOrEmptyString("M0080DisciplinePerson").Equals("04") ? "OT" : string.Empty %>") +
            printview.span("<strong><%= isOasis ? "(M0010) " : string.Empty %>CMS Certification Number:</strong> <%= data.AnswerOrEmptyString("M0010CertificationNumber").Clean() %>") +
            printview.span("<strong><%= isOasis ? "(M0018) " : string.Empty %>Physician NPI Number:</strong> <%= data.AnswerOrEmptyString("M0018NationalProviderIdUnknown").Equals("1") ? "UK &#8211; Unknown" : data.AnswerOrEmptyString("M0018NationalProviderId").Clean() %>") +
<%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
         <%  if (Model.AssessmentTypeNum.ToInteger() <= 3) { %>
            printview.span("<strong><%= isOasis ? "(M0102) " : string.Empty %>Date of Physician-Ordered Start of Care:</strong> <%= data.AnswerOrEmptyString("M0102PhysicianOrderedDateNotApplicable").Equals("1") ? "NA &#8211; No Specific SOC date ordered by physician" : data.AnswerOrEmptyString("M0102PhysicianOrderedDate").Clean() %>") +
   
            printview.col(2,
                printview.span("<%= isOasis ? "(M0104) " : string.Empty %>Date of Referral:",true) +
                printview.span("<%= data.AnswerOrEmptyString("M0104ReferralDate").Clean() %>",false,1)) +
                <%  } %>
                 <%  if (Model.AssessmentTypeNum.ToInteger() <= 4) { %>
            printview.col(2,
                printview.span("<%= isOasis ? "(M0110) " : string.Empty %>Episode Timing:",true) +
                printview.span("<%= data.AnswerOrEmptyString("M0110EpisodeTiming").Equals("01") ? "Early" : string.Empty %><%= data.AnswerOrEmptyString("M0110EpisodeTiming").Equals("02") ? "Later" : string.Empty %><%= data.AnswerOrEmptyString("M0110EpisodeTiming").Equals("UK") ? "Unknown" : string.Empty %><%= data.AnswerOrEmptyString("M0110EpisodeTiming").Equals("NA") ? "Not Applicable/No Medicare case mix group" : string.Empty %>",false,1)) +
    <%  } %>
<%  } %>"") +
        printview.col(2,
            printview.span("<%= isOasis ? "(M0100) " : string.Empty %>This assessment is currently being completed for the following reason",true) +
            printview.span("<%= Model.AssessmentTypeNum.ToInteger() % 10 == 1 ? "Start of care&#8212;further visits planned" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() == 3 ? "Resumption of care (after inpatient stay)" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() % 10 == 4 ? "Recertification (follow-up) reassessment" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() == 5 ? "Other follow-up" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() == 6 ? "Transferred to an inpatient facility&#8212;patient not discharged from agency" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() == 7 ? "Transferred to an inpatient facility&#8212;patient discharged from agency" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() == 8 ? "Death at home" : string.Empty %><%= Model.AssessmentTypeNum.ToInteger() % 10 == 9 ? "Discharge from agency" : string.Empty %>")) +
<%  if (Model.AssessmentTypeNum != "08") { %>
        printview.span("<%= isOasis ? "(M0150) " : string.Empty %>Current payment sources for home care",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; None; no charge for current services",<%= data.AnswerOrEmptyString("M0150PaymentSourceNone").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Medicare (Traditional Fee-for-Service)",<%= data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Medicare (HMO/Managed Care/Adv. Plan)",<%= data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Medicaid (Traditional Fee-for-Service)",<%= data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Medicaid (HMO/Managed Care)",<%= data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; Workers&#8217; Compensation",<%= data.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &#8211; Title Programs (e.g., Title III, V, or XX)",<%= data.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("7 &#8211; Other government (e.g., TriCare, VA, etc.)",<%= data.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("8 &#8211; Private Insurance",<%= data.AnswerOrEmptyString("M0150PaymentSourcePRVINS").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("9 &#8211; Private HMO/Managed Care",<%= data.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("10 &#8211; Self-pay",<%= data.AnswerOrEmptyString("M0150PaymentSourceSelfPay").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Unknown",<%= data.AnswerOrEmptyString("M0150PaymentSourceUnknown").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("11 &#8211; Other (specify)",<%= data.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").Equals("1").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").Equals("1") ? data.AnswerOrEmptyString("M0150PaymentSourceOther").Clean() : string.Empty %>",false,1)) +
<%  } %>
        "","Episode Information");
</script>