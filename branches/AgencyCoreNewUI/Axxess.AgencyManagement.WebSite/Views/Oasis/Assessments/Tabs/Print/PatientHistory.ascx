<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
  printview.addsection(
            printview.col(2,
                printview.span("Last Physician Visit Date:",true) +
                printview.span("<%=  data.AnswerOrEmptyString("GenericLastVisitDate").Clean() %>",false,1)));
    printview.addsection(
        printview.col(2,
            printview.span("Allergies:",true) +
            printview.span("<%= data.AnswerOrEmptyString("485Allergies").Equals("No") ? "No Known Allergies" : data.AnswerOrDefault("485AllergiesDescription", "Not Specified").Clean() %>")));
    printview.addsection(
        printview.col(4,
            printview.span("Apical Pulse:",true) +
            printview.col(3,
                printview.span("<%= data.AnswerOrEmptyString("GenericPulseApical").Clean() %>",false,1) +
                printview.checkbox("Reg",<%= data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Irreg",<%= data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("2").ToString().ToLower() %>)) +
            printview.span("Radial Pulse:",true) +
            printview.col(3,
                printview.span("<%= data.AnswerOrEmptyString("GenericPulseRadial").Clean() %>",false,1) +
                printview.checkbox("Reg",<%= data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Irreg",<%= data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("2").ToString().ToLower() %>)) +
            printview.span("<strong>Height:</strong><%= data.AnswerOrEmptyString("GenericHeight").Clean() %>") +
            printview.span("<strong>Weight:</strong><%= data.AnswerOrEmptyString("GenericWeight").Clean() %> <%= data.AnswerOrEmptyString("GenericWeightActualStated").Equals("1") ? "(Actual)" : string.Empty %><%= data.AnswerOrEmptyString("GenericWeightActualStated").Equals("2") ? "(Stated)" : string.Empty %>") +
            printview.span("<strong>Temp</strong><%= data.AnswerOrEmptyString("GenericTemp").Clean() %>") +
            printview.span("<strong>Resp</strong><%= data.AnswerOrEmptyString("GenericResp").Clean() %>") +
            printview.span("BP",true) +
            printview.span("Lying",true) +
            printview.span("Sitting",true) +
            printview.span("Standing",true) +
            printview.span("Left",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftLying").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftSitting").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPLeftStanding").Clean() %>",false,1) +
            printview.span("Right",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPRightLying").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPRightSitting").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBPRightStanding").Clean() %>",false,1)),
        "Vital Signs");
    printview.addsection(
        printview.col(6,
            printview.span("Temperature:",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericTempGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericTempLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Pulse:",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericPulseGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericPulseLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Respirations:",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericRespirationGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericRespirationLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Systolic BP:",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericSystolicBPGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericSystolicBPLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Diastolic BP:",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericDiastolicBPGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericDiastolicBPLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("O<sub>2</sub> Sat (percent):",true) +
            printview.span(" &#60;<%= data.AnswerOrDefault("Generic02SatLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Fasting Blood Sugar:",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericFastingBloodSugarGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericFastingBloodSugarLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Random Blood Sugar",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericRandomBloddSugarGreaterThan", "<span class='blank short'></span>").Clean() %> or &#60;<%= data.AnswerOrDefault("GenericRandomBloodSugarLessThan", "<span class='blank short'></span>").Clean() %>") +
            printview.span("Weight Gain/Loss (lbs/7 days):",true) +
            printview.span(" &#62;<%= data.AnswerOrDefault("GenericWeightGreaterThan", "<span class='blank short'></span>").Clean() %>")),
        "Vital Sign Parameters");
    printview.addsection(
        printview.col(6,
            printview.span("Pneumonia",true) +
            printview.span("<%= data.AnswerOrEmptyString("485Pnemonia").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("485PnemoniaDate").Clean() %>",false,1) +
            printview.span("Flu",true) +
            printview.span("<%= data.AnswerOrEmptyString("485Flu").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("485FluDate").Clean() %>",false,1) +
            printview.span("TB",true) +
            printview.span("<%= data.AnswerOrEmptyString("485TB").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("485TBDate").Clean() %>",false,1) +
            printview.span("TB Exposure",true) +
            printview.span("<%= data.AnswerOrEmptyString("485TBExposure").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("485TBExposureDate").Clean() %>",false,1) +
            <%= data.AnswerOrEmptyString("485AdditionalImmunization1Name").IsNotNullOrEmpty() ?
                "printview.span(\"" + data.AnswerOrEmptyString("485AdditionalImmunization1Name").Clean() + "\",true,1) +" +
                "printview.span(\"" + data.AnswerOrEmptyString("485AdditionalImmunization1").Clean() + "\",false,1) +" +
                "printview.span(\"" + data.AnswerOrEmptyString("485AdditionalImmunization1Date").Clean() + "\",false,1) +" : "\"\" + " %>
            <%= data.AnswerOrEmptyString("485AdditionalImmunization2Name").IsNotNullOrEmpty() ? 
                "printview.span(\"" + data.AnswerOrEmptyString("485AdditionalImmunization2Name").Clean() + "\",true,1) +" +
                "printview.span(\"" + data.AnswerOrEmptyString("485AdditionalImmunization2").Clean() + "\",false,1) +" +
                "printview.span(\"" + data.AnswerOrEmptyString("485AdditionalImmunization2Date").Clean() + "\",false,1)" : "\"\"" %>
        ) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485ImmunizationComments").Clean() %>",false,2)
        ,"Immunizations");
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1000) " : string.Empty %>From which of the following Inpatient Facilities was the patient discharged during the past 14 days? (Mark all that apply)",true) +
        printview.col(3,
            printview.checkbox("1 &#8211; Long-term nursing facility (NF)",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Skilled nursing facility (SNF/ TCU)",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Short-stay acute hospital (IPPS)",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Long-term care hospital (LTCH)",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; Inpatient rehabilitation hospital or unit (IRF)",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &#8211; Psychiatric hospital or unit",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("7 &#8211; Other (specify) <%= data.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").Equals("1") ? data.AnswerOrEmptyString("M1000InpatientFacilitiesOther").Clean() : string.Empty %>",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Not discharged from an inpatient facility",<%= data.AnswerOrEmptyString("M1000InpatientFacilitiesNone").Equals("1").ToString().ToLower() %>)));
        <%  if (!data.AnswerOrEmptyString("M1000InpatientFacilitiesNone").Equals("1")) { %>
        printview.addsection(
            printview.col(2,
                printview.span("<%= isOasis ? "(M1005) " : string.Empty %>Inpatient Discharge Date (most recent)",true) +
                printview.span("<%= data.AnswerOrEmptyString("M1005InpatientDischargeDateUnknown").Equals("1") ? "UK &#8211; Unknown" : data.AnswerOrEmptyString("M1005InpatientDischargeDate").Clean() %>",false,1)));
        printview.addsection(
            printview.span("<%= isOasis ? "(M1010) " : string.Empty %>List each Inpatient Diagnosis and ICD-9-C M code at the level of highest specificity for only those conditions treated during an inpatient stay within the last 14 days",true) +
            "%3Cspan class=%22float-left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis1").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode1").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis2").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode2").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis3").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode3").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis4").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode4").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ee.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis5").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode5").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ef.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis6").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode6").Clean() %>",false,1)) +
            "%3C/span%3E");
            <%  if (!data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCodeNotApplicable").Equals("1")) { %>
            printview.addsection(
                printview.span("<%= isOasis ? "(M1012) " : string.Empty %>List each Inpatient Procedure and the associated ICD-9-C M procedure code relevant to the plan of care.",true) +
                <%  if (data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCodeUnknown").Equals("1")) { %>
                printview.span("UK &#8211; Unknown"));
                <%  } else { %>
                "%3Cspan class=%22float-left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedure1").Clean() %>",false,1) +
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode1").Clean() %>",false,1)) +
                "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedure2").Clean() %>",false,1) +
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode2").Clean() %>",false,1)) +
                "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedure3").Clean() %>",false,1) +
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode3").Clean() %>",false,1)) +
                "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedure4").Clean() %>",false,1) +
                    printview.span("<%= data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode4").Clean() %>",false,1)) +
                "%3C/span%3E");
                <%  } %>
            <%  } %>
        <%  } %>
        <%  if (!data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisNotApplicable").Equals("1")) { %>
        printview.addsection(
            printview.span("<%= isOasis ? "(M1016) " : string.Empty %>Diagnoses Requiring Medical or Treatment Regimen Change Within Past 14 Days: List the patient&#8217;s Medical Diagnoses and ICD-9-C M codes at the level of highest specificity for those conditions requiring changed medical or treatment regimen within the past 14 days",true) +
            "%3Cspan class=%22float-left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis1").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode1").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis2").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode2").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis3").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode3").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis4").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode4").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ee.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis5").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode5").Clean() %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Cspan class=%22float-left%22%3Ef.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis6").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode6").Clean() %>",false,1)) +
            "%3C/span%3E");
        <%  } %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1018) " : string.Empty %>Conditions Prior to Medical or Treatment Regimen Change or Inpatient Stay Within Past 14 Days: If this patient experienced an inpatient facility discharge or change in medical or treatment regimen within the past 14 days, indicate any conditions which existed prior to the inpatient stay or change in medical or treatment regime.",true) +
        printview.col(2,
            printview.checkbox("1 &#8211; Urinary incontinence",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenUI").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Indwelling/suprapubic catheter",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenCATH").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Intractable pain",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenPain").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Impaired decision-making",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenDECSN").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &#8211; Disruptive or socially inappropriate behavior",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenDisruptive").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &#8211; Memory loss to the extent that supervision required",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenMemLoss").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("7 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenNone").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Unknown",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenUK").Equals("1").ToString().ToLower() %>)
        ) +
        printview.checkbox("NA &#8211; No inpatient facility discharge and no change in medical or treatment regimen in past 14 days",<%= data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenNA").Equals("1").ToString().ToLower() %>));
    <%  } %>
    printview.addsection(
        printview.col(4,
            printview.span("Column 1",true) +
            printview.span("Column 2",true) +
            printview.span("Column 3",true) +
            printview.span("Column 4",true) +
            printview.span("<%= isOasis ? "(M1020) " : string.Empty %>Primary Diagnosis",true) +
            printview.span("ICD-9-C M",true) +
            printview.span("<%= isOasis ? "(M1024) " : string.Empty %>Description/ ICD-9-C M",true) +
            printview.span("<%= isOasis ? "(M1024) " : string.Empty %>Description/ ICD-9-C M",true)) +
        "%3Cspan class=%22float-left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data.AnswerOrEmptyString("M1020PrimaryDiagnosis").Clean() %><%= data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis").Equals("2") ? " Onset " : string.Empty %><%= data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis").Equals("1") ? " Exacerbation " : string.Empty %><%= data.AnswerOrEmptyString("M1020PrimaryDiagnosisDate").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("M1020ICD9M").Clean() %> Severity: <%= data.AnswerOrEmptyString("M1020SymptomControlRating").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("M1024PaymentDiagnosesA3").Clean() %> <%= data.AnswerOrEmptyString("M1024ICD9MA3").Clean() %>",false,1) +
            printview.span("<%= data.AnswerOrEmptyString("M1024PaymentDiagnosesA4").Clean() %> <%= data.AnswerOrEmptyString("M1024ICD9MA4").Clean() %>",false,1)) +
        "%3C/span%3E" +
        printview.span("<%= isOasis ? "(M1022) " : string.Empty %>Other Diagnoses",true) +
        <%  for (int i = 1; i < 26; i++) { %>
            <%  if (data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).IsNotNullOrEmpty()) { %>
        "%3Cspan class=%22float-left%22%3E<%= (char)(97 + i) %>.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("M1022ICD9M" + i).Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(65 + i) + "3").Clean() %> <%= data.AnswerOrEmptyString("M1024ICD9M" + (char)(65 + i) + "3").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(65 + i) + "4").Clean() %> <%= data.AnswerOrEmptyString("M1024ICD9M" + (char)(65 + i) + "4").Clean() %>") +
            printview.span("Severity: <%= data.AnswerOrEmptyString("M1022OtherDiagnose" + i + "Rating").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis" + i).Equals("1") ? "Exacerbation" : string.Empty %><%= data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis" + i).Equals("2") ? "Onset" : string.Empty %>") +
            printview.span("<%= data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i + "Date").Clean() %>") +
            printview.span("")) +
        "%3C/span%3E%3Cspan class=%22clear%22%3E%3C/span%3E" +
            <%  } %>
        <%  } %>
    "");
    printview.addsection(
        printview.col(3,
            printview.span("Surgical Procedure",true) +
            printview.span("Code",true) +
            printview.span("Date",true) +
            printview.span("<%= data.AnswerOrEmptyString("485SurgicalProcedureDescription1").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("485SurgicalProcedureCode1").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("485SurgicalProcedureCode1Date").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("485SurgicalProcedureDescription2").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("485SurgicalProcedureCode2").Clean() %>") +
            printview.span("<%= data.AnswerOrEmptyString("485SurgicalProcedureCode2Date").Clean() %>")),
        "Surgical Procedure");
    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1030) " : string.Empty %>Therapies the patient receives at home",true) +
        printview.col(2,
            printview.checkbox("1 &#8211; Intravenous or infusion therapy (excludes TPN)",<%= data.AnswerOrEmptyString("M1030HomeTherapiesInfusion").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Parenteral nutrition (TPN or lipids)",<%= data.AnswerOrEmptyString("M1030HomeTherapiesParNutrition").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Enteral nutrition",<%= data.AnswerOrEmptyString("M1030HomeTherapiesEntNutrition").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1030HomeTherapiesNone").Equals("1").ToString().ToLower() %>)));
    <%  } %>
<%  } %>
</script>