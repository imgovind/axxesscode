<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(6,
            printview.span("Onset Date:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPainOnSetDate").Clean() %>",0,1) +
            printview.span("Duration:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDurationOfPain").Equals("1") ? "Continuous" : string.Empty %><%= data.AnswerOrEmptyString("GenericDurationOfPain").Equals("2") ? "Intermittent" : string.Empty %>",0,1) +
            printview.span("Pain Intensity:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericIntensityOfPain").Clean() %>",0,1) +
            printview.span("Description:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericQualityOfPain").Equals("1") ? "Aching" : string.Empty %><%= data.AnswerOrEmptyString("GenericQualityOfPain").Equals("2") ? "Throbbing" : string.Empty %><%= data.AnswerOrEmptyString("GenericQualityOfPain").Equals("3") ? "Burning" : string.Empty %><%= data.AnswerOrEmptyString("GenericQualityOfPain").Equals("4") ? "Sharp" : string.Empty %><%= data.AnswerOrEmptyString("GenericQualityOfPain").Equals("5") ? "Tender" : string.Empty %><%= data.AnswerOrEmptyString("GenericQualityOfPain").Equals("6") ? "Other" : string.Empty %>",0,1) +
            printview.span("Primary Site:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericLocationOfPain").Clean() %>",0,1) +
            printview.span("Pain Mgmt. Effect:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMedicationEffectiveness").Equals("0") ? "N/A" : string.Empty %><%= data.AnswerOrEmptyString("GenericMedicationEffectiveness").Equals("1") ? "Effective" : string.Empty %><%= data.AnswerOrEmptyString("GenericMedicationEffectiveness").Equals("2") ? "Not Effective" : string.Empty %>",0,1)) +
        printview.span("What makes pain better:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericWhatMakesPainBetter").Clean() %>",false,2) +
        printview.span("What makes pain worse:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericWhatMakesPainWorse").Clean() %>",false,2) +
        printview.span("Patient&#8217;s pain goal:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPatientPainGoal").Clean() %>",false,2) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPatientPainComment").Clean() %>",false,2),
        "Pain Scale");
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1240) " : string.Empty %>Has this patient had a formal Pain Assessment using a standardized pain assessment tool (appropriate to the patient&#8217;s ability to communicate the severity of pain)?",true) +
        printview.col(3,
            printview.checkbox("0 &#8211; No standardized assessment conducted",<%= data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes, and it does not indicate severe pain",<%= data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Yes, and it indicates severe pain",<%= data.AnswerOrEmptyString("M1240FormalPainAssessment").Equals("02").ToString().ToLower() %>)));
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() != 11 || Model.AssessmentTypeNum.ToInteger() != 14) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1242) " : string.Empty %>Frequency of Pain Interfering with patient&#8217;s activity or movement",true) +
        printview.col(2,
            printview.checkbox("0 &#8211; Patient has no pain",<%= data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Patient has pain that does not interfere with activity or movement",<%= data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Less often than daily",<%= data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Daily, but not constantly",<%= data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; All of the time",<%= data.AnswerOrEmptyString("M1242PainInterferingFrequency").Equals("04").ToString().ToLower() %>)));
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Pain.ascx", Model); %>
<%  } %>
</script>