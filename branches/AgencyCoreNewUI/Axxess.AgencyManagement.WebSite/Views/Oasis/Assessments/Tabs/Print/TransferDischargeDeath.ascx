<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
    <%  if (Model.AssessmentTypeNum != "08") { %>
    printview.addsection(
        "%3Ctable%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("<%= isOasis ? "(M2400) " : string.Empty %>Intervention Synopsis: (Check only one box in each row.) Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("Plan/ Intervention",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("No",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("Yes",true) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("NA &#8211; Not Applicable",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &#8211; Patient is not diabetic or is bilateral amputee",<%= data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. Falls prevention interventions") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &#8211; Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment",<%= data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &#8211; Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment",<%= data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("d. Intervention(s) to monitor and mitigate pain") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2400PainIntervention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2400PainIntervention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &#8211; Formal assessment did not indicate pain since the last OASIS assessment",<%= data.AnswerOrEmptyString("M2400PainIntervention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("e. Intervention(s) to prevent pressure ulcers") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &#8211; Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment",<%= data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("f. Pressure ulcer treatment based on principles of moist wound healing") +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("00").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("01").ToString().ToLower() %>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA &#8211; Dressings that support the principles of moist wound healing not indicated for this patient&#8217;s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing",<%= data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("NA").ToString().ToLower() %>) +
        "%3C/td%3E%3C/tr%3E%3C/table%3E");
    printview.addsection(
        printview.span("<%= isOasis ? "(M2410) " : string.Empty %>To which Inpatient Facility has the patient been admitted?",true) +
        printview.col(3,
            printview.checkbox("1 &#8211; Hospital",<%= data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Rehabilitation facility",<%= data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Nursing home",<%= data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Hospice",<%= data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("04").ToString().ToLower() %>)
            <%  if (Model.AssessmentTypeNum.ToInteger() == 9) { %>
            + printview.checkbox("NA &#8211; No inpatient facility admission",<%= data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("NA").ToString().ToLower() %>)
    		<%  } %>
            ));
		<%  if (data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("NA")) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M2420) " : string.Empty %>Discharge Disposition: Where is the patient after discharge from your agency? (Choose only one answer.)",true) +
        printview.checkbox("1 &#8211; Patient remained in the community (without formal assistive services)",<%= data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Patient remained in the community (with formal assistive services)",<%= data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Patient transferred to a non-institutional hospice",<%= data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Unknown because patient moved to a geographic location not served by this agency",<%= data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("04").ToString().ToLower() %>) +
        printview.checkbox("UK &#8211; Other unknown",<%= data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("UK").ToString().ToLower() %>));
		<%  } %>
        <%  if (Model.AssessmentTypeNum.ToInteger() == 6 || Model.AssessmentTypeNum.ToInteger() == 7) { %>
    printview.addsection(
        printview.span("(M2430) Reason for Hospitalization: For what reason(s) did the patient require hospitalization? (Mark all that apply.)",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Improper medication administration, medication side effects, toxicity, anaphylaxis",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationMed").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &ndash; Injury caused by fall",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationFall").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &ndash; Respiratory infection (e.g., pneumonia, bronchitis)",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationInfection").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &ndash; Other respiratory problem",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationOtherRP").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &ndash; Heart failure (e.g., fluid overload)",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartFail").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &ndash; Cardiac dysrhythmia (irregular heartbeat)",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationCardiac").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("7 &ndash; Myocardial infarction or chest pain",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationMyocardial").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("8 &ndash; Other heart disease",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartDisease").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("9 &ndash; Stroke (CVA) or TIA",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationStroke").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("10 &ndash; Hypo/Hyperglycemia, diabetes out of control",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationHypo").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("11 &ndash; GI bleeding, obstruction, constipation, impaction",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationGI").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("12 &ndash; Dehydration, malnutrition",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationDehMal").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("13 &ndash; Urinary tract infection",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationUrinaryInf").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("14 &ndash; IV catheter-related infection or complication",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationIV").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("15 &ndash; Wound infection or deterioration",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationWoundInf").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("16 &ndash; Uncontrolled pain",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationUncontrolledPain").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("17 &ndash; Acute mental/behavioral health problem",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationMental").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("18 &ndash; Deep vein thrombosis, pulmonary embolus",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationDVT").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("19 &ndash; Scheduled treatment or procedure",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationScheduled").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("20 &ndash; Other than above reasons",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationOther").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("UK &ndash; Reason unknown",<%= data.AnswerOrEmptyString("M2430ReasonForHospitalizationUK").Equals("1").ToString().ToLower() %>)));
    printview.addsection(
        printview.span("(M2440) For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all that apply.)",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Therapy services",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedTherapy").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &ndash; Respite care",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedRespite").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &ndash; Hospice care",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedHospice").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &ndash; Permanent placement",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedPermanent").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("5 &ndash; Unsafe for care at home",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnsafe").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("6 &ndash; Other",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedOther").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("UK &ndash; Unknown",<%= data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnknown").Equals("1").ToString().ToLower() %>)));
        <%  } %>
    <%  } %>
    printview.addsection(
        printview.col(2,
            printview.span("<%= isOasis ? "(M0903) " : string.Empty %>Date of Last (Most Recent) Home Visit:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0903LastHomeVisitDate").Clean() %>",false,1) +
            printview.span("<%= isOasis ? "(M0906) " : string.Empty %>Discharge/Transfer/Death Date:",true) +
            printview.span("<%= data.AnswerOrEmptyString("M0906DischargeDate").Clean() %>",false,1)));
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericDischargeNarrative").Clean() %>", false,2), "Narrative");
<%  } %>
</script>