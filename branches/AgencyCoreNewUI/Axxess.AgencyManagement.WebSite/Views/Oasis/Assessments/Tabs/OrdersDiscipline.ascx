<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>"  %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "OrdersDiscipline_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "OrdersDisciplineTreatment")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Save &#38; Check for Errors</a></li>
        <%  } else { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));Oasis.NonOasisSignature('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')">Save &#38; Complete</a></li>
        <%  } %>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
    </div>
    <div id="<%= Model.TypeName %>_BlankMasterCalendar"></div>
    <fieldset>
        <legend>Orders for Discipline and Treatments</legend>
        <div class="wide column">
            <div class="row">
                <div class="buttons align-center">
                    <ul>
                        <li><a id="<%= Model.TypeName %>_Show" href="javascript:void(0)" onclick="Oasis.LoadBlankMasterCalendar('<%= Model.TypeName %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>')">Show Calendar</a></li>
                        <li><a id="<%= Model.TypeName %>_Hide" style="display:none" href="javascript:void(0)" onclick="Oasis.HideBlankMasterCalendar('<%= Model.TypeName %>')">Hide Calendar</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485SNFrequency" class="float-left">SN Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485SNFrequency", data.AnswerOrEmptyString("485SNFrequency"), new { @id = Model.TypeName + "_485SNFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, SN Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485PTFrequency" class="float-left">PT Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485PTFrequency", data.AnswerOrEmptyString("485PTFrequency"), new { @id = Model.TypeName + "_485PTFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, PT Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485OTFrequency" class="float-left">OT Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485OTFrequency", data.AnswerOrEmptyString("485OTFrequency"), new { @id = Model.TypeName + "_485OTFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, OT Frequency" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485STFrequency" class="float-left">ST Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485STFrequency", data.AnswerOrEmptyString("485STFrequency"), new { @id = Model.TypeName + "_485STFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, ST Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485MSWFrequency" class="float-left">MSW Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485MSWFrequency", data.AnswerOrEmptyString("485MSWFrequency"), new { @id = Model.TypeName + "_485MSWFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, MSW Frequency" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485HHAFrequency" class="float-left">HHA Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.TypeName + "_485HHAFrequency", data.AnswerOrEmptyString("485HHAFrequency"), new { @id = Model.TypeName + "_485HHAFrequency", @maxlength = "70", @title = "(485 Locator 21) Orders, HHA Frequency" })%></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485OrdersDisciplineInterventionComments" class="strong">Additional Orders</label>
                <%= Html.Templates(Model.TypeName + "_485OrdersDisciplineInterventionTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485OrdersDisciplineInterventionComments" })%>
                <%= Html.TextArea(Model.TypeName + "_485OrdersDisciplineInterventionComments", data.AnswerOrEmptyString("485OrdersDisciplineInterventionComments"), 5, 70, new { @id = Model.TypeName + "_485OrdersDisciplineInterventionComments", @title = "(485 Locator 21) Orders" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Conclusions</legend>
        <%  string[] conclusions = data.AnswerArray("485Conclusions"); %>
        <%= Html.Hidden(Model.TypeName + "_485Conclusions", "", new { @id = Model.TypeName + "_485ConclusionsHidden" })%>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Conclusions, Skilled Intervention Needed' id='{0}_485Conclusions1' name='{0}_485Conclusions' value='1' type='checkbox' {1} />", Model.TypeName, conclusions.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485Conclusions1">Skilled Intervention Needed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Conclusions, Skilled Instruction Needed' id='{0}_485Conclusions2' name='{0}_485Conclusions' value='2' type='checkbox' {1} />", Model.TypeName, conclusions.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485Conclusions2">Skilled Instruction Needed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Conclusions, No Skilled Service Needed' id='{0}_485Conclusions3' name='{0}_485Conclusions' value='3' type='checkbox' {1} />", Model.TypeName, conclusions.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485Conclusions3">No Skilled Service Needed</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485ConclusionOther" class="float-left">Other</label>
                <%= Html.TextArea(Model.TypeName + "_485ConclusionOther", data.AnswerOrEmptyString("485ConclusionOther"), new { @id = Model.TypeName + "_485ConclusionOther", @title = "(Optional) Other Conclusions" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Rehabilitation Potential</legend>
        <%  string[] rehabilitationPotential = data.AnswerArray("485RehabilitationPotential"); %>
        <%= Html.Hidden(Model.TypeName + "_485RehabilitationPotential", "", new { @id = Model.TypeName + "_485RehabilitationPotentialHidden" })%>
        <div class="wide column">
            <div class="row">
                <label class="strong">Rehabilitation potential for stated goals</label>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Rehab Potential, Good' id='{0}_485RehabilitationPotential1' name='{0}_485RehabilitationPotential' value='1' type='checkbox' {1} />", Model.TypeName, rehabilitationPotential.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485RehabilitationPotential1">Good</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Rehab Potential, Fair' id='{0}_485RehabilitationPotential2' name='{0}_485RehabilitationPotential' value='2' type='checkbox' {1} />", Model.TypeName, rehabilitationPotential.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485RehabilitationPotential2">Fair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Rehab Potential, Poor' id='{0}_485RehabilitationPotential3' name='{0}_485RehabilitationPotential' value='3' type='checkbox' {1} />", Model.TypeName, rehabilitationPotential.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485RehabilitationPotential3">Poor</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485AchieveGoalsComments" class="float-left">Other rehabilitation potential</label>
                <%= Html.Templates(Model.TypeName + "_485AchieveGoalsTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485AchieveGoalsComments" })%>
                <%= Html.TextArea(Model.TypeName + "_485AchieveGoalsComments", data.AnswerOrEmptyString("485AchieveGoalsComments"), 5, 70, new { @id = Model.TypeName + "_485AchieveGoalsComments", @title = "(Optional) Other Rehab Potentials" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Discharge Plans</legend>
        <%  string[] dischargePlans = data.AnswerArray("485DischargePlans"); %>
        <%= Html.Hidden(Model.TypeName + "_485DischargePlans", "", new { @id = Model.TypeName + "_485DischargePlansHidden" })%>
        <div class="wide column">
            <div class="row">
                <label class="strong">Patient to be discharged to the care of</label>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Discharged to Physician Care' id='{0}_485DischargePlans1' name='{0}_485DischargePlans' value='1' type='checkbox' {1} />", Model.TypeName, dischargePlans.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485DischargePlans1">Physician</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Discharged to Caregiver Care' id='{0}_485DischargePlans2' name='{0}_485DischargePlans' value='2' type='checkbox' {1} />", Model.TypeName, dischargePlans.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485DischargePlans2">Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Discharged to Self Care' id='{0}_485DischargePlans3' name='{0}_485DischargePlans' value='3' type='checkbox' {1} />", Model.TypeName, dischargePlans.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485DischargePlans3">Self care</label>
                    </div>
               </div>
            </div>
            <div class="row">
                <label class="strong">Discharge Plans</label>
                <%  string[] dischargePlansReason = data.AnswerArray("485DischargePlansReason"); %>
                <%= Html.Hidden(Model.TypeName + "_485DischargePlansReason", "", new { @id = Model.TypeName + "_485DischargePlansReasonHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Discharge when Caregiver Willing/Able to Manage' id='{0}_485DischargePlansReason1' name='{0}_485DischargePlansReason' value='1' type='checkbox' {1} />", Model.TypeName, dischargePlansReason.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485DischargePlansReason1">Discharge when caregiver willing and able to manage all aspects of patient&#8217;s care.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Discharge when Goals Met' id='{0}_485DischargePlansReason2' name='{0}_485DischargePlansReason' value='2' type='checkbox' {1} />", Model.TypeName, dischargePlansReason.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485DischargePlansReason2">Discharge when goals met.</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Discharge when Wounds Healed' id='{0}_485DischargePlansReason3' name='{0}_485DischargePlansReason' value='3' type='checkbox' {1} />", Model.TypeName, dischargePlansReason.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485DischargePlansReason3">Discharge when wound(s) healed.</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485DischargePlanComments" class="strong">Additional discharge plans</label>
                <%= Html.Templates(Model.TypeName + "_485DischargePlanTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485DischargePlanComments" })%>
                <%= Html.TextArea(Model.TypeName + "_485DischargePlanComments", data.AnswerOrEmptyString("485DischargePlanComments"), 5, 70, new { @id = Model.TypeName + "_485DischargePlanComments", @title = "(Optional) Other Discharge Plans" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInterventionComments" class="strong">Skilled Intervention/Teaching</label>
                <%= Html.Templates(Model.TypeName + "_485SkilledInterventionTemplate", new { @class = "Templates", @template = "#" + Model.TypeName + "_485SkilledInterventionComments" })%>
                <%= Html.TextArea(Model.TypeName + "_485SkilledInterventionComments", data.AnswerOrEmptyString("485SkilledInterventionComments"), 5, 70, new { @id = Model.TypeName + "_485SkilledInterventionComments", @style = "height:150px", @title = "(Optional) Narrative" })%>
                <%  string[] iResponse = data.AnswerArray("485SIResponse"); %>
                <%= Html.Hidden(Model.TypeName + "_485SIResponse", "", new { @id = Model.TypeName + "_485SIResponseHidden" })%>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Verbalizes Understanding' id='{0}_485SIResponse1' name='{0}_485SIResponse' value='1' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("1").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485SIResponse1">Verbalizes</label>
                            <%= Html.TextBox(Model.TypeName + "_485SIVerbalizedUnderstandingPercent", data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPercent"), new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingPercent" })%>
                            <label for="<%= Model.TypeName %>_485SIResponse1">understanding of teaching.</label>
                            <%= Html.Hidden(Model.TypeName + "_485SIVerbalizedUnderstandingPT", "", new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Verbalizes Understanding' id='{0}_485SIVerbalizedUnderstandingPT' class='no_float' name='{0}_485SIVerbalizedUnderstandingPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIVerbalizedUnderstandingPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485SIVerbalizedUnderstandingCG", "", new { @id = Model.TypeName + "_485SIVerbalizedUnderstandingCGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Verbalizes Understanding' id='{0}_485SIVerbalizedUnderstandingCG' class='no_float' name='{0}_485SIVerbalizedUnderstandingCG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485SIVerbalizedUnderstandingCG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485SIVerbalizedUnderstandingCG">CG</label>
                        </span>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Needs Further Teaching' id='{0}_485SIResponse2' name='{0}_485SIResponse' value='2' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("2").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485SIResponse2">Needs further teaching.</label>
                            <%= Html.Hidden(Model.TypeName + "_485NEEDSFURTHERTEACHINGPT", "", new { @id = Model.TypeName + "_485NEEDSFURTHERTEACHINGPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Needs Further Teaching' id='{0}_485NEEDSFURTHERTEACHINGPT' class='no_float' name='{0}_485NEEDSFURTHERTEACHINGPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485NEEDSFURTHERTEACHINGPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NEEDSFURTHERTEACHINGPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485NEEDSFURTHERTEACHINGCG", "", new { @id = Model.TypeName + "_485NEEDSFURTHERTEACHINGCGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Needs Further Teaching' id='{0}_485NEEDSFURTHERTEACHINGCG' class='no_float' name='{0}_485NEEDSFURTHERTEACHINGCG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485NEEDSFURTHERTEACHINGCG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485NEEDSFURTHERTEACHINGCG">CG</label>
                        </span>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Able to Return Demonstration' id='{0}_485SIResponse3' name='{0}_485SIResponse' value='3' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("3").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485SIResponse3">Able to return correct demonstration of procedure.</label>
                            <%= Html.Hidden(Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPT", "", new { @id = Model.TypeName + "_485DEMONSTRATIONOFPROCEDUREPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Able to Return Demonstration' id='{0}_485DEMONSTRATIONOFPROCEDUREPT' class='no_float' name='{0}_485DEMONSTRATIONOFPROCEDUREPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDUREPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485DEMONSTRATIONOFPROCEDUREPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECG", "", new { @id = Model.TypeName + "_485DEMONSTRATIONOFPROCEDURECGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Able to Return Demonstration' id='{0}_485DEMONSTRATIONOFPROCEDURECG' class='no_float' name='{0}_485DEMONSTRATIONOFPROCEDURECG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDURECG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485DEMONSTRATIONOFPROCEDURECG">CG</label>
                        </span>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Narrative, Unable to Return Demonstration' id='{0}_485SIResponse4' name='{0}_485SIResponse' value='4' type='checkbox' {1} />", Model.TypeName, iResponse.Contains("4").ToChecked()) %>
                        <span>
                            <label for="<%= Model.TypeName %>_485SIResponse4">Unable to return correct demonstration of procedure.</label>
                            <%= Html.Hidden(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPT", "", new { @id = Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDUREPTHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Patient Unable to Return Demonstration' id='{0}_485UNDEMONSTRATIONOFPROCEDUREPT' class='no_float' name='{0}_485UNDEMONSTRATIONOFPROCEDUREPT' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDUREPT").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485UNDEMONSTRATIONOFPROCEDUREPT">PT</label>
                            <%= Html.Hidden(Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECG", "", new { @id = Model.TypeName + "_485UNDEMONSTRATIONOFPROCEDURECGHidden" })%>
                            <%= string.Format("<input title='(Optional) Narrative, Caregiver Unable to Return Demonstration' id='{0}_485DEMONSTRATIONOFPROCEDURECG' class='no_float' name='{0}_485UNDEMONSTRATIONOFPROCEDURECG' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDURECG").Equals("1").ToChecked()) %>
                            <label for="<%= Model.TypeName %>_485UNDEMONSTRATIONOFPROCEDURECG">CG</label>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Coordination of Care</legend>
        <div class="wide column">
             <div class="row">
                <label class="strong">Conferenced With</label>
                <%  string[] conferencedWithName = data.AnswerArray("485ConferencedWith"); %>
                <%= Html.Hidden(Model.TypeName + "_485ConferencedWith", "", new { @id = Model.TypeName + "_485ConferencedWithHidden" })%>
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Medical Doctor' id='{0}_485ConferencedWithMD' name='{0}_485ConferencedWith' value='MD' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("MD").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithMD">MD</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Skilled Nurse' id='{0}_485ConferencedWithSN' name='{0}_485ConferencedWith' value='SN' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("SN").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithSN">SN</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Physical Therapist' id='{0}_485ConferencedWithPT' name='{0}_485ConferencedWith' value='PT' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("PT").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithPT">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Occupational Therapist' id='{0}_485ConferencedWithOT' name='{0}_485ConferencedWith' value='OT' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("OT").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithOT">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Speech Therapist' id='{0}_485ConferencedWithST' name='{0}_485ConferencedWith' value='ST' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("ST").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithST">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Medical Social Worker' id='{0}_485ConferencedWithMSW' name='{0}_485ConferencedWith' value='MSW' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("MSW").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithMSW">MSW</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Coordination of Care Conferenced with Home Health Aide' id='{0}_485ConferencedWithHHA' name='{0}_485ConferencedWith' value='HHA' type='checkbox' {1} />", Model.TypeName, conferencedWithName.Contains("HHA").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485ConferencedWithHHA">HHA</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485ConferencedWithName" class="float-left">Name</label>
                <div class="fr">
                    <%= Html.TextBox(Model.TypeName + "_485ConferencedWithName", data.AnswerOrEmptyString("485ConferencedWithName"), new { @id = Model.TypeName + "_485ConferencedWithName", @maxlength = "30", @title = "(Optional) Cordination of Care Name" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485SkilledInterventionRegarding" class="strong">Regarding</label>
                <%= Html.TextArea(Model.TypeName + "_485SkilledInterventionRegarding", data.AnswerOrEmptyString("485SkilledInterventionRegarding"), 5, 70, new { @id = Model.TypeName + "_485SkilledInterventionRegarding", @title = "(Optional) Cordination of Care Regarding" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Save &#38; Check for Errors</a></li>
        <%  } else { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));Oasis.NonOasisSignature('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')">Save &#38; Complete</a></li>
        <%  } %>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<%  } %>