<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "Oasis" + Model.TypeName + "TransferDischargeDeath_Form" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName)%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>    
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Save &#38; Check for Errors</a></li>
        <%  } else { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));Oasis.NonOasisSignature('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')">Save &#38; Complete</a></li>
        <%  } %>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
    </div>
    <fieldset class="oasis">
        <legend>
        <%  if (Model.AssessmentTypeNum.ToInteger() == 6 || Model.AssessmentTypeNum.ToInteger() == 7) { %>
            Transfer
        <%  } else if (Model.AssessmentTypeNum.ToInteger() == 8) { %>
            Death
        <%  } else { %>
            Discharge
        <%  } %>
        </legend>
        <%  if (Model.AssessmentTypeNum != "08") { %>
        <div class="wide column">
            <div class="row" id="<%= Model.TypeName %>_M2400">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2400')">(M2400)</a>
                    Intervention Synopsis: Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?
                    <em>(Check only one box in each row)</em>
                </label>
                <table class="form">
                    <thead>
                        <tr>
                            <th colspan="4">Plan/Intervention</th>
                            <th>0 &#8211; No</th>
                            <th>1 &#8211; Yes</th>
                            <th colspan="4">NA &#8211; Not Applicable</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup">
                        <tr>
                            <td colspan="4">
                                <span class="float-left">a.</span>
                                <span>Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</span>
                                <%= Html.Hidden(Model.TypeName + "_M2400DiabeticFootCare", "", new { @id = Model.TypeName + "_M2400DiabeticFootCareHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400DiabeticFootCare", "00", data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("00"), new { @id = Model.TypeName + "_M2400DiabeticFootCare0" })%>
                                    <label for="<%= Model.TypeName %>_M2400DiabeticFootCare0">
                                        <span class="float-left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400DiabeticFootCare", "01", data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("01"), new { @id = Model.TypeName + "_M2400DiabeticFootCare1" })%>
                                    <label for="<%= Model.TypeName %>_M2400DiabeticFootCare1">
                                        <span class="float-left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="4">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400DiabeticFootCare", "NA", data.AnswerOrEmptyString("M2400DiabeticFootCare").Equals("NA"), new { @id = Model.TypeName + "_M2400DiabeticFootCareNA" })%>
                                    <label for="<%= Model.TypeName %>_M2400DiabeticFootCareNA">
                                        <span class="float-left">NA &#8211;</span>
                                        <span class="normal margin">Patient is not diabetic or is bilateral amputee</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span class="float-left">b.</span>
                                <span>Falls prevention interventions</span>
                                <%= Html.Hidden(Model.TypeName + "_M2400FallsPreventionInterventions", "", new { @id = Model.TypeName + "_M2400FallsPreventionInterventionsHidden" })%></td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400FallsPreventionInterventions", "00", data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("00"), new { @id = Model.TypeName + "_M2400FallsPreventionInterventions0" })%>
                                    <label for="<%= Model.TypeName %>_M2400FallsPreventionInterventions0">
                                        <span class="float-left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400FallsPreventionInterventions", "01", data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("01"), new { @id = Model.TypeName + "_M2400FallsPreventionInterventions1" })%>
                                    <label for="<%= Model.TypeName %>_M2400FallsPreventionInterventions1">
                                        <span class="float-left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="4">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400FallsPreventionInterventions", "NA", data.AnswerOrEmptyString("M2400FallsPreventionInterventions").Equals("NA"), new { @id = Model.TypeName + "_M2400FallsPreventionInterventionsNA" })%>
                                    <label for="<%= Model.TypeName %>_M2400FallsPreventionInterventionsNA">
                                        <span class="float-left">NA &#8211;</span>
                                        <span class="normal margin">Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span class="float-left">c.</span>
                                <span>Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</span>
                                <%= Html.Hidden(Model.TypeName + "_M2400DepressionIntervention", "", new { @id = Model.TypeName + "_M2400DepressionInterventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400DepressionIntervention", "00", data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("00"), new { @id = Model.TypeName + "_M2400DepressionIntervention0" })%>
                                    <label for="<%= Model.TypeName %>_M2400DepressionIntervention0">
                                        <span class="float-left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400DepressionIntervention", "01", data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("01"), new { @id = Model.TypeName + "_M2400DepressionIntervention1" })%>
                                    <label for="<%= Model.TypeName %>_M2400DepressionIntervention1">
                                        <span class="float-left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="4">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400DepressionIntervention", "NA", data.AnswerOrEmptyString("M2400DepressionIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2400DepressionInterventionNA" })%>
                                    <label for="<%= Model.TypeName %>_M2400DepressionInterventionNA">
                                        <span class="float-left">NA &#8211;</span>
                                        <span class="normal margin">Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span class="float-left">d.</span>
                                <span>Intervention(s) to monitor and mitigate pain</span>
                                <%= Html.Hidden(Model.TypeName + "_M2400PainIntervention", "", new { @id = Model.TypeName + "_M2400PainInterventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PainIntervention", "00", data.AnswerOrEmptyString("M2400PainIntervention").Equals("00"), new { @id = Model.TypeName + "_M2400PainIntervention0" })%>
                                    <label for="<%= Model.TypeName %>_M2400PainIntervention0">
                                        <span class="float-left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PainIntervention", "01", data.AnswerOrEmptyString("M2400PainIntervention").Equals("01"), new { @id = Model.TypeName + "_M2400PainIntervention1" })%>
                                    <label for="<%= Model.TypeName %>_M2400PainIntervention1">
                                        <span class="float-left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="4">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PainIntervention", "NA", data.AnswerOrEmptyString("M2400PainIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2400PainInterventionNA" })%>
                                    <label for="<%= Model.TypeName %>_M2400PainInterventionNA">
                                        <span class="float-left">NA &#8211;</span>
                                        <span class="normal margin">Formal assessment did not indicate pain since the last OASIS assessment</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span class="float-left">e.</span>
                                <span>Intervention(s) to prevent pressure ulcers</span>
                                <%= Html.Hidden(Model.TypeName + "_M2400PressureUlcerIntervention", "", new { @id = Model.TypeName + "_M2400PressureUlcerInterventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PressureUlcerIntervention", "00", data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("00"), new { @id = Model.TypeName + "_M2400PressureUlcerIntervention0" })%>
                                    <label for="<%= Model.TypeName %>_M2400PressureUlcerIntervention0">
                                        <span class="float-left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PressureUlcerIntervention", "01", data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("01"), new { @id = Model.TypeName + "_M2400PressureUlcerIntervention1" })%>
                                    <label for="<%= Model.TypeName %>_M2400PressureUlcerIntervention1">
                                        <span class="float-left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="4">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PressureUlcerIntervention", "NA", data.AnswerOrEmptyString("M2400PressureUlcerIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2400PressureUlcerInterventionNA" })%>
                                    <label for="<%= Model.TypeName %>_M2400PressureUlcerInterventionNA">
                                        <span class="float-left">NA &#8211;</span>
                                        <span class="normal margin">Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span class="float-left">f.</span>
                                <span>Pressure ulcer treatment based on principles of moist wound healing</span>
                                <%= Html.Hidden(Model.TypeName + "_M2400PressureUlcerTreatment", "", new { @id = Model.TypeName + "_M2400PressureUlcerTreatmentHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PressureUlcerTreatment", "00", data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("00"), new { @id = Model.TypeName + "_M2400PressureUlcerTreatment0" })%>
                                    <label for="<%= Model.TypeName %>_M2400PressureUlcerTreatment0">
                                        <span class="float-left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PressureUlcerTreatment", "01", data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("01"), new { @id = Model.TypeName + "_M2400PressureUlcerTreatment1" })%>
                                    <label for="<%= Model.TypeName %>_M2400PressureUlcerTreatment1">
                                        <span class="float-left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="4">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2400PressureUlcerTreatment", "NA", data.AnswerOrEmptyString("M2400PressureUlcerTreatment").Equals("NA"), new { @id = Model.TypeName + "_M2400PressureUlcerTreatmentNA" })%>
                                    <label for="<%= Model.TypeName %>_M2400PressureUlcerTreatmentNA">
                                        <span class="float-left">NA &#8211;</span>
                                        <span class="normal margin">Dressings that support the principles of moist wound healing not indicated for this patient&#8217;s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M2400')">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2410">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2410')">(M2410)</a>
                    To which Inpatient Facility has the patient been admitted?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2410TypeOfInpatientFacility", "", new { @id = Model.TypeName + "_M2410TypeOfInpatientFacilityHidden" })%>
                <div class="checkgroup">
            <%  if (Model.AssessmentTypeNum.ToInteger() != 9) { %>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2410TypeOfInpatientFacility", "01", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("01"), new { @id = Model.TypeName + "_M2410TypeOfInpatientFacility1" })%>
                        <label for="<%= Model.TypeName %>_M2410TypeOfInpatientFacility1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Hospital</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2410TypeOfInpatientFacility", "02", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("02"), new { @id = Model.TypeName + "_M2410TypeOfInpatientFacility2" })%>
                        <label for="<%= Model.TypeName %>_M2410TypeOfInpatientFacility2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Rehabilitation facility</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2410TypeOfInpatientFacility", "03", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("03"), new { @id = Model.TypeName + "_M2410TypeOfInpatientFacility3" })%>
                        <label for="<%= Model.TypeName %>_M2410TypeOfInpatientFacility3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Nursing home</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2410TypeOfInpatientFacility", "04", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("04"), new { @id = Model.TypeName + "_M2410TypeOfInpatientFacility4" })%>
                        <label for="<%= Model.TypeName %>_M2410TypeOfInpatientFacility4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Hospice</span>
                        </label>
                    </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2410TypeOfInpatientFacility", "NA", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility").Equals("NA"), new { @id = Model.TypeName + "_M2410TypeOfInpatientFacilityNA" })%>
                        <label for="<%= Model.TypeName %>_M2410TypeOfInpatientFacilityNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No inpatient facility admission</span>
                        </label>
                    </div>
            <%  } %>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M2410')">?</div>
                </div>
            </div>
            <%  if (Model.AssessmentTypeNum.ToInteger() == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M2420">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2420')">(M2420)</a>
                    Discharge Disposition: Where is the patient after discharge from your agency?
                    <em>(Choose only one answer)</em>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2420DischargeDisposition", "", new { @id = Model.TypeName + "_M2420DischargeDispositionHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2420DischargeDisposition", "01", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("01"), new { @id = Model.TypeName + "_M2420DischargeDisposition1" })%>
                        <label for="<%= Model.TypeName %>_M2420DischargeDisposition1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (without formal assistive services)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2420DischargeDisposition", "02", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("02"), new { @id = Model.TypeName + "_M2420DischargeDisposition2" })%>
                        <label for="<%= Model.TypeName %>_M2420DischargeDisposition2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (with formal assistive services)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2420DischargeDisposition", "03", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("03"), new { @id = Model.TypeName + "_M2420DischargeDisposition3" })%>
                        <label for="<%= Model.TypeName %>_M2420DischargeDisposition3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient transferred to a non-institutional hospice</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2420DischargeDisposition", "04", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("04"), new { @id = Model.TypeName + "_M2420DischargeDisposition4" })%>
                        <label for="<%= Model.TypeName %>_M2420DischargeDisposition4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unknown because patient moved to a geographic location not served by this agency</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2420DischargeDisposition", "UK", data.AnswerOrEmptyString("M2420DischargeDisposition").Equals("UK"), new { @id = Model.TypeName + "_M2420DischargeDispositionUK" })%>
                        <label for="<%= Model.TypeName %>_M2420DischargeDispositionUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Other unknown</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M2420')">?</div>
                </div>
            </div>
            <%  } %>
	        <%  if (Model.AssessmentTypeNum.ToInteger() == 6 || Model.AssessmentTypeNum.ToInteger() == 7) { %>
            <div class="row" id="<%= Model.TypeName %>_M2430">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2430')">(M2430)</a>
                    Reason for Hospitalization: For what reason(s) did the patient require hospitalization?
                    <em>(Mark all that apply)</em>
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationMed", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationMedHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationMed' name='{0}_M2430ReasonForHospitalizationMed' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationMed").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationMed">
                            <span class="float-left">1 &#8211;</span>
                            <span class="margin normal">Improper medication administration, medication side effects, toxicity, anaphylaxis</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationFall", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationFallHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationFall' name='{0}_M2430ReasonForHospitalizationFall' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationFall").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationFall">
                            <span class="float-left">2 &#8211;</span>
                            <span class="margin normal">Injury caused by fall</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationInfection", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationInfectionHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationInfection' name='{0}_M2430ReasonForHospitalizationInfection' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationInfection").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationInfection">
                            <span class="float-left">3 &#8211;</span>
                            <span class="margin normal">Respiratory infection (e.g., pneumonia, bronchitis)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationOtherRP", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationOtherRPHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationOtherRP' name='{0}_M2430ReasonForHospitalizationOtherRP' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationOtherRP").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationOtherRP">
                            <span class="float-left">4 &#8211;</span>
                            <span class="margin normal">Other respiratory problem</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationHeartFail", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationHeartFailHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationHeartFail' name='{0}_M2430ReasonForHospitalizationHeartFail' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartFail").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationHeartFail">
                            <span class="float-left">5 &#8211;</span>
                            <span class="margin normal">Heart failure (e.g., fluid overload)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationCardiac", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationCardiacHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationCardiac' name='{0}_M2430ReasonForHospitalizationCardiac' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationCardiac").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationCardiac">
                            <span class="float-left">6 &#8211;</span>
                            <span class="margin normal">Cardiac dysrhythmia (irregular heartbeat)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationMyocardial", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationMyocardialHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationMyocardial' name='{0}_M2430ReasonForHospitalizationMyocardial' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationMyocardial").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationMyocardial">
                            <span class="float-left">7 &#8211;</span>
                            <span class="margin normal">Myocardial infarction or chest pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationHeartDisease", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationHeartDiseaseHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationHeartDisease' name='{0}_M2430ReasonForHospitalizationHeartDisease' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationHeartDisease").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationHeartDisease">
                            <span class="float-left">8 &#8211;</span>
                            <span class="margin normal">Other heart disease</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationStroke", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationStrokeHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationStroke' name='{0}_M2430ReasonForHospitalizationStroke' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationStroke").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationStroke">
                            <span class="float-left">9 &#8211;</span>
                            <span class="margin normal">Stroke (CVA) or TIA</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationHypo", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationHypoHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationHypo' name='{0}_M2430ReasonForHospitalizationHypo' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationHypo").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationHypo">
                            <span class="float-left">10 &#8211;</span>
                            <span class="margin normal">Hypo/Hyperglycemia, diabetes out of control</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationGI", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationGIHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationGI' name='{0}_M2430ReasonForHospitalizationGI' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationGI").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationGI">
                            <span class="float-left">11 &#8211;</span>
                            <span class="margin normal">GI bleeding, obstruction, constipation, impaction</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationDehMal", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationDehMalHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationDehMal' name='{0}_M2430ReasonForHospitalizationDehMal' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationDehMal").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationDehMal">
                            <span class="float-left">12 &#8211;</span>
                            <span class="margin normal">Dehydration, malnutrition</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationUrinaryInf", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationUrinaryInfHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationUrinaryInf' name='{0}_M2430ReasonForHospitalizationUrinaryInf' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationUrinaryInf").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationUrinaryInf">
                            <span class="float-left">13 &#8211;</span>
                            <span class="margin normal">Urinary tract infection</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationIV", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationIVHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationIV' name='{0}_M2430ReasonForHospitalizationIV' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationIV").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationIV">
                            <span class="float-left">14 &#8211;</span>
                            <span class="margin normal">IV catheter-related infection or complication</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationWoundInf", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationWoundInfHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationWoundInf' name='{0}_M2430ReasonForHospitalizationWoundInf' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationWoundInf").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationWoundInf">
                            <span class="float-left">15 &#8211;</span>
                            <span class="margin normal">Wound infection or deterioration</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationUncontrolledPain", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationUncontrolledPainHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationUncontrolledPain' name='{0}_M2430ReasonForHospitalizationUncontrolledPain' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationUncontrolledPain").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationUncontrolledPain">
                            <span class="float-left">16 &#8211;</span>
                            <span class="margin normal">Uncontrolled pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationMental", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationMentalHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationMental' name='{0}_M2430ReasonForHospitalizationMental' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationMental").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationMental">
                            <span class="float-left">17 &#8211;</span>
                            <span class="margin normal">Acute mental/behavioral health problem</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationDVT", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationDVTHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationDVT' name='{0}_M2430ReasonForHospitalizationDVT' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationDVT").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationDVT">
                            <span class="float-left">18 &#8211;</span>
                            <span class="margin normal">Deep vein thrombosis, pulmonary embolus</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationScheduled", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationScheduledHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationScheduled' name='{0}_M2430ReasonForHospitalizationScheduled' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationScheduled").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationScheduled">
                            <span class="float-left">19 &#8211;</span>
                            <span class="margin normal">Scheduled treatment or procedure</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationOther", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationOtherHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationOther' name='{0}_M2430ReasonForHospitalizationOther' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationOther").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationOther">
                            <span class="float-left">20 &#8211;</span>
                            <span class="margin normal">Other than above reasons</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2430ReasonForHospitalizationUK", "", new { @id = Model.TypeName + "_M2430ReasonForHospitalizationUKHidden" })%>
                        <%= string.Format("<input id='{0}_M2430ReasonForHospitalizationUK' name='{0}_M2430ReasonForHospitalizationUK' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2430ReasonForHospitalizationUK").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2430ReasonForHospitalizationUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="margin normal">Reason unknown</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M2430');">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2440">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2440')">(M2440)</a>
                    For what Reason(s) was the patient Admitted to a Nursing Home?
                    <em>(Mark all that apply)</em>
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedTherapy", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedTherapyHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedTherapy' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedTherapy' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedTherapy").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedTherapy">
                            <span class="float-left">1 &#8211;</span>
                            <span class="margin normal">Therapy services</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedRespite", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedRespiteHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedRespite' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedRespite' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedRespite").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedRespite">
                            <span class="float-left">2 &#8211;</span>
                            <span class="margin normal">Respite care</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedHospice", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedHospiceHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedHospice' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedHospice' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedHospice").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedHospice">
                            <span class="float-left">3 &#8211;</span>
                            <span class="margin normal">Hospice care</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedPermanent", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedPermanentHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedPermanent' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedPermanent' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedPermanent").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedPermanent">
                            <span class="float-left">4 &#8211;</span>
                            <span class="margin normal">Permanent placement</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedUnsafe", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedUnsafeHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedUnsafe' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedUnsafe' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnsafe").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedUnsafe">
                            <span class="float-left">5 &#8211;</span>
                            <span class="margin normal">Unsafe for care at home</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedOther", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedOtherHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedOther' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedOther' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedOther").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedOther">
                            <span class="float-left">6 &#8211;</span>
                            <span class="margin normal">Other</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2440ReasonPatientAdmittedUnknown", "", new { @id = Model.TypeName + "_M2440ReasonPatientAdmittedUnknownHidden" })%>
                        <%= string.Format("<input id='{0}_M2440ReasonPatientAdmittedUnknown' class='radio float-left' name='{0}_M2440ReasonPatientAdmittedUnknown' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2440ReasonPatientAdmittedUnknown").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M2440ReasonPatientAdmittedUnknown">
                            <span class="float-left">UK &#8211;</span>
                            <span class="margin normal">Unknown</span>
                        </label>
                    </div>
                </div>
                <div class="fr oasis">
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M2440')">?</div>
                </div>
            </div>
	        <%  } %>
        </div>
        <div class="clear"></div>
        <%  } %>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0903">
                <label for="<%= Model.TypeName %>_M0903LastHomeVisitDate" class="float-left">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M0903')">(M0903)</a>
                    Date of Last (Most Recent) Home Visit:
                </label>
                <div class="fr">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0903LastHomeVisitDate" value="<%= data.AnswerOrEmptyString("M0903LastHomeVisitDate") %>" id="<%= Model.TypeName %>_M0903LastHomeVisitDate" />
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M0903')">?</div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0906">
                <label for="" class="float-left">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M0906')">(M0906)</a>
                    Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.
                </label>
                <div class="fr">
                    <input type="text" class="date-picker" name="<%= Model.TypeName %>_M0906DischargeDate" value="<%= data.AnswerOrEmptyString("M0906DischargeDate") %>" id="<%= Model.TypeName %>_M0906DischargeDate" />
                    <div class="oasis-tip" onclick="Oasis.ToolTip('M0906')">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNarrativeTemplate" class="strong">Narrative</label>
                <%= Html.Templates(Model.TypeName + "_GenericDischargeNarrativeTemplate", new { @class = "Templates", @template = "#" + Model.TypeName + "_GenericDischargeNarrative" })%>
                <%= Html.TextArea(Model.TypeName + "_GenericDischargeNarrative", data.AnswerOrEmptyString("GenericDischargeNarrative"), 5, 70, new { @id = Model.TypeName + "_GenericDischargeNarrative", @title = "(Optional) Narrative" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Save &#38; Check for Errors</a></li>
        <%  } else { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));Oasis.NonOasisSignature('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')">Save &#38; Complete</a></li>
        <%  } %>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
<%  } %>