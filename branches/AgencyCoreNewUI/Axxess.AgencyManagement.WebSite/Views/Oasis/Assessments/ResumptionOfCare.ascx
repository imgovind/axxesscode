﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">OASIS-C Resumption of Care | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><a href="#<%= Model.TypeName %>_Demographics" tooltip="M0010 &#8211; M0150">Clinical Record Items</a></li>
        <li><a href="#<%= Model.TypeName %>_PatientHistory" tooltip="M1000 &#8211; M1030">Patient History &#38; Diagnoses</a></li>
        <li><a href="#<%= Model.TypeName %>_RiskAssessment" tooltip="M1032 &#8211; M1036">Risk Assessment</a></li>
        <li><a href="#<%= Model.TypeName %>_Prognosis">Prognosis</a></li>
        <li><a href="#<%= Model.TypeName %>_SupportiveAssistance" tooltip="M1100">Supportive Assistance</a></li>
        <li><a href="#<%= Model.TypeName %>_Sensory" tooltip="M1200 &#8211; M1230">Sensory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Pain" tooltip="M1240 &#8211; M1242">Pain</a></li>
        <li><a href="#<%= Model.TypeName %>_Integumentary" tooltip="M1300 &#8211; M1350">Integumentary Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Respiratory" tooltip="M1400 &#8211; M1410">Respiratory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Endocrine">Endocrine</a></li>
        <li><a href="#<%= Model.TypeName %>_Cardiac">Cardiac Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Elimination" tooltip="M1600 &#8211; M1630">Elimination Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Nutrition">Nutrition</a></li>
        <li><a href="#<%= Model.TypeName %>_NeuroBehavioral" tooltip="M1700 &#8211; M1750">Neuro/Behaviourial Status</a></li>
        <li><a href="#<%= Model.TypeName %>_AdlIadl" tooltip="M1800 &#8211; M1910">ADL/IADLs</a></li>
        <li><a href="#<%= Model.TypeName %>_SuppliesDme">Supplies Worksheet</a></li>
        <li><a href="#<%= Model.TypeName %>_Medications" tooltip="M2000 &#8211; M2040">Medications</a></li>
        <li><a href="#<%= Model.TypeName %>_CareManagement" tooltip="M2100 &#8211; M2110">Care Management</a></li>
        <li><a href="#<%= Model.TypeName %>_TherapyNeed" tooltip="M2200 &#8211; M2250">Therapy Need &#38; Plan Of Care</a></li>
        <li><a href="#<%= Model.TypeName %>_OrdersDiscipline">Orders for Discipline and Treatment</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_PatientHistory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_RiskAssessment" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Prognosis" class="general loading"></div>
    <div id="<%= Model.TypeName %>_SupportiveAssistance" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Sensory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Pain" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Integumentary" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Respiratory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Endocrine" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Cardiac" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Elimination" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Nutrition" class="general loading"></div>
    <div id="<%= Model.TypeName %>_NeuroBehavioral" class="general loading"></div>
    <div id="<%= Model.TypeName %>_AdlIadl" class="general loading"></div>
    <div id="<%= Model.TypeName %>_SuppliesDme" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Medications" class="general loading"></div>
    <div id="<%= Model.TypeName %>_CareManagement" class="general loading"></div>
    <div id="<%= Model.TypeName %>_TherapyNeed" class="general loading"></div>
    <div id="<%= Model.TypeName %>_OrdersDiscipline" class="general loading"></div>
</div>