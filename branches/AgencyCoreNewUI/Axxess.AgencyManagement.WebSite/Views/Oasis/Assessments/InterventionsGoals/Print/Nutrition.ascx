<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var NutritionInterventions = data.AnswerArray("485NutritionInterventions"); %>
<%  var NutritionGoals = data.AnswerArray("485NutritionGoals"); %>
<%  if (NutritionInterventions.Length > 0 || (data.ContainsKey("485NutritionComments") && data["485NutritionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (NutritionInterventions.Contains("1")) { %>
    printview.span("SN to instruct <%= data.AnswerOrDefault("485InstructOnDietPerson", "<span class='blank'></span>").Clean() %> on <%= data.AnswerOrDefault("485InstructOnDietDesc", "<span class='blank'></span>").Clean() %> diet.") +
    <%  } %>
    <%  if (NutritionInterventions.Contains("2")) { %>
    printview.span("SN to assess patient for diet compliance.") +
    <%  } %>
    <%  if (NutritionInterventions.Contains("3")) { %>
    printview.span("SN to instruct on proper technique for tube feeding, aspiration precautions and care of feeding tube site.") +
    <%  } %>
    <%  if (NutritionInterventions.Contains("4")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructEnteralNutritionPerson", "<span class='blank'></span>").Clean() %> on enteral nutrition and the care/use of equipment.") +
    <%  } %>
    <%  if (NutritionInterventions.Contains("5")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructCareOfTubePerson", "<span class='blank'></span>").Clean() %> on proper care of <%= data.AnswerOrDefault("485InstructCareOfTubeDesc", "<span class='blank'></span>").Clean() %> tube.") +
    <%  } %>
    <%  if (NutritionInterventions.Contains("6")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructFreeWaterPerson", "<span class='blank'></span>").Clean() %> to give <%= data.AnswerOrDefault("485InstructFreeWaterAmount", "<span class='blank'></span>").Clean() %>cc of free water every <%= data.AnswerOrDefault("485InstructFreeWaterEvery", "<span class='blank'></span>").Clean() %>.") +
    <%  } %>
    <%  if (data.ContainsKey("485NutritionComments") && data["485NutritionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485NutritionComments").Clean()%>") +
    <%  } %>
    "","Nutrition Interventions");
<%  } %>
<%  if (NutritionGoals.Length > 0 || (data.ContainsKey("485NutritionGoalComments") && data["485NutritionGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (NutritionGoals.Contains("1")) { %>
    printview.span("Patient will maintain <%= data.AnswerOrDefault("485MaintainDietComplianceType", "<span class='blank'></span>").Clean() %> diet compliance during the episode.") +
    <%  } %>
    <%  if (NutritionGoals.Contains("2")) { %>
    printview.span("The <%= data.AnswerOrDefault("", "<span class='blank'></span>").Clean() %> will demonstrate proper care/use of enteral nutrition equipment by <%= data.AnswerOrDefault("485DemonstrateEnteralNutritionDate", "<span class='blank'></span>").Clean() %>.") +
    <%  } %>
    <%  if (data.ContainsKey("485NutritionGoalComments") && data["485NutritionGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485NutritionGoalComments").Clean()%>") +
    <%  } %>
    "","Nutrition Goals");
<%  } %>