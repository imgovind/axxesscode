<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var IntegumentaryInterventions = data.AnswerArray("485IntegumentaryInterventions"); %>
<%  var IntegumentaryGoals = data.AnswerArray("485IntegumentaryGoals"); %>
<%  if (IntegumentaryInterventions.Length > 0 || (data.ContainsKey("485IntegumentaryInterventionComments") && data["485IntegumentaryInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryInterventions.Contains("1")) { %>
    printview.span("SN to instruct <%= data.AnswerOrDefault("485InstructTurningRepositionPerson", "Patient/Caregiver") %> on turning/repositioning every 2 hours.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("2")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructReduceFrictionPerson", "Patient/Caregiver") %> on methods to reduce friction and shear.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("3")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructPadBonyProminencesPerson", "Patient/Caregiver") %> to pad all bony prominences.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("4")) { %>
    printview.span("SN to perform/instruct on wound care as follows:<%= data.AnswerOrEmptyString("485InstructWoundCarePersonFrequen") %>") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("5")) { %>
    printview.span("SN to assess skin for breakdown every visit.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("6")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructSignsWoundInfectionPerson", "Patient/Caregiver") %> on signs/symptoms of wound infection to report to physician, to include increased temp &#62; 100.5, chills, increase in drainage, foul odor, redness, pain and any other significant changes.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("7")) { %>
    printview.span("May discontinue wound care when wound(s) have healed.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("8")) { %>
    printview.span("SN to assess wound for S&#38;S of infection, healing status, wound deterioration, and complications.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("9")) { %>
    printview.span("SN to perform/instruct on incision/suture site care &#38; dressing.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("10")) { %>
    printview.span("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryTurningRepositionTherapyPerson", "Patient/Caregiver") %> on turning/repositioning every 2 hours.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("11")) { %>
    printview.span("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryFloatHeelsPerson", "Patient/Caregiver") %> to float heels.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("12")) { %>
    printview.span("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryReduceFrictionSheerPerson", "Patient/Caregiver") %> on methods to reduce friction and shear.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("13")) { %>
    printview.span("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryMoistureBarrierPerson", "Patient/Caregiver") %> on proper use of moisture barrier <%= data.AnswerOrDefault("485InstructMoistureBarrier", "<span class='blank'></span>") %>.") +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("14")) { %>
    printview.span("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryPadBonyPerson", "Patient/Caregiver") %> to pad all bony prominences.") +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryInterventionComments") && data["485IntegumentaryInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryInterventionComments").Clean()%>") +
    <%  } %>
    "","Integumentary Interventions");
<%  } %>
<%  if (IntegumentaryGoals.Length > 0 || (data.ContainsKey("485IntegumentaryGoalComments") && data["485IntegumentaryGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryGoals.Contains("1")) { %>
    printview.span("Wound(s) will heal without complication by the end of the episode.") +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("2")) { %>
    printview.span("Wound(s) will be free from signs and symptoms of infection during 60 day episode.") +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("3")) { %>
    printview.span("Wound(s) will decrease in size by <%= data.AnswerOrDefault("485WoundSizeDecreasePercent", "<span class='short blank'></span>") %>% by the end of the episode. ") +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("4")) { %>
    printview.span("Patient skin integrity will remain intact during this episode.") +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("5")) { %>
    printview.span("<%= data.AnswerOrDefault("485DemonstrateSterileDressingTechniquePerson", "Patient/Caregiver")%> will demonstrate understanding of changing <%= data.AnswerOrDefault("485DemonstrateSterileDressingTechniqueType", "<span class='blank'></span>")%> dressing using sterile technique.") +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryGoalComments") && data["485IntegumentaryGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryGoalComments").Clean()%>") +
    <%  } %>
    "","Integumentary Goals");
<%  } %>