<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var PainInterventions = data.AnswerArray("485PainInterventions"); %>
<%  var PainGoals = data.AnswerArray("485PainGoals"); %>
<%  if (PainInterventions.Length > 0 || (data.ContainsKey("485PainInterventionComments") && data["485PainInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (PainInterventions.Contains("1")) { %>
    printview.span("SN to assess pain level and effectiveness of pain medications and current pain management therapy every visit.") +
    <%  } %>
    <%  if (PainInterventions.Contains("2")) { %>
    printview.span("SN to instruct patient to take pain medication before pain becomes severe to achieve better pain control.") +
    <%  } %>
    <%  if (PainInterventions.Contains("3")) { %>
    printview.span("SN to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.") +
    <%  } %>
    <%  if (PainInterventions.Contains("4")) { %>
    printview.span("SN to report to physician if patient experiences pain level not acceptable to patient, pain level greater than <%= data.AnswerOrDefault("485PainTooGreatLevel", "<span class='short blank'></span>") %>, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.") +
    <%  } %>
    <%  if (PainInterventions.Contains("5")) { %>
    printview.span("Therapist to assess pain level and effectiveness of pain medications and current pain management therapy every visit.") +
    <%  } %>
    <%  if (PainInterventions.Contains("6")) { %>
    printview.span("Therapist to instruct patient to take pain medication before pain becomes severe to achieve better pain control.") +
    <%  } %>
    <%  if (PainInterventions.Contains("7")) { %>
    printview.span("Therapist to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.") +
    <%  } %>
    <%  if (PainInterventions.Contains("8")) { %>
    printview.span("Therapist to assess patient&#8217;s willingness to take pain medications and/or barriers to compliance, e.g., patient is unable to tolerate side effects such as drowsiness, dizziness, constipation.") +
    <%  } %>
    <%  if (PainInterventions.Contains("9")) { %>
    printview.span("Therapist to report to physician if patient experiences pain level not acceptable to patient, pain level greater than <%= data.AnswerOrDefault("485PainInterventionsPainLevelGreaterThan", "<span class='short blank'></span>")%>, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.") +
    <%  } %>
    <%  if (data.ContainsKey("485PainInterventionComments") && data["485PainInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485PainInterventionComments").Clean()%>") +
    <%  } %>
    "","Pain Interventions");
<%  } %>
<%  if (PainGoals.Length > 0 || (data.ContainsKey("485PainGoalComments") && data["485PainGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (PainGoals.Contains("1")) { %>
    printview.span("Patient will verbalize understanding of proper use of pain medication by the end of the episode.") +
    <%  } %>
    <%  if (PainGoals.Contains("2")) { %>
    printview.span("PT/CG will verbalize knowledge of pain medication regimen and pain relief measures by the end of the episode.") +
    <%  } %>
    <%  if (PainGoals.Contains("3")) { %>
    printview.span("Patient will have absence or control of pain as evidenced by optimal mobility and activity necessary for functioning and performing ADLs by the end of the episode.") +
    <%  } %>
    <%  if (PainGoals.Contains("4")) { %>
    printview.span("Patient will verbalize understanding of proper use of pain medication by <%= data.AnswerOrDefault("485PainGoalsMedication", "<span class='short blank'></span>")%>.") +
    <%  } %>
    <%  if (PainGoals.Contains("5")) { %>
    printview.span("Patient will achieve pain level less than <%= data.AnswerOrDefault("485PainGoalsPainLevelLessThan", "<span class='short blank'></span>")%> within <%= data.AnswerOrDefault("485PainGoalsWithinWeeks", "<span class='short blank'></span>")%> weeks.") +
    <%  } %>
    <%  if (data.ContainsKey("485PainGoalComments") && data["485PainGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485PainGoalComments").Clean()%>") +
    <%  } %>
    "","Pain Goals");
<%  } %>