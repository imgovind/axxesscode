<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var SupportiveAssistanceInterventions = data.AnswerArray("485SupportiveAssistanceInterventions"); %>
<%  if (SupportiveAssistanceInterventions.Length > 0 || (data.ContainsKey("485SupportiveAssistanceComments") && data["485SupportiveAssistanceComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (SupportiveAssistanceInterventions.Contains("1")) { %>
    printview.span("Physical therapy to evaluate.") +
    <%  } %>
    <%  if (data.ContainsKey("485SupportiveAssistanceComments") && data["485SupportiveAssistanceComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485SupportiveAssistanceComments").Clean()%>") +
    <%  } %>
    "","Supportive Assistance Interventions");
<%  } %>
<%  if (data.ContainsKey("485SupportiveAssistanceGoalsComments") && data["485SupportiveAssistanceGoalsComments"].Answer.IsNotNullOrEmpty()) { %>
printview.addsection(
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485SupportiveAssistanceGoalsComments").Clean()%>"),"Supportive Assistance Goals");
<%  } %>
