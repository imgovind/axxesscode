<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<%  string[] painInterventions = data.AnswerArray("485PainInterventions"); %>
<%  string[] painGoals = data.AnswerArray("485PainGoals"); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485PainInterventions", "", new { @id = Model.TypeName + "_485PainInterventionsHidden" })%>
    <div class="wide column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions1' name='{0}_485PainInterventions' value='1' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions1">SN to assess pain level and effectiveness of pain medications and current pain management therapy every visit.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions2' name='{0}_485PainInterventions' value='2' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions2">SN to instruct patient to take pain medication before pain becomes severe to achieve better pain control.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions3' name='{0}_485PainInterventions' value='3' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions3">SN to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions4' name='{0}_485PainInterventions' value='4' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("4").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485PainInterventions4">SN to report to physician if patient experiences pain level not acceptable to patient, pain level greater than </label>
                        <%= Html.TextBox(Model.TypeName + "_485PainTooGreatLevel", data.AnswerOrEmptyString("485PainTooGreatLevel"), new { @id = Model.TypeName + "_485PainTooGreatLevel", @class = "zip", @maxlength = "10" }) %>
                        <label for="<%= Model.TypeName %>_485PainInterventions5">, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.</label>
                    </span>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions5' name='{0}_485PainInterventions' value='5' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("5").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions5">Therapist to assess pain level and effectiveness of pain medications and current pain management therapy every visit.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions6' name='{0}_485PainInterventions' value='6' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("6").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions6">Therapist to instruct patient to take pain medication before pain becomes severe to achieve better pain control.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions7' name='{0}_485PainInterventions' value='7' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("7").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions7">Therapist to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions8' name='{0}_485PainInterventions' value='8' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("8").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainInterventions8">Therapist to assess patient&#8217;s willingness to take pain medications and/or barriers to compliance, e.g., patient is unable to tolerate side effects such as drowsiness, dizziness, constipation.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485PainInterventions9' name='{0}_485PainInterventions' value='9' type='checkbox' {1} />", Model.TypeName, painInterventions.Contains("9").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485PainInterventions9">Therapist to report to physician if patient experiences pain level not acceptable to patient, pain level greater than</label>
                        <%= Html.TextBox(Model.TypeName + "_485PainInterventionsPainLevelGreaterThan", data.AnswerOrEmptyString("485PainInterventionsPainLevelGreaterThan"), new { @id = Model.TypeName + "_485PainInterventionsPainLevelGreaterThan", @class = "zip" })%>
                        <label for="<%= Model.TypeName %>_485PainInterventions9">, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&#8217;s normal activities.</label>
                    </span>
                </div>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485PainInterventionComments" class="strong">Additional Orders</label>
            <%= Html.Templates(Model.TypeName + "_485PainOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485PainInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485PainInterventionComments", data.AnswerOrEmptyString("485PainInterventionComments"), 5, 70, new { @id = Model.TypeName + "_485PainInterventionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485PainGoals", "", new { @id = Model.TypeName + "_485PainGoalsHidden" })%>
    <div class="wide column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals1' name='{0}_485PainGoals' value='1' type='checkbox' {1} />", Model.TypeName,  painGoals.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainGoals1">Patient will verbalize understanding of proper use of pain medication by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals2' name='{0}_485PainGoals' value='2' type='checkbox' {1} />", Model.TypeName, painGoals.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainGoals2">PT/CG will verbalize knowledge of pain medication regimen and pain relief measures by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals3' name='{0}_485PainGoals' value='3' type='checkbox' {1} />", Model.TypeName, painGoals.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485PainGoals3">Patient will have absence or control of pain as evidenced by optimal mobility and activity necessary for functioning and performing ADLs by the end of the episode.</label>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals4' name='{0}_485PainGoals' value='4' type='checkbox' {1} />", Model.TypeName, painGoals.Contains("4").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485PainGoals4">Patient will verbalize understanding of proper use of pain medication by</label>
                        <%= Html.TextBox(Model.TypeName + "_485PainGoalsMedication", data.AnswerOrEmptyString("485PainGoalsMedication"), new { @id = Model.TypeName + "_485PainGoalsMedication", @class = "zip" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485PainGoals5' name='{0}_485PainGoals' value='5' type='checkbox' {1} />", Model.TypeName, painGoals.Contains("5").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485PainGoals5">Patient will achieve pain level less than</label>
                        <%= Html.TextBox(Model.TypeName + "_485PainGoalsPainLevelLessThan", data.AnswerOrEmptyString("485PainGoalsPainLevelLessThan"), new { @id = Model.TypeName + "_485PainGoalsPainLevelLessThan", @class = "zip" })%>
                        <label for="<%= Model.TypeName %>_485PainGoals5">within</label>
                        <%= Html.TextBox(Model.TypeName + "_485PainGoalsWithinWeeks", data.AnswerOrEmptyString("485PainGoalsWithinWeeks"), new { @id = Model.TypeName + "_485PainGoalsWithinWeeks", @class = "zip" })%>
                        <label for="<%= Model.TypeName %>_485PainGoals5">weeks.</label>
                    </span>
                </div>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485PainGoalComments" class="strong">Additional Goals</label>
            <%= Html.Templates(Model.TypeName + "_485PainGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485PainGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485PainGoalComments", data.AnswerOrEmptyString("485PainGoalComments"), 5, 70, new { @id = Model.TypeName + "_485PainGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.Interventions($(".interventions"));
</script>