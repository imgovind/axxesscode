<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<% string[] respiratoryInterventions = data.ContainsKey("485RespiratoryInterventions") && data["485RespiratoryInterventions"].Answer != "" ? data["485RespiratoryInterventions"].Answer.Split(',') : new string[]{}; %>
<% string[] respiratoryGoals = data.ContainsKey("485RespiratoryGoals") && data["485RespiratoryGoals"].Answer != "" ? data["485RespiratoryGoals"].Answer.Split(',') : new string[]{}; %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485RespiratoryInterventions", "", new { @id = Model.TypeName + "_485RespiratoryInterventionsHidden" })%>
    <div class="wide column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions1' name='{0}_485RespiratoryInterventions' value='1' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("1").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions1">SN to instruct the</label>
                        <%  var instructNebulizerUsePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485InstructNebulizerUsePerson", "Patient/Caregiver")); %>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructNebulizerUsePerson", instructNebulizerUsePerson) %>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions1">on proper use of nebulizer/inhaler, and assess return demonstration.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions2' class='radio float-left' name='{0}_485RespiratoryInterventions' value='2' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("2").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions2">SN to assess O<sub>2</sub> saturation on room air (freq)</label>
                        <%= Html.TextBox(Model.TypeName + "_485AssessOxySaturationFrequency", data.AnswerOrEmptyString("485AssessOxySaturationFrequency"), new { @id = Model.TypeName + "_485AssessOxySaturationFrequency", @class = "st", @maxlength = "15" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions3' class='radio float-left' name='{0}_485RespiratoryInterventions' value='3' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("3").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions3">SN to assess O<sub>2</sub> saturation on O<sub>2</sub> @</label>
                        <%= Html.TextBox(Model.TypeName + "_485AssessOxySatOnOxyAt", data.AnswerOrEmptyString("485AssessOxySatOnOxyAt"), new { @id = Model.TypeName + "_485AssessOxySatOnOxyAt", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions3">LPM/</label>
                        <%= Html.TextBox(Model.TypeName + "_485AssessOxySatLPM", data.AnswerOrEmptyString("485AssessOxySatLPM"), new { @id = Model.TypeName + "_485AssessOxySatLPM", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions3">(freq)</label>
                        <%= Html.TextBox(Model.TypeName + "_485AssessOxySatOnOxyFrequency", data.AnswerOrEmptyString("485AssessOxySatOnOxyFrequency"), new { @id = Model.TypeName + "_485AssessOxySatOnOxyFrequency", @class = "st", @maxlength = "15" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions4' class='radio float-left' name='{0}_485RespiratoryInterventions' value='4' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("4").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions4">SN to instruct the</label>
                        <%  var instructSobFactorsPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485InstructSobFactorsPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructSobFactorsPerson", instructSobFactorsPerson)%>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions4">on factors that contribute to SOB.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions5' class='radio float-left' name='{0}_485RespiratoryInterventions' value='5' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("5").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions5">SN to instruct the</label>
                        <%  var instructAvoidSmokingPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485InstructAvoidSmokingPerson","Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructAvoidSmokingPerson", instructAvoidSmokingPerson)%>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions5">to avoid smoking or allowing people to smoke in patient&#8217;s home. Instruct patient to avoid irritants/allergens known to increase SOB.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions6' class='radio float-left' name='{0}_485RespiratoryInterventions' value='6' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("6").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryInterventions6">SN to instruct patient on energy conserving measures including frequent rest periods, small frequent meals, avoiding large meals/overeating, controlling stress.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions7' class='radio float-left' name='{0}_485RespiratoryInterventions' value='7' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("7").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryInterventions7">SN to instruct caregiver on proper suctioning technique.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions8' class='radio float-left' name='{0}_485RespiratoryInterventions' value='8' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("8").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions8">SN to instruct the </label>
                        <%  var recognizePulmonaryDysfunctionPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485RecognizePulmonaryDysfunctionPerson","Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485RecognizePulmonaryDysfunctionPerson", recognizePulmonaryDysfunctionPerson)%>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions8">on methods to recognize pulmonary dysfunction and relieve complications.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions9' class='radio float-left' name='{0}_485RespiratoryInterventions' value='9' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("9").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions9">Report to physician O<sub>2</sub> saturation less than</label>
                        <%= Html.TextBox(Model.TypeName + "_485OxySaturationLessThanPercent", data.AnswerOrEmptyString("485OxySaturationLessThanPercent"), new { @id = Model.TypeName + "_485OxySaturationLessThanPercent", @class = "st", @maxlength = "15" })%>
                        <label for="<%= Model.TypeName %>_485RespiratoryInterventions9">%.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions10' class='radio float-left' name='{0}_485RespiratoryInterventions' value='10' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("10").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryInterventions10">SN to assess/instruct on signs &#38; symptoms of pulmonary complications.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions11' class='radio float-left' name='{0}_485RespiratoryInterventions' value='11' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("11").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryInterventions11">SN to instruct on all aspects of trach care and equipment. SN to instruct PT/CG on emergency procedures for complications and dislodgment of tube.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485RespiratoryInterventions12' class='radio float-left' name='{0}_485RespiratoryInterventions' value='12' type='checkbox' {1} />", Model.TypeName,  respiratoryInterventions.Contains("12").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryInterventions12">SN to perform trach care using sterile technique (i.e. suctioning, dressing change).</label>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RespiratoryInterventionComments" class="strong">Additional Orders</label>
            <%= Html.Templates(Model.TypeName + "_485RespiratoryOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485RespiratoryInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485RespiratoryInterventionComments", data.ContainsKey("485RespiratoryInterventionComments") ? data["485RespiratoryInterventionComments"].Answer : "", 2, 70, new { @id = Model.TypeName + "_485RespiratoryInterventionComments", @title = "(485 Locator 21) Orders" }) %>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%= Html.Hidden(Model.TypeName + "_485RespiratoryGoals", "", new { @id = Model.TypeName + "_485RespiratoryGoalsHidden" })%>
    <div class="wide column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals1' name='{0}_485RespiratoryGoals' value='1' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryGoals1">Respiratory status will improve with reduced shortness of breath and improved lung sounds by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals2' name='{0}_485RespiratoryGoals' value='2' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryGoals2">PT/CG will verbalize and demonstrate correct use and care of oxygen and equipment by the end of the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals3' name='{0}_485RespiratoryGoals' value='3' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485RespiratoryGoals3">Patient will be free from signs and symptoms of respiratory distress during the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals4' name='{0}_485RespiratoryGoals' value='4' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("4").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryGoals4">Patient and caregiver will verbalize an understanding of factors that contribute to shortness of breath by</label>
                        <%= Html.TextBox(Model.TypeName + "_485VerbalizeFactorsSobDate", data.AnswerOrEmptyString("485VerbalizeFactorsSobDate"), new { @id = Model.TypeName + "_485VerbalizeFactorsSobDate", @class = "st", @maxlength = "10" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals5' name='{0}_485RespiratoryGoals' value='5' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("5").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryGoals5">Patient will verbalize an understanding of energy conserving measures by</label>
                        <%= Html.TextBox(Model.TypeName + "_485VerbalizeEnergyConserveDate", data.AnswerOrEmptyString("485VerbalizeEnergyConserveDate"), new { @id = Model.TypeName + "_485VerbalizeEnergyConserveDate", @class = "st", @maxlength = "10" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals6' name='{0}_485RespiratoryGoals' value='6' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("6").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryGoals6">The</label>
                        <%  var verbalizeSafeOxyManagementPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485VerbalizeSafeOxyManagementPerson","Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485VerbalizeSafeOxyManagementPerson", verbalizeSafeOxyManagementPerson)%>
                        <label for="<%= Model.TypeName %>_485RespiratoryGoals6">will verbalize and demonstrate safe management of oxygen by</label>
                        <%= Html.TextBox(Model.TypeName + "_485VerbalizeSafeOxyManagementPersonDate", data.AnswerOrEmptyString("485VerbalizeSafeOxyManagementPersonDate"), new { @id = Model.TypeName + "_485VerbalizeSafeOxyManagementPersonDate", @class = "st", @maxlength = "10" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485RespiratoryGoals7' name='{0}_485RespiratoryGoals' value='7' type='checkbox' {1} />", Model.TypeName,  respiratoryGoals.Contains("7").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485RespiratoryGoals7">Patient will return demonstrate proper use of nebulizer treatment by </label>
                        <%= Html.TextBox(Model.TypeName + "_485DemonstrateNebulizerUseDate", data.AnswerOrEmptyString("485DemonstrateNebulizerUseDate"), new { @id = Model.TypeName + "_485DemonstrateNebulizerUseDate", @class = "st", @maxlength = "10" })%>.
                    </span>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485RespiratoryGoalComments" class="strong">Additional Goals</label>
            <%= Html.Templates(Model.TypeName + "_485RespiratoryGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485RespiratoryGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485RespiratoryGoalComments", data.ContainsKey("485RespiratoryGoalComments") ? data["485RespiratoryGoalComments"].Answer : "", 2, 70, new { @id = Model.TypeName + "_485RespiratoryGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.Interventions($(".interventions"));
    Oasis.Goals($(".goals"));
</script>