<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  if (Model.Discipline == "Nursing") { %>
<%  var data = Model.ToDictionary(); %>
<%  string[] therapyInterventions = data.AnswerArray("485TherapyInterventions"); %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485TherapyInterventions", "", new { @id = Model.TypeName + "_485TherapyInterventionsHidden" })%>
    <div class="wide column">
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485TherapyInterventions1' name='{0}_485TherapyInterventions' value='1' type='checkbox' {1} />", Model.TypeName, therapyInterventions.Contains("1").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485TherapyInterventions1">Physical therapist to evaluate and submit plan of treatment.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485TherapyInterventions2' name='{0}_485TherapyInterventions' value='2' type='checkbox' {1} />", Model.TypeName, therapyInterventions.Contains("2").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485TherapyInterventions2">Occupational therapist to evaluate and submit plan of treatment.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485TherapyInterventions3' name='{0}_485TherapyInterventions' value='3' type='checkbox' {1} />", Model.TypeName, therapyInterventions.Contains("3").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485TherapyInterventions3">Speech therapist to evaluate and submit plan of treatment.</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485TherapyComments">Additional Orders</label>
            <%= Html.Templates(Model.TypeName + "_485TherapyOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485TherapyComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485TherapyComments", data.AnswerOrEmptyString("485TherapyComments"), 5, 70, new { @id = Model.TypeName + "_485TherapyComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<%  } %>