<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%  string[] endocrineInterventions = data.AnswerArray("485EndocrineInterventions"); %>
    <%= Html.Hidden(Model.TypeName + "_485EndocrineInterventions", "", new { @id = Model.TypeName + "_485EndocrineInterventionsHidden" })%>
    <div class="wide column">
        <div class="row">
            <div class="wide checkgroup">
<%  if (Model.Discipline == "Nursing") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions1' name='{0}_485EndocrineInterventions' value='1' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("1").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions1">SN to assess</label>
                        <%  var abilityToManageDiabetic = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485AbilityToManageDiabetic", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485AbilityToManageDiabetic", abilityToManageDiabetic)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions1">ability to manage diabetic disease process.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions2' name='{0}_485EndocrineInterventions' value='2' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EndocrineInterventions2">SN to perform BG checks every visit &#38; PRN.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions3' name='{0}_485EndocrineInterventions' value='3' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EndocrineInterventions3">SN to instruct on insulin preparation, administration, site rotation and disposal of supplies.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions4' name='{0}_485EndocrineInterventions' value='4' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("4").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EndocrineInterventions4">SN to assess/instruct on diabetic management to include: nail, skin &#38; foot care, medication administration and proper diet.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions5' name='{0}_485EndocrineInterventions' value='5' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("5").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions5">SN to administer</label>
                        <%= Html.TextBox(Model.TypeName + "_485AmountOfInsuline", data.AnswerOrEmptyString("485AmountOfInsuline"), new { @id = Model.TypeName + "_485AmountOfInsuline", @maxlength = "30" })%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions5">insulin as follows</label>
                        <%= Html.TextBox(Model.TypeName + "_485AmountOfInsulineAsFollows", data.AnswerOrEmptyString("485AmountOfInsulineAsFollows"), new { @id = Model.TypeName + "_485AmountOfInsulineAsFollows", @maxlength = "50" })%>.
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions6' name='{0}_485EndocrineInterventions' value='6' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("6").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions6">SN to prefill syringes with</label>
                        <%= Html.TextBox(Model.TypeName + "_485PrefillOfInsuline", data.AnswerOrEmptyString("485PrefillOfInsuline"), new { @id = Model.TypeName + "_485PrefillOfInsuline", @maxlength = "30" })%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions6">insulin as follows</label>
                        <%= Html.TextBox(Model.TypeName + "_485PrefillOfInsulineAsFollows", data.AnswerOrEmptyString("485PrefillOfInsulineAsFollows"), new { @id = Model.TypeName + "_485PrefillOfInsulineAsFollows", @maxlength = "50" })%>.
                    </span>
                </div>
<%  } %>
<%  if (Model.Discipline == "PT" || Model.Discipline == "OT") { %>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions7' name='{0}_485EndocrineInterventions' value='7' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("7").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions7">Therapist to instruct</label>
                        <%  var endocrineInspectFeetPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineInspectFeetPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineInspectFeetPerson", endocrineInspectFeetPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions7">to inspect patient&#8217;s feet daily and report any skin or nail problems immediately.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions8' name='{0}_485EndocrineInterventions' value='8' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("8").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EndocrineInterventions8">SN needed for evaluation for patient due to knowledge deficit related to diabetic foot care.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions9' name='{0}_485EndocrineInterventions' value='9' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("9").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions9">Therapist to instruct</label>
                        <%  var endocrineWashFeetPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineWashFeetPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineWashFeetPerson", endocrineWashFeetPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions9">to wash patient&#8217;s feet in warm (not hot) water. Wash feet gently and pat dry thoroughly making sure to dry between toes.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions10' name='{0}_485EndocrineInterventions' value='10' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("10").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions10">Therapist to instruct</label>
                        <%  var endocrineMoisturizerPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineMoisturizerPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineMoisturizerPerson", endocrineMoisturizerPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions10">to use moisturizer daily but avoid getting between toes.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions11' name='{0}_485EndocrineInterventions' value='11' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("11").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EndocrineInterventions11">Therapist to instruct patient to wear clean, dry, properly-fitted socks and change them every day.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions12' name='{0}_485EndocrineInterventions' value='9' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("9").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions12">Therapist to instruct</label>
                        <%  var endocrineNailCarePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineNailCarePerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineNailCarePerson", endocrineNailCarePerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions12">on appropriate nail care as follows: trim nails straight across and file rough edges with nail file.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions13' name='{0}_485EndocrineInterventions' value='10' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("10").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions13">Therapist to instruct</label>
                        <%  var endocrineBarefootPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineBarefootPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineBarefootPerson", endocrineBarefootPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions13">that patient should never walk barefoot.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions14' name='{0}_485EndocrineInterventions' value='9' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("9").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions14">Therapist to instruct</label>
                        <%  var endocrineElevateFeetPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineElevateFeetPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineElevateFeetPerson", endocrineElevateFeetPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions14">that patient should elevate feet when sitting.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions14' name='{0}_485EndocrineInterventions' value='10' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("10").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions10">Therapist to instruct</label>
                        <%  var endocrineProtectFeetPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineProtectFeetPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineProtectFeetPerson", endocrineProtectFeetPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions10">to protect patient&#8217;s feet from extreme heat or cold.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485EndocrineInterventions9' name='{0}_485EndocrineInterventions' value='9' type='checkbox' {1} />", Model.TypeName, endocrineInterventions.Contains("9").ToChecked()) %>
                    <span>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions9">Therapist to instruct</label>
                        <%  var endocrineNoCutOffLesionsPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485EndocrineNoCutOffLesionsPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485EndocrineNoCutOffLesionsPerson", endocrineNoCutOffLesionsPerson)%>
                        <label for="<%= Model.TypeName %>_485EndocrineInterventions9">never to try to cut off corns, calluses, or any other lesions from lower extremities.</label>
                    </span>
                </div>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485EndocrineInterventionComments" class="strong">Additional Orders</label>
            <%= Html.Templates(Model.TypeName + "_485EndocrineOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485EndocrineInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485EndocrineInterventionComments", data.AnswerOrEmptyString("485EndocrineInterventionComments"), 2, 70, new { @id = Model.TypeName + "_485EndocrineInterventionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%  string[] endocrineGoals = data.AnswerArray("485EndocrineGoals"); %>
    <%= Html.Hidden(Model.TypeName + "_485EndocrineGoals", "", new { @id = Model.TypeName + "_485EndocrineGoalsHidden" })%>
    <div class="wide column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals1' name='{0}_485EndocrineGoals' value='1' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485EndocrineGoals1">Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals2' name='{0}_485EndocrineGoals' value='2' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("2").ToChecked()) %>
                    <span>
                        <%  var glucometerUseIndependencePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485GlucometerUseIndependencePerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485GlucometerUseIndependencePerson", glucometerUseIndependencePerson) %>
                        <label for="<%= Model.TypeName %>_485EndocrineGoals2">will be independent with glucometer use by the end of the episode.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals3' name='{0}_485EndocrineGoals' value='3' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("3").ToChecked()) %>
                    <span>
                        <%  var independentInsulinAdministrationPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485IndependentInsulinAdministrationPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485IndependentInsulinAdministrationPerson", independentInsulinAdministrationPerson) %>
                        <label for="<%= Model.TypeName %>_485EndocrineGoals3">will be independent with insulin administration by the end of the episode.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals4' name='{0}_485EndocrineGoals' value='4' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("4").ToChecked()) %>
                    <span>
                        <%  var verbalizeProperFootCarePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485VerbalizeProperFootCarePerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485VerbalizeProperFootCarePerson", verbalizeProperFootCarePerson) %>
                        <label for="<%= Model.TypeName %>_485EndocrineGoals4">will verbalize understanding of proper diabetic foot care by the end of the episode.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals5' name='{0}_485EndocrineGoals' value='5' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("5").ToChecked()) %>
                    <span>
                        <%  var diabetesManagement = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485DiabetesManagement", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485DiabetesManagement", diabetesManagement)%>
                        <label for="<%= Model.TypeName %>_485EndocrineGoals5">will verbalize knowledge of diabetes management, S&#38;S of complications, hypo/hyperglycemia, foot care and management during illness or stress by the end of the episode.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals6' name='{0}_485EndocrineGoals' value='6' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("6").ToChecked()) %>
                    <span>
                        <%  var generalDemonstration = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485GeneralDemonstration", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485GeneralDemonstration", generalDemonstration)%>
                        <label for="<%= Model.TypeName %>_485EndocrineGoals6">will demonstrate correct insulin preparation, injection procedure, storage and disposal of supplies by the end of the episode.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485EndocrineGoals7' name='{0}_485EndocrineGoals' value='7' type='checkbox' {1} />", Model.TypeName, endocrineGoals.Contains("7").ToChecked()) %>
                    <span>
                        <%  var generalVerbalization = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                            }, "Value", "Text", data.AnswerOrDefault("485GeneralVerbalization", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485GeneralVerbalization", generalVerbalization)%>
                        <label for="<%= Model.TypeName %>_485EndocrineGoals7">will verbalize understanding of the importance of keeping blood glucose levels within parameters by the end of the episode.</label>
                    </span>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485EndocrineGoalComments" class="strong">Additional Goals</label>
            <%= Html.Templates(Model.TypeName + "_485EndocrineGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485EndocrineGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485EndocrineGoalComments", data.AnswerOrEmptyString("485EndocrineGoalComments"), 2, 70, new { @id = Model.TypeName + "_485EndocrineGoalComments", @title = "(485 Locator 22) Goals" }) %>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.Interventions($(".interventions"));
    Oasis.Goals($(".goals"));
</script>