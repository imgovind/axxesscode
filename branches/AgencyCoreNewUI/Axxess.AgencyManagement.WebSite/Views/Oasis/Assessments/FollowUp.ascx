﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">OASIS-C Follow-up | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer.Clean() : string.Empty) + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer.Clean() : string.Empty) %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left oasis-container">
    <ul class="vertical-tab-list strong">
        <li><a href="#<%= Model.TypeName %>_Demographics" tooltip="M0010 &#8211; M0150">Clinical Record Items</a></li>
        <li><a href="#<%= Model.TypeName %>_PatientHistory" tooltip="M1020 &#8211; M1030">Patient History &#38; Diagnoses</a></li>
        <li><a href="#<%= Model.TypeName %>_Sensory" tooltip="M1200">Sensory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Pain" tooltip="M1242">Pain</a></li>
        <li><a href="#<%= Model.TypeName %>_Integumentary" tooltip="M1306 &#8211; M1350">Integumentary Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Respiratory" tooltip="M1400">Respiratory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Elimination" tooltip="M1610 &#8211; M1630">Elimination Status</a></li>
        <li><a href="#<%= Model.TypeName %>_AdlIadl" tooltip="M1810 &#8211; M1860">ADL/IADLs</a></li>
        <li><a href="#<%= Model.TypeName %>_Medications" tooltip="M2030">Medications</a></li>
        <li><a href="#<%= Model.TypeName %>_TherapyNeed" tooltip="M2200">Therapy Need &#38; Plan Of Care</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_PatientHistory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Sensory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Pain" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Integumentary" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Respiratory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Elimination" class="general loading"></div>
    <div id="<%= Model.TypeName %>_AdlIadl" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Medications" class="general loading"></div>
    <div id="<%= Model.TypeName %>_TherapyNeed" class="general loading"></div>
</div>