﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BillLean>" %>
<span class="wintitle"><%= Model.Type %> Claim Summary | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm(Model.IsElectronicSubmssion ? "Claim/SubmitDirectly" : "Claim/UpdateStatus", "Billing", FormMethod.Post, new { @id = "GenerateBilling" + Model.Type + "_Form" })) { %>
    <%  if (Model != null && Model.Claims != null && Model.Claims.Count > 0) { %>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="grid-number"></span>
                <span class="grid-icon"></span>
                <span class="grid-30">Patient</span>
                <span class="grid-20">Medicare No</span>
                <span class="grid-30">Episode</span>
                <span>Claim Amount</span>
            </li>
        </ul>
        <ol>
        <%  int i = 1; %>
        <%  foreach (var claim in Model.Claims) { %>
            <li>
                <span class="grid-number"><%= i.ToString() %>.</span>
                <span class="grid-icon"><%= string.Format("<input type='hidden' name='ClaimSelected' value='{0}' />", claim.Id) %></span>
                <span class="grid-30"><%= claim.DisplayName + " (" + claim.PatientIdNumber + ")" %></span>
                <span class="grid-20"><%= claim.MedicareNumber %></span>
                <span class="grid-30"><%= claim.EpisodeStartDate != null && claim.EpisodeStartDate.IsValid() ? claim.EpisodeStartDate.ToShortDateString() : string.Empty %><%= claim.EpisodeEndDate != null && claim.EpisodeEndDate.IsValid() ? "&#8211;" + claim.EpisodeEndDate.ToShortDateString() : string.Empty %></span>
                <span><%= claim.ProspectivePay %></span>
            </li>
            <%  i++; %>
        <%  } %>
        </ol>
    </div>
    <% = Html.Hidden("StatusType","Submit") %>
    <% = Html.Hidden("BranchId", Model.BranchId) %>
    <% = Html.Hidden("PrimaryInsurance", Model.Insurance) %>
    <% = Html.Hidden("Type", Model.Type) %>
    <div class="buttons">
        <ul>
        <%  if (Model.IsElectronicSubmssion) { %>
            <li><a class="save close">Submit Electronically</a></li>
        <%  } else { %>
            <li><a class="generate">Download Claim(s)</a></li>
            <li><a class="save close">Mark Claim(s) as Submitted</a></li>
        <%  } %>
        </ul>
    </div>
    <%  } %>
<%  } %>
</div>