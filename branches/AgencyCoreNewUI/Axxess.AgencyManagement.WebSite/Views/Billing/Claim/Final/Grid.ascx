﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<%  if (Model != null && Model.Count > 0) { %>
<input type="hidden" name="FinalSort" value="rap-name" />
<ul>
    <li class="ac"><h3>Finals (EOE) Claims</h3></li>
    <li>
        <span class="grid-number"></span>
        <span class="grid-icon"></span>
        <span class="grid-25 sortable">Patient Name</span>
        <span class="grid-12 sortable">MRN</span>
        <span class="grid-20 sortable">Episode Period</span>
        <span class="grid-labeled-icon sortable ac">RAP</span>
        <span class="grid-labeled-icon sortable ac">Visit</span>
        <span class="grid-labeled-icon sortable ac">Order</span>
        <span class="grid-labeled-icon sortable ac">Verified</span>
        <span class="grid-icon fr"></span>
    </li>
</ul>
<ol>
    <%  int i = 1; %>
    <%  foreach (var final in Model) { %>
    <li class="<%= final.IsRapGenerated ? "ready" : "notready" %>">
        <span class="grid-number"><%= i.ToString() %>.</span>
        <span class="grid-icon"><%= final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified && final.AreOrdersComplete && final.IsRapGenerated ? "<input name='ClaimSelected' class='radio' type='checkbox' value='" + final.Id + "' id='RapSelected" + final.Id + "' />" : string.Empty %></span>
        <span class="grid-25 strong">
        <%  if (final.IsRapGenerated) { %>
            <a class="link" onclick="Billing.Claim.Final.Load('<%= final.EpisodeId %>', '<%= final.PatientId %>');return false"><%= final.LastName%>, <%= final.FirstName%></a>
        <%  } else { %>
            <%= final.LastName%>, <%= final.FirstName%>
        <%  } %>
        </span>
        <span class="grid-12"><%= final.PatientIdNumber %></span>
        <span class="grid-20">
            <span class="sort-value"><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
            <%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %>
            &#8211;
            <%= final.EpisodeEndDate != null ? final.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= final.IsRapGenerated ? "0" : "1" %></span>
            <span class="img icon <%= final.IsRapGenerated ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= final.AreVisitsComplete ? "0" : "1"%></span>
            <span class="img icon <%= final.AreVisitsComplete ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= final.AreOrdersComplete ? "0" : "1"%></span>
            <a onclick="Patient.Orders.List('<%=final.EpisodeId %>','<%=final.PatientId %>');return false"><span class="img icon <%= final.AreOrdersComplete ? "success-small" : "error-small" %>"></span></a>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "0" : "1"%></span>
            <span class="img icon <%= final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-icon fr"><a class="fr" onclick="Billing.Claim.Final.Print('<%= final.EpisodeId %>', '<%= final.PatientId %>');return false"><span class="img icon print"></span></a></span>
    </li>
        <%  i++; %>
    <%  } %>
</ol>
<%  } else { %>
<ul>
    <li class="ac"><h3>Finals</h3></li>
</ul>
<h1 class="blue">No Finals Found</h1>
<%  } %>
