﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<span class="wintitle">Final (EOE) Claims | <%= Model.DisplayName %></span>
<div id="FinalVerification_TabStrip" class="wrapper main blue final">
    <ul>
	    <li class="ac">
	        <a href="#FinalVerification_Demographics">
	            <strong>Step 1 of 4</strong>
	            <br />
	            <span>Demographics</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#FinalVerification_Visits">
	            <strong>Step 2 of 4</strong>
	            <br />
	            <span>Verify Visits</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#FinalVerification_Supplies">
	            <strong>Step 3 of 4</strong>
	            <br />
	            <span>Verify Supplies</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#FinalVerification_Summary">
	            <strong>Step 4 of 4</strong>
	            <br />
	            <span>Summary</span>
	        </a>
	    </li>
    </ul>
    <div id="FinalVerification_Demographics" class="final tab-content"><%  Html.RenderPartial("Claim/Final/Demographics", Model); %></div>
    <div id="FinalVerification_Visits" class="final tab-content"></div>
    <div id="FinalVerification_Supplies" class="final tab-content"></div>
    <div id="FinalVerification_Summary" class="final tab-content"></div>
</div>