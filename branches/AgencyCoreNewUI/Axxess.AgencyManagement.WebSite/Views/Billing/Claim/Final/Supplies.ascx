﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medicare/Final/VerifySupplies", "Billing", FormMethod.Post, new { @id = "FinalSuppliesVerification_Form" })) { %>
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Episode <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy")%> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy")%></h3></li>
        </ul>
        <ol>
            <label for="FinalSuppliesVerification_IsSupplyNotBillable">
                <li>
                    <%= Html.CheckBox("IsSupplyNotBillable", Model.IsSupplyNotBillable, new { @id = "FinalSuppliesVerification_IsSupplyNotBillable" })%>
                    Check this box if the supply is not billable
                    <em>(This removes all supplies from the claim)</em>
                </li>
            </label>
        </ol>
    </div>
    <div class="note">
        <fieldset>
            <legend>Billable Supplies</legend>
            <%= Html.Hidden("FinalId", Model.Id) %>
            <%= Html.Hidden("isBillable", "false") %>
            <div class="wide column">
                <div class="row">
                    <div class="fr button-with-arrow"><a class="new-supply">New Supply</a></div>
                </div>
                <div class="row">
                    <%= Html.Telerik().Grid<Supply>().Name("FinalBillingSupply_BillableGrid").Columns(columns => {
                            columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='id' type='checkbox'  value='<#= UniqueIdentifier #>' />").ReadOnly().Title("").Width(2);
                            columns.Bound(s => s.Description).Title("Description").Width(40);
                            columns.Bound(s => s.Code).Title("HCPCS").Width(4);
                            columns.Bound(s => s.DateFormatted).Title("Date").Width(5);
                            columns.Bound(s => s.Quantity).Title("Unit").Width(3);
                            columns.Bound(s => s.UnitCostFormatted).Title("Unit Cost").Width(4);
                            columns.Bound(s => s.TotalCostFormatted).Title("Total Cost").Width(4);
                            columns.Bound(s => s.Id).ClientTemplate("<a class=\"link\" onclick=\"Billing.Claim.Final.Supply.Edit('<#=UniqueIdentifier#>','" + Model.Id + "');return false\">Edit</a> | <a class=\"link\" onclick=\"Billing.Claim.Final.Supply.Delete('<#=UniqueIdentifier#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a> | <a class=\"link\" onclick=\"Billing.Claim.Final.Supply.ChangeStatus('<#=UniqueIdentifier#>','" + Model.Id + "',false,$(this).closest('.wrapper').find('.t-grid'));return false\">Mark as Non-Billable</a>").Title("Action").Width(15);
                        }).ClientEvents(c => c.OnDataBound("Billing.Claim.Final.Supply.OnDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimFinalSupplyBillableGrid", "Billing", new { finalId = Model.Id, patientId = Model.PatientId })).Sortable().Footer(false)%>
                    <h2 class="hidden blue">No Billable Supplies</h2>
                </div>
                <div class="row buttons">
                    <ul>
                        <li><a class="mark nonbillable">Mark as Non-Billable</a></li>
                        <li><a class="delete">Delete</a></li>
                    </ul>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Non-Billable Supplies</legend>
            <%= Html.Hidden("FinalId", Model.Id) %>
            <%= Html.Hidden("isBillable", "true") %>
            <div class="wide column">
                <div class="row">
                    <%= Html.Telerik().Grid<Supply>().Name("FinalBillingSupply_NonBillableGrid").Columns(columns => {
                            columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='id' type='checkbox'  value='<#= UniqueIdentifier #>' />").ReadOnly().Title("").Width(2);
                            columns.Bound(s => s.Description).Title("Description").Width(40);
                            columns.Bound(s => s.Code).Title("HCPCS").Width(4);
                            columns.Bound(s => s.DateFormatted).Title("Date").Width(5);
                            columns.Bound(s => s.Quantity).Title("Unit").Width(3);
                            columns.Bound(s => s.UnitCostFormatted).Title("Unit Cost").Width(4);
                            columns.Bound(s => s.TotalCostFormatted).Title("Total Cost").Width(4);
                            columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<a class=\"link\" onclick=\"Billing.Claim.Final.Supply.Edit('<#=UniqueIdentifier#>','" + Model.Id + "');return false\">Edit</a> | <a class=\"link\" onclick=\"Billing.Claim.Final.Supply.Delete('<#=UniqueIdentifier#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a> | <a class=\"link\" onclick=\"Billing.Claim.Final.Supply.ChangeStatus('<#=UniqueIdentifier#>','" + Model.Id + "',true,$(this).closest('.wrapper').find('.t-grid'));return false\">Mark as Billable</a>").Title("Action").Width(15);
                        }).ClientEvents(c => c.OnDataBound("Billing.Claim.Final.Supply.OnDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimFinalSupplyNonBillableGrid", "Billing", new { finalId = Model.Id, patientId = Model.PatientId })).Sortable().Footer(false)%>
                    <h2 class="hidden blue">No Non-Billable Supplies</h2>
                </div>
                <div class="row buttons">
                    <ul>
                        <li><a class="mark billable">Mark as Billable</a></li>
                        <li><a class="delete">Delete</a></li>
                    </ul>
                </div>
            </div>
        </fieldset>
    </div>
    <%= Html.Hidden("Id", Model.Id, new { @id = "FinalSuppliesVerification_Id" })%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "FinalSuppliesVerification_EpisodeId" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "FinalSuppliesVerification_PatientId" })%>
    <div class="buttons">
        <ul>
            <li><a class="back">Back</a></li>
            <li><a class="save next">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>