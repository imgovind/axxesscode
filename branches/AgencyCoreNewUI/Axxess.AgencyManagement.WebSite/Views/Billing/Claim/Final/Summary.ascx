﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
<%= Html.Hidden("EpisodeId", Model.EpisodeId ) %>
<%= Html.Hidden("PatientId", Model.PatientId ) %>
<%  using (Html.BeginForm("Medicare/Final/Complete", "Billing", FormMethod.Post, new { @id = "FinalSummaryVerification_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <div class="fl strong">Patient First Name</div>
                <div class="fr"><%= Model.FirstName%></div>
            </div>
            <div class="row">
                <div class="fl strong">Patient Last Name</div>
                <div class="fr"><%= Model.LastName%></div>
            </div>
            <div class="row">
                <div class="fl strong">Medicare #</div>
                <div class="fr"><%= Model.MedicareNumber%></div>
            </div>
            <div class="row">
                <div class="fl strong">MRN</div>
                <div class="fr"><%= Model.PatientIdNumber%></div>
            </div>
            <div class="row">
                <div class="fl strong">Gender</div>
                <div class="fr"><%= Model.Gender%></div>
            </div>
            <div class="row">
                <div class="fl strong">DOB</div>
                <div class="fr"><%= Model.DOB != null ? Model.DOB.ToShortDateString() : string.Empty%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fl strong">Episode Start Date</div>
                <div class="fr"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl strong">Admission Date</div>
                <div class="fr"><%= Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl strong">Address Line 1</div>
                <div class="fr"><%= Model.AddressLine1%></div>
            </div>
            <div class="row">
                <div class="fl strong">Address Line 2</div>
                <div class="fr"><%= Model.AddressLine2%></div>
            </div>
            <div class="row">
                <div class="fl strong">City</div>
                <div class="fr"><%= Model.AddressCity%></div>
            </div>
            <div class="row">
                <div class="fl strong">State, Zip Code</div>
                <div class="fr"><%= Model.AddressStateCode + Model.AddressZipCode%></div>
            </div>
        </div>
    </fieldset>
    <%  var total = 0.0; %>
    <%  var diagnosis = XElement.Parse(Model.DiagnosisCode); var val = (diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : ""); %>
    <fieldset>
        <legend>Final</legend>
        <div class="column">
            <div class="row">
                <div class="fl strong">HIPPS Code</div>
                <div class="fr"><%= Model.HippsCode%></div>
            </div>
            <div class="row">
                <div class="fl strong">OASIS Matching Key</div>
                <div class="fr"><%= Model.ClaimKey%></div>
            </div>
            <div class="row">
                <div class="fl strong">Date of First Billable Visit</div>
                <div class="fr"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl strong">Physician</div>
                <div class="fr"><%= (Model.PhysicianLastName) + " " + (Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.Substring(0, 1) + "." : string.Empty)%></div>
            </div>
            <div class="row">
                <div class="fl strong">Physician NPI</div>
                <div class="fr"><%= Model.PhysicianNPI%></div>
            </div>
            <div class="row">
                <div class="strong">Remark</div>
                <div class="margin"><%= Model.Remark%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="strong">Diagonsis Codes</div>
                <div class="margin">
                    <div class="fl strong">Primary</div>
                    <div class="fr"><%= diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : string.Empty%></div>
                    <div class="clr"></div>
                    <div class="fl strong">Second</div>
                    <div class="fr"><%= diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : string.Empty%></div>
                    <div class="clr"></div>
                    <div class="fl strong">Third</div>
                    <div class="fr"><%= diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : string.Empty%></div>
                    <div class="clr"></div>
                    <div class="fl strong">Fourth</div>
                    <div class="fr"><%= diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : string.Empty%></div>
                    <div class="clr"></div>
                    <div class="fl strong">Fifth</div>
                    <div class="fr"><%= diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : string.Empty%></div>
                    <div class="clr"></div>
                    <div class="fl strong">Sixth</div>
                    <div class="fr"><%= diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : string.Empty%></div><div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="acore-grid">
        <ul>
            <li>
                <span class="grid-12"></span>
                <span class="grid-25">Description</span>
                <span class="grid-25">HCPCS/HIPPS Code</span>
                <span class="grid-12">Service Date</span>
                <span class="grid-12">Service Unit</span>
                <span class="grid-12">Total Charges</span>
            </li>
        </ul>
        <ol>
            <li>
                <span class="grid-12">0023</span>
                <span class="grid-25">Home Health Services</span>
                <span class="grid-25"><%= Model.HippsCode%></span>
                <span class="grid-12"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToString("MM/dd/yyy") : string.Empty%></span>
                <span class="grid-12"></span>
                <span class="grid-12"></span>
            </li>
    <%  if (!Model.IsSupplyNotBillable) { %>
        <%  total += Model.SupplyTotal; %>
            <li>
                <span class="grid-12">0272</span>
                <span class="grid-25">Service Supplies</span>
                <span class="grid-25"></span>
                <span class="grid-12"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToString("MM/dd/yyy") : string.Empty%></span>
                <span class="grid-12"></span>
                <span class="grid-12"><%= string.Format("${0:#0.00}", Model.SupplyTotal)%></span>
            </li>
    <%  } %>
    <%  var i = 0; %>
    <%  if (Model != null && Model.BillVisitSummaryDatas != null && Model.BillVisitSummaryDatas.Count > 0) { %>
        <%  foreach (var visit in Model.BillVisitSummaryDatas) { %>
            <li>
                <span class="grid-12"><% =visit.RevenueCode%></span>
                <span class="grid-25">Visit</span>
                <span class="grid-25"><% =visit.HCPCSCode%></span>
                <span class="grid-12"><%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "")%></span>
                <span class="grid-12"><% = visit.Unit%> </span>
                <span class="grid-12"><% = string.Format("${0:#0.00}", visit.Charge)%></span>
            </li>
            <%  i++; %>
            <%  total += visit.Charge; %>
        <%  } %>
    <%  } %>
        </ol>
        <ol>
            <li class="strong">
                <span class="grid-12">Total</span>
                <span class="grid-25"></span>
                <span class="grid-25"></span>
                <span class="grid-12"></span>
                <span class="grid-12"></span>
                <span class="grid-12"><%= string.Format("${0:#0.00}", total)%></span>
            </li>
        </ol>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="back">Back</a></li>
            <li><a class="print">Print</a></li>
            <li><a class="save close">Complete</a></li>
        </ul>
    </div>
<%  } %>
</div>