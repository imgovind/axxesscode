﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InsuranceAuthorizationViewData>" %>
<div class="row">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_HealthPlanId" class="fl strong">Health Plan Id</label>
    <div class="fr"><%= Html.TextBox("HealthPlanId", Model.HealthPlanId, new { @id = Model.ClaimTypeIdentifier + "Verification_HealthPlanId" })%></div>
</div>
<div class="row">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_GroupName" class="fl strong">Group Name</label>
    <div class="fr"><%= Html.TextBox("GroupName", Model.GroupName, new { @id = Model.ClaimTypeIdentifier + "Verification_GroupName" })%></div>
</div>
<div class="row">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_GroupId" class="fl strong">Group Id</label>
    <div class="fr"><%= Html.TextBox("GroupId", Model.GroupId, new { @id = Model.ClaimTypeIdentifier + "Verification_GroupId" })%></div>
</div>
<div class="row">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_Authorization" class="fl strong">Authorizations</label>
    <div class="fr"><%= Html.DropDownList("Authorization", Model.Authorizations != null && Model.Authorizations.Count > 0 ? Model.Authorizations : new List<SelectListItem>(), new { @id = Model.ClaimTypeIdentifier + "Verification_Authorization" })%></div>
</div>
<div id="<%= Model.ClaimTypeIdentifier %>Verification_AuthorizationInfo" class="row"><% Html.RenderPartial("Claim/AuthorizationInfo", Model.Authorization); %></div>