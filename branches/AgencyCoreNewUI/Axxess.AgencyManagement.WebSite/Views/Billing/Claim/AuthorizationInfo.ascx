﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Authorization>" %>
<div class="fr">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_AuthorizationNumber" class="strong">1.</label>
    <%= Html.TextBox("AuthorizationNumber", Model.Number1, new { @id = Model.ClaimTypeIdentifier + "Verification_AuthorizationNumber", @maxlength = "30" })%>
</div>
<div class="clr"></div>
<div class="fr">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_AuthorizationNumber2" class="strong">2.</label>
    <%= Html.TextBox("AuthorizationNumber2", Model.Number2, new { @id = Model.ClaimTypeIdentifier + "Verification_AuthorizationNumber2", @maxlength = "30" })%>
</div>
<div class="clr"></div>
<div class="fr">
    <label for="<%= Model.ClaimTypeIdentifier %>Verification_AuthorizationNumber3" class="strong">3.</label>
    <%= Html.TextBox("AuthorizationNumber3", Model.Number3, new { @id = Model.ClaimTypeIdentifier + "Verification_AuthorizationNumber3", @maxlength = "30" })%>
</div>