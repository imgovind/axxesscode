﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RapSnapShot>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Claim/RAP/Snapshot/Update", "Billing", FormMethod.Post, new { @id = "RAPSnapshotEdit_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "RAPSnapshotEdit_Id" })%>
    <%= Html.Hidden("BatchId", Model.BatchId, new { @id = "RAPSnapshotEdit_BatchId" })%>
    <fieldset>
        <legend>RAP Snapshot</legend>
        <div class="wide column">
            <div class="row">
                <label class="fl strong">Batch ID</label>
                <div class="fr"><%= Model.BatchId %></div>
            </div>
            <div class="row">
                <label class="fl strong">Episode</label>
                <div class="fr"><%= Model.EpisodeStartDate.ToShortDateString() %>&#8211;<%= Model.EpisodeEndDate.ToShortDateString() %></div>
            </div>
            <div class="row">
                <label class="fl strong">Claim Date</label>
                <div class="fr"><%= Model.ClaimDate.IsValid() ? Model.ClaimDate.ToShortDateString() : string.Empty %></div>
            </div>
            <div class="row">
                <label for="RAPSnapshotEdit_PaymentDate" class="fl strong">Payment Date</label>
                <div class="fr"><%= Html.TextBox("PaymentDate", Model.PaymentDate.IsValid() ? Model.PaymentDate.ToShortDateString() : string.Empty, new { @id = "RAPSnapshotEdit_PaymentDate", @class = "required date-picker" })%></div>
            </div>
            <div class="row">
                <label for="RAPSnapshotEdit_PaymentAmount" class="fl strong">Payment Amount</label>
                <div class="fr"><%= Html.TextBox("Payment", string.Format("{0:#0.00}", Model.Payment), new { @id = "RAPSnapshotEdit_PaymentDate", @class = "required currency" })%></div>
            </div>
            <div class="row">
                <label for="RAPSnapshotEdit_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.BillStatus("Status", Model.Status.ToString(), false, new { @id = "RAPSnapshotEdit_Status", @class = "required notzero" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>