﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RapSnapShot>>" %>
<%  if (Model.Count > 0) { %>
<ul>
    <li>
        <span class="grid-12">Batch ID</span>
        <span class="grid-20 sortable">Episode</span>
        <span class="grid-12 sortable">Claim Date</span>
        <span class="grid-12 sortable">Payment Date</span>
        <span class="grid-12 sortable">Payment Amount</span>
        <span class="grid-20 sortable">Status</span>
        <span>Action</span>
    </li>
</ul>
<ol>
    <%  foreach (var snapshot in Model) { %>
    <li>
        <span class="grid-12"><%= snapshot.BatchId %></span>
        <span class="grid-20"><%= snapshot.EpisodeStartDate.ToShortDateString() %>&#8211;<%= snapshot.EpisodeEndDate.ToShortDateString() %></span>
        <span class="grid-12"><%= snapshot.ClaimDate.IsValid() ? snapshot.ClaimDate.ToShortDateString() : string.Empty %></span>
        <span class="grid-12"><%= snapshot.PaymentDate.IsValid() ? snapshot.PaymentDate.ToShortDateString() : string.Empty %></span>
        <span class="grid-12"><%= string.Format("${0:#0.00}", snapshot.Payment) %></span>
        <span class="grid-20"><%= snapshot.StatusName %></span>
        <span><a class="link" onclick="Billing.Claim.RAP.Snapshot.Edit('<%= snapshot.BatchId %>', '<%= snapshot.Id %>');return false">Edit</a></span>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<h2 class="blue">No Snapshots Found</h2>
<%  } %>