﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<%  if (Model != null && Model.Count > 0) { %>
<input type="hidden" name="SortType" value="rap-name" />
<ul>
    <li class="ac"><h3>RAPs</h3></li>
    <li>
        <span class="grid-number"></span>
        <span class="grid-icon"></span>
        <span class="grid-25 sortable">Patient Name</span>
        <span class="grid-12 sortable">MRN</span>
        <span class="grid-25 sortable">Episode Period</span>
        <span class="grid-labeled-icon sortable ac">OASIS</span>
        <span class="grid-labeled-icon sortable ac">Billable Visit</span>
        <span class="grid-labeled-icon sortable ac">Verified</span>
        <span class="grid-icon fr"></span>
    </li>
</ul>
<ol>
    <%  int i = 1; %>
    <%  foreach (var rap in Model) { %>
    <li class="<%= rap.IsOasisComplete && rap.IsFirstBillableVisit ? "ready" : "notready" %>">
        <span class="grid-number"><%= i.ToString() %>.</span>
        <span class="grid-icon"><%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "<input name='ClaimSelected' class='radio' type='checkbox' value='" + rap.Id + "' id='RapSelected" + rap.Id + "' />" : string.Empty %></span>
        <span class="grid-25 strong">
        <%  if (rap.IsOasisComplete && rap.IsFirstBillableVisit) { %>
            <a class="link" onclick="Billing.Claim.RAP.Verify('<%= rap.EpisodeId %>','<%= rap.PatientId %>');return false"><%= rap.LastName %>, <%= rap.FirstName %></a>
        <%  } else { %>
            <%= rap.LastName %>, <%= rap.FirstName %>
        <%  } %>
        </span>
        <span class="grid-12"><%= rap.PatientIdNumber%></span>
        <span class="grid-25">
            <span class="sort-value"><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
            <%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("MM/dd/yyyy") : string.Empty %>
            &#8211;
            <%= rap.EpisodeEndDate != null ? rap.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty %>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= rap.IsOasisComplete ? "0" : "1" %></span>
            <span class="img icon <%= rap.IsOasisComplete ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= rap.IsFirstBillableVisit ? "0" : "1"%></span>
            <span class="img icon <%= rap.IsFirstBillableVisit ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "0" : "1"%></span>
            <span class="img icon <%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-icon fr"><a onclick="Billing.Claim.RAP.Print('<%= rap.EpisodeId %>','<%= rap.PatientId %>');return false"><span class="img icon print"></span></a></span>
    </li>
        <%  i++; %>
    <%  } %>
</ol>
<%  } else { %>
<ul>
    <li class="ac"><h3>RAPs</h3></li>
</ul>
<h1 class="blue">No RAPs Found</h1>
<%  } %>
