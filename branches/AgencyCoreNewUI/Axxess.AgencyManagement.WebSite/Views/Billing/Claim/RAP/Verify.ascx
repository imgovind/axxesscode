﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rap>" %>
<span class="wintitle">RAP | <%= Model.DisplayName %></span>
<%  var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<%  var diagnosis = Model.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(Model.DiagnosisCode) : null; %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medicare/RAP/Verify", "Billing", FormMethod.Post, new { @id = "RAPVerification_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "RAPVerification_Id" }) %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "RAPVerification_PatientId" }) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "RAPVerification_EpisodeId" }) %>
    <%= Html.Hidden("ClaimTypeIdentifier", "RAPHMOInfo", new { @id = "RAPVerification_ClaimTypeIdentifier" })%>
    <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO, new { @id = "RAPVerification_IsMedicareHMO" }) %>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label for="RAPVerification_FirstName" class="fl strong">Patient First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "RAPVerification_FirstName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_LastName" class="fl strong">Patient Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "RAPVerification_LastName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_MedicareNumber" class="fl strong">Medicare #</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "RAPVerification_MedicareNumber", @class = "required", @maxlength = "12" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_PrimaryInsuranceId" class="fl strong">Insurance</label>
                <div class="fr"><%= Html.InsurancesMedicareByBranch("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), Model.AgencyLocationId, true, "-- Select Insurnace --", new { @class = "required notzero", @id = "RAPVerification_PrimaryInsuranceId" })%></div>
            </div>
            <div id="RAPVerification_InsuranceInfo"><%  if (Model.PrimaryInsuranceId >= 1000) Html.RenderPartial("Claim/InsuranceInfo", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "RAPHMOInfo" }); %></div>
            <div class="row">
                <label for="RAPVerification_PatientIdNumber" class="fl strong">Patient ID/MRN</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "RAPVerification_PatientIdNumber", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label class="fl strong">Gender</label>
                <div class="fr radio">
                    <%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "RAPVerification_GenderFemale", @class = "required" })%>
                    <label for="RAPVerification_GenderFemale" class="fixed short">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "RAPVerification_GenderMale", @class = "required" })%>
                    <label for="RAPVerification_GenderMale" class="fixed short">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="RAPVerification_DOB" class="fl strong">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="RAPVerification_DOB" /></div>
            </div>
            <div class="row">
                <label for="RAPVerification_Type" class="fl strong">Bill Type</label>
                <div class="fr">
                    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Initial Rap", Value = "0" },
                            new SelectListItem { Text = "Rap Cancellation", Value = "1" }
                        }, "Value", "Text", Model.Type); %>
                    <%= Html.DropDownList("Type", billType, new { @id = "RAPVerification_Type" }) %>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="RAPVerification_StartOfCareDate" class="fl strong">Admission Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="RAPVerification_StartOfCareDate" /></div>
            </div>
            <div class="row">
                <label for="RAPVerification_AdmissionSource" class="fl strong">Admission Source</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : string.Empty, new { @id = "RAPVerification_AdmissionSource", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_PatientStatus" class="fl strong">Patient Status</label>
                <div class="fr"><%= Html.UB4PatientStatus("UB4PatientStatus", ((int)UB4PatientStatus.StillPatient).ToString(), new { @id = "RAPVerification_PatientStatus", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "RAPVerification_AddressLine1", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "RAPVerification_AddressLine2" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "RAPVerification_AddressCity", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_AddressStateCode" class="fl strong">State, Zip Code</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "RAPVerification_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "RAPVerification_AddressZipCode", @class = "required digits zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="wide column">
            <div class="row">
                <label class="strong">Condition Codes</label>
                <div class="ac">
                    <label for="RAPVerification_ConditionCode18" class="strong">18.</label>
                    <%= Html.TextBox("ConditionCode18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : string.Empty, new { @id = "RAPVerification_ConditionCode18", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode19" class="strong">19.</label>
                    <%= Html.TextBox("ConditionCode19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : string.Empty, new { @id = "RAPVerification_ConditionCode19", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode20" class="strong">20.</label>
                    <%= Html.TextBox("ConditionCode20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : string.Empty, new { @id = "RAPVerification_ConditionCode20", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode21" class="strong">21.</label>
                    <%= Html.TextBox("ConditionCode21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : string.Empty, new { @id = "RAPVerification_ConditionCode21", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode22" class="strong">22.</label>
                    <%= Html.TextBox("ConditionCode22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : string.Empty, new { @id = "RAPVerification_ConditionCode22", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode23" class="strong">23.</label>
                    <%= Html.TextBox("ConditionCode23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : string.Empty, new { @id = "RAPVerification_ConditionCode23", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode24" class="strong">24.</label>
                    <%= Html.TextBox("ConditionCode24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : string.Empty, new { @id = "RAPVerification_ConditionCode24", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode25" class="strong">25.</label>
                    <%= Html.TextBox("ConditionCode25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : string.Empty, new { @id = "RAPVerification_ConditionCode25", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode26" class="strong">26.</label>
                    <%= Html.TextBox("ConditionCode26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : string.Empty, new { @id = "RAPVerification_ConditionCode26", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode27" class="strong">27.</label>
                    <%= Html.TextBox("ConditionCode27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : string.Empty, new { @id = "RAPVerification_ConditionCode27", @class = "shortest", @maxlength = "2" })%>
                    <label for="RAPVerification_ConditionCode28" class="strong">28.</label>
                    <%= Html.TextBox("ConditionCode28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : string.Empty, new { @id = "RAPVerification_ConditionCode28", @class = "shortest", @maxlength = "2" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Episode</legend>
        <div class="column">
            <%= Html.Hidden("AssessmentType", Model.AssessmentType)%>
            <div class="row">
                <label for="RAPVerification_HippsCode" class="fl strong">HIPPS Code</label>
                <div class="fr"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @id = "RAPVerification_HippsCode", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_ClaimKey" class="fl strong">OASIS Matching Key</label>
                <div class="fr"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @id = "RAPVerification_ClaimKey", @class = "required", @maxlength = "18" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_EpisodeStartDate" class="fl strong">Episode Start Date</label>
                <div class="fr">
                    <input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="RAPVerification_EpisodeStartDate" />
                    <%= Html.Hidden("EpisodeEndDate", Model.EpisodeEndDate.ToShortDateString()) %>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Recommended/Previously Entered First Billable Date</label>
                <div class="fr"><%= Model.FirstBillableVisitDateFormat %></div>
            </div>
            <div class="row">
                <label for="RAPVerification_FirstBillableVisitDateFormatInput" class="fl strong">Date of First Billable Visit</label>
                <div class="fr"><input type="text" class="date-picker required" name="FirstBillableVisitDateFormatInput" value="<%= Model.FirstBillableVisitDateFormat %>" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="RAPVerification_FirstBillableVisitDateFormat" /></div>
                <div class="clr"></div>
                <div class="fr"><em>Please Verifiy the first billable visit date from the schedule</em></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="show-schedule">View Schedule</a></div>
            </div>
            <div class="row">
                <label for="RAPVerification_PhysicianLastName" class="fl strong">Physician Last Name</label>
                <div class="fr"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @id = "RAPVerification_PhysicianLastName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_PhysicianFirstName" class="fl strong">Physician First Name</label>
                <div class="fr"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @id = "RAPVerification_PhysicianFirstName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_PhysicianNPI" class="fl strong">Physician NPI #</label>
                <div class="fr"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @id = "RAPVerification_PhysicianNPI", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="RAPVerification_ProspectivePay" class="fl strong">HIPPS Code Payment</label>
                <div class="fr"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @id = "RAPVerification_ProspectivePay", @maxlength = "20" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="strong">Diagnosis Codes</label>
                <div class="margin">
                    <label for="RAPVerification_Primary" class="fl strong">Primary</label>
                    <div class="fr"><%= Html.TextBox("Primary", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty, new { @id = "RAPVerification_Primary" })%></div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Second" class="fl strong">Second</label>
                    <div class="fr"><%= Html.TextBox("Second", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty, new { @id = "RAPVerification_Second" })%></div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Third" class="fl strong">Third</label>
                    <div class="fr"><%= Html.TextBox("Third", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty, new { @id = "RAPVerification_Third" })%></div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Fourth" class="fl strong">Fourth</label>
                    <div class="fr"><%= Html.TextBox("Fourth", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty, new { @id = "RAPVerification_Fourth" })%></div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Fifth" class="fl strong">Fifth</label>
                    <div class="fr"><%= Html.TextBox("Fifth", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : string.Empty, new { @id = "RAPVerification_Fifth" })%></div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Sixth" class="fl strong">Sixth</label>
                    <div class="fr"><%= Html.TextBox("Sixth", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : string.Empty, new { @id = "RAPVerification_Sixth" })%></div>
                </div>
            </div>
            <div class="row">
                <label class="strong">UB04 Locator 81cc</label>
                <div class="margin">
                    <label for="RAPVerification_Locator1Code1" class="fl strong">a</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator1")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "RAPVerification_Locator1Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "RAPVerification_Locator1Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "RAPVerification_Locator1Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Locator2Code1" class="fl strong">b</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator2")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "RAPVerification_Locator2Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "RAPVerification_Locator2Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "RAPVerification_Locator2Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Locator3Code1" class="fl strong">c</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator3")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "RAPVerification_Locator3Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "RAPVerification_Locator3Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "RAPVerification_Locator3Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="RAPVerification_Locator4Code1" class="fl strong">d</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator4")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "RAPVerification_Locator4Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "RAPVerification_Locator4Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "RAPVerification_Locator4Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="wide column">
            <div class="row">
                <label for="RAPVerification_RapRemark" class="strong">Remark</label>
                <%= Html.TextArea("Remark", Model.Remark, new { @id = "RAPVerification_RapRemark", @maxcharacters = "500" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Verify</a></li>
            <li><a class="print">Print</a></li>
        </ul>
    </div>
<%  } %>
</div>