﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rap>" %>
<%  var location = Model.Agency.GetBranch(Model.BranchId); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<%  var diagnosis = Model.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(Model.DiagnosisCode) : null; %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Billing/RAP.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : string.Empty) + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty) + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : string.Empty) + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty) : string.Empty) : string.Empty).Clean()%>",
        patientname:    "<%= Model != null ? (Model.LastName.IsNotNullOrEmpty() ? Model.LastName.Clean() + ", " : string.Empty) + (Model.FirstName.IsNotNullOrEmpty() ? Model.FirstName.Clean() : string.Empty) : string.Empty %>",
        patient:        "<%= Model != null ? (Model.AddressLine1.IsNotNullOrEmpty() ? Model.AddressLine1.Clean() + "<br/>" : string.Empty) + (Model.AddressLine2.IsNotNullOrEmpty() ? Model.AddressLine2.Clean() + "<br/>" : string.Empty) + (Model.AddressCity.IsNotNullOrEmpty() ? Model.AddressCity.Clean() + ", " : string.Empty) + (Model.AddressStateCode.IsNotNullOrEmpty() ? Model.AddressStateCode.Clean() + "  " : string.Empty) + (Model.AddressZipCode.IsNotNullOrEmpty() ? Model.AddressZipCode + "<br/>" : string.Empty) : string.Empty %>",
        mcare:          "<%= Model != null && Model.MedicareNumber.IsNotNullOrEmpty() ? Model.MedicareNumber.Clean() : string.Empty %>",
        record:         "<%= Model != null && Model.PatientIdNumber.IsNotNullOrEmpty() ? Model.PatientIdNumber.Clean() : string.Empty %>",
        gender:         "<%= Model != null && Model.Gender.IsNotNullOrEmpty() ? Model.Gender.Clean() : string.Empty %>",
        dob:            "<%= Model != null && Model.DOB.IsValid() ? Model.DOB.ToShortDateString().Clean() : string.Empty %>",
        type:           "<%= Model != null ? (Model.Type == 0 ? "Initial RAP" : string.Empty) + (Model.Type == 1 ? "RAP Cancellation" : string.Empty) : string.Empty %>",
        admitdate:      "<%= Model != null && Model.StartofCareDate.IsValid() ? Model.StartofCareDate.ToShortDateString().Clean() : string.Empty %>",
        admitsource:    "<%= Model != null && Model.AdmissionSourceDisplay.IsNotNullOrEmpty() ? Model.AdmissionSourceDisplay.Clean() : string.Empty %>",
        status:         "<%= Model != null ? (Model.PatientStatus == 0 ? "Active" : string.Empty) + (Model.PatientStatus == 1 ? "Discharged" : string.Empty) : string.Empty %>",
        cond18:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value.Clean() : string.Empty %>",
        cond19:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value.Clean() : string.Empty %>",
        cond20:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value.Clean() : string.Empty %>",
        cond21:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value.Clean() : string.Empty %>",
        cond22:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value.Clean() : string.Empty %>",
        cond23:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value.Clean() : string.Empty %>",
        cond24:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value.Clean() : string.Empty %>",
        cond25:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value.Clean() : string.Empty %>",
        cond26:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value.Clean() : string.Empty %>",
        cond27:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value.Clean() : string.Empty %>",
        cond28:         "<%= conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value.Clean() : string.Empty %>",
        hipps:          "<%= Model != null && Model.HippsCode.IsNotNullOrEmpty() ? Model.HippsCode.Clean() : string.Empty %>",
        matchkey:       "<%= Model != null && Model.ClaimKey.IsNotNullOrEmpty() ? Model.ClaimKey.Clean() : string.Empty %>",
        startdate:      "<%= Model != null && Model.EpisodeStartDate.IsValid() ? Model.EpisodeStartDate.ToShortDateString().Clean() : string.Empty %>",
        billvisit:      "<%= Model != null && Model.FirstBillableVisitDate.IsValid() ? Model.FirstBillableVisitDate.ToShortDateString().Clean() : string.Empty %>",
        phys:           "<%= Model != null ? (Model.PhysicianLastName.IsNotNullOrEmpty() ? Model.PhysicianLastName.ToTitleCase() + ", " : string.Empty) + (Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.ToTitleCase() : string.Empty) : string.Empty %>",
        physnpi:        "<%= Model != null && Model.PhysicianNPI.IsNotNullOrEmpty() ? Model.PhysicianNPI.Clean() : string.Empty %>",
        hippspay:       "<%= Model != null && Model.ProspectivePay > 0 ? Model.ProspectivePay.ToString().Clean() : string.Empty %>",
        diag1:          "<%= diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value.Clean() : string.Empty %>",
        diag2:          "<%= diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value.Clean() : string.Empty %>",
        diag3:          "<%= diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value.Clean() : string.Empty %>",
        diag4:          "<%= diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value.Clean() : string.Empty %>",
        diag5:          "<%= diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value.Clean() : string.Empty %>",
        diag6:          "<%= diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value.Clean() : string.Empty %>"
    };
    PrintPreview.BuildBasic("<%= Model != null && Model.Remark.IsNotNullOrEmpty() ? Model.Remark.Clean() : string.Empty %>");
</script>