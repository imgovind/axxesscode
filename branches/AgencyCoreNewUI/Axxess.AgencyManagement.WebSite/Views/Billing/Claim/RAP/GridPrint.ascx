﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<%  var location = Model.Agency.GetBranch(Model.BranchId); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Billing/RAPGrid.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = { agency: "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : string.Empty) + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty) + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : string.Empty) + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty) : string.Empty) : string.Empty).Clean()%>" };
    PrintPreview.BuildSections([
        { Content: [ [ "<h3>RAPs</h3>" ] ] }
<%  if (Model.Claims.Count > 0) { %>
        ,
        { Content: [ [
            "<strong>Patient Name</strong>",
            "<strong>MRN</strong>",
            "<strong>Episode Date</strong>",
            "<strong>OASIS</strong>",
            "<strong>Billable Visit</strong>",
            "<strong>Verified</strong>" ] ] }
    <%  foreach (var rap in Model.Claims) { %>
        ,
        { Content: [ [
            "<%= rap.LastName.Clean() + ", " + rap.FirstName.Clean() %>",
            "<%= rap.PatientIdNumber.Clean() %>",
            "<%= rap.EpisodeStartDate.ToShortDateString().Clean() + " - " + rap.EpisodeEndDate.ToShortDateString().Clean() %>",
            PrintPreview.CheckBox("", <%= rap.IsOasisComplete.ToString().ToLower() %>),
            PrintPreview.CheckBox("", <%= rap.IsFirstBillableVisit.ToString().ToLower() %>),
            PrintPreview.CheckBox("", <%= rap.IsVerified.ToString().ToLower() %>) ] ] }
    <%  } %>
<%  } %>
    ]);
</script>