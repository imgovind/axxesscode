﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Pending Claims | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <fieldset class="grid-controls">
        <a class="grid-refresh hidden"></a>
        <div class="ac">
            <div class="filter">
                <label for="PendingClaims_BranchCode" class="strong">Branch</label>
                <%= Html.DropDownList("BranchId", Model.Branches, new { @id = "PendingClaims_BranchCode", @status = "Select Branch to Filter Results" })%>
            </div>
            <div class="filter">
                <label for="PendingClaims_Insurance" class="strong">Insurance</label>
                <%= Html.DropDownList("PrimaryInsurance", Model.Insurances, new { @id = "PendingClaims_Insurance", @status = "Select Insurance to Filter Results" })%>
            </div>
        </div>
    </fieldset>
    <div id="PendingClaims_RAPGrid" class="acore-grid"></div>
    <div id="PendingClaims_FinalGrid" class="acore-grid"></div>
</div>