﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PendingClaimLean>>" %>
<%  if (Model.Count > 0) { %>
<ul>
    <li class="ac"><h3><%= Model.FirstOrDefault().Type %>s</h3></li>
    <li>
        <span class="grid-icon"></span>
        <span class="grid-12 sortable">Payment Date</span>
        <span class="grid-20 sortable">Patient</span>
        <span class="grid-12 sortable">Medicare #</span>
        <span class="grid-12 sortable">Episode</span>
        <span class="grid-12 sortable">Claim Amount</span>
        <span class="grid-12 sortable">Status</span>
        <span class="grid-8 sortable">Payment Amount</span>
        <span>Action</span>k
    </li>
</ul>
<ol>
    <%  foreach (var claim in Model) { %>
    <li>
        <span class="grid-icon"><span class="img icon expand"><%= claim.Id %></span></span>
        <span class="grid-12"><%= claim.PaymentDate.IsValid() ? claim.PaymentDate.ToShortDateString() : string.Empty %></span>
        <span class="grid-20"><%= claim.DisplayName %></span>
        <span class="grid-12"><%= claim.MedicareNumber %></span>
        <span class="grid-12"><%= claim.EpisodeStartDate.ToShortDateString() %>&#8211;<%= claim.EpisodeEndDate.ToShortDateString() %></span>
        <span class="grid-12"><%= string.Format("${0:#0.00}", claim.ClaimAmount) %></span>
        <span class="grid-12"><%= claim.StatusName %></span>
        <span class="grid-8"><%= string.Format("${0:#0.00}", claim.PaymentAmount) %></span>
        <span><a class="link" onclick="Billing.Claim.Pending.Edit('<%= claim.Id %>','<%= Model.FirstOrDefault().Type %>');return false">Edit</a></span>
        <div class="snapshot acore-grid"></div>
    </li>
    <%  } %>
</ol>
<%  } %>