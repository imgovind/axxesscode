﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PendingClaimLean>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Claim/Pending/Update", "Billing", FormMethod.Post, new { @id = "PendingClaimEdit_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "PendingClaimEdit_Id" })%>
    <%= Html.Hidden("Type", Model.Type, new { @id = "PendingClaimEdit_Type" })%>
    <fieldset>
        <legend>Pending Claim</legend>
        <div class="wide column">
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Medicare #</label>
                <div class="fr"><%= Model.MedicareNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Episode</label>
                <div class="fr"><%= Model.EpisodeStartDate.ToShortDateString() %>&#8211;<%= Model.EpisodeEndDate.ToShortDateString() %></div>
            </div>
            <div class="row">
                <label class="fl strong">Claim Amount</label>
                <div class="fr"><%= string.Format("${0:#0.00}", Model.ClaimAmount) %></div>
            </div>
            <div class="row">
                <label for="PendingClaimEdit_PaymentDate" class="fl strong">Payment Date</label>
                <div class="fr"><%= Html.TextBox("PaymentDate", Model.PaymentDate.IsValid() ? Model.PaymentDate.ToShortDateString() :  string.Empty, new { @id = "PendingClaimEdit_PaymentDate", @class = "required date-picker" }) %></div>
            </div>
            <div class="row">
                <label for="PendingClaimEdit_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.BillStatus("Status", Model.Status.ToString(), false, new { @id = "PendingClaimEdit_Status", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="PendingClaimEdit_PaymentAmount" class="fl strong">Payment Amount</label>
                <div class="fr"><%= Html.TextBox("PaymentAmount", string.Format("{0:#0.00}", Model.PaymentAmount), new { @id = "PendingClaimEdit_PaymentDate", @class = "required currency" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">close</a></li>
        </ul>
    </div>
<%  } %>
</div>