﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedBillViewData>" %>
<span class="wintitle">Managed Care Claims | <%= Current.AgencyName %></span>
<div id="BillingManagedCenter_Content" class="main wrapper">
<%  using (Html.BeginForm("Medicare/ClaimSummary", "Billing", FormMethod.Post, new { @id = "BillingManagedCenter_Form" })) { %>
    <div class="buttons fr">
        <ul><li><a class="print">Print</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="filter">
            <label for="BillingManagedCenter_BranchCode" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "BillingManagedCenter_BranchCode" })%>
        </div>
        <div class="filter">
            <label for="BillingManagedCenter_InsuranceId" class="strong">Insurance</label>
            <%= Html.InsurancesNoneMedicare("PrimaryInsurance", Model.Insurance.ToString(), false, "", new { @id = "BillingManagedCenter_InsuranceId", @class = "required notzero" })%>
        </div>
    </fieldset>
    <div id="BillingManagedCenter_Content" class="acore-grid content"><% Html.RenderPartial("Claim/Managed/Grid", Model); %></div>
    <%  if (Current.HasRight(Permissions.GenerateClaimFiles)) { %>
    <div class="buttons">
        <ul>
            <li><a class="generate">Generate Selected</a></li>
            <li><a class="generate all">Generate All Completed</a></li>
        </ul>
    </div>
    <%  } %>
<%  } %>
</div>