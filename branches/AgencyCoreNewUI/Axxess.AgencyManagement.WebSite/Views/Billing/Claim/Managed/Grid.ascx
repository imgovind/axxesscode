﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedBillViewData>" %>
<%  if (Model != null && Model.Bills.Count > 0) { %>
    <%  var claims = Model.Bills.Where(c => c.EpisodeEndDate.Date <= DateTime.Now.Date); %>
<ul>
    <li class="ac"><h3><%= Model.InsuranceName %> Claims</h3></li>
    <li>
        <span class="grid-number"></span>
        <span class="grid-icon"></span>
        <span class="grid-25 sortable">Patient Name</span>
        <span class="grid-12 sortable">MRN</span>
        <span class="grid-20 sortable">Episode Period</span>
        <span class="grid-labeled-icon sortable ac">Detail</span>
        <span class="grid-labeled-icon sortable ac">Visit</span>
        <span class="grid-labeled-icon sortable ac">Supply</span>
        <span class="grid-labeled-icon sortable ac">Verified</span>
        <span class="grid-icon fr"></span>
    </li>
</ul>
<ol>
    <%  int i = 1; %>
    <%  foreach (var claim in claims) { %>
    <li>
        <span class="grid-number"><%= i.ToString() %>.</span>
        <span class="grid-icon"><%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? "<input name='ManagedClaimSelected' type='checkbox' value='" + claim.Id + "' id='ManagedClaimSelected" + claim.Id + "' />" : string.Empty %></span>
        <span class="grid-25 strong"><a class="link" onclick="Billing.Claim.Managed.Load('<%= claim.Id %>', '<%= claim.PatientId %>');return false"><%= claim.LastName %>, <%= claim.FirstName %></a></span>
        <span class="grid-12"><%= claim.PatientIdNumber %></span>
        <span class="grid-20">
            <span class="sort-value"><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
            <%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %>
            &#8211;
            <%= claim.EpisodeEndDate != null ? claim.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= claim.IsInfoVerified ? "0" : "1" %></span>
            <span class="img icon <%= claim.IsInfoVerified ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= claim.IsVisitVerified ? "0" : "1" %></span>
            <span class="img icon <%= claim.IsVisitVerified ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= claim.IsSupplyVerified ? "0" : "1" %></span>
            <span class="img icon <%= claim.IsSupplyVerified ? "success-small" : "error-small" %>"></span>
        </span>
        <span class="grid-labeled-icon ac">
            <span class="sort-value"><%= claim.IsVisitVerified ? "0" : "1" %></span>
            <span class="img icon <%= claim.IsVisitVerified ? "success-small" : "error-small" %>"></span>
        </span>
        <%  if (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) { %>
        <span class="grid-icon fr"><a class="fr" onclick="Billing.Claim.Managed.Print('<%= claim.Id %>', '<%= claim.PatientId %>', 'UB04');return false"><span class="img icon print"></span></a></span>
        <%  } %>
    </li>
        <%  i++; %>
    <%  } %>
</ol>
<%  } else { %>
<ul>
    <li class="ac"><h3><%= Model.InsuranceName %> Claims</h3></li>
</ul>
<h1 class="blue">No <%= Model.InsuranceName %> Claims Found</h1>
<%  } %>