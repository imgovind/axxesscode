﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Supply>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medicare/Final/Supply/Update", "Billing", FormMethod.Post, new { @id = "EditSupply_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id ="EditSupply_Id" })%>
    <%= Html.Hidden("FinalId", Model.FinalId, new { @id ="EditSupply_FinalId" })%>
    <%= Html.Hidden("UniqueIdentifier", Model.UniqueIdentifier, new { @id = "EditSupply_UniqueIdentifier" })%>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="EditSupply_Description" class="fl strong">Description</label>
                <div class="fr"><%= Html.TextBox("Description", Model.Description, new { @id = "EditSupply_Description", @class = "required longest" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditSupply_Code" class="fl strong">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = "EditSupply_Code", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditSupply_DateForEdit" class="fl strong">Date</label>
                <div class="fr"><%= Html.TextBox("Date", Model.DateForEdit.ToShortDateString(), new { @id = "EditSupply_Date", @class = "required date-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditSupply_Quantity" class="fl strong">Quantity</label>
                <div class="fr"><%= Html.TextBox("Quantity", Model.Quantity, new { @id = "EditSupply_Quantity", @class = "required numeric" })%></div>
            </div>
            <div class="row">
                <label for="EditSupply_UnitCost" class="fl strong">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", Model.UnitCost, new { @id = "EditSupply_UnitCost", @class = "required currency" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>