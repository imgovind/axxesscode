﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medicare/Final/Supply/Create", "Billing", FormMethod.Post, new { @id = "NewSupply_Form" })) { %>
    <%= Html.Hidden("FinalId", Model, new { @id = "NewSupply_FinalId" })%>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="NewSupply_Description" class="fl strong">Description</label>
                <div class="fr"><%= Html.TextBox("Description", string.Empty, new { @id = "NewSupply_Description", @class = "required longest" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewSupply_Code" class="fl strong">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", string.Empty, new { @id = "NewSupply_Code", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewSupply_DateForEdit" class="fl strong">Date</label>
                <div class="fr"><%= Html.TextBox("Date", string.Empty, new { @id = "NewSupply_Date", @class = "required date-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewSupply_Quantity" class="fl strong">Quantity</label>
                <div class="fr"><%= Html.TextBox("Quantity", "1", new { @id = "NewSupply_Quantity", @class = "required numeric" })%></div>
            </div>
            <div class="row">
                <label for="NewSupply_UnitCost" class="fl strong">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", string.Empty, new { @id = "NewSupply_UnitCost", @class = "required currency" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>