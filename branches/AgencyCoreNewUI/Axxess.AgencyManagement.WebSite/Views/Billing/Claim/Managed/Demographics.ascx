﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="wrapper main">
<%  var diagnosis = XElement.Parse(Model.DiagnosisCode); %>
<%  var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<%  using (Html.BeginForm("Medicare/Managed/VerifyDemographics", "Billing", FormMethod.Post, new { @id = "ManagedVerification_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "ManagedVerification_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "ManagedVerification_PatientId" })%>
    <%= Html.Hidden("ClaimTypeIdentifier", "ManageInfo", new { @id = "ManagedVerification_ClaimTypeIdentifier" })%>
    <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO) %>
    <fieldset>
        <legend>Final</legend>
        <div class="column">
            <div class="row">
                <label for="ManagedVerification_Type" class="fl strong">Bill Type</label>
                <div class="fr"><%=Html.BillType("Type", Model.Type, new { @id = "ManagedVerification_Type", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_FirstName" class="fl strong">Patient First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "ManagedVerification_FirstName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_LastName" class="fl strong">Patient Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "ManagedVerification_LastName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_InsuranceNumber" class="fl strong">Insurance #</label>
                <div class="fr"><%= Html.TextBox("InsuranceNumber", Model.HealthPlanId, new { @id = "ManagedVerification_InsuranceNumber", @class = "required", @maxlength = "12" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_PrimaryInsuranceId" class="fl strong">Insurance</label>
                <div class="fr"><%= Html.InsurancesMedicareByBranch("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), Model.AgencyLocationId, true, "-- Select Insurnace --", new { @class = "required notzero", @id = "ManagedVerification_PrimaryInsuranceId" })%></div>
            </div>
            <div id="ManagedVerification_InsuranceInfo"><% if (Model.PrimaryInsuranceId >= 1000) Html.RenderPartial("Claim/InsuranceInfo", new InsuranceAuthorizationViewData { GroupId = Model.GroupId, GroupName = Model.GroupName, HealthPlanId = Model.HealthPlanId, Authorizations = Model.Authorizations, Authorization = new Authorization { Number1 = Model.AuthorizationNumber, Number2 = Model.AuthorizationNumber2, Number3 = Model.AuthorizationNumber3 }, ClaimTypeIdentifier = "ManageInfo" }); %></div> 
            <div class="row">
                <label for="ManagedVerification_PatientIdNumber" class="fl strong">Patient ID/MR Number</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "ManagedVerification_PatientIdNumber", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label class="fl strong">Gender</label>
                <div class="fr radio">
                    <%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "ManagedVerification_GenderFemale", @class = "required" })%>
                    <label for="ManagedVerification_GenderFemale" class="fixed short">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "ManagedVerification_GenderMale", @class = "required" })%>
                    <label for="ManagedVerification_GenderMale" class="fixed short">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="ManagedVerification_DOB" class="fl strong">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="ManagedVerification_DOB" /></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_EpisodeStartDate" class="fl strong">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" id="ManagedVerification_EpisodeStartDate" /></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_EpisodeEndDate" class="fl strong">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EpisodeEndDate" value="<%= Model.EpisodeEndDate.ToShortDateString() %>" id="ManagedVerification_EpisodeEndDate" /></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_StartOfCareDate" class="fl strong">Admission Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="ManagedVerification_StartOfCareDate" /></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_AdmissionSource" class="fl strong">Admission Source</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : "", new { @id = "ManagedVerification_AdmissionSource", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_PatientStatus" class="fl strong">Patient Status</label>
                <div class="fr"><%= Html.UB4PatientStatus("UB4PatientStatus", Model.UB4PatientStatus, new { @id = "ManagedVerification_PatientStatus", @class = "required notzero" })%></div>
            </div>
            <div class="row" id="ManagedVerification_PatientStatusMore">
                <label for="ManagedVerification_DischargeDate" class="fl strong">Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="DischargeDate" value="<%= Model.DischargeDate.ToShortDateString() %>" id="ManagedVerification_DischargeDate" /></div>
            </div>
        </div>
        <div class="column">
            <%= Html.Hidden("AssessmentType", Model.AssessmentType, new { @id = "ManagedVerification_AssessmentType" })%>
            <div class="row">
                <label for="ManagedVerification_HippsCode" class="fl strong">HIPPS Code</label>
                <div class="fr"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @id = "ManagedVerification_HippsCode", @class = "required", @maxlength = "5" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_ClaimKey" class="fl strong">OASIS Matching Key</label>
                <div class="fr"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @id = "ManagedVerification_ClaimKey", @class = "required", @maxlength = "18" })%></div>
            </div>
             <div class="row">
                <label for="ManagedVerification_FirstBillableVisitDate" class="fl strong">First Billable Visit Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="FirstBillableVisitDate" value="<%= Model.FirstBillableVisitDate.Date > DateTime.MinValue.Date ? Model.FirstBillableVisitDate.ToShortDateString() : Model.EpisodeStartDate.ToShortDateString() %>" id="ManagedVerification_FirstBillableVisitDate" /></div>
            </div>
           <div class="row">
                <label for="ManagedVerification_PhysicianLastName" class="fl strong">Physician Last Name</label>
                <div class="fr"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @id = "ManagedVerification_PhysicianLastName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_PhysicianFirstName" class="fl strong">Physician First Name</label>
                <div class="fr"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @id = "ManagedVerification_PhysicianFirstName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_PhysicianNPI" class="fl strong">Physician NPI #</label>
                <div class="fr"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @id = "ManagedVerification_PhysicianNPI", @class = "required", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_ProspectivePay" class="fl strong">HIPPS Code Payment</label>
                <div class="fr"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @id = "ManagedVerification_ProspectivePay", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label class="strong">Diagnosis Codes</label>
                <div class="margin">
                    <label for="ManagedVerification_Primary" class="fl strong">Primary</label>
                    <div class="fr"><%= Html.TextBox("Primary", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty, new { @id = "ManagedVerification_Primary" })%></div>
                    <div class="clr"></div>
                    <label for="ManagedVerification_Second" class="fl strong">Second</label>
                    <div class="fr"><%= Html.TextBox("Second", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty, new { @id = "ManagedVerification_Second" })%></div>
                    <div class="clr"></div>
                    <label for="ManagedVerification_Third" class="fl strong">Third</label>
                    <div class="fr"><%= Html.TextBox("Third", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty, new { @id = "ManagedVerification_Third" })%></div>
                    <div class="clr"></div>
                    <label for="ManagedVerification_Fourth" class="fl strong">Fourth</label>
                    <div class="fr"><%= Html.TextBox("Fourth", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty, new { @id = "ManagedVerification_Fourth" })%></div>
                    <div class="clr"></div>
                    <label for="ManagedVerification_Fifth" class="fl strong">Fifth</label>
                    <div class="fr"><%= Html.TextBox("Fifth", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : string.Empty, new { @id = "ManagedVerification_Fifth" })%></div>
                    <div class="clr"></div>
                    <label for="ManagedVerification_Sixth" class="fl strong">Sixth</label>
                    <div class="fr"><%= Html.TextBox("Sixth", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : string.Empty, new { @id = "ManagedVerification_Sixth" })%></div>
                </div>
            </div>
            <div class="row">
                <label for="ManagedVerification_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "ManagedVerification_AddressLine1", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "ManagedVerification_AddressLine2" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "ManagedVerification_AddressCity", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="ManagedVerification_AddressStateCode" class="fl strong">State, Zip Code</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "ManagedVerification_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "ManagedVerification_AddressZipCode", @class = "numeric required zip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsHomeHealthServiceIncluded", Model.IsHomeHealthServiceIncluded, new { @id = "ManagedVerification_IsHomeHealthServiceIncluded" })%>
                        <label for="ManagedVerification_IsHomeHealthServiceIncluded">Uncheck if the home health service line is not included</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="wide column">
            <div class="row">
                <label class="strong">Condition Codes</label>
                <div class="ac">
                    <label for="ManagedVerification_ConditionCode18" class="strong">18.</label>
                    <%= Html.TextBox("ConditionCode18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode18", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode19" class="strong">19.</label>
                    <%= Html.TextBox("ConditionCode19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode19", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode20" class="strong">20.</label>
                    <%= Html.TextBox("ConditionCode20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode20", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode21" class="strong">21.</label>
                    <%= Html.TextBox("ConditionCode21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode21", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode22" class="strong">22.</label>
                    <%= Html.TextBox("ConditionCode22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode22", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode23" class="strong">23.</label>
                    <%= Html.TextBox("ConditionCode23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode23", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode24" class="strong">24.</label>
                    <%= Html.TextBox("ConditionCode24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode24", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode25" class="strong">25.</label>
                    <%= Html.TextBox("ConditionCode25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode25", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode26" class="strong">26.</label>
                    <%= Html.TextBox("ConditionCode26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode26", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode27" class="strong">27.</label>
                    <%= Html.TextBox("ConditionCode27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode27", @class = "shortest", @maxlength = "2" })%>
                    <label for="ManagedVerification_ConditionCode28" class="strong">28.</label>
                    <%= Html.TextBox("ConditionCode28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : string.Empty, new { @id = "ManagedVerification_ConditionCode28", @class = "shortest", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="ManagedVerification_Remark" class="fl strong">Remark</label>
                <div class="ac"><%= Html.TextArea("Remark", Model.Remark, new { @id = "ManagedVerification_Remark", @class= "tall", @maxcharacters = "500" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save next">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>