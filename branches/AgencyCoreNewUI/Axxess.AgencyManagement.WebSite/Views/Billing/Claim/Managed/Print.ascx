﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<%  var location = Model.Agency.GetBranch(Model.BranchId); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<%  var diagnosis = Model.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(Model.DiagnosisCode) : null; %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Billing/Final.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : string.Empty) + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty) + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : string.Empty) + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty) : string.Empty) : string.Empty).Clean()%>",
        patientname:    "<%= Model != null ? (Model.LastName.IsNotNullOrEmpty() ? Model.LastName.Clean() + ", " : string.Empty) + (Model.FirstName.IsNotNullOrEmpty() ? Model.FirstName.Clean() : string.Empty) : string.Empty %>",
        patient:        "<%= Model != null ? (Model.AddressLine1.IsNotNullOrEmpty() ? Model.AddressLine1.Clean() + "<br/>" : string.Empty) + (Model.AddressLine2.IsNotNullOrEmpty() ? Model.AddressLine2.Clean() + "<br/>" : string.Empty) + (Model.AddressCity.IsNotNullOrEmpty() ? Model.AddressCity.Clean() + ", " : string.Empty) + (Model.AddressStateCode.IsNotNullOrEmpty() ? Model.AddressStateCode.Clean() + "  " : string.Empty) + (Model.AddressZipCode.IsNotNullOrEmpty() ? Model.AddressZipCode + "<br/>" : string.Empty) : string.Empty %>",
        mcare:          "<%= Model != null && Model.MedicareNumber.IsNotNullOrEmpty() ? Model.MedicareNumber.Clean() : string.Empty %>",
        record:         "<%= Model != null && Model.PatientIdNumber.IsNotNullOrEmpty() ? Model.PatientIdNumber.Clean() : string.Empty %>",
        gender:         "<%= Model != null && Model.Gender.IsNotNullOrEmpty() ? Model.Gender.Clean() : string.Empty %>",
        dob:            "<%= Model != null && Model.DOB.IsValid() ? Model.DOB.ToShortDateString().Clean() : string.Empty %>",
        hipps:          "<%= Model != null && Model.HippsCode.IsNotNullOrEmpty() ? Model.HippsCode.Clean() : string.Empty %>",
        matchkey:       "<%= Model != null && Model.ClaimKey.IsNotNullOrEmpty() ? Model.ClaimKey.Clean() : string.Empty %>",
        episode:        "<%= Model != null && Model.EpisodeStartDate.IsValid() ? Model.EpisodeStartDate.ToShortDateString() : string.Empty %>",
        billvisit:      "<%= Model != null && Model.FirstBillableVisitDate.IsValid() ? Model.FirstBillableVisitDate.ToShortDateString().Clean() : string.Empty %>",
        phys:           "<%= Model != null ? (Model.PhysicianLastName.IsNotNullOrEmpty() ? Model.PhysicianLastName.ToTitleCase() + ", " : string.Empty) + (Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.ToTitleCase() : string.Empty) : string.Empty %>",
        physnpi:        "<%= Model != null && Model.PhysicianNPI.IsNotNullOrEmpty() ? Model.PhysicianNPI.Clean() : string.Empty %>",
        hippspay:       "<%= Model != null && Model.ProspectivePay > 0 ? Model.ProspectivePay.ToString().Clean() : string.Empty %>",
        diag1:          "<%= diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value.Clean() : string.Empty %>",
        diag2:          "<%= diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value.Clean() : string.Empty %>",
        diag3:          "<%= diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value.Clean() : string.Empty %>",
        diag4:          "<%= diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value.Clean() : string.Empty %>",
        diag5:          "<%= diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value.Clean() : string.Empty %>",
        diag6:          "<%= diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value.Clean() : string.Empty %>"
    };
    PrintPreview.BuildSections([
        { Content: [ [ "<strong>Remarks</strong><br/><%= Model != null && Model.Remark.IsNotNullOrEmpty() ? Model.Remark.Clean() : " " %>" ] ] },
        { Content: [ [
            "",
            "<strong>Description</strong>",
            "<strong>HCPCS/HIPPS Code</strong>",
            "<strong>Service Date</strong>",
            "<strong>Service Unit</strong>",
            "<strong>Total Charges</strong>" ] ] }
<%  var total = 0.0; %>
<%  if (!Model.IsSupplyNotBillable) { %>
        ,
        { Content: [ [
            "0272",
            "Service Supplies",
            "",
            "<%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : string.Empty %>",
            "",
            "<%= string.Format("${0:#0.00}", Model.SupplyTotal) %>" ] ] }
    <%  total += Model.SupplyTotal; %>
<%  } %>
<%  if (Model != null && Model.BillVisitSummaryDatas != null && Model.BillVisitSummaryDatas.Count > 0) { %>
    <%  foreach (var visit in Model.BillVisitSummaryDatas) { %>
        ,
        { Content: [ [
            "<%= visit.RevenueCode %>",
            "Visit",
            "<%= string.Format("{0} {1}", visit.HCPCSCode, visit.Modifier) %>",
            "<%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToShortDateString() : (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToShortDateString() : string.Empty) %>",
            "<%= visit.Unit > 0 ? visit.Unit.ToString() : string.Empty %>",
            "<%= visit.Charge > 0 ? string.Format("{0:#0.00}", visit.Charge) : string.Empty %>" ] ] }
        <%  total += visit.Charge; %>
    <%  } %>
<%  } %>
        ,
        { Content: [ [
            "<span class='fl strong'>Total</span><span class='fr ar'><%= String.Format("${0:#0.00}", total) %></span>" ] ] }
    ]);
</script>