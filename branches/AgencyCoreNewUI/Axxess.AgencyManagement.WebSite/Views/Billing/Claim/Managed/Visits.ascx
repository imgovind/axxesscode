﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medicare/Final/VerifyVisits", "Billing", FormMethod.Post, new { @id = "FinalVisitVerification_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "FinalVisitVerification_Id" })%>
    <%= Html.Hidden("EpisodeId", Model.PatientId, new { @id = "FinalVisitVerification_EpisodeId" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "FinalVisitVerification_PatientId" })%>
    <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO, new { @id = "FinalVisitVerification_IsMedicareHMO" })%>
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Episode <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy")%> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy")%></h3></li>
        </ul>
    <%  if (Model != null && Model.BillVisitDatas != null && Model.BillVisitDatas.Count > 0) { %>
        <%  foreach (var billCategoryKey in Model.BillVisitDatas.Keys) { %>
            <%  var billCategoryVisits = Model.BillVisitDatas[billCategoryKey]; %>
            <%  var isBillable = billCategoryKey == BillVisitCategory.Billable; %>
            <%  if (billCategoryVisits != null && billCategoryVisits.Count > 0) { %>
        <ul>
            <li class="ac"><h3><%= billCategoryKey.GetDescription() %></h3></li>
            <li>
                <span class="grid-number"></span>
                <span class="grid-icon"></span>
                <span class="grid-20">Visit Type</span>
                <span class="grid-12">Scheduled Date</span>
                <span class="grid-12">Visit Date</span>
                <span class="grid-8">HCPCS</span>
                <span class="grid-20">Status</span>
                <span class="grid-8">Units</span>
                <span class="grid-8">Charge</span>
            </li>
        </ul>
                <%  foreach (var disciplineVisitKey in billCategoryVisits.Keys) { %>
                    <%  var visits = billCategoryVisits[disciplineVisitKey]; %>
                    <%  if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="strong"><%= disciplineVisitKey.GetDescription() %></li>
                        <%  int i = 1; %>
                        <%  foreach (var visit in visits) { %>
            <label for="<%= "Visit" + visit.EventId.ToString() %>">
                <li>
                    <span class="grid-number"><label for="<%= "Visit" + visit.EventId.ToString() %>"><%= i%>.</label></span>
                    <span class="grid-icon"><input class="radio" name="Visit" type="checkbox" id="<%= "Visit" + visit.EventId.ToString() %>" value="<%= isBillable ? visit.EventId.ToString() + "\" checked=\"checked" : visit.EventId.ToString() %>" /></span>
                    <span class="grid-20"><%= visit.DisciplineTaskName%></span>
                    <span class="grid-12"><%= visit.EventDate.IsNotNullOrEmpty() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="grid-12"><%= visit.VisitDate.IsNotNullOrEmpty() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="grid-8"><%= visit.HCPCSCode %></span>
                    <span class="grid-20"><%= visit.StatusName %></span>
                    <span class="grid-8"><%= visit.Unit %></span>
                    <span class="grid-8"><%= string.Format("${0:#0.00}", visit.Charge)%></span>
                </li>
            </label>
                            <% i++; %>
                        <%  } %>
        </ol>
                    <%  } %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="back">Back</a></li>
            <li><a class="save next">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>