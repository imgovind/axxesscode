﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<%  var valueCodes = Model != null ? Model.Ub04Locator39.ToUb04LocatorDictionary() : new Dictionary<string, Locator>(); %>
<%  var data = Model.AgencyInsurance.ToBillDataDictionary().Select(s => s.Value); %>
<div class="wrapper main">
<%  using (Html.BeginForm("ManagedInsuranceVerify", "Billing", FormMethod.Post, new { @id = "managedBillingInsuranceForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "ManagedInsurance_ClaimId" })%>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "ManagedInsurance_PatientId" })%>
    <div class="buttons">
        <ul>
            <li><a class="reload">Reload Insurance</a></li>
        </ul>
    </div>
    <div class="clr"></div>
    <fieldset>
        <legend>UB-04</legend>
        <div class="column">
            <div class="row">
                <label class="strong">UB04 Locator 81cc</label>
                <div class="margin">
                    <label for="FinalVerification_Locator1Code1" class="fl strong">a</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator1")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "FinalVerification_Locator1Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "FinalVerification_Locator1Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "FinalVerification_Locator1Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="FinalVerification_Locator2Code1" class="fl strong">b</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator2")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "FinalVerification_Locator2Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "FinalVerification_Locator2Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "FinalVerification_Locator2Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="FinalVerification_Locator3Code1" class="fl strong">c</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator3")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "FinalVerification_Locator3Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "FinalVerification_Locator3Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "FinalVerification_Locator3Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="FinalVerification_Locator4Code1" class="fl strong">d</label>
                    <%= Html.Hidden("Ub04Locator81", "Locator4")%>
                    <div class="fr">
                        <%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "FinalVerification_Locator4Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "FinalVerification_Locator4Code2", @class = "shorter", @maxlength = "10" })%>
                        <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "FinalVerification_Locator4Code3", @class = "shorter", @maxlength = "12" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="strong">UB04 Locator 39</label>
                <div class="margin">
                    <label for="FinalVerification_39Locator1Code1" class="fl strong">a</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator1")%>
                    <div class="fr">
                        <%= Html.TextBox("39Locator1_Code1", valueCodes.ContainsKey("39Locator1") ? valueCodes["39Locator1"].Code1 : "61", new { @id = "FinalVerification_39Locator1Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator1_Code2", valueCodes.ContainsKey("39Locator1") ? valueCodes["39Locator1"].Code2 : Model.CBSA.PadStringNumberWithDecimal(2), new { @id = "FinalVerification_39Locator1Code2", @class = "shorter", @maxlength = "10" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="FinalVerification_39Locator2Code1" class="fl strong">b</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator2")%>
                    <div class="fr">
                        <%= Html.TextBox("39Locator2_Code1", valueCodes.ContainsKey("Locator2") ? valueCodes["Locator2"].Code1 : string.Empty, new { @id = "FinalVerification_39Locator2Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator2_Code2", valueCodes.ContainsKey("Locator2") ? valueCodes["Locator2"].Code2 : string.Empty, new { @id = "FinalVerification_39Locator2Code2", @class = "shorter", @maxlength = "10" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="FinalVerification_39Locator3Code1" class="fl strong">c</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator3")%>
                    <div class="fr">
                        <%= Html.TextBox("39Locator3_Code1", valueCodes.ContainsKey("Locator3") ? valueCodes["Locator3"].Code1 : string.Empty, new { @id = "FinalVerification_39Locator3Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator3_Code2", valueCodes.ContainsKey("Locator3") ? valueCodes["Locator3"].Code2 : string.Empty, new { @id = "FinalVerification_39Locator3Code2", @class = "shorter", @maxlength = "10" })%>
                    </div>
                    <div class="clr"></div>
                    <label for="FinalVerification_39Locator4Code1" class="fl strong">d</label>
                    <%= Html.Hidden("Ub04Locator39", "39Locator4")%>
                    <div class="fr">
                        <%= Html.TextBox("39Locator4_Code1", valueCodes.ContainsKey("Locator4") ? valueCodes["Locator4"].Code1 : string.Empty, new { @id = "FinalVerification_39Locator4Code1", @class = "shortest", @maxlength = "2" })%>
                        <%= Html.TextBox("39Locator4_Code2", valueCodes.ContainsKey("Locator4") ? valueCodes["Locator4"].Code2 : string.Empty, new { @id = "FinalVerification_39Locator4Code2", @class = "shorter", @maxlength = "10" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visit Information</legend>
        <div class="wide column">
            <div class="row buttons">
                <ul>
                    <li><a class="add-visit-info">Add Visit Information</a></li>
                </ul>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<ChargeRate>().Name("EditManagedInsurance_BillDatas").DataKeys(keys => { keys.Add(r => r.Id).RouteKey("Id"); }).Columns(columns => {
                        columns.Bound(e => e.DisciplineTaskName).Title("Task");
                        columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                        columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
                        columns.Bound(e => e.Code).Title("HCPCS").Width(55);
                        columns.Bound(e => e.Charge).Format("${0:#0.00}").Title("Rate").Width(45);
                        columns.Bound(e => e.Modifier).Title("Modifier").Width(80);
                        columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
                        columns.Bound(e => e.TimeLimitFormat).Title("Time Limit").Width(65);
                        columns.Bound(e => e.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Insurance.BillData.Edit('<#=Id#>',$('#EditInsurance_Id').val());return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Insurance.BillData.Delete('<#=Id#>',$('#EditInsurance_Id').val(),$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Width(85);
                    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimInsuranceRates", "Billing", new { InsuranceId = Model.Id })).Sortable().Footer(false)%>
                <h2 class="hidden blue">No Visit Information Found</h2>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="back">Back</a></li>
            <li><a class="save next">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>