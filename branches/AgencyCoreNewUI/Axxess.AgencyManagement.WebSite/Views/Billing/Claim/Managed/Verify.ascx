﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<span class="wintitle">Managed Claims | <%= Model.DisplayName %></span>
<div id="ManagedVerification_TabStrip" class="wrapper main blue final">
    <ul>
	    <li class="ac">
	        <a href="#ManagedVerification_Demographics">
	            <strong>Step 1 of 5</strong>
	            <br />
	            <span>Demographics</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#ManagedVerification_Insurance">
	            <strong>Step 2 of 5</strong>
	            <br />
	            <span>Verify Insurance</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#ManagedVerification_Visits">
	            <strong>Step 3 of 5</strong>
	            <br />
	            <span>Verify Visits</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#ManagedVerification_Supplies">
	            <strong>Step 4 of 5</strong>
	            <br />
	            <span>Verify Supplies</span>
	        </a>
	    </li>
	    <li class="ac">
	        <a href="#ManagedVerification_Summary">
	            <strong>Step 5 of 5</strong>
	            <br />
	            <span>Summary</span>
	        </a>
	    </li>
    </ul>
    <div id="ManagedVerification_Demographics" class="final tab-content"><%  Html.RenderPartial("Claim/Managed/Demographics", Model); %></div>
    <div id="ManagedVerification_Insurance" class="final tab-content"></div>
    <div id="ManagedVerification_Visits" class="final tab-content"></div>
    <div id="ManagedVerification_Supplies" class="final tab-content"></div>
    <div id="ManagedVerification_Summary" class="final tab-content"></div>
</div>