﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<%  var titleCaseType = Model.ClaimType.ToTitleCase(); %>
<span class="wintitle"><%= Model.ClaimType %>s | <%= Current.AgencyName %></span>
<div id="Billing<%= titleCaseType %>Center_Content" class="main wrapper">
<%  using (Html.BeginForm("Medicare/ClaimSummary", "Billing", FormMethod.Post, new { @id = "Billing" + titleCaseType + "Center_Form" })) { %>
    <div class="buttons fr">
        <ul><li><a class="print">Print</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="filter">
            <label for="Billing<%= titleCaseType %>Center_BranchCode" class="strong">Branch</label>
            <%= Html.Hidden("Type", Model.ClaimType, new { @id = "Billing" + titleCaseType + "Center_Type" })%>
            <%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "Billing" + titleCaseType + "Center_BranchCode" })%>
        </div>
        <div class="filter">
            <label for="Billing<%= titleCaseType %>Center_InsuranceId" class="strong">Insurance</label>
            <%= Html.InsurancesMedicare("PrimaryInsurance", Model.Insurance.ToString(), true, "Unassigned Insurance", new { @id = "Billing" + titleCaseType + "Center_InsuranceId" })%>
        </div>
    </fieldset>
    <div id="Billing<%= titleCaseType %>Center_Content" class="acore-grid content"><% Html.RenderPartial("Claim/" + titleCaseType + "/Grid", Model.Claims); %></div>
    <%  if (Current.HasRight(Permissions.GenerateClaimFiles)) { %>
    <div class="buttons">
        <ul>
            <li><a class="generate">Generate Selected</a></li>
            <li><a class="generate all">Generate All Completed</a></li>
        </ul>
    </div>
    <%  } %>
<%  } %>
</div>