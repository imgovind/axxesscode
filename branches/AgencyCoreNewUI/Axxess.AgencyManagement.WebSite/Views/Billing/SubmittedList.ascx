﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Claim Submission History | <%= Current.AgencyName %></span>
<% string pageName = "SubmittedClaims"; %>
<div class="wrapper main blue">
    <div class="buttons fr">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.RebindSubmittedBatchClaims('<%= pageName %>');">Generate</a></li>
            <br />
            <li><%= Html.ActionLink("Excel Export", "SubmittedBatchClaims", "Export", new { ClaimType = "ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = pageName + "_ExportLink", @class = "excel" })%></li>
        </ul>
    </div>
    <fieldset class="grid-controls">
        <div class="filter"><label class="float-left">Claim Type:</label><%= Html.ClaimTypes("ClaimType", new { @id = pageName + "_ClaimType" })%></div>
        <div class="filter"><label class="float-left">Date From:</label><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName%>_StartDate" />To:&nbsp;<input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName%>_EndDate" /></div>
    </fieldset>
    <%= Html.Telerik().Grid<ClaimDataLean>().Name(pageName + "Grid").Columns(columns =>
{
    columns.Bound(o => o.Id).Title("Batch Id").Width(80).Sortable(true).ReadOnly();
    columns.Bound(o => o.Created).Format("{0:MM/dd/yyyy}").Title("Submission Date").Width(120).Sortable(true).ReadOnly();
    columns.Bound(o => o.Count).Title("# of claims").Sortable(false).ReadOnly();
    columns.Bound(o => o.RAPCount).Title("# of RAPs").Sortable(false).ReadOnly();
    columns.Bound(o => o.FinalCount).Title("# of Finals").Sortable(false).ReadOnly();
    columns.Bound(o => o.ActionUrl).ClientTemplate("<#= ActionUrl #>").Title("Action").Width(155);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimSubmittedList", "Billing", new { ClaimType = "ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)%>
</div>
