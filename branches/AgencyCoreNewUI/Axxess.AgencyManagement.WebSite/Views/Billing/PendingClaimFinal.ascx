﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
    <script type="text/javascript">
        function onEditFinalSnapShotDataBound(e) {
            if (e != undefined && e.dataItem != undefined && e.dataItem.PaymentDate != undefined) {
                var day = new Date(e.dataItem.PaymentDate);
                if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[3].innerHTML = '&#160;';
            }
        }
        function onFinalPendingDataBound(e) {
            if (e != undefined && e.dataItem != undefined && e.dataItem.PaymentDate != undefined) {
                var day = new Date(e.dataItem.PaymentDate);
                if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[1].innerHTML = '&#160;';
            }
        }
    </script>

<%= Html.Telerik().Grid<PendingClaimLean>().HtmlAttributes(new { @style = "position: relative;" }).ToolBar(commnds => commnds.Custom())
            .Name("Finals").DataKeys(keys =>
            {
                keys.Add(r => r.Id).RouteKey("Id");
            })
        .Columns(columns =>
        {
            columns.Bound(e => e.PaymentDate).Format("{0:MM/dd/yyyy}").Title("Payment Date").Width(90);
            columns.Bound(e => e.DisplayName).Title("Patient").ReadOnly();
            columns.Bound(e => e.MedicareNumber).Title("Medicare #").Width(80).ReadOnly();
            columns.Bound(e => e.EpisodeRange).Width(180).ReadOnly();
            columns.Bound(e => e.ClaimAmount).Format("${0:#0.00}").Width(130).ReadOnly();
            columns.Bound(e => e.Status).ClientTemplate("<label><#= StatusName #></label>");
            columns.Bound(e => e.PaymentAmount).Format("${0:#0.00}").Width(130);
            columns.Command(commands =>
            {
                commands.Edit();
            }).Width(80);
        })
        .DetailView(details => details.ClientTemplate(
                    Html.Telerik().Grid<FinalSnapShot>().HtmlAttributes(new { @style = "position:relative;" })
                                    .Name("FinalSnapShot_<#= Id #>")
                                         .DataKeys(keys =>
                                         {
                                             keys.Add(r => r.Id).RouteKey("Id");
                                             keys.Add(r => r.BatchId).RouteKey("BatchId");
                                         })
                                    .Columns(columns =>
                                    {
                                        columns.Bound(o => o.BatchId).Title("Batch Id").Width(80).ReadOnly();
                                        columns.Bound(o => o.EpisodeRange).Width(180).ReadOnly(); ;
                                        columns.Bound(o => o.ClaimDate).Format("{0:MM/dd/yyyy hh:mm tt}").Width(150).Title("Claim Date").ReadOnly();
                                        columns.Bound(o => o.PaymentDate).Format("{0:MM/dd/yyyy}").Width(90).Title("Payment Date");
                                        columns.Bound(o => o.Payment).Title("Payment Amount").Format("${0:#0.00}").Width(130);
                                        columns.Bound(o => o.Status).ClientTemplate("<label><#= StatusName #></label>");
                                        columns.Command(commands =>
                                        {
                                            commands.Edit();
                                        }).Width(80);

                                    })
                                    .DataBinding(dataBinding => dataBinding.Ajax()
                                            .Select("FinalSnapShots", "Billing", new
                                            {
                                                Id = "<#= Id #>"
                                            }).Update("UpdateFinalSnapShotsClaim", "Billing")).ClientEvents(events => events.OnEdit("Billing.onEditClaim").OnRowDataBound("onEditFinalSnapShotDataBound")).Footer(false)
                                    .ToHtmlString()))
                            .DataBinding(dataBinding => dataBinding.Ajax().Select("PendingClaimFinals", "Billing", new { branchId = Model.SelecetdBranch, insuranceId = Model.SelecetdInsurance }).Update("UpdateFinalClaim", "Billing", new { branchId = Model.SelecetdBranch, insuranceId = Model.SelecetdInsurance }))
                            .ClientEvents(events => events.OnRowDataBound("onFinalPendingDataBound").OnEdit("Billing.onEditClaim").OnDetailViewCollapse("Billing.onFinalDetailViewCollapse").OnDetailViewExpand("Billing.onFinalDetailViewExpand"))
        .Sortable().Footer(false)
    %>
<script type="text/javascript">
    $("#Finals .t-grid-toolbar").empty().html("<div class='align-center'><b>Final(s)</b></div>");
</script>