﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("UpdateManagedClaimStatus", "Billing", FormMethod.Post, new { @id = "UpdateManagedClaim_Form" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <fieldset>
        <legend>Update Information</legend>
        <div class="column">
            <%  var status = new SelectList(new[] {
                    new SelectListItem { Value = "3000", Text = "Claim Created" },
                    new SelectListItem { Value = "3005", Text = "Claim Submitted" },
                    new SelectListItem { Value = "3010", Text = "Claim Rejected" },
                    new SelectListItem { Value = "3015", Text = "Payment Pending" },
                    new SelectListItem { Value = "3020", Text = "Claim Accepted/Processing" },
                    new SelectListItem { Value = "3025", Text = "Claim With Errors" },
                    new SelectListItem { Value = "3030", Text = "Paid Claim" },
                    new SelectListItem { Value = "3035", Text = "Cancelled Claim" }
                }, "Value", "Text", Model.Status); %>
            <div class="row">
                <label class="float-left">Claim Status</label>
                <div class="fr"><%= Html.DropDownList("Status", status)%></div>
            </div>
            <div class="row">
                <label class="float-left">Payment Amount</label>
                <div class="fr">$<%= Html.TextBox("PaymentAmount", Model.Payment, new {@class = "text input_wrapper", @maxlength = "" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Payment Date</label>
                <div class="fr"><input type="text" name="PaymentDateValue" value="<%= Model.PaymentDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.PaymentDate.ToShortDateString() %>" id="PaymentDateValue" /></div>
            </div>
        </div>
        <div class="column">
            <label class="float-left">Comment</label>
            <div class="wide column">
                <div class="row"><%= Html.TextArea("Comment", Model.Comment) %></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>