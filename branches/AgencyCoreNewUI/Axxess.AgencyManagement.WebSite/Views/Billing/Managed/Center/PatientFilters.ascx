﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
 <div class="row">
    <label for="BillingManagedCenter_BranchId" class="strong">Branch</label>
    <div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", "", new { @class = "managedBillingBranchCode", @id = "BillingManagedCenter_BranchId" })%></div>
 </div>
  <div class="row">
    <label for="BillingManagedCenter_StatusId" class="strong">View</label>
    <div>
        <select id="BillingManagedCenter_StatusId" name="StatusId" class="managedBillingStatusDropDown">
            <option value="1">Active Patients</option>
            <option value="2">Discharged Patients</option>
        </select>
    </div>
   </div>
  <div class="row">
        <label for="BillingManagedCenter_InsuranceId" class="strong">Insurance</label>
        <div><%= Html.DropDownList("InsuranceId", Model.Insurances, new { @class = "managedBillingInsuranceDropDown", @id = "BillingManagedCenter_InsuranceId" })%></div>
 </div>
<div class="row">
    <label for="BillingManagedCenter_TextSearch" class="strong">Text Filter</label>
    <div><input id="BillingManagedCenter_TextSearch" name="TextSearch"  type="text" /></div>
</div>
