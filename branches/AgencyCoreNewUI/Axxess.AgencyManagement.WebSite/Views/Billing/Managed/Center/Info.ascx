﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimSnapShotViewData>" %>
<fieldset>
   <div class="wide_column">
       <div class="column">
         <div class="row"><label class="fl strong">Patient Name:</label><div class="fr"><%= Model.PatientName %></div></div>
         <div class="row"><label class="fl strong">Patient ID #:</label><div class="fr"><%= Model.PatientIdNumber %></div></div>
         <div class="row"><label class="fl strong">Patient Insurance Id #:</label><div class="fr"><%= Model.IsuranceIdNumber %></div></div>
       </div>  
       <div class="column">
        <%  if (Model.Visible)
            { %>
          <div class="row"><label class="fl strong">Insurance/Payer:</label><div class="fr"><%= Model.PayorName%></div></div>
          <div class="row"><label class="fl strong">Health Plan Id:</label><div class="fr"><%= Model.HealthPlainId%> </div></div>
          <div class="row"><label class="fl strong">Authorization Number:</label><div class="fr"><%= Model.AuthorizationNumber%></div></div>
        <%  }%>
        </div>   
    </div>
</fieldset>
<div class="one-third encapsulation">
    <div class="activity-log"><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadClaimLog('ManagedClaim','{1}','{2}');\" >Activity Logs</a>", Model.Type, Model.Id, Model.PatientId)%></div>
</div>