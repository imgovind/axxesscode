﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Managed Billing Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><%  Html.RenderPartial("Managed/Center/PatientFilters", Model); %></div>
        <div class="bottom new-patient"><%  Html.RenderPartial("Managed/Center/PatientSelector", Model.SelecetdInsurance); %></div>
    </div>
    <div id="BillingManagedMainResult" class="ui-layout-center"></div>
</div>
<%  if (Model == null ) { %>
<script type="text/javascript">
    $("#BillingManagedMainResult").html(U.MessageWarn("No Patient Found", "There are no patients to be gound which match your search criteria.  Please try a different search."));
</script>
<%  } %>