﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
 <div class="top">
    <div class="winmenu">
        <ul id="BillingManagedTopMenu">
            <li><a href="javascript:void(0);" onclick="ManagedBilling.Center.NewClaim('<%=Model %>');">New Claim</a></li>
        </ul>
    </div>
    <div id="BillingManagedClaimData"></div>
 </div>
<div class="bottom">
    <div style="width:100%;background:url('/Content/Office2007/sprite.png') repeat-x scroll 0 -352px #c3d8f1;border:1px solid #aaa;height:30px;padding-top:3.5px;padding-left:5px">
            <label class="float-left">Insurance</label><div class="float-left"><%= Html.InsurancesFilter("PrimaryInsurance", "0", new { @id = "ManagedBillingHistory_PrimaryInsuranceId", @class = "managed-insurances" })%></div>
    </div>
    <% Html.RenderPartial("Managed/Center/Claims", Model); %>
 </div>   
