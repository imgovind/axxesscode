﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimSnapShotViewData>" %>
<div class="two-thirds">
    <table width="100%">
        <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Patient Name:</td>
                                <td><label class="strong"><%= Model.PatientName %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient ID #:</td>
                                <td><label class="strong"><%= Model.PatientIdNumber %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Patient Insurance Id #:</td>
                                <td><label class="strong"><%= Model.IsuranceIdNumber %></label></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <%  if (Model.Visible) { %>
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Insurance/Payer:</td>
                                <td><label class="strong"><%= Model.PayorName %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Health Plan Id:</td>
                                <td class="align-left"><label class="strong"><%= Model.HealthPlainId %></label></td>
                            </tr>
                            <tr>
                                <td class="align-right">Authorization Number:</td>
                                <td class="align-left"><label class="strong"><%= Model.AuthorizationNumber %></label></td>
                            </tr>
                        </tbody>
                    </table>
                    <%  } %>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="one-third encapsulation">
    <div class="activity-log"><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadClaimLog('ManagedClaim','{1}','{2}');\" >Activity Logs</a>", Model.Type, Model.Id, Model.PatientId)%></div>
</div>