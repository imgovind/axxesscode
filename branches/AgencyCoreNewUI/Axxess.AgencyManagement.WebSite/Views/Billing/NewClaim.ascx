﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl <NewClaimViewData>" %>
<% using (Html.BeginForm("CreateClaim", "Billing", FormMethod.Post, new { @id = "NewClaim_Form" }))%>
<%  { %>
<% if(Model!=null && Model.EpisodeData!=null && Model.EpisodeData.Count>0){ %>
<% var listOfEpisode = (Model != null && Model.EpisodeData!=null ? Model.EpisodeData : new List<ClaimEpisode>()).Select(i => new SelectListItem { Value = i.Id.ToString(), Text = string.Format("{0}-{1}", i.StartDate.ToString("MM/dd/yyyy"), i.EndDate.ToString("MM/dd/yyyy")) });%>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <%= Html.Hidden("Type",Model.Type) %>
 <div class="form-wrapper">
      <fieldset>
        <legend>Episode Information</legend>
            <div class="row">
                <label for="NewClaim_LocationId" class="float-left">Episodes </label>
                <div class="fr"><%= Html.DropDownList("EpisodeId", listOfEpisode)%></div>
            </div>
    </fieldset>
     <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add <%=Model.Type.ToUpperCase() %></a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<%} else{ %>
  <fieldset>
    <div class="wide column">
        <label>All episodes for this patient have claims associated with them. You must delete existing claims before creating new one.</label>
    </div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
 </fieldset>
<% }%>
<%} %>