﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<span class="wintitle">Remittance Advices | <%= Current.AgencyName %></span>
<%  var pagename = "ListRemittances"; %>
<div class="wrapper main">
    <div class="buttons fr">
        <ul><li><a class="print">Print</a></li></ul>
    </div>
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="filter">
            <label for="<%= pagename %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker" name="StartDate" id="<%= pagename%>_StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" />
            <label for="<%= pagename %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker" name="EndDate" id="<%= pagename%>_EndDate" value="<%= DateTime.Now.ToShortDateString() %>" />
        </div>
        <div class="upload">
<%  using (Html.BeginForm("Remittance/Detail/Upload", "Billing", FormMethod.Post, new { @id = pagename + "_Form" })) { %>
            <div class="fl">
                <label for="<%= pagename %>_ExternalRemittanceUpload" class="strong">Select a Remittance File</label>
                <input id="<%= pagename %>_ExternalRemittanceUpload" type="file" name="Upload" class="required" />
            </div>
            <div class="buttons fr">
                <ul><li><a class="save">Upload</a></li></ul>
            </div>
<%  } %>
        </div>
    </fieldset>
    <div id="<%= pagename %>_Content" class="acore-grid content"><% Html.RenderPartial("Remittance/List/Grid", Model); %></div>
</div>