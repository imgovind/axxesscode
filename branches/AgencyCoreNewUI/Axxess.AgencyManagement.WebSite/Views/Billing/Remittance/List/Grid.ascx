﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<%  if (Model != null && Model.Count > 0) { %>
<ul>
    <li class="ac"><h3>Remittance Advices</h3></li>
    <li>
        <span class="grid-number"></span>
        <span class="grid-25 sortable">Remittance ID</span>
        <span class="grid-20 sortable">Remittance Date</span>
        <span class="grid-20 sortable">Provider Payment</span>
        <span class="grid-20 sortable">Claim Count</span>
        <span>Action</span>
    </li>
</ul>
<ol>
    <%  int count = 1; %>
    <%  foreach (var remittance in Model) { %>
    <li>
        <span class="grid-number"><%= count++ %>.</span>
        <span class="grid-25 sortable"><%= remittance.RemitId %></span>
        <span class="grid-20 sortable"><%= remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToShortDateString() : string.Empty %></span>
        <span class="grid-20 sortable"><%= string.Format("${0:#,0.00}", remittance.PaymentAmount) %></span>
        <span class="grid-20 sortable"><%= remittance.TotalClaims %></span>
        <span><a class="link" onclick="Billing.Remittance.Detail.Load('<%= remittance.Id %>');return false">Details</a> | <a class="link" onclick="Billing.Remittance.Detail.Delete('<%= remittance.Id %>');return false">Delete</a></span>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<ul>
    <li class="ac"><h3>Remittance Advices</h3></li>
</ul>
<h1 class="blue">No Remittance Advices Found</h1>
<%  } %>