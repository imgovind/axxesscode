﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RemittanceListViewData>" %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Billing/Remittances.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = { agency: "<%= Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase().Clean() + "<br/>" : "") + (Model.Agency.MainLocation != null ? (Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase().Clean() : "") + (Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase().Clean() + "<br/>" : "<br/>") + (Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase().Clean() + ", " : "") + (Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper().Clean() + "  " : "") + (Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode.Clean() : "") + (Model.Agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br/>Phone: " + Model.Agency.MainLocation.PhoneWorkFormatted.Clean() : "") + (Model.Agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + Model.Agency.MainLocation.FaxNumberFormatted.Clean() : "") : "") : "" %>" };
    PrintPreview.BuildSections([
<%  int i = 0; %>
<%  if (Model != null && Model.Remittances.Count() > 0) { %>
    <%  foreach (var remittance in Model.Remittances) { %>
        <%  if (i > 0) { %>
        ,
        <%  } %>
        { Content: [ [
            "<%= (++i).ToString() %>",
            "<%= remittance.RemitId %>",
            "<%= remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToShortDateString() : string.Empty %>",
            "<%= string.Format("${0:#,0.00}", remittance.PaymentAmount) %>",
            "<%= remittance.TotalClaims.ToString() %>" ] ] }
    <%  } %>
<%  } %>
    ]);
</script>