﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<span class="wintitle">Remittance Advice Details | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%= Html.Hidden("Id", Model.Id, new { @id = "RemittanceDetail_Id" }) %>
<a class="grid-refresh hidden"></a>
    <div class="buttons">
        <ul>
            <li><a class="post all">Post Selected</a></li>
            <li><a class="print">Print</a></li>
        </ul>
    </div>
<%  if (Model != null) { %>
    <%  var data = Model.RemittanceData ?? new RemittanceData(); %>
    <fieldset>
        <legend>Remittance Advice Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Check Number</label>
                <div class="fr"><%= data.CheckNo %></div>
            </div>
            <div class="row">
                <label class="fl strong">Payment Total</label>
                <div class="fr"><%= String.Format("${0:#,0.00}", Model.PaymentAmount) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Total Claims</label>
                <div class="fr"><%= Model.TotalClaims %></div>
            </div>
            <div class="row">
                <label class="fl strong">Remittance Date</label>
                <div class="fr"><%= Model.RemittanceDate > DateTime.MinValue ? Model.RemittanceDate.ToShortDateString() : string.Empty %></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Payer</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Name</label>
                <div class="fr"><%= data.Payer != null ? data.Payer.Name : string.Empty %></div>
            </div>
    <%  if (data.Payer != null && data.Payer.RefType.IsNotNullOrEmpty()) { %>
            <div class="row">
                <label class="fl strong"><%= data.Payer.RefType %></label>
                <div class="fr"><%= data.Payer.RefNum %></div>
            </div>
    <%  } %>
            <div class="row">
                <label class="fl strong">Address 1</label>
                <div class="fr"><%= data.Payer != null ? data.Payer.Add1 : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">Address 2</label>
                <div class="fr"><%= data.Payer != null ? data.Payer.Add2 : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">City</label>
                <div class="fr"><%= data.Payer != null ? data.Payer.City : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">State</label>
                <div class="fr"><%= data.Payer != null ? data.Payer.State : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">Zip</label>
                <div class="fr"><%= data.Payer != null ? data.Payer.Zip : string.Empty %></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Payee</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Name</label>
                <div class="fr"><%= data.Payee != null ? data.Payee.Name : string.Empty %></div>
            </div>
    <%  if (data.Payee != null && data.Payee.RefType.IsNotNullOrEmpty()) { %>
            <div class="row">
                <label class="fl strong"><%= data.Payee.RefType %></label>
                <div class="fr"><%= data.Payee.RefNum %></div>
            </div>
    <%  } %>
            <div class="row">
                <label class="fl strong">Address 1</label>
                <div class="fr"><%= data.Payee != null ? data.Payee.Add1 : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">Address 2</label>
                <div class="fr"><%= data.Payee != null ? data.Payee.Add2 : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">City</label>
                <div class="fr"><%= data.Payee != null ? data.Payee.City : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">State</label>
                <div class="fr"><%= data.Payee != null ? data.Payee.State : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">Zip</label>
                <div class="fr"><%= data.Payee != null ? data.Payee.Zip : string.Empty %></div>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <div class="content acore-grid"><% Html.RenderPartial("Remittance/Detail/Grid", Model); %></div>
<%  } %>
</div>