﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<%  var RemData = Model.Data.ToObject<RemittanceData>(); %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Billing/Remittance.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase().Clean() + "<br/>" : "") + (Model.Agency.MainLocation != null ? (Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase().Clean() : "") + (Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase().Clean() + "<br/>" : "<br/>") + (Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase().Clean() + ", " : "") + (Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper().Clean() + "  " : "") + (Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode.Clean() : "") + (Model.Agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br/>Phone: " + Model.Agency.MainLocation.PhoneWorkFormatted.Clean() : "") + (Model.Agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + Model.Agency.MainLocation.FaxNumberFormatted.Clean() : "") : "") : "" %>",
<%  if (RemData.Payer != null && RemData.Payer.RefType.IsNotNullOrEmpty()) { %>
        PayerLabel1:    "<%= RemData.Payer.RefType.Clean() + ": " %>",
        PayerAnswer1:   "<%= RemData.Payer.RefNum.IsNotNullOrEmpty() ? RemData.Payer.RefNum.Clean() : string.Empty %>",
<%  } %>
<%  if (RemData.Payee != null && RemData.Payee.RefType.IsNotNullOrEmpty()) { %>
        PayeeLabel1:    "<%= RemData.Payee.RefType.Clean() + ": " %>",
        PayeeAnswer1:   "<%= RemData.Payee.RefNum.IsNotNullOrEmpty() ? RemData.Payee.RefNum.Clean() : string.Empty %>",
<%  } %>
<%  if (RemData.Payee != null && RemData.Payee.IdType.IsNotNullOrEmpty()) { %>
        PayeeLabel2:    "<%= RemData.Payee.IdType.Clean() + ": " %>",
        PayeeAnswer2:   "<%= RemData.Payee.Id.IsNotNullOrEmpty() ? RemData.Payee.Id.Clean() : string.Empty %>",
<%  } %>
        checknum:       "<%= RemData.CheckNo.IsNotNullOrEmpty() ? RemData.CheckNo.Clean() : string.Empty %>",
        total:          "<%= string.Format("${0:#,0.00}", Model.PaymentAmount).Clean() %>",
        claims:         "<%= Model.TotalClaims.ToString() %>",
        date:           "<%= Model.RemittanceDate.IsValid() ? Model.RemittanceDate.ToShortDateString().Clean() : string.Empty %>",
        PayerName:      "<%= RemData.Payer != null && RemData.Payer.Name.IsNotNullOrEmpty() ? RemData.Payer.Name.Clean() : string.Empty %>",
        PayerAddress1:  "<%= RemData.Payer != null && RemData.Payer.Add1.IsNotNullOrEmpty() ? RemData.Payer.Add1.Clean() : string.Empty %>",
        PayerAddress2:  "<%= RemData.Payer != null && RemData.Payer.Add2.IsNotNullOrEmpty() ? RemData.Payer.Add2.Clean() : string.Empty %>",
        PayerCity:      "<%= RemData.Payer != null && RemData.Payer.City.IsNotNullOrEmpty() ? RemData.Payer.City.Clean() : string.Empty %>",
        PayerState:     "<%= RemData.Payer != null && RemData.Payer.State.IsNotNullOrEmpty() ? RemData.Payer.State.Clean() : string.Empty %>",
        PayerZip:       "<%= RemData.Payer != null && RemData.Payer.Zip.IsNotNullOrEmpty() ? RemData.Payer.Zip.Clean() : string.Empty %>",
        PayeeName:      "<%= RemData.Payee != null && RemData.Payee.Name.IsNotNullOrEmpty() ? RemData.Payee.Name.Clean() : string.Empty %>",
        PayeeAddress1:  "<%= RemData.Payee != null && RemData.Payee.Add1.IsNotNullOrEmpty() ? RemData.Payee.Add1.Clean() : string.Empty %>",
        PayeeAddress2:  "<%= RemData.Payee != null && RemData.Payee.Add2.IsNotNullOrEmpty() ? RemData.Payee.Add2.Clean() : string.Empty %>",
        PayeeCity:      "<%= RemData.Payee != null && RemData.Payee.City.IsNotNullOrEmpty() ? RemData.Payee.City.Clean() : string.Empty %>",
        PayeeState:     "<%= RemData.Payee != null && RemData.Payee.State.IsNotNullOrEmpty() ? RemData.Payee.State.Clean() : string.Empty %>",
        PayeeZip:       "<%= RemData.Payee != null && RemData.Payee.Zip.IsNotNullOrEmpty() ? RemData.Payee.Zip.Clean() : string.Empty %>"
    };
    PrintPreview.BuildSections([
<%  if (RemData.Claim != null && RemData.Claim.Count > 0) { %>
    <%  int count = 0; %>
    <%  foreach (var claim in RemData.Claim) { %>
        <%  if (claim.ProviderLevelAdjustment != null) { %>
            <%  if (count > 0) { %>
        ,
            <%  } %>
        {   Title: "Provider Level Adjustment",
            Content: [
                [ "<strong>Provider Identifier</strong>", "<%= claim.ProviderLevelAdjustment.ProviderIdentifier.Clean() %>", "<strong>Fiscal Period Date</strong>", "<%= claim.ProviderLevelAdjustment.FiscalPeriodDate.IsValidPHPDate() ? claim.ProviderLevelAdjustment.FiscalPeriodDate.ToDateTimePHP().ToShortDateString().Clean() : string.Empty %>" ],
                [ "<strong>Adjustment Reason</strong>", "<%= claim.ProviderLevelAdjustment.AdjustmentReasonDesc.Clean() %>", "<strong>Adjustment Amount</strong>", "<%= string.Format("${0:#,0.00}", claim.ProviderLevelAdjustment.ProviderAdjustmentAmount).Clean() %>" ] ] }
            <%  if (count == 0) { %>
        ,
            <%  } %>
        <%  } %>
        <%  if (claim.ClaimPaymentInformation != null && claim.ClaimPaymentInformation.Count > 0) { %>
            <%  foreach (var claimPaymentInfo in claim.ClaimPaymentInformation) { %>
                <%  if (count > 0) { %>
        ,
                <%  } %>
        {   Content: [
                [ "<strong>Patient Name</strong>", "<%= claimPaymentInfo.Patient != null ? String.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName).Clean() : string.Empty %>", "<strong><%= claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty() ? claimPaymentInfo.Patient.IdQualifierName.Clean() : string.Empty %></strong>", "<%= claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty() ? claimPaymentInfo.Patient.Id.Clean() : string.Empty %>" ],
                [ "<strong>Patient Control Number</strong>", "<%= claimPaymentInfo.PatientControlNumber.Clean() %>", "<strong>ICN Number</strong>", "<%= claimPaymentInfo.PayerClaimControlNumber.Clean() %>" ],
                [ "<strong>Start Date</strong>", "<%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToShortDateString().Clean() : string.Empty %>", "<strong>End Date</strong>", "<%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToShortDateString().Clean() : string.Empty %>" ],
                [ "<strong>Type Of Bill</strong>", "<%= claimPaymentInfo.TypeOfBill.Clean() %>", "<strong>Claim Status</strong>", "<%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode).Clean() %>" ],
                [ "<strong>Claim Number</strong>", "<%= (++count).ToString() %>", "<strong>Reported Charge</strong>", "<%= string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount).Clean() %>" ],
                [ "<strong>Remittance</strong>", "<%= string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount).Clean() %>", "<strong>Line Adjustment Amount</strong>", "<%= string.Format("${0:#,0.00}", claimPaymentInfo.ServiceAdjustmentTotal).Clean() %>" ],
                [ "", "", "<strong>Paid Amount</strong>", "<%= string.Format("${0:#,0.00}", claimPaymentInfo.ClaimPaymentAmount).Clean() %>" ] ] }
            <%  } %>
        <%  } %>
    <%  } %>
<%  } else { %>
        { Title: "No Remittances Found", Content: [ [ " " ] ] }
<%  } %>
    ]);
</script>