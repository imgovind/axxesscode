﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<%  if (Model != null && Model.RemittanceData != null && Model.RemittanceData.Claim != null && Model.RemittanceData.Claim.Count > 0) { %>
<ul>
    <li><h3 class="ac">Claim<%= Model.RemittanceData.Claim.Count > 0 ? "s" : string.Empty %></h3></li>
</ul>
<ol>
    <%  foreach (var claim in Model.RemittanceData.Claim) { %>
        <%  if (claim.ProviderLevelAdjustment != null) { %>
    <li>
        <div class="wide column">
            <div class="row strong ac">Provider Level Adjustment</div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Provider Identifier</label>
                <div class="fr"><%= claim.ProviderLevelAdjustment.ProviderIdentifier %></div>
            </div>
            <div class="row">
                <label class="fl strong">Fiscal Period Date</label>
                <div class="fr"><%= claim.ProviderLevelAdjustment.FiscalPeriodDate.IsValidPHPDate() ? claim.ProviderLevelAdjustment.FiscalPeriodDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Adjustment Reason</label>
                <div class="fr"><%= claim.ProviderLevelAdjustment.AdjustmentReasonDesc %></div>
            </div>
            <div class="row">
                <label class="fl strong">Adjustment Amount</label>
                <div class="fr"><%= string.Format("${0:#,0.00}", claim.ProviderLevelAdjustment.ProviderAdjustmentAmount) %></div>
            </div>
        </div>
        <div class="clr"></div>
    </li>
        <%  } %>
        <%  if (claim.ClaimPaymentInformation != null && claim.ClaimPaymentInformation.Count > 0) { %>
            <%  int i = 1; %>
            <%  foreach (var claimPaymentInfo in claim.ClaimPaymentInformation) { %>
    <li>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient Name</label>
                <div class="fr"><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty %><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty %></div>
            </div>
                <%  if (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty()) { %>
            <div class="row">
                <label class="fl strong"><%= claimPaymentInfo.Patient.IdQualifierName %></label>
                <div class="fr"><%= claimPaymentInfo.Patient.Id %></div>
            </div>
                <%  } %>
            <div class="row">
                <label class="fl strong">Patient Control Number</label>
                <div class="fr"><%= claimPaymentInfo.PatientControlNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">ICN Number</label>
                <div class="fr"><%= claimPaymentInfo.PayerClaimControlNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Start Date</label>
                <div class="fr"><%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">End Date</label>
                <div class="fr"><%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Type of Bill</label>
                <div class="fr"><%= claimPaymentInfo.TypeOfBill %></div>
            </div>
            <div class="row">
                <label class="fl strong">Claim Status</label>
                <div class="fr"><%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Claim Number</label>
                <div class="fr"><%= i++ %></div>
            </div>
            <div class="row">
                <label class="fl strong">Reported Charge</label>
                <div class="fr"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Remittance</label>
                <div class="fr"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Line Adjustment Amount</label>
                <div class="fr"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ServiceAdjustmentTotal) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Paid Amount</label>
                <div class="fr"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ClaimPaymentAmount) %></div>
            </div>
        </div>
        <div class="clr"></div>
                <%  if (!claimPaymentInfo.IsPosted && claimPaymentInfo.AssociatedClaims != null && claimPaymentInfo.AssociatedClaims.Count > 0) { %>
                <%  int j = 1; %>
        <div class="acore-grid">
            <ul>
                <li>
                    <span class="grid-icon"></span>
                    <span class="grid-20 sortable">Patient ID</span>
                    <span class="grid-20 sortable">Patient Name</span>
                    <span class="grid-20 sortable">Episode</span>
                    <span class="grid-20 sortable">Claim Date</span>
                    <span class="sortable">Batch ID</span>
                </li>
            </ul>
            <ol>
                    <%  foreach (var cl in claimPaymentInfo.AssociatedClaims) { %>
                <li>
                    <span class="grid-icon"><%= string.Format("<input id=\"Episodes_{3}\" type=\"checkbox\" value=\"{0}|{1}|{2}\" name=\"episodes\" />", cl.EpisodeId, cl.BatchId, claimPaymentInfo.PayerClaimControlNumber , j++) %></span>
                    <span class="grid-20"><%= cl.PatientIdNumber %></span>
                    <span class="grid-20"><%= cl.DisplayName %></span>
                    <span class="grid-20"><%= cl.EpisodeRange %></span>
                    <span class="grid-20"><%= cl.ClaimDateFormatted %></span>
                    <span><%= cl.BatchId %></span>
                </li>
                    <%  } %>
            </ol>
        </div>
        <div class="buttons">
            <ul>
                <li><a class="post">Post</a></li>
            </ul>
        </div>
                <%  } %>
    </li>
            <%  } %>
        <%  } %>
    <%  } %>
</ol>
<%  } %>