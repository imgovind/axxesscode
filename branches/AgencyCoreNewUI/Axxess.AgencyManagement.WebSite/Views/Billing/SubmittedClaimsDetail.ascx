﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimInfoDetail>>" %>
<span class="wintitle">Submitted Claims Detail | <%= Current.AgencyName %></span>
<div class="wrapper">
<%= Html.Telerik().Grid(Model)
        .Name("ClaimInfoDetail_List")
        .Columns(columns =>
        {
            columns.Bound(o => o.DisplayName);
            columns.Bound(o => o.PatientIdNumber).Width(200);
            columns.Bound(o => o.Range).Title("Episode");
            columns.Bound(o => o.BillType).Format("{0:MM/dd/yyyy}").Width(120);
        }).Footer(false)
%>
</div>