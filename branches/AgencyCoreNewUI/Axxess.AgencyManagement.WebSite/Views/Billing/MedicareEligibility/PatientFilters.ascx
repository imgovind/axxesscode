﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="row">
    <label for="PatientCharts_BranchId" class="strong">Branch</label>
    <div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", "", new { @id = "PatientCharts_BranchId" })%></div>
</div>
<div class="row">
    <label for="PatientCharts_StatusId" class="strong">Status</label>
    <div>
        <select name="StatusId" id="PatientCharts_StatusId">
            <option value="1">Active Patients</option>
            <option value="2">Discharged Patients</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="PatientCharts_PaymentSourceId" class="strong">Pay Source</label>
    <div>
        <select name="PaymentSourceId" id="PatientCharts_PaymentSourceId">
            <option value="0">All</option>
            <option value="1">Medicare (traditional)</option>
            <option value="2">Medicare (HMO/managed care)</option>
            <option value="3">Medicaid (traditional)</option>
            <option value="4">Medicaid (HMO/managed care)</option>
            <option value="5">Workers' compensation</option>
            <option value="6">Title programs</option>
            <option value="7">Other government</option>
            <option value="8">Private</option>
            <option value="9">Private HMO/managed care</option>
            <option value="10">Self Pay</option>
            <option value="11">Unknown</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="PatientCharts_TextSearch" class="strong">Text Filter</label>
    <div><input id="PatientCharts_TextSearch" name="TextSearch" type="text" /></div>
</div>