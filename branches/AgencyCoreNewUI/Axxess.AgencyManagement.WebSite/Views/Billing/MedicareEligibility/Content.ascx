﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MedicareEligibility>>" %>
<span class="wintitle">Medicare Eligibility Report | <%= Current.AgencyName %></span>
<div class="main wrapper acore-grid">
<%  if (Model != null && Model.Count > 0) { %>
    <ul>
        <li class="ac"><h3>Medicare Eligibility Reports</h3></li>
        <li>
            <span class="grid-20">Task</span>
            <span class="grid-20">Episode</span>
            <span class="grid-20">Assigned</span>
            <span class="grid-20">Status</span>
            <span>Date</span>
        </li>
    </ul>
    <ol>
    <%  foreach (var item in Model) { %>
        <li>
            <span class="grid-20"><%= item.TaskName %></span>
            <span class="grid-20"><%= item.EpisodeRange %></span>
            <span class="grid-20"><%= item.AssignedTo %></span>
            <span class="grid-20"><%= item.StatusName %></span>
            <span class="grid-12"><%= item.Created.ToShortDateString().ToZeroFilled() %></span>
            <span><%= item.PrintUrl %></span>
        </li>
    <%  } %>
    </ol>
<%  } else { %>
    <ul>
        <li class="ac"><h3>Medicare Eligibility Reports</h3></li>
    </ul>
    <h1 class="blue">No Medicare Eligibility Reports for this Patient</h1>
<%  } %>
</div>