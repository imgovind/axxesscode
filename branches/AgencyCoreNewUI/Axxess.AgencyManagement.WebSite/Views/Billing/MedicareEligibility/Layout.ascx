﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Medicare Eligibility Reports | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("MedicareEligibility/PatientFilters"); %></div>
        <div class="bottom"><% Html.RenderPartial("MedicareEligibility/PatientSelector"); %></div>
    </div>
    <div id="MedicareEligibilityMainResult" class="ui-layout-center"></div>
</div>