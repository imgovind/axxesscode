﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
<fieldset>
    <legend><%= Model.Type %></legend>
<%  if (Model.Visible) { %>
    <div class="column">
        <div class="row">
            <label class="fl strong">Patient Name</label>
            <div class="fr"><%= Model.PatientName %></div>
        </div>
        <div class="row">
            <label class="fl strong">Patient MRN</label>
            <div class="fr"><%= Model.PatientIdNumber %></div>
        </div>
        <div class="row">
            <label class="fl strong">Medicare Number</label>
            <div class="fr"><%= Model.MedicareNumber %></div>
        </div>
        <div class="row">
            <label class="fl strong">Insurance/Payer</label>
            <div class="fr"><%= Model.PayorName %></div>
        </div>
        <div class="row">
            <label class="fl strong">HIPPS</label>
            <div class="fr"><%= Model.HIPPS %></div>
        </div>
        <div class="row">
            <label class="fl strong">Claim Key</label>
            <div class="fr"><%= Model.ClaimKey %></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl strong">HHRG (Grouper)</label>
            <div class="fr"><%= Model.ProspectivePayment.Hhrg %></div>
        </div>
        <div class="row">
            <label class="fl strong">Labor Portion</label>
            <div class="fr"><%= Model.ProspectivePayment.LaborAmount %></div>
        </div>
        <div class="row">
            <label class="fl strong">Non-Labor</label>
            <div class="fr"><%= Model.ProspectivePayment.NonLaborAmount %></div>
        </div>
        <div class="row">
            <label class="fl strong">Supply Reimbursement</label>
            <div class="fr"><%= Model.ProspectivePayment.NonRoutineSuppliesAmount %></div>
        </div>
        <div class="row">
            <label class="fl strong">Episode Prospective Pay</label>
            <div class="fr"><%= Model.ProspectivePayment.TotalProspectiveAmount %></div>
        </div>
        <div class="row">
            <label class="fl strong">Claim Prospective Pay</label>
            <div class="fr"><%= Model.ProspectivePayment.ClaimAmount %></div>
        </div>
    </div>
<%  } else { %>
    <div class="column">
        <div class="row">
            <label class="fl strong">Patient Name</label>
            <div class="fr"><%= Model.PatientName %></div>
        </div>
        <div class="row">
            <label class="fl strong">Patient MRN</label>
            <div class="fr"><%= Model.PatientIdNumber %></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl strong">Medicare Number</label>
            <div class="fr"><%= Model.MedicareNumber %></div>
        </div>
        <div class="row">
            <label class="fl strong">Insurance/Payer</label>
            <div class="fr"><%= Model.PayorName %></div>
        </div>
    </div>
<%  } %>
</fieldset>
<div class="activity-log fr"><span class="img icon activity"></span></div>
<div class="buttons">
    <ul>
        <li><a class="remittance">Remittance</a></li>
    </ul>
</div>