﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Medicare Claim History | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("History/Medicare/PatientFilters"); %></div>
        <div class="bottom"><% Html.RenderPartial("History/Medicare/PatientSelector"); %></div>
    </div>
    <div id="BillingHistoryMainResult" class="ui-layout-center"></div>
</div>