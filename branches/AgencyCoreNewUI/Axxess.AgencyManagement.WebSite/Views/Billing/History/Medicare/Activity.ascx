﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%  Html.Telerik().Grid<ClaimHistoryLean>().Name("BillingHistoryActivityGrid").Columns(columns => {
        columns.Bound(p => p.Type).Title("Type").Width("4%");
        columns.Bound(p => p.EpisodeRange).Title("Episode Range").Width("15%");
        columns.Bound(p => p.StatusName).Title("Status").Width("20%");
        columns.Bound(p => p.ClaimAmount).Title("Claim Amount").Format("${0:#0.00}").Width("12%");
        columns.Bound(p => p.PaymentAmount).Title("Payment Amount").Format("${0:#0.00}").Width("12%");
        columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width("12%");
        columns.Bound(p => p.Id).Title("Print").ClientTemplate("<a class=\"link\" onclick=\"Billing.Claim.PrintUB04('<#=Id#>', '<#=PatientId#>', '<#=Type#>');return false\">UB-04</a>").Width("10%");
        columns.Bound(p => p.Id).Title("Action").ClientTemplate("<a class=\"link\" onclick=\"Billing.Claim.Edit('<#=Id#>', '<#=PatientId#>', '<#=Type#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Billing.Claim.Delete('<#=Id#>', '<#=PatientId#>', '<#=Type#>');return false\">Delete</a>").Width("15%");
        columns.Bound(p => p.PatientId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DetailView(details => details.ClientTemplate(
        Html.Telerik().Grid<ClaimSnapShotViewData>().HtmlAttributes(new { @style = "position:relative;" }).Name("ClaimSnapShot_<#= Id #><#= Type #>").Columns(columns => {
            columns.Bound(o => o.BatchId).Title("Batch Id").Width(15);
            columns.Bound(o => o.EpisodeRange).Width(20);
            columns.Bound(o => o.ClaimDate).Title("Claim Date").Width(15);
            columns.Bound(o => o.PaymentDate).Format("{0:MM/dd/yyyy}").Title("Payment Date").Width(15);
            columns.Bound(o => o.PaymentAmount).Format("${0:#0.00}").Title("Payment Amount").Width(15);
            columns.Bound(o => o.StatusName).Title("Status").Width(25);
            columns.Bound(o => o.BatchId).Title("Action").ClientTemplate("<a class=\"link\" onclick=\"Billing.Claim.<#=Type#>.Snapshot.Edit('<#=BatchId#>', '<#=Id#>');return false\">Edit</a>").Width(15);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("SnapShotClaims", "Billing", new { Id = "<#= Id #>", Type = "<#= Type #>" })).ClientEvents(events => events.OnRowDataBound("onEditSnapShotDataBound")).Footer(false).ToHtmlString()
    )).DataBinding(dataBinding => dataBinding.Ajax().Select("HistoryMedicareActivityGrid", "Billing", new { patientId = Model != null ? Model : Guid.Empty })).ClientEvents(events => events.OnDataBound("Billing.History.Medicare.Activity.Init").OnRowSelected("Billing.History.Medicare.Activity.Select").OnDetailViewCollapse("Billing.onClaimDetailViewCollapse").OnDetailViewExpand("Billing.onClaimDetailViewExpand")).Sortable().Selectable().Scrollable().Footer(false).Render(); %>