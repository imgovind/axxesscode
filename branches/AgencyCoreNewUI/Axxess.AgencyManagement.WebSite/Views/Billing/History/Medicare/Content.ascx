﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BillingHistoryViewData>" %>
<div class="top">
    <div class="winmenu">
        <ul>
            <li><a class="new-rap">New RAP</a></li>
            <li><a class="new-final">New Final</a></li>
        </ul>
    </div>
    <div class="wrapper main info"><% if (!Model.ClaimInfo.Id.IsEmpty()) Html.RenderPartial("History/Medicare/Info", Model.ClaimInfo); %></div>
</div>
<div class="bottom"><% Html.RenderPartial("History/Medicare/Activity", Model.ClaimInfo.PatientId); %></div>