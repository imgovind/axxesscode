﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  Html.Telerik().Grid<PatientSelection>().Name("BillingHistorySelectionGrid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name");
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding
        .Ajax().Select("GridAllMedicare", "Patient", new { branchId = Guid.Empty, statusId = 1, paymentSourceId = 0 })
    ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
        .OnDataBound("Billing.History.Medicare.PatientSelector.Init")
        .OnRowSelected("Billing.History.Medicare.PatientSelector.Select")
    ).Render(); %>