﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BillingHistoryViewData>" %>
<%  if (Model.ClaimInfo != null) { %>
    <div class="top">
        <div class="winmenu">
            <ul>
                <li><a class="new-rap">New RAP</a></li>
                <li><a class="new-final">New Final</a></li>
            </ul>
        </div>
        <div id="BillingHistoryClaimData" class="wrapper main"><% Html.RenderPartial("Claim/History/Info", Model.ClaimInfo); %></div>
    </div>
    <div class="bottom"><% Html.RenderPartial("Claim/History/Activity", Model.ClaimInfo.PatientId); %></div>
<%  } else { %>
<script type="text/javascript">
    $("#BillingHistoryMainResult").html(U.MessageError("No Patient Information", "No patient information can be found.  Please contact Axxess for assistance."));
</script>
<%  } %>
