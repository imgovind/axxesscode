﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Patient Charts | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("Claim/History/PatientFilters"); %></div>
        <div class="bottom new-patient"><% Html.RenderPartial("Claim/History/PatientSelector"); %></div>
    </div>
    <div id="BillingHistoryMainResult" class="ui-layout-center"></div>
</div>