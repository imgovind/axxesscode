﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RecertEvent>>" %>
<% string pagename = "UpcomingRecetSchedule"; %>
<div class="wrapper main">
     <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
     <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
     <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%> 
        <div class="buttons fr">
            <ul><li><a class="report-generate">Generate Report</a></li></ul> 
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportUpcomingRecetSchedule", new { BranchCode = ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"] != null ? ViewData["ManLocationId"].ToString() : Guid.Empty.ToString(), InsuranceId = ViewData.ContainsKey("Payor") && ViewData["Payor"] != null ? ViewData["Payor"].ToString() : "0" }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
        </div>
    <fieldset  class="grid-controls">
        <legend> Upcoming Recet.</legend>
        <div class="wide-column">
            <label class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"] != null ? ViewData["ManLocationId"].ToString() : Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input insurance-list" })%></div><div class="clear"></div>
            <label class="float-left">Insurance</label><div class="fr"><%= Html.Insurances("InsuranceId", ViewData.ContainsKey("Payor") && ViewData["Payor"] != null ? ViewData["Payor"].ToString() : "0", new { @id = pagename + "_InsuranceId", @class = "Insurances report_input" })%></div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer" >
       <% Html.RenderPartial("Schedule/Content/UpcomingRecet", Model); %>
    </div>
</div>



