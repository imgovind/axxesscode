﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "PatientWeeklySchedule"; %>
<div class="wrapper main">
  <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
           <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
           <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%> 
        <div class="buttons fr">
            <ul><li><a class="report-generate">Generate Report</a></li></ul> 
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientWeeklySchedule", new { PatientId = Guid.Empty }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
        </div>
    <fieldset  class="grid-controls">
        <legend>Patient Weekly Schedule</legend>
        <div class="wide-column">
              <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input patient-list" })%></div><div class="clear"></div>
              <label  class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input patient-list"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div><div class="clear"></div>
              <label  class="float-left">Patient</label><div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, "PatientId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_PatientId", @class = "report_input" })%></div>
        </div>
     </fieldset>
     <div id="<%= pagename %>GridContainer" >
       <% Html.RenderPartial("Schedule/Content/PatientWeeklySchedule", Model); %>
    </div>
</div>

