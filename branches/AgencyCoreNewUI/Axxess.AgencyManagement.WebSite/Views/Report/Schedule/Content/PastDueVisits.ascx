﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "PastDueVisitsSchedule"; %>
<% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
                 .Columns(columns =>
                 {
                 columns.Bound(m => m.PatientIdNumber).Title("ID").Width(70);
                 columns.Bound(m => m.PatientName).Title("Patient Name");
                 columns.Bound(m => m.DisciplineTaskName).Sortable(false).Title("Task");
                 columns.Bound(m => m.EventDate).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(100);
                 columns.Bound(m => m.StatusName).Sortable(false).Title("Status");
                 columns.Bound(p => p.UserDisplayName).Title("User Name").Width(155);
               })
                           .Sortable(sorting =>
              sorting.SortMode(GridSortMode.SingleColumn)
                  .OrderBy(order =>
                  {
                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                      if (sortName == "PatientIdNumber")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientIdNumber).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientIdNumber).Descending();
                          }

                      }
                      else if (sortName == "PatientName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientName).Descending();
                          }

                      }
                      else if (sortName == "UserDisplayName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.UserName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.UserName).Descending();
                          }

                      }

                  })
                  )
               .Scrollable().Footer(false)  %>
