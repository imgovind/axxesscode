﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "EmployeeWeeklySchedule"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Title("Task").Sortable(false);
         columns.Bound(p => p.StatusName).Title("Status").Sortable(false);
         columns.Bound(p => p.ScheduleDateFormatted).Title("Schedule Date").Sortable(false).Width(120);
         columns.Bound(p => p.VisitDateFormatted).Title("Visit Date").Sortable(false).Width(110);
         })
                                     .Sortable(sorting =>
                                                          sorting.SortMode(GridSortMode.SingleColumn)
                                                              .OrderBy(order =>
                                                              {
                                                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                  if (sortName == "PatientName")
                                                                  {
                                                                      if (sortDirection == "ASC")
                                                                      {
                                                                          order.Add(o => o.PatientName).Ascending();
                                                                      }
                                                                      else if (sortDirection == "DESC")
                                                                      {
                                                                          order.Add(o => o.PatientName).Descending();
                                                                      }

                                                                  }

                                                              })
                                                      )
                                         .Scrollable()
                                                 .Footer(false)%>
   

