﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RecertEvent>>" %>
<% string pagename = "PastDueRecetSchedule"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
    {
        columns.Bound(r => r.PatientName);
        columns.Bound(r => r.PatientIdNumber).Title("MR#").Width(120);
        columns.Bound(r => r.AssignedTo).Title("Employee Responsible");
        columns.Bound(r => r.StatusName).Title("Status").Sortable(false);
        columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120);
        columns.Bound(r => r.DateDifference).Title("Past Dates").Sortable(false).Width(60);
    })
                                   .Sortable(sorting =>
                                       sorting.SortMode(GridSortMode.SingleColumn)
                                           .OrderBy(order =>
                                           {
                                               var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                               var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                               if (sortName == "PatientIdNumber")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.PatientIdNumber).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.PatientIdNumber).Descending();
                                                   }

                                               }
                                               else if (sortName == "PatientName")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.PatientName).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.PatientName).Descending();
                                                   }

                                               }
                                               else if (sortName == "AssignedTo")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.AssignedTo).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.AssignedTo).Descending();
                                                   }

                                               }
                                               else if (sortName == "TargetDate")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.TargetDate).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.TargetDate).Descending();
                                                   }

                                               }

                                           })
                                           )
                               .Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)
    %>
