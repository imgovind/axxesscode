﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "PatientMonthlySchedule"; %>
 <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
             .Columns(columns =>
                     {
                     columns.Bound(s => s.DisciplineTaskName).Title("Task");
                     columns.Bound(p => p.StatusName).Title("Status");
                     columns.Bound(p => p.EventDate).Format("{0:MM/dd/yyyy}").Title("Schedule Date").Width(100);
                     columns.Bound(p => p.VisitDate).Format("{0:MM/dd/yyyy}").Title("Visit Date").Width(80);
                     columns.Bound(s => s.UserDisplayName).Title("Employee");
                     })
                                  .Sortable(sorting =>
                                              sorting.SortMode(GridSortMode.SingleColumn)
                                                  .OrderBy(order =>
                                                  {
                                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                      if (sortName == "UserDisplayName")
                                                      {
                                                          if (sortDirection == "ASC")
                                                          {
                                                              order.Add(o => o.UserDisplayName).Ascending();
                                                          }
                                                          else if (sortDirection == "DESC")
                                                          {
                                                              order.Add(o => o.UserDisplayName).Descending();
                                                          }

                                                      }

                                                  })
                                          )
                                       .Scrollable()
                                               .Footer(false)%>
  
