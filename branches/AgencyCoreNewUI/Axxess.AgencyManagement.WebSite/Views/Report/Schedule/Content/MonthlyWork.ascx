﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "MonthlyWorkSchedule"; %>
        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Sortable(false).Title("Task");
         columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
         columns.Bound(p => p.ScheduleDateFormatted).Sortable(false).Title("Schedule Date").Width(110);
         columns.Bound(p => p.VisitDateFormatted).Sortable(false).Title("Visit Date").Width(100);
         })
                             .Sortable(sorting =>
                                          sorting.SortMode(GridSortMode.SingleColumn)
                                              .OrderBy(order =>
                                              {
                                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                  if (sortName == "PatientName")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.PatientName).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.PatientName).Descending();
                                                      }

                                                  }

                                              })
                                              )
                         .Scrollable().Footer(false)%>


