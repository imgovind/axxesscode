﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "CaseManagerTaskSchedule"; %>
 <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
             .Columns(columns =>
             {
             columns.Bound(m => m.PatientIdNumber).Title("ID").Width(70);
             columns.Bound(m => m.PatientName).Title("Patient Name");
             columns.Bound(m => m.EventDate).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Schedule Date").Width(90);
             columns.Bound(m => m.DisciplineTaskName).Sortable(false).Title("Task");
             columns.Bound(m => m.UserDisplayName).Title("User Name").Width(155);
           })
         .Sortable(sorting =>
              sorting.SortMode(GridSortMode.SingleColumn)
                  .OrderBy(order =>
                  {
                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                      if (sortName == "PatientIdNumber")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientIdNumber).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientIdNumber).Descending();
                          }

                      }
                      else if (sortName == "PatientName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientName).Descending();
                          }

                      }
                      else if (sortName == "UserDisplayName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.UserDisplayName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.UserDisplayName).Descending();
                          }

                      }

                  })
                  )
                   .Scrollable().Footer(false)
        %>
