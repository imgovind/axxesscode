﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "EmployeeWeeklySchedule"; %>
<div class="wrapper main">
  <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
           <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
           <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%> 
         <div class="buttons fr">
             <ul><li><a class="report-generate">Generate Report</a></li></ul> 
             <br />
             <ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeWeeklySchedule", new { BranchCode = Guid.Empty, UserId = Guid.Empty, StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(7) }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
         </div>

    <fieldset  class="grid-controls">
        <legend>Employee Weekly Schedule</legend>
        <div class="wide-column">
              <label class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input user-list" })%></div> <div class="clear"></div>
              <label class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input user-list"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div><div class="clear"></div>
              <label class="float-left">Employees</label> <div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input" })%></div> <div class="clear"></div>
              <label class="float-left">Date Range</label><div class="fr"><input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Today.AddDays(7).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
    </fieldset>
   <div id="<%= pagename %>GridContainer">
       <% Html.RenderPartial("Schedule/Content/EmployeeWeekly", Model); %>
    </div>
</div>

