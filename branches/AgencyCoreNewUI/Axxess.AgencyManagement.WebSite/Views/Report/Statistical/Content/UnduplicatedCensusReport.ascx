﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Patient>>" %>
<% string pagename = "StatisticalUnduplicatedCensus"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(80);
               columns.Bound(p => p.FirstName).Title("First Name");
               columns.Bound(p => p.LastName).Title("Last Name");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.StartOfCareDateFormatted).Title("Admission Date").Width(120);
               columns.Bound(p => p.DischargeDateFormatted).Title("Discharge Date").Width(120);
               columns.Bound(p => p.StatusName).Title("Status").Width(120);
           })
                                        .Sortable(sorting =>
                                                  sorting.SortMode(GridSortMode.SingleColumn)
                                                      .OrderBy(order =>
                                                      {
                                                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                          if (sortName == "FirstName")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.FirstName).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.FirstName).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "LastName")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.LastName).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.LastName).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "StartOfCareDateFormatted")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.StartOfCareDateFormatted).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.StartOfCareDateFormatted).Descending();
                                                              }

                                                          }
                                                          else if (sortName == "DischargeDateFormatted")
                                                          {
                                                              if (sortDirection == "ASC")
                                                              {
                                                                  order.Add(o => o.DischargeDateFormatted).Ascending();
                                                              }
                                                              else if (sortDirection == "DESC")
                                                              {
                                                                  order.Add(o => o.DischargeDateFormatted).Descending();
                                                              }

                                                          }
                                                      })
                                              )
                           .Scrollable()
                           .Footer(false)%>
 