﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "StatisticalPatientVisitHistory"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.DisciplineTaskName).Title("Task").Sortable(false).ReadOnly();
               columns.Bound(s => s.StatusName).Title("Status").Sortable(false).ReadOnly();
               columns.Bound(s => s.EventDate).Format("{0:MM/dd/yyyy}").Title("Schedule Date").ReadOnly();
               columns.Bound(s => s.VisitDate).Format("{0:MM/dd/yyyy}").Title("Visit Date").ReadOnly();
               columns.Bound(s => s.UserDisplayName).Title("Employee");
           })
                               .Sortable(sorting =>
                                     sorting.SortMode(GridSortMode.SingleColumn)
                                         .OrderBy(order =>
                                         {
                                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                             if (sortName == "EventDate")
                                             {
                                                 if (sortDirection == "ASC")
                                                 {
                                                     order.Add(o => o.EventDate).Ascending();
                                                 }
                                                 else if (sortDirection == "DESC")
                                                 {
                                                     order.Add(o => o.EventDate).Descending();
                                                 }
                                             }
                                             else if (sortName == "VisitDate")
                                             {
                                                 if (sortDirection == "ASC")
                                                 {
                                                     order.Add(o => o.VisitDate).Ascending();
                                                 }
                                                 else if (sortDirection == "DESC")
                                                 {
                                                     order.Add(o => o.VisitDate).Descending();
                                                 }
                                             }
                                             else if (sortName == "UserDisplayName")
                                             {
                                                 if (sortDirection == "ASC")
                                                 {
                                                     order.Add(o => o.UserDisplayName).Ascending();
                                                 }
                                                 else if (sortDirection == "DESC")
                                                 {
                                                     order.Add(o => o.UserDisplayName).Descending();
                                                 }
                                             }
                                         })
                                 )
                                   .Scrollable()
                                           .Footer(false)
        %>
