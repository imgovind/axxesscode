﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "StatisticalMonthlyAdmission"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientId).Title("MRN");
               columns.Bound(p => p.PatientDisplayName).Title("Patient Name");
               columns.Bound(p => p.PatientStatusName).Title("Status");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.InternalReferral).Title("Internal Referral");
               columns.Bound(p => p.ReferrerPhysician).Title("Referring Physician");
               columns.Bound(p => p.ReferralDate).Title("Referral Date").Width(115);
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(115);
           })
                        .Sortable(sorting =>
                          sorting.SortMode(GridSortMode.SingleColumn)
                              .OrderBy(order =>
                              {
                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                  if (sortName == "PatientId")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientId).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientId).Descending();
                                      }
                                  }
                                  else if (sortName == "PatientDisplayName")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientDisplayName).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientDisplayName).Descending();
                                      }
                                  }
                                  else if (sortName == "InternalReferral")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.InternalReferral).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.InternalReferral).Descending();
                                      }
                                  }
                                  else if (sortName == "ReferrerPhysician")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.ReferrerPhysician).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.ReferrerPhysician).Descending();
                                      }
                                  }
                                  else if (sortName == "ReferralDate")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.ReferralDate).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.ReferralDate).Descending();
                                      }
                                  }
                                  else if (sortName == "PatientSoC")
                                  {
                                      if (sortDirection == "ASC")
                                      {
                                          order.Add(o => o.PatientSoC).Ascending();
                                      }
                                      else if (sortDirection == "DESC")
                                      {
                                          order.Add(o => o.PatientSoC).Descending();
                                      }
                                  }
                              })
                      )
                   .Scrollable()
                           .Footer(false)
        %>
