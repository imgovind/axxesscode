﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "StatisticalAnnualAdmission"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientId).Title("MR#").Width(80);
               columns.Bound(p => p.PatientFirstName).Title("First Name");
               columns.Bound(p => p.PatientLastName).Title("Last Name");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(120);
           })
                        .Sortable(sorting =>
                                      sorting.SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                          {
                                              var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                              var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                              if (sortName == "PatientFirstName")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PatientFirstName).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PatientFirstName).Descending();
                                                  }

                                              }
                                              else if (sortName == "PatientLastName")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PatientLastName).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PatientLastName).Descending();
                                                  }

                                              }
                                              else if (sortName == "PatientSoC")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PatientSoC).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PatientSoC).Descending();
                                                  }

                                              }
                                          })
                                  )
                   .Scrollable()
                           .Footer(false)
        %>

