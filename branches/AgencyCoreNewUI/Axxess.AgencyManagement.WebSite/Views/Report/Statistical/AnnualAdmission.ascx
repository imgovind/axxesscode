﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "StatisticalAnnualAdmission"; %>
<div class="wrapper main">
        <div class="buttons fr">
               <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
          <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
          <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%> 
              <ul><li><a class="report-generate">Generate Report</a></li></ul> 
             <br />
             <ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalAnnualAdmission", new { BranchCode = Guid.Empty, StatusId = 1,  Year = DateTime.Now.Year }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
        </div>
    <fieldset class="grid-controls">
        <legend>Annual Admission Patients</legend>
        <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "report_input" })%></div><div class="clear"></div>
            <label  class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div><div class="clear"></div>
            <label  class="float-left">Year</label><div class="fr"> <%= Html.Months("Year", DateTime.Now.Year.ToString(), 1900, new { @id = pagename + "_Year", @class = "oe report_input" })%></div>
        </div>
    </fieldset>
 <div id="<%= pagename %>GridContainer">
       <% Html.RenderPartial("Statistical/Content/AnnualAdmission", Model); %>
    </div>
</div>

