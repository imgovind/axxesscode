﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pageName = "VisitsByPrimaryPaymentSourceReport"; %>
<div class="wrapper main"> 
     <div class="buttons fr">
     <%= Html.Hidden("ReportName", pageName, new { @id = pageName + "_ReportName", @class="report-name"})%>  
        <ul><li><a class="report-request">Request Report</a></li></ul> 
     </div>
    <fieldset class="grid-controls">
        <legend>Visits By Primary Payment Source</legend>
        <div class="wide-column">
            <label class="float-left">Branch</label><div class="fr"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = pageName + "_BranchId", @class = "report_input" })%></div><div class="clear"></div>
            <label class="float-left">Sample Year</label><div class="fr"><select id="<%= pageName %>_Year" name="Year" class = "report_input"><option value="2012" selected="selected">2012</option><option value="2011">2011</option><option value="2010">2010</option></select></div>
        </div>
    </fieldset>
</div>
