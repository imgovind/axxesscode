﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "StatisticalEmployeeVisitHistory"; %>
<div class="wrapper main">
         <div class="buttons fr">
            <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
          <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
          <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%> 
            <ul><li><a class="report-generate">Generate Report</a></li></ul> 
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalEmployeeVisitHistory", new { UserId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
        </div>
    <fieldset class="grid-controls">
        <legend> Employee Visit History </legend>
        <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input patient-list" })%></div>  <div class="clear"></div>
            <label  class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input patient-list"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div> <div class="clear"></div>
            <label  class="float-left">Employee</label><div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input" })%></div> <div class="clear"></div>
            <label  class="float-left">Date Range</label><div class="fr"><input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
       <% Html.RenderPartial("Statistical/Content/EmployeeVisitHistory", Model); %>
    </div>
</div>


