﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pageName = "PPSChargeInformationReport"; %>    
<div class="wrapper main">
 <%= Html.Hidden("ReportName", pageName, new { @id = pageName + "_ReportName", @class = "report-name" })%>  
        <div class="buttons fr">
          <ul><li><%= Html.ActionLink("Generate Report(Excel)", "PPSChargeInformationReport", "Export", new { BranchId = ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(), StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now.Date }, new { id = pageName + "_ExportLink", @class = "report-export", @isExportPrefix = false })%></li></ul>
        </div>
        <fieldset class="grid-controls">
            <legend>Charge Information Report (PPS Log)</legend>
            <div class="wide-column">
                <label class="float-left">Branch</label><div class="fr"><%= Html.BranchOnlyList("BranchId", ViewData.ContainsKey("BranchId") ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(), new { @id = pageName + "_BranchId", @class = "report_input" })%></div><div class="clear"></div>
                <label class="float-left">Date Range</label><div class="fr"><input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" /> To <input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" /></div>
            </div>
        </fieldset>
    </div>
