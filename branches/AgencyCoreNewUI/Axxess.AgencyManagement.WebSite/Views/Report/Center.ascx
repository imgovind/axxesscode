﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Report Center | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <div class="report-output buttons">
        <ul>
            <li><a class="home-button">Reports Home</a></li>
        </ul>
    </div>
    <div id="report-output" class="report-output"></div>
    <div class="report-home"><% Html.RenderPartial("Dashboard"); %></div>
</div>