﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <div class="buttons fr"><ul><li><a href="javascript:void(0);" onclick="Report.RebindClinicalOrders();">Generate Report</a></li></ul></div>
    <fieldset class="grid-controls">
        <legend>Orders Management</legend>
        <div class="wide-column">
            <label for="Report_Patient_Orders_Status" class="float-left">Status</label><div class="fr"><select id="Report_Patient_Orders_Status" name="StatusId" class="PatientStatusDropDown"><option value="5">To be Sent</option><option value="6">Pending Signature</option></select></div>
        </div>
    </fieldset>
    <div >
        <%= Html.Telerik().Grid<ClinicalOrder>().Name("Report_Patient_Orders_Grid").Columns(columns =>
           {
               columns.Bound(p => p.Number);
               columns.Bound(p => p.PatientName);
               columns.Bound(p => p.Type);
               columns.Bound(p => p.Physician);
               columns.Bound(p => p.CreatedDate);

           }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClinicalOrders", "Report", new { StatusId = 5 }))
            .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $("#Report_Patient_Orders_Grid .t-grid-content").css({ 'height': 'auto' });</script>