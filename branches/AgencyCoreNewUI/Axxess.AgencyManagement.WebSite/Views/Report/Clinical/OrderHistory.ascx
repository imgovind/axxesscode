﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PhysicianOrder>>" %>
<% string pagename = "ClinicalPhysicianOrderHistory"; %>
<div class="wrapper main">
     <div class="buttons fr">
       <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
       <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
       <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
            <ul><li><a class="report-generate">Generate Report</a></li></ul>
             <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalPhysicianOrderHistory", new { BranchCode = Guid.Empty, StatusId = 000, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
     </div>
    <fieldset class="grid-controls">
        <legend> Order History </legend>
        <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "report_input" })%></div><div class="clear"></div>
            <label  class="float-left">Status</label><div class="fr"><%var status = new SelectList(new[] { new SelectListItem { Value = "000", Text = "All" }, new SelectListItem { Value = "100", Text = "Not Yet Started" }, new SelectListItem { Value = "105", Text = "Not Yet Due" }, new SelectListItem { Value = "110", Text = "Saved" }, new SelectListItem { Value = "115", Text = "Submitted (Pending QA Review)" }, new SelectListItem { Value = "120", Text = "Returned For Review" }, new SelectListItem { Value = "125", Text = "To Be Sent To Physician" }, new SelectListItem { Value = "130", Text = "Sent To Physician (Manually)" }, new SelectListItem { Value = "135", Text = "Returned W/ Physician Signature" }, new SelectListItem { Value = "140", Text = "Reopened" }, new SelectListItem { Value = "145", Text = "Sent To Physician (Electronically)" }, new SelectListItem { Value = "150", Text = "Saved By Physician" } }, "Value", "Text", 000);%><%= Html.DropDownList("StatusId", status, new { @id = pagename + "_StatusId", @class = "report_input" })%></div><div class="clear"></div>
            <label  class="float-left">Date Range</label><div class="fr"> <input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
    </fieldset>
     <div id="<%= pagename %>GridContainer">
       <% Html.RenderPartial("Clinical/Content/OrderHistory", Model); %>
    </div>
</div>
