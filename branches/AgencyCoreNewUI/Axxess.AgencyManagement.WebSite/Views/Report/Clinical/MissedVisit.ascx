﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MissedVisit>>" %>
<% string pagename = "ClinicalMissedVisit"; %>
<div class="wrapper main">
    <div class="buttons fr">
         <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
       <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
       <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
        <ul><li><a class="report-generate">Generate Report</a></li></ul>
        <br />
        <ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalMissedVisit", new { BranchCode = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
</div>
    <fieldset class="grid-controls">
         <legend> Missed Visit</legend>
         <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input" })%></div> <div class="clear"></div>
            <label  class="float-left">Date Range</label><div class="fr"><input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /><label> To </label><input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
          </fieldset>
    <div id="<%= pagename %>GridContainer">
       <% Html.RenderPartial("Clinical/Content/MissedVisit", Model); %>
    </div>
</div>
