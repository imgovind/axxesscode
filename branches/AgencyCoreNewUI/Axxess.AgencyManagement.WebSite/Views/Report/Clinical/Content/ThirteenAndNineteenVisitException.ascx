﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeTherapyException>>" %>
<% string pagename = "ClinicalThirteenAndNineteenVisitException"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(o => o.PatientIdNumber).Title("MRN").Width(70);
               columns.Bound(o => o.PatientName).Title("Name");
               columns.Bound(o => o.EpisodeRange).Title("Episode").Format("{0:MM/dd/yyyy}").Sortable(false).ReadOnly();
               columns.Bound(o => o.ScheduledTherapy).Title("Scheduled").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.CompletedTherapy).Title("Completed").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.EpisodeDay).Title("Day").Width(30).Sortable(false).ReadOnly();
               columns.Bound(o => o.ThirteenVisit).Title("13th Visit").Sortable(false).ReadOnly();
               columns.Bound(o => o.NineteenVisit).Title("19th Visit").Sortable(false).ReadOnly();
           })
                                       .Sortable(sorting =>
                                                              sorting.SortMode(GridSortMode.SingleColumn)
                                                                  .OrderBy(order =>
                                                                  {
                                                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                      if (sortName == "PatientIdNumber")
                                                                      {
                                                                          if (sortDirection == "ASC")
                                                                          {
                                                                              order.Add(o => o.PatientIdNumber).Ascending();
                                                                          }
                                                                          else if (sortDirection == "DESC")
                                                                          {
                                                                              order.Add(o => o.PatientIdNumber).Descending();
                                                                          }

                                                                      }
                                                                      else if (sortName == "PatientName")
                                                                      {
                                                                          if (sortDirection == "ASC")
                                                                          {
                                                                              order.Add(o => o.PatientName).Ascending();
                                                                          }
                                                                          else if (sortDirection == "DESC")
                                                                          {
                                                                              order.Add(o => o.PatientName).Descending();
                                                                          }

                                                                      }
                                                                  })
                                                          )
                                   .Scrollable()
                                           .Footer(false)
        %>


