﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="float-left">
<% if (Current.HasRight(Permissions.AccessPatientReports)) { %>
    <div class="reports">
        <div class="reports-head">
            <h5>Patient Reports</h5>
        </div>
        <ul> 
            <li><a class="report-link" href="Report/Patient/Roster">Patient Roster</a></li> 
            <li><a class="report-link" href="Report/Patient/Cahps">CAHPS Report</a></li> 
            <li><a class="report-link" href="Report/Patient/EmergencyList">Emergency Contact Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/Birthdays">Patient Birthday Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/AddressList">Patient Address Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/Physician">Patient By Physician Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/SocCertPeriod">Patient SOC Cert Period Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/ResponsibleEmployee">Patient By Responsible Employee Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/ResponsibleCaseManager">Patient By Responsible CaseManager Listing</a></li> 
            <li><a class="report-link" href="Report/Patient/ExpiringAuthorizations">Expiring Authorizations</a></li> 
            <li><a class="report-link" href="Report/Patient/SurveyCensus">Survey Census</a></li>
            <li><a class="report-link" href="Report/Patient/VitalSigns">Patient Vital Sign Report</a></li>
            <li><a class="report-link" href="Report/Patient/SixtyDaySummary">60 Day Summary By Patient</a></li>
            <li><a class="report-link" href="Report/Patient/DischargePatients">Discharge Patients</a></li>
        </ul>
    </div>
<% } %>    
<% if (Current.HasRight(Permissions.AccessClinicalReports)) { %>
    <div class="reports">
        <div class="reports-head">
            <h5>Clinical Reports</h5>
        </div>
        <ul>
            <li><a class="report-link" href="Report/Clinical/OpenOasis">Open OASIS</a></li>
            <li><a class="report-link" href="Report/Clinical/MissedVisits">Missed Visit Report</a></li>
            <li><a onclick="Acore.Open('OrdersToBeSent');return false">Orders To Be Sent</a></li>
            <li><a onclick="Acore.Open('OrdersPendingSignature');return false">Orders Pending Signature</a></li>
            <li><a class="report-link" href="Report/Clinical/PhysicianOrderHistory">Physician Order History</a></li>
            <li><a class="report-link" href="Report/Clinical/PlanOfCareHistory">Plan Of Care History</a></li>
            <li><a class="report-link" href="Report/Clinical/ThirteenAndNineteenVisitException">13th And 19th Therapy Visit Exception</a></li>
            <li><a class="report-link" href="Report/Clinical/ThirteenTherapyReevaluationException">13th Therapy Re-evaluation Exception Reports</a></li>
            <li><a class="report-link" href="Report/Clinical/NineteenTherapyReevaluationException">19th Therapy Re-evaluation Exception Reports</a></li>
        </ul>
    </div>
<% } %>  
</div>
<div class="float-left">
<% if (Current.HasRight(Permissions.AccessBillingReports)) { %>
    <div class="reports">
        <div class="reports-head">
            <h5>Billing/Financial Reports</h5>
        </div>
        <ul> 
             <li><a class="report-link" href="Report/Billing/OutstandingClaims">Outstanding Claims</a></li> 
             <li><a class="report-link" href="Report/Billing/SubmittedClaims">Submitted Claims</a></li> 
             <li><a class="report-link" href="Report/Billing/ByStatusSummary">Claims History By Status</a></li> 
             <li><a class="report-link" href="Report/Billing/AccountsReceivable">Accounts Receivable Report</a></li> 
             <li><a class="report-link" href="Report/Billing/AgedAccountsReceivable">Aged Accounts Receivable Report</a></li> 
             <li><a class="report-link" href="Report/Billing/PPSRAPClaims">PPS RAP Claims Needed</a></li> 
             <li><a class="report-link" href="Report/Billing/PPSFinalClaims">PPS Final Claims Needed</a></li> 
             <li><a class="report-link" href="Report/Billing/PotentialClaimAutoCancel">Potential Claim Auto Cancel</a></li> 
             <li><a class="report-link" href="Report/Billing/BillingBatch">Billing Batch Report</a></li> 
             <li><a class="report-link" href="Report/Billing/UnearnedRevenue">Unearned Revenue Report</a></li> 
             <li><a class="report-link" href="Report/Billing/UnbilledRevenue">Unbilled Revenue Report</a></li> 
             <li><a class="report-link" href="Report/Billing/EarnedRevenue">Earned Revenue Report</a></li>
             <li><a class="report-link" href="Report/Billing/EarnedRevenueByEpisodeDays">Earned Revenue (1/60 Method) Report</a></li> 
             <li><a class="report-link" href="Report/Billing/BilledRevenue">Revenue Report</a></li>
        </ul>
    </div>
<% } %>    
<% if (Current.HasRight(Permissions.AccessScheduleReports)) { %>    
    <div class="reports">
        <div class="reports-head">
            <h5>Schedule Reports</h5>
        </div>
        <ul>
            <li><a class="report-link" href="Report/Schedule/PatientWeekly">Patient Weekly Schedule</a></li>
            <li><a class="report-link" href="Report/Schedule/PatientMonthlySchedule">Patient Monthly Schedule</a></li>
            <li><a class="report-link" href="Report/Schedule/EmployeeWeekly">Employee Weekly Schedule</a></li>
            <li><a class="report-link" href="Report/Schedule/DailySchedule">Employee Daily Work Schedule</a></li>
            <li><a class="report-link" href="Report/Schedule/MonthlySchedule">Employee Monthly Work Schedule</a></li>
            <li><a class="report-link" href="Report/Schedule/PastDueVisits">Past Due Visits</a></li>
            <li><a class="report-link" href="Report/Schedule/PastDueVisitsByDiscipline">Past Due Visits By Discipline</a></li>
            <li><a class="report-link" href="Report/Schedule/CaseManagerTask">Case Manager Task List</a></li>
            <li><a class="report-link" href="Report/Schedule/Deviation">Schedule Deviation</a></li>
            <li><a class="report-link" href="Report/Schedule/PastDueRecet">Past Due Recertification</a></li>
            <li><a class="report-link" href="Report/Schedule/UpcomingRecet">Upcoming Recertification</a></li>
        </ul>
    </div>
<% } %>
</div>
<div class="float-left">
<% if (Current.HasRight(Permissions.AccessStatisticalReports)) { %>
    <div class="reports">
        <div class="reports-head">
            <h5>Statistical Reports</h5>
        </div>
        <ul>
            <li><a class="report-link" href="Report/Statistical/PatientVisits">Patient Visit History</a></li>
            <li><a class="report-link" href="Report/Statistical/EmployeeVisits">Employee Visit History</a></li>
            <li><a class="report-link" href="Report/Statistical/CensusByPrimaryInsurance">Census By Primary Insurance</a></li>
            <li><a class="report-link" href="Report/Statistical/MonthlyAdmission">Monthly Admission Report</a></li>
            <li><a class="report-link" href="Report/Statistical/AnnualAdmission">Annual Admission Report</a></li>
            <li><a class="report-link" href="Report/Statistical/UnduplicatedCensusReport">Unduplicated Census Report</a></li>
            <li><a class="report-link" href="Report/Statistical/CostReport">Cost Report</a></li>
        </ul>
    </div>
    <div class="reports">
        <div class="reports-head">
            <h5>Annual Survey Report (Missouri)</h5>
        </div>
        <ul>
            <li><a class="report-link" href="Report/Statistical/PPSEpisodeInformation">PPS Episode Information Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/PPSVisitInformation">PPS Visit Information Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/PPSPaymentInformation">PPS Payment Information Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/PPSChargeInformation">PPS Charge Information Report</a></li> 
        </ul>
    </div>
    <div class="reports">
        <div class="reports-head">
            <h5>Annual Utilization Report (California)</h5>
        </div>
        <ul>
            <li><a class="report-link" href="Report/Statistical/PatientsAndVisitsByAge">Patients And Visits By Age Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/DischargesByReason">Discharges By Reason Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/VisitsByPrimaryPaymentSource">Primary Payment Source Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/VisitsByStaffType">Type of Staff Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/AdmissionsByReferralSource">Admissions By Referral Source Report</a></li> 
            <li><a class="report-link" href="Report/Statistical/PatientsVisitsByPrincipalDiagnosis">Principal Diagnosis Report</a></li> 
        </ul>
    </div>
<% } %>
<% if (Current.HasRight(Permissions.AccessEmployeeReports)) { %>
    <div class="reports">
        <div class="reports-head">
            <h5>Employee Reports</h5>
        </div>
        <ul>
            <li><a class="report-link" href="Report/Employee/Roster">Employee Roster</a></li>
            <li><a class="report-link" href="Report/Employee/License">Employee License Listing</a></li>
             <li><a class="report-link" href="Report/Employee/AllEmployeeLicense">All Employee License Listing</a></li>
            <li><a class="report-link" href="Report/Employee/ExpiringLicense">Expiring Licenses</a></li>
            <li><a class="report-link" href="Report/Employee/VisitByDateRange">Employee Visit By Date Range</a></li>
        </ul>
    </div>
<% } %>
</div>
