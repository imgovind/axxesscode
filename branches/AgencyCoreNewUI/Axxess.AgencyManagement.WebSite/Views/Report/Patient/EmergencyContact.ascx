﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EmergencyContactInfo>>" %>
<h3>Patient Emergency Contact</h3>
<fieldset>
    <div class="row">
        <label for="AddressStateCode">
            Patient Name</label><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientEmergencyContact_Name", "", new { @id = "PatientEmergencyContact_Name", @class = "Name report_input valid" })%>
    </div>
    <div class="row">
        <label for="AddressStateCode">
            State</label><%= Html.LookupSelectList(SelectListTypes.States, "PatientEmergencyContact_AddressStateCode", "", new { @id = "PatientEmergencyContact_AddressStateCode", @class = "AddressStateCode report_input valid" })%>
    </div>
    <div class="row">
        <label for="AddressStateCode">
            Branch</label><%= Html.LookupSelectList(SelectListTypes.Branches, "PatientEmergencyContact_AddressBranchCode", "", new { @id = "PatientEmergencyContact_AddressBranchCode", @class = "AddressBranchCode report_input valid" })%>
    </div>
    <input id="report_step" type="hidden" name="step" class="report_input" value="2" />
    <input id="report_action" type="hidden" name="action" value="PatientEmergencyContact" />
    <p>
        <input type="button" value="Generate Report" onclick="Report.populateReport($(this));" />
    </p>
</fieldset>
<div id="PatientEmergencyContactResult" class="result_grid_div">
    <% =Html.Telerik().Grid<EmergencyContactInfo>()
                 .Name("PatientEmergencyContactGrid")        
         .Columns(columns =>
   {
       columns.Bound(p => p.PatientName);
       columns.Bound(p => p.ContactName);
       columns.Bound(p => p.ContactRelation).Title("Relationship");
       columns.Bound(p => p.ContactPhoneHome);
       columns.Bound(p => p.ContactEmailAddress);
       
   })
   .DataBinding(dataBinding => dataBinding.Ajax().Select("PatientEmergencyContactResult", "Report", new { Name = Guid.Empty, AddressStateCode = " ", AddressBranchCode = Guid.Empty, ReportStep = "" }))
   .Sortable().Selectable().Scrollable().Footer(false)
  
    %>
</div>
<script type="text/javascript"> $(".t-grid-content").css({ 'height': 'auto' }); </script>