﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Birthday>>" %>
<% string pagename = "PatientBirthdayList"; %>
<div class="wrapper main">
      <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
      <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
      <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
            <div class="buttons fr">
                <ul>
                    <li><a class="report-generate">Generate Report</a></li>
                </ul>
                <br />
                <ul>
                    <li><%= Html.ActionLink("Export to Excel", "ExportPatientBirthdayList", new { BranchCode = Guid.Empty, Month = DateTime.Now.Month }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li>
                </ul>
            </div>
    <fieldset class="grid-controls">
        <legend>Patient Birthday Listing</legend>
        <div class="wide-column">
                <label class="float-left">Branch</label><div class="fr"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input" })%></div><div class="clear"></div>
                <label class="float-left">Month</label><div class="fr">
                    <%  var months = new SelectList(new[] {
                            new SelectListItem { Text = "Select Month", Value = "0" },
                            new SelectListItem { Text = "January", Value = "1" },
                            new SelectListItem { Text = "February", Value = "2" },
                            new SelectListItem { Text = "March", Value = "3" },
                            new SelectListItem { Text = "April", Value = "4" },
                            new SelectListItem { Text = "May", Value = "5" },
                            new SelectListItem { Text = "June", Value = "6" },
                            new SelectListItem { Text = "July", Value = "7" },
                            new SelectListItem { Text = "August", Value = "8" },
                            new SelectListItem { Text = "September", Value = "9" },
                            new SelectListItem { Text = "October", Value = "10" },
                            new SelectListItem { Text = "November", Value = "11" },
                            new SelectListItem { Text = "December", Value = "12" }
                        }, "Value", "Text", DateTime.Now.Month); %>
                    <%= Html.DropDownList("Month", months, new { @id = pagename + "_Month", @class = "oe report_input" })%>
                </div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer" >
       <% Html.RenderPartial("Patient/Content/BirthdayListContent", Model); %>
    </div>
</div>