﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientRoster"; %>
<div class="wrapper main">
    <div class="buttons fr">
       <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
       <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
       <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
        <ul><li><a class="report-generate">Generate Report</a></li></ul>
        <br />
        <ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientRoster", new { BranchCode = Guid.Empty, StatusId = 1, InsuranceId = 0 }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
    </div>
    <fieldset class="grid-controls">
        <legend>Patient Roster</legend>
        <div class="wide-column">
            <label class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id =  pagename +"_BranchCode", @class = "report_input insurance-list" })%></div><div class="clear"></div>
            <label class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option><option value="3">Pending</option></select></div><div class="clear"></div>
            <label class="float-left">Insurance</label><div class="fr"><%=Html.Insurances("InsuranceId", "0", new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div>
        </div>
          </fieldset>
    
    <div id="<%= pagename %>GridContainer">
       <% Html.RenderPartial("Patient/Content/RosterContent", Model); %>
    </div>
</div>
