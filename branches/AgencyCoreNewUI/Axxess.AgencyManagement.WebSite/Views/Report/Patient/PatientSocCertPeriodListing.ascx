﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSocCertPeriod>>" %>
<% string pagename = "PatientSocCertPeriodListing"; %>
<div class="wrapper main">
       <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
      <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
      <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
    <div class="buttons fr">
        <ul><li><a class="report-generate">Generate Report</a></li></ul>
        <br />
        <ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientSocCertPeriodListing", new { BranchCode = Guid.Empty, StatusId = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
    </div>
    <fieldset class="grid-controls">
         <legend> Patient SOC Cert Period Listing</legend>
         <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input" })%></div><div class="clear"></div>
            <label  class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input"><option value="0">All</option><option value="1" selected >Active</option><option value="2">Discharged</option></select></div><div class="clear"></div>
            <label  class="float-left">SOC Cert. Start Date</label><div class="fr">From : <input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
         <% Html.RenderPartial("Patient/Content/PatientSocCertPeriodListingContent", Model); %>
    </div>
</div>
