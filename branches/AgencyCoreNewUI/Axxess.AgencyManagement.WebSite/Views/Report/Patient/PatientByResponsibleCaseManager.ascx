﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByResponsibleCaseManager"; %>
<div class="wrapper main">
   <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
      <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
      <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
        <div class="buttons fr">
          <ul><li><a class="report-generate">Generate Report</a></li></ul>
          <br />
          <ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientByResponsibleCaseManager", new { BranchCode = Guid.Empty, UserId = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
        </div>
    <fieldset  class="grid-controls">
        <legend>Patient By Responsible Case Manager Listing</legend>
        <div class="wide-column">
            <label class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input user-list" })%></div><div class="clear"></div>
            <label class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="report_input user-list"><option value="0">All</option><option value="1" selected="selected">Active</option><option value="2">Inactive</option></select></div><div class="clear"></div>
            <label class="float-left">Employee</label><div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input" })%></div>
        </div>
      </fieldset>
   <div id="<%= pagename %>GridContainer" >
   <% Html.RenderPartial("Patient/Content/PatientByResponsibleCaseManagerContent", Model); %>
</div>
</div>
