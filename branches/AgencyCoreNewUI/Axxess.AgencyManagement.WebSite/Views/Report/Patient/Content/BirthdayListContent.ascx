﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Birthday>>" %>
<% string pagename = "PatientBirthdayList"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
   {
       columns.Bound(p => p.Name).Width(150);
       columns.Bound(p => p.Age).Width(50);
       columns.Bound(p => p.BirthDay).Sortable(false).Width(130);
       columns.Bound(p => p.AddressFirstRow).Sortable(false);
       columns.Bound(p => p.AddressSecondRow).Sortable(false);
       columns.Bound(p => p.PhoneHomeFormatted).Sortable(false).Title("Home Phone").Width(110);
   })
        .Sortable(sorting =>
                              sorting.SortMode(GridSortMode.SingleColumn)
                                  .OrderBy(order =>
                                  {
                                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                      if (sortName == "Name")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.Name).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.Name).Descending();
                                          }
                                      }
                                      else if (sortName == "Age")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.Age).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.Age).Descending();
                                          }
                                      }
                                      else if (sortName == "BirthDay")
                                      {
                                          if (sortDirection == "ASC")
                                          {
                                              order.Add(o => o.BirthDay).Ascending();
                                          }
                                          else if (sortDirection == "DESC")
                                          {
                                              order.Add(o => o.BirthDay).Descending();
                                          }
                                      }
                                  })
                          )
   .Scrollable()
   .Footer(false)
%>
