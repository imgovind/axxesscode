﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientRoster"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").HtmlAttributes(new  { @style="" }).Columns(columns =>
        {
            columns.Bound(r => r.PatientId).Title("MR#").Width(70);
            columns.Bound(r => r.PatientDisplayName).Title("Patient");
            columns.Bound(r => r.PatientMedicareNumber).Title("Medicare #").Sortable(false).Width(85);
            columns.Bound(r => r.PatientSoC).Title("SOC").Width(85);
            columns.Bound(r => r.PatientDischargeDate).Title("Discharge Date").Width(85);
            columns.Bound(r => r.AddressFull).Title("Address").Sortable(false);
            columns.Bound(r => r.PatientDOB).Format("{0:MM/dd/yyyy}").Title("DOB").Width(75);
            columns.Bound(r => r.PatientPhone).Title("Home Phone").Sortable(false).Width(110);
            columns.Bound(r => r.PatientGender).Title("Gender").Sortable(false).Width(60);
            columns.Bound(r => r.Triage).Title("Triage").Width(60);
        })
                 .Sortable(sorting =>
                sorting.SortMode(GridSortMode.SingleColumn)
                    .OrderBy(order =>
                    {
                        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                        if (sortName == "PatientId")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientId).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientId).Descending();
                            }
                        }
                        else if (sortName == "PatientDisplayName")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientDisplayName).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientDisplayName).Descending();
                            }
                        }
                        else if (sortName == "PatientSoC")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientSoC).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientSoC).Descending();
                            }
                        }
                        else if (sortName == "PatientDischargeDate")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientDischargeDate).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientDischargeDate).Descending();
                            }
                        }
                    })
            )
            .Scrollable()
            .Footer(false)
%>
