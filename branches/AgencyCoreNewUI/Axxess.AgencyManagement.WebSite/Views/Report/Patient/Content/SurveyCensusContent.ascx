﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<SurveyCensus>>" %>
<% string pagename = "PatientSurveyCensus"; %>
 <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
 {
 columns.Bound(p => p.PatientIdNumber).Title("MR #").Width(70);
 columns.Bound(p => p.PatientDisplayName).Title("Patient Name");
 columns.Bound(p => p.MedicareNumber).Title("Medicare #").Width(80).Sortable(false);
 columns.Bound(p => p.DOB).Format("{0:MM/dd/yyyy}").Title("DOB").Width(75);
 columns.Bound(p => p.SOC).Format("{0:MM/dd/yyyy}").Title("SOC Date").Width(75);
 columns.Bound(p => p.CertPeriod).Title("Cert Period(Recent)").Width(155).Sortable(false);
 columns.Bound(p => p.PrimaryDiagnosis).Title("Primary Diagnosis").Sortable(false);
 columns.Bound(p => p.SecondaryDiagnosis).Title("Secondary Diagnosis").Sortable(false);
 columns.Bound(p => p.Triage).Title("Triage").Sortable(false).Width(50);
 columns.Bound(p => p.Discipline).Title("Disciplines").Sortable(false);
 })
         .Sortable(sorting =>
                     sorting.SortMode(GridSortMode.SingleColumn)
                         .OrderBy(order =>
                         {
                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                             if (sortName == "PatientIdNumber")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.PatientIdNumber).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.PatientIdNumber).Descending();
                                 }
                             }
                             else if (sortName == "PatientDisplayName")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.PatientDisplayName).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.PatientDisplayName).Descending();
                                 }
                             }
                             else if (sortName == "DOB")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.DOB).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.DOB).Descending();
                                 }
                             }
                             else if (sortName == "SOC")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.SOC).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.SOC).Descending();
                                 }
                             }
                         })
                 )
     .Scrollable()
      .Footer(false)
%>

