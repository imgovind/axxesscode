﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VisitNoteViewData>>" %>
<% string pagename = "PatientSixtyDaySummary"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(v => v.UserDisplayName).Title("Employee Name");
               columns.Bound(v => v.VisitDate).Title("Visit Date");
               columns.Bound(v => v.SignatureDate).Title("Signature Date");
               columns.Bound(v => v.EpisodeRange).Title("Episode Date").Sortable(false);
               columns.Bound(v => v.PhysicianDisplayName).Title("Physician Name");
           })
                              .Sortable(sorting =>
                                                         sorting.SortMode(GridSortMode.SingleColumn)
                                                             .OrderBy(order =>
                                                             {
                                                                 var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                 var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                 if (sortName == "UserDisplayName")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.UserDisplayName).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.UserDisplayName).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "VisitDate")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.VisitDate).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.VisitDate).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "SignatureDate")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.SignatureDate).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.SignatureDate).Descending();
                                                                     }
                                                                 }
                                                                 else if (sortName == "PhysicianDisplayName")
                                                                 {
                                                                     if (sortDirection == "ASC")
                                                                     {
                                                                         order.Add(o => o.PhysicianDisplayName).Ascending();
                                                                     }
                                                                     else if (sortDirection == "DESC")
                                                                     {
                                                                         order.Add(o => o.PhysicianDisplayName).Descending();
                                                                     }
                                                                 }

                                                             }))
                           .Scrollable()
                                   .Footer(false)
        %>
