﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByResponsibleCaseManager"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns => {
        columns.Bound(p => p.PatientId).Title("MRN").Width(80);
        columns.Bound(p => p.PatientLastName).Title("Last Name").Width(150);
        columns.Bound(p => p.PatientFirstName).Title("First Name").Width(150);
        columns.Bound(p => p.AddressFull).Title("Address");
        columns.Bound(p => p.PatientSoC).Title("SOC Date").Width(80);
})
     .Sortable(sorting =>
                                 sorting.SortMode(GridSortMode.SingleColumn)
                                     .OrderBy(order =>
                                     {
                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                         if (sortName == "PatientId")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.PatientId).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.PatientId).Descending();
                                             }
                                         }
                                         else if (sortName == "PatientLastName")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.PatientLastName).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.PatientLastName).Descending();
                                             }
                                         }
                                         else if (sortName == "PatientFirstName")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.PatientFirstName).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.PatientFirstName).Descending();
                                             }
                                         }
                                         else if (sortName == "PatientSoC")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.PatientSoC).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.PatientSoC).Descending();
                                             }
                                         }
                                     })
                             ).Scrollable()
.Footer(false)%>
