﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientByPhysicians"; %>
<div class="wrapper main">
      <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
      <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
      <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
    <div class="buttons fr">
        <ul><li><a class="report-generate">Generate Report</a></li></ul>
        <br />
        <ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientByPhysicians", new { PhysicianId = Guid.Empty }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
    </div>
    <fieldset class="grid-controls">
        <legend>Patient By Physician Listing</legend>
        <div class="wide-column"><label class="float-left">Physician</label><div class="fr"><%= Html.TextBox("PhysicianId", Guid.Empty.ToString(), new { @id = pagename + "_PhysicianId", @class = "physician-picker" ,@hiddenclass="report_input" })%></div></div>
     </fieldset>
  <div id="<%= pagename %>GridContainer"><% Html.RenderPartial("Patient/Content/Physician", Model); %></div>
</div>