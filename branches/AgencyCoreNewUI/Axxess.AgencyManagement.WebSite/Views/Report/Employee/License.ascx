﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "EmployeeLicenseListing"; %>
<div class="wrapper main">
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons fr">
            <ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','EmployeeLicenseContent',{{ UserId: $('#{0}_UserId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeLicenseListing", new {UserId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul>
        </div>
    <fieldset  class="grid-controls">
         <legend> Employee License Listing</legend>
        <div class="wide-column">
          <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div><div class="clear"></div>
          <label  class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div><div class="clear"></div>
          <label  class="float-left">Employees</label> <div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input valid" })%></div>
        </div>
    </fieldset>
   <div id="<%= pagename %>GridContainer" >
         <% Html.RenderPartial("Employee/Content/License", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
 </script>
