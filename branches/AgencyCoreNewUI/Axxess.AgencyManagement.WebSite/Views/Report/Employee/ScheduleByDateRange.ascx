﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "EmployeeVisitByDateRange"; %>
<div class="wrapper main">
        <div class="buttons fr">
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','EmployeeScheduleByDateRangeContent',{{ BranchCode: $('#{0}_BranchCode').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeVisitByDateRange", new { BranchCode = Guid.Empty, StartDate = DateTime.Now.AddDays(-15), EndDate = DateTime.Now.AddDays(15) }, new { id = pagename + "_ExportLink" })%></li></ul>
        </div>
    <fieldset  class="grid-controls">
        <legend> Employee Visit By Date Range </legend>
        <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> <div class="clear"></div>
            <label  class="float-left">Date Range</label><div class="fr"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-15).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(15).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
    </fieldset>
   <div id="<%= pagename %>GridContainer">
         <% Html.RenderPartial("Employee/Content/ScheduleByDateRange", Model); %>
    </div>
</div>
