﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<% string pagename = "EmployeeExpiringLicense"; %>
<div class="wrapper main">
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons fr">
            <ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ExpiringLicenseContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeExpiringLicense", new { BranchCode = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink" })%></li></ul>
        </div>
       <fieldset  class="grid-controls">
            <legend> Employee Expiring Licenses</legend>
            <div class="wide-column">
               <label  class="float-left">Branch</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.BranchesReport,"BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> <div class="clear"></div>
               <label  class="float-left">Status</label><div class="fr"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div>
            </div>
       </fieldset>
       <div id="<%= pagename %>GridContainer" >
         <% Html.RenderPartial("Employee/Content/ExpiringLicenses", Model); %>
    </div>
</div>

