﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "EmployeeVisitByDateRange"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.UserDisplayName).Title("Employee").ReadOnly();
               columns.Bound(s => s.PatientName).Title("Patient");
               columns.Bound(s => s.TaskName).Title("Task").ReadOnly();
               columns.Bound(s => s.ScheduleDateFormatted).Title("Schedule Date").Sortable(false).Width(90).ReadOnly();
               columns.Bound(s => s.VisitDateFormatted).Title("Visit Date").Sortable(false).Width(90).ReadOnly();
               columns.Bound(s => s.StatusName).Title("Status").Sortable(false).ReadOnly();
           })
                                .Sortable(sorting =>
                                                          sorting.SortMode(GridSortMode.SingleColumn)
                                                              .OrderBy(order =>
                                                              {
                                                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                  if (sortName == "UserDisplayName")
                                                                  {
                                                                      if (sortDirection == "ASC")
                                                                      {
                                                                          order.Add(o => o.UserDisplayName).Ascending();
                                                                      }
                                                                      else if (sortDirection == "DESC")
                                                                      {
                                                                          order.Add(o => o.UserDisplayName).Descending();
                                                                      }
                                                                  }
                                                                  else if (sortName == "PatientName")
                                                                  {
                                                                      if (sortDirection == "ASC")
                                                                      {
                                                                          order.Add(o => o.PatientName).Ascending();
                                                                      }
                                                                      else if (sortDirection == "DESC")
                                                                      {
                                                                          order.Add(o => o.PatientName).Descending();
                                                                      }
                                                                  }
                                                                  else if (sortName == "TaskName")
                                                                  {
                                                                      if (sortDirection == "ASC")
                                                                      {
                                                                          order.Add(o => o.TaskName).Ascending();
                                                                      }
                                                                      else if (sortDirection == "DESC")
                                                                      {
                                                                          order.Add(o => o.TaskName).Descending();
                                                                      }
                                                                  }

                                                              })
                                                      )
                                   .Scrollable()
                                           .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','EmployeeScheduleByDateRangeContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
