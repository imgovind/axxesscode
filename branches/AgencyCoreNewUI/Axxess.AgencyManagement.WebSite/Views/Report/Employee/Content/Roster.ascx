﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<User>>" %>
<% string pagename = "EmployeeRoster"; %>
         <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.DisplayName).Title("Name");
                columns.Bound(r => r.Profile.Address).Sortable(false).Title("Address");
                columns.Bound(r => r.Profile.AddressCity).Title("City");
                columns.Bound(r => r.Profile.AddressStateCode).Title("State").Width(40);
                columns.Bound(r => r.Profile.AddressZipCode).Sortable(false).Title("Zip Code").Width(80);
                columns.Bound(r => r.HomePhone).Sortable(false).Title("Home Phone").Width(110);
                columns.Bound(r => r.Profile.Gender).Sortable(false).Title("Gender").Width(50);
            })
                    // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new {BranchId = Guid.Empty , Status=1}))
                                         .Sortable(sorting =>
                             sorting.SortMode(GridSortMode.SingleColumn)
                                 .OrderBy(order =>
                                 {
                                     var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                     var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                     if (sortName == "DisplayName")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.DisplayName).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.DisplayName).Descending();
                                         }
                                     }
                                     else if (sortName == "Profile.AddressCity")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.Profile.AddressCity).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.Profile.AddressCity).Descending();
                                         }
                                     }
                                     else if (sortName == "Profile.AddressStateCode")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.Profile.AddressStateCode).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.Profile.AddressStateCode).Descending();
                                         }
                                     }
                                     
                                 })
                         )
                                       .Scrollable()
                                                .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','EmployeeRosterContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
  </script>