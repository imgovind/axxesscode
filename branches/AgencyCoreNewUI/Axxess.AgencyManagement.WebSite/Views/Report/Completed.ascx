﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Reports | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <fieldset class="grid-controls"><div class="filter grid-search"></div></fieldset>
    <%= Html.Telerik().Grid<ReportLite>().Name("List_CompletedReports").Columns(columns => {
            columns.Bound(r => r.Name).Title("Name").Sortable(false);
            columns.Bound(r => r.Format).Title("Format").Sortable(false);
            columns.Bound(r => r.Status).Title("Status").Sortable(false);
            columns.Bound(r => r.UserName).Title("Requested By").Sortable(false);
            columns.Bound(r => r.Created).Title("Started").Sortable(false);
            columns.Bound(r => r.Completed).Title("Completed").Sortable(false);
            columns.Bound(r => r.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Report.Delete('<#=Id#>');\">Delete</a>").Title("Action").Sortable(false).Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("CompletedList", "Report")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
