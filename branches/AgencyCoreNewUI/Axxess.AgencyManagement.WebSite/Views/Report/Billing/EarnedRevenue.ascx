﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Revenue>>" %>
<% string pagename = "EarnedRevenue"; %>
<div class="wrapper main">
<% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
  <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
  <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%> 
    <div class="buttons fr">
            <ul><li><a class="report-generate">Generate Report</a></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportEarnedRevenue", new { BranchCode = Guid.Empty, InsuranceId = 0, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
     </div>
    <fieldset class="grid-controls">
        <legend>Earned Revenue</legend>
        <div class="wide-column">
              <label class="float-left">Branch</label><div class="fr"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "report_input medicare-with-hmo" })%></div><div class="clear"></div>
              <label class="float-left">Insurance</label><div class="fr"><%= Html.InsurancesMedicare("InsuranceId", "0",true,"All",new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div><div class="clear"></div>
              <label class="float-left">Date Range</label><div class="fr"><input type="text" class="date-picker shortdate report_input" name="StartDate" value="<%= DateTime.Now.AddDays(-29).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate report_input" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
         <% Html.RenderPartial("Billing/Content/EarnedRevenue", Model); %>
    </div>
</div>

