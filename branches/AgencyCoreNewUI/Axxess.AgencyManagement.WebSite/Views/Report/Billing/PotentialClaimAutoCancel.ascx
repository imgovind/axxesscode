﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<% string pagename = "PotentialClaimAutoCancel"; %>
<div class="wrapper main">
 <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
  <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
  <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
    <div class="buttons fr">
            <ul><li><a class="report-generate">Generate Report</a></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "ExportPotentialClaimAutoCancel", new { BranchCode = Guid.Empty }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
        </div>
    <fieldset class="grid-controls">
        <legend>Potential Claim Auto Cancel</legend>
        <div class="wide-column">
            <label  class="float-left">Branch</label><div class="fr"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "report_input" })%></div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
         <% Html.RenderPartial("Billing/Content/PotentialClaimAutoCancel", Model); %>
    </div>
</div>


