﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "BillingBatch"; %>
<div class="wrapper main">
  <% var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]);%>
  <%= Html.Hidden("ReportName", pagename, new { @id = pagename + "_ReportName"  , @class="report-name"})%>  
  <%= Html.Hidden("ReportSortType", sortParams, new { @id = pagename + "_ReportSortType", @class = "report-sorttype" })%>  
    <div class="buttons fr">
           <ul><li><a class="report-generate">Generate Report</a></li></ul>
           <br />
           <ul><li><%= Html.ActionLink("Export to Excel", "ExportBillingBatch", new { ClaimType = "ALL", BatchDate = DateTime.Now }, new { id = pagename + "_ExportLink", @class = "report-export" })%></li></ul>
    </div>
    <fieldset class="grid-controls">
        <legend>Billing Batch</legend>
        <div class="wide-column">
              <label class="float-left">Claim Type</label><div class="fr"><%= Html.ClaimTypes("ClaimType", new { @id = string.Format("{0}_ClaimType", pagename), @class = "report_input" })%></div><div class="clear"></div>
              <label class="float-left">Date</label><div class="fr"><input type="text" class="date-picker shortdate report_input" name="BatchDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_BatchDate" /></div>
        </div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
         <% Html.RenderPartial("Billing/Content/BillingBatch", Model); %>
    </div>
</div>
