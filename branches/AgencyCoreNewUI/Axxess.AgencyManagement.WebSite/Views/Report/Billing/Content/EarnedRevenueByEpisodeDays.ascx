﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Revenue>>" %>
<% string pagename = "EarnedRevenueByEpisodeDays"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient Name");
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode Range").Width(150);
               columns.Bound(p => p.AssessmentTypeName).Sortable(false).Title("Assessment");
               columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
               columns.Bound(p => p.BillableDayCount).Sortable(false).Title("Total Days");
               columns.Bound(p => p.ProspectivePayment).Sortable(false).Format("{0:0.00}").Title("Episode Payment");
               columns.Bound(p => p.UnitAmount).Sortable(false).Format("${0:0.00}").Title("Unit Amount");
               columns.Bound(p => p.CompletedDayCount).Sortable(false).Title("Completed Days");
               columns.Bound(p => p.EarnedRevenueAmount).Sortable(false).Format("${0:0.00}").Title("Earned Amount");
           })
                   .Sortable(sorting =>
                     sorting.SortMode(GridSortMode.SingleColumn)
                         .OrderBy(order =>
                         {
                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                             if (sortName == "PatientIdNumber")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.PatientIdNumber).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.PatientIdNumber).Descending();
                                 }
                             }
                             else if (sortName == "DisplayName")
                             {
                                 if (sortDirection == "ASC")
                                 {
                                     order.Add(o => o.DisplayName).Ascending();
                                 }
                                 else if (sortDirection == "DESC")
                                 {
                                     order.Add(o => o.DisplayName).Descending();
                                 }
                             }
                         })
                 )
              .Scrollable()
               .Footer(false)%>

