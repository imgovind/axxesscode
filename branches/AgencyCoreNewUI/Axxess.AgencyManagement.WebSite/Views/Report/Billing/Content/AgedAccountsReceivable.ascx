﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "AgedAccountsReceivable"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient Name");
               columns.Bound(p => p.EpisodeRange).Title("Episode Range").Width(160);
               columns.Bound(p => p.Type).Width(50);
               columns.Bound(p => p.StatusName).Title("Status");
               columns.Bound(p => p.ClaimDate).Template(p => string.Format("{0}", p.ClaimDate > DateTime.MinValue ? p.ClaimDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Claim Date").Width(80);
               columns.Bound(p => p.Amount30).Sortable(false).Format("${0:0.00}").Title("1-30").Width(70);
               columns.Bound(p => p.Amount60).Sortable(false).Format("${0:0.00}").Title("31-60").Width(70);
               columns.Bound(p => p.Amount90).Sortable(false).Format("${0:0.00}").Title("61-90").Width(70);
               columns.Bound(p => p.AmountOver90).Sortable(false).Format("${0:0.00}").Title("> 90").Width(70);
               columns.Bound(p => p.ClaimAmount).Sortable(false).Format("${0:0.00}").Title("Total").Width(130);
           })
                                       .Sortable(sorting =>
                                          sorting.SortMode(GridSortMode.SingleColumn)
                                              .OrderBy(order =>
                                              {
                                                  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                  if (sortName == "PatientIdNumber")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.PatientIdNumber).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.PatientIdNumber).Descending();
                                                      }
                                                  }
                                                  else if (sortName == "DisplayName")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.DisplayName).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.DisplayName).Descending();
                                                      }
                                                  }
                                                  else if (sortName == "Type")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.Type).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.Type).Descending();
                                                      }
                                                  }
                                                  else if (sortName == "StatusName")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.StatusName).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.StatusName).Descending();
                                                      }
                                                  }
                                                 
                                                  else if (sortName == "ClaimDate")
                                                  {
                                                      if (sortDirection == "ASC")
                                                      {
                                                          order.Add(o => o.ClaimDate).Ascending();
                                                      }
                                                      else if (sortDirection == "DESC")
                                                      {
                                                          order.Add(o => o.ClaimDate).Descending();
                                                      }
                                                  }

                                              })
                                      )
                                   .Scrollable()
                                           .Footer(false)
        %>
  
