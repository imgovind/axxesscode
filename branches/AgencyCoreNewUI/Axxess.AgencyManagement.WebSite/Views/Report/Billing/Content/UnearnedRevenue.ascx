﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Revenue>>" %>
<% string pagename = "UnearnedRevenue"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient");
               columns.Bound(p => p.EpisodeRange).Title("Episode").Width(150);
               columns.Bound(p => p.AssessmentTypeName).Title("Assessment");
               columns.Bound(p => p.StatusName).Title("Status");
               columns.Bound(p => p.ProspectivePayment).Format("{0:0.00}").Title("Episode Payment");
               columns.Bound(p => p.BillableVisitCount).Title("Total Visits");
               columns.Bound(p => p.CompletedVisitCount).Title("Completed Visits");
               columns.Bound(p => p.UnearnedVisitCount).Title("Unearned Visits");
               columns.Bound(p => p.UnitAmount).Format("${0:0.00}").Title("Unit Amount");
               columns.Bound(p => p.UnearnedRevenueAmount).Format("${0:0.00}").Title("Unearned Amount");
               columns.Bound(p => p.RapAmount).Format("${0:0.00}").Title("RAP Amount");
           })
                                        .Sortable(sorting =>
                                                                     sorting.SortMode(GridSortMode.SingleColumn)
                                                                         .OrderBy(order =>
                                                                         {
                                                                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                                             if (sortName == "PatientIdNumber")
                                                                             {
                                                                                 if (sortDirection == "ASC")
                                                                                 {
                                                                                     order.Add(o => o.PatientIdNumber).Ascending();
                                                                                 }
                                                                                 else if (sortDirection == "DESC")
                                                                                 {
                                                                                     order.Add(o => o.PatientIdNumber).Descending();
                                                                                 }
                                                                             }
                                                                             else if (sortName == "DisplayName")
                                                                             {
                                                                                 if (sortDirection == "ASC")
                                                                                 {
                                                                                     order.Add(o => o.DisplayName).Ascending();
                                                                                 }
                                                                                 else if (sortDirection == "DESC")
                                                                                 {
                                                                                     order.Add(o => o.DisplayName).Descending();
                                                                                 }
                                                                             }
                                                                         })
                                                                 )
                                   .Scrollable()
                                           .Footer(false)%>

