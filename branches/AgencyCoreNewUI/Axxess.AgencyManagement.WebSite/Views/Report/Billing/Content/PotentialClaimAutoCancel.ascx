﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<% string pagename = "PotentialClaimAutoCancel"; %>
<%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("ID").Width(80);
               columns.Bound(p => p.LastName).Title("Last Name");
               columns.Bound(p => p.FirstName).Title("First Name");
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode Range");
           })
                               .Sortable(sorting =>
                                 sorting.SortMode(GridSortMode.SingleColumn)
                                     .OrderBy(order =>
                                     {
                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                         if (sortName == "PatientIdNumber")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.PatientIdNumber).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.PatientIdNumber).Descending();
                                             }
                                         }
                                         else if (sortName == "LastName")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.LastName).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.LastName).Descending();
                                             }
                                         }
                                         else if (sortName == "FirstName")
                                         {
                                             if (sortDirection == "ASC")
                                             {
                                                 order.Add(o => o.FirstName).Ascending();
                                             }
                                             else if (sortDirection == "DESC")
                                             {
                                                 order.Add(o => o.FirstName).Descending();
                                             }
                                         }
                                     })
                             )
                           .Scrollable()
                                   .Footer(false)
        %>



