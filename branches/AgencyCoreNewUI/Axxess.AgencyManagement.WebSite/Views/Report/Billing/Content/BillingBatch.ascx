﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimInfoDetail>>" %>
<% string pagename = "BillingBatch"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient Name");
               columns.Bound(p => p.Range).Sortable(false).Title("Episode Range").Width(150);
               columns.Bound(p => p.MedicareNumber).Sortable(false).Title("Medicare Number");
               columns.Bound(p => p.BillType).Sortable(false).Title("Bill Type");
               columns.Bound(p => p.ProspectivePay).Sortable(false).Format("${0:0.00}").Title("Amount");
           })
          .Sortable(sorting =>
              sorting.SortMode(GridSortMode.SingleColumn)
                  .OrderBy(order =>
                  {
                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                      if (sortName == "PatientIdNumber")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientIdNumber).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientIdNumber).Descending();
                          }
                      }
                      else if (sortName == "DisplayName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.DisplayName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.DisplayName).Descending();
                          }
                      }
                  })
          )
       .Scrollable()
       .Footer(false)%>

