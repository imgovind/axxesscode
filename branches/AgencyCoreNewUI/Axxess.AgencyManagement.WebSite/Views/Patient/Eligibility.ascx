﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEligibility>" %>
<div class="wrapper main">
<% if (Model != null) { %>
    <% if (Model.Request_Validation != null && Model.Request_Validation.success.IsNotNullOrEmpty() && Model.Request_Validation.success.IsEqual("yes"))  { %>
    <div class="ac">Server Error: <%= Model.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? Model.Request_Validation.reject_reason_code : string.Empty %><%= Model.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? Model.Request_Validation.follow_up_action_code : string.Empty %></div>
    <% } else { %>
        <% if (Model.Functional_Acknowledgment != null && Model.Functional_Acknowledgment.success.IsNotNullOrEmpty() && Model.Functional_Acknowledgment.success.IsEqual("no")) { %>
            <%  if (Model.Subscriber != null && Model.Subscriber.success.IsNotNullOrEmpty() && Model.Subscriber.success.IsEqual("yes")) { %>
    <fieldset>
        <legend>Patient Information</legend>
        <div class="column">
            <div class="row">
                <div class="fl">Medicare Number</div>
                <div class="fr strong"><%= Model.Subscriber.identification_code.IsNotNullOrEmpty() ? Model.Subscriber.identification_code : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Date of Birth</div>
                <div class="fr strong"><%= Model.Subscriber.date.IsNotNullOrEmpty() ? Model.Subscriber.date.ToDateTimeString() : string.Empty %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fl">Name</div>
                <div class="fr strong"><%= Model.Subscriber.last_name.IsNotNullOrEmpty() ? Model.Subscriber.last_name : string.Empty%>, <%= Model.Subscriber.first_name.IsNotNullOrEmpty() ? Model.Subscriber.first_name : string.Empty%> <%= Model.Subscriber.middle_name.IsNotNullOrEmpty() ? Model.Subscriber.middle_name : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl">Gender</div>
                <div class="fr strong"><%= Model.Subscriber.gender.IsNotNullOrEmpty() ? Model.Subscriber.gender : string.Empty %></div>
            </div>
        </div>
    </fieldset>
            <% } %>
    <fieldset>
        <legend>Insurance Coverage Information</legend>
        <h3>Medicare</h3>
            <%  if (Model.Medicare_Part_A != null && Model.Medicare_Part_A.success.IsNotNullOrEmpty() && Model.Medicare_Part_A.success.IsEqual("yes")) { %>
        <div class="column">
            <div class="row">
                <div class="fl">Part A</div>
                <div class="fr strong"><%= Model.Medicare_Part_A.date.IsNotNullOrEmpty() ? Model.Medicare_Part_A.date.ToDateTimeString() + " - Current" : string.Empty %></div>
            </div>
        </div>
            <%  } %>
            <%  if (Model.Medicare_Part_B != null && Model.Medicare_Part_B.success.IsNotNullOrEmpty() && Model.Medicare_Part_B.success.IsEqual("yes")) { %>
        <div class="column">
            <div class="row">
                <div class="fl">Part B</div>
                <div class="fr strong"><%= Model.Medicare_Part_B.date.IsNotNullOrEmpty() ? Model.Medicare_Part_B.date.ToDateTimeString() + " - Current" : string.Empty%></div>
            </div>
        </div>
                <%  if (Model.Health_Benefit_Plan_Coverage != null && !Model.Health_Benefit_Plan_Coverage.success.IsEqual("yes")) { %>
        <div class="wide column">
            <div class="row">
                <div class="fl">Primary Payer</div>
                <div class="fr strong">Medicare</div>
            </div>
        </div>
                <%  } %>
            <%  } %>
            <%  if (Model.Episode != null && Model.Episode.name.IsNotNullOrEmpty()) { %>
        <h3>Last Home Care Episode</h3>
        <div class="column">
            <div class="row">
                <div class="fl">Payer</div>
                <div class="fr strong"><%= Model.Episode.name.IsNotNullOrEmpty() ? Model.Episode.name : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl">NPI#</div>
                <div class="fr strong"><%= Model.Episode.reference_id.IsNotNullOrEmpty() ? Model.Episode.reference_id : string.Empty%></div>
            </div>
                <%  if (Model.Other_Agency_Data != null) { %>
            <div class="row">
                <div class="fl">Name</div>
                <div class="fr strong"><%= Model.Other_Agency_Data.name.IsNotNullOrEmpty() ? Model.Other_Agency_Data.name : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl">Address</div>
                <div class="fr strong"><%= Model.Other_Agency_Data.address1 + " " + Model.Other_Agency_Data.address2 + "<br />" + Model.Other_Agency_Data.city + ", " + Model.Other_Agency_Data.state + " " + Model.Other_Agency_Data.zip %></div>
            </div>
            <div class="row">
                <div class="fl">Phone</div>
                <div class="fr strong"><%= Model.Other_Agency_Data.phone.IsNotNullOrEmpty() ? Model.Other_Agency_Data.phone.ToPhone() : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl">Fax</div>
                <div class="fr strong"><%= Model.Other_Agency_Data.fax.IsNotNullOrEmpty() ? Model.Other_Agency_Data.fax.ToPhone() : string.Empty%></div>
            </div>
                <%  } %>
        </div>
        <div class="column">
            <div class="row">
                <div class="fl">Provider Code</div>
                <div class="fr strong"><%= Model.Episode.provider_code.IsNotNullOrEmpty() ? Model.Episode.provider_code : string.Empty%></div>
            </div>
            <div class="row">
                <div class="fl">Episode Date Range</div>
                <div class="fr strong"><%= Model.Episode.period_start.IsNotNullOrEmpty() ? Model.Episode.period_start.ToDateTimeString() : string.Empty %><%= Model.Episode.period_end.IsNotNullOrEmpty() ? " &#8211; " + Model.Episode.period_end.ToDateTimeString() : string.Empty %></div>
            </div>
        </div>
            <%  } %>
            <%  if (Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() && Model.Health_Benefit_Plan_Coverage.success.IsEqual("yes")) { %>
        <h3>Health Benefit Plan Coverage</h3>
        <div class="column">
            <div class="row">
                <div class="fl">Health Benefit Plan Coverage</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.name : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Insurance Type</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.insurance_type : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Reference Id Qualifier:</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Address</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.address1 : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Phone Number</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.phone : string.Empty %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fl">Health Benefit Plan Payer</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.payer : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Reference Id</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">Date</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.date.ToDateTimeString() : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">City</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.city : string.Empty %></div>
            </div>
            <div class="row">
                <div class="fl">State</div>
                <div class="fr strong"><%= Model.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.state : string.Empty %></div>
            </div>
        </div>
            <%  } %>
    </fieldset>
        <%  } else { %>
    <div class="ac">The Medicare Eligibility Service is not available at this time.</div>
        <%  } %>
    <%  } %>    
<% } else { %>
    <div class="ac">The Medicare Eligibility Service is not available at this time.</div>
<% } %>
    <form method="post" action="Patient/EligibilityPdf">
        <%= Html.Hidden("Eligibility.Request_Result.success", Model != null && Model.Request_Result != null && Model.Request_Result.success.IsNotNullOrEmpty() ? Model.Request_Result.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Result.response", Model != null && Model.Request_Result != null && Model.Request_Result.response.IsNotNullOrEmpty() ? Model.Request_Result.response : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.success", Model != null && Model.Request_Validation != null && Model.Request_Validation.success.IsNotNullOrEmpty() ? Model.Request_Validation.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.yes_no_response_code", Model != null && Model.Request_Validation != null && Model.Request_Validation.yes_no_response_code.IsNotNullOrEmpty() ? Model.Request_Validation.yes_no_response_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.reject_reason_code", Model != null && Model.Request_Validation != null && Model.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? Model.Request_Validation.reject_reason_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Request_Validation.follow_up_action_code", Model != null && Model.Request_Validation != null && Model.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? Model.Request_Validation.follow_up_action_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Functional_Acknowledgment.success", Model != null && Model.Functional_Acknowledgment != null && Model.Functional_Acknowledgment.success.IsNotNullOrEmpty() ? Model.Functional_Acknowledgment.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_A.success", Model != null && Model.Medicare_Part_A != null && Model.Medicare_Part_A.success.IsNotNullOrEmpty() ? Model.Medicare_Part_A.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_A.date", Model != null && Model.Medicare_Part_A != null && Model.Medicare_Part_A.date.IsNotNullOrEmpty() ? Model.Medicare_Part_A.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_B.success", Model != null && Model.Medicare_Part_B != null && Model.Medicare_Part_B.success.IsNotNullOrEmpty() ? Model.Medicare_Part_B.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Medicare_Part_B.date", Model != null && Model.Medicare_Part_B != null && Model.Medicare_Part_B.date.IsNotNullOrEmpty() ? Model.Medicare_Part_B.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.success", Model != null && Model.Subscriber != null && Model.Subscriber.success.IsNotNullOrEmpty() ? Model.Subscriber.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.date", Model != null && Model.Subscriber != null && Model.Subscriber.date.IsNotNullOrEmpty() ? Model.Subscriber.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.last_name", Model != null && Model.Subscriber != null && Model.Subscriber.last_name.IsNotNullOrEmpty() ? Model.Subscriber.last_name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.first_name", Model != null && Model.Subscriber != null && Model.Subscriber.first_name.IsNotNullOrEmpty() ? Model.Subscriber.first_name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.middle_name", Model != null && Model.Subscriber != null && Model.Subscriber.middle_name.IsNotNullOrEmpty() ? Model.Subscriber.middle_name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.identification_code", Model != null && Model.Subscriber != null && Model.Subscriber.identification_code.IsNotNullOrEmpty() ? Model.Subscriber.identification_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Subscriber.gender", Model != null && Model.Subscriber != null && Model.Subscriber.gender.IsNotNullOrEmpty() ? Model.Subscriber.gender : string.Empty)%>
        <%= Html.Hidden("Eligibility.Dependent.success", Model != null && Model.Dependent != null && Model.Dependent.success.IsNotNullOrEmpty() ? Model.Dependent.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.success", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.payer", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.payer : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.name", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.insurance_type", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.insurance_type : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.reference_id_qualifier", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.reference_id", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.reference_id : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.date", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.date : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.address1", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.address1 : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.city", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.city : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.state", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.state : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.zip", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.zip.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.zip : string.Empty)%>
        <%= Html.Hidden("Eligibility.Health_Benefit_Plan_Coverage.phone", Model != null && Model.Health_Benefit_Plan_Coverage != null && Model.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? Model.Health_Benefit_Plan_Coverage.phone : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.success", Model != null && Model.Episode != null && Model.Episode.success.IsNotNullOrEmpty() ? Model.Episode.success : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.payer", Model != null && Model.Episode != null && Model.Episode.payer.IsNotNullOrEmpty() ? Model.Episode.payer : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.name", Model != null && Model.Episode != null && Model.Episode.name.IsNotNullOrEmpty() ? Model.Episode.name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.provider_code", Model != null && Model.Episode != null && Model.Episode.provider_code.IsNotNullOrEmpty() ? Model.Episode.provider_code : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.reference_id_qualifier", Model != null && Model.Episode != null && Model.Episode.reference_id_qualifier.IsNotNullOrEmpty() ? Model.Episode.reference_id_qualifier : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.reference_id", Model != null && Model.Episode != null && Model.Episode.reference_id.IsNotNullOrEmpty() ? Model.Episode.reference_id : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.period_start", Model != null && Model.Episode != null && Model.Episode.period_start.IsNotNullOrEmpty() ? Model.Episode.period_start : string.Empty)%>
        <%= Html.Hidden("Eligibility.Episode.period_end", Model != null && Model.Episode != null && Model.Episode.period_end.IsNotNullOrEmpty() ? Model.Episode.period_end : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.name", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.name.IsNotNullOrEmpty() ? Model.Other_Agency_Data.name : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.address1", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.address1.IsNotNullOrEmpty() ? Model.Other_Agency_Data.address1 : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.address2", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.address2.IsNotNullOrEmpty() ? Model.Other_Agency_Data.address2 : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.city", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.city.IsNotNullOrEmpty() ? Model.Other_Agency_Data.city : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.state", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.state.IsNotNullOrEmpty() ? Model.Other_Agency_Data.state : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.zip", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.zip.IsNotNullOrEmpty() ? Model.Other_Agency_Data.zip : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.phone", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.phone.IsNotNullOrEmpty() ? Model.Other_Agency_Data.phone : string.Empty)%>
        <%= Html.Hidden("Eligibility.Other_Agency_Data.fax", Model != null && Model.Other_Agency_Data != null && Model.Other_Agency_Data.fax.IsNotNullOrEmpty() ? Model.Other_Agency_Data.fax : string.Empty)%>
        <div class="buttons">
            <ul>
<%  if (Model != null && Model.Request_Validation != null && Model.Request_Validation.success.IsNotNullOrEmpty() && Model.Request_Validation.success.IsEqual("no")) { %>
    <%  if (Model.Functional_Acknowledgment != null && Model.Functional_Acknowledgment.success.IsNotNullOrEmpty() && Model.Functional_Acknowledgment.success.IsEqual("no")) { %>
                <li><a class="save">Print</a></li>
    <%  } %>
<%  } %>
                <li><a class="close">Close</a></li>
            </ul>
        </div>
    </form>
</div>