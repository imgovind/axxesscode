﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfile>" %>
<%  if (Model != null) { %>
    <%  var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == false).OrderBy(a => a.Name).ToList(); %>
    <%  if (allergies != null && allergies.Count() > 0) { %>
<ul>
    <li class="ac"><h3>Active Allerg<%= allergies.Count() > 1 ? "ies" : "y" %></h3></li>
    <li>
        <span class="allergy-name sortable">Name</span>
        <span class="allergy-type sortable">Type</span>
        <span>Action</span>
    </li>
</ul>
<ol>
        <%  foreach (var allergy in allergies) { %>
    <li>
        <span class="allergy-name"><%= allergy.Name %></span>
        <span class="allergy-type"><%= allergy.Type %></span>
        <span>
            <a class="link" onclick="Patient.Allergy.Edit('<%= allergy.Id %>', '<%=Model.Id %>');return false">Edit</a>
            |
            <a class="link" onclick="Patient.Allergy.Delete('<%= allergy.Id %>', '<%=Model.Id %>');return false">Delete</a>
        </span>
    </li>
        <%  } %>
</ol>
    <%  } else { %>
<ul>
    <li class="ac"><h3>Active Allergies</h3></li>
</ul>
<h1 class="blue">No Active Allergies</h1>
    <%  } %>
    <%  allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == true).OrderBy(a => a.Name).ToList(); %>
    <%  if (allergies != null && allergies.Count() > 0) { %>
<ul>
    <li class="ac"><h3>Deleted Allerg<%= allergies.Count() > 1 ? "ies" : "y" %></h3></li>
    <li>
        <span class="allergy-name sortable">Name</span>
        <span class="allergy-type sortable">Type</span>
        <span>Action</span>
    </li>
</ul>
<ol class="dc">
        <%  foreach (var allergy in allergies) { %>
    <li>
        <span class="allergy-name"><%= allergy.Name %></span>
        <span class="allergy-type"><%= allergy.Type %></span>
        <span>
            <a class="link" onclick="Patient.Allergy.Edit('<%= allergy.Id %>', '<%=Model.Id %>');return false">Edit</a>
            |
            <a class="link" onclick="Patient.Allergy.Restore('<%= allergy.Id %>', '<%=Model.Id %>');return false">Restore</a>
        </span>
    </li>
        <%  } %>
</ol>
    <%  } else { %>
<ul>
    <li class="ac"><h3>Deleted Allergies</h3></li>
</ul>
<h1 class="blue">No Deleted Allergies</h1>
    <%  } %>
<%  } %>