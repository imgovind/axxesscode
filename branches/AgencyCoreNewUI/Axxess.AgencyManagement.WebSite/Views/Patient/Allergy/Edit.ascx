﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Allergy>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Allergy/Update", "Patient", FormMethod.Post, new { @id = "EditAllergy_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditAllergy_Id" })%>
    <%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "EditAllergy_ProfileId" })%>
    <fieldset>
        <legend>Edit Allergy</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditAllergy_Name" class="fl strong">Name</label>
                <div class="fr"><%= Html.TextBox("Name", Model != null && Model.Name.IsNotNullOrEmpty() ? Model.Name : string.Empty, new { @id = "EditAllergy_Name", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="EditAllergy_Type" class="fl strong">Type</label>
                <div class="fr"><%= Html.TextBox("Type", Model != null && Model.Type.IsNotNullOrEmpty() ? Model.Type : string.Empty, new { @id = "EditAllergy_Type", @maxlength = "50" })%></div>
                <div class="clear"></div>
                <div class="ac"><em>(e.g. Medication, Food, Animal, Plants, Environmental)</em></div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>