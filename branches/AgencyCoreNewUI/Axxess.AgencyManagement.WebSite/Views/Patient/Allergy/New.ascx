﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Allergy/Create", "Patient", FormMethod.Post, new { @id = "NewAllergy_Form" })) { %>
    <%= Html.Hidden("ProfileId", Model, new { @id = "New_Allergy_ProfileId" })%>
    <fieldset>
        <legend>New Allergy</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewAllergy_Name" class="fl strong">Name</label>
                <div class="fr"><%= Html.TextBox("Name", "", new { @id = "NewAllergy_Name", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="NewAllergy_Type" class="fl strong">Type</label>
                <div class="fr"><%= Html.TextBox("Type", "", new { @id = "NewAllergy_Type", @maxlength = "50" })%></div>
                <div class="clear"></div>
                <div class="ac"><em>(e.g. Medication, Food, Animal, Plants, Environmental)</em></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save &#38; Exit</a></li>
            <li><a class="save clear">Save &#38; Add Another</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>