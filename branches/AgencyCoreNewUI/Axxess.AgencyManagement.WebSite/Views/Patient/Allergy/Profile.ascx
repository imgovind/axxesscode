﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfileViewData>" %>
<span class="wintitle">Allergy Profile | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
    <%= Html.Hidden("Id", Model.AllergyProfile.Id)%>
    <%= Html.Hidden("PatientId", Model.AllergyProfile.PatientId)%>
    <div class="note">
        <fieldset>
            <legend>Patient Information</legend>
            <div class="column">
                <div class="row">
                    <label class="fl strong">Patient</label>
                    <div class="fr"><%= Model.Patient.DisplayNameWithMi %></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label class="fl strong">MRN</label>
                    <div class="fr"><%= Model.Patient.PatientIdNumber %></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-allergy">Add Allergy</a></li>
            <li><a class="print">Print Allergy Profile</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="allergy-profile acore-grid"><% Html.RenderPartial("Allergy/List", Model.AllergyProfile); %></div>
    <div class="buttons">
        <ul>
            <li><a class="grid-refresh">Refresh Medications</a></li>
        </ul>
    </div>
</div>
