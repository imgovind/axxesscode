﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfileViewData>" %>
<%  var allergies = Model != null && Model.AllergyProfile != null && Model.AllergyProfile.Allergies.IsNotNullOrEmpty() ? Model.AllergyProfile.Allergies.ToObject<List<Allergy>>().ToList() : new List<Allergy>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Patient/allergyprofile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : string.Empty) + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty) + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : string.Empty) + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty) : string.Empty) : string.Empty).Clean()%>",
        patientname:    "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br />" : "<br />") : string.Empty).Clean() %>",
        mr:             "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>"
    };
    PrintPreview.BuildSections([
<%  bool first = true; %>
<%  if (allergies != null && allergies.Count() > 0) { %>
    <%  foreach (var allergy in allergies) { %>
        <%  if (first) first = false; %>
        <%  else { %>
        ,
        <%  } %>
        { Content: [ [
            "<%= allergy.Name.Clean() %>",
            "<%= allergy.Type.Clean() %>" ] ] }
    <%  } %>
<%  } %>
    ]);
</script>