﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Pending Patient Admissions | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <ul>
            <li><a class="new-patient">New Patient</a></li>
        </ul>
<%  } %>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<PendingPatient>().Name("ListPatientPending_Grid").Columns(columns => {
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(false).Width(20);
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(10);
            columns.Bound(p => p.PrimaryInsuranceName).Title("Primary Insurance").Sortable(false).Width(20);
            columns.Bound(p => p.Branch).Title("Branch").Sortable(false).Width(20);
            columns.Bound(p => p.CreatedDateFormatted).Title("Date Added").Width(10).Sortable(false);
            columns.Bound(p => p.Id).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"Patient.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Patient.Admit('<#=Id#>', '<#=Type#>');return false\">Admit</a> | <a class=\"link\" onclick=\"Patient.NonAdmit('<#=Id#>');return false\">Non-Admit</a>").Title("Action").Width(15);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("PendingAdmissionGrid", "Patient")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>