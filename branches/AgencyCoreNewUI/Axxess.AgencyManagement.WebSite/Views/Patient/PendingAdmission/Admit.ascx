﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PendingPatient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Admit/Create", "Patient", FormMethod.Post, new { @id = "AdmitPatient_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "AdmitPatient_Id" })%>
    <%= Html.Hidden("IsAdmit", "true", new { @id = "AdmitPatient_IsAdmit" })%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>Admit Patient</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_PatientID" class="fl strong"><span class="green">(M0020)</span> Patient ID</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "AdmitPatient_PatientID", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_StartOfCareDate" class="fl strong"><span class="green">(M0030)</span> Start of Care Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="AdmitPatient_StartOfCareDate" /></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_EpisodeStartDate" class="fl strong">Episode Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="AdmitPatient_EpisodeStartDate" /></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_PatientReferralDate" class="fl strong">Referral Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ReferralDate" value="<%= (Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : "" %>" id="AdmitPatient_PatientReferralDate" /></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_CaseManager" class="fl strong">Case Manager</label>
                <div class="fr"><%= Html.CaseManagers("CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "AdmitPatient_CaseManager", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="AdmitPatient_LocationId" class="fl strong">Agency Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty, new { @id = "AdmitPatient_LocationId", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_PrimaryInsurance" class="fl strong">Primary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance.ToString(), Model.AgencyLocationId, false, new { @id = "AdmitPatient_PrimaryInsurance", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_SecondaryInsurance" class="fl strong">Secondary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance.ToString(), Model.AgencyLocationId, false, new { @id = "AdmitPatient_SecondaryInsurance" })%></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_TertiaryInsurance" class="fl strong">Tertiary Insurance</label>
                <div class="fr"><%= Html.InsurancesByBranch("TertiaryInsurance", Model.TertiaryInsurance.ToString(), Model.AgencyLocationId, false, new { @id = "AdmitPatient_TertiaryInsurance" })%></div>
            </div>
            <div class="row">
                <label for="AdmitPatient_Assign" class="fl strong">Assign to Clinician</label>
                <div class="fr"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : string.Empty, new { @id = "AdmitPatient_Assign", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="AdmitPatient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="IsFaceToFaceEncounterCreated" />
                        <label for="AdmitPatient_IsFaceToFaceEncounterCreated">
                            Create a face to face encounter<br />
                            <em>(this is applicable for SOC date after 01/01/2011)</em>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Admit</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>