﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("NonAdmit/Create", "Patient", FormMethod.Post, new { @id = "NonAdmitPatient_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmitPatient_Id" })%>
    <%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmitPatient_IsAdmit" })%>
    <fieldset>
        <legend>Non-Admission Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.DisplayNameWithMi %></div>
            </div>
            <div class="row">
                <label for="NonAdmitPatient_Date" class="fl strong">Date</label>
                <div class="fr"><%= Html.TextBox("NonAdmitDate", DateTime.Today.ToShortDateString(), new { @id = "NonAdmitPatient_Date", @class = "required", @type = "date" })%></div>
            </div>
        </div>
        <div class="wide column">
            <div class="row">
                <label class="strong">Reason Not Admitted</label>
                <div class="checkgroup">
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonInAppropriate">Inappropriate For Home Care</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonRefused" type="checkbox" value="Patient Refused Service" name="Reason" class="required" />
                        <label for="NonAdmit_Patient_ReasonRefused">Patient Refused Service</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonOutofService">Out of Service Area</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonOnService">On Service with another agency</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonNotaProvider">Not a Provider</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonNotHomebound">Not Homebound</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonRedirected">Redirected to alternate care facility</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitPatient_ReasonOther" type="checkbox" value="Other" name="Reason" class="required" />
                        <label for="NonAdmitPatient_ReasonOther">Other <em>(specify in Comments)</em></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="NonAdmitPatient_Comments" class="strong">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", "", new { @id = "NonAdmitPatient_Comments", @maxcharacters = "200" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>