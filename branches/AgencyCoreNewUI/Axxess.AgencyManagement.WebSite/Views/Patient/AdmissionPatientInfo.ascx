﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% var type = ViewData.ContainsKey("Type") && ViewData["Type"]!=null ? ViewData["Type"].ToString() : string.Empty; %>
<% var isDischarge = ViewData.ContainsKey("IsDischarge") && ViewData["IsDischarge"] != null ? ViewData["IsDischarge"].ToString().IsEqual("true") : false; %>
<% var typeTitleCase = type.ToTitleCase(); %>
<span class="wintitle">Patient Information | <%= Model.DisplayName %></span>
<% using (Html.BeginForm("AdmissionPatient" + typeTitleCase, "Patient", FormMethod.Post, new { @id = type.ToLowerCase() + "Admission_Form" })){ %>
<%= string.Format("<input type=\"hidden\" value=\"{0}\" name=\"Id\" id=\"{1}_Admission_Id\">", Model.Id, typeTitleCase)%>  
<%= Html.Hidden("AdmissionId", Model.AdmissionId, new { @id = typeTitleCase + "_Admission_AdmissionId" })%>     
<div class="wrapper main">
    <fieldset>  
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_FirstName" class="float-left"><span class="green">(M0040)</span> First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = type + "_Admission_FirstName", @class = "text  input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_MiddleInitial" class="float-left">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = type + "_Admission_MiddleInitial", @style = "width:20px;", @maxlength="1" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_LastName" class="float-left">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = type + "_Admission_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label class="float-left"><span class="green">(M0069)</span> Gender</label>
                <div class="fr"><%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "", @class = "required" })%><label for="" class="fixed short">Female</label><%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "", @class = "required" })%><label for="" class="fixed short">Male</label></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_DOB" class="float-left"><span class="green">(M0066)</span> Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="<%=typeTitleCase %>_Admission_DOB" /></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_MaritalStatus" class="float-left">Marital Status</label>
                <div class="fr">
                    <%  var maritalStatus = new SelectList(new[] {
                            new SelectListItem { Text = "** Select **", Value = "0" },
                            new SelectListItem { Text = "Married", Value = "Married" },
                            new SelectListItem { Text = "Divorce", Value = "Divorce" },
                            new SelectListItem { Text = "Widowed", Value = "Widowed" },
                            new SelectListItem { Text = "Single", Value = "Single" }
                        }, "Value", "Text", Model.MaritalStatus); %>
                    <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = type + "_Admission_MaritalStatus", @class = "input_wrapper" })%>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Height</label>
                <div class="fr">
                    <%= Html.TextBox("Height", Model.Height > 0 ? Model.Height.ToString() : string.Empty, new { @id = type + "_Admission_Height", @class = "numeric vitals", @maxlength = "3" }) %>
                    <%= Html.RadioButton("HeightMetric", "0", Model.HeightMetric == 0, new { @id = type + "_Admission_HeightMetric0" })%>
                    <label for="<%=typeTitleCase %>_Admission_HeightMetric0" style="display:inline-block;width:20px!important">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model.HeightMetric == 1, new { @id = type + "_Admission_HeightMetric1" })%>
                    <label for="<%=typeTitleCase %>_Admission_HeightMetric1" style="display:inline-block;width:20px!important">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Weight</label>
                <div class="fr">
                    <%= Html.TextBox("Weight", Model.Weight > 0 ? Model.Weight.ToString() : string.Empty, new { @id = type + "_Admission_Weight", @class = "numeric vitals", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model.WeightMetric == 0, new { @id = type + "_Admission_WeightMetric0" })%>
                    <label for="<%=typeTitleCase %>_Admission_WeightMetric0" style="display:inline-block;width:20px!important">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model.WeightMetric == 1, new { @id = type + "_Admission_WeightMetric1" })%>
                    <label for="<%=typeTitleCase %>_Admission_WeightMetric1" style="display:inline-block;width:20px!important">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_LocationId" class="float-left">Agency Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = type + "_Admission_LocationId", @class = "BranchLocation" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_PatientID" class="float-left"><span class="green">(M0020)</span> Patient ID</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = type + "_Admission_PatientID", @class = "text required input_wrapper", @maxlength = "15" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_MedicareNumber" class="float-left"><span class="green">(M0063)</span> Medicare Number</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = type + "_Admission_MedicareNumber", @maxlength = "12" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_MedicaidNumber" class="float-left"><span class="green">(M0065)</span> Medicaid Number</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = type + "_Admission_MedicaidNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_SSN" class="float-left"><span class="green">(M0064)</span> SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = type + "_Admission_SSN", @class = "numeric ssn", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_StartOfCareDate" class="float-left"><span class="green">(M0030)</span> Start of Care Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= !Model.StartofCareDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.StartofCareDate.ToString("MM/dd/yyyy") : string.Empty %>" id="<%=typeTitleCase %>_Admission_StartOfCareDate" /></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_DischargeDate" class="float-left"> Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker<%= isDischarge? " required" : string.Empty %>" name="DischargeDate" value="<%= !Model.DischargeDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.DischargeDate.ToString("MM/dd/yyyy") : string.Empty %>" id="<%=typeTitleCase %>_Admission_DischargeDate" /></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_CaseManager" class="float-left">Case Manager</label>
                <div class="fr"><%= Html.CaseManagers("CaseManagerId", Model.CaseManagerId.ToString(), new { @id = type + "_Admission_CaseManager", @class = "Users required notzero" })%></div>
            </div>
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_Assign" class="float-left">Clinician</label>
                <div class="fr"><%= Html.Clinicians("UserId",  Model.UserId.ToString(), new { @id = type + "_Admission_Assign", @class = "Employees input_wrapper required notzero" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <table class="form">
            <tbody>
                <%string[] ethnicities = Model.Ethnicities != null && Model.Ethnicities != "" ? Model.Ethnicities.Split(';') : null;  %>
                <tr>
                    <td><%= string.Format("<input id='{1}_Admission_EthnicRaces0' type='checkbox' value='0' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("0") ? "checked='checked'" : "",typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces0">American Indian or Alaska Native</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_EthnicRaces1' type='checkbox' value='1' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("1") ? "checked='checked'" : "",typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces1">Asian</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_EthnicRaces2' type='checkbox' value='2' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("2") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces2">Black or African-American</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_EthnicRaces3' type='checkbox' value='3' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("3") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces3">Hispanic or Latino</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='{1}_Admission_EthnicRaces4' type='checkbox' value='4' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("4") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces4">Native Hawaiian or Pacific Islander</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_EthnicRaces5' type='checkbox' value='5' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("5") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces5">White</label></td>
                    <td colspan="2"><%= string.Format("<input id='{1}_Admission_EthnicRaces6' type='checkbox' value='6' name='EthnicRaces' class='required' {0} />", ethnicities != null && ethnicities.Contains("6") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_EthnicRaces6">Unknown</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span></legend>
        <table class="form">
            <tbody>
                <%string[] paymentSources = Model.PaymentSource != null && Model.PaymentSource != "" ? Model.PaymentSource.Split(';') : null;  %>
                <tr>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceNone' type='checkbox' value='0' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("0") ? "checked='checked'" : "",typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceNone">None; no charge for current services</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceMedicare' type='checkbox' value='1' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("1") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceMedicare">Medicare (traditional fee-for-service)</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceMedicareHmo' type='checkbox' value='2' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("2") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceMedicareHmo">Medicare (HMO/ managed care)</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceMedicaid' type='checkbox' value='3' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("3") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceMedicaid">Medicaid (traditional fee-for-service)</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceMedicaidHmo' type='checkbox' value='4' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("4") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceMedicaidHmo">Medicaid (HMO/ managed care)</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceWorkers' type='checkbox' value='5' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("5") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceWorkers">Workers&#8217; compensation</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceTitleProgram' type='checkbox' value='6' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("6") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceTitleProgram">Title programs (e.g., Titile III,V, or XX)</label></td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceOtherGovernment' type='checkbox' value='7' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("7") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceOtherGovernment">Other government (e.g.,CHAMPUS,VA,etc)</td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourcePrivate' type='checkbox' value='8' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("8") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourcePrivate">Private insurance</td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourcePrivateHmo' type='checkbox' value='9' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("9") ? "checked='checked'" : "",typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourcePrivateHmo">Private HMO/ managed care</td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceSelf' type='checkbox' value='10' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("10") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceSelf">Self-pay</td>
                    <td><%= string.Format("<input id='{1}_Admission_PaymentSourceUnknown' type='checkbox' value='11' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("11") ? "checked='checked'" : "", typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSourceUnknown">Unknown</td>
                </tr>
                <tr>
                    <td colspan='4'><%= string.Format("<input id='{1}_Admission_PaymentSource' type='checkbox' value='12' name='PaymentSources' class='required' {0} />", paymentSources != null && paymentSources.Contains("12") ? "checked='checked'" : "",typeTitleCase)%><label for="<%=typeTitleCase %>_Admission_PaymentSource" class="radio more">Other (specify)</label><%= Html.TextBox("OtherPaymentSource", Model.OtherPaymentSource, new { @id = type + "_Admission_OtherPaymentSource", @style = "display:none;" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="marginBreak">
            <div class="column">
                <div class="row"><label for="<%=typeTitleCase %>_Admission_AddressLine1" class="float-left">Address Line 1</label><div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = type + "_Admission_AddressLine1", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="<%=typeTitleCase %>_Admission_AddressLine2" class="float-left">Address Line 2</label><div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = type + "_Admission_AddressLine2", @maxlength = "50" })%></div></div>
                <div class="row"><label for="<%=typeTitleCase %>_Admission_AddressCity" class="float-left">City</label><div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = type + "_Admission_AddressCity", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="<%=typeTitleCase %>_Admission_AddressStateCode" class="float-left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = type + "_Admission_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = type + "_Admission_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "9" })%></div></div>
            </div>
            <div class="column">
                <div class="row"><label class="float-left">Home Phone</label><div class="fr"><span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=3 ? Model.PhoneHome.Substring(0, 3) : "", new { @id = type + "_Admission_HomePhone1", @class = "autotext required digits phone-short", @maxlength = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=6  ? Model.PhoneHome.Substring(3, 3) : "", new { @id = type + "_Admission_HomePhone2", @class = "autotext required digits phone-short", @maxlength = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10  ? Model.PhoneHome.Substring(6, 4) : "", new { @id = type + "_Admission_HomePhone3", @class = "autotext required digits phone-long", @maxlength = "4" })%></span></div></div>
                <div class="row"><label class="float-left">Mobile Phone</label><div class="fr"><span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 3 ? Model.PhoneMobile.Substring(0, 3) : "", new { @id = type + "_Admission_MobilePhone1", @class = "autotext digits phone-short", @maxlength = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 6 ? Model.PhoneMobile.Substring(3, 3) : "", new { @id = type + "_Admission_MobilePhone2", @class = "autotext digits phone-short", @maxlength = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 10 ? Model.PhoneMobile.Substring(6, 4) : "", new { @id = type + "_Admission_MobilePhone3", @class = "autotext digits phone-long", @maxlength = "4" })%></span></div></div>
                <div class="row"><label for="<%=typeTitleCase %>_Admission_Email" class="float-left">Email</label><div class="fr"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = type + "_Admission_Email", @maxlength = "50" })%></div></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Pharmacy</legend>
        <div class="halfRow">
            <div class="marginBreak">
                <div class="column"><div class="row"><label for="<%=typeTitleCase %>_Admission_PharmacyName" class="float-left">Name</label><div class="fr"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = type + "_Admission_PharmacyName", @maxlength = "100" })%></div></div></div>
                <div class="column"><div class="row"><label for="<%=typeTitleCase %>_Admission_PharmacyPhone1" class="float-left">Phone</label><div class="fr"><span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 3 ? Model.PharmacyPhone.Substring(0, 3) : "", new { @id = type + "_Admission_PharmacyPhone1", @class = "autotext digits phone-short", @maxlength = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 6 ? Model.PharmacyPhone.Substring(3, 3) : "", new { @id = type + "_Admission_PharmacyPhone2", @class = "autotext digits phone-short", @maxlength = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 10 ? Model.PharmacyPhone.Substring(6, 4) : "", new { @id = type + "_Admission_PharmacyPhone3", @class = "autotext digits phone-long", @maxlength = "4" })%></span></div></div></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
            <% var htmlAttributesPrimary = new Dictionary<string, string>(); htmlAttributesPrimary.Add("id", type + "_Admission_PrimaryInsurance"); htmlAttributesPrimary.Add("class", "Insurances required notzero");%>
            <label for="<%=typeTitleCase %>_Admission_PrimaryInsurance" class="float-left">Primary</label>
            <div class="fr"><%= Html.Insurances("PrimaryInsurance", Model.PrimaryInsurance.ToString(), false, htmlAttributesPrimary)%></div>
            <div id="<%=typeTitleCase %>_Admission_PrimaryInsuranceContent">
            <% if (Model.PrimaryInsurance >= 1000)
               { %>
                    <div class="margin fr" >
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_PrimaryHealthPlanId" class="float-left">Health Plan Id</label><div class="fr"><%= Html.TextBox("PrimaryHealthPlanId", Model.PrimaryHealthPlanId, new { @id = type + "_Admission_PrimaryHealthPlanId" })%></div></div>
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_PrimaryGroupName" class="float-left">Group Name </label><div class="fr"><%= Html.TextBox("PrimaryGroupName", Model.PrimaryGroupName, new { @id = type + "_Admission_PrimaryGroupName" })%></div></div>
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_PrimaryGroupId" class="float-left">Group Id</label><div class="fr"><%= Html.TextBox("PrimaryGroupId", Model.PrimaryGroupId, new { @id = type + "_Admission_PrimaryGroupId" })%></div></div>
                    </div>
                    <%} %>
            </div>
            </div>
            <div class="row">
            <% var htmlAttributesSecondary = new Dictionary<string, string>(); htmlAttributesSecondary.Add("id", type + "_Admission_SecondaryInsurance"); htmlAttributesSecondary.Add("class", "Insurances"); %>
            <label for="<%=typeTitleCase %>_Admission_SecondaryInsurance" class="float-left">Secondary</label><div class="fr"><%= Html.Insurances("SecondaryInsurance", Model.SecondaryInsurance.ToString(), false, htmlAttributesSecondary)%></div>
            <div class="row" id="<%=typeTitleCase %>_Admission_SecondaryInsuranceContent">
            <% if (Model.SecondaryInsurance >= 1000)
               { %>
                    <div class="margin fr" >
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_SecondaryHealthPlanId" class="float-left">Health Plan Id</label><div class="fr"><%= Html.TextBox("SecondaryHealthPlanId", Model.SecondaryHealthPlanId, new { @id = type + "_Admission_SecondaryHealthPlanId" })%></div></div>
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_SecondaryGroupName" class="float-left">Group Name </label><div class="fr"><%= Html.TextBox("SecondaryGroupName", Model.SecondaryGroupName, new { @id = type + "_Admission_SecondaryGroupName" })%></div></div>
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_SecondaryGroupId" class="float-left">Group Id</label><div class="fr"><%= Html.TextBox("SecondaryGroupId", Model.SecondaryGroupId, new { @id = type + "_Admission_SecondaryGroupId" })%></div></div>
                    </div>
                    <%} %>
            
            </div>
            </div>
            <div class="row"><% var htmlAttributesTertiary = new Dictionary<string, string>(); htmlAttributesTertiary.Add("id", type + "_Admission_TertiaryInsurance"); htmlAttributesTertiary.Add("class", "Insurances"); %><label for="<%=typeTitleCase %>_Admission_TertiaryInsurance" class="float-left">Tertiary</label><div class="fr"><%= Html.Insurances("TertiaryInsurance", Model.TertiaryInsurance.ToString(), false, htmlAttributesTertiary)%></div>
            <div class="row" id="<%=typeTitleCase %>_Admission_TertiaryInsuranceContent">
             <% if (Model.TertiaryInsurance >= 1000)
               { %>
                    <div class="margin fr" >
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_TertiaryHealthPlanId" class="float-left">Health Plan Id</label><div class="fr"><%= Html.TextBox("TertiaryHealthPlanId", Model.TertiaryHealthPlanId, new { @id = type + "_Admission_TertiaryHealthPlanId" })%></div></div>
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_TertiaryGroupName" class="float-left">Group Name </label><div class="fr"><%= Html.TextBox("TertiaryGroupName", Model.TertiaryGroupName, new { @id = type + "_Admission_TertiaryGroupName" })%></div></div>
                        <div class="row" ><label for="<%=typeTitleCase %>_Admission_TertiaryGroupId" class="float-left">Group Id</label><div class="fr"><%= Html.TextBox("TertiaryGroupId", Model.TertiaryGroupId, new { @id = type + "_Admission_TertiaryGroupId" })%></div></div>
                    </div>
                    <%} %>
            </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Emergency Triage <span class="light">(Select one)</span></legend>
        <div class="column">
            <div class="row">
                <%= Html.RadioButton("Triage", "1", Model.Triage == 1, new { @id = type + "_Admission_Triage1", @class = "required Triage float-left" })%>
                <div class="float-left strong">&#160;1.&#160;</div>
                <label class="normal margin" for="<%=typeTitleCase %>_Admission_Triage1"><strong>Life threatening</strong> (or potential) and requires ongoing medical treatment. When necessary,appropriate arrangements for evacuation to an acute care facility will be made.</label></div>
            <div class="row">
                <%= Html.RadioButton("Triage", "2", Model.Triage == 2, new { @id = type + "_Admission_Triage2", @class = "required Triage float-left" })%>
                <div class="float-left strong">&#160;2.&#160;</div>
                <label class="normal margin" for="<%=typeTitleCase %>_Admission_Triage2"><strong>Not life threatening but would suffer severe adverse effects</strong> from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</label></div>
            <div class="row">
                <%= Html.RadioButton("Triage", "3", Model.Triage == 3, new { @id = type + "_Admission_Triage3", @class = "required Triage float-left" })%>
                <div class="float-left strong">&#160;3.&#160;</div>
                <label class="normal margin" for="<%=typeTitleCase %>_Admission_Triage3"><strong>Visits could be postponed 24-48 hours</strong> without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</label></div>
            <div class="row">
                <%= Html.RadioButton("Triage", "4", Model.Triage == 4, new { @id = type + "_Admission_Triage4", @class = "required Triage float-left" })%>
                <div class="float-left strong">&#160;4.&#160;</div>
                <label class="normal margin" for="<%=typeTitleCase %>_Admission_Triage4"><strong>Visits could be postponed 72-96 hours</strong> without adverse effects (i.e., post op withno open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</label></div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Evacuation Zone</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Evacuation Zone</label>
                <div class="fr">
                    <%  var evacuationZone = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Zone A", Value = "A" },
                            new SelectListItem { Text = "Zone B", Value = "B" },
                            new SelectListItem { Text = "Zone C", Value = "C"},
                            new SelectListItem { Text = "Zone D", Value = "D" },
                            new SelectListItem { Text = "Zone E", Value = "E" },
                            new SelectListItem { Text = "Zone NE", Value = "NE"},
                            new SelectListItem { Text = "Zone SN", Value = "SN" }
                        }, "Value", "Text", Model.EvacuationZone); %>
                    <%= Html.DropDownList("EvacuationZone", evacuationZone, new { @id = type + "_Admission_EvacuationZone", @class = "input_wrapper" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Services Required <span class="light">(Optional)</span></legend>
        <table class="form">
            <tbody><%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <tr>
                    <td><%= string.Format("<input id ='Edit_Admission_ServicesRequiredCollection0' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0").ToChecked()) %><label for="ServicesRequiredCollection0">SNV</label></td>
                    <td><%= string.Format("<input id ='Edit_Admission_ServicesRequiredCollection1' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1").ToChecked()) %><label for="ServicesRequiredCollection1">HHA</label></td>
                    <td><%= string.Format("<input id ='Edit_Admission_ServicesRequiredCollection2' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2").ToChecked()) %><label for="ServicesRequiredCollection2">PT</label></td>
                    <td><%= string.Format("<input id ='Edit_Admission_ServicesRequiredCollection3' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3").ToChecked()) %><label for="ServicesRequiredCollection3">OT</label></td>
                    <td><%= string.Format("<input id ='Edit_Admission_ServicesRequiredCollection4' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4").ToChecked()) %><label for="ServicesRequiredCollection4">ST</label></td>
                    <td><%= string.Format("<input id ='Edit_Admission_ServicesRequiredCollection5' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5").ToChecked()) %><label for="ServicesRequiredCollection5">MSW</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>DME Needed <span class="light">(Optional)</span></legend>
        <table class="form"><%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="DMECollection" />
            <tbody>
                <tr class="firstrow">
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection0' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0").ToChecked()) %><label for="DMECollection0">Bedside Commode</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection1' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1").ToChecked()) %><label for="DMECollection1">Cane</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection2' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2").ToChecked()) %><label for="DMECollection2">Elevated Toilet Seat</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection3' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3").ToChecked()) %><label for="DMECollection3">Grab Bars</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection4' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4").ToChecked()) %><label for="DMECollection4">Hospital Bed</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection5' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5").ToChecked()) %><label for="DMECollection5">Nebulizer</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection6' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6").ToChecked()) %><label for="DMECollection6">Oxygen</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection7' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7").ToChecked()) %><label for="DMECollection7">Tub/Shower Bench</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection8' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8").ToChecked()) %><label for="DMECollection8">Walker</label></td>
                    <td><%= string.Format("<input id='Edit_Admission_DMECollection9' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9").ToChecked()) %><label for="DMECollection9">Wheelchair</label></td>
                </tr>
                <tr>
                    <td colspan="5"><%= string.Format("<input id='Edit_Admission_DMECollection10' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10").ToChecked()) %><label for="<%=typeTitleCase %>_Admission_OtherDME">Other</label><%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = type + "_Admission_OtherDME", @style = "display:none;" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="<%=typeTitleCase %>_Admission_ReferralPhysician" class="float-left">Physician</label>
                <div class="fr"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = type + "_Admission_ReferrerPhysician", @class = "physician-picker" })%></div>
            </div>
            <div class="row"><label for="<%=typeTitleCase %>_Admission_AdmissionSource" class="float-left">Admission Source</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource, new { @id = type + "_Admission_AdmissionSource", @class = "AdmissionSource" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="<%=typeTitleCase %>_Admission_OtherReferralSource" class="float-left">Other Referral Source</label><div class="fr"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = type + "_Admission_OtherReferralSource", @maxlength = "30" })%></div></div>
            <div class="row">
            <label for="<%=typeTitleCase %>_Admission_PatientReferralDate" class="float-left"><span class="green">(M0104)</span> Referral Date</label><div class="fr"><input type="text" class="date-picker" name="ReferralDate" value="<%= !Model.ReferralDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.ReferralDate.ToString("MM/dd/yyyy") : string.Empty %>" id="<%=typeTitleCase %>_Admission_PatientReferralDate" /></div></div>
            <div class="row"><label for="<%=typeTitleCase %>_Admission_InternalReferral" class="float-left">Internal Referral</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = type + "_Admission_InternalReferral", @class = "Users valid" })%></div></div>
        </div>
    </fieldset>
     <fieldset><legend>Discharged Reason</legend><div class="wide column"><div class="row"><textarea id="<%=typeTitleCase %>_Admission_DischargeReason" name="DischargeReason" cols="5" rows="6"><%= Model.DischargeReason%></textarea></div></div></fieldset>
    <fieldset><legend>Comments</legend><div class="wide column"><div class="row"><textarea id="<%=typeTitleCase %>_Admission_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea></div></div></fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('<%=type.ToLowerCase() %>patientadmission');">Exit</a></li></ul></div>
</div>
<% } %>
<script type="text/javascript">
    $("#window_<%= type.ToLowerCase()%>patientadmission select.Insurances").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            Acore.Open('NewInsurancePayor');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") selectList.selectedIndex = 0;
    });
    $("#<%=typeTitleCase %>_Admission_LocationId").change(function() {
        Agency.LoadAgencyInsurances($(this).val(), "<%=typeTitleCase %>_Admission_PrimaryInsurance");
        Agency.LoadAgencyInsurances($(this).val(), "<%=typeTitleCase %>_Admission_SecondaryInsurance");
        Agency.LoadAgencyInsurances($(this).val(), "<%=typeTitleCase %>_Admission_TertiaryInsurance");
    });
    $("#<%=typeTitleCase %>_Admission_PrimaryInsurance").change(function() {
    if ($(this).find(":selected").attr("ishmo") == "1") Patient.LoadInsuranceContent("#<%=typeTitleCase %>_Admission_PrimaryInsuranceContent", '<%=Model.Id %>', $(this).val(), '<%= typeTitleCase%>_Admission', 'Primary');
    else $("#<%=typeTitleCase %>_Admission_PrimaryInsuranceContent").empty();
    });
    $("#<%=typeTitleCase %>_Admission_SecondaryInsurance").change(function() {
    if ($(this).find(":selected").attr("ishmo") == "1") Patient.LoadInsuranceContent("#<%=typeTitleCase %>_Admission_SecondaryInsuranceContent", '<%=Model.Id %>', $(this).val(), '<%= typeTitleCase %>_Admission', 'Secondary');
    else $("#<%=typeTitleCase %>_Admission_SecondaryInsuranceContent").empty()
    });
    $("#<%=typeTitleCase %>_Admission_TertiaryInsurance").change(function() {
    if ($(this).find(":selected").attr("ishmo") == "1") Patient.LoadInsuranceContent("#<%=typeTitleCase %>_Admission_TertiaryInsuranceContent", '<%=Model.Id %>', $(this).val(), '<%= typeTitleCase %>_Admission', 'Tertiary')
    else $("#<%=typeTitleCase %>_Admission_TertiaryInsuranceContent").empty()
    });
    $("#<%=typeTitleCase %>_Admission_SSN").mask("999999999", { placeholder: "" });
</script>