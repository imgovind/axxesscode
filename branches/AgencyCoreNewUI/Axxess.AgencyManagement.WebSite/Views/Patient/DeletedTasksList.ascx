﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Deleted Tasks/Documents | <%= Model.DisplayName %></span>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<ScheduleEvent>().Name("ListDeletedTasks_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(e => e.DisciplineTaskName).Title("Task/Document").Sortable(true);
            columns.Bound(e => e.EventDateSortable).Title("Scheduled Date").Width(100).Sortable(true);
            columns.Bound(e => e.StatusName).Title("Status").Sortable(true);
            columns.Bound(e => e.UserDisplayName).Title("User").Sortable(true);
            columns.Bound(e => e.EventId).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"Schedule.Restore('<#=EventId#>','<#=EpisodeId#>','<#=PatientId#>');return false\">Restore</a>").Title("Action").Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("DeletedTasksGrid", "Patient", new { id = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>