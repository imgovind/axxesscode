﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">Physician Face-to-face Encounter | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<% using (Html.BeginForm("FaceToFaceEncounter/Create", "Patient", FormMethod.Post, new { @id = "NewFaceToFaceEncounter_Form" })) { %>
    <fieldset>
        <legend>Physician Face to Face Encounter</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong" for="NewFaceToFaceEncounter_PatientName">Patient Name</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : "", new { @id = "NewFaceToFaceEncounter_PatientName", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewFaceToFaceEncounter_EpisodeList">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "NewFaceToFaceEncounter_EpisodeList", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewFaceToFaceEncounter_RequestDate">Request Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="RequestDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now %>" id="NewFaceToFaceEncounter_RequestDate" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong" for="NewFaceToFaceEncounter_PhysicianDropDown">Physician</label>
                <div class="fr">
                    <%= Html.TextBox("PhysicianId", "", new { @id = "NewFaceToFaceEncounter_PhysicianDropDown", @class = "physician-picker required notzero" })%>
                    <br />
                    <div class="button-with-arrow"><a class="new-physician">New Physician</a></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton("Certification", "1", true, new { @id = "NewFaceToFaceEncounter_Certification1" }) %>
                        <label for="NewFaceToFaceEncounter_Certification1">POC Certifying Physician</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Certification", "2", new { @id = "NewFaceToFaceEncounter_Certification2" }) %>
                        <label for="NewFaceToFaceEncounter_Certification2">Non POC Certifying Physician</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <strong>Note:</strong>
                <em>Completing this document creates a Physician Face to Face Encounter request document that must be submitted to the physician. The physician will be required to certify that the patient is homebound and the home health services provided are medically necessary. The physician will certify by signing the face to face encounter document and returning to the home health agency.</em>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "NewFaceToFaceEncounter_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Submit</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>