﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Edit Patient | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Patient", FormMethod.Post, new { @id = "EditPatient_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditPatient_Id" })%>  
    <%= Html.Hidden("Status", Model.Status, new { @id = "EditPatient_Status" })%>    
    <fieldset>  
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_FirstName" class="fl strong"><span class="green">(M0040)</span> First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditPatient_FirstName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_MiddleInitial" class="fl strong">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = "EditPatient_MiddleInitial", @class = "mi", @maxlength="1" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditPatient_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label class="fl strong"><span class="green">(M0069)</span> Gender</label>
                <div class="fr radio">
                    <%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "EditPatient_Gender_F", @class = "required" })%>
                    <label for="EditPatient_Gender_F" class="fixed short">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "EditPatient_Gender_M", @class = "required" })%>
                    <label for="EditPatient_Gender_M" class="fixed short">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="EditPatient_DOB" class="fl strong"><span class="green">(M0066)</span> Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="EditPatient_DOB" /></div>
            </div>
            <div class="row">
                <label for="EditPatient_MaritalStatus" class="fl strong">Marital Status</label>
                <div class="fr">
                    <%  var maritalStatus = new SelectList(new[] {
                            new SelectListItem { Text = "** Select **", Value = "0" },
                            new SelectListItem { Text = "Married", Value = "Married" },
                            new SelectListItem { Text = "Divorce", Value = "Divorce" },
                            new SelectListItem { Text = "Widowed", Value = "Widowed" },
                            new SelectListItem { Text = "Single", Value = "Single" }
                        }, "Value", "Text", Model.MaritalStatus); %>
                    <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "EditPatient_MaritalStatus" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Height</label>
                <div class="fr radio">
                    <%= Html.TextBox("Height", Model.Height > 0 ? Model.Height.ToString() : string.Empty, new { @id = "EditPatient_Height", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", Model.HeightMetric == 0, new { @id = "EditPatient_HeightMetric0" })%>
                    <label for="EditPatient_HeightMetric0" class="fixed shortest">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model.HeightMetric == 1, new { @id = "EditPatient_HeightMetric1" })%>
                    <label for="EditPatient_HeightMetric1" class="fixed shortest">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Weight</label>
                <div class="fr radio">
                    <%= Html.TextBox("Weight", Model.Weight > 0 ? Model.Weight.ToString() : string.Empty, new { @id = "EditPatient_Weight", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model.WeightMetric == 0, new { @id = "EditPatient_WeightMetric0" })%>
                    <label for="EditPatient_WeightMetric0" class="fixed shortest">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model.WeightMetric == 1, new { @id = "EditPatient_WeightMetric1" })%>
                    <label for="EditPatient_WeightMetric1" class="fixed shortest">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="EditPatient_LocationId" class="fl strong">Agency Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "EditPatient_LocationId" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PatientID" class="fl strong"><span class="green">(M0020)</span> Patient ID</label>
                <div class="fr"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "EditPatient_PatientID", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_MedicareNumber" class="fl strong"><span class="green">(M0063)</span> Medicare Number</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "EditPatient_MedicareNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_MedicaidNumber" class="fl strong"><span class="green">(M0065)</span> Medicaid Number</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "EditPatient_MedicaidNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_SSN" class="fl strong"><span class="green">(M0064)</span> SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = "EditPatient_SSN", @class = "numeric ssn", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_StartOfCareDate" class="fl strong"><span class="green">(M0030)</span> Start of Care Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" value="<%= !Model.StartofCareDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.StartofCareDate.ToString("MM/dd/yyyy") : string.Empty %>" id="EditPatient_StartOfCareDate" /></div>
            </div>
    <%  if (Model.Status == (int)PatientStatus.Discharged) { %>
             <div class="row">
                <label for="EditPatient_DischargeDate" class="fl strong"> Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="DischargeDate" value="<%= !Model.DischargeDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.DischargeDate.ToString("MM/dd/yyyy") : string.Empty %>" id="EditPatient_DischargeDate" /></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="EditPatient_CaseManager" class="fl strong">Case Manager</label>
                <div class="fr"><%= Html.CaseManagers("CaseManagerId", Model.CaseManagerId.ToString(), new { @id = "EditPatient_CaseManager", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_Assign" class="fl strong">Clinician</label>
                <div class="fr"><%= Html.Clinicians("UserId",  Model.UserId.ToString(), new { @id = "EditPatient_Assign", @class = "required notzero" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <%  string[] ethnicities = Model.Ethnicities != null && Model.Ethnicities != "" ? Model.Ethnicities.Split(';') : new string[] { }; %>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces0' type='checkbox' value='0' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("0").ToChecked())%>
                        <label for="EditPatient_EthnicRaces0">American Indian or Alaska Native</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces1' type='checkbox' value='1' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("1").ToChecked())%>
                        <label for="EditPatient_EthnicRaces1">Asian</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces2' type='checkbox' value='2' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("2").ToChecked())%>
                        <label for="EditPatient_EthnicRaces2">Black or African-American</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces3' type='checkbox' value='3' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("3").ToChecked())%>
                        <label for="EditPatient_EthnicRaces3">Hispanic or Latino</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces4' type='checkbox' value='4' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("4").ToChecked())%>
                        <label for="EditPatient_EthnicRaces4">Native Hawaiian or Pacific Islander</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces5' type='checkbox' value='5' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("5").ToChecked())%>
                        <label for="EditPatient_EthnicRaces5">White</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces6' type='checkbox' value='6' name='EthnicRaces' class='required' {0} />", ethnicities.Contains("6").ToChecked())%>
                        <label for="EditPatient_EthnicRaces6">Unknown</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span></legend>
        <%  string[] paymentSources = Model.PaymentSource != null && Model.PaymentSource != "" ? Model.PaymentSource.Split(';') : new string[] { }; %>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceNone' type='checkbox' value='0' name='PaymentSources' class='required' {0} />", paymentSources.Contains("0").ToChecked())%>
                        <label for="EditPatient_PaymentSourceNone">None; no charge for current services</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceMedicare' type='checkbox' value='1' name='PaymentSources' class='required' {0} />", paymentSources.Contains("1").ToChecked())%>
                        <label for="EditPatient_PaymentSourceMedicare">Medicare (traditional fee-for-service)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceMedicareHmo' type='checkbox' value='2' name='PaymentSources' class='required' {0} />", paymentSources.Contains("2").ToChecked())%>
                        <label for="EditPatient_PaymentSourceMedicareHmo">Medicare (HMO/ managed care)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceMedicaid' type='checkbox' value='3' name='PaymentSources' class='required' {0} />", paymentSources.Contains("3").ToChecked())%>
                        <label for="EditPatient_PaymentSourceMedicaid">Medicaid (traditional fee-for-service)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceMedicaidHmo' type='checkbox' value='4' name='PaymentSources' class='required' {0} />", paymentSources.Contains("4").ToChecked())%>
                        <label for="EditPatient_PaymentSourceMedicaidHmo">Medicaid (HMO/ managed care)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceWorkers' type='checkbox' value='5' name='PaymentSources' class='required' {0} />", paymentSources.Contains("5").ToChecked())%>
                        <label for="EditPatient_PaymentSourceWorkers">Workers&#8217; compensation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceTitleProgram' type='checkbox' value='6' name='PaymentSources' class='required' {0} />", paymentSources.Contains("6").ToChecked())%>
                        <label for="EditPatient_PaymentSourceTitleProgram">Title programs (e.g., Titile III,V, or XX)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceOtherGovernment' type='checkbox' value='7' name='PaymentSources' class='required' {0} />", paymentSources.Contains("7").ToChecked())%>
                        <label for="EditPatient_PaymentSourceOtherGovernment">Other government (e.g.,CHAMPUS,VA,etc)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourcePrivate' type='checkbox' value='8' name='PaymentSources' class='required' {0} />", paymentSources.Contains("8").ToChecked())%>
                        <label for="EditPatient_PaymentSourcePrivate">Private insurance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourcePrivateHmo' type='checkbox' value='9' name='PaymentSources' class='required' {0} />", paymentSources.Contains("9").ToChecked())%>
                        <label for="EditPatient_PaymentSourcePrivateHmo">Private HMO/ managed care</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceSelf' type='checkbox' value='10' name='PaymentSources' class='required' {0} />", paymentSources.Contains("10").ToChecked())%>
                        <label for="EditPatient_PaymentSourceSelf">Self-pay</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_PaymentSourceUnknown' type='checkbox' value='11' name='PaymentSources' class='required' {0} />", paymentSources.Contains("11").ToChecked())%>
                        <label for="EditPatient_PaymentSourceUnknown">Unknown</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditPatient_AddressLine1", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditPatient_AddressLine2", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditPatient_AddressCity", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_AddressStateCode" class="fl strong"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "EditPatient_AddressStateCode", @class = "required state" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditPatient_AddressZipCode", @class = "required numeric zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_HomePhone1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 3 ? Model.PhoneHome.Substring(0, 3) : string.Empty, new { @id = "EditPatient_HomePhone1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 6 ? Model.PhoneHome.Substring(3, 3) : string.Empty, new { @id = "EditPatient_HomePhone2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10 ? Model.PhoneHome.Substring(6, 4) : string.Empty, new { @id = "EditPatient_HomePhone3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPatient_MobilePhone1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 3 ? Model.PhoneMobile.Substring(0, 3) : string.Empty, new { @id = "EditPatient_MobilePhone1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 6 ? Model.PhoneMobile.Substring(3, 3) : string.Empty, new { @id = "EditPatient_MobilePhone2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 10 ? Model.PhoneMobile.Substring(6, 4) : string.Empty, new { @id = "EditPatient_MobilePhone3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPatient_Email" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = "EditPatient_Email", @class = "email", @maxlength = "50" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Insurance/Payor Information</legend>
        <div class="column">
    <%  var htmlAttributes = new Dictionary<string, string>(); %>
    <%  htmlAttributes.Add("id", "EditPatient_PrimaryInsurance"); %>
    <%  htmlAttributes.Add("class", "insurance required notzero"); %>
            <div class="row">
                <label for="EditPatient_PrimaryInsurance" class="fl strong">Primary</label>
                <div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", Model.PrimaryInsurance.ToString(), Model.AgencyLocationId, true, htmlAttributes)%></div>
            </div>
            <div class="more">
                <div class="row">
                    <label for="EditPatient_PrimaryHealthPlanId" class="fl strong margin">Health Plan Id</label>
                    <div class="fr"><%= Html.TextBox("PrimaryHealthPlanId", Model.PrimaryInsurance >= 1000 ? Model.PrimaryHealthPlanId : string.Empty, new { @id = "EditPatient_PrimaryHealthPlanId" })%></div>
                </div>
                <div class="row">
                    <label for="EditPatient_PrimaryGroupName" class="fl strong margin">Group Name</label>
                    <div class="fr"><%= Html.TextBox("PrimaryGroupName", Model.PrimaryInsurance >= 1000 ? Model.PrimaryGroupName : string.Empty, new { @id = "EditPatient_PrimaryGroupName" })%></div>
                </div>
                <div class="row">
                    <label for="EditPatient_PrimaryGroupId" class="fl strong margin">Group Id</label>
                    <div class="fr"><%= Html.TextBox("PrimaryGroupId", Model.PrimaryInsurance >= 1000 ? Model.PrimaryGroupId : string.Empty, new { @id = "EditPatient_PrimaryGroupId" })%></div>
                </div>
            </div>
    <%  htmlAttributes["id"] = "EditPatient_SecondaryInsurance"; %>
    <%  htmlAttributes["class"] = "insurance"; %>
            <div class="row">
                <label for="EditPatient_SecondaryInsurance" class="fl strong">Secondary</label>
                <div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", Model.SecondaryInsurance.ToString(), Model.AgencyLocationId, true, htmlAttributes)%></div>
            </div>
            <div class="more">
                <div class="row">
                    <label for="EditPatient_SecondaryHealthPlanId" class="fl strong margin">Health Plan Id</label>
                    <div class="fr"><%= Html.TextBox("SecondaryHealthPlanId", Model.SecondaryInsurance >= 1000 ? Model.SecondaryHealthPlanId : string.Empty, new { @id = "EditPatient_SecondaryHealthPlanId" })%></div>
                </div>
                <div class="row">
                    <label for="EditPatient_SecondaryGroupName" class="fl strong margin">Group Name</label>
                    <div class="fr"><%= Html.TextBox("SecondaryGroupName", Model.SecondaryInsurance >= 1000 ? Model.SecondaryGroupName : string.Empty, new { @id = "EditPatient_SecondaryGroupName" })%></div>
                </div>
                <div class="row">
                    <label for="EditPatient_SecondaryGroupId" class="fl strong margin">Group Id</label>
                    <div class="fr"><%= Html.TextBox("SecondaryGroupId", Model.SecondaryInsurance >= 1000 ? Model.SecondaryGroupId : string.Empty, new { @id = "EditPatient_SecondaryGroupId" })%></div>
                </div>
            </div>
    <%  htmlAttributes["id"] = "EditPatient_TertiaryInsurance"; %>
            <div class="row">
                <label for="EditPatient_TertiaryInsurance" class="fl strong">Tertiary</label>
                <div class="fr"><%= Html.InsurancesByBranch("TertiaryInsurance", Model.TertiaryInsurance.ToString(), Model.AgencyLocationId, true, htmlAttributes)%></div>
            </div>
            <div class="more">
                <div class="row">
                    <label for="EditPatient_TertiaryHealthPlanId" class="fl strong margin">Health Plan Id</label>
                    <div class="fr"><%= Html.TextBox("TertiaryHealthPlanId", Model.TertiaryInsurance >= 1000 ? Model.TertiaryHealthPlanId : string.Empty, new { @id = "EditPatient_TertiaryHealthPlanId" })%></div>
                </div>
                <div class="row">
                    <label for="EditPatient_TertiaryGroupName" class="fl strong margin">Group Name</label>
                    <div class="fr"><%= Html.TextBox("TertiaryGroupName", Model.TertiaryInsurance >= 1000 ? Model.TertiaryGroupName : string.Empty, new { @id = "EditPatient_TertiaryGroupName" })%></div>
                </div>
                <div class="row">
                    <label for="EditPatient_TertiaryGroupId" class="fl strong margin">Group Id</label>
                    <div class="fr"><%= Html.TextBox("TertiaryGroupId", Model.TertiaryInsurance >= 1000 ? Model.TertiaryGroupId : string.Empty, new { @id = "EditPatient_TertiaryGroupId" })%></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Emergency Triage</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton("Triage", "1", Model.Triage == 1, new { @id = "EditPatient_Triage1", @class = "required" })%>
                        <label for="EditPatient_Triage1">
                            <span class="fl strong">1.)</span>
                            <span class="normal margin"><strong>Life threatening</strong> (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Triage", "2", Model.Triage == 2, new { @id = "EditPatient_Triage2", @class = "required" })%>
                        <label for="EditPatient_Triage2">
                            <span class="fl strong">2.)</span>
                            <span class="normal margin"><strong>Not life threatening but would suffer severe adverse effects</strong> from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Triage", "3", Model.Triage == 3, new { @id = "EditPatient_Triage3", @class = "required" })%>
                        <label for="EditPatient_Triage3">
                            <span class="fl strong">3.)</span>
                            <span class="normal margin"><strong>Visits could be postponed 24&#8211;48</strong> hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Triage", "4", Model.Triage == 4, new { @id = "EditPatient_Triage4", @class = "required" })%>
                        <label for="EditPatient_Triage4">
                            <span class="fl strong">4.)</span>
                            <span class="normal margin"><strong>Visits could be postponed 72&#8211;96</strong> hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10&#8211;14 days, routine catheter changes).</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fl">
        <legend>Pharmacy</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_PharmacyName" class="fl strong">Name</label>
                <div class="fr"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "EditPatient_PharmacyName", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PharmacyPhone1" class="fl strong">Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 3 ? Model.PharmacyPhone.Substring(0, 3) : string.Empty, new { @id = "EditPatient_PharmacyPhone1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 6 ? Model.PharmacyPhone.Substring(3, 3) : string.Empty, new { @id = "EditPatient_PharmacyPhone2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 10 ? Model.PharmacyPhone.Substring(6, 4) : string.Empty, new { @id = "EditPatient_PharmacyPhone3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Evacuation Zone</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Evacuation Zone</label>
                <div class="fr">
                    <%  var evacuationZone = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Zone A", Value = "A" },
                            new SelectListItem { Text = "Zone B", Value = "B" },
                            new SelectListItem { Text = "Zone C", Value = "C"},
                            new SelectListItem { Text = "Zone D", Value = "D" },
                            new SelectListItem { Text = "Zone E", Value = "E" },
                            new SelectListItem { Text = "Zone NE", Value = "NE"},
                            new SelectListItem { Text = "Zone SN", Value = "SN" }
                        }, "Value", "Text", Model.EvacuationZone); %>
                    <%= Html.DropDownList("EvacuationZone", evacuationZone, new { @id = "EditPatient_EvacuationZone" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Services Required</legend>
        <%  string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : new string[]{ }; %>
        <input type="hidden" value="" name="ServicesRequiredCollection" />
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("0").ToChecked())%>
                        <label for="ServicesRequiredCollection0">SNV</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("1").ToChecked())%>
                        <label for="ServicesRequiredCollection1">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("2").ToChecked())%>
                        <label for="ServicesRequiredCollection2">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("3").ToChecked())%>
                        <label for="ServicesRequiredCollection3">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("4").ToChecked())%>
                        <label for="ServicesRequiredCollection4">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("5").ToChecked())%>
                        <label for="ServicesRequiredCollection5">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <%  string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : new string[] { }; %>
        <input type="hidden" value="" name="DMECollection" />
        <input type="hidden" value="" name="ServicesRequiredCollection" />
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='DMECollection0' type='checkbox' value='0' name='DMECollection' {0} />", DME.Contains("0").ToChecked())%>
                        <label for="DMECollection0">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection1' type='checkbox' value='1' name='DMECollection' {0} />", DME.Contains("1").ToChecked())%>
                        <label for="DMECollection1">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection2' type='checkbox' value='2' name='DMECollection' {0} />", DME.Contains("2").ToChecked())%>
                        <label for="DMECollection2">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection3' type='checkbox' value='3' name='DMECollection' {0} />", DME.Contains("3").ToChecked())%>
                        <label for="DMECollection3">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection4' type='checkbox' value='4' name='DMECollection' {0} />", DME.Contains("4").ToChecked())%>
                        <label for="DMECollection4"">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection5' type='checkbox' value='5' name='DMECollection' {0} />", DME.Contains("5").ToChecked())%>
                        <label for="DMECollection5">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection6' type='checkbox' value='6' name='DMECollection' {0} />", DME.Contains("6").ToChecked())%>
                        <label for="DMECollection6">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection7' type='checkbox' value='7' name='DMECollection' {0} />", DME.Contains("7").ToChecked())%>
                        <label for="DMECollection7">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection8' type='checkbox' value='8' name='DMECollection' {0} />", DME.Contains("8").ToChecked())%>
                        <label for="DMECollection8">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection9' type='checkbox' value='9' name='DMECollection' {0} />", DME.Contains("9").ToChecked())%>
                        <label for="DMECollection9">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection10' type='checkbox' value='10' name='DMECollection' {0} />", DME.Contains("10").ToChecked())%>
                        <label for="DMECollection10">Other</label>
                        <div class="more"><%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "EditPatient_OtherDME" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatient_ReferralPhysician" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = "EditPatient_ReferrerPhysician", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
            <div class="row">
                <label for="EditPatient_AdmissionSource" class="fl strong">Admission Source</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource, new { @id = "EditPatient_AdmissionSource", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPatient_OtherReferralSource" class="fl strong">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "EditPatient_OtherReferralSource", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditPatient_PatientReferralDate" class="fl strong"><span class="green">(M0104)</span> Referral Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ReferralDate" value="<%= !Model.ReferralDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.ReferralDate.ToString("MM/dd/yyyy") : string.Empty %>" id="EditPatient_PatientReferralDate" /></div>
            </div>
            <div class="row">
                <label for="EditPatient_InternalReferral" class="fl strong">Internal Referral</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "EditPatient_InternalReferral" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Emergency Contact</legend>
        <div class="wide column">
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-emergency-contact" status="New Emergency Contact">New Emergency Contact</a></div>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<PatientEmergencyContact>().Name("Edit_patient_EmergencyContact_Grid").Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PrimaryPhoneFormatted);
                        columns.Bound(c => c.Relationship);
                        columns.Bound(c => c.EmailAddress);
                        columns.Bound(c => c.Id).ClientTemplate("<a class=\"link\" onclick=\"Patient.EmergencyContact.Edit('<#=Id#>','" + Model.Id + "');return false\">Edit</a> | <a class=\"link\" onclick=\"Patient.EmergencyContact.Delete('<#=Id#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Width(100);
                    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("EmergencyContactGrid", "Patient", new { patientId = Model.Id })).Sortable().Footer(false)%>
                <h2 class="hidden blue">No Emergency Contacts Found</h2>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physicians</legend>
        <div class="wide column">
            <div class="row">
                <div class="fl">
                    <label for="EditPatient_PhysicianSelector" class="fl strong">Add Physician to Patient</label>
                    <div class="fl"><input type="text" id="EditPatient_PhysicianSelector" class="physician-picker" /></div>
                    <div class="buttons fl">
                        <ul>
                            <li><a class="add-patient-physician">Add Selected Physician</a></li>
                        </ul>
                    </div>
                </div>
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<AgencyPhysician>().Name("EditPatient_PhysicianGrid").HtmlAttributes(new { @class = "hidden"}).Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PhoneWorkFormatted).Title("Work Phone");
                        columns.Bound(c => c.FaxNumberFormatted);
                        columns.Bound(c => c.EmailAddress);
                        columns.Bound(c => c.Id).ClientTemplate("<a class=\"link\" onclick=\"Patient.Physician.Delete('<#=Id#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a> | <a class=\"<#= !Primary ? \"link\" : \"hidden\" #>\" onclick=\"Patient.Physician.SetPrimary('<#=Id #>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Make Primary</a>").Title("Action").Width(135);
                    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("PhysicianGrid", "Patient", new { patientId = Model.Id })).Sortable().Footer(false)%>
                <h2 class="hidden blue">No Physicians Found</h2>
            </div>
        </div>
    </fieldset>
    <%  if (Model.Status == (int)PatientStatus.Discharged) { %>
    <fieldset>
        <legend>Discharged Reason</legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="EditPatient_DischargeReason" name="DischargeReason" class="tall" maxcharacters="500"><%= Model.DischargeReason%></textarea>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="EditPatient_Comments" name="Comments" class="tall" maxcharacters="500"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>