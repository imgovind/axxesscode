﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Deleted Patients | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul>
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
        <div class="filter">
            <label for="ListDeletedPatients_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "ListDeletedPatients_BranchId" })%>
        </div>
    </fieldset>
    <div id="ListDeletedPatients_Content" class="content"><% Html.RenderPartial("Deleted/Grid", Model); %></div>
</div>