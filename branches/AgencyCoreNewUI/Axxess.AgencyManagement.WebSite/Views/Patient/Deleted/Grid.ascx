﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<%= Html.Telerik().Grid(Model).Name("ListDeletedPatient_Grid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
        columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(8);
        columns.Bound(p => p.DisplayName).Title("Name").Width(15);
        columns.Bound(p => p.Address).Title("Address").Width(25).Sortable(false);
        columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(8).Sortable(true);
        columns.Bound(p => p.Gender).Width(6).Sortable(true);
        columns.Bound(p => p.Phone).Title("Phone").Width(10).Sortable(false);
        columns.Bound(p => p.Status).Title("Last Status").Width(8).Sortable(false);
        columns.Bound(p => p.Id).Width(9).Sortable(false).Template(s => string.Format("<a class=\"link\" onclick=\"Patient.Restore('{0}');return false\">Restore</a>", s.Id)).Title("Action");
    }).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)%>