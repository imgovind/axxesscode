﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">New Hospitalization Log | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Hospitalization/Create", "Patient", FormMethod.Post, new { @id = "NewHospitalization_Form" })) { %>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label for="NewHospitalization_PatientName" class="fl strong">Patient</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : "", new { @id = "NewHospitalization_PatientName", @class="required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewHospitalization_EpisodeList" class="fl strong">Episode</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "NewHospitalization_EpisodeList", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewHospitalization_User" class="fl strong">User</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", "", new { @id = "NewHospitalization_User", @class="required notzero" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Hospitalization</legend>
        <div class="column">
            <div class="row">
                <label for="NewHospitalization_M0903LastHomeVisitDate" class="fl strong"><span class="green">(M0903)</span> Date of Last (Most Recent) Home Visit</label>
                <div class="fr"><%= Html.TextBox("M0903LastHomeVisitDate", string.Empty, new { @id = "NewHospitalization_M0903LastHomeVisitDate", @class = "date-picker" })%></div>
            </div>
            <div class="row">
                <label for="NewHospitalization_M0906DischargeDate" class="fl strong"><span class="green">(M0906)</span> Transfer Date, Enter the date of the transfer of the patient</label>
                <div class="fr"><%= Html.TextBox("M0906DischargeDate", string.Empty, new { @id = "NewHospitalization_M0906DischargeDate", @class = "date-picker" })%></div>
            </div>
            <div class="row">
                <label class="strong"><span class="green">(M2410)</span> To which Inpatient Facility has the patient been admitted?</label>
                <%= Html.Hidden("M2410TypeOfInpatientFacility", "") %>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "01", false, new { @id = "NewHospitalization_M2410TypeOfInpatientFacility1" }) %>
                        <label for="NewHospitalization_M2410TypeOfInpatientFacility1">
                            <span class="fl">1 &#8211;</span>
                            <span class="margin">Hospital</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "02", false, new { @id = "NewHospitalization_M2410TypeOfInpatientFacility2" }) %>
                        <label for="NewHospitalization_M2410TypeOfInpatientFacility2">
                            <span class="fl">2 &#8211;</span>
                            <span class="margin">Rehabilitation facility</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "03", false, new { @id = "NewHospitalization_M2410TypeOfInpatientFacility3" })%>
                        <label for="NewHospitalization_M2410TypeOfInpatientFacility3">
                            <span class="fl">3 &#8211;</span>
                            <span class="margin">Nursing home</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "04", false, new { @id = "NewHospitalization_M2410TypeOfInpatientFacility4" })%>
                        <label for="NewHospitalization_M2410TypeOfInpatientFacility4">
                            <span class="fl">4 &#8211;</span>
                            <span class="margin">Hospice</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong"><span class="green">(M2440)</span> For what Reason(s) was the patient Admitted to a Nursing Home?</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedTherapy", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedTherapy" name="M2440ReasonPatientAdmittedTherapy" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedTherapy">
                            <span class="fl">1 &#8211;</span>
                            <span class="margin">Therapy services</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedRespite", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedRespite" name="M2440ReasonPatientAdmittedRespite" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedRespite">
                            <span class="fl">2 &#8211;</span>
                            <span class="margin">Respite care</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedHospice", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedHospice" name="M2440ReasonPatientAdmittedHospice" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedHospice">
                            <span class="fl">3 &#8211;</span>
                            <span class="margin">Hospice care</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedPermanent", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedPermanent" name="M2440ReasonPatientAdmittedPermanent" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedPermanent">
                            <span class="fl">4 &#8211;</span>
                            <span class="margin">Permanent placement</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedUnsafe", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedUnsafe" name="M2440ReasonPatientAdmittedUnsafe" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedUnsafe">
                            <span class="fl">5 &#8211;</span>
                            <span class="margin">Unsafe for care at home</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedOther", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedOther" name="M2440ReasonPatientAdmittedOther" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedOther">
                            <span class="fl">6 &#8211;</span>
                            <span class="margin">Other</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2440ReasonPatientAdmittedUnknown", "")%>
                        <input id="NewHospitalization_M2440ReasonPatientAdmittedUnknown" name="M2440ReasonPatientAdmittedUnknown" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2440ReasonPatientAdmittedUnknown">
                            <span class="fl">UK &#8211;</span>
                            <span class="margin">Unknown</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="strong"><span class="green">(M2430)</span> Reason for Hospitalization: For what reason(s) did the patient require hospitalization?</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationMed", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationMed" name="M2430ReasonForHospitalizationMed" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationMed">
                            <span class="fl">1 &#8211;</span>
                            <span class="margin">Improper medication administration, medication side effects, toxicity, anaphylaxis</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationFall", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationFall" name="M2430ReasonForHospitalizationFall" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationFall">
                            <span class="fl">2 &#8211;</span>
                            <span class="margin">Injury caused by fall</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationInfection", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationInfection" name="M2430ReasonForHospitalizationInfection" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationInfection">
                            <span class="fl">3 &#8211;</span>
                            <span class="margin">Respiratory infection (e.g., pneumonia, bronchitis)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationOtherRP", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationOtherRP" name="M2430ReasonForHospitalizationOtherRP" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationOtherRP">
                            <span class="fl">4 &#8211;</span>
                            <span class="margin">Other respiratory problem</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationHeartFail", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationHeartFail" name="M2430ReasonForHospitalizationHeartFail" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationHeartFail">
                            <span class="fl">5 &#8211;</span>
                            <span class="margin">Heart failure (e.g., fluid overload)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationCardiac", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationCardiac" name="M2430ReasonForHospitalizationCardiac" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationCardiac">
                            <span class="fl">6 &#8211;</span>
                            <span class="margin">Cardiac dysrhythmia (irregular heartbeat)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationMyocardial", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationMyocardial" name="M2430ReasonForHospitalizationMyocardial" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationMyocardial">
                            <span class="fl">7 &#8211;</span>
                            <span class="margin">Myocardial infarction or chest pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationHeartDisease", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationHeartDisease" name="M2430ReasonForHospitalizationHeartDisease" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationHeartDisease">
                            <span class="fl">8 &#8211;</span>
                            <span class="margin">Other heart disease</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationStroke", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationStroke" name="M2430ReasonForHospitalizationStroke" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationStroke">
                            <span class="fl">9 &#8211;</span>
                            <span class="margin">Stroke (CVA) or TIA</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationHypo", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationHypo" name="M2430ReasonForHospitalizationHypo" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationHypo">
                            <span class="fl">10 &#8211;</span>
                            <span class="margin">Hypo/Hyperglycemia, diabetes out of control</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationGI", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationGI" name="M2430ReasonForHospitalizationGI" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationGI">
                            <span class="fl">11 &#8211;</span>
                            <span class="margin">GI bleeding, obstruction, constipation, impaction</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationDehMal", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationDehMal" name="M2430ReasonForHospitalizationDehMal" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationDehMal">
                            <span class="fl">12 &#8211;</span>
                            <span class="margin">Dehydration, malnutrition</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationUrinaryInf", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationUrinaryInf" name="M2430ReasonForHospitalizationUrinaryInf" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationUrinaryInf">
                            <span class="fl">13 &#8211;</span>
                            <span class="margin">Urinary tract infection</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationIV", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationIV" name="M2430ReasonForHospitalizationIV" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationIV">
                            <span class="fl">14 &#8211;</span>
                            <span class="margin">IV catheter-related infection or complication</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationWoundInf", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationWoundInf" name="M2430ReasonForHospitalizationWoundInf" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationWoundInf">
                            <span class="fl">15 &#8211;</span>
                            <span class="margin">Wound infection or deterioration</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationUncontrolledPain", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationUncontrolledPain" name="M2430ReasonForHospitalizationUncontrolledPain" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationUncontrolledPain">
                            <span class="fl">16 &#8211;</span>
                            <span class="margin">Uncontrolled pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationMental", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationMental" name="M2430ReasonForHospitalizationMental" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationMental">
                            <span class="fl">17 &#8211;</span>
                            <span class="margin">Acute mental/behavioral health problem</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationDVT", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationDVT" name="M2430ReasonForHospitalizationDVT" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationDVT">
                            <span class="fl">18 &#8211;</span>
                            <span class="margin">Deep vein thrombosis, pulmonary embolus</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationScheduled", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationScheduled" name="M2430ReasonForHospitalizationScheduled" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationScheduled">
                            <span class="fl">19 &#8211;</span>
                            <span class="margin">Scheduled treatment or procedure</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationOther", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationOther" name="M2430ReasonForHospitalizationOther" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationOther">
                            <span class="fl">20 &#8211;</span>
                            <span class="margin">Other than above reasons</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden("M2430ReasonForHospitalizationUK", "")%>
                        <input id="NewHospitalization_M2430ReasonForHospitalizationUK" name="M2430ReasonForHospitalizationUK" type="checkbox" value="1" />
                        <label for="NewHospitalization_M2430ReasonForHospitalizationUK">
                            <span class="fl">UK &#8211;</span>
                            <span class="margin">Reason unknown</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide column">
            <div class="row ac">
                <%= Html.Templates("Templates") %>
                <%= Html.TextArea("GenericDischargeNarrative", string.Empty, 8, 20, new { @class = "tallest", @id = "NewHospitalization_GenericDischargeNarrative", @maxcharacters = "1500" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>