﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Hospitalized Patients | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul> 
           <li><a class="new-hospitalization">New Hospitalization</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls short ac">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<PatientHospitalizationData>().Name("ListHospitalizedPatient_Grid").Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(10);
            columns.Bound(p => p.DisplayName).Title("Patient").Width(20);
            columns.Bound(p => p.Source).Title("Source").Width(20);
            columns.Bound(p => p.HospitalizationDate).Title("Hospitalization Date").Width(10);
            columns.Bound(p => p.LastHomeVisitDate).Title("Last Home Visit Date").Width(10);
            columns.Bound(p => p.User).Title("User").Width(20);
            columns.Bound(p => p.Id).Width(10).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"Patient.Hospitalization.Logs('<#=PatientId#>');return false\">Show Logs</a>").Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("HospitalizationGrid", "Patient")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>