﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationLog>" %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<link rel="stylesheet" type="text/css" href="Content/printpreview.css"></link>
<link rel="stylesheet" type="text/css" href="Content/Print/Patient/HospitalizationLog.css"></link>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : "") + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "") : "") : "").Clean()%>",
        patientname:    "<%= Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase().Clean() + ", " : string.Empty) + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase().Clean() + " " : string.Empty) + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper().Clean() : string.Empty) : string.Empty %>",
        episode:        "<%= Model != null && Model.EpisodeRange.IsNotNullOrEmpty() ? Model.EpisodeRange.Clean() : string.Empty %>",
        user:           "<%= Model != null && Model.User.IsNotNullOrEmpty() ? Model.User.Clean() : string.Empty %>"
    };
    PrintPreview.BuildSections(<%= Model.PrintViewJson %>);
</script>