﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<HospitalizationLog>>" %>
<%  if (Model != null) { %>
    <%  var logs = Model.OrderBy(l => l.Created).ToList(); %>
    <%  if (logs != null && logs.Count > 0) { %>
<ul>
    <li class="ac"><h3>Hospitalization Logs</h3></li>
    <li>
        <span class="hosplog-source">Source</span>
        <span class="hosplog-user">User</span>
        <span class="hosplog-date">Date</span>
        <span class="hosplog-icon"></span>
        <span>Action</span>
    </li>
</ul>
<ol>
        <%  foreach (var log in logs) { %>
    <li>
        <span class="hosplog-source"><%= log.Source %></span>
        <span class="hosplog-user"><%= log.User %></span>
        <span class="hosplog-date"><%= log.HospitalizationDateFormatted %></span>
        <span class="hosplog-icon"><%= log.PrintUrl %></span>
        <span><a class="link" onclick="Patient.Hospitalization.Edit('<%= log.Id %>','<%=log.PatientId %>');return false">Edit</a> | <a class="link" onclick="Patient.Hospitalization.Delete('<%= log.Id %>', '<%=log.PatientId %>');return false">Delete</a></span>
    </li>
        <%  } %>
</ol>
    <%  } else { %>
<ul>
    <li class="ac"><h3>Hospitalization Logs</h3></li>
</ul>
<h1 class="blue">No Hospitalization Logs</h1>
    <%  } %>
<%  } %>