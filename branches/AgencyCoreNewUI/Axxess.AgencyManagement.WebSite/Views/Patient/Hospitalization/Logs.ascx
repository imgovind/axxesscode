﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationViewData>" %>
<span class="wintitle">Hospitalization Logs | <%= Current.AgencyName %></span>
<div class="wrapper main">
    <%= Html.Hidden("PatientId", Model.Patient.Id) %>
    <div class="note">
        <fieldset>
            <legend>Patient</legend>
            <div class="column">
                <div class="row">
                    <label class="fl strong">Patient</label>
                    <div class="fr"><%= Model.Patient.DisplayNameWithMi %></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label class="fl strong">MRN</label>
                    <div class="fr"><%= Model.Patient.PatientIdNumber %></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-hospitalization">Add Hospitalization Log</a></li>
        </ul>
        <ul class="fr">
            <li><a class="export">Export to Excel</a></li>
        </ul>
    </div>
    <div class="clr"></div>
    <div class="acore-grid"><% Html.RenderPartial("Hospitalization/Grid", Model.Logs); %></div>
</div>