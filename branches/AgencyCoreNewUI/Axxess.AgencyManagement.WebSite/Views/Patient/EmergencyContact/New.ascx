﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("EmergencyContact/Create", "Patient", FormMethod.Post, new { @id = "NewEmergencyContact_Form" })) { %>
    <%= Html.Hidden("PatientId", Model, new { @id = "NewEmergencyContact_PatientId" })%>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="NewEmergencyContact_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", "", new { @id = "NewEmergencyContact_FirstName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewEmergencyContact_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", "", new { @id = "NewEmergencyContact_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewEmergencyContact_Relationship" class="fl strong">Relationship</label>
                <div class="fr"><%= Html.TextBox("Relationship", "", new { @id = "NewEmergencyContact_Relationship" })%></div>
            </div>
            <div class="row">
                <label for="NewEmergencyContact_PrimaryPhoneArray1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhonePrimaryArray", "", new { @id = "NewEmergencyContact_PrimaryPhoneArray1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", "", new { @id = "NewEmergencyContact_PrimaryPhoneArray2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", "", new { @id = "NewEmergencyContact_PrimaryPhoneArray3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewEmergencyContact_AlternatePhoneArray1" class="fl strong">Alternate Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneAlternateArray", "", new { @id = "NewEmergencyContact_AlternatePhoneArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", "", new { @id = "NewEmergencyContact_AlternatePhoneArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", "", new { @id = "NewEmergencyContact_AlternatePhoneArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewEmergencyContact_Email" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewEmergencyContact_Email", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsPrimary", true, new { @id = "NewEmergencyContact_SetPrimary" }) %>
                        <label for="NewEmergencyContact_SetPrimary">Set Primary</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>