﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEmergencyContact>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("EmergencyContact/Update", "Patient", FormMethod.Post, new { @id = "EditEmergencyContact_Form" })) { %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditEmergencyContact_PatientId" })%>
    <%= Html.Hidden("Id", Model.Id, new { @id ="EditEmergencyContact_Id" })%>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="EditEmergencyContact_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditEmergencyContact_FirstName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditEmergencyContact_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditEmergencyContact_LastName", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditEmergencyContact_Relationship" class="fl strong">Relationship</label>
                <div class="fr"><%= Html.TextBox("Relationship", Model.Relationship, new { @id = "EditEmergencyContact_Relationship" })%></div>
            </div>
            <div class="row">
                <label for="EditEmergencyContact_PrimaryPhoneArray1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 3 ? Model.PrimaryPhone.Substring(0, 3) : string.Empty, new { @id = "EditEmergencyContact_PrimaryPhoneArray1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 6 ? Model.PrimaryPhone.Substring(3, 3) : string.Empty, new { @id = "EditEmergencyContact_PrimaryPhoneArray2", @class = "required numeric phone-short",  @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length > 6 ? Model.PrimaryPhone.Substring(6) : string.Empty, new { @id = "EditEmergencyContact_PrimaryPhoneArray3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditEmergencyContact_AlternatePhoneArray1" class="fl strong">Alternate Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length >= 3 ? Model.AlternatePhone.Substring(0, 3) : string.Empty, new { @id = "EditEmergencyContact_AlternatePhoneArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length >= 6 ? Model.AlternatePhone.Substring(3, 3) : string.Empty, new { @id = "EditEmergencyContact_AlternatePhoneArray2", @class = "numeric phone-short",  @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length > 6 ? Model.AlternatePhone.Substring(6) : string.Empty, new { @id = "EditEmergencyContact_AlternatePhoneArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditEmergencyContact_Email" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditEmergencyContact_Email", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsPrimary", Model.IsPrimary, new { @id = "EditEmergencyContact_SetPrimary" })%>
                        <label for="EditEmergencyContact_SetPrimary">Set Primary</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>