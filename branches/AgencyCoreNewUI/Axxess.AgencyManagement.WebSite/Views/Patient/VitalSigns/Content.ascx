﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<%  string pagename = "PatientVitalSigns"; %>
<%  var patientId = ViewData.ContainsKey("PatientId") ? ViewData["id"] : Guid.Empty; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
        columns.Bound(s => s.VisitDate).Width(6).Title("Visit Date").Sortable(true).ReadOnly();
        columns.Bound(s => s.UserDisplayName).Width(15).Title("Employee Name");
        columns.Bound(s => s.DisciplineTask).Width(15).Title("Task").Sortable(false).ReadOnly();
        columns.Bound(s => s.BPLying).Title("BP Lying").Sortable(false).Width(4).ReadOnly();
        columns.Bound(s => s.BPSitting).Title("BP Sit").Sortable(false).Width(4).ReadOnly();
        columns.Bound(s => s.BPStanding).Title("BP Stand").Sortable(false).Width(4).ReadOnly();
        columns.Bound(s => s.Temp).Title("Temp").Sortable(false).Width(4);
        columns.Bound(s => s.Resp).Title("Resp").Sortable(false).Width(4).ReadOnly();
        columns.Bound(s => s.ApicalPulse).Title("Apical Pulse").Sortable(false).Width(5);
        columns.Bound(s => s.RadialPulse).Title("Radial Pulse").Width(5).Sortable(false).ReadOnly();
        columns.Bound(s => s.BSMax).Title("BS").Sortable(false).Width(4);
        columns.Bound(s => s.Weight).Sortable(false).Title("Weight").Width(4);
        columns.Bound(s => s.PainLevel).Sortable(false).Title("Pain Level").Width(4);
    }).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "VisitDate") {
            if (sortDirection == "ASC") order.Add(o => o.VisitDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.VisitDate).Descending();
        } else if (sortName == "UserDisplayName") {
            if (sortDirection == "ASC") order.Add(o => o.UserDisplayName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserDisplayName).Descending();
        }
    })).Scrollable() %>