﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<span class="wintitle">Patient Vital Signs | <%= ViewData.ContainsKey("DisplayName") && ViewData["DisplayName"] != null ? ViewData["DisplayName"].ToString().Clean() : string.Empty %></span>
<%  var pagename = "PatientVitalSigns"; %>
<%  var patientId = ViewData.ContainsKey("PatientId") ? ViewData["id"] : Guid.Empty; %>
<%  var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]); %>
<div class="wrapper main blue">
    <%= Html.Hidden("PatientId", patientId, new { @id = pagename + "_PatientId" }) %>
    <div class="fr buttons">
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="filter optional">
            <label for="<%= pagename %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker" name="StartDate" id="<%= pagename %>_StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" />
            <label for="<%= pagename %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker" name="EndDate" id="<%= pagename %>_EndDate" value="<%= DateTime.Now.ToShortDateString() %>" />
        </div>
    </fieldset>
    <div id="<%= pagename %>_Content"><% Html.RenderPartial("VitalSigns/Content", Model); %></div>
</div>