﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<span class="wintitle">List Patients | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <%= Html.Hidden("ListPatient_SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]))%>
    <div class="fr ar buttons">
        <ul>
            <li><a class="new-patient">New Patient</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls">
        <a class="grid-refresh hidden"></a>
        <div class="ac">
            <div class="filter grid-search"></div>
            <div class="filter">
                <label for="ListPatient_BranchId" class="strong">Branch</label><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "ListPatient_BranchId" })%>
            </div>
            <div class="filter">
                <label for="ListPatient_Status" class="strong">Status</label>
                <select id="ListPatient_Status" name="Status">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Discharged</option>
                    <option value="3">Pending</option>
                    <option value="4">Non-Admit</option>
                </select>
            </div>
        </div>
    </fieldset>
    <div id="ListPatient_Content"><% Html.RenderPartial("ListContent", Model); %></div>
</div>