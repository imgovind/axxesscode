﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<span class="wintitle">Patient Charts | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("Charts/PatientFilters", Model); %></div>
        <div class="bottom new-patient"><% Html.RenderPartial("Charts/PatientSelector"); %></div>
    </div>
    <div id="PatientMainResult" class="ui-layout-center"></div>
</div>
<%  if (Model == null || Model.Count == 0) { %>
<script type="text/javascript">
    $("#PatientMainResult").html(U.MessageWarn("No Patient Found", "There are no patients to be gound which match your search criteria.  Please try a different search."));
</script>
<%  } %>