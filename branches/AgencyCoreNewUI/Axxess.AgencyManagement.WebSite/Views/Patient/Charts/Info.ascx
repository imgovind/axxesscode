﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="abs">
<%  if (!Model.PhotoId.IsEmpty()) { %>
    <img src="/Asset/<%= Model.PhotoId %>" alt="User Photo" />
<%  } else { %>
    <img src="/Images/blank_user.jpeg" alt="User Photo" />
<%  } %>
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
    <div class="ac">[ <a class="link change-photo">Change Photo</a> ]</div>
<%  } %>
</div>
<div class="buttons abs-right">
    <ul>
        <li><a class="refresh">Refresh</a></li>
    </ul>
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
    <br />
    <ul>
        <li><a class="change-status">Change Status</a></li>
    </ul>
<%  } %>
<%  if (Current.HasRight(Permissions.ManagePatients) && Model.IsDischarged) { %>
    <br />
    <ul>
        <li><a class="readmit">Re-Admit</a></li>
    </ul>
<%  } %>
</div>
<div class="column">
    <h1><%= Model.DisplayNameWithMi %></h1>
    <div class="row">
        <label class="fl strong">MRN</label>
        <div class="fr"><%= Model.PatientIdNumber %></div>
    </div>    
    <div class="row">
        <label class="fl strong">DOB</label>
        <div class="fr"><%= Model.DOB.ToString("MMMM dd, yyyy") %></div>
    </div>    
    <div class="row">
        <label class="fl strong">SOC</label>
        <div class="fr"><%= Model.StartofCareDate.ToString("MMMM dd, yyyy")%></div>
    </div>    
<%  if (Model.PhoneHome.IsNotNullOrEmpty()) { %>
    <div class="row">
        <label class="fl strong">Phone</label>
        <div class="fr"><%= Model.PhoneHome.ToPhone() %></div>
    </div>
<%  } %>
<%  var physician = Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0 ? Model.PhysicianContacts.SingleOrDefault(p => p.Primary) : null; %>
<%  if (physician != null) { %>
    <div class="row">
        <label class="fl strong">Physician</label>
        <div class="fr"><%= physician != null ? physician.DisplayName.Trim() : string.Empty %></div>
    </div>    
<%  } %>
    <div class="row ac">
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        [ <a class="link edit">Edit</a> ]&#160;
<%  } %>
        [ <a class="link more-info">More</a> ]&#160;
        [ <a class="link" href="http://<%= Request.ServerVariables["HTTP_HOST"] %>/Patient/Map/<%= Model.Id %>" target="_blank">Directions</a> ]
    </div>
</div>