﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="abs above ac">
    <label for="PatientCharts_ActivityFilterDiscipline">Discipline</label>
    <select id="PatientCharts_ActivityFilterDiscipline" class="short discipline">
        <option value="All" selected="selected">All</option>
        <option value="Orders">Orders</option>
        <option value="Nursing">Nursing</option>
        <option value="PT">PT</option>
        <option value="OT">OT</option>
        <option value="ST">ST</option>
        <option value="HHA">HHA</option>
        <option value="MSW">MSW</option>
        <option value="Dietician">Dietician</option>
    </select>
    <label for="PatientCharts_ActivityFilterDate">Date</label>
    <select id="PatientCharts_ActivityFilterDate" class="short date">
        <option value="All">All</option>
        <option disabled="disabled">---------------------------------------------</option>
        <option value="DateRange">Date Range</option>
        <option disabled="disabled">---------------------------------------------</option>
        <option value="ThisEpisode" selected="selected">This Episode</option>
        <option value="LastEpisode">Last Episode</option>
        <option value="NextEpisode">Next Episode</option>
        <option disabled="disabled">---------------------------------------------</option>
        <option value="Today">Today</option>
        <option value="ThisWeek">This Week</option>
        <option value="ThisWeekToDate">This Week-to-date</option>
        <option value="LastMonth">Last Month</option>
        <option value="ThisMonth">This Month</option>
        <option value="ThisMonthToDate">This Month-to-date</option>
        <option value="ThisFiscalQuarter">This Fiscal Quarter</option>
        <option value="ThisFiscalQuarterToDate">This Fiscal Quarter-to-date</option>
        <option value="ThisFiscalYear">This Fiscal Year</option>
        <option value="ThisFiscalYearToDate">This Fiscal Year-to-date</option>
        <option value="Yesterday">Yesterday</option>
        <option value="LastWeek">Last Week</option>
        <option value="LastWeekToDate">Last Week-to-date</option>
        <option value="LastMonthToDate">Last Month-to-date</option>
        <option value="LastFiscalQuarter">Last Fiscal Quarter</option>
        <option value="LastFiscalQuarterToDate">Last Fiscal Quarter-to-date</option>
        <option value="LastFiscalYear">Last Fiscal Year</option>
        <option value="LastFiscalYearToDate">Last Fiscal Year-to-date</option>
        <option value="NextWeek">Next Week</option>
        <option value="Next4Weeks">Next 4 Weeks</option>
        <option value="NextMonth">Next Month</option>
        <option value="NextFiscalQuarter">Next Fiscal Quarter</option>
        <option value="NextFiscalYear">Next Fiscal Year</option>
    </select>
    <span class="date-range">
        <label for="PatientCharts_ActivityFilterDateRangeStart">From</label>
        <input type="text" class="date-picker short start-date" name="From" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="PatientCharts_ActivityFilterDateRangeStart" />
        <label for="PatientCharts_ActivityFilterDateRangeEnd">To</label>
        <input type="text" class="date-picker short end-date" name="To" value="<%= DateTime.Today.ToShortDateString() %>" id="PatientCharts_ActivityFilterDateRangeEnd" />
    </span>
</div>
<%  var val = Model != null && !Model.IsEmpty() ? Model : Guid.Empty; %>
<%  Html.Telerik().Grid<ScheduleEvent>().Name("PatientActivityGrid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
        columns.Bound(s => s.EventId).Visible(false);
        columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task").Width(23);
        columns.Bound(s => s.EventDateSortable).Title("Scheduled Date").Width(10);
        columns.Bound(s => s.UserName).Title("Assigned To").Width(17);
        columns.Bound(s => s.StatusName).Title("Status").Width(14);
        columns.Bound(s => s.Icons).Title(" ").ClientTemplate("<#=Icons#>").Width(8).HtmlAttributes(new { @class = "ac" }).Sortable(false);
        columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(20).Sortable(false);
        columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
        columns.Bound(s => s.IsComplete).Visible(false);
    }).ClientEvents(c => c.OnRowDataBound("Patient.Charts.ActivityRowBind")).DataBinding(dataBinding => dataBinding.Ajax().Select("ChartsActivitiesGrid", "Patient", new { patientId = val, discipline = "All", dateRangeId = "ThisEpisode", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now })).Sortable().Scrollable().Footer(false).Render(); %>
