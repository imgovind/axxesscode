﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<%  if (Model.Patient != null) { %>
    <div class="top">
        <div class="winmenu">
            <ul>
    <%  if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
                <li><a class="new-order">New Order</a></li>
    <%  } %>
                <li><a class="new-communication-note">New Communication Note</a></li>
    <%  if (Current.HasRight(Permissions.ManageInsurance)) { %>
                <li><a class="new-authorization">New Authorization</a></li>
    <%  } %>
    <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
                <li><a class="reassign-schedule">Reassign Schedules</a></li>
    <%  } %>
            </ul>
        </div>
        <div class="wrapper main ac">
            <div class="quick reports fr al">
                <div class="reports-head">
                    <h5>Quick Reports</h5>
                </div>
                <ul>
                    <li><a class="patient-profile">Patient Profile</a></li>
                    <li><a class="medication-profile">Medication Profile(s)</a></li>
                    <li><a class="allergy-profile">Allergy Profile</a></li>
    <%  if (Current.HasRight(Permissions.ManageInsurance)) { %>
                    <li><a class="authorization-list">Authorizations Listing</a></li>
    <%  } %>
                    <li><a class="communication-note-list">Communication Notes</a></li>
                    <li><a class="order-list">Orders and Care Plans</a></li>
                    <li><a class="sixty-day-list">60 Day Summaries</a></li>
                    <li><a class="vital-signs-log">Vital Signs Log</a></li>
                    <li><a class="triage-classification">Triage Classification</a></li>
    <%  if (Current.HasRight(Permissions.DeleteTasks)) { %>
                    <li><a class="deleted-tasks">Deleted Tasks/Documents</a></li>
    <%  } %>
                </ul>
            </div>
            <fieldset class="patient-summary"><%  Html.RenderPartial("Charts/Info", Model.Patient); %></fieldset>
            <fieldset class="legend">
                <ul>
                    <li><span class="img icon note"></span>Visit Comments</li>
                    <li><span class="img icon note-blue"></span>Episode Comments</li>
                    <li><span class="img icon note-red"></span>Missed/Returned</li>
                </ul>
            </fieldset>
    <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
            <div class="buttons">
                <ul class="fl">
                    <li><a class="schedule-center">Schedule Activity</a></li>
                </ul>
            </div>
    <%  } %>
        </div>
   </div>
    <div class="bottom"><%  Html.RenderPartial("Charts/Activities", Model.Patient.Id); %></div>
<%  } else { %>
<script type="text/javascript">
    $("#PatientMainResult").html(U.MessageError("No Patient Information", "No patient information can be found.  Please contact Axxess for assistance."));
</script>
<%  } %>
