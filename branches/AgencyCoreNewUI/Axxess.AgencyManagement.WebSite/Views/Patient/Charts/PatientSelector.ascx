﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  Html.Telerik().Grid<PatientSelection>().Name("PatientSelectionGrid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
        columns.Bound(p => p.LastName);
        columns.Bound(p => p.ShortName).Title("First Name");
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding
        .Ajax().Select("ChartsPatientGrid", "Patient", new { branchId = Guid.Empty, statusId = 1, paymentSourceId = 0, name = string.Empty })
    ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
        .OnDataBound("Patient.Charts.PatientSelector.Init")
        .OnRowSelected("Patient.Charts.PatientSelector.Select")
    ).Render(); %>