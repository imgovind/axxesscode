﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AdmissionEpisode>>" %>
<% var model = Model ?? new List<AdmissionEpisode>(); %>
<% var admissionId = ViewData.ContainsKey("AdmissionId") && ViewData["AdmissionId"] != null ? ViewData["AdmissionId"].ToString() : string.Empty; %>
<% var isActivePatient = ViewData.ContainsKey("PatientStatus") && ViewData["PatientStatus"] != null ? ViewData["PatientStatus"].ToString()=="1" : false; %>
<% var uniqueAdms = Model != null && Model.Count > 0 ? Model.GroupBy(m => m.Id).Select(g => g.First()).OrderByDescending(m => m.StartOfCareDate).ThenByDescending(m => m.DischargedDate).ToList() : new List<AdmissionEpisode>(); %>
<% Html.Telerik()
    .Grid(uniqueAdms)
    .Name("List_PatientManagedDates").HtmlAttributes(new { @style = "top:30px;" })
    .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("Id"); keys.Add(o => o.PatientId).RouteKey("PatientId"); })
        .Columns(columns =>
        {
            columns.Bound(c => c.StartOfCareDate).Template(c => c.StartOfCareDate.Date > DateTime.MinValue ? c.StartOfCareDate.ToString("MM/dd/yyyy") : string.Empty).Title("Admission Date").Sortable(false);
            columns.Bound(c => c.DischargedDate).Template(c => isActivePatient && c.Id.ToString() == admissionId ? string.Empty : (c.DischargedDate.Date > DateTime.MinValue ? c.DischargedDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Discharge Date").Sortable(false);
            columns.Bound(c => c.Id).Template(c => c.Id != Guid.Empty ? (c.Id.ToString() == admissionId ? "Yes" : "No" + string.Format(" ( <a href=\"javascript:void(0);\" onclick=\"Patient.MarkPatientAdmissionCurrent('{0}','{1}');\">Set Current</a> )", c.PatientId, c.Id)) : "No" + string.Format(" ( <a href=\"javascript:void(0);\" onclick=\"Patient.DeletePatientAdmission('{0}','{1}');\" >Mark As Current Period</a> )", c.PatientId, c.Id)).Title("Current Admission Period").Sortable(false);
            columns.Bound(c => c.EpisodeId).Template(c => c.EpisodeId == Guid.Empty ? "No" : "Yes").Title("Associated Episodes").Sortable(false);
            columns.Bound(c => c.PatientId).Template(c => (c.Id != Guid.Empty && c.Id.ToString() == admissionId)?string.Empty: string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatientAdmission('{0}','{1}','Edit');\">Edit</a>", c.PatientId, c.Id) + (((c.Id != Guid.Empty && c.Id.ToString() == admissionId) || c.EpisodeId != Guid.Empty) ? string.Empty : string.Format(" | <a href=\"javascript:void(0);\" onclick=\"Patient.DeletePatientAdmission('{0}','{1}');\" >Delete</a>", c.PatientId, c.Id))).Title("Action");
        })
        .DetailView(detailView => detailView.Template(e =>
        {%>
        <%
    var admissionEpisodes = model.Where(d => d.Id == e.Id && d.EpisodeId != Guid.Empty).OrderByDescending(d => d.EpisodeStartDate).ToList() ?? new List<AdmissionEpisode>();
    if (admissionEpisodes != null && admissionEpisodes.Count > 0)
    {
        Html.Telerik()
            .Grid(admissionEpisodes)
            .Name("DetailPatientAdmission" + e.Id).HtmlAttributes(new { @style = "position:relative;", @class = "admissiondetail" })
            .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("Id"); keys.Add(o => o.PatientId).RouteKey("PatientId"); })
                .Columns(columns =>
                {
                    columns.Bound(c => c.EpisodeStartDate).Template(c => c.EpisodeStartDate.Date > DateTime.MinValue ? c.EpisodeStartDate.ToString("MM/dd/yyyy") : string.Empty).Title("Episode Start Date").Sortable(true);
                    columns.Bound(c => c.EpisodeEndDate).Template(c => c.EpisodeEndDate.Date > DateTime.MinValue ? c.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty).Title("Episode End Date").Sortable(false);
                    columns.Bound(c => c.EpisodeId).Width(130).Template(c => string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditEpisodeModal('{0}','{1}',function(){{ Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer','{1}');}});\">Edit Episode</a>", c.EpisodeId, c.PatientId)).Title("Action");
                })
                .Sortable()
                .Scrollable(scrolling => scrolling.Enabled(true))
                .Footer(false)
                .Render();
    }
    else
    {
        %>
        <div> No episodes associated with this period. </div>
        <% }%><%}))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true))
        .Footer(false)
        .Render();
         %>
<script type="text/javascript">
    $("#List_PatientManagedDates .t-grid-content").css("height", "auto");
    $("#List_PatientManagedDates .admissiondetail .t-grid-content").css({ 'height': 'auto' ,'position':'relative','top':'0'});
</script>