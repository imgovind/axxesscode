﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">New Patient | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Create", "Patient", FormMethod.Post, new { @id = "NewPatient_Form" })) { %>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="NewPatient_FirstName" class="fl strong"><span class="green">(M0040)</span> First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", (Model != null && Model.FirstName.IsNotNullOrEmpty()) ? Model.FirstName : string.Empty, new { @id = "NewPatient_FirstName", @class = "required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_MiddleInitial" class="fl strong"><span class="green">(M0040)</span> MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial", (Model != null && Model.MiddleInitial.IsNotNullOrEmpty()) ? Model.MiddleInitial : string.Empty, new { @id = "NewPatient_MiddleInitial", @class = "mi", @maxlength = "1" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_LastName" class="fl strong"><span class="green">(M0040)</span> Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", (Model != null && Model.LastName.IsNotNullOrEmpty()) ? Model.LastName : string.Empty, new { @id = "NewPatient_LastName", @class = "required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label class="fl strong"><span class="green">(M0069)</span> Gender</label>
                <div class="fr radio">
                    <%= Html.RadioButton("Gender", "Female", Model != null && Model.Gender == "Female", new { @id = "NewPatient_Gender_F", @class = "required" })%>
                    <label for="NewPatient_Gender_F" class="fixed short">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model != null && Model.Gender == "Male", new { @id = "NewPatient_Gender_M", @class = "required" }) %>
                    <label for="NewPatient_Gender_M" class="fixed short">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_DOB" class="fl strong"><span class="green">(M0066)</span> Date of Birth</label>
                <div class="fr"><input name="DOB" type="text" class="date-picker required" value="<%= Model != null && Model.DOB != DateTime.MinValue ? Model.DOBFormatted : string.Empty %>" id="NewPatient_DOB" /></div>
            </div>
            <div class="row">
                <label for="NewPatient_MaritalStatus" class="fl strong">Marital Status</label>
                <div class="fr">
                    <%  var maritalStatus = new SelectList(new[] {
                            new SelectListItem { Text = "** Select **", Value = "0" },
                            new SelectListItem { Text = "Married", Value = "Married" },
                            new SelectListItem { Text = "Divorce", Value = "Divorce" },
                            new SelectListItem { Text = "Widowed", Value = "Widowed" },
                            new SelectListItem { Text = "Single", Value = "Single" }
                        }, "Value", "Text", Model!=null? Model.MaritalStatus : string.Empty); %>
                    <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "NewPatient_MaritalStatus" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Height</label>
                <div class="fr radio">
                    <%= Html.TextBox("Height", Model != null ? Model.Height.ToString() : string.Empty, new { @id = "NewPatient_Height", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", Model != null && Model.HeightMetric == 0, new { @id = "NewPatient_HeightMetric0" })%>
                    <label for="NewPatient_HeightMetric0" class="fixed shortest">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model != null && Model.HeightMetric == 1, new { @id = "NewPatient_HeightMetric1" })%>
                    <label for="NewPatient_HeightMetric1" class="fixed shortest">cm</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Weight</label>
                <div class="fr radio">
                    <%= Html.TextBox("Weight", Model != null ? Model.Weight.ToString() : string.Empty, new { @id = "NewPatient_Weight", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model != null && Model.WeightMetric == 0, new { @id = "NewPatient_WeightMetric0" })%>
                    <label for="NewPatient_WeightMetric0" class="fixed shortest">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model != null && Model.WeightMetric == 1, new { @id = "NewPatient_WeightMetric1" })%>
                    <label for="NewPatient_WeightMetric1" class="fixed shortest">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_CaseManager" class="fl strong">Case Manager</label>
                <div class="fr"><%= Html.CaseManagers("CaseManagerId", "", new { @id = "NewPatient_CaseManager", @class = "required notzero" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_Assign" class="fl strong">Assign to Clinician/Case Manager</label>
                <div class="fr"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : string.Empty, new { @id = "NewPatient_Assign", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a id="NewPatient_VerifyMedicare"><span class="opt-text">Verify </span>Medicare Eligibility</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewPatient_PatientID" class="fl strong"><span class="green">(M0020)</span>Patient ID/MR Number</label>
                <div class="fr">
                    <%= Html.TextBox("PatientIdNumber", "", new { @id = "NewPatient_PatientID", @class = "required", @maxlength = "30" })%><br />
                    <em>Last MR Number Used: <strong><%= Current.LastUsedPatientId %></strong></em>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_MedicareNumber" class="fl strong"><span class="green">(M0063)</span> Medicare Number</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", (Model != null && Model.MedicareNumber.IsNotNullOrEmpty()) ? Model.MedicareNumber : string.Empty, new { @id = "NewPatient_MedicareNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewPatient_MedicaidNumber" class="fl strong"><span class="green">(M0065)</span> Medicaid Number</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", (Model != null && Model.MedicaidNumber.IsNotNullOrEmpty()) ? Model.MedicaidNumber : string.Empty, new { @id = "NewPatient_MedicaidNumber", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewPatient_SSN" class="fl strong"><span class="green">(M0064)</span> SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", (Model != null && Model.SSN.IsNotNullOrEmpty()) ? Model.SSN : string.Empty, new { @id = "NewPatient_SSN", @class = "numeric ssn", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="NewPatient_StartOfCareDate" class="fl strong"><span class="green">(M0030)</span> Start of Care Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartOfCareDate" id="NewPatient_StartOfCareDate" onchange="Patient.OnSocChange()" /></div>
            </div>
            <div class="row">
                <label for="NewPatient_EpisodeStartDate" class="fl strong">Episode Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EpisodeStartDate" id="NewPatient_EpisodeStartDate" /></div>
            </div>
            <div class="row">
                <label for="NewPatient_LocationId" class="fl strong">Agency Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "NewPatient_LocationId", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="NewPatient_CreateEpisode" type="checkbox" checked="checked" value="true" name="ShouldCreateEpisode" />
                        <label for="NewPatient_CreateEpisode">
                            Create Episode &#38; Schedule Start of Care Visit after saving<br />
                            <em>(If this box is unchecked, patient will be added to list of pending admissions)</em>
                        </label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="IsFaceToFaceEncounterCreated" />
                        <label for="NewPatient_IsFaceToFaceEncounterCreated">
                            Create a face to face encounter<br />
                            <em>(This is applicable for SOC date after 01/01/2011)</em>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="NewPatient_RaceAmericanIndian" type="checkbox" value="0" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceAmericanIndian">American Indian or Alaska Native</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_RaceAsian" type="checkbox" value="1" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceAsian">Asian</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_RaceBlack" type="checkbox" value="2" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceBlack">Black or African-American</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_RaceHispanic" type="checkbox" value="3" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceHispanic">Hispanic or Latino</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_RaceHawaiian" type="checkbox" value="4" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceHawaiian">Native Hawaiian or Pacific Islander</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_RaceWhite" type="checkbox" value="5" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceWhite">White</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_RaceUnknown" type="checkbox" value="6" name="EthnicRaces" class="required" />
                        <label for="NewPatient_RaceUnknown">Unknown</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source</legend>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="NewPatient_PaymentSourceNone" type="checkbox" value="0" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceNone">None; No Charge for Current Services</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceMedicare" type="checkbox" value="1" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceMedicare">Medicare (Traditional Fee-for-Service)</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceMedicareHmo" type="checkbox" value="2" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceMedicareHmo">Medicare (HMO/Managed Care)</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceMedicaid" type="checkbox" value="3" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceMedicaid">Medicaid (Traditional Fee-for-Service)</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceMedicaidHmo" type="checkbox" value="4" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceMedicaidHmo">Medicaid (HMO/Managed Care)</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceWorkers" type="checkbox" value="5" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceWorkers">Workers&#8217; Compensation</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceTitleProgram" type="checkbox" value="6" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceTitleProgram">Title Programs (e.g., Titile III, V, or XX)</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceOtherGovernment" type="checkbox" value="7" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceOtherGovernment">Other Government (e.g.,CHAMPUS, VA, etc.)</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourcePrivate" type="checkbox" value="8" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourcePrivate">Private Insurance</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourcePrivateHmo" type="checkbox" value="9" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourcePrivateHmo">Private HMO/Managed Care</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceSelf" type="checkbox" value="10" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceSelf">Self-Pay</label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_PaymentSourceUnknown" type="checkbox" value="11" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSourceUnknown">Unknown</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" id="NewPatient_PaymentSource" value="12" name="PaymentSources" class="required" />
                        <label for="NewPatient_PaymentSource">Other</label>
                        <div class="more"><%= Html.TextBox("OtherPaymentSource", "", new { @id = "NewPatient_OtherPaymentSource" }) %></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="column">
            <div class="row">
                <label for="NewPatient_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", (Model != null && Model.AddressLine1.IsNotNullOrEmpty()) ? Model.AddressLine1.ToString() : string.Empty, new { @id = "NewPatient_AddressLine1", @class = "required", @maxlength="50" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", (Model != null && Model.AddressLine2.IsNotNullOrEmpty()) ? Model.AddressLine2.ToString() : string.Empty, new { @id = "NewPatient_AddressLine2", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewPatient_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", (Model != null && Model.AddressCity.IsNotNullOrEmpty()) ? Model.AddressCity.ToString() : string.Empty, new { @id = "NewPatient_AddressCity", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewPatient_AddressStateCode" class="fl strong"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", (Model != null && Model.AddressStateCode.IsNotNullOrEmpty()) ? Model.AddressStateCode.ToString() : string.Empty, new { @id = "NewPatient_AddressStateCode", @class = "state required notzero", @status = "State (Required)" })%>
                    <%= Html.TextBox("AddressZipCode", (Model != null && Model.AddressZipCode.IsNotNullOrEmpty()) ? Model.AddressZipCode.ToString() : string.Empty, new { @id = "NewPatient_AddressZipCode", @class = "numeric required zip", @maxlength = "9", @status = "Zip Code (Required)" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewPatient_HomePhone1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 3 ? Model.PhoneHome.Substring(0, 3) : string.Empty, new { @id = "NewPatient_HomePhone1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 6 ? Model.PhoneHome.Substring(3, 3) : string.Empty, new { @id = "NewPatient_HomePhone2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10 ? Model.PhoneHome.Substring(6, 4) : string.Empty, new { @id = "NewPatient_HomePhone3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_MobilePhone1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneMobileArray", string.Empty, new { @id = "NewPatient_PhoneMobileArray1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneMobileArray", string.Empty, new { @id = "NewPatient_PhoneMobileArray2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneMobileArray", string.Empty, new { @id = "NewPatient_PhoneMobileArray3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_Email" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", (Model != null && Model.EmailAddress.IsNotNullOrEmpty()) ? Model.EmailAddress : string.Empty, new { @id = "NewPatient_Email", @maxlength = "50", @class = "email" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewPatient_PhysicianDropDown1" class="fl strong">Primary Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewPatient_PhysicianDropDown1", @class = "physician-picker" })%>
                    <a class="abs addrem">+</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewPatient_PhysicianDropDown2" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewPatient_PhysicianDropDown2", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewPatient_PhysicianDropDown3" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewPatient_PhysicianDropDown3", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewPatient_PhysicianDropDown4" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewPatient_PhysicianDropDown4", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewPatient_PhysicianDropDown5" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "NewPatient_PhysicianDropDown5", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Insurance/Payor Information</legend>
        <div class="column">
    <%  var htmlAttributes = new Dictionary<string, string>(); %>
    <%  htmlAttributes.Add("id", "NewPatient_PrimaryInsurance"); %>
    <%  htmlAttributes.Add("class", "insurance required notzero"); %>
            <div class="row">
                <label for="NewPatient_PrimaryInsurance" class="fl strong">Primary</label>
                <div class="fr"><%= Html.InsurancesByBranch("PrimaryInsurance", string.Empty, Guid.Empty, true, htmlAttributes)%></div>
            </div>
            <div class="more">
                <div class="row">
                    <label for="NewPatient_PrimaryHealthPlanId" class="fl strong margin">Health Plan Id</label>
                    <div class="fr"><%= Html.TextBox("PrimaryHealthPlanId", string.Empty, new { @id = "NewPatient_PrimaryHealthPlanId" })%></div>
                </div>
                <div class="row">
                    <label for="NewPatient_PrimaryGroupName" class="fl strong margin">Group Name</label>
                    <div class="fr"><%= Html.TextBox("PrimaryGroupName", string.Empty, new { @id = "NewPatient_PrimaryGroupName" })%></div>
                </div>
                <div class="row">
                    <label for="NewPatient_PrimaryGroupId" class="fl strong margin">Group Id</label>
                    <div class="fr"><%= Html.TextBox("PrimaryGroupId", string.Empty, new { @id = "NewPatient_PrimaryGroupId" })%></div>
                </div>
            </div>
    <%  htmlAttributes["id"] = "NewPatient_SecondaryInsurance"; %>
    <%  htmlAttributes["class"] = "insurance"; %>
            <div class="row">
                <label for="NewPatient_SecondaryInsurance" class="fl strong">Secondary</label>
                <div class="fr"><%= Html.InsurancesByBranch("SecondaryInsurance", string.Empty, Guid.Empty, true, htmlAttributes)%></div>
            </div>
            <div class="more">
                <div class="row">
                    <label for="NewPatient_SecondaryHealthPlanId" class="fl strong margin">Health Plan Id</label>
                    <div class="fr"><%= Html.TextBox("SecondaryHealthPlanId", string.Empty, new { @id = "NewPatient_SecondaryHealthPlanId" })%></div>
                </div>
                <div class="row">
                    <label for="NewPatient_SecondaryGroupName" class="fl strong margin">Group Name</label>
                    <div class="fr"><%= Html.TextBox("SecondaryGroupName", string.Empty, new { @id = "NewPatient_SecondaryGroupName" })%></div>
                </div>
                <div class="row">
                    <label for="NewPatient_SecondaryGroupId" class="fl strong margin">Group Id</label>
                    <div class="fr"><%= Html.TextBox("SecondaryGroupId", string.Empty, new { @id = "NewPatient_SecondaryGroupId" })%></div>
                </div>
            </div>
    <%  htmlAttributes["id"] = "NewPatient_TertiaryInsurance"; %>
            <div class="row">
                <label for="NewPatient_TertiaryInsurance" class="fl strong">Tertiary</label>
                <div class="fr"><%= Html.InsurancesByBranch("TertiaryInsurance", string.Empty, Guid.Empty, true, htmlAttributes)%></div>
            </div>
            <div class="more">
                <div class="row">
                    <label for="NewPatient_TertiaryHealthPlanId" class="fl strong margin">Health Plan Id</label>
                    <div class="fr"><%= Html.TextBox("TertiaryHealthPlanId", string.Empty, new { @id = "NewPatient_TertiaryHealthPlanId" })%></div>
                </div>
                <div class="row">
                    <label for="NewPatient_TertiaryGroupName" class="fl strong margin">Group Name</label>
                    <div class="fr"><%= Html.TextBox("TertiaryGroupName", string.Empty, new { @id = "NewPatient_TertiaryGroupName" })%></div>
                </div>
                <div class="row">
                    <label for="NewPatient_TertiaryGroupId" class="fl strong margin">Group Id</label>
                    <div class="fr"><%= Html.TextBox("TertiaryGroupId", string.Empty, new { @id = "NewPatient_TertiaryGroupId" })%></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Emergency Triage</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="NewPatient_Triage1" type="radio" value="1" name="Triage" />
                        <label for="NewPatient_Triage1">
                            <span class="fl strong">1.)</span>
                            <span class="normal margin"><strong>Life threatening</strong> (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.</span>
                        </label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_Triage2" type="radio" value="2" name="Triage" />
                        <label for="NewPatient_Triage2">
                            <span class="fl strong">2.)</span>
                            <span class="normal margin"><strong>Not life threatening but would suffer severe adverse effects</strong> from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage).</span>
                        </label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_Triage3" type="radio" value="3" name="Triage" />
                        <label for="NewPatient_Triage3">
                            <span class="fl strong">3.)</span>
                            <span class="normal margin"><strong>Visits could be postponed 24&#8211;48</strong> hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage).</span>
                        </label>
                    </div>
                    <div class="option">
                        <input id="NewPatient_Triage4" type="radio" value="4" name="Triage" />
                        <label for="NewPatient_Triage4">
                            <span class="fl strong">4.)</span>
                            <span class="normal margin"><strong>Visits could be postponed 72&#8211;96</strong> hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10&#8211;14 days, routine catheter changes).</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="NewPatient_EmergencyContactFirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.FirstName", "", new { @id = "NewPatient_EmergencyContactFirstName", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_EmergencyContactLastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.LastName", "", new { @id = "NewPatient_EmergencyContactLastName", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_EmergencyContactRelationship" class="fl strong">Relationship</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.Relationship", "", new { @id = "NewPatient_EmergencyContactRelationship" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_EmergencyContactPhonePrimary1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("EmergencyContact.PhonePrimaryArray", string.Empty, new { @id = "NewPatient_EmergencyContactPhonePrimary1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("EmergencyContact.PhonePrimaryArray", string.Empty, new { @id = "NewPatient_EmergencyContactPhonePrimary2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("EmergencyContact.PhonePrimaryArray", string.Empty, new { @id = "NewPatient_EmergencyContactPhonePrimary3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_EmergencyContactPhoneAlternate1" class="fl strong">Alternate Phone</label>
                <div class="fr">
                    <%= Html.TextBox("EmergencyContact.PhoneAlternateArray", string.Empty, new { @id = "NewPatient_EmergencyContactPhoneAlternate1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("EmergencyContact.PhoneAlternateArray", string.Empty, new { @id = "NewPatient_EmergencyContactPhoneAlternate2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("EmergencyContact.PhoneAlternateArray", string.Empty, new { @id = "NewPatient_EmergencyContactPhoneAlternate3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewPatient_EmergencyContactEmail" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmergencyContact.EmailAddress", "", new { @id = "NewPatient_EmergencyContactEmail", @class = "email", @maxlength = "100" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fr">
        <legend>Evacuation Zone</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Evacuation Zone</label>
                <div class="fr">
                    <select id="NewPatient_EvacuationZone" name="EvacuationZone">
                        <option value=""></option>
                        <option value="A">Zone A</option>
                        <option value="B">Zone B</option>
                        <option value="C">Zone C</option>
                        <option value="D">Zone D</option>
                        <option value="E">Zone E</option>
                        <option value="NE">Zone NE</option>
                        <option value="SN">Zone SN</option>
                    </select>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Pharmacy Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewPatient_PharmacyName" class="fl strong">Name</label>
                <div class="fr"><%= Html.TextBox("PharmacyName", "", new { @id = "NewPatient_PharmacyName", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="NewPatient_PharmacyPhone" class="fl strong">Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PharmacyPhoneArray", string.Empty, new { @id = "NewPatient_PharmacyPhone1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PharmacyPhoneArray", string.Empty, new { @id = "NewPatient_PharmacyPhone2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PharmacyPhoneArray", string.Empty, new { @id = "NewPatient_PharmacyPhone3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="NewPatient_ReferralPhysician" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("ReferrerPhysician", (Model != null && !Model.ReferrerPhysician.IsEmpty()) ? Model.ReferrerPhysician.ToString() : string.Empty, new { @id = "NewPatient_ReferrerPhysician", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
            <div class="row">
                <label for="NewPatient_AdmissionSource" class="fl strong">Admission Source</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource > 0) ? Model.AdmissionSource.ToString() : string.Empty, new { @id = "NewPatient_AdmissionSource", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewPatient_OtherReferralSource" class="fl strong">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("OtherReferralSource", (Model != null && Model.OtherReferralSource.IsNotNullOrEmpty()) ? Model.OtherReferralSource : string.Empty, new { @id = "NewPatient_OtherReferralSource", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="NewPatient_PatientReferralDate" class="fl strong"><span class="green">(M0104)</span> Referral Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReferralDate" id="NewPatient_PatientReferralDate" /></div>
            </div>
            <div class="row">
                <label for="NewPatient_InternalReferral" class="fl strong">Internal Referral</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", (Model != null && !Model.InternalReferral.IsEmpty()) ? Model.InternalReferral.ToString() : string.Empty, new { @id = "NewPatient_InternalReferral" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <div class="wide column">
            <div class="row">
                <%  string[] servicesRequired = Model != null && Model.ServicesRequired != null && Model.ServicesRequired.IsNotNullOrEmpty() ? Model.ServicesRequired.Split(';') : new string[] {};  %>
                <input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id ='NewPatient_ServicesRequiredCollection0' type='checkbox' value='0' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("0").ToChecked()) %>
                        <label for="NewPatient_ServicesRequiredCollection0">SNV</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='NewPatient_ServicesRequiredCollection1' type='checkbox' value='1' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("1").ToChecked()) %>
                        <label for="NewPatient_ServicesRequiredCollection1">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='NewPatient_ServicesRequiredCollection2' type='checkbox' value='2' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("2").ToChecked()) %>
                        <label for="NewPatient_ServicesRequiredCollection2">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='NewPatient_ServicesRequiredCollection3' type='checkbox' value='3' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("3").ToChecked()) %>
                        <label for="NewPatient_ServicesRequiredCollection3">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='NewPatient_ServicesRequiredCollection4' type='checkbox' value='4' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("4").ToChecked()) %>
                        <label for="NewPatient_ServicesRequiredCollection4">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='NewPatient_ServicesRequiredCollection5' type='checkbox' value='5' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("5").ToChecked()) %>
                        <label for="NewPatient_ServicesRequiredCollection5">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <div class="wide column">
            <div class="row">
                <%  string[] DME = (Model != null && Model.DME.IsNotNullOrEmpty()) ? Model.DME.Split(';') : new string[] {};  %>
                <input type="hidden" value=" " class="radio" name="DMECollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection0' type='checkbox' value='0' name='DMECollection' {0} />", DME.Contains("0").ToChecked()) %>
                        <label for="NewPatient_DMECollection0">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection1' type='checkbox' value='1' name='DMECollection' {0} />", DME.Contains("1").ToChecked()) %>
                        <label for="NewPatient_DMECollection1">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection2' type='checkbox' value='2' name='DMECollection' {0} />", DME.Contains("2").ToChecked()) %>
                        <label for="NewPatient_DMECollection2">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection3' type='checkbox' value='3' name='DMECollection' {0} />", DME.Contains("3").ToChecked()) %>
                        <label for="NewPatient_DMECollection3">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection4' type='checkbox' value='4' name='DMECollection' {0} />", DME.Contains("4").ToChecked()) %>
                        <label for="NewPatient_DMECollection4">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection5' type='checkbox' value='5' name='DMECollection' {0} />", DME.Contains("5").ToChecked()) %>
                        <label for="NewPatient_DMECollection5">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection6' type='checkbox' value='6' name='DMECollection' {0} />", DME.Contains("6").ToChecked()) %>
                        <label for="NewPatient_DMECollection6">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection7' type='checkbox' value='7' name='DMECollection' {0} />", DME.Contains("7").ToChecked()) %>
                        <label for="NewPatient_DMECollection7">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection8' type='checkbox' value='8' name='DMECollection' {0} />", DME.Contains("8").ToChecked()) %>
                        <label for="NewPatient_DMECollection8">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection9' type='checkbox' value='9' name='DMECollection' {0} />", DME.Contains("9").ToChecked()) %>
                        <label for="NewPatient_DMECollection9">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='NewPatient_DMECollection10' type='checkbox' value='10' name='DMECollection' {0} />", DME.Contains("10").ToChecked()) %>
                        <label for="NewPatient_DMECollection10">Other</label>
                        <div class="more"><%= Html.TextBox("OtherDME", (Model != null && Model.OtherDME.IsNotNullOrEmpty()) ? Model.OtherDME : string.Empty, new { @id = "NewPatient_OtherDME" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row">
                <%= Html.TextArea("Comments", (Model != null && Model.Comments.IsNotNullOrEmpty()) ? Model.Comments : string.Empty, new { @id = "NewPatient_Comments", @class = "tall", @maxcharacters = "500" })%>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "3", new { @id = "NewPatient_Status" })%>     
    <%= Html.Hidden("ReferralId", (Model != null && !Model.Id.IsEmpty()) ? Model.Id.ToString() : "false", new { @id = "NewPatient_ReferralId" })%>     
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="save admit close">Admit Patient</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>