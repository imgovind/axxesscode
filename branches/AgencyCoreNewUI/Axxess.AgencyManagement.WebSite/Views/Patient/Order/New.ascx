﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">New Order | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<% using (Html.BeginForm("Order/Create", "Patient", FormMethod.Post, new { @id = "NewOrderForm" })) { %>
    <fieldset>
        <legend>Order</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong" for="NewOrder_PatientName">Patient Name</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : "", new { @id = "NewOrder_PatientName", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewOrder_EpisodeList">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "NewOrder_EpisodeList", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewOrder_Date">Date</label>
                <div class="fr"><input type="text" name="Created" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="NewOrder_Date" class="date-picker required" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong" for="NewOrder_PhysicianDropDown">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", "", new { @id = "NewOrder_PhysicianDropDown", @class = "physician-picker required notzero" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician">New Physician</a></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="NewOrder_Summary" class="strong">Summary/Title</label>
                <div class="ac"><%= Html.TextBox("Summary", "", new { @id = "NewOrder_Summary", @class = "required fill", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewOrder_Text" class="fl strong">Order Text</label>
                <div class="ac">
                    <%= Html.Templates("NewOrder_Templates") %>
                    <%= Html.TextArea("Text", string.Empty, 8, 20, new { @class = "tallest required", @id = "NewOrder_Text", @maxcharacters = "2500" })%>
                </div>
            </div>
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsOrderForNextEpisode", false, new { @id = "NewOrder_IsOrderForNextEpisode" }) %>
                        <label for="NewOrder_IsOrderForNextEpisode">Order is for the Next Episode</label>
                    </div>
                    <div class="option">
                        <%= Html.CheckBox("IsOrderReadAndVerified", false, new { @id = "NewOrder_IsOrderReadAndVerified" })%>
                        <label for="NewOrder_IsOrderReadAndVerified">Order Read Back and Verified</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="NewOrder_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "NewOrder_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewOrder_ClinicianSignatureDate" class="fl strong">Signature Date</label>
                <div class="fr"><input type="text" name="SignatureDate" id="NewOrder_ClinicianSignatureDate" class="date-picker complete-required" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "NewOrder_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Create Order</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>