﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Order History | <%= Model.DisplayName %></span>
<div class="wrapper main blue">
    <%= Html.Hidden("PatientId", Model.Id, new { @id = "PatientOrderHistory_PatientId" })%>
    <div class="fr buttons">
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="filter optional">
            <label for="PatientOrderHistory_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker" name="StartDate" id="PatientOrderHistory_StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" />
            <label for="PatientOrderHistory_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker" name="EndDate" id="PatientOrderHistory_EndDate" value="<%= DateTime.Now.ToShortDateString() %>" />
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name("PatientOrderHistory_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
        columns.Bound(o => o.Number).Title("Order Number").Width(10).Sortable(false).ReadOnly();
        columns.Bound(o => o.Text).Title("Type").Width(18).Sortable(false).ReadOnly();
        columns.Bound(o => o.StatusName).Title("Status").Width(18).Sortable(false).ReadOnly();
        columns.Bound(o => o.PhysicianName).Title("Physician").Width(15).Sortable(false).ReadOnly();
        columns.Bound(o => o.HasPhysicianAccess).Title("Electronic").Width(10).Sortable(true).ReadOnly();
        columns.Bound(o => o.OrderDate).Title("Order Date").Width(8).Sortable(true).ReadOnly();
        columns.Bound(o => o.SendDateFormatted).Title("Sent Date").Width(8).Sortable(false);
        columns.Bound(o => o.ReceivedDateFormatted).Title("Received Date").Width(8).Sortable(false);
        columns.Bound(o => o.PrintUrl).Title(" ").Width(8).Sortable(false);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrderHistoryGrid", "Patient", new { patientId = Model.Id, startDate = DateTime.Now.AddDays(-60), endDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>