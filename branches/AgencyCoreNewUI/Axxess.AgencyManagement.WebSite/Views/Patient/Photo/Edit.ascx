﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Photo/Update", "Patient", FormMethod.Post, new { @id = "PatientPhoto_Form" })) { %>
    <%= Html.Hidden("id", Model.Id)%>
    <fieldset>
        <legend>Change Patient Photo</legend>
        <div class="wide column">
            <div class="row ac">
                <h1><%= Model.DisplayName %></h1>
            </div>
            <div class="row ac">
    <%  if (!Model.PhotoId.IsEmpty()) { %>            
                <img src="/Asset/<%= Model.PhotoId.ToString() %>" alt="User Photo" />
    <%  } else { %>
                <img src="/Images/blank_user.jpeg" alt="User Photo" />
    <%  } %>
            </div>
            <div class="row">
                <label for="PatientPhoto_Photo" class="fl strong">New Photo</label>
                <div class="fr"><input id="PatientPhoto_Photo" type="file" name="Photo1" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
    <%  if (!Model.PhotoId.IsEmpty()) { %>
            <li><a class="delete">Remove</a></li>
    <%  } %>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>