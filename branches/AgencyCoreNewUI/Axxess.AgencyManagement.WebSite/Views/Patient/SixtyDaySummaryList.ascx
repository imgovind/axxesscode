﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient 60 Day Summary | <%= Model.DisplayNameWithMi %></span>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<VisitNoteViewData>().Name("ListPatientSixtyDaySummary_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(v => v.UserDisplayName).Title("Employee Name").Sortable(false).Width(20).ReadOnly();
            columns.Bound(v => v.VisitDate).Title("Visit Date").Sortable(false).Width(10).ReadOnly();
            columns.Bound(v => v.SignatureDate).Title("Signature Date").Sortable(false).Width(10).ReadOnly();
            columns.Bound(v => v.EpisodeRange).Title("Episode Date").Sortable(false).Width(20).ReadOnly();
            columns.Bound(v => v.PhysicianDisplayName).Title("Physician Name").Sortable(false).Width(20).ReadOnly();
            columns.Bound(v => v.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(3);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("SixtyDaySummaryGrid", "Patient", new { id = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>