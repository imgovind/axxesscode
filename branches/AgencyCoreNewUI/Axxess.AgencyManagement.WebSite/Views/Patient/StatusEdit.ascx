﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Status/Update", "Patient", FormMethod.Post, new { @id = "EditPatientStatus_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Name</label>
                <div class="fr"><%= Model.DisplayNameWithMi %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">MRN</label>
                <div class="fr"><%= Model.PatientIdNumber %></div>
            </div>
        </div>
    </fieldset>
    <%  if (Model.IsDischarged) { %>
    <fieldset>
        <legend>Status</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatientStatus_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.StatusPatients("Status", "2", new { @id = "EditPatientStatus_Status" })%></div>
            </div>
        </div>
        <div class="wide column">
            <div class="row ac strong">This patient will be added to your list of active patients. If the patient to be readmitted you have to exit and readmit with a new start of care but reactivating will take the last start of care date.</div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
    <%  } else { %>
    <fieldset>
        <legend>Status</legend>
        <div class="column">
            <div class="row">
                <label for="EditPatientStatus_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.StatusPatients("Status", "1", new { @id = "EditPatientStatus_Status" })%></div>
            </div>
        </div>
    </fieldset>
    <div id="EditPatientStatus_DischargeContainer">
        <fieldset>
            <legend>Discharge Information</legend>
            <div class="column">
                <div class="row">
                    <label for="EditPatientStatus_DischargeDate" class="fl strong">Discharge Date</label>
                    <div class="fr"><input type="text" class="date-picker required" name="DateOfDischarge" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="EditPatientStatus_DischargeDate" /></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="EditPatientStatus_DischargeReason" class="fl strong">Discharge Reason</label>
                    <div class="fr"><%= Html.DischargeReasons("ReasonId", new { @id = "EditPatientStatus_DischargeReason" })%></div>
                </div>
            </div>
            <div class="wide column">
                <div class="row">
                    <label for="EditPatientStatus_DischargeComments" class="strong">Comments</label>
                    <div class="ac"><%= Html.TextArea("Comments", "", new { @id = "EditPatientStatus_DischargeComments", @maxcharacters = "500", @class = "tall" })%></div>
                </div>
                <div class="row ac strong">This patient will be added to your list of discharged patients. To provide services to this patient in the future, you will have to re-admit the patient.</div>
            </div>
        </fieldset>
    </div>
    <div id="EditPatientStatus_NonAdmissionContainer">
        <fieldset>
            <legend>Non-Admission Information</legend>
            <div class="column">
                <div class="row">
                    <label for="EditPatientStatus_NonAdmitDate" class="fl strong">Non-Admit Date</label>
                    <div class="fr"><input type="text" class="date-picker required" name="NonAdmitDate" value="<%= DateTime.Today.ToString("MM/dd/yyyy") %>" id="EditPatientStatus_NonAdmitDate" /></div>
                </div>
            </div>
            <div class="wide column">
                <div class="row">
                    <label class="strong">Reason Not Admitted</label>
                    <div class="checkgroup">
                        <div class="option">
                            <input id="EditPatientStatus_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" class="required" />
                            <label for="EditPatientStatus_ReasonInAppropriate">Inappropriate For Home Care</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonRefused" type="checkbox" value="Patient Refused Service" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonRefused">Patient Refused Service</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonOutofService">Out of Service Area</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonOnService">On Service with another agency</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonNotaProvider">Not a Provider</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonNotHomebound">Not Homebound</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonRedirected">Redirected to alternate care facility</label>
                        </div>
                        <div class="option">
                            <input id="EditPatientStatus_ReasonOther" type="checkbox" value="Other" name="Reason" class="required" />
                            <label for="ChangeStatus_ReasonOther">Other <em>(Specify in Comments)</em></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="EditPatientStatus_NonAdmissionComments" class="strong">Comments</label>
                    <div class="ac"><%= Html.TextArea("Comments", "", new { @id = "EditPatientStatus_NonAdmissionComments", @maxcharacters = "500", @class = "tall" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
    <%  } %>
<%  } %>
</div>