﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientInsuranceInfoViewData>" %>
<div class="margin fr" >
<% var action = string.Format("{0}_{1}", Model.ActionType, Model.InsuranceType); %>
<div class="row" ><label for="<% =action %>HealthPlanId" class="float-left">Health Plan Id</label><div class="fr"><%= Html.TextBox( string.Format("{0}HealthPlanId",Model.InsuranceType), Model.HealthPlanId, new { @id = action +"HealthPlanId" })%></div></div>
<div class="row" ><label for="<% =action %>GroupName" class="float-left">Group Name </label><div class="fr"><%= Html.TextBox(string.Format("{0}GroupName", Model.InsuranceType), Model.GroupName, new { @id = action + "GroupName" })%></div></div>
<div class="row" ><label for="<% =action %>GroupId" class="float-left">Group Id</label><div class="fr"><%= Html.TextBox(string.Format("{0}GroupId", Model.InsuranceType), Model.GroupId, new { @id = action + "GroupId" })%></div></div>
</div>