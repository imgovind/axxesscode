﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientData>>" %>
<%  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty; %>
<%  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty; %>
<%  var branchId = ViewData["BranchId"] != null ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<%  var status = ViewData["Status"] != null ? ViewData["Status"].ToString() : "1"; %>
<%= Html.Telerik().Grid(Model).Name("ListPatient_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
        columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(90);
        columns.Bound(p => p.DisplayName).Title("Name").Width(180);
        columns.Bound(p => p.Address).Title("Address").Sortable(false);
        columns.Bound(p => p.DateOfBirth).Format("{0:MM/dd/yyyy}").Title("Date of Birth").Width(100).Sortable(true);
        columns.Bound(p => p.Gender).Width(80).Sortable(true);
        columns.Bound(p => p.Phone).Title("Phone").Width(120).Sortable(false);
        columns.Bound(p => p.Status).Title("Status").Width(80).Sortable(false);
        columns.Bound(p => p.Id).Width(90).Sortable(false).Template(s => string.Format("<a class=\"link\" onclick=\"Patient.Edit('{0}');return false\">Edit</a> | <a class=\"link\" onclick=\"Patient.Delete('{0}');return false\">Delete</a>", s.Id)).ClientTemplate("<a class=\"link\" onclick=\"Patient.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Patient.Delete('<#=Id#>');return false\">Delete</a>").Title("Action");
    }).Sortable(sorting =>
        sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            if (sortName == "PatientIdNumber") {
                if (sortDirection == "ASC") order.Add(o => o.PatientIdNumber).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.PatientIdNumber).Descending();
            } else if (sortName == "DisplayName") {
                if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
            } else if (sortName == "DateOfBirth") {
                if (sortDirection == "ASC") order.Add(o => o.DateOfBirth).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DateOfBirth).Descending();
            } else if (sortName == "Gender") {
                if (sortDirection == "ASC") order.Add(o => o.Gender).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.Gender).Descending();
            }
        })
    ).Scrollable(scrolling => scrolling.Enabled(true)) %>