﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Non-Admitted Patients | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
    <div class="fr buttons">
        <ul>
            <li><a class="new-patient">New Patient</a></li>
        </ul>
    </div>
<%  } %>
    <fieldset class="grid-controls short ac">
        <div class="filter grid-search"></div>
    </fieldset>
<%= Html.Telerik().Grid<NonAdmit>().Name("ListNonAdmission_Grid").Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(10);
            columns.Bound(p => p.DisplayName).Title("Name").Width(15);
            columns.Bound(p => p.DateOfBirth).Title("Date of Birth").Width(8).Sortable(true);
            columns.Bound(p => p.Phone).Title("Phone").Width(10).Sortable(false);
            columns.Bound(p => p.NonAdmissionReason).Title("Non-Admit Reason").Width(30).Sortable(false);
            columns.Bound(p => p.NonAdmitDate).Title("Non-Admit Date").Width(8).Sortable(true);
            columns.Bound(p => p.Id).Width(5).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"Patient.Admit('<#=Id#>','<#=Type#>');return false\">Admit</a>").Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("NonAdmissionGrid", "Patient")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>