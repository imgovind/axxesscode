﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
    <fieldset>
        <legend>Patient Details</legend>
        <div class="wide column">
            <div class="row">
                <label class="fl strong">Id</label>
                <div class="fr"><%= Model.PatientIdNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Name</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Gender</label>
                <div class="fr"><%= Model.Gender %></div>
            </div>
            <div class="row">
                <label class="fl strong">Address</label>
                <div class="fr"><%= Model.AddressFirstRow.ToTitleCase() %></div>
            </div>
            <div class="row">
                <label class="fl strong">City, State, Zip</label>
                <div class="fr"><%= Model.AddressSecondRow.IsNotNullOrEmpty() ? Model.AddressSecondRow.ToTitleCase() : string.Empty %></div>
            </div>
            <div class="row">
                <label class="fl strong">Home Phone</label>
                <div class="fr"><%= Model.PhoneHome.ToPhone() %></div>
            </div>
            <div class="row">
                <label class="fl strong">Mobile Phone</label>
                <div class="fr"><%= Model.PhoneMobile.ToPhone() %></div>
            </div>
            <div class="row">
                <label class="fl strong">Start of Care Date</label>
                <div class="fr"><%= Model.StartOfCareDateFormatted %></div>
            </div>
            <div class="row">
                <label class="fl strong">Date of Birth</label>
                <div class="fr"><%= Model.DOBFormatted %></div>
            </div>
            <div class="row">
                <label class="fl strong">Medicare #</label>
                <div class="fr"><%= Model.MedicareNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Medicaid #</label>
                <div class="fr"><%= Model.MedicaidNumber %></div>
            </div>
<%  if (Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0) { %>
            <div class="row">
                <label class="fl strong">Physician Name</label>
                <div class="fr"><%= Model.PhysicianContacts[0].DisplayName%></div>
            </div>
            <div class="row">
                <label class="fl strong">Physician Phone</label>
                <div class="fr"><%= Model.PhysicianContacts[0].PhoneWork.ToPhone() %></div>
            </div>
<%  } %>
<%  if (Model.EmergencyContacts != null && Model.EmergencyContacts.Count > 0) { %>
            <div class="row">
                <label class="fl strong">Emergency Name</label>
                <div class="fr"><%= Model.EmergencyContacts[0].DisplayName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Emergency Phone</label>
                <div class="fr"><%= Model.EmergencyContacts[0].PrimaryPhone.ToPhone() %></div>
            </div>
<%  } %>
        </div>
    </fieldset>
</div>