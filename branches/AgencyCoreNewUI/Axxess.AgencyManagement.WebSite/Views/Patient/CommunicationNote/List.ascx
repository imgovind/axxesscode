﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CommunicationNote>>" %>
<span class="wintitle">Communication Notes | <%= Current.AgencyName %></span>
<%  var pageName = "AgencyCommunicationNotes"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-communication-note">New Communication Note</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls ac">
        <a class="grid-refresh hidden"></a>
        <div class="filter grid-search"></div>
        <div class="filter optional">
            <label for="<%= pageName%>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName+"_BranchId" })%>
        </div>
        <div class="filter optional">
            <label for="<%= pageName%>_Status" class="strong">Status</label>
            <select id="<%= pageName%>_Status" name="Status">
                <option value="0">All</option>
                <option value="1" selected="selected">Active</option>
                <option value="2">Discharged</option>
                <option value="3">Pending</option>
                <option value="4">Non-Admit</option>
            </select>
        </div>
        <div class="filter">
            <label for="<%= pageName%>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker" name="StartDate" id="<%= pageName%>_StartDate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" />
            <label for="<%= pageName%>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker" name="EndDate" id="<%= pageName%>_EndDate" value="<%= DateTime.Now.ToShortDateString() %>" />
        </div>
        <div class="filter patient">
            <label for="<%= pageName%>_Patient" class="strong">Patient</label>
            <%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", ViewData["PatientId"] != null ? ViewData["PatientId"].ToString() : Guid.Empty.ToString(), new { @id = pageName+"_PatientName" })%>
        </div>
    </fieldset>
    <div id="<%= pageName %>GridContainer"><% Html.RenderPartial("CommunicationNote/ListContent", Model); %></div>
</div>