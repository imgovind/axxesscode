﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">New Communication Note | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("CommunicationNote/Create", "Patient", FormMethod.Post, new { @id = "NewCommunicationNote_Form" })) { %>
    <fieldset>
        <legend>Communication Note</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong" for="NewCommunicationNote_PatientName">Patient Name</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : string.Empty, new { @id = "NewCommunicationNote_PatientName", @class="required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewCommunicationNote_EpisodeList">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "NewCommunicationNote_EpisodeList", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong" for="NewCommunicationNote_Date">Date</label>
                <div class="fr"><input type="text" name="Created" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="NewCommunicationNote_Date" class="date-picker required" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong" for="NewCommunicationNote_PhysicianDropDown">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", string.Empty, new { @id = "NewCommunicationNote_PhysicianDropDown", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician">New Physician</a></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="NewCommunicationNote_Text" class="fl strong">Communication Text</label>
                <div class="ac">
                    <%= Html.Templates("NewCommunicationNote_Templates") %>
                    <%= Html.TextArea("Text", string.Empty, 8, 20, new { @class = "tallest required", @id = "NewCommunicationNote_Text", @maxcharacters = "2500" })%>
                </div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("SendAsMessage", false, new { @id = "NewCommunicationNote_SendAsMessage"})%>
                        <label for="NewCommunicationNote_SendAsMessage">Send note as Message</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="NewCommunicationNote_Recipients">
                    <label class="strong">Recipients</label>
                    <%= Html.Recipients("NewCommunicationNote", new List<Guid>()) %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="NewCommunicationNote_ClinicianSignature" class="fl strong">Staff Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @id = "NewCommunicationNote_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewCommunicationNote_ClinicianSignatureDate" class="fl strong">Signature Date</label>
                <div class="fr"><input type="text" name="SignatureDate" id="NewCommunicationNote_ClinicianSignatureDate" class="date-picker complete-required" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", string.Empty, new { @id = "NewCommunicationNote_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>