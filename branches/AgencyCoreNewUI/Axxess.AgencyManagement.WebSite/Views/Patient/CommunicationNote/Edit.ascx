﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNote>" %>
<span class="wintitle">Edit Communication Note | <%= Current.AgencyName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("CommunicationNote/Update", "Patient", FormMethod.Post, new { @id = "EditCommunicationNote_Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditCommunicationNote_Id" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "EditCommunicationNote_UserId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditCommunicationNote_PatientId" })%>
    <fieldset>
        <legend>Communication Note</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient Name</label>
                <div class="fr"><%= Model != null && Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName : string.Empty %></div>
            </div>
            <div class="row">
    <%  if (Model.EpisodeId.IsEmpty()) { %>
                <label for="EditCommunicationNote_EpisodeList" class="fl strong">Episode</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "EditCommunicationNote_EpisodeList", @class = "required notzero" })%></div>
    <%  } else { %>
                <label class="fl strong">Episode</label>
                <div class="fr"><%= string.Format("{0} &#8211; {1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></div>
                <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "EditCommunicationNote_EpisodeId" })%>
    <%  } %>
            </div>
            <div class="row">
                <label for="EditCommunicationNote_Date" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="Created" value="<%= Model.Created.ToShortDateString() %>" id="EditCommunicationNote_Date" /></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditCommunicationNote_PhysicianDropDown" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : string.Empty, new { @id = "EditCommunicationNote_PhysicianDropDown", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician">New Physician</a></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="row">
                <label for="EditCommunicationNote_Text" class="fl strong">Communication Text</label>
                <div class="ac">
                    <%= Html.Templates("EditCommunicationNote_Templates")%>
                    <%= Html.TextArea("Text", Model.Text, 8, 20, new { @class = "tallest required", @id = "EditCommunicationNote_Text", @maxcharacters = "2500" })%>
                </div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("SendAsMessage", false, new { @id = "EditCommunicationNote_SendAsMessage" })%>
                        <label for="EditCommunicationNote_SendAsMessage">Send note as Message</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="EditCommunicationNote_Recipients">
                    <label class="strong">Recipients</label>
                    <%= Html.Recipients("EditCommunicationNote", new List<Guid>()) %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="EditCommunicationNote_ClinicianSignature" class="fl strong">Staff Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", string.Empty, new { @id = "EditCommunicationNote_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditCommunicationNote_ClinicianSignatureDate" class="fl strong">Signature Date</label>
                <div class="fr"><input type="text" name="SignatureDate" id="EditCommunicationNote_ClinicianSignatureDate" class="date-picker complete-required" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "EditCommunicationNote_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>