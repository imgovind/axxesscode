﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CommunicationNote>>" %>
<%  var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty; %>
<%  var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty; %>
<%  var patientId = ViewData["PatientId"] != null ? ViewData["PatientId"].ToString() : Guid.Empty.ToString(); %>
<%  var branchId = ViewData["BranchId"] != null ? ViewData["BranchId"].ToString() : Guid.Empty.ToString(); %>
<%  var status = ViewData["Status"] != null ? ViewData["Status"].ToString() : "1"; %>
<%= Html.Telerik().Grid(Model).Name("AgencyCommunicationNotes_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
        columns.Bound(c => c.DisplayName).Title("Patient Name").Sortable(true).ReadOnly().Width(12);
        columns.Bound(c => c.UserDisplayName).Title("Employee Name").Sortable(true).ReadOnly().Width(12);
        columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(true).ReadOnly().Width(6);
        columns.Bound(c => c.StatusName).Title("Status").Sortable(true).ReadOnly().Width(10);
        columns.Bound(c => c.Id).Title(" ").Template(s => string.Format("<a class=\"link\" onclick=\"Patient.CommunicationNote.Print('{0}','{1}');return false\"><span class=\"img icon print\"></span></a>", s.Id, s.PatientId)).Width(2).HtmlAttributes(new { @class = "ac" }).Sortable(false);
        columns.Bound(c => c.ActionUrl).Template(s => s.ActionUrl).Title("Action").Width(5).Sortable(false);
    }).Sortable(sorting =>
        sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
            if (sortName == "DisplayName") {
                if (sortDirection == "ASC") order.Add(o => o.DisplayName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.DisplayName).Descending();
            } else if (sortName == "UserDisplayName") {
                if (sortDirection == "ASC") order.Add(o => o.UserDisplayName).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.UserDisplayName).Descending();
            } else if (sortName == "Created") {
                if (sortDirection == "ASC") order.Add(o => o.Created).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.Created).Descending();
            } else if (sortName == "Status") {
                if (sortDirection == "ASC") order.Add(o => o.Status).Ascending();
                else if (sortDirection == "DESC") order.Add(o => o.Status).Descending();
            }
        })
    ).Scrollable(scrolling => scrolling.Enabled(true)) %>