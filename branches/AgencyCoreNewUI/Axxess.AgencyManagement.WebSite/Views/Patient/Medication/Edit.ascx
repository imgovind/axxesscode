<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medication/Update", "Patient", FormMethod.Post, new { @id = "EditMedication_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditMedication_Id" })%>
    <%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "EditMedication_ProfileId" })%>
    <fieldset>
        <legend>Edit Medication</legend>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsLongStanding", Model.IsLongStanding, new { @id = "EditMedication_IsLongStanding" })%>
                        <label for="EditMedication_IsLongStanding">Long Standing</label>
                    </div>
                </div>
            </div>
            <div class="row" id="EditMedication_StartDateRow">
                <label for="EditMedication_StartDate" class="fl strong">Start Date</label>
                <div class="fr"><input type="text" class="date-picker" name="StartDate" value="<%= Model.StartDate.IsValid() ? Model.StartDate.ToShortDateString() : string.Empty %>" id="EditMedication_StartDate" /></div>
            </div>
            <div class="row">
                <label for="EditMedication_MedicationDosage" class="fl strong">Medication &#38; Dosage</label>
                <div class="fr"><%= Html.TextBox("MedicationDosage", Model.MedicationDosage.IsNotNullOrEmpty() ? Model.MedicationDosage : string.Empty, new { @id = "EditMedication_MedicationDosage", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="EditMedication_Classification" class="fl strong">Classification</label>
                <div class="fr"><%= Html.TextBox("Classification", Model.Classification.IsNotNullOrEmpty() ? Model.Classification : string.Empty, new { @id = "EditMedication_Classification", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditMedication_Frequency" class="fl strong">Frequency</label>
                <div class="fr"><%= Html.TextBox("Frequency", Model.Frequency.IsNotNullOrEmpty() ? Model.Frequency : string.Empty, new { @id = "EditMedication_Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditMedication_Route" class="fl strong">Route</label>
                <div class="fr"><%= Html.TextBox("Route", Model.Route.IsNotNullOrEmpty() ? Model.Route : string.Empty, new { @id = "EditMedication_Route", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditMedication_Type" class="fl strong">Type</label>
                <div class="fr">
                    <%  var medicationTypes = new SelectList(new[] { 
                            new SelectListItem { Text = "New", Value = "N" },
                            new SelectListItem { Text = "Changed", Value = "C" },
                            new SelectListItem { Text = "Unchanged", Value = "U" }
                        }, "Value", "Text", Model.MedicationType.Value.IsNotNullOrEmpty() ? Model.MedicationType.Value : "N" ); %>
                    <%= Html.DropDownList("medicationType", medicationTypes) %>
                </div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>