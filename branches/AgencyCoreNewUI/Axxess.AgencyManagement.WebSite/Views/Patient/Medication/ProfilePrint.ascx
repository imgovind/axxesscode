﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileSnapshotViewData>" %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Patient/medprofile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : string.Empty) + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty) + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + location.PhoneWorkFormatted : string.Empty) + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty) : string.Empty) : string.Empty).Clean()%>",
        patientname:    "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br />" : "<br />") : string.Empty).Clean() %>",
        mr:             "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>",
        physician:      "<%= Model.PhysicianName.Clean().ToTitleCase() %>",
        episode:        "<%= Model != null && Model.StartDate.IsValid() && Model.EndDate.IsValid() ? string.Format("{0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()).Clean() : string.Empty %>",
        pharmacy:       "<%= Model != null ? Model.PharmacyName.Clean() : string.Empty %>",
        pharmphone:     "<%= Model != null ? Model.PharmacyPhone.ToPhone() : string.Empty %>",
        pridiagnosis:   "<%= Model != null && Model.PrimaryDiagnosis.IsNotNullOrEmpty() ? Model.PrimaryDiagnosis : string.Empty %>",
        secdiagnosis:   "<%= Model != null && Model.SecondaryDiagnosis.IsNotNullOrEmpty() ? Model.SecondaryDiagnosis : string.Empty %>",
        allergies:      "<%= Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies.Clean() : string.Empty %>",
        sign:           "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",
        signdate:       "<%= Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString().Clean() : string.Empty %>"
    };
    PrintPreview.BuildSections([
<%  var medications = Model != null && Model.MedicationProfile != null && Model.MedicationProfile.Medication.IsNotNullOrEmpty() ? Model.MedicationProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable) : null; %>
<%  if (medications != null && medications.Count() > 0) { %>
        { Content: [ [ "<h3>Active Medications</h3>" ] ] },
        { Content: [ [
            "<strong>LS</strong>",
            "<strong>Start Date</strong>",
            "<strong>Medication &#38; Dosage</strong>",
            "<strong>Frequency</strong>",
            "<strong>Route</strong>",
            "",
            "<strong>Classification</strong>" ] ] }
    <%  foreach (var medication in medications) { %>
        ,
        { Content: [ [
            PrintPreview.CheckBox("", <%= medication.IsLongStanding.ToString().ToLower() %>),
            "<%= medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty %>",
            "<%= medication.MedicationDosage.Clean() %>",
            "<%= medication.Frequency.Clean() %>",
            "<%= medication.Route.Clean() %>",
            "<%= medication.MedicationType.Value.Clean() %>",
            "<%= medication.Classification.Clean() %>" ] ] }
    <%  } %>
        ,
<%  } %>
<%  medications = Model != null && Model.MedicationProfile != null && Model.MedicationProfile.Medication.IsNotNullOrEmpty() ? Model.MedicationProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "DC").OrderByDescending(m => m.StartDateSortable) : null; %>
<%  if (medications != null && medications.Count() > 0) { %>
         { Content: [ [ "<h3>Discontinued Medications</h3>" ] ] },
         { Content: [ [
            "<strong>LS</strong>",
            "<strong>Start Date</strong>",
            "<strong>Medication &#38; Dosage</strong>",
            "<strong>Frequency</strong>",
            "<strong>Route</strong>",
            "",
            "<strong>Classification</strong>" ] ] }
    <%  foreach (var medication in medications) { %>
        ,
        { Content: [ [
            PrintPreview.CheckBox("", <%= medication.IsLongStanding.ToString().ToLower() %>),
            "<%= medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty %>",
            "<%= medication.MedicationDosage.Clean() %>",
            "<%= medication.Frequency.Clean() %>",
            "<%= medication.Route.Clean() %>",
            "<%= medication.MedicationType.Value.Clean() %>",
            "<%= medication.Classification.Clean() %>" ] ] }
    <%  } %>
<%  } %>
    ]);
</script>