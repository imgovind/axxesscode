﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medication/Interactions/Print", "Patient", FormMethod.Post, new { @id = "MedicationInteraction_Form" })) { %>
    <%= Html.Hidden("patientId", Model.PatientId)%>
    <fieldset>
        <div class="wide column">
            <div class="row ac">Select the medications that you wish to check for interactions below and click on the &#8220;Check Interactions&#8221; button.</div>
        </div>
    </fieldset>
    <%  if (Model != null) { %>
        <%  var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable); %>
        <%  if (medications != null && medications.Count() > 0) { %>
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Active Medication(s)</h3></li>
            <li>
                <span class="med-check"></span>
                <span class="med-interaction-name">Medication &#38; Dosage</span>
                <span class="med-interaction">Frequency</span>
                <span class="med-interaction">Route</span>
                <span class="med-interaction">Classification</span>
            </li>
        </ul>
        <div class="check-all"></div>
        <ol>
            <li>
                <span class="med-check"><input type="checkbox" class="select-all" id="MedicationInteractions_SelectAll" /></span>
                <span class="strong"><label for="MedicationInteractions_SelectAll">Select All</label></span>
            </li>
            <%  foreach (var med in medications) { %>
            <li>
                <span class="med-check"><%= string.Format("<input name=\"drugsSelected\" type=\"checkbox\" value=\"{0}\" />", med.LexiDrugId) %></span>
                <span class="med-interaction-name"><%= med.MedicationDosage %></span>
                <span class="med-interaction"><%= med.Frequency %></span>
                <span class="med-interaction"><%= med.Route %></span>
                <span class="med-interaction"><%= med.Classification %></span>
            </li>
            <%  } %>
        </ol>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="generate">Check Interactions</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
        <%  } else { %>
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Active Medication(s)</h3></li>
        </ul>
        <h1 class="blue">No Active Medications</h1>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
        <%  } %>
    <%  } %>
<%  } %>
</div>