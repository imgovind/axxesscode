﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
    <%= Html.Telerik().Grid<MedicationProfileHistory>().Name("ListMedicationSnapshot_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(m => m.UserName).Title("Signed By").Sortable(true).ReadOnly();
            columns.Bound(m => m.SignedDate).Title("Signed Date").Width(120).Sortable(true);
            columns.Bound(m => m.Id).Width(30).ReadOnly().ClientTemplate("<a onclick=\"Patient.Medication.Snapshot.Print('<#=Id#>');return false\"><span class=\"img icon print\"></span></a>").Sortable(false).Title("");
            columns.Bound(m => m.Id).Width(90).Sortable(false).Template(s => string.Format("<a class=\"link\" onclick=\"Patient.Medication.Snapshot.Edit('{0}');return false\">Edit</a> | <a class=\"link\" onclick=\"Patient.Medication.Snapshot.Delete('{0}', $(this).closest('.t-grid'));return false\">Delete</a>", s.Id)).ClientTemplate("<a class=\"link\" onclick=\"Patient.Medication.Snapshot.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Patient.Medication.Snapshot.Delete('<#=Id#>', $(this).closest('.t-grid'));return false\">Delete</a>").Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("MedicationSnapshotGrid", "Patient", new { patientId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>