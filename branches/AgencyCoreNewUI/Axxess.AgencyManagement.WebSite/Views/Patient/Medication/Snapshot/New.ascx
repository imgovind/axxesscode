﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile Snapshot | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medication/Snapshot/Create", "Patient", FormMethod.Post, new { @id = "NewMedicationSnapshot_Form" })) { %>
    <%= Html.Hidden("ProfileId", Model.MedicationProfile.Id)%>
    <%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
    <div class="note">
        <fieldset>
            <legend>Patient Information</legend>
            <div class="column">
                <div class="row">
                    <label class="fl strong">Patient Name</label>
                    <div class="fr"><%= Model.Patient.DisplayName %></div>
                </div>
                <div class="row">
                    <label class="fl strong">MRN</label>
                    <div class="fr"><%= Model.Patient.PatientIdNumber %></div>
                </div>
                <div class="row">
                    <label class="fl strong">DOB</label>
                    <div class="fr"><%= Model.Patient.DOBFormatted %></div>
                </div>
                <div class="row">
                    <label class="fl strong">Primary Diagnosis</label>
                    <div class="fr"><%= data.AnswerOrEmptyString("M1020PrimaryDiagnosis") %></div>
                </div>
                <div class="row">
                    <label class="fl strong">Secondary Diagnosis</label>
                    <div class="fr"><%= data.AnswerOrEmptyString("M1022PrimaryDiagnosis1") %></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="NewMedicationSnapshot_EpisodeRange" class="fl strong">Episode</label>
                    <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Model.EpisodeId.ToString(), Model.Patient.Id, "-- Select Episode --", new { @id = "NewMedicationSnapshot_EpisodeRange", @class = "required notzero" })%></div>
                </div>
                <div class="row">
                    <label for="NewMedicationSnapshot_Physician" class="fl strong">Physician</label>
                    <div class="fr"><%= Html.TextBox("PhysicianId", (Model != null && !Model.PhysicianId.IsEmpty()) ? Model.PhysicianId.ToString() : string.Empty, new { @id = "NewMedicationSnapshot_Physician", @class = "physician-picker" })%></div>
                </div>
                <div class="row">
                    <label for="NewMedicationSnapshot_PharmacyName" class="fl strong">Pharmacy Name</label>
                    <div class="fr"><%= Html.TextBox("PharmacyName", Model.Patient != null ? Model.Patient.PharmacyName : string.Empty, new { @id = "NewMedicationSnapshot_PharmacyName" })%></div>
                </div>
                <div class="row">
                    <label for="NewMedicationSnapshot_PharmacyPhone1" class="fl strong">Pharmacy Phone</label>
                    <div class="fr">
                        <%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 3 ? Model.PharmacyPhone.Substring(0, 3) : string.Empty, new { @id = "NewMedicationSnapshot_PharmacyPhone1", @class = "numeric phone-short", @maxlength = "3" })%>
                        -
                        <%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 6 ? Model.PharmacyPhone.Substring(3, 3) : string.Empty, new { @id = "NewMedicationSnapshot_PharmacyPhone2", @class = "numeric phone-short", @maxlength = "3" })%>
                        -
                        <%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 10 ? Model.PharmacyPhone.Substring(6, 4) : string.Empty, new { @id = "NewMedicationSnapshot_PharmacyPhone3", @class = "numerio phone-long", @maxlength = "4" })%>
                    </div>
                </div>
            </div>
            <div class="wide column">
                <div class="row ac">
                    <label for="NewMedicationSnapshot_Allergies" class="strong">Allergies</label>
                    <div><%= Html.TextArea("Allergies", Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : string.Empty, new { @id = "NewMedicationSnapshot_Allergies", @maxcharacters = "1000" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Medication(s)</h3></li>
            <li>
                <span class="med-ls">LS</span>
                <span class="med-startdate">Start Date</span>
                <span class="med-dosage">Medication &#38; Dosage</span>
                <span class="med-frequency">Frequency</span>
                <span class="med-route">Route</span>
                <span class="med-type">Type</span>
                <span class="med-classification">Classification</span>
            </li>
        </ul>
        <ol>
    <%  if (Model != null) { %>
        <%  var medications = Model.MedicationProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable); %>
        <%  foreach (var med in medications) { %>
            <li>
                <span class="med-ls"><%= string.Format("<input name='LongStanding' class='radio' disabled='true' type='checkbox' value='{0}' {1} />", med.Id, med.IsLongStanding ? "checked='checked'" : string.Empty) %></span>
                <span class="med-startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                <span class="med-dosage"><%= med.MedicationDosage %></span>
                <span class="med-frequency"><%= med.Frequency %></span>
                <span class="med-route"><%= med.Route %></span>
                <span class="med-type"><%= med.MedicationType.Text %></span>
                <span class="med-classification"><%= med.Classification %></span>
            </li>
        <%  } %>
    <%  } %>
        </ol>
    </div>
    <div class="note">
        <fieldset>
            <legend>Signature</legend>
            <div class="wide column">
                <div class="row">
                    <strong>Drug Regimen Review Acknowledgment:</strong>
                    I have reviewed all the listed medications for potential adverse effects, drug reactions, including ineffective
                    drug therapy, significant side effects, significant drug interactions, duplicate drug therapy, and noncompliance
                    with drug therapy.
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="NewMedicationSnapshot_Signature" class="fl strong">Clinician Signature</label>
                    <div class="fr"><%= Html.Password("Signature", "", new { @id = "NewMedicationSnapshot_Signature", @class = "required" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="NewMedicationSnapshot_SignedDate" class="fl strong">Date</label>
                    <div class="fr"><input type="text" name="SignedDate" id="NewMedicationSnapshot_SignedDate" class="date-picker required" /></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Sign</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>