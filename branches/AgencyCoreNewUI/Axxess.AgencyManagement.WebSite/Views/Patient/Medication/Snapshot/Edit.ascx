﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileHistory>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medication/Snapshot/Update", "Patient", FormMethod.Post, new { @id = "EditMedicationSnapshot_Form" })) { %>
    <%= Html.Hidden("id", Model.Id) %>
    <fieldset>
        <legend>Signature</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditMedicationSnapshot_SignedDate" class="fl strong">Date</label>
                <div class="fr"><%= Html.TextBox("signedDate", Model.SignedDate.IsValid() ? Model.SignedDate.ToShortDateString() : string.Empty, new { @id = "EditMedicationSnapshot_SignedDate", @class = "date-picker required" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>