<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medication/Create", "Patient", FormMethod.Post, new { @id = "NewMedication_Form" })) { %>
    <%= Html.Hidden("ProfileId", Model, new { @id = "NewMedication_ProfileId" })%>
    <%= Html.Hidden("LexiDrugId", "", new { @id = "NewMedication_DrugId" })%>   
    <fieldset>
        <legend>New Medication</legend>
        <div class="wide column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsLongStanding", false, new { @id = "NewMedication_IsLongStanding" })%>
                        <label for="NewMedication_IsLongStanding">Long Standing</label>
                    </div>
                </div>
            </div>
            <div id="NewMedication_StartDateRow" class="row">
                <label class="fl strong">Start Date</label>
                <div class="fr"><input type="text" class="date-picker" name="StartDate" id="NewMedication_StartDate" /></div>
            </div>
            <div class="row">
                <label for="NewMedication_MedicationDosage" class="fl strong">Medication &#38; Dosage</label>
                <div class="fr"><%= Html.TextBox("MedicationDosage", "", new { @id = "NewMedication_MedicationDosage", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="NewMedication_Classification" class="fl strong">Classification</label>
                <div class="fr"><%= Html.TextBox("Classification", "", new { @id = "NewMedication_Classification", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewMedication_Frequency" class="fl strong">Frequency</label>
                <div class="fr"><%= Html.TextBox("Frequency", "", new { @id = "NewMedication_Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewMedication_Route" class="fl strong">Route</label>
                <div class="fr"><%= Html.TextBox("Route", "", new { @id = "NewMedication_Route", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewMedication_Type" class="fl strong">Type</label>
                <div class="fr">
                    <% var medicationTypes = new SelectList(new[] { 
                           new SelectListItem { Text = "New", Value = "N" },
                           new SelectListItem { Text = "Changed", Value = "C" },               
                           new SelectListItem { Text = "Unchanged", Value = "U" }               
                        }, "Value", "Text"); %>
                    <%= Html.DropDownList("medicationType", medicationTypes)%>
                </div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save &#38; Exit</a></li>
            <li><a class="save clear">Save &#38; Add Another</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>