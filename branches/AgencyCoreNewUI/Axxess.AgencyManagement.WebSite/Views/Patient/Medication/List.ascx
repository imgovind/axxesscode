﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<%  if (Model != null) { %>
    <%  var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable); %>
    <%  if (medications != null && medications.Count() > 0) { %>
<ul>
    <li class="ac"><h3>Active Medication<%= medications.Count() > 1 ? "s" : string.Empty %></h3></li>
    <li>
        <span class="med-ls sortable">LS</span>
        <span class="med-startdate sortable">Start Date</span>
        <span class="med-dosage sortable">Medication &#38; Dosage</span>
        <span class="med-frequency sortable">Frequency</span>
        <span class="med-route sortable">Route</span>
        <span class="med-type sortable">Type</span>
        <span class="med-classification sortable">Classification</span>
        <span>Action</span>
    </li>
</ul>
<ol>
        <%  foreach (var med in medications) { %>
    <li>
        <span class="med-ls">
            <span class="sort-value"><%= med.IsLongStanding ? "0" : "1" %></span>
            <%= string.Format("<input disabled='disabled' type='checkbox' {0}/>", med.IsLongStanding.ToChecked())%>
        </span>
        <span class="med-startdate"><%= med.StartDate.IsValid() ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
        <span class="med-dosage"><%= med.MedicationDosage %></span>
        <span class="med-frequency"><%= med.Frequency %></span>
        <span class="med-route"><%= med.Route %></span>
        <span class="med-type"><%= med.MedicationType.Text %></span>
        <span class="med-classification"><%= med.Classification %></span>
        <span>
            <a class="link" onclick="Patient.Medication.Edit('<%= med.Id %>', '<%=Model.Id %>');return false">Edit</a>
            |
            <a class="link" onclick="Patient.Medication.Delete('<%= med.Id %>', '<%=Model.Id %>');return false">Delete</a>
            |
            <a class="link" onclick="Patient.Medication.Discontinue('<%= med.Id %>', '<%= Model.Id %>');return false">Discontinue</a>
        </span>
    </li>
        <%  } %>
</ol>
    <%  } else { %>
<ul>
    <li class="ac"><h3>Active Medications</h3></li>
</ul>
<h1 class="blue">No Active Medications</h1>
    <%  } %>
    <%  medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "DC").OrderByDescending(m => m.StartDateSortable); %>
    <%  if (medications != null && medications.Count() > 0) { %>
<ul>
    <li class="ac"><h3>Discontinued Medication<%= medications.Count() > 1 ? "s" : string.Empty %></h3></li>
    <li>
        <span class="med-ls sortable">LS</span>
        <span class="med-startdate sortable">Start Date</span>
        <span class="med-dcdosage sortable">Medication &#38; Dosage</span>
        <span class="med-dcfrequency sortable">Frequency</span>
        <span class="med-dcroute sortable">Route</span>
        <span class="med-type sortable">Type</span>
        <span class="med-dcclassification sortable">Classification</span>
        <span class="med-dischargedate sortable">D/C Date</span>
        <span>Action</span>
    </li>
</ul>
<ol class="dc">
        <%  foreach (var med in medications) { %>
    <li>
        <span class="med-ls">
            <span class="sort-value"><%= med.IsLongStanding ? "0" : "1" %></span>
            <%= string.Format("<input disabled='disabled' type='checkbox' {0}/>", med.IsLongStanding.ToChecked())%>
        </span>
        <span class="med-startdate"><%= med.StartDate.IsValid() ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
        <span class="med-dcdosage"><%= med.MedicationDosage %></span>
        <span class="med-dcfrequency"><%= med.Frequency %></span>
        <span class="med-dcroute"><%= med.Route %></span>
        <span class="med-type"><%= med.MedicationType.Text %></span>
        <span class="med-dcclassification"><%= med.Classification %></span>
        <span class="med-dischargedate"><%= med.DCDateFormated %></span>
        <span>
            <a class="link" onclick="Patient.Medication.Edit('<%= med.Id %>', '<%=Model.Id %>');return false">Edit</a>
            |
            <a class="link" onclick="Patient.Medication.Delete('<%= med.Id %>', '<%=Model.Id %>');return false">Delete</a>
            |
            <a class="link" onclick="Patient.Medication.Activate('<%= med.Id %>', '<%= Model.Id %>');return false">Activate</a>
        </span>
    </li>
        <%  } %>
</ol>
    <%  } else { %>
<ul>
    <li class="ac"><h3>Discontinued Medications</h3></li>
</ul>
<h1 class="blue">No Discontinued Medications</h1>
    <%  } %>
<%  } %>