﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Medication/UpdateStatus", "Patient", FormMethod.Post, new { @id = "DiscontinueMedication_Form" }))
    { %>
    <%= Html.Hidden("id", Model.Id, new { @id = "DiscontinueMedication_Id" })%>
    <%= Html.Hidden("profileId", Model.ProfileId, new { @id = "DiscontinueMedication_ProfileId" })%>
    <%= Html.Hidden("medicationCategory", "DC", new { @id = "DiscontinueMedication_Category" })%>
    <fieldset>
        <legend>Discontinue Medication</legend>
        <div class="wide column">
            <div class="row">
                <label class="fl strong">Medication</label>
                <div class="fr"><%= Model.MedicationDosage%></div>
            </div>
            <div class="row">
                <label for="DiscontinueMedication_Date" class="fl strong">D/C date</label>
                <div class="fr"><input type="text" class="date-picker required" name="dischargeDate" id="DiscontinueMedication_Date" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Discontinue</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
