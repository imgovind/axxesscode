﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<div class="wrapper main">
<%= Html.Hidden("Id", Model.MedicationProfile.Id) %>
<%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId) %>
    <div class="note">
        <fieldset>
            <legend>Patient Information</legend>
            <div class="column">
                <div class="row">
                    <label class="fl strong">Patient</label>
                    <div class="fr"><%= Model.Patient.DisplayNameWithMi %></div>
                </div>
                <div class="row">
                    <label class="fl strong">MRN</label>
                    <div class="fr"><%= Model.Patient.PatientIdNumber %></div>
                </div>
                <div class="row">
                    <label class="fl strong">Episode</label>
                    <div class="fr">
<%  if (Model.StartDate.IsValid() && Model.EndDate.IsValid()) { %>
                        <%= Model.StartDate.ToShortDateString() %>
                        &#8211;
                        <%= Model.EndDate.ToShortDateString() %>
<%  } else { %>
                        No current episode
<%  } %>
                    </div>
                </div>
                <div class="row">
                    <label class="fl strong">Allergies</label>
                    <div class="fr"><%= Model.Allergies %></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label class="fl strong">Primary Diagnosis</label>
                    <div class="fr"><%= data.AnswerOrEmptyString("M1020PrimaryDiagnosis")%></div>
                </div>
                <div class="row">
                    <label class="fl strong">Secondary Diagnosis</label>
                    <div class="fr"><%= data.AnswerOrEmptyString("M1022PrimaryDiagnosis1")%></div>
                </div>
                <div class="row">
                    <label class="fl strong">Pharmacy</label>
                    <div class="fr"><%= Model.Patient.PharmacyName%></div>
                </div>
                <div class="row">
                    <label class="fl strong">Pharmacy Phone</label>
                    <div class="fr"><%= Model.Patient.PharmacyPhone.ToPhone()%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="buttons">
        <ul class="fl">
            <li><a class="new-medication">Add Medication</a></li>
            <li><a class="sign-profile">Sign Medication Profile</a></li>
            <li><a class="print">Print Medication Profile</a></li>
        </ul>
        <ul class="fr">
            <li class="red"><a class="drug-interactions">Drug Interactions</a></li>
            <li><a class="profile-history">Signed Medication Profiles</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="med-profile acore-grid"><% Html.RenderPartial("Medication/List", Model.MedicationProfile); %></div>
    <div class="buttons">
        <ul>
            <li><a class="grid-refresh">Refresh Medications</a></li>
        </ul>
    </div>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
</div>