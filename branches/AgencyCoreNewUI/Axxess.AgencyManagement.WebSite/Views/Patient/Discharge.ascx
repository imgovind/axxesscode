﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("SaveDischarge", "Patient", FormMethod.Post, new { @id = "DischargePatient_Form" }))%><%  { %>
<div class="wrapper main">
    
    <%= Html.Hidden("PatientId", Model)%>
    <fieldset>
            <legend>Patient Discharge Information</legend>
            <div class="column"><div class="row"><label for="DischargeDate" class="float-left">&#160;&#160;Date of discharge</label><div class="fr"><input type="text" class="date-picker required" name="DischargeDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" /></div></div></div>
            <div class="clear"></div>
            <div class="wide column" ><label for="Comment" class="float-left">Reason for discharge</label><div class="row"><%= Html.TextArea("Comment", "", new { @class = "required" })%></div></div>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Discharge</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<% } %>