﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Authorization | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Authorization/Create", "Patient", FormMethod.Post, new { @id = "NewAuthorization_Form" })) { %>
    <fieldset>
        <legend>Detail</legend>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_PatientName" class="fl strong">Patient Name</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "NewAuthorization_PatientName", @class="required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_StartDate" class="fl strong">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartDate" id="NewAuthorization_StartDate" /></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_EndDate" class="fl strong">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" id="NewAuthorization_EndDate" /></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_LocationId" class="fl strong">Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "NewAuthorization_LocationId" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AuthorizationStatus, "Status", "", new { @id = "NewAuthorization_Status" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_Insurance" class="fl strong">Insurance</label>
                <div class="fr"><%= Html.Insurances("Insurance", "", true, new { @id = "NewAuthorization_Insurance", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_AuthNumber1" class="fl strong">Authorization Number 1</label>
                <div class="fr"><%= Html.TextBox("Number1", "", new { @id = "NewAuthorization_AuthNumber1", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_AuthNumber2" class="fl strong">Authorization Number 2</label>
                <div class="fr"><%= Html.TextBox("Number2", "", new { @id = "NewAuthorization_AuthNumber2", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="NewAuthorization_AuthNumber3" class="fl strong">Authorization Number 3</label>
                <div class="fr"><%= Html.TextBox("Number3", "", new { @id = "NewAuthorization_AuthNumber3", @maxlength = "30" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visits or Hours Authorized</legend>
    <%  var countVisitType = new SelectList(new[] {
            new SelectListItem { Text = "Visits", Value = "1" },
            new SelectListItem { Text = "Hours", Value = "2" }
        }, "Value", "Text", "1"); %>
    <%  var countHourType = new SelectList(new[] {
            new SelectListItem { Text = "Visits", Value = "1" },
            new SelectListItem { Text = "Hours", Value = "2" }
        }, "Value", "Text", "2"); %>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_SNVisit" class="fl strong">SN Count</label>
                <div class="fr">
                    <%= Html.TextBox("SNVisit", "", new { @id = "NewAuthorization_SNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("SNVisitCountType", countVisitType, new { @id = "NewAuthorization_SNVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_PTVisit" class="fl strong">PT Count</label>
                <div class="fr">
                    <%= Html.TextBox("PTVisit", "", new { @id = "NewAuthorization_PTVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("PTVisitCountType", countVisitType, new { @id = "NewAuthorization_PTVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_OTVisit" class="fl strong">OT Count</label>
                <div class="fr">
                    <%= Html.TextBox("OTVisit", "", new { @id = "NewAuthorization_OTVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("OTVisitCountType", countVisitType, new { @id = "NewAuthorization_OTVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_STVisit" class="fl strong">ST Count</label>
                <div class="fr">
                    <%= Html.TextBox("STVisit", "", new { @id = "NewAuthorization_STVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("STVisitCountType", countVisitType, new { @id = "NewAuthorization_STVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_MSWVisit" class="fl strong">MSW Count</label>
                <div class="fr">
                    <%= Html.TextBox("MSWVisit", "", new { @id = "NewAuthorization_MSWVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("MSWVisitCountType", countVisitType, new { @id = "NewAuthorization_MSWVisitCountType", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewAuthorization_HHAVisit" class="fl strong">HHA Count</label>
                <div class="fr">
                    <%= Html.TextBox("HHAVisit", "", new { @id = "NewAuthorization_HHAVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("HHAVisitCountType", countVisitType, new { @id = "NewAuthorization_HHAVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_DieticianVisit" class="fl strong">Dietician Count</label>
                <div class="fr">
                    <%= Html.TextBox("DieticianVisit", "", new { @id = "NewAuthorization_DieticianVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("DieticianVisitCountType", countVisitType, new { @id = "NewAuthorization_DieticianVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_RNVisit" class="fl strong">RN Count</label>
                <div class="fr">
                    <%= Html.TextBox("RNVisit", "", new { @id = "NewAuthorization_RNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("RNVisitCountType", countHourType, new { @id = "NewAuthorization_RNVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewAuthorization_LVNVisit" class="fl strong">LVN Count</label>
                <div class="fr">
                    <%= Html.TextBox("LVNVisit", "", new { @id = "NewAuthorization_LVNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%= Html.DropDownList("LVNVisitCountType", countHourType, new { @id = "NewAuthorization_LVNVisitCountType", @class = "shorter" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row">
                <textarea id="NewAuthorization_Comments" name="Comments" class="tall" maxcharacters="500"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>