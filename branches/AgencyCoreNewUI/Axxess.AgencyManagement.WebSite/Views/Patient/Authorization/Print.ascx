﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Authorization>" %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("printpreview.css").Add("Print/Patient/authorization.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
<script type="text/javascript">
    PrintPreview.Fields = {
        agency:         "<%= Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br/>" : String.Empty) + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br/>" : "<br/>") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) + (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br/>Phone: " + location.PhoneWorkFormatted : String.Empty) + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty) : String.Empty) : String.Empty %>",
        patientname:    "<%= Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br/>" : "<br/>") : string.Empty %>",
        mr:             "<%= Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber : string.Empty %>",
        dob:            "<%= Model.Patient != null && Model.Patient.DOBFormatted.IsNotNullOrEmpty() ? Model.Patient.DOBFormatted : string.Empty %>",
        start:          "<%= Model != null && Model.StartDateFormatted.IsNotNullOrEmpty() ? Model.StartDateFormatted : string.Empty %>",
        end:            "<%= Model != null && Model.EndDateFormatted.IsNotNullOrEmpty() ? Model.EndDateFormatted : string.Empty %>",
        branch:         "<%= Model != null && Model.Branch.IsNotNullOrEmpty() ? Model.Branch : string.Empty %>",
        status:         "<%= Model != null && Model.Status.IsNotNullOrEmpty() ? Model.Status : string.Empty %>",
        insurance:      "<%= Model != null && Model.Insurance.IsNotNullOrEmpty() ? Model.Insurance : string.Empty %>",
        auth1:          "<%= Model != null && Model.Number1.IsNotNullOrEmpty() ? Model.Number1 : string.Empty %>",
        auth2:          "<%= Model != null && Model.Number2.IsNotNullOrEmpty() ? Model.Number2 : string.Empty %>",
        auth3:          "<%= Model != null && Model.Number3.IsNotNullOrEmpty() ? Model.Number3 : string.Empty %>",
        sn:             "<%= Model != null && Model.SNVisit.IsNotNullOrEmpty() ? Model.SNVisit + " " + (Model.SNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.SNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        pt:             "<%= Model != null && Model.PTVisit.IsNotNullOrEmpty() ? Model.PTVisit + " " + (Model.PTVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.PTVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        ot:             "<%= Model != null && Model.OTVisit.IsNotNullOrEmpty() ? Model.OTVisit + " " + (Model.OTVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.OTVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        st:             "<%= Model != null && Model.STVisit.IsNotNullOrEmpty() ? Model.STVisit + " " + (Model.STVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.STVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        msw:            "<%= Model != null && Model.MSWVisit.IsNotNullOrEmpty() ? Model.MSWVisit + " " + (Model.MSWVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.MSWVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        hha:            "<%= Model != null && Model.HHAVisit.IsNotNullOrEmpty() ? Model.HHAVisit + " " + (Model.HHAVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.HHAVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        dietician:      "<%= Model != null && Model.DieticianVisit.IsNotNullOrEmpty() ? Model.DieticianVisit + " " + (Model.DieticianVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.DieticianVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        rn:             "<%= Model != null && Model.RNVisit.IsNotNullOrEmpty() ? Model.RNVisit + " " + (Model.RNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.RNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>",
        lvn:            "<%= Model != null && Model.LVNVisit.IsNotNullOrEmpty() ? Model.LVNVisit + " " + (Model.LVNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (Model.LVNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty %>"
    };
    PrintPreview.BuildBasic("<%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments.Clean() : string.Empty %>");
</script>