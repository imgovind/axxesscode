﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Authorization List | <%= Model.DisplayName %></span>
<div class="wrapper main blue">
    <%= Html.Hidden("PatientId", Model.Id)%>  
    <div class="fr buttons">
        <ul><li><a class="new-authorization">New Authorization</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%  var print = "<a onclick=\"Patient.Authorization.Print('<#=Id#>','" + Model.Id + "');return false\"><span class='img icon print'></span></a>"; %>
    <%= Html.Telerik().Grid<Authorization>().Name("List_Authorizations").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(a => a.Number1).Title("Authorization Number 1").Width(150).Sortable(false);
            columns.Bound(a => a.Number2).Title("Authorization Number 2").Width(150).Sortable(false);
            columns.Bound(a => a.Number3).Title("Authorization Number 3").Width(150).Sortable(false);
            columns.Bound(a => a.Branch).Width(200).Sortable(false);
            columns.Bound(a => a.StartDateFormatted).Title("Start Date").Width(120).Sortable(false);
            columns.Bound(a => a.EndDateFormatted).Title("End Date").Width(120).Sortable(false);
            columns.Bound(a => a.Status).Sortable(false);
            columns.Bound(a => a.Id).ClientTemplate(print).Title(" ").Sortable(false).Width(30);
            columns.Bound(a => a.Url).ClientTemplate("<#=Url#>").Title("Action").Sortable(false).Width(150);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("AuthorizationGrid", "Patient", new { patientId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>