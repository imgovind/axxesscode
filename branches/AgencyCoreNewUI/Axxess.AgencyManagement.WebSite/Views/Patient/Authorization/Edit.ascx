﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Authorization>" %>
<span class="wintitle">Edit Authorization | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Authorization/Update", "Patient", FormMethod.Post, new { @id = "EditAuthorization_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditAuthorization_Id" })%> 
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditAuthorization_PatientId" })%>     
    <fieldset>
        <legend>Detail</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient Name</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_StartDate" class="fl strong">Start Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" id="EditAuthorization_StartDate" /></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_EndDate" class="fl strong">End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate.ToShortDateString() %>" id="EditAuthorization_EndDate" /></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_LocationId" class="fl strong">Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "EditAuthorization_LocationId" })%></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AuthorizationStatus, "Status", Model.Status, new { @id = "EditAuthorization_Status" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditAuthorization_Insurance" class="fl strong">Insurance</label>
                <div class="fr"><%= Html.Insurances("Insurance", Model.Insurance, true, new { @id = "EditAuthorization_Insurance", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_AuthNumber1" class="fl strong">Authorization Number 1</label>
                <div class="fr"><%= Html.TextBox("Number1", Model.Number1, new { @id = "EditAuthorization_AuthNumber1", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_AuthNumber2" class="fl strong">Authorization Number 2</label>
                <div class="fr"><%= Html.TextBox("Number2", Model.Number2, new { @id = "EditAuthorization_AuthNumber2", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditAuthorization_AuthNumber3" class="fl strong">Authorization Number 3</label>
                <div class="fr"><%= Html.TextBox("Number3", Model.Number3, new { @id = "EditAuthorization_AuthNumber3", @maxlength = "30" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visits or Hours Authorized</legend>
        <div class="column">
            <div class="row">
                <label for="EditAuthorization_SNVisit" class="fl strong">SN Count</label>
                <div class="fr">
                    <%= Html.TextBox("SNVisit", Model.SNVisit, new { @id = "EditAuthorization_SNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeSN = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.SNVisitCountType); %>
                    <%= Html.DropDownList("SNVisitCountType", countVisitTypeSN, new { @id = "EditAuthorization_SNVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_PTVisit" class="fl strong">PT Count</label>
                <div class="fr">
                    <%= Html.TextBox("PTVisit", Model.PTVisit, new { @id = "EditAuthorization_PTVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypePT = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.PTVisitCountType); %>
                    <%= Html.DropDownList("PTVisitCountType", countVisitTypePT, new { @id = "EditAuthorization_PTVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_OTVisit" class="fl strong">OT Count</label>
                <div class="fr">
                    <%= Html.TextBox("OTVisit", Model.OTVisit, new { @id = "EditAuthorization_OTVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeOT = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.OTVisitCountType); %>
                    <%= Html.DropDownList("OTVisitCountType", countVisitTypeOT, new { @id = "EditAuthorization_OTVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_STVisit" class="fl strong">ST Count</label>
                <div class="fr">
                    <%= Html.TextBox("STVisit", Model.STVisit, new { @id = "EditAuthorization_STVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeST = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.SNVisitCountType); %>
                    <%= Html.DropDownList("STVisitCountType", countVisitTypeST, new { @id = "EditAuthorization_STVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_MSWVisit" class="fl strong">MSW Count</label>
                <div class="fr">
                    <%= Html.TextBox("MSWVisit", Model.MSWVisit, new { @id = "EditAuthorization_MSWVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeMSW = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.MSWVisitCountType); %>
                    <%= Html.DropDownList("MSWVisitCountType", countVisitTypeMSW, new { @id = "EditAuthorization_MSWVisitCountType", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditAuthorization_HHAVisit" class="fl strong">HHA Count</label>
                <div class="fr">
                    <%= Html.TextBox("HHAVisit", Model.HHAVisit, new { @id = "EditAuthorization_HHAVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeHHA = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.HHAVisitCountType); %>
                    <%= Html.DropDownList("HHAVisitCountType", countVisitTypeHHA, new { @id = "EditAuthorization_HHAVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_DieticianVisit" class="fl strong">Dietician Count</label>
                <div class="fr">
                    <%= Html.TextBox("DieticianVisit", Model.DieticianVisit, new { @id = "EditAuthorization_DieticianVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeDietician = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.DieticianVisitCountType); %>
                    <%= Html.DropDownList("DieticianVisitCountType", countVisitTypeDietician, new { @id = "EditAuthorization_DieticianVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_RNVisit" class="fl strong">RN Count</label>
                <div class="fr">
                    <%= Html.TextBox("RNVisit", Model.RNVisit, new { @id = "EditAuthorization_RNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeRN = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.RNVisitCountType); %>
                    <%= Html.DropDownList("RNVisitCountType", countVisitTypeRN, new { @id = "EditAuthorization_RNVisitCountType", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAuthorization_LVNVisit" class="fl strong">LVN Count</label>
                <div class="fr">
                    <%= Html.TextBox("LVNVisit", Model.LVNVisit, new { @id = "EditAuthorization_LVNVisit", @class = "numeric shorter", @maxlength = "4" })%>
                    <%  var countVisitTypeLVN = new SelectList(new[] {
                            new SelectListItem { Text = "Visits", Value = "1" },
                            new SelectListItem { Text = "Hours", Value = "2" }
                        }, "Value", "Text", Model.LVNVisitCountType); %>
                    <%= Html.DropDownList("LVNVisitCountType", countVisitTypeLVN, new { @id = "EditAuthorization_LVNVisitCountType", @class = "shorter" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row"><%= Html.TextArea("Comments", Model.Comments, 6, 5, new { @id = "EditAuthorization_Comments", @class = "tall", @maxcharacters = "500" })%></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>