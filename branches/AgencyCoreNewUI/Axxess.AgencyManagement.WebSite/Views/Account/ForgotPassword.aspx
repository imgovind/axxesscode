﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AgencyCore Reset Password</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion)) %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="shade"></div>
    <div class="login-promt forgot-password">
        <%  using (Html.BeginForm("ForgotPassword", "Account", FormMethod.Post, new { @id = "ForgotPasswordForm", @class = "forgotPassword" })) { %>
        <h3>Reset Password</h3>
        <div class="row">
            <label for="forgot-user" class="fl strong">Email</label>
            <%= Html.TextBoxFor(a => a.EmailAddress, new { @id = "forgot-user", @class = "fr" })%>
        </div>
        <div class="row">
            <label for="recaptcha_response_field" class="strong">Security Check</label>
            <br />
            <em>Enter both words below, separated by a space.</em>
            <%= Html.GenerateCaptcha()%>
          </div>
        <div class="clear"></div>
        <div class="buttons">
            <ul>
                <li><a class="send">Send</a></li>
                <li><a class="cancel">Cancel</a></li>
            </ul>
        </div>
        <%  } %>
    </div>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Notification.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Account.InitForgotPassword();
    <%  }).Render(); %>
</body>
</html>