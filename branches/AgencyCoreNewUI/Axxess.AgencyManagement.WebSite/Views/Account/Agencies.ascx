﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyLite>>" %>
<% if (Model != null) { %>
<div id="agency-selection-links">
    <h3>Select Agency Profile</h3>
    <ul>
    <% foreach(var agency in Model) { %>
        <li onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')" onclick="Account.Select('<%= agency.Id %>', '<%= agency.UserId %>')">
            <%= agency.Name%>
            <em>Your title is <%= agency.Title %>.</em>
            <span>Profile Created on <%= agency.Date %></span>
        </li> 
    <% } %>
    </ul> 
</div>
<% } %>