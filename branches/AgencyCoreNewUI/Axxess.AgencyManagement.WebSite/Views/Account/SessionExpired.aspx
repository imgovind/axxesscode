﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AgencyCore Session Expired</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion)) %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="shade"></div>
    <div class="login-promt">
        <h3>Session Expired</h3>
        <p>You have been logged off of the server.  This can be caused by a lapse in activity, or by your account being used on a different computer or different web browser.</p>
        <div class="buttons">
            <ul>
                <li><a class="login">OK</a></li>
            </ul>
        </div>
    </div>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Notification.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Account.InitExpire();
    <%  }).Render(); %>
</body>
</html>