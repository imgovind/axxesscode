﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>AgencyCore Change Account</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion)) %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="shade"></div>
    <div class="login-promt">
<%  if (Model.LoginId != Guid.Empty) { %>
    <%  using (Html.BeginForm("Reset", "Account", FormMethod.Post, new { @id = "ResetForm", @class = "change-password" })) { %>
        <%= Html.Hidden("LoginId", Model.LoginId, new { @id = "ChangePassword_LoginId" })%>
        <%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "ChangePassword_EmailAddress" })%>
        <%= Html.Hidden("ResetType", Model.ResetType.ToString(), new { @id = "ChangePassword_ResetType" })%>
        <h3>Reset <%= Model.ResetType.ToString() %></h3>
        <%  if (Model.ResetType == Change.Password) { %>
        <div class="row">
            <label for="Password" class="fl strong">New Password</label>
            <div class="fr"><%= Html.PasswordFor(a => a.Password, new { @class = "required", @maxlength="15" })%></div>
        </div>
        <%  } else { %>
        <div class="row">
            <label for="Signature" class="fl strong">New Signature</label>
            <div class="fr"><%= Html.PasswordFor(a => a.Signature, new { @class = "required", @maxlength="15" })%></div>
        </div>
        <%  } %>
        <div class="buttons">
            <ul>
                <li><a class="submit">Change</a></li>
            </ul>
        </div>
    <%  } %>
<%  } else { %>
        <h3>Expired Request</h3>
        <p>The reset request you are using has expired.  Please select again to reset your password/signature and get a new request</p>
        <div class="buttons">
            <ul>
                <li><a class="login">OK</a></li>
            </ul>
        </div>
<%  } %>
    </div>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Add("System/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Notification.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Account.InitReset();
    <%  }).Render(); %>
</body>
</html>