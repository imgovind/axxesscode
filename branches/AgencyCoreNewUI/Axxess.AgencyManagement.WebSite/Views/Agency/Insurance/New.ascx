﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Insurance/Payor | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Insurance/Create", "Agency", FormMethod.Post, new { @id = "NewInsurance_Form" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_Name" class="fl strong">Insurance/Payor Name</label>
                <div class="fr"><%= Html.TextBox("Name", "", new { @id = "NewInsurance_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_PayorType" class="fl strong">Payor Type</label>
                <div class="fr"><%= Html.PaymentSourceWithOutMedicareTradition("PayorType", "", new { @id = "NewInsurance_PayorType", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_InvoiceType" class="fl strong">Invoice Type</label>
                <div class="fr">
                    <%  var invoiceType = new SelectList(new[] {
                            new SelectListItem { Text = "UB-04", Value = "1" },
                            new SelectListItem { Text = "HCFA 1500", Value = "2" },
                            new SelectListItem { Text = "Invoice", Value = "3" }
                        }, "Value", "Text", "1"); %>
                    <%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "NewInsurance_InvoiceType" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewInsurance_BillType" class="fl strong">Bill Type</label>
                <div class="fr">
                    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Institutional", Value = "institutional" },
                            new SelectListItem { Text = "Professional", Value = "professional" }
                        }, "Value", "Text", "institutional"); %>
                    <%= Html.DropDownList("BillType", billType, new { @id = "NewInsurance_BillType" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_PayorId" class="fl strong">Payor ID</label>
                <div class="fr"><%= Html.TextBox("PayorId", "", new { @id = "NewInsurance_PayorId", @class = "required", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ProviderId" class="fl strong">Provider ID/Code</label>
                <div class="fr"><%= Html.TextBox("ProviderId", "", new { @id = "NewInsurance_ProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_OtherProviderId" class="fl strong">Other Provider ID</label>
                <div class="fr"><%= Html.TextBox("OtherProviderId", "", new { @id = "NewInsurance_OtherProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ProviderSubscriberId" class="fl strong">Provider Subscriber ID</label>
                <div class="fr"><%= Html.TextBox("ProviderSubscriberId", "", new { @id = "NewInsurance_ProviderSubscriberId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_SubmitterId" class="fl strong">Submitter ID</label>
                <div class="fr"><%= Html.TextBox("SubmitterId", "", new { @id = "NewInsurance_SubmitterId", @maxlength = "40" })%></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="narrow row">
                <label for="NewInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCa</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <div class="fr">
                    <%= Html.TextBox("Locator1_Code1", "", new { @id = "NewInsurance_Locator1_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator1_Code2", "", new { @id = "NewInsurance_Locator1_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator1_Code3", "", new { @id = "NewInsurance_Locator1_Code3", @maxlength = "12" })%>
                </div>
            </div>
            <div class="narrow row">
                <label for="NewInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCb</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator2")%>
                <div class="fr">
                    <%= Html.TextBox("Locator2_Code1", "", new { @id = "NewInsurance_Locator2_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator2_Code2", "", new { @id = "NewInsurance_Locator2_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator2_Code3", "", new { @id = "NewInsurance_Locator2_Code3", @maxlength = "12" })%>
                </div>
            </div>
            <div class="narrow row">
                <label for="NewInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCc</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator3")%>
                <div class="fr">
                    <%= Html.TextBox("Locator3_Code1", "", new { @id = "NewInsurance_Locator3_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator3_Code2", "", new { @id = "NewInsurance_Locator3_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator3_Code3", "", new { @id = "NewInsurance_Locator3_Code3", @maxlength = "12" })%>
                </div>
            </div>
            <div class="narrow row">
                <label for="NewInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCd</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator4")%>
                <div class="fr">
                    <%= Html.TextBox("Locator4_Code1", "", new { @id = "NewInsurance_Locator4_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator4_Code2", "", new { @id = "NewInsurance_Locator4_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator4_Code3", "", new { @id = "NewInsurance_Locator4_Code3", @maxlength = "12" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessTheBiller", false, new { @id = "NewInsurance_IsAxxessTheBiller" })%>
                        <label for="NewInsurance_IsAxxessTheBiller">Electronically Submit Claims to Clearing House through Axxess&#8482;</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_ClearingHouse" class="fl strong">Clearing House</label>
                <div class="fr">
                    <%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                        }, "Value", "Text","0"); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "NewInsurance_ClearingHouse" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="NewInsurance_EdiInformation">
            <div class="column">
                <div class="row">
                    <label for="NewInsurance_InterchangeReceiverId" class="fl strong">Interchange Receiver ID</label>
                    <div class="fr">
                        <%  var interchangeReceiverId = new SelectList(new[] {
                                new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                                new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                                new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                                new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                                new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                                new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                                new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                                new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                                new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                            }, "Value", "Text","28"); %>
                        <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "NewInsurance_InterchangeReceiverId" })%>
                    </div>
                </div>
                <div class="row">
                    <label for="NewInsurance_ClearingHouseSubmitterId" class="fl strong">Clearing House Submitter ID</label>
                    <div class="fr"><%= Html.TextBox("ClearingHouseSubmitterId", "", new { @id = "NewInsurance_ClearingHouseSubmitterId", @maxlength = "40" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="NewInsurance_SubmitterName" class="fl strong">Submitter Name</label>
                    <div class="fr"><%= Html.TextBox("SubmitterName", "", new { @id = "NewInsurance_SubmitterName", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="NewInsurance_SubmitterPhone1" class="fl strong">Submitter Phone</label>
                    <div class="fr">
                        <input type="text" class="numeric phone-short" name="SubmitterPhoneArray" id="NewInsurance_SubmitterPhone1" maxlength="3" />
                        -
                        <input type="text" class="numeric phone-short" name="SubmitterPhoneArray" id="NewInsurance_SubmitterPhone2" maxlength="3" />
                        -
                        <input type="text" class="numeric phone-long" name="SubmitterPhoneArray" id="NewInsurance_SubmitterPhone3" maxlength="4" />
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "NewInsurance_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "NewInsurance_AddressLine2", @maxlength = "75" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "NewInsurance_AddressCity", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "NewInsurance_AddressStateCode", @class = "state" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "NewInsurance_AddressZipCode", @class = "numeric zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_ContactPersonFirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", "", new { @id = "NewInsurance_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ContactPersonLastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", "", new { @id = "NewInsurance_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_ContactEmailAddress" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("ContactEmailAddress", "", new { @id = "NewInsurance_ContactEmailAddress", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_PhoneNumberArray1" class="fl strong">Phone</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short"name="PhoneNumberArray" id="NewInsurance_PhoneNumberArray1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="PhoneNumberArray" id="NewInsurance_PhoneNumberArray2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="PhoneNumberArray" id="NewInsurance_PhoneNumberArray3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewInsurance_FaxNumberArray1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewInsurance_FaxNumberArray1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewInsurance_FaxNumberArray2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="FaxNumberArray" id="NewInsurance_FaxNumberArray3" maxlength="4" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInsurance_CurrentBalance" class="fl strong">Current Balance</label>
                <div class="fr"><%= Html.TextBox("CurrentBalance", "", new { @id = "NewInsurance_CurrentBalance" })%></div>
            </div>
            <div class="row">
                <label for="NewInsurance_WorkWeekStartDay" class="fl strong">Work Week Begins</label>
                <div class="fr">
                    <%  var workWeekStartDay = new SelectList(new[] {
                            new SelectListItem { Text = "Sunday", Value = "1" },
                            new SelectListItem { Text = "Monday", Value = "2" }
                        }, "Value", "Text"); %>
                    <%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "NewInsurance_WorkWeekStartDay" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Visit Authorization Required</label>
                <div class="fr radio">
                    <%= Html.RadioButton("IsVisitAuthorizationRequired", "1", new { @id = "NewInsurance_IsVisitAuthorizationRequired1" })%>
                    <label for="NewInsurance_IsVisitAuthorizationRequired1" class="fixed short">Yes</label>
                    <%= Html.RadioButton("IsVisitAuthorizationRequired", "0", true, new { @id = "NewInsurance_IsVisitAuthorizationRequired0" })%>
                    <label for="NewInsurance_IsVisitAuthorizationRequired0" class="fixed short">No</label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Load Visit Information from existing Insurance</legend>
         <div class="column">
            <div class="row">
               <label for="NewInsurance_OldInsuranceId" class="fl strong">Choose existing Insurance</label>
               <div class="fr"><%= Html.InsurancesNoneMedicare("OldInsuranceId", "0", true, "-- Select Insurance --", new { @id = "NewInsurance_OldInsuranceId" })%></div>
            </div>
         </div>
     </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>