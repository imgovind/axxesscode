﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Insurance | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-insurance">New Insurance/Payor</a></li></ul>
        <%--
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
        --%>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<InsuranceLean>().Name("List_AgencyInsurance").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
        columns.Bound(c => c.Name).Title("Insurance Name").Sortable(true);
        columns.Bound(c => c.PayerTypeName).Title("Payer Type").Sortable(true);
        columns.Bound(c => c.PayorId).Title("Payor Id").Width(150).Sortable(true);
        columns.Bound(c => c.InvoiceTypeName).Title("Invoice Type").Width(110).Sortable(true);
        columns.Bound(c => c.PhoneNumber).Title("Phone").Width(120).Sortable(false);
        columns.Bound(c => c.ContactPerson).Title("Contact Person").Width(180).Sortable(false);
        columns.Bound(c => c.Action).Title("Action").Sortable(false).Width(120);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Insurance/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>