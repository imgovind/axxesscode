﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">Edit Visit Rates | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Insurance/VisitRates/Update", "Agency", FormMethod.Post, new { @id = "EditVisitRate_Form" })){ %>
    <fieldset>
        <legend>Location/Branch</legend>
        <div class="column">
            <div class="row">
                <label for="EditVisitRate_LocationId" class="fl strong">Agency Branch</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.Id.ToString(), new { @id = "EditVisitRate_LocationId", @class = "required notzero" }) %></div>
            </div>
        </div>
    </fieldset>
    <div id="EditVisitRate_Content"><% Html.RenderPartial("Insurance/VisitRateContent", Model); %></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>