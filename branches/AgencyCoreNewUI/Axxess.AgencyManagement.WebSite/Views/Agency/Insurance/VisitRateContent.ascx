﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  var data = Model != null ? Model.ToCostRateDictionary() : new Dictionary<string, CostRate>(); %>
<%  var taxonomyCodes = Model != null ? Model.Ub04Locator81cca.ToUb04Locator81Dictionary() : new Dictionary<string, Locator>(); %>
<fieldset>
    <legend>Insurance Information</legend>
    <div class="column">
        <div class="row">
            <label class="fl strong">Payor</label>
            <div class="fr"><%= Model.InsuranceName %></div>
        </div>
        <div class="row">
            <label class="fl strong">Submitter Name</label>
            <div class="fr"><%= Model.SubmitterName %></div>
        </div>
        <div class="row">
            <label class="fl strong">Contact Person Name</label>
            <div class="fr"><%= string.Format("{0}, {1}", Model.ContactPersonLastName, Model.ContactPersonFirstName) %></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl strong">Submitter Id</label>
            <div class="fr"><%= Model.SubmitterId %></div>
        </div>
        <div class="row">
            <label class="fl strong">Submitter Phone</label>
            <div class="fr"><%= Model.SubmitterPhone.ToPhone() %></div>
        </div>
        <div class="row">
            <label class="fl strong">Contact Person Phone</label>
            <div class="fr"><%= Model.ContactPersonPhone.ToPhone() %></div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="wide column">
        <div class="narrow row">
            <label for="EditVisitRate_Locator1_Code1" class="fl strong">UB04 Locator 81CCa</label>
            <%= Html.Hidden("Ub04Locator81", "Locator1")%>
            <div class="fr">
                <%= Html.TextBox("Locator1_Code1", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code1 : string.Empty, new { @id = "EditVisitRate_Locator1_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator1_Code2", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code2 : string.Empty, new { @id = "EditVisitRate_Locator1_Code2", @maxlength = "10" })%>
                <%= Html.TextBox("Locator1_Code3", taxonomyCodes.ContainsKey("Locator1") ? taxonomyCodes["Locator1"].Code3 : string.Empty, new { @id = "EditVisitRate_Locator1_Code3", @maxlength = "12" })%>
            </div>
        </div>
        <div class="narrow row">
            <label for="EditVisitRate_Locator2_Code1" class="fl strong">UB04 Locator 81CCb</label>
            <%= Html.Hidden("Ub04Locator81", "Locator2")%>
            <div class="fr">
                <%= Html.TextBox("Locator2_Code1", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code1 : string.Empty, new { @id = "EditVisitRate_Locator2_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator2_Code2", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code2 : string.Empty, new { @id = "EditVisitRate_Locator2_Code2", @maxlength = "10" })%>
                <%= Html.TextBox("Locator2_Code3", taxonomyCodes.ContainsKey("Locator2") ? taxonomyCodes["Locator2"].Code3 : string.Empty, new { @id = "EditVisitRate_Locator2_Code3", @maxlength = "12" })%>
            </div>
        </div>
        <div class="narrow row">
            <label for="EditVisitRate_Locator3_Code1" class="fl strong">UB04 Locator 81CCc</label>
            <%= Html.Hidden("Ub04Locator81", "Locator3")%>
            <div class="fr">
                <%= Html.TextBox("Locator3_Code1", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code1 : string.Empty, new { @id = "EditVisitRate_Locator3_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator3_Code2", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code2 : string.Empty, new { @id = "EditVisitRate_Locator3_Code2", @maxlength = "10" })%>
                <%= Html.TextBox("Locator3_Code3", taxonomyCodes.ContainsKey("Locator3") ? taxonomyCodes["Locator3"].Code3 : string.Empty, new { @id = "EditVisitRate_Locator3_Code3", @maxlength = "12" })%>
            </div>
        </div>
        <div class="narrow row">
            <label for="EditVisitRate_Locator4_Code1" class="fl strong">UB04 Locator 81CCd</label>
            <%= Html.Hidden("Ub04Locator81", "Locator4")%>
            <div class="fr">
                <%= Html.TextBox("Locator4_Code1", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code1 : string.Empty, new { @id = "EditVisitRate_Locator4_Code1", @class = "shortest", @maxlength = "2" })%>
                <%= Html.TextBox("Locator4_Code2", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code2 : string.Empty, new { @id = "EditVisitRate_Locator4_Code2", @maxlength = "10" })%>
                <%= Html.TextBox("Locator4_Code3", taxonomyCodes.ContainsKey("Locator4") ? taxonomyCodes["Locator4"].Code3 : string.Empty, new { @id = "EditVisitRate_Locator4_Code3", @maxlength = "12" })%>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Visit Cost Rates</legend>
    <div class="column">
        <div class="row">
            <label for="EditVisitRate_SkilledNurseSOCPerUnit" class="fl strong">Skilled Service for management and evaluation (SOC - G0162)</label>
            <%= Html.Hidden("RateDiscipline", "SkilledNurseSOC")%>
            <div class="fr"><%= Html.TextBox("SkilledNurseSOC_PerUnit", data.ContainsKey("SkilledNurseSOC") ? data["SkilledNurseSOC"].PerUnit : string.Empty, new { @id = "EditVisitRate_SkilledNurseSOCPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SkilledNurseRecetPerUnit" class="fl strong">Skilled Service for the observation and assessment (Recert - G0162)</label>
            <%= Html.Hidden("RateDiscipline", "SkilledNurseRecet")%>
            <div class="fr"><%= Html.TextBox("SkilledNurseRecet_PerUnit", data.ContainsKey("SkilledNurseRecet") ? data["SkilledNurseRecet"].PerUnit : string.Empty, new { @id = "EditVisitRate_SkilledNurseRecetPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SkilledNursePerUnit" class="fl strong">Skilled Nurse (Direct - G0154)</label>
            <%= Html.Hidden("RateDiscipline", "SkilledNurse")%>
            <div class="fr"><%= Html.TextBox("SkilledNurse_PerUnit", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerUnit : string.Empty, new { @id = "EditVisitRate_SkilledNursePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SkilledNurseTeachingPerUnit" class="fl strong">Skilled Nurse (Teaching - G0164) </label>
            <%= Html.Hidden("RateDiscipline", "SkilledNurseTeaching")%>
            <div class="fr"><%= Html.TextBox("SkilledNurseTeaching_PerUnit", data.ContainsKey("SkilledNurseTeaching") ? data["SkilledNurseTeaching"].PerUnit : string.Empty, new { @id = "EditVisitRate_SkilledNurseTeachingPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SkilledNurseObservationPerUnit" class="fl strong">Skilled Nurse (Observation - G0163)</label>
            <%= Html.Hidden("RateDiscipline", "SkilledNurseObservation")%>
            <div class="fr"><%= Html.TextBox("SkilledNurseObservation_PerUnit", data.ContainsKey("SkilledNurseObservation") ? data["SkilledNurseObservation"].PerUnit : string.Empty, new { @id = "EditVisitRate_SkilledNurseObservationPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SkilledNurseManagementPerUnit" class="fl strong">Skilled Nurse (Management - G0162)</label>
            <%= Html.Hidden("RateDiscipline", "SkilledNurseManagement")%>
            <div class="fr"><%= Html.TextBox("SkilledNurseManagement_PerUnit", data.ContainsKey("SkilledNurseManagement") ? data["SkilledNurseManagement"].PerUnit : string.Empty, new { @id = "EditVisitRate_SkilledNurseManagementPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_PhysicalTherapyPerUnit" class="fl strong">Physical Therapy (G0151)</label>
            <%= Html.Hidden("RateDiscipline", "PhysicalTherapy")%>
            <div class="fr"><%= Html.TextBox("PhysicalTherapy_PerUnit", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerUnit : string.Empty, new { @id = "EditVisitRate_PhysicalTherapyPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_PhysicalTherapyAssistancePerUnit" class="fl strong">Physical Therapy Assistant (G0157)</label>
            <%= Html.Hidden("RateDiscipline", "PhysicalTherapyAssistance")%>
            <div class="fr"><%= Html.TextBox("PhysicalTherapyAssistance_PerUnit", data.ContainsKey("PhysicalTherapyAssistance") ? data["PhysicalTherapyAssistance"].PerUnit : string.Empty, new { @id = "EditVisitRate_PhysicalTherapyAssistancePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_PhysicalTherapyMaintenancePerUnit" class="fl strong">Physical Therapy (Maintenance - G0159)</label>
            <%= Html.Hidden("RateDiscipline", "PhysicalTherapyMaintenance")%>
            <div class="fr"><%= Html.TextBox("PhysicalTherapyMaintenance_PerUnit", data.ContainsKey("PhysicalTherapyMaintenance") ? data["PhysicalTherapyMaintenance"].PerUnit : string.Empty, new { @id = "EditVisitRate_PhysicalTherapyMaintenancePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_OccupationalTherapyPerUnit" class="fl strong">Occupational Therapy (G0152)</label>
            <%= Html.Hidden("RateDiscipline", "OccupationalTherapy")%>
            <div class="fr"><%= Html.TextBox("OccupationalTherapy_PerUnit", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerUnit : string.Empty, new { @id = "EditVisitRate_OccupationalTherapyPerUnit", @class = "decimal shorter" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="EditVisitRate_OccupationalTherapyAssistancePerUnit" class="fl strong">Occupational Therapy Assistant (G0158)</label>
            <%= Html.Hidden("RateDiscipline", "OccupationalTherapyAssistance")%>
            <div class="fr"><%= Html.TextBox("OccupationalTherapyAssistance_PerUnit", data.ContainsKey("OccupationalTherapyAssistance") ? data["OccupationalTherapyAssistance"].PerUnit : string.Empty, new { @id = "EditVisitRate_OccupationalTherapyAssistancePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_OccupationalTherapyMaintenancePerUnit" class="fl strong">Occupational Therapy (Maintenance - G0160)</label>
            <%= Html.Hidden("RateDiscipline", "OccupationalTherapyMaintenance")%>
            <div class="fr"><%= Html.TextBox("OccupationalTherapyMaintenance_PerUnit", data.ContainsKey("OccupationalTherapyMaintenance") ? data["OccupationalTherapyMaintenance"].PerUnit : string.Empty, new { @id = "EditVisitRate_OccupationalTherapyMaintenancePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SpeechTherapyPerUnit" class="fl strong">Speech Therapy (G0153)</label>
            <%= Html.Hidden("RateDiscipline", "SpeechTherapy")%>
            <div class="fr"><%= Html.TextBox("SpeechTherapy_PerUnit", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerUnit : string.Empty, new { @id = "EditVisitRate_SpeechTherapyPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_SpeechTherapyMaintenancePerUnit" class="fl strong">Speech Therapy (Maintenance - G0161)</label>
            <%= Html.Hidden("RateDiscipline", "SpeechTherapyMaintenance")%>
            <div class="fr"><%= Html.TextBox("SpeechTherapyMaintenance_PerUnit", data.ContainsKey("SpeechTherapyMaintenance") ? data["SpeechTherapyMaintenance"].PerUnit : string.Empty, new { @id = "EditVisitRate_SpeechTherapyMaintenancePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_MedicareSocialWorkerPerUnit" class="fl strong">Medicare Social Worker (G0155)</label>
            <%= Html.Hidden("RateDiscipline", "MedicareSocialWorker")%>
            <div class="fr"><%= Html.TextBox("MedicareSocialWorker_PerUnit", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerUnit : string.Empty, new { @id = "EditVisitRate_MedicareSocialWorkerPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_HomeHealthAidePerUnit" class="fl strong">Home Health Aide (G0156)</label>
            <%= Html.Hidden("RateDiscipline", "HomeHealthAide")%>
            <div class="fr"><%= Html.TextBox("HomeHealthAide_PerUnit", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerUnit : string.Empty, new { @id = "EditVisitRate_HomeHealthAidePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_AttendantPerUnit" class="fl strong">Attendant</label>
            <%= Html.Hidden("RateDiscipline", "Attendant")%>
            <div class="fr"><%= Html.TextBox("Attendant_PerUnit", data.ContainsKey("Attendant") ? data["Attendant"].PerUnit : string.Empty, new { @id = "EditVisitRate_AttendantPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_CompanionCarePerUnit" class="fl strong">Companion Care</label>
            <%= Html.Hidden("RateDiscipline", "CompanionCare")%>
            <div class="fr"><%= Html.TextBox("CompanionCare_PerUnit", data.ContainsKey("CompanionCare") ? data["CompanionCare"].PerUnit : string.Empty, new { @id = "EditVisitRate_CompanionCarePerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_HomemakerServicesPerUnit" class="fl strong">Homemaker Services </label>
            <%= Html.Hidden("RateDiscipline", "HomemakerServices")%>
            <div class="fr"><%= Html.TextBox("HomemakerServices_PerUnit", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].PerUnit : string.Empty, new { @id = "EditVisitRate_HomemakerServicesPerUnit", @class = "decimal shorter" })%></div>
        </div>
        <div class="row">
            <label for="EditVisitRate_PrivateDutySitterPerUnit" class="fl strong">Private Duty Sitter</label>
            <%= Html.Hidden("RateDiscipline", "PrivateDutySitter")%>
            <div class="fr"><%= Html.TextBox("PrivateDutySitter_PerUnit", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].PerUnit : string.Empty, new { @id = "EditVisitRate_PrivateDutySitterPerUnit", @class = "decimal shorter" })%></div>
        </div>
    </div>
</fieldset>
