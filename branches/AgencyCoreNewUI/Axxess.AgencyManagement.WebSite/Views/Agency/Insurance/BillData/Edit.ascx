﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ChargeRate>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Insurance/BillData/Update", "Agency", FormMethod.Post, new { @id = "EditBillData_Form" })) { %>
    <%= Html.Hidden("InsuranceId", Model.InsuranceId, new { @id = "EditBillData_InsuranceId" })%>
    <fieldset>
        <legend>Edit Visit Information</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditBillData_Task" class="fl strong">Task</label>
                <div class="fr"><%= Html.InsuranceDisciplineTask("Id", Model.Id, Model.InsuranceId, true, new { @id = "EditBillData_Task", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditBillData_Description" class="fl strong">Preferred Description</label>
                <div class="fr"><%= Html.TextBox("PreferredDescription", Model.PreferredDescription, new { @id = "EditBillData_Description", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="EditBillData_RevenueCode" class="fl strong">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "EditBillData_RevenueCode", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditBillData_HCPCS" class="fl strong">HCPCS Code</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = "EditBillData_HCPCS", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditBillData_ChargeRate" class="fl strong">Rate</label>
                <div class="fr"><%= Html.TextBox("Charge", Model.Charge, new { @id = "EditBillData_ChargeRate", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditBillData_Modifier" class="fl strong">Modifier</label>
                <div class="fr">
                    <%= Html.TextBox("Modifier", Model.Modifier, new { @id = "EditBillData_Modifier", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", Model.Modifier2, new { @id = "EditBillData_Modifier2", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", Model.Modifier3, new { @id = "EditBillData_Modifier3", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", Model.Modifier4, new { @id = "EditBillData_Modifier4", @class = "shortest", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditBillData_ChargeType" class="fl strong">Service Unit Type</label>
                <div class="fr"><%= Html.UnitType("ChargeType", Model.ChargeType, new { @id = "EditBillData_ChargeType", @class = "required notzero" })%></div>
            </div>
            <div id="EditBillData_IsTimeLimitContent">
                <div class="row">
                    <div class="wide checkgroup">
                        <div class="option">
                            <%= Html.CheckBox("IsTimeLimit", Model.IsTimeLimit, new { @id = "EditBillData_IsTimeLimit" })%>
                            <label for="EditBillData_IsTimeLimit">Check if a time limit applies per unit</label>
                        </div>
                    </div>
                </div>
                <div id="EditBillData_TimeLimitContent">
                    <div class="row">
                        <label for="EditBillData_TimeLimit" class="fl strong">Time Limit</label>
                        <div class="fr radio">
                            <label for="EditBillData_TimeLimitHour">Hour</label>
                            <%= Html.TextBox("TimeLimitHour", Model.TimeLimitHour, new { @id = "EditBillData_TimeLimitHour", @class = "shortest", @maxlength = "2" })%>
                            <label for="EditBillData_TimeLimitHour">Minutes</label>
                            <%= Html.TextBox("TimeLimitMin", Model.TimeLimitMin, new { @id = "EditBillData_TimeLimitMin", @class = "shortest", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <em>This applies for visit times which exceed the insurance provider limit</em>
                    </div>
                    <div class="row">
                        <label for="EditBillData_SecondDescription" class="fl strong">Description</label>
                        <div class="fr"><%= Html.TextBox("SecondDescription", Model.SecondDescription, new { @id = "EditBillData_SecondDescription", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="EditBillData_SecondRevenueCode" class="fl strong">Revenue Code</label>
                        <div class="fr"><%= Html.TextBox("SecondRevenueCode", Model.SecondRevenueCode, new { @id = "EditBillData_SecondRevenueCode", @class = "shorter", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="EditBillData_SecondHCPCS" class="fl strong">HCPCS Code</label>
                        <div class="fr"><%= Html.TextBox("SecondCode", Model.SecondCode, new { @id = "EditBillData_SecondHCPCS", @class = "shorter", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="EditBillData_SecondModifier" class="fl strong">Modifier</label>
                        <div class="fr">
                            <%= Html.TextBox("SecondModifier", Model.SecondModifier, new { @id = "EditBillData_SecondModifier", @class = "shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", Model.SecondModifier2, new { @id = "EditBillData_SecondModifier2", @class = "shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", Model.SecondModifier3, new { @id = "EditBillData_SecondModifier3", @class = "shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", Model.SecondModifier4, new { @id = "EditBillData_SecondModifier4", @class = "shortest", @maxlength = "2" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div id="EditBillData_PerVisitUnitContent">
                <div class="row">
                    <label for="EditBillData_PerVisitUnit" class="fl strong">Service Units per Visit</label>
                    <div class="fr"><%= Html.TextBox("Unit", Model.Unit, new { @id = "EditBillData_PerVisitUnit", @class = "shorter", @maxlength = "1" }) %>
                </div>
                <div class="row">
                    <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit</em>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>