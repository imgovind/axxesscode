﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Insurance/BillData/Create", "Agency", FormMethod.Post, new { @id = "NewBillData_Form" })) { %>
    <%= Html.Hidden("InsuranceId", Model, new { @id = "NewBillData_InsuranceId" })%>
    <fieldset>
        <legend>New Visit Information</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewBillData_Task" class="fl strong">Task</label>
                <div class="fr"><%= Html.InsuranceDisciplineTask("Id", 0, Model, true, new { @id = "NewBillData_Task", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewBillData_Description" class="fl strong">Preferred Description</label>
                <div class="fr"><%= Html.TextBox("PreferredDescription", "", new { @id = "NewBillData_Description", @class = "required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="NewBillData_RevenueCode" class="fl strong">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", "", new { @id = "NewBillData_RevenueCode", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_HCPCS" class="fl strong">HCPCS Code</label>
                <div class="fr"><%= Html.TextBox("Code", "", new { @id = "NewBillData_HCPCS", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_BillData_ChargeRate" class="fl strong">Rate</label>
                <div class="fr"><%= Html.TextBox("Charge", "", new { @id = "NewBillData_ChargeRate", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewBillData_Modifier" class="fl strong">Modifier</label>
                <div class="fr">
                    <%= Html.TextBox("Modifier", "", new { @id = "NewBillData_Modifier", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier2", "", new { @id = "NewBillData_Modifier2", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier3", "", new { @id = "NewBillData_Modifier3", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Modifier4", "", new { @id = "NewBillData_Modifier4", @class = "shortest", @maxlength = "2" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewBillData_ChargeType" class="fl strong">Service Unit Type</label>
                <div class="fr"><%=Html.UnitType("ChargeType", "0", new { @id = "NewBillData_ChargeType", @class = "required notzero" })%></div>
            </div>
            <div id="NewBillData_IsTimeLimitContent">
                <div class="row">
                    <div class="wide checkgroup">
                        <div class="option">
                            <%= Html.CheckBox("IsTimeLimit",false, new { @id = "NewBillData_IsTimeLimit" })%>
                            <label for="NewBillData_IsTimeLimit">Check if a time limit applies per unit</label>
                        </div>
                    </div>
                </div>
                <div class="margin" id="NewBillData_TimeLimitContent">
                    <div class="row">
                        <label for="NewBillData_TimeLimit" class="fl strong">Time Limit</label>
                        <div class="fr radio">
                            <label for="NewBillData_TimeLimitHour">Hour</label>
                            <%= Html.TextBox("TimeLimitHour", "", new { @id = "NewBillData_TimeLimitHour", @class = "shortest", @maxlength = "2" })%>
                            <label for="NewBillData_TimeLimitMin">Minute</label>
                            <%= Html.TextBox("TimeLimitMin", "", new { @id = "NewBillData_TimeLimitMin", @class = "shorter", @maxlength = "2" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="NewBillData_SecondDescription" class="fl strong">Description</label>
                        <div class="fr"><%= Html.TextBox("SecondDescription", "", new { @id = "NewBillData_SecondDescription", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="NewBillData_SecondRevenueCode" class="fl strong">Revenue Code</label>
                        <div class="fr"><%= Html.TextBox("SecondRevenueCode", "", new { @id = "NewBillData_SecondRevenueCode", @class = "", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="NewBillData_SecondHCPCS" class="fl strong">HCPCS Code</label>
                        <div class="fr"><%= Html.TextBox("SecondCode", "", new { @id = "NewBillData_SecondHCPCS", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="NewBillData_SecondModifier" class="fl strong">Modifier</label>
                        <div class="fr">
                            <%= Html.TextBox("SecondModifier", "", new { @id = "NewBillData_SecondModifier", @class = "shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier2", "", new { @id = "NewBillData_SecondModifier2", @class = "shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier3", "", new { @id = "NewBillData_SecondModifier3", @class = "shortest", @maxlength = "2" })%>
                            <%= Html.TextBox("SecondModifier4", "", new { @id = "NewBillData_SecondModifier4", @class = "shortest", @maxlength = "2" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="NewBillData_PerVisitUnitContent">
                <label for="NewBillData_PerVisitUnit" class="fl strong">Service Units per Visit</label>
                <div class="fr"><%= Html.TextBox("Unit", "1", new { @id = "NewBillData_PerVisitUnit", @class = "shortest required", @maxlength = "1" })%></div>
                <div class="clear"></div>
                <em>Units are calculated per insurance provider specifications. For instance, per Medicare guidelines 15 minutes is equal to 1 unit</em>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>