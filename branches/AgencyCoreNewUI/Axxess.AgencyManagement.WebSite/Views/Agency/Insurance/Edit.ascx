﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyInsurance>" %>
<%  var data = Model != null ? Model.ToChargeRateDictionary() : new Dictionary<string, ChargeRate>(); %>
<%  var locator = Model != null ? Model.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
<span class="wintitle">Edit Insurance | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Insurance/Update", "Agency", FormMethod.Post, new { @id = "EditInsurance_Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "EditInsurance_Id" })%>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_Name" class="fl strong">Insurance/Payor Name</label>
                <div class="fr"><%= Html.TextBox("Name", Model.Name, new { @id = "EditInsurance_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_PayorType" class="fl strong">Payor Type</label>
                <div class="fr"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", Convert.ToString(Model.PayorType), new { @id = "EditInsurance_PayorType", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_InvoiceType" class="fl strong">Invoice Type</label>
                <div class="fr">
                    <%  var invoiceType = new SelectList(new[] {
                            new SelectListItem { Text = "UB-04", Value = "1" },
                            new SelectListItem { Text = "HCFA 1500", Value = "2" },
                            new SelectListItem { Text = "Invoice", Value = "3" }
                        }, "Value", "Text", Convert.ToString(Model.InvoiceType)); %>
                    <%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "EditInsurance_InvoiceType" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditInsurance_BillType" class="fl strong">Bill Type</label>
                <div class="fr">
                    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Institutional", Value = "institutional" },
                            new SelectListItem { Text = "Professional", Value = "professional" }
                        }, "Value", "Text", Model.BillType); %>
                    <%= Html.DropDownList("BillType", billType, new { @id = "EditInsurance_BillType" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_PayorId" class="fl strong">Payor Id</label>
                <div class="fr"><%= Html.TextBox("PayorId", Convert.ToString(Model.PayorId), new { @id = "EditInsurance_PayorId", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ProviderId" class="fl strong">Provider ID/Code</label>
                <div class="fr"><%= Html.TextBox("ProviderId", Model.ProviderId, new { @id = "EditInsurance_ProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_OtherProviderId" class="fl strong">Other Provider ID</label>
                <div class="fr"><%= Html.TextBox("OtherProviderId", Model.OtherProviderId, new { @id = "EditInsurance_OtherProviderId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ProviderSubscriberId" class="fl strong">Provider Subscriber ID</label>
                <div class="fr"><%= Html.TextBox("ProviderSubscriberId", Model.ProviderSubscriberId, new { @id = "EditInsurance_ProviderSubscriberId", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_SubmitterId" class="fl strong">Submitter ID</label>
                <div class="fr"><%= Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "EditInsurance_SubmitterId", @maxlength = "40" })%></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide column">
            <div class="narrow row">
                <label for="EditInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCa</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <div class="fr">
                    <%= Html.TextBox("Locator1_Code1", locator.ContainsKey("Locator1") ? locator["Locator1"].Code1 : string.Empty, new { @id = "EditInsurance_Locator1_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator1_Code2", locator.ContainsKey("Locator1") ? locator["Locator1"].Code2 : string.Empty, new { @id = "EditInsurance_Locator1_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator1_Code3", locator.ContainsKey("Locator1") ? locator["Locator1"].Code3 : string.Empty, new { @id = "EditInsurance_Locator1_Code3", @maxlength = "12" })%>
                </div>
            </div>
            <div class="narrow row">
                <label for="EditInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCb</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator2")%>
                <div class="fr">
                    <%= Html.TextBox("Locator2_Code1", locator.ContainsKey("Locator2") ? locator["Locator2"].Code1 : string.Empty, new { @id = "EditInsurance_Locator2_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator2_Code2", locator.ContainsKey("Locator2") ? locator["Locator2"].Code2 : string.Empty, new { @id = "EditInsurance_Locator2_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator2_Code3", locator.ContainsKey("Locator2") ? locator["Locator2"].Code3 : string.Empty, new { @id = "EditInsurance_Locator2_Code3", @maxlength = "12" })%>
                </div>
            </div>
            <div class="narrow row">
                <label for="EditInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCc</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator3")%>
                <div class="fr">
                    <%= Html.TextBox("Locator3_Code1", locator.ContainsKey("Locator3") ? locator["Locator3"].Code1 : string.Empty, new { @id = "EditInsurance_Locator3_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator3_Code2", locator.ContainsKey("Locator3") ? locator["Locator3"].Code2 : string.Empty, new { @id = "EditInsurance_Locator3_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator3_Code3", locator.ContainsKey("Locator3") ? locator["Locator3"].Code3 : string.Empty, new { @id = "EditInsurance_Locator3_Code3", @maxlength = "12" })%>
                </div>
            </div>
            <div class="narrow row">
                <label for="EditInsurance_Ub04Locator81cca" class="fl strong">UB04 Locator 81CCd</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator4")%>
                <div class="fr">
                    <%= Html.TextBox("Locator4_Code1", locator.ContainsKey("Locator4") ? locator["Locator4"].Code1 : string.Empty, new { @id = "EditInsurance_Locator4_Code1", @class = "shortest", @maxlength = "2" })%>
                    <%= Html.TextBox("Locator4_Code2", locator.ContainsKey("Locator4") ? locator["Locator4"].Code2 : string.Empty, new { @id = "EditInsurance_Locator4_Code2", @maxlength = "10" })%>
                    <%= Html.TextBox("Locator4_Code3", locator.ContainsKey("Locator4") ? locator["Locator4"].Code3 : string.Empty, new { @id = "EditInsurance_Locator4_Code3", @maxlength = "12" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessTheBiller", Model.IsAxxessTheBiller, new { @id = "EditInsurance_IsAxxessTheBiller" })%>
                        <label for="EditInsurance_IsAxxessTheBiller">Electronically Submit Claims to Clearing House through Axxess&#8482;</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_ClearingHouse" class="fl strong">Clearing House</label>
                <div class="fr">
                    <%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                        }, "Value", "Text", Model.ClearingHouse); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "EditInsurance_ClearingHouse" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="EditInsurance_EdiInformation">
            <div class="column">
                <div class="row">
                    <label for="EditInsurance_InterchangeReceiverId" class="fl strong">Interchange Receiver ID</label>
                    <div class="fr">
                        <%  var interchangeReceiverId = new SelectList(new[] {
                                new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                                new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                                new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                                new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                                new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                                new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                                new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                                new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                                new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                            }, "Value", "Text",Model.InterchangeReceiverId); %>
                        <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "EditInsurance_InterchangeReceiverId" })%>
                    </div>
                </div>
                <div class="row">
                    <label for="EditInsurance_ClearingHouseSubmitterId" class="fl strong">Clearing House Submitter ID</label>
                    <div class="fr"><%= Html.TextBox("ClearingHouseSubmitterId", Model.ClearingHouseSubmitterId, new { @id = "EditInsurance_ClearingHouseSubmitterId", @maxlength = "40" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="EditInsurance_SubmitterName" class="fl strong">Submitter Name</label>
                    <div class="fr"><%= Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "EditInsurance_SubmitterName", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="EditInsurance_SubmitterPhone1" class="fl strong">Submitter Phone</label>
                    <div class="fr">
                        <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(0, 3) : "", new { @id = "EditInsurance_SubmitterPhoneArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                        -
                        <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(3, 3) : "", new { @id = "EditInsurance_SubmitterPhoneArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                        -
                        <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(6, 4) : "", new { @id = "EditInsurance_SubmitterPhoneArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditInsurance_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditInsurance_AddressLine2", @maxlength = "75" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditInsurance_AddressCity" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "EditInsurance_AddressStateCode", @class = "state" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditInsurance_AddressZipCode", @class = "numeric zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_ContactPersonFirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "EditInsurance_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ContactPersonLastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "EditInsurance_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_ContactEmailAddress" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("ContactEmailAddress", Model.ContactEmailAddress, new { @id = "EditInsurance_ContactEmailAddress", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_PhoneNumberArray1" class="fl strong">Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(0, 3) : "", new { @id = "EditInsurance_PhoneNumberArray1", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(3, 3) : "", new { @id = "EditInsurance_PhoneNumberArray2", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(6, 4) : "", new { @id = "EditInsurance_PhoneNumberArray3", @class = "numeric required phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditInsurance_FaxNumberArray1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "EditInsurance_FaxNumberArray1", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "EditInsurance_FaxNumberArray2", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "EditInsurance_FaxNumberArray3", @class = "numeric required phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInsurance_CurrentBalance" class="fl strong">Current Balance</label>
                <div class="fr"><%= Html.TextBox("CurrentBalance", Model.CurrentBalance, new { @id = "EditInsurance_CurrentBalance", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditInsurance_WorkWeekStartDay" class="fl strong">Work Week Begins</label>
                <div class="fr">
                    <%  var workWeekStartDay = new SelectList(new[] {
                            new SelectListItem { Text = "Sunday", Value = "1" },
                            new SelectListItem { Text = "Monday", Value = "2" }
                        }, "Value", "Text", Convert.ToString(Model.WorkWeekStartDay)); %>
                    <%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "EditInsurance_WorkWeekStartDay" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Visit Authorization Required</label>
                <div class="fr radio">
                    <%= Html.RadioButton("IsVisitAuthorizationRequired", true, Model.IsVisitAuthorizationRequired, new { @id = "EditInsurance_IsAuthReqYes", @class = "required" })%>
                    <label for="EditInsurance_IsAuthReqYes" class="fixed short">Yes</label>
                    <%= Html.RadioButton("IsVisitAuthorizationRequired", false, !Model.IsVisitAuthorizationRequired, new { @id = "EditInsurance_IsAuthReqNo", @class = "required" })%>
                    <label for="EditInsurance_IsAuthReqNo" class="fixed short">No</label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visit Information</legend>
        <div class="wide column">
            <div class="row">
                <em>To apply the visit information from the selected insurance, click on the &#8220;Apply&#8221; button on the left</em>
            </div>
            <div class="row">
                <div class="fl">
                    <label for="EditInsurance_OldInsuranceId" class="fl strong">Add Physician to Patient</label>
                    <div class="fl"><%= Html.InsurancesNoneMedicare("OldInsuranceId", "0", true, "-- Select Insurance --", new { @id = "EditInsurance_OldInsuranceId" })%></div>
                    <div class="buttons fl">
                        <ul>
                            <li><a class="apply-visit-info">Apply</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row buttons">
                <ul>
                    <li><a class="add-visit-info">Add Visit Information</a></li>
                </ul>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<ChargeRate>().Name("EditInsurance_BillDatas").DataKeys(keys => { keys.Add(r => r.Id).RouteKey("Id"); }).Columns(columns => {
                        columns.Bound(e => e.DisciplineTaskName).Title("Task");
                        columns.Bound(e => e.PreferredDescription).Title("Description").ReadOnly();
                        columns.Bound(e => e.RevenueCode).Title("Revenue Code").Width(60);
                        columns.Bound(e => e.Code).Title("HCPCS").Width(55);
                        columns.Bound(e => e.Charge).Format("${0:#0.00}").Title("Rate").Width(45);
                        columns.Bound(e => e.Modifier).Title("Modifier").Width(80);
                        columns.Bound(e => e.ChargeTypeName).Title("Unit Type").Width(65);
                        columns.Bound(e => e.TimeLimitFormat).Title("Time Limit").Width(65);
                        columns.Bound(e => e.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Insurance.BillData.Edit('<#=Id#>',$('#EditInsurance_Id').val());return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Insurance.BillData.Delete('<#=Id#>',$('#EditInsurance_Id').val(),$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Width(85);
                    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("InsuranceBillDataGrid", "Agency", new { InsuranceId = Model.Id })).Sortable().Footer(false)%>
                <h2 class="hidden blue">No Visit Information Found</h2>
            </div>
        </div>
    </fieldset>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>