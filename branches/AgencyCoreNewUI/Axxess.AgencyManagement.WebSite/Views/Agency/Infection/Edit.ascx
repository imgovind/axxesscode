﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Infection>" %>
<%  if (Model != null) Model.SignatureDate = DateTime.Today; %>
<span class="wintitle">Edit Infection Report | <%= Current.AgencyName %></span>
<div class="wrapper main">
<% using (Html.BeginForm("Infection/Update", "Agency", FormMethod.Post, new { @id = "EditInfection_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditInfection_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditInfection_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditInfection_UserId" })%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId.IsEmpty() ? string.Empty : Model.EpisodeId.ToString(), new { @id = "EditInfection_EpisodeId" })%>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditInfection_PatientId" class="fl strong">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
    <%  if (Model.EpisodeId.IsEmpty()) { %>
            <div class="row">
                <label class="fl strong">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "EditInfection_EpisodeId", @class = "required notzero" })%></div>
            </div>
    <%  } else { %>
            <div class="row">
                <label class="fl strong">Episode Associated</label>
                <div class="fr"><%= string.Format("{0}-{1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="EditInfection_PhysicianId" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", Model.PhysicianId != Guid.Empty ? Model.PhysicianId.ToString() : string.Empty, new { @id = "EditInfection_PhysicianId", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
         </div>
        <div class="column">
            <div class="row">
                <label for="EditInfection_InfectionDate" class="fl strong">Date of Infection</label>
                <div class="fr"><input type="text" class="date-picker required" name="InfectionDate" value="<%= Model.InfectionDate.IsValid() ? Model.InfectionDate.ToShortDateString() : string.Empty %>" id="EditInfection_InfectionDate" /></div>
            </div>
            <div class="row">
                <label for="EditInfection_TreatmentPrescribedYes" class="fl strong">Treatment Prescribed</label>
                <div class="fr radio">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", Model.TreatmentPrescribed == "Yes", new { @id = "EditInfection_TreatmentPrescribedYes" })%>
                    <label for="EditInfection_TreatmentPrescribedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", Model.TreatmentPrescribed == "No", new { @id = "EditInfection_TreatmentPrescribedNo" })%>
                    <label for="EditInfection_TreatmentPrescribedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", Model.TreatmentPrescribed == "NA", new { @id = "EditInfection_TreatmentPrescribedNA" })%>
                    <label for="EditInfection_TreatmentPrescribedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="EditInfection_MDNotifiedYes" class="fl strong">M.D. Notified</label>
                <div class="fr radio">
                    <%= Html.RadioButton("MDNotified", "Yes", Model.MDNotified == "Yes", new { @id = "EditInfection_MDNotifiedYes" })%>
                    <label for="EditInfection_MDNotifiedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", Model.MDNotified == "No", new { @id = "EditInfection_MDNotifiedNo" })%>
                    <label for="EditInfection_MDNotifiedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", Model.MDNotified == "NA", new { @id = "EditInfection_MDNotifiedNA" })%>
                    <label for="EditInfection_MDNotifiedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="EditInfection_NewOrdersYes" class="fl strong">New Orders</label>
                <div class="fr radio">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", Model.NewOrdersCreated == "Yes", new { @id = "EditInfection_NewOrdersYes" })%>
                    <label for="EditInfection_NewOrdersYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", Model.NewOrdersCreated == "No", new { @id = "EditInfection_NewOrdersNo" })%>
                    <label for="EditInfection_NewOrdersNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", Model.NewOrdersCreated == "NA", new { @id = "EditInfection_NewOrdersNA" })%>
                    <label for="EditInfection_NewOrdersNA" class="fixed shorter">N/A</label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Type of Infection</legend>
        <%  string[] infectionTypes = Model.InfectionType != null && Model.InfectionType != "" ? Model.InfectionType.Split(';') : new string[] {}; %>
        <%= Html.Hidden("InfectionTypeArray")%>
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='EditInfection_InfectionType1' type='checkbox' value='Gastrointestinal' name='InfectionTypeArray' class='required' {0} />", infectionTypes.Contains("Gastrointestinal").ToChecked()) %>
                        <label for="EditInfection_InfectionType1">Gastrointestinal</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditInfection_InfectionType2' type='checkbox' value='Respiratory' name='InfectionTypeArray' class='required' {0} />", infectionTypes.Contains("Respiratory").ToChecked()) %>
                        <label for="EditInfection_InfectionType2">Respiratory</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditInfection_InfectionType3' type='checkbox' value='Skin' name='InfectionTypeArray' class='required' {0} />", infectionTypes.Contains("Skin").ToChecked()) %>
                        <label for="EditInfection_InfectionType3">Skin</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditInfection_InfectionType4' type='checkbox' value='Wound' name='InfectionTypeArray' class='required' {0} />", infectionTypes.Contains("Wound").ToChecked()) %>
                        <label for="EditInfection_InfectionType4">Wound</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditInfection_InfectionType5' type='checkbox' value='Urinary' name='InfectionTypeArray' class='required' {0} />", infectionTypes.Contains("Urinary").ToChecked()) %>
                        <label for="EditInfection_InfectionType5">Urinary</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditInfection_InfectionType6' type='checkbox' value='Other' name='InfectionTypeArray' class='required' {0} />", infectionTypes.Contains("Other").ToChecked()) %>
                        <label for="EditInfection_InfectionType6">Other</label>
                        <div class="more"><%= Html.TextBox("InfectionTypeOther", Model.InfectionTypeOther, new { @id = "EditInfection_InfectionTypeOther", @maxlength = "100" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Treatment</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditInfection_Treatment" class="strong">Treatment/Antibiotic</label>
                <div class="ac">
                    <%= Html.Templates("TreatmentTemplates") %>
                    <%= Html.TextArea("Treatment", Model.Treatment, new { @id = "EditInfection_Treatment", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditInfection_Narrative" class="strong">Narrative</label>
                <div class="ac">
                    <%= Html.Templates("NarrativeTemplates")%>
                    <%= Html.TextArea("Orders", Model.Orders, new { @id = "EditInfection_Narrative", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditInfection_FollowUp" class="strong">Follow Up</label>
                <div class="ac"><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "EditInfection_FollowUp", @class = "taller", @maxcharacters = "1000" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="EditInfection_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "EditInfection_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditInfection_SignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="EditInfection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "EditInfection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>