﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Infections | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-infection">New Infection Report</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Infection>().Name("List_InfectionReport").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
            columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
            columns.Bound(i => i.InfectionType).Title("Type of Infection").Sortable(true);
            columns.Bound(i => i.InfectionDateFormatted).Title("Infection Date").Sortable(true);
            columns.Bound(i => i.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
            columns.Bound(i => i.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Infection.Edit('<#=Id#>');return false\">Edit</a>").Title("Action").Width(50);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Infection/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>