﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Infection Report | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Infection/Create", "Agency", FormMethod.Post, new { @id = "NewInfection_Form" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewInfection_PatientId" class="fl strong">Patient</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "NewInfection_PatientId", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "NewInfection_EpisodeList", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewInfection_PhysicianId" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", "", new { @id = "NewInfection_PhysicianId", @class = "physician-picker" })%></div>
            </div>                
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician">New Physician</a></div>
            </div>
         </div>
        <div class="column">
            <div class="row">
                <label for="NewInfection_InfectionDate" class="fl strong">Date of Infection</label>
                <div class="fr"><input type="text" class="date-picker required" name="InfectionDate" id="NewInfection_InfectionDate" /></div>
            </div>
            <div class="row">
                <label for="NewInfection_TreatmentPrescribedYes" class="fl strong">Treatment Prescribed</label>
                <div class="fr radio">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", new { @id = "NewInfection_TreatmentPrescribedYes" })%>
                    <label for="NewInfection_TreatmentPrescribedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", new { @id = "NewInfection_TreatmentPrescribedNo" })%>
                    <label for="NewInfection_TreatmentPrescribedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", new { @id = "NewInfection_TreatmentPrescribedNA" })%>
                    <label for="NewInfection_TreatmentPrescribedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="NewInfection_MDNotifiedYes" class="fl strong">M.D. Notified</label>
                <div class="fr radio">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "NewInfection_MDNotifiedYes" })%>
                    <label for="NewInfection_MDNotifiedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "NewInfection_MDNotifiedNo" })%>
                    <label for="NewInfection_MDNotifiedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "NewInfection_MDNotifiedNA" })%>
                    <label for="NewInfection_MDNotifiedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="NewInfection_NewOrdersYes" class="fl strong">New Orders</label>
                <div class="fr radio">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "NewInfection_NewOrdersYes" })%>
                    <label for="NewInfection_NewOrdersYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "NewInfection_NewOrdersNo" })%>
                    <label for="NewInfection_NewOrdersNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "NewInfection_NewOrdersNA" })%>
                    <label for="NewInfection_NewOrdersNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-order" status="Add New Order">Add New Order</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Type of Infection</legend>
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input id="NewInfection_InfectionType1" type="checkbox" value="Gastrointestinal" name="InfectionTypeArray" />
                        <label for="NewInfection_InfectionType1">Gastrointestinal</label>
                    </div>
                    <div class="option">
                        <input id="NewInfection_InfectionType2" type="checkbox" value="Respiratory" name="InfectionTypeArray" />
                        <label for="NewInfection_InfectionType2">Respiratory</label>
                    </div>
                    <div class="option">
                        <input id="NewInfection_InfectionType3" type="checkbox" value="Skin" name="InfectionTypeArray" />
                        <label for="NewInfection_InfectionType3">Skin</label>
                    </div>
                    <div class="option">
                        <input id="NewInfection_InfectionType4" type="checkbox" value="Wound" name="InfectionTypeArray" />
                        <label for="NewInfection_InfectionType4">Wound</label>
                    </div>
                    <div class="option">
                        <input id="NewInfection_InfectionType5" type="checkbox" value="Urinary" name="InfectionTypeArray" />
                        <label for="NewInfection_InfectionType5">Urinary</label>
                    </div>
                    <div class="option">
                        <input id="NewInfection_InfectionType6" type="checkbox" value="Other" name="InfectionTypeArray" />
                        <label for="NewInfection_InfectionType6">Other</label>
                        <div class="more">
                            <%= Html.TextBox("InfectionTypeOther", "", new { @id = "NewInfection_InfectionTypeOther", @maxlength = "100" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Treatment</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewInfection_Treatment" class="strong">Treatment/Antibiotic</label>
                <div class="ac">
                    <%= Html.Templates("TreatmentTemplates") %>
                    <%= Html.TextArea("Treatment", new { @id = "NewInfection_Treatment", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewInfection_Narrative" class="strong">Narrative</label>
                <div class="ac">
                    <%= Html.Templates("NarrativeTemplates")%>
                    <%= Html.TextArea("Orders", new { @id = "NewInfection_Narrative", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewInfection_FollowUp" class="strong">Follow Up</label>
                <div class="ac"><%= Html.TextArea("FollowUp", new { @id = "NewInfection_FollowUp", @class = "taller", @maxcharacters = "1000" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="NewInfection_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "NewInfection_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewInfection_SignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="NewInfection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "NewInfection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>