﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="form-wrapper">
    <fieldset>
        <legend>OASIS Forms</legend>
        <div class="column">
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('StartOfCare','Nursing');return false">OASIS-C Start of Care</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('StartOfCare','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('StartOfCare','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('ResumptionOfCare','Nursing');return false">OASIS-C Resumption of Care</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('ResumptionOfCare','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('ResumptionOfCare','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('Recertification','Nursing');return false">OASIS-C Recertification</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('Recertification','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('Recertification','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('FollowUp','Nusring');return false">OASIS-C Follow-Up</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('FollowUp','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('FollowUp','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('TransferInPatientNotDischarged','Nursing');return false">OASIS-C Transfer Not Discharged</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('TransferInPatientNotDischarged','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('TransferInPatientNotDischarged','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('TransferInPatientDischarged','Nursing');return false">OASIS-C Transfer and Discharge</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('TransferInPatientDischarged','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('TransferInPatientDischarged','OT');return false">(OT)</a>
            </div>
        </div><div class="column">
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('DischargeFromAgencyDeath','Nursing');return false">OASIS-C Death at Home</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('DischargeFromAgencyDeath','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('DischargeFromAgencyDeath','OT');return false">(OT)</a>
            </div>
            <div class="row">
                <a class="link" onclick="OASIS.Assessment.BlankPrint('DischargeFromAgency','Nursing');return false">OASIS-C Discharge from Agency</a>
                &#160;&#8212;&#160;
                <a class="link" onclick="OASIS.Assessment.BlankPrint('DischargeFromAgency','PT');return false">(PT)</a>
                <a class="link" onclick="OASIS.Assessment.BlankPrint('DischargeFromAgency','OT');return false">(OT)</a>
            </div>
            <div class="row"><a class="link" onclick="OASIS.Assessment.BlankPrint('NonOasisStartOfCare','Nursing');return false">Non-OASIS Start of Care</a></div>
            <div class="row"><a class="link" onclick="OASIS.Assessment.BlankPrint('NonOasisRecertification','Nursing');return false">Non-OASIS Recertification</a></div>
            <div class="row"><a class="link" onclick="OASIS.Assessment.BlankPrint('NonOasisDischarge','Nursing');return false">Non-OASIS Discharge from Agency</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Forms</legend>
        <div class="column">
            <div class="row"><a class="link" onclick="Patient.FaceToFaceEncounter.Print();return false">Face to Face Encounter</a></div>
            <div class="row"><a class="link" onclick="Patient.Order.Print();return false">Physician Order</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="link" onclick="Patient.CommunicationNote.Print();return false">Communication Note</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nursing Forms</legend>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.TransferSummary %>');return false">Transfer Summary</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.CoordinationOfCare %>');return false">Coordination of Care</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.SixtyDaySummary %>');return false">60 Day Summary</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visits.MissedVisit.Print('<%= (int)DisciplineTasks.TransferSummary %>');return false">Missed Visit Form</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.LVNSupervisoryVisit %>');return false">LVN Supervisory Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.DischargeSummary %>');return false">Discharge Summary</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.SkilledNurseVisit %>');return false">Skilled Nurse Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visits.WoundCare.Print();return false">Wound Care</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Home Health Aide Forms</legend>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.HHAideCarePlan %>');return false">HHA Care Plan</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.HHAideVisit %>');return false">HHA Visit Note</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.HHAideSupervisoryVisit %>');return false">HHA Supervisory Visit</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PASCarePlan %>');return false">PAS Care Plan</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PASVisit %>');return false">PAS Note</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.UAPWoundCareVisit %>');return false">UAP Wound Care Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.UAPInsulinPrepAdminVisit %>');return false">UAP Insulin Prep/Admin Visit</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Medical Social Work Forms</legend>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.MSWEvaluationAssessment %>');return false">MSW Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.MSWProgressNote %>');return false">MSW Progress Note</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.MSWVisit %>');return false">MSW Visit</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.MSWAssessment %>');return false">MSW Assessment</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.MSWDischarge %>');return false">MSW Discharge</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.DriverOrTransportationNote %>');return false">Driver/Transportation Log</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Therapy Forms</legend>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PTDischarge %>');return false">PT Discharge</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PTEvaluation %>');return false">PT Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PTReEvaluation %>');return false">PT Re-Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PTMaintenance %>');return false">PT Maintenance</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PTVisit %>');return false">PT Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.PTAVisit %>');return false">PTA Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.OTEvaluation %>');return false">OT Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.OTReEvaluation %>');return false">OT Re-Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.OTDischarge %>');return false">OT Discharge</a></div>
        </div>
        <div class="column">
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.OTMaintenance %>');return false">OT Maintenance</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.OTVisit %>');return false">OT Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.COTAVisit %>');return false">COTA Visit</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.STEvaluation %>');return false">ST Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.STReEvaluation %>');return false">ST Re-Evaluation</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.STMaintenance %>');return false">ST Maintenance</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.STDischarge %>');return false">ST Discharge</a></div>
            <div class="row"><a class="link" onclick="Schedule.Visit.Shared.BlankPrint('<%= (int)DisciplineTasks.STVisit %>');return false">ST Visit</a></div>
        </div>
    </fieldset>
</div>