﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Template | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Template/Create", "Agency", FormMethod.Post, new { @id = "NewTemplate_Form" })) { %>
    <fieldset>
    <div class="wide column">
        <div class="row">
            <label for="NewTemplate_Title" class="fl strong">Name</label>
            <div class="ac"><%= Html.TextBox("Title", "", new { @id = "NewTemplate_Title", @class = "fill required", @maxlength = "100" })%></div>
        </div>
        <div class="row">
            <label for="NewTemplate_Text" class="strong">Text</label>
            <div class="ac"><textarea id="NewTemplate_Text" name="Text" class="tallest required" maxcharacters="5000"></textarea></div>
        </div>
    </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>