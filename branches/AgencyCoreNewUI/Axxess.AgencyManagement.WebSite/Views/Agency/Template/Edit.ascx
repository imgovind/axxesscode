﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTemplate>" %>
<span class="wintitle">Edit Template | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Template/Update", "Agency", FormMethod.Post, new { @id = "EditTemplate_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditTemplate_Id" }) %>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="EditTemplate_Title" class="fl strong">Name</label>
                <div class="ac"><%= Html.TextBox("Title", Model.Title, new { @id = "EditTemplate_Title", @class = "fill required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditTemplate_Text" class="strong">Text</label>
                <div class="ac"><textarea id="EditTemplate_Text" name="Text" class="tallest required" maxcharacters="5000"><%= Model.Text %></textarea></div>
            </div>
        </div>
    </fieldset>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>