﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-template">New Template</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencyTemplate>().Name("List_Template").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(t => t.Title).Title("Name").Sortable(true);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(120);
            columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120);
            columns.Bound(t => t.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Template.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Template.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Template/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>