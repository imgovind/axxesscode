﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Supplies | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-supply">New Supply</a></li></ul>
        <%--
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
        --%>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencySupply>().Name("List_Supply").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(t => t.Description).Sortable(true);
            columns.Bound(t => t.Code).Title("HCPCS").Sortable(true).Width(65);
            columns.Bound(t => t.RevenueCode).Title("Rev Code").Sortable(true).Width(65);
            columns.Bound(t => t.UnitCost).Format("{0:0.00}").Sortable(true).Width(70);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(100);
            columns.Bound(t => t.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Supply.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Supply.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Supply/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>