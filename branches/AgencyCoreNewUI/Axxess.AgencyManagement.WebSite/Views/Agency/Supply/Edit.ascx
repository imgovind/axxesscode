﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySupply>" %>
<span class="wintitle">Edit Supply | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Supply/Update", "Agency", FormMethod.Post, new { @id = "EditSupply_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditSupply_Id" }) %>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="EditSupply_Description" class="fl strong">Description</label>
                <div class="fr"><%= Html.TextBox("Description", Model.Description, new { @id = "EditSupply_Description", @class = "required", @maxlength = "350" })%></div>
            </div>
            <div class="row">
                <label for="EditSupply_HcpcsCode" class="fl strong">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", Model.Code, new { @id = "EditSupply_HcpcsCode", @maxlength = "6" })%></div>
            </div>
            <div class="row">
                <label for="EditSupply_RevCode" class="fl strong">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", Model.RevenueCode, new { @id = "EditSupply_RevCode", @maxlength = "6" })%></div>
            </div>
            <div class="row">
                <label for="EditSupply_UnitCost" class="fl strong">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", Model.UnitCost, new { @id = "EditSupply_UnitCost", @class = "decimal", @maxlength = "6" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>