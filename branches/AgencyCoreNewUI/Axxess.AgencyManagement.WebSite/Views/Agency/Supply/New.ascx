﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Supply | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Supply/Create", "Agency", FormMethod.Post, new { @id = "NewSupply_Form" })) { %>
<div class="wrapper main">
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="NewSupply_Description" class="fl strong">Description</label>
                <div class="fr"><%= Html.TextBox("Description", "", new { @id = "NewSupply_Description", @class = "required", @maxlength = "350" })%></div>
            </div>
            <div class="row">
                <label for="NewSupply_HcpcsCode" class="fl strong">HCPCS</label>
                <div class="fr"><%= Html.TextBox("Code", "", new { @id = "NewSupply_HcpcsCode", @maxlength = "6" })%></div>
            </div>
            <div class="row">
                <label for="NewSupply_RevCode" class="fl strong">Revenue Code</label>
                <div class="fr"><%= Html.TextBox("RevenueCode", "", new { @id = "NewSupply_RevCode", @maxlength = "6" })%></div>
            </div>
            <div class="row">
                <label for="NewSupply_UnitCost" class="fl strong">Unit Cost</label>
                <div class="fr"><%= Html.TextBox("UnitCost", "", new { @id = "NewSupply_UnitCost", @maxlength = "6" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>