﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Orders To Be Sent | <%= Current.AgencyName %></span>
<%  var pageName = "OrdersToBeSent"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul>
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="send">Send Electronically</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_SendType" class="strong">Display</label>
            <select id="<%= pageName %>_SendType" name="SendAutomatically">
                <option value="true" selected="selected">Electronic Orders</option>
                <option value="false">Manual Orders (Fax, Mail, etc)</option>
            </select>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" />
            <label for="<%= pageName %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
            columns.Bound(o => o.Type).Title(" ").ClientTemplate("<input type='checkbox' name='OrdersToBeSent' value='<#=Id#>_<#=PatientId#>' />").Width(35).Sortable(false);
            columns.Bound(o => o.Number).Title("Order").Width(80).Sortable(true);
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.Text).Title("Type").Sortable(true);
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true);
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(true);
            columns.Bound(o => o.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersToBeSentGrid", "Agency", new { branchId = Guid.Empty, sendAutomatically = true, startDate = DateTime.Now.AddDays(-59).ToShortDateString(), endDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>