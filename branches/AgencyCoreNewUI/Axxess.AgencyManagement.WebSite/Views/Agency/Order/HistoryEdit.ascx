﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Orders/History/Update", "Agency", FormMethod.Post, new { @id = "EditOrderHistory_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Update <%= Model.Text %></legend>
        <div class="wide column">
            <div class="row">
                <label for="EditOrderHistory_SendDate" class="fl strong">Sent Date</label>
                <div class="fr"><input type="text" class="date-picker" name="SendDate" id="EditOrderHistory_SendDate" value="<%= Model.SendDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.SendDate.ToShortDateString() %>" /></div>
            </div>
            <div class="row">
                <label for="EditOrderHistory_ReceivedDate" class="fl strong">Received Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ReceivedDate" id="EditOrderHistory_ReceivedDate" value="<%= Model.ReceivedDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.ReceivedDate.ToShortDateString() %>" /></div>
            </div>
            <div class="row">
                <label for="EditOrderHistory_PhysicianSignatureDate" class="fl strong">Physician Signature Date</label>
                <div class="fr"><input type="text" class="date-picker" name="PhysicianSignatureDate" id="EditOrderHistory_PhysicianSignatureDate" value="<%= Model.PhysicianSignatureDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.PhysicianSignatureDate.ToShortDateString() %>" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>