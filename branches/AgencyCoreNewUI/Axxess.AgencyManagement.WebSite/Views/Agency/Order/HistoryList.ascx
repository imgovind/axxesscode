﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Order History | <%= Current.AgencyName %></span>
<%  var pageName = "OrdersHistory"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul> 
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul> 
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" />
            <label for="<%= pageName %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.HasPhysicianAccess).Title("Electronic").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.OrderDate).Title("Order Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.SendDateFormatted).Format("{0:MM/dd/yyyy}").Title("Sent Date").Width(80).Sortable(true);
            columns.Bound(o => o.ReceivedDateFormatted).Format("{0:MM/dd/yyyy}").Title("Received Date").Width(100).Sortable(true);
            columns.Bound(o => o.PhysicianSignatureDateFormatted).Format("{0:MM/dd/yyyy}").Title("MD Sign Date").Width(100).Sortable(true);
            columns.Bound(o => o.Id).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"Agency.Orders.History.Edit('<#=Id#>','<#=PatientId#>','<#=Type#>');return false\">Edit</a>").Title("Action").Width(60);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersHistoryGrid", "Agency", new { branchId = Guid.Empty, startDate = DateTime.Now.AddDays(-59), endDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
