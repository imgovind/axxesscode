﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Orders/Receive", "Agency", FormMethod.Post, new { @id = "OrderReceive_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Receive <%= Model.Text %></legend>
        <div class="wide column">
            <div class="row">
                <label for="OrderReceive_ReceivedDate" class="fl strong">Recieved Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ReceivedDate" id="OrderReceive_ReceivedDate" /></div>
            </div>
            <div class="row">
                <label for="OrderReceive_PhysicianSignatureDate" class="fl strong">Physician Signature Date</label>
                <div class="fr"><input type="text" class="date-picker" name="PhysicianSignatureDate" id="OrderReceive_PhysicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>