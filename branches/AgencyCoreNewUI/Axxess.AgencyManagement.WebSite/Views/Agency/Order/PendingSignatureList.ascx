﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Orders Pending Signature | <%= Current.AgencyName %></span>
<%  var pageName = "OrdersPendingSignature"; %>
<div class="wrapper main blue">
     <div class="fr buttons">
        <ul> 
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul> 
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName + "_BranchId" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" />
            <label for="<%= pageName %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
        </div>
   </fieldset>
    <%= Html.Telerik().Grid<Order>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Sortable(true).Width(70).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true).Width(200).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true).ReadOnly();
            columns.Bound(o => o.CreatedDate).Title("Order Date").Sortable(true).Width(80).ReadOnly();
            columns.Bound(o => o.SentDate).Title("Sent Date").Sortable(true).Width(80).ReadOnly();
            columns.Bound(o => o.ReceivedDate).ClientTemplate("<#=''#>").Title("Received Date").Sortable(true).Width(100);
            columns.Bound(o => o.PhysicianSignatureDate).ClientTemplate("<#='' #>").Title("MD Sign Date").Sortable(true).Width(100);
            columns.Bound(o => o.Id).Sortable(false).ClientTemplate("<a class=\"link\" onclick=\"Agency.Orders.PendingSignature.Receive('<#=Id#>','<#=PatientId#>','<#=Type#>');return false\">Receive</a>").Title("Action").Width(60);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersPendingSignatureGrid", "Agency", new { branchId = Guid.Empty, startDate = DateTime.Now.AddDays(-59), endDate = DateTime.Now }).Update("MarkOrderAsReturned", "Agency")).Editable(editing => editing.Mode(GridEditMode.InLine)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
