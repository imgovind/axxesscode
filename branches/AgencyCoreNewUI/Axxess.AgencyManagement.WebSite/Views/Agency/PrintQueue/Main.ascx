﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<span class="wintitle">Print Queue | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul>
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls ac">
        <div class="buttons">
            <ul>
                <li><a status="Organize Grid by Patient" class="sort">Group By Patient</a></li>
                <li><a status="Organize Grid by Date" class="sort">Group By Date</a></li>
                <li><a status="Organize Grid by Task Type" class="sort">Group By Task</a></li>
                <li><a status="Organize Grid by Clinician" class="sort">Group By Clinician</a></li>
            </ul>
        </div>
        <%= Html.Hidden("GroupName", "EventDate") %>
    </fieldset>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post)) { %>
    <div class="content"><% Html.RenderPartial("PrintQueue/List", Model); %></div>
    <%= Html.Hidden("CommandType", "Print") %>
    <div class="buttons abs-bottom wrapper">
        <ul>
            <li><a class="save">Mark as Printed</a></li>
        </ul>
    </div>
<%  } %>
</div>