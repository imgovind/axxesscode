﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<%= Html.Telerik().Grid(Model).Name("PrintQueue_Grid").HtmlAttributes(new { @class = "bottom-gap args" }).Columns(columns => {
        columns.Bound(s => s.CustomValue).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}'/>", s.CustomValue)).Title("").Width("2%").HtmlAttributes(new { @class = "ac" });
        columns.Bound(s => s.PatientName).Width("20%");
        columns.Bound(s => s.EventDate).Width("9%");
        columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).Title("Task").Width("27%");
        columns.Bound(s => s.Status).Width("22%");
        columns.Bound(s => s.UserName).Width("20%");
    }).Groupable(settings => settings.Groups(groups => {
        var data = ViewData["GroupName"].ToString();
        if (data == "PatientName") groups.Add(s => s.PatientName);
        else if (data == "EventDate") groups.Add(s => s.EventDate);
        else if (data == "DisciplineTaskName") groups.Add(s => s.TaskName);
        else if (data == "UserName") groups.Add(s => s.UserName);
        else groups.Add(s => s.EventDate);
    })).Scrollable().Footer(false) %>