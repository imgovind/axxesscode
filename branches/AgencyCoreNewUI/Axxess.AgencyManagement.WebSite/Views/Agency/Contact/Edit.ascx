﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyContact>" %>
<span class="wintitle">Edit Contact | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Contact/Update", "Agency", FormMethod.Post, new { @id = "EditContact_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditContact_Id" }) %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditContact_CompanyName" class="fl strong">Company Name</label>
                <div class="fr"><%= Html.TextBox("CompanyName", Model.CompanyName, new { @id = "EditContact_CompanyName", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditContact_FirstName" class="fl strong">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditContact_FirstName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditContact_LastName" class="fl strong">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditContact_LastName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditContact_Email" class="fl strong">Contact Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditContact_EmailAddress", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditContact_Type" class="fl strong">Contact Type</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.ContactTypes, "ContactType", Model.ContactType, new { @id = "EditContact_Type", @class = "required" })%>
                </div>
                <div class="clr"></div>
                <div id="EditContact_TypeMore" class="fr">
                    <label for="EditContact_OtherContactType"><em>Specify</em></label>
                    <%= Html.TextBox("ContactTypeOther", Model.ContactTypeOther, new { @id = "EditContact_OtherContactType", @maxlength = "100" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditContact_AddressLine1" class="fl strong">Address</label>
                <div class="fr">
                    <%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditContact_AddressLine1", @maxlength = "75", @class = "required" })%><br />
                    <%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Contact_AddressLine2", @maxlength = "75" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditContact_AddressCity", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditContact_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "EditContact_AddressStateCode", @class = "state required" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditContact_AddressZipCode", @class = "numeric required zip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_PhonePrimary1" class="fl strong">Office Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhonePrimaryArray", Model.PhonePrimary.IsNotNullOrEmpty() ? Model.PhonePrimary.Substring(0, 3) : "", new { @id = "EditContact_PhonePrimary1", @class = "numeric phone-short required", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", Model.PhonePrimary.IsNotNullOrEmpty() ? Model.PhonePrimary.Substring(3, 3) : "", new { @id = "EditContact_PhonePrimary2", @class = "numeric phone-short required", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", Model.PhonePrimary.IsNotNullOrEmpty() ? Model.PhonePrimary.Substring(6, 4) : "", new { @id = "EditContact_PhonePrimary3", @class = "numeric phone-long required", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_PhoneAlternate1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneAlternateArray", Model.PhoneAlternate.IsNotNullOrEmpty() ? Model.PhoneAlternate.Substring(0, 3) : "", new { @id = "EditContact_PhoneAlternate1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", Model.PhoneAlternate.IsNotNullOrEmpty() ? Model.PhoneAlternate.Substring(3, 3) : "", new { @id = "EditContact_PhoneAlternate2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", Model.PhoneAlternate.IsNotNullOrEmpty() ? Model.PhoneAlternate.Substring(6, 4) : "", new { @id = "Edit_Contact_PhoneAlternate3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditContact_Fax1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "EditContact_Fax1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "EditContact_Fax2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "EditContact_Fax3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>