﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Contact | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Contact/Create", "Agency", FormMethod.Post, new { @id = "NewContact_Form" })) { %>
    <fieldset>
        <legend>Contact Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewContact_CompanyName" class="fl strong">Company Name</label>
                <div class="fr"><%= Html.TextBox("CompanyName", "", new { @id = "NewContact_CompanyName", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_FirstName" class="fl strong">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", "", new { @id = "NewContact_FirstName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_LastName" class="fl strong">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", "", new { @id = "NewContact_LastName", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_Email" class="fl strong">Contact Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewContact_EmailAddress", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_Type" class="fl strong">Contact Type</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.ContactTypes, "ContactType", "", new { @id = "NewContact_Type", @class = "required notzero" })%></div>
                <div class="clr"></div>
                <div id="NewContact_TypeMore" class="fr">
                    <label for="NewContact_OtherContactType"><em>Specify</em></label>
                    <%= Html.TextBox("ContactTypeOther", "", new { @id = "NewContact_OtherContactType", @maxlength = "100" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewContact_AddressLine1" class="fl strong">Address</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "NewContact_AddressLine1", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "NewContact_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "NewContact_AddressCity", @maxlength = "75", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewContact_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "NewContact_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "NewContact_AddressZipCode", @class = "numeric required zip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewContact_PhonePrimary1" class="fl strong">Office Phone</label>
                <div class="fr">
                    <input type="text" class="numeric required phone-short" name="PhonePrimaryArray" id="NewContact_PhonePrimary1" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-short" name="PhonePrimaryArray" id="NewContact_PhonePrimary2" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-long" name="PhonePrimaryArray" id="NewContact_PhonePrimary3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewContact_PhoneAlternate1" class="fl strong">Mobile Phone</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="PhoneAlternateArray" id="NewContact_PhoneAlternate1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="PhoneAlternateArray" id="NewContact_PhoneAlternate2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="PhoneAlternateArray" id="NewContact_PhoneAlternate3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewContact_Fax1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewContact_Fax1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewContact_Fax2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="FaxNumberArray" id="NewContact_Fax3" maxlength="4" />
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>