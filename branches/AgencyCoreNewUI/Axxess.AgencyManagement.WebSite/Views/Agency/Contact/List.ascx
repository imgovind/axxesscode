﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Contact | <%= Current.AgencyName%></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-contact">New Contact</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencyContact>().Name("List_Contact").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(c => c.DisplayName).Title("Name").Width(150).Sortable(true);
            columns.Bound(c => c.CompanyName).Title("Company").Sortable(true);
            columns.Bound(c => c.ContactType).Title("Type").Width(150).Sortable(true);
            columns.Bound(c => c.PhonePrimaryFormatted).Title("Phone").Width(120).Sortable(false);
            columns.Bound(c => c.EmailAddress).ClientTemplate("<a class=\"link\" href=\"mailto:<#=EmailAddress#>\"><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
            columns.Bound(c => c.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Contact.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Contact.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Contact/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>