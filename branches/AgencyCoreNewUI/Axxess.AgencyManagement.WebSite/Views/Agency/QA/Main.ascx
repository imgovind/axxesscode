﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<span class="wintitle">Quality Assurance (QA) Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
    <%= Html.Hidden("CaseManagement_GroupName", ViewData["GroupName"].ToString())%>
    <%= Html.Hidden("CaseManagement_SortParams", string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]))%>
    <div class="fr buttons">
        <ul><li><a status="Refresh Grid" class="grid-refresh">Refresh</a></li></ul>
        <br />
        <ul><li><a status="Export Grid as Spreadsheet (Excel Format)" class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="grid-controls">
        <div class="ac">
            <div class="filter">
                <label for="CaseManagement_BranchCode" class="strong">Branch</label>
                <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "CaseManagement_BranchCode", @status = "Select Branch to Filter Results" })%>
            </div>
            <div class="filter">
                <label for="CaseManagement_Status" class="strong">Status</label>
                <select id="CaseManagement_Status" name="StatusId" class="PatientStatusDropDown" status="Select Patient Status to Filter Results">
                    <option value="0">All</option>
                    <option value="1" selected="selected">Active</option>
                    <option value="2">Discharged</option>
                </select>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a status="Organize Grid by Patient" class="sort">Group By Patient</a></li>
                <li><a status="Organize Grid by Date" class="sort">Group By Date</a></li>
                <li><a status="Organize Grid by Task Type" class="sort">Group By Task</a></li>
                <li><a status="Organize Grid by Clinician" class="sort">Group By Clinician</a></li>
            </ul>
        </div>
    </fieldset>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post)) { %>
    <div id="CaseManagement_Content"><% Html.RenderPartial("QA/List", Model); %></div>
    <div class="buttons abs-bottom wrapper">
        <%= Html.Hidden("CommandType", "", new { @id = "BulkUpdate_Type" }) %>
        <ul>
            <li><a status="Bulk Approve All Selected" class="bulk">Approve Selected</a></li>
            <li><a status="Bulk Return All Selected" class="bulk">Return Selected</a></li>
        </ul>
    </div>
<%  } %>
</div>