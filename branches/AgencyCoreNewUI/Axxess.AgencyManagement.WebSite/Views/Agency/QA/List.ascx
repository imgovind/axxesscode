﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<%= Html.Telerik().Grid(Model).Name("CaseManagementGrid").HtmlAttributes(new { @class = "bottom-gap args" }).Columns(columns => {
        columns.Bound(s => s.CustomValue).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}' class='required' />", s.CustomValue)).ClientTemplate("<input name='CustomValue' type='checkbox' value='<#= CustomValue #>'/>").Title("").Width("2%").HtmlAttributes(new { @class = "ac" }).Sortable(false);
        columns.Bound(s => s.PatientName).Width("18%");
        columns.Bound(s => s.EventDate).Width("8%");
        columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).ClientTemplate("<#=PrintUrl#>").Title("Task").Width("25%");
        columns.Bound(s => s.Status).Width("20%");
        columns.Bound(s => s.RedNote).Template(s => string.Format("<a class=\"sticky-note-icon red\" tooltip=\"{0}\"></a>", s.RedNote)).ClientTemplate("<a class=\"sticky-note-icon red\" tooltip=\"<#=RedNote#>\"></a>").Title(" ").Width("3%");
        columns.Bound(s => s.YellowNote).Template(s => string.Format("<a class=\"sticky-note-icon\" tooltip=\"{0}\"></a>", s.YellowNote)).ClientTemplate("<a class=\"sticky-note-icon\" tooltip=\"<#=YellowNote#>\"></a>").Title(" ").Width("3%");
        columns.Bound(s => s.BlueNote).Template(s => string.Format("<a class=\"sticky-note-icon blue\" tooltip=\"{0}\"></a>", s.BlueNote)).ClientTemplate("<a class=\"sticky-note-icon blue\" tooltip=\"<#=BlueNote#>\"></a>").Title(" ").Width("3%");
        columns.Bound(s => s.UserName).Width("18%");
    }).Groupable(settings => settings.Groups(groups => {
        var groupName = ViewData["GroupName"].ToString();
        if (groupName == "PatientName") groups.Add(s => s.PatientName);
        else if (groupName == "EventDate") groups.Add(s => s.EventDate);
        else if (groupName == "DisciplineTaskName") groups.Add(s => s.TaskName);
        else if (groupName == "UserName") groups.Add(s => s.UserName);
        else groups.Add(s => s.EventDate);
    })).Sortable(sorting => sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "EventDate") {
            if (sortDirection == "ASC") order.Add(o => o.EventDate).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.EventDate).Descending();
        } else if (sortName == "TaskName") {
            if (sortDirection == "ASC") order.Add(o => o.TaskName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.TaskName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        }
    })).Scrollable().Footer(false) %>