﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Physician | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Physician/Create", "Agency", FormMethod.Post, new { @id = "NewPhysician_Form" })) { %>
    <fieldset>
        <legend>Search Physician</legend>
        <div class="wide column">
            <div class="narrow row">
                <label for="NewPhysician_NpiSearch" class="fl strong">NPI Number</label>
                <div class="fr"><input type="text" id="NewPhysician_NpiSearch" name="NewPhysician_NpiSearch" maxlength="10" class="numeric longest" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewPhysician_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", "", new { @id = "NewPhysician_FirstName", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_MiddleIntial" class="fl strong">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleName", "", new { @id = "NewPhysician_MiddleIntial", @class = "mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", "", new { @id = "NewPhysician_LastName", @class = "required", @maxlength = "50" })%></div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="NewPhysician_Credentials" class="fl strong">Credentials</label>
                <div class="fr"><%= Html.TextBox("Credentials", "", new { @id = "NewPhysician_Credentials", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_NpiNumber" class="fl strong">NPI No</label>
                <div class="fr"><%= Html.TextBox("NPI", "", new { @id = "NewPhysician_NpiNumber", @class = "numeric required", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_PecosCheck" class="fl strong">PECOS Verification</label>
                <div id="NewPhysician_PecosCheck" class="fr">Not Checked</div>
            </div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row">
                <label for="NewPhysician_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "NewPhysician_AddressLine1", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "NewPhysician_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "NewPhysician_AddressCity", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysician_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "NewPhysician_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "NewPhysician_AddressZipCode", @class = "required numeric zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewPhysician_Phone1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <input type="text" class="required numeric phone-short" name="PhoneWorkArray" id="NewPhysician_Phone1" maxlength="3" />
                    -
                    <input type="text" class="required numeric phone-short" name="PhoneWorkArray" id="NewPhysician_Phone2" maxlength="3" />
                    -
                    <input type="text" class="required numeric phone-long" name="PhoneWorkArray" id="NewPhysician_Phone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewPhysician_AltPhone1" class="fl strong">Alternate Phone</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="PhoneAltArray" id="NewPhysician_AltPhone1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="PhoneAltArray" id="NewPhysician_AltPhone2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="PhoneAltArray" id="NewPhysician_AltPhone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewPhysician_Fax1" class="fl strong">Fax</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewPhysician_Fax1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewPhysician_Fax2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="FaxNumberArray" id="NewPhysician_Fax3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewPhysician_Email" class="fl strong">E-mail</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewPhysician_Email", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("PhysicianAccess", false, new { @id = "NewPhysician_PhysicianAccess" }) %>
                        <label for="NewPhysician_PhysicianAccess">
                            Grant Physician Access<br />
                            <em>(E-mail Address Required)</em>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>