﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<span class="wintitle">Physician List | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-physician">New Physician</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencyPhysician>().Name("ListPhysician").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(p => p.NPI).Title("NPI").Sortable(true).Width(95);
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(true);
            columns.Bound(p => p.EmailAddress).Title("Email").Sortable(true);
            columns.Bound(p => p.AddressFull).Title("Address").Sortable(true);
            columns.Bound(p => p.PhoneWorkFormatted).Title("Phone Number").Sortable(false).Width(110);
            columns.Bound(p => p.PhoneAlternateFormatted).Title("Alternate Number").Sortable(false).Width(115);
            columns.Bound(p => p.FaxNumberFormatted).Title("Fax Number").Sortable(false).Width(110);
            columns.Bound(p => p.PhysicianAccess).Title("Phy. Access").Sortable(false).Width(90);
            columns.Bound(p => p.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Physician.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Physician.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>").Sortable(false).Title("Action").Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Physician/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>