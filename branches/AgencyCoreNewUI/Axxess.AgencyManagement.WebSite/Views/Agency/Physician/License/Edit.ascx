﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicianLicense>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Physician/License/Update", "Agency", FormMethod.Post, new { @id = "EditPhysicianLicense_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditPhysicianLicense_Id" })%>
    <%= Html.Hidden("PhysicianId", Model.PhysicianId, new { @id = "EditPhysicianLicense_PhysicianId" })%>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="EditPhysicianLicense_LicenseNumber" class="fl strong">License Number</label>
                <div class="fr"><%= Html.TextBox("LicenseNumber", Model.LicenseNumber, new { @id = "EditPhysicianLicense_LicenseNumber", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysicianLicense_State" class="fl strong">State</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.States, "State", Model.State, new { @id = "EditPhysicianLicense_State", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysicianLicense_InitiationDate" class="fl strong">Initiation Date</label>
                <div class="fr"><%= Html.TextBox("InitiationDate", Model.InitiationDateFormatted, new { @id = "EditPhysicianLicense_InitiationDate", @class = "required date-picker" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysicianLicense_ExpirationDate" class="fl strong">Expiration Date</label>
                <div class="fr"><%= Html.TextBox("ExpirationDate", Model.ExpirationDateFormatted, new { @id = "EditPhysicianLicense_ExpirationDate", @class = "required date-picker" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>