﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Physician/License/Create", "Agency", FormMethod.Post, new { @id = "NewPhysicianLicense_Form" })) { %>
    <%= Html.Hidden("PhysicianId", Model, new { @id = "NewPhysicianLicense_PhysicianId" })%>
    <fieldset>
        <div class="wide column">
            <div class="row">
                <label for="NewPhysicianLicense_LicenseNumber" class="fl strong">License Number</label>
                <div class="fr"><%= Html.TextBox("LicenseNumber", "", new { @id = "NewPhysicianLicense_LicenseNumber", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysicianLicense_State" class="fl strong">State</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.States, "State", "", new { @id = "NewPhysicianLicense_State", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysicianLicense_InitiationDate" class="fl strong">Initiation Date</label>
                <div class="fr"><%= Html.TextBox("InitiationDate", "", new { @id = "NewPhysicianLicense_InitiationDate", @class = "required date-picker" })%></div>
            </div>
            <div class="row">
                <label for="NewPhysicianLicense_ExpirationDate" class="fl strong">Expiration Date</label>
                <div class="fr"><%= Html.TextBox("ExpirationDate", "", new { @id = "NewPhysicianLicense_ExpirationDate", @class = "required date-picker" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>