﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<span class="wintitle">Edit Physician | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Physician/Update", "Agency", FormMethod.Post, new { @id = "EditPhysician_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditPhysician_Id" })%>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditPhysician_FirstName", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_MiddleIntial" class="fl strong">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleName", Model.MiddleName, new { @id = "EditPhysician_MiddleIntial", @class = "mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditPhysician_LastName", @class = "required", @maxlength = "50" })%></div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="EditPhysician_Credentials" class="fl strong">Credentials</label>
                <div class="fr"><%= Html.TextBox("Credentials", Model.Credentials, new { @id = "EditPhysician_Credentials", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_NpiNumber" class="fl strong">NPI</label>
                <div class="fr"><%= Html.TextBox("NPI", Model.NPI, new { @id = "EditPhysician_NpiNumber", @class = "numeric", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <div id="EditPhysician_PecosCheck" class="hidden"></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditPhysician_AddressLine1", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditPhysician_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditPhysician_AddressCity", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditPhysician_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "EditPhysician_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditPhysician_AddressZipCode", @class = "required numeric zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditPhysician_Phone1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length >= 3 ? Model.PhoneWork.Substring(0, 3) : string.Empty, new { @id = "EditPhysician_Phone1", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length >= 6 ? Model.PhoneWork.Substring(3, 3) : string.Empty, new { @id = "EditPhysician_Phone2", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length >= 10 ? Model.PhoneWork.Substring(6, 4) : string.Empty, new { @id = "EditPhysician_Phone3", @class = "numeric required phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPhysician_AltPhone1" class="fl strong">Alternate Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneAltArray", Model.PhoneAlternate.IsNotNullOrEmpty() && Model.PhoneAlternate.Length >= 3 ? Model.PhoneAlternate.Substring(0, 3) : string.Empty, new { @id = "EditPhysician_AltPhone1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAltArray", Model.PhoneAlternate.IsNotNullOrEmpty() && Model.PhoneAlternate.Length >= 6 ? Model.PhoneAlternate.Substring(3, 3) : string.Empty, new { @id = "EditPhysician_AltPhone2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAltArray", Model.PhoneAlternate.IsNotNullOrEmpty() && Model.PhoneAlternate.Length >= 10 ? Model.PhoneAlternate.Substring(6, 4) : string.Empty, new { @id = "EditPhysician_AltPhone3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPhysician_Fax1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 3 ? Model.FaxNumber.Substring(0, 3) : string.Empty, new { @id = "EditPhysician_Fax1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 6 ? Model.FaxNumber.Substring(3, 3) : string.Empty, new { @id = "EditPhysician_Fax2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 10 ? Model.FaxNumber.Substring(6, 4) : string.Empty, new { @id = "EditPhysician_Fax3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditPhysician_Email" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditPhysician_Email", @class = "email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("PhysicianAccess", Model.PhysicianAccess, new { @id = "EditPhysician_PhysicianAccess" }) %>
                        <label for="EditPhysician_PhysicianAccess">
                            Grant Physician Access<br />
                            <em>(E-mail Address Required)</em>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Licenses</legend>
        <div class="wide column">
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician-license" status="New Physician License">New Physician License</a></div>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<PhysicianLicense>().Name("EditPhysician_Licenses").HtmlAttributes(new { @class = "hidden" }).Columns(columns => {
                        columns.Bound(l => l.LicenseNumber).ReadOnly().Sortable(false);
                        columns.Bound(l => l.State).ReadOnly().Sortable(false);
                        columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Issue Date").Sortable(false);
                        columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
                        columns.Bound(l => l.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Physician.License.Edit('<#=Id#>','" + Model.Id + "');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Physician.License.Delete('<#=Id#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Width(135);
                    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("PhysicianLicenseGrid", "Agency", new { physicianId = Model.Id })).Sortable().Footer(false)%>
                <h2 class="hidden blue">No Licenses Found</h2>
            </div>
        </div>
    </fieldset>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
