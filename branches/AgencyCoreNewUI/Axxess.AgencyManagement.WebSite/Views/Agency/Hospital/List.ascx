﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Hospital | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-hospital">New Hospital</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<AgencyHospital>().Name("List_AgencyHospital").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(c => c.Name).Title("Hospital Name").Width(150).Sortable(true);
            columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(true);
            columns.Bound(c => c.AddressFull).Title("Address").Sortable(false);
            columns.Bound(c => c.PhoneFormatted).Title("Phone").Width(120).Sortable(false);
            columns.Bound(c => c.FaxFormatted).Title("Fax Number").Width(120).Sortable(false);
            columns.Bound(c => c.EmailAddress).ClientTemplate("<a class=\"link\" href=\"mailto:<#=EmailAddress#>\"><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
            columns.Bound(c => c.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Hospital.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Hospital.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Hospital/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>