﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyHospital>" %>
<span class="wintitle">Edit Hospital | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Hospital/Update", "Agency", FormMethod.Post, new { @id = "EditHospital_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditHospital_Id" })%>
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditHospital_Name" class="fl strong">Hospital Name</label>
                <div class="fr"><%= Html.TextBox("Name", Model.Name, new { @id = "EditHospital_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditHospital_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditHospital_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditHospital_AddressCity", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "EditHospital_AddressStateCode", @class = "state required" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditHospital_AddressZipCode", @class = "required numeric zip", @maxlength = "9" })%>
                </div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="EditHospital_ContactPersonFirstName" class="fl strong">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "EditHospital_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_ContactPersonLastName" class="fl strong">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "EditHospital_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_EmailAddress" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditHospital_EmailAddress", @class = "email", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditHospital_PhoneArray1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(0, 3) : "", new { @id = "EditHospital_PhoneArray1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(3, 3) : "", new { @id = "EditHospital_PhoneArray2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(6, 4) : "", new { @id = "EditHospital_PhoneArray3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditHospital_FaxNumberArray1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "EditHospital_FaxNumberArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "EditHospital_FaxNumberArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "EditHospital_FaxNumberArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div> 
        </div>
        <div class="wide column">
            <div class="row">
                <label for="EditHospital_Comments">Comments</label>
                <div class="ac"><%= Html.TextArea("Comment", Model.Comment, new { @id = "EditHospital_Comments", @maxcharacters = "500" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>