﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Hospital | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Hospital/Create", "Agency", FormMethod.Post, new { @id = "NewHospital_Form" })) { %>
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewHospital_Name" class="fl strong">Hospital Name</label>
                <div class="fr"><%= Html.TextBox("Name", "", new { @id = "NewHospital_Name", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", "", new { @id = "NewHospital_AddressLine1", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", "", new { @id = "NewHospital_AddressLine2", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", "", new { @id = "NewHospital_AddressCity", @class = "required", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_AddressZipCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "NewHospital_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "NewHospital_AddressZipCode", @class = "numeric required zip", @maxlength = "9" })%>
                </div>
            </div>
         </div>   
        <div class="column">   
            <div class="row">
                <label for="NewHospital_ContactPersonFirstName" class="fl strong">Contact First Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonFirstName", "", new { @id = "NewHospital_ContactPersonFirstName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_ContactPersonLastName" class="fl strong">Contact Last Name</label>
                <div class="fr"><%= Html.TextBox("ContactPersonLastName", "", new { @id = "NewHospital_ContactPersonLastName", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_EmailAddress" class="fl strong">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewHospital_EmailAddress", @class = "email", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewHospital_PhoneArray1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <input type="text" class="required numeric phone-short" name="PhoneArray" id="NewHospital_PhoneArray1" maxlength="3" />
                    -
                    <input type="text" class="required numeric phone-short" name="PhoneArray" id="NewHospital_PhoneArray2" maxlength="3" />
                    -
                    <input type="text" class="required numeric phone-long" name="PhoneArray" id="NewHospital_PhoneArray3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewHospital_FaxNumberArray1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewHospital_FaxNumberArray1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewHospital_FaxNumberArray2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="FaxNumberArray" id="NewHospital_FaxNumberArray3" maxlength="4" />
                </div>
            </div> 
        </div>
        <div class="wide column">
            <div class="row">
                <label for="Comment" class="strong">Comment</label>
                <div class="ac"><%= Html.TextArea("Comment", "", new { @id = "NewHospital_Comment", @class = "tall", @maxcharacters = "500" })%></div>
            </div>
        </div>
    </fieldset> 
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>