﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Incident>" %>
<%  if (Model != null) Model.SignatureDate = DateTime.Today; %>
<span class="wintitle">Edit Incident/Accident Report | <%= Current.AgencyName%></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Incident/Update", "Agency", FormMethod.Post, new { @id = "EditIncident_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditIncident_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "EditIncident_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "EditIncident_UserId" })%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId.IsEmpty() ? string.Empty : Model.EpisodeId.ToString(), new { @id = "EditIncident_EpisodeId" })%>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditIncident_PatientId" class="fl strong">Patient Name</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
    <%  if (Model.EpisodeId.IsEmpty()) { %>
            <div class="row">
                <label class="fl strong">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "EditIncident_EpisodeId", @class = "required notzero" })%></div>
            </div>
    <%  } else { %>
            <div class="row">
                <label class="fl strong">Episode Associated</label>
                <div class="fr"><%=string.Format("{0}-{1}",Model.EpisodeStartDate,Model.EpisodeEndDate) %></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="EditIncident_PhysicianId" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "EditIncident_PhysicianId", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditIncident_IncidentDate" class="fl strong">Date of Incident</label>
                <div class="fr"><input type="text" class="date-picker required" name="IncidentDate" id="EditIncident_IncidentDate" value="<%= Model.IncidentDate.IsValid() ? Model.IncidentDate.ToShortDateString() : string.Empty %>" /></div>
            </div>
            <div class="row">
                <label for="EditIncident_IncidentType" class="fl strong">Type of Incident</label>
                <div class="fr"><%= Html.TextBox("IncidentType", Model.IncidentType, new { @id = "EditIncident_IncidentType", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="EditIncident_MDNotifiedYes" class="fl strong">MD Notified</label>
                <div class="fr radio">
                    <%= Html.RadioButton("MDNotified", "Yes", Model.MDNotified == "Yes", new { @id = "EditIncident_MDNotifiedYes" })%>
                    <label for="EditIncident_MDNotifiedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", Model.MDNotified == "No", new { @id = "EditIncident_MDNotifiedNo" })%>
                    <label for="EditIncident_MDNotifiedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", Model.MDNotified == "NA", new { @id = "EditIncident_MDNotifiedNA", @checked = "checked" })%>
                    <label for="EditIncident_MDNotifiedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_FamilyNotifiedYes" class="fl strong">Family/CG Notified</label>
                <div class="fr radio">
                    <%= Html.RadioButton("FamilyNotified", "Yes", Model.FamilyNotified == "Yes", new { @id = "EditIncident_FamilyNotifiedYes" })%>
                    <label for="EditIncident_FamilyNotifiedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("FamilyNotified", "No", Model.FamilyNotified == "No", new { @id = "EditIncident_FamilyNotifiedNo" })%>
                    <label for="EditIncident_FamilyNotifiedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("FamilyNotified", "NA", Model.FamilyNotified == "NA", new { @id = "EditIncident_FamilyNotifiedNA", @checked = "checked" })%>
                    <label for="EditIncident_FamilyNotifiedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_NewOrdersYes" class="fl strong">New Orders</label>
                <div class="fr radio">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", Model.NewOrdersCreated == "Yes", new { @id = "EditIncident_NewOrdersYes" })%>
                    <label for="EditIncident_NewOrdersYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", Model.NewOrdersCreated == "No", new { @id = "EditIncident_NewOrdersNo" })%>
                    <label for="EditIncident_NewOrdersNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", Model.NewOrdersCreated == "NA", new { @id = "EditIncident_NewOrdersNA", @checked = "checked" })%>
                    <label for="EditIncident_NewOrdersNA" class="fixed shorter">N/A</label>
                </div>
            </div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) involved:</legend>
        <%  string[] individualsInvolved = Model.IndividualInvolved != null && Model.IndividualInvolved != "" ? Model.IndividualInvolved.Split(';') : new string[] {}; %>
        <%= Html.Hidden("IndividualInvolvedArray") %>
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='EditIncident_IndividualInvolved1' type='checkbox' value='Patient' name='IndividualInvolvedArray' class='required' {0} />", individualsInvolved.Contains("Patient").ToChecked()) %>
                        <label for="EditIncident_IndividualInvolved1">Patient</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditIncident_IndividualInvolved2' type='checkbox' value='Caregiver' name='IndividualInvolvedArray' class='required' {0} />", individualsInvolved.Contains("Caregiver").ToChecked()) %>
                        <label for="EditIncident_IndividualInvolved2">Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditIncident_IndividualInvolved3' type='checkbox' value='Employee/Contractor' name='IndividualInvolvedArray' class='required' {0} />", individualsInvolved.Contains("Employee/Contractor").ToChecked()) %>
                        <label for="EditIncident_IndividualInvolved3">Employee/Contractor</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditIncident_IndividualInvolved4' type='checkbox' value='Other' name='IndividualInvolvedArray' class='required' {0} />", individualsInvolved.Contains("Other").ToChecked()) %>
                        <label for="EditIncident_IndividualInvolved4">Other</label>
                        <div class="more"><%= Html.TextBox("IndividualInvolvedOther", Model.IndividualInvolvedOther, new { @id = "EditIncident_IndividualInvolvedOther", @maxlength = "100" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditIncident_Description" class="strong">Describe Incident/Accident</label>
                <div class="ac">
                    <%= Html.Templates("DescriptionTemplates")%>
                    <%= Html.TextArea("Description", Model.Description, new { @id = "EditIncident_Description", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_ActionTaken" class="strong">Action Taken/Interventions Performed</label>
                <div class="ac">
                    <%= Html.Templates("ActionTakenTemplates") %>
                    <%= Html.TextArea("ActionTaken", Model.ActionTaken, new { @id = "EditIncident_ActionTaken", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_Narrative" class="strong">Narrative</label>
                <div class="ac">
                    <%= Html.Templates("NarrativeTemplates") %>
                    <%= Html.TextArea("Orders", Model.Orders, new { @id = "EditIncident_Narrative", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditIncident_FollowUp" class="strong">Follow Up</label>
                <div class="ac"><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "EditIncident_FollowUp", @class = "taller", @maxcharacters = "1000" })%></div>
            </div>
        </div>  
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="EditIncident_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "EditIncident_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="EditIncident_SignatureDate" class="bigtext fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="EditIncident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "EditIncident_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>