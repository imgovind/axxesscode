﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Incident/Accident Report | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Incident/Create", "Agency", FormMethod.Post, new { @id = "NewIncident_Form" })) { %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewIncident_PatientId" class="fl strong">Patient</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "NewIncident_PatientId", @class="required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewIncident_EpisodeList" class="fl strong">Episode Associated</label>
                <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "NewIncident_EpisodeList", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewIncident_PhysicianId" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("PhysicianId", "", new { @id = "NewIncident_PhysicianId", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
         </div>
         <div class="column">
            <div class="row">
                <label for="NewIncident_IncidentDate" class="fl strong">Date of Incident</label>
                <div class="fr"><input type="text" class="date-picker required" name="IncidentDate" id="NewIncident_IncidentDate" /></div>
            </div>
            <div class="row">
                <label for="NewIncident_IncidentType" class="fl strong">Type of Incident</label>
                <div class="fr"><%= Html.TextBox("IncidentType", "", new { @id = "NewIncident_IncidentType", @class = "required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewIncident_MDNotifiedYes" class="fl strong">MD Notified</label>
                <div class="fr radio">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "NewIncident_MDNotifiedYes" })%>
                    <label for="NewIncident_MDNotifiedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "NewIncident_MDNotifiedNo" })%>
                    <label for="NewIncident_MDNotifiedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "NewIncident_MDNotifiedNA", @checked = "checked" })%>
                    <label for="NewIncident_MDNotifiedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_FamilyNotifiedYes" class="fl strong">Family/CG Notified</label>
                <div class="fr radio">
                    <%= Html.RadioButton("FamilyNotified", "Yes", new { @id = "NewIncident_FamilyNotifiedYes" })%>
                    <label for="NewIncident_FamilyNotifiedYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("FamilyNotified", "No", new { @id = "NewIncident_FamilyNotifiedNo" })%>
                    <label for="NewIncident_FamilyNotifiedNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("FamilyNotified", "NA", new { @id = "NewIncident_FamilyNotifiedNA", @checked = "checked" })%>
                    <label for="NewIncident_FamilyNotifiedNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_NewOrdersYes" class="fl strong">New Orders</label>
                <div class="fr radio">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "NewIncident_NewOrdersYes" })%>
                    <label for="NewIncident_NewOrdersYes" class="fixed shorter">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "NewIncident_NewOrdersNo" })%>
                    <label for="NewIncident_NewOrdersNo" class="fixed shorter">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "NewIncident_NewOrdersNA", @checked = "checked" })%>
                    <label for="NewIncident_NewOrdersNA" class="fixed shorter">N/A</label>
                </div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-order" status="Add New Order">Add New Order</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) involved:</legend>
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input id="NewIncident_IndividualInvolved1" type="checkbox" value="Patient" name="IndividualInvolvedArray" />
                        <label for="NewIncident_IndividualInvolved1">Patient</label>
                    </div>
                    <div class="option">
                        <input id="NewIncident_IndividualInvolved2" type="checkbox" value="Caregiver" name="IndividualInvolvedArray" />
                        <label for="NewIncident_IndividualInvolved2">Caregiver</label>
                    </div>
                    <div class="option">
                        <input id="NewIncident_IndividualInvolved3" type="checkbox" value="Employee/Contractor" name="IndividualInvolvedArray" />
                        <label for="NewIncident_IndividualInvolved3">Employee/Contractor</label>
                    </div>
                    <div class="option">
                        <input id="NewIncident_IndividualInvolved4" type="checkbox" value="Other" name="IndividualInvolvedArray"/>
                        <label for="NewIncident_IndividualInvolved4">Other</label>
                        <div class="more">
                            <%= Html.TextBox("IndividualInvolvedOther", "", new { @id = "NewIncident_IndividualInvolvedOther", @maxlength = "100" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewIncident_Description" class="strong">Describe Incident/Accident</label>
                <div class="ac">
                    <%= Html.Templates("DescriptionTemplates")%>
                    <%= Html.TextArea("Description", new { @id = "NewIncident_Description", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_ActionTaken" class="strong">Action Taken/Interventions Performed</label>
                <div class="ac">
                    <%= Html.Templates("ActionTakenTemplates") %>
                    <%= Html.TextArea("ActionTaken", new { @id = "NewIncident_ActionTaken", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_Narrative" class="strong">Narrative</label>
                <div class="ac">
                    <%= Html.Templates("NarrativeTemplates") %>
                    <%= Html.TextArea("Orders", new { @id = "NewIncident_Narrative", @class = "taller", @maxcharacters = "1000" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewIncident_FollowUp" class="strong">Follow Up</label>
                <div class="ac"><%= Html.TextArea("FollowUp", new { @id = "NewIncident_FollowUp", @class = "taller", @maxcharacters = "1000" })%></div>
            </div>
        </div>  
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="NewIncident_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password("SignatureText", "", new { @id = "NewIncident_ClinicianSignature", @class = "complete-required" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="NewIncident_SignatureDate" class="bigtext fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="SignatureDate" id="NewIncident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "NewIncident_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>