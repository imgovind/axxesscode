﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Incidents | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-incident">New Incident Report</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Incident>().Name("List_IncidentReport").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
            columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
            columns.Bound(i => i.IncidentType).Title("Type of Incident").Sortable(true);
            columns.Bound(i => i.IncidentDateFormatted).Title("Incident Date").Sortable(true);
            columns.Bound(i => i.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
            columns.Bound(i => i.Id).ClientTemplate("<a class=\"link\" onclick=\"Agency.Incident.Edit('<#=Id#>');return false\">Edit</a>").Sortable(false).Title("Action").Width(50);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Incident/Grid", "Agency")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>