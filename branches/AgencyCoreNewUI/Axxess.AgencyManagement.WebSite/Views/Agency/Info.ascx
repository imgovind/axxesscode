﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Manage Company Information | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("UpdateInfo", "Agency", FormMethod.Post, new { @id = "EditAgencyInfo_Form" })) { %>
<div class="form-wrapper">
    <div class="bigtext align-center"><%= Current.AgencyName%></div>
    <div class="align-center"><label for="Edit_AgencyInfo_LocationId">Agency Branch</label><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "Edit_AgencyInfo_LocationId", @class = "BranchLocation required" })%></div>
    <div id="Edit_AgencyInfo_Container" class="row">
        <% Html.RenderPartial("~/Views/Agency/InfoContent.ascx", new AgencyLocation()); %>
    </div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('EditCompanyInformation');">Close</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    Agency.LoadBranchContent($('#Edit_AgencyInfo_LocationId').val());
    $('#Edit_AgencyInfo_LocationId').change(function() { Agency.LoadBranchContent($(this).val()); });
</script>