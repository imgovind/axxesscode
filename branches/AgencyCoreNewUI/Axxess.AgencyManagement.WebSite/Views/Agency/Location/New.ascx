﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Location/Create", "Agency", FormMethod.Post, new { @id = "NewLocation_Form" })) { %>
<div class="wrapper main">
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_Location_Name" class="float-left">Location Name</label><div class="fr"> <%=Html.TextBox("Name", "", new { @id = "New_Location_Name", @class = "required", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_Location_CustomId" class="float-left">Custom Id</label><div class="fr"> <%=Html.TextBox("CustomId", "", new { @id = "New_Location_CustomId", @class = "required" })%></div></div>
         </div>   
        <div class="column"> <div class="row"><label for="New_Location_MedicareProviderNumber" class="float-left">Medicare Provider Number</label><div class="fr"><%=Html.TextBox("MedicareProviderNumber", "", new { @id = "New_Location_MedicareProviderNumber", @class = "required", @maxlength = "10" })%></div></div></div>
    </fieldset>  
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="New_Location_AddressLine1" class="float-left">Address Line 1</label><div class="fr"> <%=Html.TextBox("AddressLine1", "", new { @id = "New_Location_AddressLine1", @class = "required" })%></div></div>
            <div class="row"><label for="New_Location_AddressLine2" class="float-left">Address Line 2</label><div class="fr"> <%=Html.TextBox("AddressLine2", "", new { @id = "New_Location_AddressLine2" })%></div></div>
            <div class="row"><label for="New_Location_AddressCity" class="float-left">City</label><div class="fr"> <%=Html.TextBox("AddressCity", "", new { @id = "New_Location_AddressCity", @class = "required" })%></div></div>
            <div class="row"><label for="New_Location_AddressStateCode" class="float-left"> State, Zip</label><div class="fr"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Location_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="New_Location_PhoneArray1" class="float-left">Primary Phone</label><div class="fr"><input type="text" class="numeric required phone-short" name="PhoneArray" id="New_Location_PhoneArray1" maxlength="3" /> - <input type="text" class="numeric required phone-short" name="PhoneArray" id="New_Location_PhoneArray2" maxlength="3" /> - <input type="text" class="numeric required phone-long" name="PhoneArray" id="New_Location_PhoneArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Location_FaxNumberArray1" class="float-left">Fax Number</label><div class="fr"><input type="text" class="numeric phone-short" name="FaxNumberArray" id="New_Location_FaxNumberArray1" maxlength="3" /> - <input type="text" class="numeric phone-short" name="FaxNumberArray" id="New_Location_FaxNumberArray2" maxlength="3" /> - <input type="text" class="numeric phone-long" name="FaxNumberArray" id="New_Location_FaxNumberArray3" maxlength="4" /></div></div> 
            <div class="row"><label for="New_Location_IsMainOffice">Is Main Office?</label><div class="fr"><input type="checkbox" name="IsMainOffice" class="radio" id="New_Location_IsMainOffice" /></div></div>
        </div>
        <table class="form"><tbody><tr class="line-seperated vert"><td><label for="New_Location_Comments">Comments</label><div><%= Html.TextArea("Comments", "", new { @id = "New_Location_Comments" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newlocation');">Cancel</a></li></ul></div>
</div>
<%} %>