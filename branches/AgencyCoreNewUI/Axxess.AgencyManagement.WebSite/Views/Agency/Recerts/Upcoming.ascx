﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Upcoming Recerts | <%= Current.AgencyName %></span>
<%  var pageName = "AgencyUpcomingRecet"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul>
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName + "_BranchCode" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_InsuranceId" class="strong">Insurance</label>
            <%= Html.Insurances("InsuranceId", Model, new { @id = pageName + "_InsuranceId" })%>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<RecertEvent>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true).Width(20);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(10);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true).Width(20);
            columns.Bound(r => r.StatusName).Title("Status").Sortable(true).Width(15);
            columns.Bound(r => r.TargetDateFormatted).Title("Due Date").Width(10).Sortable(true);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsUpcomingGrid", "Agency", new { branchId = Guid.Empty, insuranceId = Model })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>