﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Past Due Recerts | <%= Current.AgencyName %></span>
<%  var pageName = "AgencyPastDueRecet"; %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul>
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
<%  } %>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter grid-search"></div>
        <div class="filter">
            <label for="<%= pageName %>_BranchId" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = pageName + "_BranchCode" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_InsuranceId" class="strong">Insurance</label>
            <%= Html.Insurances("InsuranceId", Model, new { @id = pageName + "_InsuranceId" })%>
        </div>
        <div class="filter">
            <label for="<%= pageName %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pageName %>_StartDate" />
            <label for="<%= pageName %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pageName %>_EndDate" />
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<RecertEvent>().Name(pageName + "_Grid").HtmlAttributes(new { @class = "bottom-bar args" }).Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true).Width(20);
            columns.Bound(r => r.PatientIdNumber).Title("MRN").Sortable(true).Width(15);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true).Width(20);
            columns.Bound(r => r.StatusName).Title("Status").Sortable(true).Width(20);
            columns.Bound(r => r.TargetDateFormatted).Title("Due Date").Width(15).Sortable(true);
            columns.Bound(r => r.DateDifference).Title("Past Dates").Width(10);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsPastDueGrid", "Agency", new { branchId = Guid.Empty, insuranceId = Model, startDate = DateTime.Now.AddDays(-60), endDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>