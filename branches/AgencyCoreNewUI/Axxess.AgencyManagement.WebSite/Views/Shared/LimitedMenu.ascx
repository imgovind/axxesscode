﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop();
// -- Build Menu -->
Acore.AddMenu({ Name: "Home", Id: "Home", IconX: "121", IconY: "86" });
    Acore.AddMenu({ Name: "My Account", Id: "Account", Parent: "home" });
Acore.AddMenu({ Name: "Create", Id: "Create", IconX: "140", IconY: "108" });
    Acore.AddMenu({ Name: "New", Id: "CreateNew", Parent: "create" });
Acore.AddMenu({ Name: "View", Id: "View", IconX: "161", IconY: "108" });
    <% if (Current.HasRight(Permissions.ViewLists)) { %>
    Acore.AddMenu({ Name: "Lists", Id: "ViewList", Parent: "view" });
    <% } %>
Acore.AddMenu({ Name: "Patients", Id: "Patients", IconX: "183", IconY: "108" });
Acore.AddMenu({ Name: "Help", Id: "Help", IconX: "186", IconY: "86" });
    Acore.AddMenuItem({ Name: "Discussion Forum", Href: "/Forum", Parent: "Help" });
    Acore.AddMenuItem({ Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "Help" });
    Acore.AddMenuItem({ Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "Help" });
    Acore.AddMenuItem({ Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "Help" });
// -- Home Menu -->
Acore.AddWindow({ Name: "Edit Profile", Id: "EditUserProfile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "Account" });
Acore.AddWindow({ Name: "Reset Signature", Id: "ResetSignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "Account", Resize: false, StatusBar: false, Centered: true, IgnoreMinSize: true, Height: "200px", Width: "500px" });
Acore.AddWindow({ Name: "Dashboard", MenuName: "My Dashboard", Id: "Dashboard", Url: "Home/Dashboard", OnLoad: Home.Init, Menu: "Home" });
Acore.AddWindow({ Name: "My Messages", Id: "MessageCenter", Url: "Message/Inbox", OnLoad: Message.Init, Menu: "Home" });
// -- Create Menu (Some Duplicated to Admin Menu) -->
Acore.AddWindow({ Name: "New Referral", MenuName: "Referral", Id: "NewReferral", Url: "Referral/New", OnLoad: Referral.InitNew, Menu: "CreateNew" });
Acore.AddWindow({ Name: "Compose Message", MenuName: "Message", Id: "NewMessage", Url: "Message/New", OnLoad: Message.InitNew, Menu: "CreateNew" });
// -- View Menu (Some Duplicated to Admin Menu) -->
Acore.AddWindow({ Name: "List Referrals", MenuName: "Referrals", Id: "ListReferrals", Url: "Referral/List", Menu: "ViewList" });
// -- Patient Menu -->
Acore.AddWindow({ Name: "Existing Referrals", Id: "ExistingReferrals", Url: "Referral/List", Menu: "Patients" });
// -- Help Menu -->
Acore.AddWindow({ Name: "After-Hours Support", Id: "AfterHoursSupport", Url: "Agency/AfterHoursSupport", Menu: "Help", Resize: false, StatusBar: false, Centered: true, IgnoreMinSize: true, Height: "300px", Width: "600px" });
// -- Windows not found in the menus -->
Acore.AddWindow({ Name: "Edit Referral", Id: "EditReferral" });
Acore.Activate({ GetRemoteContent: <%= AppSettings.GetRemoteContent %>, DefaultWindow: "Dashboard" });
</script>

