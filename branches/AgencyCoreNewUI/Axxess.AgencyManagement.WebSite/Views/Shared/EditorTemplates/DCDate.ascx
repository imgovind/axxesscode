﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>

<%= Html.Telerik().DatePicker()
        .Name("DCDate")
        .HtmlAttributes(new { id = "DCDate"})
        .Value(Model > DateTime.MinValue? Model : DateTime.Today)
%>