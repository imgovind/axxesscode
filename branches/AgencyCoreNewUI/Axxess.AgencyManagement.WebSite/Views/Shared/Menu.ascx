﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop({
    Menu: [
        { Name: "Home", Id: "Home", IconX: 121, IconY: 86 },
        { Name: "My Account", Id: "Account", Parent: "Home" },
        { Name: "Create", Id: "Create", IconX: 140, IconY: 108 },
        { Name: "New", Id: "CreateNew", Parent: "Create" },
        { Name: "View", Id: "View", IconX: 161, IconY: 108 },
        <% if (Current.HasRight(Permissions.ViewLists)) { %>
        { Name: "Lists", Id: "ViewList", Parent: "View" },
        <% } %>
        <% if (Current.HasRight(Permissions.AccessOrderManagementCenter)) { %>
        { Name: "Orders Management", Id: "OrdersManagement", Parent: "View" },
        <% } %>
        { Name: "Patients", Id: "Patients", IconX: 183, IconY: 108 },
        { Name: "Schedule", Id: "Schedule", IconX: 205, IconY: 108 },
        <% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
        { Name: "Billing", Id: "Billing", IconX: 227, IconY: 108 },
        { Name: "Medicare / Medicare HMO", Id: "BillingMedicare", Parent: "Billing" },
        { Name: "Managed Care", Id: "BillingManagedCare", Parent: "Billing" },
        <% } %>
        { Name: "Admin", Id: "Admin", IconX: 142, IconY: 86 },
        { Name: "New", Id: "AdminNew", Parent: "Admin" },
        <% if (Current.HasRight(Permissions.ViewLists)) { %>
        { Name: "Lists", Id: "AdminList", Parent: "Admin" },
        <% } %>
        <% if (Current.HasRight(Permissions.AccessReports)) { %>
        { Name: "Reports", Id: "Reports", IconX: 164, IconY: 86 },
        <% } %>
        { Name: "Help", Id: "Help", IconX: 186, IconY: 86 },
        { Name: "Training", Id: "Training", Parent: "Help" },
        { Name: "Support", Id: "Support", Parent: "Help" }
    ],
    Links: [
        { Name: "Training Manual", Href: '<%= string.Format("http://axxessweb.com/help?u={0}", SessionStore.SessionId) %>', Parent: "Training" },
        { Name: "Discussion Forum", Href: "/Forum", Parent: "Support" },
        { Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "Support" },
        { Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "Support" },
        { Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "Help" }
    ],
    Windows: [
        { Name: "Edit Profile", Id: "EditUserProfile", Url: "User/Profile/Edit", OnLoad: User.Profile.InitEdit, Menu: "Account" },
        { Name: "Reset Signature", Id: "ResetSignature", Url: "User/Signature/Reset", OnLoad: User.Signature.InitReset, Menu: "Account", Resize: false, Centered: true, IgnoreMinSize: true, StatusBar: false, Width: 500 },
        { Name: "Dashboard", MenuName: "My Dashboard", Id: "Dashboard", Url: "Home/Dashboard", OnLoad: Home.Init, Menu: "Home", AlwaysOnTop: true },
        { Name: "My Messages", Id: "Inbox", Url: "Message/Inbox", OnLoad: Message.InitInbox, Menu: "Home", VersitileHeight: false },
        { Name: "My Schedule/Tasks", Id: "UserSchedule", Url: "User/Schedule", OnLoad: User.Schedule.Init, Menu: [ "Home", "Schedule" ], VersitileHeight: false },
        { Name: "My Monthly Calendar", Id: "UserCalendar", Url: "User/Calendar", OnLoad: User.Calendar.Init, Menu: "Home", VersitileHeight: false },
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
        { Name: "Quality Assurance (QA) Center", Id: "QACenter", Url: "Agency/QA", OnLoad: Agency.QA.Init, Menu: "Home", VersitileHeight: false },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageReferrals)) { %>
        { Name: "New Referral", MenuName: "Referral", Id: "NewReferral", Url: "Referral/New", OnLoad: Referral.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManagePatients)) { %>
        { Name: "New Patient", MenuName: "Patient", Id: "NewPatient", Url: "Patient/New", OnLoad: Patient.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.EditEpisode)) { %>
        { Name: "New Episode", MenuName: "Episode", Id: "NewEpisode", Url: "Schedule/Episode/New", OnLoad: Schedule.Episode.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        { Name: "Compose Message", MenuName: "Message", Id: "NewMessage", Url: "Message/New", OnLoad: Message.InitCompose, Menu: [ "CreateNew", "AdminNew" ], VersitileHeight: false },
        { Name: "New Communication Note", MenuName: "Communication Note", Id: "NewCommunicationNote", Url: "Patient/CommunicationNote/New", OnLoad: Patient.CommunicationNote.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% if (Current.HasRight(Permissions.ManageInsurance)) { %>
        { Name: "New Authorization", MenuName: "Authorization", Id: "NewAuthorization", Url: "Patient/Authorization/New", OnLoad: Patient.Authorization.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
        { Name: "New Order", MenuName: "Order", Id: "NewOrder", Url: "Patient/Order/New", OnLoad: Patient.Order.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
        { Name: "New Physician Face-to-face Encounter", MenuName: "Physician Face-to-face Encounter", Id: "NewPhysicianFaceToFaceEncounter", Url: "Patient/FaceToFaceEncounter/New", OnLoad: Patient.FaceToFaceEncounter.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageHospital)) { %>
        { Name: "New Hospital", MenuName: "Hospital", Id: "NewHospital", Url: "Agency/Hospital/New", OnLoad: Agency.Hospital.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageInsurance)) { %>
        { Name: "New Insurance/Payor", MenuName: "Insurance/Payor", Id: "NewInsurance", Url: "Agency/Insurance/New", OnLoad: Agency.Insurance.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManagePhysicians)) { %>
        { Name: "New Physician", MenuName: "Physician", Id: "NewPhysician", Url: "Agency/Physician/New", OnLoad: Agency.Physician.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageContact)) { %>
        { Name: "New Contact", MenuName: "Contact", Id: "NewContact", Url: "Agency/Contact/New", OnLoad: Agency.Contact.InitNew, Menu: [ "CreateNew", "AdminNew" ] },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
        { Name: "New Incident/Accident Report", MenuName: "Incident/Accident Report", Id: "NewIncident", Url: "Agency/Incident/New", OnLoad: Agency.Incident.InitNew, Menu: "CreateNew" },
        { Name: "New Infection Report", MenuName: "Infection Report", Id: "NewInfection", Url: "Agency/Infection/New", OnLoad: Agency.Infection.InitNew, Menu: "CreateNew" },
        <% } %>
        <% if (Current.HasRight(Permissions.CreateOasisSubmitFile)) { %>
        { Name: "OASIS Export", Id: "OASISExport", Url: "Oasis/Export", OnLoad: OASIS.Export.Init, Menu: "Create", VersitileHeight: false },
        <% } %>
        <% if (Current.HasRight(Permissions.ViewLists)) { %>
        { Name: "Patient List", MenuName: "Patients", Id: "ListPatients", Url: "Patient/List", OnLoad: Patient.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Referral List", MenuName: "Referrals", Id: "ListReferrals", Url: "Referral/List", OnLoad: Referral.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Contact List", MenuName: "Contacts", Id: "ListContacts", Url: "Agency/Contact/List", OnLoad: Agency.Contact.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Template List", MenuName: "Templates", Id: "ListTemplates", Url: "Agency/Template/List", OnLoad: Agency.Template.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Supply List", MenuName: "Supplies", Id: "ListSupplies", Url: "Agency/Supply/List", OnLoad: Agency.Supply.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Hospital List", MenuName: "Hospitals", Id: "ListHospitals", Url: "Agency/Hospital/List", OnLoad: Agency.Hospital.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Insurance/Payor List", MenuName: "Insurances/Payors", Id: "ListInsurances", Url: "Agency/Insurance/List", OnLoad: Agency.Insurance.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Physician List", MenuName: "Physicians", Id: "ListPhysicians", Url: "Agency/Physician/List", OnLoad: Agency.Physician.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "User List", MenuName: "Users", Id: "ListUsers", Url: "User/List", OnLoad: User.InitList, Menu: [ "ViewList", "AdminList" ], VersitileHeight: false },
        { Name: "Incident Report List", MenuName: "Incident Reports", Id: "ListIncidents", Url: "Agency/Incident/List", OnLoad: Agency.Incident.InitList, Menu: "ViewList", VersitileHeight: false },
        { Name: "Infection Report List", MenuName: "Infection Reports", Id: "ListInfections", Url: "Agency/Infection/List", OnLoad: Agency.Infection.InitList, Menu: "ViewList", VersitileHeight: false },
        { Name: "Communication Note List", MenuName: "Communication Notes", Id: "ListCommunicationNotes", Url: "Patient/CommunicationNote/List", OnLoad: Patient.CommunicationNote.InitList, Menu: "ViewList", VersitileHeight: false },
        <% } %>
        <% if (Current.HasRight(Permissions.AccessOrderManagementCenter)) { %>
        { Name: "Orders To Be Sent", Id: "OrdersToBeSent", Url: "Agency/Orders/ToBeSent/List", Menu: "OrdersManagement", OnLoad: Agency.Orders.ToBeSent.InitList, VersitileHeight: false },
        { Name: "Orders Pending Signature", Id: "OrdersPendingSignature", Url: "Agency/Orders/PendingSignature/List", Menu: "OrdersManagement", OnLoad: Agency.Orders.PendingSignature.InitList, VersitileHeight: false },
        { Name: "Orders History", Id: "OrdersHistory", Url: "Agency/Orders/History/List", Menu: "OrdersManagement", OnLoad: Agency.Orders.History.InitList,VersitileHeight: false },
        <% } %>
        { Name: "Blank Forms", Id: "BlankForms", Url: "Agency/Blankforms", Menu: "View" },
        <% if (Current.HasRight(Permissions.ViewExportedOasis)) { %>
        { Name: "Exported OASIS", Id: "ExportedOASIS", Url: "Oasis/Exported", Menu: "View", OnLoad: OASIS.Exported.Init, VersitileHeight: false},
        <% } %>
        <% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
        { Name: "Past Due Recerts", Id: "ListPastDueRecerts", Url: "Agency/Recerts/PastDue/List", Menu: "View", OnLoad: Agency.Recerts.PastDue.Init, VersitileHeight: false },
        { Name: "Upcoming Recerts", Id: "ListUpcomingRecerts", Url: "Agency/Recerts/Upcoming/List", Menu: "View", OnLoad: Agency.Recerts.Upcoming.Init, VersitileHeight: false },
        <% } %>
        <% if (Current.HasRight(Permissions.PrintClinicalDocuments)) { %>
        { Name: "Print Queue", Id: "PrintQueue", Url: "Agency/PrintQueue", Menu: "View", OnLoad: Agency.PrintQueue.Init, VersitileHeight: false },
        <% } %>
        { Name: "Patient Charts", Id: "PatientCharts", Url: "Patient/Charts", OnLoad: Patient.Charts.Init, Menu: "Patients", VersitileHeight: false },
        <% if (Current.HasRight(Permissions.ViewExisitingReferrals)) { %>
        { Name: "Existing Referrals", Id: "ExistingReferrals", Url: "Referral/List", Menu: "Patients", OnLoad: Referral.InitList, VersitileHeight: false },
        <% } %>
        <% if (Current.HasRight(Permissions.ViewLists)) { %>
        { Name: "Pending Admissions", Id: "ListPendingPatients", Url: "Patient/PendingAdmission/List", Menu: "Patients", OnLoad: Patient.InitPendingList, VersitileHeight: false },
        { Name: "Non-Admissions", Id: "ListNonAdmission", Url: "Patient/NonAdmission/List", Menu: "Patients", OnLoad: Patient.InitNonAdmissionList, VersitileHeight: false },
        { Name: "Deleted Patients", Id: "ListDeletedPatients", Url: "Patient/Deleted/List", Menu: "Patients", OnLoad: Patient.InitDeletedList, VersitileHeight: false },
        { Name: "Hospitalization Logs", Id: "ListHospitalization", Url: "Patient/Hospitalization/List", Menu: "Patients", OnLoad: Patient.Hospitalization.InitList, VersitileHeight: false },
        <% } %>
        { Name: "Schedule Center", Id: "ScheduleCenter", Url: "Schedule/Center", OnLoad: Schedule.Center.Init, Menu: "Schedule", VersitileHeight: false },
        <% if (Current.HasRight(Permissions.AccessReports)) { %>
        { Name: "Schedule Deviation Report", Id: "ScheduleDeviationReport", Url: "Schedule/Deviation", Menu: "Schedule", OnLoad: Schedule.InitDeviation, VersitileHeight: false },
        <% } %>
        { Name: "Private Duty Schedule Center", Id: "PDScheduleCenter", Url: "Schedule/PD/Center", OnLoad: Schedule.PD.Center.Init, Menu: "Schedule", VersitileHeight: false },
        <% if (Current.HasRight(Permissions.ScheduleVisits)) { %>
        { Name: "Reassign Schedule", Id: "ScheduleReassign", Url: "Schedule/ReassignMultiple", OnLoad: Schedule.InitReassignMultiple, Menu: "Schedule", Resize: false, IgnoreMinSize: true, Width: 500 },
        <% } %>
        <% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
        { Name: "Create RAP Claims", Id: "ClaimRAPCenter", Url: "Billing/Claim/RAP/Center", OnLoad: Billing.Claim.RAP.InitList, Menu: "BillingMedicare" },
        { Name: "Create Final Claims", Id: "ClaimFinalCenter", Url: "Billing/Claim/Final/Center",  OnLoad: Billing.Claim.Final.InitList, Menu: "BillingMedicare" },
        { Name: "Pending Claims", Id: "PendingClaims", Url: "Billing/Claim/Pending", OnLoad: Billing.Claim.Pending.Init, Menu: "BillingMedicare", VersitileHeight: false },
        { Name: "Claims History", Id: "MedicareClaimsHistory", Url: "Billing/History/Medicare", OnLoad: Billing.History.Medicare.Init, Menu: "BillingMedicare", VersitileHeight: false },
        { Name: "Remittance Advice", Id: "ListRemittances", Url: "Billing/Remittance/List", OnLoad: Billing.Remittance.List.Init, Menu: "BillingMedicare" },
        { Name: "Medicare Eligibility", Id: "MedicareEligibility", Url: "Billing/MedicareEligibility", OnLoad: Billing.MedicareEligibility.Init, Menu: "BillingMedicare", VersitileHeight: false },
        { Name: "Create Claims", Id: "ClaimManagedCenter", Url: "Billing/Claim/Managed/Center",  OnLoad: Billing.Claim.Managed.InitList, Menu: "BillingManagedCare" },
        { Name: "Claims History", Id: "ManagedClaimsHistory", Url: "Billing/History/Managed", OnLoad: Billing.History.Managed.Init, Menu: "BillingManagedCare", VersitileHeight: false },
        { Name: "Claim Submission History", Id: "ClaimSubmissionHistory", Url: "Billing/SubmittedList", Menu: "Billing", VersitileHeight: false },
        { Name: "Submitted Claim Details", Id: "SubmittedClaimDetails", Url: "Billing/SubmittedClaimDetail" },
        { Name: "Claim Response", Id: "ClaimResponse", Url:"Billing/ClaimResponse", Resize: false, Centered: true, IgnoreMinSize: true, Width: "625px" },
        <% } %>
        { Name: "New Template", MenuName: "Template", Id: "NewTemplate", Url: "Agency/Template/New", OnLoad: Agency.Template.InitNew, Menu: "AdminNew" },
        { Name: "New Supply", MenuName: "Supply", Id: "NewSupply", Url: "Agency/Supply/New", OnLoad: Agency.Supply.InitNew, Menu: "AdminNew", Width: 450, Resize: false, IgnoreMinSize: true },
        <% if (Current.HasRight(Permissions.ManageUsers)) { %>
        { Name: "New User", MenuName: "User", Id: "NewUser", Url: "User/New", OnLoad: User.InitNew, Menu: "AdminNew" },
        <% } %>
        <% if (Current.HasRight(Permissions.ManagePayroll)) { %>
        { Name: "Payroll Summary", Id: "PayrollSummary", Url: "Payroll/SearchView", OnLoad: Payroll.InitSearch, Menu: "Admin" , VersitileHeight: false },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageUsers)) { %>
        { Name: "License Manager", Id: "LicenseManager", Url: "User/LicenseManager", OnLoad: User.InitLicenseManager, Menu: "Admin" },
        <% } %>
        <% if (Current.HasRight(Permissions.ManageAgencyInformation)) { %>
        { Name: "Manage Company Information", Id: "ManageCompanyInformation", Url: "Agency/Signature", Height: "210px", Width: "600px", Menu: "Admin", Modal: true, Resize: false, StatusBar: false, Centered: true, IgnoreMinSize: true },
        { Name: "Edit Company Information", Id: "EditCompanyInformation", Resize: false, StatusBar: false, Centered: true, IgnoreMinSize: true, Height: "360px", Width: "750px" },
        <% } %>
        <% if (Current.HasRight(Permissions.AccessReports)) { %>
        { Name: "Report Center", Id: "ReportCenter", Url: "Report/Center", OnLoad: Report.Init, Menu: "Reports" },
        { Name: "Completed Reports", Id: "CompletedReports", Url: "Report/Completed", Menu: "Reports", VersitileHeight: false  },
        <% } %>
        { Name: "After-Hours Support", Id: "AfterHoursSupport", Url: "Help/AfterHoursSupport", Menu: "Support", Resize: false, StatusBar: false, Centered: true, IgnoreMinSize: true, Height: "300px", Width: "600px" },
        { Name: "Join GoToMeeting", Id: "GoToMeeting", Url: "Help/GoToMeeting", Menu: "Support" },
        { Name: "Edit Patient", Id: "EditPatient", Url: "Patient/Edit", OnLoad: Patient.InitEdit },
        { Name: "Edit Referral", Id: "EditReferral", Url: "Referral/Edit", OnLoad: Referral.InitEdit },
        { Name: "Edit Contact", Id: "EditContact", Url: "Agency/Contact/Edit", OnLoad: Agency.Contact.InitEdit },
        { Name: "Edit Template", Id: "EditTemplate", Url: "Agency/Template/Edit", OnLoad: Agency.Template.InitEdit },
        { Name: "Edit Supply", Id: "EditSupply", Url: "Agency/Supply/Edit", OnLoad: Agency.Supply.InitEdit, Width: 450, Resize: false, IgnoreMinSize: true },
        { Name: "Edit Hospital", Id: "EditHospital", Url: "Agency/Hospital/Edit", OnLoad: Agency.Hospital.InitEdit },
        { Name: "Edit Insurance", Id: "EditInsurance", Url: "Agency/Insurance/Edit", OnLoad: Agency.Insurance.InitEdit },
        { Name: "Edit Visit Rates", Id: "EditVisitRates", Url: "Agency/Insurance/VisitRates/Edit", OnLoad: Agency.Insurance.VisitRates.InitEdit },
        { Name: "Edit Physician", Id: "EditPhysician", Url: "Agency/Physician/Edit", OnLoad: Agency.Physician.InitEdit },
        { Name: "Edit User", Id: "EditUser", Url: "User/Edit", OnLoad: User.InitEdit },
        { Name: "Edit Infection Report", Id: "EditInfection", Url: "Agency/Infection/Edit", OnLoad: Agency.Infection.InitEdit },
        { Name: "Edit Incident/Accident Report", Id: "EditIncident", Url: "Agency/Incident/Edit", OnLoad: Agency.Incident.InitEdit },
        { Name: "Edit Authorization", Id: "EditAuthorization", Url: "Patient/Authorization/Edit", OnLoad: Patient.Authorization.InitEdit },
        { Name: "Edit Communication Note", Id: "EditCommunicationNote", Url: "Patient/CommunicationNote/Edit", OnLoad: Patient.CommunicationNote.InitEdit },
        { Name: "Edit Episode", Id: "editepisode" },
        { Name: "Edit Order", Id: "editorder" },
        { Name: "Edit Location", Id: "editlocation" },
        { Name: "Referral Activity Log", Id: "ReferralLogs", Url: "Referral/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Contact Activity Log", Id: "ContactLogs", Url: "Agency/Contact/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Template Activity Log", Id: "TemplateLogs", Url: "Agency/Template/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Supply Activity Log", Id: "SupplyLogs", Url: "Agency/Supply/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Hospital Activity Log", Id: "HospitalLogs", Url: "Agency/Hospital/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Insurance Activity Log", Id: "InsuranceLogs", Url: "Agency/Insurance/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Physician Activity Log", Id: "PhysicianLogs", Url: "Agency/Physician/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "User Activity Log", Id: "UserLogs", Url: "User/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Episode Activity Log", Id: "EpisodeLogs", Url: "Schedule/Episode/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Medication Activity Log", Id: "MedicationLogs", Url: "Patient/Medication/Logs", Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Home Health Aide Care Plan", Id: "HHAideCarePlan", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.HHAideCarePlan.Init },
        { Name: "Home Health Aide Supervisory Visit", Id: "HHAideSupervisoryVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.HHAideSupervisoryVisit.Init },
        { Name: "Home Health Aide Progress Note", Id: "HHAideVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.HHAideVisit.Init },
        { Name: "Driver / Transportation Note", Id: "DriverOrTransportationNote", Url: "Schedule/NoteView", Onload: Schedule.Visit.DriverOrTransportationNote.Init },
        { Name: "MSW Assessment", Id: "MSWAssessment", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.MSWEvaluationAssessment.Init },
        { Name: "MSW Discharge", Id: "MSWDischarge", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.MSWEvaluationAssessment.Init },
        { Name: "MSW Evaluation", Id: "MSWEvaluationAssessment", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.MSWEvaluationAssessment.Init },
        { Name: "MSW Progress Note", Id: "MSWProgressNote", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.MSWProgressNote.Init },
        { Name: "MSW Visit", Id: "MSWVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.MSWVisit.Init },
        { Name: "Coordination of Care", Id: "CoordinationOfCare", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.TransferSummary.Init },
        { Name: "Discharge Summary", Id: "DischargeSummary", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.DischargeSummary.Init },
        { Name: "MSW Assessment", Id: "FoleyCathChange", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "LVN Supervisory Visit", Id: "LVNSupervisoryVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.LVNSupervisoryVisit.Init },
        { Name: "Skilled Nurse PICC Midline Placement", Id: "PICCMidlinePlacement", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Foley Change (PRN)", Id: "PRNFoleyChange", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit (PRN)", Id: "PRNSNV", Url: "Schedule/SkilledNurseVisit", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse VP for CMP (PRN)", Id: "PRNVPforCMP", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit PT with INR", Id: "PTWithINR", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit PT with INR (PRN)", Id: "PTWithINRPRNSNV", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Sixty Day Summary", Id: "SixtyDaySummary", Url: "Schedule/NoteView" },
        { Name: "Skilled Nurse Home Infusion SD", Id: "SkilledNurseHomeInfusionSD", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Home Infusion SD Additional", Id: "SkilledNurseHomeInfusionSDAdditional", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit", Id: "SkilledNurseVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse B12 Injection", Id: "SNB12INJ", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse BMP", Id: "SNBMP", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse CBC", Id: "SNCBC", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Discharge", Id: "SNDC", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Diabetic Daily Visit", Id: "SNDiabeticDailyVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SNDiabeticDailyVisit.Init },
        { Name: "Skilled Nurse Evaluation", Id: "SNEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Foley Change", Id: "SNFoleyChange", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Foley Lab", Id: "SNFoleyLabs", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Haldol Injection", Id: "SNHaldolInj", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse AM Insulin", Id: "SNInsulinAM", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse PM Insulin", Id: "SNInsulinPM", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Injection", Id: "SNInjection", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Injection Lab", Id: "SNInjectionLabs", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Lab", Id: "SNLabsSN", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit Discharge Planning", Id: "SNVDCPlanning", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Management and Evaluation", Id: "SNVManagementAndEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Observation and Assessment", Id: "SNVObservationAndAssessment", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Psychiatric Skilled Nurse Visit", Id: "SNVPsychNurse", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit Teaching/Training", Id: "SNVTeachingTraining", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Skilled Nurse Visit with Aide Supervision", Id: "SNVwithAideSupervision", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.SkilledNurseVisit.Init },
        { Name: "Transfer Summary", Id: "TransferSummary", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.TransferSummary.Init },
        { Name: "Wound Care Flowsheet", Id: "WoundCareFlowsheet", Url: "Schedule/WoundCare" },
        { Name: "Personal Care Services Care Plan", Id: "PASCarePlan", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PASCarePlan.Init },
        { Name: "Personal Care Services Progress Note", Id: "PASVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PASVisit.Init },
        { Name: "COTA Visit", Id: "COTAVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.OTVisit.Init },
        { Name: "OT Discharge", Id: "OTDischarge", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.OTEvaluation.Init },
        { Name: "OT Evaluation", Id: "OTEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.OTEvaluation.Init },
        { Name: "OT Maintenance", Id: "OTMaintenance", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.OTEvaluation.Init },
        { Name: "OT Re-Evaluation", Id: "OTReEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.OTEvaluation.Init },
        { Name: "OT Visit", Id: "OTVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.OTVisit.Init },
        { Name: "PTA Visit", Id: "PTAVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PTVisit.Init },
        { Name: "PT Discharge", Id: "PTDischarge", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PTDischarge.Init },
        { Name: "PT Evaluation", Id: "PTEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PTEvaluation.Init },
        { Name: "PT Maintenance", Id: "PTMaintenance", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PTEvaluation.Init },
        { Name: "PT Re-Evaluation", Id: "PTReEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PTEvaluation.Init },
        { Name: "PT Visit", Id: "PTVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.PTVisit.Init },
        { Name: "ST Discharge", Id: "STDischarge", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.STEvaluation.Init },
        { Name: "ST Evaluation", Id: "STEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.STEvaluation.Init },
        { Name: "ST Maintenance", Id: "STMaintenance", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.STEvaluation.Init },
        { Name: "ST Re-Evaluation", Id: "STReEvaluation", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.STEvaluation.Init },
        { Name: "ST Visit", Id: "STVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.STVisit.Init },
        { Name: "UAP Insulin Prep-Aministration Note", Id: "UAPInsulinPrepAdminVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.UAPInsulinPrepAdminVisit.Init },
        { Name: "UAP Wound Care Note", Id: "UAPWoundCareVisit", Url: "Schedule/NoteView", OnLoad: Schedule.Visit.UAPWoundCareVisit.Init },
        { Name: "OASIS-C Start of Care", Id: "StartOfCare", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "Non-OASIS Start of Care", Id: "NonOasisStartOfCare", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Recertification", Id: "Recertification", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "Non-OASIS Recertification", Id: "NonOasisRecertification", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Resumption of Care", Id: "ResumptionOfCare", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Follow Up", Id: "FollowUp", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Death at Home", Id: "DischargeFromAgencyDeath", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Discharge from Agency", Id: "DischargeFromAgency", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "Non-OASIS Discharge", Id: "NonOasisDischarge", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Transfer For Discharge", Id: "TransferInPatientDischarged", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "OASIS-C Transfer Not Discharge", Id: "TransferInPatientNotDischarged", Url: "Oasis/OASISView", OnLoad: OASIS.Assessment.Shared.Init },
        { Name: "Task Details", Id: "ScheduleDetails", Url: "Schedule/Details/Edit", OnLoad: Schedule.InitDetailsEdit },
        { Name: "Master Calendar", Id: "MasterCalendar", Url: "Schedule/MasterCalendar", OnLoad: Schedule.MasterCalendar.Init },
        { Name: "List of Inactive Episodes", Id: "InactiveEpisode", Url: "Schedule/Episode/InactiveList", OnLoad: Schedule.Episode.InitInactiveList, Resize: false, Centered: true, IgnoreMinSize: true, Width: "750px" },
        { Name: "Medication Profile", Id: "MedicationProfile", Url: "Patient/Medication/Profile", OnLoad: Patient.Medication.InitProfile },
        { Name: "Medication Profile Snapshot", Id: "NewMedicationSnapshot", Url: "Patient/Medication/Snapshot/New", OnLoad: Patient.Medication.Snapshot.InitNew },
        { Name: "Signed Medication Profiles", Id: "ListMedicationSnapshot", Url: "Patient/Medication/Snapshot/List", OnLoad: Patient.Medication.Snapshot.InitList, VersitileHeight: false },
        { Name: "Allergy Profile", Id: "AllergyProfile", Url: "Patient/Allergy/Profile", OnLoad: Patient.Allergy.InitProfile },
        { Name: "Authorization List", Id: "ListAuthorizations", Url: "Patient/Authorization/List", OnLoad: Patient.Authorization.InitList, VersitileHeight: false },
        { Name: "Patient Order History", Id: "PatientOrderHistory", Url: "Patient/Order/History", OnLoad: Patient.Order.InitHistory, VersitileHeight: false },
        { Name: "Patient Sixty Day Summary", Id: "SixtyDaySummaryList", Url: "Patient/SixtyDaySummary/List", VersitileHeight: false },
        { Name: "Patient Vital Signs", Id: "VitalSignsList", Url: "Patient/VitalSigns/List", OnLoad: Patient.InitVitalSignsList, VersitileHeight: false },
        { Name: "Patient Deleted Tasks/Documents", Id: "DeletedTasksList", Url: "Patient/DeletedTasksList", VersitileHeight: false },
        { Name: "New Hospitalization Log", Id: "NewHospitalization", Url: "Patient/Hospitalization/New", OnLoad: Patient.Hospitalization.InitNew },
        { Name: "Edit Hospitalization Log", Id: "EditHospitalization", Url: "Patient/Hospitalization/Edit", OnLoad: Patient.Hospitalization.InitEdit },
        { Name: "Hospitalization Logs", Id: "HospitalizationLogs", Url: "Patient/HospitalizationLogs", OnLoad: Patient.Hospitalization.InitLogs },
        { Name: "Medicare Eligibility Reports", Id: "MedicareEligibilityReports", Url: "Patient/MedicareEligibilityList" },
        { Name: "Communication Notes", Id: "PatientListCommunicationNotes" },
        <% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
        { Name: "RAP", Id: "RAP", Url: "Billing/Claim/RAP", OnLoad: Billing.Claim.RAP.Init },
        { Name: "Final", Id: "Final", Url: "Billing/Claim/Final", OnLoad: Billing.Claim.Final.Init, VersitileHeight: false },
        { Name: "Claim Summary", Id: "ClaimSummaryRAP", Url: "Billing/Claim/Summary", OnLoad: Billing.Claim.InitSummary },
        { Name: "Claim Summary", Id: "ClaimSummaryFinal", Url: "Billing/Claim/Summary", OnLoad: Billing.Claim.InitSummary },
        { Name: "Managed Care Claim", Id: "ManagedCare", Url: "Billing/Claim/Managed", OnLoad: Billing.Claim.Managed.Init, VersitileHeight: false },
        { Name: "Managed Claim Summary", Id: "ClaimSummaryMenaged", Url: "Billing/Managed/ClaimSummary" },
        { Name: "Remittance Details", Id: "RemittanceDetail", Url: "Billing/Remittance/Detail", OnLoad: Billing.Remittance.Detail.Init },
        <% } %>
        { Name: "Managed Claim", Id: "managedclaimedit" },
        { Name: "Admit Patient", Id: "admitpatient" },
        { Name: "Edit Patient Information", Id: "editpatientadmission" },
        { Name: "New Patient Information", Id: "NewPatientadmission" },
        { Name: "Patient Non-Admission", Id: "nonadmitpatient" },
        { Name: "Validation Result", Id: "validation" },
        { Name: "Missed Visit Report", Id: "newmissedvisit" },
        { Name: "Supply Worksheet", Id: "notessupplyworksheet", Url: "Schedule/SupplyWorksheet" },
        { Name: "Edit 485 - Plan of Care (From Assessment)", Id: "editplanofcare" },
        { Name: "Plan of Treatment/Care", Id: "newplanofcare" },
        { Name: "Edit Communication Note", Id: "editcommunicationnote" },
        { Name: "Edit Authorization", Id: "editauthorization" },
        { Name: "Schedule Event Logs", Id: "schdeuleeventlogs" },
        { Name: "List of Episode logs", Id: "episodelogs" },
        { Name: "List of Patient logs", Id: "patientlogs" },
        { Name: "List of Patient Medication logs", Id: "medicationlogs" },
        { Name: "List of Physician logs", Id: "physicianlogs" },
        { Name: "List of User logs", Id: "userlogs" },
        { Name: "List of insurance logs", Id: "insurancelogs" },
        { Name: "List of location logs", Id: "locationlogs" },
        { Name: "List of hospital logs", Id: "hospitallogs" },
        { Name: "List of template logs", Id: "templatelogs" },
        { Name: "List of supply logs", Id: "supplylogs" },
        { Name: "List of claim logs", Id: "claimlogs" },
        { Name: "Patient Admission Periods", Id: "patientmanageddates"},
        { Name: "Episode Orders", Id: "patientepisodeorders"},
        { Name: "Claim Remittances", Id: "claimremittances"}
    ],
    Options: { GetRemoteContent: <%= AppSettings.GetRemoteContent %>, Animated: true, DefaultWindow: "Dashboard", AgencyId: "<%= Current.AgencyId %>", UserId: "<%= Current.UserId %>" }
});
</script>