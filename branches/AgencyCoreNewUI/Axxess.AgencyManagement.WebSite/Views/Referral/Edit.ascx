﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">Edit Referral | <%= Model.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Referral", FormMethod.Post, new { @id = "EditReferral_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditReferral_Id" }) %>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="EditReferral_Physician" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("ReferrerPhysician", Model.ReferrerPhysician.ToString(), new { @id = "EditReferral_Physician", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
            <div class="row">
                <label for="EditReferral_AdmissionSource" class="fl strong">Admission Source</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "EditReferral_AdmissionSource", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditReferral_OtherReferralSource" class="fl strong">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "EditReferral_OtherReferralSource", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_Date" class="fl strong">Referral Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReferralDate" value="<%= Model.ReferralDate.ToShortDateString() %>" id="EditReferral_Date" /></div>
            </div>
            <div class="row">
                <label for="EditReferral_InternalReferral" class="fl strong">Internal Referral</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "EditReferral_InternalReferral" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="EditReferral_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "EditReferral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_MiddleInitial" class="fl strong">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial", Model.MiddleInitial, new { @id = "EditReferral_MiddleInitial", @class = "mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "EditReferral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Gender</label>
                <div class="fr radio">
                    <%= Html.RadioButton("Gender", "Female", Model.Gender == "Female", new { @id = "EditReferral_Gender_F", @class = "required" }) %>
                    <label for="EditReferral_Gender_F" class="fixed short">Female</label>
                    <%= Html.RadioButton("Gender", "Male", Model.Gender == "Male", new { @id = "EditReferral_Gender_M", @class = "required" })%>
                    <label for="EditReferral_Gender_M" class="fixed short">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_DateOfBirth" class="fl strong">Date of Birth</label>
                <div class="fr"><%= Html.TextBox("DOB", Model.DOB.ToShortDateString(), new { @id = "EditReferral_DateOfBirth", @class = "required date-picker" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_MaritalStatus" class="fl strong">Marital Status</label>
                <div class="fr">
                    <%  var maritalStatus = new SelectList(new[] {
                            new SelectListItem { Text = "** Select **", Value = "0" },
                            new SelectListItem { Text = "Married", Value = "Married" },
                            new SelectListItem { Text = "Divorce", Value = "Divorce" },
                            new SelectListItem { Text = "Widowed", Value = "Widowed" },
                            new SelectListItem { Text = "Single", Value = "Single" }
                        }, "Value", "Text", Model.MaritalStatus); %>
                    <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "EditReferral_MaritalStatus" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Height" class="fl strong">Height</label>
                <div class="fr radio">
                    <%= Html.TextBox("Height", Model.Height, new { @id = "EditReferral_Height", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", Model.HeightMetric == 0, new { @id = "EditReferral_HeightMetric0" })%>
                    <label for="EditReferral_HeightMetric0" class="fixed shortest">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", Model.HeightMetric == 1, new { @id = "EditReferral_HeightMetric1" })%>
                    <label for="EditReferral_HeightMetric1" class="fixed shortest">cm</label>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Weight" class="fl strong">Weight</label>
                <div class="fr radio">
                    <%= Html.TextBox("Weight", Model.Weight, new { @id = "EditReferral_Weight", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", Model.WeightMetric == 0, new { @id = "EditReferral_WeightMetric0" })%>
                    <label for="EditReferral_WeightMetric0" class="fixed shortest">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", Model.WeightMetric == 1, new { @id = "EditReferral_WeightMetric1" })%>
                    <label for="EditReferral_WeightMetric1" class="fixed shortest">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Assign" class="fl strong">Assign to Clinician</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "EditReferral_Assign", @class = "required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditReferral_MedicareNo" class="fl strong">Medicare No</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "EditReferral_MedicareNo", @maxlength = "12" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_MedicaidNo" class="fl strong">Medicaid No</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "EditReferral_MedicaidNo", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditReferral_SSN" class="fl strong">SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", Model.SSN, new { @id = "EditReferral_SSN", @class = "numeric ssn", @maxlength = "9" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "EditReferral_AddressLine1", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "EditReferral_AddressLine2", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "EditReferral_AddressCity", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="EditReferral_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "EditReferral_AddressStateCode", @class = "state required notzero" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "EditReferral_AddressZipCode", @class = "numeric required zip", @maxlength = "9" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_HomePhone1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 3 ? Model.PhoneHome.Substring(0, 3) : string.Empty, new { @id = "EditReferral_HomePhone1", @class = "required numeric phone-short", @maxlength = "3" }) %>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 6 ? Model.PhoneHome.Substring(3, 3) : string.Empty, new { @id = "EditReferral_HomePhone2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10 ? Model.PhoneHome.Substring(6, 4) : string.Empty, new { @id = "EditReferral_HomePhone3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditReferral_Email" class="fl strong">Email Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "EditReferral_Email", @class = "email" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <%  string[] servicesRequired = Model.ServicesRequired != string.Empty ? Model.ServicesRequired.Split(';') : new string[]{ }; %>
        <input type="hidden" value="" name="ServicesRequiredCollection" />
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("0").ToChecked())%>
                        <label for="ServicesRequiredCollection0">SNV</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("1").ToChecked())%>
                        <label for="ServicesRequiredCollection1">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("2").ToChecked())%>
                        <label for="ServicesRequiredCollection2">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("3").ToChecked())%>
                        <label for="ServicesRequiredCollection3">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("4").ToChecked())%>
                        <label for="ServicesRequiredCollection4">SP</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' name='ServicesRequiredCollection' {0} />", servicesRequired.Contains("5").ToChecked())%>
                        <label for="ServicesRequiredCollection5">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <%  string[] DME = Model.DME != string.Empty ? Model.DME.Split(';') : new string[] { }; %>
        <input type="hidden" value="" name="DMECollection" />
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='DMECollection0' type='checkbox' value='0' name='DMECollection' {0} />", DME.Contains("0").ToChecked())%>
                        <label for="DMECollection0">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection1' type='checkbox' value='1' name='DMECollection' {0} />", DME.Contains("1").ToChecked())%>
                        <label for="DMECollection1">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection2' type='checkbox' value='2' name='DMECollection' {0} />", DME.Contains("2").ToChecked())%>
                        <label for="DMECollection2">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection3' type='checkbox' value='3' name='DMECollection' {0} />", DME.Contains("3").ToChecked())%>
                        <label for="DMECollection3">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection4' type='checkbox' value='4' name='DMECollection' {0} />", DME.Contains("4").ToChecked())%>
                        <label for="DMECollection4">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection5' type='checkbox' value='5' name='DMECollection' {0} />", DME.Contains("5").ToChecked())%>
                        <label for="DMECollection5">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection6' type='checkbox' value='6' name='DMECollection' {0} />", DME.Contains("6").ToChecked())%>
                        <label for="DMECollection6">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection7' type='checkbox' value='7' name='DMECollection' {0} />", DME.Contains("7").ToChecked())%>
                        <label for="DMECollection7">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection8' type='checkbox' value='8' name='DMECollection' {0} />", DME.Contains("8").ToChecked())%>
                        <label for="DMECollection8">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection9' type='checkbox' value='9' name='DMECollection' {0} />", DME.Contains("9").ToChecked())%>
                        <label for="DMECollection9">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection10' type='checkbox' value='10' name='DMECollection' {0} />", DME.Contains("10").ToChecked())%>
                        <label for="DMECollection10">Other</label>
                        <div class="more"><%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "EditReferral_DMEOther" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physicians</legend>
        <div class="wide column">
            <div class="row">
                <div class="fl">
                    <label for="EditReferral_PhysicianSelector" class="fl strong">Add Physician to Patient</label>
                    <div class="fl"><input type="text" id="EditReferral_PhysicianSelector" class="physician-picker" /></div>
                    <div class="buttons fl">
                        <ul>
                            <li><a class="add-referral-physician">Add Selected Physician</a></li>
                        </ul>
                    </div>
                </div>
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
            <div class="row">
                <%= Html.Telerik().Grid<AgencyPhysician>().Name("EditReferral_PhysicianGrid").HtmlAttributes(new { @class = "hidden" }).Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PhoneWorkFormatted).Title("Work Phone");
                        columns.Bound(c => c.FaxNumberFormatted);
                        columns.Bound(c => c.EmailAddress);
                        columns.Bound(c => c.Id).ClientTemplate("<a class=\"link\" onclick=\"Referral.Physician.Delete('<#=Id#>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Delete</a> | <a class=\"<#= !Primary ? \"link\" : \"hidden\" #>\" onclick=\"Referral.Physician.SetPrimary('<#=Id #>','" + Model.Id + "',$(this).closest('.t-grid'));return false\">Make Primary</a>").Title("Action").Width(135);
                    }).ClientEvents(c => c.OnDataBound("U.ToggleNoneFound")).DataBinding(dataBinding => dataBinding.Ajax().Select("PhysicianGrid", "Referral", new { referralId = Model.Id })).Sortable().Footer(false)%>
                <h2 class="hidden blue">No Physicians Found</h2>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="EditReferral_Comments" name="Comments" class="tall" maxcharacters="500"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>