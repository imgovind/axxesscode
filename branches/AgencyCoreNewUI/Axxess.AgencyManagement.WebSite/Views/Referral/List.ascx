﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Existing Referral List | <%= Current.AgencyName %></span>
<div class="wrapper main blue">
    <div class="fr buttons">
        <ul><li><a class="new-referral">New Referral</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%  var action = Current.HasRight(Permissions.ManageReferrals) ? string.Format("{0}Referral.Edit('<#=Id#>'){1}Edit{2} | {0}Referral.Delete('<#=Id#>',$(this).closest('.t-grid')){1}Delete{2} | {0}Referral.Admit('<#=Id#>'){1}Admit{2} | {0}Referral.NonAdmit('<#=Id#>'){1}Non-Admit{2}", "<a class=\"link\" onclick=\"", ";return false\">", "</a>") : string.Empty; %>
    <%  var visible = Current.HasRight(Permissions.ManageReferrals); %>
    <%= Html.Telerik().Grid<ReferralData>().Name("List_Referral").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(r => r.ReferralDate).Format("{0:MM/dd/yyyy}").Title("Referral Date").Width(95);
            columns.Bound(r => r.DisplayName).Title("Name");
            columns.Bound(r => r.AdmissionSource).Title("Referral Source");
            columns.Bound(r => r.DateOfBirth).Title("Date of Birth").Width(95);
            columns.Bound(r => r.Gender).Title("Gender").Width(75);
            columns.Bound(r => r.Status).Title("Status").Width(75).Sortable(true);
            columns.Bound(r => r.CreatedBy).Title("Created By").Width(175).Sortable(true);
            columns.Bound(r => r.Id).ClientTemplate("<a onclick=\"Referral.Print(<#=Id#>');return false\"><span class=\"img icon print\"></span></a>").Title("").Width(30);
            columns.Bound(r => r.Id).ClientTemplate(action).Title("Action").Width(200).Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Referral")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>