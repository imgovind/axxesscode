﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Referral | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Create", "Referral", FormMethod.Post, new { @id = "NewReferral_Form" })) { %>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_Physician" class="fl strong">Physician</label>
                <div class="fr"><%= Html.TextBox("ReferrerPhysician", string.Empty, new { @id = "NewReferral_Physician", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
            <div class="row">
                <label for="NewReferral_AdmissionSource" class="fl strong">Admission Source</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", string.Empty, new { @id = "NewReferral_AdmissionSource", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewReferral_OtherReferralSource" class="fl strong">Other Referral Source</label>
                <div class="fr"><%= Html.TextBox("OtherReferralSource", string.Empty, new { @id = "NewReferral_OtherReferralSource", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_Date" class="fl strong">Referral Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="ReferralDate" id="NewReferral_Date" /></div>
            </div>
            <div class="row">
                <label for="NewReferral_InternalReferral" class="fl strong">Internal Referral</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", string.Empty, new { @id = "NewReferral_InternalReferral" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewReferral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_MiddleInitial" class="fl strong">MI</label>
                <div class="fr"><%= Html.TextBox("MiddleInitial", string.Empty, new { @id = "NewReferral_MiddleInitial", @class = "mi", @maxlength = "1" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewReferral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Gender</label>
                <div class="fr radio">
                    <%= Html.RadioButton("Gender", "Female", new { @id = "NewReferral_Gender_F", @class = "radio required" }) %>
                    <label for="NewReferral_Gender_F" class="fixed short">Female</label>
                    <%= Html.RadioButton("Gender", "Male", new { @id = "NewReferral_Gender_M", @class = "radio required" })%>
                    <label for="NewReferral_Gender_M" class="fixed short">Male</label>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_DateOfBirth" class="fl strong">Date of Birth</label>
                <div class="fr"><input type="text" class="date-picker required" name="DOB" id="NewReferral_DateOfBirth" /></div>
            </div>
            <div class="row">
                <label for="NewReferral_MaritalStatus" class="fl strong">Marital Status</label>
                <div class="fr">
                    <select id="NewReferral_MaritalStatus" name="MaritalStatus">
                        <option value="0" selected="selected">-- Select Marital Status --</option>
                        <option value="Married">Married</option>
                        <option value="Divorce" >Divorced</option>
                        <option value="Widowed">Widowed</option>
                        <option value="Single">Single</option>
                        <option value="Unknown">Unknown</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_Height" class="fl strong">Height</label>
                <div class="fr radio">
                    <%= Html.TextBox("Height", string.Empty, new { @id = "NewReferral_Height", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("HeightMetric", "0", new { @id = "NewReferral_HeightMetric0" })%>
                    <label for="NewReferral_HeightMetric0" class="fixed shortest">in</label>
                    <%= Html.RadioButton("HeightMetric", "1", new { @id = "NewReferral_HeightMetric1" })%>
                    <label for="NewReferral_HeightMetric1" class="fixed shortest">cm</label>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_Weight" class="fl strong">Weight</label>
                <div class="fr radio">
                    <%= Html.TextBox("Weight", string.Empty, new { @id = "NewReferral_Weight", @class = "numeric shortest", @maxlength = "3" })%>
                    <%= Html.RadioButton("WeightMetric", "0", new { @id = "NewReferral_WeightMetric0" })%>
                    <label for="NewReferral_WeightMetric0" class="fixed shortest">lb</label>
                    <%= Html.RadioButton("WeightMetric", "1", new { @id = "NewReferral_WeightMetric1" })%>
                    <label for="NewReferral_WeightMetric1" class="fixed shortest">kg</label>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_Assign" class="fl strong">Assign to Clinician</label>
                <div class="fr"><%= Html.Clinicians("UserId", string.Empty, new { @id = "NewReferral_Assign", @class = "required notzero" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewReferral_MedicareNo" class="fl strong">Medicare No</label>
                <div class="fr"><%= Html.TextBox("MedicareNumber", " ", new { @id = "NewReferral_MedicareNo", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_MedicaidNo" class="fl strong">Medicaid No</label>
                <div class="fr"><%= Html.TextBox("MedicaidNumber", " ", new { @id = "NewReferral_MedicaidNo", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewReferral_SSN" class="fl strong">SSN</label>
                <div class="fr"><%= Html.TextBox("SSN", string.Empty, new { @id = "NewReferral_SSN", @class = "numeric ssn", @maxlength = "9" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", string.Empty, new { @id = "NewReferral_AddressLine1", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", string.Empty, new { @id = "NewReferral_AddressLine2", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", string.Empty, new { @id = "NewReferral_AddressCity", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="NewReferral_AddressStateCode" class="fl strong">State, Zip Code</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", string.Empty, new { @id = "NewReferral_AddressStateCode", @class = "state required notzero", @status = "State (Required)" })%>
                    <%= Html.TextBox("AddressZipCode", string.Empty, new { @id = "NewReferral_AddressZipCode", @class = "numeric required zip", @maxlength = "9", @status = "Zip Code (Required)" })%>
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_HomePhone1" class="fl strong">Home Phone</label>
                <div class="fr">
                    <input type="text" class="numeric required phone-short" name="PhoneHomeArray" id="NewReferral_HomePhone1" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-short" name="PhoneHomeArray" id="NewReferral_HomePhone2" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-long" name="PhoneHomeArray" id="NewReferral_HomePhone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewReferral_Email" class="fl strong">Email Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", string.Empty, new { @id = "NewReferral_Email", @class = "email", @maxlength = "50" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input type="checkbox" value="0" id="ServicesRequiredCollection0" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection0">SNV</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="1" id="ServicesRequiredCollection1" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection1">HHA</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="2" id="ServicesRequiredCollection2" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection2">PT</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="3" id="ServicesRequiredCollection3" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection3">OT</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="4" id="ServicesRequiredCollection4" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection4">SP</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="5" id="ServicesRequiredCollection5" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection5">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <div class="wide column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input type="checkbox" value="0" id="DMECollection0" name="DMECollection" />
                        <label for="DMECollection0">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="1" id="DMECollection1" name="DMECollection" />
                        <label for="DMECollection1">Cane</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="2" id="DMECollection2" name="DMECollection" />
                        <label for="DMECollection2">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="3" id="DMECollection3" name="DMECollection" />
                        <label for="DMECollection3">Grab Bars</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="4" id="DMECollection4" name="DMECollection" />
                        <label for="DMECollection4">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="5" id="DMECollection5" name="DMECollection" />
                        <label for="DMECollection5">Nebulizer</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="6" id="DMECollection6" name="DMECollection" />
                        <label for="DMECollection6">Oxygen</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="7" id="DMECollection7" name="DMECollection" />
                        <label for="DMECollection7">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="8" id="DMECollection8" name="DMECollection" />
                        <label for="DMECollection8">Walker</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="9" id="DMECollection9" name="DMECollection" />
                        <label for="DMECollection9">Wheelchair</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="10" id="NewReferral_DMEOther" name="DMECollection" />
                        <label for="NewReferral_DMEOther">Other</label>
                        <div class="more"><%= Html.TextBox("OtherDME", string.Empty, new { @id = "NewReferral_OtherDME", @maxlength = "50"}) %></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewReferral_PhysicianDropDown1" class="fl strong">Primary Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "NewReferral_PhysicianDropDown1", @class = "physician-picker" })%>
                    <a class="abs addrem">+</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown2" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "NewReferral_PhysicianDropDown2", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown3" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "NewReferral_PhysicianDropDown3", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown4" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "NewReferral_PhysicianDropDown4", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
            <div class="row hidden">
                <label for="NewReferral_PhysicianDropDown5" class="fl strong">Additional Physician</label>
                <div class="fr">
                    <%= Html.TextBox("AgencyPhysicians", string.Empty, new { @id = "NewReferral_PhysicianDropDown5", @class = "physician-picker" })%>
                    <a class="abs addrem">-</a>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="fr button-with-arrow"><a class="new-physician" status="New Physician">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="NewReferral_Comments" name="Comments" cols="5" rows="6" maxcharacters="500" class="tall"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add Referral</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>