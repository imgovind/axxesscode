﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("NonAdmitComplete", "Referral", FormMethod.Post, new { @id = "NonAdmitReferral_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmitReferral_Id" })%>
    <%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmitReferral_IsAdmit" })%>
    <fieldset>
        <legend>Non-Admission Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Referral Name</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
            <div class="row">
                <label for="NonAdmitReferral_NonAdmitDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="NonAdmitDate" id="NonAdmitReferral_NonAdmitDate" /></div>
            </div>
        </div>
        <div class="wide column">
            <div class="row">
                <label class="strong">Reason Not Admitted</label>
                <div class="checkgroup">
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" />
                        <label for="NonAdmitReferral_ReasonInAppropriate">Inappropriate For Home Care</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonRefused" type="checkbox" value="Referral Refused Service" name="Reason" />
                        <label for="NonAdmitReferral_ReasonRefused">Referral Refused Service</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" />
                        <label for="NonAdmitReferral_ReasonOutofService">Out of Service Area</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" />
                        <label for="NonAdmitReferral_ReasonOnService">On Service with another agency</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" />
                        <label for="NonAdmitReferral_ReasonNotaProvider">Not a Provider</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" />
                        <label for="NonAdmitReferral_ReasonNotHomebound">Not Homebound</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" />
                        <label for="NonAdmitReferral_ReasonRedirected">Redirected to alternate care facility</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmitReferral_ReasonOther" type="checkbox" value="Other" name="Reason" />
                        <label for="NonAdmitReferral_ReasonOther">Other <em>(Specify in Comments)</em></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="NonAdmitReferral_Comments" class="strong">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", "", new { @id = "NonAdmitReferral_Comments", @maxcharacters = "200" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>