﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<span class="wintitle">Schedule Deviation | <%= Current.AgencyName %></span>
<%  string pagename = "AgencyScheduleDeviation"; %>
<%  var sortParams = string.Format("{0}-{1}", ViewData["SortColumn"], ViewData["SortDirection"]); %>
<div class="wrapper main blue">
    <%= Html.Hidden("SortParams", sortParams, new { @id = pagename + "_SortParams" })%>
    <div class="buttons fr">
        <ul>
            <li><a class="grid-refresh">Refresh</a></li>
        </ul>
        <br />
        <ul>
            <li><a class="export">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls ac">
        <div class="filter">
            <label for="<%= pagename %>_BranchCode" class="strong">Branch</label>
            <%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", "", new { @id = pagename + "_BranchOd" })%>
        </div>
        <div class="filter">
            <label for="<%= pagename %>_StartDate" class="strong">Date Range</label>
            <input type="text" class="date-picker" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />
            <label for="<%= pagename %>_EndDate" class="strong">&#8211;</label>
            <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" />
        </div>
    </fieldset>
    <div id="<%= pagename %>_Content" class="content"><% Html.RenderPartial("Deviation/Grid", Model); %></div>
</div>