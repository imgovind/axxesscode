﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<%  string pagename = "AgencyScheduleDeviation"; %>
<%= Html.Telerik().Grid(Model).Name(pagename + "Grid").HtmlAttributes(new { @class = "args" }).Columns(columns => {
        columns.Bound(m => m.PatientIdNumber).Title("MRN").Width(10);
        columns.Bound(m => m.DisciplineTaskName).Title("Task").Sortable(false).Width(20);
        columns.Bound(m => m.PatientName).Title("Patient Name").Width(20);
        columns.Bound(m => m.StatusName).Title("Status").Sortable(false).Width(20);
        columns.Bound(m => m.UserDisplayName).Title("Employee").Width(20);
        columns.Bound(m => m.EventDate).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Schedule Date").Width(10);
        columns.Bound(m => m.VisitDate).Format("{0:MM/dd/yyyy}").Sortable(false).Title("Visit Date").Width(10);
    }).Sortable(sorting =>sorting.SortMode(GridSortMode.SingleColumn).OrderBy(order => {
        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
        if (sortName == "PatientIdNumber") {
            if (sortDirection == "ASC") order.Add(o => o.PatientIdNumber).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientIdNumber).Descending();
        } else if (sortName == "PatientName") {
            if (sortDirection == "ASC") order.Add(o => o.PatientName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.PatientName).Descending();
        } else if (sortName == "UserName") {
            if (sortDirection == "ASC") order.Add(o => o.UserName).Ascending();
            else if (sortDirection == "DESC") order.Add(o => o.UserName).Descending();
        }
    })).Scrollable().Footer(false) %>