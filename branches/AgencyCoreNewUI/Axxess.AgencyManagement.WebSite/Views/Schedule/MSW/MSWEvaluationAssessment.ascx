﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericLivingSituation = data.AnswerArray("GenericLivingSituation"); %>
<%  string[] genericReasonForReferral = data.AnswerArray("GenericReasonForReferral"); %>
<%  string[] genericMentalStatus = data.AnswerArray("GenericMentalStatus"); %>
<%  string[] genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus"); %>
<%  string[] genericIdentifiedProblems = data.AnswerArray("GenericIdentifiedProblems"); %>
<%  string[] genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions"); %>
<%  string[] genericGoals = data.AnswerArray("GenericGoals"); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer.ToDateTime() : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_DisciplineTask" class="fl strong">Visit</label>
                <div class="fr"><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type + "_DisciplineTask", @class = "fr required notzero" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="fl strong">Visit Date</label>
                <div class="fr"><input type="text" id ="Text1" class="date-picker required" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString()%>" mindate="<%= Model.StartDate.ToShortDateString()%>"/></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeIn" class="fl strong">Time In</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = Model.Type + "_TimeIn", @class = "complete-required time-picker" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeOut" class="fl strong">Time Out</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = Model.Type + "_TimeOut", @class = "complete-required time-picker" })%></div>
            </div>
        </div>
        <div class="column">
    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
            <div class="row">
                <label for="<%= Model.Type %>_PreviousNotes" class="fl strong">Previous Notes</label>
                <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
            </div>
    <% } %>
            <div class="row">
                <label for="<%= Model.Type %>_AssociatedMileage" class="fl strong">Associated Mileage</label>
               <div class="fr"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : string.Empty, new { @id = Model.Type + "_AssociatedMileage", @class = "" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_Surcharge" class="fl strong">Surcharge</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Surcharge", data.ContainsKey("Surcharge") ? data["Surcharge"].Answer : string.Empty, new { @id = Model.Type + "_Surcharge", @class = "" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl strong">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                    <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl strong">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                    <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                </div>
            </div>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
            <div class="row"><div class="fr button-with-arrow"><%= Model.CarePlanOrEvalUrl %></div></div>
    <%  } %>
        </div>
    </fieldset>
    <fieldset>
        <legend>Living Situation</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_GenericPrimaryCaregiver" class="fl strong">Primary Caregiver</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPrimaryCaregiver", data.AnswerOrEmptyString("GenericPrimaryCaregiver"), new { @id = Model.Type + "_GenericPrimaryCaregiver" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericQualityOfCare" for="<%= Model.Type %>_GenericIntensityOfPain" class="fl strong">The quality of care that patient receives at home</label>
                <div class="fr">
                    <%  var genericQualityOfCare = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Good", Value = "Good" },
                            new SelectListItem { Text = "Adequate", Value = "Adequate" },
                            new SelectListItem { Text = "Marginal", Value = "Marginal" },
                            new SelectListItem { Text = "Inadequate", Value = "Inadequate" }
                        }, "Value", "Text", data.AnswerOrEmptyString("GenericQualityOfCare"));%>
                    <%= Html.DropDownList(Model.Type + "_GenericQualityOfCare", genericQualityOfCare, new { @id = Model.Type + "_GenericQualityOfCare" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericEnvironmentalConditions" class="fl strong">Environmental Conditions</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericEnvironmentalConditions", data.AnswerOrEmptyString("GenericEnvironmentalConditions"), new { @id = Model.Type + "_GenericEnvironmentalConditions" })%></div>
            </div>
        </div>
        <input type="hidden" name="<%= Model.Type %>_GenericLivingSituation" value="" />
        <div class="column wide">
            <div class=row">
                <div class="strong">Patient Lives (check all that apply)</div>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation1' name='{1}_GenericLivingSituation' value='1' type='checkbox' {0} />", genericLivingSituation.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation1">Alone</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation2' name='{1}_GenericLivingSituation' value='2' type='checkbox' {0} />", genericLivingSituation.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation2">With Friend/Family</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation3' name='{1}_GenericLivingSituation' value='3' type='checkbox' {0} />", genericLivingSituation.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation3">With Dependent</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation4' name='{1}_GenericLivingSituation' value='4' type='checkbox' {0} />", genericLivingSituation.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation4">With Spouse/Partner</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation5' name='{1}_GenericLivingSituation' value='5' type='checkbox' {0} />", genericLivingSituation.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation5">With Religious Community</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation6' name='{1}_GenericLivingSituation' value='6' type='checkbox' {0} />", genericLivingSituation.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation6">Assisted Living</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation7' name='{1}_GenericLivingSituation' value='7' type='checkbox' {0} />", genericLivingSituation.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation7">With Partner</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation8' name='{1}_GenericLivingSituation' value='8' type='checkbox' {0} />", genericLivingSituation.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation8">Has Paid Caregiver (&#62; 10 hours)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation9' name='{1}_GenericLivingSituation' value='9' type='checkbox' {0} />", genericLivingSituation.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation9">Has Live-In, Paid Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericLivingSituation10' name='{1}_GenericLivingSituation' value='10' type='checkbox' {0} />", genericLivingSituation.Contains("10").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericLivingSituation10">Other</label>
                        <div class="more"><%= Html.TextBox(Model.Type + "_GenericLivingSituationOther", data.AnswerOrEmptyString("GenericLivingSituationOther"), new { @id = Model.Type + "_GenericLivingSituationOther" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Reason(s) for Referral (check all that apply)</legend>
        <input type="hidden" name="<%= Model.Type %>_GenericReasonForReferral" value="" />
        <div class="column">
            <div class="row">
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral1' name='{1}_GenericReasonForReferral' value='1' type='checkbox' {0} />", genericReasonForReferral.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral1">Assessment for Psychosocial Coping</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral2' name='{1}_GenericReasonForReferral' value='2' type='checkbox' {0} />", genericReasonForReferral.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral2">Lives Alone, No Identified Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral3' name='{1}_GenericReasonForReferral' value='3' type='checkbox' {0} />", genericReasonForReferral.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral3">Counseling re Disease Process or Management</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral4' name='{1}_GenericReasonForReferral' value='4' type='checkbox' {0} />", genericReasonForReferral.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral4">Solo Caregiver for Minor Children and/or Other Dependents</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral5' name='{1}_GenericReasonForReferral' value='5' type='checkbox' {0} />", genericReasonForReferral.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral5">Family/Caregiver Coping Support</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral6' name='{1}_GenericReasonForReferral' value='6' type='checkbox' {0} />", genericReasonForReferral.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral6">Reported Noncompliance to Medical Plan of Care</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral7' name='{1}_GenericReasonForReferral' value='7' type='checkbox' {0} />", genericReasonForReferral.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral7">Hospice Eligibility</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral8' name='{1}_GenericReasonForReferral' value='8' type='checkbox' {0} />", genericReasonForReferral.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral8">Suspected Negligence or Abuse</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral9' name='{1}_GenericReasonForReferral' value='9' type='checkbox' {0} />", genericReasonForReferral.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral9">Financial/Practical Resources</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral10' name='{1}_GenericReasonForReferral' value='10' type='checkbox' {0} />", genericReasonForReferral.Contains("10").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral10">Assistance with Advanced Directive / DPOA/DNR</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericReasonForReferral11' name='{1}_GenericReasonForReferral' value='11' type='checkbox' {0} />", genericReasonForReferral.Contains("11").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericReasonForReferral11">Other</label>
                        <div class="more"><%= Html.TextBox(Model.Type + "_GenericReasonForReferralOther", data.AnswerOrEmptyString("GenericReasonForReferralOther"), new { @id = Model.Type + "_GenericReasonForReferralOther" })%></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Psychosocial Assessment</legend>
        <div class="column">
            <div class="row">
                <input type="hidden" name="<%= Model.Type %>_GenericMentalStatus" value="" />
                <div class="strong">Mental Status (check all that apply)</div>
                <div class="checkgroup narrow">
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus1' name='{1}_GenericMentalStatus' value='1' type='checkbox' {0} />", genericMentalStatus.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus1">Alert</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus2' name='{1}_GenericMentalStatus' value='2' type='checkbox' {0} />", genericMentalStatus.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus2">Forgetful</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus3' name='{1}_GenericMentalStatus' value='3' type='checkbox' {0} />", genericMentalStatus.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus3">Confused</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus4' name='{1}_GenericMentalStatus' value='4' type='checkbox' {0} />", genericMentalStatus.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus4">Disoriented</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus5' name='{1}_GenericMentalStatus' value='5' type='checkbox' {0} />", genericMentalStatus.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus5">Oriented</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus6' name='{1}_GenericMentalStatus' value='6' type='checkbox' {0} />", genericMentalStatus.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus6">Lethargic</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus7' name='{1}_GenericMentalStatus' value='7' type='checkbox' {0} />", genericMentalStatus.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus7">Poor Short Term Memory</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus8' name='{1}_GenericMentalStatus' value='8' type='checkbox' {0} />", genericMentalStatus.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus8">Unconscious</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericMentalStatus9' name='{1}_GenericMentalStatus' value='9' type='checkbox' {0} />", genericMentalStatus.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericMentalStatus9">Cannot Determine</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <input type="hidden" name="<%= Model.Type %>_GenericEmotionalStatus" value="" />
            <div class="align-left strong">Emotional Status (check all that apply)</div>
            <div class="row">
                <div class="checkgroup narrow">
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus1' class='float-left radio' name='{1}_GenericEmotionalStatus' value='1' type='checkbox' {0} />", genericEmotionalStatus.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus1">Stable</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus2' class='float-left radio' name='{1}_GenericEmotionalStatus' value='2' type='checkbox' {0} />", genericEmotionalStatus.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus2">Tearful</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus3' class='float-left radio' name='{1}_GenericEmotionalStatus' value='3' type='checkbox' {0} />", genericEmotionalStatus.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus3">Stressed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus4' class='float-left radio' name='{1}_GenericEmotionalStatus' value='4' type='checkbox' {0} />", genericEmotionalStatus.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus4">Angry</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus5' class='float-left radio' name='{1}_GenericEmotionalStatus' value='5' type='checkbox' {0} />", genericEmotionalStatus.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus5">Sad</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus6' class='float-left radio' name='{1}_GenericEmotionalStatus' value='6' type='checkbox' {0} />", genericEmotionalStatus.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus6">Withdrawn</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus7' class='float-left radio' name='{1}_GenericEmotionalStatus' value='7' type='checkbox' {0} />", genericEmotionalStatus.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus7">Fearful</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus8' class='float-left radio' name='{1}_GenericEmotionalStatus' value='8' type='checkbox' {0} />", genericEmotionalStatus.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus8">Anxious</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus9' class='float-left radio' name='{1}_GenericEmotionalStatus' value='9' type='checkbox' {0} />", genericEmotionalStatus.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericEmotionalStatus9">Flat Affect</label>
                    </div>
                    <div class="option">
                        <label for="<%= Model.Type %>_GenericEmotionalStatus10">Other</label>
                        <div class="fr more"><%= Html.TextBox(Model.Type + "_GenericEmotionalStatusOther", data.AnswerOrEmptyString("GenericEmotionalStatusOther"), new { @id = Model.Type + "_GenericEmotionalStatusOther" })%></div>
                        <%= string.Format("<input id='{1}_GenericEmotionalStatus10' class='float-left radio float-left' name='{1}_GenericEmotionalStatus' value='10' type='checkbox' {0} />", genericEmotionalStatus.Contains("10").ToChecked(), Model.Type)%>
                    </div>
                </div>
            </div>
        </div>
        <div class="column wide">
            <div class="row">
                <%= Html.TextArea(Model.Type + "_GenericPsychosocialAssessment", data.AnswerOrEmptyString("GenericPsychosocialAssessment"), 4, 20, new { @id = Model.Type + "_GenericPsychosocialAssessment", @class = "fill" })%>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset>
        <legend>Finacial Assessment</legend>
        <div class="column">
            <div class="row strong ac">Income Sources</div>
            <div class="row">
                <label class="fl strong">Employment</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericEmployment", "2", data.AnswerOrEmptyString("GenericEmployment").Equals("2"), new { @id = Model.Type + "_GenericEmployment2" })%>
                    <label for="<%= Model.Type %>_GenericEmployment2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericEmployment", "0", data.AnswerOrEmptyString("GenericEmployment").Equals("0"), new { @id = Model.Type + "_GenericEmployment0" })%>
                    <label for="<%= Model.Type %>_GenericEmployment0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericEmployment", "1", data.AnswerOrEmptyString("GenericEmployment").Equals("1"), new { @id = Model.Type + "_GenericEmployment1" })%>
                    <label for="<%= Model.Type %>_GenericEmployment1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericEmploymentAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericEmploymentAmount", data.AnswerOrEmptyString("GenericEmploymentAmount"), new { @id = Model.Type + "_GenericEmploymentAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Pt Social Security</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericPtSocialSecurity", "2", data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("2"), new { @id = Model.Type + "_GenericPtSocialSecurity2" })%>
                    <label for="<%= Model.Type %>_GenericPtSocialSecurity2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericPtSocialSecurity", "0", data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("0"), new { @id = Model.Type + "_GenericPtSocialSecurity0" })%>
                    <label for="<%= Model.Type %>_GenericPtSocialSecurity0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericPtSocialSecurity", "1", data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("1"), new { @id = Model.Type + "_GenericPtSocialSecurity1" })%>
                    <label for="<%= Model.Type %>_GenericPtSocialSecurity1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericPtSocialSecurityAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericPtSocialSecurityAmount", data.AnswerOrEmptyString("GenericPtSocialSecurityAmount"), new { @id = Model.Type + "_GenericPtSocialSecurityAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Spouse Social Security</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseSocialSecurity", "2", data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("2"), new { @id = Model.Type + "_GenericSpouseSocialSecurity2" })%>
                    <label for="<%= Model.Type %>_GenericSpouseSocialSecurity2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseSocialSecurity", "0", data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("0"), new { @id = Model.Type + "_GenericSpouseSocialSecurity0" })%>
                    <label for="<%= Model.Type %>_GenericSpouseSocialSecurity0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseSocialSecurity", "1", data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("1"), new { @id = Model.Type + "_GenericSpouseSocialSecurity1" })%>
                    <label for="<%= Model.Type %>_GenericSpouseSocialSecurity1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericSpouseSocialSecurityAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericSpouseSocialSecurityAmount", data.AnswerOrEmptyString("GenericSpouseSocialSecurityAmount"), new { @id = Model.Type + "_GenericSpouseSocialSecurityAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Pt SSI</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericPtSSI", "2", data.AnswerOrEmptyString("GenericPtSSI").Equals("2"), new { @id = Model.Type + "_GenericPtSSI2" })%>
                    <label for="<%= Model.Type %>_GenericPtSSI2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericPtSSI", "0", data.AnswerOrEmptyString("GenericPtSSI").Equals("0"), new { @id = Model.Type + "_GenericPtSSI0" })%>
                    <label for="<%= Model.Type %>_GenericPtSSI0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericPtSSI", "1", data.AnswerOrEmptyString("GenericPtSSI").Equals("1"), new { @id = Model.Type + "_GenericPtSSI1" })%>
                    <label for="<%= Model.Type %>_GenericPtSSI1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericPtSSIAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericPtSSIAmount", data.AnswerOrEmptyString("GenericPtSSIAmount"), new { @id = Model.Type + "_GenericPtSSIAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Spouse SSI</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseSSI", "2", data.AnswerOrEmptyString("GenericSpouseSSI").Equals("2"), new { @id = Model.Type + "_GenericSpouseSSI2" })%>
                    <label for="<%= Model.Type %>_GenericSpouseSSI2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseSSI", "0", data.AnswerOrEmptyString("GenericSpouseSSI").Equals("0"), new { @id = Model.Type + "_GenericSpouseSSI0" })%>
                    <label for="<%= Model.Type %>_GenericSpouseSSI0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseSSI", "1", data.AnswerOrEmptyString("GenericSpouseSSI").Equals("1"), new { @id = Model.Type + "_GenericSpouseSSI1" })%>
                    <label for="<%= Model.Type %>_GenericSpouseSSI1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericSpouseSSIAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericSpouseSSIAmount", data.AnswerOrEmptyString("GenericSpouseSSIAmount"), new { @id = Model.Type + "_GenericSpouseSSIAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Pensions</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericPensions", "2", data.AnswerOrEmptyString("GenericPensions").Equals("2"), new { @id = Model.Type + "_GenericPensions2" })%>
                    <label for="<%= Model.Type %>_GenericPensions2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericPensions", "0", data.AnswerOrEmptyString("GenericPensions").Equals("0"), new { @id = Model.Type + "_GenericPensions0" })%>
                    <label for="<%= Model.Type %>_GenericPensions0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericPensions", "1", data.AnswerOrEmptyString("GenericPensions").Equals("1"), new { @id = Model.Type + "_GenericPensions1" })%>
                    <label for="<%= Model.Type %>_GenericPensions1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericPensionsAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericPensionsAmount", data.AnswerOrEmptyString("GenericPensionsAmount"), new { @id = Model.Type + "_GenericPensionsAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Other Income</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericOtherIncome", "2", data.AnswerOrEmptyString("GenericOtherIncome").Equals("2"), new { @id = Model.Type + "_GenericOtherIncome2" })%>
                    <label for="<%= Model.Type %>_GenericOtherIncome2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOtherIncome", "0", data.AnswerOrEmptyString("GenericOtherIncome").Equals("0"), new { @id = Model.Type + "_GenericOtherIncome0" })%>
                    <label for="<%= Model.Type %>_GenericOtherIncome0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOtherIncome", "1", data.AnswerOrEmptyString("GenericOtherIncome").Equals("1"), new { @id = Model.Type + "_GenericOtherIncome1" })%>
                    <label for="<%= Model.Type %>_GenericOtherIncome1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericOtherIncomeAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericOtherIncomeAmount", data.AnswerOrEmptyString("GenericOtherIncomeAmount"), new { @id = Model.Type + "_GenericOtherIncomeAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Food Stamps</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericFoodStamps", "2", data.AnswerOrEmptyString("GenericFoodStamps").Equals("2"), new { @id = Model.Type + "_GenericFoodStamps2" })%>
                    <label for="<%= Model.Type %>_GenericFoodStamps2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericFoodStamps", "0", data.AnswerOrEmptyString("GenericFoodStamps").Equals("0"), new { @id = Model.Type + "_GenericFoodStamps0" })%>
                    <label for="<%= Model.Type %>_GenericFoodStamps0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericFoodStamps", "1", data.AnswerOrEmptyString("GenericFoodStamps").Equals("1"), new { @id = Model.Type + "_GenericFoodStamps1" })%>
                    <label for="<%= Model.Type %>_GenericFoodStamps1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericFoodStampsAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericFoodStampsAmount", data.AnswerOrEmptyString("GenericFoodStampsAmount"), new { @id = Model.Type + "_GenericFoodStampsAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Total</label>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericTotalIncomeAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericTotalIncomeAmount", data.AnswerOrEmptyString("GenericTotalIncomeAmount"), new { @id = Model.Type + "_GenericTotalIncomeAmount", @class = "short" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row strong ac">Assets</div>
            <div class="row">
                <label class="fl strong">Saving Account</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericSavingsAccount", "2", data.AnswerOrEmptyString("GenericSavingsAccount").Equals("2"), new { @id = Model.Type + "_GenericSavingsAccount2" })%>
                    <label for="<%= Model.Type %>_GenericSavingsAccount2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSavingsAccount", "0", data.AnswerOrEmptyString("GenericSavingsAccount").Equals("0"), new { @id = Model.Type + "_GenericSavingsAccount0" })%>
                    <label for="<%= Model.Type %>_GenericSavingsAccount0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSavingsAccount", "1", data.AnswerOrEmptyString("GenericSavingsAccount").Equals("1"), new { @id = Model.Type + "_GenericSavingsAccount1" })%>
                    <label for="<%= Model.Type %>_GenericSavingsAccount1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericSavingsAccountAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericSavingsAccountAmount", data.AnswerOrEmptyString("GenericSavingsAccountAmount"), new { @id = Model.Type + "_GenericSavingsAccountAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Owns Home (Value)</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericOwnsHome", "2", data.AnswerOrEmptyString("GenericOwnsHome").Equals("2"), new { @id = Model.Type + "_GenericOwnsHome2" })%>
                    <label for="<%= Model.Type %>_GenericOwnsHome2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOwnsHome", "0", data.AnswerOrEmptyString("GenericOwnsHome").Equals("0"), new { @id = Model.Type + "_GenericOwnsHome0" })%>
                    <label for="<%= Model.Type %>_GenericOwnsHome0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOwnsHome", "1", data.AnswerOrEmptyString("GenericOwnsHome").Equals("1"), new { @id = Model.Type + "_GenericOwnsHome1" })%>
                    <label for="<%= Model.Type %>_GenericOwnsHome1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericOwnsHomeAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericOwnsHomeAmount", data.AnswerOrEmptyString("GenericOwnsHomeAmount"), new { @id = Model.Type + "_GenericOwnsHomeAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Owns Other Property (Value)</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericOwnsOtherProperty", "2", data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("2"), new { @id = Model.Type + "_GenericOwnsOtherProperty2" })%>
                    <label for="<%= Model.Type %>_GenericOwnsOtherProperty2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOwnsOtherProperty", "0", data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("0"), new { @id = Model.Type + "_GenericOwnsOtherProperty0" })%>
                    <label for="<%= Model.Type %>_GenericOwnsOtherProperty0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOwnsOtherProperty", "1", data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("1"), new { @id = Model.Type + "_GenericOwnsOtherProperty1" })%>
                    <label for="<%= Model.Type %>_GenericOwnsOtherProperty1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericOwnsHomeAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericOwnsOtherPropertyAmount", data.AnswerOrEmptyString("GenericOwnsOtherPropertyAmount"), new { @id = Model.Type + "_GenericOwnsOtherPropertyAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">VA Aid &#38; Assistance</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericVAAid", "2", data.AnswerOrEmptyString("GenericVAAid").Equals("2"), new { @id = Model.Type + "_GenericVAAid2" })%>
                    <label for="<%= Model.Type %>_GenericVAAid2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericVAAid", "0", data.AnswerOrEmptyString("GenericVAAid").Equals("0"), new { @id = Model.Type + "_GenericVAAid0" })%>
                    <label for="<%= Model.Type %>_GenericVAAid0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericVAAid", "1", data.AnswerOrEmptyString("GenericVAAid").Equals("1"), new { @id = Model.Type + "_GenericVAAid1" })%>
                    <label for="<%= Model.Type %>_GenericVAAid1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericVAAidAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericVAAidAmount", data.AnswerOrEmptyString("GenericVAAidAmount"), new { @id = Model.Type + "_GenericOwnsOtherPropertyAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Spouse SSI</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseAssetSSI", "2", data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("2"), new { @id = Model.Type + "_GenericSpouseAssetSSI2" })%>
                    <label for="<%= Model.Type %>_GenericSpouseAssetSSI2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseAssetSSI", "0", data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("0"), new { @id = Model.Type + "_GenericSpouseAssetSSI0" })%>
                    <label for="<%= Model.Type %>_GenericSpouseAssetSSI0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericSpouseAssetSSI", "1", data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("1"), new { @id = Model.Type + "_GenericSpouseAssetSSI1" })%>
                    <label for="<%= Model.Type %>_GenericSpouseAssetSSI1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericSpouseAssetSSIAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericSpouseAssetSSIAmount", data.AnswerOrEmptyString("GenericSpouseAssetSSIAmount"), new { @id = Model.Type + "_GenericSpouseAssetSSIAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Other Assets</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_GenericOtherAssets", "2", data.AnswerOrEmptyString("GenericOtherAssets").Equals("2"), new { @id = Model.Type + "_GenericOtherAssets2" })%>
                    <label for="<%= Model.Type %>_GenericOtherAssets2">N/A</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOtherAssets", "0", data.AnswerOrEmptyString("GenericOtherAssets").Equals("0"), new { @id = Model.Type + "_GenericOtherAssets0" })%>
                    <label for="<%= Model.Type %>_GenericOtherAssets0">No</label>
                    <%= Html.RadioButton(Model.Type + "_GenericOtherAssets", "1", data.AnswerOrEmptyString("GenericOtherAssets").Equals("1"), new { @id = Model.Type + "_GenericOtherAssets1" })%>
                    <label for="<%= Model.Type %>_GenericOtherAssets1">Yes</label>
                </div>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericOtherAssetsAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericOtherAssetsAmount", data.AnswerOrEmptyString("GenericOtherAssetsAmount"), new { @id = Model.Type + "_GenericOtherAssetsAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Total</label>
                <div class="fr radio">
                    <label for="<%= Model.Type %>_GenericTotalAssetsAmount">Amount</label>
                    <%= Html.TextBox(Model.Type + "_GenericTotalAssetsAmount", data.AnswerOrEmptyString("GenericTotalAssetsAmount"), new { @id = Model.Type + "_GenericTotalAssetsAmount", @class = "short" })%>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Transportation for Medical Care Provided by</label>
                <div class="fr radio"><%= Html.TextBox(Model.Type + "_GenericTransportationProvidedBy", data.AnswerOrEmptyString("GenericTransportationProvidedBy"), new { @id = Model.Type + "_GenericTransportationProvidedBy" })%></div>
            </div>
        </div>
        <div class="column wide">
            <div class="row">
                <label class="strong">Identified Problems</label>
                <input type="hidden" name="<%= Model.Type %>_GenericIdentifiedProblems" value="" />
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems1' name='{1}_GenericIdentifiedProblems' value='1' type='checkbox' {0} />", genericIdentifiedProblems.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems1">Patient needs a meal prepared or delivered daily</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems2' name='{1}_GenericIdentifiedProblems' value='2' type='checkbox' {0} />", genericIdentifiedProblems.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems2">Patient/family reported noncompliant to medical plan of care</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems3' name='{1}_GenericIdentifiedProblems' value='3' type='checkbox' {0} />", genericIdentifiedProblems.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems3">Patient needs assistance with housekeeping/shopping</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems4' name='{1}_GenericIdentifiedProblems' value='4' type='checkbox' {0} />", genericIdentifiedProblems.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems4">Patient needs assistance with advanced directive/DPOA/DNR</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems5' name='{1}_GenericIdentifiedProblems' value='5' type='checkbox' {0} />", genericIdentifiedProblems.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems5">Patient needs daily contact to check on him/her</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems6' name='{1}_GenericIdentifiedProblems' value='6' type='checkbox' {0} />", genericIdentifiedProblems.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems6">Patient needs assistance with medical/insurance forms</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems7' name='{1}_GenericIdentifiedProblems' value='7' type='checkbox' {0} />", genericIdentifiedProblems.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems7">Patient needs assistance with alert device (ERS, PRS)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems8' name='{1}_GenericIdentifiedProblems' value='8' type='checkbox' {0} />", genericIdentifiedProblems.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems8">Patient needs assistance with entitlement forms</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems9' name='{1}_GenericIdentifiedProblems' value='9' type='checkbox' {0} />", genericIdentifiedProblems.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems9">Patient needs transportation assistance to medical care</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems10' name='{1}_GenericIdentifiedProblems' value='10' type='checkbox' {0} />", genericIdentifiedProblems.Contains("10").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems10">Patient needs alternative living arrangements</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems11' name='{1}_GenericIdentifiedProblems' value='11' type='checkbox' {0} />", genericIdentifiedProblems.Contains("11").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems11">Patient needs alternative living arrangements</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericIdentifiedProblems12' name='{1}_GenericIdentifiedProblems' value='12' type='checkbox' {0} />", genericIdentifiedProblems.Contains("12").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericIdentifiedProblems12">Psychosocial counseling indicated</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericFurtherInformation" class="strong">Provide further information</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_GenericFurtherInformation", data.AnswerOrEmptyString("GenericFurtherInformation"), 4, 20, new { @id = Model.Type + "_GenericFurtherInformation", @class = "tall" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericIdentifiedStrengths" class="strong">Identified Strengths and Supports</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_GenericIdentifiedStrengths", data.AnswerOrEmptyString("GenericIdentifiedStrengths"), 4, 20, new { @id = Model.Type + "_GenericIdentifiedStrengths", @class = "tall" })%></div>
            </div>
            <div class="row">
                <label class="strong">Planned Interventions</label>
                <input type="hidden" name="<%= Model.Type %>_GenericPlannedInterventions" value="" />
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions1' name='{1}_GenericPlannedInterventions' value='1' type='checkbox' {0} />", genericPlannedInterventions.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions1">Psychosocial Assessment</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions2' name='{1}_GenericPlannedInterventions' value='2' type='checkbox' {0} />", genericPlannedInterventions.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions2">Develop Appropriate Support System</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions3' name='{1}_GenericPlannedInterventions' value='3' type='checkbox' {0} />", genericPlannedInterventions.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions3">Counseling re Disease Process &#38; Management</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions4' name='{1}_GenericPlannedInterventions' value='4' type='checkbox' {0} />", genericPlannedInterventions.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions4">Community Resource Planning &#38; Outreach</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions5' name='{1}_GenericPlannedInterventions' value='5' type='checkbox' {0} />", genericPlannedInterventions.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions5">Counseling re Family Coping</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions6' name='{1}_GenericPlannedInterventions' value='6' type='checkbox' {0} />", genericPlannedInterventions.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions6">Stabilize Current Placement</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions7' name='{1}_GenericPlannedInterventions' value='7' type='checkbox' {0} />", genericPlannedInterventions.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions7">Crisis Intervention</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions8' name='{1}_GenericPlannedInterventions' value='8' type='checkbox' {0} />", genericPlannedInterventions.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions8">Determine/Locate Alternative Placement</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions9' name='{1}_GenericPlannedInterventions' value='9' type='checkbox' {0} />", genericPlannedInterventions.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions9">Long-range Planning &#38; Decision Making</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions10' name='{1}_GenericPlannedInterventions' value='10' type='checkbox' {0} />", genericPlannedInterventions.Contains("10").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions10">Financial Counseling and/or Referrals</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_GenericPlannedInterventions11' name='{1}_GenericPlannedInterventions' value='11' type='checkbox' {0} />", genericPlannedInterventions.Contains("11").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericPlannedInterventions11">Other</label>
                        <div class="more"><%= Html.TextBox(Model.Type + "_GenericPlannedInterventionsOther", data.AnswerOrEmptyString("GenericPlannedInterventionsOther"), new { @id = Model.Type + "_GenericPlannedInterventionsOther" })%></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericInterventionDetails" class="strong">Intervention Details</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_GenericInterventionDetails", data.AnswerOrEmptyString("GenericInterventionDetails"), 4, 20, new { @id = Model.Type + "_GenericInterventionDetails", @class = "tall" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericPlanOfCare" class="strong">Plan of Care</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_GenericPlanOfCare", data.AnswerOrEmptyString("GenericPlanOfCare"), 4, 20, new { @id = Model.Type + "_GenericPlanOfCare", @class = "tall" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Goals</legend>
        <input type="hidden" name="<%= Model.Type %>_GenericGoals" value="" />
        <div class="column wide">
            <div class="row checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals1' name='{1}_GenericGoals' value='1' type='checkbox' {0} />", genericGoals.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals1">Adequate Support System</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals2' name='{1}_GenericGoals' value='2' type='checkbox' {0} />", genericGoals.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals2">Improved Client/Family Coping</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals3' name='{1}_GenericGoals' value='3' type='checkbox' {0} />", genericGoals.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals3">Normal Grieving Process</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals4' name='{1}_GenericGoals' value='4' type='checkbox' {0} />", genericGoals.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals4">Appropriate Goals for Care Set by Client/Family</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals5' name='{1}_GenericGoals' value='5' type='checkbox' {0} />", genericGoals.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals5">Appropriate Community Resource Referrals</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals6' name='{1}_GenericGoals' value='6' type='checkbox' {0} />", genericGoals.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals6">Stable Placement Setting</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericGoals7' name='{1}_GenericGoals' value='7' type='checkbox' {0} />", genericGoals.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals7">Mobilization of Financial Resources</label>
                </div>
                <div class="option">
                    <div class="fr more"><%= Html.TextBox(Model.Type + "_GenericGoalsOther", data.AnswerOrEmptyString("GenericGoalsOther"), new { @id = Model.Type + "_GenericGoalsOther" })%></div>
                    <%= string.Format("<input id='{1}_GenericGoals8' name='{1}_GenericGoals' value='8' type='checkbox' {0} />", genericGoals.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericGoals8">Other</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_GenericVisitGoalDetails" class="strong">Visit Goal Details</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_GenericVisitGoalDetails", data.AnswerOrEmptyString("GenericVisitGoalDetails"), 4, 20, new { @id = Model.Type + "_GenericVisitGoalDetails", @class = "tall" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_Clinician" class="fl strong">Clinician</label>
                <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_SignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="" id="<%= Model.Type %>_SignatureDate" /></div>
            </div>
        </div>
        <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
        <div class="column wide">
            <div class="row narrowest">
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
            </div>
        </div>
        <%  } %>
    </fieldset>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.MSWEvaluationAssessment.Submit($(this),false,'<%= Model.Type %>')">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.MSWEvaluationAssessment.Submit($(this),true,'<%= Model.Type %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.MSWEvaluationAssessment.Submit($(this),false,'<%= Model.Type %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.MSWEvaluationAssessment.Submit($(this),false,'<%= Model.Type %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>