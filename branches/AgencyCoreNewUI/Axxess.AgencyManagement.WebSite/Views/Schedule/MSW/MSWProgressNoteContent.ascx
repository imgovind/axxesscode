﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericLivingSituationChanges = data.AnswerArray("GenericLivingSituationChanges"); %>
<%  string[] genericMentalStatus = data.AnswerArray("GenericMentalStatus"); %>
<%  string[] genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus"); %>
<%  string[] genericVisitGoals = data.AnswerArray("GenericVisitGoals"); %>
<%  string[] genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions"); %>
<%  string[]genericFollowUpPlan = data.AnswerArray("GenericFollowUpPlan"); %>
<fieldset>
    <legend>Living Situation</legend>
    <div class="column">
        <div class="row">
            <div class="checkgroup">
                <div class="option">
                    <%= Html.RadioButton(Model.Type + "_GenericIsLivingSituationChange", "2", data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("2"), new { @id = Model.Type + "_GenericIsLivingSituationChange2" })%>
                    <label for="<%= Model.Type %>_GenericIsLivingSituationChange2">Unchanged from Last Visit</label>
                </div>
                 <div class="option">
                    <%= Html.RadioButton(Model.Type + "_GenericIsLivingSituationChange", "1", data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("1"), new { @id = Model.Type + "_GenericIsLivingSituationChange1" })%>
                    <label for="<%= Model.Type %>_GenericIsLivingSituationChange1">Changed</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericLivingSituationChanged", data.AnswerOrEmptyString("GenericLivingSituationChanged"), new { @id = Model.Type + "_GenericLivingSituationChanged" })%></div>
                </div>
            </div>
        </div>
    </div>
    <div class="column checkgroup">
        <div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericLivingSituationChanges" value="" />
            <div class="checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericLivingSituationChanges1' name='{1}_GenericLivingSituationChanges' value='1' type='checkbox' {0} />", genericLivingSituationChanges.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericLivingSituationChanges1">Update to Primary Caregiver</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericUpdateToPrimaryCaregiver", data.AnswerOrEmptyString("GenericUpdateToPrimaryCaregiver"), new { @id = Model.Type + "_GenericUpdateToPrimaryCaregiver" })%></div>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericLivingSituationChanges2' name='{1}_GenericLivingSituationChanges' value='2' type='checkbox' {0} />", genericLivingSituationChanges.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericLivingSituationChanges2">Changes to Environmental Conditions</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericChangesToEnvironmentalConditions", data.AnswerOrEmptyString("GenericChangesToEnvironmentalConditions"), new { @id = Model.Type + "_GenericChangesToEnvironmentalConditions" })%></div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Psychosocial Assessment</legend>
    <div class="wide column">
        <div class="row">
            <label class="strong">Mental Status</label>
            <input type="hidden" name="<%= Model.Type %>_GenericMentalStatus" value="" />
            <div class="checkgroup narrow">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus1' name='{1}_GenericMentalStatus' value='1' type='checkbox' {0} />", genericMentalStatus.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus1">Alert</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus2' name='{1}_GenericMentalStatus' value='2' type='checkbox' {0} />", genericMentalStatus.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus2">Forgetful</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus3' name='{1}_GenericMentalStatus' value='3' type='checkbox' {0} />", genericMentalStatus.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus3">Confused</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus4' name='{1}_GenericMentalStatus' value='4' type='checkbox' {0} />", genericMentalStatus.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus4">Disoriented</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus5' name='{1}_GenericMentalStatus' value='5' type='checkbox' {0} />", genericMentalStatus.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus5">Oriented</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus6' name='{1}_GenericMentalStatus' value='6' type='checkbox' {0} />", genericMentalStatus.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus6">Lethargic</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus7' name='{1}_GenericMentalStatus' value='7' type='checkbox' {0} />", genericMentalStatus.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus7">Poor Short Term Memory</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus8' name='{1}_GenericMentalStatus' value='8' type='checkbox' {0} />", genericMentalStatus.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus8">Unconscious</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericMentalStatus9' name='{1}_GenericMentalStatus' value='9' type='checkbox' {0} />", genericMentalStatus.Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericMentalStatus9">Cannot Determine</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="strong">Emotional Status</label>
            <input type="hidden" name="<%= Model.Type %>_GenericEmotionalStatus" value="" />
            <div class="checkgroup narrow">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus1' name='{1}_GenericEmotionalStatus' value='1' type='checkbox' {0} />", genericEmotionalStatus.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus1">Stable</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus2' name='{1}_GenericEmotionalStatus' value='2' type='checkbox' {0} />", genericEmotionalStatus.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus2">Tearful</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus3' name='{1}_GenericEmotionalStatus' value='3' type='checkbox' {0} />", genericEmotionalStatus.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus3">Stressed</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus4' name='{1}_GenericEmotionalStatus' value='4' type='checkbox' {0} />", genericEmotionalStatus.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus4">Angry</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus5' name='{1}_GenericEmotionalStatus' value='5' type='checkbox' {0} />", genericEmotionalStatus.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus5">Sad</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus6' name='{1}_GenericEmotionalStatus' value='6' type='checkbox' {0} />", genericEmotionalStatus.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus6">Withdrawn</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus7' name='{1}_GenericEmotionalStatus' value='7' type='checkbox' {0} />", genericEmotionalStatus.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus7">Fearful</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus8' name='{1}_GenericEmotionalStatus' value='8' type='checkbox' {0} />", genericEmotionalStatus.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus8">Anxious</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus9' name='{1}_GenericEmotionalStatus' value='9' type='checkbox' {0} />", genericEmotionalStatus.Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus9">Flat Affect</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericEmotionalStatus10' name='{1}_GenericEmotionalStatus' value='10' type='checkbox' {0} />", genericEmotionalStatus.Contains("10").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericEmotionalStatus10">Other</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericEmotionalStatusOther", data.AnswerOrEmptyString("GenericEmotionalStatusOther"), new { @id = Model.Type + "_GenericEmotionalStatusOther" })%></div>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericPsychosocialAssessment" class="strong">Comment</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericPsychosocialAssessment", data.AnswerOrEmptyString("GenericPsychosocialAssessment"), 4, 20, new { @id = Model.Type + "_GenericPsychosocialAssessment", @class = "tall" })%></div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Visit Goals</legend>
    <div class="column wide">
        <div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericVisitGoals" value="" />
            <div class="checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals1' name='{1}_GenericVisitGoals' value='1' type='checkbox' {0} />", genericVisitGoals.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals1">Adequate Support System</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals2' name='{1}_GenericVisitGoals' value='2' type='checkbox' {0} />", genericVisitGoals.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals2">Improved Client/Family Coping</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals3' name='{1}_GenericVisitGoals' value='3' type='checkbox' {0} />", genericVisitGoals.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals3">Normal Grieving Process</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals4' name='{1}_GenericVisitGoals' value='4' type='checkbox' {0} />", genericVisitGoals.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals4">Appropriate Goals for Care Set by Client/Family</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals5' name='{1}_GenericVisitGoals' value='5' type='checkbox' {0} />", genericVisitGoals.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals5">Appropriate Community Resource Referrals</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals6' name='{1}_GenericVisitGoals' value='6' type='checkbox' {0} />", genericVisitGoals.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals6">Stable Placement Setting</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals7' name='{1}_GenericVisitGoals' value='7' type='checkbox' {0} />", genericVisitGoals.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals7">Mobilization of Financial Resources</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericVisitGoals8' name='{1}_GenericVisitGoals' value='8' type='checkbox' {0} />", genericVisitGoals.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericVisitGoals8">Other</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericVisitGoalsOther", data.AnswerOrEmptyString("GenericVisitGoalsOther"), new { @id = Model.Type + "_GenericVisitGoalsOther" })%></div>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericVisitGoalDetails" class="strong">Details</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericVisitGoalDetails", data.AnswerOrEmptyString("GenericVisitGoalDetails"), 4, 20, new { @id = Model.Type + "_GenericVisitGoalDetails", @class = "tall" })%></div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Visit Interventions</legend>
    <div class="column wide">
        <div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericPlannedInterventions" value="" />
            <div class="checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions1' name='{1}_GenericPlannedInterventions' value='1' type='checkbox' {0} />", genericPlannedInterventions.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions1">Psychosocial Assessment</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions2' name='{1}_GenericPlannedInterventions' value='2' type='checkbox' {0} />", genericPlannedInterventions.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions2">Develop Appropriate Support System</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions3' name='{1}_GenericPlannedInterventions' value='3' type='checkbox' {0} />", genericPlannedInterventions.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions3">Counseling re Disease Process &#38; Management</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions4' name='{1}_GenericPlannedInterventions' value='4' type='checkbox' {0} />", genericPlannedInterventions.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions4">Community Resource Planning &#38; Outreach</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions5' name='{1}_GenericPlannedInterventions' value='5' type='checkbox' {0} />", genericPlannedInterventions.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions5">Counseling re Family Coping</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions6' name='{1}_GenericPlannedInterventions' value='6' type='checkbox' {0} />", genericPlannedInterventions.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions6">Stabilize Current Placement</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions7' name='{1}_GenericPlannedInterventions' value='7' type='checkbox' {0} />", genericPlannedInterventions.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions7">Crisis Intervention</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions8' name='{1}_GenericPlannedInterventions' value='8' type='checkbox' {0} />", genericPlannedInterventions.Contains("8").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions8">Determine/Locate Alternative Placement</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions9' name='{1}_GenericPlannedInterventions' value='9' type='checkbox' {0} />", genericPlannedInterventions.Contains("9").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions9">Long-range Planning &#38; Decision Making</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions10' name='{1}_GenericPlannedInterventions' value='10' type='checkbox' {0} />", genericPlannedInterventions.Contains("10").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions10">Financial Counseling and/or Referrals</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlannedInterventions11' name='{1}_GenericPlannedInterventions' value='11' type='checkbox' {0} />", genericPlannedInterventions.Contains("11").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlannedInterventions11">Other</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericPlannedInterventionsOther", data.AnswerOrEmptyString("GenericPlannedInterventionsOther"), new { @id = Model.Type + "_GenericPlannedInterventionsOther" })%></div>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericInterventionDetails" class="strong">Details</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericInterventionDetails", data.AnswerOrEmptyString("GenericInterventionDetails"), 4, 20, new { @id = Model.Type + "_GenericInterventionDetails", @class = "tall" })%></div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Progress Towards Goals</legend>
    <div class="column wide">
        <div class="row narrowest">
            <label class="strong" for="<%= Model.Type %>_GenericIntensityOfPain" class="fl strong">Progress Towards Goals</label>
            <div class="fr">
                <%  var genericProgressTowardsGoals = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "None", Value = "None" },
                        new SelectListItem { Text = "Slight", Value = "Slight" },
                        new SelectListItem { Text = "Fair", Value = "Fair" },
                        new SelectListItem { Text = "Moderate", Value = "Moderate" },
                        new SelectListItem { Text = "Good", Value = "Good" },
                        new SelectListItem { Text = "Excellent", Value = "Excellent" }
                    }, "Value", "Text", data.AnswerOrEmptyString("GenericProgressTowardsGoals"));%>
                <%= Html.DropDownList(Model.Type + "_GenericProgressTowardsGoals", genericProgressTowardsGoals, new { @id = Model.Type + "_GenericProgressTowardsGoals" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericProgressTowardsGoalsDetails" class="strong">Details</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericProgressTowardsGoalsDetails", data.AnswerOrEmptyString("GenericProgressTowardsGoalsDetails"), 4, 20, new { @id = Model.Type + "_GenericProgressTowardsGoalsDetails", @class = "tall" })%></div>
        </div>
        <div class="row">
            <input type="hidden" name="<%= Model.Type %>_GenericFollowUpPlan" value="" />
            <div class="checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericFollowUpPlan1' name='{1}_GenericFollowUpPlan' value='1' type='checkbox' {0} />", genericFollowUpPlan.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFollowUpPlan1">Follow-up Visit</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericFollowUpPlan2' name='{1}_GenericFollowUpPlan' value='2' type='checkbox' {0} />", genericFollowUpPlan.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFollowUpPlan2">Confer with Team</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericFollowUpPlan3' name='{1}_GenericFollowUpPlan' value='3' type='checkbox' {0} />", genericFollowUpPlan.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFollowUpPlan3">Provide External Referral</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericFollowUpPlan4' name='{1}_GenericFollowUpPlan' value='4' type='checkbox' {0} />", genericFollowUpPlan.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFollowUpPlan4">Discharge SW Services</label>
                </div>
            </div>
        </div>
    </div>
</fieldset>