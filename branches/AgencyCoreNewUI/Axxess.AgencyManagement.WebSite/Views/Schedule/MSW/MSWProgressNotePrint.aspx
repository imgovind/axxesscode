﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  var genericLivingSituationChanges = data.AnswerArray("GenericLivingSituationChanges"); %>
<%  var genericMentalStatus = data.AnswerArray("GenericMentalStatus"); %>
<%  var genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus"); %>
<%  var genericVisitGoals = data.AnswerArray("GenericVisitGoals"); %>
<%  var genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions"); %>
<%  var genericFollowUpPlan = data.AnswerArray("GenericFollowUpPlan"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title><%= Model.Agency.Name %> | Medical Social Worker Progress Note | <%= Model.Patient.DisplayName %></title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
    </head>
    <body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean() %>%3Cbr /%3E<%= location.AddressLine1.Clean() %><%= location.AddressLine2.Clean() %>%3Cbr /%3E<%= location.AddressCity.Clean() %><%= location.AddressStateCode.Clean().ToString().ToUpper() %>&#160; <%= location.AddressZipCode.Clean() %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker Progress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient.DisplayNameWithMi.Clean() %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22octocol%22%3E%3Cspan%3E%3Cstrong%3EMR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient.PatientIdNumber.Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("VisitDate").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("TimeIn").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("TimeOut").Clean() %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean() %>%3Cbr /%3E<%= location.AddressLine1.Clean() %><%= location.AddressLine2.Clean() %>%3Cbr /%3E<%= location.AddressCity.Clean() %><%= location.AddressStateCode.Clean().ToString().ToUpper() %>&#160; <%= location.AddressZipCode.Clean() %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker Progress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient.DisplayNameWithMi.Clean() %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.SignatureText.Clean() %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model.SignatureDate.IsValidDate() ? Model.SignatureDate.Clean() : string.Empty %>" +
            "%3C/span%3E%3C/span%3E";
        printview.addsection(
            printview.col(2,
                printview.checkbox("Unchanged from Last Visit",<%= data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("2").ToString().ToLower() %>) +
                printview.checkbox("Changed <%= data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("1") ? data.AnswerOrEmptyString("GenericLivingSituationChanged").Clean() %>",<%= data.AnswerOrEmptyString("GenericIsLivingSituationChange").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("Update to Primary Caregiver",<%= genericLivingSituationChanges.Contains("1").ToString().ToLower() %>) +
                printview.span("<%= genericLivingSituationChanges.Contains("1") ? data.AnswerOrEmptyString("GenericUpdateToPrimaryCaregiver").Clean() %>",0,1) +
                printview.checkbox("Changes to Environmental Conditions",<%= genericLivingSituationChanges.Contains("2").ToString().ToLower() %>) +
                printview.span("<%= genericLivingSituationChanges.Contains("2") ? data.AnswerOrEmptyString("GenericChangesToEnvironmentalConditions").Clean() %>",0,1)),
            "Living Situation");
        printview.addsection(
            printview.span("Mental Status",true) +
            printview.col(3,
                printview.checkbox("Alert",<%= genericMentalStatus.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Forgetful",<%= genericMentalStatus.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Confused",<%= genericMentalStatus.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Disoriented",<%= genericMentalStatus.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Oriented",<%= genericMentalStatus.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Lethargic",<%= genericMentalStatus.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Poor Short Term Memory",<%= genericMentalStatus.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Unconscious",<%= genericMentalStatus.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Cannot Determine",<%= genericMentalStatus.Contains("9").ToString().ToLower() %>)) +
            printview.span("Emotional Status",true) +
            printview.col(3,
                printview.checkbox("Stable",<%= genericEmotionalStatus.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Tearful",<%= genericEmotionalStatus.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Stressed",<%= genericEmotionalStatus.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Angry",<%= genericEmotionalStatus.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Sad",<%= genericEmotionalStatus.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Withdrawn",<%= genericEmotionalStatus.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Fearful",<%= genericEmotionalStatus.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Anxious",<%= genericEmotionalStatus.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Flat Affect",<%= genericEmotionalStatus.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericEmotionalStatus.Contains("10").ToString().ToLower() %>) +
                printview.span("<%= genericEmotionalStatus.Contains("10") ? data.AnswerOrEmptyString("GenericEmotionalStatusOther").Clean() : string.Empty %>",0,1)) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPsychosocialAssessment").Clean() %>",0,2),
            "Psychosocial Assessment");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Adequate Support System",<%= genericVisitGoals.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Improved Client/Family Coping",<%= genericVisitGoals.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Normal Grieving Process",<%= genericVisitGoals.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Appropriate Goals for Care Set by Client/Family",<%= genericVisitGoals.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Appropriate Community Resource Referrals",<%= genericVisitGoals.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Stable Placement Setting",<%= genericVisitGoals.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Mobilization of Financial Resources",<%= genericVisitGoals.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericVisitGoals.Contains("8").ToString().ToLower() %>) +
                printview.span("<%= genericVisitGoals.Contains("8") ? data.AnswerOrEmptyString("GenericVisitGoalsOther").Clean() : string.Empty %>",0,1)) +
            printview.span("Visit Goal Details",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericVisitGoalDetails").Clean() %>",0,2) +
            printview.span("Visit Interventions",true) +
            printview.col(3,
                printview.checkbox("Psychosocial Assessment",<%= genericPlannedInterventions.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Develop Appropriate Support System",<%= genericPlannedInterventions.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Counseling re Disease Process &#38; Management",<%= genericPlannedInterventions.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Community Resource Planning &#38; Outreach",<%= genericPlannedInterventions.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Counseling re Family Coping",<%= genericPlannedInterventions.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Stabilize Current Placement",<%= genericPlannedInterventions.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Crisis Intervention",<%= genericPlannedInterventions.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Determine/Locate Alternative Placement",<%= genericPlannedInterventions.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Long-range Planning &#38; Decision Making",<%= genericPlannedInterventions.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Financial Counseling and/or Referrals",<%= genericPlannedInterventions.Contains("10").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericPlannedInterventions.Contains("11").ToString().ToLower() %>) +
                printview.span("<%= genericPlannedInterventions.Contains("11") ? data.AnswerOrEmptyString("GenericPlannedInterventionsOther").Clean() : string.Empty %>",0,1)) +
            printview.span("Intervention Details",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericInterventionDetails").Clean() %>",0,2),
            "Visit Goals");
        printview.addsection(
            printview.col(2,
                printview.span("Progress Towards Goals",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericProgressTowardsGoals").Clean() %>",0,1)) +
            printview.span("<%= data.AnswerOrEmptyString("GenericProgressTowardsGoalsDetails").Clean() %>",0,2) +
            printview.span("Follow-up Plan (check all that apply)",true) +
            printview.col(2,
                printview.checkbox("Follow-up Visit",<%= genericFollowUpPlan.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Confer with Team",<%= genericFollowUpPlan.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Provide External Referral",<%= genericFollowUpPlan.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Discharge SW Services",<%= genericFollowUpPlan.Contains("4").ToString().ToLower() %>)),
            "Progress Towards Goals");
<%  }).Render(); %>
    </body>
</html>