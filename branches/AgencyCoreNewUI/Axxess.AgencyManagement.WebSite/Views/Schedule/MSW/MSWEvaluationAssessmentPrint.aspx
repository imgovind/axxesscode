﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  var genericLivingSituation = data.AnswerArray("GenericLivingSituation"); %>
<%  var genericReasonForReferral = data.AnswerArray("GenericReasonForReferral"); %>
<%  var genericMentalStatus = data.AnswerArray("GenericMentalStatus"); %>
<%  var genericEmotionalStatus = data.AnswerArray("GenericEmotionalStatus"); %>
<%  var genericGoals = data.AnswerArray("GenericGoals"); %>
<%  var genericIdentifiedProblems = data.AnswerArray("GenericIdentifiedProblems"); %>
<%  var genericPlannedInterventions = data.AnswerArray("GenericPlannedInterventions"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Medical Social Worker <%= Model != null && Model.Type.IsNotNullOrEmpty() && Model.Type != "MSWEvaluationAssessment" ? Model.Type.Substring(3) : "Evaluation"%><%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
</head>
<body></body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker <%= Model != null && Model.Type.IsNotNullOrEmpty() && Model.Type != "MSWEvaluationAssessment" ? Model.Type.Substring(3) : "Evaluation"%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22octocol%22%3E%3Cspan%3E%3Cstrong%3EMR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("VisitDate").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("TimeIn").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("TimeOut").Clean() %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedical Social Worker <%= Model != null && Model.Type.IsNotNullOrEmpty() && Model.Type != "MSWEvaluationAssessment" ? Model.Type.Substring(3) : "Evaluation"%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EPhysician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3C/span%3E%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
        printview.addsection(
            printview.span("Patient Lives",true) +
            printview.col(4,
                printview.checkbox("Alone",<%= genericLivingSituation.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("With Friend/Family",<%= genericLivingSituation.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("With Dependent",<%= genericLivingSituation.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("With Spouse/Partner",<%= genericLivingSituation.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("With Religious Community",<%= genericLivingSituation.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Assisted Living",<%= genericLivingSituation.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("With Partner",<%= genericLivingSituation.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Has Paid Caregiver",<%= genericLivingSituation.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Has Live-In, Paid Caregiver",<%= genericLivingSituation.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericLivingSituation.Contains("10").ToString().ToLower() %>) +
                printview.span("<%= genericLivingSituation.Contains("10") ? data.AnswerOrEmptyString("GenericLivingSituationOther").Clean() : string.Empty %>",0,1)) +
            printview.col(2,
                printview.span("Primary Caregiver",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericPrimaryCaregiver").Clean() %>",0,1) +
                printview.span("The quality of care that patient receives at home",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericQualityOfCare").Clean() %>",0,1) +
                printview.span("Environmental Conditions",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericEnvironmentalConditions").Clean() %>",0,1)),
            "Living Situation");
        printview.addsection(
            printview.col(2,
                printview.checkbox("Assessment for Psychosocial Coping",<%= genericReasonForReferral.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Lives Alone, No Identified Caregiver",<%= genericReasonForReferral.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Counseling re Disease Process or Management",<%= genericReasonForReferral.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Solo Caregiver for Minor Children and/or Other Dependents",<%= genericReasonForReferral.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Family/Caregiver Coping Support",<%= genericReasonForReferral.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Reported Noncompliance to Medical Plan of Care",<%= genericReasonForReferral.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Hospice Eligibility",<%= genericReasonForReferral.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Suspected Negligence or Abuse",<%= genericReasonForReferral.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Financial/Practical Resources",<%= genericReasonForReferral.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Assistance with Advanced Directive / DPOA/DNR",<%= genericReasonForReferral.Contains("10").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericReasonForReferral.Contains("11").ToString().ToLower() %>) +
                printview.span("<%= genericReasonForReferral.Contains("11") ? data.AnswerOrEmptyString("GenericReasonForReferralOther").Clean() : string.Empty %>",0,1)),
            "Reason(s) for Referral");
        printview.addsection(
            printview.span("Mental Status",true) +
            printview.col(4,
                printview.checkbox("Alert",<%= genericMentalStatus.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Forgetful",<%= genericMentalStatus.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Confused",<%= genericMentalStatus.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Disoriented",<%= genericMentalStatus.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Oriented",<%= genericMentalStatus.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Lethargic",<%= genericMentalStatus.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Poor Short-Term Memory",<%= genericMentalStatus.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Unconscious",<%= genericMentalStatus.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Cannot Determine",<%= genericMentalStatus.Contains("9").ToString().ToLower() %>)) +
            printview.span("Emotional Status",true) +
            printview.col(4,
                printview.checkbox("Stable",<%= genericEmotionalStatus.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Tearful",<%= genericEmotionalStatus.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Stressed",<%= genericEmotionalStatus.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Angry",<%= genericEmotionalStatus.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Sad",<%= genericEmotionalStatus.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Withdrawn",<%= genericEmotionalStatus.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Fearful",<%= genericEmotionalStatus.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Anxious",<%= genericEmotionalStatus.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Flat Affect",<%= genericEmotionalStatus.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericEmotionalStatus.Contains("10").ToString().ToLower() %>) +
                printview.span("<%= genericEmotionalStatus.Contains("10") ? data.AnswerOrEmptyString("GenericEmotionalStatusOther").Clean() : string.Empty %>",0,1)) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPsychosocialAssessment").Clean() %>",0,10),
            "Psychosocial Assessment");
        printview.addsection(
            printview.col(2,
                printview.checkbox("Adequate Support System",<%= genericGoals.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Improved Client/Family Coping",<%= genericGoals.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Normal Grieving Process",<%= genericGoals.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Appropriate Goals for Care Set by Client/Family",<%= genericGoals.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Appropriate Community Resource Referrals",<%= genericGoals.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Stable Placement Setting",<%= genericGoals.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Mobilization of Financial Resources",<%= genericGoals.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Other <%= genericGoals.Contains("8") ? data.AnswerOrEmptyString("GenericGoalsOther").Clean() : string.Empty %>",<%= genericGoals.Contains("8").ToString().ToLower() %>)) +
            printview.span("Visit Goal Details",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericVisitGoalDetails").Clean() %>",0,10),
            "Goals");
        printview.addsection(
            printview.col(2,
                printview.span("Income Sources",true) +
                printview.col(4,
                    printview.span("N/A",true) +
                    printview.span("No",true) +
                    printview.span("Yes",true) +
                    printview.span("Amount",true)) +
                printview.span("Employment",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericEmployment").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericEmployment").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericEmployment").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericEmploymentAmount").Clean() %>",0,1)) +
                printview.span("Pt Social Security",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPtSocialSecurity").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericPtSocialSecurityAmount").Clean() %>",0,1)) +
                printview.span("Spouse Social Security",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseSocialSecurity").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSpouseSocialSecurityAmount").Clean() %>",0,1)) +
                printview.span("Pt SSI",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPtSSI").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPtSSI").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPtSSI").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericPtSSIAmount").Clean() %>",0,1)) +
                printview.span("Spouse SSI",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseSSI").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseSSI").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseSSI").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSpouseSSIAmount").Clean() %>",0,1)) +
                printview.span("Pensions",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPensions").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPensions").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericPensions").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericPensionsAmount").Clean() %>",0,1)) +
                printview.span("Other Income",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOtherIncome").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOtherIncome").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOtherIncome").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericOtherIncomeAmount").Clean() %>",0,1)) +
                printview.span("Food Stamps",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericFoodStamps").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericFoodStamps").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericFoodStamps").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericFoodStampsAmount").Clean() %>",0,1)) +
                printview.span("Total Income",true) +
                printview.col(4,
                    printview.span("") +
                    printview.span("") +
                    printview.span("") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericTotalIncomeAmount").Clean() %>",0,1)) +
                printview.span("Assets",true) +
                printview.col(4,
                    printview.span("N/A",true) +
                    printview.span("No",true) +
                    printview.span("Yes",true) +
                    printview.span("Amount",true)) +
                printview.span("Savings Account",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSavingsAccount").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSavingsAccount").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSavingsAccount").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSavingsAccountAmount").Clean() %>",0,1)) +
                printview.span("Owns Home (value)",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOwnsHome").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOwnsHome").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOwnsHome").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericOwnsHomeAmount").Clean() %>",0,1)) +
                printview.span("Owns Other Property (value)",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOwnsOtherProperty").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericOwnsOtherPropertyAmount").Clean() %>",0,1)) +
                printview.span("VA Aid &#38; Assistance",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericVAAid").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericVAAid").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericVAAid").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericVAAidAmount").Clean() %>",0,1)) +
                printview.span("Spouse SSI",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericSpouseAssetSSI").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSpouseAssetSSIAmount").Clean()%>",0,1)) +
                printview.span("Other Assets",true) +
                printview.col(4,
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOtherAssets").Equals("2").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOtherAssets").Equals("0").ToString().ToLower() %>) +
                    printview.checkbox("",<%= data.AnswerOrEmptyString("GenericOtherAssets").Equals("1").ToString().ToLower() %>) +
                    printview.span("<%= data.AnswerOrEmptyString("GenericOtherAssetsAmount").Clean() %>",0,1)) +
                printview.span("Total Assets",true) +
                printview.col(4,
                    printview.span("") +
                    printview.span("") +
                    printview.span("") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericTotalAssetsAmount").Clean() %>",0,1)) +
                printview.span("Transportation for medical care provided by",true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericTransportationProvidedBy").Clean() %>",0,1)) +
            printview.span("Identified Problems",true) +
            printview.col(2,
                printview.checkbox("Patient needs a meal prepared or delivered daily",<%= genericIdentifiedProblems.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Patient/family reported noncompliant to plan of care",<%= genericIdentifiedProblems.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Patient needs assistance with housekeeping/shopping",<%= genericIdentifiedProblems.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Patient needs assist. with advanced directive/DPOA/DNR",<%= genericIdentifiedProblems.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Patient needs daily contact to check on him/her",<%= genericIdentifiedProblems.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Patient needs assistance with medical/insurance forms",<%= genericIdentifiedProblems.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Patient needs assistance with alert device (ERS, PRS)",<%= genericIdentifiedProblems.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Patient needs assistance with entitlement forms",<%= genericIdentifiedProblems.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Patient needs transportation assistance to medical care",<%= genericIdentifiedProblems.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Patient needs alternative living arrangements",<%= genericIdentifiedProblems.Contains("10").ToString().ToLower() %>) +
                printview.checkbox("Patient needs alternative living arrangements",<%= genericIdentifiedProblems.Contains("11").ToString().ToLower() %>) +
                printview.checkbox("Psychosocial counseling indicated",<%= genericIdentifiedProblems.Contains("12").ToString().ToLower() %>)) +
            printview.span("Provide further information",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFurtherInformation").Clean() %>",0,4) +
            printview.span("Identified Strengths and Supports",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericIdentifiedStrengths").Clean() %>",0,4) +
            printview.span("Planned Interventions",true) +
            printview.col(2,
                printview.checkbox("Psychosocial Assessment",<%= genericPlannedInterventions.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Develop Appropriate Support System",<%= genericPlannedInterventions.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Counseling re Disease Process &#38; Management",<%= genericPlannedInterventions.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Community Resource Planning &#38; Outreach",<%= genericPlannedInterventions.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Counseling re Family Coping",<%= genericPlannedInterventions.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Stabilize Current Placement",<%= genericPlannedInterventions.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Crisis Intervention",<%= genericPlannedInterventions.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Determine/Locate Alternative Placement",<%= genericPlannedInterventions.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Long-range Planning &#38; Decision Making",<%= genericPlannedInterventions.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Financial Counseling and/or Referrals",<%= genericPlannedInterventions.Contains("10").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= genericPlannedInterventions.Contains("11").ToString().ToLower() %>) +
                printview.span("<%= genericPlannedInterventions.Contains("11") ? data.AnswerOrEmptyString("GenericPlannedInterventionsOther").Clean() : string.Empty %>",0,1)) +
            printview.span("Intervention Details",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericInterventionDetails").Clean() %>",0,4) +
            printview.span("Plan of Care",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPlanOfCare").Clean() %>",0,4),
            "Financial Assessment"); <%
    }).Render(); %>
</html>
