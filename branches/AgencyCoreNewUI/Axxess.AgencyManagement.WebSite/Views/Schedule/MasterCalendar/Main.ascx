﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<span class="wintitle">Master Calendar | <%= Model.DisplayName %></span>
<div class="wrapper main">
    <div class="content"><% Html.RenderPartial("MasterCalendar/Content", Model); %></div>
</div>