﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<%  var scheduleEvents = Model.ScheduleEvents; %>
<%  var startWeekDay = (int)Model.StartDate.DayOfWeek; %>
<%  var startDate = Model.StartDate; %>
<%  var endDate = Model.EndDate; %>
<%  var currentDate = Model.StartDate.AddDays(-startWeekDay); %>
<input type="hidden" name="patientId" value="<%= Model.PatientId %>" />
<div class="ac">
<%  if (Model.HasPrevious) { %>
    <span class="button top left">
        <a class="navigate previous-episode" guid="<%= Model.PreviousEpisode.Id %>"><span class="largefont">&#8617;</span>Previous Episode</a>
    </span>
<%  } %>
<%  if (Model.HasNext) { %>
    <span class="button top right">
        <a class="navigate next-episode" guid="<%= Model.NextEpisode.Id %>">Next Episode<span class="largefont">&#8618;</span></a>
    </span>
<%  } %>
    <h1><%= Model.DisplayNameWithMi %></h1>
<%  if (Model.Detail != null && Model.Detail.FrequencyList.IsNotNullOrEmpty()) { %>
    <h2>Frequencies: <%= Model.Detail.FrequencyList %></h2>
<%  } %>
    <div class="buttons inline">
        <ul>
            <li><a class="navigate" guid="<%= Model.Id %>" status="Refresh Master Calendar">Refresh</a></li>
            <li><a class="print" guid="<%= Model.Id %>" status="Print Master Calendar">Print</a></li>
        </ul>
    </div>
    <br />
    <div class="cal big ac">
        <table>
            <thead>
                <tr>
                    <td colspan="8" class="caltitle">Episode: <%= Model.StartDateFormatted %> &#8211; <%= Model.EndDateFormatted %></td>
                </tr>
                <tr>
                    <th></th>
                    <th>Sun</th>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                </tr>
            </thead>
            <tbody>
<%  for (int i = 0; i < 10; i++) { %>
    <%  if (currentDate.AddDays(i * 7) < endDate) { %>
                <tr>
                    <th>Week <%= i + 1 %></th>
        <%  int addedDate = i * 7; %>
        <%  for (int j = 0; j <= 6; j++) { %>
            <%  var specificDate = currentDate.AddDays(j + addedDate); %>
            <%  if (specificDate < startDate || specificDate > endDate) { %>
                    <td class="inactive"></td>
            <%  } else { %>
                <%  var currentSchedules = scheduleEvents.FindAll(e => e.EventDate == specificDate); %>
                <%  string tooltip = ""; %>
                <%  string label = ""; %>
                <%  if (currentSchedules.Count > 0) foreach (var evnt in currentSchedules) { %>
                    <%  if (!evnt.IsMissedVisit && evnt.Discipline != Disciplines.Orders.ToString()) { %>
                        <%  tooltip += "<strong>" + EnumExtensions.GetCustomShortDescription((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), evnt.DisciplineTask)).ToUpper() + "</strong><br/>Employee: " + evnt.UserName + "<br/>Status: " + evnt.StatusName + "<br/>"; %>                    
                        <%  label += EnumExtensions.GetCustomShortDescription((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), evnt.DisciplineTask)).ToUpper() + "<br/>"; %>                    
                    <%  } %>
                <%  } %>
                    <td>
                        <div class="datelabel" tooltip="<%= tooltip %>">
                            <%= string.Format("{0:MM/dd}", specificDate) %>
                            <br />
                            <%= label %>
                        </div>
                    </td>
            <%  } %>
        <%  } %>
                </tr>
    <%  } %>
<%  } %>
            </tbody>
        </table>
    </div>
</div>