﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<%= Html.Hidden("VisitDate", Model.EventDate, new { @id = "" })%>
<fieldset>
    <legend>Details</legend>
    <table class="form"><tbody>
        <tr>
            <td><label for="Schedule_Detail_Task" class="bold">Task</label></td>
            <td><label for="Schedule_Detail_VisitDate" class="bold">Scheduled Date</label></td>
            <td><label for="Schedule_Detail_AssignedTo" class="bold">Clinician</label></td>
            <td><label for="Schedule_Detail_Status" class="bold">Status</label></td>
        </tr><tr>
            <td><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = "Schedule_Detail_DisciplineTask", @class = "required notzero" })%></td>
            <td><input type="date" name="EventDate" value="<%= Model.EventDate.ToShortDateString() %>" id="Schedule_Detail_EventDate" class="required" /></td>
            <td><% if (!Model.IsComplete) { %><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users required notzero" })%><% } else { %><%= Html.Hidden("UserId", Model.UserId)%><%= Model.UserName%><% } %></td>
            <td><%= Html.Status("Status", Model.Status.ToString(), Model.DisciplineTask, new { @id = "Schedule_Detail_Status" })%></td>
        </tr>
    </tbody></table>               
</fieldset>
