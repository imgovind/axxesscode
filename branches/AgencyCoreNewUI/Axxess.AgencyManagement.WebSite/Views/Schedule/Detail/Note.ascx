﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<fieldset>
    <legend>Details</legend>
    <table class="form"><tbody>
        <tr>
            <td><label for="Schedule_Detail_Task" class="bold">Task</label></td>
            <td><label for="Schedule_Detail_Billable" class="bold">Billable</label></td>
            <td><% if(Model.IsSkilledNurseNote()){ %><label class="float-left">Supply</label> <%} %></td>
        </tr>
        <tr>
            <td><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = "Schedule_Detail_DisciplineTask", @class = "required notzero" })%></td>
            <td><%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "Schedule_Detail_Billable" })%></td>
            <td>
               <% if(Model.IsSkilledNurseNote()){ %> <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" class=\"float-left\" >Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li></ul></div> <%} %>
            </td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td><label for="Schedule_Detail_VisitDate" class="bold">Scheduled Date</label></td>
            <td><label for="Schedule_Detail_Status" class="bold">Actual Visit Date</label></td>
            <td><label for="Schedule_Detail_TimeIn" class="bold">Time In</label></td>
            <td><label for="Schedule_Detail_TimeOut" class="bold">Time Out</label></td>
        </tr>
        <tr>
            <td><input type="date" name="EventDate" value="<%= Model.EventDate.ToShortDateString() %>" id="Schedule_Detail_EventDate" class="required" /></td>
            <td><input type="date" name="VisitDate" value="<%= Model.VisitDate.ToShortDateString()%>" id="Schedule_Detail_VisitDate" class="required" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeIn" name="TimeIn" class="spinners" value="<%= Model.TimeIn %>" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeOut" name="TimeOut" class="spinners" value="<%= Model.TimeOut %>" /></td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td><label for="Schedule_Detail_Status" class="bold">Status</label></td>
            <td><label for="Schedule_Detail_AssignedTo" class="bold">Clinician</label></td>
            <td><label for="Schedule_Detail_Surcharge" class="bold">Surcharge</label></td>
            <td><label for="Schedule_Detail_AssociatedMileage" class="bold">Associated Mileage</label></td>
        </tr>
        <tr>
            <td><%= Html.Status("Status", Model.Status.ToString(), Model.DisciplineTask, new { @id = "Schedule_Detail_Status" }) %></td>
            <td><% if (!Model.IsComplete || Model.DisciplineTask == (int) DisciplineTasks.FaceToFaceEncounter){ %><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users required notzero" })%><% } else { %><%= Html.Hidden("UserId", Model.UserId)%><%= Model.UserName %><% } %></td>
            <td><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "Schedule_Detail_Surcharge", @class = "text digits input_wrapper", @maxlength = "5" })%></td>
            <td><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "Schedule_Detail_AssociatedMileage", @class = "text digits input_wrapper", @maxlength = "5" })%></td>
        </tr><% if (Model.Discipline.IsEqual(Disciplines.Orders.ToString())) { %><tr><td colspan="4"></td></tr>
        <tr>
            <td colspan="4"><label for="Schedule_Detail_PhysicianId" class="bold">Physician</label></td>
        </tr>
        <tr>
            <td colspan="4">
                <%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Schedule_Detail_PhysicianId", @class = "physician-picker" })%>
            </td>
        </tr><% } %>
    </tbody></table>               
</fieldset>
    