﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<span class="wintitle">Task Details | <%= Model.DisciplineTaskName %> | <%= Model.PatientName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Details/Update", "Schedule", FormMethod.Post, new { @id = "ScheduleDetails_Form" })) { %>
    <%= Html.Hidden("EventId", Model.EventId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("Discipline", Model.Discipline)%>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="fl strong">MRN</label>
                <div class="fr"><%= Model.PatientIdNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Episode</label>
                <div class="fr"><%= Model.StartDate.ToShortDateString().ToZeroFilled() %> &#8211; <%= Model.EndDate.ToShortDateString().ToZeroFilled() %></div>
                <div class="fr">
                    <div class="checkgroup">
                        <div class="option">
                            <%= Html.CheckBox("IsEpisodeReassiged", false, new { @id = "ScheduleDetails_IsEpisodeReassiged" })%>
                            <label for="ScheduleDetails_IsEpisodeReassiged">Reassign to a Different Episode</label>
                            <div class="more"><%= Html.PatientEpisodes("NewEpisodeId", Model.EpisodeId.ToString(), Model.PatientId, new { @id = "ScheduleDetails_NewEpisodeId" }) %></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_DisciplineTask" class="fl strong">Task</label>
                <div class="fr"><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = "ScheduleDetails_DisciplineTask", @class = "required notzero" })%></div>
            </div>
            <div class="row">
    <%  if (!Model.IsComplete || Model.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter) { %>
                <label for="ScheduleDetails_AssignedTo" class="fl strong">Clinician</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "ScheduleDetails_AssignedTo", @class = "required notzero" }) %></div>
    <%  } else { %>
                <label class="fl strong">Clinician</label>
                <div class="fr"><%= Html.Hidden("UserId", Model.UserId)%><%= Model.UserName %></div>
    <%  } %>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "ScheduleDetails_Billable" })%>
                        <label for="ScheduleDetails_Billable">Billable</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="ScheduleDetails_EventDate" class="fl strong">Scheduled Date</label>
                <div class="fr"><input type="text" name="EventDate" value="<%= Model.EventDate.ToShortDateString() %>" id="ScheduleDetails_EventDate" class="required date-picker" /></div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_VisitDate" class="fl strong">Actual Visit Date</label>
                <div class="fr"><input type="text" name="VisitDate" value="<%= Model.VisitDate.ToShortDateString()%>" id="ScheduleDetails_VisitDate" class="required date-picker" /></div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_TimeIn" class="fl strong">Time In</label>
                <div class="fr"><input type="text" id="ScheduleDetails_TimeIn" name="TimeIn" value="<%= Model.TimeIn %>" class="time-picker" /></div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_TimeOut" class="fl strong">Time Out</label>
                <div class="fr"><input type="text" id="ScheduleDetails_TimeOut" name="TimeOut" value="<%= Model.TimeOut %>" class="time-picker" /></div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_Status" class="fl strong">Status</label>
                <div class="fr"><%= Html.Status("Status", Model.Status.ToString(), Model.DisciplineTask, new { @id = "ScheduleDetails_Status" }) %></div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_Surcharge" class="fl strong">Surcharge</label>
                <div class="fr"><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "ScheduleDetails_Surcharge", @class = "decimal", @maxlength = "5" })%></div>
            </div>
            <div class="row">
                <label for="ScheduleDetails_AssociatedMileage" class="fl strong">Associated Mileage</label>
                <div class="fr"><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "ScheduleDetails_AssociatedMileage", @class = "numeric", @maxlength = "5" })%></div>
            </div>
            <div class="row">
                <div class="fr button-with-arrow"><a class="supply-worksheet" status="Supply Worksheet">Supply Worksheet</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments <span class="img icon note"></span><em>(Yellow Sticky Note)</em></legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="ScheduleDetails_Comments" name="Comments" class="tall" maxcharacters="500"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Attachments</legend>
        <div class="wide column">
            <div class="narrower row">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                <p>There are <span class="assest-count"><%= Model.Assets.Count.ToString() %></span> attachment<%= Model.Assets.Count == 1 ? string.Empty : "s" %>.</p>
            </div>
    <%  foreach (Guid assetId in Model.Assets) { %>
            <div class="narrower row"><%= Html.Asset(assetId) + " | " + string.Format("<a class=\"link\" onclick=\"Schedule.DeleteAsset('{0}','{1}','{2}','{3}',$(this));return false\">Delete</a>", assetId, Model.EventId, Model.EpisodeId, Model.PatientId) %></div>
    <%  } %>
            <div class="narrower row">
                <p>Use the upload fields below to upload files associated with this scheduled task.</p>
            </div>
            <div class="narrower row"><input id="ScheduleDetails_File1" type="file" name="Attachment1" /></div>
            <div class="narrower row"><input id="ScheduleDetails_File2" type="file" name="Attachment2" /></div>
            <div class="narrower row"><input id="ScheduleDetails_File3" type="file" name="Attachment3" /></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>