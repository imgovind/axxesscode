﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<%  string[] isVitalSigns = data.AnswerArray("isVitalSigns"); %>
<input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
<input name="<%= Model.Type %>_IsVitalSigns" value=" " type="hidden" />

<fieldset>
    <legend>
        <input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
        Vital Sign Parameters
        &#8212;
        <%= string.Format("<input class='radio' id='" + Model.Type + "_IsVitalSignParameter' name='" + Model.Type + "_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1"))%>
        <label for="<%= Model.Type %>_IsVitalSignParameter">N/A</label>
    </legend>
    <table id="<%= Model.Type %>_IsVitalSignParameterMore" class="fixed vitalsigns ac">
        <tbody>
            <tr>
                <th></th>
                <th>SBP</th>
                <th>DBP</th>
                <th>HR</th>
                <th>Resp</th>
                <th>Temp</th>
                <th>Weight</th>
            </tr>
            <tr>
                <th>greater than (&#62;)</th>
                <td><%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_PulseGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_RespirationGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_TempGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_WeightGreaterThan", @class = "fill" })%></td>
            </tr>
            <tr>
                <th>less than (&#60;)</th>
                <td><%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = Model.Type + "_SystolicBPLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = Model.Type + "_DiastolicBPLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = Model.Type + "_PulseLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = Model.Type + "_RespirationLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = Model.Type + "_TempLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = Model.Type + "_WeightLessThan", @class = "fill" })%></td>
            </tr>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend>
        <input name="<%= Model.Type %>_IsVitalSigns" value=" " type="hidden" />
        Vital Signs
        &#8212;
        <%= string.Format("<input class='radio' id='" + Model.Type + "_IsVitalSigns' name='" + Model.Type + "_IsVitalSigns' value='1' type='checkbox' {0} />", isVitalSigns != null && isVitalSigns.Contains("1"))%>
        <label for="<%= Model.Type %>_IsVitalSigns">N/A</label>
    </legend>
    <table id="<%= Model.Type %>_IsVitalSignsMore" class="fixed vitalsignparameter ac">
        <tbody>
            <tr>
                <th>BP</th>
                <th>HR</th>
                <th>Temp</th>
                <th>Resp</th>
                <th>Weight</th>
            </tr>
            <tr>
                <td><%= Html.TextBox(Model.Type + "_VitalSignBPVal", data.ContainsKey("VitalSignBPVal") ? data["VitalSignBPVal"].Answer : "", new { @id = Model.Type + "_VitalSignBPVal", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_VitalSignHRVal", data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer : "", new { @id = Model.Type + "_VitalSignHRVal", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_VitalSignTempVal", data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer : "", new { @id = Model.Type + "_VitalSignTempVal", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_VitalSignRespVal", data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer : "", new { @id = Model.Type + "_VitalSignRespVal", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_VitalSignWeightVal", data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer : "", new { @id = Model.Type + "_VitalSignWeightVal", @class = "fill" })%></td>
            </tr>
            <tr>
                <td colspan="5">
                   
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Tasks</legend>
    
    <div class="column">
        <table class="auto">
            <thead>
                <tr>
                    <th colspan="3" class="ac strong">Assignment</th>
                    <th colspan="3" class="ac strong">Status</th>
                </tr>
                <tr>
                    <th colspan="3" class="al strong">Personal Care</th>
                    <th class="ac strong">Completed</th>
                    <th class="ac strong">Refuse</th>
                    <th class="ac strong">N/A</th>
                </tr>
                <tr class="alt">
                    <td colspan="3" class="al">Bed Bath</td>
                    <td class="ac">
                        <%= Html.Hidden(Model.Type + "_PersonalCareBedBath", " ", new { @id = "" })%>
                        <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "2", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2", new { @id = "" })%>
                    </td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "1", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1", new { @id = ""})%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "0", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0", new { @id = ""})%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with Chair Bath</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareAssistWithChairBath", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "2", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "1", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "0", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Tub Bath</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareTubBath", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "2", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "1", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "0", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Shower</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareShower", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "2", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "1", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShower", "0", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Shower w/Chair</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareShowerWithChair", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "2", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "1", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "0", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Shampoo Hair</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareShampooHair", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "2", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "1", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "0", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Hair Care/Comb Hair</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareHairCare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "2", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "1", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "0", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Oral Care</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareOralCare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "2", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "1", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "0", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Skin Care</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareSkinCare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "2", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "1", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "0", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Pericare</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCarePericare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "2", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "1", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "0", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Nail Care</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareNailCare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "2", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "1", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "0", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Shave</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareShave", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "2", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "1", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareShave", "0", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with Dressing</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_PersonalCareAssistWithDressing", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "2", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "1", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "0", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <th colspan="3" class="al strong">Nutrition</th>
                    <th class="ac strong">Completed</th>
                    <th class="ac strong">Refuse</th>
                    <th class="ac strong">N/A</th>
                </tr>
                <tr>
                    <td colspan="3" class="al">Meal Set-up</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_NutritionMealSetUp", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "2", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "1", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "0", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with Feeding</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_NutritioAssistWithFeeding", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "2", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "1", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "0", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0", new { @id = "" })%></td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="column">
        <table class="auto">
            <thead>
                <tr>
                    <th colspan="3" class="ac strong">Assignment</th>
                    <th colspan="3" class="ac strong">Status</th>
                </tr>
                <tr>
                    <th colspan="3" class="al strong">Elimination</th>
                    <th class="ac strong">Completed</th>
                    <th class="ac strong">Refuse</th>
                    <th class="ac strong">N/A</th>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with Bed Pan/Urinal</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_EliminationAssistWithBedPan", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "2", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "1", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "0", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with BSC</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_EliminationAssistBSC", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "2", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "1", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "0", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Incontinence Care</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_EliminationIncontinenceCare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "2", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "1", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "0", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Empty Drainage Bag</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_EliminationEmptyDrainageBag", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "2", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "1", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "0", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Record Bowel Movement</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_EliminationRecordBowelMovement", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "2", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "1", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "0", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Catheter Care</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_EliminationCatheterCare", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "2", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "1", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "0", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <th colspan="3" class="al strong">Activity</th>
                    <th class="ac strong">Completed</th>
                    <th class="ac strong">Refuse</th>
                    <th class="ac strong">N/A</th>
                </tr>
                <tr>
                    <td colspan="3" class="al">Dangle on Side of Bed</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_ActivityDangleOnSideOfBed", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "2", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "1", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "0", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Turn &#38; Position</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_ActivityTurnPosition", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "2", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "1", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "0", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with Transfer</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_ActivityAssistWithTransfer", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "2", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "1", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "0", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Assist with Ambulation</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_ActivityAssistWithAmbulation", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "2", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "1", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "0", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Range of Motion</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_ActivityRangeOfMotion", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "2", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "1", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "0", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <th colspan="3" class="al strong">Household Task</th>
                    <th class="ac strong">Completed</th>
                    <th class="ac strong">Refuse</th>
                    <th class="ac strong">N/A</th>
                </tr>
                <tr>
                    <td colspan="3" class="al">Make Bed</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_HouseholdTaskMakeBed", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "2", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "1", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "0", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Change Linen</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_HouseholdTaskChangeLinen", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "2", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "1", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "0", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Light Housekeeping</td>
                    <td class="ac"><%= Html.Hidden(Model.Type + "_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "2", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "1", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1", new { @id = "" })%></td>
                    <td class="ac"><%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "0", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0", new { @id = "" })%></td>
                </tr>
                <tr>
                    <td colspan="3" class="al">Other (Describe):</td><td colspan="3"><%= Html.TextBox(Model.Type + "_HouseholdTaskOther", data.ContainsKey("HouseholdTaskOther") ? data["HouseholdTaskOther"].Answer : string.Empty, new { @id = Model.Type + "_HouseholdTaskOther", @class = "fill" })%></td>
                </tr>
            </thead>
        </table>
    </div>
</fieldset>

<fieldset>
<legend>Comments</legend>
    <div class="column ac">
        <%= Html.Templates(Model.Type + "_CommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Comment" })%><br />
        <%= Html.TextArea(Model.Type + "_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = Model.Type + "_Comment", @class = "fill tallest" })%>
    </div>
</fieldset>