﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  var scheduleEvents = Model.Episode.ScheduleEvents ?? new List<ScheduleEvent>(); %>
<%  DateTime[] startdate = new DateTime[3], enddate = new DateTime[3], currentdate = new DateTime[3]; %>
<%  startdate[0] = DateUtilities.GetStartOfMonth(Model.Episode.StartDate.Month, Model.Episode.StartDate.Year); %>
<%  enddate[0] = DateUtilities.GetEndOfMonth(Model.Episode.StartDate.Month, Model.Episode.StartDate.Year); %>
<%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
<%  startdate[1] = enddate[0].AddDays(1); %>
<%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
<%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
<%  startdate[2] = enddate[1].AddDays(1); %>
<%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
<%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
<%= Html.Hidden("patientId", Model.Episode.PatientId) %>
<%= Html.Hidden("episodeId", Model.Episode.Id) %>
<div class="wrapper main">
    <fieldset>
        <legend>Quick Employee Scheduler</legend>
        <div class="wide column">
            <div class="narrowest row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.Episode.DisplayNameWithMi %></div>
            </div>
            <div class="narrowest row">
                <label class="fl strong">Episode</label>
                <div class="fr"><%= Model.Episode.StartDate.ToShortDateString()%> &#8211; <%= Model.Episode.EndDate.ToShortDateString()%></div>
            </div>
            <div class="narrowest row">
                <label class="fl strong" for="MultiDay_Task">Task</label>
                <div class="fr"><%= Html.MultipleDisciplineTasks("MultiDay_Task")%></div>
            </div>
            <div class="narrowest row">
                <label class="fl strong" for="MultiDay_User">User/Employee</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "MultiDay_User", "", new { @id = "MultiDay_User" })%></div>
            </div>
            <div class="ac row">
                <div class="strong">To schedule visits for the selected user, click on the desired dates in the calendar below</div>
                <div class="trical">
<%  for (int c = 0; c < 3; c++) { %>
                    <div class="cal">
                        <table>
                            <thead class="ac">
                                <tr>
                                    <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td>
                                </tr>
                                <tr>
                                    <th>Su</th>
                                    <th>Mo</th>
                                    <th>Tu</th>
                                    <th>We</th>
                                    <th>Th</th>
                                    <th>Fr</th>
                                    <th>Sa</th>
                                </tr>
                            </thead>
                            <tbody>
    <%  var maxWeek = DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); %>
    <%  for (int i = 0; i <= maxWeek; i++) { %>
                                <tr>
        <%  int addedDate = (i) * 7; %>
        <%  if (currentdate[c].AddDays(7 + addedDate).Date >= Model.Episode.StartDate.Date && currentdate[c].AddDays(addedDate) <= enddate[c] && currentdate[c].Date.AddDays(addedDate) <= Model.Episode.EndDate.Date) for (int j = 0; j <= 6; j++) { %>
            <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
            <%  if (specificDate < Model.Episode.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate.Date > Model.Episode.EndDate.Date) { %>
                                    <td class="inactive"></td>
            <%  } else { %>
                                    <td date="<%= specificDate.ToJavaScriptTicks() %>">
                                        <div class="datelabel"><a><%= specificDate.Day %></a></div>
                                    </td>
            <%  } %>
        <%  } %>
                                </tr>
    <%  } %>
                            </tbody>
                        </table>
                    </div>
<%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="add-schedule another">Schedule</a></li>
            <li><a class="add-schedule">Schedule and Close</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>