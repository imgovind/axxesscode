﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] ffbs = data.AnswerArray("FFBS"); %>
<%  string[] bsTolerated = data.AnswerArray("BSTolerated"); %>
<%  string[] insulinTolerated = data.AnswerArray("InsulinTolerated"); %>
<%  string[] paDueTo = data.AnswerArray("PADueTo"); %>
<%  string[] supplies = data.AnswerArray("Supplies"); %>
<%  string[] isVitalSignParameter = data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null; %>
<%  string[] isVitalSigns = data.ContainsKey("IsVitalSigns") && data["IsVitalSigns"].Answer != "" ? data["IsVitalSigns"].Answer.Split(',') : null; %>
<%  
    if (Model.Type.IsNullOrEmpty()) Model.Type = "UAPInsulinPrepAdminVisit"; %>

<fieldset>
    <legend>
        <input name="<%= Model.Type %>_IsVitalSignParameter" value=" " type="hidden" />
        Vital Sign Parameters
        &#8212;
        <%= string.Format("<input class='radio' id='UAPInsulinPrepAdminVisit_IsVitalSignParameter' name='UAPInsulinPrepAdminVisit_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1"))%>
        <label for="<%= Model.Type %>_IsVitalSignParameter">N/A</label>
    </legend>
    <table id="<%= Model.Type %>_IsVitalSignParameterMore" class="fixed vitalsigns ac">
        <tbody>
            <tr>
                <th></th>
                <th>SBP</th>
                <th>DBP</th>
                <th>HR</th>
                <th>Resp</th>
                <th>Temp</th>
                <th>Weight</th>
            </tr>
            <tr>
                <th>greater than (&#62;)</th>
                <td><%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_PulseGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_RespirationGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_TempGreaterThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_WeightGreaterThan", @class = "fill" })%></td>
            </tr>
            <tr>
                <th>less than (&#60;)</th>
                <td><%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = Model.Type + "_SystolicBPLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = Model.Type + "_DiastolicBPLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = Model.Type + "_PulseLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = Model.Type + "_RespirationLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = Model.Type + "_TempLessThan", @class = "fill" })%></td>
                <td><%= Html.TextBox(Model.Type + "_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = Model.Type + "_WeightLessThan", @class = "fill" })%></td>
            </tr>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend>
        <input name="<%= Model.Type %>_IsVitalSigns" value=" " type="hidden" />
        Vital Signs
        &#8212;
        <%= string.Format("<input class='radio' id='UAPInsulinPrepAdminVisit_IsVitalSigns' name='UAPInsulinPrepAdminVisit_IsVitalSigns' value='1' type='checkbox' {0} />", isVitalSigns != null && isVitalSigns.Contains("1"))%>
        <label for="<%= Model.Type %>_IsVitalSigns">N/A</label>
    </legend>
    <table id="<%= Model.Type %>_IsVitalSignsMore" class="fixed vitalsignparameter ac">
        <tbody>
            <tr>
                <th>SBP</th>
                <th>DBP</th>
                <th>HR</th>
                <th>Resp</th>
                <th>Temp</th>
                <th>Weight</th>
            </tr>
            <tr>
                <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignSBPVal", data.ContainsKey("VitalSignSBPVal") ? data["VitalSignSBPVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignSBPVal", @class = "fill" })%></td>
                <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignDBPVal", data.ContainsKey("VitalSignDBPVal") ? data["VitalSignDBPVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignDBPVal", @class = "fill" })%></td>
                <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignHRVal", data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignHRVal", @class = "fill" })%></td>
                <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignRespVal", data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignRespVal", @class = "fill" })%></td>
                <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignTempVal", data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignTempVal", @class = "fill" })%></td>
                <td><%= Html.TextBox("UAPInsulinPrepAdminVisit_VitalSignWeightVal", data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer : "", new { @id = "UAPInsulinPrepAdminVisit_VitalSignWeightVal", @class = "fill" })%></td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="align-left">
                        <%= string.Format("<input id='UAPInsulinPrepAdminVisit_FootCheck1' class='float-left radio' name='UAPInsulinPrepAdminVisit_FootCheck' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FootCheck").IsEqual("1") ? "checked=checked" : "") %>
                        <label for="UAPInsulinPrepAdminVisit_FootCheck1">Foot Check</label>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Insulin Administration</legend>
    <div class="column">
        <div class="row">
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_FFBS' class='float-left radio' name='UAPInsulinPrepAdminVisit_FFBS' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FFBS").Equals("1").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_FFBS">FFBS</label>
                    <div class="more" id="UAPInsulinPrepAdminVisit_FFBSMore">
                        <div class="">
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSDigit", data.AnswerOrEmptyString("FFBSDigit"), new { @id = "UAPInsulinPrepAdminVisit_FFBSDigit", @class = "short", @maxlength = "10" })%>
                            <label for="UAPInsulinPrepAdminVisit_FFBSDigit">digit</label>
                        </div>
                        <div class="">
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSHandGlucometer", data.AnswerOrEmptyString("FFBSHandGlucometer"), new { @id = "UAPInsulinPrepAdminVisit_FFBSHandGlucometer", @class = "short", @maxlength = "10" })%>
                            <label for="UAPInsulinPrepAdminVisit_FFBSHandGlucometer">hand via glucometer</label>
                        </div>
                        <div class="">
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_FFBSMgdl", data.AnswerOrEmptyString("FFBSMgdl"), new { @id = "UAPInsulinPrepAdminVisit_FFBSMgdl", @class = "short", @maxlength = "10" })%>
                            <label for="UAPInsulinPrepAdminVisit_FFBSmgdl">mg/dl</label>
                        </div>
                        <div class="">
                            <%= string.Format("<input id='UAPInsulinPrepAdminVisit_FFBSPressureApplied1' class='radio' name='UAPInsulinPrepAdminVisit_FFBSPressureApplied' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("FFBSPressureApplied").IsEqual("1") ? "checked=checked" : "")%>
                            <label for="UAPInsulinPrepAdminVisit_FFBSPressureApplied1">pressure applied</label>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_NFFBS' class='float-left radio' name='UAPInsulinPrepAdminVisit_NFFBS' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("NFFBS").Equals("1").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_NFFBS">NFFBS</label>
                    <div class="more" id="UAPInsulinPrepAdminVisit_NFFBSMore">
                        <div>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSDigit", data.AnswerOrEmptyString("NFFBSDigit"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSDigit", @class = "short", @maxlength = "10" })%>
                            <label for="UAPInsulinPrepAdminVisit_NFFBSDigit">digit</label>
                        </div>
                        <div>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSHandGlucometer", data.AnswerOrEmptyString("NFFBSHandGlucometer"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSHandGlucometer", @class = "short", @maxlength = "10" })%>
                            <label for="UAPInsulinPrepAdminVisit_NFFBSHandGlucometer">hand via glucometer</label>
                        </div>
                        <div>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_NFFBSMgdl", data.AnswerOrEmptyString("NFFBSMgdl"), new { @id = "UAPInsulinPrepAdminVisit_NFFBSMgdl", @class = "short", @maxlength = "10" })%>
                            <label for="UAPInsulinPrepAdminVisit_NFFBSmgdl">mg/dl</label>
                        </div>
                        <div>
                            <%= string.Format("<input id='UAPInsulinPrepAdminVisit_NFFBSPressureApplied1' class='radio' name='UAPInsulinPrepAdminVisit_NFFBSPressureApplied' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("NFFBSPressureApplied").IsEqual("1") ? "checked=checked" : "")%>
                            <label for="UAPInsulinPrepAdminVisit_NFFBSPressureApplied1">pressure applied</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="float-left">Tolerated</label>
            <div class="fr">
                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_BSTolerated1' class='radio' name='UAPInsulinPrepAdminVisit_BSTolerated' value='1' type='radio' {0} />", bsTolerated.Contains("1").ToChecked())%>
                <label for="UAPInsulinPrepAdminVisit_BSTolerated1" class="inlineradio">Well</label>
                <%= string.Format("<input id='UAPInsulinPrepAdminVisit_BSTolerated2' class='radio' name='UAPInsulinPrepAdminVisit_BSTolerated' value='2' type='radio' {0} />", bsTolerated.Contains("2").ToChecked())%>
                <label for="UAPInsulinPrepAdminVisit_BSTolerated2" class="inlineradio">Poor</label>
            </div>
        </div>
        <div class="row">
            <label for="UAPInsulinPrepAdminVisit_LastMeal" class="float-left">Last Meal</label>
            <div class="fr"><%= Html.TextBox("UAPInsulinPrepAdminVisit_LastMeal", data.AnswerOrEmptyString("LastMeal"), new { @id = "UAPInsulinPrepAdminVisit_LastMeal", @maxlength = "" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTech1' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTech' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTech").IsEqual("1") ? "checked=checked" : string.Empty)%>
                    <label for="UAPInsulinPrepAdminVisit_AsepticTech1">Aseptic Tech.</label>
                    <div id="UAPInsulinPrepAdminVisit_AsepticTech1More" class="more">
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechType">Type</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechType", data.AnswerOrEmptyString("AsepticTechType"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechType", @maxlength = "30" })%>
                        </div>
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechAmount">Amount</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechAmount", data.AnswerOrEmptyString("AsepticTechAmount"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechAmount", @class = "vitals", @maxlength = "5" })%>
                            units
                        </div>
                        <div class="row">
                            <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechRoute' class='radio' name='UAPInsulinPrepAdminVisit_AsepticTechRoute' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechRoute").IsEqual("1") ? "checked=checked" : string.Empty)%>
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechRoute">Subcut. Route</label>
                        </div>
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechSite">Site</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSite", data.AnswerOrEmptyString("AsepticTechSite"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSite", @maxlength = "" })%>
                        </div>
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechTime">Time</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechTime", data.AnswerOrEmptyString("AsepticTechTime"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechTime", @class = "oe", @maxlength = "10" })%>
                        </div>
                    </div>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechSlide1' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTechSlide' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechSlide").IsEqual("1") ? "checked=checked" : string.Empty)%>
                    <label for="UAPInsulinPrepAdminVisit_AsepticTechSlide1">Aseptic Tech. Slide</label>
                    <div id="UAPInsulinPrepAdminVisit_AsepticTechSlide1More" class="more">
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideType">Per Sliding Scale</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideType", data.AnswerOrEmptyString("AsepticTechSlideType"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideType", @maxlength = "30" })%>
                        </div>
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideAmount">Amount</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideAmount", data.AnswerOrEmptyString("AsepticTechSlideAmount"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideAmount", @class = "vitals", @maxlength = "5" })%>
                            units
                        </div>
                        <div class="row">
                            <%= string.Format("<input id='UAPInsulinPrepAdminVisit_AsepticTechSlideRoute' class='float-left radio' name='UAPInsulinPrepAdminVisit_AsepticTechSlideRoute' value='1' type='checkbox' {0} />", data.AnswerOrEmptyString("AsepticTechSlideRoute").IsEqual("1") ? "checked=checked" : string.Empty)%>
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideRoute">Subcut. Route</label>
                        </div>
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideSite">Site</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideSite", data.AnswerOrEmptyString("AsepticTechSlideSite"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideSite", @maxlength = "" })%>
                        </div>
                        <div class="row">
                            <label for="UAPInsulinPrepAdminVisit_AsepticTechSlideTime">Time</label>
                            <%= Html.TextBox("UAPInsulinPrepAdminVisit_AsepticTechSlideTime", data.AnswerOrEmptyString("AsepticTechSlideTime"), new { @id = "UAPInsulinPrepAdminVisit_AsepticTechSlideTime", @class = "oe", @maxlength = "10" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="align-left strong">Tolerated:</div>
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_InsulinTolerated1' class='float-left radio' name='UAPInsulinPrepAdminVisit_InsulinTolerated' value='1' type='checkbox' {0} />", insulinTolerated.Contains("1").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_InsulinTolerated1">Well</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_InsulinTolerated2' class='float-left radio' name='UAPInsulinPrepAdminVisit_InsulinTolerated' value='2' type='checkbox' {0} />", insulinTolerated.Contains("2").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_InsulinTolerated2">Poor</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_InsulinTolerated3' class='float-left radio' name='UAPInsulinPrepAdminVisit_InsulinTolerated' value='3' type='checkbox' {0} />", insulinTolerated.Contains("3").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_InsulinTolerated3">Standard Precautions Maintained</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="align-left strong">P/A Due To:</div>
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo1' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='1' type='checkbox' {0} />", paDueTo.Contains("1").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_PADueTo1">Poor Vision</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo2' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='2' type='checkbox' {0} />", paDueTo.Contains("2").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_PADueTo2">Poor Manual Dexterity</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo3' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='3' type='checkbox' {0} />", paDueTo.Contains("3").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_PADueTo3">Fear</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo4' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='4' type='checkbox' {0} />", paDueTo.Contains("4").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_PADueTo4">Cognition</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo5' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='5' type='checkbox' {0} />", paDueTo.Contains("5").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_PADueTo5">No Willing cg</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_PADueTo6' class='float-left radio' name='UAPInsulinPrepAdminVisit_PADueTo' value='6' type='checkbox' {0} />", paDueTo.Contains("6").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_PADueTo6">Other</label>
                    <div class="more"><%= Html.TextBox("UAPInsulinPrepAdminVisit_PADueToOther", data.AnswerOrEmptyString("PADueToOther"), new { @id = "UAPInsulinPrepAdminVisit_PADueToOther", @class = "fill" })%></div>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<fieldset class="half fl">
    <legend>RN Notification</legend>
    <div class="column">
        <div class="row">
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_ClinicianNotified' name='UAPInsulinPrepAdminVisit_ClinicianNotified' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("ClinicianNotified") && data["ClinicianNotified"] != null && data["ClinicianNotified"].Answer.IsNotNullOrEmpty() && data["ClinicianNotified"].Answer.IsEqual("Yes")) %>
                    <label for="UAPInsulinPrepAdminVisit_ClinicianNotified">RN Notified</label>
                    <div class="more">
                        <div class="row"><%= Html.Clinicians("UAPInsulinPrepAdminVisit_ClinicianId", data.ContainsKey("ClinicianId") ? data["ClinicianId"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_ClinicianId", @class = "fill" })%></div>
                        <div class="row"><input type="text" style="width:85%" class="date-picker" name="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" value="<%= data.ContainsKey("ClinicianNotifiedDate") && data["ClinicianNotifiedDate"] != null && data["ClinicianNotifiedDate"].Answer.IsNotNullOrEmpty() && data["ClinicianNotifiedDate"].Answer.IsValidDate() ? data["ClinicianNotifiedDate"].Answer : string.Empty %>" id="UAPInsulinPrepAdminVisit_ClinicianNotifiedDate" /></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="UAPInsulinPrepAdminVisit_InstructionsGiven" class="strong">Instructions Given</label>
            <div class="align-center"><%= Html.TextArea("UAPInsulinPrepAdminVisit_InstructionsGiven", data.AnswerOrEmptyString("InstructionsGiven"), new { @class = "fill", @id = Model.Type + "UAPInsulinPrepAdminVisit_InstructionsGiven" })%></div>
        </div>
    </div>
</fieldset>

<fieldset class="half fr">
    <legend>MD Notification</legend>
     <div class="column">
        <div class="row">
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_DoctorNotified' name='UAPInsulinPrepAdminVisit_DoctorNotified' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("DoctorNotified") && data["DoctorNotified"] != null && data["DoctorNotified"].Answer.IsNotNullOrEmpty() && data["DoctorNotified"].Answer.IsEqual("Yes")) %>
                    <label for="UAPInsulinPrepAdminVisit_DoctorNotified">MD Notified</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_NewOrders' name='UAPInsulinPrepAdminVisit_NewOrders' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("NewOrders") && data["NewOrders"] != null && data["NewOrders"].Answer.IsNotNullOrEmpty() && data["NewOrders"].Answer.IsEqual("Yes")) %>
                    <label for="UAPInsulinPrepAdminVisit_NewOrders">New Order(s)</label>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset>
    <legend>Supplies</legend>
    <div class="column wide">
        <div class="row">
            <div class="checkgroup narrow">
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies1' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='1' type='checkbox' {0} />", supplies.Contains("1").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_Supplies1">NS Gloves</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies2' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='2' type='checkbox' {0} />", supplies.Contains("2").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_Supplies2">Alcohol Pads</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies3' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='3' type='checkbox' {0} />", supplies.Contains("3").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_Supplies3">Probe Covers</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies4' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='4' type='checkbox' {0} />", supplies.Contains("4").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_Supplies4">Insulin Syringe</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='UAPInsulinPrepAdminVisit_Supplies5' class='float-left radio' name='UAPInsulinPrepAdminVisit_Supplies' value='5' type='checkbox' {0} />", supplies.Contains("5").ToChecked())%>
                    <label for="UAPInsulinPrepAdminVisit_Supplies5">Other</label>
                    <div class="more fr">
                        <%= Html.TextBox("UAPInsulinPrepAdminVisit_SuppliesOther", data.AnswerOrEmptyString("SuppliesOther"), new { @id = "UAPInsulinPrepAdminVisit_SuppliesOther" })%>
                    </div>
                </div>
            </div>
    </div>
</fieldset>

<fieldset>
    <legend>Comments</legend>
    <div class="column wide">
        <div class="row ac">
            <%= Html.Templates("UAPInsulinPrepAdminVisit_CommentTemplates", new { @class = "Templates", @template = "#UAPInsulinPrepAdminVisit_Comment" })%><br />
            <%= Html.TextArea("UAPInsulinPrepAdminVisit_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "UAPInsulinPrepAdminVisit_Comment", @class = "fill tallest" })%>
        </div>
    </div>
</fieldset>