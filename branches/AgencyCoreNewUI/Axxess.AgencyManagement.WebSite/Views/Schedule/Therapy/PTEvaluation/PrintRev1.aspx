﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT " +
        "<%= Model.Type == "PTEvaluation" ? "Evaluation" : ""%><%= Model.Type == "PTReEvaluation" ? "Re-Evaluation" : ""%><%= Model.Type == "PTMaintenance" ? "Maintenance Visit" : ""%>" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy <%= Model.Type == "PTReEvaluation" ? "Re-" : ""%>Evaluation%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Diagnosis/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/History/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.span("Speech:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSpeech").Clean() %>",0,1) +
            printview.span("Vision:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentVision").Clean() %>",0,1) +
            printview.span("Hearing:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentHearing").Clean() %>",0,1) +
            printview.span("Skin:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSkin").Clean() %>",0,1) +
            printview.span("Edema:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEdema").Clean() %>",0,1) +
            printview.span("Muscle Tone:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentMuscleTone").Clean() %>",0,1) +
            printview.span("Coordination:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentCoordination").Clean() %>",0,1) +
            printview.span("Sensation:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentSensation").Clean() %>",0,1) +
            printview.span("Endurance:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPhysicalAssessmentEndurance").Clean() %>",0,1)),
        "Physical Assessment");
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(2,
            printview.span("Orientation:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentOrientation").Clean() %>",0,1) +
            printview.span("LOC:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMentalAssessmentLOC").Clean() %>",0,1)),
        "Mental Assessment",2);
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(2,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMEAvailable").Clean()%>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMENeeds").Clean()%>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDMESuggestion").Clean()%>",0,1)),
        "DME");
    printview.addsubsection(
        printview.col(3,
            printview.span("Level",true) +
            printview.checkbox("One", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("Multiple", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLevel").Equals("0").ToString().ToLower() %>) +
            printview.span("Stairs",true) +
            printview.checkbox("Yes", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationStairs").Equals("0").ToString().ToLower() %>)) +
        printview.col(4,
            printview.span("Lives",true) +
            printview.checkbox("Alone", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("2").ToString().ToLower() %>) +
            printview.checkbox("Family", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("Friends", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationLives").Equals("0").ToString().ToLower() %>)) +
        printview.col(3,
            printview.span("Support",true) +
            printview.checkbox("Retirement", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationSupport").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("Assisted Living", <%= data.AnswerOrEmptyString("GenericHomeSafetyEvaluationSupport").Equals("0").ToString().ToLower() %>)),
        "Home Safety Evaluation");
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(2,
            printview.span("Assistive Device", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSAssistiveDevice").Clean() %>%",0,1) +
            printview.span("Description", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSDescription").Clean()%>",0,1) +
            printview.span("Posture", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWBSPosture").Clean()%>",0,1)),
        "WBS",3);
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericAssessmentComment").Clean() %>",0,3),"Assessment");
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentCodes/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ShortTermGoals/PrintRev1.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/LongTermGoals/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("OT", <%= data.AnswerOrEmptyString("GenericDisciplineRecommendation").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("MSW", <%= data.AnswerOrEmptyString("GenericDisciplineRecommendation").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("ST", <%= data.AnswerOrEmptyString("GenericDisciplineRecommendation").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Podiatrist", <%= data.AnswerOrEmptyString("GenericDisciplineRecommendation").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.span("Other", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDisciplineRecommendationOther").Clean() %>",0,1)) +
        printview.col(4,
            printview.span("Reason", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericDisciplineRecommendationReason").Clean() %>",0,1) +
            printview.span("Frequency", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericFrequency").Clean() %> x wk<%= data.AnswerOrEmptyString("GenericFrequencyFor").Clean() %>",0,1) +
            printview.span("Rehab Potential", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericRehabPotential").Clean() %>",0,1) +
            printview.span("Prognosis", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPrognosis").Clean() %>",0,1)),
        "Other Discipline Recommendation");
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericCareCoordination").Clean() %>",0,3),"Care Coordination");
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("GenericSkilledCareProvided").Clean() %>",0,3),"Skilled Care Provided This Visit");
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>
</body>
</html>
