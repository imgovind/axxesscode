﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<fieldset>
    <legend>Bed Mobility</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/BedMobility/FormRev1.ascx", Model); %>
</fieldset>

<fieldset>
    <legend>Physical Assessment</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/PhysicalAssessment/FormRev1.ascx", Model); %>
</fieldset>

<fieldset>
    <legend>Transfer</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Transfer/FormRev1.ascx", Model); %>
</fieldset>

<fieldset class="half fl">
    <legend>Gait</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Gait/FormRev1.ascx", Model); %>
</fieldset>

<fieldset class="half fr">
    <legend>W/C Mobility</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/FormRev1.ascx", Model); %>
</fieldset>

<div class="clear"></div>

<fieldset class="half fl">
    <legend>Pain</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
</fieldset>

<fieldset class="half fr">
    <legend>Reason for Discharge</legend>
    <%  string[] genericReasonForDischarge = data.AnswerArray("GenericReasonForDischarge"); %>
    <input type="hidden" name="<%= Model.Type %>_GenericReasonForDischarge" value="" />
    <div class="column">
        <div class="row">
            <div class="checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", genericReasonForDischarge.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", genericReasonForDischarge.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge2">No Longer Homebound</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", genericReasonForDischarge.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge3">Per Patient/Family Request</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", genericReasonForDischarge.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge4">Prolonged On-Hold Status</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", genericReasonForDischarge.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge5">Prolonged On-Hold Status</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", genericReasonForDischarge.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge6">Hospitalized</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", genericReasonForDischarge.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericReasonForDischarge7">Expired</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericReasonForDischargeOther" class="float-left">Other</label>
            <%= Html.TextBox(Model.Type+"_GenericReasonForDischargeOther", data.AnswerOrEmptyString("GenericReasonForDischargeOther"), new { @class = "fr", @id = Model.Type+"_GenericReasonForDischargeOther" })%>
        </div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset>
    <legend>Treatment Plan</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/TreatmentPlan/FormRev1.ascx", Model); %>
</fieldset>


<fieldset>
    <legend></legend>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericFrequency" class="float-left">Frequency</label>
            <div class="fr"><%= Html.TextBox(Model.Type+"_GenericFrequency", data.AnswerOrEmptyString("GenericFrequency"), new { @id = Model.Type+"_GenericFrequency" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericDuration" class="float-left">Duration</label>
            <div class="fr"><%= Html.TextBox(Model.Type+"_GenericDuration", data.AnswerOrEmptyString("GenericDuration"), new { @id = Model.Type+"_GenericDuration" })%></div>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Narrative</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Narrative/FormRev1.ascx", Model); %>
</fieldset>