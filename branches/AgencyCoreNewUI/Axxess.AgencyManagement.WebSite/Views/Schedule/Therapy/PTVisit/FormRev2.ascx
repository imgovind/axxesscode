﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
<%= Html.Hidden(Model.Type+"_PatientId", Model.PatientId)%>
<%= Html.Hidden(Model.Type+"_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden(Model.Type+"_EventId", Model.EventId)%>
<%= Html.Hidden("Type", Model.Type)%>
<%= Html.Hidden("Version", Model.Version)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="6"><%= Model.TypeName %> (version 2)</th>
            </tr>
            <tr>
                <td colspan="3" class="bigtext"><%= Model.Patient.DisplayName %></td>
                <td colspan="2"><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type +"_DisciplineTask", @class = "required notzero" })%></td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl%></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div>
                        <label for="<%= Model.Type %>_MR" class="float-left">MR#</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = Model.Type + "_MR", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date</label>
                        <div class="fr"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_TimeIn", data.AnswerOrEmptyString("TimeIn"), new { @id = Model.Type + "_TimeIn", @class = "time-picker complete-required" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_TimeOut", data.AnswerOrEmptyString("TimeOut"), new { @id = Model.Type + "_TimeOut", @class = "time-picker complete-required" })%></div>
                    </div>
                </td>
                <td colspan="3">
    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                    <div>
                        <label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes</label>
                        <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type+"_PreviousNotes" })%></div>
                    </div>
                    <div class="clear"></div>
    <%  } %>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = Model.Type + "_PrimaryDiagnosis", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label
                        ><div class="fr"><%= Html.TextBox(Model.Type + "_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = Model.Type + "_PrimaryDiagnosis1", @readonly = "readonly" })%></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="<%= Model.Type %>_Content"><% Html.RenderPartial("~/Views/Schedule/Therapy/PTVisit/ContentRev2.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th>Electronic Signature</th>
            </tr>
            <tr>
                <td>
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician</label>
                        <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date</label>
                        <div class="fr"><input type="text" name="<%= Model.Type %>_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() ? data["SignatureDate"].Answer : "" %>" id="<%= Model.Type %>_SignatureDate" class="date-picker complete-required" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <tr>
                <td>
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                    <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.PTVisit.Submit($(this),false,'<%= Model.Type %>')">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.PTVisit.Submit($(this),true,'<%= Model.Type %>')">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.PTVisit.Submit($(this),false,'<%= Model.Type %>')">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.PTVisit.Submit($(this),false,'<%= Model.Type %>')">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>