﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Homebound Reason</th>
            <th colspan="2">Functional Limitations</th>
            <th colspan="2">Vital Signs</th>
        </tr>
        <tr class="align-left">
            <td colspan="2">
                <% string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : data.AnswerArray("GenericHomeboundStatusAssist"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericHomeBoundStatus" value="" />
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus7' class='radio' name='{1}_GenericHomeBoundStatus' value='7' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus7">Requires considerable and taxing effort.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus8' class='radio' name='{1}_GenericHomeBoundStatus' value='8' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus8">Medical restriction.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus1' class='radio' name='{1}_GenericHomeBoundStatus' value='1' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus1">Needs assist with transfer.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus2' class='radio' name='{1}_GenericHomeBoundStatus' value='2' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus2">Needs assist with ambulation.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus3' class='radio' name='{1}_GenericHomeBoundStatus' value='3' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus3">Needs assist leaving the home.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus4' class='radio' name='{1}_GenericHomeBoundStatus' value='4' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus4">Unable to be up for long period.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus5' class='radio' name='{1}_GenericHomeBoundStatus' value='5' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus5">Severe SOB upon exertion.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericHomeBoundStatus6' class='radio' name='{1}_GenericHomeBoundStatus' value='6' type='checkbox' {0} />", genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericHomeBoundStatus6">Unsafe to go out of home alone.</label>
                </div>
            </td>
            <td colspan="2">
                <% string[] genericFunctionalLimitations = data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer != "" ? data["GenericFunctionalLimitations"].Answer.Split(',') : null; %>
                <input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations1' class='radio' name='{1}_GenericFunctionalLimitations' value='1' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations1">ROM/Strength.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations2' class='radio' name='{1}_GenericFunctionalLimitations' value='2' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations2">Pain.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations3' class='radio' name='{1}_GenericFunctionalLimitations' value='3' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations3">Safety Techniques.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations4' class='radio' name='{1}_GenericFunctionalLimitations' value='4' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations4">W/C Mobility.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations5' class='radio' name='{1}_GenericFunctionalLimitations' value='5' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations5">Balance/Gait.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations6' class='radio' name='{1}_GenericFunctionalLimitations' value='6' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations6">Bed Mobility.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations7' class='radio' name='{1}_GenericFunctionalLimitations' value='7' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations7">Transfer.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations8' class='radio' name='{1}_GenericFunctionalLimitations' value='8' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations8">Increased fall risk.</label>
                </div>
                <div>
                    <%= string.Format("<input id='{1}_GenericFunctionalLimitations9' class='radio' name='{1}_GenericFunctionalLimitations' value='9' type='checkbox' {0} />", genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericFunctionalLimitations9">Coordination.</label>
                </div>
            </td>
            <td colspan="2">
                <label for="<%= Model.Type %>_GenericPriorBP" class="float-left">Prior BP</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericPriorBP", data.ContainsKey("GenericPriorBP") ? data["GenericPriorBP"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPriorBP" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPostBP" class="float-left">Post BP</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericPostBP", data.ContainsKey("GenericPostBP") ? data["GenericPostBP"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPostBP" })%> </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPriorPulse" class="float-left">Prior Pulse</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericPriorPulse", data.ContainsKey("GenericPriorPulse") ? data["GenericPriorPulse"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPriorPulse" })%></div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPostPulse" class="float-left">Post Pulse</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericPostPulse", data.ContainsKey("GenericPostPulse") ? data["GenericPostPulse"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericPostPulse" })%> </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Supervisory Visit</th>
            <th colspan="3">Subjective</th>
        </tr>
        <tr>
            <td colspan="3">
                <% string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericSupervisoryVisit" value=" " />
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit1' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='1' type='checkbox' {0} />", genericSupervisoryVisit.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit1" class="fixed radio">Supervisory Visit</label>
                </div>
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit2' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='2' type='checkbox' {0} />", genericSupervisoryVisit.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit2" class="fixed radio">LPTA Present</label>
                </div>
                <div class="float-left strong">
                    <%= string.Format("<input id='{1}_GenericSupervisoryVisit3' class='radio float-left' name='{1}_GenericSupervisoryVisit' value='3' type='checkbox' {0} />", genericSupervisoryVisit.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSupervisoryVisit3" class="fixed radio">Aide Present</label>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericSupervisoryVisitComment" class="strong float-left">Comment</label>
                <div><%= Html.TextArea(Model.Type + "_GenericSupervisoryVisitComment", data.AnswerOrEmptyString("GenericSupervisoryVisitComment"), new { @id = Model.Type + "_GenericSupervisoryVisitComment", @class = "fill" })%></div>
            </td>
            <td colspan="3"><%= Html.TextArea(Model.Type + "_GenericSubjective", data.AnswerOrEmptyString("GenericSubjective"), new { @id = Model.Type + "_GenericSubjective", @class = "fill" })%></td>
        </tr>
        <tr>
            <th colspan="3">Objective</th>
            <th colspan="3">Bed Mobility Training</th>
        </tr>
        <tr class="align-left">
            <td colspan="3"><% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/FormRev2.ascx", Model); %></td>
            <td colspan="3">
                <div class="align-left">
                    <strong>Functional Mobility Key</strong>
                </div>
                <div class="margin">
                    <table class="fixed">
                        <tbody>
                            <tr>
                                <td>I = Independent</td>
                                <td>Min A = 25% Assist</td>
                            </tr>
                            <tr>
                                <td>S = Supervision</td>
                                <td>Mod A = 50% Assist</td>
                            </tr>
                            <tr>
                                <td>VC = Verbal Cue</td>
                                <td>Max A = 75% Assist</td>
                            </tr>
                            <tr>
                                <td>CGA = Contact Guard Assist</td>
                                <td>Total = 100% Assist</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="float-left">
                    <div>
                        <label for="<%= Model.Type %>_GenericRolling" class="float-left">Rolling</label>
                        <div class="fr">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericRolling", data.AnswerOrEmptyString("GenericRolling"), new { @id = Model.Type + "_GenericRolling" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericRollingReps", data.AnswerOrEmptyString("GenericRollingReps"), new { @id = Model.Type + "_GenericRollingReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericSupSit" class="float-left">Sup-Sit</label>
                        <div class="fr">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericSupSit", data.AnswerOrEmptyString("GenericSupSit"), new { @id = Model.Type + "_GenericSupSit" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericSupSitReps", data.AnswerOrEmptyString("GenericSupSitReps"), new { @id = Model.Type + "_GenericSupSitReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericScootingToward" class="float-left">Scooting Toward</label>
                        <div class="fr">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericScootingToward", data.AnswerOrEmptyString("GenericScootingToward"), new { @id = Model.Type + "_GenericScootingToward" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericScootingTowardReps", data.AnswerOrEmptyString("GenericScootingTowardReps"), new { @id = Model.Type + "_GenericScootingTowardReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="<%= Model.Type %>_GenericSitToStand" class="float-left">Sit to Stand</label>
                        <div class="fr">
                            <%= Html.TherapyAssistance(Model.Type + "_GenericSitToStand", data.AnswerOrEmptyString("GenericSitToStand"), new { @id = Model.Type + "_GenericSitToStand" })%>
                            x
                            <%= Html.TextBox(Model.Type + "_GenericSitToStandReps", data.AnswerOrEmptyString("GenericSitToStandReps"), new { @id = Model.Type + "_GenericSitToStandReps", @class = "loc" })%> reps
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Transfer Training</th>
            <th colspan="3">Gait Training</th>
        </tr>
        <tr class="align-left">
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_GenericTransferTraining" class="float-left">Transfer Training</label>
                    <div class="fr">
                        x
                        <%= Html.TextBox(Model.Type + "_GenericTransferTraining", data.AnswerOrEmptyString("GenericTransferTraining"), new { @id = Model.Type + "_GenericTransferTraining", @class = "sn" })%>
                        reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label class="float-left">Assistive Device</label>
                    <div class="fr">
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericBedChairAssist" class="float-left">Bed &#8212; Chair</label>
                    <div class="fr">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericBedChairAssist", data.AnswerOrEmptyString("GenericBedChairAssist"), new { @id = Model.Type + "_GenericBedChairAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChairToiletAssist" class="float-left">Chair &#8212; Toilet</label>
                    <div class="fr">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericChairToiletAssist", data.AnswerOrEmptyString("GenericChairToiletAssist"), new { @id = Model.Type + "_GenericChairToiletAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChairCarAssist" class="float-left">Chair &#8212; Car</label>
                    <div class="fr">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericChairCarAssist", data.AnswerOrEmptyString("GenericChairCarAssist"), new { @id = Model.Type + "_GenericChairCarAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div style="margin-top: 20px;">
                    <label for="" class="float-left">Sitting Balance Activities</label><div class="clear"></div>
                    <div class="fr">
                        <label for="<%= Model.Type %>_GenericSittingStaticAssist">Static</label>
                        <%= Html.StaticBalance(Model.Type + "_GenericSittingStaticAssist", data.AnswerOrEmptyString("GenericSittingStaticAssist"), new { @id = Model.Type + "_GenericSittingStaticAssist" })%>
                        <br />
                        <label for="<%= Model.Type %>_GenericSittingDynamicAssist">Dynamic</label>
                        <%= Html.DynamicBalance(Model.Type + "_GenericSittingDynamicAssist", data.AnswerOrEmptyString("GenericSittingDynamicAssist"), new { @id = Model.Type + "_GenericSittingDynamicAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="" class="float-left">Standing Balance Activities</label>
                    <div class="fr">
                        <label for="<%= Model.Type %>_GenericStandingStaticAssist">Static</label>
                        <%= Html.StaticBalance(Model.Type + "_GenericStandingStaticAssist", data.AnswerOrEmptyString("GenericStandingStaticAssist"), new { @id = Model.Type + "_GenericStandingStaticAssist" })%>
                        <br />
                        <label for="<%= Model.Type %>_GenericStandingDynamicAssist">Dynamic</label>
                        <%= Html.DynamicBalance(Model.Type + "_GenericStandingDynamicAssist", data.AnswerOrEmptyString("GenericStandingDynamicAssist"), new { @id = Model.Type + "_GenericStandingDynamicAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
            </td>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_GenericAmbulationDistance" class="float-left">Ambulation</label>
                    <div class="fr">
                        <span>Distance</span>
                        <%= Html.TextBox(Model.Type + "_GenericAmbulationDistance", data.AnswerOrEmptyString("GenericAmbulationDistance"), new { @id = Model.Type + "_GenericAmbulationDistance", @class = "sn" })%>
                        ft x
                        <%= Html.TextBox(Model.Type + "_GenericAmbulationReps", data.AnswerOrEmptyString("GenericAmbulationReps"), new { @id = Model.Type + "_GenericAmbulationReps", @class = "sn" })%> reps
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label class="float-left">Assistive Device</label>
                    <div class="fr">
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericAmbulationAssist" class="float-left">Assistance</label>
                    <div class="fr">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericAmbulationAssist", data.AnswerOrEmptyString("GenericAmbulationAssist"), new { @id = Model.Type + "_GenericAmbulationAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericGaitQualityComment" class="strong">Gait Quality/Deviation</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericGaitQualityComment", data.AnswerOrEmptyString("GenericGaitQualityComment"), new { @id = Model.Type + "_GenericGaitQualityComment", @class = "fill" })%></div>
                </div>
                <div style="margin-top: 20px;">
                    <label for="<%= Model.Type %>_GenericNumberofSteps" class="float-left">Stairs</label>
                    <div class="fr">
                        <span># of Steps</span>
                        <%= Html.TextBox(Model.Type + "_GenericNumberofSteps", data.AnswerOrEmptyString("GenericNumberofSteps"), new { @id = Model.Type + "_GenericNumberofSteps", @class = "sn" })%>
                        <% string[] genericRails = data.AnswerArray("GenericRails"); %>
                        <input type="hidden" name="<%= Model.Type %>_GenericRails" value="" />
                        <%= string.Format("<input id='{1}_GenericRails1' class='radio' name='{1}_GenericRails' value='1' type='checkbox' {0} />", genericRails.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericRails1" class="fixed radio">Rail 1</label>
                        <%= string.Format("<input id='{1}_GenericRails2' class='radio' name='{1}_GenericRails' value='2' type='checkbox' {0} />", genericRails.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_GenericRails2" class="fixed radio">Rail 2</label>
                    </div>
                    <div class="clear"></div>
                </div>
                 <div>
                    <label class="float-left">Assistive Device</label>
                    <div class="fr">
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericStairsAssist" class="float-left">Assistance</label>
                    <div class="fr">
                        <%= Html.TherapyAssistance(Model.Type + "_GenericStairsAssist", data.AnswerOrEmptyString("GenericStairsAssist"), new { @id = Model.Type + "_GenericStairsAssist" })%>
                    </div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericStairsQualityComment" class="strong">Quality/Deviation</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericStairsQualityComment", data.AnswerOrEmptyString("GenericStairsQualityComment"), new { @id = Model.Type + "_GenericStairsQualityComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Teaching</th>
            <th colspan="3">Pain</th>
        </tr>
        <tr class="align-left">
            <td colspan="3">
                <% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
                <input type="hidden" name="<%= Model.Type %>_GenericTeaching" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching1' class='radio' name='{1}_GenericTeaching' value='1' type='checkbox' {0} />", genericTeaching.Contains("1").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching1">Patient</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching2' class='radio' name='{1}_GenericTeaching' value='2' type='checkbox' {0} />", genericTeaching.Contains("2").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching2">Caregiver</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching3' class='radio' name='{1}_GenericTeaching' value='3' type='checkbox' {0} />", genericTeaching.Contains("3").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching3">HEP</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching4' class='radio' name='{1}_GenericTeaching' value='4' type='checkbox' {0} />", genericTeaching.Contains("4").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching4">Safe Transfer</label>
                            </td>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTeaching5' class='radio' name='{1}_GenericTeaching' value='5' type='checkbox' {0} />", genericTeaching.Contains("5").ToChecked(), Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTeaching5">Safe Gait</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <label for="<%= Model.Type %>_GenericTeachingOther" class="float-left">Other</label>
                    <div class="fr">
                        <%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther", @class = "float-left" })%>
                    </div>
                    <div class="clear"></div>
                </div>
            </td>
            <td colspan="3">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPriorIntensityOfPain" class="float-left">Pain level prior to therapy</label>
                                <div class="fr">
                                    <%  var genericPriorIntensityOfPain = new SelectList(new[] {
                                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                            new SelectListItem { Text = "1", Value = "1" },
                                            new SelectListItem { Text = "2", Value = "2" },
                                            new SelectListItem { Text = "3", Value = "3" },
                                            new SelectListItem { Text = "4", Value = "4" },
                                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                            new SelectListItem { Text = "6", Value = "6" },
                                            new SelectListItem { Text = "7", Value = "7" },
                                            new SelectListItem { Text = "8", Value = "8" },
                                            new SelectListItem { Text = "9", Value = "9" },
                                            new SelectListItem { Text = "10", Value = "10" }
                                        }, "Value", "Text", data.AnswerOrDefault("GenericPriorIntensityOfPain", "0")); %>
                                    <%= Html.DropDownList(Model.Type + "_GenericPriorIntensityOfPain", genericPriorIntensityOfPain, new { @id = Model.Type + "_GenericPriorIntensityOfPain", @class = "oe" })%>
                                </div>                                
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPostIntensityOfPain" class="float-left">Pain level after therapy</label>
                                <div class="fr">
                                    <%  var genericPostIntensityOfPain = new SelectList(new[] {
                                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                            new SelectListItem { Text = "1", Value = "1" },
                                            new SelectListItem { Text = "2", Value = "2" },
                                            new SelectListItem { Text = "3", Value = "3" },
                                            new SelectListItem { Text = "4", Value = "4" },
                                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                            new SelectListItem { Text = "6", Value = "6" },
                                            new SelectListItem { Text = "7", Value = "7" },
                                            new SelectListItem { Text = "8", Value = "8" },
                                            new SelectListItem { Text = "9", Value = "9" },
                                            new SelectListItem { Text = "10", Value = "10" }
                                        }, "Value", "Text", data.AnswerOrDefault("GenericPostIntensityOfPain", "0")); %>
                                    <%= Html.DropDownList(Model.Type + "_GenericPostIntensityOfPain", genericPostIntensityOfPain, new { @id = Model.Type + "_GenericPostIntensityOfPain", @class = "oe" })%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPainLocation" class="float-left">Location</label>
                                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainLocation", data.AnswerOrEmptyString("GenericPainLocation"), new { @id = Model.Type + "_GenericPainLocation" })%></div>
                            </td>
                            <td>
                                <label for="<%= Model.Type %>_GenericPainRelievedBy" class="float-left">Relieved by</label>
                                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainRelievedBy", data.AnswerOrEmptyString("GenericPainRelievedBy"), new { @id = Model.Type + "_GenericPainRelievedBy" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="<%= Model.Type %>_GenericPainProfileComment" class="strong">Other Comment</label>
                <div><%= Html.TextArea(Model.Type + "_GenericPainProfileComment", data.AnswerOrEmptyString("GenericPainProfileComment"), new { @id = Model.Type + "_GenericPainProfileComment", @class = "fill" })%></div>
            </td>
        </tr>
        <tr>
            <th colspan="3">Assessment</th>
            <th colspan="3">Plan</th>
        </tr>
        <tr>
            <td colspan="3"><%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { @id = Model.Type + "_GenericAssessment", @class = "fill" })%></td>
            <td colspan="3">
                <div>
                    <label for="<%= Model.Type %>_GenericContinuePrescribedPlan" class="float-left">Continue Prescribed Plan</label>
                    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericContinuePrescribedPlan", data.AnswerOrEmptyString("GenericContinuePrescribedPlan"), new { @id = Model.Type + "_GenericContinuePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericChangePrescribedPlan" class="float-left">Change Prescribed Plan</label>
                    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericChangePrescribedPlan", data.AnswerOrEmptyString("GenericChangePrescribedPlan"), new { @id = Model.Type + "_GenericChangePrescribedPlan", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="<%= Model.Type %>_GenericPlanDischarge" class="float-left">Plan Discharge</label>
                    <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPlanDischarge", data.AnswerOrEmptyString("GenericPlanDischarge"), new { @id = Model.Type + "_GenericPlanDischarge", @class = "float-left" })%></div>
                    <div class="clear"></div>
                </div>
            </td>
        </tr>
        <tr>
            <th colspan="6">Narrative</th>
        </tr>
        <tr>
            <td colspan="6">
                <div>
                    <%= Html.Templates(Model.Type + "_GenericNarrativeCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericNarrativeComment" })%>
                    <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), 8, 20, new { @id = Model.Type + "_GenericNarrativeComment", @class = "fill" })%>
                </div>
            </td>
         </tr>
    </tbody>
</table>