﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapist Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapist Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EPhysician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3C/span%3E%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
<%  string[] genericHomeBoundStatus = data.AnswerArray("GenericHomeBoundStatus"); %>
    printview.addsection(
        printview.col(4,
            printview.checkbox("Needs assist with transfer.",<%= genericHomeBoundStatus.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Needs assist with gait.",<%= genericHomeBoundStatus.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Needs assist leaving the home.",<%= genericHomeBoundStatus.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Unable to be up for long period.",<%= genericHomeBoundStatus.Contains("4").ToString().ToLower() %>)),
        "Homebound Status");
<%  string[] genericFunctionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
    printview.addsection(
        printview.col(4,
            printview.checkbox("ROM/Strength.",<%= genericFunctionalLimitations.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Pain.",<%= genericFunctionalLimitations.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Safety Techniques.",<%= genericFunctionalLimitations.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("W/C Mobility.",<%= genericFunctionalLimitations.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Balance / Gait.",<%= genericFunctionalLimitations.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility.",<%= genericFunctionalLimitations.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Transfer.",<%= genericFunctionalLimitations.Contains("7").ToString().ToLower() %>)),
        "Functional Limitations");
    printview.addsection(
        printview.col(4,
            printview.span("%3Cstrong%3EPrior BP:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPriorBP").Clean() %>") +
            printview.span("%3Cstrong%3EPost BP:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPostBP").Clean() %>") +
            printview.span("%3Cstrong%3EPrior Pulse:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPriorPulse").Clean() %>") +
            printview.span("%3Cstrong%3EPost Pulse:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPostPulse").Clean() %>")),
        "Vital Signs");
<% string[] genericSupervisoryVisit = data.AnswerArray("GenericSupervisoryVisit"); %>
    printview.addsection(
        printview.col(2,
            printview.checkbox("Supervisory Visit",<%= genericSupervisoryVisit.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("LPTA Present",<%= genericSupervisoryVisit.Contains("2").ToString().ToLower() %>,true) +
            printview.span("Comments:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSupervisoryVisitComment").Clean() %>",0,2)),
        "Supervisory Visit");
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericSubjective").Clean() %>",0,2),
        "Subjective");
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("Rolling",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericRolling").Clean()%> x <%= data.AnswerOrEmptyString("GenericRollingReps").Clean() %>reps", 0, 1) +
            printview.span("Sup-Sit",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSupSit").Clean()%> x <%= data.AnswerOrEmptyString("GenericSupSitReps").Clean() %>reps", 0, 1) +
            printview.span("Scooting Toward",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericScootingToward").Clean()%> x <%= data.AnswerOrEmptyString("GenericScootingTowardReps").Clean() %>reps", 0, 1) +
            printview.span("Sit to Stand",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericSitToStand").Clean()%> x <%= data.AnswerOrEmptyString("GenericSitToStandReps").Clean() %>reps", 0, 1) +
            printview.span("Proper Foot Placement",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericProperFootPlacement").Equals("1").ToString().ToLower()  %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericProperFootPlacement").Equals("0").ToString().ToLower() %>)) +
            printview.span("Proper Unfolding",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericProperUnfolding").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericProperUnfolding").Equals("0").ToString().ToLower() %>))),
        "Bed Mobility Training");
    printview.addsection(
        printview.col(2,
            printview.span("Transfer Training", true) +
            printview.span("x <%= data.AnswerOrEmptyString("GenericTransferTraining").Clean() %> reps",0,1) +
            printview.span("Assistive Device", true) +
            printview.col(4,
                printview.checkbox("Yes", <%= data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No", <%= data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0").ToString().ToLower() %>) +
                printview.span("Specify:") +
                printview.span("<%= data.AnswerOrEmptyString("GenericAssistiveReps").Clean() %> reps"))) +
        printview.col(6,
                printview.span("Bed - Chair", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericBedChairAssist").Clean() %>% Assist") +
                printview.span("Chair - Toilet", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericChairToiletAssist").Clean() %>% Assist") +
                printview.span("Chair - Car", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericChairCarAssist").Clean() %>% Assist")) +
        printview.col(2,
            printview.col(2,
                printview.span("Sitting Balance Activities WB UE", true) +
                printview.col(2,
                    printview.checkbox("Yes", <%= data.AnswerOrEmptyString("GenericSBAWE").Equals("1").ToString().ToLower() %>) +
                    printview.checkbox("No", <%= data.AnswerOrEmptyString("GenericSBAWE").Equals("0").ToString().ToLower() %>))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Static:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSBAWEStaticAssist").Clean() %>% Assist")) +
                printview.col(2,
                    printview.span("Dynamic:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSBAWEDynamicAssist").Clean() %>% Assist"))) +
            printview.span("Standing Balance Activities", true) +
            printview.col(2,
                printview.col(2,
                    printview.span("Static:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSBAStaticAssist").Clean() %>% Assist")) +
                printview.col(2,
                    printview.span("Dynamic:") +
                    printview.span("<%= data.AnswerOrEmptyString("GenericSBADynamicAssist").Clean() %>% Assist")))),
        "Transfer Training");
<%  string[] genericWalkDirection = data.AnswerArray("GenericWalkDirection"); %>
<%  string[] genericTherapyTraning = data.AnswerArray("GenericTherapyTraning"); %>
    printview.addsection(
        printview.col(4,
            printview.span("Gait Training", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitTrainingFt").Clean() %>ft. x <%= data.AnswerOrEmptyString("GenericGaitTrainingFtX").Clean() %>",0,1) +
            printview.checkbox("Walking Sideways.",<%= genericWalkDirection.Contains("1").ToString().ToLower()  %>) +
            printview.checkbox("Walking Backwards.",<%= genericWalkDirection.Contains("2").ToString().ToLower() %>) +
            printview.span("With Device", true) +
            printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericGaitTrainingWithDevice").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericGaitTrainingWithDevice").Equals("0").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitTrainingWithDeviceAssist").Clean() %>% Assist",0,1)) +
        printview.col(2,
            printview.col(2,
                printview.span("Turning", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericTurningDistance").Clean() %> Distance",0,1)) +
            printview.col(3,
                printview.span("Uneven", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericUnevenAssist").Clean() %>% Assist",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericUnevenDevice").Clean() %> Device",0,1)) +
            printview.col(3,
                printview.span("Stairs/Steps", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericStairsStepsAssist").Clean() %>% Assist",0,1) +
                printview.span("<%= data.AnswerOrEmptyString("GenericStairsStepsDevice").Clean() %> Device",0,1)) +
            printview.col(2,
                printview.span("W/C Training", true) +
                printview.span("<%= data.AnswerOrEmptyString("GenericWCTrainingAssist").Clean() %>% Assist",0,1))) +
        printview.col(5,
            printview.checkbox("WB (L)",<%= genericTherapyTraning.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("WB (R)",<%= genericTherapyTraning.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Hip/Knee Extension",<%= genericTherapyTraning.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Stance Phase (L)",<%= genericTherapyTraning.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Stance Phase (R)",<%= genericTherapyTraning.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Heel Strike",<%= genericTherapyTraning.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Step Length",<%= genericTherapyTraning.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Proper Posture",<%= genericTherapyTraning.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Speed",<%= genericTherapyTraning.Contains("9").ToString().ToLower() %>)) +
        printview.col(2,
            printview.span("Other", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericGaitTrainingOther").Clean() %>")),
        "Gait Training");
<% string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
    printview.addsection(
        printview.col(6,
                printview.checkbox("Patient",<%= genericTeaching.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Caregiver",<%= genericTeaching.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("HEP",<%= genericTeaching.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Safe Transfer",<%= genericTeaching.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Safe Gait",<%= genericTeaching.Contains("5").ToString().ToLower() %>) +
                printview.span("Other: <%= data.AnswerOrEmptyString("GenericTeachingOther").Clean() %>",0,1)),
        "Teaching");
    printview.addsection(
        printview.col(4,
            printview.span("%3Cstrong%3EPain level prior to therapy:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPriorIntensityOfPain").Clean() %>",0,1) +
            printview.span("%3Cstrong%3EPain level after therapy:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPostIntensityOfPain").Clean() %>",0,1) +
            printview.span("%3Cstrong%3ELocation:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPainLocation").Clean() %>",0,1) +
            printview.span("%3Cstrong%3ERelieved by:%3C/strong%3E <%= data.AnswerOrEmptyString("GenericPainRelievedBy").Clean() %>",0,1)) +
        printview.span("Other Comment:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPainProfileComment").Clean() %>",0,2),
        "Pain");
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericAssessment").Clean() %>",0,2),
        "Assessment");
    printview.addsection(
        printview.col(6,
            printview.span("Continue Plan:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericContinuePrescribedPlan").Clean() %>",0,1) +
            printview.span("Change Plan:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericChangePrescribedPlan").Clean() %>",0,1) +
            printview.span("Plan Discharge:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPlanDischarge").Clean() %>",0,1)),
        "Plan");
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericNarrativeComment").Clean() %>",0,2),
        "Narrative");
</script>
</body>
</html>
