﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericSpeechTherapyDone = data.AnswerArray("GenericSpeechTherapyDone"); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<fieldset class="half fl">
    <legend>Treatment Diagnosis/Problem</legend>
    <div class="column">
        <div class="row">
            <%= Html.TextArea(Model.Type + "_GenericTreatmentDiagnosis", data.AnswerOrEmptyString("GenericTreatmentDiagnosis"), 3, 20, new { @id = Model.Type + "_GenericTreatmentDiagnosis", @class = "fill" })%>
        </div>
        <div class="row">
            <div class="checkgroup wide">
                <input type="hidden" name="<%= Model.Type %>_GenericSpeechTherapyDone" value="" />
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone1' class='radio' name='{1}_GenericSpeechTherapyDone' value='1' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone1">Speech (C2)</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone2' class='radio' name='{1}_GenericSpeechTherapyDone' value='2' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone2">Lip, tongue, facial, exercises to improve swallowing/vocal skills</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone3' class='radio' name='{1}_GenericSpeechTherapyDone' value='3' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone3">Voice</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone4' class='radio' name='{1}_GenericSpeechTherapyDone' value='4' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone4">Dysphagia Treatments (C4)</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone5' class='radio' name='{1}_GenericSpeechTherapyDone' value='5' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone5">Fluency</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone6' class='radio' name='{1}_GenericSpeechTherapyDone' value='6' type='checkbox' {0} />", genericSpeechTherapyDone.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone6">Language Disorder (C5)</label>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<fieldset class="half fr">
    <legend>Short Term Goals</legend>
    <div class="column">
        <div class="row">
            <%= Html.TextArea(Model.Type + "_GenericShortTermGoals", data.AnswerOrEmptyString("GenericShortTermGoals"), 3, 20, new { @id = Model.Type + "_GenericShortTermGoals", @class = "fill tallest" })%>
        </div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset>
    <legend>Results Of Therapy Session</legend>
    <%= Html.TextArea(Model.Type + "_GenericResultsOfTherapySession", data.AnswerOrEmptyString("GenericResultsOfTherapySession"), 3, 20, new { @id = Model.Type + "_GenericResultsOfTherapySession", @class = "fill tallest" })%>
</fieldset>