﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Functional Status",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPriorFunctionalStatus").Clean() %>",0,1) +
        printview.span("Mental Status",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericMentalStatus").Clean() %>",0,1) +
        printview.span("Medical History",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericMedicalHistory").Clean() %>",0,1) +
        printview.span("Surgical History",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericPastMedicalHistory").Clean() %>",0,1),
        "Status and History");
</script>