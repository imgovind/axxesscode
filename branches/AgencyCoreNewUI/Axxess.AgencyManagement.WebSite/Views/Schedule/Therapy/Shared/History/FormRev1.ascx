﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column wide">
    <div class="row">
        <label for="<%= Model.Type %>_GenericPriorFunctionalStatus" class="strong">Functional Status</label>
        <%= Html.TextArea(Model.Type + "_GenericPriorFunctionalStatus", data.AnswerOrEmptyString("GenericPriorFunctionalStatus"), new { @id = Model.Type + "_GenericPriorFunctionalStatus", @class = "fill" })%>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericMentalStatus" class="strong">Mental Status</label>
        <%= Html.TextArea(Model.Type + "_GenericMentalStatus", data.AnswerOrEmptyString("GenericMentalStatus"), new { @id = Model.Type + "_GenericMentalStatus", @class = "fill" })%>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericMedicalHistory" class="strong">Medical History</label>
        <%= Html.TextArea(Model.Type + "_GenericMedicalHistory", data.AnswerOrEmptyString("GenericMedicalHistory"), new { @id = Model.Type + "_GenericMedicalHistory", @class = "fill" })%>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPastMedicalHistory" class="strong">Surgical History</label>
        <%= Html.TextArea(Model.Type + "_GenericPastMedicalHistory", data.AnswerOrEmptyString("GenericPastMedicalHistory"), new { @id = Model.Type + "_GenericGenericPastMedicalHistory", @class = "fill" })%>
    </div>
</div>
