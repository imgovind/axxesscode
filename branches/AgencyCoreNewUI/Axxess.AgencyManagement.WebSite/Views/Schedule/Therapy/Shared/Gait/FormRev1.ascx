﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <label for="<%= Model.Type %>_GenericGaitLevelAssist" class="float-left">Level</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type+"_GenericGaitLevelAssist", data.AnswerOrEmptyString("GenericGaitLevelAssist"), new { @class = "sn shorter", @id = Model.Type+"_GenericGaitLevelAssist" })%>
            X
            <%= Html.TextBox(Model.Type + "_GenericGaitLevelFeet", data.AnswerOrEmptyString("GenericGaitLevelFeet"), new { @class = "sn shorter", @id = Model.Type + "_GenericGaitLevelFeet" })%>
            Feet
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericGaitUnLevelAssist" class="float-left">Unlevel</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelAssist", data.AnswerOrEmptyString("GenericGaitUnLevelAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericGaitUnLevelAssist" })%>
            X
            <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelFeet", data.AnswerOrEmptyString("GenericGaitUnLevelFeet"), new { @class = "sn shorter", @id = Model.Type + "_GenericGaitUnLevelFeet" })%>
            Feet
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericGaitStepStairAssist" class="float-left">Step/ Stair</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericGaitStepStairAssist", data.AnswerOrEmptyString("GenericGaitStepStairAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericGaitStepStairAssist" })%>
            X
            <%= Html.TextBox(Model.Type + "_GenericGaitStepStairFeet", data.AnswerOrEmptyString("GenericGaitStepStairFeet"), new { @class = "sn shorter", @id = Model.Type + "_GenericGaitStepStairFeet" })%>
            Feet
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericGaitComment" class="strong">Comment</label>
        <%= Html.TextArea(Model.Type + "_GenericGaitComment", data.ContainsKey("GenericGaitComment") ? data["GenericGaitComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitComment", @class = "fill" }) %>
    </div>
</div>
