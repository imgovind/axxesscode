﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column wide">
    <div class="row">
        <table class="fixed">
            <tbody>
                <tr>
                    <th></th>
                    <th colspan="2" class="strong">Assistive Device</th>
                    <th class="strong">% Assist</th>
                </tr>
                <tr>
                    <td class="align-left"><label for="<%= Model.Type %>_GenericTransferBedChairAssistiveDevice" class="strong">Bed-Chair</label></td>
                    <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericTransferBedChairAssistiveDevice", data.AnswerOrEmptyString("GenericTransferBedChairAssistiveDevice"), new { @id = Model.Type+"_GenericTransferBedChairAssistiveDevice" })%></td>
                    <td><%= Html.TextBox(Model.Type+"_GenericTransferBedChairAssist", data.AnswerOrEmptyString("GenericTransferBedChairAssist"), new { @class = "sn shorter", @id = Model.Type+"_GenericTransferBedChairAssist" })%> %</td>
                </tr>
                <tr>
                    <td class="align-left"><label for="<%= Model.Type %>_GenericTransferChairBedAssistiveDevice" class="strong">Chair-Bed</label></td>
                    <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericTransferChairBedAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairBedAssistiveDevice"), new { @class = "", @id = Model.Type+"_GenericTransferChairBedAssistiveDevice" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferChairBedAssist", data.AnswerOrEmptyString("GenericTransferChairBedAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferChairBedAssist" })%> %</td>
                </tr>
                <tr>
                    <td class="align-left"><label for="<%= Model.Type %>_GenericTransferChairToWCAssistiveDevice" class="strong">Chair to W/C</label></td>
                    <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericTransferChairToWCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferChairToWCAssistiveDevice"), new { @class = "", @id = Model.Type+"_GenericTransferChairToWCAssistiveDevice" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferChairToWCAssist", data.AnswerOrEmptyString("GenericTransferChairToWCAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferChairToWCAssist" })%> %</td>
                </tr>
                <tr>
                    <td class="align-left"><label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistiveDevice" class="strong">Toilet or BSC</label></td>
                    <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericTransferToiletOrBSCAssistiveDevice", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssistiveDevice"), new { @class = "", @id = Model.Type+"_GenericTransferToiletOrBSCAssistiveDevice" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferToiletOrBSCAssist", data.AnswerOrEmptyString("GenericTransferToiletOrBSCAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferToiletOrBSCAssist" })%> %</td>
                </tr>
                <tr>
                    <td class="align-left"><label for="<%= Model.Type %>_GenericTransferCanVanAssistiveDevice" class="strong">Car/Van</label></td>
                    <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericTransferCanVanAssistiveDevice", data.AnswerOrEmptyString("GenericTransferCanVanAssistiveDevice"), new { @class = "", @id = Model.Type+"_GenericTransferCanVanAssistiveDevice" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferCanVanAssist", data.AnswerOrEmptyString("GenericTransferCanVanAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferCanVanAssist" })%> %</td>
                </tr>
                <tr>
                    <td class="align-left"><label for="<%= Model.Type %>_GenericTransferTubShowerAssistiveDevice" class="strong">Tub/Shower</label></td>
                    <td colspan="2"><%= Html.TextBox(Model.Type+"_GenericTransferTubShowerAssistiveDevice", data.AnswerOrEmptyString("GenericTransferTubShowerAssistiveDevice"), new { @class = "", @id = Model.Type+"_GenericTransferTubShowerAssistiveDevice" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferTubShowerAssist", data.AnswerOrEmptyString("GenericTransferTubShowerAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferTubShowerAssist" })%> %</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
        <table class="fixed">
            <tbody> 
                <tr>
                    <th></th>
                    <th class="strong">Static</th>
                    <th class="strong">Dynamic</th>
                </tr>
                <tr>
                    <td><label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic" class="strong">Sitting Balance</label></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferSittingBalanceStatic", data.AnswerOrEmptyString("GenericTransferSittingBalanceStatic"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferSittingBalanceStatic" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferSittingBalanceDynamic", data.AnswerOrEmptyString("GenericTransferSittingBalanceDynamic"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferSittingBalanceDynamic" })%> %</td>
                </tr>
                <tr>
                    <td><label for="<%= Model.Type %>_GenericTransferStandBalanceStatic" class="strong">Stand Balance</label></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferStandBalanceStatic", data.AnswerOrEmptyString("GenericTransferStandBalanceStatic"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferStandBalanceStatic" })%></td>
                    <td><%= Html.TextBox(Model.Type + "_GenericTransferStandBalanceDynamic", data.AnswerOrEmptyString("GenericTransferStandBalanceDynamic"), new { @class = "sn shorter", @id = Model.Type + "_GenericTransferStandBalanceDynamic" })%> %</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>