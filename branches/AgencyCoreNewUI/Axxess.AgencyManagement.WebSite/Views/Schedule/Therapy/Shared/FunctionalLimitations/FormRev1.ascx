﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] functionalLimitations = data.AnswerArray("GenericFunctionalLimitations"); %>
<div class="column">
    <div class="row">
        <div class="checkgroup narrow">
            <input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations1' name='{1}_GenericFunctionalLimitations' value='1' type='checkbox' {0} />", functionalLimitations.Contains("1").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations1">Transfer</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations2' name='{1}_GenericFunctionalLimitations' value='2' type='checkbox' {0} />", functionalLimitations.Contains("2").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations2">Gait</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations3' name='{1}_GenericFunctionalLimitations' value='3' type='checkbox' {0} />", functionalLimitations.Contains("3").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations3">Strength</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations4' name='{1}_GenericFunctionalLimitations' value='4' type='checkbox' {0} />", functionalLimitations.Contains("4").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations4">Bed Mobility</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations5' name='{1}_GenericFunctionalLimitations' value='5' type='checkbox' {0} />", functionalLimitations.Contains("5").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations5">Safety Techniques</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations6' name='{1}_GenericFunctionalLimitations' value='6' type='checkbox' {0} />", functionalLimitations.Contains("6").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations6">ADL&#8217;s</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations7' name='{1}_GenericFunctionalLimitations' value='7' type='checkbox' {0} />", functionalLimitations.Contains("7").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations7">ROM</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio' id='{1}_GenericFunctionalLimitations8' name='{1}_GenericFunctionalLimitations' value='8' type='checkbox' {0} />", functionalLimitations.Contains("8").ToChecked(),Model.Type)%>
                <label for="<%= Model.Type %>_GenericFunctionalLimitations8">W/C Mobility</label>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericFunctionalLimitationsOther" class="float-left">Other</label>
        <%= Html.TextBox(Model.Type + "_GenericFunctionalLimitationsOther", data.AnswerOrEmptyString("GenericFunctionalLimitationsOther"), new { @id = Model.Type + "_GenericFunctionalLimitationsOther", @class = "fill" })%>
    </div>
</div>