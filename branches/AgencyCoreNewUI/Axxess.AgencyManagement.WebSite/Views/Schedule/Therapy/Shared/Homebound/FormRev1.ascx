﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] homeboundStatusAssist = data.AnswerArray("GenericHomeboundStatusAssist"); %>
<input type="hidden" name="<%= Model.Type %>_GenericHomeboundStatusAssist" value="" />
<div class="column">
    <div class="row">
        <div class="checkgroup wide">
            <div class="option">
                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist1' name='{1}_GenericHomeboundStatusAssist' value='1' type='checkbox' {0} />", homeboundStatusAssist.Contains("1").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist1" class="radio">Gait</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist2' name='{1}_GenericHomeboundStatusAssist' value='2' type='checkbox' {0} />", homeboundStatusAssist.Contains("2").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist2" class="radio">Leaving the Home</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist3' name='{1}_GenericHomeboundStatusAssist' value='3' type='checkbox' {0} />", homeboundStatusAssist.Contains("3").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist3" class="radio">Transfers</label>
            </div>
            <div class="option">
                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist4' name='{1}_GenericHomeboundStatusAssist' value='4' type='checkbox' {0} />", homeboundStatusAssist.Contains("4").ToChecked(), Model.Type) %>
                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist4" class="radio">SOB/Endurance</label>
            </div>
        </div>
    </div>
</div>