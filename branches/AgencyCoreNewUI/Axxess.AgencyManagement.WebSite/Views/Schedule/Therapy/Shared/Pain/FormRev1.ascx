﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column" style="width:100%;">
    <div class="row">
        <label for="<%= Model.Type %>_GenericPainLocation" class="fl strong">Pain Location</label>
        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainAssessmentLocation", data.AnswerOrEmptyString("GenericPainAssessmentLocation"), new { @id = Model.Type + "_GenericPainAssessmentLocation" }) %></div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPainLevel" class="fl strong">Pain Level</label>
        <div class="fr">
            <%  var genericPainLevel = new SelectList(new[] {
                    new SelectListItem { Text = "0 = No Pain", Value = "0" },
                    new SelectListItem { Text = "1", Value = "1" },
                    new SelectListItem { Text = "2", Value = "2" },
                    new SelectListItem { Text = "3", Value = "3" },
                    new SelectListItem { Text = "4", Value = "4" },
                    new SelectListItem { Text = "Moderate Pain", Value = "5" },
                    new SelectListItem { Text = "6", Value = "6" },
                    new SelectListItem { Text = "7", Value = "7" },
                    new SelectListItem { Text = "8", Value = "8" },
                    new SelectListItem { Text = "9", Value = "9" },
                    new SelectListItem { Text = "10", Value = "10" }
                }, "Value", "Text", data.AnswerOrDefault("GenericPainLevel", "0"));%>
            <%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type + "_GenericPainLevel", @class = "oe" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPainAssessmentIncreasedBy" class="fl strong">Increased by</label>
        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainAssessmentIncreasedBy", data.AnswerOrEmptyString("GenericPainAssessmentIncreasedBy"), new { @id = Model.Type + "_GenericPainAssessmentIncreasedBy" })%></div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPainAssessmentRelievedBy" class="fl strong">Relieved by</label>
        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPainAssessmentRelievedBy", data.AnswerOrEmptyString("GenericPainAssessmentRelievedBy"), new { @id = Model.Type + "_GenericPainAssessmentRelievedBy" })%></div>
    </div>
</div>
