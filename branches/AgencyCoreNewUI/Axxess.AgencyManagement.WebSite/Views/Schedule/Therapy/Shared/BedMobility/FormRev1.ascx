﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <label for="<%= Model.Type %>_GenericBedMobilityRollingAssistiveDevice" class="strong">Rolling</label>
        <div class="margin">
            <div class="row">
                <label class="strong">Assistive Device</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice"), new { @id = Model.Type+"_GenericBedMobilityRollingAssistiveDevice" })%></div>
            </div>
            <div class="row">
                <label class="strong">Assist</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssist", data.AnswerOrEmptyString("GenericBedMobilityRollingAssist"), new { @class = "sn shorter", @id = Model.Type+"_GenericBedMobilityRollingAssist" })%> %</div>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistiveDevice" class="strong">Sit Stand Sit</label>
        <div class="margin">
            <div class="row">
                <label class="strong">Assistive Device</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericBedMobilitySitStandSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssistiveDevice"), new { @id = Model.Type+"_GenericBedMobilitySitStandSitAssistiveDevice" })%></div>
            </div>
            <div class="row">
                <label class="strong">Assist</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericBedMobilitySitStandSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySitStandSitAssist"), new { @class = "sn shorter", @id = Model.Type+"_GenericBedMobilitySitStandSitAssist" })%> %</div>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistiveDevice" class="strong">Sup to Sit</label>
        <div class="margin">
            <div class="row">
                <label class="strong">Assistive Device</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericBedMobilitySupToSitAssistiveDevice", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssistiveDevice"), new { @id = Model.Type+"_GenericBedMobilitySupToSitAssistiveDevice" })%></div>
            </div>
            <div class="row">
                <label class="strong">Assist</label>
                <div class="fr"><%= Html.TextBox(Model.Type+"_GenericBedMobilitySupToSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySupToSitAssist"), new { @class = "sn shorter", @id = Model.Type+"_GenericBedMobilitySupToSitAssist" })%> %</div>
            </div>
        </div>
    </div>
</div>
