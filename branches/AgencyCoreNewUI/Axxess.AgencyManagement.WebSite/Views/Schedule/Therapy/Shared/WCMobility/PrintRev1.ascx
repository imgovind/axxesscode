﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<script type="text/javascript">
    printview.addsubsection(
        printview.col(3,
            printview.span("Level",true) +
            printview.span("Ramp",true) +
            printview.span("Maneuver",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWCMobilityLevel").Clean() %>%",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWCMobilityRamp").Clean() %>%",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericWCMobilityManeuver").Clean() %>%",0,1)),
        "W/C Mobility");
</script>