﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column" style="width:100%;">
    <div class="row">
        <label for="<%= Model.Type %>_GenericWCMobilityLevel" class="strong">Level</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type+"_GenericWCMobilityLevel", data.AnswerOrEmptyString("GenericWCMobilityLevel"), new { @class = "sn", @id = Model.Type+"_GenericWCMobilityLevel" })%> %
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericWCMobilityRamp" class="strong">Ramp</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type+"_GenericWCMobilityRamp", data.AnswerOrEmptyString("GenericWCMobilityRamp"), new { @class = "sn", @id = Model.Type+"_GenericWCMobilityRamp" })%> %
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericWCMobilityManeuver" class="strong">Maneuver</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type+"_GenericWCMobilityManeuver", data.AnswerOrEmptyString("GenericWCMobilityManeuver"), new { @class = "sn", @id = Model.Type+"_GenericWCMobilityManeuver" })%> %
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericWCMobilityADL" class="strong">ADL</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericWCMobilityADL", data.AnswerOrEmptyString("GenericWCMobilityADL"), new { @class = "sn", @id = Model.Type + "_GenericWCMobilityADL" })%> %
        </div>
    </div>
</div>