﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericAssessment = data.AnswerArray("GenericAssessment"); %>
<fieldset class="half fl">
    <legend>Homebound</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/FormRev1.ascx", Model); %>
</fieldset>

<fieldset class="half fr">
    <legend>Functional Limitations</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/FormRev1.ascx", Model); %>
</fieldset>

<div class="clear"></div>

<fieldset class="half fl">
    <legend>Pain Assessment</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/FormRev1.ascx", Model); %>
</fieldset>

<fieldset class="half fr">
    <legend>Objective</legend>
    <% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Objective/FormRev1.ascx", Model); %>
</fieldset>

<div class="clear"></div>

<fieldset class="half fl">
    <legend>ADL Training</legend>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLBathing" class="float-left">Bathing</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLBathing", data.AnswerOrEmptyString("GenericADLBathing"), new { @class = "sn short", @id = Model.Type + "_GenericADLBathing" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLUEDressing" class="float-left">UE Dressing</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.AnswerOrEmptyString("GenericADLUEDressing"), new { @class = "sn short", @id = Model.Type + "_GenericADLUEDressing" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLLEDressing" class="float-left">LE Dressing</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.AnswerOrEmptyString("GenericADLLEDressing"), new { @class = "sn short", @id = Model.Type + "_GenericADLLEDressing" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLGrooming" class="float-left">Grooming</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.AnswerOrEmptyString("GenericADLGrooming"), new { @class = "sn short", @id = Model.Type + "_GenericADLGrooming" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLToileting" class="float-left">Toileting</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLToileting", data.AnswerOrEmptyString("GenericADLToileting"), new { @class = "sn short", @id = Model.Type + "_GenericADLToileting" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLFeeding" class="float-left">Feeding</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.AnswerOrEmptyString("GenericADLFeeding"), new { @class = "sn short", @id = Model.Type + "_GenericADLFeeding" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLMealPrep" class="float-left">Meal Prep</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.AnswerOrEmptyString("GenericADLMealPrep"), new { @class = "sn short", @id = Model.Type + "_GenericADLMealPrep" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLHouseCleaning" class="float-left">House Cleaning</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.AnswerOrEmptyString("GenericADLHouseCleaning"), new { @class = "sn short", @id = Model.Type + "_GenericADLHouseCleaning" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLHouseSafety" class="float-left">House Safety</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLHouseSafety", data.AnswerOrEmptyString("GenericADLHouseSafety"), new { @class = "sn short", @id = Model.Type + "_GenericADLHouseSafety" })%></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="float-left">Adaptive Equipment/Assistive Device Use or Needs</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.AnswerOrEmptyString("GenericADLAdaptiveEquipment"), new { @class = "sn short", @id = Model.Type + "_GenericADLAdaptiveEquipment" })%></div>
        </div>
    </div>
</fieldset>

<fieldset class="half fr">
    <legend>Neuromuscular Reeducation</legend>
    <div class="column">
        <%  string[] genericUEWeightBearing = data.AnswerArray("GenericUEWeightBearing"); %>
        <div class="row">
            <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="float-left">Sitting Balance Activities</label>
            <div class="fr align-right">
                <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">Static</label>
                <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "sn short", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">% assist</label><br />
                <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">Dynamic</label>
                <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "sn short", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">% assist</label>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="float-left">Standing Balance Activities</label>
            <div class="fr align-right">
                <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">Static</label>
                <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesStaticAssist"), new { @class = "sn short", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">% assist</label><br />
                <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">Dynamic</label>
                <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.AnswerOrEmptyString("GenericSittingBalanceActivitiesDynamicAssist"), new { @class = "sn short", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">% assist</label>
            </div>
        </div>
        <div class="row checkgroup wide">
            <div class="option">
                <input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
                <%= string.Format("<input id='{1}_GenericUEWeightBearing1' class='radio' name='{1}_GenericUEWeightBearing' value='1' type='checkbox' {0} />", genericUEWeightBearing.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericUEWeightBearing1">UE Weight-Bearing Activities</label>
            </div>
        </div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset class="half fr">
<legend>Therapeutic Exercise</legend>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericTherapeuticExerciseROM" class="float-left">ROM</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROM", data.AnswerOrEmptyString("GenericTherapeuticExerciseROM"), new { @id = Model.Type + "_GenericTherapeuticExerciseROM", @class = "short"})%>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseROMSet"), new { @class = "sn shorter", @id = Model.Type + "_GenericTherapeuticExerciseROMSet" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseROMReps"), new { @class = "sn shortest", @id = Model.Type + "_GenericTherapeuticExerciseROMReps" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMReps">reps</label>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROM" class="float-left">AROM/AAROM</label></td>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROM", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROM"), new { @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROM", @class = "short" })%>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMSet"), new { @class = "sn shorter", @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseAROMAAROMReps"), new { @class = "sn shortest", @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROMReps">reps</label>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericTherapeuticExerciseResistive" class="float-left">Resistive (Type)</label>
            <div class="fr"> 
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistive", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistive"), new { @id = Model.Type + "_GenericTherapeuticExerciseResistive", @class = "short" })%>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveSet"), new { @class = "sn shorter", @id = Model.Type + "_GenericTherapeuticExerciseResistiveSet" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseResistiveReps"), new { @class = "sn shortest", @id = Model.Type + "_GenericTherapeuticExerciseResistiveReps" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseResistiveReps">reps</label>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericTherapeuticExerciseStretching" class="float-left">Stretching</label>
            <div class="fr"> 
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretching", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretching"), new { @id = Model.Type + "_GenericTherapeuticExerciseStretching", @class = "short" })%>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingSet", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingSet"), new { @class = "sn shorter", @id = Model.Type + "_GenericTherapeuticExerciseStretchingSet" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingReps", data.AnswerOrEmptyString("GenericTherapeuticExerciseStretchingReps"), new { @class = "sn shortest", @id = Model.Type + "_GenericTherapeuticExerciseStretchingReps" })%>
                <label for="<%= Model.Type %>_GenericTherapeuticExerciseStretchingReps">reps</label>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericTherapeuticExerciseOther" class="float-left">Other</label></td>
            <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseOther", data.AnswerOrEmptyString("GenericTherapeuticExerciseOther"), new { @id = Model.Type + "_GenericTherapeuticExerciseOther", @class = "fr" })%></td>
        </div>
    </div>
</fieldset>

<fieldset class="half fl">
    <legend>Teaching</legend>
    <div class="column">
        <%  string[] genericTeaching = data.AnswerArray("GenericTeaching"); %>
        <input type="hidden" name=Model.Type + "_GenericTeaching" value="" />
        <div class="row">
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment1' class='float-left radio' name='{1}_GenericAssessment' value='1' type='checkbox' {0} />", genericTeaching.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment1">Patient/Family</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment2' class='float-left radio' name={1}_GenericAssessment' value='2' type='checkbox' {0} />", genericTeaching.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment2">Caregiver</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment3' class='float-left radio' name='{1}_GenericAssessment' value='3' type='checkbox' {0} />", genericTeaching.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment3">Correct Use of Adaptive Equipment</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment4' class='float-left radio' name='{1}_GenericAssessment' value='4' type='checkbox' {0} />", genericTeaching.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment4">Safety Technique</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment5' class='float-left radio' name='{1}_GenericAssessment' value='5' type='checkbox' {0} />", genericTeaching.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment5">ADLs</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment6' class='float-left radio' name='{1}_GenericAssessment' value='6' type='checkbox' {0} />", genericTeaching.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment6">HEP</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericAssessment7' class='float-left radio' name='{1}_GenericAssessment' value='7' type='checkbox' {0} />", genericTeaching.Contains("7").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericAssessment7">Correct Use of Assistive Device</label>
                </div>
            </div>
        </div>
        <div  class="row">
            <label for="<%= Model.Type %>_GenericTeachingOther" class="float-left">Other: (modalities, DME/AE need, consults, etc)</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.AnswerOrEmptyString("GenericTeachingOther"), new { @id = Model.Type + "_GenericTeachingOther" })%></div>
        </div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset class="half fr">
    <legend>Therapeutic/Dynamic Activities</legend>
    <div class="column">
        <div class="row">
            <label class="float-left strong">Bed Mobility</label>
            <div class="clear"></div>
            <div class="margin">
                <label for="<%= Model.Type %>_GenericBedMobilityRollingReps" class="float-left">Rolling</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_GenericBedMobilityRollingReps">X</label>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingReps", data.AnswerOrEmptyString("GenericBedMobilityRollingReps"), new { @class = "sn short", @id = Model.Type + "_GenericBedMobilityRollingReps" })%>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssist", data.AnswerOrEmptyString("GenericBedMobilityRollingAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%>
                    <label for="<%= Model.Type %>_GenericBedMobilityRollingAssist">% assist</label>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps" class="float-left">Supine to Sit</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps">X</label>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitReps", data.AnswerOrEmptyString("GenericBedMobilitySupineToSitReps"), new { @class = "sn short", @id = Model.Type + "_GenericBedMobilitySupineToSitReps" })%>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitAssist", data.AnswerOrEmptyString("GenericBedMobilitySupineToSitAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericBedMobilitySupineToSitAssist" })%>
                    <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitAssist">% assist</label>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps" class="float-left">Dynamic Reaching</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps">X</label>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingReps", data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingReps"), new { @class = "sn short", @id = Model.Type + "_GenericBedMobilityDynamicReachingReps" })%>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingAssist", data.AnswerOrEmptyString("GenericBedMobilityDynamicReachingAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericBedMobilityDynamicReachingAssist" })%>
                    <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingAssist">% assist</label>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericBedMobilityCoordReps" class="float-left">Gross/Fine Motor Coord</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_GenericBedMobilityCoordReps">X</label>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilityCoordReps", data.AnswerOrEmptyString("GenericBedMobilityCoordReps"), new { @class = "sn short", @id = Model.Type + "_GenericBedMobilityCoordReps" })%>
                    <%= Html.TextBox(Model.Type + "_GenericBedMobilityCoordAssist", data.AnswerOrEmptyString("GenericBedMobilityCoordAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericBedMobilityCoordAssist" })%>
                    <label for="<%= Model.Type %>_GenericBedMobilityCoordAssist">% assist</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericTransfersType" class="float-left strong">Transfers</label>
            <div class="fr">
                <label for="<%= Model.Type %>_GenericTransfersType">Type</label>
                <%= Html.TextBox(Model.Type + "_GenericTransfersType", data.AnswerOrEmptyString("GenericTransfersType"), new { @id = Model.Type + "_GenericTransfersType", @class = "short" })%>
                <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordReps">X</label>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps", data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordReps"), new { @class = "sn short", @id = Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps" })%>
                <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist", data.AnswerOrEmptyString("GenericBedMobilityGrossFineMotorCoordAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist" })%>
                <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordAssist">% assist</label>
            </div>
            <div class="clear"></div>
            <div class="margin">
                <label class="float-left">Correct Unfolding</label>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "1", data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("1"), new { @id = Model.Type + "_GenericCorrectUnfolding1" })%>
                    <label for="<%= Model.Type %>_GenericCorrectUnfolding1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "0", data.AnswerOrEmptyString("GenericCorrectUnfolding").Equals("0"), new { @id = Model.Type + "_GenericCorrectUnfolding0" })%>
                    <label for="<%= Model.Type %>_GenericCorrectUnfolding0" class="fixed short">No</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Correct Foot Placement</label> 
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "1", data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("1"), new { @id = Model.Type + "_GenericCorrectFootPlacement1" })%>
                    <label for="<%= Model.Type %>_GenericCorrectFootPlacement1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "0", data.AnswerOrEmptyString("GenericCorrectFootPlacement").Equals("0"), new { @id = Model.Type + "_GenericCorrectFootPlacement0" })%>
                    <label for="<%= Model.Type %>_GenericCorrectFootPlacement0" class="fixed short">No</label>
                </div>
                <div class="clear"></div>
                <label class="float-left">Assistive Device</label>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "1", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("1"), new { @id = Model.Type + "_GenericAssistiveDevice1" })%>
                    <label for="<%= Model.Type %>_GenericAssistiveDevice1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "0", data.AnswerOrEmptyString("GenericAssistiveDevice").Equals("0"), new { @id = Model.Type + "_GenericAssistiveDevice0" })%>
                    <label for="<%= Model.Type %>_GenericAssistiveDevice0" class="fixed short">No</label>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<fieldset class="half fl">
    <legend>Assessment</legend>
    <div class="column">
        <div class="row">
            <%= Html.TextArea(Model.Type + "_GenericAssessment", data.AnswerOrEmptyString("GenericAssessment"), new { @id = Model.Type + "_GenericAssessment", @class = "fill tallest" })%>
        </div>
    </div>
</fieldset>

<div class="clear"></div>

<fieldset class="half fl">
    <legend>W/C Training</legend>
    <div class="column">
        <%  string[] genericPropulsionWith = data.AnswerArray("GenericPropulsionWith"); %>
        <div class="row">
            <label class="float-left">Propulsion with</label>
            <input type="hidden" name="<%= Model.Type %>_GenericPropulsionWith" value="" />
            <div class="clear"></div>
            <div class="checkgroup narrow">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPropulsionWith1' class='float-left radio' name='{1}_GenericPropulsionWith' value='1' type='checkbox' {0} />", genericPropulsionWith.Contains("1").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPropulsionWith1">RUE</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPropulsionWith2' class='float-left radio' name='{1}_GenericPropulsionWith' value='2' type='checkbox' {0} />", genericPropulsionWith.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPropulsionWith2">LUE</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPropulsionWith3' class='float-left radio' name='{1}_GenericPropulsionWith' value='3' type='checkbox' {0} />", genericPropulsionWith.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPropulsionWith3">BUE</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPropulsionWith4' class='float-left radio' name='{1}_GenericPropulsionWith' value='4' type='checkbox' {0} />", genericPropulsionWith.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPropulsionWith4">RLE</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPropulsionWith5' class='float-left radio' name='{1}_GenericPropulsionWith' value='5' type='checkbox' {0} />", genericPropulsionWith.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPropulsionWith5">LLE</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPropulsionWith6' class='float-left radio' name='{1}_GenericPropulsionWith' value='6' type='checkbox' {0} />", genericPropulsionWith.Contains("6").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPropulsionWith6">BLE</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericDistanceFT" class="float-left">Distance</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericDistanceFT", data.AnswerOrEmptyString("GenericDistanceFT"), new { @class = "sn short", @id = Model.Type + "_GenericDistanceFT" })%>
                <label for="<%= Model.Type %>_GenericDistanceFT">ft</label>
                <label for="<%= Model.Type %>_GenericDistanceFTReps">x</label>
                <%= Html.TextBox(Model.Type + "_GenericDistanceFTReps", data.AnswerOrEmptyString("GenericDistanceFTReps"), new { @class = "sn shorter", @id = Model.Type + "_GenericDistanceFTReps" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericManagement" class="float-left">Management</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericManagement", data.AnswerOrEmptyString("GenericManagement"), new { @id = Model.Type + "_GenericManagement", @class = "short" })%>
                <%= Html.TextBox(Model.Type + "_GenericManagementAssist", data.AnswerOrEmptyString("GenericManagementAssist"), new { @class = "sn shorter", @id = Model.Type + "_GenericManagementAssist" })%>
                <label for="<%= Model.Type %>_GenericManagementAssist">% assist</label>
            </div>
        </div>
    </div>
</fieldset>

<fieldset class="half fr">
    <legend>Plan</legend>
    <div class="column">
        <%  string[] genericPlan = data.AnswerArray("GenericPlan"); %>
        <input type="hidden" name=Model.Type + "_GenericPlan" value="" />
        <div class="row">
            <div class="checkgroup wide">
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlan1' class='float-left radio' name='{1}_GenericPlan' value='1' type='checkbox' {0} />", genericPlan.Contains("1").ToChecked(),Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlan1">Continue Prescribed Plan</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlan6' class='float-left radio' name='{1}_GenericPlan' value='6' type='checkbox' {0} />", genericPlan.Contains("6").ToChecked(),Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlan6">Change Prescribed Plan</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericPlanChangePrescribed", data.AnswerOrEmptyString("GenericPlanChangePrescribed"), new { @id = Model.Type + "_GenericPlanChangePrescribed" })%></div>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlan2' class='float-left radio' name='{1}_GenericPlan' value='2' type='checkbox' {0} />", genericPlan.Contains("2").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlan2">Plan Discharge</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlan3' class='float-left radio' name='{1}_GenericPlan' value='3' type='checkbox' {0} />", genericPlan.Contains("3").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlan3">In Progress</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlan4' class='float-left radio' name='{1}_GenericPlan' value='4' type='checkbox' {0} />", genericPlan.Contains("4").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlan4">As of Today</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{1}_GenericPlan5' class='float-left radio' name='{1}_GenericPlan' value='5' type='checkbox' {0} />", genericPlan.Contains("5").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericPlan5">Patient/Family Notified</label>
                    <span class="more float-left">
                        <%= Html.TextBox(Model.Type + "_GenericPlanPtDaysNotice", data.AnswerOrDefault("GenericPlanPtDaysNotice", "5"), new { @id = Model.Type + "_GenericPlanPtDaysNotice", @class = "small-digit" })%>
                        <label for="<%= Model.Type %>_GenericPlan5">Days Prior to Discharge</label>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="float-left">
                <strong>Agency Notification</strong>
                <%= Html.TextBox(Model.Type + "_GenericPlanAgencyDaysNotice", data.AnswerOrDefault("GenericPlanAgencyDaysNotice", "5"), new { @id = Model.Type + "_GenericPlanAgencyDaysNotice", @class = "small-digit" })%>
                <strong>Days Prior?</strong>
            </div>
            <div class="fr">
                <%= Html.RadioButton(Model.Type + "_GenericPlanIsAgencyNotification", "1", data.AnswerOrEmptyString("GenericPlanIsAgencyNotification").Equals("1"), new { @id = Model.Type + "_GenericPlanIsAgencyNotification1" })%>
                <label for="<%= Model.Type %>_GenericPlanIsAgencyNotification1" class="fixed short">Yes</label>
                <%= Html.RadioButton(Model.Type + "_GenericPlanIsAgencyNotification", "0", data.AnswerOrEmptyString("GenericPlanIsAgencyNotification").Equals("0"), new { @id = Model.Type + "_GenericPlanIsAgencyNotification0" })%>
                <label for="<%= Model.Type %>_GenericPlanIsAgencyNotification0" class="fixed short">No</label>
            </div>
        </div>
    </div>
</fieldset>
<div class="clear"></div>