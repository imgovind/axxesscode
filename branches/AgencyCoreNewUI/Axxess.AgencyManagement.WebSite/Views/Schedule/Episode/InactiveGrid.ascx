﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EpisodeLean>>" %>
<ul>
    <li class="ac"><h3>Inactive Episode(s)</h3></li>
    <li>
        <span class="episode-range ac">Episode Range</span>
        <span class="episode-action ac">Action</span>
    </li>
</ul>
<ol>
<%  if (Model != null && Model.Count > 0) { %>
    <%  foreach (var episode in Model) { %>
    <li>
        <span class="episode-range ac"><%=episode.Range%></span>
        <span class="episode-action ac">
            <a class="link" onclick="Schedule.Episode.Activate('<%= episode.Id %>', '<%= episode.PatientId %>');return false">Activate</a>
            |
            <a class="link" onclick="Schedule.Episode.Edit('<%= episode.Id %>','<%= episode.PatientId %>');return false">Edit</a>
        </span>
    </li>
    <%  } %>
<%  } else { %>
    <li class="ac strong">There is no inactive episode for this patient.</li>
<%  } %>
</ol>