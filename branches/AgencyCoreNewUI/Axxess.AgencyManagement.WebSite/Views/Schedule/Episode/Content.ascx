﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<%  if (Model.AdmissionDates == null) Model.AdmissionDates = new List<SelectListItem>(); %>
<fieldset>
   <legend>Details</legend>
   <div class="column">
        <div class="row">
            <label for="NewEpisode_AdmissionId" class="fl strong">Start of Care Date</label>
            <div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @id = "NewEpisode_AdmissionId" }) %></div>
        </div>
        <div class="row">
            <label for="NewEpisode_TargetDate" class="fl strong">Episode Start Date</label>
            <div class="fr">
                <input type="text" class="date-picker required" name="StartDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(1).ToShortDateString() : DateTime.Today.ToShortDateString() %>" id="NewEpisode_StartDate" /><br />
<%  if (Model != null && Model.EndDate != DateTime.MinValue) { %>
                <em>Last Episode End Date: <%= Model.EndDateFormatted %></em>
<%  } %>
            </div>
        </div>
        <div class="row">
            <label for="NewEpisode_EndDate" class="fl strong">Episode End Date</label>
            <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(60).ToShortDateString() : DateTime.Today.AddDays(59).ToShortDateString() %>" id="NewEpisode_EndDate" /></div>
        </div>
   </div>
   <div class="column">
        <div class="row">
            <label for="NewEpisode_CaseManager" class="fl strong">Case Manager</label>
            <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), new { @id = "NewEpisode_CaseManager", @class = "required" })%></div>
        </div>
        <div class="row">
            <label for="NewEpisode_PrimaryPhysician" class="fl strong">Primary Physician</label>
            <div class="fr"><%= Html.TextBox("Detail.PrimaryPhysician", Model.PrimaryPhysician, new { @id = "NewEpisode_PrimaryPhysician", @class = "physician-picker" })%></div>
        </div>
        <div class="row">
            <label for="NewEpisode_PrimaryInsurance" class="fl strong">Primary Insurance</label>
            <div class="fr"><%= Html.Insurances("Detail.PrimaryInsurance", Model.PrimaryInsurance.ToString(), false, new { @id = "NewEpisode_PrimaryInsurance", @class = "required notzero" })%></div>
        </div>
        <div class="row">
            <label for="NewEpisode_SecondaryInsurance" class="fl strong">Secondary Insurance</label>
            <div class="fr"><%= Html.Insurances("Detail.SecondaryInsurance", Model.SecondaryInsurance.ToString(), false, new { @id = "NewEpisode_SecondaryInsurance" })%></div>
        </div>
   </div>
</fieldset>
<fieldset>
    <legend>Comments <span class="img icon note-blue"></span><em>(Blue Sticky Note)</em></legend>
    <div class="wide column">
        <div class="row">
            <textarea id="NewEpisode_Comments" name="Detail.Comments" maxcharacters="500" class="tall"></textarea>
        </div>
    </div>
</fieldset>