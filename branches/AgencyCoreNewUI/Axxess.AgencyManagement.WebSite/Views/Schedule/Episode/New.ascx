﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<span class="wintitle">New Episode | <%= Current.AgencyName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Episode/Create", "Schedule", FormMethod.Post, new { @id = "NewEpisode_Form" })) { %>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
    <%  if (Model.DisplayName.IsNullOrEmpty()) { %>
                <label for="NewEpisode_PatientId" class="fl strong">Patient</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", Guid.Empty.ToString(), new { @id = "NewEpisode_PatientId", @class = "required notzero" })%></div>
    <%  } else { %>
                <label class="fl strong">Patient</label>
                <div class="fr">
                    <%= Model.DisplayName %>
                    <%= Html.Hidden("PatientId", Model.PatientId) %>
                </div>
    <%  } %>
            </div>
        </div>
    </fieldset>
    <div id="NewEpisode_Content"><%  Html.RenderPartial("Episode/Content", Model); %></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>