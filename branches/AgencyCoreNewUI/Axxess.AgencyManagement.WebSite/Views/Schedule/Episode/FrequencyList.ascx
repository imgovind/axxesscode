﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FrequenciesViewData>" %>
<div class="wrapper main">
    <div class="acore-grid">
        <ul>
            <li class="ac"><h3>Episode Frequency List</h3></li>
            <li>
                <span class="grid-third ac">Visit Type</span>
                <span class="grid-third ac">Frequency</span>
                <span class="grid-third ac">Visit Count</span>
            </li>
        </ul>
        <ol>
<%  if (Model != null && Model.Visits.Count > 0) { %>
    <%  foreach (var pair in Model.Visits) { %>
            <li>
                <span class="grid-third ac"><%= pair.Key%></span>
                <span class="grid-third ac"><%= pair.Value.Frequency%> </span>
                <span class="grid-third ac"><%= pair.Value.Count%> </span>
            </li>
    <%  } %>
<%  } else { %>
            <li class="ac strong">There is no frequencies set for this episode.</li>
<%  } %>
        </ol>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>