﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EpisodeLean>>" %>
<div class="wrapper main">
    <div id="InactiveEpisodesList_Content" class="content acore-grid"><% Html.RenderPartial("Episode/InactiveGrid", Model); %></div>
</div>
