﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Episode/Update", "Schedule", FormMethod.Post, new { @id = "EditEpisode_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Patient</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.DisplayName %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row">
                <label for="EditEpisode_AdmissionId" class="fl strong">Start of Care Date</label>
                <div class="fr"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @id = "EditEpisode_AdmissionId" }) %></div>
            </div>
            <div class="row">
                <label for="EditEpisode_TargetDate" class="fl strong">Episode Start Date</label>
                <div class="fr">
                    <input type="text" class="date-picker required" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" id="EditEpisode_StartDate" /><br />
<%  if (Model.PreviousEpisode != null) { %>
                    <em>Last Episode End Date: <%= Model.PreviousEpisode.EndDate.ToShortDateString()%></em>
<%  } %>
                </div>
            </div>
            <div class="row">
                <label for="EditEpisode_EndDate" class="fl strong">Episode End Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="EndDate" value="<%= Model.EndDate.ToShortDateString() %>" id="EditEpisode_EndDate" /></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsActive",!Model.IsActive, new { @id = "EditEpisode_IsActive" })%>
                        <label for="EditEpisode_IsActive">Inactivate Episode</label>
                    </div>
                </div>
            </div>
       </div>
       <div class="column">
            <div class="row">
                <label for="EditEpisode_CaseManager" class="fl strong">Case Manager</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.Detail.CaseManager, new { @id = "EditEpisode_CaseManager", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditEpisode_PrimaryPhysician" class="fl strong">Primary Physician</label>
                <div class="fr"><%= Html.TextBox("Detail.PrimaryPhysician", Model.Detail.PrimaryPhysician, new { @id = "EditEpisode_PrimaryPhysician", @class = "physician-picker" })%></div>
            </div>
            <div class="row">
                <label for="EditEpisode_PrimaryInsurance" class="fl strong">Primary Insurance</label>
                <div class="fr"><%= Html.Insurances("Detail.PrimaryInsurance", Model.Detail.PrimaryInsurance, false, new { @id = "EditEpisode_PrimaryInsurance", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="EditEpisode_SecondaryInsurance" class="fl strong">Secondary Insurance</label>
                <div class="fr"><%= Html.Insurances("Detail.SecondaryInsurance", Model.Detail.SecondaryInsurance, false, new { @id = "EditEpisode_SecondaryInsurance" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments <span class="img icon note-blue"></span><em>(Blue Sticky Note)</em></legend>
        <div class="wide column">
            <div class="row ac">
                <textarea id="EditEpisode_Comments" name="Detail.Comments"  maxcharacters="500" class="tall"><%= Model.Detail.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="activity-log fr"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>