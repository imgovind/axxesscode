﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">HHA Supervisory Visit | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer : ""; %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "55")%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label for="" class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="fl strong">Visit Date</label>
                <div class="fr"><input type="text" id ="Text1" class="date-picker required" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString()%>" mindate="<%= Model.StartDate.ToShortDateString()%>"/></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_HealthAide" class="fl strong">Health Aide</label>
                <div class="fr"><%= Html.HHAides("HealthAide", data.AnswerOrEmptyString("HealthAide"), new { @id = Model.Type + "_HealthAide" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_AssociatedMileage" class="fl strong">Associated Mileage</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : string.Empty, new { @id = Model.Type + "_AssociatedMileage", @class = "" })%></div>
            </div>
            <div class="row">
                <label class="fl strong">Aide Present</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_AidePresent", "1", data.AnswerOrEmptyString("AidePresent").Equals("1"), new { @id = Model.Type + "_AidePresentY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_AidePresentY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_AidePresent", "0", data.AnswerOrEmptyString("AidePresent").Equals("0"), new { @id = Model.Type + "_AidePresentN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_AidePresentN">No</label>
                </div>
            </div>
        <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
            <div class="row">
                <div class="fr button-with-arrow"><%= Model.CarePlanOrEvalUrl %></div>
            </div>
        <%  } %> 
        </div>
    </fieldset>
    <fieldset>
        <legend>Evaluation</legend>
        <div class="column wide">
            <div class="row narrow">
                <div class="fl strong">1. Arrives for assigned visits as scheduled</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_ArriveOnTime", "1", data.AnswerOrEmptyString("ArriveOnTime").Equals("1"), new { @id = Model.Type + "_ArriveOnTimeY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_ArriveOnTimeY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_ArriveOnTime", "0", data.AnswerOrEmptyString("ArriveOnTime").Equals("0"), new { @id = Model.Type + "_ArriveOnTimeN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_ArriveOnTimeN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">2. Follows client&#8217;s plan of care</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_FollowPOC", "1", data.AnswerOrEmptyString("FollowPOC").Equals("1"), new { @id = Model.Type + "_FollowPOCY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_FollowPOCY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_FollowPOC", "0", data.AnswerOrEmptyString("FollowPOC").Equals("0"), new { @id = Model.Type + "_FollowPOCN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_FollowPOCN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">3. Demonstrates positive and helpful attitude towards the client and others</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_HasPositiveAttitude", "1", data.AnswerOrEmptyString("HasPositiveAttitude").Equals("1"), new { @id = Model.Type + "_HasPositiveAttitudeY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_HasPositiveAttitudeY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_HasPositiveAttitude", "0", data.AnswerOrEmptyString("HasPositiveAttitude").Equals("0"), new { @id = Model.Type + "_HasPositiveAttitudeN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_HasPositiveAttitudeN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">4. Informs Nurse Supervisor of client needs and changes in condition as appropriate</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_InformChanges", "1", data.AnswerOrEmptyString("InformChanges").Equals("1"), new { @id = Model.Type + "_InformChangesY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_InformChangesY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_InformChanges", "0", data.AnswerOrEmptyString("InformChanges").Equals("0"), new { @id = Model.Type + "_InformChangesN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_InformChangesN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">5. Aide Implements Universal Precautions per agency policy</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_IsUniversalPrecautions", "1", data.AnswerOrEmptyString("IsUniversalPrecautions").Equals("1"), new { @id = Model.Type + "_IsUniversalPrecautionsY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_IsUniversalPrecautionsY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_IsUniversalPrecautions", "0", data.AnswerOrEmptyString("IsUniversalPrecautions").Equals("0"), new { @id = Model.Type + "_IsUniversalPrecautionsN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_IsUniversalPrecautionsN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">6. Any changes made to client plan of care at this time</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_POCChanges", "1", data.AnswerOrEmptyString("POCChanges").Equals("1"), new { @id = Model.Type + "_POCChangesY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_POCChangesY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_POCChanges", "0", data.AnswerOrEmptyString("POCChanges").Equals("0"), new { @id = Model.Type + "_POCChangesN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_POCChangesN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">7. Patient/CG satisfied with care and services provided by aide</div>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_IsServicesSatisfactory", "1", data.AnswerOrEmptyString("IsServicesSatisfactory").Equals("1"), new { @id = Model.Type + "_IsServicesSatisfactoryY" })%>
                    <label class="fixed short" for="<%= Model.Type %>_IsServicesSatisfactoryY">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_IsServicesSatisfactory", "0", data.AnswerOrEmptyString("IsServicesSatisfactory").Equals("0"), new { @id = Model.Type + "_IsServicesSatisfactoryN" })%>
                    <label class="fixed short" for="<%= Model.Type %>_IsServicesSatisfactoryN">No</label>
                </div>
            </div>
            <div class="row narrow">
                <div class="fl strong">8. Additional Comments/Findings</div>
                <div class="clr"></div>
                <div class="ac"><%= Html.TextArea(Model.Type + "_AdditionalComments", data.AnswerOrEmptyString("AdditionalComments"), new { @class = "tall" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= date %>" id="<%= Model.Type %>_SignatureDate" /></div>
            </div>
        </div>
        <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
        <div class="column wide">
            <div class="row narrowest">
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
            </div>
        </div>
        <%  } %>
    </fieldset>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>