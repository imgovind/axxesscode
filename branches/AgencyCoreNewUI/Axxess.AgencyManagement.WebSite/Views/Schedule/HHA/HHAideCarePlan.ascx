﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Home Health Aide Care Plan | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer : ""; %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHACarePlan_Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId) %>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId) %>
    <%= Html.Hidden("DisciplineTask", "75") %>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label for="" class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="fl strong">Visit Date</label>
                <div class="fr"><input type="text" id ="<%= Model.Type %>_VisitDate" class="date-picker required" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString()%>" mindate="<%= Model.StartDate.ToShortDateString()%>"/></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeIn" class="fl strong">Time In</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = Model.Type + "_TimeIn", @class = "complete-required time-picker" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeOut" class="fl strong">Time Out</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = Model.Type + "_TimeOut", @class = "complete-required time-picker" })%></div>
            </div>
             <div class="row">
                <label for="<%= Model.Type %>_HHAFrequency" class="fl strong">Frequency</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_HHAFrequency", data.AnswerOrEmptyString("HHAFrequency"), new { @id = Model.Type + "_HHAFrequency", @readonly = "readonly" })%></div>
            </div>
            <div class="row checkgroup wide">
                <div class="option">
                    <%  string[] diet = data.AnswerArray("IsDiet"); %>
                    <%= string.Format("<input class='radio' id='{0}_IsDiet' name='{0}_IsDiet' value='1' type='checkbox' {1} />", Model.Type, diet.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_IsDiet" class="strong">Diet</label>
                    <input name="<%= Model.Type %>_IsDiet" value="" type="hidden" />
                    <div class="more">
                        <%= Html.TextBox(Model.Type + "_Diet", data.AnswerOrEmptyString("Diet"), new { @id = Model.Type + "_Diet" })%>
                    </div>
                </div>
                <div class="option">
                    <%  string[] allergies = data.AnswerArray("Allergies"); %>
                    <%= string.Format("<input class='radio' id='{0}_Allergies' name='{0}_Allergies' value='Yes' type='checkbox' {1} />", Model.Type, allergies.Contains("Yes").ToChecked()) %>
                    <label for="<%= Model.Type %>_Allergies" class="strong">Allergies</label>
                    <input name="<%= Model.Type %>_Allergies" value="" type="hidden" />
                    <div class="more">
                        <%= Html.TextBox(Model.Type + "_AllergiesDescription", data.AnswerOrEmptyString("AllergiesDescription"), new { @id = Model.Type + "_AllergiesDescription" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
            <div class="row">
                <label for="<%= Model.Type %>_PreviousNotes" class="fl strong">Previous Care Plans</label>
                <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
            </div>
    <% } %>
            <div class="row">
                <label for="<%= Model.Type %>_AssociatedMileage" class="fl strong">Associated Mileage</label>
               <div class="fr"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : string.Empty, new { @id = Model.Type + "_AssociatedMileage", @class = "" })%></div>
            </div>
             <div class="row">
                <label for="<%= Model.Type %>_Surcharge" class="fl strong">Surcharge</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Surcharge", data.ContainsKey("Surcharge") ? data["Surcharge"].Answer : string.Empty, new { @id = Model.Type + "_Surcharge", @class = "" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_DNR" class="fl strong">DNR</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_DNR", "1", data.AnswerOrEmptyString("DNR").Equals("1"), new { @id = Model.Type + "_DNR1" })%>
                    <label for="<%= Model.Type %>_DNR1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_DNR", "0", data.AnswerOrEmptyString("DNR").Equals("0"), new { @id = Model.Type + "_DNR2" })%>
                    <label for="<%= Model.Type %>_DNR2" class="fixed short">No</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl strong">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl strong">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>
            <%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
            <input name="<%= Model.Type %>_IsVitalSignParameter" value="" type="hidden" />
            Vital Sign Parameters
            &#8212;
            <%= string.Format("<input class='radio' id='" + Model.Type + "_IsVitalSignParameter' name='" + Model.Type + "_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1"))%>
            <label for="<%= Model.Type %>_IsVitalSignParameter">N/A</label>
        </legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_SystolicBPGreaterThan" class="fl strong">Systolic Blood Pressure</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_SystolicBPGreaterThan" class="strong">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.Type + "_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_SystolicBPGreaterThan", @class = "shorter" })%>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_SystolicBPLessThan" class="strong">less than (&#60;)</label>
                    <%= Html.TextBox(Model.Type + "_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = Model.Type + "_SystolicBPLessThan", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_DiastolicBPGreaterThan" class="fl strong">Diastolic Blood Pressure</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_DiastolicBPGreaterThan" class="strong">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.Type + "_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_DiastolicBPGreaterThan", @class = "shorter" })%>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_DiastolicBPLessThan" class="strong">less than (&#60;)</label>
                    <%= Html.TextBox(Model.Type + "_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = Model.Type + "_DiastolicBPLessThan", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PulseGreaterThan" class="fl strong">Pulse/Heart Rate</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_PulseGreaterThan" class="strong">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.Type + "_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_PulseGreaterThan", @class = "shorter" })%>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_PulseLessThan" class="strong">less than (&#60;)</label>
                    <%= Html.TextBox(Model.Type + "_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = Model.Type + "_PulseLessThan", @class = "shorter" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_RespirationGreaterThan" class="fl strong">Respiration</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_RespirationGreaterThan" class="strong">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.Type + "_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_RespirationGreaterThan", @class = "shorter" })%>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_RespirationLessThan" class="strong">less than (&#60;)</label>
                    <%= Html.TextBox(Model.Type + "_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = Model.Type + "_RespirationLessThan", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TempGreaterThan" class="fl strong">Temperature</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_TempGreaterThan" class="strong">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.Type + "_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_TempGreaterThan", @class = "shorter" })%>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_TempLessThan" class="strong">less than (&#60;)</label>
                    <%= Html.TextBox(Model.Type + "_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = Model.Type + "_TempLessThan", @class = "shorter" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_WeightGreaterThan" class="fl strong">Weight</label>
                <div class="fr">
                    <label for="<%= Model.Type %>_WeightGreaterThan" class="strong">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.Type + "_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = Model.Type + "_WeightGreaterThan", @class = "shorter" })%>
                </div>
                <div class="clr"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_WeightLessThan" class="strong">less than (&#60;)</label>
                    <%= Html.TextBox(Model.Type + "_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = Model.Type + "_WeightLessThan", @class = "shorter" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Safety Precautions</legend>
        <%  string[] safetyMeasure = data.AnswerArray("SafetyMeasures"); %>
        <input type="hidden" name="<%= Model.Type %>_SafetyMeasures" value="" />
        <div class="column wide">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures1' name='{0}_SafetyMeasures' value='1' {1} />", Model.Type, safetyMeasure.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures1">Anticoagulant Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures2' name='{0}_SafetyMeasures' value='2' {1} />", Model.Type, safetyMeasure.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures2">Emergency Plan Developed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures3' name='{0}_SafetyMeasures' value='3' {1} />", Model.Type, safetyMeasure.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures3">Fall Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures4' name='{0}_SafetyMeasures' value='4' {1} />", Model.Type, safetyMeasure.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures4">Keep Pathway Clear</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures5' name='{0}_SafetyMeasures' value='5' {1} />", Model.Type, safetyMeasure.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures5">Keep Side Rails Up</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures6' name='{0}_SafetyMeasures' value='6' {1} />", Model.Type, safetyMeasure.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures6">Neutropenic Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures8' name='{0}_SafetyMeasures' value='8' {1} />", Model.Type, safetyMeasure.Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures8">Proper Position During Meals</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures9' name='{0}_SafetyMeasures' value='9' {1} />", Model.Type, safetyMeasure.Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures9">Safety in ADLs</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures10' name='{0}_SafetyMeasures' value='10' {1} />", Model.Type, safetyMeasure.Contains("10").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures10">Seizure Precautions</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures11' name='{0}_SafetyMeasures' value='11' {1} />", Model.Type, safetyMeasure.Contains("11").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures11">Sharps Safety</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures12' name='{0}_SafetyMeasures' value='12' {1} />", Model.Type, safetyMeasure.Contains("12").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures12">Slow Position Change</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures13' name='{0}_SafetyMeasures' value='13' {1} />", Model.Type, safetyMeasure.Contains("13").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures13">Standard Precautions/ Infection Control</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures14' name='{0}_SafetyMeasures' value='14' {1} />", Model.Type, safetyMeasure.Contains("14").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures14">Support During Transfer and Ambulation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures15' name='{0}_SafetyMeasures' value='15' {1} />", Model.Type, safetyMeasure.Contains("15").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures15">Use of Assistive Devices</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input class='radio float-left' type='checkbox' id='{0}_SafetyMeasures7' name='{0}_SafetyMeasures' value='7' {1} />", Model.Type, safetyMeasure.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_SafetyMeasures7">O<sub>2</sub> Precautions</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Other</label>
                <div class="ac"><%= Html.TextArea(Model.Type + "_OtherSafetyMeasures", data.AnswerOrEmptyString("OtherSafetyMeasures"), new { @id = Model.Type + "_OtherSafetyMeasures", @class = "tall" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Functional Limitations</legend>
        <%  string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
        <div class="column wide">
            <div class="row">
                <div class="checkgroup">
                    <input name="<%= Model.Type %>_FunctionLimitations" value=" " type="hidden" />
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations1' name='{0}_FunctionLimitations' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("1").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations1">Amputation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations2' name='{0}_FunctionLimitations' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("2").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations2">Bowel/Bladder Incontinence</label>
                    </div>
                    <div class="option">
                        <label for="<%= Model.Type %>_FunctionLimitations3">Contracture</label>
                        <%= string.Format("<input id='{0}_FunctionLimitations3' name='{0}_FunctionLimitations' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("3").ToChecked() ) %>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations4' name='{0}_FunctionLimitations' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("4").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations4">Hearing</label>
                    </div>
                    <div class="option">
                        <label for="<%= Model.Type %>_FunctionLimitations5">Paralysis</label>
                        <%= string.Format("<input id='{0}_FunctionLimitations5' name='{0}_FunctionLimitations' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("5").ToChecked() ) %>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations6' name='{0}_FunctionLimitations' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("6").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations6">Endurance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations7' name='{0}_FunctionLimitations' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("7").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations7">Ambulation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations8' name='{0}_FunctionLimitations' value='8' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("8").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations8">Speech</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitations9' name='{0}_FunctionLimitations' value='9' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("9").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitations9">Legally Blind</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_FunctionLimitationsA' name='{0}_FunctionLimitations' value='A' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("A").ToChecked() ) %>
                        <label for="<%= Model.Type %>_FunctionLimitationsA">Dyspnea with Minimal Exertion</label>
                    </div>
                    <div class="option">
                        <label for="<%= Model.Type %>_FunctionLimitationsB">Other</label>
                        <%= string.Format("<input id='{0}_FunctionLimitationsB' name='{0}_FunctionLimitations' value='B' class='radio float-left' type='checkbox' {1} />", Model.Type, functionLimitations.Contains("B").ToChecked() ) %>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_FunctionLimitationsOther", data.AnswerOrEmptyString("FunctionLimitationsOther"), new { @id = Model.Type + "_FunctionLimitationsOther" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Activities Permitted</legend>
        <%  string[] activitiesPermitted = data.AnswerArray("ActivitiesPermitted"); %>
        <input type="hidden" name="<%= Model.Type %>_ActivitiesPermitted" value="" />
        <div class="column wide">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted1' name='{0}_ActivitiesPermitted' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted1">Complete bed rest</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted2' name='{0}_ActivitiesPermitted' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted2">Bed rest with BRP</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted3' name='{0}_ActivitiesPermitted' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted3">Up as tolerated</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted4' name='{0}_ActivitiesPermitted' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted4">Transfer bed-chair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted5' name='{0}_ActivitiesPermitted' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted5">Exercise prescribed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted6' name='{0}_ActivitiesPermitted' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted6">Partial weight bearing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted7' name='{0}_ActivitiesPermitted' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted7">Independent at home</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted8' name='{0}_ActivitiesPermitted' value='8' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted8">Crutches</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted9' name='{0}_ActivitiesPermitted' value='9' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("9").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted9">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted10' name='{0}_ActivitiesPermitted' value='10' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("10").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted10">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted11' name='{0}_ActivitiesPermitted' value='11' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("11").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted11">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ActivitiesPermitted12' name='{0}_ActivitiesPermitted' value='12' class='radio float-left' type='checkbox' {1} />", Model.Type, activitiesPermitted.Contains("12").ToChecked())%>
                        <label for="<%= Model.Type %>_ActivitiesPermitted12">Other</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_ActivitiesPermittedOther", data.AnswerOrEmptyString("ActivitiesPermittedOther"), new { @id = Model.Type + "_ActivitiesPermittedOther" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Plan Details</legend>
        <div class="column wide">
            <div class="row ac">
                <em>* QV = Every Visit; QW = Every Week; PR = Patient Request; N/A = Not Applicable</em>
            </div>
        </div>
        <div class="column">
            <div class="row ac strong">Vital Signs</div>
            <div class="row">
                <label class="fl strong">Temperature</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_VitalSignsTemperature", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "3", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("3"), new { @id = Model.Type + "_VitalSignsTemperature3" })%>
                    <label for="<%= Model.Type %>_VitalSignsTemperature3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "2", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("2"), new { @id = Model.Type + "_VitalSignsTemperature2" })%>
                    <label for="<%= Model.Type %>_VitalSignsTemperature2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "1", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("1"), new { @id = Model.Type + "_VitalSignsTemperature1" })%>
                    <label for="<%= Model.Type %>_VitalSignsTemperature1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsTemperature", "0", data.AnswerOrEmptyString("VitalSignsTemperature").Equals("0"), new { @id = Model.Type + "_VitalSignsTemperature0" })%>
                    <label for="<%= Model.Type %>_VitalSignsTemperature0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Blood Pressure</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_VitalSignsWeight", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "3", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("3"), new { @id = Model.Type + "_VitalSignsBloodPressure3" })%>
                    <label for="<%= Model.Type %>_VitalSignsBloodPressure3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "2", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("2"), new { @id = Model.Type + "_VitalSignsBloodPressure2" })%>
                    <label for="<%= Model.Type %>_VitalSignsBloodPressure2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "1", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("1"), new { @id = Model.Type + "_VitalSignsBloodPressure1" })%>
                    <label for="<%= Model.Type %>_VitalSignsBloodPressure1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsBloodPressure", "0", data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("0"), new { @id = Model.Type + "_VitalSignsBloodPressure0" })%>
                    <label for="<%= Model.Type %>_VitalSignsBloodPressure0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Heart Rate</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_VitalSignsHeartRate", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "3", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("3"), new { @id = Model.Type + "_VitalSignsHeartRate3" })%>
                    <label for="<%= Model.Type %>_VitalSignsHeartRate3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "2", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("2"), new { @id = Model.Type + "_VitalSignsHeartRate2" })%>
                    <label for="<%= Model.Type %>_VitalSignsHeartRate2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "1", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("1"), new { @id = Model.Type + "_VitalSignsHeartRate1" })%>
                    <label for="<%= Model.Type %>_VitalSignsHeartRate1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsHeartRate", "0", data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("0"), new { @id = Model.Type + "_VitalSignsHeartRate0" })%>
                    <label for="<%= Model.Type %>_VitalSignsHeartRate0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Respirations</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_VitalSignsRespirations", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "3", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("3"), new { @id = Model.Type + "_VitalSignsRespirations3" })%>
                    <label for="<%= Model.Type %>_VitalSignsRespirations3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "2", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("2"), new { @id = Model.Type + "_VitalSignsRespirations2" })%>
                    <label for="<%= Model.Type %>_VitalSignsRespirations2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "1", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("1"), new { @id = Model.Type + "_VitalSignsRespirations1" })%>
                    <label for="<%= Model.Type %>_VitalSignsRespirations1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsRespirations", "0", data.AnswerOrEmptyString("VitalSignsRespirations").Equals("0"), new { @id = Model.Type + "_VitalSignsRespirations0" })%>
                    <label for="<%= Model.Type %>_VitalSignsRespirations0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Weight</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_VitalSignsWeight", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "3", data.AnswerOrEmptyString("VitalSignsWeight").Equals("3"), new { @id = Model.Type + "_VitalSignsWeight3" })%>
                    <label for="<%= Model.Type %>_VitalSignsWeight3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "2", data.AnswerOrEmptyString("VitalSignsWeight").Equals("2"), new { @id = Model.Type + "_VitalSignsWeight2" })%>
                    <label for="<%= Model.Type %>_VitalSignsWeight2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "1", data.AnswerOrEmptyString("VitalSignsWeight").Equals("1"), new { @id = Model.Type + "_VitalSignsWeight1" })%>
                    <label for="<%= Model.Type %>_VitalSignsWeight1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_VitalSignsWeight", "0", data.AnswerOrEmptyString("VitalSignsWeight").Equals("0"), new { @id = Model.Type + "_VitalSignsWeight0" })%>
                    <label for="<%= Model.Type %>_VitalSignsWeight0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row ac strong">Personal Care</div>
            <div class="row">
                <label class="fl strong">Bed Bath</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareBedBath", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "3", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("3"), new { @id = Model.Type + "_PersonalCareBedBath3" })%>
                    <label for="<%= Model.Type %>_PersonalCareBedBath3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "2", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("2"), new { @id = Model.Type + "_PersonalCareBedBath2" })%>
                    <label for="<%= Model.Type %>_PersonalCareBedBath2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "1", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("1"), new { @id = Model.Type + "_PersonalCareBedBath1" })%>
                    <label for="<%= Model.Type %>_PersonalCareBedBath1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareBedBath", "0", data.AnswerOrEmptyString("PersonalCareBedBath").Equals("0"), new { @id = Model.Type + "_PersonalCareBedBath0" })%>
                    <label for="<%= Model.Type %>_PersonalCareBedBath0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Assist with Chair Bath</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareAssistWithChairBath", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "3", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("3"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath3" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "2", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath2" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "1", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath1" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithChairBath", "0", data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithChairBath0" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithChairBath0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Tub Bath</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareTubBath", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "3", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("3"), new { @id = Model.Type + "_PersonalCareTubBath3" })%>
                    <label for="<%= Model.Type %>_PersonalCareTubBath3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "2", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("2"), new { @id = Model.Type + "_PersonalCareTubBath2" })%>
                    <label for="<%= Model.Type %>_PersonalCareTubBath2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "1", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("1"), new { @id = Model.Type + "_PersonalCareTubBath1" })%>
                    <label for="<%= Model.Type %>_PersonalCareTubBath1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareTubBath", "0", data.AnswerOrEmptyString("PersonalCareTubBath").Equals("0"), new { @id = Model.Type + "_PersonalCareTubBath0" })%>
                    <label for="<%= Model.Type %>_PersonalCareTubBath0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Shower</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareShower", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "3", data.AnswerOrEmptyString("PersonalCareShower").Equals("3"), new { @id = Model.Type + "_PersonalCareShower3" })%>
                    <label for="<%= Model.Type %>_PersonalCareShower3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "2", data.AnswerOrEmptyString("PersonalCareShower").Equals("2"), new { @id = Model.Type + "_PersonalCareShower2" })%>
                    <label for="<%= Model.Type %>_PersonalCareShower2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "1", data.AnswerOrEmptyString("PersonalCareShower").Equals("1"), new { @id = Model.Type + "_PersonalCareShower1" })%>
                    <label for="<%= Model.Type %>_PersonalCareShower1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShower", "0", data.AnswerOrEmptyString("PersonalCareShower").Equals("0"), new { @id = Model.Type + "_PersonalCareShower0" })%>
                    <label for="<%= Model.Type %>_PersonalCareShower0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Shower w/Chair</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareShowerWithChair", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "3", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("3"), new { @id = Model.Type + "_PersonalCareShowerWithChair3" })%>
                    <label for="<%= Model.Type %>_PersonalCareShowerWithChair3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "2", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("2"), new { @id = Model.Type + "_PersonalCareShowerWithChair2" })%>
                    <label for="<%= Model.Type %>_PersonalCareShowerWithChair2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "1", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("1"), new { @id = Model.Type + "_PersonalCareShowerWithChair1" })%>
                    <label for="<%= Model.Type %>_PersonalCareShowerWithChair1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShowerWithChair", "0", data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("0"), new { @id = Model.Type + "_PersonalCareShowerWithChair0" })%>
                    <label for="<%= Model.Type %>_PersonalCareShowerWithChair0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Shampoo Hair</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareShampooHair", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "3", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("3"), new { @id = Model.Type + "_PersonalCareShampooHair3" })%>
                    <label for="<%= Model.Type %>_PersonalCareShampooHair3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "2", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("2"), new { @id = Model.Type + "_PersonalCareShampooHair2" })%>
                    <label for="<%= Model.Type %>_PersonalCareShampooHair2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "1", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("1"), new { @id = Model.Type + "_PersonalCareShampooHair1" })%>
                    <label for="<%= Model.Type %>_PersonalCareShampooHair1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShampooHair", "0", data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("0"), new { @id = Model.Type + "_PersonalCareShampooHair0" })%>
                    <label for="<%= Model.Type %>_PersonalCareShampooHair0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Hair Care/Comb Hair</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareHairCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "3", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("3"), new { @id = Model.Type + "_PersonalCareHairCare3" })%>
                    <label for="<%= Model.Type %>_PersonalCareHairCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "2", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("2"), new { @id = Model.Type + "_PersonalCareHairCare2" })%>
                    <label for="<%= Model.Type %>_PersonalCareHairCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "1", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("1"), new { @id = Model.Type + "_PersonalCareHairCare1" })%>
                    <label for="<%= Model.Type %>_PersonalCareHairCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareHairCare", "0", data.AnswerOrEmptyString("PersonalCareHairCare").Equals("0"), new { @id = Model.Type + "_PersonalCareHairCare0" })%>
                    <label for="<%= Model.Type %>_PersonalCareHairCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Oral Care</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareOralCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "3", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("3"), new { @id = Model.Type + "_PersonalCareOralCare3" })%>
                    <label for="<%= Model.Type %>_PersonalCareOralCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "2", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("2"), new { @id = Model.Type + "_PersonalCareOralCare2" })%>
                    <label for="<%= Model.Type %>_PersonalCareOralCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "1", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("1"), new { @id = Model.Type + "_PersonalCareOralCare1" })%>
                    <label for="<%= Model.Type %>_PersonalCareOralCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareOralCare", "0", data.AnswerOrEmptyString("PersonalCareOralCare").Equals("0"), new { @id = Model.Type + "_PersonalCareOralCare0" })%>
                    <label for="<%= Model.Type %>_PersonalCareOralCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Skin Care</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareSkinCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "3", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("3"), new { @id = Model.Type + "_PersonalCareSkinCare3" })%>
                    <label for="<%= Model.Type %>_PersonalCareSkinCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "2", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("2"), new { @id = Model.Type + "_PersonalCareSkinCare2" })%>
                    <label for="<%= Model.Type %>_PersonalCareSkinCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "1", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("1"), new { @id = Model.Type + "_PersonalCareSkinCare1" })%>
                    <label for="<%= Model.Type %>_PersonalCareSkinCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareSkinCare", "0", data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("0"), new { @id = Model.Type + "_PersonalCareSkinCare0" })%>
                    <label for="<%= Model.Type %>_PersonalCareSkinCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Pericare</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCarePericare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "3", data.AnswerOrEmptyString("PersonalCarePericare").Equals("3"), new { @id = Model.Type + "_PersonalCarePericare3" })%>
                    <label for="<%= Model.Type %>_PersonalCarePericare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "2", data.AnswerOrEmptyString("PersonalCarePericare").Equals("2"), new { @id = Model.Type + "_PersonalCarePericare2" })%>
                    <label for="<%= Model.Type %>_PersonalCarePericare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "1", data.AnswerOrEmptyString("PersonalCarePericare").Equals("1"), new { @id = Model.Type + "_PersonalCarePericare1" })%>
                    <label for="<%= Model.Type %>_PersonalCarePericare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCarePericare", "0", data.AnswerOrEmptyString("PersonalCarePericare").Equals("0"), new { @id = Model.Type + "_PersonalCarePericare0" })%>
                    <label for="<%= Model.Type %>_PersonalCarePericare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Nail Care</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareNailCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "3", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("3"), new { @id = Model.Type + "_PersonalCareNailCare3" })%>
                    <label for="<%= Model.Type %>_PersonalCareNailCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "2", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("2"), new { @id = Model.Type + "_PersonalCareNailCare2" })%>
                    <label for="<%= Model.Type %>_PersonalCareNailCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "1", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("1"), new { @id = Model.Type + "_PersonalCareNailCare1" })%>
                    <label for="<%= Model.Type %>_PersonalCareNailCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareNailCare", "0", data.AnswerOrEmptyString("PersonalCareNailCare").Equals("0"), new { @id = Model.Type + "_PersonalCareNailCare0" })%>
                    <label for="<%= Model.Type %>_PersonalCareNailCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Shave</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareShave", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "3", data.AnswerOrEmptyString("PersonalCareShave").Equals("3"), new { @id = Model.Type + "_PersonalCareShave3" })%>
                    <label for="<%= Model.Type %>_PersonalCareShave3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "2", data.AnswerOrEmptyString("PersonalCareShave").Equals("2"), new { @id = Model.Type + "_PersonalCareShave2" })%>
                    <label for="<%= Model.Type %>_PersonalCareShave2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "1", data.AnswerOrEmptyString("PersonalCareShave").Equals("1"), new { @id = Model.Type + "_PersonalCareShave1" })%>
                    <label for="<%= Model.Type %>_PersonalCareShave1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareShave", "0", data.AnswerOrEmptyString("PersonalCareShave").Equals("0"), new { @id = Model.Type + "_PersonalCareShave0" })%>
                    <label for="<%= Model.Type %>_PersonalCareShave0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Assist with Dressing</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareAssistWithDressing", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "3", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("3"), new { @id = Model.Type + "_PersonalCareAssistWithDressing3" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithDressing3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "2", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("2"), new { @id = Model.Type + "_PersonalCareAssistWithDressing2" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithDressing2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "1", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("1"), new { @id = Model.Type + "_PersonalCareAssistWithDressing1" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithDressing1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareAssistWithDressing", "0", data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("0"), new { @id = Model.Type + "_PersonalCareAssistWithDressing0" })%>
                    <label for="<%= Model.Type %>_PersonalCareAssistWithDressing0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Medication Reminder</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_PersonalCareMedicationReminder", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "3", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("3"), new { @id = Model.Type + "_PersonalCareMedicationReminder3" })%>
                    <label for="<%= Model.Type %>_PersonalCareMedicationReminder3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "2", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("2"), new { @id = Model.Type + "_PersonalCareMedicationReminder2" })%>
                    <label for="<%= Model.Type %>_PersonalCareMedicationReminder2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "1", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("1"), new { @id = Model.Type + "_PersonalCareMedicationReminder1" })%>
                    <label for="<%= Model.Type %>_PersonalCareMedicationReminder1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_PersonalCareMedicationReminder", "0", data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("0"), new { @id = Model.Type + "_PersonalCareMedicationReminder0" })%>
                    <label for="<%= Model.Type %>_PersonalCareMedicationReminder0" class="fixed shortest">N/A</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row ac strong">Elimination</div>
            <div class="row">
                <label class="fl strong">Assist with Bed Pan/Urinal</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_EliminationAssistWithBedPan", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "3", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("3"), new { @id = Model.Type + "_EliminationAssistWithBedPan3" })%>
                    <label for="<%= Model.Type %>_EliminationAssistWithBedPan3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "2", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("2"), new { @id = Model.Type + "_EliminationAssistWithBedPan2" })%>
                    <label for="<%= Model.Type %>_EliminationAssistWithBedPan2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "1", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("1"), new { @id = Model.Type + "_EliminationAssistWithBedPan1" })%>
                    <label for="<%= Model.Type %>_EliminationAssistWithBedPan1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistWithBedPan", "0", data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("0"), new { @id = Model.Type + "_EliminationAssistWithBedPan0" })%>
                    <label for="<%= Model.Type %>_EliminationAssistWithBedPan0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Assist with BSC</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_EliminationAssistBSC", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "3", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("3"), new { @id = Model.Type + "_EliminationAssistBSC3" })%>
                    <label for="<%= Model.Type %>_EliminationAssistBSC3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "2", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("2"), new { @id = Model.Type + "_EliminationAssistBSC2" })%>
                    <label for="<%= Model.Type %>_EliminationAssistBSC2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "1", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("1"), new { @id = Model.Type + "_EliminationAssistBSC1" })%>
                    <label for="<%= Model.Type %>_EliminationAssistBSC1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationAssistBSC", "0", data.AnswerOrEmptyString("EliminationAssistBSC").Equals("0"), new { @id = Model.Type + "_EliminationAssistBSC0" })%>
                    <label for="<%= Model.Type %>_EliminationAssistBSC0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Incontinence Care</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_EliminationIncontinenceCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "3", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("3"), new { @id = Model.Type + "_EliminationIncontinenceCare3" })%>
                    <label for="<%= Model.Type %>_EliminationIncontinenceCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "2", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("2"), new { @id = Model.Type + "_EliminationIncontinenceCare2" })%>
                    <label for="<%= Model.Type %>_EliminationIncontinenceCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "1", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("1"), new { @id = Model.Type + "_EliminationIncontinenceCare1" })%>
                    <label for="<%= Model.Type %>_EliminationIncontinenceCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationIncontinenceCare", "0", data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("0"), new { @id = Model.Type + "_EliminationIncontinenceCare0" })%>
                    <label for="<%= Model.Type %>_EliminationIncontinenceCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Empty Drainage Bag</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_EliminationEmptyDrainageBag", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "3", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("3"), new { @id = Model.Type + "_EliminationEmptyDrainageBag3" })%>
                    <label for="<%= Model.Type %>_EliminationEmptyDrainageBag3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "2", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("2"), new { @id = Model.Type + "_EliminationEmptyDrainageBag2" })%>
                    <label for="<%= Model.Type %>_EliminationEmptyDrainageBag2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "1", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("1"), new { @id = Model.Type + "_EliminationEmptyDrainageBag1" })%>
                    <label for="<%= Model.Type %>_EliminationEmptyDrainageBag1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationEmptyDrainageBag", "0", data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("0"), new { @id = Model.Type + "_EliminationEmptyDrainageBag0" })%>
                    <label for="<%= Model.Type %>_EliminationEmptyDrainageBag0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Record Bowel Movement</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_EliminationRecordBowelMovement", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "3", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("3"), new { @id = Model.Type + "_EliminationRecordBowelMovement3" })%>
                    <label for="<%= Model.Type %>_EliminationRecordBowelMovement3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "2", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("2"), new { @id = Model.Type + "_EliminationRecordBowelMovement2" })%>
                    <label for="<%= Model.Type %>_EliminationRecordBowelMovement2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "1", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("1"), new { @id = Model.Type + "_EliminationRecordBowelMovement1" })%>
                    <label for="<%= Model.Type %>_EliminationRecordBowelMovement1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationRecordBowelMovement", "0", data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("0"), new { @id = Model.Type + "_EliminationRecordBowelMovement0" })%>
                    <label for="<%= Model.Type %>_EliminationRecordBowelMovement0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Catheter Care</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_EliminationCatheterCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "3", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("3"), new { @id = Model.Type + "_EliminationCatheterCare3" })%>
                    <label for="<%= Model.Type %>_EliminationCatheterCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "2", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("2"), new { @id = Model.Type + "_EliminationCatheterCare2" })%>
                    <label for="<%= Model.Type %>_EliminationCatheterCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "1", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("1"), new { @id = Model.Type + "_EliminationCatheterCare1" })%>
                    <label for="<%= Model.Type %>_EliminationCatheterCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_EliminationCatheterCare", "0", data.AnswerOrEmptyString("EliminationCatheterCare").Equals("0"), new { @id = Model.Type + "_EliminationCatheterCare0" })%>
                    <label for="<%= Model.Type %>_EliminationCatheterCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row ac strong">Activity</div>
            <div class="row">
                <label class="fl strong">Dangle on Side of Bed</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_ActivityDangleOnSideOfBed", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "3", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("3"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed3" })%>
                    <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "2", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("2"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed2" })%>
                    <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "1", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("1"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed1" })%>
                    <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityDangleOnSideOfBed", "0", data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("0"), new { @id = Model.Type + "_ActivityDangleOnSideOfBed0" })%>
                    <label for="<%= Model.Type %>_ActivityDangleOnSideOfBed0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Turn &#38; Position</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_ActivityTurnPosition", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "3", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("3"), new { @id = Model.Type + "_ActivityTurnPosition3" })%>
                    <label for="<%= Model.Type %>_ActivityTurnPosition3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "2", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("2"), new { @id = Model.Type + "_ActivityTurnPosition2" })%>
                    <label for="<%= Model.Type %>_ActivityTurnPosition2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "1", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("1"), new { @id = Model.Type + "_ActivityTurnPosition1" })%>
                    <label for="<%= Model.Type %>_ActivityTurnPosition1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityTurnPosition", "0", data.AnswerOrEmptyString("ActivityTurnPosition").Equals("0"), new { @id = Model.Type + "_ActivityTurnPosition0" })%>
                    <label for="<%= Model.Type %>_ActivityTurnPosition0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Assist with Transfer</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_ActivityAssistWithTransfer", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "3", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("3"), new { @id = Model.Type + "_ActivityAssistWithTransfer3" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithTransfer3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "2", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithTransfer2" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithTransfer2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "1", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithTransfer1" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithTransfer1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithTransfer", "0", data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithTransfer0" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithTransfer0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Range of Motion</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_ActivityRangeOfMotion", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "3", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("3"), new { @id = Model.Type + "_ActivityRangeOfMotion3" })%>
                    <label for="<%= Model.Type %>_ActivityRangeOfMotion3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "2", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("2"), new { @id = Model.Type + "_ActivityRangeOfMotion2" })%>
                    <label for="<%= Model.Type %>_ActivityRangeOfMotion2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "1", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("1"), new { @id = Model.Type + "_ActivityRangeOfMotion1" })%>
                    <label for="<%= Model.Type %>_ActivityRangeOfMotion1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityRangeOfMotion", "0", data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("0"), new { @id = Model.Type + "_ActivityRangeOfMotion0" })%>
                    <label for="<%= Model.Type %>_ActivityRangeOfMotion0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Assist with Ambulation</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_ActivityAssistWithAmbulation", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "3", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("3"), new { @id = Model.Type + "_ActivityAssistWithAmbulation3" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithAmbulation3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "2", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("2"), new { @id = Model.Type + "_ActivityAssistWithAmbulation2" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithAmbulation2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "1", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("1"), new { @id = Model.Type + "_ActivityAssistWithAmbulation1" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithAmbulation1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityAssistWithAmbulation", "0", data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("0"), new { @id = Model.Type + "_ActivityAssistWithAmbulation0" })%>
                    <label for="<%= Model.Type %>_ActivityAssistWithAmbulation0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Equipment Care</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_ActivityEquipmentCare", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "3", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("3"), new { @id = Model.Type + "_ActivityEquipmentCare3" })%>
                    <label for="<%= Model.Type %>_ActivityEquipmentCare3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "2", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("2"), new { @id = Model.Type + "_ActivityEquipmentCare2" })%>
                    <label for="<%= Model.Type %>_ActivityEquipmentCare2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "1", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("1"), new { @id = Model.Type + "_ActivityEquipmentCare1" })%>
                    <label for="<%= Model.Type %>_ActivityEquipmentCare1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_ActivityEquipmentCare", "0", data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("0"), new { @id = Model.Type + "_ActivityEquipmentCare0" })%>
                    <label for="<%= Model.Type %>_ActivityEquipmentCare0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row ac strong">Household Task</div>
            <div class="row">
                <label class="fl strong">Make Bed</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_HouseholdTaskMakeBed", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "3", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("3"), new { @id = Model.Type + "_HouseholdTaskMakeBed3" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskMakeBed3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "2", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("2"), new { @id = Model.Type + "_HouseholdTaskMakeBed2" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskMakeBed2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "1", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("1"), new { @id = Model.Type + "_HouseholdTaskMakeBed1" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskMakeBed1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskMakeBed", "0", data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("0"), new { @id = Model.Type + "_HouseholdTaskMakeBed0" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskMakeBed0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Change Linen</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_HouseholdTaskChangeLinen", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "3", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("3"), new { @id = Model.Type + "_HouseholdTaskChangeLinen3" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskChangeLinen3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "2", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("2"), new { @id = Model.Type + "_HouseholdTaskChangeLinen2" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskChangeLinen2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "1", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("1"), new { @id = Model.Type + "_HouseholdTaskChangeLinen1" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskChangeLinen1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskChangeLinen", "0", data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("0"), new { @id = Model.Type + "_HouseholdTaskChangeLinen0" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskChangeLinen0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Light Housekeeping</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "3", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("3"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping3" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "2", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("2"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping2" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "1", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("1"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping1" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_HouseholdTaskLightHousekeeping", "0", data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("0"), new { @id = Model.Type + "_HouseholdTaskLightHousekeeping0" })%>
                    <label for="<%= Model.Type %>_HouseholdTaskLightHousekeeping0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row ac strong">Nutrition</div>
            <div class="row">
                <label class="fl strong">Meal Set-up</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_NutritionMealSetUp", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "3", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("3"), new { @id = Model.Type + "_NutritionMealSetUp3" })%>
                    <label for="<%= Model.Type %>_NutritionMealSetUp3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "2", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("2"), new { @id = Model.Type + "_NutritionMealSetUp2" })%>
                    <label for="<%= Model.Type %>_NutritionMealSetUp2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "1", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("1"), new { @id = Model.Type + "_NutritionMealSetUp1" })%>
                    <label for="<%= Model.Type %>_NutritionMealSetUp1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_NutritionMealSetUp", "0", data.AnswerOrEmptyString("NutritionMealSetUp").Equals("0"), new { @id = Model.Type + "_NutritionMealSetUp0" })%>
                    <label for="<%= Model.Type %>_NutritionMealSetUp0" class="fixed shortest">N/A</label>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Assist with Feeding</label>
                <div class="fr radio">
                    <%= Html.Hidden(Model.Type + "_NutritioAssistWithFeeding", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "3", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("3"), new { @id = Model.Type + "_NutritioAssistWithFeeding3" })%>
                    <label for="<%= Model.Type %>_NutritioAssistWithFeeding3" class="fixed shortest">QV</label>
                    <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "2", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("2"), new { @id = Model.Type + "_NutritioAssistWithFeeding2" })%>
                    <label for="<%= Model.Type %>_NutritioAssistWithFeeding2" class="fixed shortest">QW</label>
                    <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "1", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("1"), new { @id = Model.Type + "_NutritioAssistWithFeeding1" })%>
                    <label for="<%= Model.Type %>_NutritioAssistWithFeeding1" class="fixed shortest">PR</label>
                    <%= Html.RadioButton(Model.Type + "_NutritioAssistWithFeeding", "0", data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("0"), new { @id = Model.Type + "_NutritioAssistWithFeeding0" })%>
                    <label for="<%= Model.Type %>_NutritioAssistWithFeeding0" class="fixed shortest">N/A</label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments/Additional Instructions</legend>
        <div class="column wide">
            <div class="ac">
                <%= Html.Templates(Model.Type + "_CommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Comment" })%>
                <%= Html.TextArea(Model.Type + "_Comment", data.AnswerOrEmptyString("Comment"), new { @id = Model.Type + "_Comment", @class = "fill tallest" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Notifications</legend>
        <div class="column wide">
            <div class="row narrow">
                <div class="checkgroup">
                    <div class="required-notification option">
                        <%= string.Format("<input id='{0}_ReviewedWithHHA' name='{0}_ReviewedWithHHA' value='Yes' class='radio float-left' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ReviewedWithHHA").Equals("Yes").ToChecked())%>
                        <label for="<%= Model.Type %>_ReviewedWithHHA">Reviewed with Home Health Aide</label>
                    </div>
                    <div class="required-notification option">
                        <%= string.Format("<input id='{0}_ReviewedWithPatient' name='{0}_ReviewedWithPatient' value='Yes' class='radio float-left' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("ReviewedWithPatient").Equals("Yes").ToChecked())%>
                        <label for="<%= Model.Type %>_ReviewedWithPatient">Patient oriented with Care Plan</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= date %>" id="<%= Model.Type %>_SignatureDate" /></div>
            </div>
        </div>
        <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
        <div class="column wide">
            <div class="row narrowest">
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
                
            </div>
        </div>
        <% } %>
    </fieldset>      
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>