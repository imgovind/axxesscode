﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title><%= Model.Agency.Name %> | HHA Supervisory Visit | <%= Model.Patient.DisplayName %></title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
    </head>
    <body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : string.Empty %><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : string.Empty %><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : string.Empty %>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : string.Empty %><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : string.Empty %><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EHHA Supervisory Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EHome Health Aide:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("HealthAide").IsGuid() ? UserEngine.GetName(data["HealthAide"].Answer.ToGuid(), Current.AgencyId) : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAide Present:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AidePresent").Equals("1") ? "Yes" : "No"%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("VisitDate").IsValidDate() ? data.AnswerOrEmptyString("VisitDate").Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Milage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage").Clean() %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : string.Empty %><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : string.Empty %><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : string.Empty %>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : string.Empty %><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : string.Empty %><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EHHA Supervisory Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "";
        printview.addsection(
            "%3Col%3E%3Cli%3E" +
            printview.span("Arrives for assigned visits as scheduled", true) +
            printview.span("<%= data.AnswerOrEmptyString("ArriveOnTime").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("ArriveOnTime").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Follows client&#8217;s plan of care", true) +
            printview.span("<%= data.AnswerOrEmptyString("FollowPOC").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("FollowPOC").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Demonstrates positive and helpful attitude towards the client and others", true) +
            printview.span("<%= data.AnswerOrEmptyString("HasPositiveAttitude").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("HasPositiveAttitude").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Informs Nurse Supervisor of client needs and changes in condition as appropriate", true) +
            printview.span("<%= data.AnswerOrEmptyString("InformChanges").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("InformChanges").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Aide implements Universal Precautions per agency policy", true) +
            printview.span("<%= data.AnswerOrEmptyString("IsUniversalPrecautions").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("IsUniversalPrecautions").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Any changes made to client&#8217;s plan of care at this time", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCChanges").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("POCChanges").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Patient/CG satisfied with care and services provided by aide", true) +
            printview.span("<%= data.AnswerOrEmptyString("IsServicesSatisfactory").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("IsServicesSatisfactory").Equals("0") ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Additional Comments/Findings", true) +
            printview.span("<%= data.AnswerOrEmptyString("AdditionalComments").Clean() %>",0,10) +
            "%3C/li%3E%3C/ol%3E");
        printview.addsection(
            printview.col(2,
                printview.span("Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model.SignatureText.Clean() %>",0,1) +
                printview.span("<%= Model.SignatureDate.IsValidDate() ? Model.SignatureDate.Clean() : string.Empty %>",0,1)));
<%  }).Render(); %>
    </body>
</html>