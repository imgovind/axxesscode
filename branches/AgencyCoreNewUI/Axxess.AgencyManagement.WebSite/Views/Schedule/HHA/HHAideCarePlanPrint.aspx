﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  string[] diet = data.AnswerArray("IsDiet"); %>
<%  string[] allergies = data.AnswerArray("Allergies"); %>
<%  string[] isVitalSignParameter = data.AnswerArray("IsVitalSignParameter"); %>
<%  string[] safetyMeasure = data.AnswerArray("SafetyMeasures"); %>
<%  string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
<%  string[] activitiesPermitted = data.AnswerArray("ActivitiesPermitted"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%= Model.Agency.Name %>  | Home Health Aide Care Plan | <%= Model.Patient.DisplayName %></title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
    </head>
    <body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EHome Health Aide%3Cbr /%3ECare Plan%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EHHA Frequency:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("HHAFrequency").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.StartDate.IsValid() && Model.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EDNR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("DNR").Equals("1") ? "Yes" : string.Empty %><%= data.AnswerOrEmptyString("DNR").Equals("0") ? "No" : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean() %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EHome Health Aide%3Cbr /%3ECare Plan%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "";
        printview.addsection(
            printview.col(2,
                printview.checkbox("Diet:",<%= diet.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Allergies:",<%= allergies.Contains("Yes").ToString().ToLower() %>) +
                printview.span("<%= data.AnswerOrEmptyString("Diet").Clean() %>",false,1) +
                printview.span("<%= data.AnswerOrEmptyString("AllergiesDescription").Clean() %>",false,1)));
        printview.addsection(<%
            if (isVitalSignParameter != null && isVitalSignParameter.Contains("1")) { %>
                printview.checkbox("N/A",true),<%
            } else { %>
                "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3ESBP%3C/th%3E%3Cth%3EDBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3Egreater than (&#62;)%3C/th%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("SystolicBPGreaterThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("DiastolicBPGreaterThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PulseGreaterThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("RespirationGreaterThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("TempGreaterThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("WeightGreaterThan").Clean() %>" + 
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3Eor less than (&#60;)%3C/th%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("SystolicBPLessThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("DiastolicBPLessThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PulseLessThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("RespirationLessThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("TempLessThan").Clean() %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("WeightLessThan").Clean() %>" +
                "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",<%
            } %>
            "Vital Sign Ranges");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Anticoagulant Precautions",<%= safetyMeasure.Contains("1").ToString().ToLower()%>) +
                printview.checkbox("Emergency Plan Developed",<%= safetyMeasure.Contains("2").ToString().ToLower()%>) +
                printview.checkbox("Fall Precautions",<%= safetyMeasure.Contains("3").ToString().ToLower()%>) +
                printview.checkbox("Keep Pathway Clear",<%= safetyMeasure.Contains("4").ToString().ToLower()%>) +
                printview.checkbox("Keep Side Rails Up",<%= safetyMeasure.Contains("5").ToString().ToLower()%>) +
                printview.checkbox("Neutropenic Precautions",<%= safetyMeasure.Contains("6").ToString().ToLower()%>) +
                printview.checkbox("O<sub>2</sub> Precautions",<%= safetyMeasure.Contains("7").ToString().ToLower()%>) +
                printview.checkbox("Proper Position During Meals",<%= safetyMeasure.Contains("8").ToString().ToLower()%>) +
                printview.checkbox("Standard Precautions/Infection Control",<%= safetyMeasure.Contains("9").ToString().ToLower()%>) +
                printview.checkbox("Seizure Precautions",<%= safetyMeasure.Contains("10").ToString().ToLower()%>) +
                printview.checkbox("Sharps Safety",<%= safetyMeasure.Contains("11").ToString().ToLower()%>) +
                printview.checkbox("Slow Position Change",<%= safetyMeasure.Contains("12").ToString().ToLower()%>) +
                printview.checkbox("Safety in ADLs",<%= safetyMeasure.Contains("13").ToString().ToLower()%>) +
                printview.checkbox("Support During Transfer &#38; Ambulation",<%= safetyMeasure.Contains("14").ToString().ToLower()%>) +
                printview.checkbox("Use of Assistive Devices",<%= safetyMeasure.Contains("15").ToString().ToLower()%>)) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E: <%= data.AnswerOrEmptyString("OtherSafetyMeasures").Clean() %>",false,2),
            "Safety Precautions");
        printview.addsection(
            printview.span("%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%222%22%3E%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth class=%22align-left%22%3EAssignment%3C/th%3E%3Cth colspan=%224%22%3EStatus%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align-left%22%3EVital Signs%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ETemperature%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsTemperature").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsTemperature").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsTemperature").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsTemperature").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EBlood Presure%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsBloodPressure").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EHeart Rate%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsHeartRate").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ERespirations%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsRespirations").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsRespirations").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsRespirations").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsRespirations").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EWeight%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsWeight").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsWeight").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsWeight").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("VitalSignsWeight").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align-left%22%3EPersonal Care%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EBed Bath%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareBedBath").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareBedBath").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareBedBath").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareBedBath").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with Chair Bath%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithChairBath").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ETub Bath%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareTubBath").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareTubBath").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareTubBath").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareTubBath").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EShower%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShower").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShower").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShower").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShower").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EShower w/Chair%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShowerWithChair").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EShampoo Hair%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShampooHair").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EHair Care/Comb Hair%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareHairCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareHairCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareHairCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareHairCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EOral Care%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareOralCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareOralCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareOralCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareOralCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ESkin Care%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareSkinCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EPericare%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCarePericare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCarePericare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCarePericare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCarePericare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ENail Care%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareNailCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareNailCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareNailCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareNailCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EShave%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShave").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShave").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShave").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareShave").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with Dressing%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareAssistWithDressing").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EMedication Reminder%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("PersonalCareMedicationReminder").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3Ctd%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth class=%22align-left%22%3EAssignment%3C/th%3E%3Cth colspan=%224%22%3EStatus%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align-left%22%3EElimination%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with Bed Pan/Urinal%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistWithBedPan").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with BSC%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistBSC").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistBSC").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistBSC").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationAssistBSC").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EIncontinence Care%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationIncontinenceCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EEmpty Drainage Bag%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationEmptyDrainageBag").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ERecord Bowel Movement%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationRecordBowelMovement").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ECatheter Care%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationCatheterCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationCatheterCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationCatheterCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("EliminationCatheterCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align-left%22%3EActivity%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EDangle on Side of Bed%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityDangleOnSideOfBed").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ETurn &#38; Position%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityTurnPosition").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityTurnPosition").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityTurnPosition").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityTurnPosition").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with Transfer%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithTransfer").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ERange of Motion%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityRangeOfMotion").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with Ambulation%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityAssistWithAmbulation").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EEquipment Care%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("ActivityEquipmentCare").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align-left%22%3EHousehold Task%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EMake Bed%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskMakeBed").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EChange Linen%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskChangeLinen").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3ELight Housekeeping%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("HouseholdTaskLightHousekeeping").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align-left%22%3ENutrition%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EMeal Set-up%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritionMealSetUp").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritionMealSetUp").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritionMealSetUp").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritionMealSetUp").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22%3EAssist with Feeding%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("3") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("2") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("1") ? "X" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data.AnswerOrEmptyString("NutritioAssistWithFeeding").Equals("0") ? "X" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E"));
        printview.addsection(
            printview.col(3,
                printview.checkbox("Amputation",<%= functionLimitations.Contains("1").ToString().ToLower()%>) +
                printview.checkbox("Bowel/Bladder Incontinence",<%= functionLimitations.Contains("2").ToString().ToLower()%>) +
                printview.checkbox("Contracture",<%= functionLimitations.Contains("3").ToString().ToLower()%>) +
                printview.checkbox("Hearing",<%= functionLimitations.Contains("4").ToString().ToLower()%>) +
                printview.checkbox("Paralysis",<%= functionLimitations.Contains("5").ToString().ToLower()%>) +
                printview.checkbox("Endurance",<%= functionLimitations.Contains("6").ToString().ToLower()%>) +
                printview.checkbox("Ambulation",<%= functionLimitations.Contains("7").ToString().ToLower()%>) +
                printview.checkbox("Speech",<%= functionLimitations.Contains("8").ToString().ToLower()%>) +
                printview.checkbox("Legally Blind",<%= functionLimitations.Contains("9").ToString().ToLower()%>) +
                printview.checkbox("Dyspnea with Minimal Exertion",<%= functionLimitations.Contains("A").ToString().ToLower()%>) +
                printview.checkbox("Other",<%= functionLimitations.Contains("B").ToString().ToLower()%>) +
                printview.span("<%= data.AnswerOrEmptyString("FunctionLimitationsOther").Clean() %>",0,1) ),
            "Functional Limitations");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Complete bed rest",<%= activitiesPermitted.Contains("1").ToString().ToLower()%>) +
                printview.checkbox("Bed rest with BRP",<%= activitiesPermitted.Contains("2").ToString().ToLower()%>) +
                printview.checkbox("Up as tolerated",<%= activitiesPermitted.Contains("3").ToString().ToLower()%>) +
                printview.checkbox("Transfer bed-chair",<%= activitiesPermitted.Contains("4").ToString().ToLower()%>) +
                printview.checkbox("Exercise prescribed",<%= activitiesPermitted.Contains("5").ToString().ToLower()%>) +
                printview.checkbox("Partial weight bearing",<%= activitiesPermitted.Contains("6").ToString().ToLower()%>) +
                printview.checkbox("Independent at home",<%= activitiesPermitted.Contains("7").ToString().ToLower()%>) +
                printview.checkbox("Crutches",<%= activitiesPermitted.Contains("8").ToString().ToLower()%>) +
                printview.checkbox("Cane",<%= activitiesPermitted.Contains("9").ToString().ToLower()%>) +
                printview.checkbox("Wheelchair",<%= activitiesPermitted.Contains("10").ToString().ToLower()%>) +
                printview.checkbox("Walker",<%= activitiesPermitted.Contains("11").ToString().ToLower()%>) +
                printview.checkbox("Other",<%= activitiesPermitted.Contains("12").ToString().ToLower()%>)) +
            printview.span("<%= data.AnswerOrEmptyString("ActivitiesPermittedOther").Clean() %>",0,1),
            "Activities Permitted");
        printview.addsection(printview.span("%3Cem class=%22small fr%22%3E* QV = Every Visit; QW = Every Week; PR = Patient Request; N/A = Not Applicable%3C/em%3E"));
        printview.addsection(printview.span("<%= data.AnswerOrEmptyString("Comment").Clean() %>",false,3),"Comments / Additional Instructions")
        printview.addsection(
            printview.checkbox("Reviewed with Home Health Aide",<%= data.AnswerOrEmptyString("ReviewedWithHHA").Equals("Yes").ToString().ToLower()%>),
                "Notifications");
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model.SignatureText.Clean() %>",0,1) +
                printview.span("<%= Model.SignatureDate.IsValidDate() ? Model.SignatureDate.Clean() : string.Empty %>",0,1)));
<%  }).Render(); %>
    </body>
</html>