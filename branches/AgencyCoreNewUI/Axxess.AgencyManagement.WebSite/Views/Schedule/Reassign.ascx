﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<%  using (Html.BeginForm("ReassignTask", "Schedule", FormMethod.Post, new { @id = "ReassignTask_Form" })) { %>
    <%= Html.Hidden("eventId", Model.EventId, new { @id = "ReassignTask_EventId" })%>
    <%= Html.Hidden("episodeId", Model.EpisodeId, new { @id = "ReassignTask_EpisodeId" }) %>
    <%= Html.Hidden("patientId", Model.PatientId, new { @id = "ReassignTask_PatientId" })%>
    <%= Html.Hidden("userId", Model.UserId, new { @id = "ReassignTask_UserId" })%>
    <div class="wrapper main">
        <fieldset>
            <legend>Reassign Task</legend>
            <div class="wide column">
                <div class="row">
                    <label for="ReassignTask_EmployeeId" class="fl strong">Reassign To</label>
                    <div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "newUserId", "", Guid.Empty, 1, new { @id = "ReassignTask_NewUserId", @class = "required notzero" })%></div>
                </div>
            </div>
        </fieldset>
        <div class="buttons">
            <ul>
                <li><a class="save close">Reassign</a></li>
                <li><a class="close">Cancel</a></li>
            </ul>
        </div>
    </div>
<%  } %>
