﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  string[] patientCondition = data.AnswerArray("PatientCondition"); %>
<%  string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
<%  string[] dischargeInstructionsGivenTo = data.AnswerArray("DischargeInstructionsGivenTo"); %>
<%  string[] differentTasks = data.AnswerArray("DifferentTasks"); %>
<head>
    <title><%= Model.Agency.Name %> | Discharge Summary | <%= Model.Patient.DisplayName %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean() + "<br />" + location.AddressLine1.Clean() + location.AddressLine2.Clean() + "<br />" + location.AddressCity.ToTitleCase() + ", " + location.AddressStateCode.ToString().ToUpper() + "  " + location.AddressZipCode.Clean() + "<br />Phone: " + location.PhoneWorkFormatted.Clean() + " | Fax: " + location.FaxNumberFormatted.Clean()%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EDischarge Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient.DisplayNameWithMi.Clean() %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.VisitDate.Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EDischarge Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("DischargeDate").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient.PatientIdNumber.Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan class=%22dual%22%3E" +
            "<%= Model.PhysicianDisplayName.Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPatient Notified of Discharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("IsNotificationDC").Equals("1") ? "Yes, " + data.AnswerOrDefault("NotificationDate", data.AnswerOrEmptyString("NotificationDateOther")) : "No" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EReason for D/C:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("ReasonForDC").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean() %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean() + "<br />" + location.AddressLine1.Clean() + location.AddressLine2.Clean() + "<br />" + location.AddressCity.ToTitleCase() + ", " + location.AddressStateCode.ToString().ToUpper() + "  " + location.AddressZipCode.Clean() + "<br />Phone: " + location.PhoneWorkFormatted.Clean() + " | Fax: " + location.FaxNumberFormatted.Clean()%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EDischarge Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient.DisplayNameWithMi.Clean() %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "";
        printview.addsection(
            printview.col(4,
                printview.checkbox("Stable",<%= patientCondition.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Improved",<%= patientCondition.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Unchanged",<%= patientCondition.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Unstable",<%= patientCondition.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Declined",<%= patientCondition.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Goals Met",<%= patientCondition.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Goals Not Met",<%= patientCondition.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Goals Partially Met",<%= patientCondition.Contains("8").ToString().ToLower() %>)),
            "Patient Condition and Outcomes");
        printview.addsection(
            printview.col(4,
                printview.checkbox("SN",<%= serviceProvided.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("PT",<%= serviceProvided.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("OT",<%= serviceProvided.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("ST",<%= serviceProvided.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("MSW",<%= serviceProvided.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("HHA",<%= serviceProvided.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= serviceProvided.Contains("7").ToString().ToLower() %>) +
                printview.span("<%= data.AnswerOrEmptyString("ServiceProvidedOtherValue").Clean() %>",0,1) ),
            "Service(s) Provided");
        printview.addsection(
            printview.span("<%= data.AnswerOrEmptyString("CareSummary").Clean() %>",false,8),
            "Care Summary: (Care Given, Progress, Regress Including Therapies)");
        printview.addsection(
            printview.span("<%= data.AnswerOrEmptyString("ConditionOfDischarge").Clean() %>",0,8),
            "Condition of discharge (Include VS, BS, Functional and Overall Status)");
        printview.addsection(
            printview.span("Discharge Disposition: Where is the patient after discharge from your agency?",true) +
            printview.checkbox("1 &#8211; Patient remained in the community (without formal assistive services)",<%= data.AnswerOrEmptyString("DischargeDisposition").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Patient remained in the community (with formal assistive services)",<%= data.AnswerOrEmptyString("DischargeDisposition").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Patient transferred to a non-institutional hospice",<%= data.AnswerOrEmptyString("DischargeDisposition").Equals("03").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; Unknown because patient moved to a geographic location not served by this agency",<%= data.AnswerOrEmptyString("DischargeDisposition").Equals("04").ToString().ToLower() %>) +
            printview.checkbox("UK &#8211; Other unknown",<%= data.AnswerOrEmptyString("DischargeDisposition").Equals("UK").ToString().ToLower() %>));
        printview.addsection(
            printview.span("Discharge Instructions Given To:",true) +
            printview.col(5,
                printview.checkbox("Patient",<%= dischargeInstructionsGivenTo.Contains("1").ToString().ToLower()  %>) +
                printview.checkbox("Caregiver",<%= dischargeInstructionsGivenTo.Contains("2").ToString().ToLower()  %>) +
                printview.checkbox("N/A",<%= dischargeInstructionsGivenTo.Contains("3").ToString().ToLower()  %>) +
                printview.checkbox("Other",<%= dischargeInstructionsGivenTo.Contains("4").ToString().ToLower()  %>) +
                printview.span("<%= data.AnswerOrEmptyString("DischargeInstructionsGivenToOther").Clean() %>")));
        printview.addsection(
            printview.span("Discharge Instructions",true) +
            printview.span("<%= data.AnswerOrEmptyString("DischargeInstructions").Clean() %>",0,3));
        printview.addsection(
            printview.col(2,
                printview.span("Verbalized understanding",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("IsVerbalizedUnderstanding").Equals("1").ToString().ToLower() %>) +
                    printview.checkbox("No",<%= data.AnswerOrEmptyString("IsVerbalizedUnderstanding").Equals("0").ToString().ToLower() %>))) +
            printview.checkbox("All services notified and discontinued",<%= differentTasks.Contains("1").ToString().ToLower()  %>) +
            printview.checkbox("Order and summary completed",<%= differentTasks.Contains("2").ToString().ToLower()  %>) +
            printview.checkbox("Information provided to patient for continuing needs",<%= differentTasks.Contains("3").ToString().ToLower()  %>) +
            printview.checkbox("Physician notified",<%= differentTasks.Contains("4").ToString().ToLower()  %>));
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model.SignatureText.Clean() %>",0,1) +
                printview.span("<%= Model.SignatureDate.Clean() %>",0,1)));
<%  }).Render(); %>
</body>
</html>