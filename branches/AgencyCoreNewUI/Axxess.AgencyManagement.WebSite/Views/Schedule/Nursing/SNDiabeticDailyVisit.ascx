﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Diabetic Daily Visit Nursing Note | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer : ""; %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label for="" class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="fl strong">Visit Date</label>
                    <%  var scheduledPrn = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Scheduled", Value = "Scheduled" },
                            new SelectListItem { Text = "PRN", Value = "PRN" }
                        }, "Value", "Text", data.AnswerOrDefault("ScheduledPrn", "0")); %>
                    <div class="fr">
                        <input type="text" class="date-picker" name="<%= Model.Type %>_VisitDate" id="Text1" value="<%= Model.VisitDate %>" />
                        <br />
                        <%= Html.DropDownList(Model.Type + "_ScheduledPrn", scheduledPrn, new { @id = Model.Type + "_ScheduledPrn", @class = "oe" })%>
                    </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeIn" class="fl strong">Time In</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = Model.Type + "_TimeIn", @class = "complete-required time-picker" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeOut" class="fl strong">Time Out</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = Model.Type + "_TimeOut", @class = "complete-required time-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
            *this won't work
                <label for="HHAideVisit_PreviousNotes" class="fl strong">Previous Notes</label>
                <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "HHAideVisit_PreviousNotes" })%></div>
            </div>
            <div class="row">
                <label for="HHAideVisit_AssociatedMileage" class="fl strong">Associated Mileage</label>
               <div class="fr"><%= Html.TextBox(Model.Type + "_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : string.Empty, new { @id = "HHAideVisit_AssociatedMileage", @class = "" })%></div>
            </div>
             <div class="row">
                <label for="HHAideVisit_Surcharge" class="fl strong">Surcharge</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Surcharge", data.ContainsKey("Surcharge") ? data["Surcharge"].Answer : string.Empty, new { @id = "HHAideVisit_Surcharge", @class = "" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl strong">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl strong">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Vital Signs</legend>
        <% Html.RenderPartial("Nursing/Sections/VitalSigns", Model); %>
    </fieldset>
    <fieldset class="half fr"> 
        <legend>Cardiovascular</legend>
        <% Html.RenderPartial("Nursing/Sections/Cardiovascular", Model); %>
    </fieldset>
    <div class="clr"></div>
    <fieldset class="half fl"> 
        <legend>Respiratory</legend>
        <% Html.RenderPartial("Nursing/Sections/Respiratory", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>HHA PRN Supervisory Visit</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">HHA Present</label>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_HhaPresent", "1", data.AnswerOrEmptyString("HhaPresent").Equals("1"), new { @id = Model.Type + "_HhaPresent1" })%>
                    <label for="<%= Model.Type %>_HhaPresent1">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_HhaPresent", "0", data.AnswerOrEmptyString("HhaPresent").Equals("0"), new { @id = Model.Type + "_HhaPresent0" })%>
                    <label for="<%= Model.Type %>_HhaPresent0">No</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Aide following Care Plan</label>
                <div class="fr">
                    <%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "1", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("1"), new { @id = Model.Type + "_AideFollowCarePlan1" })%>
                    <label for="<%= Model.Type %>_AideFollowCarePlan1">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "0", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("0"), new { @id = Model.Type + "_AideFollowCarePlan0" })%>
                    <label for="<%= Model.Type %>_AideFollowCarePlan0">No</label>
                </div>
            </div>
            <div class="row">
                <label class="strong">Comments</label>
                <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), 3, 20, new { @class = "fill", @id = Model.Type + "_GenericComments" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset class="half fl">
        <legend>Comments/Teaching</legend>
        <% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Narrative.ascx", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Diabetes</legend>
        <% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DiabeticCare.ascx", Model); %>
    </fieldset>
    <div class="clr"></div>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= date %>" id="<%= Model.Type %>_SignatureDate" /></div>
            </div>
        </div>
        <div class="wide column">
            <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <div class="narrowest row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0)" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>