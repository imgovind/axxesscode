﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Discharge Summary | <%= Model.Patient.DisplayName %></span>
<%  var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<%  var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer : ""; %>
<div class="wrapper main note">
<% using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "18")%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label for="" class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="strong">Visit Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_DischargeDate" class="strong">Discharge Date</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_DischargeDate" value="<%= data.AnswerOrEmptyString("DischargeDate") %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_DischargeDate" /></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_Physician" class="strong">Physician</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Physician",!Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : data.AnswerOrEmptyString("Physician"), new { @id = Model.Type + "_Physician", @class = "physician-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_PreviousNotes" class="fl strong">Previous Notes</label>
                <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
            </div>
            <div class="row">
                <label class="strong">Notification of Discharge given to patient</label>
                <div class="fr">
                    <%= Html.Hidden(Model.Type + "_IsNotificationDC", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_IsNotificationDC", "1", data.AnswerOrEmptyString("IsNotificationDC").Equals("1"), new { @id = Model.Type + "_IsNotificationDCY" })%>
                    <label for="<%= Model.Type %>_IsNotificationDCY" class="short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_IsNotificationDC", "0", data.AnswerOrEmptyString("IsNotificationDC").Equals("0"), new { @id = Model.Type + "_IsNotificationDCN" })%>
                    <label for="<%= Model.Type %>_IsNotificationDCN" class="short">No</label>
                </div>
                <div class="clear"></div>
                <div class="fr">
                    <label for="<%= Model.Type %>_NotificationDate">If Yes</label>
                    <%  var patientReceived = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "5 day", Value = "1" },
                            new SelectListItem { Text = "2 day", Value = "2" },
                            new SelectListItem { Text = "Other", Value = "3" }
                        }, "Value", "Text", data.AnswerOrDefault("NotificationDate", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_NotificationDate", patientReceived, new { @id = Model.Type + "_NotificationDate" }) %>
                    <%= Html.TextBox(Model.Type + "_NotificationDateOther", data.AnswerOrEmptyString("NotificationDateOther"), new { @id = Model.Type + "_NotificationDateOther", @style = "display:none"}) %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl strong">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl strong">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Patient Condition and Outcomes</legend>
        <div class="column">
            <div class="row">
                <%  string[] patientCondition = data.AnswerArray("PatientCondition"); %>
                <input name="<%= Model.Type %>_PatientCondition" value=" " type="hidden" />
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionStable' name='{0}_PatientCondition' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionStable">Stable</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionImproved' name='{0}_PatientCondition' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionImproved">Improved</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionUnchanged' name='{0}_PatientCondition' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionUnchanged">Unchanged</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionUnstable' name='{0}_PatientCondition' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionUnstable">Unstable</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionDeclined' name='{0}_PatientCondition' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("5").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionDeclined">Declined</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionGoalsMet' name='{0}_PatientCondition' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("6").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionGoalsMet">Goals Met</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionGoalsPartiallyMet' name='{0}_PatientCondition' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("7").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionGoalsPartiallyMet">GoalsNot Met</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_PatientConditionGoalsNotMet' name='{0}_PatientCondition' value='8' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("8").ToChecked()) %>
                        <label for="<%= Model.Type %>_PatientConditionGoalsNotMet">Goals Partially Met</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Service(s) Provided</legend>
        <div class="column">
            <div class="row">
                <%  string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
                <input name="<%= Model.Type %>_ServiceProvided" value=" " type="hidden" />
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedSN' name='{0}_ServiceProvided' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedSN">SN</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedPT' name='{0}_ServiceProvided' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedPT">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedOT' name='{0}_ServiceProvided' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedOT">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedST' name='{0}_ServiceProvided' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedST">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedMSW' name='{0}_ServiceProvided' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("5").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedMSW">MSW</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedHHA' name='{0}_ServiceProvided' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("6").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedHHA">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_ServiceProvidedOther' name='{0}_ServiceProvided' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, serviceProvided.Contains("7").ToChecked()) %>
                        <label for="<%= Model.Type %>_ServiceProvidedOther">Other</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_ServiceProvidedOtherValue", data.AnswerOrEmptyString("ServiceProvidedOtherValue"), new { @id = Model.Type + "_ServiceProvidedOtherValue", @class = "oe" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset class="half fl">
        <legend>Care Summary</legend>
        <div class="column">
            <div class="row ac">
                <em>(Care Given, Progress, Regress including Therapies)</em>
            </div>
            <div class="row ac">
                <%= Html.Templates(Model.Type + "_CareSummaryTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_CareSummary" })%>
                <%= Html.TextArea(Model.Type + "_CareSummary",data.AnswerOrEmptyString("CareSummary"), new { @class = "fill", @id = Model.Type + "_CareSummary", @style="height:150px" })%>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Condition of Discharge</legend> 
        <div class="column">
            <div class="row ac">
                <em>(Include VS, BS, Functional and Overall Status)</em>
            </div>
            <div class="row ac">
                <%= Html.Templates(Model.Type + "_ConditionOfDischargeTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_ConditionOfDischarge" })%>
                <%= Html.TextArea(Model.Type + "_ConditionOfDischarge", data.AnswerOrEmptyString("ConditionOfDischarge"), new { @class = "fill", @id = Model.Type + "_ConditionOfDischarge", @style = "height:150px" })%>
            </div>            
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset>
        <legend>Discharge Details</legend>
        <div class="column">
            <div class="row">
                <label class="strong">Discharge Disposition: Where is the Patient after Discharge from your Agency?</label>
                <%= Html.Hidden(Model.Type + "_DischargeDisposition", " ", new { @id = "" })%>
                <div class="checkgroup wide">
                    <div class="option">
                        <%= Html.RadioButton(Model.Type + "_DischargeDisposition", "01", data.AnswerOrEmptyString("DischargeDisposition").Equals("01"), new { @id = Model.Type + "_DischargeDisposition1", @class = "radio float-left" })%>
                        <label for="<%= Model.Type %>_DischargeDisposition1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (without formal assistive services)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.Type + "_DischargeDisposition", "02", data.AnswerOrEmptyString("DischargeDisposition").Equals("02"), new { @id = Model.Type + "_DischargeDisposition2", @class = "radio float-left" })%>
                        <label for="<%= Model.Type %>_DischargeDisposition2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Patient remained in the community (with formal assistive services)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.Type + "_DischargeDisposition", "03", data.AnswerOrEmptyString("DischargeDisposition").Equals("03"), new { @id = Model.Type + "_DischargeDisposition3", @class = "radio float-left" })%>
                        <label for="<%= Model.Type %>_DischargeDisposition3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Patient transferred to a non-institutional hospice)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.Type + "_DischargeDisposition", "04", data.AnswerOrEmptyString("DischargeDisposition").Equals("04"), new { @id = Model.Type + "_DischargeDisposition4", @class = "radio float-left" })%>
                        <label for="<%= Model.Type %>_DischargeDisposition4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Unknown because patient moved to a geographic location not served by this agency</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.Type + "_DischargeDisposition", "UK", data.AnswerOrEmptyString("DischargeDisposition").Equals("UK"), new { @id = Model.Type + "_DischargeDispositionUK", @class = "radio float-left" })%>
                        <label for="<%= Model.Type %>_DischargeDispositionUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Other unknown</span>
                        </label>
                    </div>  
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <%  string[] dischargeInstructionsGivenTo = data.AnswerArray("DischargeInstructionsGivenTo"); %>
                <input name="<%= Model.Type %>_DischargeInstructionsGivenTo" value=" " type="hidden" />
                <label class="strong">Discharge Instructions Given To</label>
                <div class="checkgroup narrow">
                    <div class="option">
                        <%= string.Format("<input id='{0}_DischargeInstructionsGivenTo1' name='{0}_DischargeInstructionsGivenTo' value='1' class='radio' type='checkbox' {1} />", Model.Type, dischargeInstructionsGivenTo != null ? dischargeInstructionsGivenTo.Contains("1").ToChecked(): "")%>
                        <label for="<%= Model.Type %>_DischargeInstructionsGivenTo1" class="fixed radio">Patient</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_DischargeInstructionsGivenTo2' name='{0}_DischargeInstructionsGivenTo' value='2' class='radio' type='checkbox' {1} />", Model.Type, dischargeInstructionsGivenTo != null ? dischargeInstructionsGivenTo.Contains("2").ToChecked() : "")%>
                        <label for="<%= Model.Type %>_DischargeInstructionsGivenTo2" class="fixed radio">Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_DischargeInstructionsGivenTo3' name='{0}_DischargeInstructionsGivenTo' value='3' class='radio' type='checkbox' {1} />", Model.Type, dischargeInstructionsGivenTo != null ? dischargeInstructionsGivenTo.Contains("3").ToChecked() : "")%>
                        <label for="<%= Model.Type %>_DischargeInstructionsGivenTo3" class="fixed radio">N/A</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_DischargeInstructionsGivenTo4' name='{0}_DischargeInstructionsGivenTo' value='4' class='radio' type='checkbox' {1} />", Model.Type, dischargeInstructionsGivenTo != null ? dischargeInstructionsGivenTo.Contains("4").ToChecked() : "")%>
                        <label for="<%= Model.Type %>_DischargeInstructionsGivenTo4">Other</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_DischargeInstructionsGivenToOther", data.AnswerOrEmptyString("DischargeInstructionsGivenToOther"), new { @id = Model.Type + "_DischargeInstructionsGivenToOther", @class="oe" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="strong" for="<%= Model.Type %>_DischargeInstructions">Discharge Instructions</label>
                <%= Html.TextArea(Model.Type + "_DischargeInstructions", data.AnswerOrEmptyString("DischargeInstructions"), new { @id = Model.Type + "_DischargeInstructions", @class="fill" }) %>
            </div>
            <div class="row">  
                <label class="strong">Verbalized understanding</label>
                <div class="fr">
                    <%= Html.Hidden(Model.Type + "_IsVerbalizedUnderstanding", " ", new { @id = "" })%>
                    <%= Html.RadioButton(Model.Type + "_IsVerbalizedUnderstanding", "1", data.AnswerOrEmptyString("IsVerbalizedUnderstanding").Equals("1"), new { @id = Model.Type + "_IsVerbalizedUnderstandingY" })%>
                    <label for="<%= Model.Type %>_IsVerbalizedUnderstandingY" class="short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_IsVerbalizedUnderstanding", "0", data.AnswerOrEmptyString("IsVerbalizedUnderstanding").Equals("0"), new { @id = Model.Type + "_IsVerbalizedUnderstandingN" })%>
                    <label for="<%= Model.Type %>_IsVerbalizedUnderstandingN" class="short">No</label>
                </div>
            </div>
            <div class="row">
                <%  string[] differentTasks = data.AnswerArray("DifferentTasks"); %>
                <input name="<%= Model.Type %>_DifferentTasks" value=" " type="hidden" />
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input id='{0}_DifferentTasks1' name='{0}_DifferentTasks' value='1' class='radio' type='checkbox' {1} />", Model.Type, differentTasks.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_DifferentTasks1" style="margin-left:20px">All services notified and discontinued</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_DifferentTasks2' name='{0}_DifferentTasks' value='2' class='radio' type='checkbox' {1} />", Model.Type, differentTasks.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_DifferentTasks2" style="margin-left:20px">Order and summary completed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_DifferentTasks3' name='{0}_DifferentTasks' value='3' class='radio' type='checkbox' {1} />", Model.Type, differentTasks.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_DifferentTasks3" style="margin-left:20px">Information provided to patient for continuing needs</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_DifferentTasks4' name='{0}_DifferentTasks' value='4' class='radio' type='checkbox' {1} />", Model.Type, differentTasks.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_DifferentTasks4" style="margin-left:20px">Physician notified</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignature" class="fl strong">Clinician Signature</label>
                <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_ClinicianSignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= date %>" id="<%= Model.Type %>_SignatureDate" /></div>
            </div>
        </div>
        <div class="wide column">
            <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <div class="narrowest row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0)" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>