﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  string[] homeboundStatus = data.AnswerArray("HomeboundStatus"); %>
<%  string[] patientCondition = data.AnswerArray("PatientCondition"); %>
<%  string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
<%  string[] recommendedService = data.AnswerArray("RecommendedService"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name %> | 60 Day Summary/Case Conference  | <%= Model.Patient.DisplayName %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/Nursing/SixtyDaySummary.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= Model.Agency.Name.Clean() + "<br />" + location.AddressLine1.Clean() + location.AddressLine2.Clean() + "<br />" + location.AddressCity.ToTitleCase() + ", " + location.AddressStateCode.ToString().ToUpper() + "  " + location.AddressZipCode.Clean() + "<br />Phone: " + location.PhoneWorkFormatted.Clean() + " | Fax: " + location.FaxNumberFormatted.Clean()%>",
            "patientname": "<%= Model.Patient.DisplayNameWithMi.Clean() %>",
            "mr": "<%= Model.Patient.PatientIdNumber.Clean() %>",
            "dob": "<%= Model.Patient.DOBFormatted %>",
            "episode": "<%= string.Format("{0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()).Clean() %>",
            "physician": "<%= Model.PhysicianDisplayName.Clean() %>",
            "dnr": "<%= (data.AnswerOrEmptyString("AreHospitalization").Equals("0") ? "No" : string.Empty) + (data.AnswerOrEmptyString("AreHospitalization").Equals("1") ? "Yes, " + data.AnswerOrEmptyString("HospitalizationDate").Clean() : string.Empty) %>",
            "sign": "<%= Model.SignatureText.Clean()%>",
            "signdate": "<%= Model.SignatureDate.Clean()%>"
        };
        PdfPrint.BuildSections(<%= Model.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>