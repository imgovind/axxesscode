﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<head>
    <title><%= Model.Agency.Name %> | <%= Model.TypeName %> | <%= Model.Patient.DisplayName %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<%  string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
<%  string[] patientCondition = data.AnswerArray("PatientCondition"); %>
<%  string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.2.min.js")
        .Add("Print/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean() + "<br />" + location.AddressLine1.Clean() + location.AddressLine2.Clean() + "<br />" + location.AddressCity.ToTitleCase() + ", " + location.AddressStateCode.ToString().ToUpper() + "  " + location.AddressZipCode.Clean() + "<br />Phone: " + location.PhoneWorkFormatted.Clean() + " | Fax: " + location.FaxNumberFormatted.Clean()%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3E" +
            "<%= Model.TypeName %>" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient.DisplayNameWithMi.Clean() %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3ECompletion Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("CompletedDate").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= string.Format("{0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()).Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient.PatientIdNumber.Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.PhysicianDisplayName.Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("PrimaryDiagnosis2").Clean() %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EHomebound Status:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("HomeboundStatus").Clean() %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean() + "<br />" + location.AddressLine1.Clean() + location.AddressLine2.Clean() + "<br />" + location.AddressCity.ToTitleCase() + ", " + location.AddressStateCode.ToString().ToUpper() + "  " + location.AddressZipCode.Clean() + "<br />Phone: " + location.PhoneWorkFormatted.Clean() + " | Fax: " + location.FaxNumberFormatted.Clean()%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3ECoordination of Care/%3Cbr /%3ETransfer Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient.DisplayNameWithMi.Clean() %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "";
        printview.addsection(
            printview.col(3,
                printview.checkbox("Amputation",<%= functionLimitations.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Legally Blind",<%= functionLimitations.Contains("9").ToString().ToLower() %>) +
                printview.checkbox("Endurance",<%= functionLimitations.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Contracture",<%= functionLimitations.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Hearing",<%= functionLimitations.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Paralysis",<%= functionLimitations.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Bowel/Bladder Incontinence",<%= functionLimitations.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Dyspnea with Minimal Exertion",<%= functionLimitations.Contains("A").ToString().ToLower() %>) +
                printview.checkbox("Ambulation",<%= functionLimitations.Contains("7").ToString().ToLower() %>) +
                printview.checkbox("Speech",<%= functionLimitations.Contains("8").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= functionLimitations.Contains("B").ToString().ToLower() %>) +
                printview.span("<%= data.AnswerOrEmptyString("FunctionLimitationsOther").Clean() %>",0,1) ),
            "Functional Limitations");
        printview.addsection(
            printview.col(5,
                printview.checkbox("Stable",<%= patientCondition.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Improved",<%= patientCondition.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Unchanged",<%= patientCondition.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Unstable",<%= patientCondition.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("Declined",<%= patientCondition.Contains("5").ToString().ToLower() %>) ),
            "Patient Condition");
        printview.addsection(
            printview.col(3,
                printview.checkbox("SN",<%= serviceProvided.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("PT",<%= serviceProvided.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("OT",<%= serviceProvided.Contains("3").ToString().ToLower() %>) +
                printview.checkbox("ST",<%= serviceProvided.Contains("4").ToString().ToLower() %>) +
                printview.checkbox("MSW",<%= serviceProvided.Contains("5").ToString().ToLower() %>) +
                printview.checkbox("HHA",<%= serviceProvided.Contains("6").ToString().ToLower() %>) +
                printview.checkbox("Other",<%= serviceProvided.Contains("7").ToString().ToLower() %>) +
                printview.span("<%= data.AnswerOrEmptyString("ServiceProvidedOtherValue").Clean() %>",0,1) ),
            "Service(s) Provided");
        printview.addsection(
            "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3EBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3Cth%3EBG%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3ELowest%3C/th%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignBPMin").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignHRMin").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignRespMin").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignTempMin").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignWeightMin").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignBGMin").Clean() %>" + 
            "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3EHighest%3C/th%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignBPMax").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignHRMax").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignRespMax").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignTempMax").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignWeightMax").Clean() %>" +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            "<%= data.AnswerOrEmptyString("VitalSignBGMax").Clean() %>" +
            "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",
            "Vital Sign Ranges");
        printview.addsection(printview.span("<%= data.AnswerOrEmptyString("SummaryOfCareProvided").Clean() %>",0,10),"Summary of Care Provided by HHA");
        printview.addsection(
            printview.col(6,
                printview.span("Facility:",true) +
                printview.span("<%= data.AnswerOrEmptyString("Facility").Clean() %>",0,1) +
                printview.span("Phone:",true) +
                printview.span("<%= data.AnswerOrEmptyString("Phone").Clean() %>",0,1) +
                printview.span("Contact:",true) +
                printview.span("<%= data.AnswerOrEmptyString("Contact").Clean() %>",0,1)) +
            printview.span("Services Providing:",true) +
            printview.span("<%= data.AnswerOrEmptyString("ServicesProviding").Clean() %>",0,10),
            "Transfer Facility Information");
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model.SignatureText.Clean() %>",0,1) +
                printview.span("<%= Model.SignatureDate.Clean() %>",0,1)));
<%  }).Render(); %>
</body>
</html>