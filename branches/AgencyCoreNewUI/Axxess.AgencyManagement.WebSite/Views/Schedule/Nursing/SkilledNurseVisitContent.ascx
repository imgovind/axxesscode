﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
    <fieldset class="half fl">
        <legend>Vital Signs</legend>
        <% Html.RenderPartial("Nursing/Sections/VitalSigns", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Pain Profile</legend>
        <% Html.RenderPartial("Nursing/Sections/PainProfile", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fr">
        <legend>Skin</legend>
        <% Html.RenderPartial("Nursing/Sections/Skin", Model); %>
    </fieldset>
    <fieldset class="half fl"> 
        <legend>Respiratory</legend>
        <% Html.RenderPartial("Nursing/Sections/Respiratory", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fl"> 
        <legend>Cardiovascular</legend>
        <% Html.RenderPartial("Nursing/Sections/Cardiovascular", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Neurological</legend>
        <% Html.RenderPartial("Nursing/Sections/Neurological", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fl"> 
        <legend>Musculoskeletal</legend>
        <% Html.RenderPartial("Nursing/Sections/Musculoskeletal", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Gastrointestinal</legend>
        <% Html.RenderPartial("Nursing/Sections/Gastrointestinal", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fl"> 
        <legend>Nutrition</legend>
        <% Html.RenderPartial("Nursing/Sections/Nutrition", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Genitourinary</legend>
        <% Html.RenderPartial("Nursing/Sections/Genitourinary", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset> 
        <legend>
            Diabetic Care &#8212;
            <%  string[] isDiabeticCareApplied = data.AnswerArray("GenericIsDiabeticCareApplied"); %>
            <%= Html.Hidden(Model.Type + "_GenericIsDiabeticCareApplied", string.Empty, new { @id = Model.Type + "_GenericIsDiabeticCareAppliedHidden" })%>
            <%= string.Format("<input class='radio' id='{0}_GenericIsDiabeticCareApplied1' name='{0}_GenericIsDiabeticCareApplied' value='1' type='checkbox' {1} />", Model.Type, isDiabeticCareApplied.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericIsDiabeticCareApplied1">N/A</label>
        </legend>
        <% Html.RenderPartial("Nursing/Sections/DiabeticCare", Model); %>
    </fieldset>
    <fieldset>
        <legend>
            IV &#8212;
            <%  string[] isIVApplied = data.AnswerArray("GenericIsIVApplied"); %>
            <%= Html.Hidden(Model.Type + "isIVApplied", string.Empty, new { @id = Model.Type + "_GenericIsIVAppliedHidden" })%>
            <%= string.Format("<input class='radio' id='{0}_GenericIsIVApplied1' name='{0}_GenericIsIVApplied' value='1' type='checkbox' {1} />", Model.Type, isIVApplied.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>isIVApplied1">N/A</label>
        </legend>
        <% Html.RenderPartial("Nursing/Sections/IV", Model); %>
    </fieldset>
    <fieldset class="half fl"> 
        <legend>Infection Control</legend>
        <% Html.RenderPartial("Nursing/Sections/InfectionControl", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Care Coordination</legend>
        <% Html.RenderPartial("Nursing/Sections/CareCoordination", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fl">
        <legend>Response to Teaching/Interventions</legend>
        <% Html.RenderPartial("Nursing/Sections/Response", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Discharge Planning</legend>
        <% Html.RenderPartial("Nursing/Sections/DischargePlanning", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half fl">
        <legend>Home Bound Status</legend>
        <% Html.RenderPartial("Nursing/Sections/HomeBoundStatus", Model); %>
    </fieldset>
    <fieldset class="half fr">
        <legend>Care Plan</legend>
        <% Html.RenderPartial("Nursing/Sections/CarePlan", Model); %>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>    
            Phlebotomy &#8212;
            <%  string[] isPhlebotomyApplied = data.AnswerArray("GenericIsPhlebotomyApplied"); %>
            <%= Html.Hidden(Model.Type + "_GenericIsPhlebotomyApplied", string.Empty, new { @id = Model.Type + "_GenericIsPhlebotomyAppliedHidden" })%>
            <%= string.Format("<input class='radio' id='{0}_GenericIsPhlebotomyApplied1' name='{0}_GenericIsPhlebotomyApplied' value='1' type='checkbox' {1} />", Model.Type, isPhlebotomyApplied.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericIsPhlebotomyApplied1">N/A</label>
        </legend>
        <% Html.RenderPartial("Nursing/Sections/Phlebotomy", Model); %>
    </fieldset>
    <fieldset>
        <legend>Interventions</legend>
        <% Html.RenderPartial("Nursing/Sections/Interventions", Model); %>
    </fieldset>
    <fieldset>
        <legend>Narrative &#38; Teaching</legend>
        <% Html.RenderPartial("Nursing/Sections/Narrative", Model); %>
    </fieldset>