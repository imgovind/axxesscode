﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <%  string[] genericCardioVascular = data.AnswerArray("GenericCardioVascular"); %>
        <%= Html.Hidden(Model.Type + "_GenericCardioVascular", string.Empty, new { @id = Model.Type + "_GenericCardioVascularHidden" })%>
        <div class="wide checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular1' name='{0}_GenericCardioVascular' value='1' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular1" class="strong">WNL (Within Normal Limits)</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular2' name='{0}_GenericCardioVascular' value='2' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular2" class="strong">Heart Rhythm</label>
                <div class="more">
                    <%  var heartRhythm = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Regular/WNL", Value = "1" },
                            new SelectListItem { Text = "Tachycardia", Value = "2" },
                            new SelectListItem { Text = "Bradycardia", Value = "3" },
                            new SelectListItem { Text = "Arrhythmia/ Dysrhythmia", Value = "4" },
                            new SelectListItem { Text = "Other", Value = "5" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericHeartSoundsType", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericHeartSoundsType", heartRhythm, new { @id = Model.Type + "_GenericHeartSoundsType" }) %>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular3' name='{0}_GenericCardioVascular' value='3' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular3" class="strong">Cap. Refill</label>
                <div class="more">
                    <% var CapRefill = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "<3 sec", Value = "1" },
                        new SelectListItem { Text = ">3 sec", Value = "0" }
                    }, "Value", "Text", data.AnswerOrEmptyString("GenericCapRefillLessThan3")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericCapRefillLessThan3", CapRefill, new { @id = Model.Type + "_GenericCapRefillLessThan3" }) %>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular4' name='{0}_GenericCardioVascular' value='4' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular4" class="strong">Pulses</label>
                <div class="more">
                    <div class="row">
                        <label for="GenericCardiovascularRadial" class="fl">Radial</label>
                        <div class="fr radio">
                            <%  var radialPulse = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "1+ Weak", Value = "1" },
                                    new SelectListItem { Text = "2+ Normal", Value = "2" },
                                    new SelectListItem { Text = "3+ Strong", Value = "3" },
                                    new SelectListItem { Text = "4+ Bounding", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularRadial", "0")); %>
                            <%= Html.DropDownList(Model.Type + "_GenericCardiovascularRadial", radialPulse, new { @id = Model.Type + "_GenericCardiovascularRadial" })%>
                            <%= string.Format("<input id='{0}_GenericCardiovascularRadialPosition0' name='{0}_GenericCardiovascularRadialPosition' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("0").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericCardiovascularRadialPosition0" class="fixed short">Bilateral</label>
                            <%= string.Format("<input id='{0}_GenericCardiovascularRadialPosition1' name='{0}_GenericCardiovascularRadialPosition' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("1").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericCardiovascularRadialPosition1" class="fixed shorter">Left</label>
                            <%= string.Format("<input id='{0}_GenericCardiovascularRadialPosition2' name='{0}_GenericCardiovascularRadialPosition' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("2").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericCardiovascularRadialPosition2" class="fixed shorter">Right</label>
                        </div>
                    </div>
                    <div class="row">
                        <label for="GenericCardiovascularPedal" class="fl">Pedal</label>
                        <div class="fr radio">
                            <%  var pedalPulse = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "1+ Weak", Value = "1" },
                                    new SelectListItem { Text = "2+ Normal", Value = "2" },
                                    new SelectListItem { Text = "3+ Strong", Value = "3" },
                                    new SelectListItem { Text = "4+ Bounding", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularPedal", "0")); %>
                            <%= Html.DropDownList(Model.Type + "_GenericCardiovascularPedal", pedalPulse, new { @id = Model.Type + "_GenericCardiovascularPedal" }) %>
                            <%= string.Format("<input id='{0}_GenericCardiovascularPedalPosition0' name='{0}_GenericCardiovascularPedalPosition' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("0").ToChecked())%>
                            <label for="<%= Model.Type %>_GenericCardiovascularPedalPosition0" class="fixed short">Bilateral</label>
                            <%= string.Format("<input id='{0}_GenericCardiovascularPedalPosition1' name='{0}_GenericCardiovascularPedalPosition' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("1").ToChecked())%>
                            <label for="<%= Model.Type %>_GenericCardiovascularPedalPosition1" class="fixed shorter">Left</label>
                            <%= string.Format("<input id='{0}_GenericCardiovascularPedalPosition2' name='{0}_GenericCardiovascularPedalPosition' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("2").ToChecked())%>
                            <label for="<%= Model.Type %>_GenericCardiovascularPedalPosition2" class="fixed shorter">Right</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular5' name='{0}_GenericCardioVascular' value='5' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("5").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular5" class="strong">Edema</label>
                <div class="more">
                    <div class="row">
                        <label for="GenericEdemaLocation" class="fl">Location</label>
                        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericEdemaLocation", data.AnswerOrEmptyString("GenericEdemaLocation"), new { @id = Model.Type + "_GenericEdemaLocation", @maxlength = "50" })%></div>
                    </div>
                    <div class="row">
                        <%  string[] genericPittingEdemaType = data.AnswerArray("GenericPittingEdemaType"); %>
                        <%= Html.Hidden(Model.Type + "_GenericPittingEdemaType", string.Empty, new { @id = Model.Type + "_GenericPittingEdemaTypeHidden" })%>
                        <%= string.Format("<input id='{0}_GenericPittingEdemaType1' name='{0}_GenericPittingEdemaType' value='1' type='checkbox' {1} />", Model.Type, genericPittingEdemaType.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericPittingEdemaType1">Non-Pitting</label>
                        <div id="<%= Model.Type %>_GenericPittingEdemaType1More" class="fr radio">
                            <%  var nonPitting = new SelectList( new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "N/A", Value = "1" },
                                    new SelectListItem { Text = "Mild", Value = "2" },
                                    new SelectListItem { Text = "Moderate", Value = "3" },
                                    new SelectListItem { Text = "Severe", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericEdemaNonPitting", "0")); %>
                            <%= Html.DropDownList(Model.Type + "_GenericEdemaNonPitting", nonPitting, new { @id = Model.Type + "_GenericEdemaNonPitting"}) %>
                            <%= string.Format("<input id='{0}_GenericEdemaNonPittingPosition0' name='{0}_GenericEdemaNonPittingPosition' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("0").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericEdemaNonPittingPosition0" class="fixed short">Bilateral</label>
                            <%= string.Format("<input id='{0}_GenericEdemaNonPittingPosition1' name='{0}_GenericEdemaNonPittingPosition' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("1").ToChecked())%>
                            <label for="<%= Model.Type %>_GenericEdemaNonPittingPosition1" class="fixed shorter">Left</label>
                            <%= string.Format("<input id='{0}_GenericEdemaNonPittingPosition2' name='{0}_GenericEdemaNonPittingPosition' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("2").ToChecked())%>
                            <label for="<%= Model.Type %>_GenericEdemaNonPittingPosition2" class="fixed shorter">Right</label>
                        </div>
                    </div>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericPittingEdemaType2' name='{0}_GenericPittingEdemaType' value='2' type='checkbox' {1} />", Model.Type, genericPittingEdemaType.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericPittingEdemaType2">Pitting</label>
                        <div id="<%= Model.Type %>_GenericPittingEdemaType2More" class="fr">
                            <%  var pitting = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "1+", Value = "1" },
                                    new SelectListItem { Text = "2+", Value = "2" },
                                    new SelectListItem { Text = "3+", Value = "3" },
                                    new SelectListItem { Text = "4+", Value = "4" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericEdemaPitting", "0")); %>
                            <%= Html.DropDownList(Model.Type + "_GenericEdemaPitting", pitting, new { @id = Model.Type + "_GenericEdemaPitting" }) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular6' name='{0}_GenericCardioVascular' value='6' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("6").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular6" class="strong">Heart Sound</label>
                <div class="more">
                    <%  var heartSound = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Distant", Value = "1" },
                            new SelectListItem { Text = "Faint", Value = "2" },
                            new SelectListItem { Text = "Reg", Value = "3" },
                            new SelectListItem { Text = "Murmur", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericHeartSound", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericHeartSound", heartSound, new { @id = Model.Type + "_GenericHeartSound" })%>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCardioVascular7' name='{0}_GenericCardioVascular' value='7' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("7").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCardioVascular7" class="strong">Neck Veins</label>
                <div class="more">
                    <%  var neckVeins = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Flat", Value = "1" },
                            new SelectListItem { Text = "Distended", Value = "2" },
                            new SelectListItem { Text = "Cyanosis", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericNeckVeins", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericNeckVeins", neckVeins, new { @id = Model.Type + "_GenericNeckVeins", @class = "oe" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericCardiovascularComment" class="strong">Comment</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericCardiovascularComment", data.AnswerOrEmptyString("GenericCardiovascularComment"), new { @id = Model.Type + "_GenericCardiovascularComment", @maxcharacters = "500" })%></div>
    </div>
</div>