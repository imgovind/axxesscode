﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wide column">
    <div class="row">
        <%  string[] interventions = data.AnswerArray("GenericInterventions"); %>
        <%= Html.Hidden(Model.Type + "_GenericInterventions", string.Empty, new { @id = Model.Type + "_GenericInterventionsHidden" })%>
        <div class="checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions1' name='{0}_GenericInterventions' value='1' type='checkbox' {1} />", Model.Type, interventions.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions1">Skilled Observation/Assessment</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions2' name='{0}_GenericInterventions' value='2' type='checkbox' {1} />", Model.Type, interventions.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions2">Instructed on Safety Precautions</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions3' name='{0}_GenericInterventions' value='3' type='checkbox' {1} />", Model.Type, interventions.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions3">Medication Adminstration</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions4' name='{0}_GenericInterventions' value='4' type='checkbox' {1} />", Model.Type, interventions.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions4">Diabetic Monitoring/Care</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions5' name='{0}_GenericInterventions' value='5' type='checkbox' {1} />", Model.Type, interventions.Contains("5").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions5">Foley Change</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions6' name='{0}_GenericInterventions' value='6' type='checkbox' {1} />", Model.Type, interventions.Contains("6").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions6">IV Tubing Change</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions7' name='{0}_GenericInterventions' value='7' type='checkbox' {1} />", Model.Type, interventions.Contains("7").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions7">Patient/CG teaching</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions8' name='{0}_GenericInterventions' value='8' type='checkbox' {1} />", Model.Type, interventions.Contains("8").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions8">Instructed on Emergency Preparedness</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions17' name='{0}_GenericInterventions' value='17' type='checkbox' {1} />", Model.Type, interventions.Contains("17").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions17">Wound Care / Dressing Change</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions18' name='{0}_GenericInterventions' value='18' type='checkbox' {1} />", Model.Type, interventions.Contains("18").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions18">IV Site Change</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions19' name='{0}_GenericInterventions' value='19' type='checkbox' {1} />", Model.Type, interventions.Contains("19").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions19">Foley Irrigation</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions9' name='{0}_GenericInterventions' value='9' type='checkbox' {1} />", Model.Type, interventions.Contains("9").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions9">Prep./Admin. Insulin</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions10' name='{0}_GenericInterventions' value='10' type='checkbox' {1} />", Model.Type, interventions.Contains("10").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions10">Administer Enteral nutrition</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions11' name='{0}_GenericInterventions' value='11' type='checkbox' {1} />", Model.Type, interventions.Contains("11").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions11">Glucometer calibration</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions12' name='{0}_GenericInterventions' value='12' type='checkbox' {1} />", Model.Type, interventions.Contains("12").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions12">Trachea care</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions13' name='{0}_GenericInterventions' value='13' type='checkbox' {1} />", Model.Type, interventions.Contains("13").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions13">IV Site Dressing Change</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions14' name='{0}_GenericInterventions' value='14' type='checkbox' {1} />", Model.Type, interventions.Contains("14").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions14">IM Injection / SQ Injection</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions15' name='{0}_GenericInterventions' value='15' type='checkbox' {1} />", Model.Type, interventions.Contains("15").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions15">Peg/GT Tube Site care</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions16' name='{0}_GenericInterventions' value='16' type='checkbox' {1} />", Model.Type, interventions.Contains("16").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions16">Foot care performed</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions20' name='{0}_GenericInterventions' value='20' type='checkbox' {1} />", Model.Type, interventions.Contains("20").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions20">Diet Teaching</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericDietTeaching", data.AnswerOrEmptyString("GenericDietTeaching"), new { @id = Model.Type + "_GenericDietTeaching" })%></div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions21' name='{0}_GenericInterventions' value='21' type='checkbox' {1} />", Model.Type, interventions.Contains("21").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions21">Venipuncture/Lab</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericVenipuncture", data.AnswerOrEmptyString("GenericVenipuncture"), new { @id = Model.Type + "_GenericVenipuncture" })%></div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions22' name='{0}_GenericInterventions' value='22' type='checkbox' {1} />", Model.Type, interventions.Contains("22").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions22">Instructed on Medication</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericInstructedOnMedication", data.AnswerOrEmptyString("GenericInstructedOnMedication"), new { @id = Model.Type + "_GenericInstructedOnMedication" })%></div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericInterventions23' name='{0}_GenericInterventions' value='23' type='checkbox' {1} />", Model.Type, interventions.Contains("23").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericInterventions23">Instructed on Disease Process to include</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericInstructedOnDiseaseProcess", data.AnswerOrEmptyString("GenericInstructedOnDiseaseProcess"), new { @id = Model.Type + "_GenericInstructedOnDiseaseProcess" })%></div>
            </div>
        </div>
    </div>
</div>