<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
    <div class="row">
        <div class="button-with-arrow fr"><%= Model.CarePlanOrEvalUrl %></div>
    </div>
    <%  } %>
    <div class="row">
        <label for="GenericCarePlan" class="fl strong">Care Plan</label>
        <div class="fr">
            <%  var carePlan = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "No changes at this time", Value = "No changes at this time" },
                    new SelectListItem { Text = "Reviewed w/Pt or CG", Value = "Reviewed w/Pt or CG" },
                    new SelectListItem { Text = "Revised w/Pt or CG", Value = "Revised w/Pt or CG" },
                    new SelectListItem { Text = "Other", Value = "Other" }
                }, "Value", "Text", data.AnswerOrDefault("GenericCarePlan", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericCarePlan", carePlan, new { @id = Model.Type + "_GenericCarePlan" }) %>
        </div>
    </div>
    <div class="row">
        <label for="GenericCarePlanProgressTowardsGoals" class="fl strong">Progress Towards Goals</label>
        <div class="fr">
            <%  var progressTowardsGoals = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Good", Value = "Good" },
                    new SelectListItem { Text = "Fair", Value = "Fair" },
                    new SelectListItem { Text = "Poor", Value = "Poor" }
                }, "Value", "Text", data.AnswerOrDefault("GenericCarePlanProgressTowardsGoals", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericCarePlanProgressTowardsGoals", progressTowardsGoals, new { @id = Model.Type + "_GenericCarePlanProgressTowardsGoals" }) %>
        </div>
    </div>
    <div class="row">
        <label class="fl strong">New/Changed Orders or Updates to Care Plan</label>
        <div class="fr radio">
            <%= Html.RadioButton(Model.Type + "_GenericCarePlanIsChanged", "1", data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("1"), new { @id = Model.Type + "_GenericCarePlanIsChanged1" }) %>
            <label for="<%= Model.Type %>_GenericCarePlanIsChanged1" class="fixed">Yes</label>
            <%= Html.RadioButton(Model.Type + "_GenericCarePlanIsChanged", "0", data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("0"), new { @id = Model.Type + "_GenericCarePlanIsChanged0" }) %>
            <label for="<%= Model.Type %>_GenericCarePlanIsChanged0" class="fixed">No</label>
        </div>
    </div>
    <%  if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
    <div class="row">
        <div class="button-with-arrow fr">
            <a class="new-order">Add New Order</a>
        </div>
    </div>
    <%  } %>
    <div class="row">
        <label for="<%= Model.Type %>_GenericCarePlanComment" class="strong">Comments</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericCarePlanComment", data.AnswerOrEmptyString("GenericCarePlanComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericCarePlanComment" })%></div>
    </div>
    <%  if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
    <div class="row">
        <div class="button-with-arrow fr">
            <a class="new-incident-report">Add Incident Report</a>
        </div>
    </div>
    <%  } %>
</div>