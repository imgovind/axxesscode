﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <label for="<%= Model.Type %>_GenericTemp" class="fl strong">Temp</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @class = "shortest", @id = Model.Type + "_GenericTemp" }) %>
            <%  var temp = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Axillary ", Value = "Axillary" },
                    new SelectListItem { Text = "Oral", Value = "Oral" },
                    new SelectListItem { Text = "Tympanic", Value = "Tympanic" },
                    new SelectListItem { Text = "Temporal", Value = "Temporal" }
                }, "Value", "Text", data.AnswerOrDefault("GenericTempType", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericTempType", temp, new { @id = Model.Type + "_GenericTempType", @class = "short" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericResp" class="fl strong">Resp</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @class = "shortest", @id = Model.Type + "_GenericResp" })%>
            <%  var resp = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Regular ", Value = "Regular" },
                    new SelectListItem { Text = "Irregular", Value = "Irregular" }
                }, "Value", "Text", data.AnswerOrDefault("GenericRespType", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericRespType", resp, new { @id = Model.Type + "_GenericRespType", @class = "short" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPulseApical" class="fl strong">Apical Pulse</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericPulseApical", data.AnswerOrEmptyString("GenericPulseApical"), new { @class = "shortest", @id = Model.Type + "_GenericPulseApical" })%>
            <%  var pulseApical = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Regular ", Value = "Regular" },
                    new SelectListItem { Text = "Irregular", Value = "Irregular" }
                }, "Value", "Text", data.AnswerOrDefault("GenericPulseApicalRegular", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericPulseApicalRegular", pulseApical, new { @id = Model.Type + "_GenericPulseApicalRegular", @class = "short" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPulseRadial" class="fl strong">Radial Pulse</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericPulseRadial", data.AnswerOrEmptyString("GenericPulseRadial"), new { @class = "shortest", @id = Model.Type + "_GenericPulseRadial" })%>
            <%  var pulseRadial = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Regular ", Value = "Regular" },
                    new SelectListItem { Text = "Irregular", Value = "Irregular" }
                }, "Value", "Text", data.AnswerOrDefault("GenericPulseRadialRegular", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericPulseRadialRegular", pulseRadial, new { @id = Model.Type + "_GenericPulseRadialRegular", @class = "short" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericBPLeftLying" class="fl strong">Blood Pressure Lying</label>
        <div class="fr">
            <label for="<%= Model.Type %>_GenericBPLeftLying">Left</label>
            <%= Html.TextBox(Model.Type + "_GenericBPLeftLying", data.AnswerOrEmptyString("GenericBPLeftLying"), new { @class = "shortest", @id = Model.Type + "_GenericBPLeftLying" })%>
            <label for="<%= Model.Type %>_GenericBPRightLying">Right</label>
            <%= Html.TextBox(Model.Type + "_GenericBPRightLying", data.AnswerOrEmptyString("GenericBPRightLying"), new { @class = "shortest", @id = Model.Type + "_GenericBPRightLying" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericBPLeftSitting" class="fl strong">Blood Pressure Sitting</label>
        <div class="fr">
            <label for="<%= Model.Type %>_GenericBPLeftSitting">Left</label>
            <%= Html.TextBox(Model.Type + "_GenericBPLeftSitting", data.AnswerOrEmptyString("GenericBPLeftSitting"), new { @class = "shortest", @id = Model.Type + "_GenericBPLeftSitting" })%>
            <label for="<%= Model.Type %>_GenericBPRightSitting">Right</label>
            <%= Html.TextBox(Model.Type + "_GenericBPRightSitting", data.AnswerOrEmptyString("GenericBPRightSitting"), new { @class = "shortest", @id = Model.Type + "_GenericBPRightSitting" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericBPLeftStanding" class="fl strong">Blood Pressure Standing</label>
        <div class="fr">
            <label for="<%= Model.Type %>_GenericBPLeftStanding">Left</label>
            <%= Html.TextBox(Model.Type + "_GenericBPLeftStanding", data.AnswerOrEmptyString("GenericBPLeftStanding"), new { @class = "shortest", @id = Model.Type + "_GenericBPLeftStanding" })%>
            <label for="<%= Model.Type %>_GenericBPRightStanding">Right</label>
            <%= Html.TextBox(Model.Type + "_GenericBPRightStanding", data.AnswerOrEmptyString("GenericBPRightStanding"), new { @class = "shortest", @id = Model.Type + "_GenericBPRightStanding" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericWeight" class="fl strong">Weight</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @class = "shortest", @id = Model.Type + "_GenericWeight" })%>
            <%  var weight = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "N/A ", Value = "N/A" },
                    new SelectListItem { Text = "kg", Value = "kg" },
                    new SelectListItem { Text = "lbs", Value = "lbs" }
                }, "Value", "Text", data.AnswerOrDefault("GenericWeightUnit", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericWeightUnit", weight, new { @id = Model.Type + "_GenericWeightUnit", @class = "short" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPulseOximetry" class="fl strong">Pulse Oximetry</label>
        <div class="fr">
            <%= Html.TextBox(Model.Type + "_GenericPulseOximetry", data.AnswerOrEmptyString("GenericPulseOximetry"), new { @class = "shortest", @id = Model.Type + "_GenericPulseOximetry" })%>
            <%  var pulseOximetry = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "N/A ", Value = "N/A" },
                    new SelectListItem { Text = "On O2", Value = "On O2" },
                    new SelectListItem { Text = "on RA", Value = "on RA" }
                }, "Value", "Text", data.AnswerOrDefault("GenericPulseOximetryUnit", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericPulseOximetryUnit", pulseOximetry, new { @id = Model.Type + "_GenericPulseOximetryUnit", @class = "short" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericVitlaSignComment" class="strong">Comments</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericVitlaSignComment", data.AnswerOrEmptyString("GenericVitlaSignComment"), new { @id = Model.Type + "_GenericVitlaSignComment", @maxcharacters = "500" })%></div>
    </div>
</div>