﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <label for="<%= Model.Type %>_GenericRespiratoryBreathSounds" class="fl strong">Breath Sounds</label>
        <div class="fr radio">
            <%  var breathSounds = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Clear/WNL", Value = "Clear/WNL" },
                    new SelectListItem { Text = "Rales/Crackles", Value = "Rales/Crackles" },
                    new SelectListItem { Text = "Rhonchi/Wheezing", Value = "Rhonchi/Wheezing" },
                    new SelectListItem { Text = "Diminished/Absent", Value = "Diminished/Absent" },
                    new SelectListItem { Text = "Stridor", Value = "Stridor" }
                }, "Value", "Text", data.AnswerOrDefault("GenericRespiratoryBreathSounds", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericRespiratoryBreathSounds", breathSounds, new { @id = Model.Type + "_GenericRespiratoryBreathSounds" }) %>
            <%= string.Format("<input id ='{0}_GenericRespiratoryBreathSoundsPosition0' type='radio' value='0' name='{0}_GenericRespiratoryBreathSoundsPosition' {1} />", Model.Type, data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("0").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition0" class="fixed short">Bilateral</label>
            <%= string.Format("<input id ='{0}_GenericRespiratoryBreathSoundsPosition1' type='radio' value='1' name='{0}_GenericRespiratoryBreathSoundsPosition' {1} />", Model.Type, data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition1" class="fixed shorter">Left</label>
            <%= string.Format("<input id ='{0}_GenericRespiratoryBreathSoundsPosition2' type='radio' value='2' name='{0}_GenericRespiratoryBreathSoundsPosition' {1} />", Model.Type, data.AnswerOrEmptyString("GenericRespiratoryBreathSoundsPosition").Equals("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericRespiratoryBreathSoundsPosition2" class="fixed shorter">Right</label>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericRespiratoryDyspnea" class="fl strong">Dyspnea</label>
        <div class="fr">
            <%  var dyspnea = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "N/A", Value = "N/A" },
                    new SelectListItem { Text = "On Exertion", Value = "On Exertion" },
                    new SelectListItem { Text = "Orthopnea", Value = "Orthopnea" },
                    new SelectListItem { Text = "At Rest", Value = "At Rest" },
                    new SelectListItem { Text = "Other", Value = "Other" }
                }, "Value", "Text", data.AnswerOrDefault("GenericRespiratoryDyspnea", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericRespiratoryDyspnea", dyspnea, new { @id = Model.Type + "_GenericRespiratoryDyspnea" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericCoughList" class="fl strong">Cough</label>
        <div class="fr">
            <%  var cough = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "N/A", Value = "N/A" },
                    new SelectListItem { Text = "Non-Productive", Value = "Non-Productive" },
                    new SelectListItem { Text = "Productive", Value = "Productive" },
                    new SelectListItem { Text = "Other", Value = "Other" }
                }, "Value", "Text", data.AnswerOrDefault("GenericCoughList", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericCoughList", cough, new { @id = Model.Type + "_GenericCoughList" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_Generic02AtText" class="fl strong">O<sub>2</sub> at</label>
        <div class="fr"><%= Html.TextBox(Model.Type + "_Generic02AtText", data.AnswerOrEmptyString("Generic02AtText"), new { @class = "shortest", @id = Model.Type + "_Generic02AtText" })%></div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericLPMVia" class="fl strong">LPM via</label>
        <div class="fr">
            <% var via = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "" },
                new SelectListItem { Text = "Nasal Cannula", Value = "Nasal Cannula" },
                new SelectListItem { Text = "Mask", Value = "Mask" },
                new SelectListItem { Text = "Trach", Value = "Trach" },
                new SelectListItem { Text = "Other", Value = "Other" }
            }, "Value", "Text", data.AnswerOrDefault("GenericLPMVia", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericLPMVia", via, new { @id = Model.Type + "_GenericLPMVia" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericRespiratoryFreq" class="fl strong">Freq</label>
        <div class="fr">
            <%  var respiratoryFreq = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Intermittent", Value = "Intermittent" },
                    new SelectListItem { Text = "Continuous", Value = "Continuous" },
                    new SelectListItem { Text = "PRN", Value = "PRN" }
                }, "Value", "Text", data.AnswerOrDefault("GenericRespiratoryFreq", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericRespiratoryFreq", respiratoryFreq, new { @id = Model.Type + "_GenericRespiratoryFreq" }) %>
        </div>
    </div>
    <div class="row">
        <ul class="wide checkgroup">
            <li>
                <div class="option">
                    <%= string.Format("<input id='{0}_RespiratoryO2Precautions' type='checkbox' value='false' name='{0}_RespiratoryO2Precautions' {1} />", Model.Type, data.AnswerOrEmptyString("RespiratoryO2Precautions").Equals("false").ToChecked()) %>
                    <label for="<%= Model.Type %>_RespiratoryO2Precautions">O<sub>2</sub> Precautions</label>
                </div>
            </li>
        </ul>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericRespiratoryComment" class="strong">Comment</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericRespiratoryComment", data.AnswerOrEmptyString("GenericRespiratoryComment"), new { @id = Model.Type + "_GenericRespiratoryComment", @maxcharacters = "500" })%></div>
    </div>
</div>