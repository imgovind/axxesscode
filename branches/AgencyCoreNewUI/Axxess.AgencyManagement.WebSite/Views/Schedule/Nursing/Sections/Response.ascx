﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <% string[] responseToTeachingInterventions = data.AnswerArray("GenericResponseToTeachingInterventions"); %>
        <%= Html.Hidden(Model.Type + "_GenericResponseToTeachingInterventions", string.Empty)%>
        <div class="wide checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions1' name='{0}_GenericResponseToTeachingInterventions' value='1' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("1").ToChecked()) %>
                <span>
                    <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions1">Verbalize</label>
                    <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsUVI", data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsUVI"), new { @id = Model.Type + "_GenericResponseToTeachingInterventionsUVI" }) %>
                    <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions1">understanding of verbal instructions/teaching given.</label>
                </span>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions2' name='{0}_GenericResponseToTeachingInterventions' value='2' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("2").ToChecked())%>
                <span>
                    <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions2">Able to return</label>
                    <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsCDP", data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsCDP"), new { @id = Model.Type + "_GenericResponseToTeachingInterventionsCDP" }) %>
                    <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions2">correct demonstration of procedure/technique instructed on.</label>
                </span>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions3' name='{0}_GenericResponseToTeachingInterventions' value='3' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions3">Treatment/Procedure well tolerated by patient.</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions4' name='{0}_GenericResponseToTeachingInterventions' value='4' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions4">Infusion well tolerated by patient.</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions5' name='{0}_GenericResponseToTeachingInterventions' value='5' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions.Contains("5").ToChecked())%>
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions5">Other</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsOther", data.AnswerOrEmptyString("GenericResponseToTeachingInterventionsOther"), new { @id = Model.Type + "_GenericResponseToTeachingInterventionsOther" })%></div>
            </div>
        </div>
    </div>
</div>