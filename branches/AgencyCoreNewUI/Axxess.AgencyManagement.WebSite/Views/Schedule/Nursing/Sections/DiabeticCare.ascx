﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_DiabeticCareContainer">
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericBloodSugarLevelText" class="fl strong">Blood Sugar AM</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericBloodSugarAMLevelText", data.AnswerOrEmptyString("GenericBloodSugarAMLevelText"), new { @class = "shortest", @id = Model.Type + "_GenericBloodSugarAMLevelText" })%>
                <label for="<%= Model.Type %>_GenericBloodSugarAMLevelText">mg/dl</label>
                <%  var diabeticCareBsAm = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Random", Value = "Random" },
                    new SelectListItem { Text = "Fasting", Value = "Fasting" }
                }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarAMLevel", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericBloodSugarAMLevel", diabeticCareBsAm, new { @id = Model.Type + "_GenericBloodSugarAMLevel", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBloodSugarLevelText" class="fl strong">Blood Sugar Noon</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericBloodSugarLevelText", data.AnswerOrEmptyString("GenericBloodSugarLevelText"), new { @class = "shortest", @id = Model.Type + "_GenericBloodSugarLevelText" })%>
                <label for="<%= Model.Type %>_GenericBloodSugarLevelText">mg/dl</label>
                <%  var diabeticCareBsNoon = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Random", Value = "Random" },
                    new SelectListItem { Text = "Fasting", Value = "Fasting" }
                }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarLevel", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericBloodSugarLevel", diabeticCareBsNoon, new { @id = Model.Type + "_GenericBloodSugarLevel", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBloodSugarLevelText" class="fl strong">Blood Sugar PM</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericBloodSugarPMLevelText", data.AnswerOrEmptyString("GenericBloodSugarPMLevelText"), new { @class = "shortest", @id = Model.Type + "_GenericBloodSugarPMLevelText" })%>
                <label for="<%= Model.Type %>_GenericBloodSugarPMLevelText">mg/dl</label>
                <%  var diabeticCareBsPm = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Random", Value = "Random" },
                    new SelectListItem { Text = "Fasting", Value = "Fasting" }
                }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarPMLevel", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericBloodSugarPMLevel", diabeticCareBsPm, new { @id = Model.Type + "_GenericBloodSugarPMLevel", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBloodSugarLevelText" class="fl strong">Blood Sugar HS</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericBloodSugarHSLevelText", data.AnswerOrEmptyString("GenericBloodSugarHSLevelText"), new { @class = "shortest", @id = Model.Type + "_GenericBloodSugarHSLevelText" })%>
                <label for="<%= Model.Type %>_GenericBloodSugarHSLevelText">mg/dl</label>
                <%  var diabeticCareBsHs = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Random", Value = "Random" },
                    new SelectListItem { Text = "Fasting", Value = "Fasting" }
                }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarHSLevel", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericBloodSugarHSLevel", diabeticCareBsHs, new { @id = Model.Type + "_GenericBloodSugarHSLevel", @class = "short" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBloodSugarCheckedBy" class="fl strong">Performed by</label>
            <div class="fr">
                <%  var diabeticCarePerformedby = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Patient", Value = "Patient" },
                    new SelectListItem { Text = "SN", Value = "SN" },
                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                }, "Value", "Text", data.AnswerOrDefault("GenericBloodSugarCheckedBy", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericBloodSugarCheckedBy", diabeticCarePerformedby, new { @id = Model.Type + "_GenericBloodSugarCheckedBy" }) %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericBloodSugarSiteText" class="fl strong">Site</label>
            <div class="fr radio">
                <%= Html.TextBox(Model.Type + "_GenericBloodSugarSiteText", data.AnswerOrEmptyString("GenericBloodSugarSiteText"), new { @id = Model.Type + "_GenericBloodSugarSiteText" })%>
                <%= Html.Hidden(Model.Type + "_GenericBloodSugarSite") %>
                <%= string.Format("<input id ='{0}_GenericBloodSugarLevelRandom' type='checkbox' value='Left' name='{0}_GenericBloodSugarSite' {1} />", Model.Type, data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Left").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericBloodSugarLevelRandom" class="fixed">Left</label>
                <%= string.Format("<input id ='{0}_GenericBloodSugarLevelFasting' type='checkbox' value='Right' name='{0}_GenericBloodSugarSite' {1} />", Model.Type, data.AnswerOrEmptyString("GenericBloodSugarSite").Equals("Right").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericBloodSugarLevelFasting" class="fixed">Right</label> 
            </div>
        </div>
        <div class="row">
            <label for="" class="fl strong">Insulin</label>
            <div class="fr">
                <label for="<%= Model.Type %>_GenericInsulinType1" class="fixed shorter">Type</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinType1", data.AnswerOrEmptyString("GenericInsulinType1"), new { @id = Model.Type + "_GenericInsulinType1", @class = "short" })%>
                <label for="<%= Model.Type %>_GenericInsulinDose1" class="fixed shorter">Dose</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinDose1", data.AnswerOrEmptyString("GenericInsulinDose1"), new { @id = Model.Type + "_GenericInsulinDose1", @class = "shorter" })%>
                <br />
                <label for="<%= Model.Type %>_GenericInsulinSite1" class="fixed shorter">Site</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinSite1", data.AnswerOrEmptyString("GenericInsulinSite1"), new { @id = Model.Type + "_GenericInsulinSite1", @class = "short" })%>
                <label for="<%= Model.Type %>_GenericInsulinRoute1" class="fixed shorter">Route</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinRoute1", data.AnswerOrEmptyString("GenericInsulinRoute1"), new { @id = Model.Type + "_GenericInsulinRoute1", @class = "shorter" })%>
            </div>
        </div>
        <div class="row">
            <label for="" class="fl strong">Insulin</label>
            <div class="fr">
                <label for="<%= Model.Type %>_GenericInsulinType2" class="fixed shorter">Type</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinType2", data.AnswerOrEmptyString("GenericInsulinType2"), new { @id = Model.Type + "_GenericInsulinType2", @class = "short" })%>
                <label for="<%= Model.Type %>_GenericInsulinDose2" class="fixed shorter">Dose</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinDose2", data.AnswerOrEmptyString("GenericInsulinDose2"), new { @id = Model.Type + "_GenericInsulinDose2", @class = "shorter" })%>
                <br />
                <label for="<%= Model.Type %>_GenericInsulinSite2" class="fixed shorter">Site</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinSite2", data.AnswerOrEmptyString("GenericInsulinSite2"), new { @id = Model.Type + "_GenericInsulinSite2", @class = "short" })%>
                <label for="<%= Model.Type %>_GenericInsulinRoute2" class="fixed shorter">Route</label>
                <%= Html.TextBox(Model.Type + "_GenericInsulinRoute2", data.AnswerOrEmptyString("GenericInsulinRoute2"), new { @id = Model.Type + "_GenericInsulinRoute2", @class = "shorter" })%>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="strong">Diabetic Management</label>
            <%  string[] diabeticCareDiabeticManagement = data.AnswerArray("GenericDiabeticCareDiabeticManagement"); %>
            <%= Html.Hidden(Model.Type + "_GenericDiabeticCareDiabeticManagement", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareDiabeticManagementHidden" })%>
            <div class="narrow checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement1' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='1' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement1">Diet</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement2' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='2' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement2">Oral Hypoglycemic</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement3' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='3' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement3">Exercise</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareDiabeticManagement4' type='checkbox' name='{0}_GenericDiabeticCareDiabeticManagement' value='4' {1} />", Model.Type, diabeticCareDiabeticManagement.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareDiabeticManagement4">Insulin</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="strong">Insulin Administered by</label>
            <%  string[] diabeticCareInsulinAdministeredby = data.AnswerArray("GenericDiabeticCareInsulinAdministeredby"); %>
            <%= Html.Hidden(Model.Type + "_GenericDiabeticCareInsulinAdministeredby", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareInsulinAdministeredbyHidden" })%>
            <div class="narrow checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby1' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='1' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby1">N/A</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby2' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='2' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby2">Patient</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby3' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='3' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby3">Caregiver</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareInsulinAdministeredby4' type='checkbox' name='{0}_GenericDiabeticCareInsulinAdministeredby' value='4' {1} />", Model.Type, diabeticCareInsulinAdministeredby.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareInsulinAdministeredby4">SN</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="strong">S&#38;S of Hyperglycemia</label>
            <%  string[] genericHyperglycemia = data.AnswerArray("GenericDiabeticCareHyperglycemia"); %>
            <%= Html.Hidden(Model.Type + "_GenericDiabeticCareHyperglycemia", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareHyperglycemiaHidden" })%>
            <div class="narrow checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia1' name='{0}_GenericDiabeticCareHyperglycemia' value='1' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia1">Fatigue</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia2' name='{0}_GenericDiabeticCareHyperglycemia' value='2' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia2">Blurred Vision</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia3' name='{0}_GenericDiabeticCareHyperglycemia' value='3' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia3">Polydipsia</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia4' name='{0}_GenericDiabeticCareHyperglycemia' value='4' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia4">Polyuria</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia5' name='{0}_GenericDiabeticCareHyperglycemia' value='5' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("5").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia5">Polyphagia</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericDiabeticCareHyperglycemia6' name='{0}_GenericDiabeticCareHyperglycemia' value='6' type='checkbox' {1} />", Model.Type, genericHyperglycemia.Contains("6").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDiabeticCareHyperglycemia6">Other</label>
                    <div class="more">
                        <%= Html.TextBox(Model.Type + "_GenericDiabeticCareHyperglycemiaOther", data.AnswerOrEmptyString("GenericDiabeticCareHyperglycemiaOther"), new { @class = "oe", @id = Model.Type + "_GenericDiabeticCareHyperglycemiaOther" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="strong">S&#38;S of Hypoglycemia</label>
            <%  string[] genericHypoglycemia = data.AnswerArray("GenericDiabeticCareHypoglycemia"); %>
            <%= Html.Hidden(Model.Type + "_GenericDiabeticCareHypoglycemia", string.Empty, new { @id = Model.Type + "_GenericDiabeticCareHypoglycemiaHidden" })%>
            <ul class="narrow checkgroup">
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia1' name='{0}_GenericDiabeticCareHypoglycemia' value='1' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia1">Anxious</label>
                    </div>
                </li>
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia2' name='{0}_GenericDiabeticCareHypoglycemia' value='2' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia2">Dizziness</label>
                    </div>
                </li>
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia3' name='{0}_GenericDiabeticCareHypoglycemia' value='3' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia3">Fatigue</label>
                    </div>
                </li>
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia4' name='{0}_GenericDiabeticCareHypoglycemia' value='4' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia4">Perspiration</label>
                    </div>
                </li>
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia5' name='{0}_GenericDiabeticCareHypoglycemia' value='5' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("5").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia5">Weakness</label>
                    </div>
                </li>
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericDiabeticCareHypoglycemia6' name='{0}_GenericDiabeticCareHypoglycemia' value='6' type='checkbox' {1} />", Model.Type, genericHypoglycemia.Contains("6").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDiabeticCareHypoglycemia6">Other</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_GenericDiabeticCareHypoglycemiaOther", data.AnswerOrEmptyString("GenericDiabeticCareHypoglycemiaOther"), new { @id = Model.Type + "_GenericDiabeticCareHypoglycemiaOther", @class = "oe" })%>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="wide column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericDiabeticCareComment" class="strong">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericDiabeticCareComment", data.AnswerOrEmptyString("GenericDiabeticCareComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericDiabeticCareComment" })%></div>
        </div>
    </div>
