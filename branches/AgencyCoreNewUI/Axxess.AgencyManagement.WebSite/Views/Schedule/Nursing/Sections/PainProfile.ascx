﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <label for="<%= Model.Type %>_GenericIntensityOfPain" class="fl strong">Pain Intensity</label>
        <div class="fr">
            <%  var painIntensity = new SelectList(new[] {
                    new SelectListItem { Text = "0 = No Pain", Value = "0" },
                    new SelectListItem { Text = "1", Value = "1" },
                    new SelectListItem { Text = "2", Value = "2" },
                    new SelectListItem { Text = "3", Value = "3" },
                    new SelectListItem { Text = "4", Value = "4" },
                    new SelectListItem { Text = "Moderate Pain", Value = "5" },
                    new SelectListItem { Text = "6", Value = "6" },
                    new SelectListItem { Text = "7", Value = "7" },
                    new SelectListItem { Text = "8", Value = "8" },
                    new SelectListItem { Text = "9", Value = "9" },
                    new SelectListItem { Text = "10", Value = "10" }
                }, "Value", "Text", data.AnswerOrDefault("GenericIntensityOfPain", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericIntensityOfPain", painIntensity, new { @id = Model.Type + "_GenericIntensityOfPain" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericQualityOfPain" class="fl strong">Description</label>
        <div class="fr">
            <%  var painDescription = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Aching", Value = "Aching" },
                    new SelectListItem { Text = "Throbbing", Value = "Throbbing" },
                    new SelectListItem { Text = "Burning", Value = "Burning" },
                    new SelectListItem { Text = "Sharp", Value = "Sharp" },
                    new SelectListItem { Text = "Tender", Value = "Tender" },
                    new SelectListItem { Text = "Other", Value = "Other" }
                }, "Value", "Text", data.AnswerOrDefault("GenericQualityOfPain", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericQualityOfPain", painDescription, new { @id = Model.Type + "_GenericQualityOfPain" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericDurationOfPain" class="fl strong">Duration</label>
        <div class="fr">
            <%  var duration = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Continuous", Value = "Continuous" },
                    new SelectListItem { Text = "Intermittent", Value = "Intermittent" }
                }, "Value", "Text", data.AnswerOrDefault("GenericDurationOfPain", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericDurationOfPain", duration, new { @id = Model.Type + "_GenericDurationOfPain" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericLocationOfPain" class="fl strong">Primary Site</label>
        <div class="fr"><%= Html.TextBox(Model.Type + "_GenericLocationOfPain", data.AnswerOrEmptyString("GenericLocationOfPain"), new { @id = Model.Type + "_GenericLocationOfPain" })%></div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericFrequencyOfPain" class="fl strong">Frequency of Pain Interfering with patient&#8217;s activity or movement</label>
        <div class="fr">
            <%  var frequencyOfPainInterfering = new SelectList(new[] {
                    new SelectListItem { Text = "Patient has no pain", Value = "Patient has no pain" },
                    new SelectListItem { Text = "Patient has pain that does not interfere with activity or movement", Value = "Patient has pain that does not interfere with activity or movement" },
                    new SelectListItem { Text = "Less often than daily", Value = "Less often than daily" },
                    new SelectListItem { Text = "Daily, but not constantly", Value = "Daily, but not constantly" },
                    new SelectListItem { Text = "Patient is in constant pain", Value = "Patient is in constant pain" }
                }, "Value", "Text", data.AnswerOrDefault("GenericFrequencyOfPain", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericFrequencyOfPain", frequencyOfPainInterfering, new { @id = Model.Type + "_GenericFrequencyOfPain" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericMedicationEffectiveness" class="fl strong">Current Pain Management Effectiveness</label>
        <div class="fr">
            <%  var currentPainManagementEffectiveness = new SelectList(new[] {
                    new SelectListItem { Text = "N/A", Value = "N/A" },
                    new SelectListItem { Text = "Effective", Value = "Effective" },
                    new SelectListItem { Text = "Not Effective", Value = "Not Effective" }
                }, "Value", "Text", data.AnswerOrDefault("GenericMedicationEffectiveness", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericMedicationEffectiveness", currentPainManagementEffectiveness, new { @id = Model.Type + "_GenericMedicationEffectiveness" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPainProfileComment" class="strong">Comment</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericPainProfileComment", data.AnswerOrEmptyString("GenericPainProfileComment"), new { @id = Model.Type + "_GenericPainProfileComment", @maxcharacters = "500" })%></div>
    </div>
</div>