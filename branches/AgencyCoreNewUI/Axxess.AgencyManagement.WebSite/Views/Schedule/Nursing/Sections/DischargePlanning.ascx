﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <label class="fl strong">Discharge Planning Discussed</label>
        <div class="fr radio">
            <%= Html.RadioButton(Model.Type + "_GenericDischargePlanningDiscussed", "1", data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("1"), new { @id = Model.Type + "_GenericDischargePlanningDiscussed1" }) %>
            <label for="<%= Model.Type %>_GenericDischargePlanningDiscussed1" class="fixed">Yes</label>
            <%= Html.RadioButton(Model.Type + "_GenericDischargePlanningDiscussed", "0", data.AnswerOrEmptyString("GenericDischargePlanningDiscussed").Equals("0"), new { @id = Model.Type + "_GenericDischargePlanningDiscussed0" }) %>
            <label for="<%= Model.Type %>_GenericDischargePlanningDiscussed0" class="fixed">No</label>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericDischargePlanningDiscussedWith" class="fl strong">Discharge Planning Discussed with</label>
        <div class="fr">
            <%  var dischargePlanningDiscussed = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Patient", Value = "Patient" },
                    new SelectListItem { Text = "Caregiver", Value = "Caregiver" },
                    new SelectListItem { Text = "Physician", Value = "Physician" },
                    new SelectListItem { Text = "Pt/CG", Value = "Pt/CG" },
                    new SelectListItem { Text = "Pt/CG/Physician", Value = "Pt/CG/Physician" },
                    new SelectListItem { Text = "N/A", Value = "N/A" }
                }, "Value", "Text", data.AnswerOrDefault("GenericDischargePlanningDiscussedWith", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericDischargePlanningDiscussedWith", dischargePlanningDiscussed, new { @id = Model.Type + "_GenericDischargePlanningDiscussedWith" }) %>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericDischargeReasonForDischarge" class="fl strong">Reason for Discharge</label>
        <div class="fr">
            <%  var reasonForDischarge = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "Goals Met", Value = "Goals Met" },
                    new SelectListItem { Text = "Transfer", Value = "Transfer" },
                    new SelectListItem { Text = "Deceased", Value = "Deceased" },
                    new SelectListItem { Text = "Other", Value = "Other" }
                }, "Value", "Text", data.AnswerOrDefault("GenericDischargeReasonForDischarge", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericDischargeReasonForDischarge", reasonForDischarge, new { @id = Model.Type + "_GenericDischargeReasonForDischarge" }) %>
        </div>
    </div>
    <div class="row">
        <label class="fl strong">Physician Notified of Discharge</label>
        <div class="fr radio">
            <%= Html.RadioButton(Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge", "1", data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("1"), new { @id = Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge1" }) %>
            <label for="<%= Model.Type %>_GenericDischargePhysicianNotifiedOfDischarge1" class="fixed">Yes</label>
            <%= Html.RadioButton(Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge", "0", data.AnswerOrEmptyString("GenericDischargePhysicianNotifiedOfDischarge").Equals("0"), new { @id = Model.Type + "_GenericDischargePhysicianNotifiedOfDischarge0" }) %>
            <label for="<%= Model.Type %>_GenericDischargePhysicianNotifiedOfDischarge0" class="fixed">No</label>
        </div>
    </div>
    <div class="row">
        <%  string[] pateintReceivedDischarge = data.AnswerArray("GenericPateintReceivedDischarge"); %>
        <%= Html.Hidden(Model.Type + "_GenericPateintReceivedDischarge", string.Empty, new { @id = Model.Type + "_GenericPateintReceivedDischargeHidden" })%>
        <div class="wide checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericPateintReceivedDischarge1' name='{0}_GenericPateintReceivedDischarge' value='1' type='checkbox' {1} />", Model.Type, pateintReceivedDischarge.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericPateintReceivedDischarge1">Patient received discharge notice per agency policy.</label>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericDischargeComment" class="strong">Comments</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericDischargeComment", data.AnswerOrEmptyString("GenericDischargeComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericDischargeComment" })%></div>
    </div>
</div>