﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
    <%  string[] genericGU = data.AnswerArray("GenericGU"); %>
    <%= Html.Hidden(Model.Type + "_GenericGU", string.Empty, new { @id = Model.Type + "_GenericGUHidden" })%>
        <div class="wide checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU1' name='{0}_GenericGU' value='1' type='checkbox' {1} />", Model.Type, genericGU.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU1" class="strong">WNL (Within Normal Limits)</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU2' name='{0}_GenericGU' value='2' type='checkbox' {1} />", Model.Type, genericGU.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU2" class="strong">Incontinence</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU3' name='{0}_GenericGU' value='3' type='checkbox' {1} />", Model.Type, genericGU.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU3" class="strong">Bladder Distention</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU4' name='{0}_GenericGU' value='4' type='checkbox' {1} />", Model.Type, genericGU.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU4" class="strong">Discharge</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU5' name='{0}_GenericGU' value='5' type='checkbox' {1} />", Model.Type, genericGU.Contains("5").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU5" class="strong">Frequency</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU6' name='{0}_GenericGU' value='6' type='checkbox' {1} />", Model.Type, genericGU.Contains("6").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU6" class="strong">Dysuria</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU7' name='{0}_GenericGU' value='7' type='checkbox' {1} />", Model.Type, genericGU.Contains("7").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU7" class="strong">Retention</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU8' name='{0}_GenericGU' value='8' type='checkbox' {1} />", Model.Type, genericGU.Contains("8").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU8" class="strong">Urgency</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU9' name='{0}_GenericGU' value='9' type='checkbox' {1} />", Model.Type, genericGU.Contains("9").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU9" class="strong">Oliguria</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU10' name='{0}_GenericGU' value='10' type='checkbox' {1} />", Model.Type, genericGU.Contains("10").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU10" class="strong">Catheter/Device</label>
                <div class="more">
                    <div class="row">
                        <div class="fr">
                            <%  var genericGUCatheterList = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "" },
                                    new SelectListItem { Text = "N/A", Value = "N/A" },
                                    new SelectListItem { Text = "Foley Catheter ", Value = "Foley Catheter" },
                                    new SelectListItem { Text = "Condom Catheter", Value = "Condom Catheter" },
                                    new SelectListItem { Text = "Suprapubic Catheter", Value = "Suprapubic Catheter" },
                                    new SelectListItem { Text = "Urostomy", Value = "Urostomy" },
                                    new SelectListItem { Text = "Other", Value = "Other" }
                                }, "Value", "Text", data.AnswerOrDefault("GenericGUCatheterList", "0")); %>
                            <%= Html.DropDownList(Model.Type + "_GenericGUCatheterList", genericGUCatheterList, new { @id = Model.Type + "_GenericGUCatheterList" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="<%= Model.Type %>_GenericGUCatheterLastChanged" class="fl">Last Changed</label>
                        <div class="fr">
                            <%= Html.TextBox(Model.Type + "_GenericGUCatheterLastChanged", data.AnswerOrEmptyString("GenericGUCatheterLastChanged"), new { @id = Model.Type + "_GenericGUCatheterLastChanged", @class = "shorter", @maxlength = "10" }) %>
                            <%= Html.TextBox(Model.Type + "_GenericGUCatheterFrequency", data.AnswerOrEmptyString("GenericGUCatheterFrequency"), new { @id = Model.Type + "_GenericGUCatheterFrequency", @class = "shortest", @maxlength = "5" }) %>
                            Fr
                            <%= Html.TextBox(Model.Type + "_GenericGUCatheterAmount", data.AnswerOrEmptyString("GenericGUCatheterAmount"), new { @id = Model.Type + "_GenericGUCatheterAmount", @class = "shortest", @maxlength = "5" }) %>
                            ml
                        </div>
                    </div>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericGU11' name='{0}_GenericGU' value='11' type='checkbox' {1} />", Model.Type, genericGU.Contains("11").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericGU11" class="strong">Urine</label>
                <div class="more">
                    <%  string[] genericGUUrine = data.AnswerArray("GenericGUUrine"); %>
                    <%= Html.Hidden(Model.Type + "_GenericGUUrine", string.Empty, new { @id = Model.Type + "_GenericGUUrineHidden" })%>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericGUUrine6' name='{0}_GenericGUUrine' value='6' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("6").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine6">Clear</label>
                    </div>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericGUUrine1' name='{0}_GenericGUUrine' value='1' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine1">Cloudy</label>
                    </div>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericGUUrine2' name='{0}_GenericGUUrine' value='2' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine2">Odorous</label>
                    </div>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericGUUrine3' name='{0}_GenericGUUrine' value='3' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine3">Sediment</label>
                    </div>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericGUUrine4' name='{0}_GenericGUUrine' value='4' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine4">Hematuria</label>
                    </div>
                    <div class="row">
                        <%= string.Format("<input id='{0}_GenericGUUrine5' name='{0}_GenericGUUrine' value='5' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("5").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine5">Other</label>
                        <div id="<%= Model.Type %>_GenericGUUrine5More" class="fr"><%= Html.TextBox(Model.Type + "_GenericGUOtherText", data.AnswerOrEmptyString("GenericGUOtherText"), new { @id = Model.Type + "_GenericGUOtherText", @maxlength = "20" }) %></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericGenitourinaryComment" class="strong">Comments</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericGenitourinaryComment", data.AnswerOrEmptyString("GenericGenitourinaryComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericGenitourinaryComment" })%></div>
    </div>
</div>