﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_GenericPhlebotomyContainer">
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericPhlebotomyLabs" class="fl strong">Labs</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericPhlebotomyLabs", data.AnswerOrEmptyString("GenericPhlebotomyLabs"), new { @id = Model.Type + "_GenericPhlebotomyLabs" })%></div>
        </div>
        <div class="row">
            <%  string[] phlebotomyActivity = data.AnswerArray("GenericPhlebotomyActivity"); %>
            <%= Html.Hidden(Model.Type + "_GenericPhlebotomyActivity", string.Empty, new { @id = Model.Type + "_GenericPhlebotomyActivityHidden" })%>
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyActivity1' name='{0}_GenericPhlebotomyActivity' value='1' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_phlebotomyActivity1">Pt tolerated procedure</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyActivity1' name='{0}_GenericPhlebotomyActivity' value='2' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyActivity2">No bruising</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyActivity3' name='{0}_GenericPhlebotomyActivity' value='3' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyActivity3">No bleeding</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyActivity4' name='{0}_GenericPhlebotomyActivity' value='4' type='checkbox' {1} />", Model.Type, phlebotomyActivity.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyActivity4">Sharps Disposal</label>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="fl strong">Venipuncture</label>
            <div class="fr">
                <%= Html.TextBox(Model.Type + "_GenericPhlebotomyVenipuncture", data.AnswerOrEmptyString("GenericPhlebotomyVenipuncture"), new { @class = "shortest", @id = Model.Type + "_GenericPhlebotomyVenipuncture" })%>
                (# of attempts) to
                <%= Html.TextBox(Model.Type + "_GenericPhlebotomySite", data.AnswerOrEmptyString("GenericPhlebotomySite"), new { @class = "shorter", @id = Model.Type + "_GenericPhlebotomySite" })%>
                (site) with
                <%= Html.TextBox(Model.Type + "_GenericPhlebotomyGA", data.AnswerOrEmptyString("GenericPhlebotomyGA"), new { @class = "shortest", @id = Model.Type + "_GenericPhlebotomyGA" })%>
                ga. needle/vacutainer using aseptic technique.  Applied pressure for
                <%= Html.TextBox(Model.Type + "_GenericPhlebotomyMinutes", data.AnswerOrEmptyString("GenericPhlebotomyMinutes"), new { @class = "shortest", @id = Model.Type + "_GenericPhlebotomyMinutes" })%>
                minutes.
            </div>
        </div>
        <div class="row">
            <%  string[] phlebotomyDeliveredTo = data.AnswerArray("GenericPhlebotomyDeliveredTo"); %>
            <%= Html.Hidden(Model.Type + "_GenericPhlebotomyDeliveredTo", string.Empty, new { @id = Model.Type + "_GenericPhlebotomyDeliveredToHidden" })%>
            <label class="strong">Delivered to</label>
            <div class="narrow checkgroup">
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo1' name='{0}_GenericPhlebotomyDeliveredTo' value='1' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo1">Lab</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo2' name='{0}_GenericPhlebotomyDeliveredTo' value='2' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo2">Hospital</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo3' name='{0}_GenericPhlebotomyDeliveredTo' value='3' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo3">MD</label>
                </div>
                <div class="option">
                    <%= string.Format("<input id='{0}_GenericPhlebotomyDeliveredTo4' name='{0}_GenericPhlebotomyDeliveredTo' value='4' type='checkbox' {1} />", Model.Type, phlebotomyDeliveredTo.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPhlebotomyDeliveredTo4">Other</label>
                    <div class="more"><%= Html.TextBox(Model.Type + "_GenericPhlebotomyDeliveredToOther", data.AnswerOrEmptyString("GenericPhlebotomyDeliveredToOther"), new { @id = Model.Type + "_GenericPhlebotomyDeliveredToOther" }) %></div>
                </div>
            </div>
        </div>
    </div>
    <div class="wide column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericPhlebotomyComment" class="strong">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericPhlebotomyComment", data.AnswerOrEmptyString("GenericPhlebotomyComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericPhlebotomyComment" })%></div>
        </div>
    </div>
</div>