﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <%  if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
    <div class="row">
        <div class="button-with-arrow fr">
            <a class="infection-report">Add Infection Report</a>
        </div>
    </div>
    <%  } %>
    <%  string[] infectionControl = data.AnswerArray("InfectionControl"); %>
    <%= Html.Hidden(Model.Type + "_InfectionControl", string.Empty, new { @id = Model.Type + "_InfectionControlHidden" })%>
    <div class="row">
        <div class="wide checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_InfectionControl1' name='{0}_InfectionControl' value='1' type='checkbox' {1} />", Model.Type, infectionControl.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_InfectionControl1">Universal Precautions Observed</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_InfectionControl2' name='{0}_InfectionControl' value='2' type='checkbox' {1} />", Model.Type, infectionControl.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_InfectionControl2">Sharps/Waste Disposal</label>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="fl strong">New Infection</label>
        <div class="fr radio">
            <%= Html.RadioButton(Model.Type + "_GenericIsNewInfection", "1", data.AnswerOrEmptyString("GenericIsNewInfection").Equals("1"), new { @id = Model.Type + "_GenericIsNewInfection1" }) %>
            <label for="<%= Model.Type %>_GenericIsNewInfection1" class="fixed">Yes</label>
            <%= Html.RadioButton(Model.Type + "_GenericIsNewInfection", "0", data.AnswerOrEmptyString("GenericIsNewInfection").Equals("0"), new { @id = Model.Type + "_GenericIsNewInfection0" }) %>
            <label for="<%= Model.Type %>_GenericIsNewInfection0" class="fixed">No</label>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericInfectionControlComment" class="strong">Comments</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericInfectionControlComment", data.AnswerOrEmptyString("GenericInfectionControlComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericInfectionControlComment" })%></div>
    </div>
</div>