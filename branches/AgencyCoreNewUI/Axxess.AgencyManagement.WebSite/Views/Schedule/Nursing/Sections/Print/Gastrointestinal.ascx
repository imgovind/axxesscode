﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
<%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
<%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
<%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
printview.checkbox("WNL (Within Normal Limits)",<%= genericDigestive.Contains("1").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Bowel Sounds",<%= genericDigestive.Contains("2").ToString().ToLower() %>) +
    printview.span("<%= (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("1") ? "Present/WNL" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("2") ? "Hyperactive" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("3") ? "Hypoactive" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveBowelSoundsType").Equals("4") ? "Absent" : string.Empty) %>") +
    printview.checkbox("Ab. Palpation",<%= genericDigestive.Contains("3").ToString().ToLower() %>) +
    printview.span("<%= (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("1") ? "Soft/WNL" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("2") ? "Firm" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("3") ? "Tender" : string.Empty) + (data.AnswerOrEmptyString("GenericAbdominalPalpation").Equals("4") ? "Other" : string.Empty) %>") +
    printview.checkbox("Abd Girth",<%= genericDigestive.Contains("8").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength").Clean() %>")) +
printview.checkbox("Bowel Incontinence",<%= genericDigestive.Contains("4").ToString().ToLower() %>) +
printview.col(3,
    printview.checkbox("Nausea",<%= genericDigestive.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Vomiting",<%= genericDigestive.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("GERD",<%= genericDigestive.Contains("7").ToString().ToLower() %>)) +
printview.span("Elimination:",true) +
printview.checkbox("Last BM:",<%= genericDigestive.Contains("11").ToString().ToLower() %>) +
printview.col(2,
    printview.span("Date",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate").Clean() %>")) +
printview.checkbox("WNL",<%= genericDigestiveLastBM.Contains("1").ToString().ToLower() %>) +
printview.checkbox("Abnormal Stool",<%= genericDigestiveLastBM.Contains("2").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Gray",<%= genericDigestiveLastBMAbnormalStool.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Tarry",<%= genericDigestiveLastBMAbnormalStool.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Black",<%= genericDigestiveLastBMAbnormalStool.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Fresh Blood",<%= genericDigestiveLastBMAbnormalStool.Contains("4").ToString().ToLower() %>)) +
printview.checkbox("Constipation",<%= genericDigestiveLastBM.Contains("3").ToString().ToLower() %>) +
printview.col(3,
    printview.checkbox("Chronic",<%= data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Chronic").ToString().ToLower() %>) +
    printview.checkbox("Acute",<%= data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Acute").ToString().ToLower() %>) +
    printview.checkbox("Occasional",<%= data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Occasional").ToString().ToLower() %>)) +
printview.checkbox("Diarrhea:",<%= genericDigestiveLastBM.Contains("4").ToString().ToLower() %>) +
printview.col(3,
    printview.checkbox("Chronic",<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Chronic").ToString().ToLower() %>) +
    printview.checkbox("Acute",<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Acute").ToString().ToLower() %>) +
    printview.checkbox("Occasional",<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Occasional").ToString().ToLower() %>)) +
printview.span("Ostomy:",true) +
printview.col(2,
    printview.checkbox("Ostomy Type:",<%= genericDigestiveOstomy.Contains("1").ToString().ToLower() %>) +
    printview.span("<%= (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("2") ? "Ileostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("3") ? "Colostomy" : string.Empty) + (data.AnswerOrEmptyString("GenericDigestiveOstomyType").Equals("4") ? "Other" : string.Empty) %>") +
    printview.checkbox("Stoma Appearance:",<%= genericDigestiveOstomy.Contains("2").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDigestiveStomaAppearance").Clean() %>") +
    printview.checkbox("Surrounding Skin:",<%= genericDigestiveOstomy.Contains("3").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericDigestiveSurSkinType").Clean() %>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericGastrointestinalComment").Clean() %>"),
"Gastrointestinal"