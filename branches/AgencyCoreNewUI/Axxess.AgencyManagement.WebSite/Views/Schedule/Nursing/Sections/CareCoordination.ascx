﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <div class="button-with-arrow fr">
            <a class="update-med-profile">Update Med Profile</a>
        </div>
    </div>
    <div class="row">
        <%  string[] genericCareCoordination = data.AnswerArray("GenericCareCoordination"); %>
        <%= Html.Hidden(Model.Type + "_GenericCareCoordination", string.Empty, new { @id = Model.Type + "_GenericCareCoordinationHidden" })%>
        <label class="strong">Care Coordination with</label>
        <div class="narrow checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination1' name='{0}_GenericCareCoordination' value='1' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination1">SN</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination2' name='{0}_GenericCareCoordination' value='2' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination2">PT</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination3' name='{0}_GenericCareCoordination' value='3' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination3">ST</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination4' name='{0}_GenericCareCoordination' value='4' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination4">MSW</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination5' name='{0}_GenericCareCoordination' value='5' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("5").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination5">HHA</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination6' name='{0}_GenericCareCoordination' value='6' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("6").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination6">MD</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericCareCoordination7' name='{0}_GenericCareCoordination' value='7' type='checkbox' {1} />", Model.Type, genericCareCoordination.Contains("7").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericCareCoordination7">Other</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericCareCoordinationOther", data.AnswerOrEmptyString("GenericCareCoordinationOther"), new { @id = Model.Type + "_GenericCareCoordinationOther" }) %></div>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericCareCoordinationRegarding" class="strong">Regarding</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericCareCoordinationRegarding", data.AnswerOrEmptyString("GenericCareCoordinationRegarding"), new { @maxcharacters = "500", @id = Model.Type + "_GenericCareCoordinationRegarding" })%></div>
    </div>
</div>