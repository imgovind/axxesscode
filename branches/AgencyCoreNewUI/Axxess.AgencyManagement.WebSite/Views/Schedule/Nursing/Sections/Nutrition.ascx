﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="column">
    <div class="row">
        <%  string[] genericNutrition = data.AnswerArray("GenericNutrition"); %>
        <%= Html.Hidden(Model.Type + "_GenericNutrition", string.Empty, new { @id = Model.Type + "_GenericNutritionHidden" })%>
        <div class="wide checkgroup">
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition1' name='{0}_GenericNutrition' value='1' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition1" class="strong">WNL (Within Normal Limits)</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition2' name='{0}_GenericNutrition' value='2' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("2").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition2" class="strong">Dysphagia</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition3' name='{0}_GenericNutrition' value='3' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("3").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition3" class="strong">Decreased Appetite</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition4' name='{0}_GenericNutrition' value='4' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("4").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition4" class="strong">Weight</label>
                <div class="more">
                    <div class="row">
                        <div class="fr radio">
                            <%= string.Format("<input id ='{0}_GenericNutritionWeightLoss' type='checkbox' value='Loss' name='{0}_GenericNutritionWeightGainLoss' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionWeightLoss" class="fixed">Loss</label>
                            <%= string.Format("<input id ='{0}_GenericNutritionWeightGain' type='checkbox' value='Gain' name='{0}_GenericNutritionWeightGainLoss' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionWeightGain" class="fixed">Gain</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition5' name='{0}_GenericNutrition' value='5' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("5").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition5" class="strong">Diet</label>
                <div class="more">
                    <div class="row">
                        <div class="fr radio">
                            <%= string.Format("<input id='{0}_GenericNutritionDietAdequate' type='checkbox' value='Adequate' name='{0}_GenericNutritionDietAdequate' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionDietAdequate" class="fixed">Adequate</label>
                            <%= string.Format("<input id='{0}_GenericNutritionDietInadequate' type='checkbox' value='Inadequate' name='{0}_GenericNutritionDietAdequate' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionDietInadequate" class="fixed">Inadequate</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition9' name='{0}_GenericNutrition' value='9' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("9").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition9" class="strong">Diet Type</label>
                <div class="more"><%= Html.TextBox(Model.Type + "_GenericNutritionDietType", data.AnswerOrEmptyString("GenericNutritionDietType"), new { @id = Model.Type + "_GenericNutritionDietType" })%></div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition6' name='{0}_GenericNutrition' value='6' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("6").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition6" class="strong">Enteral Feeding</label>
                <div class="more">
                    <div class="row">
                        <%  string[] genericNutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
                        <%= Html.Hidden(Model.Type + "_GenericNutritionEnteralFeeding", string.Empty, new { @id = Model.Type + "_GenericNutritionEnteralFeedingHidden" })%>
                        <div class="fr radio">
                            <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding1' name='{0}_GenericNutritionEnteralFeeding' value='1' type='checkbox' {1} />", Model.Type, genericNutritionEnteralFeeding.Contains("1").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionEnteralFeeding1" class="fixed shorter">NG</label>
                            <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding2' name='{0}_GenericNutritionEnteralFeeding' value='2' type='checkbox' {1} />", Model.Type, genericNutritionEnteralFeeding.Contains("2").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionEnteralFeeding2" class="fixed shorter">PEG</label>
                            <%= string.Format("<input id='{0}_GenericNutritionEnteralFeeding3' name='{0}_GenericNutritionEnteralFeeding' value='3' type='checkbox' {1} />", Model.Type, genericNutritionEnteralFeeding.Contains("3").ToChecked()) %>
                            <label for="<%= Model.Type %>_GenericNutritionEnteralFeeding3" class="fixed short">Dobhoff</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition7' name='{0}_GenericNutrition' value='7' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("7").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition7" class="strong">Tube Placement Checked</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_GenericNutrition8' name='{0}_GenericNutrition' value='8' type='checkbox' {1} />", Model.Type, genericNutrition.Contains("8").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericNutrition8" class="strong">Residual Checked</label>
                <div class="more">
                    <div class="row">
                        <div class="fr">
                            <label for="<%= Model.Type %>_GenericNutritionResidualCheckedAmount">Amount</label>
                            <%= Html.TextBox(Model.Type + "_GenericNutritionResidualCheckedAmount", data.AnswerOrEmptyString("GenericNutritionResidualCheckedAmount"), new { @id = Model.Type + "_GenericNutritionResidualCheckedAmount", @class = "shortest" }) %>
                            ml
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericNutritionComment" class="strong">Comments</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericNutritionComment", data.AnswerOrEmptyString("GenericNutritionComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericNutritionComment" })%></div>
    </div>
</div>