﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_IVContainer">
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericIVAccess" class="fl strong">IV Access</label>
            <div class="fr">
                <%  var IVAccess = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "Saline Lock", Value = "Saline Lock" },
                        new SelectListItem { Text = "PICC Line", Value = "PICC Line" },
                        new SelectListItem { Text = "Central Line", Value = "Central Line" },
                        new SelectListItem { Text = "Port-A-Cath", Value = "Port-A-Cath" },
                        new SelectListItem { Text = "Med-A-Port", Value = "Med-A-Port" },
                        new SelectListItem { Text = "Other", Value = "Other" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericIVAccess", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericIVAccess", IVAccess, new { @id = Model.Type + "_GenericIVAccess" }) %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericIVLocation" class="fl strong">IV Location</label>
            <div class="fr"><%= Html.TextBox(Model.Type + "_GenericIVLocation", data.AnswerOrEmptyString("GenericIVLocation"), new { @id = Model.Type + "_GenericIVLocation" }) %></div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericIVConditionOfIVSite" class="fl strong">Condition of IV Site</label>
            <div class="fr">
                <%  var IVConditionOfIVSite = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "WNL", Value = "WNL" },
                        new SelectListItem { Text = "Phlebitis", Value = "Phlebitis" },
                        new SelectListItem { Text = "Redness", Value = "Redness" },
                        new SelectListItem { Text = "Swelling", Value = "Swelling" },
                        new SelectListItem { Text = "Pallor ", Value = "Pallor" },
                        new SelectListItem { Text = "Warmth", Value = "Warmth" },
                        new SelectListItem { Text = "Bleeding", Value = "Bleeding" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfIVSite", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericIVConditionOfIVSite", IVConditionOfIVSite, new { @id = Model.Type + "_GenericIVConditionOfIVSite" }) %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericIVConditionOfDressing" class="fl strong">Condition of Dressing</label>
            <div class="fr">
                <%  var IVConditionOfDressing = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "Dry & Intact/WNL", Value = "Dry & Intact/WNL" },
                        new SelectListItem { Text = "Bloody", Value = "Bloody" },
                        new SelectListItem { Text = "Soiled", Value = "Soiled" },
                        new SelectListItem { Text = "Other", Value = "Other" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfDressing", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericIVConditionOfDressing", IVConditionOfDressing, new { @id = Model.Type + "_GenericIVConditionOfDressing" }) %>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericIVSiteDressing" class="fl strong">IV site Dressing Changed performed on this visit</label>
            <div class="fr">
                <%  var IVSiteDressing = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "N/A", Value = "N/A" },
                        new SelectListItem { Text = "Yes", Value = "Yes" },
                        new SelectListItem { Text = "No", Value = "No" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressing", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericIVSiteDressing", IVSiteDressing, new { @id = Model.Type + "_GenericIVSiteDressing" }) %>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.Type %>_GenericFlush" class="fl strong">Flush</label>
            <div class="fr radio">
                <%= Html.RadioButton(Model.Type + "_GenericFlush", "1", data.AnswerOrEmptyString("GenericFlush").Equals("1"), new { @id = Model.Type + "_GenericFlush1" }) %>
                <label for="<%= Model.Type %>_GenericFlush1" class="fixed">Yes</label>
                <%= Html.RadioButton(Model.Type + "_GenericFlush", "0", data.AnswerOrEmptyString("GenericFlush").Equals("0"), new { @id = Model.Type + "_GenericFlush0" }) %>
                <label for="<%= Model.Type %>_GenericFlush0" class="fixed">No</label>
            </div>
            <div class="clear"></div>
            <div id="<%= Model.Type %>_GenericFlush1More" class="fr">
                <label for="<%= Model.Type %>_GenericIVAccessFlushed">Flushed with</label>
                <%= Html.TextBox(Model.Type + "_GenericIVAccessFlushed", data.AnswerOrEmptyString("GenericIVAccessFlushed"), new { @class = "shortest", @id = Model.Type + "_GenericIVAccessFlushed" })%>
                <label for="<%= Model.Type %>_GenericIVSiteDressingUnit">/ml of</label>
                <%  var IVSiteDressingUnit = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "Normal Saline", Value = "Normal Saline" },
                        new SelectListItem { Text = "Heparin", Value = "Heparin" },
                        new SelectListItem { Text = "Other", Value = "Other" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressingUnit", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericIVSiteDressingUnit", IVSiteDressingUnit, new { @id = Model.Type + "_GenericIVSiteDressingUnit", @class = "short" }) %>
            </div>
        </div>
    </div>
    <div class="wide column">
        <div class="row">
            <label for="<%= Model.Type %>_GenericIVComment" class="strong">Comments</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GenericIVComment", data.AnswerOrEmptyString("GenericIVComment"), new { @maxcharacters = "500", @id = Model.Type + "_GenericIVComment" }) %></div>
        </div>
    </div>
</div>