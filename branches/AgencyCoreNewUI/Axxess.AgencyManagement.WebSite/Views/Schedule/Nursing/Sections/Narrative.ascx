﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wide column">
<%  if (Model.Type == "SkilledNurseVisit") { %>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
    <div class="narrow row">
        <div class="button-with-arrow fr"><%= Model.CarePlanOrEvalUrl %></div>
    </div>
    <%  } %>
    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
    <div class="narrower row">
        <label class="fl strong">Primary Diagnosis</label>
        <div class="fr">
            <%= data.AnswerOrEmptyString("PrimaryDiagnosis") %>
            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M").Trim() %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
        </div>
    </div>
    <%  } %>
    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
    <div class="narrower row">
        <label class="fl strong">Secondary Diagnosis</label>
        <div class="fr">
            <%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%>
            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1").Trim() %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
        </div>
    </div>
    <%  } %>
<%  } %>
    <div class="row">
        <label class="strong">Narrative</label>
        <div class="ac">
            <%= Html.Templates(Model.Type + "_GenericNarrativeCommentTemplates") %>
            <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.AnswerOrEmptyString("GenericNarrativeComment"), 8, 20, new { @class = "tallest", @id = Model.Type + "_GenericNarrativeComment", @maxcharacters = "1500" })%>
        </div>
    </div>
</div>
<%  if (Model.Type == "SNDiabeticDailyVisit") { %>
<div class="column">
    <div class="row">
        <label class="fl strong">Tolerated Cares</label>
        <div class="fr">
            <%= Html.RadioButton(Model.Type + "_ToleratedCares", "1", data.AnswerOrEmptyString("ToleratedCares").Equals("1"), new { @id = Model.Type + "_ToleratedCares1" })%>
            <label for="<%= Model.Type %>_ToleratedCares1" class="">Yes</label>
            <%= Html.RadioButton(Model.Type + "_ToleratedCares", "0", data.AnswerOrEmptyString("ToleratedCares").Equals("0"), new { @id = Model.Type + "_ToleratedCares0" })%>
            <label for="<%= Model.Type %>_ToleratedCares0" class="">No</label>
        </div>
        <div class="clear"></div>
        <div id="<%= Model.Type %>_ToleratedCares1More">
            <label for="<%= Model.Type %>_ToleratedCaresDescribe">Describe</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_ToleratedCaresDescribe", data.AnswerOrEmptyString("ToleratedCaresDescribe"), 3, 30, new { @id = Model.Type + "_ToleratedCaresDescribe", @maxcharacters = "500" })%></div>
        </div>
    </div>
    <div class="row">
        <label class="fl strong">Patient Goals Met this Visit</label>
        <div class="fr">
            <%= Html.RadioButton(Model.Type + "_GoalsMet", "1", data.AnswerOrEmptyString("GoalsMet").Equals("1"), new { @id = Model.Type + "_GoalsMet1" })%>
            <label for="<%= Model.Type %>_GoalsMet1" class="">Yes</label>
            <%= Html.RadioButton(Model.Type + "_GoalsMet", "0", data.AnswerOrEmptyString("GoalsMet").Equals("0"), new { @id = Model.Type + "_GoalsMet0" })%>
            <label for="<%= Model.Type %>_GoalsMet0" class="">No</label>
        </div>
        <div class="clear"></div>
        <div id="<%= Model.Type %>_GoalsMet1More">
            <label for="<%= Model.Type %>_GoalsMetSpecify">Specify</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_GoalsMetSpecify", data.AnswerOrEmptyString("GoalsMetSpecify"), 3, 30, new { @id = Model.Type + "_GoalsMetSpecify", @maxcharacters = "500" })%></div>
        </div>
    </div>
</div>
<div class="column">
    <div class="row">
        <label class="fl strong">Care Plan Revised</label>
        <div class="fr">
            <%= Html.RadioButton(Model.Type + "_RevisedCarePlan", "1", data.AnswerOrEmptyString("RevisedCarePlan").Equals("1"), new { @id = Model.Type + "_RevisedCarePlan1" })%>
            <label for="<%= Model.Type %>_RevisedCarePlan1" class="">Yes</label>
            <%= Html.RadioButton(Model.Type + "_RevisedCarePlan", "0", data.AnswerOrEmptyString("RevisedCarePlan").Equals("0"), new { @id = Model.Type + "_RevisedCarePlan0" })%>
            <label for="<%= Model.Type %>_RevisedCarePlan0" class="">No</label>
        </div>
        <div class="clear"></div>
        <div id="<%= Model.Type %>_RevisedCarePlan1More">
            <label for="<%= Model.Type %>_RevisedCarePlanSpecify">Specify</label>
            <div class="ac"><%= Html.TextArea(Model.Type + "_RevisedCarePlanSpecify", data.AnswerOrEmptyString("RevisedCarePlanSpecify"), 3, 30, new { @id = Model.Type + "_RevisedCarePlanSpecify", @maxcharacters = "500" })%></div>
        </div>
    </div>
    <div class="row">
        <label for="<%= Model.Type %>_GenericPatientResponse" class="strong">Patient Response</label>
        <div class="ac"><%= Html.TextArea(Model.Type + "_GenericPatientResponse", data.AnswerOrEmptyString("GenericPatientResponse"), 3, 20, new { @maxcharacters = "500", @id = Model.Type + "_GenericPatientResponse" })%></div>
    </div>
</div>
<%  } %>