﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label for="" class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_VisitDate" class="fl strong">Visit Date</label>
                <div class="fr"><input type="text" id ="Text1" class="date-picker required" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString()%>" mindate="<%= Model.StartDate.ToShortDateString()%>"/></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeIn" class="fl strong">Time In</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = Model.Type + "_TimeIn", @class = "complete-required time-picker" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_TimeOut" class="fl strong">Time Out</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = Model.Type + "_TimeOut", @class = "complete-required time-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_HomeboundStatus" class="fl strong">Homebound Status</label>
                <div class="fr">
                    <%  var homeboundStatus = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "N/A", Value = "N/A" },
                            new SelectListItem { Text = "Exhibits considerable & taxing effort to leave home", Value = "Exhibits considerable & taxing effort to leave home" },
                            new SelectListItem { Text = "Requires the assistance of another to get up and moving safely", Value = "Requires the assistance of another to get up and moving safely" },
                            new SelectListItem { Text = "Severe Dyspnea", Value = "Severe Dyspnea" },
                            new SelectListItem { Text = "Unable to safely leave home unassisted", Value = "Unable to safely leave home unassisted" },
                            new SelectListItem { Text = "Unsafe to leave home due to cognitive or psychiatric impairments", Value = "Unsafe to leave home due to cognitive or psychiatric impairments" },
                            new SelectListItem { Text = "Unable to leave home due to medical restriction(s)", Value = "Unable to leave home due to medical restriction(s)" },
                            new SelectListItem { Text = "Other", Value = "Other" }
                        }, "Value", "Text", data.AnswerOrEmptyString("HomeboundStatus")); %>
                    <%= Html.DropDownList(Model.Type + "_HomeboundStatus", homeboundStatus, new { @id = Model.Type + "_HomeboundStatus" })%>
                    <%= Html.TextBox(Model.Type + "_HomeboundStatusOther", data.AnswerOrEmptyString("HomeboundStatusOther"), new { @id = Model.Type + "_HomeboundStatusOther", @style = "display:none" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl strong">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl strong">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
        </div>
        <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
        <div class="column wide">
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li><%= Model.CarePlanOrEvalUrl%></li>
                    </ul>
                </div>
            </div>
        </div>
        <%  } %>
    </fieldset>
    <fieldset>
        <legend>Functional Limitations</legend>
        <div class="column wide">
            <div class="row">
                <%  string[] functionLimitations = data.AnswerArray("FunctionLimitations"); %>
                <input name="<%= Model.Type %>_FunctionLimitations" value=" " type="hidden" />
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations1' name='{1}_FunctionLimitations' value='1' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations1">Amputation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations5' name='{1}_FunctionLimitations' value='5' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations5">Paralysis</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations9' name='{1}_FunctionLimitations' value='9' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("9").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations9">Legally Blind</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations2' name='{1}_FunctionLimitations' value='2' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations2">Bowel/Bladder Incontinence</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations6' name='{1}_FunctionLimitations' value='6' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations6">Endurance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitationsA' name='{1}_FunctionLimitations' value='A' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("A").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitationsA">Dyspnea with Minimal Exertion</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations3' name='{1}_FunctionLimitations' value='3' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations3">Contracture</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations7' name='{1}_FunctionLimitations' value='7' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations7">Ambulation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations4' name='{1}_FunctionLimitations' value='4' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations4">Hearing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitations8' name='{1}_FunctionLimitations' value='8' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("8").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitations8">Speech</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_FunctionLimitationsB' name='{1}_FunctionLimitations' value='B' class='radio float-left' type='checkbox' {0} />", functionLimitations.Contains("B").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_FunctionLimitationsB">Other (Specify)</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_FunctionLimitationsOther", data.AnswerOrEmptyString("FunctionLimitationsOther"), new { @id = Model.Type + "_FunctionLimitationsOther", @class = "oe" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Patient Condition</legend>
        <div class="column">
            <div class="row">
                <% string[] patientCondition = data.AnswerArray("PatientCondition"); %>
                <input name="<%= Model.Type %>_PatientCondition" value=" " type="hidden" />
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input id='{1}_PatientConditionStable' name='{1}_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_PatientConditionStable">Stable</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_PatientConditionImproved' name='{1}_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_PatientConditionImproved">Improved</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_PatientConditionUnchanged' name='{1}_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_PatientConditionUnchanged">Unchanged</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_PatientConditionUnstable' name='{1}_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_PatientConditionUnstable">Unstable</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_PatientConditionDeclined' name='{1}_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_PatientConditionDeclined">Declined</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Service(s) Provided</legend>
        <div class="column">
            <div class="row">
                <% string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
                <input name="<%= Model.Type %>_ServiceProvided" value="" type="hidden" />
                <div class="checkgroup narrow">
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedSN' name='{1}_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("1").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedSN">SN</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedPT' name='{1}_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("2").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedPT">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedOT' name='{1}_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("3").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedOT">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedST' name='{1}_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("4").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedST">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedMSW' name='{1}_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("5").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedMSW">MSW</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedHHA' name='{1}_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("6").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedHHA">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{1}_ServiceProvidedOther' name='{1}_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided.Contains("7").ToChecked(), Model.Type)%>
                        <label for="<%= Model.Type %>_ServiceProvidedOther">Other</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_ServiceProvidedOtherValue", data.AnswerOrEmptyString("ServiceProvidedOtherValue"), new { @id = Model.Type + "_ServiceProvidedOtherValue", @class = "oe" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset>
        <legend>Vital Sign Ranges</legend>
        <div class="column wide">
            <table class="fixed ac">
                <thead>
                    <tr>
                        <th></th>
                        <th>BP</th>
                        <th>HR</th>
                        <th>Resp</th>
                        <th>Temp</th>
                        <th>Weight</th>
                        <th>BG</th>
                    </tr>
                    <tr>
                        <th>Lowest</th>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBPMin", data.AnswerOrEmptyString("VitalSignBPMin"), new { @id = Model.Type + "_VitalSignBPMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignHRMin", data.AnswerOrEmptyString("VitalSignHRMin"), new { @id = Model.Type + "_VitalSignHRMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignRespMin", data.AnswerOrEmptyString("VitalSignRespMin"), new { @id = Model.Type + "_VitalSignRespMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignTempMin", data.AnswerOrEmptyString("VitalSignTempMin"), new { @id = Model.Type + "_VitalSignTempMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMin", data.AnswerOrEmptyString("VitalSignWeightMin"), new { @id = Model.Type + "_VitalSignWeightMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBGMin", data.AnswerOrEmptyString("VitalSignBGMin"), new { @id = Model.Type + "_VitalSignBGMin", @class = "fill" })%></td>
                    </tr>
                    <tr>
                        <th>Highest</th>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBPMax", data.AnswerOrEmptyString("VitalSignBPMax"), new { @id = Model.Type + "_VitalSignBPMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignHRMax", data.AnswerOrEmptyString("VitalSignHRMax"), new { @id = Model.Type + "_VitalSignHRMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignRespMax", data.AnswerOrEmptyString("VitalSignRespMax"), new { @id = Model.Type + "_VitalSignRespMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignTempMax", data.AnswerOrEmptyString("VitalSignTempMax"), new { @id = Model.Type + "_VitalSignTempMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMax", data.AnswerOrEmptyString("VitalSignWeightMax"), new { @id = Model.Type + "_VitalSignWeightMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBGMax", data.AnswerOrEmptyString("VitalSignBGMax"), new { @id = Model.Type + "_VitalSignBGMax", @class = "fill" })%></td>
                    </tr>
                </thead>
            </table>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Summary of Care Provided by HHA</legend>
        <div class="column">
            <div class="row">
                <%= Html.TextArea(Model.Type + "_SummaryOfCareProvided", data.AnswerOrEmptyString("SummaryOfCareProvided"), new { @class = "fill tall", @id = Model.Type + "_SummaryOfCareProvided" })%>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Transfer Facility Information</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_Facility" class="float-left">Facility</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Facility", data.AnswerOrEmptyString("Facility"), new { @id = Model.Type + "_Facility" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_Phone1" class="float-left">Phone</label>
                <div class="fr">
                    <%= Html.TextBox(Model.Type + "_Phone1", data.AnswerOrEmptyString("Phone1"), new { @id = Model.Type + "_Phone1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox(Model.Type + "_Phone2", data.AnswerOrEmptyString("Phone2"), new { @id = Model.Type + "_Phone2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox(Model.Type + "_Phone3", data.AnswerOrEmptyString("Phone3"), new { @id = Model.Type + "_Phone3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_Contact" class="float-left">Contact</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Contact", data.AnswerOrEmptyString("Contact"), new { @id = Model.Type + "_Contact" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Services Providing</label>
                <div class="fr"><%= Html.TextArea(Model.Type + "_ServicesProviding", data.AnswerOrEmptyString("ServicesProviding"), new { @class = "fill", @id = Model.Type + "_ServicesProviding" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_Clinician" class="fl strong">Clinician</label>
                <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician", @class = "complete-required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_SignatureDate" class="fl strong">Date</label>
                <div class="fr"><input type="text" class="date-picker complete-required" name="<%= Model.Type %>_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() ? data["SignatureDate"].Answer : "" %>" id="<%= Model.Type %>_SignatureDate" /></div>
            </div>
        </div>
        <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
        <div class="column wide">
            <div class="row narrowest">
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
            </div>
        </div>
        <%  } %>
    </fieldset>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0)" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>