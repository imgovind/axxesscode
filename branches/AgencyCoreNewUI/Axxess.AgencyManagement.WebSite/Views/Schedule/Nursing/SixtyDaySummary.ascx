﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle">60 Day Summary/Case Conference | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "85")%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>
            Details
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <a class="sticky-note-icon red" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
        </legend>
        <div class="column">
            <div class="row">
                <label for="" class="fl strong">Patient</label>
                <div class="fr"><%= Model.Patient.DisplayNameWithMi %> (<%= Model.Patient.PatientIdNumber %>)</div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_EpsPeriod" class="strong fl">Episode Period</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_EpsPeriod", Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString(), new { @id = Model.Type + "_EpsPeriod", @readonly = "readonly" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_Physician" class="strong fl">Physician</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_Physician", data.AnswerOrEmptyString("Physician"), new { @id = Model.Type + "_Physician", @class = "physician-picker" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.Type %>_DNR" class="fl strong">DNR</label>
                <div class="fr radio">
                    <%= Html.RadioButton(Model.Type + "_DNR", "1", data.AnswerOrEmptyString("DNR").Equals("1"), new { @id = Model.Type + "_DNR1" })%>
                    <label for="<%= Model.Type %>_DNR1" class="fixed short">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_DNR", "0", data.AnswerOrEmptyString("DNR").Equals("0"), new { @id = Model.Type + "_DNR2" })%>
                    <label for="<%= Model.Type %>_DNR2" class="fixed short">No</label>
                </div>
            </div>
            <div class="row">
                <div class="fr">
                    <label for="<%= Model.Type %>_NotificationDate" class="float-left">If Yes</label>
                    <%  var patientReceived = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "5 day", Value = "1" },
                            new SelectListItem { Text = "2 day", Value = "2" },
                            new SelectListItem { Text = "Other", Value = "3" }
                        }, "Value", "Text", data.AnswerOrDefault("NotificationDate", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_NotificationDate", patientReceived, new { @id = Model.Type + "_NotificationDate" })%>
                    <%= Html.TextBox(Model.Type + "_NotificationDateOther", data.AnswerOrEmptyString("NotificationDateOther"), new { @id = Model.Type + "_NotificationDateOther" })%>
                </div>
            </div>
        <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis" class="fl strong">Primary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="fl strong">Secondary Diagnosis</label>
                <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                <div class="fr">
                    <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1") %></span>
                    <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                     <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                    <%  } %>
                </div>
            </div>
        </div>
        <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
        <div class="column wide">
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li><%= Model.CarePlanOrEvalUrl%></li>
                    </ul>
                </div>
            </div>
        </div>
        <%  } %>
    </fieldset>
    <fieldset>
        <legend>Homebound Status</legend>
        <div class="column wide">
            <div class="row">
                <div class="checkgroup">
                    <%  string[] homeboundStatus = data.AnswerArray("HomeboundStatus"); %>
                    <input name="<%= Model.Type %>_HomeboundStatus" value=" " type="hidden" />
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusLeave' name='{0}_HomeboundStatus' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusLeave" class="radio normal">Exhibits considerable &#38; taxing effort to leave home</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusAssistRequired' name='{0}_HomeboundStatus' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusAssistRequired" class="radio normal">Requires the assistance of another to get up and move safely</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusDyspnea' name='{0}_HomeboundStatus' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusDyspnea" class="radio normal">Severe Dyspnea</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusUnableToLeave' name='{0}_HomeboundStatus' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusUnableToLeave" class="radio normal">Unable to safely leave home unassisted</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusUnsafeToLeave' name='{0}_HomeboundStatus' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusUnsafeToLeave" class="radio normal">Unsafe to leave home due to cognitive or psychiatric impairments</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusMedReason' name='{0}_HomeboundStatus' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusMedReason" class="radio normal">Unable to leave home due to medical restriction(s)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_HomeboundStatusOtherCheck' name='{0}_HomeboundStatus' value='8' class='radio float-left' type='checkbox' {1} />", Model.Type, homeboundStatus.Contains("8").ToChecked())%>
                        <label for="<%= Model.Type %>_HomeboundStatusOtherCheck" class="radio normal">Other</label>
                       <div class="more">
                            <%= Html.TextBox(Model.Type + "_HomeboundStatusOther", data.AnswerOrEmptyString("HomeboundStatusOther"), new { @id = Model.Type + "_HomeboundStatusOther", @class = "float-left" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fl">
        <legend>Patient Condition</legend>
        <%  string[] patientCondition = data.AnswerArray("PatientCondition"); %>
        <input name="<%= Model.Type %>_PatientCondition" value=" " type="hidden" />
        <div class="column checkgroup narrow">
            <div class="option">
                <%= string.Format("<input id='{0}_PatientConditionStable' name='{0}_PatientCondition' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("1").ToChecked() )%>
                <label for="<%= Model.Type %>_PatientConditionStable">Stable</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_PatientConditionImproved' name='{0}_PatientCondition' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("2").ToChecked())%>
                <label for="<%= Model.Type %>_PatientConditionImproved">Improved</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_PatientConditionUnchanged' name='{0}_PatientCondition' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("3").ToChecked())%>
                <label for="<%= Model.Type %>_PatientConditionUnchanged">Unchanged</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_PatientConditionUnstable' name='{0}_PatientCondition' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("4").ToChecked())%>
                <label for="<%= Model.Type %>_PatientConditionUnstable">Unstable</label>
            </div>
            <div class="option">
                <%= string.Format("<input id='{0}_PatientConditionDeclined' name='{0}_PatientCondition' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, patientCondition.Contains("5").ToChecked())%>
                <label for="<%= Model.Type %>_PatientConditionDeclined">Declined</label>
            </div>
        </div>
    </fieldset>
    <fieldset class="half fr">
        <legend>Service(s) Provided</legend>
        <div class="column">
            <div class="row">
                <div class="checkgroup narrow">
                    <%  string[] serviceProvided = data.AnswerArray("ServiceProvided"); %>
                    <input name="<%= Model.Type %>_ServiceProvided" value=" " type="hidden" />
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedSN' name='SixtyDaySummary_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "" )%>
                        <label for="SixtyDaySummary_ServiceProvidedSN" class="radio">SN</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedPT' name='SixtyDaySummary_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_ServiceProvidedPT" class="radio">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOT' name='SixtyDaySummary_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_ServiceProvidedOT" class="radio">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedST' name='SixtyDaySummary_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_ServiceProvidedST" class="radio">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedMSW' name='SixtyDaySummary_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_ServiceProvidedMSW" class="radio">MSW</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedHHA' name='SixtyDaySummary_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_ServiceProvidedHHA" class="radio">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOther' name='SixtyDaySummary_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%>
                        <label for="SixtyDaySummary_ServiceProvidedOther" class="radio">Other</label> 
                        <div class="more">
                            <%= Html.TextBox("SixtyDaySummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = "SixtyDaySummary_ServiceProvidedOtherValue", @class = "fill" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clr"></div>
    <fieldset>
        <legend>Vital Sign Ranges</legend>
        <div class="column wide">
            <table class="fixed ac">
                <tbody>
                    <tr>
                        <th></th>
                        <th>BP Systolic</th>
                        <th>BP Diastolic</th>
                        <th>HR</th>
                        <th>Resp</th>
                        <th>Temp</th>
                        <th>Weight</th>
                        <th>BG</th>
                        <th>Pain</th>
                    </tr>
                    <tr>
                        <th>Lowest</th>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBPMin", data.AnswerOrEmptyString("VitalSignBPMin"), new { @id = Model.Type + "_VitalSignBPMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBPDiaMin", data.AnswerOrEmptyString("VitalSignBPDiaMin"), new { @id = Model.Type + "_VitalSignBPDiaMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignHRMin", data.AnswerOrEmptyString("VitalSignHRMin"), new { @id = Model.Type + "_VitalSignHRMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignRespMin", data.AnswerOrEmptyString("VitalSignRespMin"), new { @id = Model.Type + "_VitalSignRespMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignTempMin", data.AnswerOrEmptyString("VitalSignTempMin"), new { @id = Model.Type + "_VitalSignTempMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMin", data.AnswerOrEmptyString("VitalSignWeightMin"), new { @id = Model.Type + "_VitalSignWeightMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBGMin", data.AnswerOrEmptyString("VitalSignBGMin"), new { @id = Model.Type + "_VitalSignBGMin", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignPainMin", data.AnswerOrEmptyString("VitalSignPainMin"), new { @id = Model.Type + "_VitalSignPainMin", @class = "fill" })%></td>
                    </tr>
                    <tr>
                        <th>Highest</th>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBPMax", data.AnswerOrEmptyString("VitalSignBPMax"), new { @id = Model.Type + "_VitalSignBPMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBPDiaMax", data.AnswerOrEmptyString("VitalSignBPDiaMax"), new { @id = Model.Type + "_VitalSignBPDiaMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignHRMax", data.AnswerOrEmptyString("VitalSignHRMax"), new { @id = Model.Type + "_VitalSignHRMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignRespMax", data.AnswerOrEmptyString("VitalSignRespMax"), new { @id = Model.Type + "_VitalSignRespMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignTempMax", data.AnswerOrEmptyString("VitalSignTempMax"), new { @id = Model.Type + "_VitalSignTempMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignWeightMax", data.AnswerOrEmptyString("VitalSignWeightMax"), new { @id = Model.Type + "_VitalSignWeightMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignBGMax", data.AnswerOrEmptyString("VitalSignBGMax"), new { @id = Model.Type + "_VitalSignBGMax", @class = "fill" })%></td>
                        <td><%= Html.TextBox(Model.Type + "_VitalSignPainMax", data.AnswerOrEmptyString("VitalSignPainMax"), new { @id = Model.Type + "_VitalSignPainMax", @class = "fill" })%></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </fieldset>
    <fieldset>
        <legend>Summary of Care Provided</legend>
        <div class="column wide">
            <div class="ac">
                <%= Html.Templates(Model.Type + "_SummaryOfCareProvidedTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_SummaryOfCareProvided" })%>
                <%= Html.TextArea(Model.Type + "_SummaryOfCareProvided", data.AnswerOrEmptyString("SummaryOfCareProvided"), new { @class = "fill tallest", @id = Model.Type + "_SummaryOfCareProvided", @rows = "15" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient&#8217;s Current Condition</legend>
        <div class="column wide">
            <div class="ac">
                <%= Html.Templates(Model.Type + "_PatientCurrentConditionTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_PatientCurrentCondition" })%>
                <%= Html.TextArea(Model.Type + "_PatientCurrentCondition", data.AnswerOrEmptyString("PatientCurrentCondition"), new { @class = "fill tallest", @id = Model.Type + "_PatientCurrentCondition", @rows = "10" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Goals</legend>
        <div class="column wide">
            <div class="ac">
                <%= Html.Templates(Model.Type + "_GoalsTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_Goals" })%>
                <%= Html.TextArea(Model.Type + "_Goals", data.AnswerOrEmptyString("Goals"), new { @class = "fill tallest", @id = Model.Type + "_Goals", @rows = "10" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Recomended Services</legend>
        <div class="column wide">
            <div class="row">
                <%  string[] recommendedService = data.AnswerArray("RecommendedService"); %>
                <input name="<%= Model.Type %>_RecommendedService" value="" type="hidden" />
                <div class="checkgroup narrow">
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServiceSN' name='{0}_RecommendedService' value='1' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("1").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServiceSN">SN</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServicePT' name='{0}_RecommendedService' value='2' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("2").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServicePT">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServiceOT' name='{0}_RecommendedService' value='3' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("3").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServiceOT">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServiceST' name='{0}_RecommendedService' value='4' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("4").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServiceST">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServiceMSW' name='{0}_RecommendedService' value='5' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("5").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServiceMSW">MSW</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServiceHHA' name='{0}_RecommendedService' value='6' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("6").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServiceHHA">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='{0}_RecommendedServiceOther' name='{0}_RecommendedService' value='7' class='radio float-left' type='checkbox' {1} />", Model.Type, recommendedService.Contains("7").ToChecked())%>
                        <label for="<%= Model.Type %>_RecommendedServiceOther">Other</label>
                        <div class="more">
                            <%= Html.TextBox(Model.Type + "_RecommendedServiceOtherValue", data.AnswerOrEmptyString("RecommendedServiceOtherValue"), new { @id = Model.Type + "_RecommendedServiceOtherValue", @class = "fill" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>  
    <fieldset>
        <legend>Notifications</legend>
        <div class="column wide">
            <div class="row">
                <div class="checkgroup wide">
                    <div class="option">
                        <%= string.Format("<input id='{0}_SummarySentToPhysician' name='{0}_SummarySentToPhysician' value='Yes' class='radio float-left' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("SummarySentToPhysician").IsEqual("Yes").ToChecked())%>
                        <label for="<%= Model.Type %>_SummarySentToPhysician">Summary Sent To Physician</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_SummarySentBy">Sent By</label>
                <div class="fr"><%= Html.Users(Model.Type + "_SummarySentBy", data.AnswerOrEmptyString("SummarySentBy"), new { @id = Model.Type + "_SummarySentBy", @class = "fill" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.Type %>_SummarySentDate">Date Sent</label>
                <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_SummarySentDate" value="<%= data.AnswerOrEmptyString("SummarySentDate") %>" id="Text1" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
            <div class="column">
                <div class="row">
                    <label class="strong fl" for="<%= Model.Type %>_ClinicianSignature" class="float-left">Clinician</label>
                    <div class="fr"><%= Html.Password(Model.Type + "_Clinician", "", new { @id = Model.Type + "_Clinician" })%></div>
                </div>
                <div class="row">
                    <label class="strong fl" for="<%= Model.Type %>_ClinicianSignature2" class="float-left">Clinician</label>
                    <div class="fr"><%= Html.Password(Model.Type + "_Clinician2", "", new { @id = Model.Type + "_Clinician2" })%></div>
                </div>
                
                <div class="row">
                    <label class="strong fl" for="<%= Model.Type %>_ClinicianSignature3" class="float-left">Clinician</label>
                    <div class="fr"><%= Html.Password(Model.Type + "_Clinician3", "", new { @id = Model.Type + "_Clinician3" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label class="strong fl" for="<%= Model.Type %>_ClinicianSignatureDate" class="float-left">Date</label>
                    <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="Text2" /></div>
                </div>
                <div class="row">
                    <label class="strong fl" for="<%= Model.Type %>_ClinicianSignatureDate2" class="float-left">Date</label>
                    <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate2" value="<%= data.AnswerOrEmptyString("SignatureDate2") %>" id="Text3" /></div>
                </div>
                <div class="row">
                    <label class="strong fl" for="<%= Model.Type %>_ClinicianSignatureDate3" class="float-left">Date</label>
                    <div class="fr"><input type="text" class="date-picker" name="<%= Model.Type %>_SignatureDate3" value="<%= data.AnswerOrEmptyString("SignatureDate3") %>" id="Text4" /></div>
                </div>
            </div>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <div class="checkgroup wide">
                <div class="row narrowest">
                    <div class="option wide">
                        <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                        <label for="<%= Model.Type %>_ReturnForSignature">Return to Clinician for Signature</label>
                    </div>
                </div>
            </div>
    <%  } %>
    </fieldset>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0)" onclick="Schedule.Visit.<%= Model.Type %>.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0)" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>