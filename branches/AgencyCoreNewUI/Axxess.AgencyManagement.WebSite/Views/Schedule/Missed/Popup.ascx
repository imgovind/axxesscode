﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<fieldset>
    <div class="wide column">
        <div class="row"><label for="Missed_Visit_Info_Name" class="float-left">Patient Name</label><div class="fr"><%= Model != null && Model.PatientName.IsNotNullOrEmpty() ? Model.PatientName : "Unknown" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_TypeofVisit" class="float-left">Type of Visit</label><div class="fr"><%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType  : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_DateofVisit" class="float-left">Date of Visit</label><div class="fr"><%= Model != null ? Model.Date.ToShortDateString():string.Empty %></div></div>
        <div class="row"><label for="Missed_Visit_Info_UserName" class="float-left">Assigned To</label><div class="fr"><%= Model != null && Model.UserName.IsNotNullOrEmpty() ? Model.UserName : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_OrderGenerated" class="float-left">Order Generated</label><div class="fr"><%= (Model != null && Model.IsOrderGenerated) ? "Yes" : "No"%></div></div>
        <div class="row"><label for="Missed_Visit_Info_PhysicianOfficeNotified" class="float-left">Physician Office Notified</label><div class="fr"><%= (Model != null && Model.IsPhysicianOfficeNotified) ? "Yes" : "No"%></div></div>
        <div class="row"><label for="Missed_Visit_Info_Reason" class="float-left">Reason</label><div class="fr"><%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason : "" %></div></div>
        <div class="row"><label for="Missed_Visit_Info_Comments" class="float-left">Comments</label><div class="fr"><%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments : "" %></div></div>
    </div>
</fieldset>