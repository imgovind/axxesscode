﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignMultipleViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Reassign" + Model.Type, "Schedule", FormMethod.Post, new { @id = "Reassign" + Model.Type + "_Form" })) { %>
    <%  if (!Model.Type.IsEqual("All")) { %>
        <%= Html.Hidden("PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" }) %>
    <% } %>
    <fieldset>
        <legend>Reassign Tasks</legend>
        <div class="wide column">
    <%  if (!Model.Type.IsEqual("All")) { %>
            <div class="row">
                <label class="fl strong">Patient</label>
                <div class="fr"><%= Model.PatientDisplayName %></div>
            </div>
    <%  } %>
            <div class="row">
                <label for="Reassign<%= Model.Type %>_EmployeeOldId" class="fl strong">Reassign From</label>
                <div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "EmployeeOldId", "", Guid.Empty, 0, new { @id = "Reassign" + Model.Type + "_EmployeeOldId", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="Reassign<%= Model.Type %>_EmployeeId" class="fl strong">Reassign To</label>
                <div class="fr"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "EmployeeId", "", Guid.Empty, 1, new { @id = "Reassign" + Model.Type + "_EmployeeId", @class = "required notzero" })%></div>
            </div>
            <div class="row"><em><strong>Notice</strong>: Only tasks that are not started and not yet due will be reassigned.</em></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Reassign</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>