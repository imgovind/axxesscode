﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<TaskLog>>" %>
<%= Html.Telerik().Grid(Model)
        .Name("ScheduleEventLogsGrid")
        .PrefixUrlParameters(false)
        .Columns(columns => {
            columns.Bound(l => l.UserName).Width(81);
            columns.Bound(l => l.StatusName).Width(81);
            columns.Bound(l => l.Date).Format("{0:MM/dd/yyyy hh:mm tt}").Width(100);
        }).Footer(false).Sortable() %>