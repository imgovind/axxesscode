﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<div class="buttons heading">
    <ul>
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <li><a class="new-patient">Add New Patient</a></li>
<%  } %>
    </ul>
</div>
<div class="row">
    <label for="ScheduleCenter_BranchId" class="strong">Branch</label>
    <div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", "", new { @id = "ScheduleCenter_BranchId" })%></div>
</div>
<div class="row">
    <label for="ScheduleCenter_StatusId" class="strong">Status</label>
    <div>
        <select name="StatusId" id="ScheduleCenter_StatusId">
            <option value="1">Active Patients</option>
            <option value="2">Discharged Patients</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="ScheduleCenter_PaymentSourceId" class="strong">Pay Source</label>
    <div>
        <select name="PaymentSourceId" id="ScheduleCenter_PaymentSourceId">
            <option value="0">All</option>
            <option value="1">Medicare (traditional)</option>
            <option value="2">Medicare (HMO/managed care)</option>
            <option value="3">Medicaid (traditional)</option>
            <option value="4">Medicaid (HMO/managed care)</option>
            <option value="5">Workers' compensation</option>
            <option value="6">Title programs</option>
            <option value="7">Other government</option>
            <option value="8">Private</option>
            <option value="9">Private HMO/managed care</option>
            <option value="10">Self Pay</option>
            <option value="11">Unknown</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="ScheduleCenter_TextSearch" class="strong">Text Filter</label>
    <div><input id="ScheduleCenter_TextSearch" name="TextSearch" type="text" /></div>
</div>