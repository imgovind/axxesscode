﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<span class="wintitle">Schedule Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><%  Html.RenderPartial("Center/PatientFilters", Model); %></div>
        <div class="bottom new-patient"><%  Html.RenderPartial("Center/PatientSelector"); %></div>
    </div>
    <div id="ScheduleMainResult" class="ui-layout-center"></div>
</div>
<%  if (Model == null || Model.Count == 0) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(U.MessageWarn("No Patient Found", "There are no patients to be gound which match your search criteria.  Please try a different search."));
</script>
<%  } %>