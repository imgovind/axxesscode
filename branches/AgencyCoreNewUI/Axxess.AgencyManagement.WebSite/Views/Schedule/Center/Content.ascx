﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<%  string[] stabs = new string[] { "Nursing", "HHA","MSW", "Therapy" }; %>
<%  if (Model.Episode != null) { %>
<div class="top">
    <div class="winmenu">
        <ul>
    <%  if (!Model.IsDischarged && Current.HasRight(Permissions.EditEpisode)) { %>
            <li><a class="new-episode" status="Add New Episode">New Episode</a></li>
    <%  } %>
    <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
            <li><a class="schedule-employee" status="Multiple Employee">Schedule Employee</a></li>
    <%  } %>
    <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
            <li><a class="reassign-schedule" status="Reassign Schedules">Reassign Schedules</a></li>
    <%  } %>
            <li><a class="master-calendar" status="Master Calendar">Master Calendar</a></li>
    <%  if (Current.HasRight(Permissions.EditEpisode)) { %>
            <li><a class="inactive-episodes" status="Inactive Episodes">Inactive Episodes</a></li>
    <%  } %>
            <li><a class="episode-frequencies" status="Episode Frequencies">Episode Frequencies</a></li>
        </ul>
    </div>
    <div class="wrapper main rel">
    <%  if (Model.Episode.HasPrevious) { %>
        <span class="button top left">
            <a class="navigate previous-episode" guid="<%= Model.Episode.PreviousEpisode.Id %>"><span class="largefont">&#8617;</span>Previous Episode</a>
        </span>
    <%  } %>
    <%  if (Model.Episode.HasNext) { %>
        <span class="button top right">
            <a class="navigate next-episode" guid="<%= Model.Episode.NextEpisode.Id %>">Next Episode<span class="largefont">&#8618;</span></a>
        </span>
    <%  } %>
        <fieldset class="patient-summary ma">
            <div class="wide column">
                <div class="row">
                    <h1 class="fl"><%= Model.Episode.DisplayNameWithMi %></h1>
                    <div class="fl"><%= Html.PatientEpisodes("EpisodeList", Model.Episode.Id.ToString(), Model.PatientId, new { @id = "ScheduleCenter_EpisodeList" }) %></div>
                    <div class="buttons fr">
                        <ul>
    <%  if (Current.HasRight(Permissions.EditEpisode)) { %>
                            <li><a class="edit-episode">Manage Episode</a></li>
    <%  } %>
                            <li><a class="refresh">Refresh</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="calendar"><%  Html.RenderPartial("Center/Calendar", new CalendarViewData { Episode = Model.Episode, PatientId = Model.Episode.PatientId, IsDischarged = Model.IsDischarged }); %></div>
    </div>
    <div class="ac">
    <%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <div class="buttons abs-left">
            <ul>
                <li><a class="patient-charts">View Patient Chart</a></li>
            </ul>
        </div>
    <%  } %>
        <fieldset class="ac legend">
            <ul>
                <li><div class="scheduled">&#160;</div>Scheduled</li>
                <li><div class="completed">&#160;</div>Completed</li>
                <li><div class="missed">&#160;</div>Missed</li>
                <li><div class="multi">&#160;</div>Multiple</li>
            </ul>
        </fieldset>
    </div>
    <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
    <div id="ScheduleCenter_Collapsed"><a class="show-scheduler">Show Scheduler</a></div>
    <div id="ScheduleCenter_TabStrip" class="scheduler"><% Html.RenderPartial("Center/Scheduler", Model.Episode); %></div>
    <%  } %>
</div>
<div class="bottom"><% Html.RenderPartial("Center/Activities", new ScheduleActivityArgument { EpisodeId = Model.Episode.Id, PatientId = Model.Episode.PatientId, Discpline = "all" }); %></div>
<%  } else { %>
    <%  if (!Model.IsDischarged) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(U.MessageWarn("No Episode Found", "No episode found for this patient.  Please add a new episode.").append(
        $("<div/>", { "class": "heading" }).Buttons([
            { Text: "Add New Episode", Click: function() { Schedule.Episode.New("<%= Model.PatientId.ToString() %>") } },
            { Text: "Inactive Episodes", Click: function() { Schedule.Episodes.Inactive("<%= Model.PatientId %>") } }
        ])
    ));
</script>
    <%  } else { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(U.MessageWarn("No Episode Found", "No episodes found for this discharged patient. Re-admit the patient to create new episodes."));
</script>
    <%  } %>
<%  } %>