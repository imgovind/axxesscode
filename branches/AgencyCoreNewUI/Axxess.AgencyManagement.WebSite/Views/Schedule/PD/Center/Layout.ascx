﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Private Duty Schedule Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><%  Html.RenderPartial("PD/Center/PatientFilters"); %></div>
        <div class="bottom new-patient"><%  Html.RenderPartial("PD/Center/PatientSelector"); %></div>
    </div>
    <div id="PDScheduleMainResult" class="ui-layout-center">
        <div class="winmenu">
            <ul>
                <li><a class="schedule-employee">Schedule Employee</a></li>
                <li><a class="reassign-schedule">Reassign Schedules</a></li>
                <li><a class="master-calendar">Master Calendar</a></li>
                <li><a class="schedule-employee">Schedule Employee</a></li>
            </ul>
        </div>
        <div class="wrapper main rel">
            <div class="pd-calendar"></div>
        </div>
    </div>
</div>