﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var pageName = "PDScheduleCenter";  %>
<div class="buttons heading">
    <ul>
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <li><a class="new-patient">Add New Patient</a></li>
<%  } %>
    </ul>
</div>
<div class="row">
    <label for="<%=pageName %>_BranchId" class="strong">Branch</label>
    <div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", "", new { @id = pageName+"_BranchId" })%></div>
</div>
<div class="row">
    <label for="<%=pageName %>_StatusId" class="strong">Status</label>
    <div>
        <select name="StatusId" id="<%=pageName %>_StatusId">
            <option value="1">Active Patients</option>
            <option value="2">Discharged Patients</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="<%=pageName %>_PaymentSourceId" class="strong">Pay Source</label>
    <div>
        <select name="PaymentSourceId" id="<%=pageName %>_PaymentSourceId">
            <option value="0">All</option>
            <option value="1">Medicare (traditional)</option>
            <option value="2">Medicare (HMO/managed care)</option>
            <option value="3">Medicaid (traditional)</option>
            <option value="4">Medicaid (HMO/managed care)</option>
            <option value="5">Workers' compensation</option>
            <option value="6">Title programs</option>
            <option value="7">Other government</option>
            <option value="8">Private</option>
            <option value="9">Private HMO/managed care</option>
            <option value="10">Self Pay</option>
            <option value="11">Unknown</option>
        </select>
    </div>
</div>
<div class="row">
    <label for="<%=pageName %>_TextSearch" class="strong">Text Filter</label>
    <div><input id="<%=pageName %>_TextSearch" name="TextSearch" type="text" /></div>
</div>