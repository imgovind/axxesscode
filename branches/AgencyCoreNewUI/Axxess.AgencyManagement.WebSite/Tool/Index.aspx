﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Browser and IP Address Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">

        <% System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
           string browserInfo = "<strong>Your Browser Information</strong><br />"
               + "Name = " + browser.Browser + "<br />"
               + "Version = " + browser.Version + "<br />"
               + "Is Beta = " + browser.Beta + "<br />"
               + "Is Mobile = " + browser.IsMobileDevice + "<br />"
               + "User Agent = " + Request.UserAgent.ToLower() + "<br />";
        %>
        <%= browserInfo %>
            
        <% string ipAddressInfo = "<br /><strong>IP Address</strong><br />"
               + "Current IP = " + HttpContext.Current.Request.UserHostAddress; %>
        <%= ipAddressInfo%>
        
        <% string remoteIpAddressInfo = "<br />"
               + "Remote IP = " + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]; %>
        <%= remoteIpAddressInfo%>
        
        <% string forwaredIpAddressInfo = "<br />"
               + "Forwarded IP = " + HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]; %>
        <%= forwaredIpAddressInfo%>
        
        <% string serverInfo = "<br /><br /><strong>Server Information</strong><br />"
               + "Name = " + Environment.MachineName + "<br />"
               + "IP Address = " + HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"]; %>
        <%= serverInfo%>
        
         <% string softVersion = "<br /><br /><strong>Software Version</strong><br />"
               + "Version = " + Current.AssemblyVersion; %>
        <%= softVersion%>
        <% string signature = "jesspar1205";
            var saltedHash = new SaltedHash();
            string newSignatureSalt = string.Empty;
            string newSignatureHash = string.Empty;

            saltedHash.GetHashAndSalt(signature, out newSignatureHash, out newSignatureSalt);
             %><br />
        <%= newSignatureHash%><br />
        <%= newSignatureSalt %>
</asp:Content>
    