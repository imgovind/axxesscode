﻿$.extend(Patient, {
    Hospitalization: {
        Delete: function(id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this hospitalization log?",
                Yes: function() {
                    U.PostUrl("Patient/Hospitalization/Delete", { id: id, patientId: patientId }, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) Patient.Hospitalization.RefreshLogs();
                    })
                }
            })
        },
        Edit: function(id, patientId) { Acore.Open("EditHospitalization", { id: id, patientId: patientId }) },
        Export: function(patientId) { U.GetAttachment("Patient/Hospitalization/Export", { patientId: patientId }) },
        InitEdit: function(r, t, x, e) { Patient.Hospitalization.InitShared(r, t, x, e) },
        InitList: function(r, t, x, e) {
            $(".new-hospitalization", e).click(function() {
                Patient.Hospitalization.New($("[name=PatientId]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Patient.Hospitalization.Export();
                return false;
            });
        },
        InitLogs: function(r, t, x, e) {
            $(".new-hospitalization", e).click(function() {
                Patient.Hospitalization.New($("[name=PatientId]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Patient.Hospitalization.Export($("[name=PatientId]", e).val());
                return false;
            });
        },
        InitNew: function(r, t, x, e) { Patient.Hospitalization.InitShared(r, t, x, e) },
        InitShared: function(r, t, x, e) {
            Schedule.InitEpisodeDropDown(e);
            U.InitShared(r, t, x, e);
        },
        List: function() { Acore.Open("ListHospitalization") },
        Logs: function(patientId) { Acore.Open("HospitalizationLogs", { patientId: patientId }) },
        New: function(patientId) { Acore.Open("NewHospitalization", { patientId: patientId }) },
        Print: function(id, patientId) {
            Acore.OpenPrintView({
                PreviewUrl: "Patient/Hospitalization/PrintPreview/" + id + "/" + patientId,
                PdfUrl: "Patient/Hospitalization/Print/" + id + "/" + patientId
            })
        },
        RefreshLogs: function() { $("#window_HospitalizationLogs .acore-grid").Load("Patient/Hospitalization/LogsGrid", { patientId: $("#window_HospitalizationLogs [name=PatientId]").val() }) }
    }
});