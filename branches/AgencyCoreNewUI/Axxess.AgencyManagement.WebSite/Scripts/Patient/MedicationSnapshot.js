﻿$.extend(Patient.Medication, {
    Snapshot: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this medication snapshot?",
                Yes: function() { U.GridAjaxCommand("Patient/Medication/Snapshot/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Modal({
                Name: "Edit Medication Snapshot",
                Url: "Patient/Medication/Snapshot/Edit",
                Input: { id: id },
                OnLoad: Patient.Medication.Snapshot.InitEdit,
                Width: 450
            })
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitList: function(r, t, x, e) { },
        InitNew: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        List: function(patientId) {
            Acore.Open("ListMedicationSnapshot", { patientId: patientId });
        },
        New: function(patientId) {
            Acore.Open("NewMedicationSnapshot", { patientId: patientId });
        },
        Print: function(id) {
            Acore.OpenPrintView({
                PreviewUrl: "Patient/Medication/Snapshot/PrintPreview/" + id,
                PdfUrl: "Patient/Medication/Snapshot/Print/" + id
            })
        }
    }
})