﻿$.extend(Patient, {
    Authorization: {
        Delete: function(id, patientId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this authorization?",
                Yes: function() { U.GridAjaxCommand("Patient/Authorization/Delete", { id: id, patientId: patientId }, $grid) }
            });
        },
        Edit: function(id, patientId) {
            Acore.Open("EditAuthorization", { id: id, patientId: patientId });
        },
        Export: function(patientId) {
            U.GetAttachment("Patient/Authorization/Export", { patientId: patientId });
        },
        InitEdit: function(r, t, x, e) {
            var Type = U.GetType(e), Prefix = "#" + Type + "_";
            U.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Patient.Authorization.Logs($(Prefix + "Id").val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-authorization", e).click(function() {
                Patient.Authorization.New($("[name=PatientId]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Patient.Authorization.Export($("[name=PatientId]", e).val());
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            $(prefix + "Insurance").change(function() {
                if ($(this).val() == "new") {
                    $(this).val("0");
                    Agency.Insurance.New();
                }
            });
            U.InitShared(r, t, x, e);
        },
        List: function(patientId) {
            Acore.Open("ListAuthorizations", { patientId: patientId });
        },
        Logs: function(id) {
            Acore.Open("AuthorizationLogs", { id: id });
        },
        New: function(patientId) {
            Acore.Open("NewAuthorization", { patientId: patientId });
        },
        Print: function(id, patientId) {
            Acore.OpenPrintView({
                PreviewUrl: "Patient/Authorization/PrintPreview/" + id + "/" + patientId,
                PdfUrl: "Patient/Authorization/Print/" + id + "/" + patientId
            });
        }
    }
});