﻿$.extend(Patient, {
    Allergy: {
        AssessmentType: "",
        Delete: function(id, profileId) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this allergy?",
                Yes: function() {
                    U.PostUrl("Patient/Allergy/UpdateStatus", { id: id, profileId: profileId, isDeprecated: true }, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) Patient.Allergy.Refresh();
                    })
                }
            })
        },
        Edit: function(id, profileId) {
            Acore.Modal({
                Name: "Edit Allergy",
                Url: "Patient/Allergy/Edit",
                Input: { id: id, profileId: profileId },
                OnLoad: Patient.Allergy.InitEdit,
                Width: 650
            })
        },
        InitEdit: function(r, t, x, e) {
            Patient.Allergy.InitShared(r, t, x, e);
        },
        InitNew: function(r, t, x, e) {
            Patient.Allergy.InitShared(r, t, x, e);
        },
        InitProfile: function(r, t, x, e) {
            $(".new-allergy", e).click(function() {
                Patient.Allergy.New($("[name=Id]", e).val());
                return false;
            });
            $(".print", e).click(function() {
                Patient.Allergy.ProfilePrint($("[name=PatientId]", e).val());
                return false;
            });
            $(".grid-refresh", e).click(function() {
                Patient.Allergy.Refresh();
                return false;
            });
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            $("[name=Type]", e).Autocomplete({ source: [ "Medication", "Food", "Animals", "Plants", "Latex", "Environmental" ] });
            U.InitShared(r, t, x, e);
        },
        New: function(profileId) {
            Acore.Modal({
                Name: "Add New Allergy",
                Url: "Patient/Allergy/New",
                Input: { profileId: profileId },
                OnLoad: Patient.Allergy.InitNew,
                Width: 650
            })
        },
        Profile: function(patientId) {
            Acore.Open("AllergyProfile", { patientId: patientId });
        },
        ProfilePrint: function(id) {
            Acore.OpenPrintView({
                PreviewUrl: "Patient/Allergy/Profile/PrintPreview/" + id,
                PdfUrl: "Patient/Allergy/Profile/Print/" + id
            });
        },
        Refresh: function() {
            $(".allergy-profile.acore-grid").each(function() {
                $(this).Load("Patient/Allergy/Profile/Grid", { profileId: $(this).closest(".wrapper").find("[name=Id]").val() })
            })
        },
        Restore: function(id, profileId) {
            Acore.Confirm({
                Message: "Are you sure you want to restore this allergy?",
                Yes: function() {
                    U.PostUrl("Patient/Allergy/UpdateStatus", { id: id, profileId: profileId, isDeprecated: false }, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) Patient.Allergy.Refresh();
                    })
                }
            })
        }
    }
})