﻿$.extend(Patient, {
    Photo: {
        Delete: function(id) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this photo?",
                Yes: function() {
                    U.PostUrl("Patient/Photo/Delete", { id: id }, function(r) {
                        U.Growl(r.errorMessage, r.isSuccessful ? "success" : "error");
                        Patient.Charts.Refresh();
                    })
                }
            });
        },
        Edit: function(id) {
            Acore.Modal({
                Name: "Edit Patient Photo",
                Url: "Patient/Photo/Edit",
                Input: { id: id },
                OnLoad: Patient.Photo.InitEdit,
                Width: 400
            });
        },
        InitEdit: function(r, t, x, e) {
            $(".save", e).hide();
            $("[type=file]", e).change(Patient.Photo.Validate);
            $(".delete", e).click(function() {
                Patient.Photo.Delete($("[name=id]", e).val());
                return false;
            });
            U.InitShared(r, t, x, e);
        },
        Validate: function() {
            var ext = $(this).val().split(".").pop().toLowerCase(), allow = new Array("gif", "png", "jpg", "jpeg");
            if ($.inArray(ext, allow) == -1) {
                U.Growl("Please select a valid image<br/>Accepts GIF, PNG, and JPEG", "error");
                $(this).val("");
                $(this).closest(".wrapper").find(".save").hide();
            } else $(this).closest(".wrapper").find(".save").show();
        }
    }
});