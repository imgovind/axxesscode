﻿$.extend(Patient, {
    Order: {
        Approve: function(id, episodeId, patientId) {
            Patient.Order.UpdateStatus(id, episodeId, patientId, "Approve");
        },
        Delete: function(id, patientId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this Order?",
                Yes: function() { U.GridAjaxCommand("Patient/Order/Delete", { id: id, patientId: patientId }, $grid) }
            })
        },
        Edit: function(id, patientId) {
            Acore.Open("EditOrder", { id: id, patientId: patientId });
        },
        History: function(patientId) {
            Acore.Open("PatientOrderHistory", { patientId: patientId });
        },
        HistoryExport: function(patientId, startDate, endDate) {
            U.GetAttachment("Patient/Order/History/Export", { patientId: patientId, startDate: startDate, endDate: endDate });
        },
        HistoryRefresh: function() {
            U.RebindGrid($("#PatientOrderHistory_Grid"), {
                patientId: $("#PatientOrderHistory_PatientId").val(),
                startDate: $("#PatientOrderHistory_StartDate").val(),
                endDate: $("#PatientOrderHistory_EndDate").val()
            })
        },
        InitEdit: function(r, t, x, e) {
            Patient.Order.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Patient.Order.Logs($("[name=Id]", e).val(), $("[name=PatientId]", e).val());
                return false;
            })
        },
        InitHistory: function(r, t, x, e) {
            $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                Patient.Order.HistoryRefresh();
                return false;
            });
            $(".grid-controls .date-picker", e).change(function() { Patient.Order.HistoryRefresh() });
            $(".export", e).click(function() {
                Patient.Order.HistoryExport($("[name=PatientId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                return false;
            })
        },
        InitNew: function(r, t, x, e) {
            Patient.Order.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            Schedule.InitEpisodeDropDown(e);
            U.InitShared(r, t, x, e);
        },
        Logs: function(id, patientId) {
            Acore.Open("OrderLogs", { id: id, patientId: patientId });
        },
        New: function(patientId) {
            Acore.Open("NewOrder", { patientId: patientId });
        },
        EditModal: function(id, patientId, type) {
            Acore.Modal({
                Name: "Orders History",
                Url: "Agency/OrderHistoryEdit",
                Input: { id: id, patientId: patientId, type: type },
                OnLoad: function() { U.InitTemplate($("#updateOrderHistory"), function() { Agency.GenerateClick('OrdersHistory'); UserInterface.CloseModal(); }, "Order successfully updated"); },
                Width: "500px",
                Height: "200px",
                WindowFrame: false
            })
        },
        Print: function(id, patientId, episodeId, QA) {
            if (id == undefined) U.GetAttachment("Patient/Order/Print", {});
            else {
                var Arguments = {
                    PreviewUrl: "Patient/Order/PrintPreview/" + id + "/" + patientId,
                    PdfUrl: "Patient/Order/Print/" + id + "/" + patientId
                };
                if (QA) $.extend(Arguments, {
                    ReturnClick: function() { Patient.Order.Return(id, episodeId, patientId) },
                    Buttons: [
                        { Text: "Edit", Click: function() { Patient.Order.Edit(id, patientId) } },
                        { Text: "Approve", Click: function() { Patient.Order.Approve(id, episodeId, patientId) } }
                    ]
                });
                Acore.OpenPrintView(Arguments);
            }
        },
        UpdateStatus: function(id, episodeId, patientId, action, reason) {
            U.PostUrl("Patient/Order/UpdateStatus", {
                id: id,
                episodeId: episodeId,
                patientId: patientId,
                actionType: action,
                reason: reason
            }, function(result) {
                U.GrowlResult(result);
                if (result.isSuccessful) {
                    Acore.CloseDialog();
                    U.RebindAllGrids();
                }
            })
        },
        Return: function(id, episodeId, patientId) {
            Patient.Order.UpdateStatus(id, episodeId, patientId, "Return", $("#print-return-reason textarea").val());
        }
    }
});