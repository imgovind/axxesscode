﻿$.extend(Patient, {
    Medication: {
        AssessmentType: "",
        Activate: function(id, profileId) {
            Acore.Confirm({
                Message: "Are you sure you want to activate this medication?",
                Yes: function() {
                    U.PostUrl("Patient/Medication/UpdateStatus", { id: id, profileId: profileId, medicationCategory: "Active", dischargeDate: "01/01/0001" }, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) Patient.Medication.Refresh();
                    })
                }
            })
        },
        New: function(profileId) {
            Acore.Modal({
                Name: "Add New Medication",
                Url: "Patient/Medication/New",
                Input: { profileId: profileId },
                OnLoad: Patient.Medication.InitNew,
                Width: 650
            })
        },
        Delete: function(id, profileId) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this medication?",
                Yes: function() {
                    U.PostUrl("Patient/Medication/Delete", { id: id, profileId: profileId }, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) Patient.Medication.Refresh();
                    })
                }
            })
        },
        Discontinue: function(id, profileId) {
            Acore.Modal({
                Name: "Discontinue Medication",
                Url: "Patient/Medication/Discontinue",
                Input: { id: id, profileId: profileId },
                OnLoad: Patient.Medication.InitDiscontinue,
                Width: 450
            })
        },
        Edit: function(id, profileId) {
            Acore.Modal({
                Name: "Edit Medication",
                Url: "Patient/Medication/Edit",
                Input: { id: id, profileId: profileId },
                OnLoad: Patient.Medication.InitEdit,
                Width: 650
            })
        },
        InitDiscontinue: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitEdit: function(r, t, x, e) {
            Patient.Medication.InitShared(r, t, x, e);
        },
        InitInteractions: function(r, t, x, e) {
            U.SelectAll($(".select-all", e), $("[name=drugsSelected]", e));
            $(".generate", e).click(function() {
                if ($("[name=drugsSelected]:checked", e).length) $("form", e).submit();
                else U.Growl("Please select at least one medication.", "error");
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            Patient.Medication.InitShared(r, t, x, e);
        },
        InitProfile: function(r, t, x, e) {
            $(".new-medication", e).click(function() {
                Patient.Medication.New($("[name=Id]", e).val());
                return false;
            });
            $(".sign-profile", e).click(function() {
                Patient.Medication.Snapshot.New($("[name=PatientId]", e).val());
                return false;
            });
            $(".print", e).click(function() {
                Patient.Medication.ProfilePrint($("[name=PatientId]", e).val());
                return false;
            });
            $(".drug-interactions", e).click(function() {
                Patient.Medication.Interactions($("[name=Id]", e).val());
                return false;
            });
            $(".profile-history", e).click(function() {
                Patient.Medication.Snapshot.List($("[name=PatientId]", e).val());
                return false;
            });
            $(".grid-refresh", e).click(function() {
                Patient.Medication.Refresh();
                return false;
            });
            $(".activity-log", e).click(function() {
                Patient.Medication.Logs($("[name=PatientId]", e).val());
                return false;
            });
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.HideIfChecked($(prefix + "IsLongStanding"), $(prefix + "StartDateRow"));
            $("[name=MedicationDosage]", e).AjaxAutocomplete({
                minLength: 2,
                SourceUrl: "LookUp/Drugs",
                Format: function(json) {
                    return json.Name
                },
                Select: function(json, input) {
                    input.val(json.Name).next().val(json.Name).closest("form");
                    $("[name=LexiDrugId]", e).val(json.LexiDrugId);
                    U.PostUrl("LookUp/DrugClassifications", { genericDrugId: json.LexiDrugId }, function(result) {
                        if (result != undefined && result.length == 1) $("[name=Classification]", e).val(result[0]);
                        else if (result.length > 1) $("[name=Classification]", e).Autocomplete({
                            minLength: 0,
                            source: result
                        }).autocomplete("search", "");
                    })
                }
            });
            $("[name=Route]", e).AjaxAutocomplete({
                minLength: 1,
                SourceUrl: "LookUp/MedicationRoute",
                Format: function(json) {
                    return json.Id + " &#8211; " + json.ShortName + " " + json.LongName
                },
                Select: function(json, input) {
                    input.val(json.LongName + " (" + json.ShortName + ")").next().val(json.LongName + " (" + json.ShortName + ")")
                }
            });
            $("[name=Frequency]", e).Autocomplete({ source: ["Before Meals", "After meals", "Daily", "Every Day", "Twice/day", "Three times/day", "Four times /day", "Every other day", "PRN", "Bedtime"] });
            U.InitShared(r, t, x, e);
        },
        Interactions: function(profileId) {
            Acore.Modal({
                Name: "Drug-Drug Interactions",
                Url: "Patient/Medication/Interactions",
                Input: { profileId: profileId },
                OnLoad: Patient.Medication.InitInteractions,
                Width: 750
            })
        },
        Logs: function(patientId) {
            Acore.Open("MedicationLogs", { patientId: patientId });
        },
        Profile: function(patientId) {
            Acore.Open("MedicationProfile", { patientId: patientId });
        },
        ProfilePrint: function(patientId) {
            Acore.OpenPrintView({
                PreviewUrl: "Patient/Medication/Profile/PrintPreview/" + patientId,
                PdfUrl: "Patient/Medication/Profile/Print/" + patientId
            });
        },
        Refresh: function() {
            $(".med-profile.acore-grid").each(function() {
                $(this).Load("Patient/Medication/Profile/Grid", { profileId: $(this).closest(".wrapper").find("[name=Id]").val() })
            })
        }
    }
})