﻿$.extend(Patient, {
    Physician: {
        Add: function(id, patientId, $grid) {
            U.GridAjaxCommand("Patient/Physician/Add", { id: id, patientId: patientId }, $grid);
        },
        AddRemove: function(e) {
            if (e.text() == "+") e.closest(".column").find(".row.hidden:first").removeClass("hidden").find("input:first").blur();
            else e.closest(".row").addClass("hidden").appendTo($(el).closest(".column")).find("select").val("00000000-0000-0000-0000-000000000000");
        },
        Delete: function(id, patientId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to remove this physician from this patient?",
                Yes: function() { U.GridAjaxCommand("Patient/Physician/Delete", { id: id, patientId: patientId }, $grid) }
            })
        },
        SetPrimary: function(id, patientId, $grid) {
            U.GridAjaxCommand("Patient/Physician/SetPrimary", { id: id, patientId: patientId }, $grid);
        }
    }
});