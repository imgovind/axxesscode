﻿$.extend(Patient, {
    EmergencyContact: {
        Delete: function(id, patientId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this emergency contact?",
                Yes: function() { U.GridAjaxCommand("Patient/EmergencyContact/Delete", { id: id, patientId: patientId }, $grid) }
            })
        },
        Edit: function(id, patientId) {
            Acore.Modal({
                Name: "Edit Emergency Contact",
                Url: "Patient/EmergencyContact/Edit",
                Input: { id: id, patientId: patientId },
                OnLoad: Patient.EmergencyContact.InitEdit,
                Width: 450
            })
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitNew: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        New: function(patientId) {
            Acore.Modal({
                Name: "New Emergency Contact",
                Url: "Patient/EmergencyContact/New",
                Input: { patientId: patientId },
                OnLoad: Patient.EmergencyContact.InitNew,
                Width: 450
            })
        }
    }
})
