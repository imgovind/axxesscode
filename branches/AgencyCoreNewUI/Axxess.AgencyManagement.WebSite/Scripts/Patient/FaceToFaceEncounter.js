﻿$.extend(Patient, {
    FaceToFaceEncounter: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this FaceToFaceEncounter?",
                Yes: function() { U.GridAjaxCommand("Patient/FaceToFaceEncounter/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditFaceToFaceEncounter", { id: id });
        },
        Export: function() {
            U.GetAttachment("Patient/FaceToFaceEncounter/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            var Type = U.GetType(e), Prefix = "#" + Type + "_";
            U.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Patient.FaceToFaceEncounter.Logs($(Prefix + "Id").val());
                return false;
            })
        },
        InitList: function(r, t, x, e) {
            $(".new-infection", e).click(function() {
                Patient.FaceToFaceEncounter.New();
                return false;
            });
            $(".export", e).click(function() {
                Patient.FaceToFaceEncounter.Export();
                return false;
            })
        },
        InitNew: function(r, t, x, e) {
            Schedule.InitEpisodeDropDown(e);
            U.InitShared(r, t, x, e);
        },
        Logs: function(id) {
            Acore.Open("FaceToFaceEncounterLogs", { id: id });
        },
        New: function() {
            Acore.Open("NewFaceToFaceEncounter");
        },
        Print: function(id, patientId) {
            if (id == undefined) U.GetAttachment("Patient/Order/Print", {});
            else Acore.OpenPrintView({
                Url: "Patient/FaceToFaceEncounter/PrintPreview",
                PdfUrl: "Patient/FaceToFaceEncounter/Print",
                Data: { id: id, patientId: patientId }
            })
        }
    }
});