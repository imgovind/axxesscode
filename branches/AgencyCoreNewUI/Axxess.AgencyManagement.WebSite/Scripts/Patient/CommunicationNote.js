﻿$.extend(Patient, {
    CommunicationNote: {
        Delete: function(id, patientId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this communication note?",
                Yes: function() { U.GridAjaxCommand("Patient/CommunicationNote/Delete", { id: id, patientId: patientId }, $grid) }
            })
        },
        Edit: function(id, patientId) {
            Acore.Open("EditCommunicationNote", { id: id, patientId: patientId });
        },
        Export: function() {
            U.GetAttachment("Patient/CommunicationNote/Export", {
                patientId: $("#ListCommunicationNotes_PatientName").val(),
                branchId: $("#ListCommunicationNotes_BranchId").length ? $("#ListCommunicationNotes_BranchId").val() : U.GuidEmpty,
                status: $("#ListCommunicationNotes_Status").length ? $("#ListCommunicationNotes_Status").val() : "0",
                startDate: $("#ListCommunicationNotes_StartDate").length ? $("#ListCommunicationNotes_StartDate").val() : "",
                endDate: $("#ListCommunicationNotes_EndDate").length ? $("#ListCommunicationNotes_EndDate").val() : ""
            });
        },
        InitEdit: function(r, t, x, e) {
            Patient.CommunicationNote.InitShared(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            if ($("#AgencyCommunicationNotes_PatientName").val() != U.GuidEmpty) {
                $(".filter.optional", e).remove();
                $(".filter.patient", e).hide();
            }
            $(".new-communication-note", e).click(function() {
                Patient.CommunicationNote.New();
                return false;
            });
            $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                Patient.CommunicationNote.RefreshList();
                return false;
            });
            $(".grid-controls select,.grid-controls .date-picker", e).change(function() { Patient.CommunicationNote.RefreshList() });
            $(".export", e).click(function() {
                Patient.CommunicationNote.Export();
                return false;
            })
        },
        InitNew: function(r, t, x, e) {
            Patient.CommunicationNote.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            Schedule.InitEpisodeDropDown(e);
            U.ShowIfChecked($("[name=SendAsMessage]", e), $(prefix + "Recipients"));
            U.InitShared(r, t, x, e);
        },
        List: function(patientId) {
            Acore.Open("ListCommunicationNotes", { patientId: patientId });
        },
        Logs: function(id) {
            Acore.Open("CommunicationNoteLogs", { id: id });
        },
        New: function(patientId) {
            Acore.Open("NewCommunicationNote", { patientId: patientId });
        },
        Print: function(id, patientId, episodeId, QA) {
            if (id == undefined) U.GetAttachment("Patient/Order/Print", {});
            else {
                var Arguments = {
                    Url: "Patient/CommunicationNote/PrintPreview",
                    PdfUrl: "Patient/CommunicationNote/Print",
                    Data: { EventId: id, PatientId: patientId, EpidsodeId: episodeId }
                };
                if (QA) $.extend(Arguments, {
                    ReturnClick: function() { Patient.CommunicationNote.Return(id, patientId, episodeId) },
                    Buttons: [
                        { Text: "Edit", Click: function() { Patient.CommunicationNote.Edit(id) } },
                        { Text: "Approve", Click: function() { Patient.CommunicationNote.Approve(id, patientId, episodeId) } }
                    ]
                });
                Acore.OpenPrintView(Arguments);
            }
        },
        RefreshList: function() {
            $("#AgencyCommunicationNotesGridContainer").Load("Patient/CommunicationNote/Grid", {
                patientId: $("#AgencyCommunicationNotes_PatientName").val(),
                branch: $("#AgencyCommunicationNotes_BranchId").val(),
                status: $("#AgencyCommunicationNotes_Status").val(),
                start: $("#AgencyCommunicationNotes_StartDate").val(),
                end: $("#AgencyCommunicationNotes_EndDate").val(),
                sort: $("#AgencyCommunicationNotes_SortParams").val()
            }, function(r, t, x, e) {
                e.closest(".wrapper").find(".grid-search input").keyup()
            })
        }
    }
});