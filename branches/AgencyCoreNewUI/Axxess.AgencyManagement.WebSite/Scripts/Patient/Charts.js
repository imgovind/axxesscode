﻿$.extend(Patient, {
    Charts: {
        AcoreId: "PatientCharts",   // Acore Window ID for Patient Charts
        CookieId: "PatientCharts",  // Name for cookie to store previous state
        PatientId: "",              // Current Active Patient
        Discipline: "",             // Current Active Discipline
        Date: "",                   // Current Active Date Range
        StartDate: "",              // If Custom Date Range, Current Start Date
        EndDate: "",                // If Custom Date Range, Current End Date
        // Initialize each row in schedule activity grid
        ActivityRowBind: function(e) {
            // Initalize all tooltips
            ToolTip.Init(e.row);
            // Add effects for completed and orphaned tasks
            if (e.dataItem.IsComplete) $(e.row).addClass("completed");
            if (e.dataItem.IsOrphaned) {
                $(e.row).addClass("black").tooltip({ track: true, showURL: false, top: 5, left: 5, extraClass: "error",
                    bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
                });
                $(e.row.cells[1]).addClass("error");
            }
            // Setup context menu
            $(e.row).bind("contextmenu", function(Event) {
                var Menu = $("<ul/>");
                if (!e.dataItem.IsComplete) Menu.append(
                    $("<li/>", { text: "Edit Note" }).click(function() {
                        $(e.row).find("a:first").click();
                    })).append(
                    $("<li/>", { text: "Print" }).click(function() {
                        $(e.row).find(".print").parent().click();
                    }));
                $("td:last a", e.row).each(function() {
                    Menu.append(
                        $("<li/>", { text: $(this).text() }).click(function() {
                            $(this).click();
                        })
                    )
                });
                Menu.ContextMenu(Event);
            })
        },
        ChangeDiscipline: function(discipline) {
            Patient.Charts.Discipline = discipline;
            Patient.Charts.Refresh();
        },
        // Initialize Schedule Center
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            // Set new patient button
            $(".buttons .new-patient", e).click(function() {
                Patient.New();
                return false;
            });
            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() { Patient.Charts.PatientSelector.Filter(e) });
            $(".ui-layout-west .top select", e).change(function() { Patient.Charts.PatientSelector.Rebind(e) });
        },
        // Initialize Main/Content section
        InitContent: function(r, t, x, e) {
            Patient.Charts.Discipline = $(".discipline", e).val();
            Patient.Charts.Date = $(".date", e).val();
            Patient.Charts.StartDate = $(".start-date", e).val();
            Patient.Charts.EndDate = $(".end-date", e).val();
            U.ShowIfSelectEquals($(".date", e), "DateRange", $(".date-range", e));
            $(".new-order", e).click(function() {
                Patient.Order.New(Patient.Charts.PatientId);
                return false;
            });
            $(".new-communication-note", e).click(function() {
                Patient.CommunicationNote.New(Patient.Charts.PatientId);
                return false;
            });
            $(".new-authorization", e).click(function() {
                Patient.Authorization.New(Patient.Charts.PatientId);
                return false;
            });
            $(".reassign-schedule", e).click(function() {
                Schedule.ReassignMultiple(U.GuidEmpty, Patient.Charts.PatientId, "Patient");
                return false;
            });
            $(".patient-profile", e).click(function() {
                Patient.Profile(Patient.Charts.PatientId);
                return false;
            });
            $(".medication-profile", e).click(function() {
                Patient.Medication.Profile(Patient.Charts.PatientId);
                return false;
            });
            $(".allergy-profile", e).click(function() {
                Patient.Allergy.Profile(Patient.Charts.PatientId);
                return false;
            });
            $(".authorization-list", e).click(function() {
                Patient.Authorization.List(Patient.Charts.PatientId);
                return false;
            });
            $(".communication-note-list", e).click(function() {
                Patient.CommunicationNote.List(Patient.Charts.PatientId);
                return false;
            });
            $(".order-list", e).click(function() {
                Patient.Order.History(Patient.Charts.PatientId);
                return false;
            });
            $(".sixty-day-list", e).click(function() {
                Patient.SixtyDaySummaryList(Patient.Charts.PatientId);
                return false;
            });
            $(".vital-signs-log", e).click(function() {
                Patient.VitalSignsList(Patient.Charts.PatientId);
                return false;
            });
            $(".triage-classification", e).click(function() {
                Patient.TriageClassificationPrint(Patient.Charts.PatientId);
                return false;
            });
            $(".deleted-tasks", e).click(function() {
                Patient.DeletedTasksList(Patient.Charts.PatientId);
                return false;
            });
            $(".schedule-center", e).click(function() {
                Schedule.Center.Load(Patient.Charts.PatientId);
                return false;
            });
            $(".bottom .above select", e).change(function() {
                Patient.Charts.Discipline = $(".discipline", e).val();
                Patient.Charts.Date = $(".date", e).val();
                Patient.Charts.Refresh();
            });
            $(".bottom .above input", e).blur(function() {
                Patient.Charts.StartDate = $(".start-date", e).val();
                Patient.Charts.EndDate = $(".end-date", e).val();
                Patient.Charts.Refresh();
            });
            Patient.Charts.InitInfo(r, t, x, e);
        },
        InitInfo: function(r, t, x, e) {
            $(".change-photo", e).click(function() {
                Patient.Photo.Edit(Patient.Charts.PatientId);
                return false;
            });
            $(".edit", e).click(function() {
                Patient.Edit(Patient.Charts.PatientId);
                return false;
            });
            $(".more-info", e).click(function() {
                Patient.Details(Patient.Charts.PatientId);
                return false;
            });
            $(".refresh", e).click(function() {
                Patient.Charts.Refresh();
                return false;
            });
            $(".change-status", e).click(function() {
                Patient.ChangeStatus(Patient.Charts.PatientId);
                return false;
            });
            $(".readmit", e).click(function() {
                Patient.ReAdmit(Patient.Charts.PatientId);
                return false;
            });
        },
        Load: function(patientId) {
            if (Acore.Windows[Patient.Charts.AcoreId].IsOpen) {
                Patient.Charts.LoadContent(patientId);
                $("#window_" + Patient.Charts.AcoreId).WinFocus();
            } else {
                Patient.Charts.PatientId = patientId;
                Acore.Open(Patient.Charts.AcoreId);
            }
        },
        LoadActivities: function(patientId, discipline, date, startDate, endDate) {
            var e = $("#window_" + Patient.Charts.AcoreId + "_content");
            U.RebindGrid($(".ui-layout-center .bottom .t-grid", e), {
                patientId: patientId,
                discipline: discipline,
                dateRangeId: date,
                rangeStartDate: startDate,
                rangeEndDate: endDate
            });
            Patient.Charts.PatientId = patientId;
            $(".discipline", e).val(Patient.Charts.Discipline = discipline);
            $(".date", e).val(Patient.Charts.Date = date);
            $(".start-date", e).val(Patient.Charts.StartDate = startDate);
            $(".end-date", e).val(Patient.Charts.EndDate = endDate);
        },
        LoadContent: function(patientId) {
            var e = $("#window_" + Patient.Charts.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll)
            $(".ui-layout-center", e).Load("Patient/Charts/Content", { patientId: patientId }, Patient.Charts.InitContent);
            Patient.Charts.SaveState(patientId);
        },
        LoadInfo: function(patientId) {
            var e = $("#window_" + Patient.Charts.AcoreId + "_content");
            $(".patient-summary", e).Load("Patient/Charts/Info", { patientId: patientId }, Patient.Charts.InitInfo);
        },
        PatientSelector: {
            Init: function() {
                var PreviousState = U.Cookie.Get(Patient.Charts.CookieId);
                if (PreviousState) Patient.Charts.PatientId = PreviousState.PatientId;
                Patient.Charts.PatientSelector.Filter($("#window_" + Patient.Charts.AcoreId));
                var e = $("#window_" + Patient.Charts.AcoreId + "_content");
                if (U.IsGuid(Patient.Charts.PatientId) && $(".ui-layout-west .t-last:contains(" + Patient.Charts.PatientId + ")", e).length) Patient.Charts.LoadContent(Patient.Charts.PatientId);
                else if ($(".ui-layout-west .t-last", e).length) Patient.Charts.LoadContent($(".ui-layout-west .t-last:first", e).text());
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
            },
            Filter: function(e) {
                var text = $(".ui-layout-west .top input", e).val();
                if (text && text.length) {
                    search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            },
            Rebind: function(e) {
                U.RebindGrid($(".ui-layout-west .t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    statusId: $("[name=StatusId]", e).val(),
                    paymentSourceId: $("[name=PaymentSourceId]").val()
                })
            },
            Select: function(e) {
                Patient.Charts.LoadContent($(".t-last", e.row).text());
            }
        },
        Refresh: function() {
            var e = $("#window_" + Patient.Charts.AcoreId + "_content");
            Patient.Charts.LoadActivities(Patient.Charts.PatientId, Patient.Charts.Discipline, Patient.Charts.Date, Patient.Charts.StartDate, Patient.Charts.EndDate);
            Patient.Charts.LoadInfo(Patient.Charts.PatientId);
        },
        SaveState: function(patientId) {
            Patient.Charts.PatientId = patientId;
            U.Cookie.Set(Patient.Charts.CookieId, { PatientId: patientId });
        }
    }
});
