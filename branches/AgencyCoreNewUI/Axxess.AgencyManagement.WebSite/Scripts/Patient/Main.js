﻿var Patient = {
    Admit: function(id, type) {
        Acore.Modal({
            Name: "Admit Patient",
            Url: "Patient/Admit/New",
            Input: { id: id, type: type },
            OnLoad: U.InitShared,
            Width: 800
        })
    },
    ChangeStatus: function(id) {
        Acore.Modal({
            Name: "Change Patient Status",
            Url: "Patient/Status/Edit",
            Input: { id: id },
            OnLoad: Patient.InitEditStatus,
            Width: 800
        })
    },
    Delete: function(id) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this patient?",
            Yes: function() {
                U.PostUrl("Patient/Delete", { id: id }, function(result) {
                    U.GrowlResult(result);
                    U.RebindAllGrids()
                })
            }
        })
    },
    DeletedTasksList: function(id) {
        Acore.Open("DeletedTasksList", { id: id });
    },
    Details: function(id) {
        Acore.Dialog({
            Name: "Patient Details",
            Url: "Patient/Details",
            Input: { id: id },
            Width: 350
        });
    },
    Edit: function(id) {
        Acore.Open("EditPatient", { id: id });
    },
    Eligibility: function(e) {
        var MedicareNumber = $("input[name=MedicareNumber]").val(),
            LastName = $("input[name=LastName]").val(),
            FirstName = $("input[name=FirstName]").val(),
            DOB = $("input[name=DOB]").val(),
            Gender = $("input[name=Gender]:checked").val();
        if (MedicareNumber == "" || LastName == "" || FirstName == "" || DOB == "" || Gender == undefined) {
            var error = "Unable to process Medicare eligibility request:";
            if (MedicareNumber == "") error += "<br /> &#8226; Medicare Number is Required";
            if (LastName == "") error += "<br /> &#8226; Last Name is Required";
            if (FirstName == "") error += "<br /> &#8226; First Name is Required";
            if (DOB == "") error += "<br /> &#8226; Date of Birth is Required";
            if (Gender == undefined) error += "<br /> &#8226; Gender is Required";
            U.Growl(error, "error");
        } else {
            Acore.Modal({
                Name: "Medicare Eligibility",
                Url: "Patient/Eligibility",
                Input: {
                    medicareNumber: MedicareNumber,
                    lastName: LastName,
                    firstName: FirstName,
                    dob: DOB,
                    gender: Gender
                },
                Width: 800
            })
        }
    },
    Export: function(branch, status) {
        U.GetAttachment("Patient/Export", { branch: branch, status: status });
    },
    ExportPending: function() {
        U.GetAttachment("Patient/PendingAdmission/Export", {});
    },
    ExportDeleted: function(branchId) {
        U.GetAttachment("Patient/Deleted/Export", { branchId: branchId });
    },
    InitDeletedList: function(r, t, x, e) {
        $(".grid-refresh", e).click(function() {
            Patient.RefreshDeleted();
            return false;
        });
        $(".export", e).click(function() {
            Patient.ExportDeleted($("[name=BranchId]", e).val());
            return false;
        });
        $("[name=BranchId]", e).change(function() { Patient.RefreshDeleted() });
    },
    InitEdit: function(r, t, x, e) {
        var Type = U.GetType(e), Prefix = "#" + Type + "_";
        $(".new-emergency-contact", e).click(function() {
            Patient.EmergencyContact.New($(Prefix + "Id").val());
            return false;
        });
        $(".add-patient-physician", e).click(function() {
            var $physician = $(this).closest(".row").find(".physician-picker"), physicianId = $physician.siblings("input[type=hidden]").val();
            $physician.AjaxAutocomplete("reset");
            if (U.IsGuid(physicianId)) Patient.Physician.Add(physicianId, $(Prefix + "Id").val(), $(Prefix + "PhysicianGrid"));
            else U.Growl("Please Select a Physician", "error");
        });
        $(".activity-log", e).click(function() {
            Patient.Logs($(Prefix + "Id").val());
            return false;
        });
        Patient.InitShared(r, t, x, e);
    },
    InitEditStatus: function(r, t, x, e) {
        e.addClass("loading");
        var type = U.GetType(e), prefix = "#" + type + "_", win = e.closest(".window");
        U.ShowIfSelectEquals($(prefix + "Status"), "2", $(prefix + "DischargeContainer"));
        U.ShowIfSelectEquals($(prefix + "Status"), "4", $(prefix + "NonAdmissionContainer"));
        $(prefix + "Status").change(function() {
            e.addClass("loading");
            setTimeout(function() { win.SetHeight(false) }, 50)
        });
        setTimeout(function() { win.SetHeight(false) }, 1000);
        $("form", e).Validate({
            Success: function(result, form) {
                if (Acore.Windows[Patient.Charts.AcoreId].IsOpen) Patient.Charts.PatientSelector.Rebind($("#window_" + Patient.Charts.AcoreId));
                if (Acore.Windows[Schedule.Center.AcoreId].IsOpen) Schedule.Center.PatientSelector.Rebind($("#window_" + Schedule.Center.AcoreId));
            }
        });
    },
    InitList: function(r, t, x, e) {
        $(".new-patient", e).click(function() {
            Patient.New();
            return false;
        });
        $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
            Patient.RefreshList();
            return false;
        });
        $(".grid-controls select", e).change(function() { Patient.RefreshList() });
        $(".export", e).click(function() {
            Patient.Export($("#ListPatient_BranchId").val(), $("#ListPatient_Status").val());
            return false;
        });
    },
    InitNew: function(r, t, x, e) {
        var type = U.GetType(e), prefix = "#" + type + "_";
        $(".addrem", e).click(function() {
            Patient.Physician.AddRemove($(this));
            return false;
        });
        $(prefix + "VerifyMedicare").click(function() {
            Patient.Eligibility($(this).closest("fieldset"));
            return false;
        });
        $(".admit", e).click(function() {
            $(prefix + "Status").val("1");
        });
        Patient.InitShared(r, t, x, e);
    },
    InitNonAdmissionList: function(r, t, x, e) {
        $(".new-patient", e).click(function() {
            Patient.New();
            return false;
        });
    },
    InitPendingList: function(r, t, x, e) {
        $(".new-patient", e).click(function() {
            Patient.New();
            return false;
        });
        $(".export", e).click(function() {
            Patient.ExportPending();
            return false;
        });
    },
    InitShared: function(r, t, x, e) {
        $(".insurance", e).each(function() {
            if ($(":selected", this).attr("ishmo") == "1") $(this).closest(".row").next(".more").show().find("input:eq(0)").addClass("required");
            else $(this).closest(".row").next(".more").hide().find("input").removeClass("required");
        });
        $(".insurance", e).change(function() {
            if ($(":selected", this).attr("ishmo") == "1") $(this).closest(".row").next(".more").show().find("input:eq(0)").addClass("required");
            else {
                $(this).closest(".row").next(".more").hide().find("input:eq(0)").removeClass("required");
                if ($(":selected", this).val() == "new") {
                    Acore.Open("NewInsurance");
                    this.selectedIndex = 0;
                } else if ($(":selected", this).val() == "spacer") this.selectedIndex = 0;
            }
        })
        U.InitShared(r, t, x, e);
    },
    InitVitalSignsList: function(r, t, x, e) {
        $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
            Patient.VitalSignsRefresh();
            return false;
        });
        $(".grid-controls .date-picker", e).change(function() { Patient.VitalSignsRefresh() });
        $(".export", e).click(function() {
            Patient.VitalSignsExport($("[name=PatientId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
            return false;
        })
    },
    Logs: function(id) {
        Acore.Open("PatientLogs", { id: id });
    },
    New: function() {
        Acore.Open("NewPatient");
    },
    NonAdmit: function(id) {
        Acore.Modal({
            Name: "Non-Admit Patient",
            Url: "Patient/NonAdmit/New",
            Input: { id: id },
            OnLoad: U.InitShared,
            Width: 800
        })
    },
    Profile: function(id) {
        Acore.OpenPrintView({
            PreviewUrl: "Patient/Profile/PrintPreview/" + id,
            PdfUrl: "Patient/Profile/Print/" + id
        })
    },
    RefreshList: function() {
        $("#ListPatient_Content").Load("Patient/Grid", {
            branch: $("#ListPatient_BranchId").val(),
            status: $("#ListPatient_Status").val(),
            sort: $("#ListPatient_SortParams").val()
        }, function(r, t, x, e) {
            $(".grid-search input").keyup()
        })
    },
    RefreshDeleted: function() {
        $("#ListDeletedPatients_Content").Load("Patient/Deleted/Grid", { branchId: $("#ListDeletedPatients_BranchId").val() }, function(r, t, x, e) { $(".grid-search input").keyup() });
    },
    Restore: function(id) {
        Acore.Confirm({
            Message: "Are you sure you want to restore this patient?",
            Yes: function() {
                U.PostUrl("Patient/Restore", { id: id }, function(result) {
                    U.GrowlResult(result);
                    U.RebindAllGrids()
                })
            }
        })
    },
    SixtyDaySummaryList: function(id) {
        Acore.Open("SixtyDaySummaryList", { id: id });
    },
    TriageClassificationPrint: function(id) {
        Acore.OpenPrintView({
            PreviewUrl: "Patient/TriageClassification/PrintPreview/" + id,
            PdfUrl: "Patient/TriageClassification/Print/" + id
        })
    },
    VitalSignsExport: function(id, startDate, endDate) {
        U.GetAttachment("Patient/VitalSigns/Export", { PatientId: id, StartDate: startDate, EndDate: endDate })
    },
    VitalSignsList: function(id) {
        Acore.Open("VitalSignsList", { id: id });
    },
    VitalSignsRefresh: function(id) {
        $("#PatientVitalSigns_Content").Load("Patient/VitalSigns/Grid", {
            PatientId: $("#PatientVitalSigns_PatientId").val(),
            StartDate: $("#PatientVitalSigns_StartDate").val(),
            EndDate: $("#PatientVitalSigns_EndDate").val()
        })
    }
}
