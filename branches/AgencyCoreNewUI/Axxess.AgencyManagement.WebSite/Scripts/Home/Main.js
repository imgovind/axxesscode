﻿var Home = {
    CookieId: "DashboardOrder",
    Init: function() {
        var order = U.Cookie.Get(Home.CookieId);
        if (order) for (var i = 0; i < order.length; i++) for (var j = 0; j < order[i].length; j++) $("#" + order[i][j]).appendTo("#widget-column-" + String(i + 1));
        $(".widget:not(.stationary)", ".widgets").disableSelection().find(".widget-head").css("cursor", "move").mousedown(function() {
            $(".widget:not(.stationary)", ".widgets").css("width", "");
            $(this).parent().css("width", $(this).parent().width());
        }).mouseup(function() {
            $(this).parent().hasClass("dragging") ? $(".widgets").sortable("disable") : $(this).parent().css("width", "")
        });
        $(".widgets").sortable({
            items: $(".widget:not(.stationary)", ".widgets"),
            connectWith: $(".widgets"),
            handle: ".widget-head",
            placeholder: "widget-place-holder",
            forcePlaceholderSize: true,
            revert: 300,
            delay: 100,
            opacity: 0.8,
            containment: "document",
            start: function(e, ui) {
                $(ui.helper).addClass("dragging");
            },
            stop: function(e, ui) {
                $(ui.item).css("width", "").removeClass("dragging");
                $(".widgets").sortable("enable");
                var col = $(ui.item).closest(".widgets");
                while (col.find(".widget").length > 3) {
                    if (col.index() + 1 == $(".widgets").length) move = $(".widgets").first();
                    else move = col.next();
                    if ($(".stationary", move).length) $(".stationary", move).last().after(col.find(".widget").last());
                    else move.prepend(col.find(".widget").last());
                    col = move;
                }
                Home.SetState();
            }
        });
        if ($("#custom-message-widget").length) Home.InitCustomMessage();
        if ($("#news-widget").length) Home.InitNews();
        if ($("#past-due-recert-widget").length) Home.InitPastDueRecert();
        if ($("#local-widget").length) Home.InitLocal();
        if ($("#user-schedule-widget").length) Home.InitUserSchedule();
        if ($("#upcoming-recert-widget").length) Home.InitUpcomingRecert();
        if ($("#message-widget").length) Home.InitMessage();
        if ($("#birthday-widget").length) Home.InitBirthday();
        if ($("#outstanding-claim-widget").length) Home.InitOutstandingClaim();
    },
    InitBirthday: function() {
        $(".widget-more,.widget-more a", "#birthday-widget").click(function() {
            Report.Load("Report/Patient/Birthdays");
            return false;
        });
        U.PostUrl("Report/PatientBirthdayWidget", { page: 1, size: 5, orderBy: "", groupBy: "", filter: "" }, function(Birthday) {
            if (Birthday.Data != undefined && Birthday.Data.length > 0) for (var i = 0; i < Birthday.Data.length && i < 5; i++) $(".widget-content tbody", "#birthday-widget").append(
                    $("<tr/>").append(
                        $("<td/>").text(Birthday.Data[i].BirthDay)).append(
                        $("<td/>").text(Birthday.Data[i].Age)).append(
                        $("<td/>").text(Birthday.Data[i].Name)).append(
                        $("<td/>").text(Birthday.Data[i].PhoneHomeFormatted))).closest(".widget").find(".widget-more").show();
            else $(".widget-content tbody", "#birthday-widget").append(
                    $("<tr/>").append(
                        $("<td/>").attr("colspan", "4").append(
                            $("<h1/>", { "class": "blue", text: "No Birthdays Found" })))).closest(".widget").find(".widget-more").hide();
        });
    },
    InitCustomMessage: function() {
        if (Acore.GetRemoteContent) {
            U.PostUrl("Message/CustomMessageWidget", { page: 1, size: 5, orderBy: "", groupBy: "", filter: "" }, function(Message) {
                if (Message != undefined && Message.Text != undefined && Message.Text.length > 0) $(".widget-content", "#custom-message-widget").html(Message.Text);
                else $(".widget-content", "#custom-message-widget").empty().append(
                    $("<div/>").addClass("align-center").append(
                        $("<span/>").addClass("bigtext").text("Welcome to")).append(
                        $("<br/>")).append(
                        $("<span/>").addClass("img acorelogo"))).append(
                    $("<div/>").html("AgencyCore&#8482; is a secure Enterpise Agency Management software designed from the ground up to provide powerful online capabilities that enables real-time collaboration for you and your team."));
            })
        }
    },
    InitLocal: function() {
        $("#local-widget .widget-head h5").text(U.DisplayDate());
        $.simpleWeather({
            zipcode: $("#weather-zip").val(),
            unit: "f",
            success: function(Weather) {
                $(".city", "#weather-info").html(Weather.city + ", " + Weather.region);
                $(".temp", "#weather-info").html(Weather.temp + "&#176; " + Weather.units.temp);
                $(".desc", "#weather-info").html(Weather.currently);
                $(".wind", "#weather-info").html("Wind: " + Weather.wind.direction + " " + Weather.wind.speed + " " + Weather.units.speed);
                $(".range", "#weather-info").html("High: " + Weather.high + "&#176; " + Weather.units.temp + " Low: " + Weather.low + "&#176; " + Weather.units.temp);
                $(".link a", "#weather-info").attr("href", Weather.link);
                $("#weather-image").css("background-image", "url(" + Weather.image + ")");
            },
            error: function(error) {
                $("#weather-info").html(
                    $("<h1/>", { "class": "blue", text: error }));
            }
        });
        $("#digital-clock").jdigiclock();
    },
    InitMessage: function() {
        $(".widget-more,li:eq(0)", "#message-widget").click(function() { Message.Inbox() });
        $("li:eq(1)", "#message-widget").click(function() {
            Acore.Open("NewMessage");
            return false;
        });
        $("li:eq(2)", "#message-widget").click(function() {
            if ($("input:checked", "#message-widget .widget-message-list").length) Acore.Confirm({
                Message: "Are you sure you want to delete selected messages?",
                Yes: function() {
                    for (i = 0; i < $("input:checked", "#message-widget").length; i++) {
                        if (i != $("input:checked", "#message-widget").length - 1) Message.Delete($("input:checked", "#message-widget").eq(i).attr("guid"), $("input:checked", "#message-widget").eq(i).attr("messagetype"), true, function() { });
                        else Message.Delete($("input:checked", "#message-widget").eq(i).attr("guid"), $("input:checked", "#message-widget").eq(i).attr("messagetype"), true, function() {
                            U.Growl("Messages Deleted Successfully");
                            $("#message-widget .widget-message-list").empty();
                            Message.GetMessagesWidget($(".widget-message-list", "#message-widget"));
                        })
                    }
                }
            });
            else U.Growl("Please select the messages you would like to delete", "error");
        });
        Message.GetMessagesWidget($(".widget-message-list", "#message-widget"));
    },
    InitNews: function() { },
    InitOutstandingClaim: function() {
        $(".widget-more:eq(0),.widget-more a:eq(0)", "#outstanding-claim-widget").click(function() {
            Billing.Claim.RAP.List();
            return false;
        });
        $(".widget-more:eq(1),.widget-more a:eq(1)", "#outstanding-claim-widget").click(function() {
            Billing.Claim.Final.List();
            return false;
        });
        U.PostUrl("Billing/Unprocessed", {}, function(OutstandingClaim) {
            if (OutstandingClaim != undefined && OutstandingClaim.length > 0) for (var i = 0; i < OutstandingClaim.length && i < 5; i++) $(".widget-content tbody", "#outstanding-claim-widget").append(
                $("<tr/>").append(
                    $("<td/>").text(OutstandingClaim[i].LastName + ", " + OutstandingClaim[i].FirstName)).append(
                    $("<td/>").text(OutstandingClaim[i].Type)).append(
                    $("<td/>").text(OutstandingClaim[i].EpisodeRange))).closest(".widget").find(".widget-more").show();
            else $(".widget-content tbody", "#outstanding-claim-widget").append(
                $("<tr/>").append(
                    $("<td/>").attr("colspan", "3").append(
                        $("<h1/>", { "class": "blue", text: "No Outstanding Claims Found" })))).closest(".widget").find(".widget-more").hide();
        })
    },
    InitPastDueRecert: function() {
        $(".widget-more,.widget-more a", "#past-due-recert-widget").click(function() {
            Agency.Recerts.PastDue.List();
            return false;
        });
        U.PostUrl("Agency/Recerts/PastDue/Widget", { page: 1, size: 5, orderBy: "", groupBy: "", filter: "" }, function(PastDueRecert) {
            if (PastDueRecert != undefined && PastDueRecert.length > 0) for (var i = 0; i < PastDueRecert.length && i < 5; i++) $(".widget-content tbody", "#past-due-recert-widget").append(
                $("<tr/>").append(
                    $("<td/>").text(PastDueRecert[i].PatientName)).append(
                    $("<td/>").text(PastDueRecert[i].PatientIdNumber)).append(
                    $("<td/>").text(PastDueRecert[i].TargetDateFormatted))).closest(".widget").find(".widget-more").show();
            else $(".widget-content tbody", "#past-due-recert-widget").append(
                $("<tr/>").append(
                    $("<td/>").attr("colspan", "3").append(
                        $("<h1/>", { "class": "blue", text: "No Past Due Recertifications Found" })))).closest(".widget").find(".widget-more").hide();
        })
    },
    InitUpcomingRecert: function() {
        $(".widget-more,.widget-more a", "#upcoming-recert-widget").click(function() {
            Agency.Recerts.Upcoming.List();
            return false;
        });
        U.PostUrl("Agency/Recerts/Upcoming/Widget", { page: 1, size: 5, orderBy: "", groupBy: "", filter: "" }, function(UpcomingRecert) {
            if (UpcomingRecert != undefined && UpcomingRecert.length > 0) for (var i = 0; i < UpcomingRecert.length && i < 5; i++) $(".widget-content tbody", "#upcoming-recert-widget").append(
                $("<tr/>").append(
                    $("<td/>").text(UpcomingRecert[i].PatientName)).append(
                    $("<td/>").text(UpcomingRecert[i].PatientIdNumber)).append(
                    $("<td/>").text(UpcomingRecert[i].TargetDateFormatted))).closest(".widget").find(".widget-more").show();
            else $(".widget-content tbody", "#upcoming-recerts-widget").append(
                $("<tr/>").append(
                    $("<td/>").attr("colspan", "3").append(
                        $("<h1/>", { "class": "blue", text: "No Upcoming Recertifications Found" })))).closest(".widget").find(".widget-more").hide();
        })
    },
    InitUserSchedule: function() {
        $(".widget-more,.widget-more a", "#user-schedule-widget").click(function() {
            User.Schedule.Open();
            return false;
        });
        U.PostUrl("User/ScheduleWidget", { page: 1, size: 5, orderBy: "", groupBy: "", filter: "" }, function(UserSchedule) {
            if (UserSchedule != undefined && UserSchedule.length > 0) for (var i = 0; i < UserSchedule.length && i < 5; i++) $(".widget-content tbody", "#user-schedule-widget").append(
                $("<tr/>").append(
                    $("<td/>").text(UserSchedule[i].PatientName)).append(
                    $("<td/>").text(UserSchedule[i].TaskName)).append(
                    $("<td/>").text(UserSchedule[i].EventDate))).closest(".widget").find(".widget-more").show();
            else $(".widget-content tbody", "#user-schedule-widget").append(
                $("<tr/>").append(
                    $("<td/>").attr("colspan", "3").append(
                        $("<h1/>", { "class": "blue", text: "No Scheduled Tasks Found" })))).closest(".widget").find(".widget-more").hide();
        })
    },
    SetState: function() {
        var order = new Array(3);
        for (var i = 0; i < 3; i++) {
            order[i] = new Array();
            $("#widget-column-" + String(i + 1) + " .widget").each(function() { order[i].push($(this).attr("id")) });
        }
        U.Cookie.Set(Home.CookieId, order);
    }
}