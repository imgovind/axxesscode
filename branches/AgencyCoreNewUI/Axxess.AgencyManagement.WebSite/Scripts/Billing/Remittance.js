﻿$.extend(Billing, {
    Remittance: {
        Detail: {
            AcoreId: "RemittanceDetail",
            Delete: function(id) {
                Acore.Confirm({
                    Message: "Are you sure you want to delete this remittance?",
                    Yes: function() {
                        U.PostURL("Billing/Remittance/Detail/Delete", { id: id }, function(result) {
                            U.GrowlResult(result);
                            if (result.isSuccessful) U.RebindAllGrids();
                        })
                    }
                })
            },
            Load: function(id) { Acore.Open(Billing.Remittance.Detail.AcoreId, { id: id }) },
            Init: function(r, t, x, e) {
                $(".grid-refresh", e).click(function() {
                    Billing.Remittance.Detail.Refresh();
                    return false;
                });
                $(".print", e).click(function() {
                    Billing.Remittance.Detail.Print($("[name=Id]", e).val());
                    return false;
                });
                $(".post", e).click(function() {
                    if ($(this).hasClass("all")) var control = $(".content", e);
                    else var control = $(this).closest(".buttons").siblings(".acore-grid");
                    Billing.Remittance.Detail.Post($("[name=Id]", e).val(), control);
                    return false;
                });
            },
            Post: function(id, control) {
                var data = $("[type=checkbox]", $(control)).serializeArray();
                if (data != null && data != undefined && data.length > 0) {
                    data[data.length] = { name: "id", value: id };
                    U.PostUrl("Billing/Remittance/Detail/Post", data, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) Billing.Remittance.Detail.Refresh();
                    });
                } else U.Growl("Please select at least one claim to post.", "error");
            },
            Print: function(id) {
                Acore.OpenPrintView({
                    PreviewUrl: "Billing/Remittance/Detail/PrintPreview/" + id,
                    PdfUrl: "Billing/Remittance/Detail/Print/" + id
                })
            },
            Refresh: function() {
                var e = $("#window_" + Billing.Remittance.Detail.AcoreId + "_content");
                $(".content", e).Load("Billing/Remittance/Detail/Grid", { id: $("[name=Id]", e).val() }, function() {
                    $(".content .post", e).click(function() {
                        Billing.Remittance.Detail.Post($("[name=Id]", e).val(), $(this).closest(".buttons").siblings(".acore-grid"));
                        return false;
                    })
                })
            }
        },
        List: {
            AcoreId: "ListRemittances",
            Init: function(r, t, x, e) {
                $(".filter .date-picker", e).change(function() { Billing.Remittance.List.Refresh() });
                $(".grid-refresh", e).click(function() {
                    Billing.Remittance.List.Refresh();
                    return false;
                });
                $(".print", e).click(function() {
                    Billing.Remittance.List.Print($("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                    return false;
                });
                U.InitShared(r, t, x, e);
            },
            Print: function(startDate, endDate) {
                Acore.OpenPrintView({
                    PreviewUrl: "Billing/Remittance/List/PrintPreview/" + U.URIDate(startDate) + "/" + U.URIDate(endDate),
                    PdfUrl: "Billing/Remittance/List/Print/" + U.URIDate(startDate) + "/" + U.URIDate(endDate)
                })
            },
            Refresh: function() {
                var e = $("#window_" + Billing.Remittance.List.AcoreId + "_content");
                $(".content", e).Load("Billing/Remittance/List/Grid", { startDate: $("[name=StartDate]", e).val(), endDate: $("[name=EndDate]", e).val() })
            }
        }
    }
});