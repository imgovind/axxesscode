﻿$.extend(Billing, {
    Medicare: {
        AcoreId: "MedicareClaimsHistory",
        // Initialize Schedule Center
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() { Billing.Medicare.Filter(e) });
            $(".ui-layout-west .top select", e).change(function() { Billing.Medicare.Rebind(e) });
        },
        InitSelecter: function() {
            Billing.Medicare.Filter($("#window_" + Billing.Medicare.AcoreId));
            var e = $("#window_" + Billing.Medicare.AcoreId + "_content");
            if (U.IsGuid(Billing.PatientId) && $(".ui-layout-west .t-last:contains(" + Billing.PatientId + ")", e).length) { Billing.Medicare.LoadContent(Billing.PatientId); }
            else if ($(".ui-layout-west .t-last", e).length) { Billing.Medicare.LoadContent($(".ui-layout-west .t-last:first", e).text()); }
            else { $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window.")); }
        },
        Filter: function(e) {
            var text = $(".ui-layout-west .top input", e).val();
            if (text && text.length) {
                var search = text.split(" ");
                $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() { if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match") });
                $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
            } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra().find("tr:first").click();
        },
        LoadContent: function(patientId) {
            var e = $("#window_" + Billing.Medicare.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll)
            $(".ui-layout-center", e).Load("Medicare/Charts/Content", { patientId: patientId, insuranceId: 0 }, function() { });
            Billing.PatientId = patientId;
        },
        Rebind: function(e) {
            U.RebindGrid($(".ui-layout-west .t-grid", e), {
                branchId: $("[name=BranchId]", e).val(),
                statusId: $("[name=StatusId]", e).val(),
                insuranceId: $("[name=InsuranceId]", e).val()
            })
        },
        Select: function(e) { Billing.Medicare.LoadContent($(".t-last", e.row).text()); },
        InitClaim: function(patientId) { U.InitTemplate($("#UpdateClaim_Form"), function() { UserInterface.CloseModal(); Billing.Medicare.Activity.Rebind(patientId); Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val()); Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val()); }, "Claim update is successfully."); },
        InitNewClaim: function(patientId) { U.InitTemplate($("#NewClaim_Form"), function() { UserInterface.CloseModal(); Billing.Medicare.Activity.Rebind(patientId); Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val()); Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val()); }, "Claim is created successfully."); },
        EditClaim: function(patientId, Id, type) {
            Acore.Modal({
                Name: "Edit Claim",
                Url: "Billing/Update",
                Input: { patientId: patientId, id: Id, type: type },
                OnLoad: function(patientId) { Billing.Medicare.InitClaim(patientId); },
                Width: "800px",
                Height: "270px",
                WindowFrame: false
            })
        },
        NewClaim: function(patientId, type) {
            Acore.Modal({
                Name: "New Claim",
                Url: "Billing/NewClaim",
                Input: { patientId: patientId, type: type },
                OnLoad: function(patientId) { Billing.Medicare.InitNewClaim(patientId); },
                Width: "500px",
                Height: "140px",
                WindowFrame: false
            })
        },
        DeleteClaim: function(patientId, id, type) {
            if (confirm("Are you sure you want to delete this claim?")) {
                U.PostUrl("Billing/DeleteClaim", { patientId: patientId, id: id, type: type }, function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        Billing.Medicare.Activity.Rebind(patientId);
                        Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val());
                        Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val());
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error")
                })
            }
        },
        Activity: {
            Init: function() {
                if ($("#BillingMedicareActivityGrid .t-grid-content tr").length) { $('#BillingMedicareActivityGrid .t-grid-content tbody tr.t-master-row').eq(0).click(); }
                else { $("#BillingMedicareClaimsHistoryClaimData").empty(); }
            },
            LoadContent: function(e) { var claimId = $("td.claimId", e.row).text(); var type = $("td.type", e.row).text(); var pId = $("td.pId", e.row).text(); $("#BillingMedicareClaimsHistoryClaimData").Load("Billing/ClaimInfo", { patientId: pId, claimId: claimId, claimType: type }, function() { }); },
            Select: function(e) { Billing.Medicare.Activity.LoadContent(e); },
            Collapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
            Expand: function(e) { $("#BillingMedicareActivityGrid table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#BillingMedicareActivityGrid table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
            RowBound: function(e) { var pd = $("td.pd", e.row).text(); if (pd != null && pd != "" && pd == '01/01/1') { $("td.pd", e.row).text(""); } },
            Rebind: function(patientId) { U.RebindGrid($("#BillingMedicareActivityGrid"), { patientId: patientId, insuranceId: 0 }); }
        }
    }
});