﻿$.extend(Billing.Claim, {
    Final: {
        AcoreId: "ClaimFinalCenter",
        Export: function(branchId, insuranceId, sortType) { U.GetAttachment("Billing/Claim/Final/Export", { branchId: branchId, insuranceId: insuranceId, sortType: sortType }) },
        Init: function(r, t, x, e) {
            $("#FinalVerification_TabStrip").tabs().bind("tabsselect", {
                episodeId: $("#FinalVerification_EpisodeId").val(),
                patientId: $("#FinalVerification_PatientId").val()
            }, function(event, ui) {
                $($(ui.tab).attr("href")).Load("Billing/Claim/Final/Category", {
                    episodeId: event.data.episodeId,
                    patientId: event.data.patientId,
                    category: $(ui.tab).attr("href")
                }, Billing.Claim.Final.InitSection)
            });
            U.InitShared(r, t, x, e);
            Billing.Claim.Final.Tabs.Demographics(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            if ($("h1.blue", e).length) $(".buttons li", e).hide().eq(0).show();
            $(".filter select", e).change(function() { Billing.Claim.Final.Refresh() });
            $(".grid-refresh", e).click(function() {
                Billing.Claim.Final.Refresh();
                return false;
            });
            $(".buttons .print", e).click(function() {
                Billing.Claim.Print($("[name=BranchId]", e).val(), $("[name=PrimaryInsurance]", e).val(), $("[name=SortType]", e).val(), $("[name=Type]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Billing.Claim.Export($("[name=BranchId]", e).val(), $("[name=PrimaryInsurance]", e).val(), $("[name=SortType]", e).val(), $("[name=Type]", e).val());
                return false;
            });
            $(".generate", e).click(function() {
                Billing.Claim.Summary("Final", $(this).closest("form"), $(this).hasClass("all"));
                return false;
            });
        },
        InitSection: function(r, t, x, e) {
            var category = e.attr("id").split("_")[1];
            U.InitShared(r, t, x, e);
            if ($(".buttons .back", e).length) $(".buttons .back", e).click(function() {
                $(this).closest(".ui-tabs").find(".ui-tabs-selected").prev("li").find("a").click();
                return false;
            });
            if (typeof Billing.Claim.Final.Tabs[category] == "function") Billing.Claim.Final.Tabs[category](r, t, x, e);
        },
        List: function() { Acore.Open(Billing.Claim.Final.AcoreId) },
        Load: function(episodeId, patientId) { Acore.Open("Final", { episodeId: episodeId, patientId: patientId }) },
        Print: function(episodeId, patientId) {
            Acore.OpenPrintView({
                PreviewUrl: "Billing/Claim/Final/PrintPreview/" + episodeId + "/" + patientId,
                PdfUrl: "Billing/Claim/Final/Print/" + episodeId + "/" + patientId
            })
        },
        PrintList: function(branchId, insuranceId, sortType) {
            Acore.OpenPrintView({
                PreviewUrl: "Billing/Claim/Final/PrintPreview/" + branchId + "/" + insuranceId + "/" + sortType,
                PdfUrl: "Billing/Claim/Final/Print/" + branchId + "/" + insuranceId + "/" + sortType
            })
        },
        Refresh: function() {
            var e = $("#window_" + Billing.Claim.Final.AcoreId + " .main");
            $(".content", e).Load("Billing/Claim/Final/Grid", { branchId: $("[name=BranchId]", e).val(), insuranceId: $("[name=PrimaryInsurance]", e).val() }, function() {
                if ($("h1.blue", e).length) $(".buttons li", e).hide().eq(0).show();
                else $(".buttons li", e).show();
            })
        },
        Snapshot: {
            Edit: function(batchId, finalId) {
                Acore.Modal({
                    Name: "Edit Final Snapshot",
                    Url: "Billing/Claim/Final/Snapshot/Edit",
                    Input: { batchId: batchId, id: finalId },
                    OnLoad: U.InitShared,
                    Width: 450
                })
            }
        },
        Supply: {
            ChangeStatus: function(id, finalId, isBillable, $grid) {
                if (U.IsGuid(id)) var input = { id: id, finalId: finalId, isBillable: isBillable };
                else {
                    var input = id;
                    input.push({ name: "finalId", value: finalId });
                    input.push({ name: "isBillable", value: isBillable });
                }
                Acore.Confirm({
                    Message: "Are you sure you want to mark the selected supplies as " + (isBillable ? "billable?" : "not billable?"),
                    Yes: function() { U.GridAjaxCommand("Billing/Claim/Final/Supply/ChangeStatus", input, $grid) }
                })
            },
            Delete: function(id, finalId, $grid) {
                if (U.IsGuid(id)) var input = { id: id, finalId: finalId };
                else {
                    var input = id;
                    input.push({ name: "finalId", value: finalId });
                }
                Acore.Confirm({
                    Message: "Are you sure you want to delete this supply?",
                    Yes: function() { U.GridAjaxCommand("Billing/Claim/Final/Supply/Delete", input, $grid) }
                })
            },
            Edit: function(id, finalId) {
                Acore.Modal({
                    Name: "Edit Supply",
                    Url: "Billing/Claim/Final/Supply/Edit",
                    Input: { id: id, finalId: finalId },
                    OnLoad: Billing.Claim.Final.Supply.InitShared,
                    Width: 750
                })
            },
            InitShared: function(r, t, x, e) {
                $("[name=Description]", e).AjaxAutocomplete({
                    ExtraParameters: { limit: 50 },
                    minLength: 1,
                    SourceUrl: "Agency/SupplySearch",
                    Format: function(json) { return json.Description },
                    Select: function(json, input) {
                        input.val(json.Description);
                        $("[name=Code]", e).val(json.Code);
                        $("[name=UnitCost]", e).val(json.UnitCost);
                        $("[name=UniqueIdentifier]", e).val(json.Id);
                        $("[name=RevenueCode]", e).val(json.RevenueCode);
                    }
                });
                U.InitShared(r, t, x, e);
            },
            New: function(finalId) {
                Acore.Modal({
                    Name: "New Supply",
                    Url: "Billing/Claim/Final/Supply/New",
                    Input: { finalId: finalId },
                    OnLoad: Billing.Claim.Final.Supply.InitShared,
                    Width: 750
                })
            },
            OnDataBound: function(e) {
                if ($("tbody tr", e.target).length == 0) $(e.target).closest("fieldset").find(".buttons").hide();
                else $(e.target).closest("fieldset").find(".buttons").show();
                U.ToggleNoneFound(e);
            }
        },
        Tabs: {
            Demographics: function(r, t, x, e) {
                var type = U.GetType(e), prefix = "#" + type + "_";
                U.HideIfSelectEquals($(prefix + "PatientStatus"), "0|30", $(prefix + "PatientStatusMore"));
                Billing.Claim.InitShared(r, t, x, e);
                $(".show-schedule", e).click(function() {
                    Schedule.Center.Load($("[name=PatientId]", e).val(), $("[name=EpisodeId]", e).val());
                    return false;
                });
            },
            Summary: function(r, t, x, e) {
                $(".print", e).click(function() {
                    Billing.Claim.Final.Print($("[name=EpisodeId]", e).val(), $("[name=PatientId]", e).val());
                    return false;
                });
            },
            Supplies: function(r, t, x, e) {
                $(".new-supply", e).click(function() {
                    Billing.Claim.Final.Supply.New($("[name=Id]", e).val());
                    return false;
                });
                $(".delete", e).click(function() {
                    Billing.Claim.Final.Supply.Delete($(this).closest("fieldset").find("[name=id]:checked").serializeArray(), $("[name=Id]", e).val(), $(this).closest("fieldset").find(".t-grid"));
                    return false;
                });
                $(".mark", e).click(function() {
                    Billing.Claim.Final.Supply.ChangeStatus($(this).closest("fieldset").find("[name=id]:checked").serializeArray(), $("[name=Id]", e).val(), $(this).hasClass("billable"), $(this).closest(".main").find(".t-grid"));
                    return false;
                });
            }
        }
    }
});