﻿var ManagedBilling = {
    _patientId: "",
    _ClaimId: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) { if (result.isSuccessful) { var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active")); } ManagedBilling.RebindActivity(ManagedBilling._patientId); } else U.Growl(result.errorMessage, 'error'); },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    managedClaimTabStripOnSelect: function(e) {
        ManagedBilling.LoadContent($(e.item).find('.t-link').data('ContentUrl'), e.contentElement)
    },
    ReLoadUnProcessedManagedClaim: function(branchId, insuranceId) {
        $("#Billing_ManagedClaimCenterContent ol").addClass('loading');
        $("#Billing_ManagedClaimCenterContent").load('Billing/ManagedGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#Billing_ManagedClaimCenterContent ol").removeClass("loading");
            if (textStatus == 'error') $('#Billing_ManagedClaimCenterContent').html(U.AjaxError)
        })
    },
    SubmitClaimDirectly: function(control) {
        U.PostUrl('Billing/SubmitManagedClaimDirectly', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    LoadGeneratedManagedClaim: function(control) {
        U.PostUrl("/Billing/CreateManagedANSI", $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
            else U.Growl(result.errorMessage, "error")
        }, function(result) { })
    },
    LoadGeneratedSingleManagedClaim: function(id) {
        U.PostUrl("/Billing/CreateSingleManagedANSI", { Id: id }, function(result) {
            if (result != null) {
                if (result.isSuccessful) $('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove();
                else U.Growl(result.errorMessage, "error")
            }
        }, function(result) { })
    },
    UpdateStatus: function(control) {
        U.PostUrl('Billing/SubmitManagedClaims', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error")
        }, null)
    },
    HideOrShowNewClaimMenu: function() {
        var gridTr = $('#ManagedBillingSelectionGrid').find('.t-grid-content tbody tr');
        if (gridTr != undefined && gridTr != null) {
            if (gridTr.length > 0) $("#managedBillingTopMenu").show();
            else $("#managedBillingTopMenu").hide();
        }
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl)
            $(contentElement).html("&#160;").addClass("loading").load(contentUrl, null, function() {
                $(contentElement).removeClass("loading");
            });
    },
    Navigate: function(index, id, patientId) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) { if (result.isSuccessful) { var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active")); } ManagedBilling.RebindActivity(patientId); ManagedBilling.ReLoadUnProcessedManagedClaim($("#Billing_ManagedClaimCenterBranchCode").val(), $("#Billing_ManagedClaimCenterPrimaryInsurance").val()); } else U.Growl(result.errorMessage, 'error'); },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    NavigateBack: function(index) {
        var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    loadGenerate: function(control) {
        Acore.Open("managedclaimsummary", 'Billing/ManagedClaimSummary', function() { }, $(":input", $(control)).serializeArray())
    },
    GenerateAllCompleted: function(control) {
        $("input[name=ManagedClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true)
        });
        ManagedBilling.loadGenerate(control)
    },
    NoPatientBind: function(id) { ManagedBilling.Center.Activity.Rebind(id) },
    ManagedComplete: function(id, patientId) {
        U.PostUrl('Billing/ManagedComplete', { id: id, patientId: patientId }, function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimedit');
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
                U.PostUrl("Billing/ChangeSupplyBillStatus", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        U.RebindTGrid($("#ManagedBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#ManagedUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                    }
                })
            }
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else {
            if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
                if (inputs != null) {
                    inputs[inputs.length] = { name: "Id", value: Id };
                    inputs[inputs.length] = { name: "PatientId", value: PatientId };
                    U.PostUrl("Billing/ManagedCareSuppliesDelete", inputs, function(result) {
                        if (result != null && result.isSuccessful) {
                            U.RebindTGrid($("#ManagedBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                            U.RebindTGrid($("#ManagedUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        }
                    })
                }
            }
        }
    },
    loadInsuranceForActivity: function(reportName, title, isZeroIndexPrefilled, action) {
        U.PostUrl("Agency/InsuranceSelectList", { branchId: $("#" + reportName + "_BranchCode").val() }, function(data) {
            var s = $("select#" + reportName + "_PrimaryInsuranceId");
            s.children('option').remove();
            if (isZeroIndexPrefilled) { s.get(0).options[s.get(0).options.length] = new Option(title, "0", false, false); }
            $.each(data, function(index, itemData) { s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false) })
            s.get(0).options[s.get(0).options.length] = new Option("No Insurance assigned", "-1", false, false);
            if (action != null && action != undefined && typeof (action) == "function") action();
        })
    },
    ShowManagedClaim: function(Id, patientId) {
        Acore.Open("managedclaimedit", "Billing/ManagedClaim", function() { }, { Id: Id, patientId: patientId });
    },
    Center: {
        AcoreId: "ManagedClaimsHistory",
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });

            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() {
                ManagedBilling.Center.Filter(e);
            });
            $(".ui-layout-west .top select", e).change(function() {
                ManagedBilling.Center.Rebind(e);
            });
        },
        InitContent: function(r, t, x, e) {
            $(".bottom  select.managed-insurances", e).change(function() {
                ManagedBilling.Center.Activity.Rebind(ManagedBilling._patientId);
            });
        },
        Filter: function(e) {
            var text = $(".ui-layout-west .top input", e).val();
            if (text && text.length) {
                var search = text.split(" ");
                $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                    if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                });
                $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
            } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra().find("tr:first").click();
        },
        Rebind: function(e) {
            U.RebindGrid($(".ui-layout-west .t-grid", e), {
                branchId: $("[name=BranchId]", e).val(),
                statusId: $("[name=StatusId]", e).val(),
                insuranceId: $("[name=InsuranceId]", e).val()
            })
        },
        LoadContent: function(patientId) {
            var e = $("#window_" + ManagedBilling.Center.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll)
            $(".ui-layout-center", e).Load("Managed/Charts/Content", { patientId: patientId, insuranceId: 0 }, ManagedBilling.Center.InitContent);
            ManagedBilling._patientId = patientId;
        },
        Select: function(e) { ManagedBilling.Center.LoadContent($(".t-last", e.row).text()); },
        InitSelecter: function() {
            ManagedBilling.Center.Filter($("#window_" + ManagedBilling.Center.AcoreId));
            var e = $("#window_" + ManagedBilling.Center.AcoreId + "_content");
            if (U.IsGuid(ManagedBilling._patientId) && $(".ui-layout-west .t-last:contains(" + ManagedBilling._patientId + ")", e).length) {
                ManagedBilling.Center.LoadContent(ManagedBilling._patientId);
            }
            else if ($(".ui-layout-west .t-last", e).length) {
                ManagedBilling.Center.LoadContent($(".ui-layout-west .t-last:first", e).text());
            }
            else {
                $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
            }
        },
        NewClaim: function(patientId) {
            Acore.Modal({
                Name: "New Managed Claim",
                Url: "Billing/NewManagedClaim",
                Input: { patientId: patientId },
                OnLoad: function(patientId) { ManagedBilling.Center.InitNewClaim(patientId) },
                Width: "500px",
                Height: "175px",
                WindowFrame: false
            })
        },
        UpdateClaim: function(patientId, Id) {
            Acore.Modal({
                Name: "Update Status of Managed Claim",
                Url: "Billing/UpdateManagedClaim",
                Input: { patientId: patientId, id: Id },
                OnLoad: function(patientId) { ManagedBilling.Center.InitUpdateClaim(patientId) },
                Width: "800px",
                Height: "210px",
                WindowFrame: false
            })
        },
        InitUpdateClaim: function(patientId) {
            U.InitTemplate($("#UpdateManagedClaim_Form"), function() {
                UserInterface.CloseModal();
                ManagedBilling.Center.Activity.Rebind(patientId);
            }, "Claim update is successfully.")
        },
        InitNewClaim: function(patientId) {
            U.InitTemplate($("#CreateManagedClaim_Form"), function() {
                UserInterface.CloseModal();
                ManagedBilling.Center.Activity.Rebind(patientId);
            }, "Claim is created successfully.")
        },
        DeleteClaim: function(patientId, id) {
            if (confirm("Are you sure you want to delete this claim?")) U.PostUrl("/Billing/DeleteManagedClaim", { patientId: patientId, id: id }, function(result) {
                if (result.isSuccessful) {
                    ManagedBilling.Center.Activity.Rebind(patientId);
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            })
        },
        Activity: {
            LoadContent: function(e) { var claimId = $("td.claimId", e.row).text();  var pId = $("td.pId", e.row).text(); $("#BillingManagedClaimData").Load("Billing/ManagedSnapShotClaimInfo", { patientId: pId, claimId: claimId }, function() { }); },
            Select: function(e) { ManagedBilling.Center.Activity.LoadContent(e); },
            DataBound: function(e) {
                if ($("#ManagedBillingActivityGrid .t-grid-content tr").length) $('#ManagedBillingActivityGrid .t-grid-content tbody tr').eq(0).click();
                else $("#BillingManagedClaimData").empty();
            },
            Rebind: function(patientId) { U.RebindGrid($("#ManagedBillingActivityGrid"), { patientId: patientId, insuranceId: $("#ManagedBillingHistory_PrimaryInsuranceId").val() });}
        }
    }
}