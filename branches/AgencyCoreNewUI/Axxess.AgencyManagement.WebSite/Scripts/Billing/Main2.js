﻿var Billing = {

    _patientId: "",
    _ClaimId: "",
    _ClaimType: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) { if (result.isSuccessful) { var tabstrip = $("#final-tabs").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { Billing.LoadContent(contentUrl, $("#final-tabs").find("div.t-content.t-state-active")); } } else U.Growl(result.errorMessage, 'error'); },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    finalTabStripOnSelect: function(e) {
        Billing.LoadContent($(e.item).find('.t-link').data('ContentUrl'), e.contentElement)
    },
    finalTabStripOnLoad: function(e) { setTimeout(function() { Acore.OnLoad($(e.target)) }, 500) },
    HideNewRapAndFinalMenu: function() { var gridTr = $('#BillingMedicareSelectionGrid').find('.t-grid-content tbody tr'); if (gridTr != undefined && gridTr != null) { if (gridTr.length > 0) { $("#BillingMedicareClaimsHistoryTopMenu").show(); } else { $("#BillingMedicareClaimsHistoryTopMenu").hide(); } } },
    InitRap: function() { U.InitTemplate($("#rapVerification"), function() { UserInterface.CloseWindow('rap'); Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val()); Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val()); }, "Rap is successfully verified."); },
    InitFinal: function() { Acore.OnLoad($(contentElement)); },
    ReLoadUnProcessedClaim: function(branchId, insuranceId, type) { $("#Billing_CenterContentRap ol").addClass('loading'); $("#Billing_CenterContentRap").load('Billing/RapGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) { $("#Billing_CenterContentRap ol").removeClass("loading"); if (textStatus == 'error') $('#Billing_CenterContentRap').html(U.AjaxError); }); },
    ReLoadUpProcessedRap: function(branchId, insuranceId) { $("#Billing_CenterContentRap ol").addClass('loading'); $("#Billing_CenterContentRap").load('Billing/RapGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) { $("#Billing_CenterContentRap ol").removeClass("loading"); if (textStatus == 'error') $('#Billing_CenterContentRap').html(U.AjaxError); }); },
    ReLoadUpProcessedFinal: function(branchId, insuranceId) { $("#Billing_CenterContentFinal ol").addClass('loading'); $("#Billing_CenterContentFinal").load('Billing/FinalGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) { $("#Billing_CenterContentFinal ol").removeClass("loading"); if (textStatus == 'error') $('#Billing_CenterContentFinal').html(U.AjaxError); }); },
    LoadContent: function(contentUrl, contentElement) { if (contentUrl) { $(contentElement).html("&#160;").addClass("loading").Load(contentUrl, null, function() { $(contentElement).removeClass("loading"); }); } },
    FinalTabLoad: function(e) { Acore.OnLoad($(e.target)) },
    loadGenerate: function(control, pageId) { Acore.Open(pageId, 'Billing/ClaimSummary', function() { }, $(":input", $(control)).serializeArray()) },
    Navigate: function(index, id) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            var tabstrip = $("#final-tabs").data("tTabStrip");
                            var item = $("li", tabstrip.element)[index];
                            tabstrip.select(item);
                            var $link = $(item).find('.t-link');
                            var contentUrl = $link.data('ContentUrl');
                            if (contentUrl != null) Billing.LoadContent(contentUrl, $("#final-tabs").find("div.t-content.t-state-active"));
                            if (index == 1 || index == 2) Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val())
                        } else U.Growl(result.errorMessage, 'error');
                    },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    NavigateBack: function(index) {
        var tabstrip = $("#final-tabs").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    UpdateStatus: function(control) {
        U.PostUrl('Billing/UpdateStatus', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val());
                Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val());
                UserInterface.CloseWindow('claimSummaryrap');
                UserInterface.CloseWindow('claimSummaryfinal');
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    FinalComplete: function(id) {
        U.PostUrl('Billing/FinalComplete', { id: id }, function(result) {
            if (result.isSuccessful) {
                Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val());
                Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val());
                UserInterface.CloseWindow('final');
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    GenerateAllCompleted: function(control, pageId) {
        $("input[name=ClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true)
        });
        Billing.loadGenerate(control, pageId);
    },
    NavigateBack: function(index) { var tabstrip = $("#final-tabs").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); },
    NoPatientBind: function(id) { Billing.RebindActivity(id); },
    UpdateStatus: function(control, type) { U.PostUrl('Billing/UpdateStatus', $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val()); Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val()); UserInterface.CloseWindow('claimSummary' + type); U.Growl(result.errorMessage, "success"); } else { U.Growl(result.errorMessage, "error"); } }, null); },
    FinalComplete: function(id) { U.PostUrl('Billing/FinalComplete', { id: id }, function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val()); Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val()); UserInterface.CloseWindow('final'); U.Growl(result.errorMessage, "success"); } else { U.Growl(result.errorMessage, "error"); } }, null); },
    GenerateAllCompleted: function(control, pageId) { $("input[name=ClaimSelected]", $(control)).each(function() { $(this).attr("checked", true) }); Billing.loadGenerate(control, pageId); },
    InitPendingClaimUpdate: function() { U.InitTemplate($("#insuranceId"), function() { Billing.loadPendingClaimRap(); Billing.loadPendingClaimFinal(); Billing.ReLoadUpProcessedRap($('#Billing_RapCenter_BranchCode').val(), $('#Billing_RapCenter_InsuranceId').val()); Billing.ReLoadUpProcessedFinal($('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val()); }, "Claim update is successfully."); },
    loadPendingClaimRap: function() {
        $("#MedicarePendingClaims_rapContent").empty().addClass("loading").load('Billing/PendingClaimRap', null, function(responseText, textStatus, XMLHttpRequest) {
            $("#MedicarePendingClaims_rapContent").removeClass("loading");
            if (textStatus == 'error') $('#MedicarePendingClaims_rapContent').html(U.AjaxError)
        })
    },
    loadPendingClaimFinal: function() {
        $("#MedicarePendingClaims_finalContent").empty().addClass("loading").load('Billing/PendingClaimFinal', null, function(responseText, textStatus, XMLHttpRequest) {
            $("#MedicarePendingClaims_finalContent").removeClass("loading");
            if (textStatus == 'error') $('#MedicarePendingClaims_finalContent').html(U.AjaxError)
        })
    },
    OpenUBOFour: function(patientId, claimId, claimType) {
        U.GetAttachment("Billing/UB04Pdf", { patientId: patientId, Id: claimId, type: claimType })
    },
    Sort: function(type, sortBy) {
        $("#Billing_CenterContent" + type + " ol").addClass("loading");
        var invert = false;
        if (Billing._sort[type] == sortBy) invert = true, Billing._sort[type] = "";
        else Billing._sort[type] = sortBy;
        setTimeout(function() {
            var items = new Array();
            if (sortBy == "rap-episode") $("#Billing_CenterContent" + type + " ol ." + sortBy + " .very-hidden").each(function() { items.push($(this).text()) });
            else $("#Billing_CenterContent" + type + " ol span." + sortBy).each(function() { items.push($(this).text()) });
            items = items.sort();
            if (invert) items.reverse();
            for (var i = 0; i < items.length; i++) $("#Billing_CenterContent" + type + " ol").append($("#Billing_CenterContent" + type + " ol ." + sortBy + ":contains('" + items[i] + "')").closest("li"));
            for (var i = 0; i < items.length; i++) $("#Billing_CenterContent" + type + " ol li:eq(" + i + ") .rap-checkbox").html((i + 1) + "." + $("#Billing_CenterContent" + type + " ol li:eq(" + i + ") .rap-checkbox").html().split(".")[1]);
            $("#Billing_CenterContent" + type + " ol li:nth-child(even)").removeClass("odd").addClass("even");
            $("#Billing_CenterContent" + type + " ol li:nth-child(odd)").removeClass("even").addClass("odd");
            $("#Billing_CenterContent" + type + " ol").removeClass("loading");
            if (invert) $("input[name=" + type + "Sort]").val(sortBy + "-invert");
            else $("input[name=" + type + "Sort]").val(sortBy);
        }, 100);
    },
    SubmitClaimDirectly: function(control, type) { U.PostUrl('Billing/SubmitClaimDirectly', $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { UserInterface.CloseWindow('claimSummary' + type); U.Growl(result.errorMessage, "success"); } else { U.Growl(result.errorMessage, "error"); } }, null); },
    LoadGeneratedClaim: function(control) { U.PostUrl("/Billing/CreateANSI", $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { jQuery('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove(); } else { U.Growl(result.errorMessage, "error"); } }, function(result) { }); },
    onRapDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onRapDetailViewExpand: function(e) { $("#Raps table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#Raps table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    onFinalDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onFinalDetailViewExpand: function(e) { $("#Finals table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#Finals table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    onEditClaim: function(e) {
        if (e.dataItem != undefined) {
            var form = $(e.form);
            if (form != null && form != undefined) {
                $(form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; });
                var day = e.dataItem.PaymentDate;
                if (day != null && day != undefined && day.getFullYear() == "1") {
                    $(form).find('input[name=PaymentDate]').val("");
                }
            }
        }
    },
    expandFirstRow: function(grid, row) { if (grid.$rows().index(row) == 0) { grid.expandRow(row); } },
    onRapOnRowDataBound: function(e) { var grid = $("#Raps").data('tGrid'); Billing.expandFirstRow(grid, e.row); },
    ReLoadPendingClaimRap: function(branchId, insuranceId) { U.RebindTGrid($("#Raps"), { branchId: branchId, insuranceId: insuranceId }); },
    ReLoadPendingClaimFinal: function(branchId, insuranceId) { U.RebindTGrid($("#Finals"), { branchId: branchId, insuranceId: insuranceId }); },
    InitRemittance: function() {
        $("#externalRemittanceUploadForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("The upload is successful.", "success");
                            $("#Billing_ExternalRemittanceUpload").val('');
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("The upload is successful.", "success");
                            $("#Billing_ExternalRemittanceUpload").val('');
                        } else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    RebindSubmittedBatchClaims: function() {
        U.RebindTGrid($("#List_SubmittedClaims"), { ClaimType: $("#SubmittedClaims_ClaimType").val(), StartDate: $("#SubmittedClaims_StartDate").val(), EndDate: $("#SubmittedClaims_EndDate").val() });
        var $exportLink = $('#SubmittedClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/ClaimType=([^&]*)/, 'ClaimType=' + $("#SubmittedClaims_ClaimType").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SubmittedClaims_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#SubmittedClaims_EndDate").val());
        $exportLink.attr('href', href);
    },
    ReloadEligibilityList: function(patientId) {
        $("#MedicareEligiblity_ReportContent ol").addClass('loading');
        $("#MedicareEligiblity_ReportContent").load('Billing/EligibilityList', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $("#MedicareEligiblity_ReportContent ol").removeClass("loading");
            if (textStatus == 'error') $('#MedicareEligiblity_ReportContent').html(U.AjaxError);
        })
    },
    SubmittedClaimDetail: function(Id) {
        Acore.Open("SubmittedClaimDetails", 'Billing/SubmittedClaimDetail', function() { }, { Id: Id })
    },
    RemittanceDelete: function(id) {
        if (confirm("Are you sure you want to delete this remittance?")) U.PostUrl("/Billing/DeleteRemittance", { Id: id }, function(result) {
            if (result.isSuccessful) {
                U.Growl(result.errorMessage, "success");
                U.RebindDataGridContent('BillingRemittance', 'Billing/RemittanceContent', { StartDate: $('#BillingRemittance_StartDate').val(), EndDate: $('#BillingRemittance_EndDate').val() }, null);
            } else U.Growl(result.errorMessage, "error");
        })
    },
    loadAuthorizationContent: function(contentId, authorizationId) {
        $(contentId).load('Billing/AuthorizationContent', { AuthorizationId: authorizationId }, function(responseText, textStatus, XMLHttpRequest) { });
    },
    LoadInsuranceContent: function(contentId, patientId, insuranceId, startDate, endDate, claimTypeIdentifier) {
        $(contentId).load('Billing/InsuranceInfoContent', {
            PatientId: patientId,
            InsuranceId: insuranceId,
            StartDate: startDate,
            EndDate: endDate,
            ClaimTypeIdentifier: claimTypeIdentifier
        }, function(responseText, textStatus, XMLHttpRequest) { })
    },
    onEditRapSnapShotDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined && dataItem.PaymentDate != undefined) {
            var day = new Date(e.dataItem.PaymentDate);
            if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[3].innerHTML = '&#160;';
        }
    },
    onRapPendingDataBound: function(e) {
        if (e != undefined && e.dataItem != undefined && e.dataItem.PaymentDate != undefined) {
            var day = new Date(e.dataItem.PaymentDate);
            if (day != null && day != undefined && day.getFullYear() == "1") e.row.cells[1].innerHTML = '&#160;';
        }
    },
    ChangeSupplyBillableStatus: function(Id, PatientId, control, IsBillable) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Mark As Billable" button.');
            return;
        } else if (inputs != null) {
            inputs[inputs.length] = { name: "Id", value: Id };
            inputs[inputs.length] = { name: "PatientId", value: PatientId };
            inputs[inputs.length] = { name: "IsBillable", value: IsBillable };
            U.PostUrl("Billing/ChangeFinalSupplyStatus", inputs, function(result) {
                if (result != null && result.isSuccessful) {
                    U.RebindTGrid($("#FinalBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                    U.RebindTGrid($("#FinalUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                }
            })
        }
    },
    ChangeSupplyStatus: function(Id, PatientId, control) {
        var inputs = $(":input", $(control)).serializeArray();
        if (inputs.length < 1) {
            alert('Select at least one supply before clicking on the "Delete" button.');
            return;
        } else if (confirm("Are you sure you want to delete this Supply/Supplies?")) {
            if (inputs != null) {
                inputs[inputs.length] = { name: "Id", value: Id };
                inputs[inputs.length] = { name: "PatientId", value: PatientId };
                U.PostUrl("Billing/FinalSuppliesDelete", inputs, function(result) {
                    if (result != null && result.isSuccessful) {
                        U.RebindTGrid($("#FinalBillingSupplyGrid"), { Id: Id, patientId: PatientId });
                        U.RebindTGrid($("#FinalUnBillingSupplyGrid"), { Id: Id, patientId: PatientId });

                    }
                })
            }
        }
    },
    PostRemittance: function(Id, control) {
        var inputs = $("input:checkbox", $(control)).serializeArray();
        if (inputs != null && inputs != undefined && inputs.length > 0) {
            inputs[inputs.length] = { name: "Id", value: Id };
            U.PostUrl("Billing/PostRemittance", inputs, function(result) {
                if (result != null && result.isSuccessful) {
                    if (result.isSuccessful) {
                        U.RebindPageContent("RemittanceDetail", 'Billing/RemittanceDetailContent', { Id: Id });
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                }
            });
        }
        else {
            alert('Select at least one option.');
            return;
        }
    }
}