﻿$.extend(Billing, {
    MedicareEligibility: {
        AcoreId: "MedicareEligibility",     // Acore Window ID for Medicare Eligibility Window
        PatientId: "",                      // Current Active Patient
        // Initialize Medicare Eligibility Window
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() { Billing.MedicareEligibility.PatientSelector.Filter(e) });
            $(".ui-layout-west .top select", e).change(function() { Billing.MedicareEligibility.PatientSelector.Rebind(e) });
        },
        // Initialize Main/Content section
        InitContent: function(r, t, x, e) {

        },
        Load: function(patientId) {
            if (Acore.Windows[Billing.MedicareEligibility.AcoreId].IsOpen) {
                Billing.MedicareEligibility.LoadContent(patientId);
                $("#window_" + Billing.MedicareEligibility.AcoreId).WinFocus();
            } else {
                Billing.MedicareEligibility.PatientId = patientId;
                Acore.Open(Billing.MedicareEligibility.AcoreId);
            }
        },
        LoadContent: function(patientId) {
            var e = $("#window_" + Billing.MedicareEligibility.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll)
            $(".ui-layout-center", e).Load("Billing/MedicareEligibility/Content", { patientId: patientId }, Billing.MedicareEligibility.InitContent);
            Billing.MedicareEligibility.PatientId = patientId;
        },
        PatientSelector: {
            Init: function() {
                Billing.MedicareEligibility.PatientSelector.Filter($("#window_" + Billing.MedicareEligibility.AcoreId));
                var e = $("#window_" + Billing.MedicareEligibility.AcoreId + "_content");
                if (U.IsGuid(Billing.MedicareEligibility.PatientId) && $(".ui-layout-west .t-last:contains(" + Billing.MedicareEligibility.PatientId + ")", e).length) Billing.MedicareEligibility.LoadContent(Billing.MedicareEligibility.PatientId);
                else if ($(".ui-layout-west .t-last", e).length) Billing.MedicareEligibility.LoadContent($(".ui-layout-west .t-last:first", e).text());
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters to show more patients."));
            },
            Filter: function(e) {
                var text = $(".ui-layout-west .top input", e).val();
                if (text && text.length) {
                    search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            },
            Rebind: function(e) {
                U.RebindGrid($(".ui-layout-west .t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    statusId: $("[name=StatusId]", e).val(),
                    paymentSourceId: $("[name=PaymentSourceId]").val()
                })
            },
            Select: function(e) { Billing.MedicareEligibility.LoadContent($(".t-last", e.row).text()) }
        },
        Refresh: function() {
            Billing.MedicareEligibility.LoadContent(Billing.MedicareEligibility.PatientId);
        }
    }
});