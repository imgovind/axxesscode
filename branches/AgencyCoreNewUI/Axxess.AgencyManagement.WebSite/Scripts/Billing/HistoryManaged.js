﻿$.extend(Billing.History, {
    Managed: {
        AcoreId: "ManagedClaimsHistory",   // Acore Window ID for Medicare History Window
        ClaimId: "",                        // Current Active Claim
        ClaimType: "",                      // Current Active Claim Type
        PatientId: "",                      // Current Active Patient
        Activity: {
            Init: function() {
                var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
                if (U.IsGuid(Billing.History.Managed.ClaimId) && $(".ui-layout-center .t-last:contains(" + Billing.History.Managed.ClaimId + ")", e).length) {
                    var row = $(".ui-layout-center .t-last:contains(" + Billing.History.Managed.ClaimId + ")", e).parent().find("td:eq(0):contains(" + Billing.History.Managed.ClaimType + ")", row).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
                    if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
                    else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll);
                }
                else if ($(".ui-layout-center .t-grid-content tr", e).length) $(".ui-layout-center .t-grid-content tr:first", e).click();
            },
            Select: function(e) { Billing.History.Managed.LoadInfo($("td:eq(9)", e.row).text(), $(".t-last", e.row).text(), $("td:eq(1)", e.row).text()) }
        },
        // Initialize Medicare History Window
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() { Billing.History.Managed.PatientSelector.Filter(e) });
            $(".ui-layout-west .top select", e).change(function() { Billing.History.Managed.PatientSelector.Rebind(e) });
        },
        // Initialize Main/Content section
        InitContent: function(r, t, x, e) {
            $(".new-rap", e).click(function() {
                return false;
            });
            $(".new-final", e).click(function() {
                return false;
            });
            Billing.History.Managed.InitInfo(r, t, x, e);
        },
        InitInfo: function(r, t, x, e) {
            $(".activity-log", e).click(function() {
                return false;
            });
            $(".remittance", e).click(function() {
                return false;
            });
            // Fix activity chart top positioning
            Billing.History.Managed.PositionBottom();
        },
        Load: function(patientId, claimId, claimType) {
            if (Acore.Windows[Billing.History.Managed.AcoreId].IsOpen) {
                Billing.History.Managed.LoadContent(patientId, claimId, claimType);
                $("#window_" + Billing.History.Managed.AcoreId).WinFocus();
            } else {
                Billing.History.Managed.PatientId = patientId, Billing.History.Managed.ClaimId = claimId, Billing.History.Managed.ClaimType = claimType;
                Acore.Open(Billing.History.Managed.AcoreId);
            }
        },
        LoadActivities: function(patientId) {
            if (discipline == undefined) discipline = "all";
            var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
            $(".ui-layout-center .bottom", e).Load("Billing/History/Managed/Activities", { patientId: patientId });
            Billing.History.Managed.PatientId = patientId;
        },
        LoadContent: function(patientId, claimId, claimType) {
            var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll)
            $(".ui-layout-center", e).Load("Billing/History/Managed/Content", { patientId: patientId, claimId: claimId, claimType: claimType }, Billing.History.Managed.InitContent);
            Billing.History.Managed.PatientId = patientId, Billing.History.Managed.ClaimId = claimId, Billing.History.Managed.ClaimType = claimType;
        },
        LoadInfo: function(patientId, claimId, claimType) {
            var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
            $(".ui-layout-center .info", e).Load("Billing/History/Managed/Info", { patientId: patientId, claimId: claimId, claimType: claimType }, Billing.History.Managed.InitInfo);
            Billing.History.Managed.PatientId = patientId, Billing.History.Managed.ClaimId = claimId, Billing.History.Managed.ClaimType = claimType;
        },
        PatientSelector: {
            Init: function() {
                Billing.History.Managed.PatientSelector.Filter($("#window_" + Billing.History.Managed.AcoreId));
                var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
                if (U.IsGuid(Billing.History.Managed.PatientId) && $(".ui-layout-west .t-last:contains(" + Billing.History.Managed.PatientId + ")", e).length) Billing.History.Managed.LoadContent(Billing.History.Managed.PatientId, Billing.History.Managed.ClaimId, Billing.History.Managed.ClaimType);
                else if ($(".ui-layout-west .t-last", e).length) Billing.History.Managed.LoadContent($(".ui-layout-west .t-last:first", e).text());
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
            },
            Filter: function(e) {
                var text = $(".ui-layout-west .top input", e).val();
                if (text && text.length) {
                    search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            },
            Rebind: function(e) {
                U.RebindGrid($(".ui-layout-west .t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    statusId: $("[name=StatusId]", e).val(),
                    paymentSourceId: $("[name=PaymentSourceId]").val()
                })
            },
            Select: function(e) { Billing.History.Managed.LoadContent($(".t-last", e.row).text()) }
        },
        PositionBottom: function() {
            var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
            $(".ui-layout-center .bottom", e).css("top", $(".ui-layout-center .top", e).height());
        },
        Refresh: function() {
            var e = $("#window_" + Billing.History.Managed.AcoreId + "_content");
            $(".ui-layout-center", e).addClass("loading");
            Billing.History.Managed.LoadInfo(Billing.History.Managed.PatientId, Billing.History.Managed.ClaimId);
            Billing.History.Managed.LoadActivities(Billing.History.Managed.PatientId, Billing.History.Managed.ClaimId);
        }
    }
});