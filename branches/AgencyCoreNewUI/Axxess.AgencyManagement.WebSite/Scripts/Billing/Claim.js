﻿$.extend(Billing, {
    Claim: {
        Export: function(branchId, insuranceId, sortType, claimType) {
            U.GetAttachment("Billing/Claim/Export", {
                branchId: branchId,
                insuranceId: insuranceId,
                sortType: sortType,
                claimType: claimType
            })
        },
        Generate: function($form) {
            U.PostUrl("Billing/Claim/CreateANSI", $form.serializeArray(), function(result) {
                if (result.isSuccessful) U.GetAttachment("Billing/Claim/Generate", { ansiId: result.Id });
                else U.GrowlResult(result);
            })
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            if ($("[name=PrimaryInsuranceId]", e).val() > 1000) $(prefix + "InsuranceInfo").show();
            else $(prefix + "InsuranceInfo").hide();
            $("[name=PrimaryInsuranceId]", e).change(function() {
                if ($(this).val() > 1000) $(prefix + "InsuranceInfo").hide().load("Billing/InsuranceInfo", {
                    patientId: $("[name=PatientId]", e).val(),
                    insuranceId: $(this).val(),
                    startDate: $("[name=EpisodeStartDate]", e).val(),
                    endDate: $("[name=EpisodeEndDate]", e).val(),
                    claimTypeIdentifier: $("[name=ClaimTypeIdentifier]", e).val()
                }, function() { $(prefix + "InsuranceInfo").show() });
                else $(prefix + "InsuranceInfo").hide();
            });
            U.InitShared(r, t, x, e);
        },
        InitSummary: function(r, t, x, e) {
            $(".generate", e).click(function() {
                Billing.Claim.Generate($("form", e));
                return false;
            });
            U.InitShared(r, t, x, e);
        },
        Pending: {
            Edit: function(id, type) {
                Acore.Modal({
                    Name: "Edit Pending Claim",
                    Url: "Billing/Claim/Pending/Edit",
                    Input: { id: id, type: type },
                    OnLoad: U.InitShared,
                    Width: 450
                })
            },
            Init: function(r, t, x, e) {
                Billing.Claim.Pending.Refresh();
                $(".grid-controls select", e).change(function() { Billing.Claim.Pending.Refresh() });
                $(".grid-refresh", e).click(function() {
                    Billing.Claim.Pending.Refresh();
                    return false;
                })
            },
            Refresh: function() {
                $("#PendingClaims_RAPGrid").Load("Billing/Claim/Pending/RAPGrid", { branchId: $("#PendingClaims_BranchCode").val(), insuranceId: $("#PendingClaims_Insurance").val() }, function(r, t, x, e) {
                    if ($("ol li", e).length) $(".expand,.collapse", e).click(function() {
                        if ($(this).hasClass("expand")) $(this).removeClass("expand").addClass("collapse").closest("li").find(".snapshot").Load("Billing/Claim/RAP/Snapshot/Grid", { id: $(this).text() });
                        else $(this).removeClass("collapse").addClass("expand").closest("li").find(".snapshot").empty();
                    });
                    else e.html(
                        $("<ul/>").append(
                            $("<li/>").addClass("ac").append(
                                $("<h3/>").text("RAPs")))).append(
                        $("<h1/>").addClass("blue").text("No Pending RAPs"));
                });
                $("#PendingClaims_FinalGrid").Load("Billing/Claim/Pending/FinalGrid", { branchId: $("#PendingClaims_BranchCode").val(), insuranceId: $("#PendingClaims_Insurance").val() }, function(r, t, x, e) {
                    if ($("ol li", e).length) $(".expand,.collapse", e).click(function() {
                        if ($(this).hasClass("expand")) $(this).removeClass("expand").addClass("collapse").closest("li").find(".snapshot").Load("Billing/Claim/Final/Snapshot/Grid", { id: $(this).text() });
                        else $(this).removeClass("collapse").addClass("expand").closest("li").find(".snapshot").empty();
                    });
                    else e.html(
                        $("<ul/>").append(
                            $("<li/>").addClass("ac").append(
                                $("<h3/>").text("Finals")))).append(
                        $("<h1/>").addClass("blue").text("No Pending Finals"));
                });
            }
        },
        Print: function(branchId, insuranceId, sortType, claimType) {
            Acore.OpenPrintView({
                PreviewUrl: "Billing/Claim/PrintPreview/" + branchId + "/" + insuranceId + "/" + sortType + "/" + claimType,
                PdfUrl: "Billing/Claim/Print/" + branchId + "/" + insuranceId + "/" + sortType + "/" + claimType
            })
        },
        Summary: function(type, $form, all) {
            if (all) $("[type=checkbox]", $form).each(function() { $(this).prop("checked", true) });
            if ($("[name=ClaimSelected]:checked", $form).length) Acore.Open("ClaimSummary" + type, $form.serializeArray());
            else U.Growl("Please select at least one claim to generate.", "error");
        }
    }
});