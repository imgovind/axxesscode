﻿$.extend(Billing.Claim, {
    Managed: {
        AcoreId: "ClaimManagedCenter",
        Export: function(branchId, insuranceId, sortType) { U.GetAttachment("Billing/Claim/Managed/Export", { branchId: branchId, insuranceId: insuranceId, sortType: sortType }) },
        Init: function(r, t, x, e) {
            $("#ManagedVerification_TabStrip").tabs().bind("tabsselect", {
                id: $("#ManagedVerification_Id").val(),
                patientId: $("#ManagedVerification_PatientId").val()
            }, function(event, ui) {
                $($(ui.tab).attr("href")).Load("Billing/Claim/Managed/Category", {
                    id: event.data.id,
                    patientId: event.data.patientId,
                    category: $(ui.tab).attr("href")
                }, Billing.Claim.Managed.InitSection)
            });
            U.InitShared(r, t, x, e);
            Billing.Claim.Managed.Tabs.Demographics(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            if ($("h1.blue", e).length) $(".buttons li", e).hide().eq(0).show();
            $(".filter select", e).change(function() { Billing.Claim.Managed.Refresh() });
            $(".grid-refresh", e).click(function() {
                Billing.Claim.Managed.Refresh();
                return false;
            });
            $(".buttons .print", e).click(function() {
                Billing.Claim.Print($("[name=BranchId]", e).val(), $("[name=PrimaryInsurance]", e).val(), $("[name=SortType]", e).val(), $("[name=Type]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Billing.Claim.Export($("[name=BranchId]", e).val(), $("[name=PrimaryInsurance]", e).val(), $("[name=SortType]", e).val(), $("[name=Type]", e).val());
                return false;
            });
            $(".generate", e).click(function() {
                Billing.Claim.Summary("Final", $(this).closest("form"), $(this).hasClass("all"));
                return false;
            });
        },
        InitSection: function(r, t, x, e) {
            var category = e.attr("id").split("_")[1];
            U.InitShared(r, t, x, e);
            if ($(".buttons .back", e).length) $(".buttons .back", e).click(function() {
                $(this).closest(".ui-tabs").find(".ui-tabs-selected").prev("li").find("a").click();
                return false;
            });
            if (typeof Billing.Claim.Managed.Tabs[category] == "function") Billing.Claim.Managed.Tabs[category](r, t, x, e);
        },
        List: function() { Acore.Open(Billing.Claim.Managed.AcoreId) },
        Load: function(id, patientId) { Acore.Open("ManagedCare", { id: id, patientId: patientId }) },
        Print: function(id, patientId, type) {
            Acore.OpenPrintView({
                PreviewUrl: "Billing/Claim/" + type + "/PrintPreview/" + id + "/" + patientId,
                PdfUrl: "Billing/Claim/" + type + "/Print/" + id + "/" + patientId
            })
        },
        PrintList: function(branchId, insuranceId, sortType) {
            Acore.OpenPrintView({
                PreviewUrl: "Billing/Claim/Managed/PrintPreview/" + branchId + "/" + insuranceId + "/" + sortType,
                PdfUrl: "Billing/Claim/Managed/Print/" + branchId + "/" + insuranceId + "/" + sortType
            })
        },
        Refresh: function() {
            var e = $("#window_" + Billing.Claim.Managed.AcoreId + " .main");
            $(".content", e).Load("Billing/Claim/Managed/Grid", { branchId: $("[name=BranchId]", e).val(), insuranceId: $("[name=PrimaryInsurance]", e).val() }, function() {
                if ($("h1.blue", e).length) $(".buttons li", e).hide().eq(0).show();
                else $(".buttons li", e).show();
            })
        },
        Snapshot: {
            Edit: function(batchId, finalId) {
                Acore.Modal({
                    Name: "Edit Managed Claim Snapshot",
                    Url: "Billing/Claim/Managed/Snapshot/Edit",
                    Input: { batchId: batchId, id: finalId },
                    OnLoad: U.InitShared,
                    Width: 450
                })
            }
        },
        Supply: {
            ChangeStatus: function(id, finalId, isBillable, $grid) {
                if (U.IsGuid(id)) var input = { id: id, finalId: finalId, isBillable: isBillable };
                else {
                    var input = id;
                    input.push({ name: "finalId", value: finalId });
                    input.push({ name: "isBillable", value: isBillable });
                }
                Acore.Confirm({
                    Message: "Are you sure you want to mark the selected supplies as " + (isBillable ? "billable?" : "not billable?"),
                    Yes: function() { U.GridAjaxCommand("Billing/Claim/Managed/Supply/ChangeStatus", input, $grid) }
                })
            },
            Delete: function(id, finalId, $grid) {
                if (U.IsGuid(id)) var input = { id: id, finalId: finalId };
                else {
                    var input = id;
                    input.push({ name: "finalId", value: finalId });
                }
                Acore.Confirm({
                    Message: "Are you sure you want to delete this supply?",
                    Yes: function() { U.GridAjaxCommand("Billing/Claim/Managed/Supply/Delete", input, $grid) }
                })
            },
            Edit: function(id, finalId) {
                Acore.Modal({
                    Name: "Edit Supply",
                    Url: "Billing/Claim/Managed/Supply/Edit",
                    Input: { id: id, finalId: finalId },
                    OnLoad: Billing.Claim.Final.Supply.InitShared,
                    Width: 750
                })
            },
            InitShared: function(r, t, x, e) {
                $("[name=Description]", e).AjaxAutocomplete({
                    ExtraParameters: { limit: 50 },
                    minLength: 1,
                    SourceUrl: "Agency/SupplySearch",
                    Format: function(json) { return json.Description },
                    Select: function(json, input) {
                        input.val(json.Description);
                        $("[name=Code]", e).val(json.Code);
                        $("[name=UnitCost]", e).val(json.UnitCost);
                        $("[name=UniqueIdentifier]", e).val(json.Id);
                        $("[name=RevenueCode]", e).val(json.RevenueCode);
                    }
                });
                U.InitShared(r, t, x, e);
            },
            New: function(finalId) {
                Acore.Modal({
                    Name: "New Supply",
                    Url: "Billing/Claim/Managed/Supply/New",
                    Input: { finalId: finalId },
                    OnLoad: Billing.Claim.Final.Supply.InitShared,
                    Width: 750
                })
            },
            OnDataBound: function(e) {
                if ($("tbody tr", e.target).length == 0) $(e.target).closest("fieldset").find(".buttons").hide();
                else $(e.target).closest("fieldset").find(".buttons").show();
                U.ToggleNoneFound(e);
            }
        },
        Tabs: {
            Demographics: function(r, t, x, e) {
                var type = U.GetType(e), prefix = "#" + type + "_";
                U.HideIfSelectEquals($(prefix + "PatientStatus"), "0|30", $(prefix + "PatientStatusMore"));
                Billing.Claim.InitShared(r, t, x, e);
                $(".show-schedule", e).click(function() {
                    Schedule.Center.Load($("[name=PatientId]", e).val(), $("[name=EpisodeId]", e).val());
                    return false;
                });
            },
            Summary: function(r, t, x, e) {
                $(".print", e).click(function() {
                    Billing.Claim.Managed.Print($("[name=EpisodeId]", e).val(), $("[name=PatientId]", e).val());
                    return false;
                });
            },
            Supplies: function(r, t, x, e) {
                $(".new-supply", e).click(function() {
                    Billing.Claim.Managed.Supply.New($("[name=Id]", e).val());
                    return false;
                });
                $(".delete", e).click(function() {
                    Billing.Claim.Managed.Supply.Delete($(this).closest("fieldset").find("[name=id]:checked").serializeArray(), $("[name=Id]", e).val(), $(this).closest("fieldset").find(".t-grid"));
                    return false;
                });
                $(".mark", e).click(function() {
                    Billing.Claim.Managed.Supply.ChangeStatus($(this).closest("fieldset").find("[name=id]:checked").serializeArray(), $("[name=Id]", e).val(), $(this).hasClass("billable"), $(this).closest(".main").find(".t-grid"));
                    return false;
                });
            }
        }
    }
});