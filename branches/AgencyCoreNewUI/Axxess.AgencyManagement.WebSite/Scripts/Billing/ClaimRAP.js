﻿$.extend(Billing.Claim, {
    RAP: {
        AcoreId: "ClaimRAPCenter",
        Init: function(r, t, x, e) {
            $(".print", e).click(function() {
                Billing.Claim.RAP.Print($("[name=EpisodeId]", e).val(), $("[name=PatientId]", e).val());
                return false;
            });
            Billing.Claim.InitShared(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            if ($("h1.blue", e).length) $(".buttons li", e).hide().eq(0).show();
            $(".filter select", e).change(function() { Billing.Claim.RAP.Refresh() });
            $(".grid-refresh", e).click(function() {
                Billing.Claim.RAP.Refresh();
                return false;
            });
            $(".buttons .print", e).click(function() {
                Billing.Claim.Print($("[name=BranchId]", e).val(), $("[name=PrimaryInsurance]", e).val(), $("[name=SortType]", e).val(), $("[name=Type]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Billing.Claim.Export($("[name=BranchId]", e).val(), $("[name=PrimaryInsurance]", e).val(), $("[name=SortType]", e).val(), $("[name=Type]", e).val());
                return false;
            });
            $(".generate", e).click(function() {
                Billing.Claim.Summary("RAP", $(this).closest("form"), $(this).hasClass("all"));
                return false;
            });
        },
        List: function() {
            Acore.Open(Billing.Claim.RAP.AcoreId)
        },
        Print: function(episodeId, patientId) {
            Acore.OpenPrintView({
                PreviewUrl: "Billing/Claim/RAP/PrintPreview/" + episodeId + "/" + patientId,
                PdfUrl: "Billing/Claim/RAP/Print/" + episodeId + "/" + patientId
            })
        },
        Refresh: function() {
            var e = $("#window_" + Billing.Claim.RAP.AcoreId + " .main");
            $(".content", e).Load("Billing/Claim/RAP/Grid", { branchId: $("[name=BranchId]", e).val(), insuranceId: $("[name=PrimaryInsurance]", e).val() }, function() {
                if ($("h1.blue", e).length) $(".buttons li", e).hide().eq(0).show();
                else $(".buttons li", e).show();
            })
        },
        Snapshot: {
            Edit: function(batchId, rapId) {
                Acore.Modal({
                    Name: "Edit RAP Snapshot",
                    Url: "Billing/Claim/RAP/Snapshot/Edit",
                    Input: { batchId: batchId, id: rapId },
                    OnLoad: U.InitShared,
                    Width: 450
                })
            }
        },
        Verify: function(episodeId, patientId) {
            Acore.Open("RAP", { episodeId: episodeId, patientId: patientId })
        }
    }
});