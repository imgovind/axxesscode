﻿$.extend(User, {
    Schedule: {
        Group: "",
        Sort: "",
        Export: function() {
            U.GetAttachment("User/Schedule/Export", {});
        },
        Init: function(r, t, x, e) {
            $(".grid-refresh", e).click(function() {
                User.Schedule.Refresh(e.find(".content"));
                return false;
            });
            $(".excel-export", e).click(function() {
                User.Schedule.Export();
                return false;
            });
            $(".sort", e).click(function() {
                if ($(this).text() == "Group By Patient") User.Schedule.Refresh(e.find(".content"), "PatientName", $("[name=Sort]", e).val());
                else if ($(this).text() == "Group By Date") User.Schedule.Refresh(e.find(".content"), "VisitDate", $("[name=Sort]", e).val());
                else if ($(this).text() == "Group By Task") User.Schedule.Refresh(e.find(".content"), "TaskName", $("[name=Sort]", e).val());
                return false;
            });
        },
        Refresh: function(e, group, sort) {
            if (group != undefined) User.Schedule.Group = group;
            if (sort != undefined) User.Schedule.Sort = sort;
            e.Load("User/Schedule/List", { group: User.Schedule.Group, sort: User.Schedule.Sort });
        }
    }
});