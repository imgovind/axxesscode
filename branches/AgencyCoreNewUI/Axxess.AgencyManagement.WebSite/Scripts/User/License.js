﻿$.extend(User, {
    License: {
        Delete: function(id, userId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this license?",
                Yes: function() { U.GridAjaxCommand("User/License/Delete", { id: id, userId: userId }, $grid) }
            })
        },
        Edit: function(id, userId) {
            Acore.Modal({
                Name: "Edit License",
                Url: "User/License/Edit",
                Input: { id: id, userId: userId },
                OnLoad: User.License.InitEdit,
                Width: 450
            })
        },
        InitEdit: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            User.License.InitShared(r, t, x, e);
        },
        InitNew: function(r, t, x, e) {
            User.License.InitShared(r, t, x, e)
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "LicenseType"), "Other", $(prefix + "LicenseTypeMore"));
            U.InitShared(r, t, x, e);
        },
        New: function(userId) {
            Acore.Modal({
                Name: "New License",
                Url: "User/License/New",
                Input: { userId: userId },
                OnLoad: User.License.InitNew,
                Width: 450
            })
        }
    }
});