﻿$.extend(User, {
    PayRate: {
        Delete: function(id, userId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this rate?",
                Yes: function() { U.GridAjaxCommand("User/PayRate/Delete", { id: id, userId: userId }, $grid) }
            })
        },
        Edit: function(id, userId) {
            Acore.Modal({
                Name: "Edit Pay Rate",
                Url: "User/PayRate/Edit",
                Input: { id: id, userId: userId },
                OnLoad: User.PayRate.InitEdit,
                Width: 450
            })
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e)
        },
        InitNew: function(r, t, x, e) {
            U.InitShared(r, t, x, e)
        },
        New: function(userId) {
            Acore.Modal({
                Name: "New Pay Rate",
                Url: "User/PayRate/New",
                Input: { userId: userId },
                OnLoad: User.PayRate.InitNew,
                Width: 450
            })
        }
    }
});