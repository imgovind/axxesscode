﻿$.extend(User, {
    Signature: {
        Reset: function() {
            Acore.Open("ResetSignature");
        },
        InitReset: function(r, t, x, e) {
            $(".reset", e).click(function() {
                U.PostUrl("User/Signature/Email", {}, function(result) {
                    U.GrowlResult(result);
                    if (result.isSuccessful) e.closest(".window").Close();
                });
            });
        }
    }
});