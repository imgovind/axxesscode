﻿$.extend(User, {
    Calendar: {
        Month: "",
        Year: "",
        Init: function(r, t, x, e) {
            $("a[month][year]", e).click(function() {
                User.Calendar.Navigate(e, $(this).attr("month"), $(this).attr("year"));
                return false;
            });
            $(".print", e).click(function() {
                User.Calendar.Print($(this).attr("date"));
                return false;
            });
        },
        Navigate: function(e, month, year) {
            if (month != undefined) User.Calendar.Month = month;
            if (year != undefined) User.Calendar.Year = year;
            e.Load("User/Calendar/Navigate", { month: User.Calendar.Month, year: User.Calendar.Year }, function(r, t, x, e) { User.Calendar.Init(r, t, x, e) });
        },
        Print: function(month) {
            U.GetAttachment("User/Calendar/Pdf", { month: month });
        }
    }
});