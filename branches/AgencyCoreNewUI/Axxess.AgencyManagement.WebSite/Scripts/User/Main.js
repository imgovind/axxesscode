﻿var User = {
    Activate: function(id, $grid) {
        Acore.Confirm({
            Message: "Are you sure you want to activate this user?",
            Yes: function() { U.GridAjaxCommand("User/Activate", { id: id }, $grid) }
        })
    },
    Deactivate: function(id, $grid) {
        Acore.Confirm({
            Message: "Are you sure you want to deactivate this user?",
            Yes: function() { U.GridAjaxCommand("User/Deactivate", { id: id }, $grid) }
        })
    },
    Delete: function(id, $grid) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this user?",
            Yes: function() { U.GridAjaxCommand("User/Delete", { id: id }, $grid) }
        })
    },
    Edit: function(id) {
        Acore.Open("EditUser", { id: id });
    },
    ExportActive: function() {
        U.GetAttachment("User/ExportActive", {});
    },
    ExportInactive: function() {
        U.GetAttachment("User/ExportInactive", {});
    },
    InitList: function(r, t, x, e) {
        $(".new-user", e).click(function() {
            User.New();
            return false;
        });
        $(".export.active", e).click(function() {
            User.ExportActive();
            return false;
        });
        $(".export.inactive", e).click(function() {
            User.ExportInactive();
            return false;
        });
    },
    InitEdit: function(r, t, x, e) {
        var type = U.GetType(e), prefix = "#" + type + "_";
        $(prefix + "Tabs").tabs().bind("tabsselect", {
            Id: $(prefix + "Id").val()
        }, function(event, ui) {
            $($(ui.tab).attr("href")).Load("User/EditSection", { id: event.data.Id, category: $(ui.tab).attr("href") }, User.InitSection);
        });
        User.InitSection(r, t, x, e.find(".general:eq(0)"));
    },
    InitNew: function(r, t, x, e) {
        User.Sections.Information(e);
        User.Sections.Permissions(e);
        U.InitShared(r, t, x, e);
    },
    InitSection: function(r, t, x, e) {
        var Category = e.attr("id").split("_")[1];
        if (typeof User.Sections[Category] == "function") User.Sections[Category](r, t, x, e);
    },
    Logs: function(id) {
        Acore.Open("UserLogs", { id: id });
    },
    New: function() {
        Acore.Open("NewUser");
    },
    Sections: {
        Information: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "TitleType"), "Other", $(prefix + "TitleTypeMore"));
            U.ShowIfSelectEquals($(prefix + "Credentials"), "Other", $(prefix + "CredentialsMore"));
            $(".activity-log", e).click(function() {
                User.Logs(e.closest(".window-content").find("[name=Id]").val());
                return false;
            });
            U.InitShared(r, t, x, e);
        },
        Licenses: function(r, t, x, e) {
            $(".new-user-license", e).click(function() {
                User.License.New(e.closest(".window-content").find("[name=Id]").val());
                return false;
            });
        },
        PayRates: function(r, t, x, e) {
            $(".new-pay-rate", e).click(function() {
                User.PayRate.New(e.closest(".window-content").find("[name=Id]").val());
                return false;
            });
        },
        Permissions: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.SelectAll($(prefix + "AllPermissions"), $("input[name=PermissionsArray]", e));
            $(".select-all-group", e).each(function() { U.SelectAll($(this), $("." + $(this).attr("group"), e)) });
            ToolTip.Init(e);
            U.InitShared(r, t, x, e);
        }
    }
}