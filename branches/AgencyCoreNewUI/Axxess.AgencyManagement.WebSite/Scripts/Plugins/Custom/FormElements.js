(function($) {
    $.extend($.fn, {
        Buttons: function(Buttons) {
            return this.each(function() {
                var $buttons = $(this);
                $buttons.addClass("buttons").append(
                    $("<ul/>")
                );
                $.each(Buttons, function() {
                    $("ul", $buttons).append(
                        $("<li/>").append(
                            $("<a/>", { "href": "javascript:void(0)", "text": this.Text }).bind("click", this.Click)
                        )
                    )
                })
            })
        },
        DeselectableRadio: function() {
            return this.each(function() {
                var $radio = $(this);
                $radio.mouseup(function() {
                    if ($(this).prop("checked")) $(this).addClass("deselect")
                });
                $radio.click(function() {
                    if ($(this).hasClass("deselect")) $(this).removeClass("deselect").prop("checked", false).change();
                    else return true
                });
            })
        },
        GridSearch: function() {
            return this.each(function() {
                $(this).append(
                    $("<label/>", { text: "Filter by Text", "class": "strong" }).click(function() {
                        $(this).next().focus()
                    })).append(
                    $("<input/>", { type: "text", "class": "input-directions" }).focus(function() {
                        $(this).removeClass("input-directions")
                    }).blur(function() {
                        if (!$(this).val()) $(this).addClass("input-directions")
                    }).keyup(function() {
                        var SearchString = $(this).val(),
                            GridContent = $(this).closest(".wrapper").find(".t-grid-content:eq(0)");
                        if (SearchString) $("tr", GridContent).each(function() {
                            $(this).removeClass("match").hide();
                            $("td", this).each(function() {
                                if ($(this).text().toLowerCase().indexOf(SearchString.toLowerCase()) > -1) $(this).closest("tr").addClass("match");
                            })
                        });
                        else $("tr", GridContent).addClass("match");
                        $(".match", GridContent).removeClass("t-alt").show().filter(":even").addClass("t-alt");
                    })
                )
            })
        },
        MultiSelect: function(Options) {
            return this.each(function() {
                if (Options != null && typeof Options.OnSelect == "function") var Callback = Options.OnSelect;
                $(this).multiSelect({
                    selectAll: false,
                    noneSelected: Options != null && Options.DefaultText != undefined ? Options.DefaultText : "",
                    oneOrMoreSelected: "*"
                }, Callback)
                $(".multiSelect").attr("href","").append(
                    $("<select/>").focus(function() { $(this).parent().focus() })
                )
            })
        },
        Select: function(Options) {
            return this.each(function() {
                var $select = $(this);
                if (Options.Attr) $select.attr(Options.Attr);
                if (Options.Css) $select.css(Options.Css);
                if (Options.BlankEntry == null || Options.BlankEntry) $select.append(
                    $("<option/>")
                );
                $.each(Options.Options, function() {
                    $select.append(
                        $("<option/>").val(this.Value).text(this.Text ? this.Text : this.Value).attr("selected", Options.Value === this.Value)
                    );
                })
            })
        },
        TemplateSelect: function() {
            return this.each(function() {
                var $select = $(this),
                    $textarea = $select.parent().next("textarea");
                $select.change(function() {
                    if ($select.val() == "empty") {
                        $textarea.empty();
                        $select.prop("selectedIndex", 0);
                    } else if ($select.val() == "spacer") $select.prop("selectedIndex", 0);
                    else if ($select.val().length > 0) U.PostUrl("Agency/Template/Get", { "id": $select.val() }, function(template) {
                        if (template != undefined) $textarea.append(($textarea.text().trim() == "" ? "" : "\n") + template.Text).focus();
                    })
                }).prev("label").attr("for", $(this).attr("id"))
            })
        },
        Uploader: function(Options) {
            return this.each(function() {
                $(this).attr({
                    type: "file",
                    name: Options.Name,
                    id: Options.Id ? Options.Id : Options.Name,
                    size: Options.Size ? String(Options.Size) : "15"
                }).addClass(Options.Class).change(function() {
                    $(this).next(".dummy-file").find("input").val($(this).val())
                }).after(
                    $("<div/>", { "class": "dummy-file" }).append(
                        $("<input/>")).append(
                        $("<div/>").Buttons([
                            { Text: "Browse", Click: function() { } }
                        ])
                    )
                )
            })
        },
        Row: function(Options) {
            return this.each(function() {
                var $row = $(this),
                    Id = Options.Prefix + "_" + Options.Name;
                $row.addClass("row" + (Options.Class ? " " + Options.Class : "")).append(
                    $("<label/>", { "for": Id, "text": Options.Label })).append(
                    $("<div/>", { "class": "answer" })
                );
                if (Options.Type == "select") $(".answer", $row).append(
                    $("<select>").Select({
                        Attr: {
                            "id": Id,
                            "name": Id
                        },
                        Options: Options.Options,
                        Value: Options.Value
                    })
                );
                else if (Options.Type == "file") $(".answer", $row).append(
                    $("<input/>").Uploader({
                        Name: Id,
                        Size: Options.Size
                    })
                );
                else {
                    $(".answer", $row).append(
                        $("<input/>", {
                            "id": Id,
                            "name": Id,
                            "type": "text",
                            "value": Options.Value
                        })
                    );
                    if (Options.Type == "date") $(".answer input", $row).DatePicker();
                    else if (Options.Type == "centimeters") $(".answer input", $row).addClass("centimeter").after("cm");
                    else if (Options.Type == "percent") $(".answer input", $row).addClass("percent").after("%");
                }
            })
        },
        MultiRow: function(Options) {
            return this.each(function() {
                var $row = $(this);
                $row.addClass("row").append(
                    $("<div/>", { "class": "main-label", "text": Options.Label })
                );
                $.each(Options.Subrows, function() {
                    $row.append(
                        $("<div/>").Row($.extend({}, { Prefix: Options.Prefix }, this)).removeClass("row").addClass("subrow")
                    )
                })
            })
        }
    })
})(jQuery);