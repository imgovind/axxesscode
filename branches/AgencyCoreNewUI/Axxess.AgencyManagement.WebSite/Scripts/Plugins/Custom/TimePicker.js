(function($) {
    $.extend($.fn, {
        TimePicker: function() {
            return this.each(function() {
                var $input = $(this);
                // If of input type time, change to type text
                if ($input.attr("type") == "time") {
                    var $newinput = $("<input/>", { "type": "text", "name": $input.attr("name"), "id": $input.attr("id"), "class": $input.attr("class"), "value": $input.attr("value") });
                    $input.after($newinput);
                    $input.remove();
                    $input = $newinput;
                }
                // Initialize time picker
                $input.addClass("time").Autocomplete({
                    minLength: 0,
                    source: [   "07:00 AM", "07:15 AM", "07:30 AM", "07:45 AM", "08:00 AM", "08:15 AM", "08:30 AM", "08:45 AM", "09:00 AM", "09:15 AM", "09:30 AM", "09:45 AM",
                                "10:00 AM", "10:15 AM", "10:30 AM", "10:45 AM", "11:00 AM", "11:15 AM", "11:30 AM", "11:45 AM", "12:00 PM", "12:15 PM", "12:30 PM", "12:45 PM",
                                "01:00 PM", "01:15 PM", "01:30 PM", "01:45 PM", "02:00 PM", "02:15 PM", "02:30 PM", "02:45 PM", "03:00 PM", "03:15 PM", "03:30 PM", "03:45 PM",
                                "04:00 PM", "04:15 PM", "04:30 PM", "04:45 PM", "05:00 PM", "05:15 PM", "05:30 PM", "05:45 PM", "06:00 PM", "06:15 PM", "06:30 PM", "06:45 PM",
                                "07:00 PM", "07:15 PM", "07:30 PM", "07:45 PM", "08:00 PM", "08:15 PM", "08:30 PM", "08:45 PM", "09:00 PM", "09:15 PM", "09:30 PM", "09:45 PM",
                                "10:00 PM", "10:15 PM", "10:30 PM", "10:45 PM", "11:00 PM", "11:15 PM", "11:30 PM", "11:45 PM", "12:00 AM", "12:15 AM", "12:30 AM", "12:45 AM",
                                "01:00 AM", "01:15 AM", "01:30 AM", "01:45 AM", "02:00 AM", "02:15 AM", "02:30 AM", "02:45 AM", "03:00 AM", "03:15 AM", "03:30 AM", "03:45 AM",
                                "04:00 AM", "04:15 AM", "04:30 AM", "04:45 AM", "05:00 AM", "05:15 AM", "05:30 AM", "05:45 AM", "06:00 AM", "06:15 AM", "06:30 AM", "06:45 AM" ],
                    change: function() { $(this).trigger("ValueSet") }
                }).bind("ValueSet", function() { }).blur(function() {
                    if (!U.IsTime($(this).val())) {
                        // Grab value of only numerics and letter p
                        var t = $(this).val().replace(/[^0-9Pp]/g, "").toLowerCase(), am = false;
                        // Replace hour zero to twelve
                        if (t.replace(/[^0-9]/g, "").length == 4 && t.match(/^00/)) t = t.replace(/^00/, "12"), am = true;
                        else if (t.replace(/[^0-9]/g, "").length == 3 && t.match(/^0/)) t = t.replace(/^0/, "12"), am = true;
                        else if (t.match(/^0+$/)) t = "1200", am = true;
                        // Remove leading zeros, if any
                        t = t.replace(/^0*/, "");
                        // If value is entered that a time can be deciphered from
                        if (t.match(/^\d{1,4}(p)?$/)) {
                            var pm = t.indexOf("p") > 0,
                                hours = 0,
                                minutes = "";
                            t = t.replace(/[^0-9]/g, "");
                            if (t.length == 4) hours = parseInt(t.substr(0, 2)), minutes = t.substr(2, 2);
                            else if (t.length == 3) hours = parseInt(t.substr(0, 1)), minutes = t.substr(1, 2);
                            else hours = parseInt(t), minutes = "00";
                            if (hours > 0 && hours < 24) {
                                if (hours > 12) hours -= 12, pm = true;
                                if (hours == 12 && !am) pm = true;
                                if (hours < 10) t = "0" + String(hours) + ":" + minutes + " " + (pm ? "PM" : "AM");
                                else t = String(hours) + ":" + minutes + " " + (pm ? "PM" : "AM");
                                if (U.IsTime(t)) $(this).val(t).keyup().trigger("ValueSet");
                                else $(this).removeClass("valid").addClass("error");
                            } else $(this).removeClass("valid").addClass("error");
                        } else if ($(this).val().length || ($(this).hasClass("required") && !$(this).val())) $(this).removeClass("valid").addClass("error");
                    }
                }).focus(function() {
                    $(this).autocomplete("search", $(this).val())
                }).after(
                    $("<span/>", { "class": "img icon clock ui-icon", "text": "select time" }).click(function() {
                        $(this).prev().focus()
                    })
                )
            })
        }
    })
})(jQuery);