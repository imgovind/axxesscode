(function($) {
    $.extend($.fn, {
        PDScheduler: function(command) {
            return this.each(function() {
                if (command === "refresh") $(this).fullCalendar("today").fullCalendar("rerenderEvents").fullCalendar("changeView", "month");
                else {
                    var ratio = $(this).closest(".ui-layout-center").width() / ($(this).closest(".ui-layout-center").height() - 75);
                    $(this).fullCalendar({
                        aspectRatio: ratio,
                        editable: true,
                        endParam: "EndDate",
                        events: {
                            url: "Schedule/PD/Events",
                            type: "POST",
                            data: { patientId: $(this).attr("patient") },
                            error: function() { U.Growl("There was an error fetching the schedule.  Please try again.", "error") }
                        },
                        header: { left: "prev,next today", center: "title", right: "month,agendaWeek,agendaDay" },
                        loading: function(isLoading, view) {
                            if (isLoading) $(view).addClass("loading");
                            else $(view).removeClass("loading");
                        },
                        startParam: "StartDate",
                        titleFormat: {
                            month: "'<span class=\"fc-patient-name\"></span> &#8212; 'MMMM yyyy",
                            week: "'<span class=\"fc-patient-name\"></span> &#8212; 'MMM d[ yyyy]{'&#8211;'[ MMM]d, yyyy}",
                            day: "'<span class=\"fc-patient-name\"></span> &#8212; 'dddd, MMM d, yyyy"
                        },
                        viewDisplay: function(view) {
                            $(view.element).closest(".pd-calendar").find(".fc-patient-name").text($(view.element).closest(".pd-calendar").attr("patient"));
                        },
                        windowResize: function(view) { $(this).fullCalendar("option", "aspectRatio", $("#PDScheduleMainResult").width() / ($("#PDScheduleMainResult").height() - 75)) }
                    });
                    // Fix Styling
                    $(".fc-button-effect", this).remove();
                    $(".fc-button-content", this).each(function() {
                        $(this).text($(this).text().replace(/\b./g, function(m) { return m.toUpperCase(); }))
                    });
                    // Add new event button
                    $(".fc-header-left", this).prepend(
                        $("<div>").addClass("abs").Buttons([{ Text: "New Task", Click: function() { return true } }])
                    );
                }
            })
        }
    })
})(jQuery);

