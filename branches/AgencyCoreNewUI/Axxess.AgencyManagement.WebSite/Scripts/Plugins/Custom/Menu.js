(function($) {
    $.extend($.fn, {
        Menu: function() {
            return this.each(function() {
                var BeforeShow = function() {
                    if ($(this).parent().length && $(this).parent().offset().left + $(this).parent().width() + $(this).width() > $(document).width()) $(this).css({ right: $(this).css("left"), left: "auto" });
                    if ($(this).parent().length && $(this).parent().offset().top + $(this).height() > $(document).height()) $(this).css({ top: $(document).height() - ($(this).parent().offset().top + $(this).height() + 10) });
                };
                var Hide = function() { $(this).attr("style", "") };
                if (Acore.Animated) {
                    if ($(this).parent().attr("id") == "bar-top") var Animation = { height: "show" };
                    else var Animation = { width: "show" };
                    $(this).superfish({
                        animation: Animation,
                        speed: Acore.AnimationSpeed,
                        onBeforeShow: BeforeShow,
                        onHide: Hide
                    });
                    $(".menu", this).superfish({
                        animation: { width: "show" },
                        speed: Acore.AnimationSpeed,
                        onBeforeShow: BeforeShow,
                        onHide: Hide
                    });
                } else $(this).superfish({
                    speed: 0,
                    onBeforeShow: BeforeShow,
                    onHide: Hide
                })
            })
        }
    })
})(jQuery);