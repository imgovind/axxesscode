﻿var Report = {
    Init: function() {
        $(".report-output.buttons a.home-button").click(Report.Home);
        $(".reports a.report-link").click(Report.Show);
        Report.Home();
    },
    Show: function(url) {
        if (!url.length) url = $(this).attr("href");
        $(".report-home").hide();
        $("#report-output").empty().show().addClass("loading").Load(url, function(r, t, x, e) {
            $(".report-output").show();
            $("#report-output").removeClass("loading");
            if (t == "success") {
                var reportName = e.find(".report-name").val();
                var isGridExist = $("div#" + reportName + "GridContainer", e).length > 0;
                $(".report-generate", e).click(function() {
                    if (isGridExist) {
                        var input = {};
                        $.each($(".grid-controls  .report_input:input", e).serializeArray(), function(key, value) { input[value.name] = value.value });
                        Report.RebindReportGridContent(reportName, reportName + "Content", input, e.find(".report-sorttype").val());
                    }
                });
                $(".report-request", e).click(function() { Report.RequestReport("/Request/" + reportName, $(".grid-controls  .report_input:input", e).serializeArray()); });
                $(".report-export", e).click(function() { var isExportPrefix = $(this).attr("isexportprefix");  Report.Export(isExportPrefix != undefined && (isExportPrefix == "False" || isExportPrefix == "false") ? reportName : ("Export" + reportName), $(".grid-controls  .report_input:input", e).serializeArray()); });
                $("select.medicare-with-hmo", e).change(function() { Insurance.loadMedicareWithHMOInsuarnceDropDown(reportName, 'All', true, function() { }); });
                $("select.user-list", e).change(function() { Report.loadUsersDropDown(reportName); });
                $("select.insurance-list").change(function() { var title = $(this).attr("zeroindextitle"); Insurance.loadInsuarnceDropDown(reportName, title != undefined && title != "" ? title : 'All', true); });
                $("select.patient-list").change(function() { Report.loadPatientsDropDown(reportName); });
                if (isGridExist) {
                    var initInput = {};
                    $.each($(".grid-controls  .report_input:input", e).serializeArray(), function(key, value) { initInput[value.name] = value.value });
                    Report.InitContent(reportName, reportName + "Content", initInput);
                }
                $("#report-output .report-grid").css("top", $("#window_ReportCenter_content > .wrapper").height());
            }
            else $("#report-output").html(U.AjaxError);
        })
        return false;
    },
    Home: function() {
        $(".report-output").hide();
        $(".report-home").show();
    },
    loadUsersDropDown: function(reportName) {
        U.PostUrl("User/BranchList", {
            branchId: $("#" + reportName + "_BranchCode").val(),
            status: $("#" + reportName + "_StatusId").val()
        }, function(data) {
            var s = $("select#" + reportName + "_UserId");
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select User --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false)
            })
        })
    },
    loadPatientsDropDown: function(reportName) {
        U.PostUrl("Patient/BranchList", {
            branchId: $("#" + reportName + "_BranchCode").val(),
            status: $("#" + reportName + "_StatusId").val()
        }, function(data) {
            var s = $("select#" + reportName + "_PatientId");
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Patient --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false)
            })
        })
    },
    RebindReportGridContent: function(reportName, urlAction, jsonData, sortParams) { U.RebindDataGridContent(reportName, 'Report/' + urlAction, jsonData, sortParams, function() { Report.InitContent(reportName, urlAction, jsonData) }); },
    InitContent: function(reportName, urlAction, jsonData) {
        $("#" + reportName + "Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
            var link = $(this).attr("href");
            $(this).attr("href", "javascript:void(0)").click(function() { Report.RebindReportGridContent(reportName, urlAction, jsonData, U.ParameterByName(link, reportName + 'Grid-orderBy')); });
        });
    },
    Export: function(pageName, Input) {
        var $exportLink = $('#' + pageName + '_ExportLink');
        if ($exportLink != null && $exportLink != undefined) {
            var href = $exportLink.attr('href');
            if (href != null && href != undefined) {
                $.each(Input, function(key, value) {
                    var filter = new RegExp(value.name + "=([^&]*)");
                    href = href.replace(filter, value.name + '=' + value.value);
                });
                $exportLink.attr('href', href);
            }
        }
    },
    RebindClinicalOrders: function() {
        var grid = $('#Report_Patient_Orders_Grid').data('tGrid');
        if (grid != null) { grid.rebind({ StatusId: $("#Report_Patient_Orders_Status").val() }); }
    },
    RebindCompletedList: function() { U.RebindTGrid($('#List_CompletedReports')); },
    RequestReport: function(url, input) {
        U.PostUrl(url, input, function(result) {
            if (result.isSuccessful) U.Growl(result.errorMessage, "success");
            else U.Growl(result.errorMessage, "error");
        });
    },
    Delete: function(completedReportId) {
        if (confirm("Are you sure you want to delete this Report?")) {
            U.PostUrl("Request/DeleteReport", {
                completedReportId: completedReportId
            }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Report.RebindCompletedList();
                }
                else U.Growl(result.errorMessage, "error");
            });
        }
    }
}