﻿$.extend(Schedule, {
    Center: {
        AcoreId: "ScheduleCenter",  // Acore Window ID for Schedule Center
        CookieId: "ScheduleCenter", // Name for cookie to store previous state
        EpisodeId: "",              // Current Active Episode
        PatientId: "",              // Current Active Patient
        Discipline: "all",          // Current Active Discipline
        // Initialize each row in schedule activity grid
        ActivityRowBind: function(e) {
            // Initalize all tooltips
            ToolTip.Init(e.row);
            // Add effects for completed and orphaned tasks
            if (e.dataItem.IsComplete) $(e.row).addClass("completed");
            if (e.dataItem.IsOrphaned) {
                $(e.row).removeClass("completed").tooltip({ track: true, showURL: false, top: 5, left: 5, extraClass: "error",
                    bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
                });
                $(e.row.cells[1]).addClass("error");
            }
            // Setup context menu
            $(e.row).bind("contextmenu", function(Event) {
                var Menu = $("<ul/>");
                if (!e.dataItem.IsComplete) Menu.append(
                    $("<li/>", { text: "Edit Note" }).click(function() {
                        $(e.row).find("a:first").click();
                    })).append(
                    $("<li/>", { text: "Print" }).click(function() {
                        $(e.row).find(".print").parent().click();
                    }));
                $("td:last a", e.row).each(function() {
                    Menu.append(
                        $("<li/>", { text: $(this).text() }).click(function() {
                            $(this).click();
                        })
                    )
                });
                Menu.ContextMenu(Event);
            })
        },
        ChangeDiscipline: function(discipline) {
            Schedule.Center.Discipline = discipline;
            Schedule.Center.Refresh();
        },
        // Initialize Schedule Center
        Init: function(r, t, x, e) {
            // Set layout for patient selector
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            // Set new patient button
            $(".buttons .new-patient", e).click(function() {
                Patient.New();
                return false;
            });
            // Set patient selector filters
            $(".ui-layout-west .top input", e).keyup(function() { Schedule.Center.PatientSelector.Filter(e) });
            $(".ui-layout-west .top select", e).change(function() { Schedule.Center.PatientSelector.Rebind(e) });
        },
        // Initialize Main/Content section
        InitContent: function(r, t, x, e) {
            // Set current active episode
            Schedule.Center.EpisodeId = $("[name=EpisodeList]", e).val();
            // Set top menu
            $(".new-episode", e).click(function() {
                Schedule.Episode.NewModal(Schedule.Center.PatientId);
                return false;
            });
            $(".schedule-employee", e).click(function() {
                Schedule.MultiDay(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
                return false;
            });
            $(".reassign-schedule", e).click(function() {
                Schedule.ReassignMultiple(Schedule.Center.EpisodeId, Schedule.Center.PatientId, "Episode");
                return false;
            });
            $(".master-calendar", e).click(function() {
                Schedule.MasterCalendar.Load(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
                return false;
            });
            $(".inactive-episodes", e).click(function() {
                Schedule.Episode.InactiveList(Schedule.Center.PatientId);
                return false;
            });
            $(".episode-frequencies", e).click(function() {
                Schedule.Episode.FrequencyList(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
                return false;
            });
            // Set episode navigation controls
            $(".navigate", e).each(function() {
                if (U.IsGuid($(this).attr("guid"))) $(this).click(function() {
                    Schedule.Center.LoadContent(Schedule.Center.PatientId, $(this).attr("guid"));
                    return false;
                });
                else $(this).remove();
            });
            $("[name=EpisodeList]", e).change(function() {
                if ($(this).val() != Schedule.Center.EpisodeId) Schedule.Center.LoadContent(Schedule.Center.PatientId, $(this).val());
            });
            // Set click functions for buttons
            $(".edit-episode", e).click(function() {
                Schedule.Episode.Edit(Schedule.Center.EpisodeId, Schedule.Center.PatientId);
                return false;
            });
            $(".refresh", e).click(function() {
                Schedule.Center.Refresh();
                return false;
            });
            $(".patient-charts", e).click(function() {
                Patient.Charts.Load(Schedule.Center.PatientId);
                return false;
            });
            // Set scheduler controls
            if ($(".show-scheduler", e).length) Schedule.Center.Scheduler.Init(r, t, x, e);
            // Fix activity chart top positioning
            Schedule.Center.PositionBottom();
        },
        InitCalendar: function(r, t, x, e) {
            var e = $("#window_" + Schedule.Center.AcoreId + "_content");
            $(".ui-layout-center", e).removeClass("loading");
            if ($(".show-scheduler", e).length) $(".cal td:not(.inactive) a", e).click(function() {
                Schedule.Center.Scheduler.RowAdd($(this).attr("date"));
                return false;
            });
            Schedule.Center.PositionBottom();
        },
        Load: function(patientId, episodeId) {
            if (Acore.Windows[Schedule.Center.AcoreId].IsOpen) {
                Schedule.Center.LoadContent(patientId, episodeId);
                $("#window_" + Schedule.Center.AcoreId).WinFocus();
            } else {
                Schedule.Center.SaveState(patientId, episodeId);
                Acore.Open(Schedule.Center.AcoreId);
            }
        },
        LoadActivities: function(patientId, episodeId, discipline) {
            if (discipline == undefined) discipline = "all";
            var e = $("#window_" + Schedule.Center.AcoreId + "_content");
            $(".ui-layout-center .bottom", e).Load("Schedule/Center/Activities", { patientId: patientId, episodeId: episodeId, discipline: discipline });
            Schedule.Center.SaveState(patientId, episodeId);
        },
        LoadCalendar: function(patientId, episodeId, discipline) {
            if (discipline == undefined) discipline = "all";
            var e = $("#window_" + Schedule.Center.AcoreId + "_content");
            $(".ui-layout-center .calendar", e).Load("Schedule/Center/Calendar", { patientId: patientId, episodeId: episodeId, discipline: discipline }, Schedule.Center.InitCalendar);
            Schedule.Center.SaveState(patientId, episodeId);
        },
        LoadContent: function(patientId, episodeId) {
            var e = $("#window_" + Schedule.Center.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll)
            $(".ui-layout-center", e).Load("Schedule/Center/Content", { patientId: patientId, episodeId: episodeId }, Schedule.Center.InitContent);
            Schedule.Center.SaveState(patientId, episodeId);
        },
        PatientSelector: {
            Init: function() {
                var PreviousState = U.Cookie.Get(Schedule.Center.CookieId);
                if (PreviousState) Schedule.Center.PatientId = PreviousState.PatientId, Schedule.Center.EpisodeId = PreviousState.EpisodeId;
                Schedule.Center.PatientSelector.Filter($("#window_" + Schedule.Center.AcoreId));
                var e = $("#window_" + Schedule.Center.AcoreId + "_content");
                if (U.IsGuid(Schedule.Center.PatientId) && $(".ui-layout-west .t-last:contains(" + Schedule.Center.PatientId + ")", e).length) Schedule.Center.LoadContent(Schedule.Center.PatientId, Schedule.Center.EpisodeId);
                else if ($(".ui-layout-west .t-last", e).length) Schedule.Center.LoadContent($(".ui-layout-west .t-last:first", e).text());
                else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
            },
            Filter: function(e) {
                var text = $(".ui-layout-west .top input", e).val();
                if (text && text.length) {
                    search = text.split(" ");
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            },
            Rebind: function(e) {
                U.RebindGrid($(".ui-layout-west .t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    statusId: $("[name=StatusId]", e).val(),
                    paymentSourceId: $("[name=PaymentSourceId]").val()
                })
            },
            Select: function(e) {
                Schedule.Center.LoadContent($(".t-last", e.row).text());
            }
        },
        PositionBottom: function() {
            var e = $("#window_" + Schedule.Center.AcoreId + "_content");
            $(".ui-layout-center .bottom", e).css("top", $(".ui-layout-center .top", e).height());
        },
        Refresh: function() {
            var e = $("#window_" + Schedule.Center.AcoreId + "_content");
            $(".ui-layout-center", e).addClass("loading");
            Schedule.Center.LoadCalendar(Schedule.Center.PatientId, Schedule.Center.EpisodeId, Schedule.Center.Discipline);
            Schedule.Center.LoadActivities(Schedule.Center.PatientId, Schedule.Center.EpisodeId, Schedule.Center.Discipline);
        },
        Scheduler: {
            // Function for adding to schedule
            Add: function(rows) {
                var id = "#" + Schedule.Center.AcoreId + "_TabStrip", events = new Array(), Discipline = $(".ui-tabs-selected a", id).attr("discipline"), multi = false;
                if (Discipline == "Multiple") multi = true;
                rows.each(function() {
                    if (!parseInt($("select:eq(0)", this).val())) $("select:eq(0)", this).addClass("error");
                    else {
                        var DisciplineTask = $("select:eq(0)", this).removeClass("error").val();
                        var IsBillable = $("select:eq(0) option:selected", this).attr("isbillable");
                        if (multi) Discipline = $("select:eq(0) option:selected", this).attr("discipline");
                    }
                    if (!U.IsGuid($("select:eq(1)", this).val())) $("select:eq(1)", this).addClass("error");
                    else var UserId = $("select:eq(1)", this).removeClass("error").val();
                    if (!U.IsDate($(".date-picker:eq(0)", this).val())) $(".date-picker:eq(0)", this).addClass("error");
                    else {
                        var date = $(".date-picker:eq(0)", this).datepicker("getDate"), startDate = new Date($(".date-picker:eq(0)", this).datepicker("option", "minDate")), endDate = new Date($(".date-picker:eq(0)", this).datepicker("option", "maxDate"));
                        if (date.getTime() < startDate.getTime() || date.getTime() > endDate.getTime()) $(".date-picker:eq(0)", this).addClass("error");
                        else if (!multi) var EventDate = "\/Date(" + $(".date-picker:eq(0)", this).removeClass("error").datepicker("getDate").getTime() + ")\/";
                    }
                    if (DisciplineTask && UserId && Discipline && IsBillable && multi) {
                        if (!U.IsDate($(".date-picker:eq(1)", this).val())) $(".date-picker:eq(1)", this).addClass("error");
                        else {
                            var date = $(".date-picker:eq(1)", this).datepicker("getDate"), startDate = new Date($(".date-picker:eq(1)", this).datepicker("option", "minDate")), endDate = new Date($(".date-picker:eq(1)", this).datepicker("option", "maxDate"));
                            if (date.getTime() < startDate.getTime() || date.getTime() > endDate.getTime()) $(".date-picker:eq(1)", this).addClass("error");
                            else for (var i = $(".date-picker:eq(0)", this).datepicker("getDate"); i.getTime() <= $(".date-picker:eq(1)", this).datepicker("getDate").getTime(); i.setDate(i.getDate() + 1)) events.push({
                                DisciplineTask: DisciplineTask,
                                UserId: UserId,
                                EventDate: "\/Date(" + i.getTime() + ")\/",
                                Discipline: Discipline,
                                IsBillable: IsBillable
                            })
                        }
                    } else if (DisciplineTask && UserId && EventDate && Discipline && IsBillable) events.push({
                        DisciplineTask: DisciplineTask,
                        UserId: UserId,
                        EventDate: EventDate,
                        Discipline: Discipline,
                        IsBillable: IsBillable
                    })
                });
                if (events.length > 0) Schedule.Add(Schedule.Center.EpisodeId, Schedule.Center.PatientId, events, function(result) {
                    if (result.isSuccessful) {
                        rows.each(function() { if ($(this).find(".error").length == 0) $(this).removeClass("visible").appendTo($(this).parent()) });
                        if ($(this).parent().find(".visible").length == 0) Schedule.Center.Scheduler.ShowHide();
                        $("tbody", id).each(function() {
                            if ($(".visible", this).length == 0) $(this).find("tr:first").addClass("visible");
                        });
                    }
                })
            },
            Init: function(r, t, x, e) {
                var id = "#" + Schedule.Center.AcoreId + "_TabStrip";
                // Clicking on calendar
                $(".cal td:not(.inactive) a", e).click(function() {
                    Schedule.Center.Scheduler.RowAdd($(this).attr("date"));
                    return false;
                });
                // Clicking on scheduler tab
                $(".show-scheduler", e).click(function() {
                    Schedule.Center.Scheduler.ShowHide();
                    return false;
                });
                // Initialize tabs
                $(id).tabs({
                    select: function(event, ui) { Schedule.Center.ChangeDiscipline($(ui.tab).attr("discipline") == "Multiple" ? "all" : $(ui.tab).attr("discipline")) }
                });
                // Initialize remove row button
                $(".addrem", id).click(function() {
                    Schedule.Center.Scheduler.RowRemove($(this).closest("tr"));
                    return false;
                });
                // Initialize cancel button
                $(".cancel-schedule").click(function() {
                    Schedule.Center.Scheduler.Reset();
                    return false;
                });
                // Initialize save button
                $(".save-schedule").click(function() {
                    Schedule.Center.Scheduler.Add($(this).closest(".schedule-tab-content").find("tr.visible"));
                    return false;
                })
            },
            // Reset scheduler
            Reset: function() {
                var id = "#" + Schedule.Center.AcoreId + "_TabStrip";
                Schedule.Center.Scheduler.ShowHide();
                $(":input", id).val("");
                $("tbody tr", id).removeClass("visible");
                $("tbody", id).each(function() { $(this).find("tr:first").addClass("visible") });
            },
            // Function for adding row from scheduler
            RowAdd: function(date) {
                var e = $("#" + Schedule.Center.AcoreId + "_TabStrip"), active = $(".schedule-tab-content:not(.ui-tabs-hide)", e);
                // If scheduler is hidden, display it
                if (e.is(":hidden")) Schedule.Center.Scheduler.ShowHide();
                // If empty slot found, set date
                if ($("tr.visible input[value=]", active).length) $("tr.visible input[value=]:first", active).val(date);
                // Add new row, if space permits, set date on new row
                else $("tr.visible:last", active).next("tr").addClass("visible").find("input").val(date);
                // Cycle through hidden tabs and populate date on first available visible slot
                $(".schedule-tab-content.ui-tabs-hide", e).each(function() {
                    if ($("tr.visible input[value=]", this).length) $("tr.visible input[value=]:first", this).val(date);
                })
            },
            // Function for removing row from scheduler
            RowRemove: function(e) {
                e.find("input,select").val("");
                if (e.closest("tbody").find("tr.visible").length > 1) e.closest("tr").removeClass("visible").appendTo(e.closest("tbody"));
            },
            // Function to toggle the scheduler
            ShowHide: function() {
                var e = $("#window_" + Schedule.Center.AcoreId + "_content"), scheduler = $("#" + Schedule.Center.AcoreId + "_TabStrip"), activityPosition = parseInt($(".ui-layout-center .bottom", e).css("top"));
                if (scheduler.is(":visible")) {
                    $(".show-scheduler", e).text("Show Scheduler");
                    Schedule.Center.Discipline = "all";
                    if (Acore.Animated) $(".ui-layout-center .bottom", e).animate({ top: activityPosition - 120 }, Acore.AnimationSpeed, function() {
                        scheduler.hide();
                        Schedule.Center.Refresh();
                    });
                    else {
                        $(".ui-layout-center .bottom", e).css("top", activityPosition - 120);
                        scheduler.hide();
                        Schedule.Center.Refresh();
                    }
                } else {
                    $(".show-scheduler", e).text("Hide Scheduler");
                    scheduler.show();
                    Schedule.Center.Discipline = $(".ui-tabs-selected a", e).attr("discipline");
                    if (Acore.Animated) $(".ui-layout-center .bottom", e).animate({ top: activityPosition + 120 }, Acore.AnimationSpeed, function() { Schedule.Center.Refresh() });
                    else {
                        $(".ui-layout-center .bottom", e).css("top", activityPosition + 120);
                        Schedule.Center.Refresh();
                    }
                }
            }
        },
        SaveState: function(patientId, episodeId) {
            Schedule.Center.PatientId = patientId, Schedule.Center.EpisodeId = episodeId;
            U.Cookie.Set(Schedule.Center.CookieId, { PatientId: patientId, EpisodeId: episodeId });
        }
    }
});
