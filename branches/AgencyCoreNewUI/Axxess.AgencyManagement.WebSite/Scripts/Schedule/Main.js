﻿var Schedule = {
    Add: function(episodeId, patientId, event, callback) {
        event = JSON.stringify(event);
        U.PostUrl("Schedule/Add", {
            episodeId: episodeId,
            patientId: patientId,
            Patient_Schedule: event
        }, function(result) {
            U.GrowlResult(result);
            if (typeof callback == "function") callback(result);
        })
    },
    Delete: function(eventId, episodeId, patientId) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this task?",
            Yes: function() {
                U.PostUrl("Schedule/Delete", { eventId: eventId, episodeId: episodeId, patientId: patientId }, function(result) {
                    U.GrowlResult(result);
                    if (result.isSuccessful) U.RebindAllGrids();
                })
            }
        })
    },
    Details: function(eventId, episodeId, patientId) {
        Acore.Open("ScheduleDetails", { eventId: eventId, episodeId: episodeId, patientId: patientId });
    },
    ExportDeviation: function(branchId, startDate, endDate) {
        U.GetAttachment("Schedule/Deviation/Export", { branchId: branchId, startDate: startDate, endDate: endDate });
    },
    LoadEpisodeDropDown: function($episode, patientId) {
        $episode.empty().append(new Option("-- Select Episode --", U.GuidEmpty, false, false));
        if (patientId != U.GuidEmpty) U.PostUrl("Schedule/EpisodeRangeList", { patientId: patientId }, function(data) {
            $.each(data, function(index, data) {
                $episode.append(new Option(data.Range, data.Id, false, false))
            })
        })
    },
    InitDetailsEdit: function(r, t, x, e) {
        U.InitShared(e);
    },
    InitDeviation: function(r, t, x, e) {
        $(".grid-refresh").click(function() {
            Schedule.RefreshDeviation();
            return false;
        });
        $(".export", e).click(function() {
            Schedule.ExportDeviation($("[name=BranchId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
            return false;
        });
        $(".filter select,.filter .date-picker", e).change(function() { Schedule.RefreshDeviation() });
    },
    InitEpisodeDropDown: function(e) {
        Schedule.LoadEpisodeDropDown($("select[name=EpisodeId]", e), $("select[name=PatientId]", e).val());
        $("select[name=PatientId]", e).change(function() {
            Schedule.LoadEpisodeDropDown($("select[name=EpisodeId]", e), $(this).val())
        })
    },
    InitMultiDay: function(r, t, x, e) {
        $(".cal td:not(.inactive)", e).click(function() { $(this).toggleClass("selected") });
        $(".add-schedule", e).click(function() {
            Schedule.MultiDayAdd(e, $(this).hasClass("another"));
            return false;
        })
    },
    InitReassign: function(r, t, x, e) {
        U.InitShared(r, t, x, e);
    },
    InitReassignMultiple: function(r, t, x, e) {
        U.InitShared(r, t, x, e);
    },
    MultiDay: function(episodeId, patientId) {
        Acore.Modal({
            Name: "Quick Employee Scheduler",
            Url: "Schedule/MultiDay",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: Schedule.InitMultiDay,
            Width: 800
        })
    },
    MultiDayAdd: function(e, another) {
        if (!parseInt($("select:eq(0)", e).val())) $("select:eq(0)", e).addClass("error");
        else {
            var DisciplineTask = $("select:eq(0)", e).removeClass("error").val();
            var Discipline = $("select:eq(0) option:selected", e).attr("discipline");
            var IsBillable = $("select:eq(0) option:selected", e).attr("isbillable");
        }
        if (!U.IsGuid($("select:eq(1)", e).val())) $("select:eq(1)", e).addClass("error");
        else var UserId = $("select:eq(1)", e).removeClass("error").val();
        if (Discipline && DisciplineTask && UserId && IsBillable) {
            var events = new Array();
            if ($(".cal td.selected", e).length == 0) U.Growl("Please select date(s) on the calendar", "error");
            else $(".cal td.selected", e).each(function() {
                events.push({
                    DisciplineTask: DisciplineTask,
                    UserId: UserId,
                    EventDate: "\/Date(" + $(this).attr("date") + ")\/",
                    Discipline: Discipline,
                    IsBillable: IsBillable
                })
            });
            if (events.length) {
                $(this).closest(".window-content").addClass("loading");
                Schedule.Add($("[name=episodeId]", e).val(), $("[name=patientId]", e).val(), events, function(result) {
                    Schedule.Center.Refresh();
                    if (result.isSuccessful) {
                        if (another) {
                            $("select", e).val("");
                            $(".cal td.selected", e).removeClass("selected");
                            $("#window_" + Acore.DialogId + "_content").removeClass("loading");
                        } else $("#window_" + Acore.DialogId).Close();
                    } else $("#window_" + Acore.DialogId + "_content").removeClass("loading");
                })
            }
        }
    },
    Reassign: function(eventId, episodeId, patientId, userId) {
        Acore.Modal({
            Name: "Reassign Task",
            Url: "Schedule/Reassign",
            Input: { eventId: eventId, episodeId: episodeId, patientId: patientId, userId: userId },
            OnLoad: Schedule.InitReassign,
            Width: 500
        })
    },
    ReassignMultiple: function(episodeId, patientId, type) {
        Acore.Modal({
            Name: "Reasign Multiple Tasks",
            Url: "Schedule/ReassignMultiple",
            Input: { episodeId: episodeId, patientId: patientId, type: type },
            OnLoad: Schedule.InitReassignMultiple,
            Width: 500
        })
    },
    RefreshDeviation: function() {
        var e = $("#window_ScheduleDeviationReport .main");
        $(".content", e).Load("Schedule/Deviation/Grid", {
            branchId: $("[name=BranchId]").val(),
            startDate: $("[name=StartDate]").val(),
            endDate: $("[name=EndDate]").val(),
            sortParams: $("[name=SortParams]").val()
        })
    },
    Restore: function(id, episodeId, patientId) {
        Acore.Confirm({
            Message: "Are you sure you want to restore this task/document?",
            Yes: function() {
                U.PostUrl("Schedule/Restore", { id: id, episodeId: episodeId, patientId: patientId }, function(result) {
                    U.GrowlResult(result);
                    if (result.isSuccessful) U.RebindAllGrids();
                })
            }
        })
    }
}

