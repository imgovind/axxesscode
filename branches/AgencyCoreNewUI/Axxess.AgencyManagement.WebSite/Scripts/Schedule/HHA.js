﻿$.extend(Schedule.Visit, {
    HHAideCarePlan: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            U.HideIfChecked($(prefix + "IsVitalSignParameter"), $(prefix + "IsVitalSignParameterMore"));
            Schedule.Visit.Shared.Init(type, Schedule.Visit.HHAideCarePlan.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "HHAideCarePlan") }
    },
    HHAideSupervisoryVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.HHAideSupervisoryVisit.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "HHAideSupervisoryVisit") }
    },
    HHAideVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "HomeboundStatus"), "8", $(prefix + "HomeboundStatusOther"));
            U.HideIfChecked($(prefix + "IsVitalSignParameter"), $(prefix + "IsVitalSignParameterMore"));
            U.HideIfChecked($(prefix + "IsVitalSigns"), $(prefix + "IsVitalSignsMore"));
            Schedule.Visit.Shared.Init(type, Schedule.Visit.HHAideVisit.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "HHAideVisit") }
    }
});