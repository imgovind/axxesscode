﻿$.extend(Schedule, {
    MissedVisit: {
        InitNew: function(r, t, x, e) {
        },
        New: function(episodeId, patientId, eventId) {
            Acore.Modal({
                Name: "Missed Visit",
                Url: "Schedule/MissedVisit",
                Input: { episodeId: episodeId, patientId: patientId, eventId: eventId },
                OnLoad: Schedule.MissedVisit.InitNew,
                Width: 800
            })
        },
        View: function(id) {
            Acore.Modal({
                Name: "Missed Visit",
                Url: "Schedule/MissedVisitInfo",
                Input: { id: id },
                Width: 500
            })
        }
    }
});
