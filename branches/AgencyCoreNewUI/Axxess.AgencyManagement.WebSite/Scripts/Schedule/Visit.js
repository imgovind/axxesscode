﻿$.extend(Schedule, {
    Visit: {
        Shared: {
            Init: function(Type, InitFunction, ContentUrl) {
                var Prefix = "#" + Type + "_";
                if (ContentUrl == undefined) ContentUrl = "Schedule/NoteContent";
                if ($("#" + Type + "_PreviousNotes").length > 0) {
                    $("#" + Type + "_PreviousNotes").change(function() {
                        $(Prefix + "Content").Load(ContentUrl, {
                            patientId: $("#" + Type + "_PatientId").val(),
                            noteId: $("#" + Type + "_EventId").val(),
                            previousNoteId: $("#" + Type + "_PreviousNotes").val(),
                            type: Type
                        }, InitFunction);
                    });
                }
                if ($("#" + Type + "_EditStatusComment").length > 0) $("#" + Type + "_EditStatusComment").click(function() { Acore.ReturnReason($("#" + Type + "_StatusComment")) });
            },
            Load: function(EpisodeId, PatientId, EventId, Type) {
                Acore.Open(Type, { eventId: EventId, episodeId: EpisodeId, patientId: PatientId });
            },
            Submit: function(Button, Completing, Type) {
                if (Completing) $(".complete-required", "#window_" + Type).addClass("required");
                else $(".complete-required", "#window_" + Type).removeClass("required");
                $("#" + Type + "_Button").val(Button.text());
                Button.closest("form").validate();
                if (Button.closest("form").valid()) {
                    Button.closest("form").ajaxSubmit({
                        dataType: "json",
                        beforeSubmit: Schedule.Visit.Shared.BeforeSubmit,
                        success: function(Result) { Schedule.Visit.Shared.AfterSubmit(Result, Button.text(), Type) },
                        error: function(Result) { Schedule.Visit.Shared.AfterSubmit(Result, Button.text(), Type) }
                    })
                } else U.ValidationError(Button);
            },
            BeforeSubmit: function() { },
            AfterSubmit: function(Result, Command, Type) {
                if (Result.isSuccessful) switch (Command) {
                    case "Save":
                        U.Growl(Result.errorMessage, "success");
                        UserInterface.Refresh();
                        break;
                    case "Complete":
                    case "Approve":
                    case "Return":
                        U.Growl(Result.errorMessage, "success");
                        UserInterface.CloseAndRefresh(Type);
                        break;
                } else U.Growl(Result.errorMessage, "error");
            },
            Print: function(EventId, EpisodeId, PatientId, QA, Type, Load) {
                if (Load == undefined) Load = function() {
                    eval("Schedule.Visit.Load('" + EpisodeId + "','" + PatientId + "','" + EventId + "','" + Type + "')");
                    UserInterface.CloseModal();
                };
                var Arguments = {
                    Url: "Schedule/NotePrintPreview/" + EpisodeId + "/" + PatientId + "/" + EventId,
                    PdfUrl: "Schedule/NotePdf",
                    Data: {
                        EventId: EventId,
                        EpisodeId: EpisodeId,
                        PatientId: PatientId
                    }
                };
                if (QA) $.extend(Arguments, {
                    ReturnClick: function() { Schedule.ProcessNote("Return", EpisodeId, PatientId, EventId) },
                    Buttons: [
                        {
                            Text: "Edit",
                            Click: function() {
                                if (typeof Load === "function") {
                                    try {
                                        Load();
                                    } catch (e) {
                                        U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                        console.log("Error in edit function on load for " + Type + ": " + e.toString());
                                        $.error(e)
                                    }
                                }
                            }
                        }, {
                            Text: "Approve",
                            Click: function() {
                                Schedule.ProcessNote("Approve", EpisodeId, PatientId, EventId)
                            }
                        }
                    ]
                });
                Acore.OpenPrintView(Arguments);
            },
            BlankPrint: function(task) { U.GetAttachment("Schedule/NoteBlank", { Type: task }); }
        }
    }
});