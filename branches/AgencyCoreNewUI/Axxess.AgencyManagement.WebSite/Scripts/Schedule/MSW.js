﻿$.extend(Schedule.Visit, {
    DriverOrTransportationNote: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.DriverOrTransportationNote.Init);
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    MSWEvaluationAssessment: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            U.ShowIfChecked($(prefix + "GenericLivingSituation10"), $(prefix + "GenericLivingSituationOther"));
            U.ShowIfChecked($(prefix + "GenericReasonForReferral11"), $(prefix + "GenericReasonForReferralOther"));
            U.ShowIfChecked($(prefix + "GenericEmotionalStatus10"), $(prefix + "GenericEmotionalStatusOther"));
            U.ShowIfChecked($(prefix + "GenericPlannedInterventions11"), $(prefix + "GenericPlannedInterventionsOther"));
            U.ShowIfChecked($(prefix + "GenericGoals8"), $(prefix + "GenericGoalsOther"));
            Schedule.Visit.Shared.Init(type, Schedule.Visit.MSWEvaluationAssessment.Init);
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    MSWProgressNote: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.MSWProgressNote.Init);
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    MSWVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", element).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.MSWVisit.Init);
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    }
});