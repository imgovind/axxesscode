﻿$.extend(Schedule, {
    PD: {
        Center: {
            AcoreId: "PDScheduleCenter",    // Acore Window ID for Private Duty Schedule Center
            CookieId: "PDScheduleCenter",   // Name for cookie to store previous state
            PatientId: "",                  // Current Active Patient's GUID
            PatientName: "",                // Current Active Patient's Name
            // Initialize Private Duty Schedule Center
            Init: function(r, t, x, e) {
                // Set layout for patient selector
                $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
                // Set new patient button
                $(".new-patient", e).click(function() {
                    Patient.New();
                    return false;
                });
                // Set patient selector filters
                $(".ui-layout-west .top input", e).keyup(function() { Schedule.PD.Center.PatientSelector.Filter(e) });
                $(".ui-layout-west .top select", e).change(function() { Schedule.PD.Center.PatientSelector.Rebind(e) });
                $(".pd-calendar", e).PDScheduler();
            },
            Load: function(patientId, currentDate) {
                if (Acore.Windows[Schedule.PD.Center.AcoreId].IsOpen) {
                    Schedule.PD.Center.LoadContent(patientId, episodeId);
                    $("#window_" + Schedule.PD.Center.AcoreId).WinFocus();
                } else {
                    Schedule.PD.Center.PatientId = patientId, Schedule.PD.Center.CurrentDate = currentDate;
                    Acore.Open(Schedule.PD.Center.AcoreId);
                }
            },
            LoadContent: function(patientId) {
                var e = $("#window_" + Schedule.PD.Center.AcoreId + "_content");
                var row = $(".ui-layout-west .t-last:contains(" + patientId + ")", e).parent(), scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
                if (Acore.Animated) row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
                else row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll);
                if (U.IsGuid(patientId)) Schedule.PD.Center.PatientId = patientId;
                Schedule.PD.Center.Refresh();
            },
            PatientSelector: {
                Init: function() {
                    var PreviousState = U.Cookie.Get(Schedule.PD.Center.CookieId);
                    if (PreviousState) Schedule.PD.Center.PatientId = PreviousState.PatientId;
                    var e = $("#window_" + Schedule.PD.Center.AcoreId + "_content");
                    Schedule.PD.Center.PatientSelector.Filter(e);
                    if (U.IsGuid(Schedule.PD.Center.PatientId) && $(".ui-layout-west .t-last:contains(" + Schedule.PD.Center.PatientId + ")", e).length) Schedule.PD.Center.LoadContent(Schedule.PD.Center.PatientId);
                    else if ($(".ui-layout-west .t-last", e).length) Schedule.PD.Center.LoadContent($(".ui-layout-west .t-last:first", e).text());
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "No patients found for your selection.  Please reduce your filters or add a new patient on the top left of this window."));
                },
                Filter: function(e) {
                    var text = $(".ui-layout-west .top input", e).val();
                    if (text && text.length) {
                        search = text.split(" ");
                        $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                        for (var i = 0; i < search.length; i++) $(".ui-layout-west .t-grid-content", e).find("td").each(function() {
                            if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                        });
                        $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                        if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                        else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Patients", "There were no patients found meeting your search parameters"));
                    } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
                },
                Rebind: function(e) {
                    U.RebindGrid($(".ui-layout-west .t-grid", e), {
                        branchId: $("[name=BranchId]", e).val(),
                        statusId: $("[name=StatusId]", e).val(),
                        paymentSourceId: $("[name=PaymentSourceId]").val()
                    })
                },
                Select: function(e) {
                    Schedule.PD.Center.PatientName = $("td:first", e.row).text() + ", " + $("td:eq(1)", e.row).text();
                    Schedule.PD.Center.LoadContent($(".t-last", e.row).text());
                }
            },
            Refresh: function() {
                var e = $("#window_" + Schedule.PD.Center.AcoreId + "_content");
                $(".pd-calendar", e).attr("patient", Schedule.PD.Center.PatientId).PDScheduler("refresh");
                //$(".fc-patient-name", e).html(Schedule.PD.Center.PatientName + " &#8211; ");
            },
            Scheduler: {
                Init: function(r, t, x, e) {
                    $(".add-schedule", e).click(function() {
                        Schedule.PD.Center.Scheduler.Add(e, $(this).hasClass("another"));
                        return false;
                    })
                    $("select", e).change(function() { if ($(this).val() == "" || $(this).val() == U.GuidEmpty) { $(this).addClass("error") } else { $(this).removeClass("error") } });
                },
                New: function(patientId, date, view) {
                    Acore.Modal({
                        Name: "Quick Employee Scheduler",
                        Url: "Schedule/PD/Event/New",
                        Input: { patientId: patientId, date: date, view: view },
                        OnLoad: Schedule.PD.Center.Scheduler.Init,
                        Width: 800
                    })
                },
                Add: function(e, another) {
                    if (!parseInt($("select:eq(0)", e).val())) $("select:eq(0)", e).addClass("error");
                    else $("select:eq(0)", e).removeClass("error");
                    if (!U.IsGuid($("select:eq(1)", e).val())) $("select:eq(1)", e).addClass("error");
                    else $("select:eq(1)", e).removeClass("error");
                    var eventDays = e.find("fieldset input[name=EventDate]").serializeArray();
                    var nonDateField = e.find("fieldset .non-date-field").serializeArray();
                    if (eventDays != undefined && eventDays != null && eventDays.length > 1) {
                        var input = [];
                        $.merge(input, nonDateField);
                        $.merge(input, eventDays);
                        U.PostUrl("Schedule/PD/Event/Add", input, function(result) {
                            U.GrowlResult(result);
                            Schedule.PD.Center.Refresh();
                            if (result.isSuccessful) {
                                if (another) {
                                    $("select", e).val("");
                                    $("input[name=EventDate]", e).attr('checked', false);
                                    $("#window_" + Acore.DialogId + "_content").removeClass("loading");
                                } else $("#window_" + Acore.DialogId).Close();
                            } else $("#window_" + Acore.DialogId + "_content").removeClass("loading");
                        });
                    }
                    else U.Growl("Please select date(s) on the calendar", "error");
                }
            }
        }
    }
});
