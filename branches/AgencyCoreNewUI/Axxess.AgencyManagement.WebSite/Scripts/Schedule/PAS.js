﻿$.extend(Schedule.Visit, {
    PASCarePlan: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.ShowIfChecked($(Prefix + "IsDiet"), $(Prefix + "Diet"));
            U.ShowIfChecked($(Prefix + "Allergies"), $(Prefix + "AllergiesDescription"));
            U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
            U.ShowIfChecked($(Prefix + "ActivitiesPermitted12"), $(Prefix + "ActivitiesPermittedOther"));
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PASCarePlan.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "PASCarePlan") }
    },
    PASVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.ShowIfSelectEquals($(Prefix + "HomeboundStatus"), "8", $(Prefix + "HomeboundStatusOther"));
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PASVisit.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "PASVisit") }
    }
});