﻿$.extend(Schedule, {
    MasterCalendar: {
        Load: function(episodeId, patientId) {
            Acore.Open("MasterCalendar", { episodeId: episodeId, patientId: patientId });
        },
        Init: function(r, t, x, e) {
            $(".navigate", e).click(function() {
                Schedule.MasterCalendar.Navigate($(this).attr("guid"), $("[name=patientId]", e).val(), e);
                return false;
            });
            $(".print", e).click(function() {
                Schedule.MasterCalendar.Print($(this).attr("guid"), $("[name=patientId]", e).val());
                return false;
            })
        },
        Navigate: function(episodeId, patientId, e) {
            if (!e.hasClass("content")) e = $(".content", e);
            e.Load("Schedule/MasterCalendar/Content", { episodeId: episodeId, patientId: patientId }, Schedule.MasterCalendar.Init);
        },
        Print: function(episodeId, patientId) {
            U.GetAttachment("Schedule/MasterCalendar/Print", { episodeId: episodeId, patientId: patientId });
        }
    }
});
