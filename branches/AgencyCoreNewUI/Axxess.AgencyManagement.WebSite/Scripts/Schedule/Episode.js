﻿$.extend(Schedule, {
    Episode: {
        Activate: function(id, patientId) {
            Acore.Confirm({
                Message: "Are you sure you want to reactivate this episode?",
                Yes: function() {
                    U.PostUrl("Schedule/Episode/Activate", { id: id, patientId: patientId }, function() {
                        if ($("#InactiveEpisodesList_Content").length) Schedule.Episode.RefreshInactiveList(patientId);
                    })
                }
            })
        },
        Edit: function(id, patientId) {
            Acore.Modal({
                Name: "Edit Episode",
                Url: "Schedule/Episode/Edit",
                Input: { id: id, patientId: patientId },
                OnLoad: Schedule.Episode.InitEdit,
                Width: 800
            })
        },
        FrequencyList: function(id, patientId) {
            Acore.Modal({
                Name: "FrequencyList",
                Url: "Schedule/Episode/FrequencyList",
                Input: { id: id, patientId: patientId },
                OnLoad: Schedule.Episode.InitFrequencyList,
                Width: 550
            })
        },
        InactiveList: function(patientId) {
            Acore.Open("InactiveEpisode", { patientId: patientId });
        },
        InitEdit: function(r, t, x, e) {
            var patientId = $("[name=PatientId]", e).val(), active = $("[name=IsActive]", e).prop("checked");
            $("input[name=StartDate]", e).change(function() { if (U.IsDate($(this).val())) Schedule.Episode.SetEndDate(e) });
            $(".activity-log", e).click(function() {
                Schedule.Episode.Logs($("[name=Id]", e).val(), $("[name=PatientId]", e).val());
                return false;
            });
            $("form", e).Validate({
                Success: function(r, e) {
                    if ($("[name=IsActive]", e).prop("checked") != active && $("#InactiveEpisodesList_Content").length) Schedule.Episode.RefreshInactiveList(patientId);
                    if ($("[name=IsActive]", e).prop("checked")) Schedule.Center.Load(patientId);
                }
            });
        },
        InitFrequencyList: function(r, t, x, e) { },
        InitInactiveList: function(r, t, x, e) { },
        InitNew: function(r, t, x, e) {
            if (e.closest(".window").attr("id") == "window_Dialog") U.NewPrefix("NewEpisodeModal", e);
            var Type = U.GetType(e), Prefix = "#" + Type + "_";
            $("input[name=StartDate]", e).change(function() { if (U.IsDate($(this).val())) Schedule.Episode.SetEndDate(e) });
            $(Prefix + "PatientId").change(function() {
                $(Prefix + "Content").Load("Schedule/NewEpisodeContent", { patientId: $(this).val() }, function(r, t, x, e) { Schedule.SetEndDate(e) })
            });
            U.InitShared(e);
        },
        Logs: function(id, patientId) {
            Acore.CloseDialog();
            Acore.Open("EpisodeLogs", { id: id, patientId: patientId });
        },
        New: function(id, modal) {
            if (id) var args = { patientId: id };
            else var args = {};
            if (modal) Acore.Modal({
                Name: "New Episode",
                Url: "Schedule/Episode/New",
                Input: args,
                OnLoad: Schedule.Episode.InitNew,
                Width: 800
            });
            else Acore.Open("NewEpisode", args);
        },
        NewModal: function(id) {
            Schedule.Episode.New(id, true);
        },
        RefreshInactiveList: function(patientId) {
            $("#InactiveEpisodesList_Content").Load("Schedule/Episode/InactiveGrid", { patientId: patientId });
        },
        SetEndDate: function(e) {
            $("input[name=StartDate]", e).blur(function() {
                $("input[name=EndDate]", e).datepicker("setDate", new Date($(this).datepicker("getDate").getTime() + 60 * 24 * 60 * 60 * 1000))
            })
        }
    }
});
