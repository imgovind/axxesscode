﻿$.extend(Schedule.Visit, {
    DischargeSummary: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.DischargeSummary.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "DischargeSummary") }
    },
    LVNSupervisoryVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.LVNSupervisoryVisit.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "LVNSupervisoryVisit") }
    },
    SixtyDaySummary: {
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "SixtyDaySummary") }
    },
    SkilledNurseVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            $(prefix + "GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
            U.HideIfChecked($(prefix + "GenericIsIVApplied1"), $(prefix + "IVContainer"));
            U.HideIfChecked($(prefix + "GenericIsDiabeticCareApplied1"), $(prefix + "DiabeticCareContainer"));
            U.HideIfChecked($(prefix + "GenericIsPhlebotomyApplied1"), $(prefix + "GenericPhlebotomyContainer"));
            Schedule.Visit.Shared.Init(type, Schedule.Visit.SkilledNurseVisit.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "SkilledNurseVisit") }
    },
    SNDiabeticDailyVisit: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.SNDiabeticDailyVisit.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "SNDiabeticDailyVisit") }
    },
    TransferSummary: {
        Init: function(r, t, x, e) {
            var type = $("input[name=Type]", e).val(), prefix = "#" + type + "_";
            Schedule.Visit.Shared.Init(type, Schedule.Visit.TransferSummary.Init);
        },
        Submit: function(Button, Completing) { Schedule.Visit.Shared.Submit(Button, Completing, "TransferSummary") }
    }
});