﻿$.extend(Schedule.Visit, {
    OTEvaluation: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.OTVisit.Init, "Schedule/OTEvaluationContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    OTVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.OTVisit.Init, "Schedule/OTVisitContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    PTDischarge: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PTEvaluation.Init, "Schedule/PTDischargeContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    PTEvaluation: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PTEvaluation.Init, "Schedule/PTEvaluationContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    PTVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PTVisit.Init, "Schedule/PTVisitContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    STEvaluation: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.ShowIfChecked($(Prefix + "GenericHomeboundReason9"), $(Prefix + "GenericHomeboundReasonOther"));
            U.ShowIfRadioEquals(Type + "_GenericOrdersForEvaluationOnly", "0", $(Prefix + "GenericIfNoOrdersAreSpan"));
            U.ShowIfRadioEquals(Type + "_GenericIsSSE", "1", $(Prefix + "GenericGenericSSESpecifySpan"));
            U.ShowIfRadioEquals(Type + "_GenericIsVideoFluoroscopy", "1", $(Prefix + "GenericVideoFluoroscopySpecify"));
            U.ShowIfChecked($(Prefix + "GenericLiquids2"), $(Prefix + "GenericLiquidsThick"));
            U.ShowIfChecked($(Prefix + "GenericLiquids3"), $(Prefix + "GenericLiquidsOther"));
            U.ShowIfChecked($(Prefix + "GenericReferralFor4"), $(Prefix + "GenericReferralForOther"));
            U.ShowIfChecked($(Prefix + "GenericDischargeDiscussedWith4"), $(Prefix + "GenericDischargeDiscussedWithOther"));
            U.ShowIfChecked($(Prefix + "GenericCareCoordination7"), $(Prefix + "GenericCareCoordinationOther"));
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PTEvaluation.Init, "Schedule/STEvaluationContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    },
    STVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Schedule.Visit.Shared.Init(Type, Schedule.Visit.PTVisit.Init, "Schedule/STVisitContent");
        },
        Submit: function(Button, Completing, Type) { Schedule.Visit.Shared.Submit(Button, Completing, Type) }
    }
});