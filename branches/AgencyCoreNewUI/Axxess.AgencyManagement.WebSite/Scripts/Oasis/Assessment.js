﻿$.extend(OASIS, {
    Assessment: {
        Load: function(Id, PatientId, AssessmentType) { Acore.Open(AssessmentType, "Oasis/OASISView", function() { OASIS.Assessment.Shared.Init(Id, PatientId, AssessmentType) }, { Id: Id, PatientId: PatientId }) },
        Print: function(EventId, EpisodeId, PatientId, QA) { OASIS.Assessment.Shared.Print(EventId, EpisodeId, PatientId, QA) },
        BlankPrint: function(AssessmentType, Discipline) { U.GetAttachment("Oasis/OASISBlank", { AssessmentType: AssessmentType, Discipline: Discipline }); },
        Shared: {
            GetAssessmentTypeNumber: function(AssessmentType) {
                switch (AssessmentType) {
                    case "StartOfCare": return 1;
                    case "ResumptionOfCare": return 3;
                    case "Recertification": return 4;
                    case "FollowUp": return 5;
                    case "TransferInPatientNotDischarged": return 6;
                    case "TransferInPatientDischarged": return 7;
                    case "DischargeFromAgencyDeath": return 8;
                    case "DischargeFromAgency": return 9;
                    case "NonOasisStartOfCare": return 11;
                    case "NonOasisRecertification": return 14;
                    case "NonOasisDischarge": return 19;
                }
            },
            Init: function(Id, PatientId, AssessmentType) {
                AssessmentType = $("input[name=assessment]").val();
                Id = $("#" + AssessmentType + "_Id").val();
                PatientId = $("#" + AssessmentType + "_PatientGuid").val();
                $("#" + AssessmentType + "_Tabs").addClass("ui-tabs-vertical ui-helper-clearfix").tabs().bind("tabsselect", {
                    Id: Id,
                    PatientId: PatientId,
                    AssessmentType: AssessmentType
                }, function(event, ui) {
                    $($(ui.tab).attr("href")).Load("Oasis/Category", {
                        Id: event.data.Id,
                        PatientId: event.data.PatientId,
                        AssessmentType: event.data.AssessmentType,
                        Category: $(ui.tab).attr("href")
                    }, OASIS.Assessment.Shared.InitSection);
                }).find("li").removeClass("ui-corner-top").addClass("ui-corner-left");
                OASIS.Assessment.Shared.InitSection(undefined, undefined, undefined, $(".general:eq(0)", "#" + AssessmentType + "_Tabs"))
            },
            InitSection: function(r, t, x, e) {
                var AssessmentType = e.attr("id").split("_")[0],
                   Category = e.attr("id").split("_")[1];
                if (/^NonOasis.*$/.test(AssessmentType)) {
                    $("fieldset.oasis", e).removeClass("oasis");
                    $("a.green,.oasis-tip", e).remove();
                } else $("fieldset.oasis.loc485", e).removeClass("loc485");
                $(".interventions input[type=checkbox],.goals input[type=checkbox]", e).each(function() {
                    if ($(this).parent().find("span.radio").length) {
                        if ($(this).is(":not(:checked)")) $(this).parent().find("span.radio :input").attr("disabled", true);
                        $(this).change(function() {
                            if ($(this).is(":checked")) $(this).parent().find("span.radio :input").attr("disabled", false);
                            else $(this).parent().find("span.radio :input").attr("disabled", true);
                        })
                    }
                })
                if (typeof OASIS.Assessment.Shared.Tabs[Category] == "function") OASIS.Assessment.Shared.Tabs[Category](AssessmentType, OASIS.Assessment.Shared.GetAssessmentTypeNumber(AssessmentType), e);
            },
            Print: function(EventId, EpisodeId, PatientId, QA) {
                var Arguments = {
                    Url: "OASISPrintPreview/" + EpisodeId + "/" + PatientId + "/" + EventId,
                    PdfUrl: "Oasis/OASISPdf",
                    Data: {
                        EventId: EventId,
                        EpisodeId: EpisodeId,
                        PatientId: PatientId
                    }
                };
                if (QA) $.extend(Arguments, {
                    ReturnClick: function() { Oasis.StatusAction(EventId, PatientId, EpisodeId, "Return") },
                    Buttons: [
                        {
                            Text: "Edit",
                            Click: function() {
                                if (typeof Load === "function") {
                                    try {
                                        eval("OASIS.Assessment.Load('" + EpisodeId + "','" + PatientId + "','" + EventId + "')");
                                        UserInterface.CloseModal();
                                    } catch (e) {
                                        U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                        console.log("Error in edit function on load for this  Assessment : " + e.toString());
                                        $.error(e)
                                    }
                                }
                            }
                        }, {
                            Text: "OASIS Scrubber",
                            Click: function() {
                                U.GetAttachment("Oasis/AuditPdf", {
                                    id: EventId,
                                    patientId: PatientId,
                                    episodeId: EpisodeId
                                })
                            }
                        }, {
                            Text: "Approve",
                            Click: function() { Oasis.StatusAction(EventId, PatientId, EpisodeId, "Approve") }
                        }
                    ]
                });
                Acore.OpenPrintView(Arguments);
            },
            Submit: function(Button, AssessmentType) {
                Button.closest("form").validate();
                if (Button.closest("form").valid()) {
                    Button.closest("form").ajaxSubmit({
                        dataType: "json",
                        beforeSubmit: OASIS.Assessment.Shared.BeforeSubmit,
                        success: function(Result) { OASIS.Assessment.Shared.AfterSubmit(Result, Button.text(), AssessmentType) },
                        error: function(Result) { OASIS.Assessment.Shared.AfterSubmit(Result, Button.text(), AssessmentType) }
                    })
                } else U.ValidationError(Button);
            },
            BeforeSubmit: function() { },
            AfterSubmit: function(Result, Command, AssessmentType) {
                if (Result.isSuccessful) switch (Command) {
                    case "Save":
                        U.Growl(Result.errorMessage, "success");
                        break;
                    case "Save & Continue":
                        U.Growl(Result.errorMessage, "success");
                        $("#" + AssessmentType + "_Tabs").tabs().tabs("select", $(".vertical-tab-list", "#" + AssessmentType + "_Tabs").children("li").index($(".ui-tabs-selected")) + 1)
                        break;
                    case "Save & Check for Errors":
                    case "Check for Errors":
                        Oasis.Validate(AssessmentType);
                        break;
                    case "Save & Complete":
                        Oasis.NonOasisSignature(AssessmentType);
                        break;
                    case "Approve":
                    case "Return":
                    case "Save & Exit":
                        U.Growl(Result.errorMessage, "success");
                        UserInterface.CloseAndRefresh(AssessmentType);
                        break;
                } else U.Growl(Result.errorMessage, "error");
            },
            Tabs: {
                AdlIadl: function(type, typeNum, e) {
                    if (typeNum % 10 < 5) {
                        U.ShowIfChecked($("#" + type + "_485ActivitiesPermittedD"), $("#" + type + "_485ActivitiesPermittedDMore"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal2"), $("#" + type + "_GenericMusculoskeletal2More"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal3"), $("#" + type + "_GenericMusculoskeletalImpairedMotorSkills"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal4"), $("#" + type + "_GenericMusculoskeletal4More"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal5"), $("#" + type + "_GenericMusculoskeletalMobility"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal6"), $("#" + type + "_GenericMusculoskeletal6More"));
                        U.ShowIfChecked($("#" + type + "_GenericAssistiveDevice5"), $("#" + type + "_GenericAssistiveDeviceOther"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal7"), $("#" + type + "_GenericMusculoskeletal7More"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal9"), $("#" + type + "_GenericMusculoskeletal9More"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal12"), $("#" + type + "_GenericMusculoskeletal12More"));
                        U.ShowIfChecked($("#" + type + "_GenericMusculoskeletal13"), $("#" + type + "_GenericMusculoskeletal13More"));
                        $("#" + type + "_FallAssessment").FallAssessment();
                        $("#" + type + "_TinettiAssessment").TinettiAssessment();
                    }
                },
                Cardiac: function(type, typeNum, e) {
                    if (typeNum % 10 < 5) {
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular2"), $("#" + type + "_GenericCardioVascular2More"));
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular3"), $("#" + type + "_GenericCardioVascular3More"));
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular4"), $("#" + type + "_GenericCardioVascular4More"));
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular5"), $("#" + type + "_GenericCardioVascular5More"));
                        U.ShowIfChecked($("#" + type + "_GenericPittingEdemaType1"), $("#" + type + "_GenericPittingEdemaType1More"));
                        U.ShowIfChecked($("#" + type + "_GenericPittingEdemaType2"), $("." + type + "_GenericPittingEdemaType2More"));
                        U.ShowIfChecked($("#" + type + "_GenericPittingEdemaType2"), $("#" + type + "_GenericPittingEdemaType2More"));
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular6"), $("#" + type + "_GenericCardioVascular6More"));
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular7"), $("#" + type + "_GenericCardioVascular7More"));
                        U.ShowIfChecked($("#" + type + "_GenericCardioVascular8"), $("#" + type + "_GenericCardioVascular8More"));
                    } else U.HideIfRadioEquals(type + "_M1500HeartFailureSymptons", "00|NA", $("#" + type + "_M1510"));
                },
                Demographics: function(type, typeNum, e) {
                    $("input[name=" + type + "_M0100AssessmentType]").click(function() {
                        $("input[name=" + type + "_M0100AssessmentType]").eq(typeNum % 10 > 1 ? typeNum % 10 - 1 : typeNum % 10).prop("checked", true)
                    })
                    U.HideIfChecked($("#" + type + "_M0064PatientSSNUnknown"), $("#" + type + "_M0064PatientSSN"));
                    U.HideIfChecked($("#" + type + "_M0063PatientMedicareNumberUnknown"), $("#" + type + "_M0063PatientMedicareNumber"));
                    U.HideIfChecked($("#" + type + "_M0065PatientMedicaidNumberUnknown"), $("#" + type + "_M0065PatientMedicaidNumber"));
                    U.HideIfChecked($("#" + type + "_M0032ROCDateNotApplicable"), $("#" + type + "_M0032ROCDate").parent());
                    U.HideIfChecked($("#" + type + "_M0018NationalProviderIdUnknown"), $("#" + type + "_M0018NationalProviderId"));
                    if (typeNum < 6) {
                        U.HideIfChecked($("#" + type + "_M0102PhysicianOrderedDateNotApplicable"), $("#" + type + "_M0102PhysicianOrderedDate").parent());
                        $("#" + type + "_M0102PhysicianOrderedDateNotApplicable").change(function() {
                            var date = $("#" + type + "_M0102PhysicianOrderedDate").val();
                            if ($(this).prop("checked")) $("#" + type + "_M0104").show().removeClass("form-omitted");
                            else if (date && date.length) $("#" + type + "_M0104").hide().addClass("form-omitted");
                            else $("#" + type + "_M0104").show().removeClass("form-omitted");
                        });
                        $("#" + type + "_M0102PhysicianOrderedDate").blur(function() {
                            if ($(this).val() && $(this).val().length) $("#" + type + "_M0104").hide().addClass("form-omitted");
                            else $("#" + type + "_M0104").show().removeClass("form-omitted")
                        })
                    }
                    if (typeNum != 8) {
                        U.NoneOfTheAbove($("#" + type + "_M0150PaymentSourceUnknown"), $("#" + type + "_M0150 .M0150"));
                        U.ShowIfChecked($("#" + type + "_M0150PaymentSourceOtherSRS"), $("#" + type + "_M0150PaymentSourceOtherSRSMore"));
                    }
                    $("#" + type + "_PreviousAssessmentButton").click(function() {
                        var previousAssessmentArray = $("#" + type + "_PreviousAssessments").val().split('_');
                        if (previousAssessmentArray.length > 1) {
                            if (confirm("Are you sure you want to load this assessment?")) {
                                var episodeId = $("#" + type + "_EpisodeId").val(),
                            patientId = $("#" + type + "_PatientGuid").val(),
                            assessmentId = $("#" + type + "_Id").val(),
                            type = $("#" + type + "_AssessmentType").val();
                                U.PostUrl("Oasis/LoadPrevious", {
                                    episodeId: episodeId,
                                    patientId: patientId,
                                    assessmentId: assessmentId,
                                    assessmentType: assessmentType,
                                    previousAssessmentId: previousAssessmentArray[0],
                                    previousAssessmentType: previousAssessmentArray[1]
                                }, function(result) {
                                    if (result.isSuccessful) {
                                        Schedule.Rebind();
                                        U.Growl(result.errorMessage, "success");
                                    } else U.Growl(result.errorMessage, "error");
                                })
                            }
                        } else U.Growl("Please select a previous assessment first", "error")
                    })
                },
                Elimination: function(type, typeNum, e) {
                    U.ShowIfChecked($("#" + type + "_GenericGU10"), $("." + type + "_GenericGU10More"));
                    U.ShowIfChecked($("#" + type + "_GenericGU11"), $("#" + type + "_GenericGU11More"));
                    U.ShowIfChecked($("#" + type + "_GenericGUUrine5"), $("#" + type + "_GenericGUUrine5More"));
                    U.ShowIfChecked($("#" + type + "_GenericGU12"), $("#" + type + "_GenericGU12More"));
                    U.ShowIfRadioEquals(type + "_GenericPatientOnDialysis", "1", $("#" + type + "_Dialysis"));
                    U.ShowIfChecked($("#" + type + "_GenericDialysis5"), $("#" + type + "_GenericDialysis5More"));
                    U.ShowIfChecked($("#" + type + "_GenericDialysisHemodialysis1"), $("#" + type + "_GenericDialysisHemodialysis1More"));
                    U.ShowIfChecked($("#" + type + "_GenericDialysisHemodialysis2"), $("#" + type + "_GenericDialysisHemodialysis2More"));
                    U.ShowIfChecked($("#" + type + "_GenericDialysis7"), $("#" + type + "_GenericDialysis7More"));
                    U.ShowIfRadioEquals(type + "_M1610UrinaryIncontinence", "01", $("#" + type + "_M1615"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestive2"), $("#" + type + "_GenericDigestive2More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestive3"), $("#" + type + "_GenericDigestive3More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestive8"), $("#" + type + "_GenericDigestive8More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestive11"), $("#" + type + "_GenericDigestive11More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestiveLastBM2"), $("#" + type + "_GenericDigestiveLastBM2More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestiveLastBM3"), $("#" + type + "_GenericDigestiveLastBM3More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestiveLastBM4"), $("#" + type + "_GenericDigestiveLastBM4More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestiveOstomy1"), $("#" + type + "_GenericDigestiveOstomy1More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestiveOstomy2"), $("#" + type + "_GenericDigestiveOstomy2More"));
                    U.ShowIfChecked($("#" + type + "_GenericDigestiveOstomy3"), $("#" + type + "_GenericDigestiveOstomy3More"));
                },
                EmergentCare: function(type, typeNum, e) {
                    U.HideIfRadioEquals(type + "_M2300EmergentCare", "00|UK", $("#" + type + "_M2310"));
                    U.NoneOfTheAbove($("#" + type + "_M2310ReasonForEmergentCareUK"), $(".M2310", Element));
                },
                Endocrine: function(type, typeNum, e) {
                    U.ShowIfRadioEquals(type + "_GenericPatientDiabetic", "1", $("#window_" + type + " .diabetic"));
                    U.ShowIfRadioEquals(type + "_GenericInsulinDependent", "1", $(type + "_GenericInsulinDependentMore"));
                    $("#" + type + "_GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
                },
                Integumentary: function(type, typeNum, e) {
                    if (typeNum % 10 < 5) {
                        $("#" + type + "_BradenScale").BradenScale();
                        U.ShowIfChecked($("#" + type + "_GenericSkinColor5"), $("#" + type + "_GenericSkinColor5More"));
                        U.ShowIfChecked($("#" + type + "_GenericSkinCondition6"), $("#" + type + "_GenericSkinCondition6More"));
                        U.ShowIfRadioEquals(type + "_GenericNails", "Problem", $("#" + type + "_GenericNailsProblemMore"));
                        U.ShowIfRadioEquals(type + "_GenericPressureRelievingDevice", "1", $("#" + type + "_GenericPressureRelievingDeviceMore"));
                        U.ShowIfSelectEquals($("#" + type + "_GenericPressureRelievingDeviceType"), "4", $("#" + type + "_GenericPressureRelievingDeviceTypeOther"));
                        U.ShowIfRadioEquals(type + "_GenericIVAccess", "1", $("." + type + "_IVAccess"));
                        U.ShowIfRadioEquals(type + "_GenericFlush", "1", $("#" + type + "_GenericFlushMore"));
                    }
                    if (typeNum != 11 && typeNum != 14) {
                        if (typeNum % 10 < 5) U.HideIfRadioEquals(type + "_M1300PressureUlcerAssessment", "00", $("#" + type + "_M1302"));
                        U.HideIfRadioEquals(type + "_M1306UnhealedPressureUlcers", "0", $("." + type + "_M1306_00"));
                        if (typeNum % 10 > 8) U.ShowIfRadioEquals(type + "_M1307NonEpithelializedStageTwoUlcer", "02", $("#" + type + "_M1307NonEpithelializedStageTwoUlcer02More"));
                        U.HideIfRadioEquals(type + "_M1330StasisUlcer", "00|03", $("#" + type + "_M1332"));
                        U.HideIfRadioEquals(type + "_M1330StasisUlcer", "00|03", $("#" + type + "_M1334"));
                        U.HideIfRadioEquals(type + "_M1340SurgicalWound", "00|02", $("#" + type + "_M1342"));
                        $("#" + type + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent,#" + type + "_M1308NumberNonEpithelializedStageFourUlcerCurrent,#" + type + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").keyup(function() {
                            var stage3 = $("#" + type + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(),
	                        stage4 = $("#" + type + "_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(),
	                        cvrg = $("#" + type + "_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val();
                            if (parseInt(stage3) > 0 || parseInt(stage4) > 0 || parseInt(cvrg) > 0) $("#" + type + "_M1310,#" + type + "_M1312,#" + type + "_M1314").show();
                            else $("#" + type + "_M1310,#" + type + "_M1312,#" + type + "_M1314").each(function() { $("input", this).val("") }).hide();
                        });
                        $("input[name=" + type + "_M1306UnhealedPressureUlcers]").change(function() {
                            $("#" + type + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent").keyup()
                        });
                        $("input[name=" + type + "_M1306UnhealedPressureUlcers]").change();
                    }
                    if (typeNum % 10 < 5) {
                        $(".WoundType").Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"] });
                        $(".DeviceType").Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
                        $(".IVSiteDressingUnit").Autocomplete({ source: ["Normal Saline", "Heparin"] });
                    }
                },
                Medications: function(type, typeNum, e) {
                    U.HideIfRadioEquals(type + "_M2000DrugRegimenReview", "00|01|NA", $("#" + type + "_M2002"));
                    U.HideIfRadioEquals(type + "_M2000DrugRegimenReview", "NA", $("#" + type + "_M2010"));
                    U.HideIfRadioEquals(type + "_M2000DrugRegimenReview", "NA", $("#" + type + "_M2020"));
                    U.HideIfRadioEquals(type + "_M2000DrugRegimenReview", "NA", $("#" + type + "_M2030"));
                },
                Nutrition: function(type, typeNum, e) {
                    $("#" + type + "_NutritionalHealthScreen").NutritionalHealthScreen();
                    U.ShowIfChecked($("#" + type + "_GenericNutrition4"), $("#" + type + "_GenericNutrition4More"));
                    U.ShowIfChecked($("#" + type + "_GenericNutrition5"), $("#" + type + "_GenericNutrition5More"));
                    U.ShowIfChecked($("#" + type + "_GenericNutrition6"), $("#" + type + "_GenericNutrition6More"));
                    U.ShowIfChecked($("#" + type + "_GenericNutrition8"), $("#" + type + "_GenericNutrition8More"));
                    U.ShowIfChecked($("#" + type + "_GenericNutrition9"), $("#" + type + "_GenericNutrition9More"));
                    U.EnableIfChecked($("#" + type + "_485NutritionalReqs6"), $("#" + type + "_485NutritionalReqs6More"));
                    U.EnableIfChecked($("#" + type + "_485NutritionalReqs8"), $("#" + type + "_485NutritionalReqs8More"));
                    U.ShowIfChecked($("#" + type + "_485NutritionalReqs12"), $("#" + type + "_485NutritionalReqs12More"));
                    U.ShowIfChecked($("#" + type + "_485NutritionalReqs13"), $("#" + type + "_485NutritionalReqs13More"));
                    U.ShowIfChecked($("#" + type + "_485NutritionalReqs14"), $("#" + type + "_485NutritionalReqs14More"));
                    U.ShowIfChecked($("#" + type + "_485NutritionalReqs15"), $("#" + type + "_485NutritionalReqs15More"));
                    U.ShowIfChecked($("#" + type + "_485NutritionalReqsEnteralPer4"), $("#" + type + "_485NutritionalReqsEnteralPer4More"));
                    U.ShowIfChecked($("#" + type + "_485NutritionalReqs16"), $("#" + type + "_485NutritionalReqs16More"));
                },
                NeuroBehavioral: function(type, typeNum, e) {
                    U.ShowIfChecked($("#" + type + "_GenericNeurologicalStatus1"), $("#" + type + "_GenericNeurologicalStatus1More"));
                    U.ShowIfChecked($("#" + type + "_GenericNeurologicalStatus2"), $("." + type + "_GenericNeurologicalStatus2More"));
                    U.ShowIfChecked($("#" + type + "_GenericNeurologicalStatus3"), $("#" + type + "_GenericNeurologicalStatus3More"));
                    U.ShowIfChecked($("#" + type + "_GenericNeurologicalStatus4"), $("#" + type + "_GenericNeurologicalStatus4More"));
                    U.ShowIfChecked($("#" + type + "_GenericNeurologicalStatus5"), $("#" + type + "_GenericNeurologicalStatus5More"));
                    U.ShowIfChecked($("#" + type + "_GenericNeurologicalStatus9"), $("#" + type + "_GenericNeurologicalStatus9More"));
                    U.ShowIfChecked($("#" + type + "_485MentalStatus8"), $("#" + type + "_485MentalStatus8More"));
                    U.ShowIfChecked($("#" + type + "_GenericBehaviour8"), $("#" + type + "_GenericBehaviour8More"));
                    U.ShowIfChecked($("#" + type + "_GenericPsychosocial5"), $("#" + type + "_GenericPsychosocial5More"));
                    U.ShowIfChecked($("#" + type + "_485PatientStrengths5"), $("#" + type + "_485PatientStrengths5More"));
                    U.ShowIfRadioEquals(type + "_M1730DepressionScreening", "01", $("#" + type + "_Phq2"));
                    U.NoneOfTheAbove($("#" + type + "_M1740CognitiveBehavioralPsychiatricSymptomsNone"), $(type + "_M1740 .M1740"));
                },
                OrdersDiscipline: function(type, typeNum, e) {
                    $("#" + type + "_485SIVerbalizedUnderstandingPercent").Autocomplete({ source: ["GOOD", "NO", "FAIR", "POOR", "0%", "100%", "50%", "FULL", "PARTIAL"] })
                },
                PatientHistory: function(type, typeNum, e) {
                    U.ShowIfRadioEquals(type + "_485Allergies", "Yes", $("#" + type + "_485AllergiesDescription"));
                    U.ShowIfChecked($("#" + type + "_M1000InpatientFacilitiesOTHR"), $("#" + type + "_M1000InpatientFacilitiesOTHRMore"));
                    U.NoneOfTheAbove($("#" + type + "_M1000InpatientFacilitiesNone"), $("#" + type + "_M1000 .M1000"));
                    U.HideIfChecked($("#" + type + "_M1000InpatientFacilitiesNone"), $("#" + type + "_M1005").closest("fieldset"));
                    U.HideIfChecked($("#" + type + "_M1000InpatientFacilitiesNone"), $("#" + type + "_M1010").closest("fieldset"));
                    U.HideIfChecked($("#" + type + "_M1000InpatientFacilitiesNone"), $("#" + type + "_M1012").closest("fieldset"));
                    U.HideIfChecked($("#" + type + "_M1005InpatientDischargeDateUnknown"), $("#" + type + "_M1005InpatientDischargeDate").parent());
                    U.NoneOfTheAbove($("#" + type + "_M1012InpatientFacilityProcedureCodeNotApplicable"), $("#" + type + "_M1012InpatientFacilityProcedureCodeUnknown"));
                    U.NoneOfTheAbove($("#" + type + "_M1012InpatientFacilityProcedureCodeUnknown"), $("#" + type + "_M1012InpatientFacilityProcedureCodeNotApplicable"));
                    U.HideIfChecked($("#" + type + "_M1012InpatientFacilityProcedureCodeNotApplicable"), $("#" + type + "_M1012 .sm-diagnoses-list"));
                    U.HideIfChecked($("#" + type + "_M1012InpatientFacilityProcedureCodeUnknown"), $("#" + type + "_M1012 .sm-diagnoses-list"));
                    U.HideIfChecked($("#" + type + "_M1016MedicalRegimenDiagnosisNotApplicable"), $("#" + type + "_M1016 .sm-diagnoses-list"));
                    U.NoneOfTheAbove($("#" + type + "_M1018ConditionsPriorToMedicalRegimenNone"), $("#" + type + "_M1018 .M1018"));
                    U.NoneOfTheAbove($("#" + type + "_M1018ConditionsPriorToMedicalRegimenNA"), $("#" + type + "_M1018 .M1018"));
                    U.NoneOfTheAbove($("#" + type + "_M1018ConditionsPriorToMedicalRegimenUK"), $("#" + type + "_M1018 .M1018"));
                    U.NoneOfTheAbove($("#" + type + "_M1030HomeTherapiesNone"), $("#" + type + "_M1030 .M1030"));
                    $("#" + type + "_OasisDiagnoses").DiagnosesList()
                },
                Prognosis: function(type, typeNum, e) {
                    U.ShowIfChecked($("#" + type + "_485AdvancedDirectivesIntentOther"), $("#" + type + "_485AdvancedDirectivesIntentOtherMore"));
                    U.ShowIfChecked($("#" + type + "_485FunctionLimitationsB"), $("#" + type + "_485FunctionLimitationsBMore"));
                },
                Respiratory: function(type, typeNum, e) {
                    U.ShowIfChecked($("#" + type + "_GenericRespiratoryCondition2"), $("#" + type + "_GenericRespiratoryCondition2More"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratoryCondition3"), $("#" + type + "_GenericRespiratoryCondition3More"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds1"), $("#" + type + "_GenericLungSoundsCTAText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds2"), $("#" + type + "_GenericLungSoundsRalesText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds3"), $("#" + type + "_GenericLungSoundsRhonchiText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds4"), $("#" + type + "_GenericLungSoundsWheezesText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds5"), $("#" + type + "_GenericLungSoundsCracklesText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds6"), $("#" + type + "_GenericLungSoundsDiminishedText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds7"), $("#" + type + "_GenericLungSoundsAbsentText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratorySounds8"), $("#" + type + "_GenericLungSoundsStridorText"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratoryCondition4"), $("#" + type + "_GenericRespiratoryCondition4More"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratoryCondition5"), $("#" + type + "_GenericRespiratoryCondition5More"));
                    U.ShowIfChecked($("#" + type + "_GenericRespiratoryCondition6"), $("#" + type + "_GenericNebulizerText"));
                    U.NoneOfTheAbove($("#" + type + "_M1410HomeRespiratoryTreatmentsNone"), $("#" + type + "_M1410 .M1410"));
                },
                RiskAssessment: function(type, typeNum, e) {
                    U.NoneOfTheAbove($("#" + type + "_M1032HospitalizationRiskNone"), $("#" + type + "_M1032 .M1032"));
                    U.NoneOfTheAbove($("#" + type + "_M1036RiskFactorsNone"), $("#" + type + "_M1036 .M1036"));
                    U.NoneOfTheAbove($("#" + type + "_M1036RiskFactorsUnknown"), $("#" + type + "_M1036 .M1036"));
                },
                Sensory: function(type, typeNum, e) {
                    U.ShowIfChecked($("#" + type + "_GenericEyes13"), $("#" + type + "_GenericEyes13More"));
                    U.ShowIfChecked($("#" + type + "_GenericEars2"), $("#" + type + "_GenericEars2More"));
                    U.ShowIfChecked($("#" + type + "_GenericEars6"), $("#" + type + "_GenericEars6More"));
                    U.ShowIfChecked($("#" + type + "_GenericEars7"), $("#" + type + "_GenericEars7More"));
                    U.ShowIfChecked($("#" + type + "_GenericNose4"), $("#" + type + "_GenericNose4More"));
                    U.ShowIfChecked($("#" + type + "_GenericNose5"), $("#" + type + "_GenericNose5More"));
                    U.ShowIfChecked($("#" + type + "_GenericMouth5"), $("#" + type + "_GenericMouth5More"));
                    U.ShowIfChecked($("#" + type + "_GenericThroat4"), $("#" + type + "_GenericThroat4More"));
                },
                SupportiveAssistance: function(type, typeNum, e) {
                    U.ShowIfChecked($("#" + type + "_GenericHazardsIdentified9"), $("#" + type + "_GenericHazardsIdentified9More"));
                    U.ShowIfChecked($("#" + type + "_GenericUseOfInterpreter4"), $("#" + type + "_GenericUseOfInterpreter4More"));
                    U.ShowIfRadioEquals(type + "_GenericCulturalPractices", "1", $("#" + type + "_GenericCulturalPracticesYesMore"));
                    U.ShowIfRadioEquals(type + "_GenericIsHomeBound", "Yes", $(".HomeBound", e));
                    U.ShowIfChecked($("#" + type + "_GenericHomeBoundReason7"), $("#" + type + "_GenericHomeBoundReason7More"));
                    Lookup.Language("#StartOfCare_GenericPrimaryLanguage");
                },
                TherapyNeed: function(type, typeNum, e) {
                    U.HideIfChecked($("#" + type + "_M2200TherapyNeedNA"), $("#" + type + "_M2200Right"));
                },
                TransferDischargeDeath: function(type, typeNum, e) {
                    if (typeNum == 9) $("input[name=" + type + "_M2410TypeOfInpatientFacility]").click(function() {
                        $("input[name=" + type + "_M2410TypeOfInpatientFacility]").eq(5).prop("checked", true)
                    });
                    else {
                        U.HideIfRadioEquals(type + "_M2410TypeOfInpatientFacility", "02|03|04", $("#" + type + "_M2430"));
                        U.HideIfRadioEquals(type + "_M2410TypeOfInpatientFacility", "01|02|04", $("#" + type + "_M2440"));
                    }
                }
            }
        }
    }
});