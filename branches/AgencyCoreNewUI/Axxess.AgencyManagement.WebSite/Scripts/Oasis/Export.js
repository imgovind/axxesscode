﻿$.extend(OASIS, {
    Export: {
        AcoreId: "OASISExport",
        BuildPaymentSourceArray: function() {
            var e = $("#window_" + OASIS.Export.AcoreId + " .main"), paymentSources = [];
            $("[name=PaymentSources]:checked", e).each(function() { paymentSources.push($(this).val()) });
            return paymentSources;
        },
        BuildOasisSelectedArray: function() {
            var e = $("#window_" + OASIS.Export.AcoreId + " .main"), oasisSelected = [];
            $("[name=OasisSelected]:checked", e).each(function() { oasisSelected.push($(this).val()) });
            return oasisSelected;
        },
        Mark: function(isExported) {
            var e = $("#window_" + OASIS.Export.AcoreId + " .main"), Quantity = $("[name=OasisSelected]:checked", e).length, Message = isExported ? "Are you sure that " + (Quantity > 1 ? "these" : "this") + " OASIS file has been accepted by CMS and you would like to mark them as exported?" : "Are you sure that you want mark this OASIS completed without export?";
            if (Quantity > 0) Acore.Confirm({
                Message: Message,
                Yes: function() {
                    U.PostUrl("Oasis/Mark", { oasisSelected: OASIS.Export.BuildOasisSelectedArray(), statusType: isExported ? "Exported" : "CompletedNotExported" }, function(result) {
                        U.GrowlResult(result);
                        if (result.isSuccessful) U.RebindAllGrids();
                    })
                }
            });
            else U.Growl("Select at least one OASIS assessment to mark as " + (isExported ? "Exported" : "Completed (Not Exported)"), "error");
        },
        EditCorrectionNumber: function(id, patientId, episodeId, type, correctionNumber) {
            Acore.Modal({
                Name: "OASIS Correction Number",
                Url: "Oasis/Correction",
                Input: { id: id, patientId: patientId, episodeId: episodeId, type: type, correctionNumber: correctionNumber },
                OnLoad: U.InitShared,
                Width: 650
            })
        },
        Export: function(branchId, paymentSources) { U.GetAttachment("Oasis/Export/Export", { branchId: branchId, paymentSources: paymentSources }) },
        Generate: function() {
            var e = $("#window_" + OASIS.Export.AcoreId + " .main");
            if ($("[name=OasisSelected]:checked", e).length) U.GetAttachment("Oasis/Generate", { branchId: $("[name=BranchId]", e).val(), oasisSelected: OASIS.Export.BuildOasisSelectedArray() });
            else U.Growl("Select at least one OASIS assessment to generate.", "error");
        },
        Init: function(r, t, x, e) {
            U.SelectAll($(".select-all", e), $("[name=OasisSelected]", e));
            $(".grid-refresh", e).click(function() {
                OASIS.Export.Refresh();
                return false;
            });
            $(".export", e).click(function() {
                OASIS.Export.Export($("[name=BranchId]", e).val(), OASIS.Export.BuildPaymentSourceArray());
                return false;
            });
            $(".filter select", e).change(function() { OASIS.Export.Refresh() });
            $(".multiselect", e).MultiSelect({
                DefaultText: "Select Payment Source",
                OnSelect: function() { OASIS.Export.Refresh() }
            });
            $(".generate-oasis", e).click(function() {
                OASIS.Export.Generate();
                return false;
            });
            $(".mark-exported", e).click(function() {
                OASIS.Export.Mark(true);
                return false;
            });
            $(".mark-completed", e).click(function() {
                OASIS.Export.Mark(false);
                return false;
            });
        },
        Refresh: function() {
            var e = $("#window_" + OASIS.Export.AcoreId + " .main");
            U.RebindGrid($(".t-grid", e), { branchId: $("[name=BranchId]", e).val(), paymentSources: OASIS.Export.BuildPaymentSourceArray() });
        }
    },
    Exported: {
        AcoreId: "ExportedOASIS",
        Export: function(branchId, startDate, endDate) { U.GetAttachment("Oasis/Exported/Export", { branchId: branchId, status: 1, startDate: startDate, endDate: endDate }) },
        GenerateCancel: function(id, type) {
            U.PostUrl("Oasis/Inactivate", { id: id, type: type }, function(result) {
                if (result.isValid) U.GetAttachment("Oasis/GenerateCancel", { id: id, type: type });
                else U.Growl("There was a problem inactivating this OASIS assessment.  Please try again", "error");
            })
        },
        Init: function(r, t, x, e) {
            $(".grid-refresh", e).click(function() {
                OASIS.Exported.Refresh();
                return false;
            });
            $(".export", e).click(function() {
                OASIS.Exported.Export($("[name=BranchId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                return false;
            });
            $(".filter select,.filter .date-picker", e).change(function() { OASIS.Exported.Refresh() });
        },
        Refresh: function() {
            var e = $("#window_" + OASIS.Exported.AcoreId + " .main");
            U.RebindGrid($(".t-grid", e), { branchId: $("[name=BranchId]", e).val(), status: 1, startDate: $("[name=StartDate]", e).val(), endDate: $("[name=EndDate]", e).val() });
        },
        Reopen: function(id, patientId, episodeId, assessmentType) {
            Acore.Modal({
                Name: "Reopen OASIS",
                Url: "Oasis/Exported/Reopen",
                OnLoad: U.InitShared,
                Input: { id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType },
                Width: 650
            })
        }
    }
});