var OASIS = {};
var Oasis = {
    DeleteAsset: function(Control, AssessmentType, Name, AssetId) {
        U.Delete("Asset", "Oasis/DeleteWoundCareAsset", {
            episodeId: $("#" + AssessmentType + "_EpisodeId").val(),
            patientId: $("#" + AssessmentType + "_PatientGuid").val(),
            eventId: $("#" + AssessmentType + "_Id").val(),
            assessmentType: AssessmentType,
            name: Name,
            assetId: AssetId
        }, function() {
            $(Control).closest("td").empty().append(
                $("<input/>", {
                    type: "file",
                    name: AssessmentType + "_" + Name,
                    value: "Upload",
                    size: "1",
                    "class": "float-left uploadWidth"
                })
            )
        })
    },
    DeleteRow: function(Control) {
        if ($(Control).closest("tbody").find("tr").length == 1) {
            $(".schedule-tables.purgable").each(function() {
                $(this).find("tbody").empty()
            });
            Schedule.ShowScheduler();
        } else $(Control).closest("tr").remove();
        Schedule.positionBottom();
    },
    GenerateSubmit: function(Control) {
        if ($("input[name=OasisSelected]:checked", $(Control).closest("form")).length > 0 && U.IsGuid($("#OasisExport_BranchCode").val())) $(Control).closest('form').submit();
        else U.Growl("Select at least one OASIS assessment to generate.", "error");
    },
    Goals: function(Fieldset) {
        Fieldset.find("input[type=checkbox]").each(function() {
            if ($(this).parent().find("span.radio").length) {
                if (!$(this).prop("checked")) $(this).parent().find("span.radio :input").prop("disabled", true);
                $(this).change(function() {
                    if ($(this).prop("checked")) $(this).parent().find("span.radio :input").prop("disabled", false);
                    else $(this).parent().find("span.radio :input").prop("disabled", true);
                })
            }
        })
    },
    GotoQuestion: function(Question, AssessmentType) {
        var tab = 1, q = parseInt(Question.replace(/^[M0]*/, ""));
        if (AssessmentType == "StartOfCare" || AssessmentType == "ResumptionOfCare") {
            if (q >= 1000 && q <= 1030) tab = 2;
            if (q >= 1032 && q <= 1036) tab = 3;
            if (q == 1100) tab = 5;
            if (q >= 1200 && q <= 1230) tab = 6;
            if (q >= 1240 && q <= 1242) tab = 7;
            if (q >= 1300 && q <= 1350) tab = 8;
            if (q >= 1400 && q <= 1410) tab = 9;
            if (q >= 1600 && q <= 1630) tab = 12;
            if (q >= 1700 && q <= 1750) tab = 14;
            if (q >= 1800 && q <= 1910) tab = 15;
            if (q >= 2000 && q <= 2040) tab = 17;
            if (q >= 2100 && q <= 2110) tab = 18;
            if (q >= 2200 && q <= 2250) tab = 19;
        } else if (AssessmentType == "Recertification") {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 5;
            if (q == 1242) tab = 6;
            if (q >= 1306 && q <= 1350) tab = 7;
            if (q >= 1400 && q <= 1410) tab = 8;
            if (q >= 1610 && q <= 1630) tab = 11;
            if (q >= 1810 && q <= 1910) tab = 14;
            if (q == 2030) tab = 16;
            if (q >= 2200) tab = 17;
        } else if (AssessmentType == "FollowUp") {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 3;
            if (q == 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q == 1400) tab = 6;
            if (q >= 1610 && q <= 1630) tab = 7;
            if (q >= 1810 && q <= 1860) tab = 8;
            if (q == 2030) tab = 9;
            if (q == 2200) tab = 10;
        } else if (AssessmentType == "TransferInPatientNotDischarged" || AssessmentType == "TransferInPatientDischarged") {
            if (q >= 1040 && q <= 1055) tab = 2;
            if (q >= 1500 && q <= 1510) tab = 3;
            if (q >= 2004 && q <= 2015) tab = 4;
            if (q >= 2300 && q <= 2310) tab = 5;
            if (q == 903 || q == 906 || (q >= 2400 && q <= 2440)) tab = 6;
        } else if (AssessmentType == "DischargeFromAgencyDeath") {
            if (q == 903 || q == 906) tab = 2;
        } else if (AssessmentType == "DischargeFromAgency") {
            if (q >= 1040 && q <= 1050) tab = 2;
            if (q == 1230) tab = 3;
            if (q >= 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q >= 1400 && q <= 1410) tab = 6;
            if (q >= 1500 && q <= 1510) tab = 7;
            if (q >= 1600 && q <= 1620) tab = 8;
            if (q >= 1700 && q <= 1750) tab = 9;
            if (q >= 1800 && q <= 1890) tab = 10;
            if (q >= 2004 && q <= 2030) tab = 11;
            if (q >= 2100 && q <= 2110) tab = 12;
            if (q >= 2300 && q <= 2310) tab = 13;
            if (q == 903 || q == 906 || (q >= 2400 && q <= 2420)) tab = 14;
        }
        if (Acore.Windows[AssessmentType].IsOpen) {
            $("#window_" + AssessmentType).WinFocus();
            $('.vertical-tab-list li:nth-child(' + tab + ') a', "#window_" + AssessmentType).click();
            setTimeout(function() {
                if ($("#" + AssessmentType + "_" + Question).length)
                    $(".general:nth-child(" + (tab + 1) + ")", "#window_" + AssessmentType + "_content").scrollTop(
                        $("#" + AssessmentType + "_" + Question).closest("fieldset").position().top
                    );
            }, 1000);
        }
    },
    HideBlankMasterCalendar: function(AssessmentType) {
        $("#" + AssessmentType + "_BlankMasterCalendar").empty();
        $("#" + AssessmentType + "_Show").show();
        $("#" + AssessmentType + "_Hide").hide();
    },
    Interventions: function(Fieldset) {
        Oasis.Goals(Fieldset)
    },
    MarkAsExported: function(Control, Type) {
        var Fields = $(":input", $(Control)).serializeArray();
        if (Fields != undefined && Fields.length > 0) {
            Fields[Fields.length] = { name: "StatusType", value: Type };
            U.PostUrl("Oasis/MarkExported", Fields, function(Result) {
                if (Result.isSuccessful) {
                    U.Growl(Result.errorMessage, "success");
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    Agency.RebindCaseManagement();
                } else U.Growl(Result.errorMessage, "error");
            })
        }
    },
    MarkAsSubmited: function(Control, Type) {
        var Flag = Type === "Exported";
        if ($("input[name=OasisSelected]:checked", $(Control)).length > 0) {
            Acore.Modal({
                Name: "OASIS Mark as " + (Flag ? "Exported" : "Completed (Not Exported)"),
                Content: $("<div/>", { "id": "oasisMarkAsExportDialog", "class": "wrapper main" }).append(
                    $("<fieldset/>").append(function() {
                        if (Flag) return $("<div/>").html(U.Warning("Verify that your OASIS Submission was Accepted", "Please verify that your OASIS submission was accepted by CMS (Center for Medicare & Medicaid Services) before you complete this step. If this OASIS file was not accepted, do not mark as exported."));
                        else return $("<div/>")
                    }).append(
                        $("<div/>").addClass("strong align-center").html(Flag ? "Are you sure that this OASIS file has been accepted by CMS?" : "Are you sure that you want mark this OASIS completed without export?"))).append(
                    $("<div/>").Buttons([
                        { Text: "Yes, Mark as " + (Flag ? "Exported" : "Completed (Not Exported)"), Click: function() { Oasis.MarkAsExported(Control, Type); $(this).closest(".window").Close() } },
                        { Text: "No, Cancel", Click: function() { $(this).closest(".window").Close() } }
                    ])),
                Width: "700px",
                Height: "150px",
                WindowFrame: false
            })
        } else U.Growl("Select at least one OASIS assessment to mark as " + (Flag ? "Exported." : "Completed ( Not Exported )."), "error");
    },
    NonOasisSignature: function(Id, PatientId, EpisodeId, AssessmentType) {
        Acore.Modal({
            Name: "Non-OASIS Signature",
            Url: "Oasis/NonOasisSignature",
            Input: {
                Id: Id,
                PatientId: PatientId,
                EpisodeId: EpisodeId,
                AssessmentType: AssessmentType
            },
            OnLoad: function() { Oasis.NonOasisSignatureSubmit($("#NonOasisSignatureForm")) },
            Width: "800px",
            Height: "200px",
            WindowFrame: false
        })
    },
    NonOasisSignatureSubmit: function(form) {
        form.validate({
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataTpe: "json",
                    success: function(Result) {
                        if (Result.isSuccessful) {
                            var window = $("input[name=OasisValidationType]").val().toLowerCase();
                            U.Growl(Result.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindCaseManagement();
                            UserInterface.CloseModal();
                            UserInterface.CloseWindow(window);
                        } else U.Growl(Result.errorMessage, "error");
                    }
                });
                return false;
            }
        })
    },
    RebindExported: function(pageName) {
        U.RebindGridWithExportLink(pageName, $('#' + pageName + '_Grid').data('tGrid'), { BranchId: $("#" + pageName + "_BranchCode").val(), Status: $("#" + pageName + "_Status").val(), StartDate: $("#" + pageName + "_StartDate").val(), EndDate: $("#" + pageName + "_EndDate").val() });
    },
    RebindToExport: function() {
        if ($("#OASISToExport_Grid").data("tGrid") != null) $("#OASISToExport_Grid").data("tGrid").rebind()
    },
    SignatureSubmit: function(Form) {
        Form.validate({
            submitHandler: function(Form) {
                $(Form).ajaxSubmit({
                    dataType: "json",
                    success: function(Result) {
                        if (Result.isSuccessful) {
                            window.parent.UserInterface.CloseWindow($("#oasisPageName").val());
                            window.parent.U.Growl(Result.errorMessage, "success");
                            try {
                                window.parent.Patient.Rebind();
                                window.parent.Schedule.Rebind();
                                window.parent.Oasis.RebindToExport();
                                window.parent.Oasis.RebindExported();
                                window.parent.Agency.RebindCaseManagement();
                            } catch (e) { }
                            window.parent.UserInterface.CloseModal();
                        } else window.parent.U.Growl(Result.errorMessage, "error");
                    }
                });
                return false;
            }
        })
    },
    StatusAction: function(Id, PatientId, EpisodeId, ActionType, PageName) {
        var Reason = "";
        if (ActionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide()
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown("slow");
            } else {
                Reason = $("#print-return-reason textarea").val();
                U.PostUrl("Oasis/Submit", {
                    Id: Id,
                    patientId: PatientId,
                    episodeId: EpisodeId,
                    actionType: ActionType,
                    reason: Reason
                }, function(Result) {
                    if (Result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        Oasis.RebindToExport();
                        Oasis.RebindExported();
                        UserInterface.CloseWindow(pageName);
                        U.Growl(Result.errorMessage, "success");
                    } else U.Growl(Result.errorMessage, "error");
                })
            }
        } else {
            U.PostUrl("Oasis/Submit", {
                Id: Id,
                patientId: PatientId,
                episodeId: EpisodeId,
                actionType: ActionType,
                reason: Reason
            }, function(Result) {
                if (Result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    UserInterface.CloseWindow(pageName);
                    U.Growl(Result.errorMessage, "success");
                } else U.Growl(Result.errorMessage, "error");
            })
        }
    },
    ToolTip: function(QuestionNumber) {
        U.PostUrl("Oasis/Guide", { mooCode: QuestionNumber }, function(Result) {
            $(".oasis-tip-box").remove()
            if (Result.ItemIntent == null) return false;
            $("#desktop").append(
                $("<div/>").addClass("oasis-tip-box").append(
                    $("<div/>", { "class": "closer", text: "X" }).click(function() {
                        $(this).parent().remove()
                    })).append(
                    $("<h3/>").text("Item Intent")).append(
                    $("<div/>", { "class": "content intent", text: Result.ItemIntent })).append(
                    $("<h3/>").text("Response")).append(
                    $("<div/>", { "class": "content response", text: Result.Response })).append(
                    $("<h3/>").text("Data Sources")).append(
                    $("<div/>", { "class": "content sources", text: Result.DataSources })
                )
            )
        })
    },
    Validate: function(Id, PatientId, EpisodeId, AssessmentType) {
        Acore.OpenPrintView({
            Url: "Validate/" + Id + "/" + PatientId + "/" + EpisodeId + "/" + AssessmentType,
            Buttons: [
                {
                    Text: "OASIS Scrubber",
                    Click: function() {
                        U.GetAttachment("Oasis/AuditPdf", {
                            id: Id,
                            patientId: PatientId,
                            episodeId: EpisodeId,
                            assessmentType: AssessmentType
                        })
                    }
                }
            ]
        });
        $("#print-controls a:last").text("Return to OASIS");
        $("#print-controls li:first").addClass("red").tooltip({
            track: true,
            showURL: false,
            top: 25,
            left: 25,
            bodyHandler: function() {
                return $("<div/>").addClass("strong").text("Powered by").after($("<img/>", { src: "/Images/hhg-logo.png" }));
            }
        })
    },
    RebindPastDueRecet: function(pageName) { U.RebindGridWithExportLink(pageName, $('#' + pageName + '_Grid').data('tGrid'), { BranchId: $("#" + pageName + "_BranchCode").val(), InsuranceId: $("#" + pageName + "_InsuranceId").val(), StartDate: $("#" + pageName + "_StartDate").val() }); },
    RebindUpcomingRecet: function(pageName) { U.RebindGridWithExportLink(pageName, $('#' + pageName + '_Grid').data('tGrid'), { BranchId: $("#" + pageName + "_BranchCode").val(), InsuranceId: $("#" + pageName + "_InsuranceId").val() }); }
}