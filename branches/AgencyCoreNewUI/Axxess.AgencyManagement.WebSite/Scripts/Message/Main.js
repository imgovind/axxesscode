﻿var Message = {
    Delete: function(id, type, bypass, callback) {
        if (bypass) U.GridAjaxCommand("Message/Delete", { Id: id, Type: type }, $("#list-messages"), callback);
        else Acore.Confirm({
            Message: "Are you sure you want to delete this message?",
            Yes: function() { U.GridAjaxCommand("Message/Delete", { Id: id, Type: type }, $("#list-messages")) }
        })
    },
    GetMessagesWidget: function(e) {
        U.PostUrl("Message/GridWidget", {}, function(data) {
            if (data != undefined && data.Data != undefined && data.Data.length > 0) {
                var Id = "";
                for (var i = 0; i < data.Data.length; i++) e.append(
                    $("<li/>", { "class": (data.Data[i].MarkAsRead ? "" : "strong") }).append(
                        $("<input/>", { type: "checkbox", messagetype: data.Data[i].Type, guid: data.Data[i].Id })).append(
                        $("<a/>", { "html": data.Data[i].FromName + " &#8211; " + data.Data[i].Subject }).click(function() {
                            $(this).parent().removeClass("strong");
                            Message.Inbox($(this).siblings("input").attr("guid"))
                        })
                    ).mouseover(function() {
                        $(this).addClass("hover");
                        $(this).closest(".window").Status("View this Message in the Message Center Window");
                    }).mouseout(function() {
                        $(this).removeClass("hover");
                        $(this).closest(".window").Status("");
                    }))
            } else e.html($("<h1/>", { "class": "ac blue", "text": "No Messages Found" }));
        })
    },
    New: function() {
        Acore.Open("NewMessage");
    },
    SetSlider: function(e) {
        e.layout({ west: { paneSelector: ".ui-layout-west", size: 300, minSize: 160, maxSize: 400} })
    }
}