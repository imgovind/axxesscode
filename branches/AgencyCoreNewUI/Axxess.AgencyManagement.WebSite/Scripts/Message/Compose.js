﻿$.extend(Message, {
    CKELoaded: false,
    TokenList: null,
    AddRemoveRecipient: function(input) {
        if ($("#" + input).prop("checked")) $.data($("#" + input).get(0), "tokenbox", { "token": Message.TokenList.insertToken($("#" + input).attr("value"), $("#" + input).attr("title")) });
        else Message.TokenList.removeToken($.data($("#" + input).get(0), "tokenbox").token);
        $(".recipient-list-token-input").removeClass("error");
        Message.PositionBottom();
    },
    Compose: function() {
        Message.New();
    },
    InitCKE: function() {
        var e = "NewMessage_Body", args = {
            skin: "office2003", resize_enabled: false, height: "100%", removePlugins: "elementspath", toolbarCanCollapse: false,
            toolbar: [
                ["Bold", "Italic", "Underline", "-",
                    "NumberedList", "BulletedList", "Outdent", "Indent", "-",
                    "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-",
                    "Link", "Unlink", "-",
                    "Font", "FontSize", "TextColor", "-",
                    "SpellChecker", "Scayt"
                ]
            ]
        };
        if (!Message.CKELoaded) $.getScript("Scripts/Plugins/ckeditor/ckeditor.js", function() {
            Message.CKELoaded = true;
            CKEDITOR.replace(e, args);
        });
        else CKEDITOR.replace(e, args);
    },
    InitExistingMessage: function(message, e) {
        var type = "NewMessage", prefix = "#" + type + "_";
        Message.Forward = Message.Reply = Message.ReplyAll = false;
        Message.DefaultMessage = "";
        $(prefix + "Subject").val(message.Subject);
        $(prefix + "PatientId").val(message.PatientId);
        $(prefix + "Body").val(message.ReplyForwardBody);
        Message.InitCKE();
    },
    InitForward: function(message, e) {
        $(".compose-header span", e).text("Forward Message");
        if (message.Subject.substring(0, 3) != "FW:") message.Subject = "FW: " + message.Subject;
        Message.InitExistingMessage(message, e);
    },
    InitCompose: function(r, t, x, e) {
        var type = "NewMessage", prefix = "#" + type + "_";
        Message.SetSlider($(".layout", e));
        $("input[name=Recipients]", e).change(function() { Message.AddRemoveRecipient($(this).attr("id")) });
        U.SelectAll($(prefix + "SelectAllRecipients"), $("[name=Recipients]", e));
        Message.TokenList = $.fn.tokenInput(prefix + "Recipents", "Message/Recipients", {
            classes: { tokenList: "recipient-list-input", token: "recipient-list-token", selectedToken: "recipient-list-token-selected", dropdown: "recipient-list-select", inputToken: "recipient-list-token-input" }
        });
        if (Message.Forward || Message.Reply || Message.ReplyAll) U.PostUrl("Message/Get", { id: Message.DefaultMessage }, function(data) {
            if (Message.Forward) Message.InitForward(data, e);
            if (Message.Reply) Message.InitReply(data, e);
            if (Message.ReplyAll) Message.InitReplyAll(data, e);
        });
        else Message.InitCKE();
        $("form", e).Validate({
            BeforeSubmit: function() {
                if (!$(".recipient-list-token", e).length) {
                    $(".recipient-list-token-input").addClass("error");
                    U.Growl("Please Select a Recipient", "error");
                    return false;
                }
                if (CKEDITOR.instances["NewMessage_Body"].getData().length) $("[name=Body]", e).val(CKEDITOR.instances["NewMessage_Body"].getData());
                else {
                    U.Growl("Please Type in a Message", "error");
                    return false;
                }
            },
            Success: function() {
                $(this).closest(".window").Close();
            }
        });
    },
    InitReply: function(message, e) {
        $(".compose-header span", e).text("Reply Message");
        if (message.Subject.substring(0, 3) != "RE:") message.Subject = "RE: " + message.Subject;
        if ($("[name=Recipients][value=" + message.FromId + "]", e).length) $("[name=Recipients][value=" + message.FromId + "]", e).prop("checked", true).change();
        Message.InitExistingMessage(message, e);
    },
    InitReplyAll: function(message, e) {
        $.each(message.RecipientNames.split(/;\s?/), function(index, data) {
            $(".recipient:not(.self)").each(function() {
                if ($("label", this).text() == data) $("input", this).prop("checked", true).change();
            })
        });
        Message.InitReply(message, e);
    },
    PositionBottom: function() {
        $("#NewMessage_BodyWrapper").css("top", $(".compose.message-header-container").height() + 50)
    }
});