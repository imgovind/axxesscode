﻿$.extend(Message, {
    DefaultMessage: "",
    Forward: false,
    Reply: false,
    ReplyAll: false,
    ChangeFolder: function(e) {
        var Index = e.prevAll().length;
        if (!$(".folder-selection li").eq(Index).hasClass("selected")) U.RebindGrid($("#list-messages"), { inboxType: Index ? "sent" : "inbox" });
        $(".folder-selection li").removeClass("selected").eq(Index).addClass("selected");
        $(".folder-selector").hide()
    },
    Inbox: function(id) {
        if (id) Message.DefaultMessage = id;
        Acore.Open("Inbox");
    },
    InitInbox: function(r, t, x, e) {
        Message.SetSlider($(".layout", e));
        $(".folder-selector").hide().find("li").click(function() {
            Message.ChangeFolder($(this));
            return false;
        })
        $(".folder-selection").click(function() {
            $(".folder-selector").show();
            return false;
        });
        $(".new-message", e).click(function() {
            Message.New();
            return false;
        });
    },
    LoadMessage: function(e) {
        $("#message-view").Load("Message/View", {
            id: $("td > div", e.row).attr("id"),
            type: $("td > div", e.row).attr("type")
        }, function() {
            $("td > div", e.row).removeClass("read-false").addClass("read-true");
            $(".message-body-container", "#message-content-panel").css("top", $(".message-header-container", "#message-content-panel").height() + 52);
            $(".winmenu a", "#message-content").click(function() {
                if ($(this).hasClass("delete")) Message.Delete($(".t-state-selected > td > div", "#list-messages").attr("id"), $(".t-state-selected > td > div", "#list-messages").attr("type"));
                else {
                    Message.DefaultMessage = $(".t-state-selected > td > div", "#list-messages").attr("id");
                    if ($(this).hasClass("reply")) Message.Reply = true;
                    else if ($(this).hasClass("reply-all")) Message.ReplyAll = true;
                    else if ($(this).hasClass("forward")) Message.Forward = true;
                    Message.New();
                }
            })
        })
    },
    OnMessageListReady: function(e) {
        if ($("#" + Message.DefaultMessage).length) $("#" + Message.DefaultMessage).closest("tr").click();
        else if ($("#list-messages .t-grid-content tr").length) $("#list-messages .t-grid-content tr").eq(0).click();
        else $("#message-view").removeClass("loading").html(
            U.MessageInfo("No Messages Found", "There are currently no messages in this mailbox."));
        Message.DefaultMessage = "";
    }
});