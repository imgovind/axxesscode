﻿var U = {
    GuidEmpty: "00000000-0000-0000-0000-000000000000",
    // Display current date formatted
    DisplayDate: function() {
        var CurrentDate = new Date(),
                Days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                DateSuffix = "th";
        if (CurrentDate.getDate() % 10 == 1 && CurrentDate.getDate() != 11) DateSuffix = "st";
        else if (CurrentDate.getDate() % 10 == 2 && CurrentDate.getDate() != 12) DateSuffix = "nd";
        else if (CurrentDate.getDate() % 10 == 3 && CurrentDate.getDate() != 13) DateSuffix = "rd";
        return Days[CurrentDate.getDay()] + ", " + Months[CurrentDate.getMonth()] + " " + CurrentDate.getDate() + DateSuffix + ", " + CurrentDate.getFullYear()
    },
    // Takes a number and returns it as a formatted string for money amount display
    FormatMoney: function(Num) {
        if (isNaN(Number(Num))) return "";
        var Result = String(Math.round(Math.abs(Num) * 100));
        Result = Result.substr(0, Result.length - 2) + "." + Result.substr(Result.length - 2);
        for (var i = 0; i * 4 + 6 < Result.length; i++) Result = Result.substr(0, Result.length - (i * 4 + 6)) + "," + Result.substr(Result.length - (i * 4 + 6));
        Result = "$" + Result;
        if (Num < 0) Result = "-" + Result;
        return Result;
    },
    // Ajax request for download
    GetAttachment: function(Url, Data) {
        $("body").append(
            $("<form/>", { "id": "AjaxAttachmentForm", "method": "post", "action": Url })
        );
        $.each(Data, function(Key, Val) {
            $("#AjaxAttachmentForm").append(
                $("<input/>", { "type": "hidden", "name": Key, "value": Val })
            )
        });
        $("#AjaxAttachmentForm").submit().remove();
    },
    // Get Type/Prefix for form
    GetType: function(e) {
        if ($("form", e).length && $("form", e).attr("id").length) return $("form", e).eq(0).attr("id").replace(/_Form$/, "");
        else return "";
    },
    // Run AJAX command and rebind specific grid
    GridAjaxCommand: function(Url, Input, Grid, Callback) {
        U.PostUrl(Url, Input, function(Result) {
            if (typeof Callback == "function") {
                try {
                    Callback();
                } catch (e) {
                    $.error(e);
                }
            } else if (Result.isSuccessful) {
                U.RebindGrids(Grid);
                U.Growl(Result.errorMessage, "success");
            } else U.Growl(Result.errorMessage, "error")
        })
    },
    // Shared initialization file for all forms
    InitShared: function(r, t, x, e) {
        $("form", e).Validate();
    },
    // Check if string is a valid date per our standard format
    IsDate: function(Date, Strict) {
        if (Strict) var pattern = /^(0[1-9]|1[012])\/(0[1-9]|[12]\d|3[01])\/(19|20)\d\d$/;
        else var pattern = /^(0?[1-9]|1[012])\/(0?[1-9]|[12]\d|3[01])\/(19|20)?\d\d$/;
        if (pattern.test(Date)) {
            Date = Date.split("/");
            if (/^(0?[469]|11)$/.test(Date[0]) && parseInt(Date[1]) > 30) return false; // More than 30 days in April, June, September, or November
            if (/^(0?2)$/.test(Date[0])) {
                if (parseInt(Date[2]) % 4 && parseInt(Date[1]) > 28) return false; // More than 28 days in February on non-Leap Year
                if (parseInt(Date[2]) % 4 == 0 && parseInt(Date[1]) > 29) return false; // More than 29 days in February on a Leap Year
            }
            return true;
        }
        return false;
    },
    IsDateStrict: function(Date) { return U.IsDate(Date, true) },
    // Check if string is valid email address
    IsEmail: function(Email) {
        return /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(Email)
    },
    // Check if string is valid Guid
    IsGuid: function(Guid) {
        return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i.test(Guid) && !(/^[0-]*$/.test(Guid))
    },
    // Check if string is valid time per our standard format
    IsTime: function(Time) {
        return /^(0?[1-9]|1[012]):[0-5][0-9] [AP]M$/.test(Time)
    },
    // Shade out entire window to display loading modal (when app is first logging in)
    Loading: function(Show) {
        if (Show) $("body").append(
            $("<div/>", { "id": "shade" })).append(
            $("<div/>", { "class": "loading loading-modal" }));
        else $("#shade,.loading-modal").remove()
    },
    // Used to rename all id within an element to use a new prefix
    NewPrefix: function(prefix, element) {
        $("label[for]", element).each(function() { $(this).attr("for", $(this).attr("for").replace(/^[^_]*/, prefix)) });
        $("[id]", element).each(function() { $(this).attr("id", $(this).attr("id").replace(/^[^_]*/, prefix)) });
    },
    // Make AJAX request
    PostUrl: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: "POST",
            dataType: "json",
            success: function(data) {
                if (typeof onSuccess == "function") try {
                    onSuccess(data);
                } catch (e) {
                    $.error(e)
                }
            },
            error: function(data) {
                if (typeof (onFailure) == "function") try {
                    onFailure(data);
                } catch (e) {
                    $.error(e)
                }
            }
        });
    },
    // Convert Date for to pass in GET
    URIDate: function(date) {
        if (U.IsDateStrict(date)) return date.substr(6, 4) + date.substr(0, 2) + date.substr(3, 2);
        else return "";
    },
    // Rebind specific grid
    RebindGrid: function(Grid, Input) {
        if (Grid.data("tGrid") != null) Grid.data("tGrid").rebind(Input)
    },
    // Rebind specific grid and update excel link arg
    RebindGridWithExportLink: function(pageName, Grid, Input) {
        if (Grid != null && Grid != undefined) {
            Grid.rebind(Input);
            U.ExportLinkRebind(pageName, Input);
        }
    },
    // Rebind multiple grids
    RebindGrids: function(Grid) {
        Grid.each(function() { U.RebindGrid($(this)) });
    },
    // Rebind all current grids in DOM
    RebindAllGrids: function() {
        if ($(".t-grid").not(".args").length) $(".t-grid").not(".args").each(function() { if ($(this).data("tGrid")) $(this).data("tGrid").rebind() });
        if ($(".grid-refresh").length) $(".grid-refresh").each(function() { $(this).click() });
        if (Acore.Windows[Patient.Charts.AcoreId].IsOpen) Patient.Charts.Refresh();
        if (Acore.Windows[Schedule.Center.AcoreId].IsOpen) Schedule.Center.Refresh();
    },
    // Switches between displaying an grid or a message mentioning no records found
    ToggleNoneFound: function(Element) {
        if ($("tbody tr", Element.target).length > 0) $(Element.target).show().next().hide();
        else $(Element.target).hide().next().show();
    },
    // Capitalize the first letter of every word in string
    ToTitleCase: function(Text) {
        var TextArray = Text.toLowerCase().split(" ");
        if (TextArray.length > 1) {
            Text = "";
            for (var i = 0; i < TextArray.length; i++) Text += TextArray[i].substr(0, 1).toUpperCase() + TextArray[i].substr(1) + " ";
        } else Text = Text.substr(0, 1).toUpperCase() + Text.toLowerCase().substr(1);
        return Text;
    },
    // Raise Validation Error
    ValidationError: function(control) {
        if (control.closest(".window-content").find(".error:first").closest("fieldset").length) var scroll = control.closest(".window-content").find(".error:first").closest("fieldset").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("td").length) var scroll = control.closest(".window-content").find(".error:first").closest("td").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("div").length) var scroll = control.closest(".window-content").find(".error:first").closest("div").position().top;
        else var scroll = 0;
        control.closest(".window-content").scrollTop(scroll);
        U.Growl("Error: There was a problem validating your form, please review your information and try again.", "error");
    },
    // Covert two digit year into four digit year
    Y2KConvert: function(ShortYear) {
        var CurrentYear = String(new Date().getFullYear()),
            CurrentPrefix = parseInt(CurrentYear.substr(0, 2)),
            CurrentShortYear = parseInt(CurrentYear.substr(2, 2));
        if (parseInt(ShortYear) > CurrentShortYear) return String(--CurrentPrefix) + String(ShortYear);
        else return String(CurrentPrefix) + String(ShortYear);
    },
    ParameterByName: function(url, name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if (results == null) return "";
        else return decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    InitTemplate: function(formobj, callback, message, before) {
        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        formobj.validate({
            submitHandler: function(form) {
                if (before != undefined && typeof (before) == 'function') before();
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.Growl(message, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RebindDataGridContent: function(pageName, url, jsonData, sortParams, callback) {
        var input = jsonData;
        if (sortParams != null && sortParams != undefined) {
            $.extend(input, { SortParams: sortParams });
        }
        $("#" + pageName + "GridContainer").empty().addClass("loading").Load(url, input, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("This report can't be loaded. Please close this window and try again.", 'error');
            }
            else if (textStatus == "success") {
                $(this).removeClass("loading");
                U.ExportLinkRebind(pageName, jsonData);
                if (callback != undefined && typeof (callback) == 'function') callback();
            }
        });
    },
    ExportLinkRebind: function(pageName, Input) {
        var $exportLink = $('#' + pageName + '_ExportLink');
        if ($exportLink != null && $exportLink != undefined) {
            var href = $exportLink.attr('href');
            if (href != null && href != undefined) {
                $.each(Input, function(key, value) {
                    var filter = new RegExp(key + "=([^&]*)");
                    href = href.replace(filter, key + '=' + value);
                });
                $exportLink.attr('href', href);
            }
        }
    }
};
$.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }
