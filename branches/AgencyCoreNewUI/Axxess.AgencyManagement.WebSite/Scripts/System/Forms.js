﻿$.extend(U, {
    EnableIfChecked: function(Checkbox, Field) { this.IfChecked(true, Checkbox, Field, true) },
    EnableIfRadioEquals: function(Group, Value, Field) { this.IfRadioEquals(true, Group, Value, Field, true) },
    HideIfChecked: function(Checkbox, Field) { this.IfChecked(false, Checkbox, Field) },
    HideIfRadioEquals: function(Group, Value, Field) { this.IfRadioEquals(false, Group, Value, Field) },
    HideIfSelectEquals: function(Select, Value, Field) { this.IfSelectEquals(false, Select, Value, Field) },
    IfChecked: function(Show, Checkbox, Field, Enable) {
        if (Enable == undefined) Enable = false;
        if (Checkbox.prop("checked") == Show) U.ToggleOn(Field, Enable);
        else U.ToggleOff(Field, Enable);
        Checkbox.change(function() {
            if (Checkbox.prop("checked") == Show) U.ToggleOn(Field, Enable);
            else U.ToggleOff(Field, Enable);
        })
    },
    IfRadioEquals: function(Show, Group, Value, Field, Enable) {
        Value = Value.split("|");
        if (($.inArray($(":radio[name=" + Group + "]:checked").val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable);
        else U.ToggleOff(Field, Enable);
        $(":radio[name=" + Group + "]").click(function() {
            if (($.inArray($(":radio[name=" + Group + "]:checked").val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable);
            else U.ToggleOff(Field, Enable);
        })
    },
    IfSelectEquals: function(Show, Select, Value, Field, Enable) {
        Value = Value.split("|");
        if (($.inArray(Select.val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable);
        else U.ToggleOff(Field, Enable);
        Select.change(function() {
            if (($.inArray(Select.val(), Value) >= 0) == Show) U.ToggleOn(Field, Enable);
            else U.ToggleOff(Field, Enable);
        })
    },
    LoadSelect: function(Url, Arguments, Select, Title, ZeroIndex, Action) {
        U.PostUrl(Url, Arguments, function(Data) {
            Select.empty();
            if (ZeroIndex) Select.append(new Option(Title, "0"));
            $.each(Data, function(Key, Value) { Select.append(new Option(Value.Name, Value.Id)) });
            if (typeof Action == "function") Action();
        })
    },
    NoneOfTheAbove: function(Checkbox, Group) {
        if (Checkbox.prop("checked")) Group.each(function() {
            if ($(this).prop("id") != Checkbox.prop("id")) $(this).prop("checked", false).change();
        });
        Group.change(function() {
            if ($(this).prop("id") != Checkbox.prop("id") && $(this).prop("checked")) Checkbox.prop("checked", false).change();
        });
        Checkbox.change(function() {
            if ($(this).prop("checked")) Group.each(function() {
                if ($(this).prop("id") != Checkbox.prop("id")) $(this).prop("checked", false).change();
            })
        })
    },
    SelectAll: function(Checkbox, Group) {
        Checkbox.change(function() {
            Group.each(function() {
                if ($(this).prop("checked") != Checkbox.prop("checked")) $(this).prop("checked", Checkbox.prop("checked")).change();
            });
            var Label = $(this).closest(".window-content").find("label[for=" + $(this).attr("id") + "]");
            if (Label != undefined && Label.text().indexOf(" ")) {
                var LabelSplit = Label.text().split(/ /);
                if (LabelSplit[0] == "Select" || "Deselect") Label.text((Checkbox.prop("checked") ? "Deselect" : "Select") + Label.text().substring(Label.text().indexOf(" "), Label.text().length));
            }
        });
    },
    ShowIfChecked: function(Checkbox, Field) { this.IfChecked(true, Checkbox, Field) },
    ShowIfRadioEquals: function(Group, Value, Field) { this.IfRadioEquals(true, Group, Value, Field) },
    ShowIfSelectEquals: function(Select, Value, Field) { this.IfSelectEquals(true, Select, Value, Field) },
    ToggleOn: function(Field, Enable) {
        if (Enable) Field.removeAttr("disabled").removeClass("form-omitted");
        else Field.show().removeClass("form-omitted");
        if (Field.hasClass("required-disabled")) Field.removeClass("required-disabled").addClass("required");
        if ($(".required-disabled", Field).length) $(".required-disabled", Field).removeClass("required-disabled").addClass("required");
    },
    ToggleOff: function(Field, Enable) {
        if (Enable) Field.attr("disabled", true).addClass("form-omitted");
        else Field.hide().addClass("form-omitted");
        if (Field.hasClass("required")) Field.removeClass("required").addClass("required-disabled");
        if ($(".required", Field).length) $(".required", Field).removeClass("required").addClass("required-disabled");
    },
    ToggleRequired: function(Checkbox, Checked, Field) {
        if (Checkbox.prop("checked") == Checked) Field.addClass("required");
        else Field.removeClass("required");
        Checkbox.change(function() {
            if (Checkbox.prop("checked") == Checked) Field.addClass("required");
            else Field.removeClass("required");
        })
    }
});