﻿$.extend(Referral, {
    Physician: {
        Add: function(id, referralId, $grid) {
            U.GridAjaxCommand("Referral/Physician/Add", { id: id, referralId: referralId }, $grid);
        },
        AddRemove: function(e) {
            if (e.text() == "+") e.closest(".column").find(".row.hidden:first").removeClass("hidden").find("input:first").blur();
            else e.closest(".row").addClass("hidden").appendTo($(el).closest(".column")).find("select").val("00000000-0000-0000-0000-000000000000");
        },
        Delete: function(id, referralId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to remove this physician?",
                Yes: function() { U.GridAjaxCommand("Referral/Physician/Delete", { id: id, referralId: referralId }, $grid) }
            })
        },
        SetPrimary: function(id, referralId, $grid) {
            U.GridAjaxCommand("Referral/Physician/SetPrimary", { id: id, referralId: referralId }, $grid);
        }
    }
});