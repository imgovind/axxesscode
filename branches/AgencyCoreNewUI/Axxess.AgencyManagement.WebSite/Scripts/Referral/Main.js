﻿var Referral = {
    Admit: function(id) {
        Acore.Open("NewPatient", "Patient/Admit", { id: id })
    },
    Delete: function(id, $grid) {
        Acore.Confirm({
            Message: "Are you sure you want to delete this referral?",
            Yes: function() { U.GridAjaxCommand("Referral/Delete", { id: id }, $grid) }
        })
    },
    Edit: function(id) {
        Acore.Open("EditReferral", { id: id })
    },
    Export: function() {
        U.GetAttachment("Referral/Export", {})
    },
    InitEdit: function(r, t, x, e) {
        var Type = U.GetType(e), Prefix = "#" + Type + "_";
        U.InitShared(r, t, x, e);
        $(".add-referral-physician", e).click(function() {
            var $physician = $(this).closest(".row").find(".physician-picker"), physicianId = $physician.siblings("input[type=hidden]").val();
            $physician.AjaxAutocomplete("reset");
            if (U.IsGuid(physicianId)) Referral.Physician.Add(physicianId, $(Prefix + "Id").val(), $(Prefix + "PhysicianGrid"));
            else U.Growl("Please Select a Physician", "error");
        });
        $(".activity-log", e).click(function() {
            Referral.Logs($(Prefix + "Id").val());
            return false;
        });
    },
    InitNew: function(r, t, x, e) {
        U.InitShared(r, t, x, e);
        $(".addrem", e).click(function() {
            Referral.Physician.AddRemove($(this));
            return false;
        })
    },
    InitNonAdmit: function(r, t, x, e) {
        U.InitShared(r, t, x, e);
    },
    InitList: function(r, t, x, e) {
        $(".new-referral", e).click(function() {
            Referral.New();
            return false;
        });
        $(".export", e).click(function() {
            Referral.Export();
            return false;
        });
    },
    Logs: function(id) {
        Acore.Open("ReferralLogs", { id: id })
    },
    New: function() {
        Acore.Open("NewReferral")
    },
    NonAdmit: function(id) {
        Acore.Modal({
            Name: "Non-Admit Referral",
            Url: "Referral/NonAdmit",
            Input: { id: id },
            OnLoad: Referral.InitNonAdmit,
            Width: 800
        })
    },
    Print: function(id) {
        U.GetAttachment("Referral/Print", { id: id })
    }
}