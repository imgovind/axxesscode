﻿$.extend(Agency, {
    Contact: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this contact?",
                Yes: function() { U.GridAjaxCommand("Agency/Contact/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditContact", { id: id });
        },
        Export: function() {
            U.GetAttachment("Agency/Contact/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            Agency.Contact.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Agency.Contact.Logs($("[name=Id]", e).val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-contact", e).click(function() {
                Agency.Contact.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Contact.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            Agency.Contact.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.ShowIfSelectEquals($(prefix + "Type"), "Other", $(prefix + "TypeMore"))
            U.InitShared(r, t, x, e);
        },
        Logs: function(id) {
            Acore.Open("ContactLogs", { id: id });
        },
        New: function() {
            Acore.Open("NewContact");
        }
    }
});