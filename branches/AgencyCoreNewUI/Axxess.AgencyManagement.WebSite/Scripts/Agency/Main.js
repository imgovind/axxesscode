﻿var Agency = {
    CheckSignature: function() {
        if ($("#CheckSignature").val().length > 0) {
            U.PostUrl("Agency/CheckSignature", { signature: $("#CheckSignature").val() }, function(result) {
                U.GrowlResult(result);
                if (result.isSuccessful) {
                    Acore.Open("EditCompanyInformation", "Agency/Info", function(r, t, x, e) { $("form", e).Validate() })
                    $(this).closest(".window").Close();
                }
            })
        } else U.Growl("Please provide your signature before proceeding", "error");
    },
    Edit: function(id) {
        Acore.Open("EditAgency", { id: id });
    },
    GenerateClick: function(pageName) {
        var data = { BranchId: $("#" + pageName + "_BranchId").val(), StartDate: $("#" + pageName + "_StartDate").val(), EndDate: $("#" + pageName + "_EndDate").val() };
        if (pageName == "OrdersToBeSent") {data.sendAutomatically = $("#" + pageName + "_SendType").val();}
        U.RebindGridWithExportLink(pageName, $('#' + pageName + '_Grid').data('tGrid'), data);
    },
    InitOrderHistoryEdit: function() { $("form", e).Validate() },
    LoadAgencyInsurances: function(branchId, insuranceControlId) {
        U.PostUrl("Agency/Insurance/PatientSelectList", { branchId: branchId }, function(data) {
            var s = $("select#" + insuranceControlId);
            s.children("option").remove();
            s.append(data);
        })
    },
    LoadBranchContent: function(branchId) { $("#EditAgencyInfo_Content").Load("Agency/InfoContent", { branchId: branchId }) },
    LoadCaseManagement: function() {
        $("#CaseManagement_Content").Load("Agency/CaseManagementContent", {
            groupName: $("#CaseManagement_GroupName").val(),
            BranchId: $("#CaseManagement_BranchCode").val(),
            Status: $("#CaseManagement_Status").val(),
            SortParams: $("#CaseManagement_SortParams").val()
        })
    },
    LoadPrintQueue: function(groupName) { $("#PrintQueue_Content").Load("Agency/PrintQueueContent", { groupName: groupName }) },
    LoadVisitRate: function() { Acore.Open("VisitRates") },
    OrderCenterOnLoad: function(e) { $(".t-grid-edit", e.row).html("Receive Order") },
    PendingSignatureOrdersOnEdit: function(e) {
        var form = e.form;
        var dataItem = e.dataItem;
        if (dataItem != null) {
            dataItem.StartDate = $("#OrdersPendingSignature_StartDate").val();
            dataItem.EndDate = $("#OrdersPendingSignature_EndDate").val();
            dataItem.BranchId = $("#OrdersPendingSignature_BranchId").val();
            e.dateItem = dataItem;
        }
    },
    PastDueRecerts: function() {Acore.Open("ListPastDueRecerts", 'Agency/RecertsPastDueGrid', function() { });},
    UpcomingRecerts: function() {Acore.Open("ListUpcomingRecerts", 'Agency/RecertsUpcomingGrid', function() { });},
    RebindPrintQueue: function() { U.RebindGrid($('#PrintQueue_Grid')); }
}