﻿$.extend(Agency, {
    Supply: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this supply?",
                Yes: function() { U.GridAjaxCommand("Agency/Supply/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditSupply", { id: id });
        },
        Export: function() {
            U.GetAttachment("Agency/Supply/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Agency.Supply.Logs($("[name=Id]", e).val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-supply", e).click(function() {
                Agency.Supply.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Supply.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        Logs: function(id) {
            Acore.Open("SupplyLogs", { id: id });
        },
        New: function() {
            Acore.Open("NewSupply");
        }
    }
});