﻿$.extend(Agency, {
    Incident: {
        Edit: function(id) {
            Acore.Open("EditIncident", { id: id })
        },
        Export: function() {
            U.GetAttachment("Agency/Incident/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            $(".new-incident", e).click(function() {
                Agency.Incident.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Incident.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            Schedule.InitEpisodeDropDown(e);
            U.InitShared(r, t, x, e);
        },
        New: function() {
            Acore.Open("NewIncident");
        },
        Print: function(id, patientId, episodeId, QA) {
            var Arguments = {
                Url: "Agency/Incident/PrintPreview",
                PdfUrl: "Agency/Incident/Print",
                Data: { id: id, patientId: patientId, epidsodeId: episodeId }
            };
            if (QA) $.extend(Arguments, {
                ReturnClick: function() { Agency.Incident.Return(id, patientId, episodeId) },
                Buttons: [
                    { Text: "Edit", Click: function() { Agency.Incident.Edit(id) } },
                    { Text: "Approve", Click: function() { Agency.Incident.Approve(id, patientId, episodeId) } }
                ]
            });
            Acore.OpenPrintView(Arguments);
        }
    }
});