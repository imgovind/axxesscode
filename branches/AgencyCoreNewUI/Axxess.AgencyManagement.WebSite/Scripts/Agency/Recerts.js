﻿$.extend(Agency, {
    Recerts: {
        PastDue: {
            AcoreId: "ListPastDueRecerts",
            Export: function(branchId, insuranceId, startDate, endDate) { U.GetAttachment("Agency/Recerts/PastDue/Export", { branchId: branchId, insuranceId: insuranceId, startDate: startDate, endDate: endDate }) },
            Init: function(r, t, x, e) {
                $(".export", e).click(function() {
                    Agency.Recerts.PastDue.Export($("[name=BranchId]", e).val(), $("[name=InsuranceId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                    return false;
                });
                $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                    Agency.Recerts.PastDue.Refresh();
                    return false;
                });
                $("[name=BranchId],[name=InsuranceId],[name=StartDate],[name=EndDate]", e).change(function() { Agency.Recerts.PastDue.Refresh() });
            },
            List: function() { Acore.Open(Agency.Recerts.PastDue.AcoreId) },
            Refresh: function() {
                var e = $("#window_" + Agency.Recerts.PastDue.AcoreId + " .main");
                U.RebindGrid($(".t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    insuranceId: $("[name=InsuranceId]", e).val(),
                    startDate: $("[name=StartDate]", e).val(),
                    endDate: $("[name=EndDate]", e).val()
                })
            }
        },
        Upcoming: {
            AcoreId: "ListUpcomingRecerts",
            Export: function(branchId, insuranceId) { U.GetAttachment("Agency/Recerts/Upcoming/Export", { branchId: branchId, insuranceId: insuranceId }) },
            Init: function(r, t, x, e) {
                $(".export", e).click(function() {
                    Agency.Recerts.Upcoming.Export($("[name=BranchId]", e).val(), $("[name=InsuranceId]", e).val());
                    return false;
                });
                $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                    Agency.Recerts.Upcoming.Refresh();
                    return false;
                });
                $("[name=BranchId],[name=InsuranceId]", e).change(function() { Agency.Recerts.Upcoming.Refresh() });
            },
            List: function() { Acore.Open(Agency.Recerts.Upcoming.AcoreId) },
            Refresh: function() {
                var e = $("#window_" + Agency.Recerts.Upcoming.AcoreId + " .main");
                U.RebindGrid($(".t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    insuranceId: $("[name=InsuranceId]", e).val()
                })
            }
        }
    }
});