﻿$.extend(Agency, {
    Insurance: {
        BillData: {
            Delete: function(id, insuranceId, $grid) {
                Acore.Confirm({
                    Message: "Are you sure you want to delete this visit rate?",
                    Yes: function() { U.GridAjaxCommand("Agency/BillData/Delete", { InsuranceId: insuranceId, Id: id }, $grid) }
                })
            },
            Edit: function(id, insuranceId) {
                Acore.Modal({
                    Name: "Visit Bill Information",
                    Url: "Agency/Insurance/BillData/Edit",
                    Input: { id: id, insuranceId: insuranceId },
                    OnLoad: Agency.Insurance.BillData.InitEdit,
                    Width: 450
                })
            },
            InitEdit: function(r, t, x, e) {
                Agency.Insurance.BillData.InitShared(r, t, x, e);
            },
            InitNew: function(r, t, x, e) {
                Agency.Insurance.BillData.InitShared(r, t, x, e);
            },
            InitShared: function(r, t, x, e) {
                var type = U.GetType(e), prefix = "#" + type + "_", win = e.closest(".window");
                // Set loading class until window is properly setup
                e.addClass("loading");
                U.ShowIfSelectEquals($(prefix + "ChargeType"), "3", $(prefix + "IsTimeLimitContent"));
                U.ShowIfSelectEquals($(prefix + "ChargeType"), "1", $(prefix + "PerVisitUnitContent"));
                U.ShowIfChecked($(prefix + "IsTimeLimit"), $(prefix + "TimeLimitContent"));
                // Set window resizing on form elements that show/hide other elements
                $(prefix + "ChargeType").change(function() {
                    e.addClass("loading");
                    setTimeout(function() { win.SetHeight(false) }, 50)
                });
                $(prefix + "IsTimeLimit").click(function() {
                    e.addClass("loading");
                    setTimeout(function() { win.SetHeight(false) }, 50)
                });
                // Give a quarter second for layout to rest, then initiate resize
                setTimeout(function() { win.SetHeight(false) }, 1000);
                U.InitShared(r, t, x, e);
            },
            Import: function(from, to, $grid) {
                if (from == 0) U.Growl("Select the insurance", "error");
                else Acore.Confirm({
                    Message: "Are you sure you want to overwrite visit information?",
                    Yes: function() { U.GridAjaxCommand("Agency/Insurance/DuplicateVisitRates", { id: to, replacedId: from }, $grid) }
                })
            },
            New: function(insuranceId) {
                Acore.Modal({
                    Name: "Visit Bill Information",
                    Url: "Agency/Insurance/BillData/New",
                    Input: { insuranceId: insuranceId },
                    OnLoad: Agency.Insurance.BillData.InitNew,
                    Width: 450
                })
            }
        },
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this payor?",
                Yes: function() { U.GridAjaxCommand("Agency/Insurance/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditInsurance", { id: id });
        },
        Export: function() {
            U.GetAttachment("Agency/Insurance/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            Agency.Insurance.InitShared(r, t, x, e);
            $(".apply-visit-info", e).click(function() {
                Agency.Insurance.BillData.Import($(prefix + "OldInsuranceId").val(), $("[name=Id]", e).val(), $(prefix + "BillDatas"));
                $(prefix + "OldInsuranceId").val("0");
                return false;
            });
            $(".add-visit-info", e).click(function() {
                Agency.Insurance.BillData.New($("[name=Id]", e).val());
                return false;
            });
            $(".activity-log", e).click(function() {
                Agency.Insurance.Logs($("[name=Id]", e).val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-insurance", e).click(function() {
                Agency.Insurance.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Insurance.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            Agency.Insurance.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.HideIfChecked($(prefix + "IsAxxessTheBiller"), $(prefix + "EdiInformation"));
            U.InitShared(r, t, x, e);
        },
        Logs: function(id) {
            Acore.Open("InsuranceLogs", { id: id });
        },
        New: function() {
            Acore.Open("NewInsurance");
        },
        VisitRates: {
            Edit: function() {
                Acore.Open("EditVisitRates");
            },
            InitEdit: function(r, t, x, e) {
                var type = U.GetType(e), prefix = "#" + type + "_";
                U.InitShared(r, t, x, e);
                $(prefix + "LocationId").change(function() {
                    $(prefix + "Content").Load("Agency/VisitRateContent", { branchId: $(this).val() });
                });
            }
        }
    }
});