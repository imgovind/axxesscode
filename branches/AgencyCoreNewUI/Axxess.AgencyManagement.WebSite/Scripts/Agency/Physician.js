﻿$.extend(Agency, {
    Physician: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this physician?",
                Yes: function() { U.GridAjaxCommand("Agency/Physician/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditPhysician", { Id: id });
        },
        Export: function() {
            U.GetAttachment("Agency/Physician/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            $(".new-physician-license", e).click(function() {
                Agency.Physician.License.New($("[name=Id]", e).val());
                return false;
            });
            $(".activity-log", e).click(function() {
                Agency.Physician.Logs($("[name=Id]", e).val());
                return false;
            });
            Agency.Physician.InitShared(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            $(".new-physician", e).click(function() {
                Acore.Open("NewPhysician");
                return false;
            });
            $(".export", e).click(function() {
                Agency.Physician.Export();
                return false;
            })
        },
        InitNew: function(r, t, x, e) {
            if (e.closest(".window").IsDialog()) U.NewPrefix("NewPhysicianModal", e);
            var type = U.GetType(e), prefix = "#" + type + "_";
            $(prefix + "NpiNumber").blur(function() { Agency.Physician.PecosCheck($(this).val(), $(prefix + "PecosCheck")) });
            $(prefix + "NpiSearch").NpiSearch(prefix);
            Agency.Physician.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.ToggleRequired($(prefix + "PhysicianAccess"), true, $(prefix + "Email"));
            U.InitShared(r, t, x, e);
        },
        License: {
            Delete: function(id, physicianId, $grid) {
                Acore.Confirm({
                    Message: "Are you sure you want to delete this physician license?",
                    Yes: function() { U.GridAjaxCommand("Agency/Physician/License/Delete", { id: id, physicianId: physicianId }, $grid) }
                })
            },
            Edit: function(id, physicianId) {
                Acore.Modal({
                    Name: "Edit Physician License",
                    Url: "Agency/Physician/License/Edit",
                    Input: { id: id, physicianId: physicianId },
                    OnLoad: Agency.Physician.License.InitEdit,
                    Width: 450
                })
            },
            InitEdit: function(r, t, x, e) {
                U.InitShared(r, t, x, e);
            },
            InitNew: function(r, t, x, e) {
                U.InitShared(r, t, x, e);
            },
            New: function(physicianId) {
                Acore.Modal({
                    Name: "New Physician License",
                    Url: "Agency/Physician/License/New",
                    Input: { physicianId: physicianId },
                    OnLoad: Agency.Physician.License.InitNew,
                    Width: 450
                })
            }
        },
        New: function(modal) {
            if (modal) Acore.Modal({
                Name: "New Physician",
                Url: "Agency/Physician/New",
                OnLoad: Agency.Physician.InitNew,
                Width: 900
            });
            else Acore.Open("NewPhysician");
        },
        NewModal: function() {
            Agency.Physician.New(true);
        },
        Logs: function(id) {
            Acore.Open("PhysicianLogs", { id: id });
        },
        PecosCheck: function(npi, $display) {
            if (npi.length > 0) U.PostUrl("Physician/CheckPecos", { npi: npi }, function(result) {
                if (result.isSuccessful) $display.html($("<span/>").addClass("img icon success-small"));
                else $display.html($("<span/>").addClass("img icon error-small"));
            });
            else $display.hide();
        }
    }
});