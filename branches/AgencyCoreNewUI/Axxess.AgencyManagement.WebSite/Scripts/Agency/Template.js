﻿$.extend(Agency, {
    Template: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this template?",
                Yes: function() { U.GridAjaxCommand("Agency/Template/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditTemplate", { id: id });
        },
        Export: function() {
            U.GetAttachment("Agency/Template/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Agency.Template.Logs($("[name=Id]", e).val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-template", e).click(function() {
                Agency.Template.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Template.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        Logs: function(id) {
            Acore.Open("TemplateLogs", { id: id });
        },
        New: function() {
            Acore.Open("NewTemplate");
        }
    }
});