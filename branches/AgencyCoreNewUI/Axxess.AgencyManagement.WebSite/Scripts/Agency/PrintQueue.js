﻿$.extend(Agency, {
    PrintQueue: {
        AcoreId: "PrintQueue",
        Export: function() { U.GetAttachment("Agency/PrintQueue/Export", {}) },
        Init: function(r, t, x, e) {
            $(".grid-refresh", e).click(function() {
                Agency.PrintQueue.Refresh();
                return false;
            });
            $(".export", e).click(function() {
                Agency.PrintQueue.Export();
                return false;
            });
            $(".sort", e).click(function() {
                switch ($(this).text()) {
                    case "Group By Patient": $("[name=GroupName]", e).val("PatientName"); break;
                    case "Group By Date": $("[name=GroupName]", e).val("EventDate"); break;
                    case "Group By Task": $("[name=GroupName]", e).val("DisciplineTaskName"); break;
                    case "Group By Clinician": $("[name=GroupName]", e).val("UserName"); break;
                }
                Agency.PrintQueue.Refresh();
                return false;
            });
            U.InitShared(r, t, x, e);
        },
        Refresh: function() {
            var e = $("#window_" + Agency.PrintQueue.AcoreId + " .main");
            $(".content", e).Load("Agency/PrintQueue/Grid", { groupName: $("[name=GroupName]", e).val() });
        }
    }
});