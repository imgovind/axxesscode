﻿$.extend(Agency, {
    QA: {
        Export: function(branch, status) {
            U.GetAttachment("Agency/QA/Export", { branch: branch, status: status });
        },
        Init: function(r, t, x, e) {
            $(".grid-refresh", e).click(function() {
                Agency.QA.Refresh();
                return false;
            });
            $(".grid-controls select", e).change(function() { Agency.QA.Refresh() });
            $(".export", e).click(function() {
                Agency.QA.Export($("#CaseManagement_BranchCode").val(), $("#CaseManagement_Status").val());
                return false;
            });
            $(".sort", e).click(function() {
                switch ($(this).text()) {
                    case "Group By Patient": $("#CaseManagement_GroupName").val("PatientName"); break;
                    case "Group By Date": $("#CaseManagement_GroupName").val("EventDate"); break;
                    case "Group By Task": $("#CaseManagement_GroupName").val("DisciplineTaskName"); break;
                    case "Group By Clinician": $("#CaseManagement_GroupName").val("UserName"); break;
                }
                Agency.QA.Refresh();
                return false;
            });
            $(".bulk", e).click(function() {
                $("[name=CommandType]", e).val($(this).text().split(" ")[0]).closest("form").submit();
                return false;
            });
            U.InitShared(r, t, x, e);
        },
        Refresh: function() {
            $("#CaseManagement_Content").Load("Agency/QA/Grid", {
                group: $("#CaseManagement_GroupName").val(),
                branch: $("#CaseManagement_BranchCode").val(),
                status: $("#CaseManagement_Status").val(),
                sort: $("#CaseManagement_SortParams").val()
            });
        },
        MarkOrder: function(mark, container) {
            fields = $("select.SendAutomatically, input.OrdersToBeSent:checked", $(container)).serializeArray();
            U.PostUrl("Agency/MarkOrdersAs" + mark, fields, function(data) {
                U.RebindAllGrids();
                U.GrowlResult(result);
            })
        },
        MarkOrdersAsReturned: function(container) {
            Agency.QA.MarkOrder("Returned", container);
        },
        MarkOrdersAsSent: function(container) {
            Agency.QA.MarkOrder("Sent", container);
        }
    }
});