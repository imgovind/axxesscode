﻿$.extend(Agency, {
    Infection: {
        Edit: function(id) {
            Acore.Open("EditInfection", { id: id })
        },
        Export: function() {
            U.GetAttachment("Agency/Infection/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitList: function(r, t, x, e) {
            $(".new-infection", e).click(function() {
                Agency.Infection.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Infection.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            Schedule.InitEpisodeDropDown(e);
            U.InitShared(r, t, x, e);
        },
        New: function() {
            Acore.Open("NewInfection");
        },
        Print: function(id, patientId, episodeId, QA) {
            var Arguments = {
                Url: "Agency/Infection/PrintPreview",
                PdfUrl: "Agency/Infection/Print",
                Data: { id: id, patientId: patientId, epidsodeId: episodeId }
            };
            if (QA) $.extend(Arguments, {
                ReturnClick: function() { Agency.Infection.Return(id, patientId, episodeId) },
                Buttons: [
                    { Text: "Edit", Click: function() { Agency.Infection.Edit(id) } },
                    { Text: "Approve", Click: function() { Agency.Infection.Approve(id, patientId, episodeId) } }
                ]
            });
            Acore.OpenPrintView(Arguments);
        }
    }
});