﻿$.extend(Agency, {
    Hospital: {
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this hospital?",
                Yes: function() { U.GridAjaxCommand("Agency/Hospital/Delete", { id: id }, $grid) }
            })
        },
        Edit: function(id) {
            Acore.Open("EditHospital", { id: id });
        },
        Export: function() {
            U.GetAttachment("Agency/Hospital/Export", {});
        },
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Agency.Hospital.Logs($("[name=Id]", e).val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-hospital", e).click(function() {
                Agency.Hospital.New();
                return false;
            });
            $(".export", e).click(function() {
                Agency.Hospital.Export();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        Logs: function(id) {
            Acore.Open("HospitalLogs", { id: id });
        },
        New: function() {
            Acore.Open("NewHospital");
        }
    }
});