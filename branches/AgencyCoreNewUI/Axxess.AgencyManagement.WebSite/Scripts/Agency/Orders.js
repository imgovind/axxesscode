﻿$.extend(Agency, {
    Orders: {
        History: {
            AcoreId: "OrdersHistory",
            Edit: function(id, patientId, type) {
                Acore.Modal({
                    Name: "Edit Order",
                    Url: "Agency/Orders/History/Edit",
                    Input: { id: id, patientId: patientId, type: type },
                    OnLoad: Agency.Orders.History.InitEdit,
                    Width: 450
                })
            },
            Export: function(branchId, startDate, endDate) { U.GetAttachment("Agency/Orders/History/Export", { branchId: branchId, startDate: startDate, endDate: endDate }) },
            InitEdit: function(r, t, x, e) { U.InitShared(r, t, x, e) },
            InitList: function(r, t, x, e) {
                $(".export", e).click(function() {
                    Agency.Orders.History.Export($("[name=BranchId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                    return false;
                });
                $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                    Agency.Orders.History.Refresh();
                    return false;
                });
                $("[name=BranchId],[name=StartDate],[name=EndDate]", e).change(function() { Agency.Orders.History.Refresh() });
            },
            List: function() { Acore.Open(Agency.Orders.History.AcoreId) },
            Refresh: function() {
                var e = $("#window_" + Agency.Orders.History.AcoreId + " .main");
                U.RebindGrid($(".t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    startDate: $("[name=StartDate]", e).val(),
                    endDate: $("[name=EndDate]", e).val()
                })
            }
        },
        PendingSignature: {
            AcoreId: "OrdersPendingSignature",
            Export: function(branchId, startDate, endDate) { U.GetAttachment("Agency/Orders/PendingSignature/Export", { branchId: branchId, startDate: startDate, endDate: endDate }) },
            InitList: function(r, t, x, e) {
                $(".export", e).click(function() {
                    Agency.Orders.PendingSignature.Export($("[name=BranchId]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                    return false;
                });
                $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                    Agency.Orders.PendingSignature.Refresh();
                    return false;
                });
                $("[name=BranchId],[name=StartDate],[name=EndDate]", e).change(function() { Agency.Orders.PendingSignature.Refresh() });
            },
            InitReceive: function(r, t, x, e) { U.InitShared(r, t, x, e) },
            List: function() { Acore.Open(Agency.Orders.PendingSignature.AcoreId) },
            Receive: function(id, patientId, type) {
                Acore.Modal({
                    Name: "Receive Order",
                    Url: "Agency/Orders/PendingSignature/Receive",
                    Input: { id: id, patientId: patientId, type: type },
                    OnLoad: Agency.Orders.PendingSignature.InitReceive,
                    Width: 450
                })
            },
            Refresh: function() {
                var e = $("#window_" + Agency.Orders.PendingSignature.AcoreId + " .main");
                U.RebindGrid($(".t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    startDate: $("[name=StartDate]", e).val(),
                    endDate: $("[name=EndDate]", e).val()
                })
            }
        },
        ToBeSent: {
            AcoreId: "OrdersToBeSent",
            Export: function(branchId, sendAutomatically, startDate, endDate) { U.GetAttachment("Agency/Orders/ToBeSent/Export", { branchId: branchId, sendAutomatically: sendAutomatically, startDate: startDate, endDate: endDate }) },
            InitList: function(r, t, x, e) {
                $(".export", e).click(function() {
                    Agency.Orders.ToBeSent.Export($("[name=BranchId]", e).val(), $("[name=SendAutomatically]", e).val(), $("[name=StartDate]", e).val(), $("[name=EndDate]", e).val());
                    return false;
                });
                $(".grid-refresh,.t-refresh", e).attr("href", "").click(function() {
                    Agency.Orders.ToBeSent.Refresh();
                    return false;
                });
                $("[name=BranchId],[name=StartDate],[name=EndDate]", e).change(function() { Agency.Orders.ToBeSent.Refresh() });
                $("[name=SendAutomatically]", e).change(function() {
                    if ($(this).val() == "true") $(".send", e).text("Send Electronically");
                    else $(".send", e).text("Mark as Sent");
                    Agency.Orders.ToBeSent.Refresh();
                });
                $(".send").click(function() {
                    Agency.Orders.ToBeSent.Send();
                    return false;
                });
            },
            List: function() { Acore.Open(Agency.Orders.ToBeSent.AcoreId) },
            Refresh: function() {
                var e = $("#window_" + Agency.Orders.ToBeSent.AcoreId + " .main");
                U.RebindGrid($(".t-grid", e), {
                    branchId: $("[name=BranchId]", e).val(),
                    sendAutomatically: $("[name=SendAutomatically]", e).val(),
                    startDate: $("[name=StartDate]", e).val(),
                    endDate: $("[name=EndDate]", e).val()
                })
            },
            Send: function() {
                var e = $("#window_" + Agency.Orders.ToBeSent.AcoreId + " .main");
                if ($("[name=OrdersToBeSent]:checked", e).length > 0) U.PostUrl("Agency/Orders/Send", $("[name=SendAutomatically],[name=OrdersToBeSent]:checked", e).serializeArray(), function(result) {
                    U.GrowlResult(result);
                    if (result.isSuccessful) U.RebindAllGrids();
                });
                else U.Growl("Please select order(s) to be sent to physician.", "error");
            }
        }
    }
});