﻿//namespace Axxess.AgencyManagement.PD.App.Controllers
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;
//    using Axxess.Core.Infrastructure;
//    using System.Web.Mvc;
//    using Telerik.Web.Mvc;
//    using Axxess.AgencyManagement.Repositories;
//    using Axxess.AgencyManagement.PD.Domain;
//    using Axxess.AgencyManagement.Domain;
//    using Axxess.Core;
//    using Axxess.AgencyManagement.Enums;
//    using Axxess.AgencyManagement.PD.App.ViewData;

//    [Compress]
//    [Authorize]
//    [HandleError]
//    [SslRedirect]
//    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
//   public class PDScheduleController : BaseController
//    {
//        #region Constructor

//        private readonly IUserRepository userRepository;
//        private readonly IAssetRepository assetRepository;
//        private readonly IAgencyRepository agencyRepository;
//        private readonly IPatientRepository patientRepository;
//        private readonly IBillingRepository billingRepository;
//        private readonly IPhysicianRepository physicianRepository;

//        public PDScheduleController(IAgencyManagementDataProvider dataProvider)
//        {

//            this.userRepository = dataProvider.UserRepository;
//            this.assetRepository = dataProvider.AssetRepository;
//            this.agencyRepository = dataProvider.AgencyRepository;
//            this.patientRepository = dataProvider.PatientRepository;
//            this.billingRepository = dataProvider.BillingRepository;
//            this.physicianRepository = dataProvider.PhysicianRepository;
//        }

//        #endregion

//        #region Schedule Center
//        [GridAction]
//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult Center()
//        {
//            //var viewData = new PdS();
//            //if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) viewData.Count = patientRepository.GetPatientStatusCount(Current.AgencyId, (int)PatientStatus.Active);
//            //else if (Current.IsClinicianOrHHA) viewData.Count = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).Count;
//            //else viewData.Count = 0;
//            return PartialView("Center/Layout");
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult CenterContent(Guid patientId)
//        {
//            var viewData = new PDCalendarViewData();
//            viewData.StartDate = DateUtilities.GetStartOfMonth(DateTime.Now.Month, DateTime.Now.Year);
//            viewData.Year = viewData.StartDate.Year;
//            viewData.Month = viewData.StartDate.Month;
//            viewData.Day = viewData.StartDate.Day;
//            viewData.PatientId = patientId;
//            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
//            if (patient != null)
//            {
//                //if (episodeId.HasValue) viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, (Guid)episodeId, patientId, "all");
//                //else viewData.Episode = patientRepository.GetEpisodeNew(Current.AgencyId, patientId, DateTime.Now, "all");
//                //if (viewData.Episode != null) viewData.Episode.DisplayName = patient.DisplayName;
//                //if (viewData.Episode != null) viewData.Episode.DisplayNameWithMi = patient.DisplayNameWithMi;
//                viewData.IsDischarged = patient.IsDischarged;
//            }
//            return PartialView("Center/Content", viewData);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult Monthly(Guid patientId, DateTime date)
//        {
//            var viewData = new PDCalendarViewData();
//            viewData.PatientId = patientId;
//            viewData.StartDate = DateUtilities.GetStartOfMonth(date.Month, date.Year);
//            viewData.EndDate = DateUtilities.GetEndOfMonth(date.Month, date.Year);
//            viewData.Year = viewData.StartDate.Year;
//            viewData.Month = viewData.StartDate.Month;
//            viewData.Day = viewData.StartDate.Day;
//            return PartialView("Center/Monthly", viewData);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult Weekly(Guid patientId, DateTime date)
//        {
//            var viewData = new PDCalendarViewData();
//            viewData.PatientId = patientId;
//            viewData.StartDate = DateUtilities.GetStartOfWeek(date, DayOfWeek.Sunday);
//            viewData.EndDate = DateUtilities.GetEndOfWeek(date, DayOfWeek.Sunday);
//            viewData.Year = viewData.StartDate.Year;
//            viewData.Month = viewData.StartDate.Month;
//            viewData.Day = viewData.StartDate.Day;
//            return PartialView("Center/Weekly", viewData);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult Daily(Guid patientId, DateTime date)
//        {
//            var viewData = new PDCalendarViewData();
//            viewData.PatientId = patientId;
//            viewData.StartDate = date;
//            viewData.Year = date.Year;
//            viewData.Month = date.Month;
//            viewData.Day = date.Day;
//            return PartialView("Center/Daily", viewData);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult CenterCalendar(Guid patientId, Guid? episodeId, string discipline)
//        {
//            var viewData = new ScheduleViewData();
//            viewData.PatientId = patientId;
//            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
//            if (patient != null)
//            {
//                if (episodeId.HasValue) viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, (Guid)episodeId, patientId, discipline);
//                else viewData.Episode = patientRepository.GetEpisodeNew(Current.AgencyId, patientId, DateTime.Now, discipline);
//                if (viewData.Episode != null) viewData.Episode.DisplayName = patient.DisplayName;
//                if (viewData.Episode != null) viewData.Episode.DisplayNameWithMi = patient.DisplayNameWithMi;
//                viewData.IsDischarged = patient.IsDischarged;
//            }
//            return PartialView("Center/Calendar", new CalendarViewData { Episode = viewData.Episode, PatientId = patientId, IsDischarged = viewData.IsDischarged });
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult CenterActivities(Guid patientId, Guid episodeId, string discipline)
//        {
//            return PartialView("Center/Activities", new ScheduleActivityArgument { EpisodeId = episodeId, PatientId = patientId, Discpline = discipline });
//        }

//        [GridAction]
//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult CenterActivitiesGrid(Guid patientId, int clendarType, int day, int week , int month, int year, string discipline)
//        {
//            return View(new GridModel(new List<PDScheduleEvent>()));//patientService.GetScheduledEventsNew(episodeId, patientId, discipline)
//        }

//        [GridAction]
//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult CenterPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
//        {
//            var patientList = new List<PatientSelection>();
//            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) 
//                patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId);
//            else if (Current.IsClinicianOrHHA)
//                patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
//            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult NewEvent(Guid patientId, DateTime date, string view)
//        {
//            var viewData = new PDCalendarViewData();
//            viewData.PatientId = patientId;
//            if (view == CalendarType.Monthly.ToString())
//            {
//                viewData.StartDate = DateUtilities.GetStartOfMonth(date.Month, date.Year);
//                viewData.EndDate = DateUtilities.GetEndOfMonth(date.Month, date.Year);
//            }
//            else if (view == CalendarType.Weekly.ToString())
//            {
//                 viewData.StartDate = DateUtilities.GetStartOfWeek(date,DayOfWeek.Sunday);
//                 viewData.EndDate = DateUtilities.GetEndOfMonth(viewData.StartDate.Month, viewData.StartDate.Year);
//            }
//            else if (view == CalendarType.Daily.ToString())
//            {
//                viewData.StartDate = date;
//                viewData.EndDate = DateUtilities.GetEndOfMonth(date.Month, date.Year);
//            }
//            viewData.Year = viewData.StartDate.Year;
//            viewData.Month = viewData.StartDate.Month;
//            viewData.Day = viewData.StartDate.Day;
//             var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
//             if (patient != null)
//             {
//                 viewData.DisplayNameWithMi = patient.DisplayNameWithMi;
//             }
//             return PartialView("NewEvent", viewData);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult NewEventAdd(Guid PatientId, Guid UserId, int Task, List<DateTime> EventDate)
//        {
//            var viewData = new JsonViewData();
//            viewData.isSuccessful = true;
//            viewData.errorMessage = string.Empty;
//            //viewData.PatientId = patientId;
//            //if (view == CalendarType.Monthly.ToString())
//            //{
//            //    viewData.StartDate = DateUtilities.GetStartOfMonth(date.Month, date.Year);
//            //    viewData.EndDate = DateUtilities.GetEndOfMonth(date.Month, date.Year);
//            //}
//            //else if (view == CalendarType.Weekly.ToString())
//            //{
//            //    viewData.StartDate = DateUtilities.GetStartOfWeek(date, DayOfWeek.Sunday);
//            //    viewData.EndDate = DateUtilities.GetEndOfMonth(viewData.StartDate.Month, viewData.StartDate.Year);
//            //}
//            //else if (view == CalendarType.Daily.ToString())
//            //{
//            //    viewData.StartDate = date;
//            //    viewData.EndDate = DateUtilities.GetEndOfMonth(date.Month, date.Year);
//            //}
//            //viewData.Year = viewData.StartDate.Year;
//            //viewData.Month = viewData.StartDate.Month;
//            //viewData.Day = viewData.StartDate.Day;
//            //var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
//            //if (patient != null)
//            //{
//            //    viewData.DisplayNameWithMi = patient.DisplayNameWithMi;
//            //}
//            return Json(viewData);
//        }

//        #endregion
        

//    }
//}
