﻿namespace Axxess.AgencyManagement.PD.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.PD.Domain;
    using Axxess.AgencyManagement.Enums;

   public class PDCalendarViewData
    {
       public Guid PatientId { get; set; }
       public bool IsDischarged { get; set; }
       public List<PDScheduleEvent> Schedules { get; set; }
       public CalendarType CalendarType { get; set; }
       public DateTime EndDate { get; set; }
       public DateTime StartDate { get; set; }
       public int Year { get; set; }
       public int Month { get; set; }
       public int Week { get; set; }
       public int Day { get; set; }
       public string DisplayNameWithMi { get; set; }
    }
}
