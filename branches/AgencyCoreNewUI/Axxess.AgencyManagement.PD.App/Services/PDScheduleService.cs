﻿namespace Axxess.AgencyManagement.PD.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.PD.Domain;
    using Axxess.AgencyManagement.PD.Repositories;
    using Axxess.AgencyManagement.Repositories;

    public class PDScheduleService : IPDScheduleService
    {
        #region Private Members


        private readonly IPDScheduleRepository pDScheduleRepository;
        private readonly IPatientRepository patientRepository;
        #endregion

        #region Constructor

        public PDScheduleService(IAgencyManagementPDDataProvider agencyManagementPDDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            this.pDScheduleRepository = agencyManagementPDDataProvider.PDScheduleRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        #endregion


        public bool AddSchedule(Guid agencyId, Guid patientId, Guid userId, int disciplineTask, List<DateTime> days, int timeIn, int timeOut)
        {
            bool result = false;
            var pdSchedules = new List<PDScheduleEvent>();
            if (!patientId.IsEmpty())
            {
                if (!userId.IsEmpty())
                {
                    if (Enum.IsDefined(typeof(DisciplineTasks), disciplineTask))
                    {
                        if (days != null && days.Count > 0)
                        {
                            days.ForEach(day =>
                            {
                                if (day.IsValid())
                                {
                                    var schedule = new PDScheduleEvent
                                    {
                                        AgencyId = agencyId,
                                        EventId = Guid.NewGuid(),
                                        PatientId = patientId,
                                        UserId = userId,
                                        DisciplineTask = disciplineTask,
                                        STimeIn = day.AddMinutes(timeIn),
                                        STimeOut = day.AddMinutes(timeOut)

                                    };
                                    ProcessScheduleFactory(schedule);
                                    pdSchedules.Add(schedule);
                                }
                            });
                            if (pdSchedules != null && pdSchedules.Count > 0)
                            {
                                result = this.AddScheduleWithNote(agencyId, patientId, pdSchedules);
                            }
                        }
                    }
                }

            }
            return result;
        }

        public bool AddScheduleWithNote(Guid agencyId, Guid patientId, List<PDScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var patient = patientRepository.GetPatientOnly(patientId, agencyId);
                if (patient != null)
                {
                    scheduleEvents.ForEach(ev =>
                    {
                        //  ProcessScheduleFactory(ev);
                        if (pDScheduleRepository.InsertSchedule(ev))
                        {
                            if (SaveScheduleModalFactory(ev))
                            {
                                if (pDScheduleRepository.InsertNoteMD(ev.EventId))
                                {
                                    result = true;
                                    // Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                                }
                                else
                                {
                                    pDScheduleRepository.RemoveModel<PDScheduleEvent>(ev.EventId);
                                    pDScheduleRepository.RemoveModel<PDPatientVisitNote>(ev.EventId);
                                }
                            }
                            else
                            {
                                pDScheduleRepository.RemoveModel<PDScheduleEvent>(ev.EventId);
                            }
                        }
                    });
                }
            }
            return result;
        }

        private void ProcessScheduleFactory(PDScheduleEvent scheduleEvent)
        {
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask))
            {
                case DisciplineTasks.OASISCDeath:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDeathOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDeathPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischargeOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischargePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.NonOASISDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCFollowUp:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCFollowupPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCFollowupOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertification:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertificationPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertificationOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.SNAssessmentRecert:
                case DisciplineTasks.NonOASISRecertification:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCarePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCareOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCarePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCareOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.NonOASISStartofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCTransferDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransfer:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransferPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransferOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.LVNSupervisoryVisit:
                case DisciplineTasks.HHAideSupervisoryVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PTEvaluation:
                case DisciplineTasks.PTReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.PTVisit:
                case DisciplineTasks.PTDischarge:
                case DisciplineTasks.PTAVisit:
                case DisciplineTasks.PTMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OTEvaluation:
                case DisciplineTasks.OTReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OTDischarge:
                case DisciplineTasks.OTVisit:
                case DisciplineTasks.COTAVisit:
                case DisciplineTasks.OTMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.STEvaluation:
                case DisciplineTasks.STReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.STVisit:
                case DisciplineTasks.STDischarge:
                case DisciplineTasks.STMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.MSWEvaluationAssessment:
                case DisciplineTasks.MSWVisit:
                case DisciplineTasks.MSWDischarge:
                case DisciplineTasks.MSWAssessment:
                case DisciplineTasks.MSWProgressNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.DriverOrTransportationNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.DieticianVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Dietician.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HHAideVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.HHAideCarePlan:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PASVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.PASCarePlan:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.DischargeSummary:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PhysicianOrder:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA485StandAlone:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.NonOasisHCFA485:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA485:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA486:
                case DisciplineTasks.PostHospitalizationOrder:
                case DisciplineTasks.MedicaidPOC:
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    break;
                case DisciplineTasks.IncidentAccidentReport:
                    scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.InfectionReport:
                    scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.SixtyDaySummary:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.TransferSummary:
                case DisciplineTasks.CoordinationOfCare:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.CommunicationNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.FaceToFaceEncounter:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.UAPWoundCareVisit:
                case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
            }
        }

        private bool SaveScheduleModalFactory(PDScheduleEvent scheduleEvent)
        {
            bool result = false;
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask))
            {
                //case DisciplineTasks.OASISCDeath:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                //    break;
                //case DisciplineTasks.OASISCDeathOT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                //    break;
                //case DisciplineTasks.OASISCDeathPT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                //    break;
                //case DisciplineTasks.OASISCDischarge:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                //    break;
                //case DisciplineTasks.OASISCDischargeOT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                //    break;
                //case DisciplineTasks.OASISCDischargePT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                //    break;
                //case DisciplineTasks.NonOASISDischarge:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisDischarge, episode);
                //    break;
                //case DisciplineTasks.OASISCFollowUp:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                //    break;
                //case DisciplineTasks.OASISCFollowupPT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                //    break;
                //case DisciplineTasks.OASISCFollowupOT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                //    break;
                //case DisciplineTasks.OASISCRecertification:
                //    var currentMedRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertification);
                //    break;
                //case DisciplineTasks.OASISCRecertificationPT:
                //    var currentMedRecertificationPT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationPT);
                //    break;
                //case DisciplineTasks.OASISCRecertificationOT:
                //    var currentMedRecertificationOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationOT);
                //    break;
                //case DisciplineTasks.SNAssessmentRecert:
                //case DisciplineTasks.NonOASISRecertification:
                //    var currentMedNonOasisRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisRecertification, episode, currentMedNonOasisRecertification);
                //    break;
                //case DisciplineTasks.OASISCResumptionofCare:
                //    var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                //    break;
                //case DisciplineTasks.OASISCResumptionofCarePT:
                //    var currentMedResumptionofCarePT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCarePT);
                //    break;
                //case DisciplineTasks.OASISCResumptionofCareOT:
                //    var currentMedResumptionofCareOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCareOT);
                //    break;
                //case DisciplineTasks.OASISCStartofCare:
                //    var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                //    break;
                //case DisciplineTasks.OASISCStartofCarePT:
                //    var currentMedStartofCarePT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCarePT);
                //    break;
                //case DisciplineTasks.OASISCStartofCareOT:
                //    var currentMedStartofCareOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCareOT);
                //    break;
                //case DisciplineTasks.SNAssessment:
                //case DisciplineTasks.NonOASISStartofCare:
                //    var currentMedNonOasisStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisStartOfCare, episode, currentMedNonOasisStartofCare);
                //    break;
                //case DisciplineTasks.OASISCTransferDischarge:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                //    break;
                //case DisciplineTasks.OASISCTransfer:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                //    break;
                //case DisciplineTasks.OASISCTransferPT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                //    break;
                //case DisciplineTasks.OASISCTransferOT:
                //    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                //    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    var snNote = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = pDScheduleRepository.AddVisitNote(snNote);
                    break;
                //case DisciplineTasks.LVNSupervisoryVisit:
                //case DisciplineTasks.HHAideSupervisoryVisit:
                //    var snNoteNonebillableNursing = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(snNoteNonebillableNursing);
                //    break;
                //case DisciplineTasks.PTEvaluation:
                //case DisciplineTasks.PTReEvaluation:
                //    var ptEval = new PDPatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(ptEval);
                //    break;
                //case DisciplineTasks.PTVisit:
                //case DisciplineTasks.PTDischarge:
                //case DisciplineTasks.PTAVisit:
                //case DisciplineTasks.PTMaintenance:
                //    var snNoteNonebillablePT = new PDPatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(snNoteNonebillablePT);
                //    break;
                //case DisciplineTasks.OTEvaluation:
                //case DisciplineTasks.OTReEvaluation:
                //    var otEval = new PDPatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(otEval);
                //    break;
                //case DisciplineTasks.OTDischarge:
                //case DisciplineTasks.OTVisit:
                //case DisciplineTasks.COTAVisit:
                //case DisciplineTasks.OTMaintenance:
                //    var snNoteNonebillableOT = new PDPatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(snNoteNonebillableOT);
                //    break;
                //case DisciplineTasks.STEvaluation:
                //case DisciplineTasks.STReEvaluation:
                //    var stEval = new PDPatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id,  NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(stEval);
                //    break;
                //case DisciplineTasks.STVisit:
                //case DisciplineTasks.STDischarge:
                //case DisciplineTasks.STMaintenance:
                //    var snNoteNonebillableST = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(snNoteNonebillableST);
                //    break;
                //case DisciplineTasks.MSWEvaluationAssessment:
                //case DisciplineTasks.MSWVisit:
                //case DisciplineTasks.MSWDischarge:
                //case DisciplineTasks.MSWAssessment:
                //case DisciplineTasks.MSWProgressNote:
                //    var snNoteNonebillableMSW = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(snNoteNonebillableMSW);
                //    break;
                //case DisciplineTasks.DriverOrTransportationNote:
                //    var driverOrTransportationNote = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id,  NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(driverOrTransportationNote);
                //    break;
                //case DisciplineTasks.DieticianVisit:
                //    var snNoteNonebillableDietician = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id,  NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(snNoteNonebillableDietician);
                //    break;
                //case DisciplineTasks.HHAideVisit:
                //    var hhAideVisit = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(hhAideVisit);
                //    break;
                //case DisciplineTasks.HHAideCarePlan:
                //    var hhAideCarePlan = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(hhAideCarePlan);
                //    break;
                //case DisciplineTasks.PASVisit:
                //    var pasVisit = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(pasVisit);
                //    break;
                //case DisciplineTasks.PASCarePlan:
                //    var pasCarePlan = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    result = pDScheduleRepository.AddVisitNote(pasCarePlan);
                //    break;
                //case DisciplineTasks.DischargeSummary:
                //    var physician = physicianRepository.GetPatientPhysicians(patient.Id, scheduleEvent.AgencyId).SingleOrDefault(p => p.Primary);
                //    var dischargeSummary = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    if (physician != null)
                //    {
                //        var questions = new List<NotesQuestion>();
                //        questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(physician.Id), Type = "DischargeSummary" });
                //        dischargeSummary.Note = questions.ToXml();
                //        result = pDScheduleRepository.AddVisitNote(dischargeSummary);
                //    }
                //    else
                //    {
                //        result = pDScheduleRepository.AddVisitNote(dischargeSummary);
                //    }
                //    break;

                //case DisciplineTasks.PhysicianOrder:
                //    var order = new PhysicianOrder { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, OrderDate = scheduleEvent.EventDate, Created = DateTime.Now, Text = "", Summary = "", Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                //    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                //    {
                //        order.PhysicianId = patient.PhysicianContacts[0].Id;
                //    }
                //    result = patientRepository.AddOrder(order);
                //    break;

                //case DisciplineTasks.HCFA485StandAlone:
                //    var planofCare = new PlanofCareStandAlone { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                //    planofCare.Questions = new List<Question>();
                //    planofCare.Data = planofCare.Questions.ToXml();
                //    result = planofCareRepository.AddStandAlone(planofCare);
                //    break;

                //case DisciplineTasks.NonOasisHCFA485:

                //    break;

                //case DisciplineTasks.HCFA485:

                //    break;

                //case DisciplineTasks.HCFA486:
                //case DisciplineTasks.PostHospitalizationOrder:
                //case DisciplineTasks.MedicaidPOC:

                //    break;
                //case DisciplineTasks.IncidentAccidentReport:
                //    var incidentReport = new Incident { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, IncidentDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                //    result = agencyRepository.AddIncident(incidentReport);
                //    break;
                //case DisciplineTasks.InfectionReport:
                //    var infectionReport = new Infection { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, InfectionDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                //    result = agencyRepository.AddInfection(infectionReport);
                //    break;
                case DisciplineTasks.SixtyDaySummary:
                    var sixtyDaySummary = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = pDScheduleRepository.AddVisitNote(sixtyDaySummary);
                    break;
                case DisciplineTasks.TransferSummary:
                case DisciplineTasks.CoordinationOfCare:
                    var transferSummary = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = pDScheduleRepository.AddVisitNote(transferSummary);
                    break;
                //case DisciplineTasks.CommunicationNote:
                //    var comNote = new CommunicationNote { Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, AgencyId = Current.AgencyId, UserId = scheduleEvent.UserId, Status = ((int)ScheduleStatus.NoteNotYetDue), Created = scheduleEvent.EventDate, Modified = DateTime.Now };
                //    result = patientRepository.AddCommunicationNote(comNote);
                //    break;
                //case DisciplineTasks.FaceToFaceEncounter:
                //    var faceToFaceEncounter = new FaceToFaceEncounter { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, RequestDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                //    if (patient.PhysicianContacts.Count > 0)
                //    {
                //        faceToFaceEncounter.PhysicianId = patient.PhysicianContacts[0].Id;
                //    }
                //    result = patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                //    break;
                case DisciplineTasks.UAPWoundCareVisit:
                case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    var uapNote = new PDPatientVisitNote { AgencyId = scheduleEvent.AgencyId, Id = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = pDScheduleRepository.AddVisitNote(uapNote);
                    break;
            }
            return result;
        }
    }
}
