﻿namespace Axxess.AgencyManagement.PD.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IPDScheduleService
    {
        bool AddSchedule(Guid agencyId, Guid patientId, Guid userId, int disciplineTasks, List<DateTime> days, int timeIn, int timeOut);
    }
}
