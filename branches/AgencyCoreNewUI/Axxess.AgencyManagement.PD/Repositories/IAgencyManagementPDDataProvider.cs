﻿namespace Axxess.AgencyManagement.PD.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public interface IAgencyManagementPDDataProvider
    {
       IPDScheduleRepository PDScheduleRepository { get; }
    }
}
