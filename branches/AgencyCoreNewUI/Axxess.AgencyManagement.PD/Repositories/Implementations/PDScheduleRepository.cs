﻿namespace Axxess.AgencyManagement.PD.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MongoDB;
    using MongoDB.Driver;
    using MongoDB.Bson;
using SubSonic.Repository;
    using Axxess.AgencyManagement.PD.Domain;
using Axxess.AgencyManagement.PD.Domain.Mongo;


   public class PDScheduleRepository : IPDScheduleRepository
    {

       private readonly MongoServer mongoServer;
       private readonly SimpleRepository mySqlDatabase;

       public PDScheduleRepository(MongoServer mongoServer, SimpleRepository mySqlDatabase)
       {
           this.mongoServer = mongoServer;
           this.mySqlDatabase = mySqlDatabase;
       }

       public bool InsertSchedule(PDScheduleEvent scheduleEvent)
       {
           var result = false;
           try
           {
               mySqlDatabase.Add<PDScheduleEvent>(scheduleEvent);
               return true;
           }
           catch (Exception ex)
           {
               return false;
           }
           return result;
       }

       public bool InsertNoteMD(Guid eventId)
       {
           var result = false;
           var database = mongoServer.GetDatabase("agencymanagement");
           if (database != null)
           {
               var notes = database.GetCollection<VisitNoteBson>("VisitNoteBsons");
               if (notes != null)
               {
                   notes.Insert(new VisitNoteBson { _id = eventId.ToString(), Note = string.Empty });
               }
           }
           return false;
       }

       public bool AddVisitNote(PDPatientVisitNote patientVisitNote)
       {
           try
           {
               mySqlDatabase.Add<PDPatientVisitNote>(patientVisitNote);
               return true;
           }
           catch (Exception ex)
           {
               return false;
           }
       }

       public bool RemoveModel<T>(Guid Id) where T : class, new()
       {
           var result = false;
           try
           {
               mySqlDatabase.Delete<T>(Id);
               result = true;
           }
           catch (Exception ex)
           {
               return false;
           }
           return result;
       }

    }
}
