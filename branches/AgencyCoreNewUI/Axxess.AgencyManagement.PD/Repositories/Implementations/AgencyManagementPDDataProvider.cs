﻿namespace Axxess.AgencyManagement.PD.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MongoDB.Driver;
    using Axxess.Core.Infrastructure;
    using SubSonic.Repository;

    public class AgencyManagementPDDataProvider : IAgencyManagementPDDataProvider
    {
        private readonly MongoServer mongoServer;
        private readonly SimpleRepository mySqlDatabase;
        public AgencyManagementPDDataProvider()
        {
            this.mongoServer = MongoServer.Create(CoreSettings.MongoDBServerPath);
            this.mySqlDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);


        }
        #region AgencyManagementPDDataProvider Members

        private IPDScheduleRepository pDScheduleRepository;
        public IPDScheduleRepository PDScheduleRepository
        {
            get
            {
                if (pDScheduleRepository == null)
                {
                    pDScheduleRepository = new PDScheduleRepository(this.mongoServer, this.mySqlDatabase);
                }
                return pDScheduleRepository;
            }
        }
        #endregion
    }
}
