﻿namespace Axxess.AgencyManagement.PD.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.PD.Domain;

   public interface IPDScheduleRepository
    {
       bool InsertSchedule(PDScheduleEvent scheduleEvent);
       bool InsertNoteMD(Guid eventId);
       bool AddVisitNote(PDPatientVisitNote patientVisitNote);
       bool RemoveModel<T>(Guid Id) where T : class, new();

    }
}
