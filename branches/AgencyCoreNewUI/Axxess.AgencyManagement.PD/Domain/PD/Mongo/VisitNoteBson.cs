﻿namespace Axxess.AgencyManagement.PD.Domain.Mongo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MongoDB.Bson.Serialization.Attributes;

    public class VisitNoteBson
    {
        [BsonId]
        public string _id { get; set; }
        public string Note { get; set; }
    }
}
