﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.PTEvaluation.ToString(), "Physical Therapy Evaluation" },
    { DisciplineTasks.PTReEvaluation.ToString(), "Physical Therapy ReEvaluation" },
    { DisciplineTasks.PTMaintenance.ToString(), "Physical Therapy Maintenance" }
     }; %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>Physical Therapy <%= Model.Type == "PTReEvaluation" ? "Re-" : string.Empty %>Evaluation<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT " +
        "<%= Model.Type == "PTEvaluation" ? "Evaluation" : ""%><%= Model.Type == "PTReEvaluation" ? "Re-Evaluation" : ""%><%= Model.Type == "PTMaintenance" ? "Maintenance Visit" : ""%>" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy <%= Model.Type == "PTReEvaluation" ? "Re-" : ""%>Evaluation%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EPhysician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3C/span%3E%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
    printview.addsection(
        printview.col(6,
            printview.span("%3Cstrong%3ETemp:%3C/strong%3E <%= data != null && data.ContainsKey("GenericTemp") && data["GenericTemp"].Answer.IsNotNullOrEmpty() ? data["GenericTemp"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EResp:%3C/strong%3E <%= data != null && data.ContainsKey("GenericResp") && data["GenericResp"].Answer.IsNotNullOrEmpty() ? data["GenericResp"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EPulse:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPulse") && data["GenericPulse"].Answer.IsNotNullOrEmpty() ? data["GenericPulse"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EWeight:%3C/strong%3E <%= data != null && data.ContainsKey("GenericWeight") && data["GenericWeight"].Answer.IsNotNullOrEmpty() ? data["GenericWeight"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EBP:%3C/strong%3E <%= data != null && data.ContainsKey("GenericBloodPressure") && data["GenericBloodPressure"].Answer.IsNotNullOrEmpty() ? data["GenericBloodPressure"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>/<%= data != null && data.ContainsKey("GenericBloodPressurePer") && data["GenericBloodPressurePer"].Answer.IsNotNullOrEmpty() ? data["GenericBloodPressurePer"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EBG:%3C/strong%3E <%= data != null && data.ContainsKey("GenericBloodSugar") && data["GenericBloodSugar"].Answer.IsNotNullOrEmpty() ? data["GenericBloodSugar"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>")) +
        printview.col(4,
            printview.span("Medical Diagnosis:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMedicalDiagnosis") && data["GenericMedicalDiagnosis"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalDiagnosis"].Answer.Clean() : string.Empty%>",0,1) +
            printview.checkbox("Onset", <%= data != null && data.ContainsKey("GenericMedicalDiagnosisOnset") && data["GenericMedicalDiagnosisOnset"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("MedicalDiagnosisDate") && data["MedicalDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["MedicalDiagnosisDate"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("PT Diagnosis:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPTDiagnosis") && data["GenericPTDiagnosis"].Answer.IsNotNullOrEmpty() ? data["GenericPTDiagnosis"].Answer.Clean() : string.Empty%>",0,1) +
            printview.checkbox("Onset", <%= data != null && data.ContainsKey("GenericPTDiagnosis") && data["GenericPTDiagnosis"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("PTDiagnosisDate") && data["PTDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["PTDiagnosisDate"].Answer.Clean() : string.Empty%>",0,1)) +
        printview.col(2,
            printview.span("Medical History:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalHistory"].Answer.Clean() : string.Empty%>",0,2)),
        "Vital Signs");
    printview.addsection(
        printview.col(6,
            printview.span("Speech:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentSpeech") && data["GenericPhysicalAssessmentSpeech"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentSpeech"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Vision:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentVision") && data["GenericPhysicalAssessmentVision"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentVision"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Hearing:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentHearing") && data["GenericPhysicalAssessmentHearing"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentHearing"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Skin:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentSkin") && data["GenericPhysicalAssessmentSkin"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentSkin"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Edema:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentEdema") && data["GenericPhysicalAssessmentEdema"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentEdema"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Muscle Tone:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentMuscleTone") && data["GenericPhysicalAssessmentMuscleTone"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentMuscleTone"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Coordination:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentCoordination") && data["GenericPhysicalAssessmentCoordination"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentCoordination"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Sensation:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentSensation") && data["GenericPhysicalAssessmentSensation"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentSensation"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Endurance:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPhysicalAssessmentEndurance") && data["GenericPhysicalAssessmentEndurance"].Answer.IsNotNullOrEmpty() ? data["GenericPhysicalAssessmentEndurance"].Answer.Clean() : string.Empty%>",0,1)) +
        printview.col(3,
            printview.span("&#160;") +
            printview.span("ROM",true) +
            printview.span("Strength",true)) +
        printview.col(6,
            printview.span("Part",true) +
            printview.span("Action",true) +
            printview.span("Right",true) +
            printview.span("Left",true) +
            printview.span("Right",true) +
            printview.span("Left",true) +
            printview.span("Shoulder",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionROMRight") && data["GenericShoulderFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionROMLeft") && data["GenericShoulderFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionStrengthRight") && data["GenericShoulderFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionStrengthLeft") && data["GenericShoulderFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionROMRight") && data["GenericShoulderExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionROMLeft") && data["GenericShoulderExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionStrengthRight") && data["GenericShoulderExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionStrengthLeft") && data["GenericShoulderExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Abduction") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionROMRight") && data["GenericShoulderAbductionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionROMLeft") && data["GenericShoulderAbductionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionStrengthRight") && data["GenericShoulderAbductionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionStrengthLeft") && data["GenericShoulderAbductionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Int Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotROMRight") && data["GenericShoulderIntRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotROMLeft") && data["GenericShoulderIntRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotStrengthRight") && data["GenericShoulderIntRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotStrengthLeft") && data["GenericShoulderIntRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Ext Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotROMRight") && data["GenericShoulderExtRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotROMLeft") && data["GenericShoulderExtRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotStrengthRight") && data["GenericShoulderExtRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotStrengthLeft") && data["GenericShoulderExtRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Elbow",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionROMRight") && data["GenericElbowFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionROMLeft") && data["GenericElbowFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionStrengthRight") && data["GenericElbowFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionStrengthLeft") && data["GenericElbowFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionROMRight") && data["GenericElbowExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionROMLeft") && data["GenericElbowExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionStrengthRight") && data["GenericElbowExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionStrengthLeft") && data["GenericElbowExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Finger",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionROMRight") && data["GenericFingerFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionROMLeft") && data["GenericFingerFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionStrengthRight") && data["GenericFingerFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionStrengthLeft") && data["GenericFingerFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionROMRight") && data["GenericFingerExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionROMLeft") && data["GenericFingerExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionStrengthRight") && data["GenericFingerExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionStrengthLeft") && data["GenericFingerExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Wrist",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionROMRight") && data["GenericWristFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionROMLeft") && data["GenericWristFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionStrengthRight") && data["GenericWristFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionStrengthLeft") && data["GenericWristFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionROMRight") && data["GenericWristExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionROMLeft") && data["GenericWristExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionStrengthRight") && data["GenericWristExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionStrengthLeft") && data["GenericWristExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Hip",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionROMRight") && data["GenericHipFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionROMLeft") && data["GenericHipFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionStrengthRight") && data["GenericHipFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionStrengthLeft") && data["GenericHipFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionROMRight") && data["GenericHipExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionROMLeft") && data["GenericHipExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionStrengthRight") && data["GenericHipExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionStrengthLeft") && data["GenericHipExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Abduction") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionROMRight") && data["GenericHipAbductionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionROMLeft") && data["GenericHipAbductionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionStrengthRight") && data["GenericHipAbductionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionStrengthLeft") && data["GenericHipAbductionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Int Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotROMRight") && data["GenericHipIntRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotROMLeft") && data["GenericHipIntRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotStrengthRight") && data["GenericHipIntRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotStrengthLeft") && data["GenericHipIntRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Ext Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotROMRight") && data["GenericHipExtRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotROMLeft") && data["GenericHipExtRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotStrengthRight") && data["GenericHipExtRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotStrengthLeft") && data["GenericHipExtRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Knee",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionROMRight") && data["GenericKneeFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionROMLeft") && data["GenericKneeFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionStrengthRight") && data["GenericKneeFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionStrengthLeft") && data["GenericKneeFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionROMRight") && data["GenericKneeExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionROMLeft") && data["GenericKneeExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionStrengthRight") && data["GenericKneeExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionStrengthLeft") && data["GenericKneeExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Ankle",true) +
            printview.span("Plantarflexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionROMRight") && data["GenericAnklePlantFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionROMLeft") && data["GenericAnklePlantFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionStrengthRight") && data["GenericAnklePlantFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionStrengthLeft") && data["GenericAnklePlantFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Dorsiflexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionROMRight") && data["GenericAnkleFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionROMLeft") && data["GenericAnkleFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionStrengthRight") && data["GenericAnkleFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionStrengthLeft") && data["GenericAnkleFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Trunk",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionROMRight") && data["GenericTrunkFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionROMLeft") && data["GenericTrunkFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionStrengthRight") && data["GenericTrunkFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionStrengthLeft") && data["GenericTrunkFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Rotation") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationROMRight") && data["GenericTrunkRotationROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationROMLeft") && data["GenericTrunkRotationROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationStrengthRight") && data["GenericTrunkRotationStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationStrengthLeft") && data["GenericTrunkRotationStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionROMRight") && data["GenericTrunkExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionROMLeft") && data["GenericTrunkExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionStrengthRight") && data["GenericTrunkExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionStrengthLeft") && data["GenericTrunkExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Neck",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionROMRight") && data["GenericNeckFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionROMLeft") && data["GenericNeckFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionStrengthRight") && data["GenericNeckFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionStrengthLeft") && data["GenericNeckFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionROMRight") && data["GenericNeckExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionROMLeft") && data["GenericNeckExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionStrengthRight") && data["GenericNeckExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionStrengthLeft") && data["GenericNeckExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Lat Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionROMRight") && data["GenericNeckLatFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionROMLeft") && data["GenericNeckLatFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionStrengthRight") && data["GenericNeckLatFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionStrengthLeft") && data["GenericNeckLatFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Long  Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionROMRight") && data["GenericNeckLongFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionROMLeft") && data["GenericNeckLongFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionStrengthRight") && data["GenericNeckLongFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionStrengthLeft") && data["GenericNeckLongFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Rotation") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationROMRight") && data["GenericNeckRotationROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationROMLeft") && data["GenericNeckRotationROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationStrengthRight") && data["GenericNeckRotationStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationStrengthLeft") && data["GenericNeckRotationStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationStrengthLeft"].Answer.Clean() : string.Empty%>",0,1)),
        "Physical Assessment");
    printview.addsubsection(
        printview.col(2,
            printview.span("Orientation:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMentalAssessmentOrientation") && data["GenericMentalAssessmentOrientation"].Answer.IsNotNullOrEmpty() ? data["GenericMentalAssessmentOrientation"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("LOC:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMentalAssessmentLOC") && data["GenericMentalAssessmentLOC"].Answer.IsNotNullOrEmpty() ? data["GenericMentalAssessmentLOC"].Answer.Clean() : string.Empty%>",0,1)),
        "Mental Assessment",2);
    printview.addsubsection(
        printview.col(2,
            printview.span("Pain level:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericIntensityOfPain") && data["GenericIntensityOfPain"].Answer.IsNotNullOrEmpty() ? data["GenericIntensityOfPain"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Location:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainAssessmentLocation") && data["GenericPainAssessmentLocation"].Answer.IsNotNullOrEmpty() ? data["GenericPainAssessmentLocation"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Increased by:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainAssessmentIncreasedBy") && data["GenericPainAssessmentIncreasedBy"].Answer.IsNotNullOrEmpty() ? data["GenericPainAssessmentIncreasedBy"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Relieved by:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainAssessmentRelievedBy") && data["GenericPainAssessmentRelievedBy"].Answer.IsNotNullOrEmpty() ? data["GenericPainAssessmentRelievedBy"].Answer.Clean() : string.Empty%>",0,1)),
        "Pain Assessment");
    printview.addsubsection(
        printview.col(2,
            printview.span("Available:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDMEAvailable") && data["GenericDMEAvailable"].Answer.IsNotNullOrEmpty() ? data["GenericDMEAvailable"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDMENeeds") && data["GenericDMENeeds"].Answer.IsNotNullOrEmpty() ? data["GenericDMENeeds"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDMESuggestion") && data["GenericDMESuggestion"].Answer.IsNotNullOrEmpty() ? data["GenericDMESuggestion"].Answer.Clean() : string.Empty%>",0,1)),
        "DME");
    printview.addsubsection(
        printview.col(3,
            printview.span("Level",true) +
            printview.checkbox("One", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationLevel") && data["GenericHomeSafetyEvaluationLevel"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Multiple", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationLevel") && data["GenericHomeSafetyEvaluationLevel"].Answer == "0" ? "true" : "false"%>) +
            printview.span("Stairs",true) +
            printview.checkbox("Yes", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationStairs") && data["GenericHomeSafetyEvaluationStairs"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("No", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationStairs") && data["GenericHomeSafetyEvaluationStairs"].Answer == "0" ? "true" : "false"%>)) +
        printview.col(4,
            printview.span("Lives",true) +
            printview.checkbox("Alone", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationLives") && data["GenericHomeSafetyEvaluationLives"].Answer == "2" ? "true" : "false"%>) +
            printview.checkbox("Family", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationLives") && data["GenericHomeSafetyEvaluationLives"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Friends", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationLives") && data["GenericHomeSafetyEvaluationLives"].Answer == "0" ? "true" : "false"%>)) +
        printview.col(3,
            printview.span("Support",true) +
            printview.checkbox("Retirement", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationSupport") && data["GenericHomeSafetyEvaluationSupport"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Assisted Living", <%= data != null && data.ContainsKey("GenericHomeSafetyEvaluationSupport") && data["GenericHomeSafetyEvaluationSupport"].Answer == "0" ? "true" : "false"%>)),
        "Home Safety Evaluation");
    printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistive Device", true) +
            printview.span("% Assist", true) +
            printview.col(2,
                printview.span("Rolling", true) +
                printview.col(4,
                    printview.checkbox("L", <%= data != null && data.ContainsKey("GenericBedMobilityRolling") && data["GenericBedMobilityRolling"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("R", <%= data != null && data.ContainsKey("GenericBedMobilityRolling") && data["GenericBedMobilityRolling"].Answer.Split(',').Contains("2") ? "true" : "false"%>))) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityRollingAssistiveDevice") && data["GenericBedMobilityRollingAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityRollingAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityRollingAssist") && data["GenericBedMobilityRollingAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityRollingAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Sit Stand Sit", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySitStandSitAssistiveDevice") && data["GenericBedMobilitySitStandSitAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySitStandSitAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySitStandSitAssist") && data["GenericBedMobilitySitStandSitAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySitStandSitAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Sup to Sit", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySupToSitAssistiveDevice") && data["GenericBedMobilitySupToSitAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySupToSitAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySupToSitAssist") && data["GenericBedMobilitySupToSitAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySupToSitAssist"].Answer.Clean() + "%" : string.Empty%>",0,1)),
        "Bed Mobility");
    printview.addsubsection(
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitLevelAssist") && data["GenericGaitLevelAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitLevelAssist"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>x<%= data != null && data.ContainsKey("GenericGaitLevelFeet") && data["GenericGaitLevelFeet"].Answer.IsNotNullOrEmpty() ? data["GenericGaitLevelFeet"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> feet") +
            printview.span("Unlevel",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitUnLevelAssist") && data["GenericGaitUnLevelAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitUnLevelAssist"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>x<%= data != null && data.ContainsKey("GenericGaitUnLevelFeet") && data["GenericGaitUnLevelFeet"].Answer.IsNotNullOrEmpty() ? data["GenericGaitUnLevelFeet"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> feet") +
            printview.span("Step/Stair",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitStepStairAssist") && data["GenericGaitStepStairAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitStepStairAssist"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>x<%= data != null && data.ContainsKey("GenericGaitStepStairFeet") && data["GenericGaitStepStairFeet"].Answer.IsNotNullOrEmpty() ? data["GenericGaitStepStairFeet"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%> feet") +
            printview.span("Comments",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitComment") && data["GenericGaitComment"].Answer.IsNotNullOrEmpty() ? data["GenericGaitComment"].Answer.Clean() : string.Empty%>",0,2)),
        "Gait");
    printview.addsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistive Device", true) +
            printview.span("% Assist", true) +
            printview.span("Bed-Chair", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferBedChairAssistiveDevice") && data["GenericTransferBedChairAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferBedChairAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferBedChairAssist") && data["GenericTransferBedChairAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferBedChairAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Chair-Bed", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairBedAssistiveDevice") && data["GenericTransferChairBedAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairBedAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairBedAssist") && data["GenericTransferChairBedAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairBedAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Chair to W/C", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairToWCAssistiveDevice") && data["GenericTransferChairToWCAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairToWCAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairToWCAssist") && data["GenericTransferChairToWCAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairToWCAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Toilet or BSC", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferToiletOrBSCAssistiveDevice") && data["GenericTransferToiletOrBSCAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferToiletOrBSCAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferToiletOrBSCAssist") && data["GenericTransferToiletOrBSCAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferToiletOrBSCAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Car/Van", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferCanVanAssistiveDevice") && data["GenericTransferCanVanAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferCanVanAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferCanVanAssist") && data["GenericTransferCanVanAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferCanVanAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Tub/Shower", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferTubShowerAssistiveDevice") && data["GenericTransferTubShowerAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferTubShowerAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferTubShowerAssist") && data["GenericTransferTubShowerAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferTubShowerAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.col(3,
                printview.span("Sitting Balance", true) +
                printview.checkbox("Supported", <%= data != null && data.ContainsKey("GenericTransferSittingBalance") && data["GenericTransferSittingBalance"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Unsupported", <%= data != null && data.ContainsKey("GenericTransferSittingBalance") && data["GenericTransferSittingBalance"].Answer.Split(',').Contains("2") ? "true" : "false"%>)) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferSittingBalanceStatic") && data["GenericTransferSittingBalanceStatic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferSittingBalanceStatic"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferSittingBalanceDynamic") && data["GenericTransferSittingBalanceDynamic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferSittingBalanceDynamic"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.col(3,
                printview.span("Stand Balance", true) +
                printview.checkbox("Supported", <%= data != null && data.ContainsKey("GenericTransferStandBalance") && data["GenericTransferStandBalance"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Unsupported", <%= data != null && data.ContainsKey("GenericTransferStandBalance") && data["GenericTransferStandBalance"].Answer.Split(',').Contains("2") ? "true" : "false"%>)) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferStandBalanceStatic") && data["GenericTransferStandBalanceStatic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferStandBalanceStatic"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferStandBalanceDynamic") && data["GenericTransferStandBalanceDynamic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferStandBalanceDynamic"].Answer.Clean() + "%" : string.Empty%>",0,1)),
        "Transfer");
    printview.addsubsection(
        printview.col(2,
            printview.span("Assistive Device", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWBSAssistiveDevice") && data["GenericWBSAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericWBSAssistiveDevice"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Description", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWBSDescription") && data["GenericWBSDescription"].Answer.IsNotNullOrEmpty() ? data["GenericWBSDescription"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Posture", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWBSPosture") && data["GenericWBSPosture"].Answer.IsNotNullOrEmpty() ? data["GenericWBSPosture"].Answer.Clean() : string.Empty%>",0,1)),
        "WBS",3);
    printview.addsubsection(
        printview.col(2,
            printview.span("Level", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityLevel") && data["GenericWCMobilityLevel"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityLevel"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Ramp", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityRamp") && data["GenericWCMobilityRamp"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityRamp"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Maneuver", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityManeuver") && data["GenericWCMobilityManeuver"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityManeuver"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("ADL", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityADL") && data["GenericWCMobilityADL"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityADL"].Answer.Clean() + "%" : string.Empty%>",0,1)),
        "W/C Mobility");
    printview.addsubsection(printview.span("<%= data != null && data.ContainsKey("GenericAssessmentComment") && data["GenericAssessmentComment"].Answer.IsNotNullOrEmpty() ? data["GenericAssessmentComment"].Answer.Clean() : string.Empty%>",0,3),"Assessment");
    printview.addsubsection(
        printview.col(2,
            printview.checkbox("B1 Evaluation", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("B2 Thera Ex", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("B3 Transfer Training", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("B4 Home Program", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("B5 Gait Training", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("B6 Chest PT", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("B7 Ultrasound", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("B8 Electrother", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("B9 Prosthetic Training", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("B10 Muscle Re-ed", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
            printview.checkbox("B11 Muscle Re-ed", <%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
            printview.span("%3Cstrong%3EOther:%3C/strong%3E <%= data != null && data.ContainsKey("GenericTreatmentCodesOther") && data["GenericTreatmentCodesOther"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentCodesOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>")),
        "Treatment Codes",2);
    printview.addsubsection(
        printview.col(2,
            printview.checkbox("Thera Ex", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Bed Mobility Training", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Transfer Training", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Balance Training", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Gait Training", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("HEP", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Electrotherapy", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Ultrasound", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Prosthetic Training", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Manual Therapy", <%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
            printview.span("%3Cstrong%3EOther:%3C/strong%3E <%= data != null && data.ContainsKey("GenericTreatmentCodesOther") && data["GenericTreatmentCodesOther"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentCodesOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>")),
        "Treatment Plan");
    printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal1") && data["GenericShortTermGoal1"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal1"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal1TimeFrame") && data["GenericShortTermGoal1TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal1TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal2") && data["GenericShortTermGoal2"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal2"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal2TimeFrame") && data["GenericShortTermGoal2TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal2TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal3") && data["GenericShortTermGoal3"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal3"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal3TimeFrame") && data["GenericShortTermGoal3TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal3TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal4") && data["GenericShortTermGoal4"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal4"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal4TimeFrame") && data["GenericShortTermGoal4TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal4TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal5") && data["GenericShortTermGoal5"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal5"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal5TimeFrame") && data["GenericShortTermGoal5TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal5TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E",
        "Short Term Goals");
    printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal1") && data["GenericLongTermGoal1"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal1"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal1TimeFrame") && data["GenericLongTermGoal1TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal1TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal2") && data["GenericLongTermGoal2"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal2"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal2TimeFrame") && data["GenericLongTermGoal2TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal2TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal3") && data["GenericLongTermGoal3"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal3"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal3TimeFrame") && data["GenericLongTermGoal3TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal3TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal4") && data["GenericLongTermGoal4"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal4"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal4TimeFrame") && data["GenericLongTermGoal4TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal4TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal5") && data["GenericLongTermGoal5"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal5"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal5TimeFrame") && data["GenericLongTermGoal5TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal5TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E",
        "Long Term Goals");
    printview.addsection(
        printview.col(6,
            printview.checkbox("OT", <%= data != null && data.ContainsKey("GenericDisciplineRecommendation") && data["GenericDisciplineRecommendation"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("MSW", <%= data != null && data.ContainsKey("GenericDisciplineRecommendation") && data["GenericDisciplineRecommendation"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("ST", <%= data != null && data.ContainsKey("GenericDisciplineRecommendation") && data["GenericDisciplineRecommendation"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Podiatrist", <%= data != null && data.ContainsKey("GenericDisciplineRecommendation") && data["GenericDisciplineRecommendation"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.span("Other", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDisciplineRecommendationOther") && data["GenericDisciplineRecommendationOther"].Answer.IsNotNullOrEmpty() ? data["GenericDisciplineRecommendationOther"].Answer.Clean() : string.Empty%>",0,1)) +
        printview.col(4,
            printview.span("Reason", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDisciplineRecommendationReason") && data["GenericDisciplineRecommendationReason"].Answer.IsNotNullOrEmpty() ? data["GenericDisciplineRecommendationReason"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Frequency", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericFrequency") && data["GenericFrequency"].Answer.IsNotNullOrEmpty() ? data["GenericFrequency"].Answer.Clean() + " X wk" : string.Empty%><%= data != null && data.ContainsKey("GenericFrequencyFor") && data["GenericFrequencyFor"].Answer.IsNotNullOrEmpty() ? " for " + data["GenericFrequencyFor"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Rehab Potential", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer.IsNotNullOrEmpty() ? data["GenericRehabPotential"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Prognosis", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPrognosis") && data["GenericPrognosis"].Answer.IsNotNullOrEmpty() ? data["GenericPrognosis"].Answer.Clean() : string.Empty%>",0,1)),
        "Other Discipline Recommendation");
    printview.addsubsection(printview.span("<%= data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer.IsNotNullOrEmpty() ? data["GenericCareCoordination"].Answer.Clean() : string.Empty%>",0,3),"Care Coordination");
    printview.addsubsection(printview.span("<%= data != null && data.ContainsKey("GenericSkilledCareProvided") && data["GenericSkilledCareProvided"].Answer.IsNotNullOrEmpty() ? data["GenericSkilledCareProvided"].Answer.Clean() : string.Empty%>",0,3),"Skilled Care Provided This Visit");
    <%  }).Render(); %>
</html>
