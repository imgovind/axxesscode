﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<FaceToFaceEncounter>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
     <title><%= Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Face to Face Encounter<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Patient/physface2face.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") : "") : "").Clean() %>",
            "patientname": "<%= Model != null && Model.Patient != null ?
                (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : "") +
                (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : "") +
                (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br/>" : "<br/>") : "" %>",
            "patient": "<%=(Model != null && Model.Patient != null ? (Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.ToTitleCase() + "<br/>" : "") + (Model.Patient.AddressLine2.IsNotNullOrEmpty() ? Model.Patient.AddressLine2.ToTitleCase() + "<br/>" : "") + (Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.ToTitleCase() + ", " : "") + (Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode.ToTitleCase() + "  " : "") + (Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode + "<br/>" : "") + (Model.Patient.PhoneHome.IsNotNullOrEmpty() ? Model.Patient.PhoneHome.ToPhone() + "<br/>" : "") + (Model.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + Model.Patient.MedicareNumber : "") : "")%>",
            "physicianname": "<%= Model != null && Model.Physician != null && Model.Physician.DisplayName.IsNotNullOrEmpty() ? Model.Physician.DisplayName : string.Empty%>",
            "physician": "<%= (Model != null && Model.Physician != null ? (Model.Physician.AddressLine1.IsNotNullOrEmpty() ? Model.Physician.AddressLine1.ToTitleCase() : "") + (Model.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.Physician.AddressLine2.ToTitleCase() + "<br/>" : "<br/>") + (Model.Physician.AddressCity.IsNotNullOrEmpty() ? Model.Physician.AddressCity.ToTitleCase() + ", " : "") + (Model.Physician.AddressStateCode.IsNotNullOrEmpty() ? Model.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (Model.Physician.AddressZipCode.IsNotNullOrEmpty() ? Model.Physician.AddressZipCode + "<br/>" : "") + (Model.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + Model.Physician.PhoneWork.ToPhone() : "") + (Model.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + Model.Physician.FaxNumber.ToPhone() + "<br/>" : "<br/>") + (Model.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + Model.Physician.NPI : "") : "")%>",
            "mr": "<%= Model != null && Model.Patient != null && Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? Model.Patient.PatientIdNumber : string.Empty%>",
            "order": "<%= Model != null && Model.OrderNumber != null ? Model.OrderNumber.ToString() : string.Empty %>",
            "dob": "<%= Model != null && Model.Patient != null && Model.Patient.DOBFormatted.IsNotNullOrEmpty() ? Model.Patient.DOBFormatted : string.Empty%>",
            "socdate": "<%= Model != null && Model.Patient != null && Model.Patient.StartofCareDate.IsValid() && Model.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? Model.Patient.StartOfCareDateFormatted : string.Empty%>",
            "episode": "<%= (Model != null && Model.EpisodeStartDate.IsValidDate() ? Model.EpisodeStartDate + "—" : string.Empty) + (Model != null && Model.EpisodeEndDate.IsValidDate() ? Model.EpisodeEndDate : string.Empty)%>",
            "physsign": "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>",
            "physsigndate": "<%= Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty %>"
        };
        PdfPrint.BuildSections(<%= Model.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>