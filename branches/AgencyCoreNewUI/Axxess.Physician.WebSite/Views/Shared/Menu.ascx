﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
    $("body").AcoreDesktop();
    Acore.AddMenu({ Name: "View", Id: "view", IconX: "43", IconY: "87" });

    Acore.AddWindow({ Name: "Orders Pending Review", Id: "listorders", Url: "Order/List", Menu: "view" });
    Acore.AddWindow({ Name: "Face-To-Face Encounters", Id: "listfacetoface", Url: "Order/FaceToFaceList", Menu: "view" });
    Acore.AddWindow({ Name: "Orders Completed", Id: "listpastorders", Url: "Order/CompleteList", Menu: "view" });
    Acore.AddWindow({ Name: "Face-To-Face Encounter", Id: "faceToFace", Url: "Order/FaceToFaceEncounter" });

    $("ul#mainmenu").superfish();

    Acore.Open("listorders");
</script>

