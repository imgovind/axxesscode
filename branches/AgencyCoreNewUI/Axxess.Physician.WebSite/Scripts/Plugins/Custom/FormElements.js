(function($) {
    $.extend($.fn, {
        Buttons: function(Buttons) {
            return this.each(function() {
                var $buttons = $(this);
                $buttons.addClass("buttons").append(
                    $("<ul/>")
                );
                $.each(Buttons, function() {
                    var ClickFunction = this.Click;
                    $("ul", $buttons).append(
                        $("<li/>").append(
                            $("<a/>", { "text": this.Text }).click(function() {
                                if (typeof ClickFunction == "function") {
                                    try {
                                        ClickFunction();
                                    } catch (e) {
                                        U.Growl(U.MessageErrorJS(true), "error")
                                        console.log("Error on button click, exception returned: " + e.toString());
                                    }
                                }
                                return false
                            })
                        )
                    )
                })
            })
        },
        DeselectableRadio: function() {
            return this.each(function() {
                var $radio = $(this);
                $radio.mouseup(function() {
                    if ($(this).prop("checked")) $(this).addClass("deselect")
                });
                $radio.click(function() {
                    if ($(this).hasClass("deselect")) $(this).removeClass("deselect").prop("checked", false).change();
                    else return true
                });
            })
        }
    })
})(jQuery);