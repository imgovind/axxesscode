﻿var UserInterface = {
    CloseWindow: function(window) {
        $("#window_" + window).Close();
    },
    ShowEditFaceToFaceEncounter: function(agencyId, episodeId, patientId, orderId) {
        Acore.Open("faceToFace", 'Order/FaceToFaceEncounter', Order.InitFaceToFaceEncounter, { agencyId: agencyId, episodeId: episodeId, patientId: patientId, id: orderId })
    }
}
