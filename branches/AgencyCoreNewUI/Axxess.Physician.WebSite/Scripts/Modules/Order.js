﻿var Order = {
    Load: function(groupby) {
        $("#orderListContent").empty().addClass("loading").load('Order/ListView', { groupName: groupby }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.Growl('Pending Orders List could not be grouped. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#orderListContent").removeClass("loading");
        });
    },
    Rebind: function() {
        U.RebindTGrid($('#orderGrid'));
    },
    RebindFaceToFace: function() {
        U.RebindTGrid($('#List_FaceToFace'));
    },
    Update: function(action, agencyId, episodeId, patientId, orderId, orderType) {
        var reason = "";
        if (action == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.PostUrl("/Order/Update", { action: action, agencyId: agencyId, episodeId: episodeId, patientId: patientId, orderId: orderId, orderType: orderType, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        $("#window_ModalWindow").Close();
                        U.Growl(result.errorMessage, "success");
                        Order.Rebind();
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Order/Update", { action: action, agencyId: agencyId, episodeId: episodeId, patientId: patientId, orderId: orderId, orderType: orderType, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    $("#window_ModalWindow").Close();
                    U.Growl(result.errorMessage, "success");
                    Order.Rebind();
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    InitFaceToFaceEncounter: function(responseText, textStatus, XMLHttpRequest, Element) {
        U.ShowIfChecked($("#FaceToFace_Services7"), $("#FaceToFace_ServicesOther"));
        U.ShowIfOtherChecked($("#FaceToFace_Services7"), $("#FaceToFace_ServicesOther"));
        $(".complete", Element).click(function() {
            $("#Edit_FaceToFace_Status").val("135");
            $(this).closest("form").submit();
        });
        $("#editFaceToFaceForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            Order.RebindFaceToFace();
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow("faceToFace");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        })
    }
}