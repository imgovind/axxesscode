﻿namespace Axxess.Api
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    public class AbilityNetworkAgent : BaseAgent<IAbilityNetworkService>
    {
        #region Overrides

        public override string ToString()
        {
            return "AbilityNetworkService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Grouper Methods

        public string GetHiqhRequest(string hic, string lastName, string firstInitial, DateTime dateOfBirth, string sex, string providerId)
        {
            string response = string.Empty;
            BaseAgent<IAbilityNetworkService>.Call(a => response = a.GetHiqhRequest(hic, lastName, firstInitial, dateOfBirth, sex, providerId), this.ToString());
            return response;
        }

        public bool ChangePasswordRequest(string userId, string oldPassword, string newPassword)
        {
            bool response = false;
            BaseAgent<IAbilityNetworkService>.Call(a => response = a.GetPasswordChangeRequest(userId, oldPassword, newPassword), this.ToString());
            return response;
        }

        #endregion
    }
}
