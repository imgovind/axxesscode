﻿<html>
    <head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <style type="text/css" media="screen">
		    p {
			    margin: 0 0 10px 0;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
			    font-family: arial, sans-serif;
			    font-size: 13px;
		    }

		    body, div, td, th, textarea, input, h2, h3 {
			    font-family: arial, sans-serif;
			    font-size: 12px;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
		    }
		    ol, ul {
 	            list-style: none;
 	            margin: 0;
            }
	    </style>
	</head>
	<body>
<p><strong>Initial Welcome Letter</strong><br />
Thank you for choosing Axxess, the most innovative, comprehensive and user friendly software platform available in the home health industry today.  We are confident you and your staff will be very pleased with our AgencyCore application.  The information included in this email will get us started on this exciting journey together.  Let us begin by saying, on behalf of everyone at Axxess, WELCOME and we are glad you are now part of our Axxess family.  Now let’s get started!</p>
<p><strong>What can you expect from us?</strong>
<ul>
<li>1.	First and foremost, the Axxess team will set up and grant you system access within 2 business days of receiving your signed agreement.  Once your agency is set up, an email notification from Axxess will be sent to the email account provided with the agreement.  This email will contain a link and instructions on how to complete your account set up.  Please check your junk mail folder if you do not receive the setup email.  You will be required to create a password to complete the registration process.  Once a successful password has been created, the system will bring you into AgencyCore.</li>

<li>2.	If you bill Medicare, we will complete the EDI provider authorization for your fiscal intermediary once you have provided us with your Medicare provider number. We will track this process to ensure that enrollment occurs.  Electronic billing can begin after the authorization is processed. Our Implementation team will assist you in setting up other Insurance providers that you bill.</li>

<li>3.	We want you to know you can rely on us through this entire process. To make this transition as smooth as possible, you will be assigned one of our very own Axxess Implementation Specialists. This Implementation Specialist will be a single point of contact to assist you through the transition process. You will be receiving an introductory call from your assigned Implementation Specialist within 3 business days from receiving your signed agreement. We look forward to working with you!</li>
</ul></p>
<p><strong>What do we need from you?</strong><br />
Now let’s talk about what we need you to do at this point. There are 3 attachments with this email. One of those is a Patient Data Load excel spreadsheet. We will need this spreadsheet completed by you or you may submit your own excel spreadsheet with the information contained in the Patient Data Load spreadsheet. This will allow the Axxess team to load your current patients into AgencyCore. In some cases, our implementation specialists will help you pull that information directly from your current software vendor if you provide us with the login details. We have successfully switched/transitioned customers over from other software and have the expertise to assist you too. 
In addition, we need you to identify a staff member to add additional users into AgencyCore. This needs to be someone who has access to employee demographic information and a current list of staff emails. At this point, if you haven’t already, you need to identify if the staff will be using company or personal emails for their log-in to AgencyCore. 
Once all of these steps are completed, we are well on our way!</p>

<p><strong>How may we help you?</strong><br />
We will be with you through every step of the way. To get you started, we have included 2 additional attachments with this email. The first one is the Agency Implementation Checklist. This checklist will guide you through your transition process by identifying key information to gather and address during the course of your AgencyCore implementation.
The last attachment with this email is a quick guide titled Creating a New User. This quick guide will be used by the staff member you identify to add additional users into AgencyCore.  It will walk him/her through setting up your staff as users in the system. Once a staff member is set up as a user, the user will receive an automatic email notification, to the email designated in the user set up, with a link to complete their registration process and log into AgencyCore. 
Ok, let us remind you once more, WE ARE HERE FOR YOU! Our philosophy, at Axxess, is to focus on our customers at all times. We have several ways we can assist you, under our Help tab you will find our online Training Manual; Free Webinars, a user discussion board, support page, and after hours support.  </p>
<p>In addition to the help available online you can call us at any time with any question. We have provided additional information below on how we can be reached:</p>
<p><strong>Support Information</strong><br />
Monday-Friday from 8am - 7pm CST <br />
Toll Free:  1-866-795-5990<br />
Online: <a href="http://www.axxess.com/contact">www.axxessconsult.com/contact</a><br />
Email: <a href="mailto:support@axxessconsult.com">support@axxessconsult.com</a> <br />
Fax: 214.575.7722<br />
Welcome to the AXXESS family!</p>

		<p style="font-family: arial, sans-serif; font-size: 11px;">This is an automated e-mail, please do not reply.</p>
		<p style="font-family: arial, sans-serif; font-size: 11px;">This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>