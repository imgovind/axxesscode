﻿namespace Axxess.AgencyManagement.SupportSite
{
    using System;
    using System.Web;

    using Axxess.AgencyManagement.SupportApp;

    public class MvcApplication : AxxessSupportApplication
    {
        protected void Session_Start(Object sender, EventArgs e)
        {
            Session["init"] = 0;
        }
    }
}