﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Axxess.Membership.Domain.DdeCredential>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "DdeCredential", FormMethod.Post, new { @id = "EditDdeCredential_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditDdeCredential_Id" })%>
    <fieldset>
        <legend>DDE Credential</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditDdeCredential_UserId">User Id</label>
                <div class="fr"><%= Model.UserId %></div>
            </div>
            <div class="row">
                <label for="EditDdeCredential_Password">Current Password</label>
                <div class="fr"><%=Html.TextBox("Password", Model.Password, new { @id = "EditDdeCredential_Password", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditDdeCredential_SuggestedPassword">Suggested Password</label>
                <div class="fr"><%= RandomPasswordGenerator.Generate() %></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsDeprecated", Model.IsDeprecated, new { @id = "EditDdeCredential_IsDeprecated" })%>
                        <label for="EditDdeCredential_IsDeprecated">Is Deprecated?</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>