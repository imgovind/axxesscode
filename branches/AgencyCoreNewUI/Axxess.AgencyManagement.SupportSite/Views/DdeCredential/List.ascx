﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Axxess.Membership.Domain.DdeCredential>().Name("ListAbility_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(l => l.UserId).Title("User Id").Sortable(true).Width(20);
            columns.Bound(l => l.Password).Sortable(true).Width(25);
            columns.Bound(l => l.IsDeprecated).Sortable(false).Width(8);
            columns.Bound(l => l.CreatedFormatted).Title("Created").Width(6);
            columns.Bound(l => l.Id).Title("Action").Sortable(false).Width(10).ClientTemplate("<a class=\"link\" onclick=\"DdeCredential.Edit('<#=Id#>');return false\">Edit</a> | <a class=\"link\" onclick=\"DdeCredential.Logs('<#=Id#>');return false\">Logs</a>");
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "DdeCredential")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>