﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Create", "Login", FormMethod.Post, new { @id = "NewLogin_Form" })) { %>
    <fieldset>
        <legend>Login Information</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewLogin_EmailAddress" class="fl strong">E-mail Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewLogin_EmailAddress", @class = "email required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewLogin_DisplayName" class="fl strong">Display Name</label>
                <div class="fr"><%=Html.TextBox("DisplayName", "", new { @id = "NewLogin_DisplayName", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                
                <div class="fr"></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessAdmin", false, new { @id = "NewLogin_IsAxxessAdmin" })%>
                        <label for="NewLogin_IsAxxessAdmin">Axxess Admin</label>
                    </div>
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessSupport", false, new { @id = "NewLogin_IsAxxessSupport" })%>
                        <label for="NewLogin_IsAxxessSupport">Axxess Support</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>