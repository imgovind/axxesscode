﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("System/Create", "Message", FormMethod.Post, new { @id = "NewSystemMessage_Form" })) { %>
    <fieldset>
        <legend>System Message</legend>
        <div class="wide column">
            <div class="narrow row">
                <label for="NewSystemMessage_Subject" class="fl strong">Subject</label>
                <div class="fr"><input type="text" id="NewSystemMessage_Subject" name="Subject" maxlength="150" class="longest" /></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Created By</label>
                <div class="fr"><%= Current.DisplayName %></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Created On</label>
                <div class="fr"><%= DateTime.Now.ToShortDateString() %></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Agency</label>
                <div class="fr"><%=Html.Agencies("AgencyId", "0", new { @id = "NewSystemMessage_Agency" }, true)%></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Agency Roles</label>
                <div class="fr"><%=Html.AgencyRoles("AgencyRoleId", new { @id = "NewSystemMessage_AgencyRoleId" })%></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Credentials</label>
                <div class="fr"><%=Html.TitleTypes("TitleType", new { @id = "NewSystemMessage_TitleType" })%></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">State:</label>
                <div class="fr"><%=Html.AgencyStates("AgencyStateCode", new { @id = "NewSystemMessage_AgencyStateCode" })%></div>
            </div>
            <div class="row wysiwyg"><textarea id="NewSystemMessage_Body" name="Body" class="tallest"></textarea></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="send">Send</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>