﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Dashboard/Create", "Message", FormMethod.Post, new { @id = "NewDashboardMessage_Form" })) { %>
    <fieldset>
        <legend>Dashboard Message</legend>
        <div class="wide column">
            <div class="narrow row">
                <label for="NewDashboardMessage_Title" class="fl strong">Title</label>
                <div class="fr"><input type="text" id="NewDashboardMessage_Title" name="Title" maxlength="150" class="longest" /></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Created By</label>
                <div class="fr"><%= Current.DisplayName %></div>
            </div>
            <div class="narrow row">
                <label class="fl strong">Created On</label>
                <div class="fr"><%= DateTime.Now.ToShortDateString() %></div>
            </div>
            <div class="row wysiwyg"><textarea id="NewDashboardMessage_Text" name="Text" class="tallest"></textarea></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="preview">Preview</a></li>
            <li><a class="send">Send</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>