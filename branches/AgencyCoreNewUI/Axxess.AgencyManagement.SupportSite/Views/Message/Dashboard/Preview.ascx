﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <ul>
        <li class="widget">
            <div class="widget-head"><h5>Hello,&#160;<%= Current.DisplayName %>!</h5></div>
            <div class="widget-content"></div>
        </li>
    </ul>
    <div class="buttons">
        <ul>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>