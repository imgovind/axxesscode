﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Axxess Support Application Login Page</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)) %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div class="promo-container"></div>
    <div class="login-promt"></div>
    <div class="login-bar">
        <div class="ac">
            <span class="img logo axxess"></span>
            <br />
            <span class="img logo support"></span>
        </div>
        <% var browser = HttpContext.Current.Request.Browser; %>
        <% if (browser.IsBrowserAllowed()) { %>
        <div id="reflected-box" class="login-container" style="z-index:2;">
            <% string action = "LogOn"; string controller = "Account"; %>
            <% if (Model.ReturnUrl.IsNotNullOrEmpty()) { action = "WindowDirect"; } %>
            <%  using (Html.BeginForm(action, controller, FormMethod.Post, new { @returnUrl = Request.QueryString["returnUrl"] })) { %>
            <%= Html.Hidden("returnUrl", Model.ReturnUrl) %>
            <div class="row">
                <label class="fl strong" for="login-user">E-mail Address</label>
                <%= Html.TextBoxFor(m => m.UserName, new { @id = "login-user", @class = "fr required" })%>
            </div>
            <div class="row">
                <label class="fl strong" for="login-pass">Password</label>
                <%= Html.PasswordFor(m => m.Password, new { @id = "login-pass", @class = "fr required" })%>
            </div>
            <div class="login-forgot">
                <a class="forgot link">Forgot your password?</a>
            </div>
            <div class="row">
                <div class="fl">
                    <input type="checkbox" id="login-remember" checked="checked" name="RememberMe" value="true">
                    <label class="checkbox" for="login-remember">Remember me</label>
                </div>
                <div class="buttons fr">
                    <ul>
                        <li><a class="submit">Login</a></li>
                    </ul>
                </div>
                <input type="submit" />
            </div>
            <%  } %>
        </div>
        <div id="reflection" style="mask:url(#reflection-mask)"></div>
        <% } else { %>
        <div class="error-box">
            <h3>Incompatible Browser</h3>
            <p>
                Your browser does not meet our minimum requirements.  Our software supports Internet Explorer 8 and FireFox 3+.  Please download Internet Explorer 8 or Firefox by clicking on the links below.<br />
                <a href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer 8 Download">Download Internet Explorer</a><br />
                <a href="http://www.mozilla.com/en-US/firefox/" title="Firefox Download">Download Firefox</a>
            </p>
        </div>
        <noscript>
            <div class="error-box">
                <h3>JavaScript Disabled</h3>
                <p>Our software requires JavaScript to be enabled in your browser.  Please contact your IT staff or us for assistance in enabling JavaScript in your browser.</p>
            </div>
        </noscript> 
        <%  } %>
        <!-- SVG from http://hacks.mozilla.org/2010/08/mozelement/ -->
        <svg>
          <mask id="reflection-mask" maskContentUnits="objectBoundingBox">
            <rect x="-0.1" width="1.2" height="1" fill="url(#reflection-gradient)"/>
          </mask>
          <linearGradient id="reflection-gradient" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="0" y2="0">
            <stop stop-color="white" stop-opacity="0.6" offset="0"/>
            <stop stop-color="white" stop-opacity="0" offset="100%"/>
          </linearGradient>
        </svg>
    </div>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("System/Account.js")
            .Add("System/Utility.js")
            .Add("System/Notification.js")
            .Compress(true).Combined(true).CacheDurationInDays(1)
        ).OnDocumentReady(() => { %>
            Account.Init();
    <%  }).Render(); %>
    <%  if (Model.IsLocked) { %>
    <script type="text/javascript">
        $("body").html(U.Error("Account Locked", "You have made too many failed login attempts.  Please contact your Agency/Company's Administrator"));
    </script>
    <%  } %>
</body>
</html>

