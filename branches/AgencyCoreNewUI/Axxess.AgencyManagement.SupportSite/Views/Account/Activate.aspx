﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Activate Account - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group.DefaultPath("~/Content")
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true).CacheDurationInDays(3))
    %>
    <link href="/images/icons/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="activateaccount-wrapper">
        <div id="activateaccount-window">
            <% if (Model.LoginId != Guid.Empty) { %>
                <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">Axxess&trade; Account Activation</span></div>
                <div class="box">
                    <div id="messages"></div>
                    <h1>Welcome to Axxess&trade; Support System.</h1>
                    To activate your account, follow the steps below.
                    <div class="row">&nbsp;</div>
                    <h2><strong>Step 1</strong> - Verify your information below.</h2>
                     <% using (Html.BeginForm("Activate", "Account", FormMethod.Post, new { @id = "activateAccountForm", @class = "activateaccount" })) %>
                    <% { %>
                    <%= Html.Hidden("LoginId", Model.LoginId, new { @id = "Activate_User_LoginId" })%>
                    <div class="row">
                        <%= Html.LabelFor(a => a.Name) %>
                        <%= Model.Name %>
                    </div>
                    <div class="row">
                        <%= Html.LabelFor(a => a.EmailAddress) %>
                        <%= Model.EmailAddress %>
                    </div>
                    <div class="row">&nbsp;</div>
                    <h2><strong>Step 2</strong> - Enter a new password.</h2>
                    <div class="row">
                        <%= Html.LabelFor(a => a.Password)%>
                        <%= Html.PasswordFor(a => a.Password, new { @class = "required", @maxlength="20" })%>
                    </div>
                    <div class="bottom-right">
                        <input type="button" value="Activate Account" class="button" style="clear: both; width: 150px!important;" onclick="$(this).closest('form').submit();" />
                    </div>
                    <% } %>
                </div>
            <% } else { %>
                <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">The page you requested was not found.</span></div>
                <div class="box">
                    <div class="notification warning">You may have mistyped the address or clicked on an expired link. Click <a href="/Login">here</a> to Login.
                    </div>
                </div>
            <% } %>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(false)
         .DefaultGroup(group => group
            .Add("jquery-1.7.2.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("System/Account.js")
            .Add("System/Utility.js")
            .Add("System/Notification.js")
             .Compress(true).Combined(true))
        .OnDocumentReady(() =>
        { 
    %>
    Account.InitActivate();
    <% 
        }).Render(); %>
</body>
</html>