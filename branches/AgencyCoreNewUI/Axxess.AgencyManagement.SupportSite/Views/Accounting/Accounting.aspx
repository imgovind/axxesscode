<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html>
<html>
    <head>
	    <title>Axxess Accounting</title>
	    <meta charset="utf-8">
	    <link href="/Content/bootstrap.fontawesome.css" rel="stylesheet">
	    <link href="/Content/datepicker.css" rel="stylesheet">
	    <link href="/Content/accounting.css" rel="stylesheet">
	    
	
    </head>
    <body>
	
        <!-- Top Navigation -->
        <div id="navbar" class="navbar navbar-fixed-top"></div>
        
        <!-- Content -->
        <div id="content"></div>
        <div id="loading">
    	    <div class="animation">
    		    <i class="icon-refresh icon-spin"></i>
    	    </div>
        </div>
        
        <div id="modalLeft" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <!--div class="modal-header">
                <div class="row-fluid">
	    	        <div class="span6">
				        <div class="update-account btn btn-primary span12">Update</div>
			        </div>
			        <div class="span6">
				        <div class="cancel-account btn btn-primary span12">Cancel</div>
			        </div>
		        </div>
		    </div-->
            <div class="modal-body"></div>
            <div class="modal-footer">
                <div class="row-fluid">
	    	        <div class="span6">
				        <div class="update-account btn btn-primary span12">Save</div>
			        </div>
			        <div class="span6">
				        <div class="cancel-account btn btn-info span12">Close</div>
			        </div>
		        </div>
		    </div>
        </div>
        <!-- Main Menu -->
	    <script type="text/handlebars" id="navbar">	
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand fjalla">Accounting</a>
                    <ul class="nav">
                        <li><a href="#" data-view="metrics">Metrics</a></li>
                        <li><a href="#" data-view="dashboard">Dashboard</a></li>
                        <li><a href="#" data-view="weekly">Weekly</a></li>
                        <li><a href="#" data-view="monthly">Monthly</a></li>
                        <li><a href="#" data-view="yearly">Yearly</a></li>
                        <li><a href="#" data-view="range">Month Range</a></li>
                        <!--
                        <li><a href="accounting/revenue">Revenue</a></li>
                        <li><a href="accounting/opened">New Agencies</a></li>
					    -->
                    </ul>
                    <ul class="nav">
						
                        {{#if prev}}
                            <li><a href="javascript:void(0)" class="prev"><i class="icon-chevron-sign-left"></i></a></li>
                            <li><a class="title-{{view}}">{{title}}</a></li>
                            <li><a href="javascript:void(0)" class="next"><i class="icon-chevron-sign-right"></i></a></li>
                        {{/if}}
						
					    {{#if start}}
                            <li class="input-daterange datepicker">
                                <input id="accounting_date_start" type="text" class="input-small" 
                            	    name="accounting_date_start" value="{{start}}" placeholder="Month Year" />
                            </li>
                            <li><span class="add-on">to</span></li>
                            <li class="input-daterange datepicker">
                                <input id="accounting_date_end" type="text" class="input-small" 
                            	    name="accounting_date_end" value="{{end}}" placeholder="Month Year" />
                            </li>
                            <li><div id="update" class="btn pull-right">Update</div></li>
					    {{/if}}
                    </ul>
                    
                    <a class="btn pull-right export">Export</a>
				    <!-- <div id="nav_head" class="pull-right"></div> -->
                </div>
            </div>
	    </script>
	    
	    <script type="text/handlebars" id="dashboard">
	        <div class="container-fluid">
	            <div class="row-fluid">
    	            <div class="span6 chart" data-chart="MRRBookings"></div>
	                <div class="span6 chart" data-chart="EndingMRR"></div>
	            </div>
	        </div>
	    </script>
	    <!-- Dashboard -->
	    <script type="text/handlebars" id="chart">
            <h3 class="title">{{Caption}}</h3>
        
            <div class="navbar">
                <div class="navbar-inner">
                    <form class="navbar-form form-inline pull-left">
                        <div class="btn-group chart-type">
                            <button type="button" class="btn btn-default active" data-chart-type="line">Line</button>
                            <button type="button" class="btn btn-default" data-chart-type="column">Bar</button>
                        </div>
                    </form>
                    <form class="navbar-form form-inline pull-right">
                        <div class="input-prepend">
                            <span class="add-on">From</span>
                            <input type="text" class="date-picker input-small" name="StartDate" value="{{moment StartDate format="MMM YYYY"}}"/>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on">To</span>
                            <input type="text" class="date-picker input-small" name="EndDate" value="{{moment EndDate format="MMM YYYY"}}"/>
                        </div>
                        <button class="btn update"><i class="icon-refresh"></i></button>
                    </form>
                </div>
            </div>
            <div class="graph"></div>
        </script>

        <!-- Modal Windows -->	    
        <script type="text/handlebars" id="modalLeft">
	        <div id="modalLeft" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-header">{{>modalControls}}</div>
	            <div class="modal-body"></div>
	            <div class="modal-footer">{{>modalControls}}</div>
	        </div>
        </script>
	    
        <script type="text/handlebars" id="modal">
	        <div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
	                <h3 id="modalLabel">Modal</h3>
	            </div>
	            <div class="modal-body"></div>
	            <div class="modal-footer"></div>
	        </div>
        </script>
	
	    <script type="text/handlebars" id="modalRight">
	        <div id="modalRight" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
	                <h3 id="modalLabel">Modal</h3>
	            </div>
	            <div class="modal-body"></div>
	            <div class="modal-footer"></div>
	        </div>
	    </script>
		
	    <script type="text/handlebars" id="modalControls">
		    <div class="row-fluid">
	    	    <div class="span6">
				    <div class="update-account btn btn-primary span12">Update</div>
			    </div>
			    <div class="span6">
				    <div class="cancel-account btn btn-primary span12">Cancel</div>
			    </div>
		    </div>
	    </script>
	    <!-- Metrics -->
	    <script type="text/handlebars" id="metrics">
		    <table class="table table-striped payments" >
		        <tr id="header">
		            <th>Axxess Metrics</th>
		            {{#each Months}}
		        	    <th class="header-right">{{this}}</th>
		            {{/each}}
		            <th>Total</th>
		        </tr>
			    
			    {{#each MetricsRows}}
			        <tr class="{{MonthClass}}">
			            <td>{{Name}}</td>
			            {{#each Data}}
			       		    <td class="numeric "><a>{{this}}</a></td>
			       	    {{/each}}
				   	   <td class="numeric"> <a class="{{MonthClass}}">{{Total}}</a></td>
			        </tr>
			    {{/each}}
		    </table>
			
		    <table id="header-fixed" class="table table-striped"></table>
		    
	    </script>
		<!-- Yearly, Monthly, Weekly and Date Range -->
	    <script type="text/handlebars" id="content">
		    <table class="table table-striped payments" >
		        <tr id="header">
		            <th>Account Name</th>
		            {{#each Titles}}
		        	    <th class="header-right">{{this}}</th>
		            {{/each}}
		            <th>Total</th>
		        </tr>
			    
			    {{#each AgencyPaymentData}}
			        <tr data-id="{{AccountId}}">
			            <td>
			                <a id="{{AccountId}}" data-toggle="modal" href="#modalLeft" class="update-account-modal {{SourceClass PaymentSource}}" 
			            	    rel="tooltip" data-placement="right" data-html="true" 
			            	    title="{{AccountId}}<br>Source: {{PaymentSource}}<br>Location:<br>{{City}}, {{State}}<br>{{ZipCode}}">
			                    {{AgencyName}}
			                </a>
			            </td>
			            {{#each Payments}}
			       		    <td class="numeric">{{this}}</td>
			       	    {{/each}}
				   	    <td class="numeric">{{Total}}</td>
			        </tr>
			    {{/each}}
			
		        <tr id="footer">
		            <th style="text-align:left;">Total: {{AgencyPaymentData.length}} Accounts</th>
		            {{#each ListOfTotalAmounts}}
			            <th class="numeric">{{this}}</th>
			        {{/each}}
		            <th class="numeric">{{BigTotal}}</th>
		        </tr>
		    </table>
			
		    <table id="header-fixed" class="table table-striped"></table>
		    <table id="footer-fixed" class="table table-striped"></table>
	    </script>
	    <!-- Account Information -->
	    <script type="text/handlebars" id="AccountInformation">
	        <form>
	            <input type="hidden" id="accountId" name="accountId" value="{{AccountId}}" />
	            <input type="hidden" id="currentinitialsigndate" name="currentinitialsigndate" value="{{moment InitialSignDate}}" />
	            <div class="row-fluid">
	                <h5>Account Number: {{AccountId}}</h5>
	            </div>
	            <div class="row-fluid">
	                <h5>Account Name</h5>
	                <div class="span12">
	                    <input type="text" class="span12" id="agencyName" name="agencyName" value="{{AgencyName}}" />
	                </div>
	            </div>
	            <div class="row-fluid">
	                <div class="span6">
	                    <h5>City</h5>
	                    <div class="span12">
	                        <input type="text" class="span12" id="city" name="city" value="{{City}}" />
	                    </div>
	                </div>
	                <div class="span6">
	                    <h5>State</h5>
	                    <div class="span12">
	                      <input type="hidden" class="span12" id="state" name="state" value="{{State}}" />
                          <%=Html.LookupSelectList(SelectListTypes.States, "StateFilter", "{{State}}", new { @id="StateFilter", @class = "state state-filter" })%>
	                    </div>
	                </div>
	            </div>
	            <div class="row-fluid">
	                <div class="span6">
	                    <h5>Zip</h5>
	                    <div class="span12">
	                        <input type="text" class="span12" id="zipCode" name="zipCode" value="{{ZipCode}}" />
	                    </div>
	                </div>
	                <div class="span3">
	                    <h5>Status</h5>
	                    <div class="span12">
	                        <select class="span12" id="status" name="status" value="{{Status}}">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="span3">
	                    <h5>Start Up</h5>
	                    <div class="span12">
	                        <select class="span12" id="startup" name="startup" value="{{StartUp}}">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
	                        </select>
	                    </div>
	                </div>
	            </div>
	            <div class="row-fluid">
	                <h5>Sales Person</h5>
	                <div class="span12">
	                    <input type="text" class="span12" id="salesperson" name="salesperson" value="{{SalesPerson}}" />
	                </div>
	            </div>
	            <div class="row-fluid">
	                <h5>Implementation Fee</h5>
	                <div class="span12">
	                    <input type="text" class="span12" id="implementationfee" name="implementationfee" value="{{ImplementationFee}}" />
	                </div>
	            </div>
	            <div class="row-fluid">
	                <div class="span6">
	                    <h5>Payment Source</h5>
	                    <div class="span12">
                        <select id="paymentsource" name="paymentsource" value="{{Paymentsource}}" >
                          <option value="Bank">Bank</option>
                          <option value="Credit">Credit</option>
                          <option value="Unknown">Unknown</option>
                        </select>
	                    </div>
	                </div>
	                <div class="span6">
	                    <h5>Initial Sign Date</h5>
	                    <div class="span12 datepicker">
	                        <input type="text" class="span12" id="initialsigndate" name="initialsigndate" 
	            	            value="{{moment InitialSignDate}}" placeholder="yyyy-mm-dd" />
	                    </div>
	                </div>
	            </div>
	            <div class="row-fluid">
	                <h5>Special Notes</h5>
	                <div class="span12">
	                    <textarea type="text" class="span12" id="specialnotes" name="specialnotes">{{SpecialNotes}}</textarea>
	                </div>
	            </div>
	            {{#if Payments}}
		            <div class="row-fluid">
		                <div class="span12">
		                    <h5>Payment History</h5>
		                    <table class="table table-striped">
		                        {{#each Payments}}
			                        <tr data-id="{{Id}}">
			                            <td>
			                                <input type="hidden" class="" id="" name="payments[{{@index}}].Id" value="{{Id}}" placeholder="Id" />
                                        </td>
			                            <td class="datepicker">
			                                <input type="text" class="" id="" name="payments[{{@index}}].Start" value="{{moment Start}}" placeholder="yyyy-mm-dd" />
			                            </td>
			                            <td class="datepicker">
			                                <input type="hidden" class="" id="" name="payments[{{@index}}].End" value="{{moment End}}" placeholder="yyyy-mm-dd" />
			                            </td>
			                            <td>
			                                <input type="text" class="" id="" name="payments[{{@index}}].Amount" value="{{Amount}}" placeholder="Amount" />
                                        </td>
			                            <td>
		                                    <select class="package" name="payments[{{@index}}].Package">
                                                 <option value="{{id}}">{{text}}</option>
	                    	                     <option value="5">Bronze (1 to 5 users)</option>
                                                 <option value="10">Silver (6 to 10 users)</option>
                                                 <option value="20">Gold (11 to 20 users)</option>
                                                 <option value="40">Platinum (21 to 40 users)</option>
                                                 <option value="100000">Titanium (41 to unlimited users)</option>
                                                 <option value="100001">Premier (Unlimited users)</option>
                                                 <option value="100002">Enterprise (Unlimited users)</option>
		                                    </select>
			                                
                                        </td>
			                        </tr>
				                {{/each}}
		                        <tr id="addPayment">
		                            <th colspan="3"><div class="addPayment span10 offset1 btn btn-primary">Add Payment</div></th>
		                        </tr>
		                        <tr id="addPaymentForm" class="hide addPaymentForm">
		                            <td><!-- Id --></td>
		                            <td class="datepicker">
		                                <input type="text" name="updatePayment.start" placeholder="Start" />
		                            </td>
		                            <td><!-- End --></td>
		                            <td>
		                                <input type="text" name="updatePayment.amount" placeholder="Amount" />
		                            </td>
		                            <td>
	                                    <select name="updatePayment.package">
            	                         <option value="5">Bronze (1 to 5 users)</option>
                                         <option value="10">Silver (6 to 10 users)</option>
                                         <option value="20">Gold (11 to 20 users)</option>
                                         <option value="40">Platinum (21 to 40 users)</option>
                                         <option value="100000">Titanium (41 to unlimited users)</option>
                                         <option value="100001">Premier (Unlimited users)</option>
                                         <option value="100002">Enterprise (Unlimited users)</option>
	                                    </select>
		                            </td>
		                        </tr>
		                    </table>
		                </div>
		            </div>
	            {{/if}}
            </form>
        </script>
        
        <!-- Scripts -->
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/jquery.js"></script>
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/bootstrap.js"></script>
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/bootstrap-datepicker.js"></script>
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/moment.js"></script>
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/handlebars.js"></script>
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/min/highcharts.js"></script>
	    <script type="text/javascript" src="/Scripts/Modules/Accounting/accounting.js"></script>
    </body>
</html>