﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Axxess.Log.Domain.SupportAppAudit>>" %>

<%= Html.Telerik().Grid(Model)
        .Name("LogsGrid")
        .PrefixUrlParameters(false)
        .Columns(columns =>
        {
            columns.Bound(l => l.UserName).Width(30);
            columns.Bound(l => l.ActionDescription).Width(100);
            columns.Bound(l => l.Date).Format("{0:MM/dd/yyyy hh:mm tt}").Width(70);
        })
        .Footer(false)
        .Sortable()
%>

