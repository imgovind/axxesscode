﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MenuViewData>" %>
<script type="text/javascript">
$("body").AcoreDesktop({
    Menu: [ 
        <% if(Model.IsNewExist){ %>
        { Name: "Create", Id: "Create", IconX: 140, IconY: 108 },
//        { Name: "New", Id: "New", Parent: "Create" },
        <% }%>
         <% if(Model.IsViewExist || Model.IsListExist ){ %>
        { Name: "View", Id: "View", IconX: 161, IconY: 108 },
        <% }%>
         <% if(Model.IsListExist){ %>
        { Name: "List", Id: "List", Parent: "View" },
        <% }%>
         <% if(Model.IsAxxessExist){ %>
        { Name: "Axxess", Id: "Axxess", IconX: 121, IconY: 86 }
        <% }%>
    ],
    Windows: [ <% for (int i = 0; i < Model.MenuElement.Count; i++) { %>
		   <%= Model.MenuElement[i].Json %><%= i + 1 < Model.MenuElement.Count ? "," : string.Empty %>
	    <% } %>
		],
    
    Options: { GetRemoteContent: true, Animated: true, DefaultWindow: "AgenciesCenter" }
});
var query = "<%=Request.Url.Query %>".split("=");
Linker.Link(query[0], query[1]);
window.history.pushState("","","/");
</script>

