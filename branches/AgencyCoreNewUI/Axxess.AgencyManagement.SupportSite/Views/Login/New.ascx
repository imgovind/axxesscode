﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>>>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Create", "Login", FormMethod.Post, new { @id = "NewLogin_Form" })) { %>
    <fieldset>
        <legend>Login Information</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewLogin_EmailAddress" class="fl strong">E-mail Address</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", "", new { @id = "NewLogin_EmailAddress", @class = "email required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewLogin_DisplayName" class="fl strong">Display Name</label>
                <div class="fr"><%=Html.TextBox("DisplayName", "", new { @id = "NewLogin_DisplayName", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Permission_TemplateId" class="fl strong">Permission Template </label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.PermissionTemplate, "PermissionTemplate", "", new { @id = "NewLogin_Permission_TemplateId", @class = "required" })%></div>
            </div>
        </div>
    </fieldset>
     <%  Html.RenderPartial("Permissions", Model, new ViewDataDictionary { { "IdPrefix", "NewLogin" } }); %>
    
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>