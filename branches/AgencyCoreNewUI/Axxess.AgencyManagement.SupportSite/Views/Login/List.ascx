﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div class="wrapper main blue">
<%  if (Model.IsUserCanAdd){ %>
    <div class="fr buttons">
        <ul><li><a class="new-login">New Login</a></li></ul>
    </div>
<%  } %>
    <fieldset class="ac grid-controls short">
        <div class="align-center">
            <label for="SelectActiveFilter" class="strong">Filter Users:</label>
            <select id="SelectActiveFilter">
                <option value= true selected="selected">Active</option>
                <option value= false>Inactive</option>
            </select>
        </div>
        <div class="filter grid-search"></div>
    </fieldset>
    <% var actions = new List<string>();
       if (Model.IsUserCanEdit)
       {
           actions.Add("<a class=\"link\" onclick=\"Login.Edit('<#=Id#>');return false\">Edit</a>");
           actions.Add("<a class=\"link\" onclick=\"Login.ToggleStatus('<#=Id#>', <#=IsActive#>);return false;\"><#=IsActive ? 'Deactivate' : 'Activate'#></a>");
           
       }
       if (Model.IsUserCanViewLog)
       {
           actions.Add("<a class=\"link\" onclick=\"Login.Logs('<#=Id#>');return false\">Logs</a>");
       }
       var width = actions.Count * 4;
       var actionLinks = actions.ToArray().Join("");
         %>
    <%= Html.Telerik().Grid<Axxess.Membership.Domain.SupportLogin>().Name("ListLogins_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(l => l.DisplayName).Title("Display Name").Sortable(true).Width(20);
            columns.Bound(l => l.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("E-mail Address").Sortable(true).Width(25);
            columns.Bound(l => l.CreatedFormatted).Title("Created").Width(8);
            columns.Bound(l => l.Id).Title("Action").Sortable(false).Width(10).ClientTemplate(actionLinks).Visible(Model.IsUserCanEdit || Model.IsUserCanViewLog).Width(width);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Login", new { status = true })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>