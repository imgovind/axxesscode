﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Login", FormMethod.Post, new { @id = "EditLogin_Form" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditLogin_Id" }) %>
    <fieldset>
        <legend>Login Information</legend>
        <div class="wide column">
            <div class="row">
                <label for="EditLogin_EmailAddress" class="fl strong">E-mail Address</label>
                <div class="fr"><%= Model.EmailAddress %></div>
            </div>
            <div class="row">
                <label for="EditLogin_DisplayName" class="fl strong">Display Name</label>
                <div class="fr"><%=Html.TextBox("DisplayName", Model.DisplayName, new { @id = "EditLogin_DisplayName", @class = "required", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Permission_TemplateId" class="fl strong">Permission Template </label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.PermissionTemplate, "PermissionTemplate", "", new { @id = "EditLogin_Permission_TemplateId", @class = "required" })%></div>
            </div>
        </div>
    </fieldset>
   <%  Html.RenderPartial("Permissions", Model.PermissionData, new ViewDataDictionary { { "IdPrefix", "EditLogin" } }); %>
   <% if (Model.IsUserCanViewLog) { %>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
   <% } %>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>