﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>>>" %>
<%var IdPrefix = ViewData["IdPrefix"];%>
 <fieldset>
     <legend>Permissions</legend>
    <div class="wide-column">
        <div class="row">
            <ul class="checkgroup">
                <li class="option">
                    <input id="<%= IdPrefix%>_AllPermissions" type="checkbox" value="" class="allpermissions"/>
					<label for="<%= IdPrefix%>_AllPermissions">Select all Permissions</label>
				</li>
            </ul>
        </div>
    </div>
    <div class="clear" />
	<div id="<%= IdPrefix%>_Accordion" class="accordion-container">
        <% foreach (var groupPermission in Model){ %>
            <% if(groupPermission.Key.NumberOfPermissions > 1){%>
                <h3><input type="checkbox" class="select-all" target="div" <%= groupPermission.Key.IsAllSelected.ToChecked() %>/><%= groupPermission.Key.Name %></h3>
			    <div class="<%= groupPermission.Key.HasSubCategories ? "accordion" : string.Empty %>">
				    <% if(!groupPermission.Key.HasSubCategories) { %>
				        <% foreach (var parentPermission in groupPermission.Value){ %>
                          <%var parentPermissions = parentPermission.Value.OrderBy(p => p.Description); %>
					      <% foreach (var permission in parentPermissions) { %>
						        <h6 tooltip="<%= permission.ToolTip %>">
						            <input type="checkbox" name="<%= "Permissions[" + parentPermission.Key.Id + "].Value" %>" value="<%= permission.ChildId%>" <%= permission.IsChecked.ToChecked() %>/>
							        <%= permission.Description %>
						        </h6>
					        <% } %>
					        <%= Html.Hidden("Permissions.Index", parentPermission.Key.Id)%>
			                <%= Html.Hidden("Permissions[" + parentPermission.Key.Id + "].Key", parentPermission.Key.Id)%>
				        <% } %>
				    <% }else{ %>
				        <% foreach (var parentPermission in groupPermission.Value){ %>
				            <%= Html.Hidden("Permissions.Index", parentPermission.Key.Id)%>
						    <%= Html.Hidden("Permissions[" + parentPermission.Key.Id + "].Key", parentPermission.Key.Id)%>
				            <% if(parentPermission.Value.Count > 1) { %>
					            <h3><input type="checkbox" class="select-all" target="div" <%= parentPermission.Key.IsAllSelected.ToChecked() %>/><%= parentPermission.Key.Name%></h3>
					            <div>
						            <%var parentPermissions = parentPermission.Value.OrderBy(p => p.Description); %>
						            <% foreach (var permission in parentPermissions) { %>
						                <h6 tooltip="<%= permission.ToolTip %>">
							                <input type="checkbox" name="<%= "Permissions[" + parentPermission.Key.Id + "].Value" %>" value="<%= permission.ChildId%>" <%= permission.IsChecked.ToChecked() %>/>
							                <%= permission.Description %>
						                </h6>
						            <% } %>
					            </div>
					        <% } else { %>
					            <% var permission = parentPermission.Value[0]; %>
					            <h3 class="ui-accordion-no-expansion" tooltip="<%= permission.ToolTip %>"><input type="checkbox" name="<%= "Permissions[" + parentPermission.Key.Id + "].Value" %>" value="<%= permission.ChildId%>" <%= permission.IsChecked.ToChecked() %>/><%= permission.Description %></h3>
					        <% } %>
					    <% } %>
				    <% } %>
				</div>
		    <% } else if(groupPermission.Key.NumberOfPermissions ==1){ %>
		        <% var parentPermission = groupPermission.Value.ElementAt(0);%>
		        <% var permission = parentPermission.Value[0];%>
		        <%= Html.Hidden("Permissions.Index", parentPermission.Key.Id)%>
				<%= Html.Hidden("Permissions[" + parentPermission.Key.Id + "].Key", parentPermission.Key.Id)%>
			    <h3 class="ui-accordion-no-expansion" tooltip="<%= permission.ToolTip %>"><input type="checkbox" name="<%= "Permissions[" + parentPermission.Key.Id + "].Value" %>" value="<%= permission.ChildId%>" <%= permission.IsChecked.ToChecked() %>/><%= permission.Description %></h3>
		    <% } %>
		<% } %>
	</div>
</fieldset>