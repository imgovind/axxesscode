﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div class="wrapper main blue">
    <div class="fr buttons">
        <%  if(Model.IsUserCanAdd) { %>
            <ul><li><a class="new-template">New Permission Template</a></li></ul>
         <% } %>
    </div>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <% var actions = new List<string>();
       if (Model.IsUserCanEdit)
       {
           actions.Add("<a class=\"link\" onclick=\"Login.Permission.Edit('<#=Id#>');return false\">Edit</a>");
       }
       if (Model.IsUserCanDelete)
       {
           actions.Add("<a class=\"link\" onclick=\"Login.Permission.Delete('<#=Id#>',$(this).closest('.t-grid'));return false\">Delete</a>");
       }
           var width = actions.Count * 4;
           var actionLinks = actions.ToArray().Join("");
    %>
    <%= Html.Telerik().Grid<PermissionTemplate>().Name("TemplateList_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(l => l.Name).Title("Template Name").Sortable(true).Width(20);
            columns.Bound(l => l.Id).Title("Action").Sortable(false).Width(10).ClientTemplate(actionLinks).Width(width).Visible(Model.IsUserCanEdit || Model.IsUserCanDelete);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("TemplateGrid", "Login")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>