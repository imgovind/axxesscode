﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PermissionTemplateViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("UpdatePermissionTemplate", "Login", FormMethod.Post, new { @id = "EditTemplate_Form" }))
    { %>
     <%= Html.Hidden("Id", Model.Id, new { @id = "EditTemplate_Id" }) %>
     <fieldset>
        <legend>Template Information</legend>
        <div class="wide column">
            <div class="row">
                <label for="NewTemplate_TemplateName" class="fl strong">Permission Template</label>
                <div class="fr"><%=Html.TextBox("TemplateName", Model.Name, new { @id = "EditTemplate_TemplateName", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <div class="fr"></div>
            </div>
        </div>
    </fieldset>
    <%  Html.RenderPartial("Permissions", Model.PermissionData, new ViewDataDictionary { { "IdPrefix", "EditTemplate" } }); %>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>