﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<% using (Html.BeginForm("Create", "Agency", FormMethod.Post, new { @id = "NewAgency_Form" })) { %>
    <div>
        <fieldset class="half fl">
            <legend>Agreement Information</legend>
            <div class="row">
                <div class="row">
                    <label for="NewAgency_IsAgreementSignedYes" class="fl strong">Agreement Signed?</label>
                    <div class="fr radio">
                        <input type="radio" class="required" id="NewAgency_IsAgreementSignedYes" name="IsAgreementSigned" value="true" />
                        <label for="NewAgency_IsAgreementSignedYes" class="fixed shorter">Yes</label>
                        <input type="radio" class="required" id="NewAgency_IsAgreementSignedNo" value="false" name="IsAgreementSigned" />
                        <label for="NewAgency_IsAgreementSignedNo" class="fixed shorter">No</label>
                    </div>
                </div>
                <div class="row" id="NewAgency_IsAgreementSignedYesMore">
                    <label for="NewAgency_TrialPeriod" class="fl strong">Trial Period</label>
                    <div class="fr">
                        <%  var trialPeriod = new SelectList(new[] {
                                new SelectListItem { Text = "-- Select Trial Period --", Value = "" },
                                new SelectListItem { Text = "30 Days", Value = "30" },
                                new SelectListItem { Text = "60 Days", Value = "60" },
                                new SelectListItem { Text = "90 Days", Value = "90" }
                            }, "Value", "Text"); %>
                        <%= Html.DropDownList("TrialPeriod", trialPeriod, new { @id = "NewAgency_TrialPeriod" }) %>
                    </div>
                </div>
                
                 <%--<div class="row" id="NewAgency_Cluster">
                    <label for="NewAgency_Cluster" class="fl strong">Cluster</label>
                    <div class="fr">
                        <%  var clusters = new SelectList(new[] {
                                new SelectListItem { Text = "-- Select Cluster --", Value = "" },
                                new SelectListItem { Text = "Cluster 1", Value = "1" },
                                new SelectListItem { Text = "Cluster 2", Value = "2" }
                            }, "Value", "Text",CoreSettings.CurrentClusterId); %>
                        <%= Html.DropDownList("ClusterId", clusters, new { @id = "NewAgency_ClusterId" ,@class = "required"})%>
                    </div>
                </div>--%>
            </div>
            
            <div class="row">
                <label for="NewAgency_Services" class="fl strong">Services</label>
                <div class="fr">
                    <%= Html.AgencyServicesDropDown("ServiceArray", AgencyServices.HomeHealth, new { @class = "required", multiple = "multiple", style = "width:200px", id = "NewAgency_Services" })%>
                </div>
            </div>
        </fieldset>
        <fieldset class="half fr">
            <legend>Import</legend>
            <div class="row">
                <div class="row">
                    <div class="row">
                        <label class="fl strong">Select Agency</label>
                        <select class="fr" id="ESign_Agencies" name="ESign">
                            <option value="0">-- Select Agency --</option>
                        </select>
                    </div>
                    <div class="column"><div class="row"></div></div>            
                </div>
            </div>
        </fieldset>
    </div>
    <div class="clr">
        <fieldset class="half fl">
            <legend>Admin Information</legend>
            <div class="column">
                <div class="row">
                    <label for="NewAgency_AdminUsername" class="fl strong">E-mail</label>
                    <div class="fr"><%=Html.TextBox("AgencyAdminUsername", string.Empty, new { @id = "NewAgency_AdminUsername", @class = "required", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="NewAgency_AdminFirstName" class="fl strong">First Name</label>
                    <div class="fr"><%=Html.TextBox("AgencyAdminFirstName", string.Empty, new { @id = "NewAgency_AdminFirstName", @class = "required", @maxlength = "50" })%></div>
                </div>
                <div class="row">
                    <label for="NewAgency_AdminLastName" class="fl strong">Last Name</label>
                    <div class="fr"><%=Html.TextBox("AgencyAdminLastName", string.Empty, new { @id = "NewAgency_AdminLastName", @class = "required", @maxlength = "50" })%></div>
                </div>
            </div>
        </fieldset>
        <fieldset class="half fr">
        <legend>Audit Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewAgency_SalesPerson" class="fl strong">Sales Person</label>
                <div class="fr"><%= Html.Users("SalesPerson", string.Empty, new { @id = "NewAgency_SalesPerson", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_SalesPerson" class="fl strong">Trainer</label>
                <div class="fr"><%= Html.Users("Trainer", string.Empty, new { @id = "NewAgency_Trainer", @class = "required notzero" })%></div>
            </div>
            <div class="row">
                <label class="fl strong">Backup Trainer</label>
                <div class="fr"><%=Html.Users("BackupTrainer", string.Empty, new { @id = "NewAgency_BackupTrainer", @class = "required notzero"}) %></div>
            </div>
        </div>
    </fieldset>
    </div>
    <div class="clr"></div>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewAgency_CompanyName" class="fl strong">Company Name</label>
                <div class="fr"><%=Html.TextBox("Name", string.Empty, new { @id = "NewAgency_CompanyName", @maxlength = "100", @class = "required" })%></div>
            </div>
           <div class="row">
                <label for="NewAgency_TaxId" class="fl strong">Tax Id</label>
                <div class="fr"><%=Html.TextBox("TaxId", string.Empty, new { @id = "NewAgency_TaxId", @maxlength = "10", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_TaxIdType" class="fl strong">Tax Id Type</label>
                <div class="fr">
                    <select id="NewAgency_TaxIdType" name="TaxIdType">
                        <option value="0">-- Select Tax ID Type --</option>
                        <option value="1">EIN (Employer Identification Number)</option>
                        <option value="2">SSN (Social Security Number)</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label for="NewAgency_Payor" class="fl strong required">Payor</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.Payors, "Payor", "", new { @id = "NewAgency_Payor" })%>
                </div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input type="checkbox" class="radio" name="NewAgency_SameAsAdmin" id="NewAgency_SameAsAdmin" />
                        <label for="NewAgency_SameAsAdmin">Check here if the Admin is same as the Contact Person</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="NewAgency_ContactPersonEmail" class="fl strong">Contact Person E-mail</label>
                <div class="fr"><%=Html.TextBox("ContactPersonEmail", string.Empty, new { @id = "NewAgency_ContactPersonEmail", @class = "required email", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_ContactPhone1" class="fl strong">Contact Person Phone</label>
                <div class="fr">
                    <input type="text" class="required numeric phone-short" name="ContactPhoneArray" id="NewAgency_ContactPhone1" maxlength="3" />
                    -
                    <input type="text" class="required numeric phone-short" name="ContactPhoneArray" id="NewAgency_ContactPhone2" maxlength="3" />
                    -
                    <input type="text" class="required numeric phone-long" name="ContactPhoneArray" id="NewAgency_ContactPhone3" maxlength="4"/>
                </div>
            </div>
        </div>
        <div class="column">
          <div class="row">
                <label for="NewAgency_AccountId" class="fl strong">Account Id</label>
                <div class="fr"><%=Html.TextBox("AccountId", string.Empty, new { @id = "NewAgency_AccountId", @maxlength = "75"})%></div>
            </div>
           <div class="row">
                <label for="NewAgency_NationalProviderNo" class="fl strong">National Provider Number</label>
                <div class="fr"><%=Html.TextBox("NationalProviderNumber", string.Empty, new { @id = "NewAgency_NationalProviderNo", @maxlength = "10", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_MedicareProviderNo" class="fl strong">Medicare Provider Number</label>
                <div class="fr"><%=Html.TextBox("MedicareProviderNumber", string.Empty, new { @id = "NewAgency_MedicareProviderNo", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_MedicaidProviderNo" class="fl strong">Medicaid Provider Number</label>
                <div class="fr"><%=Html.TextBox("MedicaidProviderNumber", string.Empty, new { @id = "NewAgency_MedicaidProviderNo", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_HomeHealthAgencyId" class="fl strong">Unique Agency OASIS ID Code</label>
                <div class="fr"><%=Html.TextBox("HomeHealthAgencyId", string.Empty, new { @id = "NewAgency_HomeHealthAgencyId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_CahpsVendor" class="fl strong">CAHPS Vendor</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.CahpsVendors, "CahpsVendor", string.Empty, new { @id = "NewAgency_CahpsVendor" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_ContactPersonFirstName" class="fl strong">Contact Person First Name</label>
                <div class="fr"><%=Html.TextBox("ContactPersonFirstName", string.Empty, new { @id = "NewAgency_ContactPersonFirstName", @maxlength = "50", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_ContactPersonLastName" class="fl strong">Contact Person Last Name</label>
                <div class="fr"><%=Html.TextBox("ContactPersonLastName", string.Empty, new { @id = "NewAgency_ContactPersonLastName", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_PreviousSoftware" class="fl strong">Previous Software</label>
                <div class="fr"><%=Html.TextBox("PreviousSoftware", string.Empty, new { @id = "NewAgency_PreviousSoftware", @maxlength = "100" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Submitter Information (Medicare)</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessTheBiller", false, new { @id = "NewAgency_AxxessBiller" }) %>
                        <label for="NewAgency_AxxessBiller">Check here if Axxess&trade; HealthCare Consult handles billing for this Agency.</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="NewAgency_SubmitterId" class="fl strong">Submitter Id</label>
                <div class="fr"><%= Html.TextBox("SubmitterId", string.Empty, new { @id = "NewAgency_SubmitterId", @maxlength = "15", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_SubmitterName" class="fl strong">Submitter Name</label>
                <div class="fr"><%= Html.TextBox("SubmitterName", string.Empty, new { @id = "NewAgency_SubmitterName", @maxlength = "50", @class = "required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewAgency_SubmitterPhone1" class="fl strong">Submitter Phone Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short required" name="SubmitterPhoneArray" id="NewAgency_SubmitterPhone1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short required" name="SubmitterPhoneArray" id="NewAgency_SubmitterPhone2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long required" name="SubmitterPhoneArray" id="NewAgency_SubmitterPhone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewAgency_SubmitterFax1" class="fl strong">Submitter Fax Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short required" name="SubmitterFaxArray" id="NewAgency_SubmitterFax1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short required" name="SubmitterFaxArray" id="NewAgency_SubmitterFax2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long required" name="SubmitterFaxArray" id="NewAgency_SubmitterFax3" maxlength="4" />
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row">
                <label for="NewAgency_LocationName" class="fl strong">Location Name: (e.g. Headquarters)</label>
                <div class="fr"><%=Html.TextBox("LocationName", "", new { @id = "NewAgency_LocationName", @maxlength = "50", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_Phone1" class="fl strong">Phone Number</label>
                <div class="fr">
                    <input type="text" class="numeric required phone-short" name="PhoneArray" id="NewAgency_Phone1" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-short" name="PhoneArray" id="NewAgency_Phone2" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-long" name="PhoneArray" id="NewAgency_Phone3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewAgency_Fax1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="FaxArray" id="NewAgency_Fax1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="FaxArray" id="NewAgency_Fax2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="FaxArray" id="NewAgency_Fax3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <label for="NewAgency_IsUserPlan" class="fl strong">Plan Type</label>
                    <div class="fr radio">
                        <input type="radio" class="required" id="NewAgency_IsUserPlanYes" name="IsUserPlan" value="true" />
                        <label for="NewAgency_IsUserPlanYes" class="fixed shorter">User</label>
                        <input type="radio" class="required" id="NewAgency_IsUserPlanNo" value="false" name="IsUserPlan" />
                        <label for="NewAgency_IsUserPlanNo" class="fixed shorter">Patient</label>
                    </div>
                </div>
                <div class="row" id="NewAgency_UserSubscriptionPlan">
                    <label for="NewAgency_Package" class="fl strong">User Subscription Plan</label>
                    <div class="fr">
                        <%  var userPackages = new SelectList(new[] {
                                new SelectListItem { Text = "** Select Plan **", Value = "" },
                                new SelectListItem { Text = "1 to 5 users", Value = "5" },
                                new SelectListItem { Text = "6 to 10 users", Value = "10" },
                                new SelectListItem { Text = "11 to 20 users", Value = "20" },
                                new SelectListItem { Text = "21 to 30 users", Value = "30" },
                                new SelectListItem { Text = "31 to 40 users", Value = "40" },
                                new SelectListItem { Text = "41 to 100 users", Value = "100" },
                                new SelectListItem { Text = "101 to 200 users", Value = "200" },
                                new SelectListItem { Text = "Over 200 users", Value = "100000" },
                            }, "Value", "Text"); %>
                        <%= Html.DropDownList("PlanLimit", userPackages, new { @id = "NewAgency_PlanLimit", @class="required" })%>
                    </div>
                </div>
                <div class="row" id="NewAgency_PatientSubscriptionPlan">
                    <label for="NewAgency_Package" class="fl strong">Patient Subscription Plan</label>
                    <div class="fr">
                        <%  var patientPackages = new SelectList(new[] {
                                new SelectListItem { Text = "** Select Plan **", Value = "" },
                                new SelectListItem { Text = "1 to 25 patients", Value = "25" },
                                new SelectListItem { Text = "26 to 50 patients", Value = "50" },
                                new SelectListItem { Text = "51 to 75 patients", Value = "75" },
                                new SelectListItem { Text = "76 to 100 patients", Value = "100" },
                                new SelectListItem { Text = "101 to 150 patients", Value = "150" },
                                new SelectListItem { Text = "151 to 200 patients", Value = "200" },
                                new SelectListItem { Text = "201 to 300 patients", Value = "300" },
                                new SelectListItem { Text = "over 300 patients", Value = "100000" },
                            }, "Value", "Text"); %>
                        <%= Html.DropDownList("PlanLimit", patientPackages, new { @id = "NewAgency_PlanLimit", @class = "required" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NewAgency_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", string.Empty, new { @id = "NewAgency_AddressLine1", @maxlength = "50", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", string.Empty, new { @id = "NewAgency_AddressLine2", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", string.Empty, new { @id = "NewAgency_AddressCity", @maxlength = "40", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewAgency_AddressStateCode" class="fl strong">State, Zip Code</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", string.Empty, new { @id = "NewAgency_AddressStateCode", @class = "state required" }) %>
                    <%= Html.TextBox("AddressZipCode", string.Empty, new { @id = "NewAgency_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%>
                    &nbsp;-&nbsp;<%= Html.TextBox("AddressZipCodeFour", string.Empty, new { @id = "NewAgency_AddressZipCodeFour", @class = "text required digits isValidUSZip zip-small", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>