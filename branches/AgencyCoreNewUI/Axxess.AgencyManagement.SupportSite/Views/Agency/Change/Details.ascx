﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyChangeViewData>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("ChangeUpdate", "Agency", FormMethod.Post, new { @id = "ChangeSave_Form" }))
        {%>
    <%= Html.Hidden("Id", Model.preAgencyInfo.Id, new { @id = "EditChange_Id" })%>
    <%= Html.Hidden("AgencyId", Model.preAgencyInfo.AgencyId, new { @id = "EditChange_AgencyId" })%>
    <fieldset>
        <legend>Request Information</legend>
        <div class="wide column">
            <div class="row"><label class="fl strong">Requested By:</label><div class="fr"><%= Model.preAgencyInfo.FullName%></div></div>
            <div class="row"><label class="fl strong">Requested Date:</label><div class="fr"><%= Model.preAgencyInfo.CreatedDateFormatted%></div></div>
        </div>
    </fieldset>
    <fieldset class="fl half" >
        <legend>Previous Copmany Information</legend>
        <div class="column" id ="previousInformation">
            <div class="row"><label class="fl">Company Name:</label><div name="AgencyName" class="fr"><%= Model.preAgencyInfo.Name%></div></div>
            <div class="row"><label class="fl">Tax Id:</label><div name="TaxId" class="fr"><%= Model.preAgencyInfo.TaxId%></div></div>
            <div class="row"><label class="fl">Tax Id Type:</label><div name="TaxIdType" class="fr"><%= Model.preAgencyInfo.TaxIdType%></div></div>
            <div class="row"><label class="fl">Contact Person E-mail:</label><div name="ContactPersonEmail" class="fr"><%= Model.preAgencyInfo.ContactPersonEmail%></div></div>
            <div class="row"><label class="fl">Contact Person Phone:</label><div name="ContactPersonPhone" class="fr"><%= Model.preAgencyInfo.ContactPersonPhone%></div></div>
            <div class="row"><label class="fl">CAHPS Vendor:</label><div name="CahpsVendor" class="fr"><%= Model.preAgencyInfo.CahpsVendor%></div></div>
            <div class="row"><label class="fl">National Provider Number:</label><div name="NationalProviderNumber" class="fr"><%= Model.preAgencyInfo.NationalProviderNumber%></div></div>
            <div class="row"><label class="fl">Medicare Provider Number:</label><div name="MedicareProviderNumber" class="fr"><%= Model.preAgencyInfo.MedicareProviderNumber%></div></div>
            <div class="row"><label class="fl">Medicaid Provider Number:</label><div name="MedicaidProviderNumber" class="fr"><%= Model.preAgencyInfo.MedicaidProviderNumber%></div></div>
            <div class="row"><label class="fl">Unique Agency OASIS ID Code:</label><div name="HomeHealthAgencyId" class="fr"><%= Model.preAgencyInfo.HomeHealthAgencyId%></div></div>
            <div class="row"><label class="fl">Contact Person First Name:</label><div name="ContactPersonFirstName" class="fr"><%= Model.preAgencyInfo.ContactPersonFirstName%></div></div>
            <div class="row"><label class="fl">Contact Person Last Name:</label><div name="ContactPersonLastName" class="fr"><%= Model.preAgencyInfo.ContactPersonLastName%></div></div>
            <br />
            <div class="ac row"><label class="strong">Location Information</label></div>
            <div class="row"><label class="fl">Name:</label><div name="ÄddressName" class="fr"><%= Model.preAgencyInfo.LocationName%></div></div>
            <div class="row"><label class="fl">Address Line 1:</label><div  name="ÄddressLine1" class="fr"><%= Model.preAgencyInfo.LocationAddressLine1%></div></div>
            <div class="row"><label class="fl">Address Line 2:</label><div name="ÄddressLine2" class="fr"><%= Model.preAgencyInfo.LocationAddressLine2%></div></div>
            <div class="row"><label class="fl">Address City:</label><div name="AddressCity" class="fr"><%= Model.preAgencyInfo.LocationAddressCity%></div></div>
            <div class="row"><label class="fl">Address State Code:</label><div name="AddressState" class="fr"><%= Model.preAgencyInfo.LocationAddressStateCode%></div></div>
            <div class="row"><label class="fl">Address Zip Code:</label><div name="AddressZipCode" class="fr"><%= Model.preAgencyInfo.LocationAddressZipCode%></div></div>
            <div class="row"><label class="fl">Primary Phone:</label><div name="AddressPhoneWork" class="fr"><%= Model.preAgencyInfo.LocationPhoneWork%></div></div>
            <div class="row"><label class="fl">Fax Number:</label><div name="AddressFaxNumber" class="fr"><%= Model.preAgencyInfo.LocationFaxNumber%></div></div>
        </div>
        <%--<div style="border-left:thin #333333 solid; position:absolute; left:50%; height:480px;" />--%>
    </fieldset>
    <fieldset class="fr half">
        <legend>Current Company Information</legend>
        <div class="column" id ="currentInformation">
            <div class="row"><label class="fl">Company Name:</label><div name="AgencyName" class="fr"><%= Model.curAgencyInfo.Name%></div></div>
            <div class="row"><label class="fl">Tax Id:</label><div name="TaxId" class="fr"><%= Model.curAgencyInfo.TaxId%></div></div>
            <div class="row"><label class="fl">Tax Id Type:</label><div name="TaxIdType" class="fr"><%= Model.curAgencyInfo.TaxIdType%></div></div>
            <div class="row"><label class="fl">Contact Person E-mail:</label><div name="ContactPersonEmail" class="fr"><%= Model.curAgencyInfo.ContactPersonEmail%></div></div>
            <div class="row"><label class="fl">Contact Person Phone:</label><div name="ContactPersonPhone" class="fr"><%= Model.curAgencyInfo.ContactPersonPhone%></div></div>
            <div class="row"><label class="fl">CAHPS Vendor:</label><div name="CahpsVendor" class="fr"><%= Model.curAgencyInfo.CahpsVendor%></div></div>
            <div class="row"><label class="fl">National Provider Number:</label><div name="NationalProviderNumber" class="fr"><%= Model.curAgencyInfo.NationalProviderNumber%></div></div>
            <div class="row"><label class="fl">Medicare Provider Number:</label><div name="MedicareProviderNumber" class="fr"><%= Model.curAgencyInfo.MedicareProviderNumber%></div></div>
            <div class="row"><label class="fl">Medicaid Provider Number:</label><div name="MedicaidProviderNumber" class="fr"><%= Model.curAgencyInfo.MedicaidProviderNumber%></div></div>
            <div class="row"><label class="fl">Unique Agency OASIS ID Code:</label><div name="HomeHealthAgencyId" class="fr"><%= Model.curAgencyInfo.HomeHealthAgencyId%></div></div>
            <div class="row"><label class="fl">Contact Person First Name:</label><div name="ContactPersonFirstName" class="fr"><%= Model.curAgencyInfo.ContactPersonFirstName%></div></div>
            <div class="row"><label class="fl">Contact Person Last Name:</label><div name="ContactPersonLastName"class="fr"><%= Model.curAgencyInfo.ContactPersonLastName%></div></div>
            <br />
            <div class="ac row"><label class="strong">Location Information</label></div>
            <div class="row"><label class="fl">Name:</label><div name="ÄddressName" class="fr"><%= Model.curAgencyInfo.LocationName%></div></div>
            <div class="row"><label class="fl">Address Line 1:</label><div name="ÄddressLine1" class="fr"><%= Model.curAgencyInfo.LocationAddressLine1%></div></div>
            <div class="row"><label class="fl">Address Line 2:</label><div name="ÄddressLine2" class="fr "><%= Model.curAgencyInfo.LocationAddressLine2%></div></div>
            <div class="row"><label class="fl">Address City:</label><div name="AddressCity" class="fr"><%= Model.curAgencyInfo.LocationAddressCity%></div></div>
            <div class="row"><label class="fl">Address State Code:</label><div name="AddressState" class="fr"><%= Model.curAgencyInfo.LocationAddressStateCode%></div></div>
            <div class="row"><label class="fl">Address Zip Code:</label><div name="AddressZipCode" class="fr"><%= Model.curAgencyInfo.LocationAddressZipCode%></div></div>
            <div class="row"><label class="fl">Primary Phone:</label><div name="AddressPhoneWork" class="fr"><%= Model.curAgencyInfo.LocationPhoneWork%></div></div>
            <div class="row"><label class="fl">Fax Number:</label><div name="AddressFaxNumber" class="fr"><%= Model.curAgencyInfo.LocationFaxNumber%></div></div>
        </div>
    </fieldset>
    <div class="clr" /> 
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <%= Html.TextArea("Comments", Model.preAgencyInfo.Comments, new { @id = "EditChange_Comments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <% if (!Model.preAgencyInfo.IsComplete)
               { %>
            <%= Html.Hidden("IsComplete", true, new { @id = "EditChange_Complete" })%>
            <li id="EditUpgrade_CompleteButton"><a class="save close">Complete</a></li>
            <% }
               else
               { %>
            <%= Html.Hidden("IsComplete", false, new { @id = "EditChange_NotComplete" })%>
            <li id="EditUpgrade_UnCompleteButton"><a class="save close">Not Complete</a></li>
            <% } %>
            
            <li><a class="close">Close</a></li>
        </ul>
    </div>
    <%  } %>
</div>
