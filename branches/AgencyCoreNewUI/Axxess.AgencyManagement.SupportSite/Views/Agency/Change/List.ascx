﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div class="wrapper main blue">
    <div class="buttons float-right">
        <ul><li><a href="javascript:void(0);" onclick="Agency.Change.Rebind();">Generate</a></li></ul>
        <br />
        <%  if(Model.IsUserCanExport) { %>
            <ul><li><a href="javascript:void(0);" onclick="Agency.Change.ChangeExport();">Export to Excel</a></li></ul>
        <% } %>
    </div>
    <fieldset class="ac grid-controls">
        <div id="ListAgencyChange_TopFilter" class="filter row">
            <label class="strong">Filter:</label>
            <input class="input-directions" type="text" />
            <label class="strong">Completed?</label>
            <select name="IsCompleted" id="ListAgencyChange_IsCompleted">
                <option value="all" selected>All</option>
                <option value="false">No</option>
                <option value="true">Yes</option>
            </select>
        </div>
        <div class="filter row">
            <label class="strong">Range:</label>
            <% var months = new SelectList(new[] { new SelectListItem { Text = "January", Value = "1" }, 
                                               new SelectListItem { Text = "February", Value = "2" }, 
                                               new SelectListItem { Text = "March", Value = "3" }, 
                                               new SelectListItem { Text = "April", Value = "4" }, 
                                               new SelectListItem { Text = "May", Value = "5" }, 
                                               new SelectListItem { Text = "June", Value = "6" }, 
                                               new SelectListItem { Text = "July", Value = "7" }, 
                                               new SelectListItem { Text = "August", Value = "8" }, 
                                               new SelectListItem { Text = "September", Value = "9" }, 
                                               new SelectListItem { Text = "October", Value = "10" }, 
                                               new SelectListItem { Text = "November", Value = "11" }, 
                                               new SelectListItem { Text = "December", Value = "12" }, 
                                               new SelectListItem { Text = "Custom", Value = "13"} }, "Value", "Text", DateTime.Now.Month);%>
            <%= Html.DropDownList("ListAgencyChange_Month", months, new { name = "UpgradeMonth", @id = "ListAgencyChange_Month", @style = "width:170px", @class = "oe" })%>
            <%= Html.Years("ListAgencyChange_Year", DateTime.Now.Year.ToString(), 2010, new { name = "UpgradeYear", @id = "ListAgencyChange_Year", @style = "width:170px", @class = "oe" })%>
        </div>
        <div class="filter row" id="ListAgencyChange_DateRange">
            <label class="strong">Date Range:</label>
            <input type="text" class="date-picker shortdate" name="startDate" value="<%= DateTime.Now.AddDays(-30).ToShortDateString() %>"
                id="ListAgencyChange_StartDate" />
            <label class="strong">To</label>
            <input type="text" class="date-picker shortdate" name="endDate" value="<%= DateTime.Now.ToShortDateString() %>"
                id="ListAgencyChange_EndDate" />
        </div>
    </fieldset>
    <% var actions = new List<string>();
       if (Model.IsUserCanEdit)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Change.View('<#=Id#>', '<#=AgencyId#>');return false\">Edit</a>");
       }
       var width = actions.Count * 120;
       var actionLinks = actions.ToArray().Join("");  
    %>
    <%= Html.Telerik().Grid<AgencyChange>().Name("ListAgencyChange_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns =>
    {
        columns.Bound(l => l.Id).ClientTemplate("<#= Agency.Change.GetRowIndex() #>").Title("").Width(50).Sortable(false);
        columns.Bound(l => l.Name).ClientTemplate(Model.IsUserCanView?"<a class=\"link\" onclick=\"Agency.Center.Load('<#=AgencyId#>');return false\"><#=Name#></a>":string.Empty).Title("Agency Name").Width(350).Sortable(true);
        columns.Bound(l => l.FullName).Title("Requested By").Width(200);
        columns.Bound(l => l.CreatedDateFormatted).Title("Requested Date").Width(200);
        columns.Bound(l => l.Complete).Title("Completed").Width(150);
        columns.Bound(l => l.Id).Title("Action").Sortable(false).ClientTemplate(actionLinks).Width(width).Visible(Model.IsUserCanEdit);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Change/Grid", "Agency", new { isComplete = "all", startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) }))
            .Sortable().Scrollable(scrolling => scrolling.Enabled(true))
            .ClientEvents(events => events.OnDataBound("Agency.Change.OnDataBound").OnLoad("Agency.Change.OnLoad"))%>
</div>

