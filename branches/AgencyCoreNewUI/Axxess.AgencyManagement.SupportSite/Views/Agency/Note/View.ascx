﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CustomerNote>" %>
<span class="wintitle">View Note | <%=Model.AgencyName %></span>
<div class="wrapper main">
    <fieldset>
        <legend>Details</legend>
        <div class="column fl">
            <div class="row">
                <label class="fl strong">Agency Name</label>
                <div class="fr"><a class="link" onclick="Agency.Center.Load('<%= Model.AgencyId %>');return false;"><%= Model.AgencyName %></a></div>
            </div>
            <div class="row">
                <label class="fl strong">Customer Support Representative</label>
                <div class="fr"><%= Model.RepName %></div>
            </div>
            <% if (Model.ModifiedBy != Guid.Empty) { %>
                <div class="row">
                    <label class="fl strong">Last Modified by</label>
                    <div class="fr"><%= Model.ModifiedByName %></div>
                </div>
            <% } %>
            <div class="row">
                <label class="fl strong">Date &#38; Time of Call</label>
                <div class="fr"><%= Model.Created.ToString("MM/dd/yyy hh:mm tt") %></div>
            </div>
        </div>
        <div class="column fr">
            <div class="row">
                <label class="fl strong">Sales Person</label>
                <div class="fr"><%= Model.SalesPersonName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Trainer</label>
                <div class="fr"><%= Model.TrainerName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Phone Number</label>             
                <div class="fr"><%= Model.ContactNumber %></div>
            </div>
        </div>
    </fieldset> 
    <fieldset>
        <legend>Note Information</legend>
        <div class="column fl">
            <div class="row">
                <label class="fl strong">Note Type</label>
                <div class="fr"><%= Model.NoteTypeName %></div>
            </div>
            <%if ((CustomerNoteType)Model.NoteType == CustomerNoteType.DataLoad){ %>
                <div class="row">
                    <label class="fl strong">Previous Software</label>
                    <div class="fr"><%=Model.PreviousSoftware%></div>
                </div>
            <%} %>
            <div class="row">
                <label class="fl strong">Caller</label>
                <%if (Model.CallerId != Guid.Empty) {
                      var impersonateLink = string.Format("Agency/User/LiveImpersonate/{0}/{1}", Model.AgencyId, Model.CallerId); %>
                    <div class="fr"><a href="<%=impersonateLink %>" target="_blank">
                        <%=Model.CallerName %></a></div>
                <%} else { %>
                    <div class="fr"><%= Model.CallerName %></div>
                <%} %>
            </div>
            <div class="row">
                <label class="fl strong">Sent</label>
                <div class="fr"><%= Model.Completed %></div>
            </div>
            <div class="row">
                <label class="fl strong">Summary</label>
                <div class="fr"><%= Model.Summary %></div>
            </div>
          <% if (Model.HasAttachment) { %>   
                <div class="row">
                    <label class="fl strong">Attachment</label>
                    <span id="messageAttachment" class="fr"><%= string.Format("<a class=\"link\" href=\"/Asset/{0}/{2}\">{1}</a>&#160;", Model.AttachmentId, Model.AttachmentName, Model.AgencyId)%></span>
                </div>
           <%} %>
        </div>
        <div class="column fr">
            <% if (Model.IssueId != 0) { %>
                <div class="row">
                    <label class="fl strong">Status</label>
                    <div class="fr"><%=Model.TextStatus %></div>
                </div>
            <% } %>
            <div class="row">
                <label class="fl strong">Patient Name</label>
                <div class="fr"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Episode Period</label>
                <div class="fr"><%= Model.EpisodePeriod %></div>
            </div>
            <div class="row">
                <label class="fl strong">Visit Type</label>
                <div class="fr"><%= Model.VisitType %></div>
            </div>
            <div class="row">
                <label class="fl strong">Date of Visit</label>
                <div class="fr"><%= Model.DateofVisit %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row"><%= Model.Comments %></div>
        </div>
    </fieldset>
    <%if(Model.DevUpdates.IsNotNullOrEmpty()){ %>
        <fieldset>
            <legend>Developer Comments</legend>
            <div class="wide column">
                <div class="row dev-comments"><%= Model.DevUpdates %></div>
            </div>
        </fieldset>
    <%} %>
    <div class="buttons">
        <ul>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>