﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<span class="wintitle">List Notes | <%= Model.Name %></span>
<% var template = Current.IsAxxessAdmin ? "<a class=\"link\" onclick=\"Agency.Note.View('<#=Id#>','<#=AgencyId#>');return false\">View</a> | <a class=\"link\" onclick=\"Agency.Note.Edit('<#=Id#>','<#=AgencyId#>');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Note.Delete('<#=Id#>','<#=AgencyId#>',$(this).closest('.t-grid'));return false\">Delete</a>" : "<a class=\"link\" onclick=\"Agency.Note.View('<#=Id#>','<#=AgencyId#>');return false\">View</a>"; %>
<%= Html.Hidden("DisplayName", Current.DisplayName, new { @id="NoteList_DisplayName"}) %>
<% if(!Current.IsAxxessAdmin){ %>
    <%= Html.Hidden("Current", Current.LoginId, new { @id = "NoteList_Current" }) %>
<% } %>

<div class="wrapper main blue">
    <%= Html.Hidden("AgencyId", Model.Id) %>
    <div class="fr buttons">
        <ul><li><a class="new-note">New Note</a></li></ul>
        <br />
        <ul><li><a class="export">Excel Export</a></li></ul>
    </div>
    <fieldset class="ac grid-controls">
        <div class="filter grid-search"></div>
        <span class="currentUser-tab tab">My Notes</span>
    </fieldset>
    <%= Html.Telerik().Grid<CustomerNote>().Name("ListAgencyNotes_Grid").HtmlAttributes(new { @class = "bottom-bar"}).Columns(columns =>
        {
            columns.Bound(c => c.RepName).Title("Rep Name").Width(180).Sortable(true);
            columns.Bound(c => c.Summary).Title("Summary").Sortable(true);
            columns.Bound(c => c.Preview).Title("Preview").Sortable(true);
            columns.Bound(c => c.NoteTypeName).Title("Note Type").Width(120).Sortable(true);
            columns.Bound(c => c.Completed).Title("Sent").Width(40);
            columns.Bound(c => c.CreatedFormatted).Width(150).Title("Time").Sortable(false);
            columns.Bound(c => c.Id).Title("Action").Sortable(false).Width(120).ClientTemplate(template);
        }).ClientEvents(c=>c.OnRowDataBound("Agency.Note.RowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("NoteGrid", "Agency", new { agencyId = Model.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>