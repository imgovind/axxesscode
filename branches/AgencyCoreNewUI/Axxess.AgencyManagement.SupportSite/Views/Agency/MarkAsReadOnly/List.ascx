﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div class="wrapper main blue">
    <div class="buttons float-right">
        <ul><li><a href="javascript:void(0);" onclick="Agency.MarkAsReadOnly.Rebind();">Generate</a></li></ul>
        <br />
        <%  if(Model.IsUserCanExport) { %>
            <ul><li><a href="javascript:void(0);" onclick="Agency.MarkAsReadOnly.MarkAsReadOnlyExport();">Export to Excel</a></li></ul>
        <% } %>
        
    </div>
    <fieldset class="ac grid-controls">
         <div id="ListAgencyUpgrade_TopFilter" class="filter row">
            <label class="strong">Filter:</label>
            <input class="input-directions" type="text" />
        </div>
        <div class="filter row">
            <label class="strong">Range:</label>
            <% var months = new SelectList(new[] { new SelectListItem { Text = "January", Value = "1" }, 
                                               new SelectListItem { Text = "February", Value = "2" }, 
                                               new SelectListItem { Text = "March", Value = "3" }, 
                                               new SelectListItem { Text = "April", Value = "4" }, 
                                               new SelectListItem { Text = "May", Value = "5" }, 
                                               new SelectListItem { Text = "June", Value = "6" }, 
                                               new SelectListItem { Text = "July", Value = "7" }, 
                                               new SelectListItem { Text = "August", Value = "8" }, 
                                               new SelectListItem { Text = "September", Value = "9" }, 
                                               new SelectListItem { Text = "October", Value = "10" }, 
                                               new SelectListItem { Text = "November", Value = "11" }, 
                                               new SelectListItem { Text = "December", Value = "12" }, 
                                               new SelectListItem { Text = "Custom", Value = "13"} }, "Value", "Text", DateTime.Now.Month);%>
            <%= Html.DropDownList("ListMarkAsReadOnly_Month", months, new {  name="UpgradeMonth", @id = "ListMarkAsReadOnly_Month", @style = "width:170px", @class = "oe" })%>
            <%= Html.Years("ListMarkAsReadOnly_Year", DateTime.Now.Year.ToString(), 2010, new { name = "UpgradeYear", @id = "ListMarkAsReadOnly_Year", @style = "width:170px", @class = "oe" })%>
        </div>
        <div class="filter row" id="ListMarkAsReadOnly_DateRange">
            <label class="strong">Date Range:</label>
            <input type="text" class="date-picker shortdate" name="startDate" value="<%= DateTime.Now.AddDays(-30).ToShortDateString() %>"
                id="ListMarkAsReadOnly_StartDate" />
            <label class="strong">To</label>
            <input type="text" class="date-picker shortdate" name="endDate" value="<%= DateTime.Now.ToShortDateString() %>"
                id="ListMarkAsReadOnly_EndDate" />
        </div>
    </fieldset>
    
    <%= Html.Telerik().Grid<AgencyMarkAsReadOnly>().Name("ListMarkAsReadOnly_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns =>
    {
        columns.Bound(l => l.AgencyId).ClientTemplate("<#= Agency.MarkAsReadOnly.GetRowIndex() #>").Title("").Width(30).Sortable(false);
        columns.Bound(l => l.AgencyName).ClientTemplate(Model.IsUserCanView ? "<a class=\"link\" onclick=\"Agency.Center.Load('<#=AgencyId#>');return false\"><#=AgencyName#></a>" : string.Empty).Title("Agency Name").Width(150).Sortable(true);
        columns.Bound(l => l.SuspendPayment).Title("Suspend Payment?").Width(70).Sortable(false);
        columns.Bound(l => l.Frozen).Title("Frozen?").Sortable(false).Width(50);
        columns.Bound(l => l.EffectiveDateFormatted).Title("Effective Date").Width(100).Sortable(false);
        columns.Bound(l => l.FrozenReason).Title("Frozen Reason").Sortable(false).Width(170);
        columns.Bound(l => l.CreateDateFormatted).Title("Created Date").Sortable(false).Width(100);
        //columns.Bound(l => l.ContactPersonDisplayName).Title("ContactPersonDisplayName").Sortable(false).Width(120);
        //columns.Bound(l => l.ContactPersonEmail).Title("ContactPersonEmail").Width(150);
        //columns.Bound(l => l.ContactPersonPhone).Title("ContactPersonPhone").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("MarkAsReadOnly/Grid", "Agency", new {startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) }))
            .Sortable().Scrollable(scrolling => scrolling.Enabled(true))
            .ClientEvents(events => events.OnDataBound("Agency.MarkAsReadOnly.OnDataBound").OnLoad("Agency.MarkAsReadOnly.OnLoad"))%>
</div>

