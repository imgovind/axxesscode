﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">List Location | <%= Model.AgencySnapshot.Name%></span>
<div class="wrapper main blue">
    <%= Html.Hidden("AgencyId", Model.AgencySnapshot.Id)%>
<%  if (Model.IsUserCanAdd) { %>
    <div class="fr buttons">
        <ul><li><a class="new-location">New Location</a></li></ul>
    </div>
<%  } %>
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
     <% var actions = new List<string>();
       if (Model.IsUserCanEdit)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Location.Edit('<#=Id#>','{0}');\">Edit</a>");
       }
       if (Model.IsUserCanViewLog)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Location.Logs('<#=Id#>', '{0}');return false\">Logs</a>");
       }
       var width = actions.Count * 6;
       var actionLinks = actions.ToArray().Join("");
       var editLink = string.Format(actionLinks, Model.AgencySnapshot.Id.ToString());
         %>
    
    <%= Html.Telerik().Grid<AgencyLocationViewData>().Name("ListAgencyLocations_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(l => l.Name).Title("Location Name").Width(20).Sortable(true);
            columns.Bound(l => l.ProviderNumber).Title("Provider Number").Sortable(false).Width(15);
            columns.Bound(l => l.PlanType).Title("Plan Type").Sortable(false).Width(15);
            columns.Bound(l => l.Address).Title("Address").Sortable(true).Width(50);
            columns.Bound(l => l.Phone).Title("Phone Number").Width(15);
            columns.Bound(l => l.Fax).Title("Fax Number").Width(15);
            columns.Bound(l => l.Id).ClientTemplate(editLink).Title("Action").Width(width).Visible(Model.IsUserCanEdit | Model.IsUserCanViewLog);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("LocationGrid", "Agency", new { agencyId = Model.AgencySnapshot.Id })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>