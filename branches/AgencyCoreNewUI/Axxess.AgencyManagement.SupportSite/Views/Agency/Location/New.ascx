﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySnapshot>" %>
<span class="wintitle">New Location | <%= Model.Name %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Location/Create", "Agency", FormMethod.Post, new { @id = "NewLocation_Form" })) { %>
    <%= Html.Hidden("AgencyId", Model.Id, new { @id = "NewLocation_AgencyId" })%>
    <div>
        <fieldset class="half fl">
            <legend>Information</legend>
            <div class="column">
                <div class="row">
                    <label for="NewLocation_Name" class="fl strong">Location Name</label>
                    <div class="fr"><%= Html.TextBox("Name", string.Empty, new { @id = "NewLocation_Name", @class = "required", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="NewLocation_CustomId" class="fl strong">Custom Id</label>
                    <div class="fr"><%= Html.TextBox("CustomId", "", new { @id = "NewLocation_CustomId" })%></div>
                </div>
                <div class="row">
                <div class="row">
                    <label for="NewLocation_IsUserPlan" class="fl strong">Plan Type</label>
                    <div class="fr radio">
                        <input type="radio" class="required" id="NewLocation_IsUserPlanYes" name="IsUserPlan" value="true" />
                        <label for="NewLocation_IsUserPlanYes" class="fixed shorter">User</label>
                        <input type="radio" class="required" id="NewLocation_IsUserPlanNo" value="false" name="IsUserPlan" />
                        <label for="NewLocation_IsUserPlanNo" class="fixed shorter">Patient</label>
                    </div>
                </div>
                <div class="row" id="NewLocation_UserSubscriptionPlan">
                    <label for="NewLocation_Package" class="fl strong">User Subscription Plan</label>
                    <div class="fr">
                        <%  var userPackages = new SelectList(new[] {
                                new SelectListItem { Text = "** Select Plan **", Value = "" },
                                new SelectListItem { Text = "1 to 5 users", Value = "5" },
                                new SelectListItem { Text = "6 to 10 users", Value = "10" },
                                new SelectListItem { Text = "11 to 20 users", Value = "20" },
                                new SelectListItem { Text = "21 to 30 users", Value = "30" },
                                new SelectListItem { Text = "31 to 40 users", Value = "40" },
                                new SelectListItem { Text = "41 to 100 users", Value = "100" },
                                new SelectListItem { Text = "101 to 200 users", Value = "200" },
                                new SelectListItem { Text = "Over 200 users", Value = "100000" },
                            }, "Value", "Text"); %>
                        <%= Html.DropDownList("PlanLimit", userPackages, new { @id = "NewLocation_PlanLimit", @class = "required" })%>
                    </div>
                </div>
                <div class="row" id="NewLocation_PatientSubscriptionPlan">
                    <label for="NewLocation_Package" class="fl strong">Patient Subscription Plan</label>
                    <div class="fr">
                        <%  var patientPackages = new SelectList(new[] {
                                new SelectListItem { Text = "** Select Plan **", Value = "" },
                                new SelectListItem { Text = "1 to 25 patients", Value = "25" },
                                new SelectListItem { Text = "26 to 50 patients", Value = "50" },
                                new SelectListItem { Text = "51 to 75 patients", Value = "75" },
                                new SelectListItem { Text = "76 to 100 patients", Value = "100" },
                                new SelectListItem { Text = "101 to 150 patients", Value = "150" },
                                new SelectListItem { Text = "151 to 200 patients", Value = "200" },
                                new SelectListItem { Text = "201 to 300 patients", Value = "300" },
                                new SelectListItem { Text = "over 300 patients", Value = "100000" },
                            }, "Value", "Text"); %>
                        <%= Html.DropDownList("PlanLimit", patientPackages, new { @id = "NewLocation_PlanLimit", @class = "required" })%>
                    </div>
                </div>
            </div>
            </div>
        </fieldset> 
        <fieldset class="half fr">
            <legend>OASIS Submission Information (Medicare)</legend>
            <div class="column">
                <div class="row">
                    <label for="NewLocation_BranchID" class="fl strong"><span class="green">(M0016)</span> Branch ID Number</label>
                    <div class="fr">
                        <%  var branchId = new SelectList(new[] {
                                new SelectListItem { Text = "-- Select Branch Id --", Value = "0" },
                                new SelectListItem { Text = "N", Value = "N" },
                                new SelectListItem { Text = "P", Value = "P" },
                                new SelectListItem { Text = "Other", Value = "Other" }
                            }, "Value", "Text", "0"); %>
                        <%= Html.DropDownList("BranchId", branchId, new { @id = "NewLocation_BranchId", @class = "required notzero" })%>
                    </div>
                    <div class="clr"></div>
                    <div class="fr"><%= Html.TextBox("BranchIdOther", string.Empty, new { @id = "NewLocation_BranchIdOther" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="clr"></div>
    <fieldset>
        <legend>Branch Information</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsLocationStandAlone", false, new { @id = "NewLocation_IsLocationStandAlone" })%>
                        <label for="NewLocation_IsLocationStandAlone">Check here if this branch has a different Medicare Provider Number</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div id="NewLocation_StandAloneInfoContent"><% Html.RenderPartial("Location/StandAloneInfo", new AgencyLocation()); %></div>
    </fieldset>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row">
                <label for="NewLocation_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%= Html.TextBox("AddressLine1", string.Empty, new { @id = "NewLocation_AddressLine1", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewLocation_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%= Html.TextBox("AddressLine2", string.Empty, new { @id = "NewLocation_AddressLine2" })%></div>
            </div>
            <div class="row">
                <label for="NewLocation_AddressCity" class="fl strong">City</label>
                <div class="fr"><%= Html.TextBox("AddressCity", string.Empty, new { @id = "NewLocation_AddressCity", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewLocation_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "NewLocation_AddressStateCode", @class = "state required" })%>
                    <%= Html.TextBox("AddressZipCode", string.Empty, new { @id = "NewLocation_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%>
                    &nbsp;-&nbsp;<%= Html.TextBox("AddressZipCodeFour", string.Empty, new { @id = "NewLocation_AddressZipCodeFour", @class = "text required digits isValidUSZip zip-small", @maxlength = "4" })%>
                </div>
            </div>
        </div>   
        <div class="column">   
            <div class="row">
                <label for="NewLocation_PhoneArray1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <input type="text" class="numeric required phone-short" name="PhoneArray" id="NewLocation_PhoneArray1" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-short" name="PhoneArray" id="NewLocation_PhoneArray2" maxlength="3" />
                    -
                    <input type="text" class="numeric required phone-long" name="PhoneArray" id="NewLocation_PhoneArray3" maxlength="4" />
                </div>
            </div>
            <div class="row">
                <label for="NewLocation_FaxNumberArray1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewLocation_FaxNumberArray1" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-short" name="FaxNumberArray" id="NewLocation_FaxNumberArray2" maxlength="3" />
                    -
                    <input type="text" class="numeric phone-long" name="FaxNumberArray" id="NewLocation_FaxNumberArray3" maxlength="4" />
                </div>
            </div> 
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input type="checkbox" name="IsMainOffice" id="NewLocation_IsMainOffice" />
                        <label for="NewLocation_IsMainOffice" class="fl strong">Is Main Office</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="wide column">
            <div class="row">
                <label for="NewLocation_Comments" class="strong">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", string.Empty, new { @id = "NewLocation_Comments", @maxcharacters = "500" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>