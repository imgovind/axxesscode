﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  var type = Model != null && Model.Id != Guid.Empty ? "Edit" : "New"; %>
<div class="column">
    <div class="row">
        <label for="<%=type %>Location_TaxId" class="fl strong">Tax Id</label>
        <div class="fr"><%= Html.TextBox("TaxId", Model.TaxId, new { @id = type + "Location_TaxId", @maxlength = "10", @class = "required" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_TaxIdType" class="fl strong">Tax Id Type</label>
        <div class="fr">
            <%  var taxIdTypes = new SelectList(new[] {
                    new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" },
                    new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" },
                    new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" }
                }, "Value", "Text", Model.TaxIdType); %>
            <%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = type + "Location_TaxIdType" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_Payor" class="fl strong">Payor</label>
        <div class="fr">
            <%  var payors = new SelectList(new[] {
                    new SelectListItem { Text = "** Select Payor **", Value = "0" },
                    new SelectListItem { Text = "Palmetto GBA", Value = "1" },
                    new SelectListItem { Text = "National Government Services (formerly known as United Government Services)", Value = "2" },
                    new SelectListItem { Text = "Blue Cross Blue Shield of Alabama (AKA Cahaba GBA)", Value = "3" },
                    new SelectListItem { Text = "Anthem Health Plans of Maine", Value = "4" }
                }, "Value", "Text", Model.Payor); %>
            <%= Html.DropDownList("Payor", payors, new { @id = type + "Location_Payor" })%>
        </div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_NationalProviderNo" class="fl strong">National Provider Number</label>
        <div class="fr"><%= Html.TextBox("NationalProviderNumber", Model.NationalProviderNumber, new { @id = type + "Location_NationalProviderNo", @maxlength = "10" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_MedicareProviderNo" class="fl strong">Medicare Provider Number</label>
        <div class="fr"><%= Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = type + "Location_MedicareProviderNo", @maxlength = "10" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_MedicaidProviderNo" class="fl strong">Medicaid Provider Number</label>
        <div class="fr"><%= Html.TextBox("MedicaidProviderNumber", Model.MedicaidProviderNumber, new { @id = type + "Location_MedicaidProviderNo", @maxlength = "10" })%></div>
    </div>
</div>
<div class="column">
    <div class="row">
        <label for="<%=type %>Location_HomeHealthAgencyId" class="fl strong">Unique Agency OASIS ID Code</label>
        <div class="fr"><%= Html.TextBox("HomeHealthAgencyId", Model.HomeHealthAgencyId, new { @id = type + "Location_HomeHealthAgencyId", @maxlength = "20" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_CahpsVendor" class="fl strong">CAHPS Vendor</label>
        <div class="fr"><%= Html.LookupSelectList(SelectListTypes.CahpsVendors, "CahpsVendor", Model.CahpsVendor.ToString(), new { @id = type + "Location_CahpsVendor" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_ContactPersonFirstName" class="fl strong">Contact Person First Name</label>
        <div class="fr"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = type + "Location_ContactPersonFirstName", @maxlength = "50", @class = "required" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_ContactPersonLastName" class="fl strong">Contact Person Last Name</label>
        <div class="fr"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = type + "Location_ContactPersonLastName", @class = "required", @maxlength = "30" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_ContactPersonEmail" class="fl strong">Contact Person E-mail</label>
        <div class="fr"><%= Html.TextBox("ContactPersonEmail", Model.ContactPersonEmail, new { @id = type + "Location_ContactPersonEmail", @class = "required", @maxlength = "40" })%></div>
    </div>
    <div class="row">
        <label for="<%=type %>Location_ContactPhone1" class="fl strong">Contact Person Phone</label>
        <div class="fr">
            <%= Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(0, 3) : string.Empty, new { @id = type + "Location_ContactPhone1", @class = "required numeric phone-short", @maxlength = "3" })%>
            -
            <%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(3, 3) : string.Empty, new { @id = type + "Location_ContactPhone2", @class = "required numeric phone-short", @maxlength = "3" })%>
            -
            <%=Html.TextBox("ContactPhoneArray", Model.ContactPersonPhone.IsNotNullOrEmpty() ? Model.ContactPersonPhone.Substring(6, 4) : string.Empty, new { @id = type + "Location_ContactPhone3", @class = "required numeric phone-long", @maxlength = "4" })%>
        </div>
    </div>
</div>
<div class="column">
    <div class="row">
        <div class="wide checkgroup">
            <div class="option">
                <%= Html.CheckBox("IsAxxessTheBiller", Model.IsAxxessTheBiller, new { @id = type + "Location_IsAxxessTheBiller" })%>
                <label for="<%=type %>Location_IsAxxessTheBiller">Check here if Axxess&trade; HealthCare Consult handles billing for this Agency</label>
            </div>
        </div>
    </div>
</div>        
<div class="clr"></div>
<div id="<%=type %>Location_SubmiterInfoContent">
    <div class="column">
        <div class="row">
            <label for="<%=type %>Location_SubmitterId" class="fl strong">Submitter Id</label>
            <div class="fr"><%= Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = type + "Location_SubmitterId", @maxlength = "15", @class = "required" })%></div>
        </div>
        <div class="row">
            <label for="<%=type %>Location_SubmitterName" class="fl strong">Submitter Name</label>
            <div class="fr"><%=Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = type + "Location_SubmitterName", @maxlength = "50", @class = "required" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label for="<%=type %>Location_SubmitterPhone1" class="fl strong">Submitter Phone Number</label>
            <div class="fr">
                <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 3 ? Model.SubmitterPhone.Substring(0, 3) : string.Empty, new { @id = type + "Location_SubmitterPhone1", @class = "numeric phone-short required", @maxlength = "3" })%>
                -
                <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 6 != null && Model.SubmitterPhone != "" ? Model.SubmitterPhone.Substring(3, 3) : string.Empty, new { @id = type + "Location_SubmitterPhone2", @class = "numeric phone-short required", @maxlength = "3" })%>
                -
                <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() && Model.SubmitterPhone.Length >= 10 != null && Model.SubmitterPhone != "" ? Model.SubmitterPhone.Substring(6, 4) : string.Empty, new { @id = type + "Location_SubmitterPhone3", @class = "required digits phone-long", @maxlength = "4" })%>
            </div>
        </div>
        <div class="row">
            <label for="<%=type %>Location_SubmitterFax1" class="fl strong">Submitter Fax Number</label>
            <div class="fr">
                <%= Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 3 ? Model.SubmitterFax.Substring(0, 3) : string.Empty, new { @id = type + "Location_SubmitterFax1", @class = "numeric phone-short required", @maxlength = "3" })%>
                -
                <%= Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 6 != null && Model.SubmitterFax != "" ? Model.SubmitterFax.Substring(3, 3) : string.Empty, new { @id = type + "Location_SubmitterFax2", @class = "numeric phone-short required", @maxlength = "3" })%>
                -
                <%= Html.TextBox("SubmitterFaxArray", Model.SubmitterFax.IsNotNullOrEmpty() && Model.SubmitterFax.Length >= 10 != null && Model.SubmitterFax != "" ? Model.SubmitterFax.Substring(6, 4) : string.Empty, new { @id = type + "Location_SubmitterFax3", @class = "required digits phone-long", @maxlength = "4" })%>
            </div>
        </div>
    </div>
</div>