﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">Edit Location | <%= Model != null ? Model.AgencyLocation.Name.ToTitleCase() : string.Empty%></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Location/Update", "Agency", FormMethod.Post, new { @id = "EditLocation_Form" })) { %>
    <%= Html.Hidden("Id", Model.AgencyLocation.Id, new { @id = "EditLocation_Id" })%>
    <%= Html.Hidden("AgencyId", Model.AgencyLocation.AgencyId, new { @id = "EditLocation_AgencyId" })%>
    <div>
        <fieldset class="half fl">
            <legend>Location Information</legend>
            <div class="column">
                <div class="row">
                    <label for="EditLocation_Name" class="fl strong">Name</label>
                    <div class="fr"><%= Html.TextBox("Name", Model.AgencyLocation.Name, new { @id = "EditLocation_Name", @class = "required", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="EditLocation_CustomId" class="fl strong">Custom ID</label>
                    <div class="fr"><%=Html.TextBox("CustomId", Model.AgencyLocation.CustomId, new { @id = "EditLocation_CustomId" })%></div>
                </div>
                <div class="row">
                    <label for="EditLocation_IsUserPlan" class="fl strong">Plan Type</label>
                    <div class="fr radio">
                        <%= Html.RadioButton("IsUserPlan", "true", Model.SubscriptionPlan != null ? Model.SubscriptionPlan.IsUserPlan : false, new { @id = "EditLocation_IsUserPlanYes", @class = "required" })%>
                        <label for="EditLocation_IsUserPlanYes" class="fixed shorter">User</label>
                        <%= Html.RadioButton("IsUserPlan", "false", Model.SubscriptionPlan != null ? !Model.SubscriptionPlan.IsUserPlan : false, new { @id = "EditLocation_IsUserPlanNo", @class = "required" })%>
                        <label for="EditLocation_IsUserPlanNo" class="fixed shorter">Patient</label>
                    </div>
                </div>
                <div class="row" id="EditLocation_UserSubscriptionPlan">
                    <label for="EditLocation_Package" class="fl strong">User Subscription Plan</label>
                    <div class="fr">
                        <%  var userPackages = new SelectList(new[] {
                                new SelectListItem { Text = "** Select Plan **", Value = "" },
                                new SelectListItem { Text = "1 to 5 users", Value = "5" },
                                new SelectListItem { Text = "6 to 10 users", Value = "10" },
                                new SelectListItem { Text = "11 to 20 users", Value = "20" },
                                new SelectListItem { Text = "21 to 30 users", Value = "30" },
                                new SelectListItem { Text = "31 to 40 users", Value = "40" },
                                new SelectListItem { Text = "41 to 100 users", Value = "100" },
                                new SelectListItem { Text = "101 to 200 users", Value = "200" },
                                new SelectListItem { Text = "Over 200 users", Value = "100000" },
                            }, "Value", "Text", Model.SubscriptionPlan != null ? Model.SubscriptionPlan.PlanLimit : 0); %>
                        <%= Html.DropDownList("PlanLimit", userPackages, new { @id = "EditLocation_PlanLimit", @class = "required" })%>
                    </div>
                </div>
                <div class="row" id="EditLocation_PatientSubscriptionPlan">
                    <label for="EditLocation_Package" class="fl strong">Patient Subscription Plan</label>
                    <div class="fr">
                        <%  var patientPackages = new SelectList(new[] {
                                new SelectListItem { Text = "** Select Plan **", Value = "" },
                                new SelectListItem { Text = "1 to 25 patients", Value = "25" },
                                new SelectListItem { Text = "26 to 50 patients", Value = "50" },
                                new SelectListItem { Text = "51 to 75 patients", Value = "75" },
                                new SelectListItem { Text = "76 to 100 patients", Value = "100" },
                                new SelectListItem { Text = "101 to 150 patients", Value = "150" },
                                new SelectListItem { Text = "151 to 200 patients", Value = "200" },
                                new SelectListItem { Text = "201 to 300 patients", Value = "300" },
                                new SelectListItem { Text = "over 300 patients", Value = "100000" },
                            }, "Value", "Text", Model.SubscriptionPlan != null ? Model.SubscriptionPlan.PlanLimit : 0); %>
                        <%= Html.DropDownList("PlanLimit", patientPackages, new { @id = "EditLocation_PlanLimit", @class = "required" })%>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset class="half fr">
            <legend>OASIS Submission (Medicare)</legend>
            <div class="column">
                <div class="row">
                    <label for="EditLocation_BranchID" class="fl strong"><span class="green">(M0016)</span> Branch ID Number</label>
                    <div class="fr">
                        <%  var branchId = new SelectList(new[] {
                                new SelectListItem { Text = "-- Select Branch Id --", Value = "0" },
                                new SelectListItem { Text = "N", Value = "N" },
                                new SelectListItem { Text = "P", Value = "P" },
                                new SelectListItem { Text = "Other", Value = "Other" }
                            }, "Value", "Text", Model.AgencyLocation.BranchId.IsNotNullOrEmpty() ? Model.AgencyLocation.BranchId : "0"); %>
                        <%= Html.DropDownList("BranchId", branchId, new { @id = "EditLocation_BranchId", @class = "required notzero" })%>
                    </div>
                    <div class="clr"></div>
                    <div class="fr"><%= Html.TextBox("BranchIdOther", Model.AgencyLocation.BranchIdOther, new { @id = "EditLocation_BranchIdOther" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="clr"></div>
    <fieldset>
        <legend>Branch Information</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsLocationStandAlone", Model.AgencyLocation.IsLocationStandAlone, new { @id = "EditLocation_IsLocationStandAlone" })%>
                        <label for="EditLocation_IsLocationStandAlone">Check here if this branch office Stand Alone</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div id="EditLocation_StandAloneInfoContent"><% Html.RenderPartial("Location/StandAloneInfo", Model.AgencyLocation); %></div>
    </fieldset> 
    <fieldset>
        <legend>Location Address</legend>
        <div class="column">
            <div class="row">
                <label for="EditLocation_AddressLine1" class="fl strong">Address Line 1</label>
                <div class="fr"><%=Html.TextBox("AddressLine1", Model.AgencyLocation.AddressLine1, new { @id = "EditLocation_AddressLine1", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditLocation_AddressLine2" class="fl strong">Address Line 2</label>
                <div class="fr"><%=Html.TextBox("AddressLine2", Model.AgencyLocation.AddressLine2, new { @id = "EditLocation_AddressLine2" })%></div>
            </div>
            <div class="row">
                <label for="EditLocation_AddressCity" class="fl strong">City</label>
                <div class="fr"><%=Html.TextBox("AddressCity", Model.AgencyLocation.AddressCity, new { @id = "EditLocation_AddressCity", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditLocation_AddressStateCode" class="fl strong">State, Zip</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AgencyLocation.AddressStateCode, new { @id = "EditLocation_AddressStateCode", @class = "state required" })%>
                    <%= Html.TextBox("AddressZipCode", Model.AgencyLocation.AddressZipCode, new { @id = "EditLocation_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%>
                    &nbsp;-&nbsp;<%= Html.TextBox("AddressZipCodeFour", Model.AgencyLocation.AddressZipCodeFour, new { @id = "Edit_Location_AddressZipCodeFour", @class = "text required digits isValidUSZip zip-small", @maxlength = "4" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditLocation_PhoneArray1" class="fl strong">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneArray", Model.AgencyLocation.PhoneWork.IsNotNullOrEmpty() && Model.AgencyLocation.PhoneWork.Length >= 3 ? Model.AgencyLocation.PhoneWork.Substring(0, 3) : string.Empty, new { @id = "EditLocation_PhoneArray1", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%=Html.TextBox("PhoneArray", Model.AgencyLocation.PhoneWork.IsNotNullOrEmpty() && Model.AgencyLocation.PhoneWork.Length >= 6 ? Model.AgencyLocation.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Location_PhoneArray2", @class = "numeric required phone-short", @maxlength = "3" })%>
                    -
                    <%=Html.TextBox("PhoneArray", Model.AgencyLocation.PhoneWork.IsNotNullOrEmpty() && Model.AgencyLocation.PhoneWork.Length >= 10 ? Model.AgencyLocation.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Location_PhoneArray3", @class = "numeric required phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditLocation_FaxNumberArray1" class="fl strong">Fax Number</label>
                <div class="fr">
                    <%= Html.TextBox("FaxNumberArray", Model.AgencyLocation.FaxNumber.IsNotNullOrEmpty() && Model.AgencyLocation.FaxNumber.Length >= 3 ? Model.AgencyLocation.FaxNumber.Substring(0, 3) : string.Empty, new { @id = "EditLocation_FaxNumberArray1", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%=Html.TextBox("FaxNumberArray", Model.AgencyLocation.FaxNumber.IsNotNullOrEmpty() && Model.AgencyLocation.FaxNumber.Length >= 6 ? Model.AgencyLocation.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Location_FaxNumberArray2", @class = "numeric phone-short", @maxlength = "3" })%>
                    -
                    <%=Html.TextBox("FaxNumberArray", Model.AgencyLocation.FaxNumber.IsNotNullOrEmpty() && Model.AgencyLocation.FaxNumber.Length >= 10 ? Model.AgencyLocation.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Location_FaxNumberArray3", @class = "numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
        <div class="wide column">
            <div class="row">
                <label for="EditLocation_Comments" class="strong">Comments</label>
                <div class="ac"><%= Html.TextArea("Comments", Model.AgencyLocation.Comments, new { @id = "EditLocation_Comments", @maxcharacters = "500" })%></div>
            </div>
        </div>
    </fieldset>
    <%if (Model.IsUserCanViewLog) { %>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <% } %>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>