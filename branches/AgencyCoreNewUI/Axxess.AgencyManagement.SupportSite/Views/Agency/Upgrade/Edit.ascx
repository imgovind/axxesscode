﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyUpgrade>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Upgrade/Update", "Agency", FormMethod.Post, new { @id = "EditUpgrade_Form" }))
        {%>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditUpgrade_Id" }) %>
    <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "EditUpgrade_AgencyId" }) %>
    <%= Html.Hidden("AccountId", Model.AccountId, new { @id = "EditUpgrade_AccountId" })%>
    <%= Html.Hidden("RequestedPackageId", Model.RequestedPackageId, new { @id = "EditUpgrade_RequestedPackageId" })%>
    <fieldset>
        <legend>Request Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Agency Name</label>
                <div class="fr">
                    <%= Model.AgencyName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Previous Subscription Plan</label>
                <div class="fr">
                    <%= Model.PreviousPackageDescription %></div>
            </div>
            <div class="row">
                <label class="fl strong">Requested Subscription Plan</label>
                <div class="fr">
                    <%= Model.RequestedPackageDescription %></div>
            </div>
            <div class="row">
                <label class="fl strong">Annual Subscription Plan</label>
                <div class="fr">
                    <%= Model.AnnualPlanDescription %></div>
            </div>
            <div class="row">
                <label class="fl strong">Requested By</label>
                <div class="fr">
                    <%= Model.RequestedBy %></div>
            </div>
            <div class="row">
                <label class="fl strong">Requested On</label>
                <div class="fr">
                    <%= Model.Created.ToString("MM/dd/yyy hh:mm:ss tt") %></div>
            </div>
        </div>
        <div class="column">
         <div class="row">
                <label class="fl strong">Request Package Id</label>
                <div class="fr"><%= Model.RequestedPackageId%></div>
            </div>
            <div class="row">
                <label class="fl strong">Account Id</label>
                <div class="fr"><%= Model.AccountId %></div>
            </div>
            <div class="row">
                <label class="fl strong">Old Pricing</label>
               <div class="fr"><%=Html.TextBox("PreviousAmount", Model.PreviousAmount == -1 ? string.Empty : Model.PreviousAmount.ToString(), new { @id = "EditUpgrade_PreviousAmount", @class = "text decimal input_wrapper" })%>
                    </div>
            </div>
            <div class="row">
                <label class="fl strong">New Pricing</label>
               <div class="fr"><%=Html.TextBox("Amount", Model.Amount == -1 ? string.Empty : Model.Amount.ToString(), new { @id = "EditUpgrade_Amount", @class = "text decimal input_wrapper" })%>
                    </div>
            </div>
            <div class="row">
                <label class="fl strong">Effective Date</label>
               <div class="fr"><%=Html.TextBox("EffectiveDate", Model.EffectiveDate.ToString("MM/dd/yyyy"), new { @id = "EditUpgrade_EffectiveDate", @class = "date-picker complete-required" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
             <div class="row ac">
                <%= Html.TextArea("Comments", Model.Comments, new { @id = "EditUpgrade_Comments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
           <li><a onclick=" UpgradeSave();">Save</a></li>
            <% if (!Model.IsComplete)
               { %>
            <%= Html.Hidden("IsComplete", true, new { @id = "EditUpgrade_Complete" }) %>
            <li id="EditUpgrade_CompleteButton"><a class="save close">Complete</a></li>
            <% }
               else
               { %>
            <%= Html.Hidden("IsComplete", false, new { @id = "EditUpgrade_NotComplete" }) %>
            <li id="EditUpgrade_UnCompleteButton"><a class="save close">Not Complete</a></li>
            <% } %>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
    <%  } %>
</div>
<script type="text/javascript">
    function UpgradeSave() {
        U.PostUrl("Agency/UpgradeSave", $("#EditUpgrade_Form").serialize(), function(data) {
            U.Growl(data.errorMessage, data.isSuccessful ? "success" : "error");
        }, function() {
            U.Growl("An error occured while saving the upgrade.", "error");
        });
    }
</script>

