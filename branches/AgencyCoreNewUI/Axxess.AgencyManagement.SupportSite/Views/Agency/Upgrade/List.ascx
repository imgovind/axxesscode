﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div class="wrapper main blue">
    <div class="buttons float-right">
        <ul><li><a href="javascript:void(0);" onclick="Agency.Upgrade.Rebind();">Generate</a></li></ul>
        <br />
        <%  if(Model.IsUserCanExport) { %>
            <ul><li><a href="javascript:void(0);" onclick="Agency.Upgrade.UpgradeExport();">Export to Excel</a></li></ul>
        <% } %>
        
    </div>
    <fieldset class="ac grid-controls">
        <div id="ListAgencyUpgrade_TopFilter" class="filter row">
            <label class="strong">Filter:</label>
            <input class="input-directions" type="text" />
            <label class="strong">Completed?</label>
            <select name="IsCompleted" id="ListAgencyUpgrade_IsCompleted">
                <option value="all" selected>All</option>
                <option value="false">No</option>
                <option value="true">Yes</option>
            </select>
        </div>
        <div class="filter row">
            <label class="strong">Range:</label>
            <% var months = new SelectList(new[] { new SelectListItem { Text = "January", Value = "1" }, 
                                               new SelectListItem { Text = "February", Value = "2" }, 
                                               new SelectListItem { Text = "March", Value = "3" }, 
                                               new SelectListItem { Text = "April", Value = "4" }, 
                                               new SelectListItem { Text = "May", Value = "5" }, 
                                               new SelectListItem { Text = "June", Value = "6" }, 
                                               new SelectListItem { Text = "July", Value = "7" }, 
                                               new SelectListItem { Text = "August", Value = "8" }, 
                                               new SelectListItem { Text = "September", Value = "9" }, 
                                               new SelectListItem { Text = "October", Value = "10" }, 
                                               new SelectListItem { Text = "November", Value = "11" }, 
                                               new SelectListItem { Text = "December", Value = "12" }, 
                                               new SelectListItem { Text = "Custom", Value = "13"} }, "Value", "Text", DateTime.Now.Month);%>
            <%= Html.DropDownList("ListAgencyUpgrade_Month", months, new {  name="UpgradeMonth", @id = "ListAgencyUpgrade_Month", @style = "width:170px", @class = "oe" })%>
            <%= Html.Years("ListAgencyUpgrade_Year", DateTime.Now.Year.ToString(), 2010, new { name = "UpgradeYear", @id = "ListAgencyUpgrade_Year", @style = "width:170px", @class = "oe" })%>
        </div>
        <div class="filter row" id="ListAgencyUpgrade_DateRange">
            <label class="strong">Date Range:</label>
            <input type="text" class="date-picker shortdate" name="startDate" value="<%= DateTime.Now.AddDays(-30).ToShortDateString() %>"
                id="ListAgencyUpgrade_StartDate" />
            <label class="strong">To</label>
            <input type="text" class="date-picker shortdate" name="endDate" value="<%= DateTime.Now.ToShortDateString() %>"
                id="ListAgencyUpgrade_EndDate" />
        </div>
    </fieldset>
    <% var actions = new List<string>();
       if (Model.IsUserCanViewDetail) 
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Upgrade.View('<#=Id#>', '<#=AgencyId#>');return false\">View</a>"); 
       }
       if (Model.IsUserCanEditDetail)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Upgrade.Edit('<#=Id#>', '<#=AgencyId#>');return false\">Edit</a>");
       }
       var width = actions.Count * 45;
       var actionLinks = actions.ToArray().Join("");
        
    %>
    <%= Html.Telerik().Grid<AgencyUpgrade>().Name("ListAgencyUpgrade_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns =>
    {
        columns.Bound(l => l.Id).ClientTemplate("<#= Agency.Upgrade.GetRowIndex() #>").Title("").Width(40).Sortable(false);
        columns.Bound(l => l.AgencyName).ClientTemplate(Model.IsUserCanView ? "<a class=\"link\" onclick=\"Agency.Center.Load('<#=AgencyId#>');return false\"><#=AgencyName#></a>" : string.Empty).Title("Agency Name").Width(200).Sortable(true);
        columns.Bound(l => l.LocationName).Title("Location").Width(150).Sortable(false);
        columns.Bound(l => l.PreviousPackageDescription).Title("Old Plan").Sortable(false);
        columns.Bound(l => l.RequestedPackageDescription).Title("New Plan").Sortable(false);
        columns.Bound(l => l.RequestedBy).Title("Requested By").Width(100);
        columns.Bound(l => l.CreatedDateFormatted).Title("Requested Date").Width(100);
        columns.Bound(l => l.PreviousAmount).Title("Old Pricing").Sortable(false).Width(80);
        columns.Bound(l => l.Amount).Title("New Pricing").Sortable(false).Width(80);
        columns.Bound(l => l.Complete).Title("Completed").Sortable(false).Width(70);
        columns.Bound(l => l.Id).Title("Action").Sortable(false).ClientTemplate(actionLinks).Width(width).Visible(Model.IsUserCanViewDetail | Model.IsUserCanEditDetail);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("Upgrade/Grid", "Agency", new { isComplete = "all", startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) }))
            .Sortable().Scrollable(scrolling => scrolling.Enabled(true))
            .ClientEvents(events => events.OnDataBound("Agency.Upgrade.OnDataBound").OnLoad("Agency.Upgrade.OnLoad"))%>
</div>

