﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyUpgrade>" %>
<div class="wrapper main">
    <%  using (Html.BeginForm("Upgrade/Update", "Agency", FormMethod.Post, new { @id = "ViewUpgrade_Form" }))
        {%>
    <%= Html.Hidden("Id", Model.Id, new { @id = "ViewUpgrade_Id" }) %>
    <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "ViewUpgrade_AgencyId" })%>
    <%= Html.Hidden("AccountId", Model.AccountId, new { @id = "ViewUpgrade_AccountId" })%>
    <%= Html.Hidden("RequestedPackageId", Model.RequestedPackageId, new { @id = "ViewUpgrade_RequestedPackageId" })%>
    <fieldset>
        <legend>Request Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Agency Name</label>
                <div class="fr">
                    <%= Model.AgencyName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Previous Subscription Plan</label>
                <div class="fr">
                    <%= Model.PreviousPackageDescription %></div>
            </div>
            <div class="row">
                <label class="fl strong">Requested Subscription Plan</label>
                <div class="fr">
                    <%= Model.RequestedPackageDescription %></div>
            </div>
            <div class="row">
                <label class="fl strong">Annual Subscription Plan</label>
                <div class="fr">
                    <%= Model.AnnualPlanDescription %></div>
            </div>
            <div class="row">
                <label class="fl strong">Requested By</label>
                <div class="fr">
                    <%= Model.RequestedBy %></div>
            </div>
            <div class="row">
                <label class="fl strong">Requested On</label>
                <div class="fr">
                    <%= Model.Created.ToString("MM/dd/yyy hh:mm:ss tt") %></div>
            </div>
        </div>
        <div class="column">
         <div class="row">
                <label class="fl strong">Request Package Id</label>
                <div class="fr"><%= Model.RequestedPackageId%></div>
            </div>
            <div class="row">
                <label class="fl strong">Account Id</label>
                <div class="fr"><%= Model.AccountId %></div>
            </div>
            <div class="row">
                <label class="fl strong">Old Pricing</label>
                <div class="fr"><%= Model.PreviousAmount == -1 ? "0" : Model.PreviousAmount.ToString()%></div>
            </div>
            <div class="row">
                <label class="fl strong">New Pricing</label>
                <div class="fr"><%= Model.Amount == -1 ? "0" : Model.Amount.ToString()%></div>
            </div>
            <div class="row">
                <label class="fl strong">Effective Date</label>
                <div class="fr"><%= Model.EffectiveDate.ToString("MM/dd/yyyy")%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row">
                <%= Model.Comments %></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
    <%  } %>
</div>
