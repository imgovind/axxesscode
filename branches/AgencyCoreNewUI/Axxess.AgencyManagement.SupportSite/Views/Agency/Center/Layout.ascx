﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">Agency Center</span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top"><% Html.RenderPartial("Center/AgencyFilters"); %></div>
        <div class="bottom new-agency"><% Html.RenderPartial("Center/AgencySelector"); %></div>
    </div>
    <div id="AgencyMainResult" class="ui-layout-center"></div>
</div>
<%--<% if (Model == null || Model.Count == 0) { %>
   <script type="text/javascript">
       $("#AgencyMainResult").html(U.MessageWarn("No Agency Found", "There are no agencies to be found which match your search critieria. Please try a different search."));
   </script>
<% } %>--%>