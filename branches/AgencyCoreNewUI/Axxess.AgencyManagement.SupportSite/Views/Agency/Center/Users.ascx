﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<%= Html.Hidden("AgencyId", Model.Agency.Id) %>
<%var actions = new List<string>();
  if (Model.IsUserCanEditAgencyUser) 
  {
    actions.Add("<a class=\"link\" onclick=\"Agency.User.Edit('<#=LoginId#>');return false\">Edit</a>");
  }
  //if (Model.IsUserCanQALoginAsUser)
  //{
  //    actions.Add("<a class='link' href='Agency/User/TestImpersonate/" + Model.Agency.Id + "/<#=Id#>' target='_blank'>QA Login</a>");
  //}
  if (Model.IsUserCanLiveLoginAsUser)
  {
      actions.Add("<a class='link' href='Agency/User/LiveImpersonate/"+Model.Agency.Id+"/<#=Id#>' target='_blank'>Live Login</a>");
  }
  if (Model.IsUserCanSendActiveLink)
  {
      actions.Add("<a class='link' onclick=\"Agency.User.ResendActivationLink('<#=Id#>','" + Model.Agency.Id + "');return false\">Activate Link</a>");
  }
  if (Model.IsUserCanViewLog)
  {
      actions.Add("<a class='link' onclick=\"Agency.User.Logs('<#=Id#>'); return false\">Logs</a>");
  }
  var width = actions.Count * 4;
  var actionLinks = actions.ToArray().Join(""); 
%>


<div>
    <div class="abs above ac">    
       <label for="AgencyCenter_UsersSearch" class="strong">Search Users</label>
       <input id="AgencyCenter_UsersSearch" name="UsersSearch" type="text" />
    </div>
         <%= Html.Telerik().Grid<UserSelection>().Name("ListAgencyUsers_Grid").HtmlAttributes(new { @class = "wrapper bottom-bar", @id="UsersGrid" }).Columns(columns => {
                columns.Bound(u => u.DisplayName).Title("Name").Width(20);            
                columns.Bound(u => u.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Width(20);
                columns.Bound(u => u.Title).Sortable(false).Width(20);
                columns.Bound(u => u.LoginActive).Title("Login Active").Sortable(false).Width(10);
                columns.Bound(u => u.LoginCreatedFormat).Title("Login Created").Sortable(false).Width(10);
                columns.Bound(u => u.Id).Sortable(false).ClientTemplate(actionLinks).Title("Action").Width(width).Visible(Model.IsUserCanViewAgencyUser | Model.IsUserCanLiveLoginAsUser | Model.IsUserCanQALoginAsUser | Model.IsUserCanSendActiveLink | Model.IsUserCanViewLog);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("UserGrid", "Agency", new { agencyId = Model.Agency.Id })).Pageable(paging => paging.PageSize(500)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
