﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="buttons heading">
    <% if (Current.IsAxxessAdmin) { %>
    <ul>
        <li><a class="new-agency">Add New Agency</a></li>
    </ul>
    <% } %>
</div>
<div class="row">
    <label for="AgencyCenter_TextSearch" class="strong">Search</label>
    <div><input id="AgencyCenter_TextSearch" name="TextSearch" type="text" /></div>
</div>
<%--<div  class="row">

 <label> Server</label>
 <div>
         <% var clusters = new SelectList(new[] { 
                                                   new SelectListItem { Text = "All", Value = "0" },
                                                   new SelectListItem { Text = "Server 1", Value = "1" },
                                                   new SelectListItem { Text = "Server 2", Value = "2" }}, "Value", "Text", 0);%>
                <%= Html.DropDownList("ClusterId", clusters, new { @id = "AgencyCenter_ClusterId",@class = "" })%></div>
                </div>--%>
<div class="row">
    <label class="strong">Attention</label>
    <div>
        <select name="AttentionLevels" id="AgencyCenter_AttentionLevels">
            <option value="0">All</option>
            <option value="1">Red - TLC</option>
            <option value="2">Blue - Over 90 days</option>
            <option value="3">Standard - Veterans</option>
        </select>
    </div>
</div>
<div class="row">
    <label class="strong">State</label>
    <div>
        <%=Html.LookupSelectList(SelectListTypes.States, "AgencyCenter_StateFilter", string.Empty, new { @id = "AgencyCenter_StateFilter", @class = "state state-filter" })%>
    </div>
</div>
<div class="row">
    <label class="strong">Status</label>
    <div>
        <select name="DeletedStatus" id="AgencyCenter_DeletedStatus">
            <option value="0">Active</option>
            <option value="1">Deleted</option>
        </select>
    </div>
</div>
