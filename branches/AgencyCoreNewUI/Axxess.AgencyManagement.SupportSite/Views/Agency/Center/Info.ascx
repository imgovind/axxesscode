﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<h1><%= Model.Agency.Name%></h1>
<div>
    <div class="divided fl">
        <div class="row">
            <label class="fl strong">Address</label>
            <div class="fr">
                <%if (Model.Agency != null){ %>
                    <div classs="fl"><%=Model.Agency.AddressFirstRow%></div>
                    <div class="fl"><%=Model.Agency.AddressSecondRow%></div>
                <%} %>
            </div>
        </div>
        <div class="row">
            <label class="fl strong">Phone Number</label>
            <div class="fr"><%=Model.Agency.ContactPersonPhoneFormatted%></div>
        </div>
        <div class="row">
            <label class="fl strong">Primary Contact</label>
            <div class="fr"><%=Model.Agency.ContactPersonDisplayName%></div>
        </div>
        <div class="row">
            <label class="fl strong">Email</label>
            <% if (Model.IsUserCanEmailToAgencyUser) { %>
            <div class="fr"><a href="mailto:<%=Model.Agency.ContactPersonEmail %>" class="link"><%=Model.Agency.ContactPersonEmail%></a></div>
            <% } else { %>
            <div class="fr"><%=Model.Agency.ContactPersonEmail%></div>
            <% } %>
        </div>
    </div>
    <div class="divider"></div>
    <div class=" divided fr">
        <div class="row">
            <label class="fl strong">Sales Person</label>
            <div class="fr"><%= Html.LoginDisplayName(Model.Agency.SalesPerson)%></div>
        </div>
        <div class="row">
            <label class="fl strong">Trainer</label>
            <div class="fr"><%= Html.LoginDisplayName(Model.Agency.Trainer)%></div>
        </div>
        <div class="row">
            <label class="fl strong">Implementation Specialist</label>
            <div class="fr"><%= Html.LoginDisplayName(Model.Agency.ImplementationSpecialist)%></div>
        </div>
        <div class="row">
            <label class="fl strong">Customer Since</label>
            <div class="fr"><%=Model.Agency.Created.ToShortDateString()%></div>
        </div>
        <div class="row">
            <label class="fl strong">CAHPS Vendor</label>
            <div class="fr"><%=Model.Agency.CahpsVenderName%></div>
        </div>
        <div class="row">
            <label class="fl strong">Axxess is Biller</label>
            <div class="fr"><%=Model.Agency.IsAxxessTheBiller?"Yes":"No"%></div>
        </div>
    </div>
</div>
<div class="column ac">
    <div class="row ac">
        [<a class="link more-info">More</a>]
    </div>
</div>