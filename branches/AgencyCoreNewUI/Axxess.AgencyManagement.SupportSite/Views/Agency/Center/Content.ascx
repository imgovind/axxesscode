﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<% if (Model.Agency != null) { %>
    <div class="top">
        <div class="winmenu">
            <ul>
            <% if (Model.IsUserCanAdd){ %>
                <li><a class="new-note">New Customer Note</a></li>
            <% }%>
            <% if (Model.IsUserCanView){ %>
                <li><a class="agency-list">View Agency List</a></li>
            <% }%>
            </ul>
        </div>
        <div class="wrapper main ac">
            <% if (Model.IsUserCanEdit | Model.IsUserCanViewLog | Model.IsUserCanView | Model.IsUserCanLoadTemplate | Model.IsUserCanLoadSupply | Model.IsUserCanViewPhysician | Model.IsUserCanViewNote | Model.IsUserCanSuspendAgency | Model.IsUserCanMarkAgencyReadonly){ %>
                <div class="quick reports fr al">
                    <div class="reports-head">
                        <h5>Actions</h5>
                    </div>
                    <ul>
                        <% if (Model.IsUserCanEdit){ %><li><a class="link" onclick="Agency.Edit('<%=Model.Agency.Id %>');return false">Edit</a></li><% }%>
                        <% if (Model.IsUserCanViewLog){ %><li><a class="link" onclick="Agency.Logs('<%=Model.Agency.Id %>');return false">Logs</a></li><% }%>
                        <% if (Model.IsUserCanView){ %><li><a class="link" onclick="Agency.Location.List('<%=Model.Agency.Id %>');return false">Locations</a></li><% }%>
                        <% if (Model.IsUserCanLoadTemplate){ %><li><a class="link" onclick="Agency.Templates('<%=Model.Agency.Id %>');return false">Templates</a></li><% }%>
                        <% if (Model.IsUserCanLoadSupply){ %><li><a class="link" onclick="Agency.Supplies('<%=Model.Agency.Id %>');return false">Supplies</a></li><% }%>
                        <% if (Model.IsUserCanViewPhysician){ %><li><a class="link" onclick="Agency.Physician.List('<%=Model.Agency.Id %>');return false">Physicians</a></li><% }%>
                        <% if (Model.IsUserCanViewNote){ %><li><a class="link" onclick="Agency.Note.List('<%=Model.Agency.Id %>');return false">Notes</a></li><% }%>
                        <% if (!Model.Agency.IsSuspended && Model.IsUserCanSuspendAgency){%><li><a class="link" onclick="Agency.Center.Suspend('<%=Model.Agency.Id %>');return false">Suspend</a></li> <%}else if(Model.Agency.IsSuspended && Model.IsUserCanSuspendAgency) { %><li><a class="link" onclick="Agency.Center.Restore('<%=Model.Agency.Id %>');return false">Restore</a></li><%} %>
                        <% if (!Model.Agency.IsFrozen && Model.IsUserCanMarkAgencyReadonly){%><li><a class="link" onclick="Agency.Center.ReadOnly('<%=Model.Agency.Id %>');return false">Mark as Read-only</a></li> <%}else if(Model.Agency.IsFrozen && Model.IsUserCanMarkAgencyReadonly) { %><li><a class="link" onclick="Agency.Center.UndoReadOnly('<%=Model.Agency.Id %>');return false">Undo Read-only</a></li><%} %>
                    </ul>
                </div>
            <% }%>
            <fieldset class="agency-summary"><% Html.RenderPartial("Center/Info", Model); %></fieldset>  
        </div>
    </div>
    <%if (Model.IsUserCanViewAgencyUser) { %>
    <div class="bottom" style="top:25em;"><% Html.RenderPartial("Center/Users", Model); %></div>
    <% } %>
<% } else { %>
<script type="text/javascript">
    $("#AgencyMainResult").html(U.MessageError("No Agency Information", "No agency information can be found."));
</script>
<%} %>