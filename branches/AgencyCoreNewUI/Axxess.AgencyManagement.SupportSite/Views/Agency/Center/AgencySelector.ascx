﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% Html.Telerik().Grid<AgencySelection>().Name("AgencySelectionGrid").HtmlAttributes(new { @class = "args" }).Columns(columns =>
   {
       columns.Bound(a => a.Name);
       columns.Bound(a => a.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
   }).DataBinding(dataBinding => dataBinding
       .Ajax().Select("CenterAgencyGrid", "Agency", new { clusterId = 0, stateCode = "", isDeleted = false, name = string.Empty })
       ).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events
           .OnRowDataBound("Agency.Center.AgencySelector.SelectorRowBound")
           .OnDataBound("Agency.Center.AgencySelector.Init")
           .OnRowSelected("Agency.Center.AgencySelector.Select")
       ).Render(); %>