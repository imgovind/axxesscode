﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<div class="wrapper main">
    <fieldset>
        <legend>Agency Details</legend>
        <div class="wide column">
            <div class="row">
                <label class="fl strong">Nation Provider Number</label>
                <div class="fr"><%=Model.NationalProviderNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Medicare Provider Number</label>
                <div class="fr"><%=Model.MedicareProviderNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Medicaid Provider Number</label>
                <div class="fr"><%=Model.MedicaidProviderNumber %></div>
            </div>
            <div class="row">
                <label class="fl strong">Unique Agency OASIS Id Code</label>
                <div class="fr"><%=Model.HomeHealthAgencyId %></div>
            </div>
            <div class="row">
                <label class="fl strong">Tax Id Number</label>
                <div class="fr"><%=Model.TaxId %></div>
            </div>
            <div class="row">
                <label class="fl strong">Account Id Number</label>
                <div class="fr"><%=Model.AccountId %></div>
            </div>
        </div>
    </fieldset>
</div>