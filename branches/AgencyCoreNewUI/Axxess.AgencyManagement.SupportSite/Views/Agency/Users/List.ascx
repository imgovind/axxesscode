﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">Agency Users | <%= Model.AgencySnapshot.Name%></span>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
    <%var actions = new List<string>();
      if (Model.IsUserCanEditAgencyUser)
      {
          actions.Add("<a class=\"link\" onclick=\"Agency.User.Edit('<#=LoginId#>');return false\">Edit</a>");
      }
      if (Model.IsUserCanQALoginAsUser)
      {
          actions.Add("<a class='link' href='Agency/User/TestImpersonate/" + Model.AgencySnapshot.Id + "/<#=Id#>' target='_blank'>QA Login</a>");
      }
      if (Model.IsUserCanLiveLoginAsUser)
      {
          actions.Add("<a class='link' href='Agency/User/LiveImpersonate/" + Model.AgencySnapshot.Id + "/<#=Id#>' target='_blank'>Live Login</a>");
      }
      if (Model.IsUserCanSendActiveLink)
      {
          actions.Add("<a class='link' onclick=\"Agency.User.ResendActivationLink('<#=Id#>','" + Model.AgencySnapshot.Id + "');return false\">Activate Link</a>");
      }
      var width = actions.Count * 6;
      var actionLinks = actions.ToArray().Join("");
    %>
    
    <%= Html.Telerik().Grid<UserSelection>().Name("ListAgencyUsers_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(u => u.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(20);
            columns.Bound(u => u.DisplayName).Title("Name").Width(20);
            columns.Bound(u => u.Credentials).Sortable(false).Width(20);
            columns.Bound(u => u.IsLoginActive).Title("Login Active").Sortable(false).Width(10);
            columns.Bound(u => u.LoginCreatedFormat).Title("Login Created").Sortable(false).Width(10);
            columns.Bound(u => u.Id).Sortable(false).ClientTemplate(actionLinks).Title("Action").Width(width).Visible(Model.IsUserCanEditAgencyUser | Model.IsUserCanLiveLoginAsUser | Model.IsUserCanQALoginAsUser | Model.IsUserCanSendActiveLink);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("UserGrid", "Agency", new { agencyId = Model.AgencySnapshot.Id }))
            .Pageable(paging => paging.PageSize(500)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>