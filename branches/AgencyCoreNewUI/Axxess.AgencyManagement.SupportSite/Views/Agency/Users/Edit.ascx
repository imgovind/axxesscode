﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyUserViewData>" %>
<% using (Html.BeginForm("UserUpdateEmail", "Agency", FormMethod.Post, new { @id = "editEmailForm" })) {%>
<%= Html.Hidden("id", Model.Login.Id, new { @id = "Edit_Email_LoginId" }) %>
<div class="form-wrapper">
    <fieldset>
        <legend>Change Email Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Email_DisplayName">Dashboard Name</label><div class="fr"><%=Html.TextBox("DisplayName", Model.Login.DisplayName, new { @id = "Edit_Email_DisplayName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Email_EmailAddress">E-mail Address</label><div class="fr"><%=Html.TextBox("EmailAddress", Model.Login.EmailAddress, new { @id = "Edit_Email_EmailAddress", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a class="close">Close</a></li>
    </ul></div>
</div>
<% } %>

<% using (Html.BeginForm("UserUpdatePassword", "Agency", FormMethod.Post, new { @id = "editPasswordForm" })) {%>
<%= Html.Hidden("id", Model.Login.Id, new { @id = "Edit_Password_LoginId" })%>
<div class="form-wrapper">
    <fieldset>
        <legend>Change Password/Signature</legend>
        <div class="column">
            <div class="row"><label for="Edit_Password_Password">New Password</label><div class="fr"><%=Html.TextBox("Password", "", new { @id = "Edit_Password_Password", @class = "text required input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Password_Signature">New Signature</label><div class="fr"><%=Html.TextBox("Signature", "", new { @id = "Edit_Password_Signature", @maxlength = "20" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update</a></li>
        <li><a class="close">Close</a></li>
    </ul></div>
</div>
<% } %>
<%if (Model.IsUserCanViewLog) { %>
<div class="fr activity-log"><span class="img icon activity"></span></div>
<% } %>
