﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySnapshot>" %>
<div class="wrapper main">
    <% using (Html.BeginForm("MarkReadOnly", "Agency", FormMethod.Post, new { @id = "EditAgency_Form" }))
       { %>
    <%= Html.Hidden("id", Model.Id, new { @id = "EditAgency_ReadOnlyId" }) %>
    <fieldset>
        <legend>Agency</legend><span><em>Provide an effective date for marking the Agency read-only.</em></span>
        <div class="wide column">
            <div class="row">
                <label class="fl strong">Name</label>
                <div class="fr">
                    <%=Model.Name %></div>
            </div>
            <div class="row">
                <label class="fl strong">Effective Date</label>
                <div class="fr">
                    <%= Html.TextBox("readOnlyDate", DateTime.Today.AddDays(30).ToShortDateString(), new { @id = "EditAgency_ReadOnlyDate", @class = "date-picker shortdate" })%></div>
            </div>
            <div class="row">
                <%= Html.CheckBox("isSuspendPayment", true, new { @id = "EditAgency_SuspendPayment" })%>
                <label for="EditAgency_SuspendPayment">Suspend Payment?</label>
            </div>
            <div class="row">
                <label for="EditAgency_FrozenReason" class="fl strong">Reason:</label>
                <%= Html.TextArea("frozenReason", string.Empty, new { @id = "EditAgency_FrozenReason" })%>
            </div>
            <div class="row">
                <label class="fl strong">Document</label>
                <div class="fr">
                    <input type="file" id="EditAgency_Attachment" name="Attachment1" class="fill" />
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a id="EditAgency_ReadOnlySubmit" class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
    <%  } %>
</div>
