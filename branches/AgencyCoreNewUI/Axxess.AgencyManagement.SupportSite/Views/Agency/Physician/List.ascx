﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">Agency Physicians | <%= Model.AgencySnapshot.Name%></span>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls short">
        <div class="filter grid-search"></div>
    </fieldset>
     <% var actions = new List<string>();
       if (Model.IsUserCanLoginAsPhysician)
       {
           actions.Add("<a class=\"link\" href=\"/Agency/Physician/Impersonate/<#=LoginId#>\" target=\"_blank\">QA Login</a>");
           actions.Add("<a class=\"link\" href=\"/Agency/Physician/LiveImpersonate/<#=LoginId#>\" target=\"_blank\">Live Login</a>");
       }
       if (Model.IsUserCanActivateLinkPhysician)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Physician.ResendActivationLink('<#=Id#>','{0}');return false\">Activate Link</a>");
       }
       var width = actions.Count * 8;
       var actionLinks = actions.ToArray().Join("");
       var editLink = string.Format(actionLinks, Model.AgencySnapshot.Id.ToString());
         %>
    <%= Html.Telerik().Grid<PhysicianSelection>().Name("ListAgenciesPhysicians_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(u => u.DisplayName).Title("Name").Width(30);
            columns.Bound(u => u.EmailAddress).ClientTemplate(Model.IsUserCanEmailToPhysician ? "<a class=\"link\" href=\"mailto:<#=EmailAddress#>\"><#=EmailAddress#></a>" : string.Empty).Width(30);
            columns.Bound(u => u.IsLoginActive).Title("Active").Sortable(false).Width(10);
            columns.Bound(u => u.LoginId).Sortable(false).ClientTemplate(editLink).Title("Action").Width(width).Visible(Model.IsUserCanLoginAsPhysician | Model.IsUserCanActivateLinkPhysician);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("PhysicianGrid", "Agency", new { agencyId = Model.AgencySnapshot.Id })).Pageable(paging => paging.PageSize(500)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>