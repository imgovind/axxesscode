﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls">
        <div id="ListImplementationSpecialist_TopFilter" class="filter">
            <label class="strong">Filter:</label>
            <input class="input-directions" type="text" />
            <label class="strong">Specialist:</label>
            <%= Html.ImplementationSpecialists("ImplementationSpecialistId", string.Empty, new { @id = "ListImplementationSpecialist_ImplementationSpecialist" })%>
        </div>
        <div class="filter">
            <label class="strong">Date Range:</label>
            <input type="text" class="date-picker shortdate" name="startDate" value="<%= DateTime.Now.AddDays(-90).ToShortDateString() %>" id="ListImplementationSpecialist_StartDate" />
            <label class="strong">To</label>
            <input type="text" class="date-picker shortdate" name="endDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ListImplementationSpecialist_EndDate" />
        </div>
        <div class="buttons float-right"><ul><li><a href="javascript:void(0);" onclick="Agency.ImplementationSpecialist.Rebind();">Generate</a></li></ul></div>
    </fieldset>
    <%var actions = new List<string>();
      if (Model.IsUserCanReassign)
      {
          actions.Add("<a class=\"link\" onclick=\"Agency.ImplementationSpecialist.Edit('<#=AgencyId#>');return false\">Reassign</a>");
      }
      if (Model.IsUserCanAdd)
      {
          actions.Add("<a class=\"link\" onclick=\"Agency.ImplementationSpecialist.NewNote('<#=AgencyId#>');return false\">New Note</a>");
      }
      var width = actions.Count * 80;
      var actionLinks = actions.ToArray().Join("");
    %>
    <%= Html.Telerik().Grid<AgencySpecialist>().Name("ListImplementationSpecialist_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns =>
    {
        columns.Bound(l => l.AgencyName).Title("Agency Name").ClientTemplate(Model.IsUserCanView ? "<a class=\"link\" onclick=\"Agency.Center.Load('<#=AgencyId#>');return false\"><#=AgencyName#></a>": string.Empty).Sortable(true);
        columns.Bound(l => l.AgencyContactDisplayName).Title("Agency Contact").Sortable(false);
        columns.Bound(l => l.AgencyContactPersonEmail).Title("Agency Contact E-mail").ClientTemplate(Model.IsUserCanEmailToContact?"<a href='mailto:<#=AgencyContactPersonEmail#>'><#=AgencyContactPersonEmail#></a>":string.Empty).Sortable(false);
        columns.Bound(l => l.AgencyContactPersonPhone).Title("Agency Contact Phone").Sortable(false);
        columns.Bound(l => l.SpecialistName).Title("Implementation Specialist").Sortable(true);
        columns.Bound(l => l.CreatedDateFormatted).Title("Agency Created").Sortable(true);
        columns.Bound(l => l.AgencyId).Title("Action").Sortable(false).ClientTemplate(actionLinks).Width(width).Visible(Model.IsUserCanReassign | Model.IsUserCanAdd);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ImplementationSpecialist/Grid", "Agency", new { implementationSpecialistId = Guid.Empty, startDate = DateTime.Now.AddDays(-90).ToShortDateString(), endDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>