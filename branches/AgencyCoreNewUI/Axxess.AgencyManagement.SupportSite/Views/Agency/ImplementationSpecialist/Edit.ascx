﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencySpecialist>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("ImplementationSpecialist/Update", "Agency", FormMethod.Post, new { @id = "EditImplementationSpecialist_Form" })) {%>
    <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "EditImplementationSpecialist_AgencyId" }) %>
    <%= Html.Hidden("AgencyName", Model.AgencyName) %>
    <fieldset>
        <legend>Implementation Specialist</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Agency Name:</label>
                <div class="fr"><%= Model.AgencyName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Agency Created:</label>
                <div class="fr"><%= Model.CreatedDateFormatted %></div>
            </div>
            <div class="row">
                <label class="fl strong">Specialist:</label>             
                <div class="fr"><%= Html.ImplementationSpecialists("SpecialistId", Model.SpecialistId.ToString(), new { @id = "EditImplementationSpecialist_ImplementationSpecialist" })%></div>
            </div>
        </div>
    </fieldset>
   <div class="buttons">
        <ul>
            <li><a class="save close">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>