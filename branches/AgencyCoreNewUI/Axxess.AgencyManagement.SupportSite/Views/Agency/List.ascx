﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<span class="wintitle">Agency List</span>
<div class="wrapper main blue">
 <div class="fr buttons">
        <ul><li><a href="javascript:void(0);" onclick="Agency.RebindList();">Generate</a></li></ul>
        <br />
    <%  if (Model.IsUserCanAdd) { %>
        <ul><li><a class="new-agency">New Agency</a></li></ul>
    <%  } %>
        <br />
    <%  if (Model.IsUserCanExport) { %>
        <ul><li><a class="export">Excel Export</a></li></ul>
    <%  } %>
</div>
    <fieldset class="ac grid-controls">
        <div class="filter grid-search"></div>
       <%-- <label> Server</label>
         <% var clusters = new SelectList(new[] { 
                                                   new SelectListItem { Text = "All", Value = "0" },
                                                   new SelectListItem { Text = "Server 1", Value = "1" },
                                                   new SelectListItem { Text = "Server 2", Value = "2" }}, "Value", "Text", 0);%>
                <%= Html.DropDownList("ListAgencies_ClusterId", clusters, new { name = "ClusterId", @id = "ListAgencies_ClusterId", @style = "width:170px", @class = "oe" })%>--%>
        <label for="AgencyList_StateFilter" class="strong">Filter by State</label>
        <%=Html.LookupSelectList(SelectListTypes.States, "AgencyList_StateFilter", string.Empty, new { @id="AgencyList_StateFilter", @class = "state state-filter" })%>
        
        <div id="ListAgencies_DateFilter">
            <div class="filter row">
                <label class="strong">Range:</label>
                <% var months = new SelectList(new[] { 
                                                   new SelectListItem { Text = "All", Value = "0" },
                                                   new SelectListItem { Text = "January", Value = "1" }, 
                                                   new SelectListItem { Text = "February", Value = "2" }, 
                                                   new SelectListItem { Text = "March", Value = "3" }, 
                                                   new SelectListItem { Text = "April", Value = "4" }, 
                                                   new SelectListItem { Text = "May", Value = "5" }, 
                                                   new SelectListItem { Text = "June", Value = "6" }, 
                                                   new SelectListItem { Text = "July", Value = "7" }, 
                                                   new SelectListItem { Text = "August", Value = "8" }, 
                                                   new SelectListItem { Text = "September", Value = "9" }, 
                                                   new SelectListItem { Text = "October", Value = "10" }, 
                                                   new SelectListItem { Text = "November", Value = "11" }, 
                                                   new SelectListItem { Text = "December", Value = "12" }, 
                                                   new SelectListItem { Text = "Custom", Value = "13"}}, "Value", "Text", DateTime.Now.Month);%>
                <%= Html.DropDownList("ListAgencies_Month", months, new { name = "UpgradeMonth", @id = "ListAgencies_Month", @style = "width:170px", @class = "oe" })%>
                <%= Html.Years("ListAgencies_Year", DateTime.Now.Year.ToString(), 2010, new { name = "UpgradeYear", @id = "ListAgencies_Year", @style = "width:170px", @class = "oe" })%>
                
            </div>
            <div class="filter row" id="ListAgencies_DateRange">
                <label class="strong">Date Range:</label>
                <input type="text" class="date-picker shortdate" name="startDate" value="<%= DateTime.Now.AddDays(-30).ToShortDateString() %>" id="ListAgencies_StartDate" />
                <label class="strong">To</label>
                <input type="text" class="date-picker shortdate" name="endDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ListAgencies_EndDate" />
            </div>
        </div>
        <div class="filter">
            <span class="redrow age-filter selected">Red &#8211; TLC</span>
            <span class="bluerow age-filter selected">Blue &#8211; Over 90 days</span>
            <span class="blackrow age-filter selected">Standard &#8211; Veterans</span>
        </div>
    </fieldset>

<% var actions = new List<string>();
       if (Model.IsUserCanEdit)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.Edit('<#=Id#>');return false\">Edit</a></li>");
       }
       if (Model.IsUserCanViewLocation)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.Location.List('<#=Id#>');return false\">Locations</a></li>");
       }
       if (Model.IsUserCanLoadTemplate)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.Templates('<#=Id#>');return false\">Templates</a></li>");
       }
       if (Model.IsUserCanLoadSupply)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.Supplies('<#=Id#>');return false\">Supplies</a></li>");
       }
       if (Model.IsUserCanViewPhysician)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.Physician.List('<#=Id#>');return false\">Physicians</a></li>");
       }
       if (Model.IsUserCanViewNote)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.Note.List('<#=Id#>');return false\">Notes</a></li>");
       }
       if (Model.IsUserCanSuspend)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.<#=ActionText#>('<#=Id#>', $(this).closest('.t-grid'));return false\"><#=ActionText#></a></li>");
       }
       if (Model.IsUserCanReadOnly)
       {
           actions.Add("<li><a class=\"link\" onclick=\"Agency.<#=ReadOnlyText#>('<#=Id#>', $(this).closest('.t-grid'));return false\"><#=ReadOnlyText#></a></li>");
       }
       //var width = 8 + (actions.Count -1) * 4;
       var actionLinks = "<div class=\"grid-action\"><span class='img icon pointer right'/><ul class='action-menu'>" + actions.ToArray().Join("") + "</ul></div>";
         %>


<%  using (Html.BeginForm("Agencies", "Export", FormMethod.Post))
    { %>
    <% Html.Telerik().Grid<AgencyLite>().Name("ListAgencies_Grid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns =>
        {
            columns.Bound(a => a.Id).ClientTemplate("<#= Agency.GetRowIndex() #>").Title("").Width(5).Sortable(false);
            columns.Bound(a => a.Name).ClientTemplate(Model.IsUserCanViewAgencyUser ? "<a class=\"link\" onclick=\"Agency.User.List('<#=Id#>');return false\"><#=Name#></a>" : string.Empty).Title("Name").Sortable(true).Width(35);
            columns.Bound(a => a.ContactPersonDisplayName).ClientTemplate(Model.IsUserCanEmailToContact ? "<a href=\"mailto:<#=ContactPersonEmail#>\" class=\"link\"><#=ContactPersonDisplayName#></a>" : string.Empty).Title("Contact").Sortable(true).Width(15);
            columns.Bound(a => a.ContactPersonPhoneFormatted).Title("Phone").Width(15).Sortable(true);
            columns.Bound(a => a.Location).Title("Location").Sortable(true).Width(15);
            columns.Bound(a => a.SalesPerson).Title("Sales Person").Sortable(true).Width(15);
            columns.Bound(a => a.DateSortable).Title("Created").Sortable(true).Width(10);
            columns.Bound(a => a.Id).ClientTemplate(actionLinks)
                .Title("Action")
                .HeaderHtmlAttributes(new { @class = "action menu-based" })
                .HtmlAttributes(new { @class = "action menu-based" })
                .Width(8)
                .Sortable(false)
                .Visible(Model.IsUserCanEdit | Model.IsUserCanViewLocation | Model.IsUserCanLoadTemplate | Model.IsUserCanLoadSupply | Model.IsUserCanViewPhysician | Model.IsUserCanViewNote | Model.IsUserCanSuspend | Model.IsUserCanReadOnly);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Agency", new { clusterId = 0, stateFilter = "all", startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) }))
                .ClientEvents(c => c.OnRowDataBound("Agency.ListRowDataBound").OnDataBound("Agency.OnDataBound").OnLoad("Agency.OnLoad")).Selectable().Sortable().Scrollable().Render(); %>
<%  } %>
</div>