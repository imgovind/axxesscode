﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">Edit Agency | <%= Model != null ? Model.Agency.Name.ToTitleCase() : string.Empty %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Agency", FormMethod.Post, new { @id = "EditAgency_Form" })) { %>
    <%= Html.Hidden("Id", Model.Agency.Id, new { @id = "EditAgency_Id" }) %>
    <div>
        <fieldset class="half fl">
            <legend>Trial Information</legend>
            <div class="column">
                <div class="row">
                    <label for="EditAgency_IsAgreementSignedYes" class="fl strong">Agreement Signed?</label>
                    <div class="fr radio">
                        <%= Html.RadioButton("IsAgreementSigned", "true", Model.Agency.IsAgreementSigned, new { @id = "EditAgency_IsAgreementSignedYes", @class = "required" })%>
                        <label for="EditAgency_IsAgreementSignedYes" class="fixed short">Yes</label>
                        <%=Html.RadioButton("IsAgreementSigned", "false", Model.Agency.IsAgreementSigned == false, new { @id = "EditAgency_IsAgreementSignedNo", @class = "required" })%>
                        <label for="EditAgency_IsAgreementSignedNo" class="fixed short">No</label>
                    </div>
                </div>
                <div class="row" id="EditAgency_IsAgreementSignedYesMore">
                    <label for="EditAgency_TrialPeriod" class="fl strong">Trial Period</label>
                    <div class="fr">
                        <%  var trialPeriod = new SelectList(new[] {
                                new SelectListItem { Text = "-- Select Trial Period --", Value = "" },
                                new SelectListItem { Text = "30 Days", Value = "30" },
                                new SelectListItem { Text = "60 Days", Value = "60" },
                                new SelectListItem { Text = "90 Days", Value = "90" }
                            }, "Value", "Text", Model.Agency.TrialPeriod); %>
                        <%= Html.DropDownList("TrialPeriod", trialPeriod, new { @id = "EditAgency_TrialPeriod" }) %>
                    </div>
                </div>
                <div class="row">
                    <label for="EditAgency_Services" class="fl strong">Services</label>
                    <div class="fr">
                        <%= Html.AgencyServicesDropDown("ServiceArray", Model.Agency.Services, new { @class = "required", multiple = "multiple", style = "width:200px", id = "EditAgency_Services" })%>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset class="half fr">
            <legend>Audit Information</legend>
            <div class="column">
                <div class="row">
                    <label for="EditAgency_SalesPerson" class="fl strong">Sales Person</label>
                    <div class="fr"><%= Html.Users("SalesPerson", Model.Agency.SalesPerson.ToString(), new { @id = "EditAgency_SalesPerson" })%></div>
                </div>
                <div class="row">
                    <label for="EditAgency_SalesPerson" class="fl strong">Trainer</label>
                    <div class="fr"><%= Html.Users("Trainer", Model.Agency.Trainer.ToString(), new { @id = "EditAgency_Trainer" })%></div>
                </div>
                <div class="row">
                    <label for="EditAgency_BackupTriner" class="fl strong">Backup Trainer</label>
                    <div class="fr"><%=Html.Users("BackupTrainer", Model.Agency.BackupTrainer.ToString(), new { @id="EditAgency_BackupTriner"}) %></div>
                </div>
                <div class="row">
                    <label for="EditAgency_ImplementationSpecialist" class="fl strong">Implementation Specialist</label>
                    <div class="fr"><%= Html.ImplementationSpecialists("ImplementationSpecialist", Model.Agency.ImplementationSpecialist.ToString(), new { @id = "EditAgency_ImplementationSpecialist" })%></div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="clr"></div>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row">
                <label for="EditAgency_CompanyName" class="fl strong">Company Name</label>
                <div class="fr"><%=Html.TextBox("Name", Model.Agency.Name, new { @id = "EditAgency_CompanyName", @maxlength = "100", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_TaxId" class="fl strong">Tax Id</label>
                <div class="fr"><%=Html.TextBox("TaxId", Model.Agency.TaxId, new { @id = "EditAgency_TaxId", @maxlength = "10", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_TaxIdType" class="fl strong">Tax Id Type</label>
                <div class="fr">
                    <%  var taxIdTypes = new SelectList(new[] {
                            new SelectListItem { Text = "** Select Tax Id Type **", Value = "0" },
                            new SelectListItem { Text = "EIN (Employer Identification Number)", Value = "1" },
                            new SelectListItem { Text = "SSN (Social Security Number)", Value = "2" }
                        }, "Value", "Text", Model.Agency.TaxIdType); %><%= Html.DropDownList("TaxIdType", taxIdTypes, new { @id = "EditAgency_TaxIdType" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAgency_Payor" class="fl strong">Payor</label>
                <div class="fr">
                    <%= Html.LookupSelectList(SelectListTypes.Payors, "Payor", Model.Agency.Payor, new { @id = "EditAgency_Payor" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAgency_ContactPersonEmail" class="fl strong">Contact Person E-mail</label>
                <div class="fr"><%= Html.TextBox("ContactPersonEmail", Model.Agency.ContactPersonEmail, new { @id = "EditAgency_ContactPersonEmail", @class = "required", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_ContactPhone1" class="fl strong">Contact Person Phone</label>
                <div class="fr">
                    <%= Html.TextBox("ContactPhoneArray", Model.Agency.ContactPersonPhone.IsNotNullOrEmpty() ? Model.Agency.ContactPersonPhone.Substring(0, 3) : string.Empty, new { @id = "EditAgency_ContactPhone1", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("ContactPhoneArray", Model.Agency.ContactPersonPhone.IsNotNullOrEmpty() ? Model.Agency.ContactPersonPhone.Substring(3, 3) : string.Empty, new { @id = "EditAgency_ContactPhone2", @class = "required numeric phone-short", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("ContactPhoneArray", Model.Agency.ContactPersonPhone.IsNotNullOrEmpty() ? Model.Agency.ContactPersonPhone.Substring(6, 4) : string.Empty, new { @id = "EditAgency_ContactPhone3", @class = "required numeric phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAgency_CahpsVendor" class="fl strong">CAHPS Vendor</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.CahpsVendors, "CahpsVendor", "", new { @id = "EditAgency_CahpsVendor" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditAgency_NationalProviderNo" class="fl strong">National Provider Number</label>
                <div class="fr"><%=Html.TextBox("NationalProviderNumber", Model.Agency.NationalProviderNumber, new { @id = "EditAgency_NationalProviderNo", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_MedicareProviderNo" class="fl strong">Medicare Provider Number</label>
                <div class="fr"><%=Html.TextBox("MedicareProviderNumber", Model.Agency.MedicareProviderNumber, new { @id = "EditAgency_MedicareProviderNo", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_MedicaidProviderNo" class="fl strong">Medicaid Provider Number</label>
                <div class="fr"><%=Html.TextBox("MedicaidProviderNumber", Model.Agency.MedicaidProviderNumber, new { @id = "EditAgency_MedicaidProviderNo", @maxlength = "10" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_HomeHealthAgencyId" class="fl strong">Unique Agency OASIS ID Code</label>
                <div class="fr"><%=Html.TextBox("HomeHealthAgencyId", Model.Agency.HomeHealthAgencyId, new { @id = "EditAgency_HomeHealthAgencyId", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_ContactPersonFirstName" class="fl strong">Contact Person First Name</label>
                <div class="fr"><%=Html.TextBox("ContactPersonFirstName", Model.Agency.ContactPersonFirstName, new { @id = "EditAgency_ContactPersonFirstName", @maxlength = "50", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_ContactPersonLastName" class="fl strong">Contact Person Last Name</label>
                <div class="fr"><%=Html.TextBox("ContactPersonLastName", Model.Agency.ContactPersonLastName, new { @id = "EditAgency_ContactPersonLastName", @class = "required", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_PrimaryUser" class="fl strong">Primary User</label>
                <div class="fr"><%= Html.AgencyUsers(Model.Agency.Id, "PrimaryUser", Model.Agency.PrimaryUser.ToString(), new { @id = "EditAgency_PrimaryUser" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_PreviousSoftware" class="fl strong">Previous Software</label>
                <div class="fr"><%=Html.TextBox("PreviousSoftware", Model.Agency.PreviousSoftware, new { @id = "EditAgency_PreviousSoftware", @maxlength = "100"}) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Submitter Information (Medicare)</legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessTheBiller", Model.Agency.IsAxxessTheBiller, new { @id = "EditAgency_AxxessBiller" }) %>
                        <label for="EditAgency_AxxessBiller">Check here if Axxess&trade; HealthCare Consult handles billing for this Agency.</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="EditAgency_SubmitterId" class="fl strong">Submitter Id</label>
                <div class="fr"><%= Html.TextBox("SubmitterId", Model.Agency.SubmitterId, new { @id = "EditAgency_SubmitterId", @maxlength = "15", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="EditAgency_SubmitterName" class="fl strong">Submitter Name</label>
                <div class="fr"><%=Html.TextBox("SubmitterName", Model.Agency.SubmitterName, new { @id = "EditAgency_SubmitterName", @maxlength = "50", @class = "required" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="EditAgency_SubmitterPhone1" class="fl strong">Submitter Phone Number</label>
                <div class="fr">
                    <%= Html.TextBox("SubmitterPhoneArray", Model.Agency.SubmitterPhone.IsNotNullOrEmpty() && Model.Agency.SubmitterPhone.Length >= 3 ? Model.Agency.SubmitterPhone.Substring(0, 3) : string.Empty, new { @id = "EditAgency_SubmitterPhone1", @class = "numeric phone-short required", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("SubmitterPhoneArray", Model.Agency.SubmitterPhone.IsNotNullOrEmpty() && Model.Agency.SubmitterPhone.Length >= 6 != null && Model.Agency.SubmitterPhone != "" ? Model.Agency.SubmitterPhone.Substring(3, 3) : string.Empty, new { @id = "EditAgency_SubmitterPhone2", @class = "numeric phone-short required", @maxlength = "3" })%>
                    -
                    <%= Html.TextBox("SubmitterPhoneArray", Model.Agency.SubmitterPhone.IsNotNullOrEmpty() && Model.Agency.SubmitterPhone.Length >= 10 != null && Model.Agency.SubmitterPhone != "" ? Model.Agency.SubmitterPhone.Substring(6, 4) : string.Empty, new { @id = "EditAgency_SubmitterPhone3", @class = "numeric required phone-long", @maxlength = "4" })%>
                </div>
            </div>
            <div class="row">
                <label for="EditAgency_SubmitterFax1" class="fl strong">Submitter Fax Number</label>
                <div class="fr">
                    <%=Html.TextBox("SubmitterFaxArray", Model.Agency.SubmitterFax.IsNotNullOrEmpty() && Model.Agency.SubmitterFax.Length >= 3 ? Model.Agency.SubmitterFax.Substring(0, 3) : string.Empty, new { @id = "EditAgency_SubmitterFax1", @class = "numeric phone-short required", @maxlength = "3" })%>
                    -
                    <%=Html.TextBox("SubmitterFaxArray", Model.Agency.SubmitterFax.IsNotNullOrEmpty() && Model.Agency.SubmitterFax.Length >= 6 != null && Model.Agency.SubmitterFax != "" ? Model.Agency.SubmitterFax.Substring(3, 3) : string.Empty, new { @id = "EditAgency_SubmitterFax2", @class = "numeric phone-short required", @maxlength = "3" })%>
                    -
                    <%=Html.TextBox("SubmitterFaxArray", Model.Agency.SubmitterFax.IsNotNullOrEmpty() && Model.Agency.SubmitterFax.Length >= 10 != null && Model.Agency.SubmitterFax != "" ? Model.Agency.SubmitterFax.Substring(6, 4) : string.Empty, new { @id = "EditAgency_SubmitterFax3", @class = "required digits phone-long", @maxlength = "4" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <% if (Model.IsUserCanViewLog) { %>
    <div class="fr activity-log"><span class="img icon activity"></span></div>
    <% } %>
    <div class="buttons">
        <ul>
            <li><a class="save close">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>