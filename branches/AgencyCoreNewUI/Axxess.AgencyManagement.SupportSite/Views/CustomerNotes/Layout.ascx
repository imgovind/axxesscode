﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>

<span class="wintitle">Customer Notes</span>
<div class="wrapper main blue">
    <fieldset class="ac grid-controls">
        <div class="filter">
            <label class="strong">Filter by Text</label>
            <input class="input-directions" type="text" />
            <%=Html.CustomerNoteTypeSelector("CustomerNoteType", string.Empty, new { @id = "Filter_CustomerNoteType"},true, true) %>
        </div>
        <div class="view-tabs">
            <span class="recent-tab tab selected">Recently Added</span>
            <span class="updated-tab tab">Updated</span>
            <span class="currentUser-tab tab">My Notes</span>
        </div>
    </fieldset>
       <div><%Html.RenderPartial("CurrentUserNotesList"); %></div>
       <div><%Html.RenderPartial("UpdatedNotesList"); %></div>
       <div><%Html.RenderPartial("RecentNotesList"); %></div>
</div> 