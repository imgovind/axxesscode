﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyCenterViewData>" %>
<span class="wintitle">List Notes | <%= Model.AgencySnapshot.Name%></span>
<%= Html.Hidden("DisplayName", Current.DisplayName, new { @id="NoteList_DisplayName"}) %>

<div class="wrapper main blue">
    <%= Html.Hidden("AgencyId", Model.AgencySnapshot.Id)%>
    <div class="fr buttons">
    <% if (Model.IsUserCanAdd) {%>
        <ul><li><a class="new-note">New Note</a></li></ul>
        <br />
    <% } %>
    <% if (Model.IsUserCanExport) {%>
        <ul><li><a class="export">Excel Export</a></li></ul>
    <% } %>
    </div>
    <fieldset class="ac grid-controls">
        <div class="filter grid-search"></div>
        <span class="currentUser-tab tab">My Notes</span>
    </fieldset>
    
    <% var actions = new List<string>();
       if (Model.IsUserCanViewDetail)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Note.View('<#=Id#>','<#=AgencyId#>');return false\">View</a>");
       }
       if (Model.IsUserCanEdit)
       {
           actions.Add("<a class=\"link edit\" onclick=\"Agency.Note.Edit('<#=Id#>','<#=AgencyId#>');return false\">Edit</a>");
       }
       if (Model.IsUserCanDelete)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Note.Delete('<#=Id#>','<#=AgencyId#>',$(this).closest('.t-grid'));return false\">Delete</a>");
       }
       var width = actions.Count * 50;
       var actionLinks = actions.ToArray().Join("");
         %>
    
    <%= Html.Telerik().Grid<CustomerNote>().Name("ListAgencyNotes_Grid").HtmlAttributes(new { @class = "bottom-bar"}).Columns(columns =>
        {
            columns.Bound(c => c.RepName).Title("Rep Name").Width(180).Sortable(true);
            columns.Bound(c => c.Summary).Title("Summary").Sortable(true);
            columns.Bound(c => c.Preview).Title("Preview").Sortable(true);
            columns.Bound(c => c.NoteTypeName).Title("Note Type").Width(120).Sortable(true);
            columns.Bound(c => c.Completed).Title("Sent").Width(40);
            columns.Bound(c => c.CreatedFormatted).Width(150).Title("Time").Sortable(false);
            columns.Bound(c => c.Id).Title("Action").Sortable(false).Width(width).ClientTemplate(actionLinks).Visible(Model.IsUserCanViewDetail | Model.IsUserCanEdit | Model.IsUserCanDelete);
        }).ClientEvents(c => c.OnRowDataBound("Agency.Note.RowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("NoteGrid", "CustomerNotes", new { agencyId = Model.AgencySnapshot.Id })).Sortable(sort => sort.Enabled(true).OrderBy(sortOrder => sortOrder.Add(o => o.Created).Descending())).Scrollable(scrolling => scrolling.Enabled(true))%>
</div>