﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CustomerNote>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("Note/Update", "CustomerNotes", FormMethod.Post, new { @id = "EditNote_Form" })) {%>
    <%= Html.Hidden("Id", Model.Id, new { @id = "EditNote_Id" }) %>
    <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "EditNote_AgencyId" }) %>
    <%= Html.Hidden("IssueId", Model.IssueId, new { @id = "EditNote_IssueId" }) %>
    <%= Html.Hidden("LoginId", Model.LoginId, new { @id = "EditNote_ModifiedBy" }) %>
    <%= Html.Hidden("ModifiedBy", Current.LoginId, new { @id = "EditNote_ModifiedBy" }) %>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Customer Support Representative</label>
                <div class="fr"><%= Model.RepName %></div>
            </div>
             <% if (Model.ModifiedBy != Guid.Empty) { %>
                <div class="row">
                    <label class="fl strong">Last Modified by</label>
                    <div class="fr"><%= Model.ModifiedByName %></div>
                </div>
            <% } %>
            <div class="row">
                <label class="fl strong">Date &#38; Time of Call</label>
                <div class="fr"><%= Model.Created.ToString("MM/dd/yyy hh:mm:ss tt") %></div>
            </div>
            <div class="row">
                <label class="fl strong">Phone Number</label>             
                <div class="fr"><%= Model.ContactNumber %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Sales Person</label>
                <div class="fr"><%= Model.SalesPersonName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Trainer</label>
                <div class="fr"><%= Model.TrainerName %></div>
            </div>
           <div class="row">
                <label class="fl strong">Backup Trainer</label>
                <div class="fr">
                    <%= Model.BackupTrainerName %></div>
            </div>
        </div>
    </fieldset> 
    <fieldset>
        <legend>Note Information</legend>
        <div class="column fl">
            <div class = "row">
                  <label class="fl strong">Note Type</label>
                  <div class="fr">
                    <%= Html.CustomerNoteTypeSelector("NoteType", Model.NoteType.ToString(), new { @id = "EditNote_NoteType", @class = "required notzero"}, true, false) %>
                  </div>
            </div>
            <%if((CustomerNoteType)Model.NoteType==CustomerNoteType.DataLoad){ %>
                <div class="row" id="EditNote_PreviousSoftwareDiv">
            <%}else {%>
                <div class="row hidden" id="EditNote_PreviousSoftwareDiv">
           <%} %>
                <label class="fl strong">Previous Software</label>
                <div class="fr">
                    <%= Html.TextBox("PreviousSoftware", Model.PreviousSoftware, new { @id="EditNote_PreviousSoftware", @class="required" }) %>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Caller</label>
                <div class="fr">
                    <%= Html.AgencyUsers(Model.AgencyId, "CallerId", Model.CallerId.ToString(), new {@id = "newNote_CallerId"}) %>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Sent</label>
                <div class="fr"><%= Model.Completed %></div>
            </div>
            <div class="row">
                <label class="fl strong">Summary</label>
                <div class="fr">
                    <%= Html.TextBox("Summary", Model.Summary, new { @id = "NewNote_Summary", @class = "required", @maxcharacters = "75"})%>
                </div>
            </div>
            <% if (Model.HasAttachment) { %>   
                <div class="row">
                    <label class="fl strong">Attachment</label>
                    <span id="messageAttachment" class="fr"><%= string.Format("<a class=\"link\" href=\"/Asset/{0}/{2}\">{1}</a>&#160;", Model.AttachmentId, Model.AttachmentName, Model.AgencyId)%></span>
                </div>
           <%} %>
           <div class="row">
                <% if (Model.HasAttachment){ %>
                    <label class = "fl strong">Replace current attachment</label>
                <%}else{ %>
                    <label class = "fl strong">Attachment</label>
                <%} %>
                <div class="fr">
                    <input type="file" id="EditNote_Attachment" name="Attachment1" class="fill" />
                </div>
           </div>
        </div>
        <div class="column fr">
            <div class="row">
                <label class="fl strong">Patient Name</label>
                <div class="fr">
                    <%= Html.TextBox("PatientName", Model.PatientName, new { @id="NewNote_PatientName"})%>
                </div>
            </div>
             <div class="row">
                <label class="fl strong">Episode Period</label>
                <div class="fr">
                    <%= Html.TextBox("EpisodePeriod", Model.EpisodePeriod, new { @id = "NewNote_EpisodePeriod" })%>
                </div>
            </div>
             <div class="row">
                <label class="fl strong">Visit Type</label>
                <div class="fr">
                    <%= Html.TextBox("VisitType", Model.VisitType, new { @id = "NewNote_VisitType" })%>
                </div>
            </div>
             <div class="row">
                <label class="fl strong">Date of Visit</label>
                <div class="fr">
                    <%= Html.TextBox("DateofVisit", Model.DateofVisit, new { @id = "NewNote_DateofVisit" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <%= Html.TextArea("Comments", Model.Comments, new { @id = "EditNote_Comments", @class = "required tallest" })%>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("SendAsMessage", Model.SendAsMessage, new { @id = "EditNote_SendAsMessage"})%>
                        <label for="EditNote_SendAsMessage">Send note as Message</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="EditNote_Recipients">
                    <label class="strong">Recipients</label>
                    <%= Html.Recipients("EditNote", Model.RecipientArray) %>
                </div>
            </div>
        </div>
    </fieldset>
   <div class="buttons">
            <input type="hidden" name="buttonClicked" id="buttonClicked" value="" />
        <ul>
            <li id="EditNote_SaveButton"><a class="save close">Save</a></li>
            <li id="EditNote_SendButton"><a class="complete">Send</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>