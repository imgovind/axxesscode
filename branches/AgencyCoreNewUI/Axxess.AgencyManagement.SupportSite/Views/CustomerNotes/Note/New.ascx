﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<span class="wintitle">New Support Note |
    <%= Model.Name %></span>
<div class="wrapper main">
    <%  using (Html.BeginForm("Note/Create", "CustomerNotes", FormMethod.Post, new { @id = "NewNote_Form" }))
        { %>
    <%= Html.Hidden("AgencyId", Model.Id, new { @id = "NewNote_AgencyId" })%>
    <%= Html.Hidden("LoginId", Current.LoginId, new { @id = "NewNote_LoginId" })%>
    
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Agency Name</label>
                <div class="fr"><a class="link" onclick="Agency.Center.Load('<%= Model.Id %>');return false;"><%= Model.Name %></a></div>
            </div>
            <div class="row">
                <label class="fl strong">Customer Support Representative</label>
                <div class="fr"><%= Current.DisplayName %></div>
            </div>
            <div class="row">
                <label class="fl strong">Date &#38; Time of Call</label>
                <div class="fr"><%= DateTime.Now.ToString("MM/dd/yyy hh:mm:ss tt") %></div>
            </div>
            <div class="row">
                <label class="fl strong">Phone Number</label>             
                <div class="fr"><%= Model.ContactPersonPhoneFormatted %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Sales Person</label>
                <div class="fr"><%= Html.LoginDisplayName(Model.SalesPerson) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Trainer</label>
                <div class="fr"><%= Html.LoginDisplayName(Model.Trainer) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Backup Trainer</label>
                <div class="fr"><%= Html.LoginDisplayName(Model.BackupTrainer) %></div>
            </div>
            <div class="row">
                <label class="fl strong">Implementation Specialist</label>
                <div class="fr"><%= Html.LoginDisplayName(Model.ImplementationSpecialist) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Note Information</legend>
        <div class="column fl">
            <div class="row">
                <label class="fl strong">
                    Note Type</label>
                <div class="fr">
                    <%= Html.CustomerNoteTypeSelector("NoteType", "0", new { @id = "NewNote_NoteType", @class = "required notzero"}, false, false) %>
                </div>
            </div>
            <div class="row hidden" id="NewNote_PreviousSoftwareDiv">
                <label class="fl strong">Previous Software</label>
                <div class="fr">
                    <%= Html.TextBox("PreviousSoftware",string.Empty,new { @id="NewNote_PreviousSoftware", @class="required" }) %>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">Caller</label>
                <div class="fr">
                    <%= Html.AgencyUsers(Model.Id, "CallerId", string.Empty, new { @id = "NewNote_CallerId"}) %>
                </div>
            </div>
            <div class="row">
                <label class="fl strong">
                    Summary</label>
                <div class="fr ac">
                    <%= Html.TextBox("Summary", string.Empty, new { @id = "NewNote_Summary", @class = "required", @maxcharacters = "75"}) %>
               </div>
            </div>
            <div class="row">
                <label class="fl strong">Attachments</label>
                <div class="fr">
                    <input type="file" id="NewNote_Attachment" name="Attachment1" class="fill" />
                </div>
            </div>
        </div>
        <div class="column fr">
            <div class="row">
                <label class="fl strong">Patient Name</label>
                <div class="fr">
                    <%= Html.TextBox("PatientName", string.Empty, new { @id="NewNote_PatientName", @name="patientName" })%>
                </div>
            </div>
             <div class="row">
                <label class="fl strong">Episode Period</label>
                <div class="fr">
                    <%= Html.TextBox("EpisodePeriod", string.Empty, new { @id = "NewNote_EpisodePeriod" })%>
                </div>
            </div>
             <div class="row">
                <label class="fl strong">Visit Type</label>
                <div class="fr">
                    <%= Html.TextBox("VisitType", string.Empty, new { @id = "NewNote_VisitType" })%>
                </div>
            </div>
             <div class="row">
                <label class="fl strong">Date of Visit</label>
                <div class="fr">
                    <%= Html.TextBox("DateofVisit", string.Empty, new { @id = "NewNote_DateofVisit" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide column">
            <div class="row ac">
                <%= Html.TextArea("Comments", string.Empty, new { @id = "NewNote_Comments", @class = "tallest required" })%>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("SendAsMessage", false, new { @id = "NewNote_SendAsMessage"})%>
                        <label for="NewNote_SendAsMessage">
                            Send note as Message</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="NewNote_Recipients">
                    <label class="strong">
                        Recipients</label>
                    <%= Html.Recipients("NewNote", new List<Guid>()) %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
            <input type="hidden" name="buttonClicked" id="buttonClicked" value="" />
        <ul>
            <li id="NewNote_SaveButton"><a class="save close">Save</a></li>
            <li id="NewNote_SendButton"><a class="complete">Send</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
    <%  } %>
</div>
