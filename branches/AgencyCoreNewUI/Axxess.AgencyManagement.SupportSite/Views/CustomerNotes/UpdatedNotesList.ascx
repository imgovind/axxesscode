﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InternalUserPermissionViewData>" %>
<div>
    <% var actions = new List<string>();
      if (Model.IsUserCanViewDetail)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Note.View('<#=Id#>','<#=AgencyId#>');return false\">View</a>");
       }
      if (Model.IsUserCanViewNote)
       {
           actions.Add("<a class=\"link\" onclick=\"Agency.Note.List('<#=AgencyId#>');return false\">Notes</a>");
       }
       var width = 70 + (actions.Count - 1) * 40;
       var actionLinks = actions.ToArray().Join("");
    %>
    <% Html.Telerik().Grid<CustomerNote>().Name("CustomerNotes_UpdatedGrid").HtmlAttributes(new { @class = "wrapper bottom-bar"}).Columns(columns =>
           {
               columns.Bound(c => c.AgencyName).ClientTemplate(Model.IsUserCanView ? "<a class=\"link\" onclick=\"Agency.Center.Load('<#=AgencyId#>');return false\"><#=AgencyName#></a>":string.Empty).Title("Agency Name").Sortable(true).Width(250);
               columns.Bound(c => c.RepName).Title("Customer Representative").Sortable(true).Width(180);
               columns.Bound(c => c.NoteTypeName).Title("Note Type").Sortable(true).Width(120);
               columns.Bound(c => c.TextStatus).Title("Status").Sortable(true).Width(80);
               columns.Bound(c => c.ModifiedFormatted).Title("Updated on").Sortable(true).Width(150);
               columns.Bound(c => c.Summary).Sortable(false);
               columns.Bound(c => c.Id).ClientTemplate(actionLinks).Title("Action").Sortable(false).Width(width).Visible(Model.IsUserCanViewDetail | Model.IsUserCanViewNote);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select("UpdatedNotesGrid", "CustomerNotes")).ClientEvents(events =>
                events.OnRowDataBound("CustomerNotes.RowBound")
                .OnDataBound("CustomerNotes.ApplyFilters")).Selectable().Sortable().Scrollable().Render(); %>
</div>