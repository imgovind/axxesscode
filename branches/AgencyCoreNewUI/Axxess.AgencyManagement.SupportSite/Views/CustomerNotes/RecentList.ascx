﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Customer Notes</span>
<div class ="wrapper main blue">
    <fieldset class="ac grid-controls">
        <div class="filter grid-search"></div>
    </fieldset>
    <% var actionUrl = "<a class=\"link\" onclick=\"Agency.Note.View('<#=Id#>','<#=AgencyId#>');return false\">View</a>"; %>
    <% Html.Telerik().Grid<CustomerNote>().Name("CustomerNotes_Grid").HtmlAttributes(new { @class = "wrapper bottom-bar" }).Columns(columns =>
           {
               columns.Bound(c => c.AgencyName).ClientTemplate("<a class=\"link\" onclick=\"Agency.Note.List('<#=AgencyId#>');return false\"><#=AgencyName#></a>").Title("Agency Name").Sortable(true).Width(250);
               columns.Bound(c => c.RepName).Title("Customer Representative").Sortable(true).Width(200);
               columns.Bound(c => c.CreatedFormatted).Title("Submitted on").Sortable(true).Width(150);
               columns.Bound(c => c.Summary).Sortable(false);
               columns.Bound(c => c.Id).ClientTemplate(actionUrl).Title("Action").Sortable(false).Width(80);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecentNotesGrid", "CustomerNotes")).Selectable().Sortable().Scrollable().Render(); %>
</div>