﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Smoke Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
       string browserInfo = "<strong>Your Browser Information</strong><br />"
           + "Name = " + browser.Browser + "<br />"
           + "Version = " + browser.Version + "<br />"
           + "Is Beta = " + browser.Beta + "<br />"
           + "Is Mobile = " + browser.IsMobileDevice + "<br />"
           + "User Agent = " + Request.UserAgent.ToLower() + "<br />";
    %>
    <%= browserInfo %>
</asp:Content>
    