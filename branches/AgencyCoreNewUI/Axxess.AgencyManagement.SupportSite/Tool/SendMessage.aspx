﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Message Send Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
        <% 
           //string welcomeBodyText = string.Empty;
           //string welcomeSubject = "Welcome to AgencyCore, rated the Most Recommended Home Health Software Solution";

           //welcomeBodyText = MessageBuilder.PrepareTextFrom("NewUserWelcome");
           //Notify.User(CoreSettings.NoReplyEmail, "aolowu@axxessconsult.com", welcomeSubject, welcomeBodyText, new string[] { "AgencyCore_Implementation_Checklist.docx", "New_Customer_Quick_Guide.docx", "PatientDataTemplate.xlsx" });

            var implementationSpecialist = ImplementationSpecialist.Instance.Next();
            string iSpecialistSubject = string.Format("New Home Health Agency {0} created.", "New Agency");
            string iSpecialistBodyText = MessageBuilder.PrepareTextFrom("ImplementationSpecialist",
               "displayname", implementationSpecialist.ToString(),
               "agencyname", "New Agency",
               "agencyphone", "817-355-5696");

           Notify.User(CoreSettings.NoReplyEmail, "aolowu@axxessconsult.com", iSpecialistSubject, iSpecialistBodyText, new string[] { "Implementation_ToDo_List.docx" });
        %>
</asp:Content>
    