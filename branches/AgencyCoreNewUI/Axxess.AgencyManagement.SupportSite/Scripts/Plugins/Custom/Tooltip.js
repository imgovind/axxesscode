﻿(function($) {
    $.extend($.fn, {
        Tooltip: function(options) {
            var settings = {
                top: 15,
                left: 0,
                hide: true,
                show: true,
                extraClass: "standard",
                bodyHandler: function(element) {
                    var text = element.attr("tooltip");
                    if (!text && element.is("a")) text = element.html();
                    return text;
                },
                isNote: false
            };
            if (typeof options == "string") {
                settings.bodyHandler = function() {
                    return options;
                };
                settings.hide = false;
                settings.show = false;
            } else if (typeof options == "object") $.extend(settings, options);
            return this.each(function() {
                var element = $(this),
                    text = settings.bodyHandler(element);
                if (text) {
                    if (settings.isNote) {
                        var color = "yellow";
                        if (element.hasClass("blue")) color = "blue";
                        if (element.hasClass("red")) color = "red";
                        settings.extraClass += " " + color;
                        element.on("click", function() {
                            $("body").append(
                                $("<div/>", { id: "shade" })).append(
                                    $("<div/>").addClass("note-modal bottom " + color)).append(
                                        $("<div/>").addClass("note-modal middle " + color)).append(
                                            $("<div/>").addClass("note-modal top " + color).append(
                                                $("<div/>").addClass("note-close").text("X")).append(
                                                    $("<div>").addClass("note-content").html(text)));
                            $(".note-close").on("click", function() {
                                $("#shade,.note-modal").remove();
                            });
                        });
                    }
                    if (!settings.items) settings.items = element.prop("tagName");
                    element.tooltip({
                        hide: settings.hide,
                        show: settings.show,
                        items: settings.items,
                        tooltipClass: settings.extraClass,
                        position: { my: "left" + (settings.left > 0 ? "+" + settings.left : "-" + settings.left) + " top" + (settings.top > 0 ? "+" + settings.top : "-" + settings.top), at: "left bottom", collision: "flipfit" },
                        track: settings.track,
                        content: function() { return text; }
                    });
                } else if (settings.isNote) {
                    element.hide();
                }
            });
        }
    });
})(jQuery);