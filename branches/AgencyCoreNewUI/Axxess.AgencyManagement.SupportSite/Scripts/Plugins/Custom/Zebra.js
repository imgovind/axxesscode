(function($) {
    $.extend($.fn, {
        Zebra: function(selector) {
            return this.each(function() {
                if (selector == undefined) var even = "tr:even,li:even", odd = "tr:odd,li:odd";
                else var even = selector + ":even", odd = selector + ":odd";
                $(even, this).addClass("t-alt even").removeClass("odd");
                $(odd, this).addClass("odd").removeClass("t-alt even");
            })
        }
    })
})(jQuery);