﻿(function($) {
    $.extend($.fn, {
        TogglePanels: function(options) {
            var acc, toggle;
            // Default options
            options = $.extend({ 
                "active": false
            }, options);
            toggle = function(target) {
                if (options.ontoggle !== undefined) {
                    if (typeof (options.ontoggle) !== 'function') {
                        console.log("opts.ontoggle is not a function");
                    }
                    else if (options.ontoggle(target) === false)
                        return;
                }
                $(target)
                .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
                .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
                .next().slideToggle();
            };

            acc = this.each(function() {
                $(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
                          .children("h3")
                          .addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
                          .hover(function() { $(this).toggleClass("ui-state-hover"); })
                          .not(".ui-accordion-no-expansion")
                          .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
                          .click(function() {
                              toggle(this);
                              return false;
                          })
                          .next()
                            .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
                            .hide();
            });
            $.each($(this).children("h3"), function(i) {
                if (options.active !== undefined) {
                    if (typeof (options.active) === 'object') {
                        if ($.inArray(i, options.active) !== -1)
                            toggle(this);
                    }
                    else if (typeof (options.active) === 'number') {
                        if (options.active === i)
                            toggle(this);
                    }
                }
            });
            this.getActive = function() {
                var isActive = [];
                $.each($(this).children("h3"), function(i) {
                    if ($(this).hasClass("ui-state-active"))
                        isActive.push(i);
                });
                return isActive;
            };
            return acc;
        }
    });
})(jQuery);