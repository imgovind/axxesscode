(function($) {
    $.extend($.fn, {
        // Plugin for AJAX-based autocomplete inputs
        AjaxAutocomplete: function(Options) {
            return this.each(function() {
                //  Options:
                //      ReverseLookup       URL to POST to for looking up literal value from hidden value
                //      MagnifyingGlass     Bool to switch on and off the Magnifying Glass icon
                //      SourceUrl           URL to obtain source JSON list
                //      ExtraParameters     JSON for additional variables to be sent to the server
                //      Width               Width of drop-down list in pixels, if undefined, will be set to match input field
                //      FilterSearch(Term)  Function used to parse search term prior to the AJAX call
                //      Format(Json)        Function to parse the JSON data and return a string formatted for the drop-down menu
                //      Select(Json, Input) Function called after menu selection has been made

                // If passing Options object and control is not already enabled with this plugin
                if (typeof Options === "object" && !$(this).hasClass("ajax-autocomplete")) {
                    // Create alias for control, remember existing maxlength settings, and create numeric iterator for concurrent uses of this plugin
                    var Input = $(this), MaxLength = $(this).attr("maxlength"),
                        Nth = Acore.AutocompleteId++,
                        Loaded = false;
                    // On focus of input box, set maxlength to seemingly unreachable thresholds, and clear the input of "Start Typing"
                    Input.addClass("ajax-autocomplete").focus(function() {
                        if (!Loaded) {
                            // Enable parent plugin
                            Input.autocomplete(Options);
                            // Set identifier on related jQuery UI autocomplete drop-down menu
                            $(".ui-autocomplete:last").attr("id", "autocomplete" + Nth);
                        }
                        $(this).attr("maxlength", 9999).AjaxAutocomplete("clear");
                    });
                    // If control works off a reverse lookup
                    if (Options.ReverseLookup != null) {
                        // Add hidden saving input after display input
                        Input.after(
					        $("<input/>", { type: "hidden", name: Input.attr("name"), value: Input.attr("value"), "class": (Input.attr("hiddenclass") && Input.attr("hiddenclass").length) ? Input.attr("hiddenclass") : "" })
    				    );
                        // Rename display input and set blur function to ensure an option is selected
                        Input.attr("name", Input.attr("name") + "_text");
                        Options.Select = function(json, input) {
                            input.val(json.Text).siblings("input[type=hidden]").val(json.Value);
                        };
                        Options.change = function() {
                            var Input = $(this), Guid = Input.siblings("input[type=hidden]").val();
                            if (Input.val().length > 0 && U.IsGuid(Guid)) U.PostUrl(Options.ReverseLookup, { "id": Guid }, function(data) {
                                // If reverse lookup is successful, set display input to reverse lookup response
                                Input.val(data);
                            }, function() {
                                // If reverse lookup failed, reset the inputs
                                Input.AjaxAutocomplete("reset").siblings("input[type=hidden]").val("");
                            })
                        };
                        // If input has an existing, GUID value, run reverse lookup to populate data
                        if (U.IsGuid(Input.val())) U.PostUrl(Options.ReverseLookup, { "id": Input.val() }, function(data) {
                            // If reverse lookup is successful, set hidden input to existing GUID value and set visible input to reverse lookup response
                            Input.siblings("input[type=hidden]").val(Input.val()).prev(".ajax-autocomplete").val(data);
                        }, function() {
                            // If reverse lookup failed, reset the inputs
                            Input.AjaxAutocomplete("reset");
                        });
                        else Input.AjaxAutocomplete("reset");
                        // Else if control doesn't use a reverse lookup
                    } else {
                        // Set blur function, if field is cleared, set input to reset settings
                        Input.blur(function() {
                            if (Input.val().length == 0) Input.AjaxAutocomplete("reset");
                        });
                        // If field is currently empty, set input to reset settings
                        if (Input.val().length == 0) Input.AjaxAutocomplete("reset");
                    }
                    // If the magnifying glass is enabled, add in the icon and set the click to empty search
                    if (Options.MagnifyingGlass) {
                        Input.next().after(
                            $("<span/>", { "class": "img icon find", status: Input.attr("status") }).click(function() {
                                $(this).siblings(".ajax-autocomplete").autocomplete("search", "")
                            })
                        );
                        Options.minLength = 0;
                    }
                    // Set parent plugin's source function (event when control is initialized and requests data for menu)
                    Options.source = function(request, response) {
                        // If filter search function exists, filter search term
                        if (typeof Options.FilterSearch == "function") request.term = Options.FilterSearch(request.term);
                        // Merge extra parameters with the current request object
                        if (typeof Options.ExtraParameters == "object") request = $.extend({}, request, Options.ExtraParameters);
                        // Make the request for menu options
                        U.PostUrl(Options.SourceUrl, request, function(data) {
                            // Build an array of server responses and pass it to the parent plugin's response function
                            var Text = new Array();
                            for (var i = 0; i < data.length; i++) Text[i] = JSON.stringify(data[i]);
                            response(Text);
                            Input.removeClass("ui-autocomplete-loading");
                        }, function() {
                            Input.removeClass("ui-autocomplete-loading");
                        })
                    };
                    // If width is not set, default it to input width
                    if (Options.Width == null) Options.Width = Input.width();
                    // Set parent plugin's open function (event when drop-down is first displayed)
                    Options.open = function(event, ui) {
                        // Set menu width
                        $("#autocomplete" + Nth).css("width", (Options.Width > 0 ? Options.Width : 200) + "px");
                        // Cycle through each menu item and format it per Format function
                        $(".ui-menu-item a", "#autocomplete" + Nth).each(function() {
                            var Json = JSON.parse($(this).text());
                            $(this).html(Options.Format(Json));
                        });
                    };
                    // Set parent plugin's select function (event when an option is selected)
                    Options.select = function(event, ui) {
                        // If validator has marked it as invalid, remove the error
                        if (Input.hasClass("error")) Input.removeClass("error").next("label.error").remove();
                        // Create JSON object and parse it through the Select function
                        var Json = JSON.parse(ui.item.value);
                        if (Json) Options.Select(Json, Input);
                        // If previously had maxlength set, restore that value
                        if (MaxLength > 0) Input.attr("maxlength", MaxLength);
                        // Return false, to not set input value back to JSON
                        return false;
                    };
                    // Set parent plugin's focus function (event when option is highlighted in the drop-down)
                    Options.focus = function(event, ui) {
                        // Set input to menu item's text
                        $(this).val($("#ui-active-menuitem").text());
                        return false;
                    };
                    // If issuing a command to an existing plugin
                } else if (typeof Options == "string" && $(this).hasClass("ajax-autocomplete")) {
                    // If command is reset, then set default settings
                    if (Options == "reset" && $(this).width() > 100) $(this).val("").addClass("input-directions");
                    if (Options == "clear") $(this).removeClass("input-directions");
                }
            })
        },
        // Plugin for standard (non-AJAX) autocomplete inputs
        Autocomplete: function(Options) {
            return this.each(function() {
                // If width is not set, default to control's width
                if (Options.Width == null) Options.Width = $(this).width();
                // Set parent plugin's open function (event when drop-down is first displayed)
                Options.open = function(event, ui) {
                    // Set menu width
                    $(ui)
                    $(".ui-autocomplete").css("width", Options.Width + "px");
                }
                // Enable parent plugin
                $(this).autocomplete(Options);
            })
        },
        // Plugin for Diagnosis, Procedure, and ICD-9 inputs
        IcdInput: function() {
            return this.each(function() {
                // Set type and url based upon control's class settings
                var type, url;
                if ($(this).hasClass("diagnosis")) type = "DIAGNOSIS", url = "LookUp/DiagnosisCode";
                else if ($(this).hasClass("icd")) type = "ICD", url = "LookUp/DiagnosisCode";
                else if ($(this).hasClass("procedureDiagnosis")) type = "PROCEDURE", url = "LookUp/ProcedureCode";
                else if ($(this).hasClass("procedureICD")) type = "PROCEDUREICD", url = "LookUp/ProcedureCode";
                else if ($(this).hasClass("diagnosisM1024")) type = "DIAGNOSIS", url = "LookUp/DiagnosisCodeNoVE";
                else if ($(this).hasClass("ICDM1024")) type = "icd", url = "LookUp/DiagnosisCodeNoVE";
                // If both variables are set, procede in enabling the plugin
                if (type && url) {
                    if ($(this).hasClass("icd") || $(this).hasClass("procedureICD") || $(this).hasClass("ICDM1024")) $(this).Decimal();
                    $(this).AjaxAutocomplete({
                        ExtraParameters: { "type": type },
                        minLength: 2,
                        SourceUrl: url,
                        Width: 400,
                        FilterSearch: function(Term) {
                            if (type == "ICD" || type == "PROCEDUREICD" || type == "icd") return Term.replace(/\./g, "");
                            else return Term;
                        },
                        Format: function(json) {
                            // Format menu to "ICD9 - Description"
                            return json.FormatCode + " &#8211; " + json.ShortDescription;
                        },
                        Select: function(json, input) {
                            // If standard diagnosis lookup
                            if (url == "LookUp/DiagnosisCode") {
                                // If Diagnosis
                                if (type == "DIAGNOSIS")
                                // Set diagnosis box to description
                                    input.val(json.ShortDescription)
                                // Jump up to closest list item or table row
                                    .closest("li,tr")
                                // Find corresponding ICD-9 box and set to ICD-9 code
                                    .find(".icd").AjaxAutocomplete("clear").val(json.FormatCode);
                                // If ICD-9
                                else
                                // Set ICD-9 box to ICD-9 code
                                    input.val(json.FormatCode)
                                // Jump up to closest list item or table row
                                    .closest("li,tr")
                                // Find corresponding diagnosis box and set to description
                                    .find(".diagnosis").AjaxAutocomplete("clear").val(json.ShortDescription);
                                // If not on the Plan of Care forms
                                if (!input.attr("id").match(/^Edit_485.*$/))
                                // Jump to closest list item or table row
                                    input.closest("li,tr")
                                // Find corresponding ICD-9 box and jump to its parent
                                    .find(".icd").parent()
                                // Find existing teaching guides, and remove is necessary
                                    .find(".teachingguide").remove()
                                // Jump out of teachingguides class selector
                                    .end()
                                // Append new teaching guide
                                    .append(
                                    $("<a/>", {
                                        "class": "teachingguide",
                                        "href": "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=" + json.FormatCode + "&informationRecipient.languageCode.c=en",
                                        "target": "_blank"
                                    })
                                )
                            }
                            // If procedure code
                            else if (type == "PROCEDURE")
                            // Set procedure box to description
                                input.val(json.ShortDescription)
                            // Jump to closest table row
                                .closest("li")
                            // Find corresponding ICD-9 box and set to ICD-9 code
                                .find(".procedureICD").AjaxAutocomplete("clear").val(json.FormatCode);
                            // If procedure ICD-9 code
                            else if (type == "PROCEDUREICD")
                            // Set ICD-9 box to ICD-9 code
                                input.val(json.FormatCode)
                            // Jump to closest table row
                                .closest("li")
                            // Find corresponding procedure box and set to description
                                .find(".procedureDiagnosis").AjaxAutocomplete("clear").val(json.ShortDescription);
                            // If payment diagnosis
                            else if (type == "DIAGNOSIS")
                            // Set diagnosis box to description
                                input.val(json.ShortDescription)
                            // Jump to next of class ICDM1024 and set value to ICD-9 code
                                .next(".ICDM1024").AjaxAutocomplete("clear").val(json.FormatCode);
                            else if (type == "icd")
                            // Set ICD-9 box to ICD-9 code
                                input.val(json.FormatCode)
                            // Jump to previous of class ICDM1024 and set value to ICD-9 code
                                .prev(".diagnosisM1024").AjaxAutocomplete("clear").val(json.ShortDescription);
                        }
                    })
                }
            })
        },
        // Plugin for populating physician data per NPI search
        NpiSearch: function(Prefix) {
            return this.each(function() {
                $(this).AjaxAutocomplete({
                    minLength: 1,
                    SourceUrl: "LookUp/Npis",
                    Format: function(Json) { return Json.Id + " &#8211 " + Json.ProviderFirstName + "  " + Json.ProviderLastName },
                    Select: function(Json, Input) {
                        Input.val(Json.Id);
                        if (id = Json.Id) $(Prefix + "NpiNumber").val(id).blur();
                        if (fname = Json.ProviderFirstName) $(Prefix + "FirstName").val(U.ToTitleCase(fname));
                        if (mname = Json.ProviderMiddleName) $(Prefix + "MiddleIntial").val(mname.substring(0, 1).toUpperCase());
                        if (lname = Json.ProviderLastName) $(Prefix + "LastName").val(U.ToTitleCase(lname));
                        if (cred = Json.ProviderCredentialText) $(Prefix + "Credentials").val(cred);
                        if (add1 = Json.ProviderFirstLineBusinessPracticeLocationAddress) $(Prefix + "AddressLine1").val(U.ToTitleCase(add1));
                        if (add2 = Json.ProviderSecondLineBusinessPracticeLocationAddress) $(Prefix + "AddressLine2").val(U.ToTitleCase(add2));
                        if (city = Json.ProviderBusinessPracticeLocationAddressCityName) $(Prefix + "AddressCity").val(U.ToTitleCase(city));
                        if (state = Json.ProviderBusinessPracticeLocationAddressStateName) $(Prefix + "AddressStateCode").val(state);
                        if (zip = Json.ProviderBusinessPracticeLocationAddressPostalCode) $(Prefix + "AddressZipCode").val(zip.substring(0, 9));
                        if (phone = Json.ProviderBusinessPracticeLocationAddressTelephoneNumber) {
                            $(Prefix + "Phone1").val(phone.substring(0, 3));
                            $(Prefix + "Phone2").val(phone.substring(3, 6));
                            $(Prefix + "Phone3").val(phone.substring(6, 10))
                        }
                        if (fax = Json.ProviderBusinessPracticeLocationAddressFaxNumber) {
                            $(Prefix + "Fax1").val(fax.substring(0, 3));
                            $(Prefix + "Fax2").val(fax.substring(3, 6));
                            $(Prefix + "Fax3").val(fax.substring(6, 10))
                        }
                    }
                })
            })
        },
        // Plugin for Physician Selector
        PhysicianInput: function() {
            return this.each(function() {
                $(this).AjaxAutocomplete({
                    MagnifyingGlass: true,
                    Width: 183,
                    ReverseLookup: "Lookup/PhysicianName",
                    SourceUrl: "LookUp/Physicians",
                    Format: function(json) {
                        return json.Text;
                    }
                })
            })
        }
    })
})(jQuery);