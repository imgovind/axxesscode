﻿(function($) {
    $.extend($.fn, {
        Tree: function(Function) {
            return this.each(function() {
                if (typeof Function === "string") {
                    if (Function === "reset") {
                        var allMainCheckBox = $(this).closest("fieldset").find(".allpermissions");
                        if (allMainCheckBox.prop("checked")) allMainCheckBox.click();
                        else $(this).find("input[type=checkbox]").prop("indeterminate", false).prop("checked", false);
                    }
                }
                else {
                    $(".select-all", $(this)).each(function() {
                        var checkbox = $(this),
                        permission = checkbox.attr("permission"),
                        service = checkbox.attr("service"),
                        target = checkbox.attr("target"),
                        filter = "[value=" + service + "]" + (permission ? "[permission=" + permission + "]" : ""),
                        group;
                        //Targets all the checkboxes within the div next to the select all
                        if (target == "div") group = checkbox.parent().next("div").find("input[type=checkbox]");
                        //Targets all the checkboxes that match the service in the acore grid
                        else if (target == "ol") group = checkbox.closest(".acore-grid").find("ol input[type=checkbox]").filter(filter);
                        else if (target == "all") {
                            var tempGroup = checkbox.closest(".acore-grid").find("ol input[type=checkbox]").filter(filter).toArray();
                            group = $(tempGroup.concat(checkbox.closest("span").find("input[type=checkbox]").not(checkbox).toArray()));
                        }
                        if (group) {
                            checkbox.data("group", group);
                            group.each(function() {
                                var selectAll;
                                if ($(this).hasClass("select-all") && permission) selectAll = checkbox.closest("span").find("input[type=checkbox]")[0];
                                else selectAll = checkbox[0];
                                $(this).data("all", selectAll);
                            });
                        }
                    });
                    $(this).on("evaluate change", "input[type=checkbox]", function(e) {
                        var checkbox = $(this),
                        all = checkbox.data("all");
                        if (checkbox.hasClass("select-all")) {
                            var group = checkbox.data("group"),
                            indeterminate = false;
                            if (e.type == "evaluate") {
                                var checkedGroup = group.filter(":checked"),
                                check = checkedGroup.length == group.length;
                                indeterminate = checkedGroup.length > 0 && !check;
                                checkbox.prop("checked", check);
                            } else if (e.type == "change") {
                                group.prop("indeterminate", false);
                                group.prop("checked", checkbox.prop("checked"));
                            }
                            checkbox.prop("indeterminate", indeterminate);
                        }
                        if (all) $(all).trigger("evaluate");
                    });
                }
            });
        }
    });
})(jQuery);