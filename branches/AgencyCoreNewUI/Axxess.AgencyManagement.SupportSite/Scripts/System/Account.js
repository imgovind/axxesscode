﻿var Account = {
    Animated: false,
    AnimationSpeed: 500,
    Promo: false,
    AccountInUse: function(Id, Email) {
        $(".loading-modal").hide();
        $(".login-promt").empty().append(
            $("<h3/>", { "text": "Account Already in Use" })).append(
            $("<p/>", { "text": "This user is already logged in on another computer. If you choose to proceed, the user will be automatically logged off the other computer and their work will not be saved." })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "text": "Continue" }).click(function() {
                            if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(".loading-modal").show() });
                            else {
                                $(".login-promt").hide();
                                $(".loading-modal").show();
                            }
                            U.PostUrl("Account/Kick", { loginId: Id, emailAddress: Email }, function(result) {
                                if (result.isSuccessful) $(location).attr("href", result.redirectUrl);
                                else {
                                    U.Growl(result.errorMessage, "error");
                                    U.Loading(false);
                                    if (Account.Animated) $(".login-bar").animate({ right: "0em" }, Account.AnimationSpeed, function() { if (Account.Promo) $(".promo-container").fadeIn(Account.AnimationSpeed) });
                                    else {
                                        $(".login-bar").css("right", 0);
                                        if (Account.Promo) $(".promo-container").show();
                                    }
                                }
                            });
                            return false;
                        }))).append(
                    $("<li/>").append(
                        $("<a/>", { "text": "Cancel" }).click(function() {
                            U.Loading(false);
                            if (Account.Animated) {
                                $(".login-promt").fadeOut(Account.AnimationSpeed);
                                $(".login-bar").animate({ right: "0em" }, Account.AnimationSpeed, function() { if (Account.Promo) $(".promo-container").fadeIn(Account.AnimationSpeed) });
                            } else {
                                $(".login-promt").hide();
                                $(".login-bar").css("right", 0);
                                if (Account.Promo) $(".promo-container").show();
                            }
                            return false;
                        })
                    )
                )
            )
        );
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed)
        else $(".login-promt").show();
    },
    AccountLocked: function() {
        $(".loading-modal").hide();
        $(".login-promt").empty().append(
            $("<h3/>", { "text": "Account Locked" })).append(
            $("<p/>", { "text": "Due to excessive failed login attempts, this account has been locked.  Please wait five minutes before trying to login to this account again." })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "text": "OK" }).click(function() {
                            U.Loading(false);
                            if (Account.Animated) {
                                $(".login-promt").fadeOut(Account.AnimationSpeed);
                                $(".login-bar").animate({ right: "0em" }, Account.AnimationSpeed, function() { if (Account.Promo) $(".promo-container").fadeIn(Account.AnimationSpeed) });
                            } else {
                                $(".login-promt").hide();
                                $(".login-bar").css("right", 0);
                                if (Account.Promo) $(".promo-container").show();
                            }
                        })
                    )
                )
            )
        );
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed)
        else $(".login-promt").show();
    },
    AgencySelect: function(Id) {
        $(".loading-modal").hide();
        $(".login-promt").empty().load("Account/Agencies", { loginId: Id }, function() {
            if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
            else $(".login-promt").show();
        })
    },
    Fail: function() {
        $(".loading-modal").hide();
        $(".login-promt").empty().append(
            $("<h3/>", { "text": "Login Failed" })).append(
            $("<p/>", { "text": "We could not validate your email address and password. Please check this information and try again." })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "text": "OK" }).click(function() {
                            U.Loading(false);
                            if (Account.Animated) {
                                $(".login-promt").fadeOut(Account.AnimationSpeed);
                                $(".login-bar").animate({ right: "0em" }, Account.AnimationSpeed, function() { if (Account.Promo) $(".promo-container").fadeIn(Account.AnimationSpeed) });
                            } else {
                                $(".login-promt").hide();
                                $(".login-bar").css("right", 0);
                                if (Account.Promo) $(".promo-container").show();
                            }
                        })
                    )
                )
            )
        );
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
        else $(".login-promt").show();
    },
    Init: function() {
        var a = $("form");
        $("form").validate({
            messages: { UserName: "", Password: "" },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    beforeSubmit: function(values, form, options) {
                        U.Loading(true);
                        if (Account.Animated) {
                            $(".login-bar").animate({ right: "-50em" }, Account.AnimationSpeed);
                            if (Account.Promo) $(".promo-container").fadeOut(Account.AnimationSpeed);
                        } else {
                            $(".login-bar").css("right", "-50em");
                            if (Account.Promo) $(".promo-container").hide();
                        }
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            if (result.hasMultiple) Account.AgencySelect(result.id);
                            else $(location).attr("href", result.redirectUrl);
                        } else if (result.isAccountInUse) Account.AccountInUse(result.id, result.email);
                        else if (result.isLocked) Account.AccountLocked();
                        else Account.Fail();
                    }
                });
                return false;
            }
        });
        if (Account.Animated) $(".login-bar").animate({ right: "0em" }, Account.AnimationSpeed, function() { if (Account.Promo) $(".promo-container").fadeIn(Account.AnimationSpeed) });
        else {
            $(".login-bar").css("right", 0);
            if (Account.Promo) $(".promo-container").show();
        }
        $(".submit").click(function() { $("[type=submit]").click() });
        $(".forgot").click(function() {
            U.Loading(true);
            if (Account.Animated) {
                if (Account.Promo) $(".promo-container").fadeOut(Account.AnimationSpeed);
                $(".login-bar").animate({ right: "-50em" }, Account.AnimationSpeed, function() { $(location).attr("href", "Forgot") });
            } else {
                $(".login-bar").css("right", "-50em");
                if (Account.Promo) $(".promo-container").hide();
                $(location).attr("href", "Forgot");
            }
            return false;
        });
    },
    InitActivate: function() {
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
        else $(".login-promt").show();
        $("#activateAccountForm").validate({
            rules: { Password: { required: true, minlength: 8} },
            messages: { Password: { required: "* required", minlength: "8 characters minimum"} },
            submitHandler: function(form) {
                if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(".shade").remove(); U.Loading(true); })
                else {
                    $(".login-promt").hide();
                    $(".shade").remove();
                    U.Loading(true);
                }
                $(form).ajaxSubmit({
                    dataType: "json",
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $("#login-promt").empty().append(
                                $("<h3/>", { "text": "Activation Failed" })).append(
                                $("<p/>", { "text": result.errorMessage }));
                            $(".loading-modal").remove();
                            if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
                            else $(".login-promt").show();
                        }
                    }
                });
                return false;
            }
        })
        $(".submit").click(function() {
            $(this).closest("form").submit();
            return false;
        });
    },
    InitExpire: function() {
        $("html").bind("keypress", function(e) { if (e.keyCode == 13) $(location).attr("href", "Logout") });
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
        else $(".login-promt").show();
        $(".login").click(function() {
            if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(location).attr("href", "Logout") });
            else {
                $(".login-promt").hide();
                $(location).attr("href", "Logout");
            }
            return false;
        })
    },
    InitForgotPassword: function() {
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
        else $(".login-promt").show();
        $("#ForgotPasswordForm").submit(function() {
            if ($("#forgot-user").val().length && U.IsEmail($("#forgot-user").val()) && $("#recaptcha_response_field").val().length) $(this).closest("form").ajaxSubmit({
                dataType: "json",
                success: function(result) {
                    if (result.isSuccessful) {
                        if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(location).attr("href", "Login") });
                        else {
                            $(".login-promt").hide();
                            $(location).attr("href", "Login");
                        }
                    } else U.Growl(result.errorMessage, "error")
                }
            });
            else U.Growl("Please Enter your Email Address and Security Check", "error");
            return false;
        });
        $(".login-promt .cancel").click(function() {
            $(this).closest("form").submit();
            return false;
        });
        $(".login-promt .cancel").click(function() {
            if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(location).attr("href", "Login") });
            else {
                $(".login-promt").hide();
                $(location).attr("href", "Login");
            }
            return false;
        })
    },
    InitLink: function() {
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
        else $(".login-promt").show();
        $(".login").click(function() {
            if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(location).attr("href", "Login") });
            else {
                $(".login-promt").hide();
                $(location).attr("href", "Login");
            }
            return false;
        })
    },
    InitReset: function() {
        if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
        else $(".login-promt").show();
        $("#ResetForm").submit(function() {
            if (($("#Password").length && !$("#Password").val().length) || ($("#Signature").length && !$("#Signature").val().length)) {
                $("#Password,#Signature").addClass("error");
                return false;
            }
            var ResetType = $("#ChangePassword_ResetType").val(),
                Args = {
                    LoginId: $("#ChangePassword_LoginId").val(),
                    EmailAddress: $("#ChangePassword_EmailAddress").val(),
                    ResetType: ResetType
                };
            if (ResetType == "Password") $.extend(Args, { Password: $("#Password").val() });
            else if (ResetType == "Signature") $.extend(Args, { Signature: $("#Signature").val() });
            if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed);
            else $(".login-promt").hide();
            U.PostUrl("Reset", Args, function(result) {
                $(".login-promt").empty().append(
                    $("<h3/>").text("Reset " + (result.isSuccessful ? "Successful" : "Failed"))).append(
                    $("<p/>").text(result.errorMessage)).append(
                    $("<div/>", { "class": "buttons" }).append(
                        $("<ul/>").append(
                            $("<li/>").append(
                                $("<a/>", { "text": "OK" }).click(function() {
                                    if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(location).attr("href", "/") });
                                    else {
                                        $(".login-promt").hide();
                                        $(location).attr("href", "/");
                                    }
                                    return false;
                                })
                            )
                        )
                    )
                );
                if (Account.Animated) $(".login-promt").fadeIn(Account.AnimationSpeed);
                else $(".login-promt").show();
            });
            return false;
        });
        $(".submit").click(function() {
            $(this).closest("form").submit();
            return false;
        });
        $(".login").click(function() {
            if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed, function() { $(location).attr("href", "/") });
            else {
                $(".login-promt").hide();
                $(location).attr("href", "/");
            }
            return false;
        })
    },
    Select: function(agencyId, userId) {
        if (Account.Animated) $(".login-promt").fadeOut(Account.AnimationSpeed);
        else $(".login-promt").hide();
        U.PostUrl("Account/Agency", { agencyId: agencyId, userId: userId }, function(result) {
            if (result.isSuccessful) $(location).attr("href", result.redirectUrl);
            else if (result.isAccountInUse) Account.AccountInUse(result.id, result.email);
            else Account.Fail();
        })
    }
}