﻿var ToolTip = {
    Options: { track: true, showURL: false, bodyHandler: function() { return $(this).attr("tooltip") } },
    Modal: function(Text, Color) {
        if (Color != "red" && Color != "blue") Color = "yellow";
        $("body").append(
            $("<div/>", { id: "shade" })).append(
            $("<div/>", { "class": "note-modal bottom " + Color })).append(
            $("<div/>", { "class": "note-modal middle " + Color })).append(
            $("<div/>", { "class": "note-modal top " + Color }).append(
                $("<div/>", { id: "note-close", text: "X" }).click(function() {
                    $("#shade,.note-modal").remove()
                })).append(
                $("<p/>").text(Text)));
    },
    InitNote: function(e) {
        if (e.type == "dataBound") e = e.target;
        $(".sticky-note-icon", e).each(function() {
            if ($(this).attr("tooltip")) {
                if ($(this).hasClass("blue")) var c = "blue";
                else if ($(this).hasClass("red")) var c = "red";
                else var c = "";
                var Options = { top: 5, left: -15, extraClass: "sticky-note " + c };
                $.extend(Options, ToolTip.Options);
                $(this).click(function() { ToolTip.Modal($(this).attr("tooltip"), c) });
                $(this).tooltip(Options);
            } else $(this).remove();
        })
    },
    Init: function(e) {
        if ($(".sticky-note-icon", e).length) ToolTip.InitNote(e);
        $("[tooltip]:not(.sticky-note-icon)", e).each(function() {
            if ($(this).hasClass("error")) var c = "error";
            if ($(this).attr("tooltip")) {
                var Options = { top: 10, left: 10, extraClass: c };
                $.extend(Options, ToolTip.Options);
                $(this).tooltip(Options);
            }
        })
    }
};