﻿$.extend(U, {
    // Display temporary notification on the top right corner of the window
    Growl: function(Message, Theme) {
        if (typeof Message == "object") Message = Message.toSource();
        else if (typeof Message != "string") Message = String(Message);
        $.jGrowl($.trim(Message), { theme: Theme, life: 5000 })
    },
    GrowlResult: function(result) {
        U.Growl(result.errorMessage, result.isSuccessful ? "success" : "error");
    },
    Message: function(Title, Text, Class) {
        return $("<div/>", { "class": "error-box" }).addClass(Class).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon float-left" }).addClass(Class)).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageError: function(Title, Text) {
        return $("<div/>", { "class": "error-box" }).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon error float-left" })).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageErrorAjax: function() {
        return U.MessageError("Request Error", "An error has been detected while procuring data. Please check your connection and try again, if problem persists, contact Axxess for assistance.");
    },
    MessageErrorJS: function(Growl) {
        var Title = "Browser Error",
            Text = "An error has been detected in your browser. Please refresh the page and try again, if problem persists, contact Axxess for assistance.";
        if (Growl) return Title + "<br />" + Text;
        else return U.MessageError(Title, Text);
    },
    MessageInfo: function(Title, Text) {
        return U.Message(Title, Text, "info")
    },
    MessageSuccess: function(title, text) {
        return U.Message(Title, Text, "success")
    },
    MessageWarn: function(Title, Text) {
        return U.Message(Title, Text, "warning")
    }
});