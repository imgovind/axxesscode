﻿var Agency = {
    AcoreId: "ListAgencies",
    Location: {
        Delete: function(id, agencyId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this location?",
                Yes: function() { U.GridAjaxCommand("Agency/Location/Delete", { id: id, agencyId: agencyId }, $grid) }
            })
        },
        Edit: function(id, agencyId) { Acore.Open("EditLocation", { id: id, agencyId: agencyId }) },
        InitEdit: function(r, t, x, e) {
            Agency.Location.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Agency.Location.Logs($("[name=Id]", e).val(), $("[name=AgencyId]", e).val());
                return false;
            });
        },
        InitList: function(r, t, x, e) {
            $(".new-location", e).click(function() {
                Agency.Location.New($("[name=AgencyId]", e).val());
                return false;
            })
        },
        InitNew: function(r, t, x, e) { Agency.Location.InitShared(r, t, x, e) },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.ShowIfChecked($("[name=IsLocationStandAlone]", e), $(prefix + "StandAloneInfoContent"));
            U.HideIfChecked($("[name=IsAxxessTheBiller]", e), $(prefix + "SubmiterInfoContent"));
            U.ShowIfSelectEquals($("[name=BranchId]", e), "Other", $("[name=BranchIdOther]", e));
            U.ShowIfRadioEquals("IsUserPlan", "true", $(prefix + "UserSubscriptionPlan"));
            U.ShowIfRadioEquals("IsUserPlan", "false", $(prefix + "PatientSubscriptionPlan"));
            U.InitShared(r, t, x, e);
        },
        List: function(agencyId) { Acore.Open("ListAgencyLocations", { agencyId: agencyId }) },
        New: function(agencyId) { Acore.Open("NewLocation", { agencyId: agencyId }) },
        Logs: function(id, agencyId) { Acore.Open("LocationLogs", { id: id, agencyId: agencyId }) }
    },
    Note: {
        ViewAcoreId: "ViewNote",
        ListAcoreId: "ListNotes",
        Delete: function(id, agencyId, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this note?",
                Yes: function() { U.GridAjaxCommand("CustomerNotes/Note/Delete", { id: id, agencyId: agencyId }, $grid) }
            })
        },
        New: function(agencyId) { Acore.Open("NewNote", { agencyId: agencyId }) },
        InitNew: function(r, t, x, e) {
            var DataLoad = "data load";
            var notRedmine = new Array("online training", "onsite visit", "complaint");
            Agency.Note.InitShared(r, t, x, e);
            $("#NewNote_NoteType", e).change(function() {
                if (this.options[this.selectedIndex].text.toLowerCase().indexOf(DataLoad) !== -1) {
                    $("#NewNote_PreviousSoftwareDiv", e).show();
                } else {
                    $("#NewNote_PreviousSoftwareDiv", e).hide();
                }
                if ($.inArray(this.options[this.selectedIndex].text.toLowerCase(), notRedmine) != -1) {
                    $("a", "#NewNote_SendButton").text("Submit");
                    $("a", "#NewNote_SaveButton").text("Save for Later");
                } else {
                    $("a", "#NewNote_SaveButton").text("Save for Later");
                    $("a", "#NewNote_SendButton").text("Send to Developers");
                }
            });
        },
        Edit: function(id, agencyId) { Acore.Open("EditNote", { id: id, agencyId: agencyId }) },
        InitEdit: function(r, t, x, e) {
            var DataLoad = "data load";
            Agency.Note.InitShared(r, t, x, e);
            Agency.Note.ChangeEditButtons($("#EditNote_NoteType", e));
            $("#EditNote_NoteType", e).change(function() {
                if (this.options[this.selectedIndex].text.toLowerCase().indexOf(DataLoad) !== -1) {
                    $("#EditNote_PreviousSoftwareDiv", e).show();
                } else {
                    $("#EditNote_PreviousSoftwareDiv", e).hide();
                }
                Agency.Note.ChangeEditButtons(this);
            });
        },
        ChangeEditButtons: function(selectBox) {
            var notRedmine = new Array("online training", "onsite visit", "complaint");
            if ($.inArray(selectBox.get(0).options[selectBox.get(0).selectedIndex].text.toLowerCase(), notRedmine) != -1) {
                $("a", "#EditNote_SendButton").text("Submit");
                $("a", "#EditNote_SaveButton").text("Save for Later");
            } else {
                $("a", "#EditNote_SaveButton").text("Save for Later");
                $("a", "#EditNote_SendButton").text("Send to Developers");
            }
        },
        List: function(agencyId) { Acore.Open("ListNotes", { agencyId: agencyId }) },
        InitList: function(r, t, x, e) {
            $(".new-note", e).click(function() {
                Agency.Note.New($("[name=AgencyId]", e).val());
                return false;
            });
            $(".export", e).click(function() {
                Agency.Note.Export($("[name=AgencyId]", e).val());
                return false;
            });
            $(".currentUser-tab").click(function() {
                Agency.Note.TabClick(this);
            });
            $(".filter input").keyup(function() {
                if ($(".currentUser-tab").hasClass("selected")) {
                    var grid = $(".t-grid-content", "#window_" + Agency.Note.ListAcoreId + "_content");
                    $(".match", grid).hide();
                    $(".match.nameMatch", grid).show().removeClass("t-alt").filter(":even").addClass("t-alt");
                } else {
                    $(".match", grid).show().removeClass("t-alt").filter(":even").addClass("t-alt");
                }
            });
        },
        TabClick: function(e) {
            var window = $("#window_" + Agency.Note.ListAcoreId + "_content");
            var displayName = $("#NoteList_DisplayName", window).val();
            e = $(e);
            if (!e.hasClass("selected")) {
                e.addClass("selected");
                var col;
                $(".t-grid-header th", window).each(function() {
                    if ($(this).text() == "Rep Name") {
                        col = $(this).parent().children().index($(this));
                    }
                });
                col++;
                $(".t-grid-content tr", window).each(function() {
                    $(this).removeClass("nameMatch").hide();
                    var text = $.trim($("td:nth-child(" + col + ")", this).text().toLowerCase());
                    if ($.trim($("td:nth-child(" + col + ")", this).text().toLowerCase()) == $.trim(displayName.toLowerCase())) {
                        $(this).addClass("nameMatch");
                    }
                });
                if ($(".filter input", window).val()) {
                    $(".match.nameMatch", $(".t-grid-content", window)).show().removeClass("t-alt").filter(":even").addClass("t-alt");
                } else {
                    $(".nameMatch", $(".t-grid-content", window)).show().removeClass("t-alt").filter(":even").addClass("t-alt");
                }
            } else {
                e.removeClass("selected");
                if ($(".filter input", window).val()) {
                    $("tr", $(".t-grid-content", window)).removeClass("nameMatch");
                    $(".match", $(".t-grid-content", window)).show().removeClass("t-alt").filter(":even").addClass("t-alt");
                } else {
                    $("tr", $(".t-grid-content", window)).removeClass("nameMatch").show().removeClass("t-alt").filter(":even").addClass("t-alt");
                }
            }
        },
        View: function(id, agencyId) { Acore.Open("ViewNote", { id: id, agencyId: agencyId }) },
        InitView: function() {
            var e = $(".dev-comments", "#window_" + Agency.Note.ViewAcoreId + "_content");
            $("li div", e).addClass("column fl");
            $("li p", e).each(function() {
                $(this).wrapAll("<div class='column fr'></div>");
            });
        },
        InitShared: function(r, t, x, e) {
            var type = U.GetType(e), prefix = "#" + type + "_";
            U.ShowIfChecked($("[name=SendAsMessage]", e), $(prefix + "Recipients"));
            U.InitShared(r, t, x, e);
        },
        Export: function(agencyId) {
            U.GetAttachment("CustomerNotes/Note/Export", {
                agencyId: agencyId
            })
        },
        RowDataBound: function(e) {
            var data = e.dataItem;

            if (!data.IsEditable) {
                $(".t-last", e.row).find(".edit").remove();
            }

        }
    },
    Physician: {
        List: function(agencyId) { Acore.Open("ListAgencyPhysicians", { agencyId: agencyId }) },
        ResendActivationLink: function(userId, agencyId) { Agency.ResendActivationLink("physician", userId, agencyId) }
    },
    User: {
        InitEdit: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
            $(".activity-log", e).click(function() {
                Agency.User.Logs($("[name=id]", e).val());
                return false;
            });
        },
        Edit: function(id) { Acore.Open("EditAgencyUser", { id: id }) },
        List: function(agencyId) { Acore.Open("ListAgencyUsers", { agencyId: agencyId }) },
        ResendActivationLink: function(userId, agencyId) { Agency.ResendActivationLink("user", userId, agencyId) },
        Logs: function(id) { Acore.Open("AgencyUserLogs", { id: id }) }
    },
    Details: function(id) {
        Acore.Dialog({
            Name: "Agency Details",
            Url: "Agency/Details",
            Input: { id: id },
            Width: 350
        });
    },
    Edit: function(id) { Acore.Open("EditAgency", { id: id }) },
    Export: function() {
        var stateFilter = $("#AgencyList_StateFilter").val()
        var grid = $("#ListAgencies_Grid").data('tGrid');
        var start = "";
        var end = "";
        var month = $("#ListAgencies_Month").val();
        if (month == 13) {
            start = $("#ListAgencies_StartDate").val();
            end = $("#ListAgencies_EndDate").val();
        }
        if (month != "13" && month != "0") {
            var year = $("#ListAgencies_Year").val();
            var month = $("#ListAgencies_Month").val();
            start = new Date(year, month - 1, 1).toJSON();
            end = new Date(year, month, 0).toJSON();
        }
        var data = { clusterId: $("#ListAgencies_ClusterId").val(), stateFilter: $("#AgencyList_StateFilter").val() !== " " ? $("#AgencyList_StateFilter").val() : "all", startDate: start, endDate: end };
        U.GetAttachment("Agency/Export", data)
    },
    Filter: function() {
        var e = $("#window_" + Agency.AcoreId), red = $(".redrow.age-filter").hasClass("selected"), blue = $(".bluerow.age-filter").hasClass("selected"), black = $(".blackrow.age-filter").hasClass("selected"),
            i = 1,
            match = "";
        if ($(".grid-search input", e).val().length) {
            match = ".match";
        }
        if (red && blue && black) {
            $(".t-grid-content tr" + match, e).each(function() {
                $(this).show();
                this.cells[0].innerHTML = i++;
            });
        } else {
            $(".t-grid-content .blackrow" + match, e).each(function() {
                $(this).toggle(black);
                if (black) this.cells[0].innerHTML = i++;
            });
            $(".t-grid-content .bluerow" + match, e).each(function() {
                $(this).toggle(blue);
                if (blue) this.cells[0].innerHTML = i++;
            });
            $(".t-grid-content .redrow" + match, e).each(function() {
                $(this).toggle(red);
                if (red) this.cells[0].innerHTML = i++;
            });
            if ($(".grid-search input", e).val().length) $(".t-grid-content", e).Zebra("tr.match:visible");
            else $(".t-grid-content", e).Zebra("tr:visible");
        }
    },
    FilterByState: function() {
        var e = $("#window_" + Agency.AcoreId);
        var search = $(".state-filter", e).val();
        var col;
        var ths = $(".t-grid-header th", e);
        ths.each(function() {
            if ($(this).html() == "Location") {
                col = $(this).index();
            }
        });
        col++;
        $(".t-grid-content", e).find("tr").removeClass("match t-alt").show();
        var tds = $(".t-grid-content", e).find("td:nth-child(" + col + ")")
        tds.each(function() {
            var text = $(this).text().split(",")[1];
            if (text != undefined) {
                text = text.toLowerCase();
                if (text.indexOf(search.toLowerCase()) > -1)
                    $(this).parent().addClass("match");
            }
        });

        $(".t-grid-content", e).find("tr").not(".match").hide();
        //$(".t-grid-content", e).find("tr").rowIndex = 0;
        if ($(".t-grid-content .match", e).length)
            $(".t-grid-content tbody", e).Zebra(".match");

    },
    InitEdit: function(r, t, x, e) {
        Agency.InitShared(r, t, x, e);
        $(".activity-log", e).click(function() {
            Agency.Logs($("[name=Id]", e).val());
            return false;
        });
        $("#EditAgency_Services").MultiSelect({
            oneOrMoreSelected: '*'
        });
    },
    InitList: function(r, t, x, e) {
        $(".new-agency", e).click(function() {
            Agency.New();
            return false;
        });
        $(".export", e).click(function() {
            Agency.Export();
            return false;
        });
        $(".age-filter", e).click(function() {
            $(this).toggleClass("selected");
            Agency.Filter();
        });
        //        $(".state-filter", e).change(function() {
        //            Agency.FilterByState();
        //        });
        $("#ListAgencies_Month").change(function() {
            var value = $(this).val(),
            dateRange = $("#ListAgencies_DateRange"),
            yearDropDown = $("#ListAgencies_Year");
            if (value == 0) {
                dateRange.hide();
                yearDropDown.hide();
            } else if (value == 13) {
                dateRange.show();
                yearDropDown.hide();
            } else {
                yearDropDown.show()
                dateRange.hide();
            }
        }).change();
        U.ActivityGridContextMenuInit($("#ListAgencies_Grid", e));
        $(".t-grid", e).bind("additionalFilters", function() {
            Agency.Filter();
        });
    },
    GetRowIndex: function() {
        return ++$('#ListAgencies_Grid').data('tGrid').rowIndex;
    },
    OnLoad: function(e) {
        var grid = $(this).data("tGrid");
        grid.rowIndex = 0;
    },
    OnDataBound: function(e) {
        $(".grid-search input", $(this).closest(".window")).keyup();
    },
    RebindList: function() {
        var grid = $("#ListAgencies_Grid").data('tGrid')
        var start = "";
        var end = "";
        var month = $("#ListAgencies_Month").val();
        if (month != "13" && month != "0") {
            var year = $("#ListAgencies_Year").val();

            start = new Date(year, month - 1, 1).toJSON();
            end = new Date(year, month, 0).toJSON();
        } else if (month == "13") {

            start = $("#ListAgencies_StartDate").val();
            end = $("#ListAgencies_EndDate").val();
        }

        var data = { clusterId: $("#ListAgencies_ClusterId").val(), stateFilter: $("#AgencyList_StateFilter").val() !== " " ? $("#AgencyList_StateFilter").val() : "all", startDate: start, endDate: end }
        if (grid != null) grid.rebind(data);
    },

    InitNew: function(r, t, x, e) {
        $("[name=NewAgency_SameAsAdmin]").change(function() {
            if ($(this).prop("checked")) {
                $("#NewAgency_ContactPersonEmail").val($("#NewAgency_AdminUsername").val());
                $("#NewAgency_ContactPersonFirstName").val($("#NewAgency_AdminFirstName").val());
                $("#NewAgency_ContactPersonLastName").val($("#NewAgency_AdminLastName").val());
            } else $("#NewAgency_ContactPersonEmail,#NewAgency_ContactPersonFirstName,#NewAgency_ContactPersonLastName").val("");
        });
        $("#ESign_Agencies").change(function() {
            Agency.ESign.GetInformation($(this).val())
        });
        Agency.ESign.InitList();
        $("#NewAgency_Services").MultiSelect({
            oneOrMoreSelected: '*'
        });
        Agency.InitShared(r, t, x, e);
    },
    InitShared: function(r, t, x, e) {
        var type = U.GetType(e), prefix = "#" + type + "_";
        if ($(prefix + "AxxessBiller").prop("checked")) {
            if ($(prefix + "Payor").val() == "1") {
                $(prefix + "SubmitterId").val("SW23071");
                $(prefix + "SubmitterName").val("Axxess Healthcare Consult");
            } else if ($(prefix + "Payor").val() == "2") {
                $(prefix + "SubmitterId").val("CH0001841");
                $(prefix + "SubmitterName").val("Axxess Healthcare Consult");
            } else if ($(prefix + "Payor").val() == "3") {
                $(prefix + "SubmitterId").val("IA003133");
                $(prefix + "SubmitterName").val("Axxess Consult Inc");
            } else $(prefix + "SubmitterName").val("Axxess Healthcare Consult");
            $(prefix + "SubmitterPhone1").val("214");
            $(prefix + "SubmitterPhone2").val("575");
            $(prefix + "SubmitterPhone3").val("7711");
            $(prefix + "SubmitterFax1").val("214");
            $(prefix + "SubmitterFax2").val("575");
            $(prefix + "SubmitterFax3").val("7722");
        }
        $(prefix + "AxxessBiller").change(function() {
            if ($(this).prop("checked")) {
                if ($(prefix + "Payor").val() == "1") {
                    $(prefix + "SubmitterId").val("SW23071");
                    $(prefix + "SubmitterName").val("Axxess Healthcare Consult");
                } else if ($(prefix + "Payor").val() == "2") {
                    $(prefix + "SubmitterId").val("CH0001841");
                    $(prefix + "SubmitterName").val("Axxess Healthcare Consult");
                } else if ($(prefix + "Payor").val() == "3") {
                    $(prefix + "SubmitterId").val("IA003133");
                    $(prefix + "SubmitterName").val("Axxess Consult Inc");
                } else $(prefix + "SubmitterName").val("Axxess Healthcare Consult");
                $(prefix + "SubmitterPhone1").val("214");
                $(prefix + "SubmitterPhone2").val("575");
                $(prefix + "SubmitterPhone3").val("7711");
                $(prefix + "SubmitterFax1").val("214");
                $(prefix + "SubmitterFax2").val("575");
                $(prefix + "SubmitterFax3").val("7722");
            } else $(prefix + "SubmitterId," + prefix + "SubmitterName,[name=SubmitterPhoneArray],[name=SubmitterFaxArray]", e).val("");
        });
        U.ShowIfRadioEquals("IsAgreementSigned", "false", $(prefix + "IsAgreementSignedYesMore"));
        U.ShowIfRadioEquals("IsUserPlan", "true", $(prefix + "UserSubscriptionPlan"));
        U.ShowIfRadioEquals("IsUserPlan", "false", $(prefix + "PatientSubscriptionPlan"));

        U.InitShared(r, t, x, e);
    },
    ListRowDataBound: function(e) {
        $(this).data("tGrid").rowIndex = 0;
        var dataItem = e.dataItem,
            gridAction = $(".grid-action", e.row),
            menu = $(".action-menu", gridAction);
        if (menu) gridAction.data("menu", menu.detach());
        if (dataItem.AttentionLevel == "red")
            $(e.row).addClass("redrow");
        else if (dataItem.AttentionLevel == "blue")
            $(e.row).addClass("bluerow");
        else
            $(e.row).addClass("blackrow");
        if (dataItem.PopUpContent != undefined) {
            $(e.row).Tooltip({ top: 5, left: 5,
                bodyHandler: function() { return dataItem.PopUpContent; }
            })
        }
    },
    New: function() { Acore.Open("NewAgency") },
    ResendActivationLink: function(type, userId, agencyId) {
        if (type == "user") {
            Acore.Confirm({
                Message: "Are you sure you want to resend the activation link for this " + type + "?",
                Yes: function() {
                    U.PostUrl("Account/ResendLink", { userId: userId, agencyId: agencyId }, function(result) { U.GrowlResult(result) })
                }
            })
        } else {
            Acore.Confirm({
                Message: "Are you sure you want to resend the activation link for this " + type + "?",
                Yes: function() {
                    U.PostUrl("Account/PhysicianActivateLink", { physicianId: userId, agencyId: agencyId }, function(result) { U.GrowlResult(result) })
                }
            })
        }
    },
    Supplies: function(id) {
        Acore.Confirm({
            Message: "Before proceeding, verify that supplies have not been previously loaded for this agency.<br/>Are you sure you want to load supplies for this Agency?",
            Yes: function() { U.PostUrl("Agency/Supplies", { id: id }, function(result) { U.GrowlResult(result) }) }
        })
    },
    Suspend: function(id) {
        Acore.Confirm({
            Message: "Are you sure you want to suspend this agency?",
            Yes: function() { U.PostUrl("Agency/ToggleDelete", { id: id }, function() { Agency.RebindList() }) }
        })
    },
    Restore: function(id) {
        Acore.Confirm({
            Message: "Are you sure you want to restore this agency?",
            Yes: function() { U.PostUrl("Agency/ToggleDelete", { id: id }, function() { Agency.RebindList() }) }
        })
    },
    ReadOnly: function(id) {
        Acore.Modal({
            Name: "Read-Only Information",
            Url: "Agency/EditReadOnly",
            Input: { id: id },
            OnLoad: function() {
                $("#EditAgency_Form").Validate({ Success: function(result) {

                }
                });
            },
            Width: 450
        });
    },
    UndoReadOnly: function(id) {
        Acore.Confirm({
            Message: "Are you sure you want to unmark this agency as read-only?",
            Yes: function() { U.PostUrl("Agency/UndoReadOnly", { id: id }, function(result) { U.GrowlResult(result); Agency.RebindList() }) }
        })
    },
    Templates: function(id) {
        Acore.Confirm({
            Message: "Before proceeding, verify that templates have not been previously loaded for this agency.<br/>Are you sure you want to load templates for this Agency?",
            Yes: function() { U.PostUrl("Agency/Templates", { id: id }, function(result) { U.GrowlResult(result) }) }
        })
    },
    Logs: function(id) {
        Acore.Open("AgencyLogs", { id: id });
    },
    Change: {
        List: function() { Acore.Open("ListAgencyChanges", {}); },
        InitList: function(r, t, x, e) {
            Agency.Change.InitSearch(e);
            U.ShowIfSelectEquals($("#ListAgencyChange_Month"), "13", $("#ListAgencyChange_DateRange"));
            U.HideIfSelectEquals($("#ListAgencyChange_Month"), "13", $("#ListAgencyChange_Year"));
        },
        View: function(id, agencyId) { Acore.Open("ViewAgencyChange", { id: id, agencyId: agencyId }) },
        InitView: function(r, t, x, e) {
            Agency.Change.InitShared(r, t, x, e);
            $("#previousInformation div[name]").each(function() {
                var prev = $(this), current = $("#currentInformation div[name=" + prev.attr("name") + "]");
                if (prev.text() != current.text()) current.addClass("difference");
            });
        },
        InitShared: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitSearch: function(e) {
            $(".filter label", e).click(function() {
                $(this).next().focus()
            });
            $("#ListAgencyChange_IsCompleted").change(function() {
                Agency.Change.Rebind();
                Agency.Change.Filter($(".filter input", e));
            });
            $("#ListAgencyChange_TopFilter.filter input", e).focus(function() {
                $(this).removeClass("input-directions")
            }).blur(function() {
                if (!$(this).val())
                    $(this).addClass("input-directions")
            }).keyup(function() {
                Agency.Change.Filter($(this));
            });
        },
        Rebind: function() {
            var grid = $("#ListAgencyChange_Grid").data('tGrid');
            var start = $("#ListAgencyChange_StartDate").val();
            var end = $("#ListAgencyChange_EndDate").val();
            if ($("#ListAgencyChange_Month").val() != "13") {
                var year = $("#ListAgencyChange_Year").val();
                var month = $("#ListAgencyChange_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { isComplete: $("#ListAgencyChange_IsCompleted").val(), startDate: start, endDate: end };
            if (grid != null) grid.rebind(data);
        },
        ChangeExport: function() {
            var grid = $("#ListAgencyChange_Grid").data('tGrid');
            var start = $("#ListAgencyChange_StartDate").val();
            var end = $("#ListAgencyChange_EndDate").val();
            if ($("#ListAgencyChange_Month").val() != "13") {
                var year = $("#ListAgencyChange_Year").val();
                var month = $("#ListAgencyChange_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { isComplete: $("#ListAgencyChange_IsCompleted").val(), startDate: start, endDate: end };
            U.GetAttachment('Change/Export', data);
        },
        GetRowIndex: function() {
            return ++$('#ListAgencyChange_Grid').data('tGrid').rowIndex;
        },
        OnLoad: function(e) {
            var grid = $(this).data("tGrid");
            grid.rowIndex = 0;
        },
        OnDataBound: function(e) {
            $(this).data("tGrid").rowIndex = 0;
        },
        Filter: function(e) {
            var searchString = e.val();
            var grid = $("#ListAgencyChange_Grid .t-grid-content");
            if (grid != undefined) {
                if (searchString)
                    $("tr", grid).each(function() {
                        $(this).removeClass("match").hide();
                        $("td", this).each(function() {
                            if ($(this).text().toLowerCase().indexOf(searchString.toLowerCase()) > -1)
                                $(this).closest("tr").addClass("match");
                        })
                    });
                else
                    $("tr", grid).addClass("match");
                $(".match", grid).removeClass("t-alt").show().filter(":even").addClass("t-alt");
            }
        }
    },
    Upgrade: {
        List: function() { Acore.Open("ListAgencyUpgrades", {}); },
        InitList: function(r, t, x, e) {
            Agency.Upgrade.InitSearch(e);
            U.ShowIfSelectEquals($("#ListAgencyUpgrade_Month"), "13", $("#ListAgencyUpgrade_DateRange"));
            U.HideIfSelectEquals($("#ListAgencyUpgrade_Month"), "13", $("#ListAgencyUpgrade_Year"));
        },
        Edit: function(id, agencyId) { Acore.Open("EditAgencyUpgrade", { id: id, agencyId: agencyId }) },
        View: function(id, agencyId) { Acore.Open("ViewAgencyUpgrade", { id: id, agencyId: agencyId }) },
        InitEdit: function(r, t, x, e) {
            Agency.Upgrade.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitSearch: function(e) {
            $(".filter label", e).click(function() {
                $(this).next().focus()
            });
            $("#ListAgencyUpgrade_TopFilter.filter input", e).focus(function() {
                $(this).removeClass("input-directions")
            }).blur(function() {
                if (!$(this).val())
                    $(this).addClass("input-directions")
            }).keyup(function() {
                Agency.Upgrade.Filter($(this));
            });
            $("#ListAgencyUpgrade_IsCompleted").change(function() {
                Agency.Upgrade.Rebind();
                Agency.Upgrade.Filter($(".filter input", e));
            });
        },
        Rebind: function() {
            var grid = $("#ListAgencyUpgrade_Grid").data('tGrid');
            var start = $("#ListAgencyUpgrade_StartDate").val();
            var end = $("#ListAgencyUpgrade_EndDate").val();
            if ($("#ListAgencyUpgrade_Month").val() != "13") {
                var year = $("#ListAgencyUpgrade_Year").val();
                var month = $("#ListAgencyUpgrade_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { isComplete: $("#ListAgencyUpgrade_IsCompleted").val(), startDate: start, endDate: end };
            if (grid != null) grid.rebind(data);
        },
        UpgradeExport: function() {
            var grid = $("#ListAgencyUpgrade_Grid").data('tGrid');
            var start = $("#ListAgencyUpgrade_StartDate").val();
            var end = $("#ListAgencyUpgrade_EndDate").val();
            if ($("#ListAgencyUpgrade_Month").val() != "13") {
                var year = $("#ListAgencyUpgrade_Year").val();
                var month = $("#ListAgencyUpgrade_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { IsCompleted: $("#ListAgencyUpgrade_IsCompleted").val(), startDate: start, endDate: end };
            U.GetAttachment('Upgrade/Export', data);
        },
        GetRowIndex: function() {
            return ++$('#ListAgencyUpgrade_Grid').data('tGrid').rowIndex;
        },
        OnLoad: function(e) {
            var grid = $(this).data("tGrid");
            grid.rowIndex = 0;
        },
        OnDataBound: function(e) {
            $(this).data("tGrid").rowIndex = 0;
        },
        Filter: function(e) {
            var searchString = e.val();
            var grid = $("#ListAgencyUpgrade_Grid .t-grid-content");
            if (grid != undefined) {
                if (searchString)
                    $("tr", grid).each(function() {
                        $(this).removeClass("match").hide();
                        $("td", this).each(function() {
                            if ($(this).text().toLowerCase().indexOf(searchString.toLowerCase()) > -1)
                                $(this).closest("tr").addClass("match");
                        })
                    });
                else
                    $("tr", grid).addClass("match");
                $(".match", grid).removeClass("t-alt").show().filter(":even").addClass("t-alt");
            }
        }
    },
    AnnualPlan: {
        List: function() { Acore.Open("ListAgencyAnnualPlan", {}); },
        InitList: function(r, t, x, e) {
            Agency.AnnualPlan.InitSearch(e);
            U.ShowIfSelectEquals($("#ListAgencyAnnualPlan_Month"), "13", $("#ListAgencyAnnualPlan_DateRange"));
            U.HideIfSelectEquals($("#ListAgencyAnnualPlan_Month"), "13", $("#ListAgencyAnnualPlan_Year"));
        },
        Edit: function(id, agencyId) { Acore.Open("EditAgencyAnnualPlan", { id: id, agencyId: agencyId }) },
        View: function(id, agencyId) { Acore.Open("ViewAgencyAnnualPlan", { id: id, agencyId: agencyId }) },
        InitEdit: function(r, t, x, e) {
            Agency.AnnualPlan.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitSearch: function(e) {
            $(".filter label", e).click(function() {
                $(this).next().focus()
            });
            $("#ListAgencyAnnualPlan_TopFilter.filter input", e).focus(function() {
                $(this).removeClass("input-directions")
            }).blur(function() {
                if (!$(this).val())
                    $(this).addClass("input-directions")
            }).keyup(function() {
                Agency.AnnualPlan.Filter($(this));
            });
            $("#ListAgencyAnnualPlan_IsCompleted").change(function() {
                Agency.AnnualPlan.Rebind();
                Agency.AnnualPlan.Filter($(".filter input", e));
            });
        },
        Rebind: function() {
            var grid = $("#ListAgencyAnnualPlan_Grid").data('tGrid');
            var start = $("#ListAgencyAnnualPlan_StartDate").val();
            var end = $("#ListAgencyAnnualPlan_EndDate").val();
            if ($("#ListAgencyAnnualPlan_Month").val() != "13") {
                var year = $("#ListAgencyAnnualPlan_Year").val();
                var month = $("#ListAgencyAnnualPlan_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { isComplete: $("#ListAgencyAnnualPlan_IsCompleted").val(), startDate: start, endDate: end };
            if (grid != null) grid.rebind(data);
        },
        AnnualPlanExport: function() {
            var grid = $("#ListAgencyAnnualPlan_Grid").data('tGrid');
            var start = $("#ListAgencyAnnualPlan_StartDate").val();
            var end = $("#ListAgencyAnnualPlan_EndDate").val();
            if ($("#ListAgencyAnnualPlan_Month").val() != "13") {
                var year = $("#ListAgencyAnnualPlan_Year").val();
                var month = $("#ListAgencyAnnualPlan_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { IsCompleted: $("#ListAgencyAnnualPlan_IsCompleted").val(), startDate: start, endDate: end };
            U.GetAttachment('AnnualPlan/Export', data);
        },
        GetRowIndex: function() {
            return ++$('#ListAgencyAnnualPlan_Grid').data('tGrid').rowIndex;
        },
        OnLoad: function(e) {
            var grid = $(this).data("tGrid");
            grid.rowIndex = 0;
        },
        OnDataBound: function(e) {
            $(this).data("tGrid").rowIndex = 0;
        },
        Filter: function(e) {
            var searchString = e.val();
            var grid = $("#ListAgencyAnnualPlan_Grid .t-grid-content");
            if (grid != undefined) {
                if (searchString)
                    $("tr", grid).each(function() {
                        $(this).removeClass("match").hide();
                        $("td", this).each(function() {
                            if ($(this).text().toLowerCase().indexOf(searchString.toLowerCase()) > -1)
                                $(this).closest("tr").addClass("match");
                        })
                    });
                else
                    $("tr", grid).addClass("match");
                $(".match", grid).removeClass("t-alt").show().filter(":even").addClass("t-alt");
            }
        }
    },
    MarkAsReadOnly: {
        List: function() { Acore.Open("ListMarkAsReadOnly", {}); },
        InitList: function(r, t, x, e) {
            Agency.MarkAsReadOnly.InitSearch(e);
            U.ShowIfSelectEquals($("#ListMarkAsReadOnly_Month"), "13", $("#ListMarkAsReadOnly_DateRange"));
            U.HideIfSelectEquals($("#ListMarkAsReadOnly_Month"), "13", $("#ListMarkAsReadOnly_Year"));
        },
        InitShared: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitSearch: function(e) {
            $(".filter label", e).click(function() {
                $(this).next().focus()
            });
            $("#ListMarkAsReadOnly_TopFilter.filter input", e).focus(function() {
                $(this).removeClass("input-directions")
            }).blur(function() {
                if (!$(this).val())
                    $(this).addClass("input-directions")
            }).keyup(function() {
                Agency.Upgrade.Filter($(this));
            });
            $("#ListMarkAsReadOnly_IsCompleted").change(function() {
                Agency.Upgrade.Rebind();
                Agency.Upgrade.Filter($(".filter input", e));
            });
        },
        Rebind: function() {
            var grid = $("#ListMarkAsReadOnly_Grid").data('tGrid');
            var start = $("#ListMarkAsReadOnly_StartDate").val();
            var end = $("#ListMarkAsReadOnly_EndDate").val();
            if ($("#ListMarkAsReadOnly_Month").val() != "13") {
                var year = $("#ListMarkAsReadOnly_Year").val();
                var month = $("#ListMarkAsReadOnly_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { startDate: start, endDate: end };
            if (grid != null) grid.rebind(data);
        },
        MarkAsReadOnlyExport: function() {
            var grid = $("#ListMarkAsReadOnly_Grid").data('tGrid');
            var start = $("#ListMarkAsReadOnly_StartDate").val();
            var end = $("#ListMarkAsReadOnly_EndDate").val();
            if ($("#ListMarkAsReadOnly_Month").val() != "13") {
                var year = $("#ListMarkAsReadOnly_Year").val();
                var month = $("#ListMarkAsReadOnly_Month").val();
                start = new Date(year, month - 1, 1).toJSON();
                end = new Date(year, month, 0).toJSON();
            }
            var data = { startDate: start, endDate: end };
            U.GetAttachment('MarkAsReadOnly/Export', data);
        },
        GetRowIndex: function() {
            return ++$('#ListMarkAsReadOnly_Grid').data('tGrid').rowIndex;
        },
        OnLoad: function(e) {
            var grid = $(this).data("tGrid");
            grid.rowIndex = 0;
        },
        OnDataBound: function(e) {
            $(this).data("tGrid").rowIndex = 0;
        },
        Filter: function(e) {
            var searchString = e.val();
            var grid = $("#ListMarkAsReadOnly_Grid .t-grid-content");
            if (grid != undefined) {
                if (searchString)
                    $("tr", grid).each(function() {
                        $(this).removeClass("match").hide();
                        $("td", this).each(function() {
                            if ($(this).text().toLowerCase().indexOf(searchString.toLowerCase()) > -1)
                                $(this).closest("tr").addClass("match");
                        })
                    });
                else
                    $("tr", grid).addClass("match");
                $(".match", grid).removeClass("t-alt").show().filter(":even").addClass("t-alt");
            }
        }
    },
    ImplementationSpecialist: {
        List: function() { Acore.Open("ListImplementationSpecialist", {}); },
        InitList: function(r, t, x, e) {
            Agency.ImplementationSpecialist.InitSearch(e);
        },
        NewNote: function(agencyId) { Agency.Note.New(agencyId); },
        Edit: function(agencyId) { Acore.Modal({ Name: "Edit Implementation Specialist", Id: "EditImplementationSpecialist", Url: "Agency/ImplementationSpecialist/Edit", Input: { agencyId: agencyId }, OnLoad: Agency.ImplementationSpecialist.InitEdit, Centered: true, Width: 700 }) },
        InitEdit: function(r, t, x, e) {
            Agency.ImplementationSpecialist.InitShared(r, t, x, e);
        },
        InitShared: function(r, t, x, e) {
            U.InitShared(r, t, x, e);
        },
        InitSearch: function(e) {
            $(".filter label", e).click(function() {
                $(this).next().focus()
            });
            $("#ListImplementationSpecialist_TopFilter.filter input", e).focus(function() {
                $(this).removeClass("input-directions")
            }).blur(function() {
                if (!$(this).val())
                    $(this).addClass("input-directions")
            }).keyup(function() {
                Agency.ImplementationSpecialist.Filter($(this));
            });
        },
        Rebind: function() {
            var grid = $("#ListImplementationSpecialist_Grid").data('tGrid');
            var data = { implementationSpecialistId: $("#ListImplementationSpecialist_ImplementationSpecialist").val(), startDate: $("#ListImplementationSpecialist_StartDate").val(), endDate: $("#ListImplementationSpecialist_EndDate").val() };
            if (grid != null) grid.rebind(data);
        },
        Filter: function(e) {
            var searchString = e.val();
            var grid = $("#ListImplementationSpecialist_Grid .t-grid-content");
            if (grid != undefined) {
                if (searchString)
                    $("tr", grid).each(function() {
                        $(this).removeClass("match").hide();
                        $("td", this).each(function() {
                            if ($(this).text().toLowerCase().indexOf(searchString.toLowerCase()) > -1)
                                $(this).closest("tr").addClass("match");
                        })
                    });
                else
                    $("tr", grid).addClass("match");
                $(".match", grid).removeClass("t-alt").show().filter(":even").addClass("t-alt");
            }
        }

    }
}