﻿var Linker = {
    CustomerNoteACId: "ViewNote",
    Link: function(window, id){
      if(window.indexOf("customerNote")!= -1){
         Linker.ViewCustomerNote(id); 
      }  
    },
    ViewCustomerNote: function(noteId){
        Acore.Open(Linker.CustomerNoteACId,{id: noteId, agencyId: U.GuidEmpty});
    }
}
