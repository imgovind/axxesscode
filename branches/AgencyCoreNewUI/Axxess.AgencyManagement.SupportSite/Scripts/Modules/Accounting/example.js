// Run the script on DOM ready:
$(function() {

window.App = {
	
		init: function() {
		    this.loading = $('#loading');
			//$('table').visualize({type: 'pie', height: '300px', width: '420px'});
            $('table').visualize({ type: 'bar', width: '420px' });
            //$('table').visualize({type: 'area', width: '420px'});
            $('table').visualize({ type: 'line', width: '420px' });

			this.request();
		},
		
		request: function() {
			this.loading.show();
			$.ajax({
				url: '/Accounting/EndingMRR',
				type: 'POST',
				dataType: 'json',
				success: $.proxy(function(data) {
					this.loading.hide();
					this.data = data;
				}, this)
			});	
		},
	};
	App.init()

});