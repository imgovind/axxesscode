$(function() {

    window.App = {

        page: 'yearly',

        init: function() {

            this.loading = $('#loading');

            // Compile Templates
            Handlebars.partials = Handlebars.templates = {};
            $('script[type="text/handlebars"]').each(function(i, el) {
                var el = $(el);
                Handlebars.templates[el.attr('id')] = Handlebars.compile(el.html())
            });

            Handlebars.registerHelper('moment', function(date, options) {
                return moment(date).format(options.hash.format || 'YYYY-MM-DD')
            });

            Handlebars.registerHelper('SourceClass', function(source) {
                switch (source) {
                    case 'Credit':
                        return 'text-info';
                    case 'Bank':
                        return 'text-success';
                    default:
                    case 'Unknown':
                        return 'text-error';
                }
            });
            Handlebars.registerHelper('MonthClass', function() {
                switch (this.Name) {
                    case 'Gross Lost Customers':
                        return 'text-blue';
                        
                    case 'Churned MRR':
                    case 'Downgrade MRR':
                    case 'Churned ARR':
                    case 'Downgrade ARR':
                        return 'text-green';
                        
                    case 'Monthly $ Churn%':
                    case 'Annualized $ Churn%':
                    case 'Monthly # Churn%':
                    case 'Annualized # Churn%':
                        return 'text-red';
                        
                    case 'Beginning Customers':
                    case 'Gross New Customers':
                    case 'Net New Customers':
                    case 'Ending Customers':
                    case 'Deal Closed Customers':
                        return 'text-bluetitle';
                        
                    case 'Beginning MRR':
                    case 'NEW MRR':
                    case 'Expansion MRR':
                    case 'Net MRR':
                    case 'Ending MRR':
                    case 'Avg. MRR':
                    case 'Beginning ARR':
                    case 'NEW ARR':
                    case 'Expansion ARR':
                    case 'Net ARR':
                    case 'Ending ARR':
                    case 'Avg. ARR':
                        return 'text-greentitle';
                        
                    default:
                        return 'text-redtitle';
                }
            });

            // Modal
            var $body = $('body')
				.on('shown', '.modal', function() { $body.addClass('modal-open') })
				.on('hidden', '.modal', function() { $body.removeClass('modal-open') })
				.tooltip({ selector: '[rel=tooltip]' });

            $('.modal-body').on('scroll', function() {
                $('.datepicker input').datepicker('place')
            });

            this.bind();
            // Dashboard should be called first!
            this.requestReport(this.page, { date: moment().year() }); // with callback
        },

        bind: function() {

            var $doc = $(document)

            $(window).add($doc).resize(this.stickyHeaderFooter);

            // Nav (Weekly, Monthly, Yearly, Range)
            $doc.on('click', '#navbar .nav:first a', $.proxy(function(e) {
                this.page = $(e.target).data('view');
                var date = moment(),
                    format = 'YYYY-MM-DD',
                    monthFormat = 'M',
                    yearFormat = 'YYYY',
                    data = {};

                switch (this.page) {
                    case 'metrics':
                        this.requestMetrics();
                        break;
                    case 'dashboard':
                        $('#content')
				            .html(Handlebars.templates['dashboard']())
				            .find('.chart').each($.proxy(function(i, el) {
				                // Render every chart
				                el = $(el);
				                this.requestChart(el.data('chart'), el)
				            }, this));
                        break;
                    case 'weekly':
                        this.requestReport(this.page, {
                            date: date.startOf('week').format(format)
                        });
                        break;
                    case 'monthly':
                        this.requestReport(this.page, {
                            date: date.startOf('month').format(format)
                        });
                        break;
                    default:
                    case 'yearly':
                        this.requestReport(this.page, { date: date.year() });
                        break;
                    case 'range':
                        this.requestReport(this.page, {
                            startMonth: moment(date).startOf('year').format(monthFormat),
                            startYear: moment(date).startOf('year').format(yearFormat),
                            endMonth: moment(date).endOf('year').format(monthFormat),
                            endYear: moment(date).endOf('year').format(yearFormat)
                        });
                        break;
                }
            }, this));

            $doc.on('click', '#navbar .prev, #navbar .next', $.proxy(function(e) {
                var date, data, format = 'YYYY-MM-DD';

                switch ($(e.currentTarget).attr('class')) {
                    case 'prev':
                        date = this.data.PrevViewDate;
                        break;
                    case 'next':
                        date = this.data.NextViewDate;
                        break;
                }

                switch (this.page) {
                    case 'weekly':
                        date = moment(date, 'DD, MMM, YYYY');
                        data = { date: date.startOf('week').format(format) };
                        break;
                    case 'monthly':
                        date = moment(date, 'MMM, YYYY');
                        data = { date: date.startOf('month').format(format) };
                        break;
                    case 'yearly':
                    default:
                        data = { date: date };
                        break;
                }

                this.requestReport(this.page, data);
            }, this))


            // Update Month Range and Send Request
            $doc.on("click touchstart", "#update", $.proxy(function(e) {

                var start = moment($('#accounting_date_start').val(), 'MMM, YYYY'),
                    end = moment($('#accounting_date_end').val(), 'MMM YYYY');

                if (start < end) {
                    switch (this.page) {
                        case 'metrics':
                            this.requestMetrics({
                                startMonth: start.month() + 1,
                                startYear: start.year(),
                                endMonth: end.month() + 1,
                                endYear: end.year()
                            });
                            break;
                        default:
                            // TODO are we updating Report range or Metrics range?
                            this.requestReport(this.page, {
                                startMonth: start.month() + 1,
                                startYear: start.year(),
                                endMonth: end.month() + 1,
                                endYear: end.year()
                            });
                    }
                } else {
                    alert('The start date should be before the end date!');
                }
            }, this));

            // Load Modal
            $doc.on("click", ".update-account-modal", function(e) {

                $.ajax({
                    url: '/Accounting/Information',
                    dataType: 'json',
                    type: 'POST',
                    data: { accountId: $(this).attr('id') },
                    success: function(data) {
                        // Change all dates here
                        var tmpl = Handlebars.templates['AccountInformation'],
			                modal = $('#modalLeft .modal-body').html(tmpl(data));
                        modal.find(".datepicker input").datepicker({ format: 'yyyy-mm-dd' });

                        // Now that the form has been rendered, we need to go back 
                        // and select the options.
                        var keys = [ // Index 0 is element id, 1 is data attribute
			                ['StateFilter', 'State'],
			                ['status', 'Status'],
			                ['paymentsource', 'PaymentSource'],
			                ['startup', 'StartUp']
			            ];
                        for (var i = 0; i < keys.length; i++) {
                            modal.find('#' + keys[i][0]).val(data[keys[i][1]]);
                        }

                        for (var i = 0; i < data.Payments.length; i++) {
                            var payment = data.Payments[i],
			                    id = payment.Id;
                            modal.find('tr[data-id="' + id + '"] select.package').val(payment.Package);
                        }
                    }
                })
            });

            // Add Payment
            $doc.on("click", ".addPayment", function(e) {
                $('#addPayment').addClass('hide');
                $('#addPaymentForm').removeClass('hide');
            });

            var cleanup = function() {
                $('.modal').modal('hide');
                $('.datepicker.dropdown-menu').remove();
            }

            /* Close Update Account Information Sidebar */
            $doc.on("click", ".cancel-account", function(e) {
                cleanup();
            });

            /* Update Account Information Sidebar Submit */
            $doc.on("click", '.update-account', function(e) {
                $form = $('form');
                $.ajax({
                    type: "POST",
                    url: "/accounting/Update",
                    data: $form.serialize(),
                    success: function(data) {
                        /* Consider error catching if update doesn't work */
                        cleanup();
                        var id = $('#accountId').val(),
			                row = $('.payments tr[data-id="' + id + '"]'),
			                duration = 500;
                        row.addClass('success').fadeOut(duration).fadeIn(duration).fadeOut(duration).fadeIn(duration).queue(function() { row.removeClass('success') });
                    }
                });
                // refresh view
            });

            //export accounts infoormation.
            $doc.on("click", '.export', $.proxy(function(e) {
                if (this.page == 'metrics') {
                    window.parent.U.GetAttachment('/accounting/ExportMetrics', { json: JSON.stringify(window.App.data) });
                }
                else {
                    window.parent.U.GetAttachment('/accounting/Export', { json: JSON.stringify(window.App.data) });
                }

            }, this));

            return this;
        },

        requestMetrics: function(data) {
            this.loading.show();
            $.ajax({
                url: '/Accounting/Metrics',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: $.proxy(function(data) {
                    this.loading.hide();
                    this.data = data;
                    this.updateNavbar();
                    $('#content').html(Handlebars.templates['metrics'](data))
                    this.stickyHeaderFooter();
                }, this)
            });

        },

        requestChart: function(chartType, el, data) {
            $.ajax({
                url: '/Accounting/' + chartType,
                type: 'POST',
                dataType: 'json',
                data: data || {},
                success: $.proxy(function(data) {

                    if (el.is(':empty')) {

                        el.html($(Handlebars.templates['chart'](data)));

                        // Datepicker
                        el.find('.date-picker').datepicker({
                            format: 'M yyyy',
                            viewMode: 1,
                            minViewMode: 1
                        })

                        // Click on Chart Type button group
                        el.on('click', '.chart-type .btn', $.proxy(function(event) {
                            var btn = $(event.target),
					            chart = btn.closest('.chart');
                            btn.addClass('active').siblings('.active').removeClass('active');
                            this.renderChart(
					            chart.find('.graph')[0],
					            chart.data('data'),
					            btn.data('chart-type')
					        );
                        }, this))

                        el.on('click', '.update', $.proxy(function(event) {
                            var btn = $(event.target),
					            chart = btn.closest('.chart');
                            this.requestChart(chart.data('chart'), chart, {
                                StartDate: moment(chart.find('[name="StartDate"]').val(), 'MMM YYYY').format('YYYY-MM-DD'),
                                EndDate: moment(chart.find('[name="EndDate"]').val(), 'MMM YYYY').format('YYYY-MM-DD')
                            });
                            return false;
                        }, this))

                    } else {
                        el.find('.title').text(data.Caption)
                        el.find('[name="StartDate"]').val(moment(data.StartDate).format('MMM YYYY'));
                        el.find('[name="EndDate"]').val(moment(data.EndDate).format('MMM YYYY'));
                    }

                    el.data({
                        data: data,
                        graph: this.renderChart(
					        el.find('.graph')[0],
					        data,
					        el.find('.chart-type .active').data('chart-type')
					    )
                    });

                }, this)
            });
        },

        renderChart: function(el, data, chartType) {
            return new Highcharts.Chart({
                chart: {
                    renderTo: el,
                    type: chartType || 'line'
                },
                title: { text: null /* data.Caption */ },
                xAxis: {
                    categories: data.Column
                },
                yAxis: {
                    title: {
                        text: 'Amount'
                    },
                    labels: {
                        formatter: function() {
                            return '$' + Highcharts.numberFormat(this.value, 2);
                        }
                    }
                },
                tooltip: {
                    formatter: function() {
                        return "<b>" + this.series.name + "</b><br/>" + this.x + ": " + "$" + Highcharts.numberFormat(this.y, 2);
                    }
                },
                series: data.Rows,
                credits: { enabled: false }
            });

            // Set Chart Type ?
        },

        // Request Different Report Pages
        requestReport: function(view, data) {
            this.loading.show();
            $.ajax({
                url: '/Accounting/' + view,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: $.proxy(function(data) {
                    this.loading.hide();
                    this.data = data;
                    this.updateNavbar();
                    $('#content').html(Handlebars.templates['content'](data));
                    this.stickyHeaderFooter();
                }, this)
            });
        },

        updateNavbar: function(data) {

            var tmpl = Handlebars.templates['navbar'],
				scope = {};
            if (this.page == 'weekly' || this.page == 'monthly' || this.page == 'yearly') {
                scope = {
                    view: this.page,
                    title: this.data.CurrentViewDate,
                    prev: this.data.PrevViewDate,
                    next: this.data.NextViewDate
                };

            }
            else if (this.page == 'range') {
                scope = {
                    view: this.page,
                    start: this.data.PrevViewDate,
                    end: this.data.NextViewDate
                };

            }
            else if (this.page == 'metrics') {
                scope = {
                    view: this.page,
                    start: this.data.PrevViewDate,
                    end: this.data.NextViewDate
                };

            }

            $('#navbar').html(tmpl(scope));

            if (this.page === 'range' || this.page === 'metrics') {
                $("#navbar .datepicker input")
			        .datepicker({ format: 'M yyyy', viewMode: 1, minViewMode: 1 })
            }
            return this;
        },

        stickyHeaderFooter: function() {

            var cloneAndSetWidth = function($el, $fixed) {
                var children = $el.children(),
					clone = $el.clone();
                clone.children().width(function(i, val) {
                    return children.eq(i).width();
                });
                $fixed
					.html(clone)
			    	.css({
			    	    'position': 'fixed',
			    	    'width': $el.width()
			    	});
            }

            var headerFixed = $('#header-fixed'),
			    footerFixed = $("#footer-fixed");
            cloneAndSetWidth($('#header'), headerFixed);
            cloneAndSetWidth($('#footer'), footerFixed);
            footerFixed.hide();

            var $window = $(window)
		        .unbind('scroll')
		    	.bind('scroll', function() {
		    	    var offset = $window.scrollTop();

		    	    /* Above fix for monthly view sticky fix */
		    	    headerFixed.css({ top: '40px', left: -$window.scrollLeft() });
		    	    footerFixed.css({ bottom: 0, left: -$window.scrollLeft(), margin: 0 });

		    	    // Show and Hide Header
		    	    if (offset >= 20 && headerFixed.is(":hidden")) {
		    	        headerFixed.show();
		    	    } else if (offset < 20) {
		    	        headerFixed.hide();
		    	    }

		    	    // Show and Hide Footer
		    	    var buffer = $(document.body).height() - $window.height() - 40;
		    	    if (offset <= buffer && footerFixed.is(":hidden")) {
		    	        footerFixed.show();
		    	    } else if (offset > buffer) {
		    	        footerFixed.hide();
		    	    }

		    	});
        }
    };
    App.init()
});