$.extend(Agency, {
    ESign: {
        APIUrl: "https://esign.axxessweb.com/api/newAgency/",
        DevAPIUrl: "http://new.esign.axxessweb.com/api/newAgency/",
        InitList: function() {
            //Agency.ESign.APIUrl = Agency.ESign.DevAPIUrl; 
            $.ajax({
                type: "POST",
                url: "/Agency/ESign",
                cache: true
            }).done(function(data){
                Agency.ESign.GetList(data);
            });
        },
        AddOptions: function(jsonResult) {
            if(jsonResult == undefined || jsonResult == "" || jsonResult =="null")
            {
                var emptyOption = $("<option></option>").attr("value", "0").text("No agencies available");
                $("#ESign_Agencies").empty().append(emptyOption);
                return;
            }
            var data = $.parseJSON(jsonResult);
            var id = data[0].Id;
            for(datum in data){
                var id = data[datum].Id;
                var name = data[datum].Agency;
                $("#ESign_Agencies").append("<option value="+id+">"+name+"</option");
            }
        },
        GetList: function(id) {
            var url = Agency.ESign.APIUrl + id +"/";
            if($.browser.msie && window.XDomainRequest){
                var xdr = new XDomainRequest();
                xdr.open("get", url);
                xdr.onload = function(){
                    Agency.ESign.AddOptions(xdr.responseText);
                };
                xdr.send();
            } else {
                $.ajax({
                    type: "GET",
                    url:  url,
                    cache: true
                }).done(function(data){
                    Agency.ESign.AddOptions(data);                        
                });
            }
        },
        GetInformation: function(id){
            var  url;
            $.ajax({
                type: "POST",
                url: "/Agency/ESign",
                cache: true
            }).done(function(data){
                url = Agency.ESign.APIUrl+data+"/"+id;
                if($.browser.msie && window.XDomainRequest){
                    var xdr = new XDomainRequest();
                    xdr.open("get",url);
                    xdr.onload = function(){
                        Agency.ESign.AutoFillAgency(xdr.responseText);
                };
                xdr.send();
                }else{
                   $.ajax({
                       type: "GET",
                       url: url,
                       cache: true
                   }).done(function(agency){
                       Agency.ESign.AutoFillAgency(agency);
                   })
                }
            });
        },
        AutoFillAgency: function(agency){
            console.log(agency);
            var agency = $.parseJSON(agency);
            $.each(agency, function(key, val){
                $("#"+key).val(val);
            });
            if(agency.NewAgency_CompanyName==undefined){
                $("#NewAgency_IsAgreementSignedYes").removeAttr("checked");
            } else {
                $("#NewAgency_IsAgreementSignedYes").attr("checked", "checked")
            }
        }   
    }
});
