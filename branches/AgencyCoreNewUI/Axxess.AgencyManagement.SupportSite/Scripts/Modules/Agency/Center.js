﻿$.extend(Agency, {
    Center: {
        AcoreId: "AgenciesCenter",
        CookieId: "AgenciesCenter",
        AgencyId: "",
        Init: function(r, t, x, e) {
            $(".layout", e).layout({ west: { paneSelector: ".ui-layout-west", size: 200, minSize: 160, maxSize: 400} });
            $(".buttons .new-agency", e).click(function() {
                Agency.New();
                return false;
            });
            $("#AgencyCenter_TextSearch", e).keyup(function() {
                Agency.Center.AgencySelector.Filter(e)
            });
            $("#AgencyCenter_AttentionLevels", e).change(function() {
                Agency.Center.AgencySelector.AttentionFilter()
            });
            $("#AgencyCenter_StateFilter", e).change(function() {
                Agency.Center.AgencySelector.Rebind(e)
            });
            $("#AgencyCenter_DeletedStatus", e).change(function() {
                Agency.Center.AgencySelector.Rebind(e)
            });

            $("#AgencyCenter_ClusterId", e).change(function() {
                Agency.Center.AgencySelector.Rebind(e)
            });
        },
        InitContent: function(r, t, x, e) {
            $(".new-note", e).click(function() {
                Agency.Note.New(Agency.Center.AgencyId);
                return false
            });
            $(".agency-list", e).click(function() {
                Acore.Open("ListAgencies");
                return false;
            });
            Agency.Center.InitInfo(r, t, x, e);
            $("#AgencyCenter_UsersSearch", e).keyup(function() { Agency.Center.AgencyUsers.Filter(e) });
        },
        InitInfo: function(r, t, x, e) {
            $(".more-info", e).click(function() {
                Agency.Details(Agency.Center.AgencyId);
                return false;
            });
        },
        Load: function(agencyId) {
            if (Acore.Windows[Agency.Center.AcoreId].IsOpen) {
                Agency.Center.LoadContent(agencyId);
                $("#window_" + Agency.Center.AcoreId).WinFocus();
            } else {
                Agency.Center.AgencyId = agencyId;
                Agency.Center.SaveState(Agency.Center.AgencyId);
                Acore.Open(Agency.Center.AcoreId);
            }
        },
        LoadCenter: function(agencyId) {
            var e = $("#window_" + Agency.Center.AcoreId + "_content");
            U.RebindGrid($(".ui-layout-center .bottom .t-grid", e), {
                agencyId: agencyId
            });
            Agency.Center.AgencyId = agencyId;
        },
        LoadContent: function(agencyId) {
            var e = $("#window_" + Agency.Center.AcoreId + "_content");
            var row = $(".ui-layout-west .t-last:contains(" + agencyId + ")", e).parent();
            var scroll = row.position().top + row.closest(".t-grid-content").scrollTop() - 24;
            if (Acore.Animated)
                row.addClass("t-state-selected").closest(".t-grid-content").animate({ scrollTop: scroll }, Acore.AnimationSpeed);
            else
                row.addClass("t-state-selected").closest(".t-grid-content").scrollTop(scroll);
            $(".ui-layout-center", e).Load("Agency/Center/Content", { agencyId: agencyId }, Agency.Center.InitContent);
            Agency.Center.SaveState(agencyId);
        },
        LoadInfo: function(agencyId) {
            var e = $("#window_" + Agency.Center.AcoreId + "_content");
            $(".agency-summar", e).Load("Agency/Center/Info", { agencyId: agencyId }, Agency.Center.InitInfo);
        },
        AgencyUsers: {
            Filter: function(e) {
                var text = $("#AgencyCenter_UsersSearch", e).val();
                if (text && text.length) {
                    search = text.split(" ");
                    $(".t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    for (var i = 0; i < search.length; i++) $(".t-grid-content", e).find("td").each(function() {
                        if ($(this).text().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
                    });
                    $(".t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".t-grid-content .match", e).length) $(".t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Users", "There were no users found meeting your search parameters"));
                } else $(".t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            }
        },
        AgencySelector: {
            Init: function() {
                var PreviousState = U.Cookie.Get(Agency.Center.CookieId);
                if (PreviousState) Agency.Center.AgencyId = PreviousState.AgencyId;
                Agency.Center.AgencySelector.Filter($("#window_" + Agency.Center.AcoreId));
                var e = $("#window_" + Agency.Center.AcoreId + "_content");
                if (U.IsGuid(Agency.Center.AgencyId) && $(".ui-layout-west .t-last:contains(" + Agency.Center.AgencyId + ")", e).length)
                    Agency.Center.LoadContent(Agency.Center.AgencyId);
                else if ($(".ui-layout-west .t-last", e).length)
                    Agency.Center.LoadContent($(".ui-layout-west .t-last:first", e).text());
                else
                    $(".ui-layout-center", e).empty().html(U.MessageWarn("No Agencies", "No agencies found for your selection. Please reduce your filters."));
            },
            SelectorRowBound: function(e) {
                var dataItem = e.dataItem;
                if (dataItem.AttentionLevel == "red")
                    $(e.row).addClass("redrow");
                else if (dataItem.AttentionLevel == "blue")
                    $(e.row).addClass("bluerow");
                else
                    $(e.row).addClass("blackrow");
                if (dataItem.PopUpContent != undefined) {
                    $(e.row).Tooltip({ top: 5, left: 5, bodyHandler: function() { return dataItem.PopUpContent; } })
                }
            },
            AttentionFilter: function() {
                var e = $(".ui-layout-west");
                var all = 0, red = 1, blue = 2, black = 3;
                var selection = $("#AgencyCenter_AttentionLevels").val();
                if (selection == all) {
                    $(".t-grid-content", e).show();
                    $("#AgencyCenter_TextSearch", e).keyup();
                } else {

                    if ($("#AgencyCenter_TextSearch").val().length)
                        var match = ".match";
                    else
                        var match = "";
                    if (selection == red)
                        $(".t-grid-content .redrow" + match, e).show();
                    else
                        $(".t-grid-content .redrow" + match, e).hide();
                    if (selection == blue)
                        $(".t-grid-content .bluerow" + match, e).show();
                    else
                        $(".t-grid-content .bluerow" + match, e).hide();
                    if (selection == black)
                        $(".t-grid-content .blackrow" + match, e).show();
                    else
                        $(".t-grid-content .blackrow" + match, e).hide();
                }
                if ($("#AgencyCenter_TextSearch").val().length)
                    $(".t-grid-content", e).Zebra("tr.match:visible");
                else
                    $(".t-grid-content", e).Zebra("tr:visible");
            },
            Filter: function(e) {
                var text = $("#AgencyCenter_TextSearch", e).val();
                if (text && text.length) {
                    $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show();
                    $(".ui-layout-west .t-grid-content", e).find("td").not(".t-last").each(function() {
                        var blah = $(this).text().toLowerCase();
                        if (blah.indexOf(text.toLowerCase()) > -1)
                            $(this).parent().addClass("match");
                    });
                    $(".ui-layout-west .t-grid-content", e).find("tr").not(".match").hide();
                    if ($(".ui-layout-west .t-grid-content .match", e).length) $(".ui-layout-west .t-grid-content tbody", e).Zebra(".match").find(".match:first").click();
                    else $(".ui-layout-center", e).empty().html(U.MessageWarn("No Agencies", "There were no agencies found meeting your search parameters"));
                } else $(".ui-layout-west .t-grid-content", e).find("tr").removeClass("match t-alt").show().closest(".t-grid-content tbody").Zebra();
            },
            Rebind: function(e) {
                U.RebindGrid($(".ui-layout-west .t-grid", e), {
                    clusterId: $("#AgencyCenter_ClusterId", e).val(),
                    stateCode: $("#AgencyCenter_StateFilter", e).val(),
                    isDeleted: ($("#AgencyCenter_DeletedStatus", e).val() === "1")
                })
            },
            Select: function(e) {
                Agency.Center.LoadContent($(".t-last", e.row).text());
            }
        },
        Refresh: function() {
            var e = $("#window_" + Agency.Center.AcoreId + "_content");
            Agency.Center.LoadInfo(Agency.Center.AgencyId);
        },
        SaveState: function(agencyId) {
            Agency.Center.AgencyId = agencyId;
            U.Cookie.Set(Agency.Center.CookieId, { AgencyId: agencyId });
        },
        Suspend: function(id) {
            Acore.Confirm({
                Message: "Are you sure you want to suspend this agency?",
                Yes: function() { U.PostUrl("Agency/ToggleDelete", { id: id }, function() { Agency.Center.LoadContent(Agency.Center.AgencyId) }) }
            })
        },
        Restore: function(id) {
            Acore.Confirm({
                Message: "Are you sure you want to restore this agency?",
                Yes: function() { U.PostUrl("Agency/ToggleDelete", { id: id }, function() { Agency.Center.LoadContent(Agency.Center.AgencyId) }) }
            })
        },
        ReadOnly: function(id) {
            Acore.Modal({
                Name: "Read-Only Information",
                Url: "Agency/EditReadOnly",
                Input: { id: id },
                OnLoad: function() {
                    $("#EditAgency_Form").Validate({ Success: function(result) {
                        if (result.isSuccessful) Agency.Center.LoadContent(Agency.Center.AgencyId)
                    }
                    });
                },
                Width: 450
            });
        },
        UndoReadOnly: function(id) {
            Acore.Confirm({
                Message: "Are you sure you want to unmark this agency as read-only?",
                Yes: function() { U.PostUrl("Agency/UndoReadOnly", { id: id }, function(result) { U.GrowlResult(result); Agency.Center.LoadContent(Agency.Center.AgencyId) }) }
            })
        }
    }
});