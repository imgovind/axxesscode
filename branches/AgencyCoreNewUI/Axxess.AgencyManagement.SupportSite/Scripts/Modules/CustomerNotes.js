﻿var CustomerNotes = {
    AcoreId : "CustomerNotes",
    RefreshInterval : (60 * 1000),
    RefreshTimer : null,

    Refresh : function() {
        var e = $("#window_" + CustomerNotes.AcoreId + "_content");
        U.RebindGrid($("#CustomerNotes_RecentGrid", e));
        U.RebindGrid($("#CustomerNotes_UpdatedGrid", e));
    },
    Init : function() {
        var e = $("#window_" + CustomerNotes.AcoreId + "_content");
        CustomerNotes.InitSearch(e);
        $(".recent-tab", e).click(function() {
            CustomerNotes.TabClick($("#CustomerNotes_RecentGrid", e), e, this);
        });

        $(".updated-tab", e).click(function() {
            CustomerNotes.TabClick($("#CustomerNotes_UpdatedGrid", e), e, this);
        });

        $(".currentUser-tab", e).click(function() {
            CustomerNotes.TabClick($("#CustomerNotes_CurrentUserNotesGrid", e), e, this);
        });

        $("#CustomerNotes_SentOnly").click(function() {
            CustomerNotes.ApplyFilters();
        });

        $("#Filter_CustomerNoteType").change(function() {
            CustomerNotes.TypeFilter();
        });

        //RefreshTimer = setInterval(CustomerNotes.Refresh, CustomerNotes.RefreshInterval);
    },
    TabClick : function(grid, w, e) {
        $(".tab", w).removeClass("selected");
        $(e).addClass("selected");
        $(".t-grid", w).hide();
        $(grid, w).show();
        CustomerNotes.ApplyFilters();
    },
    InitSearch : function(e) {
        $(".filter label", e).click(function() {
            $(this).next().focus()
        });
        $(".filter input", e).focus(function() {
            $(this).removeClass("input-directions")
        }).blur(function() {
            if (!$(this).val())
                $(this).addClass("input-directions")
        }).keyup(function() {
            CustomerNotes.Filter($(this));
            CustomerNotes.TypeFilter();
        });
    },
    Filter : function(e) {
        var searchString = e.val();
        var grid;
        if ($(".recent-tab").hasClass("selected"))
            grid = $("#CustomerNotes_RecentGrid .t-grid-content");
        if ($(".updated-tab").hasClass("selected"))
            grid = $("#CustomerNotes_UpdatedGrid .t-grid-content");
        if ($(".currentUser-tab").hasClass("selected"))
            grid = $("#CustomerNotes_CurrentUserNotesGrid .t-grid-content");
        if (grid != undefined) {
            if (searchString)
                $("tr", grid).each(function() {
                    $(this).removeClass("match").hide();
                    $("td", this).each(function() {
                        if ($(this).text().toLowerCase().indexOf(searchString.toLowerCase()) > -1)
                            $(this).closest("tr").addClass("match");
                    })
                });
            else
                $("tr", grid).addClass("match");
            $(".match", grid).removeClass("t-alt").show().filter(":even").addClass("t-alt");
        }
    },
    RowBound : function(e) {
        var dataitem = e.dataItem;
        if (dataitem.IssueId != 0)
            $(e.row).addClass("sent");
    },
    ApplyFilters : function() {
        var e = $("#window_" + CustomerNotes.AcoreId + "_content");
        var search = $(".filter input", e);
        if (search.val()) {
            CustomerNotes.Filter(search);
        }
        CustomerNotes.TypeFilter();
    },
    SentOnly : function() {
        var e = $("#window_" + CustomerNotes.AcoreId + "_content");
        var grids = $(".t-grid-content", e);
        if ($("#CustomerNotes_SentOnly").is(":checked")) {
            if ($(".filter input", e).val()) {
                $(".match", grids).each(function() {
                    $(this).hide();
                    if ($(this).hasClass("sent"))
                        $(this).show();
                });
                $(".match.sent", grids).removeClass("t-alt").filter(":even").addClass("t-alt");
            } else {
                $("tr", grids).each(function() {
                    $(this).hide();
                    if ($(this).hasClass("sent"))
                        $(this).show();
                });
                $(".sent", grids).removeClass("t-alt").filter(":even").addClass("t-alt");
            }
        } else {
            if ($(".filter input", e).val()) {
                $(".match", grids).each(function() {
                    $(this).show();
                });
                $(".match", grids).removeClass("t-alt").filter(":even").addClass("t-alt");
            } else {
                $("tr", grids).each(function() {
                    $(this).show();
                });
                $("tr", grids).removeClass("t-alt").filter(":even").addClass("t-alt");
            }
        }
    },
    TypeFilter : function() {
        var e = $("#window_" + CustomerNotes.AcoreId + "_content");
        var fullGrids = $(".t-grid", e);
        var grids = $(".t-grid-content", e);
        var selected = $("#Filter_CustomerNoteType option:selected").text();
        if (selected != "-- All --") {
            $(fullGrids).each(function() {
                var col;
                $(".t-grid-header th", this).each(function() {
                    if ($(this).text() == "Note Type") {
                        col = $(this).parent().children().index($(this));
                    }
                });
                col++;
                $(".t-grid-content tr", this).each(function() {
                    $(this).removeClass("matchType").hide();
                    if ($.trim($("td:nth-child(" + col + ")", this).text().toLowerCase()) == $.trim(selected.toLocaleLowerCase())) {
                        $(this).addClass("matchType");
                    }
                });
            });
            if ($(".filter input", e).val()) {
                $(".match.matchType", grids).show().removeClass("t-alt").filter(":even").addClass("t-alt");
            } else {
                $(".matchType", grids).show().removeClass("t-alt").filter(":even").addClass("t-alt");
            }
        } else {
            if($(".filter input", e).val()){
                $("tr", grids).removeClass("matchType");
                $(".match", grids).show().removeClass("t-alt").filter(":even").addClass("t-alt");
            } else {
                $("tr", grids).removeClass("matchType").show().removeClass("t-alt").filter(":even").addClass("t-alt");  
            }
        }
    }
}

