﻿var DdeCredential = {
    Edit: function(id) { Acore.Open("EditDdeCredential", { id: id }) },
    InitEdit: function(r, t, x, e) {
        U.InitShared(r, t, x, e);
        $(".activity-log", e).click(function() {
            DdeCredential.Logs($("[name=Id]", e).val());
            return false;
        });  
    },
    InitList: function(r, t, x, e) {
        $(".new-ddecredential", e).click(function() {
            Login.New();
            return false;
        })
    },
    InitNew: function(r, t, x, e) { U.InitShared(r, t, x, e) },
    List: function() { Acore.Open("ListDdeCredentials") },
    New: function(agencyId) { Acore.Open("DdeCredential") },
    Logs: function(id) { Acore.Open("DdeCredentialLogs", { id: id }) }
}