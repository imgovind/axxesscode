﻿var Login = {
    Edit: function(id) { Acore.Open("EditLogin", { id: id }) },
    InitEdit: function(r, t, x, e) {
        $(".activity-log", e).click(function() {
            Login.Logs($("[name=Id]", e).val());
            return false;
        });
        Login.InitShared(r, t, x, e);
        U.InitShared(r, t, x, e);
    },
    InitNew: function(r, t, x, e) {
        Login.InitShared(r, t, x, e);
        U.InitShared(r, t, x, e);
    },
    InitList: function(r, t, x, e) {
        $(".new-login", e).click(function() {
            Login.New();
            return false;
        })
        $("#SelectActiveFilter", e).change(function() {
        var grid = $("#ListLogins_Grid").data('tGrid');
        var data = { status: $("#SelectActiveFilter").val() };
        if (grid != null) grid.rebind(data);
        });
    },
    InitShared: function(r, t, x, e) {
        var allPermissionCkbx = $(".allpermissions", e),
        mainAccordion = $("div.accordion-container", e);
        mainAccordion.Tree();
        U.SelectAll(allPermissionCkbx, $("input:checkbox", e));
        mainAccordion.TogglePanels();
        $(".accordion", mainAccordion).TogglePanels();
        //Allows the checkboxes within the accordion headers to be checked and open the accordion if it closed only if checking the checkbox
        $("h3 > .select-all", mainAccordion).click(function(evnt) {
            if ($(this).prop("checked") && !$(this).closest("h3").hasClass("ui-accordion-header-active")) $(this).parent().click();
            evnt.stopPropagation();
        });
        if (!$("[type=checkbox]", mainAccordion).not(":checked").length) {
            allPermissionCkbx.prop("checked", true);
            U.SelectAllLabelRename(allPermissionCkbx);
        }
        $("select[name=PermissionTemplate]", e).change(function() {
            mainAccordion.Tree("reset");
            var permissions = JSON.parse($(this).val());
            permissions.forEach(function(element) {
                $("input[name=Permissions\\[" + element.Key + "\\]\\.Value]").each(function() {
                    var checkBox = $(this);
                    if ($.inArray(+checkBox.val(), element.Value) > -1) checkBox.click();
                });
            });
        });
    },
    ToggleStatus: function(Id, IsActive) {
            Acore.Confirm({
                Message: "Are you sure you want to " + (IsActive ? "deactivate" : "activate") + " this user?",
                Yes: function() { U.GridAjaxCommand("Login/ToggleUser", { id: Id, isActive: IsActive }, $("#ListLogins_Grid")) }
           })
        },
    Permission: {
        InitList: function(r, t, x, e) {
            $(".new-template", e).click(function() {
                Login.NewTemplate();
                return false;
            });
        },
        InitNew: function(r, t, x, e) {
            Login.InitShared(r, t, x, e);
            U.InitShared(r, t, x, e);
        },
        InitEdit: function(r, t, x, e) {
            $(".activity-log", e).click(function() {
                Login.Logs($("[name=Id]", e).val());
                return false;
            });
            Login.InitShared(r, t, x, e);
            U.InitShared(r, t, x, e);
        },
        Edit: function(id) { Acore.Open("EditLoginsTemplate", { id: id }) },
        Delete: function(id, $grid) {
            Acore.Confirm({
                Message: "Are you sure you want to delete this permission template?",
                Yes: function() { U.GridAjaxCommand("Login/DeleteTemplate", { id: id }, $grid) }
           })
        }
    },
    List: function() { Acore.Open("ListLogins") },
    New: function(agencyId) { Acore.Open("NewLogin") },
    Logs: function(id) { Acore.Open("LoginLogs", { id: id }) },
    NewTemplate: function() { Acore.Open("NewLoginsTemplate") }
}