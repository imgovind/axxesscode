﻿var Message = {
    _ckeloaded: false,
    Dashboard: {
        InitNew: function(r, t, x, e) {
            if (!this._ckeloaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() {
                Message._ckeloaded = true;
                Message.InitCKEditor("NewDashboardMessage_Text");
            });
            $(".preview", e).click(function() {
                Message.Dashboard.Preview(CKEDITOR.instances["NewDashboardMessage_Text"].getData());
                return false;
            });
            $(".send").click(function() {
                if (CKEDITOR.instances["NewDashboardMessage_Text"].getData().length) {
                    $("[name=Text]", e).val(CKEDITOR.instances["NewDashboardMessage_Text"].getData());
                    $(this).closest("form").submit();
                } else U.Growl("Please Type in a Message", "error");
                return false;
            });
            $("form", e).Validate({ Success: function() { e.Close(); } });
        },
        Preview: function(content) {
            Acore.Modal({
                Name: "Dashboard Preview",
                Url: "Message/Dashboard/Preview",
                OnLoad: function(r, t, x, e) { $(".widget-content", e).html(content) },
                Width: 500
            })
        }
    },
    System: {
        InitNew: function(r, t, x, e) {
            if (!this._ckeloaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() {
                Message._ckeloaded = true;
                Message.InitCKEditor("NewSystemMessage_Body");
            });
            $(".send").click(function() {
                if ($("#NewSystemMessage_Subject").val().length == 0) {
                    U.Growl("Please type in a Subject.", "error");
                }
                else {
                    if (CKEDITOR.instances["NewSystemMessage_Body"].getData().length) {
                        $("[name=Body]", e).val(CKEDITOR.instances["NewSystemMessage_Body"].getData());
                        $(this).closest("form").submit();
                    } else U.Growl("Please type in a Message.", "error");
                }
                return false;
            });
            $("form", e).Validate({ Success: function() { e.Close(); } });
        }
    },
    InitCKEditor: function(type) {
        CKEDITOR.replace(type, {
            skin: "office2003",
            resize_enabled: false,
            height: "20em",
            removePlugins: "elementspath",
            toolbarCanCollapse: false,
            toolbar: [[
                "Source", "Preview", "-",
                "Bold", "Italic", "Underline", "-",
                "NumberedList", "BulletedList", "Outdent", "Indent", "-",
                "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-",
                "Link", "Unlink", "-",
                "Font", "FontSize", "TextColor", "-", "SpellChecker", "Scayt"
            ]]
        });
    }
}