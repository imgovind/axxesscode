﻿namespace Axxess.Scheduled.Eligibility.Data
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Model;
    using Axxess.AgencyManagement.Domain;

    public static class Database
    {
        private const string SELECT_PATIENT_BY_PRIMARY_INSURANCE = @"SELECT `Id`,`AgencyId`,`MedicareNumber`,`FirstName`,`LastName`,`MiddleInitial`,`DOB`,`Gender`, `LastEligibilityCheck` FROM `patients` WHERE (`PrimaryInsurance` = 1 || `PrimaryInsurance` = 2 || `PrimaryInsurance` = 3 || `PrimaryInsurance` = 4) AND `IsDeprecated` = 0 AND `Status` = 1;";
        private const string SELECT_PATIENT_EPISODE_CURRENT = @"SELECT patientepisodes.Id, patientepisodes.AgencyId, patientepisodes.PatientId, patientepisodes.EndDate, patientepisodes.StartDate FROM patientepisodes WHERE patientepisodes.AgencyId = '{0}' AND patientepisodes.PatientId = '{1}' AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND CURDATE() BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate LIMIT 0,1";//, patientepisodes.Schedule
        private const string INSERT_MEDICARE_ELIGIBILITY = @"INSERT INTO `medicareeligibilities` (`Id`,`AgencyId`,`PatientId`,`EpisodeId`,`Result`,`Status`,`Created`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', {5}, '{6}')";
        private const string UPDATE_PATIENT = @"Update `patients` SET `LastEligibilityCheck` = '{0}' WHERE `Id` = '{1}' AND `AgencyId` = '{2}';";
       // private const string UPDATE_PATIENT_EPISODE = @"Update `patientepisodes` SET `Schedule` = '{0}' WHERE `Id` = '{1}' AND `AgencyId` = '{2}' AND `PatientId` = '{3}';";
        private const string INSERT_SCHEDULEEVENTS = @"INSERT INTO 
                                        `scheduleevents` 
                                                    (
                                                        `AgencyId`,
                                                        `EventId`, 
                                                        `EpisodeId`, 
                                                        `PatientId`, 
                                                        `UserId`,
                                                        `UserName`, 
                                                        `Discipline`, 
                                                        `DisciplineTask`,
                                                        `Status`,
                                                        `EventDate`,
                                                        `VisitDate`,
                                                        `StartDate`, 
                                                        `EndDate`, 
                                                        `IsBillable`,
                                                        `Asset`, 
                                                        `Version`
                                                    )
                                                    VALUES
                                                    (
                                                        '{0}', 
                                                        '{1}',
                                                        '{2}',
                                                        '{3}',
                                                        '{4}',
                                                        '{5}', 
                                                        '{6}',
                                                        '{7}',
                                                        '{8}', 
                                                        '{9}', 
                                                        '{10}',
                                                        '{11}',
                                                        '{12}', 
                                                        '{13}', 
                                                        '<ArrayOfGuid />', 
                                                        1
                                                    );";
               

        public static Queue<PatientEligibilityData> GetPatients()
        {
            var queue = new Queue<PatientEligibilityData>();
            var patientList = new List<PatientEligibilityData>();

            using (var cmd = new FluentCommand<PatientEligibilityData>(SELECT_PATIENT_BY_PRIMARY_INSURANCE))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                patientList = cmd.SetMap(reader => new PatientEligibilityData
                {
                    Id = reader.GetGuid("Id"),
                    Gender = reader.GetString("Gender"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetString("LastName"),
                    DateofBirth = reader.GetDateTime("DOB"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    LastEligibilityCheckDate = reader.GetDateTime("LastEligibilityCheck")
                }).AsList();
            }

            patientList.ForEach(patient =>
            {
                queue.Enqueue(patient);
            });

            return queue;
        }

        public static EpisodeData GetCurrentEpisode(PatientEligibilityData patientData)
        {
            var result = new EpisodeData();
            var sql = string.Format(SELECT_PATIENT_EPISODE_CURRENT, patientData.AgencyId, patientData.Id);

            using (var cmd = new FluentCommand<EpisodeData>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                result = cmd.SetMap(reader => new EpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    //Schedule = reader.GetString("Schedule"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                }).AsSingle();
            }

            return result;
        }

        public static bool InsertMedicareEligibility(PatientEligibilityData patientData, string rawResult)
        {
            var result = false;
            var sql = string.Format(INSERT_MEDICARE_ELIGIBILITY, patientData.EligibilityId, patientData.AgencyId, patientData.Id, patientData.EpisodeId, rawResult.Replace("'", "\\'"), 525, patientData.LastEligibilityCheckDate.ToString("yyyy-M-d"));
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        //public static bool UpdateEpisode(EpisodeData episodeData)
        //{
        //    var result = false;
        //    var sql = string.Format(UPDATE_PATIENT_EPISODE, episodeData.Schedule, episodeData.Id, episodeData.AgencyId, episodeData.PatientId);
        //    using (var cmd = new FluentCommand<int>(sql))
        //    {
        //        cmd.SetConnection("AgencyManagementConnectionString");
        //        if (cmd.AsNonQuery() > 0)
        //        {
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        public static bool UpdatePatient(PatientEligibilityData patientData)
        {
            var result = false;
            var sql = string.Format(UPDATE_PATIENT, patientData.LastEligibilityCheckDate.ToString("yyyy-M-d"), patientData.Id, patientData.AgencyId);
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool InsertScheduleEvent(ScheduleEvent schedule)
        {
            var result = false;
            var sql = string.Format(INSERT_SCHEDULEEVENTS,
                schedule.AgencyId,
                schedule.EventId,
                schedule.EpisodeId,
                schedule.PatientId,
                schedule.UserId,
                schedule.UserName,
                schedule.Discipline,
                schedule.DisciplineTask,
                schedule.Status,
                schedule.EventDate,
                schedule.VisitDate,
                schedule.StartDate,
                schedule.EndDate,
                schedule.IsBillable);
            using (var cmd = new FluentCommand<int>(sql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
