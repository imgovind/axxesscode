﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Scheduled.Eligibility.Model
{
    public class EpisodeData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
       // public string Schedule { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
    }
}
