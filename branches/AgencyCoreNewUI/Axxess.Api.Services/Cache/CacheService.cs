﻿namespace Axxess.Api.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;

    public class CacheService : BaseService, ICacheService
    {
        #region ICacheService Members

        public void RefreshAgency(Guid agencyId)
        {
            try
            {
                AgencyEngine.Instance.Refresh(agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.ToString());
            }
        }

        public string GetAgencyXml(Guid agencyId)
        {
            var agencyXml = string.Empty;
            try
            {
                agencyXml = AgencyEngine.Instance.Get(agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return agencyXml;
        }

        public void RefreshPhysicians(Guid agencyId)
        {
            try
            {
                PhysicianEngine.Instance.Refresh(agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.Message);
            }
        }

        public string GetPhysicianXml(Guid physicianId, Guid agencyId)
        {
            var physicianXml = string.Empty;
            try
            {
                physicianXml = PhysicianEngine.Instance.Get(physicianId, agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.Message);
            }
            return physicianXml;
        }

        public List<string> GetPhysicians(Guid agencyId)
        {
            var physicians = new List<string>();
            try
            {
                physicians = PhysicianEngine.Instance.GetPhysicians(agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.Message);
            }
            return physicians;
        }

        public void RefreshUsers(Guid agencyId)
        {
            try
            {
                UserEngine.Instance.Refresh(agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.Message);
            }
        }

        public string GetUserDisplayName(Guid userId, Guid agencyId)
        {
            var displayName = string.Empty;
            try
            {
                displayName = UserEngine.Instance.GetName(userId, agencyId);
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.Message);
            }
            return displayName;
        }

        public List<UserData> GetUsers()
        {
            var users = new List<UserData>();
            try
            {
                users = UserEngine.Instance.GetUsers();
            }
            catch (Exception ex)
            {
                throw GetFault<DefaultFault>(ex.Message);
            }
            return users;
        }

        #endregion

    }
}