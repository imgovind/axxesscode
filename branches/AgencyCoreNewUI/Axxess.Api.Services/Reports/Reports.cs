﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    internal static class Reports
    {
        private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();

        #region Static Helper Methods

        internal static AgencyData GetAgency(Guid agencyId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator`  FROM `agencies` WHERE `Id` = @agencyid;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static AgencyData GetAgencyLocation(Guid agencyId, Guid agencyLocationId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator` , `IsLocationStandAlone`  FROM `agencylocations` WHERE `Id` = @agencylocationid  AND `AgencyId`= @agencyid ;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static string NumberEligible(Guid agencyId, Guid agencyLocationId, DateTime sampleMonthLast)
        {
            int count = 0;
            var script = @"SELECT Count(`Id`) FROM `patients` WHERE `AgencyId` = @agencyid AND `AgencyLocationId` = @agencylocationid  AND `IsDeprecated` = 0 AND `Status` in (1, 2) " +
                "AND `PrimaryInsurance` in (1,2,3,4) AND (`Created` <= @month || `StartofCareDate` <= @month)";

            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("month", sampleMonthLast)
                .AsScalar();
            }

            return count.ToString();
        }

        internal static string SkilledVisitCount(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();

            episodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (schedule != null && schedule.Count > 0)
                    {
                        var scheduledEvents = schedule.Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsSkilledCare() && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            list.AddRange(scheduledEvents);
                        }
                    }
                }
            });

            return list.Count.ToString();
        }

        internal static string SkilledVisitCount(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var episodes = GetEpisodesBetweenByPatientId(agencyId, patientId, startDate, endDate);
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var scheduledEvents = schedule.Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsSkilledCare() && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                            if (scheduledEvents != null && scheduledEvents.Count > 0)
                            {
                                list.AddRange(scheduledEvents);
                            }
                        }
                    }
                });
            }

            return list.Count.ToString();
        }

        internal static List<ScheduleEvent> AllSkilledVisits(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var patientEpisodes = episodes.Where(e =>
                 (e.StartDate.Date >= startDate.Date && e.StartDate.Date <= endDate.Date)
                || (e.EndDate.Date >= startDate.Date && e.EndDate.Date <= endDate.Date)
                || (startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date)
                || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date));
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(s => s.EventId != Guid.Empty && s.IsDeprecated == false && (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW())
                               && (s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)).ToList();
                    if (schedule != null && schedule.Count > 0)
                    {
                        list.AddRange(schedule);
                    }
                }
            });

            return list;
        }

        internal static List<EpisodeData> GetEpisodesBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.AgencyLocationId = @agencylocationid  " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesBetweenByPatientId(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate , patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Id = @patientid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<EpisodeData> GetEpisodesBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate, List<int> paymentSources)
        {
            var items = new List<EpisodeData>();
            var results = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.PaymentSource as PaymentSource, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.AgencyLocationId = @agencylocationid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                items = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PaymentSources = reader.GetStringNullable("PaymentSource")
                })
                .AsList();
            }

            string sourceList = string.Empty;
            paymentSources.ForEach(p => sourceList += string.Format("{0} ", p));
            Windows.EventLog.WriteEntry(string.Format("Selected Payment Sources: {0}", sourceList), System.Diagnostics.EventLogEntryType.Information);

            if (items != null && items.Count > 0)
            {
                items.ForEach(i =>
                {
                    if (i.PaymentSources.IsNotNullOrEmpty())
                    {
                        Windows.EventLog.WriteEntry(string.Format("Patient Payment Sources: {0}", i.PaymentSources), System.Diagnostics.EventLogEntryType.Information);
                        paymentSources.ForEach(p =>
                        {
                            if (i.PaymentSources.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Contains(p.ToString()))
                            {
                                results.Add(i);
                                return;
                            }
                        });
                    }
                });
            }
            return results;
        }

        internal static List<EpisodeData> GetEpisodesBetweenForPatient(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<EpisodeData>();
            var script =
                @"SELECT patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.MiddleInitial as MiddleInitial, patients.DischargeDate as DischargeDate, " +
                "patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber, patients.AddressLine1 as AddressLine1, " +
                "patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.PhoneHome as PhoneHome, " +
                "patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patientepisodes.EndDate as EndDate, " +
                "patientepisodes.StartDate as StartDate , patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Id = @patientid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new EpisodeData
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Status = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static Assessment GetEpisodeAssessment(EpisodeData episode, DateTime end)
        {
            Assessment assessment = null;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var sampleMonthEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= end.Date && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                    .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                if (sampleMonthEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(sampleMonthEvent.EventId, sampleMonthEvent.GetAssessmentType(), episode.AgencyId);
                }
                else
                {
                    var previousEpisode = GetPreviousEpisode(episode.AgencyId, episode.PatientId, episode.Id);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var samplePreviousEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= end.Date && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                            .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                        if (samplePreviousEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(samplePreviousEvent.EventId, samplePreviousEvent.GetAssessmentType(), episode.AgencyId);
                        }
                    }
                }
            }
            return assessment;
        }

        internal static EpisodeData GetPreviousEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var episodes = GetPatientEpisodes(agencyId, patientId);
            if (episodes != null && episodes.Count > 0)
            {
                var episode = episodes.Where(e => e.Id == episodeId).FirstOrDefault();
                if (episode != null)
                {
                    return episodes.Where(e => e.EndDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                }
            }
            return null;
        }

        internal static List<EpisodeData> GetPatientEpisodes(Guid agencyId, Guid patientId)
        {
            var list = new List<EpisodeData>();
            var script = @"SELECT patientepisodes.Id, patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid " +
                "AND patientepisodes.IsActive = 1";

            using (var cmd = new FluentCommand<EpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new EpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        #endregion

        #region Helper Functions

        internal static DateTime GetStartOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month, 1, 0, 0, 0, 0);
        }

        internal static DateTime GetEndOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month,
               DateTime.DaysInMonth(Year, (int)Month), 23, 59, 59, 999);
        }

        internal static DateTime GetStartOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetStartOfMonth(12, Year - 1);
            else
                return GetStartOfMonth(Month - 1, Year);
        }

        internal static DateTime GetEndOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetEndOfMonth(12, Year - 1);
            else
                return GetEndOfMonth(Month - 1, Year);
        }

        #endregion
    }
}
