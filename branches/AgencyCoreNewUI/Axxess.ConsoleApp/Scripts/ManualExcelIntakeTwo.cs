﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    using Axxess.Core.Extension;

    public static class ManualExcelIntakeTwo
    {
        private static string input = Path.Combine(App.Root, "Files\\firstaid.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\firstaid_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        var patientData = new PatientData();
                                        patientData.AgencyId = "fa842737-ebee-4925-a451-90d1339dcc32";
                                        patientData.AgencyLocationId = "90c39dbe-498e-487d-899d-6dab61592bf3";
                                        
                                        var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (nameArray != null && nameArray.Length > 1)
                                        {
                                            patientData.LastName = nameArray[0].Trim();
                                            patientData.FirstName = nameArray[1].Trim();
                                        }

                                        if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                        {
                                            var status = dataRow.GetValue(1);

                                            if (status.IsEqual("A"))
                                            {
                                                patientData.PatientStatusId = "1";
                                            }
                                            else if (status.IsEqual("D"))
                                            {
                                                patientData.PatientStatusId = "2";
                                            }
                                            else if (status.IsEqual("Non-Admit"))
                                            {
                                                patientData.PatientStatusId = "4";
                                            }
                                            else
                                            {
                                                patientData.PatientStatusId = "3";
                                            }
                                        }
                                        patientData.PatientNumber = dataRow.GetValue(3);
                                        if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                        {
                                            patientData.StartofCareDate = dataRow.GetValue(4).ToMySqlDate(0);
                                        }
                                        if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                        {
                                            patientData.PatientStatusId = "2";
                                            patientData.DischargeDate = dataRow.GetValue(5).ToMySqlDate(0);
                                        }
                                        if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                        {
                                            patientData.BirthDate = dataRow.GetValue(6).ToMySqlDate(0);
                                        }
                                        patientData.Gender = dataRow.GetValue(7).IsEqual("M") ? "Male" : "Female";
                                        if (dataRow.GetValue(8).IsNotNullOrEmpty() && dataRow.GetValue(9).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Current Episode: {0} - {1}. ", dataRow.GetValue(8).ToDateTime().ToShortDateString(), dataRow.GetValue(9).ToDateTime().ToShortDateString());
                                        }
                                        if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                        {
                                            patientData.Phone = dataRow.GetValue(11).ToPhoneDB();
                                        }
                                        if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                        {
                                            var addressArray = dataRow.GetValue(12).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                            if (addressArray != null)
                                            {
                                                if (addressArray.Length == 3)
                                                {
                                                    patientData.AddressLine1 = addressArray[0].Replace("'", "`").Trim();
                                                    patientData.AddressLine2 = "";
                                                    patientData.AddressCity = addressArray[1].Trim();

                                                    var locationArray = addressArray[2].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (locationArray != null && locationArray.Length == 2)
                                                    {
                                                        patientData.AddressState = locationArray[0].Trim();
                                                        patientData.AddressZipCode = locationArray[1].Trim();
                                                    }
                                                }

                                                if (addressArray.Length == 4)
                                                {
                                                    patientData.AddressLine1 = addressArray[0].Replace("`", "").Trim();
                                                    patientData.AddressLine2 = addressArray[1].Replace("`", "").Trim();
                                                    patientData.AddressCity = addressArray[2].Trim();

                                                    var locationArray = addressArray[3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (locationArray != null && locationArray.Length == 2)
                                                    {
                                                        patientData.AddressState = locationArray[0].Trim();
                                                        patientData.AddressZipCode = locationArray[1].Trim();
                                                    }
                                                }
                                            }
                                        }
                                        if (dataRow.GetValue(13).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("PCP: {0}. ", dataRow.GetValue(13));
                                        }
                                        if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("PCP Phone: {0}. ", dataRow.GetValue(14));
                                        }
                                        if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("PCP Fax: {0}. ", dataRow.GetValue(15));
                                        }
                                        if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Diagnosis: {0}. ", dataRow.GetValue(16));
                                        }
                                        if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                        {
                                            var payee = dataRow.GetValue(17).Replace("(", "").Replace(")", "");
                                            var payeeArray = payee.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (payeeArray != null && payeeArray.Length == 2)
                                            {
                                                if (dataRow.GetValue(17).ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "1";
                                                    patientData.MedicareNumber = payeeArray[1].Trim();
                                                }
                                                else
                                                {
                                                    patientData.Comments += string.Format("{0}. ", dataRow.GetValue(17));
                                                }
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("{0}. ", dataRow.GetValue(17));
                                            }
                                        }
                                        if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("PT Needs: {0}. ", dataRow.GetValue(18));
                                        }
                                        if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Emergency Contact: {0}. ", dataRow.GetValue(19));
                                        }
                                        if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Pharmacy Information: {0}. ", dataRow.GetValue(20));
                                        }
                                        
                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                        textWriter.Write(textWriter.NewLine);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
