﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.OasisC.Domain;
using Axxess.OasisC.Repositories;

using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.Repositories;

namespace Axxess.ConsoleApp.Tests
{
    public static class FluentAdoTest
    {
        public static void Run()
        {
            //GetPatientsFluent();
            //GetPatientsSubsonic();
            FixRocs();
        }

        private static void FixPlanofCare()
        {
            string sql = @"select * from planofcares";

            var planofcares = new FluentCommand<PlanofCare>(sql)
                .SetConnection("OasisCConnectionString")
                .AsList();

            if (planofcares != null)
            {
                var oasisDataProvider = new OasisCDataProvider();
                planofcares.ForEach(planofcare =>
                {
                    if (planofcare.Data.IsNotNullOrEmpty())
                    {
                        planofcare.Data = Clean(planofcare.Data);
                        if (oasisDataProvider.PlanofCareRepository.Update(planofcare)){
                            Console.WriteLine("Saved: {0}", planofcare.ToString());
                        }
                    }
                });
            }
        }

        private static void FixRocs()
        {
            string sql = @"select * from resumptionofcareassessments";

            var oasisList = new FluentCommand<ResumptionofCareAssessment>(sql)
                .SetConnection("OasisCConnectionString")
                .AsList();

            if (oasisList != null)
            {
                var oasisDataProvider = new OasisCDataProvider();
                oasisList.ForEach(oasis =>
                {
                    if (oasis.OasisData.IsNotNullOrEmpty())
                    {
                        oasis.OasisData = Clean(oasis.OasisData);
                        oasis.Questions = oasis.OasisData.ToObject<List<Question>>();
                        if (oasisDataProvider.OasisAssessmentRepository.Update(oasis))
                        {
                            Console.WriteLine("Saved: {0}", oasis.ToString());
                        }
                    }
                });
            }
        }

        private static void FixStartofCares()
        {
            string sql = @"select * from startofcareassessments";

            var oasisList = new FluentCommand<StartOfCareAssessment>(sql)
                .SetConnection("OasisCConnectionString")
                .AsList();

            if (oasisList != null)
            {
                var oasisDataProvider = new OasisCDataProvider();
                oasisList.ForEach(oasis =>
                {
                    if (oasis.OasisData.IsNotNullOrEmpty())
                    {
                        oasis.OasisData = Clean(oasis.OasisData);
                        oasis.Questions = oasis.OasisData.ToObject<List<Question>>();
                        if (oasisDataProvider.OasisAssessmentRepository.Update(oasis))
                        {
                            Console.WriteLine("Saved: {0}", oasis.ToString());
                        }
                    }
                });
            }
        }

        private static string Clean(string data)
        {
            var i = 1;
            var text = data;

            do
            {
                text = text.Replace(string.Format("PrimaryDiagnosisDate{0}", i), string.Format("PrimaryDiagnosis{0}Date", i));
                text = text.Replace(string.Format("SymptomControlRating{0}", i), string.Format("OtherDiagnose{0}Rating", i));
                i++;
            } while (i < 9);

            return text;
        }

        private static void GetPatientsFluent()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            string sql = @"select FirstName, LastName from patients where agencyid = @agencyid and status = @statusid order by created desc limit 0, 5";

            var patients = new FluentCommand<Patient>(sql)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", new Guid("B95CB587-587F-45E9-95A8-2698FDD4EC0D"))
                .AddInt("statusid", 1)
                .AsTable();

            if (patients != null)
            {
                int counter = 0;
                foreach (DataRow patient in patients.Rows)
                {
                    counter++;
                    Console.WriteLine("({0}) {1} {2}", counter.ToString(), patient[0].ToString().ToTitleCase(), patient[1].ToString().ToTitleCase());
                }
            }

            stopWatch.Stop();
            Console.WriteLine("Took: {0:F} seconds", stopWatch.Elapsed.TotalSeconds);
        }

        private static void GetPatientsSubsonic()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var agencyDataProvider = new AgencyManagementDataProvider();
            var patients = agencyDataProvider.PatientRepository.FindPatientOnly(1, new Guid("B95CB587-587F-45E9-95A8-2698FDD4EC0D")).OrderByDescending(p => p.Created).Take(5).ToList();

            if (patients != null)
            {
                int counter = 0;
                patients.ForEach(patient =>
                {
                    counter++;
                    Console.WriteLine("({0}) {1} {2}", counter.ToString(), patient.FirstName.ToTitleCase(), patient.LastName.ToTitleCase());
                });
            }

            stopWatch.Stop();
            Console.WriteLine("Took: {0:F} seconds", stopWatch.Elapsed.TotalSeconds);
        }
    }
}
