﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class SynergyIntakeThree
    {
        private static string input = Path.Combine(App.Root, "Files\\gateway.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\gateway_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        var patientData = new PatientData();
                                        patientData.AgencyId = "146a3b4b-e47a-499f-93a3-2f1af8de04d7";
                                        patientData.AgencyLocationId = "62b1d5fb-c718-4274-b2ef-df43666a2098";

                                        if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                        {
                                            var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                patientData.FirstName = nameArray[1].Trim();
                                                patientData.LastName = nameArray[0].Trim();
                                            }
                                        }
                                        patientData.PatientNumber = dataRow.GetValue(4);
                                        patientData.MedicareNumber = dataRow.GetValue(5);
                                        if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                        {
                                            patientData.BirthDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7))).ToString("yyyy-M-d");
                                        }
                                        if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                        {
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(10))).ToString("yyyy-M-d");
                                        }
                                        if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                        {
                                            var status = dataRow.GetValue(14).ToLower();
                                            if (status.Contains("admitted"))
                                            {
                                                patientData.PatientStatusId = "1";
                                            }
                                            else if (status.Contains("discharged"))
                                            {
                                                patientData.PatientStatusId = "2";
                                            }
                                            else
                                            {
                                                patientData.PatientStatusId = "3";
                                            }
                                        }
                                        if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Triage Level: {0}. ", dataRow.GetValue(17));
                                        }
                                        if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("DX Code: {0}. ", dataRow.GetValue(20));
                                        }
                                        var stringBuilder = new StringBuilder();
                                        if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                        {
                                            stringBuilder.AppendFormat("Physician Name: {0}. ", dataRow.GetValue(25));
                                        }

                                        if (dataRow.GetValue(35).IsNotNullOrEmpty())
                                        {
                                            stringBuilder.AppendFormat("Physician Phone: {0}. ", dataRow.GetValue(35).ToPhoneDB());
                                        }

                                        if (dataRow.GetValue(40).IsNotNullOrEmpty())
                                        {
                                            stringBuilder.AppendFormat("Physician UPIN: {0}. ", dataRow.GetValue(40));
                                        }

                                        patientData.Comments = stringBuilder.ToString().Trim();

                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                        textWriter.Write(textWriter.NewLine);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
