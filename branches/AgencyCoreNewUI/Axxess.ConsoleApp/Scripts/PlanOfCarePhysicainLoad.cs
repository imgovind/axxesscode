﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.Text;
    using System.IO;

    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.OasisC.Repositories;

    public static class PlanOfCarePhysicianLoad
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\PlanOfCareOrders_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static readonly IPlanofCareRepository planofCareRepository = new PlanofCareRepository(new SimpleRepository("OasisCConnectionString", SimpleRepositoryOptions.None));
        private static readonly IPhysicianRepository physicianRepository = new PhysicianRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        public static void Run()
        {
            var planOfCareOrders = planofCareRepository.GetAllPlanOfCareOrders();
            if (planOfCareOrders != null && planOfCareOrders.Count > 0)
            {
                using (TextWriter textWriter = new StreamWriter(output, true))
                {
                    foreach (var order in planOfCareOrders)
                    {
                        if (!order.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(order.PhysicianId, order.AgencyId);
                            if (physician != null)
                            {
                                var xmlData = new StringBuilder(physician.ToXml());
                                var data = xmlData.Replace("'", @"\'");

                                textWriter.WriteLine(string.Format("UPDATE `planofcares` SET `PhysicianData` = '{0}'   WHERE   `Id` = '{1}' AND `AgencyId` = '{2}' ;", data, order.Id, order.AgencyId));
                                
                                textWriter.Write(textWriter.NewLine);

                            }
                        }
                    }
                }
            }
        }
    }
}
