﻿namespace Axxess.ConsoleApp
{
    public class PackageScript
    {
        private PackageData packageData;
        private const string PACKAGE_INSERT = "INSERT INTO `drugpackages`(`SequenceNumber`,`Code`,`Size`,`Type`) VALUES ('{0}', '{1}', '{2}', '{3}');";

        public PackageScript(PackageData packageData)
        {
            this.packageData = packageData;
        }

        public override string ToString()
        {
            return string.Format(PACKAGE_INSERT, packageData.SequenceNumber, packageData.Code, packageData.Size, packageData.Type);
        }
    }
}
