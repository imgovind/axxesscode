﻿namespace Axxess.Api.ExternalLinker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Api.Contracts;
    using System.Diagnostics;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Membership.Repositories;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using StructureMap.Configuration.DSL;
    using StructureMap;
    using System.ComponentModel;
    using Axxess.AgencyManagement.Enums;

    public class ExternalLinkerRegistry : Registry
    {
        public ExternalLinkerRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.WithDefaultConventions();
            });
            For<IWebConfiguration>().Use<WebConfiguration>();
            For<INotification>().Use<EmailNotification>();
            For<ICryptoProvider>().Use<RijndaelProvider>();
        }
    }

    public class ExternalLinkerService : BaseService, IExternalLinkerService
    {
        #region Constructor 
        ISupportManagementDataProvider supportManagementDataProvider;
        ICustomerNotesRepository customerNotesRepository;

        IAgencyManagementMembershipDataProvider agencyManagementMembershipDataProvider;
        MembershipDataProvider membershipDataProvider;
        ISupportRepository supportLoginRepository;
        ILoginRepository loginRepository;
        public ExternalLinkerService()
        {
            init();
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());
            agencyManagementMembershipDataProvider = new AgencyManagementMembershipDataProvider();
            supportManagementDataProvider = new SupportManagementDataProvider();
            membershipDataProvider = new MembershipDataProvider();
            customerNotesRepository = supportManagementDataProvider.CustomerNotesRepository;
            supportLoginRepository = membershipDataProvider.SupportRepository;
        }

        private void init()
        {
            ObjectFactory.Initialize(x =>
                {
                    x.AddRegistry<ExternalLinkerRegistry>();
                });
        }
        #endregion

        public string UpdatedNote(string noteId)
        {
            string result = "";
            var note = customerNotesRepository.GetCustomerNote(new Guid(noteId));
            if(note!=null)
            {
                note.RecipientArray = note.Recipients.IsNotNullOrEmpty() ? note.Recipients.ToObject<List<Guid>>() : new List<Guid>();
                note.RecipientArray.Add(note.LoginId);
                var agencyAgencySnapshot = loginRepository.GetAgencySnapshot(note.AgencyId);
                note.AgencyName = agencyAgencySnapshot.Name;
                var url = GetLinkUrl(LinkEntityType.CustomerNote, noteId);
                var link = string.Format("<a href=\"{0}\">Click Here</a>", url);
                foreach (var recipient in note.RecipientArray)
                {
                    var body = string.Format("<p>Hello {0},</p><p>THIS IS AN AUTOMATED RESPONSE, PLEASE DO NOT REPLY!!!</p><p>Axxess Technology Developers have updated a note for <b>{1}</b></p><p>To view this note, please {2}.</p><p>Thank you, and have a great day!</p>",
                        supportLoginRepository.GetLoginDisplayName(recipient),note.AgencyName,link);
                    Axxess.Core.Infrastructure.Notify.User(CoreSettings.NoReplyEmail, supportLoginRepository.GetLoginEmailAddress(recipient), "Axxess Technology Solutions updated a note", body);
                }
            }
            return result;
        }

        public string GetLinkUrl(LinkEntityType entityType, string entityId)
        {
            var conf = Axxess.Core.Infrastructure.Container.Resolve<IWebConfiguration>();
            var url = new StringBuilder();
            if (true)
            {
                url.Append(conf.AppSettings["DirectLogin"]);
                url.Append(Crypto.Encrypt(string.Format(entityType.GetDescription(),entityId)));
            }
            return url.ToString();
        }
    }
}
