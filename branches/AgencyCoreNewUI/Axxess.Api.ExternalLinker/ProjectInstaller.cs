﻿namespace Axxess.Api.Oasis
{
    using System;
    using System.ServiceProcess;
    using System.ComponentModel;
    using System.Configuration.Install;

    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller externalLinkerInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.externalLinkerInstaller = new ServiceInstaller();
            this.externalLinkerInstaller.StartType = ServiceStartMode.Automatic;
            this.externalLinkerInstaller.ServiceName = "ExternalLinkerService";
            this.externalLinkerInstaller.DisplayName = "External Linker Service";
            this.externalLinkerInstaller.Description = "Provides external links to the AgencyCore Application.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.externalLinkerInstaller });
        }
    }
}
