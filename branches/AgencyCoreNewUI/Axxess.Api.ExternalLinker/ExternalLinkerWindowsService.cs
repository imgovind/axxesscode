﻿namespace Axxess.Api.ExternalLinker
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    public partial class ExternalLinkerWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public ExternalLinkerWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "ExternalLinker";
        }

        public void RunConsole(string[] args)
        {
            OnStart(args);
            Console.WriteLine("Running...");
            while (true) { }
            OnStop();

        }

        protected override void OnStart(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(ExternalLinkerService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("External Linker Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("External Linker Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
