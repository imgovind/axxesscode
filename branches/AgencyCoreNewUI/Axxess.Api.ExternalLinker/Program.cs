﻿namespace Axxess.Api.ExternalLinker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using System.ServiceProcess;
    using System.Diagnostics;

    class Program
    {
        static void Main()
        {
            //new ExternalLinkerWindowsService().RunConsole(null);
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new ExternalLinkerWindowsService()
                };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
