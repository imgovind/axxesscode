﻿namespace Axxess.Membership.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
   
    public class PermissionTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Permissions { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }


    }
}
