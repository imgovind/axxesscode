﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class Logon
    {
        [Required]
        [DisplayName("E-mail Address")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }

        public bool IsLocked { get; set; }
    }
}
