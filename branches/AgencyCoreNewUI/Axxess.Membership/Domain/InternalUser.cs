﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class InternalUser
    {
        public int Id { get; set; }
        public Guid LoginId { get; set; }
        public string Permissions { get; set; }

        [SubSonicIgnore]
        public bool IsActive { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public string EmailAddress { get; set; }

     
    }
}
