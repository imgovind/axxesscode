﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class PermissionLink
    {
        public int Id { get; set; }
        public int ChildId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SubCategory { get; set; }
        public bool IsSubCategory { get; set; }
        public string Category { get; set; }
    }
}
