﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

   public class Permission
   {
       public int Id { get; set; }
       public bool HasChild { get; set; }
       public string ShortName { get; set; }
       public string LongName { get; set; }
       public string Description { get; set; }
       public string ChildPermissions { get; set; }
       public int Category { get; set; }
       public int ChildGroup { get; set; }
       public bool IsDeprecated { get; set; }

       [SubSonicIgnore]
       public string CategoryName { get; set; }
    }
}
