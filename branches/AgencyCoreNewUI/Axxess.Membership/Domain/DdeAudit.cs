﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class DdeAudit
    {
        #region Members

        public int Id { get; set; }
        public Guid LoginId { get; set; }
        public string Action { get; set; }
        public DateTime Created { get; set; }
        public int DdeCredentialId { get; set; }

        #endregion
    }
}
