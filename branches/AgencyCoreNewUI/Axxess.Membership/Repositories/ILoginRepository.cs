﻿//namespace Axxess.Membership.Repositories
//{
//    using System;
//    using System.Collections.Generic;

//    using Enums;
//    using Domain;

//    using Axxess.Core.Infrastructure;
//    using Axxess.Core;

//    public interface ILoginRepository
//    {
//        SupportLogin Find(Guid id);
//        bool Add(SupportLogin login);
//        bool Update(SupportLogin login);
//        bool Delete(Guid loginId);
//        bool RemoveLogin(Guid id);
//        ICollection<SupportLogin> GetAll();
//        SupportLogin Find(string emailAddress);
//        ICollection<SupportLogin> AutoComplete(string searchString);

//        string GetLoginDisplayName(Guid loginId);
//        string GetLoginEmailAddress(Guid loginId);

//        IList<SupportLogin> GetAllByRole(Roles role);

//        DdeCredential GetDdeCredential(Guid id);
//        IList<DdeCredential> GetDdeCredentials();
//        bool UpdateDdeCredential(DdeCredential ddeCredential);

//       List<PermissionLink> GetPermissions();
//       InternalUser GetInternalUserOnly(Guid loginId);
//       InternalUser GetInternalUser(Guid loginId);
//       bool UpdateInternalUserPermission(InternalUser internalUser);
//       List<MenuElement> GetMenuElements();
//       bool AddInternalUser(InternalUser internalUser);
//    }
//}
