﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;
    using Axxess.Membership.Enums;
    using System.Text;

    public class SupportRepository : ISupportRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private string connectionString;

        public SupportRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionString = "SupportMembershipConnectionString";
        }

        #endregion

        #region ILoginRepository Members

        public bool Add(SupportLogin login)
        {
            if (login != null)
            {
                login.Id = Guid.NewGuid();
                login.Created = DateTime.Now;
                database.Add<SupportLogin>(login);
                return true;
            }
            return false;
        }

        public SupportLogin Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<SupportLogin>(l => l.Id == id);
        }

        public SupportLogin Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<SupportLogin>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(SupportLogin login)
        {
            if (login != null)
            {
                database.Update<SupportLogin>(login);
                return true;
            }
            return false;
        }

        public bool RemoveLogin(Guid id)
        {
            try
            {
                return database.Delete<SupportLogin>(id) > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            var login = database.Single<SupportLogin>(l => l.Id == loginId);

            if (login != null)
            {
                login.IsActive = false;
                database.Update<SupportLogin>(login);
                result = true;
            }
            return result;
        }

        public ICollection<SupportLogin> GetAll(bool status)
        {
            return database.All<SupportLogin>().Where(s => s.IsActive == status).OrderBy(s => s.DisplayName).ToList().AsReadOnly();
        }

        public IList<SupportLogin> GetAllByRole(Roles role)
        {
            return database.Find<SupportLogin>(l => l.Role == role.ToString() && l.IsActive == true).OrderBy(l => l.DisplayName).ToList();
        }

        public ICollection<SupportLogin> AutoComplete(string searchString)
        {
            return database.Find<SupportLogin>(u => u.EmailAddress.StartsWith(searchString)).ToList();
        }

        public string GetLoginDisplayName(Guid loginId)
        {
            var name = string.Empty;
            if (!loginId.IsEmpty())
            {
                var script = @"SELECT DisplayName from logins where Id = @loginid;";
                using (var cmd = new FluentCommand<SupportLogin>(script))
                {
                    var login = cmd.SetConnection(connectionString)
                        .AddGuid("loginid", loginId)
                        .SetMap(reader => new SupportLogin
                        {
                            DisplayName = reader.GetString("DisplayName")
                        }).AsSingle();

                    if (login != null)
                    {
                        name = login.DisplayName;
                    }
                }
            }

            return name;
        }

        public List<SupportLogin> GetLoginDisplayNamesWithEmail(List<Guid> loginIds)
        {
            var list = new List<SupportLogin>();
            if (loginIds != null && loginIds.Count > 0)
            {

                var ids = loginIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT Id, DisplayName,EmailAddress  from logins where Id IN ({0});", ids);
                using (var cmd = new FluentCommand<SupportLogin>(script))
                {
                    list = cmd.SetConnection(connectionString)
                         .SetMap(reader => new SupportLogin
                         {
                             Id = reader.GetGuid("Id"),
                             DisplayName = reader.GetString("DisplayName"),
                             EmailAddress = reader.GetString("EmailAddress")
                         }).AsList();


                }
            }
            return list;
        }

        public string GetLoginEmailAddress(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.EmailAddress;
                }
            }

            return name;
        }

        #endregion

        #region DDE Credentials Members

        public DdeCredential GetDdeCredential(Guid id)
        {
            return database.Single<DdeCredential>(d => d.Id == id);
        }

        public IList<DdeCredential> GetDdeCredentials()
        {
            return database.All<DdeCredential>().ToList();
        }

        public bool UpdateDdeCredential(DdeCredential ddeCredential)
        {
            if (ddeCredential != null)
            {
                database.Update<DdeCredential>(ddeCredential);
                return true;
            }
            return false;
        }

        #endregion

        public List<PermissionLink> GetPermissions()
        {
            var script = string.Format(@"SELECT 
                                        pal.PermissionId as PermissionId ,
                                        pal.ActionId as ActionId ,
                                        pal.Name as Name, 
                                        pal.Description as Description ,
                                        psc.Name as SubCategory,
                                        psc.IsSubCategory as IsSubCategory,
                                        pc.Name as Category
                                                 FROM
                                                    permissionactivitylinks as pal
                                                        INNER JOIN permissionsubcategories as psc ON psc.`Id` = pal.PermissionId 
                                                        INNER JOIN permissioncategories as pc ON pc.`Id` = psc.CategoryId     
                                                        INNER JOIN permissionactions as pa ON pa.`Id` = pal.ActionId ");

            var permissionLinks = new List<PermissionLink>();
            using (var cmd = new FluentCommand<PermissionLink>(script))
            {
                permissionLinks = cmd.SetConnection(connectionString)
                .SetMap(reader => new PermissionLink
                {
                    Id = reader.GetInt("PermissionId"),
                    ChildId = reader.GetInt("ActionId"),
                    Name = reader.GetStringNullable("Name"),
                    Description = reader.GetStringNullable("Description"),
                    Category = reader.GetString("Category"),
                    SubCategory = reader.GetString("SubCategory"),
                    IsSubCategory = reader.GetBoolean("IsSubCategory")
                })
                .AsList();
            }
            return permissionLinks.OrderBy(o => o.Category).ThenBy(o => o.IsSubCategory ? o.SubCategory : o.Name).ToList();
        }

        public InternalUser GetInternalUserOnly(Guid loginId)
        {
            var script = @"SELECT
                        internalusers.Id as Id,
                        internalusers.LoginId as LoginId,
                        internalusers.Permissions as Permissions
                            FROM
                                internalusers 
                                  where LoginId = @loginid 
                                        LIMIT 1";

            return new FluentCommand<InternalUser>(script)
                .SetConnection(connectionString)
                .AddGuid("loginid", loginId)
                .SetMap(reader => new InternalUser
                {
                    Id = reader.GetInt("Id"),
                    LoginId = reader.GetGuid("LoginId"),
                    Permissions = reader.GetStringNullable("Permissions")
                })
                .AsSingle();
        }

        public InternalUser GetInternalUser(Guid loginId)
        {
            var script = @"SELECT
                        internalusers.Id as Id,
                        internalusers.LoginId as LoginId,
                        internalusers.Permissions as Permissions, 
                        logins.DisplayName as DisplayName,
                        logins.EmailAddress as EmailAddress
                            FROM
                                internalusers 
                                    INNER JOIN logins ON logins.Id = internalusers.LoginId 
                                       where internalusers.LoginId = @loginid LIMIT 1";

            return new FluentCommand<InternalUser>(script)
                .SetConnection(connectionString)
                .AddGuid("loginid", loginId)
                .SetMap(reader => new InternalUser
                {
                    Id = reader.GetInt("Id"),
                    LoginId = reader.GetGuid("LoginId"),
                    Permissions = reader.GetStringNullable("Permissions"),
                    DisplayName = reader.GetStringNullable("DisplayName"),
                    EmailAddress = reader.GetStringNullable("EmailAddress")
                })
                .AsSingle();
        }

        public bool AddInternalUser(InternalUser internalUser)
        {
            try
            {

                database.Add<InternalUser>(internalUser);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateInternalUserPermission(InternalUser internalUser)
        {
            var count = 0;
            try
            {
                var script = @"Update 
                                  internalusers
                                   set
                                    Permissions = @permissions
                                          where
                                                Id = @id AND
                                                LoginId = @loginid";

                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionString)
                       .AddGuid("loginid", internalUser.LoginId)
                       .AddInt("id", internalUser.Id)
                       .AddString("permissions", internalUser.Permissions)
                       .AsNonQuery();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return count > 0;
        }

        public List<MenuElement> GetMenuElements()
        {
            var script = string.Format(@"SELECT 
                                        mn.Id,
                                        mn.Name,
                                        mn.MenuName,
                                        mn.Url,
                                        mn.OnLoad,
                                        mn.ParentMenus,
                                        mn.Category,
                                        mn.OtherCategory,
                                        mn.Action,
                                        mn.StyleOptions
                                                 FROM
                                                    menus as mn
                                                        ORDER BY mn.MenuName asc");

            var menuElements = new List<MenuElement>();
            using (var cmd = new FluentCommand<MenuElement>(script))
            {
                menuElements = cmd.SetConnection(this.connectionString)
                .SetMap(reader => new MenuElement
                {
                    Id = reader.GetString("Id"),
                    Name = reader.GetStringNullable("Name"),
                    MenuName = reader.GetStringNullable("MenuName"),
                    Url = reader.GetStringNullable("Url"),
                    OnLoad = reader.GetStringNullable("OnLoad"),
                    ParentMenus = reader.GetStringNullable("ParentMenus"),
                    Category = reader.GetInt("Category"),
                    OtherCategory = reader.GetStringNullable("OtherCategory"),
                    Action = reader.GetInt("Action"),
                    StyleOptions = reader.GetStringNullable("StyleOptions")
                })
                .AsList();
            }
            return menuElements;
        }

        public bool AddTemplate(PermissionTemplate template)
        {
            if (template != null)
            {
                template.IsDeprecated = false;
                template.Modified = DateTime.Now;
                database.Add<PermissionTemplate>(template);
                return true;
            }
            return false;
        }


        public bool DeleteTemplate(int Id)
        {
            var template = database.Single<PermissionTemplate>(p => p.Id == Id);
            if (template != null)
            {
                template.IsDeprecated = true;
                template.Modified = DateTime.Now;
                database.Update<PermissionTemplate>(template);
                return true;
            }
            return false;
        }

        public PermissionTemplate GetTemplateOnly(int Id)
        {
            return database.Single<PermissionTemplate>(p => p.Id == Id);
        }

        public IList<PermissionTemplate> GetTemplate()
        {
            return database.All<PermissionTemplate>().ToList();
        }

        public List<PermissionTemplate> GetTemplatePermissions()
        {
            return database.Find<PermissionTemplate>(l => l.IsDeprecated ==  false).OrderBy(l => l.Name).ToList();
        }


        public bool UpdateTemplate(PermissionTemplate template)
        {
            if (template != null)
            {
                template.IsDeprecated = false;
                template.Modified = DateTime.Now;
                database.Update<PermissionTemplate>(template);
                return true;
            }
            return false;
        }

        public bool UpdateUserStatus(Guid loginId, int IsActive)
        {
            var count = 0;
            try
            {
                var script = @"Update 
                              logins
                               set
                                IsActive = @IsActive
                                      where
                                            Id = @loginId";

                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionString)
                       .AddGuid("loginId", loginId)
                       .AddInt("IsActive", IsActive)
                       .AsNonQuery();

                }
            }
            catch (Exception)
            {
                return false;
            }
            return count > 0;
        }





    }
}
