﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Membership.Enums;

    public interface ISupportRepository
    {
        SupportLogin Find(Guid id);
        bool Add(SupportLogin login);
        bool Update(SupportLogin login);
        bool Delete(Guid loginId);
        bool RemoveLogin(Guid id);
        ICollection<SupportLogin> GetAll(bool status);
        SupportLogin Find(string emailAddress);
        ICollection<SupportLogin> AutoComplete(string searchString);
        string GetLoginDisplayName(Guid loginId);
        List<SupportLogin> GetLoginDisplayNamesWithEmail(List<Guid> loginIds);
        string GetLoginEmailAddress(Guid loginId);
        IList<SupportLogin> GetAllByRole(Roles role);
        DdeCredential GetDdeCredential(Guid id);
        IList<DdeCredential> GetDdeCredentials();
        bool UpdateDdeCredential(DdeCredential ddeCredential);
        List<PermissionLink> GetPermissions();
        InternalUser GetInternalUserOnly(Guid loginId);
        InternalUser GetInternalUser(Guid loginId);
        bool UpdateInternalUserPermission(InternalUser internalUser);
        List<MenuElement> GetMenuElements();
        bool AddInternalUser(InternalUser internalUser);
        bool AddTemplate(PermissionTemplate template);
        bool DeleteTemplate(int Id);
        PermissionTemplate GetTemplateOnly(int Id);
        IList<PermissionTemplate> GetTemplate();
        List<PermissionTemplate> GetTemplatePermissions();
        bool UpdateTemplate(PermissionTemplate template);
        bool UpdateUserStatus(Guid loginId, int IsActive);

     
    }
}
