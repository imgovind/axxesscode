﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    
    public class AgencyUserViewData
    {
        public Login Login { get; set; }

        // Holds the permission for View Log
        public bool IsUserCanViewLog { get; set; }
    }
}
