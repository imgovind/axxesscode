﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    
    public class AgencyChangeViewData
    {
        public AgencyChange preAgencyInfo { get; set; }
        public AgencyChange curAgencyInfo { get; set; } 
    }
}
