﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    
    public class AgencyUpgradeViewData
    {
        public Guid AgencyId { get; set; }
        public string Created { get; set; }
        public string AgencyName { get; set; }
        public string AgencyLocationName { get; set; }
        public string RequestedBy { get; set; }
        public Guid AgencyUpgradeId { get; set; }
        public string OldSubscriptionPlan { get; set; }
        public string NewSubscriptionPlan { get; set; }
    }
}
