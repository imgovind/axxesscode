﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using System.Xml.Serialization;
   // using SubSonic.SqlGeneration.Schema;


    public class AgencyCenterViewData
    {
        public Agency Agency { get; set; }
        public AgencySnapshot AgencySnapshot { get; set; }
        public AgencyLocation AgencyLocation { get; set; }
        public AgencySubscriptionPlan SubscriptionPlan { get; set; }
        public List<CustomerNote> CustomerNotes { get; set; }
        public List<AgencySelection> Agencies { get; set; }
        public int Count { get; set; }

        // Holds the permission for Add
        public bool IsUserCanAdd { get; set; }

        // Holds the permission for View Log
        public bool IsUserCanViewLog { get; set; }

        // Holds the permission for View Agency/location
        public bool IsUserCanView { get; set; }

        // Holds the permission for Edit
        public bool IsUserCanEdit { get; set; }

        // Holds the permission for login as physican
        public bool IsUserCanLoginAsPhysician { get; set; }

        // Holds the permission for activate link
        public bool IsUserCanActivateLinkPhysician { get; set; }

        // Holds the permission for email to physician
        public bool IsUserCanEmailToPhysician { get; set; }

        // Holds the permission for export
        public bool IsUserCanExport { get; set; }

        // Holds the permission for view detail
        public bool IsUserCanViewDetail { get; set; }

        // Holds the permission for delete
        public bool IsUserCanDelete { get; set; }

        // Holds the permission for view note list
        public bool IsUserCanViewNote { get; set; }

        // Holds the permission for view Physician
        public bool IsUserCanViewPhysician { get; set; }

        // Holds the permission for load agency supplies
        public bool IsUserCanLoadSupply { get; set; }

        // Holds the permission for load templates
        public bool IsUserCanLoadTemplate { get; set; }

        // Holds the permission for mark agency read-only
        public bool IsUserCanMarkAgencyReadonly { get; set; }

        // Holds the permission for load templates
        public bool IsUserCanSuspendAgency { get; set; }

        // Holds the permission for view agency user
        public bool IsUserCanViewAgencyUser { get; set; }

        // Holds the permission for edit agency user
        public bool IsUserCanEditAgencyUser { get; set; }

        // Holds the permission for Live login as user
        public bool IsUserCanLiveLoginAsUser { get; set; }

        // Holds the permission for QA login as user
        public bool IsUserCanQALoginAsUser { get; set; }

        // Holds the permission for send active link for agency user
        public bool IsUserCanSendActiveLink { get; set; }

        // Holds the permission for email to agency user
        public bool IsUserCanEmailToAgencyUser { get; set; }

      

    }
}
