﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using System.Web.Script.Serialization;
    
    public class MenuViewData
    {
        public List<MenuElement> MenuElement { get; set; }
        [ScriptIgnore]
        public bool IsNewExist { get; set; }
        [ScriptIgnore]
        public bool IsListExist { get; set; }
        [ScriptIgnore]
        public bool IsViewExist { get; set; }
        [ScriptIgnore]
        public bool IsAxxessExist { get; set; }
    }
}
