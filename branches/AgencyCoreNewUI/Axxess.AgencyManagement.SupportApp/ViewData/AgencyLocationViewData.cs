﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    
    public class AgencyLocationViewData
    {
        public Guid Id { get; set; }
        public string Fax { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public Guid AgencyId { get; set; }
        public int PlanLimit { get; set; }
        public string Address { get; set; }
        public bool IsUserPlan { get; set; }
        public string ProviderNumber { get; set; }
        public string PlanType
        {
            get
            {
                if (this.IsUserPlan)
                {
                    return this.PlanLimit.UserSubscriptionPlanName();
                }
                else
                {
                    return this.PlanLimit.PatientSubscriptionPlanName();
                }
            }
        }
    }
}
