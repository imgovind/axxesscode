﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;

    public class CustomerNoteViewData
    {
        public IList<CustomerNote> NotesList { get; set; }

        // Holds the permission for view note
        public bool IsUserCanViewNote { get; set; }

        // Holds the permission for view note detail
        public bool IsUserCanViewNoteDetail { get; set; }

        // Holds the permission for view agency
        public bool IsUserCanView { get; set; }
    }
}
