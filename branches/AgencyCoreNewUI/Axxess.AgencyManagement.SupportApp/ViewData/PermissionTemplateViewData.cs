﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Membership.Domain;

    public class PermissionTemplateViewData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>> PermissionData { get; set; }
    }
}
