﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

  public  class InternalUserPermissionViewData
    {
        // Holds the permission for Add
        public bool IsUserCanAdd { get; set; }

        // Holds the permission for Edit
        public bool IsUserCanEdit { get; set; }

        // Holds the permission for Delete
        public bool IsUserCanDelete { get; set; }

        // Holds the permission for Edit Detail
        public bool IsUserCanEditDetail { get; set; }

        // Holds the permission for view
        public bool IsUserCanView { get; set; }

        // Holds the permission for view
        public bool IsUserCanViewNote { get; set; }

        // Holds the permission for view log
        public bool IsUserCanViewLog { get; set; }

        // Holds the permission for Export Agency List
        public bool IsUserCanExport { get; set; }

        // Holds the permission for view agency user
        public bool IsUserCanViewAgencyUser { get; set; }

        // Holds the permission for load template
        public bool IsUserCanLoadTemplate { get; set; }

        // Holds the permission for view physician
        public bool IsUserCanViewPhysician { get; set; }

        // Holds the permission for view note detail
        public bool IsUserCanViewDetail { get; set; }

        // Holds the permission for vire location
        public bool IsUserCanViewLocation { get; set; }

        // Holds the permission for load supply
        public bool IsUserCanLoadSupply { get; set; }

        // Holds the permission for Suspend Agency
        public bool IsUserCanSuspend { get; set; }

        // Holds the permission for ReadOnly
        public bool IsUserCanReadOnly { get; set; }

        // Holds the permission for email to contact
        public bool IsUserCanEmailToContact { get; set; }

        // Holds the permission for email to contact
        public bool IsUserCanReassign { get; set; }

        
       

        

        
    }
}
