﻿namespace Axxess.AgencyManagement.SupportApp.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Membership.Domain;

   public class InternalUserViewData
    {
        public int Id { get; set; }
        public Guid LoginId { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>> PermissionData { get; set; }

        // Holds the permission for View Log
        public bool IsUserCanViewLog { get; set; }
    }
}
