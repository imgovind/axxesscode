﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Web.Mvc;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Membership.Logging;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Repositories;

    using Security;

    using Axxess.LookUp.Repositories;
    using Axxess.Log.Repositories;
    using Axxess.AgencyManagement.SupportApp.Services.Interfaces;

    public class SupportApplicationRegistry : Registry
    {
        public SupportApplicationRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.AddAllTypesOf<IController>();
                x.WithDefaultConventions();
            });

            For<ILog>().Use<DatabaseLog>();
            For<IMembershipDataProvider>().Use<MembershipDataProvider>();
            For<ILookUpDataProvider>().Use<LookUpDataProvider>();
            For<IAgencyManagementDataProvider>().Use<AgencyManagementDataProvider>();
            For<IAccountingManagementDataProvider>().Use<AccountingManagementDataProvider>();
            For<IAgencyManagementMembershipDataProvider>().Use<AgencyManagementMembershipDataProvider>();
            For<ISupportManagementDataProvider>().Use<SupportManagementDataProvider>();
            For<ISupportMembershipService>().Use<SupportMembershipService>();
            For<IFormsAuthenticationService>().Use<FormsAuthenticationService>();
            For<ILogDataProvider>().Use<LogDataProvider>();
        }
    }
}
