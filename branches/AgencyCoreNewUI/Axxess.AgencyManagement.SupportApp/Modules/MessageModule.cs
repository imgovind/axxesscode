﻿namespace Axxess.AgencyManagement.SupportApp.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class MessageModule : Module
    {
        public override string Name
        {
            get { return "Message"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region System
            routes.MapRoute(
                "SystemNew",
                "Message/System/New",
                new { controller = this.Name, action = "SystemNew" });
            routes.MapRoute(
                "SystemCreate",
                "Message/System/Create",
                new { controller = this.Name, action = "SystemCreate" });
            #endregion

            #region Dashboard
            routes.MapRoute(
                "DashboardNew",
                "Message/Dashboard/New",
                new { controller = this.Name, action = "DashboardNew" });
            routes.MapRoute(
                "DashboardCreate",
                "Message/Dashboard/Create",
                new { controller = this.Name, action = "DashboardCreate" });
            routes.MapRoute(
                "DashboardPreview",
                "Message/Dashboard/Preview",
                new { controller = this.Name, action = "DashboardPreview" });
            #endregion
        }
    }
}
