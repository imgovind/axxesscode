﻿namespace Axxess.AgencyManagement.SupportApp.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AgencyModule : Module
    {
        public override string Name
        {
            get { return "Agency"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Location
            
            routes.MapRoute(
                "LocationNew",
                "Agency/Location/New",
                new { controller = this.Name, action = "LocationNew", agencyId = new IsGuid() });
            routes.MapRoute(
                "LocationCreate",
                "Agency/Location/Create",
                new { controller = this.Name, action = "LocationCreate" });
            routes.MapRoute(
                "LocationList",
                "Agency/Location/List",
                new { controller = this.Name, action = "LocationList", agencyId = new IsGuid() });
            routes.MapRoute(
                "LocationGrid",
                "Agency/Location/Grid",
                new { controller = this.Name, action = "LocationGrid", agencyId = new IsGuid() });
            routes.MapRoute(
                "LocationStandAloneInfo",
                "Agency/Location/StandAloneInfo",
                new { controller = this.Name, action = "LocationStandAloneInfo", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "LocationEdit",
                "Agency/Location/Edit",
                new { controller = this.Name, action = "LocationEdit", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "LocationUpdate",
                "Agency/Location/Update",
                new { controller = this.Name, action = "LocationUpdate" });
            routes.MapRoute(
                "LocationDelete",
                "Agency/Location/Delete",
                new { controller = this.Name, action = "LocationDelete", id = new IsGuid(), agencyId = new IsGuid() });
            
            #endregion

            #region Physician
           
            routes.MapRoute(
                "PhysicianList",
                "Agency/Physician/List",
                new { controller = this.Name, action = "PhysicianList", agencyId = new IsGuid() });
            routes.MapRoute(
                "PhysicianGrid",
                "Agency/Physician/Grid",
                new { controller = this.Name, action = "PhysicianGrid", agencyId = new IsGuid() });
            routes.MapRoute(
                "PhysicianImpersonate",
                "Agency/Physician/Impersonate/{id}",
                new { controller = this.Name, action = "PhysicianImpersonate", id = new IsGuid() });

            routes.MapRoute(
               "PhysicianLiveImpersonate",
               "Agency/Physician/LiveImpersonate/{id}",
               new { controller = this.Name, action = "PhysicianLiveImpersonate", id = new IsGuid() });


            
            #endregion

            #region User
            routes.MapRoute(
                "UserList",
                "Agency/User/List",
                new { controller = this.Name, action = "UserList", agencyId = new IsGuid() });
            routes.MapRoute(
                "UserGrid",
                "Agency/User/Grid",
                new { controller = this.Name, action = "UserGrid", agencyId = new IsGuid() });
            routes.MapRoute(
                "UserEdit",
                "Agency/User/Edit",
                new { controller = this.Name, action = "UserEdit", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "UserUpdateEmail",
                "Agency/User/UpdateEmail",
                new { controller = this.Name, action = "UserUpdateEmail", id = new IsGuid() });
            routes.MapRoute(
                "UserUpdatePassword",
                "Agency/User/UpdatePassword",
                new { controller = this.Name, action = "UserUpdatePassword", id = new IsGuid() });
            routes.MapRoute(
                "TestImpersonate",
                "Agency/User/TestImpersonate/{agencyId}/{id}",
                new { controller = this.Name, action = "TestImpersonate", id = new IsGuid(), agencyId = new IsGuid() });

            routes.MapRoute(
                "LiveImpersonate",
                "Agency/User/LiveImpersonate/{agencyId}/{id}",
                new { controller = this.Name, action = "LiveImpersonate", id = new IsGuid(), agencyId = new IsGuid() });
            #endregion

            #region Center
           
            routes.MapRoute(
                "CenterContent",
                "Agency/Center/Content",
                new { controller = this.Name, action = "CenterContent", agencyId = new IsGuid() });           
            routes.MapRoute(
                "CenterInfo",
                "Agency/Center/Info",
                new { controller = this.Name, action = "CenterInfo", agencyId = new IsGuid() }); 
            routes.MapRoute(
                "CenterUsersGrid",
                "Agency/Center/UsersGrid",
                new { controller = this.Name, action = "CenterUsersGrid", agencyId = new IsGuid() });
            
            #endregion

            #region Agency

            routes.MapRoute(
                "AgencyExport",
                "Agency/Export",
                new { controller = "Export", action = "Agencies" });
            routes.MapRoute(
                "AgencyLogs",
                "Agency/Logs",
                new { controller = "Agency", action = "AgencyLogs" });
            routes.MapRoute(
               "LocationLogs",
               "Agency/Location/Logs",
               new { controller = "Agency", action = "LocationLogs" });
            routes.MapRoute(
               "AgencyUserLogs",
               "Agency/User/Logs",
               new { controller = "Agency", action = "UserLogs" });

            #endregion

            #region Upgrade
            
            routes.MapRoute(
                "UpgradeList",
                "Agency/Upgrade/List",
                new { controller = this.Name, action = "UpgradeList" });
            routes.MapRoute(
                "UpgradeGrid",
                "Agency/Upgrade/Grid",
                new { controller = this.Name, action = "UpgradeGrid" });
            routes.MapRoute(
                "UpgradeEdit",
                "Agency/Upgrade/Edit",
                new { controller = this.Name, action = "UpgradeEdit", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "UpgradeView",
                "Agency/Upgrade/View",
                new { controller = this.Name, action = "UpgradeView", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "UpgradeUpdate",
                "Agency/Upgrade/Update",
                new { controller = this.Name, action = "UpgradeUpdate" });
            routes.MapRoute(
                "UpgradeExport",
                "Upgrade/Export",
                new { controller = "Export", action = "Upgrade" });

            #endregion

            #region AnnualPlan

            routes.MapRoute(
                "AnnualPlanList",
                "Agency/AnnualPlan/List",
                new { controller = this.Name, action = "AnnualPlanList" });
            routes.MapRoute(
                "AnnualPlanGrid",
                "Agency/AnnualPlan/Grid",
                new { controller = this.Name, action = "AnnualPlanGrid" });
            routes.MapRoute(
                "AnnualPlanEdit",
                "Agency/AnnualPlan/Edit",
                new { controller = this.Name, action = "AnnualPlanEdit", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "AnnualPlanView",
                "Agency/AnnualPlan/View",
                new { controller = this.Name, action = "AnnualPlanView", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "AnnualPlanUpdate",
                "Agency/AnnualPlan/Update",
                new { controller = this.Name, action = "AnnualPlanUpdate" });
            routes.MapRoute(
                "AnnualPlanExport",
                "AnnualPlan/Export",
                new { controller = "Export", action = "AnnualPlan" });

            #endregion

            #region Change

            routes.MapRoute(
               "ChangeList",
               "Agency/Change/List",
               new { controller = this.Name, action = "ChangeList" });
            routes.MapRoute(
                "ChangeGrid",
                "Agency/Change/Grid",
                new { controller = this.Name, action = "ChangeGrid" });
            routes.MapRoute(
               "ChangeView",
               "Agency/Change/View",
               new { controller = this.Name, action = "ChangeView", id = new IsGuid()});
            routes.MapRoute(
               "ChangeExport",
               "Change/Export",
               new { controller = "Export", action = "Change" });
            
            #endregion

            #region Implementation Specialist

            routes.MapRoute(
                "ImplementationSpecialistList",
                "Agency/ImplementationSpecialist/List",
                new { controller = this.Name, action = "ImplementationSpecialistList" });
            routes.MapRoute(
                "ImplementationSpecialistGrid",
                "Agency/ImplementationSpecialist/Grid",
                new { controller = this.Name, action = "ImplementationSpecialistGrid" });
            routes.MapRoute(
                "ImplementationSpecialistEdit",
                "Agency/ImplementationSpecialist/Edit",
                new { controller = this.Name, action = "ImplementationSpecialistEdit", agencyId = new IsGuid() });
            routes.MapRoute(
                "ImplementationSpecialistUpdate",
                "Agency/ImplementationSpecialist/Update",
                new { controller = this.Name, action = "ImplementationSpecialistUpdate" });

            #endregion

            #region MarkAsReadOnly

            routes.MapRoute(
                "MarkAsReadOnlyList",
                "Agency/MarkAsReadOnly/List",
                new { controller = this.Name, action = "MarkAsReadOnlyList" });

            routes.MapRoute(
                "MarkAsReadOnlyGrid",
                "Agency/MarkAsReadOnly/Grid",
                new { controller = this.Name, action = "MarkAsReadOnlyGrid" });
            
            routes.MapRoute(
               "MarkAsReadOnly",
               "MarkAsReadOnly/Export",
               new { controller = "Export", action = "MarkAsReadOnly" });
            
            

            #endregion

        }
    }
}
