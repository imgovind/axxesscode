﻿namespace Axxess.AgencyManagement.SupportApp.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    class CustomerNotesModule : Module
    {
        public override string Name
        {
            get { return "CustomerNotes"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Note
            routes.MapRoute(
                "NoteNew",
                "CustomerNotes/Note/New",
                new { controller = this.Name, action = "NoteNew", agencyId = new IsGuid() });
            routes.MapRoute(
                "NoteCreate",
                "CustomerNotes/Note/Create",
                new { controller = this.Name, action = "NoteCreate" });
            routes.MapRoute(
                "NoteView",
                "CustomerNotes/Note/View",
                new { controller = this.Name, action = "NoteView", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "NoteList",
                "CustomerNotes/Note/List",
                new { controller = this.Name, action = "NoteList", agencyId = new IsGuid() });
            routes.MapRoute(
                "NoteGrid",
                "CustomerNotes/Note/Grid",
                new { controller = this.Name, action = "NoteGrid", agencyId = new IsGuid() });
            routes.MapRoute(
                "NoteEdit",
                "CustomerNotes/Note/Edit",
                new { controller = this.Name, action = "NoteEdit", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "NoteUpdate",
                "CustomerNotes/Note/Update",
                new { controller = this.Name, action = "NoteUpdate" });
            routes.MapRoute(
                "NoteDelete",
                "CustomerNotes/Note/Delete",
                new { controller = this.Name, action = "NoteDelete", id = new IsGuid(), agencyId = new IsGuid() });
            routes.MapRoute(
                "NoteExport",
                "CustomerNotes/Note/Export",
                new { controller = this.Name, action = "NoteExport", agencyId = new IsGuid() });
            #endregion
        }

    }
}
