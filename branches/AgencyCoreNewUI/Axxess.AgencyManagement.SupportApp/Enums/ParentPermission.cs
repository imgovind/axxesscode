﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.SupportApp.Enums
{
   public enum ParentPermission
    {
        [Description("Agency")]
        Agency = 1,
        [Description("User")]
        User = 2,
        [Description("Customer Note")]
        Note = 3,
        [Description("Agency Physicians")]
        Physician = 4,
        [Description("Agency Templates")]
        Template = 5,
        [Description("Agency Supplies")]
        Supply = 6,
        [Description("System Message")]
        SystemMessage = 7,
        [Description("Dashboard Message")]
        DashboardMessage = 8,
        [Description("DDE Credential")]
        DDECredential = 9,
        [Description("Upgrade Requests")]
        UpgradeRequests = 10,
        [Description("Implementation Specialists")]
        ImplementationSpecialist = 11,
        [Description("Sales Agreement")]
        SalesAgreement = 12,
        [Description("HR Management")]
        HRManagement = 13,
        [Description("Marketing Intelligence")]
        MarketingIntelligence = 14,
        [Description("Accounting")]
        Accounting = 15,
        [Description("Raffle")]
        Raffle = 16,
        [Description("Change Requests")]
        ChangeRequests = 17,
        [Description("Permission Templates")]
        PermissionTemplates = 18,
        [Description("New Accounting")]
        NewAccounting = 19
    }
}
