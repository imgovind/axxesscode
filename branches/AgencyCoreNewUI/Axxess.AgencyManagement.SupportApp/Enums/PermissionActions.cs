﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.SupportApp
{
    public enum PermissionActions
    {
        /// <summary>
        /// Common Permission Actions
        /// </summary>
        [Description("Add")]
        Add = 1,
        [Description("Edit")]
        Edit = 2,
        [Description("Delete")]
        Delete = 3,
        [Description("Load")]
        Load = 4,
        [Description("Suspend")]
        Suspend = 5,
        [Description("Export")]
        Export = 6,
        [Description("View List")]
        ViewList = 7,
        [Description("Login Live")]
        LiveLogin = 8,
        [Description("Activate")]
        ActivateLink = 9,
        [Description("View Details")]
        ViewDetail = 10,
        ReadOnly = 11,
        [Description("View Log")]
        ViewLog = 12,
        [Description("Edit Details")]
        EditDetail = 13,
        [Description("Login QA")]
        QALogin = 14,
        [Description("View Agency User")]
        ViewAgencyUser = 15,
        [Description("Edit Agency User")]
        EditAgencyUser = 16,
        [Description("Email To")]
        EmailTo = 17,
        [Description("Reassign")]
        Reassign = 18
    }
}
