﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Exports;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Repositories;
    using System;
    using Axxess.AgencyManagement.SupportApp.Filter;
    using Axxess.AgencyManagement.SupportApp.Enums;
using Axxess.AgencyManagement.SupportApp.Services;
    using Axxess.AgencyManagement.Domain;
    using System.Collections.Generic;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAgencyRepository agencyRepository;
        private readonly IAgencyService agencyService;
        private readonly IAccountingRepository accountingRepository;

        public ExportController(IAgencyManagementDataProvider agencyManagementDataProvider,IAccountingManagementDataProvider accountingManagementDataProvider, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.agencyService = agencyService;
            this.accountingRepository = accountingManagementDataProvider.AccountingRepository;
        }

        #endregion

        #region ExportController Actions

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Agencies(int clusterId, string stateFilter, DateTime? startDate, DateTime? endDate)
        {
            var agencies = agencyService.GetAllAgencies(stateFilter, startDate.HasValue ? startDate.Value : DateTime.MinValue, endDate.HasValue ? endDate.Value : DateTime.MaxValue, clusterId);
            var export = new AgencyExporter(agencies);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Agencies.xls");
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Upgrade(string IsCompleted, DateTime startDate, DateTime endDate)
        {
            var upgradeList = agencyService.GetAgencyUpgrades(IsCompleted, startDate, endDate, 0) ?? new List<AgencyUpgrade>();
            //var clusterIds = new int[] { 1, 2 };
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            //        var List = agencyRepository.GetAgencyUpgrades(IsCompleted, startDate, endDate, clusterId);
            //        if (List != null && List.Count > 0)
            //        {
            //            UpgradeList.AddRange(List);
            //        }
            //    }
            //}
            var export = new UpgradeExporter(upgradeList.ToList(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AgencyUpgrade_{0}.xls", DateTime.Now.Ticks));
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AnnualPlan(string IsCompleted, DateTime startDate, DateTime endDate)
        {
            //var annualPlanList = new List<AgencyUpgrade>();
            //var clusterIds = new int[] { 1, 2 };
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            //        var List = agencyRepository.GetAgencyAnnualPlan(IsCompleted, startDate, endDate, clusterId);
            //        if (List != null && List.Count > 0)
            //        {
            //            annualPlanList.AddRange(List);
            //        }
            //    }
            //}
            var export = new AnnualPlanExporter(agencyService.GetAgencyAnnualPlan(IsCompleted, startDate, endDate, 0) ?? new List<AgencyUpgrade>(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AgencyAnnualPlan_{0}.xls", DateTime.Now.Ticks));
        }

        [PermissionFilter(ParentPermission.ChangeRequests, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Change(string IsComplete, DateTime startDate, DateTime endDate)
        {
            var changeList = accountingRepository.GetAgencyChanges(IsComplete, startDate, endDate) ?? new List<AgencyChange>();
            //var clusterIds = new int[] { 1, 2 };
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            //        var List = accou.GetAgencyChanges(IsComplete, startDate, endDate);
            //        if (List != null && List.Count > 0)
            //        {
            //            ChangeList.AddRange(List);
            //        }
            //    }
            //}
            var export = new ChangeExporter(changeList.ToList(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AgencyChange_{0}.xls", DateTime.Now.Ticks));
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MarkAsReadOnly(DateTime startDate, DateTime endDate)
        {
            var MarkAsReadOnlyList = agencyService.GetAgencyMarkAsReadOnly(startDate, endDate) ?? new List<AgencyMarkAsReadOnly>();
            //var clusterIds = new int[] { 1, 2 };
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            //        var List = agencyRepository.GetAgencyMarkAsReadOnly(startDate, endDate, clusterId);
            //        if (List != null && List.Count > 0)
            //        {
            //            MarkAsReadOnlyList.AddRange(List);
            //        }
            //    }
            //}
            var export = new MarkAsReadOnlyExporter(MarkAsReadOnlyList.ToList(), startDate, endDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AgencyUpgrade_{0}.xls", DateTime.Now.Ticks));
        }
        #endregion
    }
}
