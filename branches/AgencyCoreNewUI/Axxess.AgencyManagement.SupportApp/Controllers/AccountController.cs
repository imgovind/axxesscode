﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Diagnostics;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Security;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Enums;
    using System.Collections.Generic;
    using System.Web.Routing;

    [Compress]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountController : BaseController
    {
        #region Constructor

        public AccountController()
            : this(null, null)
        {
        }

        public AccountController(IFormsAuthenticationService formsAuthentication, ISupportMembershipService membershipService)
        {
            this.MembershipService = membershipService;
            this.AuthenticationService = formsAuthentication;
        }

        #endregion

        #region Properties

        public ISupportMembershipService MembershipService
        {
            get;
            private set;
        }

        public IFormsAuthenticationService AuthenticationService
        {
            get;
            private set;
        }

        #endregion

        #region AccountController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Activate()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["loginid"] != null)
            {
                var supportRepository = Container.Resolve<IMembershipDataProvider>().SupportRepository;
                var login = supportRepository.Find(HttpContext.Request.QueryString["loginid"].ToGuid());
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.Name = login.DisplayName;
                    account.EmailAddress = login.EmailAddress;
                }
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Activate([Bind] Account account)
        {
            var viewData = Validate<AccountViewData>(
                            new Validation(() => string.IsNullOrEmpty(account.Password), "You must specify a password. <br/>"),
                            new Validation(() => account.Password.Length < 8, "The minimum password length is 8 characters.")
                            );

            if (viewData.isSuccessful)
            {
                if (this.MembershipService.Activate(account))
                {
                    this.AuthenticationService.SignIn(account.UserName, false);
                    viewData.redirectUrl = "/";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Account Activation failed. Please try again.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOn()
        {
            var logon = new Logon();

            if ((HttpContext.Request.Cookies.Count > 0) && (HttpContext.Request.Cookies[SupportAppSettings.RememberMeCookie] != null))
            {
                var cookie = HttpContext.Request.Cookies[SupportAppSettings.RememberMeCookie];
                logon.UserName = Crypto.Decrypt(cookie.Value);
            }

            return View(logon);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public JsonResult LogOn([Bind] Logon logon)
        {
            logon.UserName = logon.UserName.IsNotNullOrEmpty() ? logon.UserName.Trim() : string.Empty;
            logon.Password = logon.Password.IsNotNullOrEmpty() ? logon.Password.Trim() : string.Empty;
            var viewData = Validate<AccountViewData>(
                             new Validation(() => !logon.UserName.IsNotNullOrEmpty(), "You must specify a username. <br/>"),
                             new Validation(() => logon.UserName.IsNotNullOrEmpty() ? !logon.UserName.IsEmail() : false, "E-mail Address is not valid.<br/>"),
                             new Validation(() => logon.UserName.IsEmail() ? !CoreSettings.ValidDomains.Any(d => logon.UserName.ToLowerCase().EndsWith(d)) : false, "Invalid login Information. <br/>"),
                             new Validation(() => !logon.Password.IsNotNullOrEmpty(), "You must specify a password.  <br/>")

                        );
            if (viewData.isSuccessful)
            {
                LoginAttemptType loginAttempt = LoginAttemptType.Failed;
                if (LoginMonitor.Instance.IsUserLocked(logon.UserName, Current.IpAddress))
                {
                    loginAttempt = LoginAttemptType.TooManyAttempts;
                }
                else
                {
                    loginAttempt = MembershipService.Validate(logon.UserName.Trim(), logon.Password.Trim());
                }

                switch (loginAttempt)
                {
                    case LoginAttemptType.Success:
                        this.AuthenticationService.SignIn(logon.UserName.Trim(), logon.RememberMe);
                        viewData.redirectUrl = "/";
                        break;
                    case LoginAttemptType.TooManyAttempts:
                        viewData.isLocked = true;
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "You exceeded the maximum number of login attempts. Please note that your online access to Support will be disabled for 5 minutes.";
                        break;
                    default:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Username / Password combination failed.";
                        break;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOff()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(Current.User.Name);
                this.AuthenticationService.SignOut();
            }

            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ResendLink(Guid userId, Guid agencyId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Activation Link could not be resent." };

            if (MembershipService.ResendActivationLink(userId, agencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Activation Link has been successfully sent to the user";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PhysicianActivateLink(Guid physicianId, Guid agencyId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Activation Link could not be resent." };

            if (MembershipService.ResendPhysicianActivationLink(physicianId, agencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Activation Link has been successfully sent to the user";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SessionExpired()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult WindowDirect()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                var viewData = new AccountViewData();
                var req = Request.Url.Query;
                var rvd = new RouteValueDictionary();
                rvd.Add(req.Replace("?", "").Split('=')[0], req.Split('=')[1]);
                return RedirectToAction("Index", "Home", rvd);
            }

            var query = string.Format("{0}={1}",Request.QueryString.Keys.Get(0),Request.QueryString.Get(0));
            var logon = new Logon();
            if ((HttpContext.Request.Cookies.Count > 0) && (HttpContext.Request.Cookies[SupportAppSettings.RememberMeCookie] != null))
            {
                var cookie = HttpContext.Request.Cookies[SupportAppSettings.RememberMeCookie];
                logon.UserName = Crypto.Decrypt(cookie.Value);
                logon.ReturnUrl = query;
            }
            return View("Logon", logon);


            
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WindowDirect([Bind] Logon logon)
        {
            var viewData = Validate<AccountViewData>(
                    new Validation(() => string.IsNullOrEmpty(logon.UserName.Trim()), "You must specify a username. <br/>"),
                    new Validation(() => string.IsNullOrEmpty(logon.Password.Trim()), "You must specify a password. <br/>"),
                    new Validation(() => !logon.UserName.Trim().IsEmail(), "E-mail Address is not valid. <br/>")
                );
            if (viewData.isSuccessful)
            {
                LoginAttemptType loginAttempt = LoginAttemptType.Failed;
                if (LoginMonitor.Instance.IsUserLocked(logon.UserName, Current.IpAddress))
                {
                    loginAttempt = LoginAttemptType.TooManyAttempts;
                }
                else
                {
                    loginAttempt = MembershipService.Validate(logon.UserName.Trim(), logon.Password.Trim());
                }

                switch (loginAttempt)
                {
                    case LoginAttemptType.TooManyAttempts:
                        viewData.isLocked = true;
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "You exceeded the maximum number of login attempts. Please note that your online access to Support will be disabled for 5 minutes.";
                        break;
                    case LoginAttemptType.Success:
                        this.AuthenticationService.SignIn(logon.UserName.Trim(), logon.RememberMe);
                        var request = Request["returnUrl"];
                        viewData.redirectUrl = "/?enc=" + Crypto.Encrypt(request);
                        break;
                    default:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Username / Password combination failed.";
                        break;
                }
            }
            return Json(viewData);
        }
        #endregion
    }

}
