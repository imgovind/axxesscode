﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Web.Mvc;
    
    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.Log.Common;
    using Axxess.Log.Enums;

    using Axxess.AgencyManagement.SupportApp.Services.Interfaces;

    using Axxess.Api;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class DdeCredentialController : BaseController
    {
        #region Private Members / Constructor

        private readonly ILoginService loginService;
        private readonly ISupportRepository supportRepository;
        private static AbilityNetworkAgent abilityAgent = new AbilityNetworkAgent();

        public DdeCredentialController(IMembershipDataProvider membershipDataProvider, ILoginService loginService)
        {
            Check.Argument.IsNotNull(loginService, "loginService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.loginService = loginService;
            this.supportRepository = membershipDataProvider.SupportRepository;
        }

        #endregion

        #region Ability Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind] DdeCredential ddeCredential)
        {
            Check.Argument.IsNotNull(ddeCredential, "ddeCredential");
            var viewData = new JsonViewData();
            //if (loginRepository.Find(ddeCredential.EmailAddress) == null)
            //{
            //    ddeCredential.Role = Roles.AxxessAdmin.ToString();
            //    ddeCredential.IsActive = true;
            //    ddeCredential.IsLocked = false;
            //    if (!loginRepository.Add(ddeCredential))
            //    {
            //        viewData.isSuccessful = false;
            //        viewData.errorMessage = "Error in adding the login.";
            //    }
            //    else
            //    {
            //        Auditor.AddSupportLog(Current.LoginId, LogDomain.SupportUser, ddeCredential.Id, ddeCredential.Id.ToString(), LogType.Login, LogAction.UserAdded, "");
            //        var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}", ddeCredential.Id)));
            //        var bodyText = MessageBuilder.PrepareTextFrom(
            //            "NewSupportUserConfirmation",
            //            "firstname", ddeCredential.DisplayName,
            //            "encryptedQueryString", encryptedQueryString);
            //        Notify.User(CoreSettings.NoReplyEmail, ddeCredential.EmailAddress, "Welcome to Axxess Support", bodyText);
            //        viewData.isSuccessful = true;
            //        viewData.errorMessage = "New Login was added successfully";
            //    }
            //}
            //else
            //{
            //    viewData.isSuccessful = false;
            //    viewData.errorMessage = "The Login e-mail address provided is already in use.";
            //}
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView(supportRepository.GetDdeCredential(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] DdeCredential ddeCredential)
        {
            Check.Argument.IsNotNull(ddeCredential, "DDE Credential");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error in updating the DDE Credentials." };

            var existing = supportRepository.GetDdeCredential(ddeCredential.Id);
            if (existing != null)
            {
                if (abilityAgent.ChangePasswordRequest(existing.UserId, existing.Password, ddeCredential.Password))
                {
                    var details = string.Format("Changed from {0} to {1}.", existing.Password, ddeCredential.Password);
                    existing.Password = ddeCredential.Password;
                    existing.IsDeprecated = ddeCredential.IsDeprecated;
                    if (supportRepository.UpdateDdeCredential(existing))
                    {
                        Auditor.AddSupportLog(Current.LoginId, LogDomain.SupportUser, ddeCredential.Id, ddeCredential.Id.ToString(), LogType.DDECredential, LogAction.DDECredentialUpdated, details);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "DDE Credential was updated successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "DDE Credential could not be changed by Ability Network";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid()
        {
            return View(new GridModel(supportRepository.GetDdeCredentials()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Logs(Guid id)
        {
            var ddeCredentialInformation = loginService.GetSupportLogs(LogDomain.SupportUser, LogType.DDECredential, id, id.ToString());
            return PartialView("ActivityLogs", ddeCredentialInformation);
        }

        #endregion
    }
}
