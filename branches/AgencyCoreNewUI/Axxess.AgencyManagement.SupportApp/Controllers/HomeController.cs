﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System.Web.Mvc;
    using System.Linq;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Repositories;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.SupportApp.Enums;
    using Axxess.AgencyManagement.SupportApp.ViewData;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : BaseController
    {
        #region Private Members/Constructor

        private readonly ISupportRepository supportRepository;
        private readonly IErrorRepository errorRepository;

        public HomeController(IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.supportRepository = membershipDataProvider.SupportRepository;
            this.errorRepository = membershipDataProvider.ErrorRepository;
        }

        #endregion

        #region HomeController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            var menus = MenuHelper();
            return View("Index", menus);
        }


        private MenuViewData MenuHelper()
        {
            var viewData = new MenuViewData();
            var menus = new List<MenuElement>();
            var menuList = supportRepository.GetMenuElements();
            
            var datas = Current.Permissions ?? new Dictionary<int, List<int>>();
            foreach (var category in datas)
            {
                var actions = category.Value;
                if (actions != null && actions.Count > 0)
                {
                    var menuCategoryList = menuList.Where(m => m.Category == category.Key).ToList();
                    if (menuCategoryList != null && menuCategoryList.Count > 0)
                    {
                        menuCategoryList.ForEach(m =>
                        {
                            if (actions.Contains(m.Action))
                            {
                                menus.Add(new MenuElement
                                {
                                    Id = m.Id,
                                    Name = m.Name,
                                    MenuName = m.MenuName,
                                    Url = m.Url,
                                    OnLoad = m.OnLoad,
                                    StyleOptions = m.StyleOptions,
                                    ParentMenus = m.ParentMenus
                                });
                            }
                        });
                    }
                }
            }
            viewData.MenuElement = menus.OrderBy(m=>m.MenuName).ToList();
            foreach (var menu in menus)
            {

                if (menu.ParentMenus == "Create") { viewData.IsNewExist = true; }
                if (menu.ParentMenus == "List") { viewData.IsListExist = true; }
                if (menu.ParentMenus == "View") { viewData.IsViewExist = true; }
                if (menu.ParentMenus == "Axxess") { viewData.IsAxxessExist = true; }
            }
            return viewData;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Error()
        {
            return View("~/Views/Shared/Error.aspx");
        }

        #endregion
    }
}
