﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.SupportApp;
    using Axxess.AgencyManagement.SupportApp.Exports;
    using Axxess.AgencyManagement.SupportApp.ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;
    using Axxess.Log.Common;
    using Axxess.Log.Domain;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Redmine.Net.Api;
    
    using Security;
    using Services;
    
    using Telerik.Web.Mvc;
    using Axxess.Membership.Domain;
    using System.Web.Script.Serialization;
    using Axxess.AgencyManagement.SupportApp.Filter;
    using Axxess.AgencyManagement.SupportApp.Enums;
    
    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IUserRepository userRepository;
        private readonly ISupportRepository supportRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ICustomerNotesRepository customerNotesRepository;
        private readonly RedmineManager redmineManager;
        private readonly IAccountingRepository accountingRepository;

        private ISupportMembershipService membershipService = Container.Resolve<ISupportMembershipService>();

        public AgencyController(ISupportManagementDataProvider supportManagementDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IAgencyManagementMembershipDataProvider agencyManagementMembershipDataProvider, IMembershipDataProvider membershipDataProvider, IAgencyService agencyService, IUserService userService, IAccountingManagementDataProvider accountingManagementDataProvider)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
            Check.Argument.IsNotNull(accountingManagementDataProvider, "accountingManagementDataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.assetRepository = supportManagementDataProvider.AssetRepository;
            this.customerNotesRepository = supportManagementDataProvider.CustomerNotesRepository;
            this.loginRepository = agencyManagementMembershipDataProvider.LoginRepository;
            this.accountingRepository = accountingManagementDataProvider.AccountingRepository;
            redmineManager = new RedmineManager(CoreSettings.RedmineHost, CoreSettings.RedmineAPIKey, false);
        }

        #endregion

        #region CRUD

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New()
        {
            return PartialView();
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind] Agency agency, [ModelBinder(typeof(CommaSeparatedModelBinder))]List<int> ServiceArray)
        {
            Check.Argument.IsNotNull(agency, "agency");
            var viewData = new JsonViewData();
            if (ServiceArray != null && ServiceArray.Count > 0)
            {
                agency.Services = (AgencyServices)ServiceArray.Sum();
            }
            if (agency.IsValid)
            {
                var error = string.Empty;
                agency.ClusterId = CoreSettings.CurrentClusterId;
                if (!agencyService.CreateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, agency.Id, agency.Id.ToString(), LogType.Agency, LogAction.AgencyAdded, "");
                    string responseText = "";
                    var esignId = Request.Form["ESign"];
                    if (esignId != "0")
                    {
                        var displayName = supportRepository.GetLoginDisplayName(Current.LoginId);
                        displayName = displayName.Replace(" ", "%20");
                        var url = string.Format(CoreSettings.ESignAPI + "{0}/{1}/{2}/{3}", Session.SessionID, esignId, displayName, agency.Id);
                        var client = new WebClient();
                        using (var data = client.OpenRead(url))
                        {
                            using (var reader = new StreamReader(data))
                            {
                                responseText = reader.ReadToEnd();
                            }
                        }
                    }

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List()
        {
            var permission = new InternalUserPermissionViewData();
            var agencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (agencyPermissions != null && agencyPermissions.Count > 0)
            {
                permission.IsUserCanAdd = agencyPermissions.Contains((int)PermissionActions.Add);
                permission.IsUserCanEdit = agencyPermissions.Contains((int)PermissionActions.Edit);
                permission.IsUserCanViewAgencyUser = agencyPermissions.Contains((int)PermissionActions.ViewAgencyUser);
                permission.IsUserCanExport = agencyPermissions.Contains((int)PermissionActions.Export);
                permission.IsUserCanViewLocation = agencyPermissions.Contains((int)PermissionActions.ViewList);
                permission.IsUserCanSuspend = agencyPermissions.Contains((int)PermissionActions.Suspend);
                permission.IsUserCanReadOnly = agencyPermissions.Contains((int)PermissionActions.ReadOnly);
                permission.IsUserCanEmailToContact = agencyPermissions.Contains((int)PermissionActions.EmailTo);
            }
            var templatePermissions = Current.CategoryPermissions(ParentPermission.Template);
            if (templatePermissions != null && templatePermissions.Count > 0)
            {
                permission.IsUserCanLoadTemplate = templatePermissions.Contains((int)PermissionActions.Load);
            }
            var supplyPermissions = Current.CategoryPermissions(ParentPermission.Supply);
            if (supplyPermissions != null && supplyPermissions.Count > 0)
            {
                permission.IsUserCanLoadSupply = supplyPermissions.Contains((int)PermissionActions.Load);
            }
            var physicianPermissions = Current.CategoryPermissions(ParentPermission.Physician);
            if (physicianPermissions != null && physicianPermissions.Count > 0)
            {
                permission.IsUserCanViewPhysician = physicianPermissions.Contains((int)PermissionActions.ViewList);
            }
            var notePermissions = Current.CategoryPermissions(ParentPermission.Note);
            if (notePermissions != null && notePermissions.Count > 0)
            {
                permission.IsUserCanViewNote = notePermissions.Contains((int)PermissionActions.ViewList);
            }
            return PartialView(permission);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(int? clusterId, string stateFilter, DateTime? startDate, DateTime? endDate)
        {
            var agencies = agencyService.GetAllAgencies(stateFilter, startDate.HasValue ? startDate.Value : DateTime.MinValue, endDate.HasValue ? endDate.Value : DateTime.MaxValue, clusterId.HasValue ? clusterId.Value : 0) ?? new List<AgencyLite>();
            return View(new GridModel(agencies));
        }


        [PermissionFilter(ParentPermission.Agency, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            var viewData = new AgencyCenterViewData();
            var agencySnapShot = loginRepository.GetAgencySnapshot(id);
            if (agencySnapShot != null)
            {
                var agency = agencyRepository.GetAgencyOnly(id, agencySnapShot.ClusterId);
                if (agency != null)
                {
                    var primaryUser = userRepository.GetPrimaryUser(id, agencySnapShot.ClusterId);
                    agency.PrimaryUser = primaryUser != null ? primaryUser.Id : Guid.Empty;
                    agency.Trainer = agencySnapShot.Trainer;
                    agency.BackupTrainer = agencySnapShot.BackupTrainer;
                    agency.SalesPerson = agencySnapShot.SalesPerson;
                    agency.ImplementationSpecialist = agencySnapShot.ImplementationSpecialist;
                    viewData.Agency = agency;
                }
                var agencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
                if (agencyPermissions != null && agencyPermissions.Count > 0)
                {
                    viewData.IsUserCanViewLog = agencyPermissions.Contains((int)PermissionActions.ViewLog);
                }
            }
            return PartialView(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Agency agency, [ModelBinder(typeof(CommaSeparatedModelBinder))]List<int> ServiceArray)
        {
            Check.Argument.IsNotNull(agency, "agency");
            var viewData = new JsonViewData();
            if (ServiceArray != null && ServiceArray.Count > 0)
            {
                agency.Services = (AgencyServices)ServiceArray.Sum();
            }
            if (agency.IsValid)
            {
                if (!agencyService.UpdateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, agency.Id, agency.Id.ToString(), LogType.AgencyInformation, LogAction.AgencyUpdated, "");
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Suspend)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ToggleDelete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not complete this action for this agency." };
            var agency = loginRepository.GetAgencySnapshot(id);
            if (agency != null)
            {
                if (agencyRepository.ToggleDelete(id, DateTime.Now, agency.ClusterId))
                {
                    if (loginRepository.ToggleDelete(id))
                    {
                        accountingRepository.ToggleAccuntAgency(id);
                        //if (agencyRepository.GetAgencyOnly(id).IsSuspended)
                        //{
                        Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, id, id.ToString(), LogType.Agency, LogAction.AgencySuspended, "");
                        //}
                        //else
                        //{
                        //    Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, id, id.ToString(), LogType.Agency, LogAction.AgencyRestored, "");
                        //}
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Agency Information has been updated.";
                    }
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ReadOnly)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditReadOnly(Guid id)
        {
            //var agency = agencyRepository.Get(id);
            var agencySnapShot = loginRepository.GetAgencySnapshot(id) ?? new AgencySnapshot { Id = id };
            return PartialView("ReadOnly", agencySnapShot);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ReadOnly)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkReadOnly(Guid id, DateTime readOnlyDate, string frozenReason, bool isSuspendPayment)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not complete this action for this agency." };
            //var agencySnapShot = loginRepository.GetAgencySnapshot(id);
            var account = loginRepository.GetReadOnlyAcccountId(id);
            var modified = DateTime.Now;
            if (account != null && account.ClusterId > 0)
            {
                if (agencyRepository.ToggleReadOnly(id, readOnlyDate, !account.IsFrozen, modified, account.ClusterId))
                {
                    if (loginRepository.ToggleReadOnly(id, readOnlyDate, !account.IsFrozen))
                    {
                        var assetId = agencyService.AddFrozenAsset(id, Request.Files);
                        if (accountingRepository.AddAgencyProfile(id, isSuspendPayment, readOnlyDate, frozenReason, assetId))
                        {
                            if (account.Account.IsNotNullOrEmpty())
                            {
                                viewData.isSuccessful = accountingRepository.UpdateReadOnlyInformation(account.Account, frozenReason, 0);
                                if (isSuspendPayment)
                                {
                                    decimal amount = (decimal)(0.00);
                                    var lastPayment = accountingRepository.GetLastPayments(account.Account);
                                    if (accountingRepository.UpdateReadOnlyLastPayment(lastPayment.Id, readOnlyDate.AddDays(-1)) && accountingRepository.AddAccountingpayments(account.Account, lastPayment.Sequence + 1, readOnlyDate, amount, lastPayment.Package))
                                    {
                                        viewData.isSuccessful = true;
                                    }
                                }
                            }
                            if (!account.IsFrozen)
                            {
                                Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, id, id.ToString(), LogType.Agency, LogAction.AgencyFrozen, "");
                            }
                            else
                            {
                                Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, id, id.ToString(), LogType.Agency, LogAction.AgencyUnfrozen, "");
                            }
                            if (viewData.isSuccessful)
                            {
                                viewData.errorMessage = " Agency information and accounting information have been updated.";
                            }
                            else
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = " Agency information has been updated. But accounting information hasn't been updated.";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ReadOnly)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UndoReadOnly(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not complete this action for this agency." };
            //var agencySnapShot = loginRepository.GetAgencySnapshot(id);
            var account = loginRepository.GetReadOnlyAcccountId(id);
            if (account != null && account.ClusterId > 0)
            {
                var modified = DateTime.Now;
                if (agencyRepository.ToggleReadOnly(id, DateTime.MinValue, !account.IsFrozen, modified, account.ClusterId))
                {
                    if (loginRepository.ToggleReadOnly(id, DateTime.MinValue, !account.IsFrozen))
                    {
                        if (account.Account.IsNotNullOrEmpty())
                        {
                            viewData.isSuccessful = accountingRepository.UpdateUndoReadOnlyInformation(account.Account, 1);
                        }

                        if (!account.IsFrozen)
                        {
                            Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, id, id.ToString(), LogType.Agency, LogAction.AgencyFrozen, "");
                        }
                        else
                        {
                            Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, id, id.ToString(), LogType.Agency, LogAction.AgencyUnfrozen, "");
                        }
                        if (viewData.isSuccessful)
                        {
                            viewData.errorMessage = "Agency information and accounting information have been updated.";
                        }
                        else
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Agency information has been updated. But accounting information hasn't been updated.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(Guid id)
        {
            var agency = new Agency();
            var agencySnapShot = loginRepository.GetAgencySnapshot(id);
            if (agencySnapShot != null)
            {
                agency = agencyRepository.GetAgencyOnly(id, agencySnapShot.ClusterId);
                if (agency != null)
                {
                    agency.Trainer = agencySnapShot.Trainer;
                    agency.BackupTrainer = agencySnapShot.BackupTrainer;
                    agency.SalesPerson = agencySnapShot.SalesPerson;
                    agency.ImplementationSpecialist = agencySnapShot.ImplementationSpecialist;
                }
            }
            return PartialView(agency);
        }
        
        #endregion

        #region Location

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationNew(Guid agencyId)
        {
            var locationNew = loginRepository.GetAgencySnapshot(agencyId);
            return PartialView("Location/New", locationNew);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationCreate([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");
            var viewData = new JsonViewData();
            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, location.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, "");
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationList(Guid agencyId)
        {
            var viewData = new AgencyCenterViewData();
            viewData.AgencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                viewData.IsUserCanAdd = categoryPermissions.Contains((int)PermissionActions.Add);
                viewData.IsUserCanEdit = categoryPermissions.Contains((int)PermissionActions.Edit);
                viewData.IsUserCanViewLog = categoryPermissions.Contains((int)PermissionActions.ViewLog);
            }
            return PartialView("Location/List", viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationGrid(Guid agencyId)
        {
            return View(new GridModel(agencyService.GetBranches(agencyId)));
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationEdit(Guid id, Guid agencyId)
        {
            var viewData = new AgencyCenterViewData();
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapShot != null && agencySnapShot.ClusterId > 0)
            {
                viewData.AgencyLocation = agencyRepository.FindLocation(agencyId, id, agencySnapShot.ClusterId);
                viewData.SubscriptionPlan = accountingRepository.GetSubscriptionPlan(agencyId, id);
                var categoryPermissions = Current.CategoryPermissions(ParentPermission.Agency);
                if (categoryPermissions != null && categoryPermissions.Count > 0)
                {
                    viewData.IsUserCanViewLog = categoryPermissions.Contains((int)PermissionActions.ViewLog);
                }
            }
            return PartialView("Location/Edit", viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationUpdate([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");
            var viewData = new JsonViewData();
            if (location.IsValid)
            {
                if (!agencyService.UpdateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the location.";
                }
                else
                {
                    Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, location.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, "");
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was edited successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationStandAloneInfo(Guid id, Guid agencyId)
        {
            var agencyLocation = new AgencyLocation();
           
            if (!id.IsEmpty() && !agencyId.IsEmpty())
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapShot != null && agencySnapShot.ClusterId > 0)
                {
                    agencyLocation = agencyRepository.FindLocation(agencyId, id, agencySnapShot.ClusterId) ?? new AgencyLocation();
                }
            }
            return PartialView("Location/StandAloneInfo", agencyLocation);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationLogs(Guid id, Guid agencyId)
        {
            var agencyInformation = agencyService.GetSupportLogs(LogDomain.Agency, LogType.AgencyLocation, agencyId, id.ToString());
            return PartialView("ActivityLogs", agencyInformation);
        }
        
        #endregion

        #region Physician

        [PermissionFilter(ParentPermission.Physician, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianList(Guid agencyId)
        {
            var viewData = new AgencyCenterViewData();
            viewData.AgencySnapshot = loginRepository.GetAgencySnapshot(agencyId) ?? new AgencySnapshot();
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.Physician);
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                viewData.IsUserCanLoginAsPhysician = categoryPermissions.Contains((int)PermissionActions.LiveLogin);
                viewData.IsUserCanActivateLinkPhysician = categoryPermissions.Contains((int)PermissionActions.ActivateLink);
                viewData.IsUserCanEmailToPhysician = categoryPermissions.Contains((int)PermissionActions.EmailTo);
            }

            return PartialView("Physician/List", viewData);
        }

        [PermissionFilter(ParentPermission.Physician, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianGrid(Guid agencyId)
        {
            IList<PhysicianSelection> physicianList = new List<PhysicianSelection>();
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            var physicians = physicianRepository.GetAgencyPhysicians(agencyId, agencySnapshot.ClusterId);
            physicians.ForEach(p =>
            {
                if (!p.LoginId.IsEmpty() && p.PhysicianAccess)
                {
                    var login = loginRepository.Find(p.LoginId);
                    if (login != null)
                    {
                        physicianList.Add(new PhysicianSelection
                        {
                            Id = p.Id,
                            LoginId = login.Id,
                            LastName = p.LastName,
                            FirstName = p.FirstName,
                            Credentials = p.Credentials,
                            DisplayName = p.IsDeprecated ? p.DisplayName + " [Deprecated]" : p.DisplayName,
                            EmailAddress = login.EmailAddress,
                            IsLoginActive = login.IsActive
                        });
                    }
                }
            });
            return View(new GridModel(physicianList.OrderBy(u => u.DisplayName)));
        }

        [PermissionFilter(ParentPermission.Physician, PermissionActions.LiveLogin)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianImpersonate(Guid id)
        {
            var url = membershipService.GetImpersonationPhysicianUrl(id);
            if (url.IsNotNullOrEmpty()) return Redirect(url);
            return RedirectToAction("Index", "Home");
        }


        [PermissionFilter(ParentPermission.Physician, PermissionActions.LiveLogin)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianLiveImpersonate(Guid id)
        {
            var url = membershipService.GetLiveImpersonationPhysicianUrl(id);
            if (url.IsNotNullOrEmpty()) return Redirect(url);
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region User

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewAgencyUser)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserList(Guid agencyId)
        {
            
            var viewData = new AgencyCenterViewData();
            viewData.AgencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            var CategoryPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (CategoryPermissions != null && CategoryPermissions.Count > 0)
            {
                viewData.IsUserCanEditAgencyUser = CategoryPermissions.Contains((int)PermissionActions.EditAgencyUser);
                viewData.IsUserCanQALoginAsUser = CategoryPermissions.Contains((int)PermissionActions.QALogin);
                viewData.IsUserCanLiveLoginAsUser = CategoryPermissions.Contains((int)PermissionActions.LiveLogin);
                viewData.IsUserCanSendActiveLink = CategoryPermissions.Contains((int)PermissionActions.ActivateLink);
            }
            return PartialView("Users/List", viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewAgencyUser)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserGrid(Guid agencyId)
        {
            var users = userService.GetUsersWithLoginInfo(agencyId);

            if (users != null && users.Count > 0)
            {
                users = users.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
            }
            //if (users != null)
            //{
            //    users.ForEach(u =>
            //    {
            //       // var login = loginRepository.Find(u.LoginId);
            //        if (login != null)
            //        {
            //            userList.Add(new UserSelection
            //            {
            //                Id = u.Id,
            //                LoginId = login.Id,
            //                LastName = u.LastName,
            //                FirstName = u.FirstName,
            //                Credential = u.DisplayTitle,
            //                DisplayName = u.IsDeprecated ? u.DisplayName + " [Deprecated]" : u.DisplayName,
            //                EmailAddress = login.EmailAddress,
            //                LoginCreated = login.CreatedFormatted,
            //                IsLoginActive = login.IsActive
            //            });
            //        }
            //    });
            //}

            return View(new GridModel(users));
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.EditAgencyUser)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserEdit(Guid id)
        {
            var viewData = new AgencyUserViewData();
            viewData.Login = loginRepository.Find(id);
            var CategoryPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (CategoryPermissions != null && CategoryPermissions.Count > 0)
            {
                viewData.IsUserCanViewLog = CategoryPermissions.Contains((int)PermissionActions.ViewLog);
            }

            return PartialView("Users/Edit", viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.EditAgencyUser)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserUpdateEmail(Guid id, string EmailAddress, string DisplayName)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Information could not be saved" };
            if (EmailAddress.IsNotNullOrEmpty() && EmailAddress.IsEmail())
            {
                
                    var login = loginRepository.Find(id);
                    if (login != null)
                    {

                        if (login.EmailAddress.IsEqual(EmailAddress))
                        {
                            login.DisplayName = DisplayName;
                            if (loginRepository.Update(login))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User information was saved successfully";
                            }
                        }
                        else
                        {
                            if (!userService.IsEmailAddressInUse(EmailAddress.Trim()))
                            {
                                login.DisplayName = DisplayName;
                                login.EmailAddress = EmailAddress;
                                if (!loginRepository.Update(login))
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Error in saving the User information.";
                                }
                                else
                                {
                                    //var userSnapshot = loginRepository.GetUserSnapshotByLoginId(login.Id);
                                    // var agencySnapshot = agencyRepository.GetAgencySnapshot(userSnapshot.AgencyId);
                                   
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User information was saved successfully";
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "This e-mail address is already in use.";
                            }

                            if (viewData.isSuccessful)
                            {
                                var users = loginRepository.GetUserSnapshotByLoginIdWithClusterid(login.Id);
                                if (users != null && users.Count > 0)
                                {
                                    users.ForEach(user =>
                                    {
                                        if (user.ClusterId > 0)
                                        {
                                            var userData = userRepository.GetUserProfileData(user.AgencyId, user.Id, user.ClusterId);
                                            if (userData != null && userData.ProfileData.IsNotNullOrEmpty())
                                            {
                                                userData.Profile = userData.ProfileData.ToObject<UserProfile>();
                                                if (userData.Profile != null)
                                                {
                                                    userData.Profile.EmailWork = EmailAddress.Trim();
                                                    userData.ProfileData = userData.Profile.ToXml();
                                                    userRepository.UpdateUserProfileData(user.AgencyId, user.Id, userData.ProfileData, user.ClusterId);
                                                    Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, user.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserEdited, "User email/Display name updated.");
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
               
                  
                       
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "User Information was not found.";
                    }
                
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "No valid E-mail Address provided.";
            }

            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.EditAgencyUser)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserUpdatePassword(Guid id, string Password, string Signature)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Password/Signature could not be saved" };
            var login = loginRepository.Find(id);
            if (login != null)
            {
                var passSalt = string.Empty;
                var passHash = string.Empty;
                var sigSalt = string.Empty;
                var sigHash = string.Empty;
                var saltedHash = new SaltedHash();
                if (Password.IsNotNullOrEmpty())
                {
                    saltedHash.GetHashAndSalt(Password, out passHash, out passSalt);
                    login.PasswordHash = passHash;
                    login.PasswordSalt = passSalt;
                }
                if (Signature.IsNotNullOrEmpty())
                {
                    saltedHash.GetHashAndSalt(Signature, out sigHash, out sigSalt);
                    login.SignatureHash = sigHash;
                    login.SignatureSalt = sigSalt;
                }
                if (!loginRepository.Update(login))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the password or signature.";
                }
                else
                {
                   // var userSnapshot = userRepository.GetUserSnapshotByLoginId(login.Id);
                   // var agencySnapshot = agencyRepository.GetAgencySnapshot(userSnapshot.AgencyId);
                    var users = loginRepository.GetUserSnapshotByLoginId(id);
                    if (users != null && users.Count > 0)
                    {
                        users.ForEach(u =>
                        {
                            Auditor.AddSupportLog(Current.LoginId, LogDomain.Agency, u.AgencyId, u.Id.ToString(), LogType.User, LogAction.UserEdited, "User Password/Signature updated");
                        });
                    }
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Password/Signature was saved successfully. ";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User Login was not found.";
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.QALogin)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TestImpersonate(Guid agencyId, Guid id )
        {
            var url = membershipService.GetImpersonationUrl(agencyId, id);
            if (url.IsNotNullOrEmpty()) return Redirect(url);
            return RedirectToAction("Index", "Home");
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.LiveLogin)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LiveImpersonate(Guid agencyId, Guid id)
        {
            var url = membershipService.GetLiveImpersonationUrl(agencyId, id);
            if (url.IsNotNullOrEmpty()) return Redirect(url);
            return RedirectToAction("Index", "Home");
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserLogs(Guid id)
        {
            var logList = new List<SupportAppAudit>();
            var userSnapshot = loginRepository.GetUserSnapshot(id);
            if (userSnapshot != null)
            {
                logList = agencyService.GetSupportLogs(LogDomain.Agency, LogType.User, userSnapshot.AgencyId, userSnapshot.Id.ToString()); new List<SupportAppAudit>();
            }
            //user.ForEach(u =>
            //    {
            //        agencyService.GetSupportLogs(LogDomain.Agency, LogType.User, u.AgencyId, u.Id.ToString()).ForEach(l =>
            //            {
            //                logList.Add(l);
            //            });
            //    });
            return PartialView("ActivityLogs", logList);
        }
        
        #endregion

        #region Other Views/Actions

        [PermissionFilter(ParentPermission.SalesAgreement, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Agreement()
        {
            return PartialView("Agreement");
        }

        [PermissionFilter(ParentPermission.MarketingIntelligence, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Marketing()
        {
           return PartialView("Marketing");
        }

        [PermissionFilter(ParentPermission.HRManagement, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HumanResource()
        {
            return PartialView();
        }

        [PermissionFilter(ParentPermission.Accounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Accounting()
        {
            return PartialView("Accounting");
        }

        [PermissionFilter(ParentPermission.Raffle, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Raffle()
        {
            return PartialView("Raffle");
        }

        [PermissionFilter(ParentPermission.Template, PermissionActions.Load)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Templates(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not load templates for this agency." };
            try
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(id);
                if (agencySnapShot != null && agencySnapShot.ClusterId > 0)
                {
                    if (agencyRepository.InsertTemplates(id, agencySnapShot.ClusterId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Templates have been added for this agency.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Supply, PermissionActions.Load)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Supplies(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not load supplies for this agency." };
            try
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(id);
                if (agencySnapShot !=null && agencySnapShot.ClusterId > 0)
                {
                    if (agencyRepository.InsertSupplies(id, agencySnapShot.ClusterId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Supplies have been added for this agency.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgencyLogs(Guid id)
        {
            var agencyInformation = agencyService.GetSupportLogs(LogDomain.Agency, LogType.AgencyInformation, id, id.ToString());
            agencyService.GetSupportLogs(LogDomain.Agency, LogType.Agency, id, id.ToString()).ForEach(l => agencyInformation.Add(l));
            return PartialView("ActivityLogs", agencyInformation);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ESign()
        {
            return Json(SessionStore.SessionId);
        }
        #endregion

        #region Center

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Center()
        {
            var viewData = new AgencyCenterViewData();
            //viewData.Count = agencyRepository.GetAgencyCount();
            return PartialView("Center/Layout", viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterContent(Guid agencyId)
        {
            var viewData = new AgencyCenterViewData();
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapShot != null)
            {
                var agency = agencyRepository.Get(agencyId, agencySnapShot.ClusterId);
                if (agency != null)
                {
                    agency.Trainer = agencySnapShot.Trainer;
                    agency.BackupTrainer = agencySnapShot.BackupTrainer;
                    agency.SalesPerson = agencySnapShot.SalesPerson;
                    agency.ImplementationSpecialist = agencySnapShot.ImplementationSpecialist;
                    var mainLocation = agencyRepository.GetMainLocation(agency.Id, agencySnapShot.ClusterId);
                    if (mainLocation != null)
                    {
                        agency.AddressLine1 = mainLocation.AddressLine1;
                        agency.AddressLine2 = mainLocation.AddressLine2;
                        agency.AddressCity = mainLocation.AddressCity;
                        agency.AddressStateCode = mainLocation.AddressStateCode;
                        agency.AddressZipCode = mainLocation.AddressZipCode;
                    }
                    viewData.Agency = agency;
                    var agencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
                    if (agencyPermissions != null && agencyPermissions.Count > 0)
                    {

                        viewData.IsUserCanEdit = agencyPermissions.Contains((int)PermissionActions.Edit);
                        viewData.IsUserCanView = agencyPermissions.Contains((int)PermissionActions.ViewList);
                        viewData.IsUserCanViewLog = agencyPermissions.Contains((int)PermissionActions.ViewLog);
                        viewData.IsUserCanMarkAgencyReadonly = agencyPermissions.Contains((int)PermissionActions.ReadOnly);
                        viewData.IsUserCanSuspendAgency = agencyPermissions.Contains((int)PermissionActions.Suspend);
                        viewData.IsUserCanViewAgencyUser = agencyPermissions.Contains((int)PermissionActions.ViewAgencyUser);
                        viewData.IsUserCanEditAgencyUser = agencyPermissions.Contains((int)PermissionActions.EditAgencyUser);
                        viewData.IsUserCanLiveLoginAsUser = agencyPermissions.Contains((int)PermissionActions.LiveLogin);
                        viewData.IsUserCanQALoginAsUser = agencyPermissions.Contains((int)PermissionActions.QALogin);
                        viewData.IsUserCanSendActiveLink = agencyPermissions.Contains((int)PermissionActions.ActivateLink);
                        viewData.IsUserCanEmailToAgencyUser = agencyPermissions.Contains((int)PermissionActions.EmailTo);
                    }
                    var templatePermissions = Current.CategoryPermissions(ParentPermission.Template);
                    if (templatePermissions != null && templatePermissions.Count > 0)
                    {
                        viewData.IsUserCanLoadTemplate = templatePermissions.Contains((int)PermissionActions.Load);
                    }
                    var supplyPermissions = Current.CategoryPermissions(ParentPermission.Supply);
                    if (supplyPermissions != null && supplyPermissions.Count > 0)
                    {
                        viewData.IsUserCanLoadSupply = supplyPermissions.Contains((int)PermissionActions.Load);
                    }
                    var physicianPermissions = Current.CategoryPermissions(ParentPermission.Physician);
                    if (physicianPermissions != null && physicianPermissions.Count > 0)
                    {
                        viewData.IsUserCanViewPhysician = physicianPermissions.Contains((int)PermissionActions.ViewList);
                    }
                    var notePermissions = Current.CategoryPermissions(ParentPermission.Note);
                    if (notePermissions != null && notePermissions.Count > 0)
                    {
                        viewData.IsUserCanAdd = notePermissions.Contains((int)PermissionActions.Add);
                        viewData.IsUserCanViewNote = notePermissions.Contains((int)PermissionActions.ViewList);

                    }
                }
            }
            return PartialView("Center/Content", viewData);
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterInfo(Guid agencyId)
        {
            var agency = new Agency();
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapShot != null)
            {
                agency = agencyRepository.Get(agencyId, agencySnapShot.ClusterId);
                if (agency != null)
                {
                    agency.Trainer = agencySnapShot.Trainer;
                    agency.BackupTrainer = agencySnapShot.BackupTrainer;
                    agency.SalesPerson = agencySnapShot.SalesPerson;
                    agency.ImplementationSpecialist = agencySnapShot.ImplementationSpecialist;
                }
            }
            return PartialView("Charts/Info", agency);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterUsersGrid(Guid agencyId)
        {
            return View(new GridModel( customerNotesRepository.GetCustomerNotes(agencyId)));
        }

        [PermissionFilter(ParentPermission.Agency, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterAgencyGrid(string stateCode, bool isDeleted, int? clusterId)
        {
            var agencyList = new List<AgencySelection>();
            if (clusterId > 0)
            {
                agencyList = agencyRepository.GetAgencySelection(isDeleted, clusterId.HasValue?clusterId.Value:0);
            }
            else
            {
                var clusters = new int[] { 1, 2 };
                clusters.ForEach(c =>
                {
                    var list = agencyRepository.GetAgencySelection(isDeleted, c);
                    if (list != null && list.Count > 0)
                    {
                        agencyList.AddRange(list);
                    }

                });
            }
            if (agencyList != null && agencyList.Count > 0)
            {
                if (stateCode.IsNotNullOrEmpty())
                {
                    agencyList = agencyList.Where(a => ((a.StateCode == null) ? "" : a.StateCode) == stateCode).ToList();
                }
            }
            return View(new GridModel(agencyList.OrderBy(a => a.Name)));
        }
        #endregion

        #region Upgrades

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeList()
        {
            var permission = new InternalUserPermissionViewData();
            var UpgradePermissions = Current.CategoryPermissions(ParentPermission.UpgradeRequests);
            if (UpgradePermissions != null && UpgradePermissions.Count > 0)
            {
                permission.IsUserCanExport = UpgradePermissions.Contains((int)PermissionActions.Export);
                permission.IsUserCanViewDetail = UpgradePermissions.Contains((int)PermissionActions.ViewDetail);
                permission.IsUserCanEditDetail = UpgradePermissions.Contains((int)PermissionActions.EditDetail);
            }
            var agencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (agencyPermissions != null && agencyPermissions.Count > 0)
            {
                permission.IsUserCanView = agencyPermissions.Contains((int)PermissionActions.ViewList);  
            }
            return PartialView("Upgrade/List", permission);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeGrid(string isComplete, DateTime startDate, DateTime endDate)
        {
            var allList = agencyService.GetAgencyUpgrades(isComplete, startDate, endDate, 0);
            return View(new GridModel(allList));
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.EditDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeEdit(Guid id, Guid agencyId)
        {
           
            var upgrade = accountingRepository.GetAgencyUpgradeOnly(id, agencyId);
            if (upgrade != null)
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapShot != null)
                {
                    upgrade.AgencyName = agencySnapShot.Name;
                    var user = userRepository.GetUserForName(agencyId, upgrade.RequestedById, agencySnapShot.ClusterId);
                    if (user != null)
                    {
                        upgrade.RequestedBy = user.DisplayName;
                    }
                }
               
             return PartialView("Upgrade/Edit", upgrade);
            }
            return PartialView("Upgrade/Edit", new AgencyUpgrade { });
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeView(Guid id, Guid agencyId)
        {
            var upgrade = accountingRepository.GetAgencyUpgradeOnly(id, agencyId);
            if (upgrade != null)
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapShot != null)
                {
                    upgrade.AgencyName = agencySnapShot.Name;
                    var user = userRepository.GetUserForName(agencyId, upgrade.RequestedById, agencySnapShot.ClusterId);
                    if (user != null)
                    {
                        upgrade.RequestedBy = user.DisplayName;
                    }
                }
                return PartialView("Upgrade/Details", upgrade);
            }
            return PartialView("Upgrade/Details", new AgencyUpgrade { });
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.EditDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeUpdate(Guid Id, Guid AgencyId, string Amount, string PreviousAmount, DateTime EffectiveDate, string Comments, string AccountId, string RequestedPackageId, bool IsComplete)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Agency upgrade could not be updated." };
            var exsitingUpgrade = accountingRepository.GetAgencyUpgradeOnly(Id, AgencyId);
            if (exsitingUpgrade != null)
            {
                if (EffectiveDate >= DateTime.Today && Amount.IsNotNullOrEmpty() && Amount.ToDouble() >= 0 && PreviousAmount.IsNotNullOrEmpty() && PreviousAmount.ToDouble() >= 0)
                {
                    var lastPayment = accountingRepository.GetLastPayments(AccountId);
                    if (EffectiveDate > lastPayment.Start)
                    {
                        if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, IsComplete))
                        {
                            if (accountingRepository.AddAccountingpayments(AccountId, lastPayment.Sequence + 1, EffectiveDate, decimal.Parse(Amount), RequestedPackageId.ToInteger()))
                            {
                                if (accountingRepository.UpdateLastPayment(lastPayment.Id, EffectiveDate.AddDays(-1)))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Agency upgrade was updated successfully";
                                }
                                else
                                {
                                    accountingRepository.removeAccountingpayments(AccountId, lastPayment.Sequence + 1);
                                    accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, exsitingUpgrade.Amount, exsitingUpgrade.PreviousAmount, exsitingUpgrade.EffectiveDate, exsitingUpgrade.Comments, exsitingUpgrade.IsComplete);
                                }

                                //var encoding = new ASCIIEncoding();
                                //string postData = ("action=payment&account=" + upgrade.AccountId + "&amount=" + upgrade.Amount + "&start=" + upgrade.EffectiveDate.ToString("yyyy-MM-dd") + "&package=" + upgrade.RequestedPackageId);
                                //byte[] data = encoding.GetBytes(postData);
                                //var request = (HttpWebRequest)WebRequest.Create(string.Format(CoreSettings.AgencyUpgrade + "{0}", Session.SessionID));
                                //request.Method = "POST";
                                //request.ContentType = "application/x-www-form-urlencoded";
                                //request.ContentLength = data.Length;
                                //using (var newStream = request.GetRequestStream())
                                //{
                                //    newStream.Write(data, 0, data.Length);
                                //}
                                //var response = (HttpWebResponse)request.GetResponse();
                                //if (response != null && response.StatusCode == HttpStatusCode.OK)
                                //{
                                //viewData.isSuccessful = true;
                                //viewData.errorMessage = "Agency upgrade was updated successfully";
                                //}
                            }
                            else
                            {
                                accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, exsitingUpgrade.Amount, exsitingUpgrade.PreviousAmount, exsitingUpgrade.EffectiveDate, exsitingUpgrade.Comments, exsitingUpgrade.IsComplete);
                            }
                        }
                    }
                    else if (EffectiveDate == lastPayment.Start)
                    {
                        if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, IsComplete))
                        {
                            if (accountingRepository.UpdateLastPaymentStartDate(lastPayment.Id, decimal.Parse(Amount), EffectiveDate))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Agency upgrade was updated successfully";
                            }
                            else
                            {
                                accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, exsitingUpgrade.Amount, exsitingUpgrade.PreviousAmount, exsitingUpgrade.EffectiveDate, exsitingUpgrade.Comments, exsitingUpgrade.IsComplete);
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Effective Date should after start date.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "please enter a valid Effective Date and pricing.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.EditDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpgradeSave(Guid Id, Guid AgencyId, string Amount, string PreviousAmount, DateTime EffectiveDate, string Comments, bool IsComplete)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Agency upgrade could not be saved." };
            if (EffectiveDate >= DateTime.Today && Amount.IsNotNullOrEmpty() && Amount.ToDouble() >= 0 && PreviousAmount.IsNotNullOrEmpty() && PreviousAmount.ToDouble() >= 0)
            {
                if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, !IsComplete))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency upgrade was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "please enter a valid Effective Date and pricing.";
            }
            return Json(viewData);
        }

        #endregion

        #region AnnualPlan

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualPlanList()
        {
            var permission = new InternalUserPermissionViewData();
            var UpgradePermissions = Current.CategoryPermissions(ParentPermission.UpgradeRequests);
            if (UpgradePermissions != null && UpgradePermissions.Count > 0)
            {
                permission.IsUserCanExport = UpgradePermissions.Contains((int)PermissionActions.Export);
                permission.IsUserCanViewDetail = UpgradePermissions.Contains((int)PermissionActions.ViewDetail);
                permission.IsUserCanEditDetail = UpgradePermissions.Contains((int)PermissionActions.EditDetail);
            }
            var agencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (agencyPermissions != null && agencyPermissions.Count > 0)
            {
                permission.IsUserCanView = agencyPermissions.Contains((int)PermissionActions.ViewList);
            }
            return PartialView("AnnualPlan/List", permission);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualPlanGrid(string isComplete, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(agencyService.GetAgencyAnnualPlan(isComplete,  startDate,  endDate, 0)?? new List<AgencyUpgrade>()));
        }

     
        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.EditDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualPlanEdit(Guid id, Guid agencyId)
        {

            var annualPlan = accountingRepository.GetAgencyUpgradeOnly(id, agencyId);
            if (annualPlan != null)
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapShot != null)
                {
                    annualPlan.AgencyName = agencySnapShot.Name;

                    var user = userRepository.GetUserForName(agencyId, annualPlan.RequestedById, agencySnapShot.ClusterId);
                    if (user != null)
                    {
                        annualPlan.RequestedBy = user.DisplayName;
                    }
                }
            }
            return PartialView("AnnualPlan/Edit", annualPlan);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualPlanView(Guid id, Guid agencyId)
        {
            var annualPlan = accountingRepository.GetAgencyUpgradeOnly(id, agencyId);
            if (annualPlan != null)
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapShot != null)
                {
                    annualPlan.AgencyName = agencySnapShot.Name;
                    var user = userRepository.GetUserForName(agencyId, annualPlan.RequestedById, agencySnapShot.ClusterId);
                    if (user != null)
                    {
                        annualPlan.RequestedBy = user.DisplayName;
                    }
                }
            }
            return PartialView("AnnualPlan/Details", annualPlan);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.EditDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualPlanUpdate(Guid Id, Guid AgencyId, string Amount, string PreviousAmount, DateTime EffectiveDate, string Comments, string AccountId, string RequestedPackageId, bool IsComplete)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Agency annual plan could not be updated." };
            var exsitingAnnualPlan = accountingRepository.GetAgencyUpgradeOnly(Id, AgencyId);
            if (EffectiveDate >= DateTime.Today && Amount.IsNotNullOrEmpty() && Amount.ToDouble() >= 0 && PreviousAmount.IsNotNullOrEmpty() && PreviousAmount.ToDouble() >= 0)
            {
                if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, IsComplete))
                {
                    var lastPayment = accountingRepository.GetLastPayments(AccountId);
                    if (EffectiveDate > lastPayment.Start)
                    {
                        if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, IsComplete))
                        {
                            if (accountingRepository.AddAccountingpayments(AccountId, lastPayment.Sequence + 1, EffectiveDate, decimal.Parse(Amount), RequestedPackageId.ToInteger()))
                            {
                                if (accountingRepository.UpdateLastPayment(lastPayment.Id, EffectiveDate.AddDays(-1)))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Annual plan was updated successfully";
                                }
                                else
                                {
                                    accountingRepository.removeAccountingpayments(AccountId, lastPayment.Sequence + 1);
                                    accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, exsitingAnnualPlan.Amount, exsitingAnnualPlan.PreviousAmount, exsitingAnnualPlan.EffectiveDate, exsitingAnnualPlan.Comments, exsitingAnnualPlan.IsComplete);
                                }
                            }
                            else
                            {
                                accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, exsitingAnnualPlan.Amount, exsitingAnnualPlan.PreviousAmount, exsitingAnnualPlan.EffectiveDate, exsitingAnnualPlan.Comments, exsitingAnnualPlan.IsComplete);
                            }
                        }
                    }
                    else if (EffectiveDate == lastPayment.Start)
                    {
                        if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, IsComplete))
                        {
                            if (accountingRepository.UpdateLastPaymentStartDate(lastPayment.Id, decimal.Parse(Amount), EffectiveDate))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Agency upgrade was updated successfully";
                            }
                            else
                            {
                                accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, exsitingAnnualPlan.Amount, exsitingAnnualPlan.PreviousAmount, exsitingAnnualPlan.EffectiveDate, exsitingAnnualPlan.Comments, exsitingAnnualPlan.IsComplete);
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Effective Date should after start date.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "please enter a valid Effective Date and pricing.";
                }
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.EditDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AnnualPlanSave(Guid Id, Guid AgencyId, string Amount, string PreviousAmount, DateTime EffectiveDate, string Comments, bool IsComplete)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Agency annual plan could not be updated." };
                if (EffectiveDate >= DateTime.Today && Amount.IsNotNullOrEmpty() && Amount.ToDouble() >= 0 && PreviousAmount.IsNotNullOrEmpty() && PreviousAmount.ToDouble() >= 0)
                {
                    if (accountingRepository.UpdateAgencyUpgrade(Id, AgencyId, Amount.ToDouble(), PreviousAmount.ToDouble(), EffectiveDate, Comments, !IsComplete))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Agency annual plan was saved successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "please enter a valid Effective Date and pricing.";
                }
            return Json(viewData);
        }

        #endregion

        #region Changes

        [PermissionFilter(ParentPermission.ChangeRequests, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeList()
        {
            var permission = new InternalUserPermissionViewData();
            var ChangePermissions = Current.CategoryPermissions(ParentPermission.ChangeRequests);
            if (ChangePermissions != null && ChangePermissions.Count > 0)
            {
                permission.IsUserCanExport = ChangePermissions.Contains((int)PermissionActions.Export);
                permission.IsUserCanEdit = ChangePermissions.Contains((int)PermissionActions.Edit);
                permission.IsUserCanView = ChangePermissions.Contains((int)PermissionActions.ViewList);
            }
            
            return PartialView("Change/List", permission);
        }

        [PermissionFilter(ParentPermission.ChangeRequests, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeGrid(string isComplete, DateTime startDate, DateTime endDate)
        {
            var allList = accountingRepository.GetAgencyChanges(isComplete, startDate, endDate) ?? new List<AgencyChange>();
            //var clusterIds = new int[] { 1, 2 };
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            //        var List = accountingRepository.GetAgencyChanges(isComplete, startDate, endDate);
            //        if (List != null && List.Count > 0)
            //        {
            //            allList.AddRange(List);
            //        }
            //    }
            //}
            //var result = agencyRepository.GetAgencyChanges(isComplete, startDate, endDate, int clusterId);
            return View(new GridModel(allList));
        }

        [PermissionFilter(ParentPermission.ChangeRequests, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeView(Guid id, Guid agencyId)
        {
            var viewData = new AgencyChangeViewData();
            
            viewData.preAgencyInfo = accountingRepository.GetAgencyChangePrev(id, agencyId);
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapShot != null && agencySnapShot.ClusterId > 0)
            {
                viewData.curAgencyInfo = agencyRepository.GetAgencyChangeCur(agencyId, agencySnapShot.ClusterId);
            }
            return PartialView("Change/Details", viewData);         
        }

        [PermissionFilter(ParentPermission.ChangeRequests, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeUpdate(Guid Id, Guid AgencyId, string Comments, bool IsComplete)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Agency Change could not be completed." };
           // var agencySnapShot = loginRepository.GetAgencySnapshot(AgencyId);
            if (accountingRepository.UpdateAgencyChange(Id, AgencyId, Comments, IsComplete))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Agency change was completed successfully";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Agency Change could not be completed.";
            }
            return Json(viewData);
        }

        #endregion

        #region MarkAsReadOnly

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkAsReadOnlyList()
        {
            var permission = new InternalUserPermissionViewData();
            var UpgradePermissions = Current.CategoryPermissions(ParentPermission.UpgradeRequests);
            if (UpgradePermissions != null && UpgradePermissions.Count > 0)
            {
                permission.IsUserCanExport = UpgradePermissions.Contains((int)PermissionActions.Export);
                permission.IsUserCanViewDetail = UpgradePermissions.Contains((int)PermissionActions.ViewDetail);
                permission.IsUserCanEditDetail = UpgradePermissions.Contains((int)PermissionActions.EditDetail);
            }
            var agencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (agencyPermissions != null && agencyPermissions.Count > 0)
            {
                permission.IsUserCanView = agencyPermissions.Contains((int)PermissionActions.ViewList);
            }
            return PartialView("MarkAsReadOnly/List", permission);
        }

        [PermissionFilter(ParentPermission.UpgradeRequests, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkAsReadOnlyGrid(DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(agencyService.GetAgencyMarkAsReadOnly(startDate, endDate)));
        }

      
     
        #endregion

        #region Implementation Specialists

        [PermissionFilter(ParentPermission.ImplementationSpecialist, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ImplementationSpecialistList()
        {

            var permission = new InternalUserPermissionViewData();
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.ImplementationSpecialist);
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                permission.IsUserCanReassign = categoryPermissions.Contains((int)PermissionActions.Reassign);
            }
            var AgencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (AgencyPermissions != null && AgencyPermissions.Count > 0)
            {
                permission.IsUserCanView = AgencyPermissions.Contains((int)PermissionActions.ViewList);
                permission.IsUserCanEmailToContact = AgencyPermissions.Contains((int)PermissionActions.EmailTo);

            }
            var NotePermissions = Current.CategoryPermissions(ParentPermission.Note);
            if (NotePermissions != null && NotePermissions.Count > 0)
            {
                permission.IsUserCanAdd = NotePermissions.Contains((int)PermissionActions.Add);
            }
            return PartialView("ImplementationSpecialist/List", permission);
        }

        [PermissionFilter(ParentPermission.ImplementationSpecialist, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ImplementationSpecialistGrid(Guid implementationSpecialistId, DateTime startDate, DateTime endDate)
        {
            return View(new GridModel(agencyService.GetImplementationSpecialists(implementationSpecialistId,  startDate,  endDate)));
        }

   
        [PermissionFilter(ParentPermission.ImplementationSpecialist, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ImplementationSpecialistEdit(Guid agencyId)
        {
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            var implementationSpecialist = loginRepository.GetImplementationSpecialist(agencyId);
            if (implementationSpecialist != null)
            {
                implementationSpecialist.SpecialistName = supportRepository.GetLoginDisplayName(implementationSpecialist.SpecialistId);
                return PartialView("ImplementationSpecialist/Edit", implementationSpecialist);
            }
            return PartialView("ImplementationSpecialist/Edit", new AgencySpecialist { });
        }

        [PermissionFilter(ParentPermission.ImplementationSpecialist, PermissionActions.Reassign)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ImplementationSpecialistUpdate(AgencySpecialist specialist)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Agency Implementation Specialist could not be updated." };
            var agencySnapShot = loginRepository.GetAgencySnapshot(specialist.AgencyId);
            if (agencySnapShot != null)
            {
                if (loginRepository.UpdateImplementationSpecialist(specialist.AgencyId, specialist.SpecialistId))
                {
                    if (agencyRepository.UpdateImplementationSpecialist(agencySnapShot.Id, specialist.SpecialistId, agencySnapShot.ClusterId))
                    {
                        var login = supportRepository.Find(specialist.SpecialistId);
                        if (login != null)
                        {
                            specialist.SpecialistName = login.DisplayName;
                            specialist.SpecialistEmailAddress = login.EmailAddress;
                            System.Threading.ThreadPool.QueueUserWorkItem(state => SendImplementationSpecialistNotification(specialist));
                        }
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Agency Implementation Specialist was saved successfully";
                    }
                    else
                    {
                        loginRepository.UpdateImplementationSpecialist(agencySnapShot.Id, agencySnapShot.ImplementationSpecialist);
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Static Methods

        private static void SendImplementationSpecialistNotification(AgencySpecialist specialist)
        {
            string iSpecialistSubject = string.Format("You are the Implementation Specialist for {0}", specialist.AgencyName);
            string iSpecialistBodyText = MessageBuilder.PrepareTextFrom("ImplementationSpecialistUpdate",
                "agencyname", specialist.AgencyName,
                "displayname", specialist.SpecialistName);
            Notify.User(CoreSettings.NoReplyEmail, specialist.SpecialistEmailAddress, iSpecialistSubject, iSpecialistBodyText, new string[] { "Implementation_Specialist_Checklist.docx" });
        }

        #endregion
    }
}
