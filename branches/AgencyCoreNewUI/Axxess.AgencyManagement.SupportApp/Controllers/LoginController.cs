﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.Log.Common;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.SupportApp.Services.Interfaces;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.SupportApp.ViewData;
    using Axxess.AgencyManagement.SupportApp.Enums;
    using Axxess.AgencyManagement.SupportApp.Filter;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class LoginController : BaseController
    {
        #region Private Members / Constructor

        private readonly ISupportRepository supportRepository;
        private readonly ILoginService loginService;

        public LoginController(IMembershipDataProvider membershipDataProvider, ILoginService loginService)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.loginService = loginService;
            this.supportRepository = membershipDataProvider.SupportRepository;
        }

        #endregion

        #region Login Account Actions

        [PermissionFilter(ParentPermission.User, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New()
        {
            return View(loginService.GetNewInternalUser());
        }

        [PermissionFilter(ParentPermission.User, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind] SupportLogin login, [Bind(Prefix = "Permissions")] Dictionary<int, List<int>> Permissions)
        {
            Check.Argument.IsNotNull(login, "login");
            var viewData = new JsonViewData();
            if (supportRepository.Find(login.EmailAddress) == null)
            {
                login.Role = Roles.AxxessAdmin.ToString();
                login.IsActive = true;
                login.IsLocked = false;
                if (supportRepository.Add(login))
                {
                    if (supportRepository.AddInternalUser(new InternalUser { LoginId = login.Id, Permissions = Permissions.ToJson() }))
                    {
                        Auditor.AddSupportLog(Current.LoginId, LogDomain.SupportUser, login.Id, login.Id.ToString(), LogType.Login, LogAction.UserAdded, "");
                        var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}", login.Id)));
                        var bodyText = MessageBuilder.PrepareTextFrom(
                            "NewSupportUserConfirmation",
                            "firstname", login.DisplayName,
                            "encryptedQueryString", encryptedQueryString);
                        Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Welcome to Axxess Support", bodyText);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "New Login was added successfully";
                    }
                    else
                    {
                        supportRepository.RemoveLogin(login.Id);
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in adding the login."; 
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The Login e-mail address provided is already in use.";
            }
            return Json(viewData);
        }
        
        [PermissionFilter(ParentPermission.User, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            var result = loginService.GetUserPermissionsOnly(id);
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.User);
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                result.IsUserCanViewLog = categoryPermissions.Contains((int)PermissionActions.ViewLog);
            }
            return PartialView(result);
        }

        [PermissionFilter(ParentPermission.User, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ToggleUser(Guid Id, bool isActive)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User status could not be updated" };
            if (supportRepository.UpdateUserStatus(Id, isActive? 0: 1))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User status was updated successfully. ";
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.User, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(SupportLogin login,[Bind(Prefix="Permissions")] Dictionary<int, List<int>> Permissions)
        {
            Check.Argument.IsNotNull(login, "login");
            return Json(loginService.UpdateInternalUser(login,Permissions));
        }

        [PermissionFilter(ParentPermission.User, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List()
        {
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.User);
            var permission = new InternalUserPermissionViewData();
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                permission.IsUserCanAdd = categoryPermissions.Contains((int)PermissionActions.Add);
                permission.IsUserCanEdit = categoryPermissions.Contains((int)PermissionActions.Edit);
                permission.IsUserCanViewLog = categoryPermissions.Contains((int)PermissionActions.ViewLog);
            }
            return PartialView(permission);
        }

        [PermissionFilter(ParentPermission.User, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(bool status)
        {
            var result = supportRepository.GetAll(status);
            return View(new GridModel(result));
        }

        [PermissionFilter(ParentPermission.User, PermissionActions.ViewLog)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoginLogs(Guid id)
        {
            var userInformation = loginService.GetSupportLogs(LogDomain.SupportUser, LogType.Login, id, id.ToString());
            return PartialView("ActivityLogs", userInformation);
        }
        #endregion

        #region pemission template

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewTemplate()
        {
            var viewData = new PermissionTemplateViewData();
            viewData.PermissionData = loginService.GetNewInternalUser();
            return PartialView("Permission/NewTemplate", viewData);
        }

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateList()
        {
            var permission = new InternalUserPermissionViewData();
            var TemplatePermissions = Current.CategoryPermissions(ParentPermission.PermissionTemplates);
            if (TemplatePermissions != null && TemplatePermissions.Count > 0)
            {
                permission.IsUserCanAdd = TemplatePermissions.Contains((int)PermissionActions.Add);
                permission.IsUserCanEdit = TemplatePermissions.Contains((int)PermissionActions.Edit);
                permission.IsUserCanDelete = TemplatePermissions.Contains((int)PermissionActions.Delete);
            }
            return PartialView("Permission/TemplateList", permission);
        }

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateGrid()
        {
            return View(new GridModel(supportRepository.GetTemplatePermissions()));
        }

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.Edit)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditTemplate(int Id)
        {
            var result = loginService.GetUsePermissionsTemplates(Id);
            return PartialView("Permission/EditTemplate", result);
        }

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.Delete)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteTemplate(int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not delete permission template." };
            if (supportRepository.DeleteTemplate(Id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Permission template has been updated.";
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPermissionTemplate(string templateName, [Bind(Prefix = "Permissions")] Dictionary<int, List<int>> permissions)
        {
            Check.Argument.IsNotNull(templateName, "templateName");
            var viewData = new JsonViewData();
            var permissionTemplate = new PermissionTemplate();
            permissionTemplate.Name = templateName;
            permissionTemplate.Permissions = permissions.ToJson();
            if (supportRepository.AddTemplate(permissionTemplate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "New template was added successfully";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in adding the template.";
            }
                
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.PermissionTemplates, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePermissionTemplate(int Id, string templateName, [Bind(Prefix = "Permissions")] Dictionary<int, List<int>> permissions)
        {
            Check.Argument.IsNotNull(Id, "Id");
            Check.Argument.IsNotNull(templateName, "templateName");
            var viewData = new JsonViewData();
            var permissionTemplate = new PermissionTemplate();
            permissionTemplate.Id = Id;
            permissionTemplate.Name = templateName;
            permissionTemplate.Permissions = permissions.ToJson();
            if (supportRepository.UpdateTemplate(permissionTemplate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "New template was updated successfully";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in updating the template.";
            }
            return Json(viewData);
        }
        
        #endregion
    }
}
