﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Domain;
    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.SupportApp.Services;
    using Axxess.AgencyManagement.SupportApp.Exports;
    using Axxess.AgencyManagement.SupportApp.ViewData;
    using Axxess.AgencyManagement.SupportApp.Enums;
    using Axxess.AgencyManagement.SupportApp.Filter;
    using System.Web.Script.Serialization;


    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountingController : BaseController
    {
        #region Constructor

        IAccountingRepository accountingRepository;
        IAccountingService accountingService;

        public AccountingController(IAccountingManagementDataProvider accountingManagementDataProvider, IAccountingService accountingService)
        {
            Check.Argument.IsNotNull(accountingManagementDataProvider, "accountingManagementDataProvider");
            this.accountingRepository = accountingManagementDataProvider.AccountingRepository;
            this.accountingService = accountingService;
        }

        #endregion

        #region Accounting Module

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List()
        {
            return PartialView();
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAccountingFrame()
        {
            return View("Iframe");
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewAccountingContent()
        {
            return View("Accounting");
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Information(string accountId)
        {
            var information = accountingRepository.GetOneAccountingInformation(accountId);
            var agencyAccount = new AgencyAccount();
            var infoGroup = information.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    agencyAccount = new AgencyAccount { AgencyName = key.Name, AccountId = key.Account, PaymentSource = key.PaymentSource, City = key.City, State = key.State, ZipCode = key.Zip, SalesPerson = key.SalesPerson, ImplementationFee = key.ImplementationFee, InitialSignDate = key.InitialSignDate, SpecialNotes = key.SpecialNotes, StartUp = key.StartUp, Status = key.Status, Payments = new List<PaymentHistory>() };
                    var lastPayment = value.OrderBy(v => v.Start).ToList();
                    
                    if (lastPayment != null)
                    {
                        for (var i = 0; i < lastPayment.Count; i++)
                        {
                            var payamentHistory = new PaymentHistory();
                            payamentHistory.Id = lastPayment[i].PaymentId;
                            payamentHistory.Start = lastPayment[i].Start;
                            payamentHistory.End = lastPayment[i].End;
                            payamentHistory.Package = lastPayment[i].Package;
                            payamentHistory.Amount = String.Format("{0:0.00}", lastPayment[i].Amount);
                            agencyAccount.Payments.Add(payamentHistory);
                        }
                    }
                });
            }
            return Json(agencyAccount);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(AgencyAccount agencyAccount)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Account could not be updated." };
            if (accountingRepository.UpdateAccountingInformation(agencyAccount.AccountId, agencyAccount.AgencyName, agencyAccount.City, agencyAccount.StateFilter, agencyAccount.ZipCode, agencyAccount.ImplementationFee, agencyAccount.PaymentSource, agencyAccount.InitialSignDate, agencyAccount.SpecialNotes, agencyAccount.Status, agencyAccount.StartUp, agencyAccount.SalesPerson))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Account information was updated successfully";
                var paymentRecord = accountingRepository.GetAccountingPayments(agencyAccount.AccountId);
                for (var i = 0; i < agencyAccount.Payments.Count; i++)
                {
                    var payment = agencyAccount.Payments[i];
                    if (i == 0 && (payment.Start != paymentRecord[i].Start || payment.Amount.ToDouble() != paymentRecord[i].Amount || payment.Package != paymentRecord[i].Package))
                    {
                        viewData.isSuccessful = accountingRepository.UpdateAccountingpayments(payment.Id, payment.Start, payment.End, decimal.Parse(payment.Amount), payment.Package);
                        viewData.errorMessage = "Account payment was updated successfully";
                    }

                    else if (i > 0 && (payment.Start != paymentRecord[i].Start || payment.Amount.ToDouble() != paymentRecord[i].Amount || payment.Package != paymentRecord[i].Package))
                    {
                        if (paymentRecord[i].Start != payment.Start)
                        {
                            if (agencyAccount.Payments[i].Start > agencyAccount.Payments[i - 1].Start && agencyAccount.Payments[i].Start < agencyAccount.Payments[i].End)
                            {
                                var prePayment = agencyAccount.Payments[i - 1];
                                prePayment.End = payment.Start.AddDays(-1);
                                viewData.isSuccessful = accountingRepository.UpdateAccountingpayments(prePayment.Id, prePayment.Start, prePayment.End, decimal.Parse(prePayment.Amount), prePayment.Package);
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "please enter a correct start date";
                            }
                        }
                        if (viewData.isSuccessful)
                        {
                            viewData.isSuccessful = accountingRepository.UpdateAccountingpayments(payment.Id, payment.Start, payment.End, decimal.Parse(payment.Amount), payment.Package);
                            viewData.errorMessage = "Account payment was updated successfully";
                        }
                    }
                }

                if (agencyAccount.UpdatePayment.Amount != null)
                {
                    var addPayment = agencyAccount.UpdatePayment;
                    var last = agencyAccount.Payments.Count;
                    var lastPayment = agencyAccount.Payments[last - 1];
                    addPayment.Sequence = agencyAccount.Payments.Count + 1;
                    addPayment.Account = agencyAccount.AccountId;
                    viewData.isSuccessful = accountingRepository.AddAccountingpayments(addPayment.Account, addPayment.Sequence, addPayment.Start, decimal.Parse(addPayment.Amount), addPayment.Package);
                    lastPayment.End = agencyAccount.UpdatePayment.Start.AddDays(-1);
                    viewData.isSuccessful = accountingRepository.UpdateAccountingpayments(lastPayment.Id, lastPayment.Start, lastPayment.End, decimal.Parse(lastPayment.Amount), lastPayment.Package);
                    viewData.errorMessage += "Account payment was added successfully";
                }
            }

            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Range(int startMonth, int startYear, int endMonth, int endYear)
        {
            var start = new DateTime(startYear, startMonth, 1);
            var endDate = new DateTime(endYear, endMonth, 1);
            var end = endDate.AddMonths(1).AddDays(-1);
            var rangeResult = accountingService.CalculateDateRange(start, end);
            return Json(rangeResult);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Weekly(DateTime date)
        {
            var start = date;
            var end = start.AddDays(6);
            var weeklyResult = accountingService.CalculateWeekly(start, end);
            return Json(weeklyResult);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Monthly(DateTime date)
        {
            var start = new DateTime(date.Year, date.Month, 1);
            var end = start.AddMonths(1).AddDays(-1);
            var monthlyResult = accountingService.CalculateMonthly(start, end);
            return Json(monthlyResult);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Yearly(int date)
        {
            var start = new DateTime(date, 1, 1);
            var end = start.AddYears(1).AddDays(-1);

            var yearlyResult = accountingService.CalculateYearly(start, end);
            return Json(yearlyResult);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Export(string json)
        {
            AccountingPaymentViewData accountingPaymentViewData = new JavaScriptSerializer().Deserialize<AccountingPaymentViewData>(json);
            var export = new AccountingExporter(accountingPaymentViewData);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Accounting_{0}.xls", DateTime.Now.Ticks));
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ExportMetrics(string json)
        {
            AccountingMetricsViewData accountingMetricsViewData = new JavaScriptSerializer().Deserialize<AccountingMetricsViewData>(json);
            var export = new AccountingMetricsExporter(accountingMetricsViewData);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Accounting_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Accounting Charts

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChartFrame()
        {
            return View("ChartIframe");
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewAccountingChart()
        {
            return View("AccountingChart");
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EndingMRR(DateTime? StartDate, DateTime ? EndDate)
        {
            var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            var start = end.AddMonths(-6);

            if (StartDate.HasValue && EndDate.HasValue)
            {
                end = new DateTime(EndDate.Value.Year, EndDate.Value.Month, 1).AddMonths(1).AddDays(-1);
                start = new DateTime(StartDate.Value.Year, StartDate.Value.Month, 1);
            }
            var endingMRRResult = accountingService.CalculateEndingMRR(start, end);
            return Json(endingMRRResult);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MRRBookings(DateTime? StartDate, DateTime? EndDate)
        {
            var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            var start = end.AddMonths(-6);

            if (StartDate.HasValue && EndDate.HasValue)
            {
                end = new DateTime(EndDate.Value.Year, EndDate.Value.Month, 1).AddMonths(1).AddDays(-1);
                start = new DateTime(StartDate.Value.Year, StartDate.Value.Month, 1);
            }
            var mrrBookingResult = accountingService.CalculateMRRBooking(start, end);
            return Json(mrrBookingResult);
        }

        [PermissionFilter(ParentPermission.NewAccounting, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Metrics(int? startMonth, int? startYear, int? endMonth, int? endYear)
        {
            var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            var start =  new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-7);

            if (startMonth.HasValue && startYear.HasValue && endMonth.HasValue && endYear.HasValue)
            {
                end = new DateTime(endYear.Value, endMonth.Value, 1).AddMonths(1).AddDays(-1);
                start = new DateTime(startYear.Value, startMonth.Value, 1);
            }
            var metricsResult = accountingService.CalculateMetrics(start, end);
            return Json(metricsResult);
        }
        #endregion
    }   
}
