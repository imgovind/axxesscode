﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using System.Web.Mvc;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.Domain;
    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.SupportApp.Services;
    using Axxess.AgencyManagement.SupportApp.Exports;
    using Axxess.AgencyManagement.SupportApp.ViewData;
    using Axxess.AgencyManagement.SupportApp.Enums;
    using Axxess.AgencyManagement.SupportApp.Filter;
    using Axxess.Membership.Domain;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class CustomerNotesController : BaseController
    {
        #region Constructor

        ICustomerNotesService customerNotesService;
        ICustomerNotesRepository customerNotesRepository;
        IAgencyRepository agencyRepository;
        IUserRepository userRepository;
        ISupportRepository supportRepository;
        IAssetRepository assetRepository;
        private ILoginRepository loginRepository;

        public CustomerNotesController(IAgencyManagementMembershipDataProvider agencyManagementMembershipDataProvider, ISupportManagementDataProvider supportManagementDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ICustomerNotesService customerNotesService)
        {
            Check.Argument.IsNotNull(customerNotesService, "customerNotesService");
            this.customerNotesService = customerNotesService;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.customerNotesRepository = supportManagementDataProvider.CustomerNotesRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = supportManagementDataProvider.AssetRepository;
            this.loginRepository = agencyManagementMembershipDataProvider.LoginRepository;
        }
        #endregion

        #region Recent
        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Layout()
        {
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.Note);
            var permission = new InternalUserPermissionViewData();
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                permission.IsUserCanEdit = categoryPermissions.Contains((int)PermissionActions.Edit);
                permission.IsUserCanViewDetail = categoryPermissions.Contains((int)PermissionActions.ViewDetail);
                permission.IsUserCanViewNote = categoryPermissions.Contains((int)PermissionActions.ViewList);
            }
            var AgencyPermissions = Current.CategoryPermissions(ParentPermission.Agency);
            if (AgencyPermissions != null && AgencyPermissions.Count > 0)
            {
                permission.IsUserCanView = AgencyPermissions.Contains((int)PermissionActions.ViewList);
            }

            return PartialView(permission);
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RecentNotesGrid()
        {
            var notesList = customerNotesService.GetRecentNotes();
            return View(new GridModel(notesList));

        }

        //[PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult RecentNotesGrid()
        //{
        //    var endDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
        //    var startDate = endDate.AddDays(-7);
        //    var notesList = customerNotesService.GetRecentNotes(startDate, endDate);
        //    return View(new GridModel(notesList));

        //}
        #endregion

        #region Updated
        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatedNotesGrid()
        {
            var notesList = customerNotesService.GetRecentNotes();
            return View(new GridModel(notesList));
        }
        #endregion

        #region CurrentUser

        //[PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult CurrentUserNotesGrid()
        //{
        //    var endDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
        //    var startDate = endDate.AddDays(-7);

        //    var notesList = customerNotesRepository.GetCurrentUserNotes(Current.LoginId, startDate, endDate).OrderByDescending(c => c.Created).ToList();
        //    return View(new GridModel(notesList));
        //}

        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CurrentUserNotesGrid()
        {
            var notesList = customerNotesRepository.GetCurrentUserNotes(Current.LoginId).ToList();
            if (notesList != null && notesList.Count > 0)
            {
                var internalUserIds = new List<Guid>();
                var agencyIds = new List<Guid>();
                notesList.ForEach(n => { if (!n.LoginId.IsEmpty()) { internalUserIds.Add(n.LoginId); } if (!n.AgencyId.IsEmpty()) { agencyIds.Add(n.AgencyId); } });
                internalUserIds = internalUserIds.Distinct().ToList();
                agencyIds = agencyIds.Distinct().ToList();
                var supports = internalUserIds.Count > 0 ? supportRepository.GetLoginDisplayNamesWithEmail(internalUserIds) ?? new List<SupportLogin>() : new List<SupportLogin>();
                var agencies = agencyIds.Count > 0 ? loginRepository.GetAgencySnapshotNames(agencyIds) ?? new List<AgencySnapshot>() : new List<AgencySnapshot>();

                notesList.ForEach(n =>
                {
                    if (!n.LoginId.IsEmpty())
                    {
                        var support = supports.FirstOrDefault(s => s.Id == n.LoginId);
                        if (support != null)
                        {
                            n.RepName = support.DisplayName;
                        }
                    }
                    if (!n.AgencyId.IsEmpty())
                    {
                        var agency = agencies.FirstOrDefault(u => u.Id == n.AgencyId);
                        if (agency != null)
                        {
                            n.AgencyName = agency.Name;
                        }
                    }
                    //n.AgencyName = loginRepository.GetAgencySnapshot(n.AgencyId).Name;
                    //n.RepName = supportRepository.GetLoginDisplayName(n.LoginId);
                });
            }
            return View(new GridModel(notesList));
        }
        
        #endregion

        #region Note
        [PermissionFilter(ParentPermission.Note, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteNew(Guid agencyId)
        {
            var agency = new Agency();
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapShot != null)
            {
                agency = agencyRepository.GetAgencyOnly(agencyId, agencySnapShot.ClusterId);
                if (agency != null)
                {
                    agency.Trainer = agencySnapShot.Trainer;
                    agency.BackupTrainer = agencySnapShot.BackupTrainer;
                    agency.SalesPerson = agencySnapShot.SalesPerson;
                    agency.ImplementationSpecialist = agencySnapShot.ImplementationSpecialist;
                }
            }
            return PartialView("Note/New", agency);
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteCreate([Bind] CustomerNote note)
        {
            Check.Argument.IsNotNull(note, "note");
            var viewData = new JsonViewData();
            if (note.IsValid)
            {
                var button = Request.Form["buttonClicked"];
                if (!button.IsNullOrEmpty())
                {
                    if (button == "saveButton")
                    {
                        if (customerNotesService.SaveNote(note, Request.Files, false))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Note was saved successfully";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in saving the Note.";
                        }
                    }
                    else if (button == "completeButton")
                    {
                        if (customerNotesService.SaveNote(note, Request.Files, true))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Note was sent successfully";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in sending the Note.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = note.ValidationMessage;
            }
            return Json(viewData, "text/plain");
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewDetail)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteView(Guid id, Guid agencyId)
        {
            CustomerNote note = null;
            if (agencyId == Guid.Empty)
            {
                note = customerNotesRepository.GetCustomerNote(id);
            }
            else
            {
                note = customerNotesRepository.GetCustomerNote(agencyId, id);
            }
            if (note != null)
            {
                var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapshot != null)
                {
                    note.RepName = supportRepository.GetLoginDisplayName(note.LoginId);
                    
                    note.AgencyName = agencySnapshot.Name;
                    note.SalesPersonName = supportRepository.GetLoginDisplayName(agencySnapshot.SalesPerson);
                    note.TrainerName = supportRepository.GetLoginDisplayName(agencySnapshot.Trainer);
                    note.ModifiedByName = supportRepository.GetLoginDisplayName(note.ModifiedBy);
                    note = customerNotesService.GetAdditionalInformation(note);
                    if (note.CallerId != Guid.Empty)
                    {
                        var user = userRepository.GetUserForName(note.AgencyId, note.CallerId, agencySnapshot.ClusterId);
                        if (user != null)
                        {
                            note.CallerName = user.DisplayName;
                        }
                    }
                    if (note.AttachmentId != Guid.Empty)
                    {
                        var asset = assetRepository.Get(note.AttachmentId, note.AgencyId);
                        if (asset != null)
                        {
                            note.AttachmentName = asset.FileName;
                            note.HasAttachment = true;
                        }
                    }
                    var agency = agencyRepository.GetAgencyOnly(note.AgencyId, agencySnapshot.ClusterId);
                    if (agency != null)
                    {
                        note.ContactNumber = agency.ContactPersonPhoneFormatted;
                    }
                }
                return PartialView("Note/View", note);
            }
            return PartialView("Note/View", new CustomerNote { });
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteList(Guid agencyId)
        {
            var viewData = new AgencyCenterViewData();
            viewData.AgencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            var categoryPermissions = Current.CategoryPermissions(ParentPermission.Note);
            if (categoryPermissions != null && categoryPermissions.Count > 0)
            {
                viewData.IsUserCanAdd = categoryPermissions.Contains((int)PermissionActions.Add);
                viewData.IsUserCanExport = categoryPermissions.Contains((int)PermissionActions.Export);
                viewData.IsUserCanViewDetail = categoryPermissions.Contains((int)PermissionActions.ViewDetail);
                viewData.IsUserCanEdit = categoryPermissions.Contains((int)PermissionActions.Edit);
                viewData.IsUserCanDelete = categoryPermissions.Contains((int)PermissionActions.Delete);
            }
            return PartialView("Note/List", viewData);
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.ViewList)]
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteGrid(Guid agencyId)
        {
            IList<CustomerNote> customerNotes = new List<CustomerNote>();
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null)
            {
                customerNotes = customerNotesService.GetCustomerNotes(agencyId, agencySnapshot.ClusterId) ?? new List<CustomerNote>();
            }
            //var notes = customerNotesRepository.GetCustomerNotes(agencyId);
            //notes.ForEach(n =>
            //{
            //    n.RepName = supportRepository.GetLoginDisplayName(n.LoginId);
            //    n.IsEditable = Current.LoginId == n.LoginId;
            //});
            return View(new GridModel(customerNotes));
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteEdit(Guid id, Guid agencyId)
        {
            var note = customerNotesRepository.GetCustomerNote(agencyId, id);
            if (note != null)
            {
                var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
                if (agencySnapshot != null)
                {
                    note.RepName = supportRepository.GetLoginDisplayName(note.LoginId);
                    note.RecipientArray = note.Recipients.IsNotNullOrEmpty() ? note.Recipients.ToObject<List<Guid>>() : new List<Guid>();
                  
                    note.SalesPersonName = supportRepository.GetLoginDisplayName(agencySnapshot.SalesPerson);
                    note.TrainerName = supportRepository.GetLoginDisplayName(agencySnapshot.Trainer);
                    note.BackupTrainerName = supportRepository.GetLoginDisplayName(agencySnapshot.BackupTrainer);
                   
                    note = customerNotesService.GetAdditionalInformation(note);
                    if (note.RecipientArray.Count > 0)
                    {
                        note.SendAsMessage = true;
                    }
                    if (note.AttachmentId != Guid.Empty)
                    {
                        var asset = assetRepository.Get(note.AttachmentId, note.AgencyId);
                        if (asset != null)
                        {
                            note.AttachmentName = asset.FileName;
                            note.HasAttachment = true;
                        }
                    }
                     var agency = agencyRepository.GetAgencyOnly(agencyId, agencySnapshot.ClusterId);
                     if (agency != null)
                     {
                         note.ContactNumber = agency.ContactPersonPhoneFormatted;
                     }
                }
                return PartialView("Note/Edit", note);
            }
            return PartialView("Note/Edit", new CustomerNote { });
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.Edit)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteUpdate([Bind] CustomerNote note)
        {
            Check.Argument.IsNotNull(note, "note");
            var viewData = new JsonViewData();
            if (note.IsValid)
            {
                if (note != null)
                {
                    var button = Request.Form["buttonClicked"];
                    if (!button.IsNullOrEmpty())
                    {
                        if (button == "saveButton")
                        {
                            if (customerNotesService.UpdateNote(note, Request.Files))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Note was saved successfully";
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Error in saving the Note.";
                            }
                        }
                        else if (button == "completeButton")
                        {
                            if (customerNotesService.SendNote(note, Request.Files))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Note was updated and sent successfully";
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Error in updating and sending the note.";
                            }
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = note.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.Delete)]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NoteDelete(Guid id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not delete this note." };
            var existingNote = customerNotesRepository.GetCustomerNote(agencyId, id);
            if (existingNote != null)
            {
                existingNote.IsDeprecated = true;
                if (customerNotesRepository.DeleteCustomerNote(existingNote))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Note has been deleted.";
                }
            }
            return Json(viewData, "text/plain");
        }

        [PermissionFilter(ParentPermission.Note, PermissionActions.Export)]
        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult NoteExport(Guid agencyId)
        {

            var agencyName = string.Empty;
            IList<CustomerNote> customerNotes = new List<CustomerNote>();
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null)
            {
                agencyName = agencySnapshot.Name;
                customerNotes =customerNotesService.GetCustomerNotes(agencyId, agencySnapshot.ClusterId) ?? new List<CustomerNote>();
            }
            var export = new CustomerNoteExporter(customerNotes, agencyName);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "CustomerNoteSummary.xls");
        }

      
        #endregion
    }
}
