﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Text;
    using System.Web.Mvc;

    using Services;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AssetController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAssetRepository assetRepository;

        public AssetController(ISupportManagementDataProvider supportManagementDataProvider)
        {
            Check.Argument.IsNotNull(supportManagementDataProvider, "supportManagementDataProvider");
            
            this.assetRepository = supportManagementDataProvider.AssetRepository;
        }

        #endregion

        #region AssetController Actions

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Serve(Guid assetId, Guid agencyId)
        //{
        //    var asset = assetRepository.Get(assetId, agencyId);
        //    return File(asset.Bytes, asset.ContentType, asset.FileName);
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Serve(Guid assetId, Guid agencyId)
        {
            var asset = assetRepository.Get(assetId, agencyId);
            if (asset != null)
            {
                asset.FileName = asset.FileName.Replace(",", "-").Replace(";", "-");
                if (SupportAppSettings.UseDBAssets)
                {
                    return File(asset.Bytes, asset.ContentType, asset.FileName);
                }
                else
                {
                    string key = SupportAppSettings.AssetKey.ToLower();
                    var date = DateTime.Now.AddMinutes(5).ToString("yyyyMMddHHmmss");
                    var hash = AssetHelper.CreateHash(date);
                    var url = string.Format(SupportAppSettings.AssetServeURL, asset.Id.ToString(), key, Convert.ToBase64String(Encoding.UTF8.GetBytes(asset.AgencyId.ToString())), date, hash, asset.FileName);
                    return Redirect(url);
                }
            }
            return Json(new JsonViewData { isSuccessful = false, errorMessage = "Asset could not be served" });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Delete(Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (assetRepository.Delete(assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }
        
        #endregion
    }
}
