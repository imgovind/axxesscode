﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.SupportApp.Filter;
    using Axxess.AgencyManagement.SupportApp.Enums;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MessageController : BaseController
    {
        #region Private Members/Constructor
        private readonly IUserRepository userRepository;
        private readonly IAccountingRepository accountingRepository;
        private readonly IMessageRepository messageRepository;
        private readonly ILoginRepository loginRepository;

        public MessageController(IAgencyManagementDataProvider agencyManagementDataProvider, IAgencyManagementMembershipDataProvider agencyManagementMembershipDataProvider, IMembershipDataProvider membershipDataProvider, IAccountingManagementDataProvider accountingManagementDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
            this.accountingRepository = accountingManagementDataProvider.AccountingRepository;
            this.loginRepository = agencyManagementMembershipDataProvider.LoginRepository;
        }
        #endregion

        #region Dashboard Messages

        [PermissionFilter(ParentPermission.DashboardMessage, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DashboardNew()
        {
            return PartialView("Dashboard/New");
        }

        [PermissionFilter(ParentPermission.DashboardMessage, PermissionActions.Add)]
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DashboardCreate([Bind] DashboardMessage message)
        {
            Check.Argument.IsNotNull(message, "message");
            var viewData = new JsonViewData();
            if (message.IsValid)
            {
                message.CreatedBy = Current.DisplayName;
                if (!messageRepository.AddDashboardMessage(message))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Dashboard message.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Dashboard message was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }
            return Json(viewData);
        }

        [PermissionFilter(ParentPermission.DashboardMessage, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DashboardPreview()
        {
            return PartialView("Dashboard/Preview");
        }

        #endregion

        #region System Messages

        [PermissionFilter(ParentPermission.SystemMessage, PermissionActions.Add)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SystemNew()
        {
            return PartialView("System/New");
        }

        [PermissionFilter(ParentPermission.SystemMessage, PermissionActions.Add)]
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SystemCreate([Bind] SystemMessage message)
        {
            Check.Argument.IsNotNull(message, "message");

            var viewData = new JsonViewData();
            if (message.IsValid)
            {
                message.CreatedBy = Current.DisplayName;
                if (!messageRepository.AddSystemMessage(message))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the system message.";
                }
                else if(SendMessage(message))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "System message was sent successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }
            return Json(viewData);
        }

        #endregion

        #region Private Methods

        //Fixing sending system messages
        [PermissionFilter(ParentPermission.SystemMessage, PermissionActions.Add)]
        private bool SendMessage(SystemMessage message)
        {
            var excludeList = new List<Guid>();
            var agencySnapshot = loginRepository.GetAgencySnapshot(message.AgencyId);
            IList<User> users = userRepository.GetUsersByRoleOrTitle(message.AgencyId, message.AgencyRoleId, message.TitleType, message.AgencyStateCode, agencySnapshot.ClusterId);
            var agencyIds = users.Select(s => s.AgencyId).Distinct().ToList();

            if (users != null)
            {
                if (message.Subject.IsEqual("Discounts on Annual Axxess Software Subscription"))
                {
                    //GetExistingAgencyUpgrades only gets the agency guid
                    List<AgencyUpgrade> agencyUpgradeList = accountingRepository.GetExistingAgencyUpgrades(agencyIds).ToList();
                    if (agencyUpgradeList != null && agencyUpgradeList.Count > 0)
                    {
                        excludeList = agencyUpgradeList.Select(a => a.AgencyId).Distinct().ToList();
                    }
                }
                List<User> recipients;
                var userMessages = GenerateUserMessages(users.ToList(), excludeList, message.Id, out recipients);
                if (messageRepository.AddUserMessages(userMessages))
                {
                    ThreadPool.QueueUserWorkItem(state => SendNotifications(recipients));
                    return true;
                }

            }
            return false;
        }

        private List<UserMessage> GenerateUserMessages(List<User> users, List<Guid> excludeList, Guid messageId, out List<User> recipients)
        {
            var userMessages = new List<UserMessage>();
            recipients = new List<User>();
            //Cannot use LINQ foreach due to adding to an out parameter
            foreach (var user in users)
            {
                if ((excludeList == null || excludeList.Count == 0) || !excludeList.Contains(user.AgencyId))
                {
                    if (user != null && user.FirstName.IsNotNullOrEmpty() && user.EmailAddress.IsNotNullOrEmpty())
                    {
                        userMessages.Add(new UserMessage
                        {
                            IsRead = false,
                            UserId = user.Id,
                            Id = Guid.NewGuid(),
                            IsDeprecated = false,
                            FolderId = Guid.Empty,
                            MessageId = messageId,
                            AgencyId = user.AgencyId,
                            MessageType = (int)MessageType.System
                        });
                        recipients.Add(user);
                    }
                }
            }
            return userMessages;

        }

        private void SendNotifications(List<User> users)
        {
            users.ForEach(user =>
            {
                SendNotification(user);
            });
        }

        private void SendNotification(User user)
        {
            var parameters = new string[4];
            parameters[0] = "recipientfirstname";
            parameters[1] = user.FirstName;
            parameters[2] = "senderfullname";
            parameters[3] = "Axxess Technology Solutions";
            var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, "Axxess Technology Solutions sent you a message.", bodyText);
        }

        #endregion

    }
}
