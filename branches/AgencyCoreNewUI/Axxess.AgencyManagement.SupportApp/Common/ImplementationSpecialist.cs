﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using System.IO;

    public sealed class ImplementationSpecialist
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.Initialize();
            }

            internal static readonly ImplementationSpecialist instance = new ImplementationSpecialist();
        }

        #endregion

        #region Private Members

        private int limit;
        private string directory;
        private Dictionary<int, SupportLogin> dictionary;

        #endregion

        #region Public Instance

        public static ImplementationSpecialist Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private ImplementationSpecialist()
        {
            directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "implementationspecialist.txt");
            if (!File.Exists(directory))
            {
                File.Create(directory);
            }
        }

        private void Initialize()
        {
            this.dictionary = new Dictionary<int, SupportLogin>();
            this.limit = SupportAppSettings.ImplementationSpecialists.Count;

            var supportUsers = Container.Resolve<IMembershipDataProvider>().SupportRepository.GetAllByRole(Axxess.Membership.Enums.Roles.AxxessAdmin);
            if (supportUsers != null && supportUsers.Count > 0)
            {
                int count = 1;
                supportUsers.ForEach(user =>
                {
                    if (SupportAppSettings.ImplementationSpecialists.Contains(user.EmailAddress.ToLower()))
                    {
                        dictionary.Add(count, user);
                        count++;
                    }
                });

                var next = File.ReadAllText(directory);
                if (next.IsNullOrEmpty())
                {
                    var nextId = new Random().Next(1, this.limit);
                    using (var textWriter = new StreamWriter(directory, false))
                    {
                        textWriter.Write(nextId);
                    }
                }
            }
        }

        #endregion

        #region Public Methods

        public Guid Next()
        {
            var result = Guid.Empty;
            var next = File.ReadAllText(directory);
            if (next.IsNotNullOrEmpty())
            {
                var nextId = next.ToInteger() + 1;
                if (nextId > limit)
                {
                    nextId = 1;
                }

                var login = dictionary[nextId];
                if (login != null)
                {
                    result = login.Id;

                    using (var textWriter = new StreamWriter(directory, false))
                    {
                        textWriter.Write(nextId);
                    }
                }
            }
            return result;
        }

        public string NextName()
        {
            var result = string.Empty;
            var next = File.ReadAllText(directory);
            if (next.IsNotNullOrEmpty())
            {
                var nextId = next.ToInteger() + 1;
                if (nextId > limit)
                {
                    nextId = 1;
                }

                var login = dictionary[nextId];
                if (login != null)
                {
                    result = login.DisplayName;

                    using (var textWriter = new StreamWriter(directory, false))
                    {
                        textWriter.Write(nextId);
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
