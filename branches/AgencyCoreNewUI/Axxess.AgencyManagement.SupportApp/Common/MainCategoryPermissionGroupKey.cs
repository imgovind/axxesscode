﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.SupportApp.Enums;

namespace Axxess.AgencyManagement.SupportApp
{
    public class MainCategoryPermissionGroupKey
    {
        public string Name { get; set; }
        public bool IsAllSelected { get; set; }
        public bool HasSubCategories { get; set; }
        public int NumberOfPermissions { get; set; }

        public MainCategoryPermissionGroupKey(string name, bool hasSubCategories)
        {
            this.Name = name;
            this.HasSubCategories = hasSubCategories;
        }

        public override bool Equals(object obj)
        {
            if (obj is MainCategoryPermissionGroupKey)
            {
                return (obj as MainCategoryPermissionGroupKey).Name == this.Name;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
