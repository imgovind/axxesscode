﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using Axxess.AgencyManagement.Domain;
using System.Web;
using Axxess.Core.Extension;
namespace Axxess.AgencyManagement.SupportApp
{
    public static class AssetHelper
    {

        public static string CreateHash(string date)
        {
            string secret = SupportAppSettings.AssetSecret.ToLower();
            string key = SupportAppSettings.AssetKey.ToLower();

            var hmacsha256 = new HMACSHA256(Encoding.UTF8.GetBytes(key));
            var secDate = secret + date;
            hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(secDate));
            string sig = "";
            foreach (byte character in hmacsha256.Hash)
            {
                sig += character.ToString("X2");
            }
            var sigBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(sig));
            sig = sigBase64;
            return sig;
        }

        public static bool HttpUpload(string url, Asset asset, byte[] bytes)
        {
            var result = false;
            try
            {
                byte[] temp = bytes;
                string paramName = "file";
                string key = SupportAppSettings.AssetKey.ToLower();
                var sig = AssetHelper.CreateHash("");
                var nvc = new NameValueCollection { { "key", key }, { "sig", sig }, { "assetId", asset.Id.ToString() }, { "agencyId", asset.AgencyId.ToString() } };

                string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
                byte[] boundaryBytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                webRequest.Method = "POST";

                Stream requestStream = webRequest.GetRequestStream();

                string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
                foreach (string name in nvc)
                {
                    requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                    string formItem = string.Format(formdataTemplate, name, nvc[name]);
                    byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(formItem);
                    requestStream.Write(formItemBytes, 0, formItemBytes.Length);
                }
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);

                string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
                string header = string.Format(headerTemplate, paramName, asset.FileName, asset.ContentType);
                byte[] headerBytes = System.Text.Encoding.UTF8.GetBytes(header);
                requestStream.Write(headerBytes, 0, headerBytes.Length);

                requestStream.Write(temp, 0, temp.Length);

                byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
                requestStream.Write(trailer, 0, trailer.Length);
                requestStream.Close();

                WebResponse webResponse = null;
                try
                {
                    webResponse = webRequest.GetResponse();
                    using (Stream responseStream = webResponse.GetResponseStream())
                    {
                        using (StreamReader responseReader = new StreamReader(responseStream))
                        {
                            var text = responseReader.ReadToEnd();
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (webResponse != null)
                    {
                        webResponse.Close();
                        webResponse = null;
                    }
                    result = false;
                }
                finally
                {
                    webRequest = null;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }


        public static Byte [] GetUpload(Guid agencyId, Guid assetId,string fileName)
        {
            var bytes = new Byte[1];
            try
            {
                string key = SupportAppSettings.AssetKey.ToLower();
                var date = DateTime.Now.AddMinutes(5).ToString("yyyyMMddHHmmss");
                var hash = AssetHelper.CreateHash(date);
                var url = string.Format(SupportAppSettings.AssetServeURL, assetId.ToString(), key, Convert.ToBase64String(Encoding.UTF8.GetBytes(agencyId.ToString())), date, hash, fileName);

                var encoding = new ASCIIEncoding();
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                var response = (HttpWebResponse)request.GetResponse();
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    var receiveStream = response.GetResponseStream();
                    var encode = System.Text.Encoding.GetEncoding("utf-8");
                    var readStream = new StreamReader(receiveStream, encode);
                    var strResult = readStream.ReadToEnd();
                    bytes = Encoding.UTF8.GetBytes(strResult);
                    return bytes;
                }
            }
            catch (Exception ex)
            {
               
                return bytes;
            }
            return bytes;
        }


        public static Asset CreateAsset(Guid agencyId, HttpFileCollectionBase httpFiles)
        {
            Asset asset = null;
            if (httpFiles != null && httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Attachment1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = agencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };

                    }
                }
            }
            return asset;
        }
            

    }
}
