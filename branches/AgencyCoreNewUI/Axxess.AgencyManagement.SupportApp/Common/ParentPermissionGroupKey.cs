﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.SupportApp.Enums;

namespace Axxess.AgencyManagement.SupportApp
{
   public class ParentPermissionGroupKey
    {
        private string name;
        public string Name
        {
            get
            {
                if (name.IsNullOrEmpty() && Enum.IsDefined(typeof(ParentPermission), Id))
                {
                    name = ((ParentPermission)Id).GetDescription();
                }
                return name;
            }
        }

        public int Id { get; set; }
        public int AllSelected { get; set; }
        public bool IsAllSelected { get; set; }

        public ParentPermissionGroupKey(int id)
        {
            this.Id = id;
        }

        public override bool Equals(object obj)
        {
            if (obj is ParentPermissionGroupKey)
            {
                return (obj as ParentPermissionGroupKey).Id == this.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
