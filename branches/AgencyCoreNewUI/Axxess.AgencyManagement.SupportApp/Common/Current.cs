﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Web;
    using System.Reflection;
    using System.Security.Principal;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;

    using Axxess.AgencyManagement.Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    public static class Current
    {
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();
        public static Func<DateTime> Time = () => DateTime.UtcNow;

        public static AxxessSupportIdentity User
        {
            get
            {
                AxxessSupportIdentity identity = null;
                if (HttpContext.Current.User is WindowsPrincipal)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessSupportPrincipal)
                {
                    AxxessSupportPrincipal principal = (AxxessSupportPrincipal)HttpContext.Current.User;
                    identity = (AxxessSupportIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DisplayName;
                }

                return string.Empty;
            }
        }

        public static string IpAddress
        {
            get
            {
                var ipAddress = string.Empty;

                if (HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR").IsNotNullOrEmpty())
                {
                    ipAddress = HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
                }
                else
                {
                    ipAddress = HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR");
                }
                return ipAddress;
            }
        }


        public static bool IsAxxessAdmin
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.IsAxxessAdmin;
                }
                return false;
            }
        }

        public static bool IsAxxessSupport
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.IsAxxessSupport;
                }
                return false;
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            }
        }

        public static bool IsIpAddressRestricted
        {
            get
            {
                var result = true;

                if (Current.IpAddress.StartsWith("10.0.1")
                    || Current.IpAddress.StartsWith("97.79.164.66")
                    || Current.IpAddress.StartsWith("10.0.5")
                    || Current.IpAddress.IsEqual("127.0.0.1"))
                {
                    result = false;
                }
                return result;
            }
        }

        public static List<SupportUser> ActiveUsers
        {
            get
            {
                return authenticationAgent.GetSupportUsers(true);
            }
        }

        public static List<SupportUser> InActiveUsers
        {
            get
            {
                return authenticationAgent.GetSupportUsers(false);
            }
        }

        public static Dictionary<int, List<int>> Permissions
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.Permissions;
                }
                else
                {
                    return new Dictionary<int, List<int>>();
                }
            }
        }

        public static bool HasRight(ParentPermission category, PermissionActions action)
        {
            bool result = false;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }
            if (User != null && User.Session != null)
            {
                if (User.Session.Permissions != null && User.Session.Permissions.Count > 0)
                {
                    var categoryList = new List<int>();
                    if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
                    {
                        result = categoryList.Contains((int)action);
                    }
                }
            }
            return result;
        }

        public static bool HasRightAtLeastOne(ParentPermission category, PermissionActions[] actions)
        {
            bool result = false;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (User != null && User.Session != null)
            {
                if (User.Session.Permissions != null && User.Session.Permissions.Count > 0)
                {
                    var categoryList = new List<int>();
                    if (User.Session.Permissions.TryGetValue((int)category, out categoryList))
                    {
                        foreach (var action in actions)
                        {
                            if (categoryList.Contains((int)action))
                            {
                                result = true;
                                break;

                            }
                        }
                    }
                }
            }
            return result;
        }



        public static List<int> CategoryPermissions(ParentPermission category)
        {
            var result = new List<int>();
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (User != null && User.Session != null)
            {
                if (User.Session.Permissions != null && User.Session.Permissions.Count > 0)
                {
                    User.Session.Permissions.TryGetValue((int)category, out result);
                }
            }
            return result;
        }

    }
}
