﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class SupportCustomer
    {
        /// <summary>
        /// Login Id
        /// </summary>
        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public List<IGrouping<ParentPermissionGroupKey, PermissionRow>> Permissions { get; set; }

    }
}
