﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class SupportAppSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static string ImpersonateUrl
        {
            get
            {
                return configuration.AppSettings["ImpersonateUrl"];
            }
        }

        public static string LiveImpersonateUrl
        {
            get
            {
                return configuration.AppSettings["LiveImpersonateUrl"];
            }
        }

        public static string ImpersonatePhysicianUrl
        {
            get
            {
                return configuration.AppSettings["ImpersonatePhysicianUrl"];
            }
        }

        public static string LiveImpersonatePhysicianUrl
        {
            get
            {
                return configuration.AppSettings["LiveImpersonatePhysicianUrl"];
            }
        }

        public static string RememberMeCookie
        {
            get
            {
                return configuration.AppSettings["RememberMeCookie"];
            }
        }

        public static int RememberMeForTheseDays
        {
            get
            {
                return configuration.AppSettings["RememberMeForTheseDays"].ToInteger();
            }
        }

        public static string AuthenticationType
        {
            get
            {
                return configuration.AppSettings["AuthenticationType"];
            }
        }

        public static bool UsePersistentCookies
        {
            get
            {
                return configuration.AppSettings["UsePersistentCookies"].ToBoolean();
            }
        }

        public static List<string> ImplementationSpecialists
        {
            get
            {
                return configuration.AppSettings["ImplementationSpecialists"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }

        public static bool UseDBAssets { get { return configuration.AppSettings["UseDBAssets"].ToBoolean(); } }
        public static bool StoreAssetBytesInDB { get { return configuration.AppSettings["StoreAssetBytesInDB"].ToBoolean(); } }
        public static string AssetKey { get { return configuration.AppSettings["AssetKey"]; } }
        public static string AssetUploadURL { get { return configuration.AppSettings["AssetUploadURL"]; } }
        public static string AssetServeURL { get { return configuration.AppSettings["AssetServeURL"]; } }
        public static string AssetSecret { get { return configuration.AppSettings["AssetSecret"]; } }
    }
}
