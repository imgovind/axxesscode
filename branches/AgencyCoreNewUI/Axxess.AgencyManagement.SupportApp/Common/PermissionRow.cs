﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.SupportApp
{
  public  class PermissionRow
    {
        public string Description { get; set; }
        public string ToolTip { get; set; }
        public int ChildId { get; set; }
        public int Checked { get; set; }
        public bool IsChecked { get; set; }
    }
}
