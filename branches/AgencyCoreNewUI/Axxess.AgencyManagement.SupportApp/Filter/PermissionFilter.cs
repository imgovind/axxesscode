﻿namespace Axxess.AgencyManagement.SupportApp.Filter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.SupportApp.Enums;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;
    using System.Net;
    public class PermissionFilter : ActionFilterAttribute
    {
        private ParentPermission Category { get; set; }
        private PermissionActions Action { get; set; }
        private ParentPermission[] MultiCategory { get; set; }
        private List<PermissionActions[]> MultiAction { get; set; }
        private bool IsMultipleAction { get; set; }
        private bool IsMultipleCategory { get; set; }

        public PermissionFilter(ParentPermission Category, PermissionActions Action)
        {
            this.Category = Category;
            this.Action = Action;
            this.IsMultipleAction = false;
            this.IsMultipleCategory = false;
        }

        public PermissionFilter(ParentPermission Category, PermissionActions[] Actions)
        {
            this.Category = Category;
            this.MultiAction = new List<PermissionActions[]>();
            this.MultiAction.Add(Actions);
            this.IsMultipleAction = true;
            this.IsMultipleCategory = false;
        }

        /// <summary>
        /// the the index of the category will corresponde with the index of the action list
        /// It is used to check if the user has the permission in the list of category and corresponding actions
        /// </summary>
        /// <param name="Service"></param>
        /// <param name="Categories"></param>
        /// <param name="Actions"></param>
        public PermissionFilter(ParentPermission[] Categories, params object[] Actions)
        {
            this.MultiAction = new List<PermissionActions[]>();
            foreach (PermissionActions[] a in Actions)
            {
                this.MultiAction.Add(a);
            }
            this.MultiCategory = Categories;
            this.IsMultipleAction = true;
            this.IsMultipleCategory = true;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!this.IsMultipleCategory)
                {
                    if (!this.IsMultipleAction)
                    {
                        if (Current.HasRight(this.Category, this.Action))
                        {
                            return;
                        }
                        else
                        {
                            FilterHelper(filterContext);
                            return;
                        }
                    }
                    else
                    {
                        if (Current.HasRightAtLeastOne(this.Category, this.MultiAction.FirstOrDefault()))
                        {
                            return;
                        }
                        else
                        {
                            FilterHelper(filterContext);
                            return;
                        }
                    }
                }
                else
                {
                    var hasPermission = false;
                    var count = 1;
                    var actionCount = this.MultiAction.Count;
                    foreach (var catagory in this.MultiCategory)
                    {
                        if (count <= actionCount)
                        {
                            if (Current.HasRightAtLeastOne(catagory, this.MultiAction[count - 1]))
                            {
                                hasPermission = true;
                                break;
                            }
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (hasPermission)
                    {
                        return;
                    }
                    else
                    {
                        FilterHelper(filterContext);
                        return;
                    }
                }
            
        }

        private static void FilterHelper(ActionExecutingContext filterContext)
        {
            var result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = new JsonViewData { isSuccessful = false, errorMessage = "You don't have enough permission to perform this action." };
            filterContext.HttpContext.Response.StatusDescription = "Unauthorized action.";
            filterContext.Result = result;
            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return;
        }
    }
}
