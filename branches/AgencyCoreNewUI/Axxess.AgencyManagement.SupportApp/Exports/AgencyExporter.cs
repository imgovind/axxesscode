﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AgencyExporter : BaseExporter
    {
        private List<AgencyLite> agencies;
        public AgencyExporter(List<AgencyLite> agencies)
            : base()
        {
            this.agencies = agencies;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agencies";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Agencies");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue("");
            titleRow.CreateCell(1).SetCellValue("Agency List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Contact");
            headerRow.CreateCell(2).SetCellValue("Phone");
            headerRow.CreateCell(3).SetCellValue("Location");
            headerRow.CreateCell(4).SetCellValue("Created");

            if (this.agencies.Count > 0)
            {
                int i = 2;
                this.agencies.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.Name);
                    dataRow.CreateCell(1).SetCellValue(a.ContactPersonDisplayName);
                    dataRow.CreateCell(2).SetCellValue(a.ContactPersonPhoneFormatted);
                    dataRow.CreateCell(3).SetCellValue(a.Location);
                    dataRow.CreateCell(4).SetCellValue(a.Created.ToString("MM/dd/yyy"));
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 1);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agencies: {0}", agencies.Count));
            }

            base.workBook.FinishWritingToExcelSpreadsheet(4);
            
        }
    }
}
