﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class UpgradeExporter : BaseExporter
    {
        private IList<AgencyUpgrade> agencyUpgrade;
        private DateTime startDate;
        private DateTime endDate;
        public UpgradeExporter(IList<AgencyUpgrade> agencyUpgrade, DateTime startDate, DateTime endDate)
            : base()
        {
            this.agencyUpgrade = agencyUpgrade;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Upgrade";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("AgencyUpgrade");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue("");
            titleRow.CreateCell(1).SetCellValue("Upgrade Requests");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.startDate.ToString("MM/dd/yyyy"), this.endDate.ToString("MM/dd/yyyy")));
            

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Agency Name");
            headerRow.CreateCell(1).SetCellValue("Location Name");
            headerRow.CreateCell(2).SetCellValue("Old Subscription Plan");
            headerRow.CreateCell(3).SetCellValue("New Subscription Plan");
            headerRow.CreateCell(4).SetCellValue("Requested By");
            headerRow.CreateCell(5).SetCellValue("Requested Date");
            headerRow.CreateCell(6).SetCellValue("Old Pricing");
            headerRow.CreateCell(7).SetCellValue("New Pricing");
            headerRow.CreateCell(8).SetCellValue("Completed");

            if (this.agencyUpgrade.Count > 0)
            {
                int i = 2;
                this.agencyUpgrade.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.AgencyName);
                    dataRow.CreateCell(1).SetCellValue(a.LocationName);
                    dataRow.CreateCell(2).SetCellValue(a.PreviousPackageDescription);
                    dataRow.CreateCell(3).SetCellValue(a.RequestedPackageDescription);
                    dataRow.CreateCell(4).SetCellValue(a.RequestedBy);
                    dataRow.CreateCell(5).SetCellValue(a.CreatedDateFormatted);
                    dataRow.CreateCell(6).SetCellValue(a.PreviousAmount);
                    dataRow.CreateCell(7).SetCellValue(a.Amount);
                    dataRow.CreateCell(8).SetCellValue(a.IsComplete? "Yes" : "No");
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 1);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Upgrade Requests: {0}", agencyUpgrade.Count));
            }

            base.workBook.FinishWritingToExcelSpreadsheet(9);
        }
    }
}
