﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class MarkAsReadOnlyExporter : BaseExporter
    {
        private IList<AgencyMarkAsReadOnly> AgencyMarkAsReadOnly;
        private DateTime startDate;
        private DateTime endDate;
        public MarkAsReadOnlyExporter(IList<AgencyMarkAsReadOnly> AgencyMarkAsReadOnly, DateTime startDate, DateTime endDate)
            : base()
        {
            this.AgencyMarkAsReadOnly = AgencyMarkAsReadOnly;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Mark as Read-only";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("AgencyMarkAsReadOnly");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue("");
            titleRow.CreateCell(1).SetCellValue("Mark as Read-only");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.startDate.ToString("MM/dd/yyyy"), this.endDate.ToString("MM/dd/yyyy")));
            

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Agency Name");
            headerRow.CreateCell(1).SetCellValue("Suspend Payment?");
            headerRow.CreateCell(2).SetCellValue("Frozen?");
            headerRow.CreateCell(3).SetCellValue("Effective Date");
            headerRow.CreateCell(4).SetCellValue("Frozen Reason");
            headerRow.CreateCell(5).SetCellValue("Created Date");
            if (this.AgencyMarkAsReadOnly.Count > 0)
            {
                int i = 2;
                this.AgencyMarkAsReadOnly.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.AgencyName);
                    dataRow.CreateCell(1).SetCellValue(a.SuspendPayment);
                    dataRow.CreateCell(2).SetCellValue(a.Frozen);
                    dataRow.CreateCell(3).SetCellValue(a.EffectiveDateFormatted);
                    dataRow.CreateCell(4).SetCellValue(a.FrozenReason);
                    dataRow.CreateCell(5).SetCellValue(a.CreateDateFormatted);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 1);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Mark as Read-only: {0}", AgencyMarkAsReadOnly.Count));
            }

            base.workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
