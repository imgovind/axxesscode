﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;
    using Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AccountingExporter : BaseExporter
    {
        private AccountingPaymentViewData accountingPaymentViewData;

        public AccountingExporter(AccountingPaymentViewData accountingPaymentViewData)
            : base()
        {
            this.accountingPaymentViewData = accountingPaymentViewData;
           
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Accounting";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Accounting");
            
            int titleNumber = accountingPaymentViewData.Titles.Count;
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue("");
            titleRow.CreateCell(1).SetCellValue("Accounting Payment Information");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Account Number");
            headerRow.CreateCell(1).SetCellValue(" Account Name");
            headerRow.CreateCell(2).SetCellValue("Zip");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Initial Sign Date");
            headerRow.CreateCell(6).SetCellValue("First Billable Date");
            headerRow.CreateCell(7).SetCellValue("Payment Source");
            headerRow.CreateCell(8).SetCellValue("StartUp");
            headerRow.CreateCell(9).SetCellValue("Status");
            headerRow.CreateCell(10).SetCellValue("Sales Person");
            headerRow.CreateCell(11).SetCellValue("Special Notes");

            for (var n = 0; n < titleNumber; n++)
            { 
                headerRow.CreateCell(12 + n).SetCellValue(accountingPaymentViewData.Titles[n]);
            }
            headerRow.CreateCell(12 + titleNumber).SetCellValue("Total");

            if (accountingPaymentViewData.AgencyPaymentData.Count > 0)
            {

                int i = 2;
                var currencyStyle = workBook.CreateCellStyle();
                currencyStyle.DataFormat = (short)8;
                this.accountingPaymentViewData.AgencyPaymentData.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.AccountId);
                    dataRow.CreateCell(1).SetCellValue(a.AgencyName);
                    dataRow.CreateCell(2).SetCellValue(a.ZipCode);
                    dataRow.CreateCell(3).SetCellValue(a.City);
                    dataRow.CreateCell(4).SetCellValue(a.State);
                    dataRow.CreateCell(5).SetCellValue(a.InitialSignDate);
                    dataRow.CreateCell(6).SetCellValue(a.FirstBillableDate);
                    dataRow.CreateCell(7).SetCellValue(a.PaymentSource);
                    dataRow.CreateCell(8).SetCellValue(a.StartUp == 1? "Yes" : "No");
                    dataRow.CreateCell(9).SetCellValue(a.Status ==1? "Active" : "Inactive");
                    dataRow.CreateCell(10).SetCellValue(a.SalesPerson);
                    dataRow.CreateCell(11).SetCellValue(a.SpecialNotes);
                    for (var t = 0; t < titleNumber; t++)
                    {
                        var pay = a.Payments[t].ToCurrency();
                        var cell = dataRow.CreateCell(12 + t);
                        cell.CellStyle = currencyStyle;
                        cell.SetCellValue(pay); 
                    }
                    var total = a.Total.ToCurrency();
                    var cellTotal = dataRow.CreateCell(12 + titleNumber);
                    cellTotal.CellStyle = currencyStyle;
                    cellTotal.SetCellValue(total); 
                    i++;
                });
                var x = 12;
                var y = 0;
                var lastRow = sheet.CreateRow(i+1);
                lastRow.CreateCell(1).SetCellValue("Total " + accountingPaymentViewData.AgencyPaymentData.Count + " Accounts");
                this.accountingPaymentViewData.ListOfTotalAmounts.ForEach(a =>
                {
                    var totalAmount = accountingPaymentViewData.ListOfTotalAmounts[y].ToCurrency();
                    var last = lastRow.CreateCell(x);
                    last.CellStyle = currencyStyle;
                    last.SetCellValue(totalAmount);
                    y++;
                    x++;
                });
                var maxTotal = accountingPaymentViewData.BigTotal.ToCurrency();
                var max = lastRow.CreateCell(accountingPaymentViewData.ListOfTotalAmounts.Count + 12);
                max.CellStyle = currencyStyle;
                max.SetCellValue(maxTotal); 
            }

                base.workBook.FinishWritingToExcelSpreadsheet(13 + titleNumber);
        }
    }
}
