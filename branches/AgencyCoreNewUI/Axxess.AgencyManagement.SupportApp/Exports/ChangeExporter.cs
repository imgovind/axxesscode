﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;
    using Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class ChangeExporter : BaseExporter
    {
        private IList<AgencyChange> agencyChange;
        private DateTime startDate;
        private DateTime endDate;
        public ChangeExporter(IList<AgencyChange> agencyChange, DateTime startDate, DateTime endDate)
            : base()
        {
            this.agencyChange = agencyChange;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Change";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("AgencyChange");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue("");
            titleRow.CreateCell(1).SetCellValue("Company Information Change");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.startDate.ToString("MM/dd/yyyy"), this.endDate.ToString("MM/dd/yyyy")));
            

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Agency Name");
            headerRow.CreateCell(1).SetCellValue("Requested By");
            headerRow.CreateCell(2).SetCellValue("Requested Date");
            headerRow.CreateCell(3).SetCellValue("Completed");


            if (this.agencyChange.Count > 0)
            {
                int i = 2;
                this.agencyChange.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.Name);
                    dataRow.CreateCell(1).SetCellValue(a.FullName);
                    dataRow.CreateCell(2).SetCellValue(a.CreatedDateFormatted);
                    dataRow.CreateCell(3).SetCellValue(a.Complete);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 1);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Company Information Change Requests: {0}", agencyChange.Count));
            }

            base.workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
