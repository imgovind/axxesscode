﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;
    using Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AccountingMetricsExporter : BaseExporter
    {
        private AccountingMetricsViewData accountingMetricsViewData;

        public AccountingMetricsExporter(AccountingMetricsViewData accountingMetricsViewData)
            : base()
        {
            this.accountingMetricsViewData = accountingMetricsViewData;
           
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Accounting Metrics";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Accounting");

            int titleNumber = accountingMetricsViewData.Months.Count;
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue("");
            titleRow.CreateCell(1).SetCellValue("Accounting Metrics Information");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("AXXESS Metrics");
            for (var n = 0; n < titleNumber; n++)
            {
                headerRow.CreateCell(1 + n).SetCellValue(accountingMetricsViewData.Months[n]);
            }
            headerRow.CreateCell(1 + titleNumber).SetCellValue("Total");

            if (accountingMetricsViewData.MetricsRows.Count > 0)
            {

                int i = 2;
                var currencyStyle = workBook.CreateCellStyle();
                currencyStyle.DataFormat = (short)8;
                var percentageStyle = workBook.CreateCellStyle();
                percentageStyle.DataFormat = (short)0xa;
               
               
                this.accountingMetricsViewData.MetricsRows.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.Name);

                    for (var t = 0; t < titleNumber; t++)
                    {

                        if (a.Data[t].Contains("%"))
                        {
                            var pay = a.Data[t].Replace("%", "").ToDouble() / 100.0;
                            var cell = dataRow.CreateCell(1 + t);
                            cell.CellStyle = percentageStyle;
                            cell.SetCellValue(pay);
                        }
                        else if (a.Data[t].Contains("$"))
                        {

                            var pay = a.Data[t].Replace("$", "").Replace("(", "-").Replace(")", "").Replace(",", "").ToDouble();
                            var cell = dataRow.CreateCell(1 + t);
                            cell.CellStyle = currencyStyle;
                            cell.SetCellValue(pay);
                        }
                        else
                        {
                            var pay = a.Data[t].ToInteger();
                            var cell = dataRow.CreateCell(1 + t);
                            cell.SetCellValue(pay);
                        }
                    }
                    if(a.Total != null)
                    {
                        var total = a.Total.ToInteger();
                        var cellTotal = dataRow.CreateCell(1 + titleNumber);
                        cellTotal.SetCellValue(total);
                    }
                    i++;
                });
            }

                base.workBook.FinishWritingToExcelSpreadsheet(2 + titleNumber);
        }
    }
}
