﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Extensions;
    using Axxess.Core.Extension;
    using Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    class CustomerNoteExporter : BaseExporter
    {
        private IList<CustomerNote> notes;
        private string agencyName;
        public CustomerNoteExporter(IList<CustomerNote> notes, string agencyName)
            : base()
        {
            this.notes = notes;
            this.agencyName = agencyName;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Customer Notes";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Customer Notes");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titalRow = sheet.CreateRow(0);
            titalRow.CreateCell(0).SetCellValue(agencyName);
            titalRow.CreateCell(1).SetCellValue("Customer Notes");
            titalRow.CreateCell(2).SetCellValue(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            

            Row headerRow = sheet.CreateRow(2);
            headerRow.CreateCell(0).SetCellValue("Rep Name");
            headerRow.CreateCell(1).SetCellValue("Summary");
            headerRow.CreateCell(2).SetCellValue("Note Type");
            headerRow.CreateCell(3).SetCellValue("Sent");
            headerRow.CreateCell(4).SetCellValue("Time");
            headerRow.CreateCell(5).SetCellValue("Preview");

            titalRow.RowStyle = headerStyle;
            var f = headerStyle.GetFont(base.workBook);
            f.FontHeightInPoints = 10;
            headerStyle.SetFont(f);

            headerRow.RowStyle = headerStyle;

            if (this.notes.Count > 0)
            {
                int i = 3;
                this.notes.ForEach(n =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(n.RepName);
                    dataRow.CreateCell(1).SetCellValue(n.Summary);
                    dataRow.CreateCell(2).SetCellValue(n.NoteTypeName);
                    dataRow.CreateCell(3).SetCellValue(n.IsCompleted? "Yes":"No");
                    dataRow.CreateCell(4).SetCellValue(n.CreatedFormatted);
                    dataRow.CreateCell(5).SetCellValue(n.Preview);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Customer Notes: {0}", notes.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
        }
    }
}
