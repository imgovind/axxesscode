﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Web;
    using System.Text;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Collections.Generic;

    using Axxess.LookUp;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Extensions;

    public static class HtmlHelperExtensions
    {
       

        private static ISupportRepository supportRepository
        {
            get
            {
                IMembershipDataProvider dataProvider = Container.Resolve<IMembershipDataProvider>();
                return dataProvider.SupportRepository;
            }
        }

        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        private static IUserRepository userRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.UserRepository;
            }
        }

        //private static IAssetRepository assetRepository
        //{
        //    get
        //    {
        //        IAgencyManagementDataProvider dataProvider = Container.Resolve<I>();
        //        return dataProvider.AssetRepository;
        //    }
        //}

        private static ILoginRepository loginRepository
        {
            get
            {
                IAgencyManagementMembershipDataProvider dataProvider = Container.Resolve<IAgencyManagementMembershipDataProvider>();
                return dataProvider.LoginRepository;
            }
        }

        //public static MvcHtmlString Asset(this HtmlHelper html, Guid assetId, CustomerNote note)
        //{
        //    var sb = new StringBuilder();
        //    if (!assetId.IsEmpty())
        //    {
        //        var asset = assetRepository.Get(assetId, note.AgencyId);
        //        if (asset != null)
        //        {
        //            sb.AppendFormat("<a class=\"link\" href=\"/Asset/{0}/{2}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName, note.AgencyId);
        //        }
        //    }

        //    return MvcHtmlString.Create(sb.ToString());
        //}

        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes, bool allowAll)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var agencies = loginRepository.All();
            tempItems = from agency in agencies
                        select new SelectListItem
                        {
                            Text = agency.Name,
                            Value = agency.Id.ToString(),
                            Selected = (agency.Id.ToString().IsEqual(value))
                        };

            items = tempItems.OrderBy(i => i.Text).ToList();
            if (allowAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All Agencies --",
                    Value = Guid.Empty.ToString()
                });
            }
            else
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- Select Agency --",
                    Value = Guid.Empty.ToString()
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, Guid agencyId, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null && agencySnapshot.ClusterId > 0)
            {
                IEnumerable<SelectListItem> tempItems = null;
                var branches = agencyRepository.GetBranches(agencyId, agencySnapshot.ClusterId);
                if (branches != null && branches.Count > 0)
                {
                    tempItems = from branch in branches
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                }
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectList(this HtmlHelper html, SelectListTypes listType, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.States:
                    var states = lookupRepository.States();
                    tempItems = from state in states
                                select new SelectListItem
                                {
                                    Text = state.Name,
                                    Value = state.Code,
                                    Selected = (state.Code.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select State --",
                        Value = " "
                    });
                    break;
                case SelectListTypes.CahpsVendors:
                    var cahpsVendors = new Dictionary<string, int>();
                    Array cahpsVendorValues = Enum.GetValues(typeof(CahpsVendors));
                    foreach (CahpsVendors cahpsVendor in cahpsVendorValues)
                    {
                        cahpsVendors.Add(cahpsVendor.GetDescription(), (int)cahpsVendor);
                    }

                    tempItems = from type in cahpsVendors
                                select new SelectListItem
                                {
                                    Text = type.Key,
                                    Value = type.Value.ToString(),
                                    Selected = (type.Value.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.LicenseTypes:
                    var licenseTypes = new List<string>();
                    Array licenseTypeValues = Enum.GetValues(typeof(LicenseTypes));
                    foreach (LicenseTypes licenseType in licenseTypeValues)
                    {
                        licenseTypes.Add(licenseType.GetDescription());
                    }

                    tempItems = from type in licenseTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select License Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.CredentialTypes:
                    var credentialTypes = new List<string>();
                    Array credentialValues = Enum.GetValues(typeof(CredentialTypes));
                    foreach (CredentialTypes credentialType in credentialValues)
                    {
                        credentialTypes.Add(credentialType.GetDescription());
                    }

                    tempItems = from type in credentialTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Unknown --",
                        Value = ""
                    });
                    break;
                case SelectListTypes.TitleTypes:
                    var titleTypes = new List<string>();
                    Array titleValues = Enum.GetValues(typeof(TitleTypes));
                    foreach (TitleTypes titleType in titleValues)
                    {
                        titleTypes.Add(titleType.GetDescription());
                    }

                    tempItems = from type in titleTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Title Type --",
                        Value = "",
                    });
                    break;
                case SelectListTypes.Payors:
                    var payors = lookupRepository.MedicareIntermediaries();
                    if (payors != null && payors.Count > 0)
                    {
                        tempItems = from payor in payors
                                    select new SelectListItem
                                    {
                                        Text = string.Format("{0} [{1}]", payor.Name, payor.Code),
                                        Value = payor.Id.ToString(),
                                        Selected = (payor.Id.ToString().IsEqual(value))
                                    };
                        items = tempItems.ToList();
                    }
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Medicare Intermediary --",
                        Value = " "
                    });
                    break;
                case SelectListTypes.PermissionTemplate:
                    var PermissionTemplates = supportRepository.GetTemplate();
                    tempItems = from template in PermissionTemplates
                                select new SelectListItem
                                {
                                    Text = template.Name,
                                    Value = template.Permissions,
                                    Selected = (template.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Template --",
                        Value = " "
                    });
                    break;
                default:
                    break;
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString CustomerNoteTypeSelector(this HtmlHelper html, string name, string value, object htmlAttributes, bool allowRecall, bool allowAll)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var noteTypes = new Dictionary<string, int>();
            Array noteTypeValues = Enum.GetValues(typeof(CustomerNoteType));
            foreach (CustomerNoteType noteType in noteTypeValues)
            {
                noteTypes.Add(noteType.GetDescription(), (int)noteType);
            }
            tempItems = from type in noteTypes
                        select new SelectListItem
                        {
                            Text = type.Key,
                            Value = type.Value.ToString(),
                            Selected = (type.Value.ToString().IsEqual(value))
                        };
            items = tempItems.OrderBy(x => x.Text).ToList();
            if (!allowRecall)
            {
                items.RemoveAll(i => i.Text == CustomerNoteType.Recalled.GetDescription());
            }
            if (allowAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All --",
                    Value = ""
                });
            }
            else
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- Select Type --",
                    Value = "",
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Users(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = supportRepository.GetAllByRole(Roles.AxxessAdmin);
            users = users.OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select User --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString ImplementationSpecialists(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var implementationSpecialists = SupportAppSettings.ImplementationSpecialists;
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = supportRepository.GetAllByRole(Roles.AxxessAdmin);
            users = users.OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        where implementationSpecialists.Contains(user.EmailAddress)
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select User --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AgencyUsers(this HtmlHelper html, Guid agencyId, string name, string value, object htmlAtrributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null && agencySnapshot.ClusterId > 0)
            {
                var users = userRepository.GetAgencyUsers(agencyId, agencySnapshot.ClusterId);
                users = users.OrderBy(u => u.DisplayName).ToList();
                tempItems = from user in users
                            select new SelectListItem
                            {
                                Text = user.DisplayName,
                                Value = user.Id.ToString(),
                                Selected = (user.Id.ToString().IsEqual(value))
                            };
                items = tempItems.ToList();
                items.Insert(0, new SelectListItem
                {
                    Text = "-- Select User --",
                    Value = Guid.Empty.ToString()
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAtrributes);
            
        }

        public static MvcHtmlString Recipients(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"recipient-list\" id=\"recipient-list\">");
            sb.AppendLine("<div class=\"compose-header\">Recipient List</div>");
            sb.AppendLine("<div class=\"recipient-panel\">");
            sb.AppendLine("<div class=\"recipient\">");
            sb.AppendLine("<input type=\"checkbox\" class=\"contact\" id=\"NewMessage_SelectAllRecipients\" />");
            sb.AppendLine("<label for=\"NewMessage_SelectAllRecipients\" class=\"strong\">Select All</label>");
            sb.AppendLine("</div>"); // recipient
            var recipients = supportRepository.GetAllByRole(Roles.AxxessAdmin);
            if (recipients.Count > 0)
            {
                int counter = 1;
                recipients.ForEach(r =>
                {
                    if (r.Id != Current.LoginId)
                    {
                        sb.AppendFormat("<div class=\"recipient{0}\">", counter % 2 == 1 ? " t-alt" : string.Empty);
                        sb.AppendFormat("<input name=\"Recipients\" type=\"checkbox\" id=\"NewMessage_Recipient_{0}\" value=\"{1}\" title=\"{2}\" />", counter.ToString(), r.Id.ToString(), r.DisplayName);
                        sb.AppendFormat("<label for=\"NewMessage_Recipient_{0}\">{1}</label>", counter.ToString(), r.DisplayName);
                        sb.AppendLine("</div>");
                        sb.AppendLine();
                        counter++;
                    }
                });
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html, string prefix, List<Guid> selectedRecipients)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"checkgroup\">");
            var recipients = supportRepository.GetAllByRole(Roles.AxxessAdmin);
            recipients = recipients.OrderBy(l => l.DisplayName).ToList();
            if (recipients.Count > 0)
                for (int i = 0; i < recipients.Count; i++) sb.AppendFormat(
                    "<div class=\"option\"><input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" title=\"{3}\"{4} /><label for=\"{0}_Recipient{1}\">{3}</label></div>",
                    prefix, i.ToString(), recipients[i].Id.ToString(), recipients[i].DisplayName, selectedRecipients.Contains(recipients[i].Id) ? " checked='checked'" : string.Empty);
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString LoginDisplayName(this HtmlHelper html, Guid login)
        {
            string name = supportRepository.GetLoginDisplayName(login);
            return MvcHtmlString.Create(name);
        }

        public static MvcHtmlString AgencyServicesDropDown(this HtmlHelper html, string name, AgencyServices value, object htmlAttributes)
        {
            var list = new List<SelectListItem>(){
                new SelectListItem { Text = "Home Health", Value = ((int)AgencyServices.HomeHealth).ToString(), Selected = value.Has(AgencyServices.HomeHealth) },
                new SelectListItem { Text = "Private Duty", Value = "2", Selected = value.Has(AgencyServices.PrivateDuty) },
                new SelectListItem { Text = "Therapy", Value = "4", Selected = value.Has(AgencyServices.Therapy) }
            };

            return html.SelectList(name, list.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString SelectList(this HtmlHelper html, string name, IEnumerable<SelectListItem> items, object htmlAttributes)
        {
            StringBuilder builder = new StringBuilder();
            var classValue = htmlAttributes.GetProperty<string>("class");
            var idValue = htmlAttributes.GetProperty<string>("id");
            var styleValue = htmlAttributes.GetProperty<string>("style");
            var multipleValue = htmlAttributes.GetProperty<string>("multiple");
            var multipleValueString = multipleValue.IsNotNullOrEmpty() ? string.Format("multiple='{0}'", multipleValue) : string.Empty;
            builder.AppendFormat("<select class='{0}' id='{1}' name='{2}' style='{3}' {4}>\r\n", classValue, idValue, name, styleValue, multipleValueString);
            foreach (var item in items)
            {
                builder.Append(RenderOption(item.Value, item.Text, item.Selected));
            }
            builder.AppendFormat("</select>");
            return MvcHtmlString.Create(builder.ToString());
        }

        private static string RenderOption(string value, string text, bool isSelected)
        {
            return string.Format("<option value='{0}' {1}>{2}</option>\r\n", value, isSelected ? "selected='selected'" : "", text);
        }

        public static MvcHtmlString AgencyRoles(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array agencyRoleValues = Enum.GetValues(typeof(AgencyRoles));
            foreach (AgencyRoles agencyRole in agencyRoleValues)
            {
                items.Add(new SelectListItem
                {
                    Text = agencyRole.GetDescription(),
                    Value = ((int)agencyRole).ToString()
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency Role --",
                Value = "0",
                Selected = true
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString TitleTypes(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array titleTypeValues = Enum.GetValues(typeof(TitleTypes));
            foreach (TitleTypes titleType in titleTypeValues)
            {
                items.Add(new SelectListItem
                {
                    Text = titleType.GetDescription(),
                    Value = titleType.GetDescription()
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Title --",
                Value = "",
                Selected = true
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString AgencyStates(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var states = lookupRepository.States();
            foreach (var state in states)
            {
                items.Add(new SelectListItem
                {
                    Text = state.Name,
                    Value = state.Code
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency State --",
                Value = string.Empty,
                Selected = true
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
       
        public static MvcHtmlString Months(this HtmlHelper html, string name, string value, int start, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            List<SelectListItem> tempItems = new List<SelectListItem>();

            int currentYear = DateTime.Now.Year;
            if (start < currentYear)
            {
                for (int val = start; val <= currentYear; val++)
                {
                    tempItems.Add(new SelectListItem
                    {
                        Text = val.ToString(),
                        Value = val.ToString(),
                        Selected = (val.ToString().IsEqual(value))
                    });
                }
            }
            tempItems.Reverse();
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
        
        public static MvcHtmlString Years(this HtmlHelper html, string name, string value, int start, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            List<SelectListItem> tempItems = new List<SelectListItem>();

            int yearList = DateTime.Now.AddYears(1).Year;
            if (start < yearList)
            {
                for (int val = start; val <= yearList; val++)
                {
                    tempItems.Add(new SelectListItem
                    {
                        Text = val.ToString(),
                        Value = val.ToString(),
                        Selected = (val.ToString().IsEqual(value))
                    });
                }
            }
            tempItems.Reverse();
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
    }
}
