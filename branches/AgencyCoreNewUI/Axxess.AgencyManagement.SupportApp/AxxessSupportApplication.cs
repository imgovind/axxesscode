﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Logging;

    using Security;
    using Extensions;

    public class AxxessSupportApplication : HttpApplication
    {
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        public AxxessSupportApplication()
        {
            this.AcquireRequestState += new EventHandler(AxxessApplication_AcquireRequestState);
        }

        protected void AxxessApplication_AcquireRequestState(object sender, EventArgs e)
        {
            if (Context.Handler is MvcHandler
               && Context.User != null
               && Context.User.Identity.IsAuthenticated)
            {
                ISupportMembershipService membershipService = Container.Resolve<ISupportMembershipService>();
                IFormsAuthenticationService formAuthService = Container.Resolve<IFormsAuthenticationService>();

                var userName = Context.User.Identity.Name;
                AxxessSupportPrincipal principal = membershipService.Get(userName);

                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;

                    if (authenticationAgent.VerifySupportuser(principal.ToSupportUser()))
                    {
                        return;
                    }
                }
                membershipService.LogOff(userName);
                formAuthService.SignOut();
                formAuthService.RedirectToLogin();
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                Logger.Exception(exception);
            }
        }
    }
}