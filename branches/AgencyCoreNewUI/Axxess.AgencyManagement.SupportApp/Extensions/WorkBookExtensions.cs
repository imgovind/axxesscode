﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;


namespace Axxess.Core.Extension
{
    public static class WorkBookExtensions
    {
        /*
         * Loops through all of the cells within a HSSFWorkbook and adjusts the font size.
         * Does not automatically adjust the width of the columns and does not set borders.
         */
        public static void FinishWritingToExcelSpreadsheet(this HSSFWorkbook workBook, int columns)
        {
            Font headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Font normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            CellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyle.SetFont(normalFont);

            Sheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    Row row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        for(int i = 0; i< row.PhysicalNumberOfCells ; i++)
                        {
                            var cell = row.GetCell(i);
                            if (rowCount == 0)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (rowCount < 2)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {
                                cell.CellStyle = normalStyle;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                        }
                    }
                }
                Row totalRow = sheet.GetRow(numberOfRows + 1);
                numberOfRows += totalRow != null ? 0 : 1;
                totalRow = (totalRow != null ? totalRow : sheet.GetRow(numberOfRows + 1));
                if (totalRow != null)
                {
                    numberOfRows++;
                    var totalCell = totalRow.GetCell(0);
                    totalCell.CellStyle = normalStyle;
                }
                sheet.AutoResizeColumns(columns);
            }
        }

        public static void AutoResizeColumns(this Sheet sheet, int columns)
        {
            int resizeColCounter = 0;
            do
            {
                sheet.AutoSizeColumn(resizeColCounter);
                resizeColCounter++;
            }
            while (resizeColCounter < columns);
            sheet.PrintSetup.Landscape = true;
            sheet.PrintSetup.FitHeight = 0;
            sheet.FitToPage = true;
        }

        private static string GetCellValue(this Cell cell)
        {
            string result = "";
            if (cell is HSSFCell)
            {
                HSSFCell hssfCell = cell as HSSFCell;
                
                switch (hssfCell.CellType)
                {
                    case CellType.STRING:
                        result = hssfCell.StringCellValue;
                        break;
                    case CellType.NUMERIC:
                        result = hssfCell.NumericCellValue.ToString();
                        break;
                }
            }
            return result;
        }
    }
}
