﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;
    using System.Collections.Generic;

    public interface IUserService
    {
        bool CreateUser(User user, int clusterId);
        bool IsEmailAddressInUse(string emailAddress);
        IList<UserSelection> GetUsersWithLoginInfo(Guid agencyId);
    }
}
