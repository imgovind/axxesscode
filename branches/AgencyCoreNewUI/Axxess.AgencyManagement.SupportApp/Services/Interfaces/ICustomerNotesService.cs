﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Domain;
    using System.Web;

    public interface ICustomerNotesService
    {
       
        IList<CustomerNote> GetRecentNotes(DateTime startDate,DateTime endDate);
        IList<CustomerNote> GetRecentNotes();
        bool SendNote(CustomerNote note, HttpFileCollectionBase httpFiles);
        bool SaveNote(CustomerNote note, HttpFileCollectionBase httpFiles, bool isSend);
        bool UpdateNote(CustomerNote note, HttpFileCollectionBase httFiles);
        CustomerNote GetAdditionalInformation(CustomerNote note);
        IList<CustomerNote> GetCustomerNotes(Guid agencyId, int clusterId);
    }
}
