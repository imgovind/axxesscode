﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    
    using Axxess.AgencyManagement.Domain;
    using Axxess.Log.Domain;
    using Axxess.Log.Enums;

    using ViewData;

    public interface IAgencyService
    {
        bool CreateAgency(Agency agency);
        bool UpdateAgency(Agency agency);
        bool CreateLocation(AgencyLocation location);
        bool UpdateLocation(AgencyLocation location);
        List<AgencyLocationViewData> GetBranches(Guid agencyId);
        string AddFrozenAsset(Guid agencyId, HttpFileCollectionBase httpFiles);
        List<SupportAppAudit> GetSupportLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId);
        List<AgencyLite> GetAllAgencies(string stateFilter, DateTime? startDate, DateTime? endDate, int clusterId);

        IList<AgencyUpgrade> GetAgencyUpgrades(string isComplete, DateTime startDate, DateTime endDate, int clusterId);
        IList<AgencyUpgrade> GetAgencyAnnualPlan(string isComplete, DateTime startDate, DateTime endDate, int clusterId);
        IList<AgencySpecialist> GetImplementationSpecialists(Guid implementationSpecialistId, DateTime startDate, DateTime endDate);
        IList<AgencyMarkAsReadOnly> GetAgencyMarkAsReadOnly(DateTime startDate, DateTime endDate);
    }
}
