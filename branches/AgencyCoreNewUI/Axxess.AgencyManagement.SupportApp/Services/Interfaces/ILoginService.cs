﻿namespace Axxess.AgencyManagement.SupportApp.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Log.Domain;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.SupportApp.ViewData;
    using Axxess.Core.Infrastructure;
    using Axxess.Membership.Domain;

    public interface ILoginService
    {
        List<SupportAppAudit> GetSupportLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId);
        InternalUserViewData GetUserPermissionsOnly(Guid loginId);
        Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>> GetNewInternalUser();
        JsonViewData UpdateInternalUser(SupportLogin login, Dictionary<int, List<int>> permissions);
        PermissionTemplateViewData GetUsePermissionsTemplates(int Id);
    }
}
