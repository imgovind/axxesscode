﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Domain;
    using System.Web;

    public interface IAccountingService
    {
        AccountingPaymentViewData CalculateDateRange(DateTime start, DateTime end);
        AccountingPaymentViewData CalculateWeekly(DateTime start, DateTime end);
        AccountingPaymentViewData CalculateMonthly(DateTime start, DateTime end);
        AccountingPaymentViewData CalculateYearly(DateTime start, DateTime end);

        EndingMRRViewData CalculateEndingMRR(DateTime start, DateTime end);
        EndingMRRViewData CalculateMRRBooking(DateTime start, DateTime end);
        AccountingMetricsViewData CalculateMetrics(DateTime start, DateTime end);
    }
}
