﻿namespace Axxess.AgencyManagement.SupportApp.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Log.Domain;
    using Axxess.Log.Repositories;
    using Axxess.Log.Enums;
    using Axxess.Membership.Repositories;
    using Axxess.Core;

    public class LoginService : ILoginService
    {
        private readonly ILoginRepository loginRepository;
        private readonly ILogRepository logRepository;

        public LoginService( IMembershipDataProvider membershipDataProvider,ILogDataProvider logDataProvider)
        {
            Check.Argument.IsNotNull(logDataProvider, "logDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.logRepository = logDataProvider.LogRepository;
        }


        public List<SupportAppAudit> GetSupportLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var supportLogs = new List<SupportAppAudit>();
            var logs = logRepository.GetSupportAudits(logDomain.ToString(), logType.ToString(), domainId, entityId);
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = loginRepository.GetLoginDisplayName(log.UserId);
                    supportLogs.Add(log);
                });
            }
            return supportLogs;
        }
    }
}
