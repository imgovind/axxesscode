﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Membership.Repositories;
    using Axxess.Core.Infrastructure;
    using System.Diagnostics;
    using Axxess.AgencyManagement.Enums;
    using System.IO;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Redmine.Net.Api.Types;
    using Redmine.Net.Api;
    using System.Threading;
    using System.Collections.Specialized;
    using Axxess.Membership.Logging;
    using System.Web;
    using Axxess.Membership.Domain;

    public class CustomerNotesService : ICustomerNotesService
    {
        #region Constructor

        private readonly ICustomerNotesRepository customerNotesRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IUserRepository userRepository;
        private readonly ISupportRepository supportRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ILoginRepository loginRepository;
        private readonly RedmineManager redmineManager;

        public CustomerNotesService(ISupportManagementDataProvider supportManagementDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAgencyManagementMembershipDataProvider agencyManagementMembershipDataProvider)
        {
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.customerNotesRepository = supportManagementDataProvider.CustomerNotesRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = supportManagementDataProvider.AssetRepository;
            this.loginRepository = agencyManagementMembershipDataProvider.LoginRepository;
            redmineManager = new RedmineManager(CoreSettings.RedmineHost, CoreSettings.RedmineAPIKey, false);
        }
        #endregion
        
        #region ICustomerNotesService Members

        public IList<CustomerNote> GetRecentNotes(DateTime startDate,DateTime endDate)
        {
            List<CustomerNote> customerNotes = null;

            customerNotes = customerNotesRepository.GetRecentCustomerNotes(startDate, endDate).OrderByDescending(c => c.Created).ToList();
            return customerNotes;
        }

        public IList<CustomerNote> GetRecentNotes()
        {
            string cacheKey = "recentCustomerNotes";
            List<CustomerNote> customerNotes = null;
            if (InProcCacher.TryGet<List<CustomerNote>>(cacheKey, out customerNotes))
            {
                var lastAdded = customerNotesRepository.GetLastAddedTime();
                if (lastAdded != customerNotes.OrderByDescending(n => n.Created).First().Created)
                {
                    customerNotes = customerNotesRepository.GetActiveCustomerNotes().OrderByDescending(c => c.Created).ToList();
                    InProcCacher.Set<List<CustomerNote>>(cacheKey, customerNotes);
                }
            }
            else
            {
                customerNotes = customerNotesRepository.GetActiveCustomerNotes().OrderByDescending(c => c.Created).ToList();
                InProcCacher.Set<List<CustomerNote>>(cacheKey, customerNotes);
            }
            if (customerNotes != null && customerNotes.Count > 0)
            {
                var loginIds = new List<Guid>();
                var agencyIds = new List<Guid>();
                customerNotes.ForEach(c => { if (!c.LoginId.IsEmpty()) { loginIds.Add(c.LoginId); } if (!c.AgencyId.IsEmpty()) { agencyIds.Add(c.AgencyId); } });
                loginIds = loginIds.Distinct().ToList();
                var loginNames = supportRepository.GetLoginDisplayNamesWithEmail(loginIds);
                agencyIds = agencyIds.Distinct().ToList();
                var agencyNames = loginRepository.GetAgencySnapshotNames(agencyIds);
                customerNotes.ForEach(n =>
                {
                    var agency = agencyNames.FirstOrDefault(l => l.Id == n.AgencyId);
                    if (agency != null)
                    {
                        n.AgencyName = agency.Name;
                    }
                    var login = loginNames.FirstOrDefault(l => l.Id == n.LoginId);
                    if (login != null)
                    {
                        n.RepName = login.DisplayName;
                    }
                });
            }
            return customerNotes;
        }

        #endregion

        #region notes and asset

        public bool SendNote(CustomerNote note, HttpFileCollectionBase httpFiles)
        {
            var existingNote = customerNotesRepository.GetCustomerNote(note.AgencyId, note.Id);
            if (existingNote == null)
            {
                return false;
            }

            if (note.NoteType == (int)CustomerNoteType.Recalled)
            {
                return false;
            }
            existingNote.IsCompleted = true;
            note.Saved = false;
            bool result = false;
            if (note.RecipientArray != null && note.RecipientArray.Count > 0)
            {
                existingNote.Recipients = note.RecipientArray.ToXml();
            }
            existingNote.AdditionalInformation = GetAdditionalInformationBody(note);
            existingNote.NoteType = note.NoteType;
            existingNote.CallerId = note.CallerId;
            existingNote.Summary = note.Summary;
            existingNote.Comments = note.Comments;
            existingNote.Modified = DateTime.Now;
            Upload upload = null;
            var assetByte = new Byte[1];
            Asset asset = AssetHelper.CreateAsset(existingNote.AgencyId, httpFiles);
            if (asset != null)
            {
                assetByte = asset.Bytes;
                AddAsset(asset);
                if (existingNote.AttachmentId != Guid.Empty)
                {
                   // var agencySnapshot = agencyRepository.GetAgencySnapshot(note.AgencyId);
                    assetRepository.RemoveAsset(existingNote.AttachmentId);
                }
                existingNote.AttachmentId = asset.Id;

                upload = GetUpload(assetByte, asset.ContentType, existingNote.NoteType);
            }
            else
            {
                if (existingNote.IssueId <= 0)
                {
                    if (!existingNote.AttachmentId.IsEmpty())
                    {
                        //var agencySnapshot = agencyRepository.GetAgencySnapshot(note.AgencyId);
                        asset = assetRepository.Get(existingNote.AttachmentId, existingNote.AgencyId);
                        if (asset != null)
                        {
                            var bytes = AssetHelper.GetUpload(note.AgencyId, asset.Id, asset.FileName);
                            upload = GetUpload(bytes, asset.ContentType, existingNote.NoteType);
                        }
                    }
                }
            }

            existingNote.AdditionalInformation = GetAdditionalInformationBody(note);
            existingNote.SendAsMessage = note.SendAsMessage;
            Send(existingNote, upload, input => (customerNotesRepository.UpdateCustomerNote(input)), note.RecipientArray, Current.LoginId);

            result = true;
            return result;
        }

        private void Send(CustomerNote existingNote, Upload upload, Func<CustomerNote, bool> check, List<Guid> recipients, Guid loginId)
        {
            if (check(existingNote))
            {
                new Thread(delegate(object login)
                {
                    var isNoteUpdate = false;
                    var issueId = 0;

                    if (existingNote.IssueId > 0)
                    {

                        try
                        {
                            var ticket = redmineManager.GetObject<Issue>(existingNote.IssueId.ToString(), new NameValueCollection { { "issue_id", "*" } });
                            if (Update(existingNote, ticket, upload))
                            {
                                issueId = existingNote.IssueId;
                            }
                        }
                        catch (Exception)
                        {
                            issueId = Add(existingNote, upload);
                            if (issueId > 0)
                            {
                                isNoteUpdate = true;
                            }
                        }

                    }
                    else
                    {

                        issueId = Add(existingNote, upload);
                        if (issueId > 0)
                        {
                            isNoteUpdate = true;
                        }
                    }
                    if (issueId == 0 || issueId == -1)
                    {
                        existingNote.Status = 255;
                    }
                    if (isNoteUpdate)
                    {
                        customerNotesRepository.UpdateCustomerNoteIssueAndStatus(existingNote.AgencyId, existingNote.Id, issueId, existingNote.Status);
                    }
                    if (existingNote.SendAsMessage)
                    {
                        string subject = existingNote.Summary;
                        string body = GetMessageBody(loginId, existingNote);
                        if (recipients != null && recipients.Count > 0)
                        {
                            recipients.ForEach(r =>
                            {
                                Notify.User(CoreSettings.NoReplyEmail, supportRepository.GetLoginEmailAddress(r), subject, body);
                            });
                        }
                    }

                    //if (existingNote.IssueId > 0)
                    //{
                    //    redmineManager.DeleteObject<Issue>(existingNote.IssueId.ToString(), null);
                    //}


                }).Start(loginId);
            }
        }

        public bool SaveNote(CustomerNote note, HttpFileCollectionBase httpFiles, bool isSend)
        {
            bool result = false;
            note.Saved = true;
            if (note.RecipientArray != null && note.RecipientArray.Count > 0)
            {
                note.Recipients = note.RecipientArray.ToXml();
            }
            Asset asset = AssetHelper.CreateAsset(note.AgencyId, httpFiles);
            var assetBytes = new Byte[1];
            if (asset != null)
            {
                assetBytes = asset.Bytes;
                AddAsset(asset);
                note.AttachmentId = asset.Id;
            }
            note.AdditionalInformation = GetAdditionalInformationBody(note);
            note.Status = (int)NoteStatus.New;
            if (!isSend)
            {
                if (customerNotesRepository.AddCustomerNote(note))
                {
                    result = true;
                }
            }
            else
            {
                Upload upload = null;
                if (asset != null)
                {
                    upload = GetUpload(assetBytes ?? new Byte[1], asset.ContentType, note.NoteType);
                }
                Send(note, upload, input => (customerNotesRepository.AddCustomerNote(input)), note.RecipientArray, Current.LoginId);
                result = true;
            }
            return result;
        }

        public bool UpdateNote(CustomerNote note, HttpFileCollectionBase httpFiles)
        {
            var existingNote = customerNotesRepository.GetCustomerNote(note.AgencyId, note.Id);
            if (existingNote == null)
            {
                return false;
            }
            bool result = false;
            if (note.RecipientArray != null && note.RecipientArray.Count > 0)
            {
                existingNote.Recipients = note.RecipientArray.ToXml();
            }
            existingNote.AdditionalInformation = GetAdditionalInformationBody(note);
            existingNote.NoteType = note.NoteType;
            existingNote.CallerId = note.CallerId;
            existingNote.Summary = note.Summary;
            existingNote.Comments = note.Comments;
            existingNote.Modified = DateTime.Now;


            note.Saved = false;
            Upload upload = null;
            var assetBytes = new Byte[1];
            Asset asset = AssetHelper.CreateAsset(existingNote.AgencyId, httpFiles);
            if (asset != null)
            {
                assetBytes = asset.Bytes;
                AddAsset(asset);
                if (existingNote.AttachmentId != Guid.Empty)
                {
                    //var agencySnapshot = agencyRepository.GetAgencySnapshot(existingNote.AgencyId);
                    assetRepository.RemoveAsset(existingNote.AttachmentId);
                }
                existingNote.AttachmentId = asset.Id;
                if (existingNote.IssueId > 0)
                {
                    upload = GetUpload(assetBytes, asset.ContentType, existingNote.NoteType);
                }
            }
            
            try
            {
                if (existingNote.IssueId > 0)
                {
                    UpdateIssue(existingNote, upload);
                }

            }
            catch (Exception ex) { }
            if (customerNotesRepository.UpdateCustomerNote(existingNote))
            {
                if (existingNote.IssueId > 0)
                {
                    existingNote.RecipientArray = !existingNote.Recipients.IsNullOrEmpty() ? existingNote.Recipients.ToObject<List<Guid>>() : new List<Guid>();
                    string body = GetMessageBody(Current.LoginId, existingNote);
                    string subject = note.Summary;

                    if (note.RecipientArray != null && note.RecipientArray.Count > 0)
                    {
                        foreach (var recipient in note.RecipientArray)
                        {
                            if (!existingNote.RecipientArray.Contains(recipient))
                            {
                                Notify.User(CoreSettings.NoReplyEmail, supportRepository.GetLoginEmailAddress(recipient), subject, body);
                            }
                        }
                    }
                }
                result = true;
            }
            return result;
        }

        public CustomerNote GetAdditionalInformation(CustomerNote note)
        {
            string additionalInformation = note.AdditionalInformation;
            if (additionalInformation.IsNotNullOrEmpty())
            {
                var strArray = additionalInformation.Split('\n');
                if (strArray.Length > 2)
                {
                    note.PatientName = strArray[0].Split(':').Last().Trim();
                    note.EpisodePeriod = strArray[1].Split(':').Last().Trim();
                    note.VisitType = strArray[2].Split(':').Last().Trim();
                    note.DateofVisit = strArray[3].Split(':').Last().Trim();
                }
                else if (note.NoteType == (int)CustomerNoteType.DataLoad)
                {
                    note.PreviousSoftware = strArray[0].Split(':').Last().Trim();
                }
                if (strArray.Length > 2 && note.NoteType == (int)CustomerNoteType.DataLoad)
                {
                    note.PreviousSoftware = strArray[4].Split(':').Last().Trim();
                }
            }
            return note;
        }

        public IList<CustomerNote> GetCustomerNotes(Guid agencyId, int clusterId)
        {
            var customerNotes = customerNotesRepository.GetCustomerNotes(agencyId);
            if (customerNotes != null && customerNotes.Count > 0)
            {
                var internalUserIds = new List<Guid>();
                var agencyUsersIds = new List<Guid>();
                customerNotes.ForEach(n => { if (!n.LoginId.IsEmpty()) { internalUserIds.Add(n.LoginId); } if (!n.CallerId.IsEmpty()) { agencyUsersIds.Add(n.CallerId); } });
                internalUserIds = internalUserIds.Distinct().ToList();
                agencyUsersIds = agencyUsersIds.Distinct().ToList();
                var supports = internalUserIds.Count > 0 ? supportRepository.GetLoginDisplayNamesWithEmail(internalUserIds) ?? new List<SupportLogin>() : new List<SupportLogin>();
                var users = agencyUsersIds.Count > 0 ? userRepository.GetUsersWithLoginInfo(agencyId, agencyUsersIds, clusterId) ?? new List<Axxess.AgencyManagement.Domain.User>() : new List<Axxess.AgencyManagement.Domain.User>();
                customerNotes.ForEach(n =>
                {
                    if (!n.LoginId.IsEmpty())
                    {
                        var support = supports.FirstOrDefault(s => s.Id == n.LoginId);
                        if (support != null)
                        {
                            n.RepName = support.DisplayName;
                        }
                        n.IsEditable = Current.LoginId == n.LoginId;
                    }
                    if (!n.CallerId.IsEmpty())
                    {
                        var user = users.FirstOrDefault(u => u.Id == n.CallerId);
                        if (user != null)
                        {
                            n.CallerName = user.DisplayName;
                        }
                    }
                });
            }
            return customerNotes;
        }

        #endregion

        #region Asset

        public bool AddAsset(Asset asset)
        {
            bool result = false;
            if (SupportAppSettings.UseDBAssets)
            {
                //var agencySnapshot = agencyRepository.GetAgencySnapshot(asset.AgencyId);
                if (assetRepository.Add(asset))
                {
                    result = true;
                }
            }
            else
            {
                if (HttpUpload(SupportAppSettings.AssetUploadURL, asset))
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion

        #region Private Methods

        private string GetMessageBody(Guid userId, CustomerNote note)
        {
            string endSubject = "";
            string body = "";
            var agencyName = "";
            var userName = "";
            var agency = loginRepository.GetAgencySnapshot(note.AgencyId);
            if (agency != null)
            {
                agencyName = agency.Name;

                if (userId != null && userId != Guid.Empty)
                {
                    userName = supportRepository.GetLoginDisplayName(userId);
                }
                if (note != null)
                {
                    var noteType = (CustomerNoteType)note.NoteType;
                    switch (noteType)
                    {
                        case CustomerNoteType.Bug:
                            endSubject = " reported a bug.";
                            break;
                        case CustomerNoteType.Enhancement:
                            endSubject = " requested an enhancement.";
                            break;
                        case CustomerNoteType.Feature:
                            endSubject = " requested a feature.";
                            break;
                        case CustomerNoteType.OnlineTraining:
                            endSubject = " commented about an online training.";
                            break;
                        case CustomerNoteType.OnsiteVisit:
                            endSubject = " commented about an onsite visit.";
                            break;
                        case CustomerNoteType.DataLoad:
                            endSubject = " requested a data load.";
                            break;
                    }
                    string callerName = "";
                    if (note.CallerId != null && note.CallerId != Guid.Empty)
                    {
                        //var agencySnapshot = agencyRepository.GetAgencySnapshot(note.AgencyId);
                        var caller = userRepository.GetUserForName(note.AgencyId, note.CallerId, agency.ClusterId);
                        if (caller != null)
                        {
                            callerName = caller.DisplayName;
                        }
                    }
                    string bodySubject = "";
                    if (callerName.IsNullOrEmpty())
                    {
                        bodySubject = string.Format("{0} {1}", agencyName, endSubject);
                    }
                    else
                    {
                        bodySubject = string.Format("{0} at {1} {2}", callerName, agencyName, endSubject);
                    }
                    body = string.Format("Submitted by: {0}\r\n{1}\r\n\r\n{2}", userName, bodySubject, note.Comments);
                }
            }
            return body;
        }

        private string GetAdditionalInformationBody(CustomerNote note)
        {
            string result = "";
            if (note.PatientName.IsNotNullOrEmpty() ||
                note.EpisodePeriod.IsNotNullOrEmpty() ||
                note.VisitType.IsNotNullOrEmpty() ||
                note.DateofVisit.IsNotNullOrEmpty())
            {
                string patientName = (note.PatientName.IsNullOrEmpty()) ? "" : note.PatientName;
                string episodePeriod = (note.EpisodePeriod.IsNullOrEmpty()) ? "" : note.EpisodePeriod;
                string visitType = (note.VisitType.IsNullOrEmpty()) ? "" : note.VisitType;
                string dateOfVisit = (note.DateofVisit.IsNullOrEmpty()) ? "" : note.DateofVisit;

                result = string.Format("Patient Name: {0}\r\nEpisode Period: {1}\r\nVisit Type: {2}\r\nDate of Visit: {3}", patientName, episodePeriod, visitType, dateOfVisit);
            }
            if (note.NoteType == (int)CustomerNoteType.DataLoad)
            {
                var previous = (note.PreviousSoftware.IsNullOrEmpty()) ? "" : note.PreviousSoftware;
                result += string.Format("Previous Software: {0}\r\n", previous);
            }
            return result;
        }

        private Upload GetUpload(byte[] assetLoad, string contentType, int noteType)
        {
            Upload upload = null;
            try
            {
                switch (noteType)
                {
                    case (int)CustomerNoteType.Feature:
                    case (int)CustomerNoteType.Bug:
                    case (int)CustomerNoteType.Enhancement:
                    case (int)CustomerNoteType.DataLoad:
                    case (int)CustomerNoteType.RemitAdvice:
                    case (int)CustomerNoteType.PrintDiscrepancy:
                    case (int)CustomerNoteType.AppleMobileApp:
                    case (int)CustomerNoteType.AndroidMobileApp:
                    case (int)CustomerNoteType.PrivateDuty:
                        upload = redmineManager.UploadData(assetLoad);
                        upload.ContentType = contentType;
                        // upload.FileName = fileName;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return upload;
        }

        private int SaveIssue(CustomerNote note, Upload upload)
        {
            int issueId = -1;
            if (IsValidNote(note.NoteType))
            {
                issueId = Add(note, upload);
            }
            return issueId;
        }

        private void UpdateIssue(CustomerNote note, Upload upload)
        {
            if (IsValidNote(note.NoteType))
            {
                string body = GetMessageBody(note.LoginId, note);
                Issue ticket = null;
                try
                {
                    ticket = redmineManager.GetObject<Issue>(note.IssueId.ToString(), new NameValueCollection { { "issue_id", "*" } });
                }
                catch (Exception)
                {
                    note.IssueId = SaveIssue(note, upload);
                }
                if (ticket != null)
                {
                    Update(note, ticket, upload);
                }
            }
        }

        private bool IsValidNote(int noteType)
        {
            switch (noteType)
            {
                case (int)CustomerNoteType.Feature:
                case (int)CustomerNoteType.Bug:
                case (int)CustomerNoteType.Enhancement:
                case (int)CustomerNoteType.DataLoad:
                case (int)CustomerNoteType.RemitAdvice:
                case (int)CustomerNoteType.PrintDiscrepancy:
                case (int)CustomerNoteType.Recalled:
                case (int)CustomerNoteType.AppleMobileApp:
                case (int)CustomerNoteType.AndroidMobileApp:
                case (int)CustomerNoteType.PrivateDuty:
                    return true;
                    break;
                default:
                    return false;
                    break;
            }
            return false;
        }

        private bool Update(CustomerNote note, Issue ticket, Upload upload)
        {
            var reuslt = false;
            try
            {
                string body = GetMessageBody(note.LoginId, note);
                var description = string.Format("{0}\r\n\r\n-- Additional Information --\r\n{1}", body, note.AdditionalInformation);
                var trackType = ((CustomerNoteType)note.NoteType).GetDescription();
                if (note.NoteType == 13 || note.NoteType == 14 || note.NoteType == 15)
                {
                    trackType = CustomerNoteType.Bug.GetDescription();
                }
                int trackerId = redmineManager.GetObjectList<Tracker>(new NameValueCollection { { "tracker_id", "*" } }).Where(t => t.Name == trackType).Select(t => t.Id).First();
                ticket.Description = description;
                ticket.Subject = note.Summary;
                ticket.Tracker = new IdentifiableName { Id = trackerId };
                if (ticket.Uploads != null && upload != null)
                {
                    ticket.Uploads.Add(upload);
                }
                else if (upload != null)
                {
                    ticket.Uploads = new List<Upload>();
                    ticket.Uploads.Add(upload);
                }
                redmineManager.UpdateObject<Issue>(ticket.Id.ToString(), ticket);
                reuslt = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return reuslt;
        }

        private int Add(CustomerNote note, Upload upload)
        {
            var issueId = 0;
            string body = GetMessageBody(note.LoginId, note);
            CustomerNoteType noteType = (CustomerNoteType)note.NoteType;
            string subject = note.Summary;
            var parameters = new NameValueCollection { { "project_id", "*" } };
            int projectId = -1;
            try
            {
                if (note.NoteType == 13)
                {
                    projectId = (from project in redmineManager.GetObjectList<Project>(parameters)
                                 where project.Name == RedMineProjects.IOSApp.GetDescription()
                                 select project.Id).First();
                    noteType = CustomerNoteType.Bug;
                }
                else if (note.NoteType == 14)
                {
                    projectId = (from project in redmineManager.GetObjectList<Project>(parameters)
                                 where project.Name == RedMineProjects.AndroidApp.GetDescription()
                                 select project.Id).First();
                    noteType = CustomerNoteType.Bug;
                }
                else if (note.NoteType == 15)
                {
                    projectId = (from project in redmineManager.GetObjectList<Project>(parameters)
                                 where project.Name == RedMineProjects.AgencyCoreV5.GetDescription()
                                 select project.Id).First();
                    noteType = CustomerNoteType.Bug;
                }
                else
                {
                    projectId = (from project in redmineManager.GetObjectList<Project>(parameters)
                                 where project.Name == RedMineProjects.AgencyCoreV4.GetDescription()
                                 select project.Id).First();
                }
            }
            catch (Exception) { }
            int trackerId = -1;
            var trackType = noteType.GetDescription();
            try
            {
                if ((int)noteType == 13 || (int)noteType == 14 || (int)noteType == 15)
                {
                    trackType = CustomerNoteType.Bug.GetDescription();
                }
                trackerId = (from tracker in redmineManager.GetObjectList<Tracker>(new NameValueCollection { { "tracker_id", "*" } })
                             where tracker.Name == trackType
                             select tracker.Id).First();
            }
            catch (Exception) { }
            if (projectId > -1 && trackerId > -1)
            {
                var description = string.Format("{0}\r\n\r\n-- Additional Information --\r\n{1}", body, note.AdditionalInformation);
                var issue = new Issue { Project = new IdentifiableName { Id = projectId }, Subject = subject, Description = description, Tracker = new IdentifiableName { Id = trackerId } };
                if (upload != null)
                {
                    issue.Uploads = new List<Upload>();
                    issue.Uploads.Add(upload);
                }
                try
                {
                    issueId = redmineManager.CreateIssue(issue);
                }
                catch (Exception ex)
                {
                    Logger.Exception(ex);
                    return issueId;
                }

            }
            return issueId;
        }

        private bool HttpUpload(string url, Asset asset)
        {
            var result = false;
            byte[] temp = asset.Bytes;

            if (!SupportAppSettings.StoreAssetBytesInDB)
            {
                asset.Bytes = new Byte[1];
            }
            //var agencySnapshot = agencyRepository.GetAgencySnapshot(asset.AgencyId);
            if (assetRepository.Add(asset))
            {
                result = AssetHelper.HttpUpload(url, asset, temp);
            }
            return result;
        }

        #endregion
    }
}
