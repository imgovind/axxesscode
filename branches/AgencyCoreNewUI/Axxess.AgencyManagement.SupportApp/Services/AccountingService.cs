﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Membership.Repositories;
    using Axxess.Core.Infrastructure;
    using System.Diagnostics;
    using Axxess.AgencyManagement.Enums;
    using System.IO;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Redmine.Net.Api.Types;
    using Redmine.Net.Api;
    using System.Threading;
    using System.Collections.Specialized;
    using Axxess.Membership.Logging;
    using System.Web;

    public class AccountingService : IAccountingService
    {
        #region Constructor

        private readonly IAccountingRepository accountingRepository;


        public AccountingService(IAccountingManagementDataProvider accountingManagementDataProvider)
        {
            this.accountingRepository = accountingManagementDataProvider.AccountingRepository;
        }
        #endregion

        #region Accounting Tables

        public AccountingPaymentViewData CalculateDateRange(DateTime start, DateTime end)
        {
            var firstPayments = accountingRepository.GetAllFirstPayment();
            var informations = accountingRepository.GetAllAccountingInformation(start, end);
            var infoGroup = informations.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();
            var listOfAmounts = new List<double>();
            var monthTotal = new List<string>();
            var countStartDate = start;
            var titles = new List<string>();
            var countEnd = end.AddDays(1);
            while (!(countStartDate.Month == countEnd.Month && countStartDate.Year == countEnd.Year))
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, 1));
                listOfAmounts.Add(0);
                monthTotal.Add("0.00");
                titles.Add(string.Format("{0:MMM, yyyy}", countStartDate));
                countStartDate = countStartDate.AddMonths(1);
            }
            var prevYear = string.Format("{0:MMM yyyy}", start);
            var currentYear = string.Format("{0:MMM yyyy}", start);
            var nextYear = string.Format("{0:MMM yyyy}", end);
            var bigTotal = 0.00;
            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles, PrevViewDate = prevYear, CurrentViewDate = currentYear, NextViewDate = nextYear };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var billableDate = new DateTime();
                    foreach (var t in firstPayments)
                    {
                        if (t.Account == key.Account) { billableDate = t.Start; }
                    }
                    var agencyInfo = new AgencyPayments { AgencyName = key.Name, AccountId = key.Account, PaymentSource = key.PaymentSource, City = key.City, State = key.State, InitialSignDate = key.InitialSignDate.ToString("MM-dd-yyyy"), FirstBillableDate = billableDate.ToString("MM-dd-yyyy"), ZipCode = key.Zip, SpecialNotes = key.SpecialNotes, SalesPerson = key.SalesPerson, StartUp = key.StartUp, Status = key.Status, Payments = new List<string>() };
                    var agencyYearTotal = 0.00;
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l.AddDays(-1);
                        var endDay = l.AddMonths(1);
                        var lastPayment = value.Where(v => v.Start < endDay && startDay < v.End).OrderByDescending(v => v.Start).FirstOrDefault();

                        if (lastPayment != null)
                        {
                            var round = String.Format("{0:0.00}", lastPayment.Amount);
                            agencyInfo.Payments.Add(round);
                            agencyYearTotal += lastPayment.Amount;
                            listOfAmounts[i] += lastPayment.Amount;
                        }
                        else
                        {
                            var round = String.Format("{0:0.00}", 0);
                            agencyInfo.Payments.Add(round);
                        }
                        i++;
                    });
                    agencyInfo.Total = String.Format("{0:0.00}", agencyYearTotal);
                    if (agencyYearTotal > 0)
                    {
                        viewData.AgencyPaymentData.Add(agencyInfo);
                    }
                });
            }
            for (var i = 0; i < listOfAmounts.Count; i++)
            {
                bigTotal += listOfAmounts[i];
                monthTotal[i] = String.Format("{0:0.00}", listOfAmounts[i]);
            }
            viewData.ListOfTotalAmounts = monthTotal;
            viewData.BigTotal = String.Format("{0:0.00}", bigTotal);
            return viewData;
        }

        public AccountingPaymentViewData CalculateWeekly(DateTime start, DateTime end)
        {
            var firstPayments = accountingRepository.GetAllFirstPayment();
            var informations = accountingRepository.GetAllAccountingInformation(start, end);
            var infoGroup = informations.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();
            var listOfAmounts = new List<double>();
            var monthTotal = new List<string>();
            var countStartDate = start;
            var titles = new List<string>();
            var countEnd = end.AddDays(1);
            while (!(countStartDate.Day == countEnd.Day && countStartDate.Month == countEnd.Month && countStartDate.Year == countEnd.Year))
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, countStartDate.Day));
                titles.Add(string.Format("{0:dddd d}", countStartDate));
                listOfAmounts.Add(0);
                monthTotal.Add("0.00");
                countStartDate = countStartDate.AddDays(1);
            }
            var prevWeek = string.Format("{0:dd, MMM, yyyy}", start.AddDays(-7));
            var currentWeek = string.Format("{0:dd, MMM, yyyy}", start);
            var nextWeek = string.Format("{0:dd, MMM, yyyy}", start.AddDays(7));
            var bigTotal = 0.00;
            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles, PrevViewDate = prevWeek, CurrentViewDate = currentWeek, NextViewDate = nextWeek };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var billableDate = new DateTime();
                    foreach (var t in firstPayments)
                    {
                        if (t.Account == key.Account) { billableDate = t.Start; }
                    }
                    var agencyInfo = new AgencyPayments { AgencyName = key.Name, AccountId = key.Account, PaymentSource = key.PaymentSource, City = key.City, State = key.State, InitialSignDate = key.InitialSignDate.ToString("MM-dd-yyyy"), FirstBillableDate = billableDate.ToString("MM-dd-yyyy"), ZipCode = key.Zip, SpecialNotes = key.SpecialNotes, SalesPerson = key.SalesPerson, StartUp = key.StartUp, Status = key.Status, Payments = new List<string>() };
                    var agencyYearTotal = 0.00;
                    var i = 0;
                    var validPayments = value.Where(v => !(v.Start < start && v.End > start && v.End < end));
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l.AddDays(-1);
                        var endDay = l.AddDays(1);
                        var round = validPayments.Where(v => v.Start.Day == l.Day).Sum(s => s.Amount);
                        agencyYearTotal += round;
                        listOfAmounts[i] += round;
                        agencyInfo.Payments.Add(String.Format("{0:0.00}", round));
                        i++;
                    });
                    agencyInfo.Total = String.Format("{0:0.00}", agencyYearTotal);
                    if (agencyYearTotal > 0)
                    {
                        viewData.AgencyPaymentData.Add(agencyInfo);
                    }
                });

            }

            for (var i = 0; i < listOfAmounts.Count; i++)
            {
                bigTotal += listOfAmounts[i];
                monthTotal[i] = String.Format("{0:0.00}", listOfAmounts[i]);
            }
            viewData.ListOfTotalAmounts = monthTotal;
            viewData.BigTotal = String.Format("{0:0.00}", bigTotal);
            return viewData;
         
        }

        public AccountingPaymentViewData CalculateMonthly(DateTime start, DateTime end)
        {
            var firstPayments = accountingRepository.GetAllFirstPayment();
            var informations = accountingRepository.GetAllAccountingInformation(start, end);
            var infoGroup = informations.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();
            var listOfAmounts = new List<double>();
            var monthTotal = new List<string>();
            var countStartDate = start;
            var titles = new List<string>();
            var endCount = start.AddMonths(1);
            var numberOfDays = end.Day;
            var count = 1;
            do
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, countStartDate.Day));
                titles.Add(string.Format("{0:dd}", countStartDate));
                listOfAmounts.Add(0);
                monthTotal.Add("0.00");
                countStartDate = countStartDate.AddDays(1);
                count++;
            } while ((count != (numberOfDays + 1)));

            var prevMonth = string.Format("{0:MMM, yyyy}", start.AddMonths(-1));
            var currentMonth = string.Format("{0:MMM, yyyy}", start);
            var nextMonth = string.Format("{0:MMM, yyyy}", start.AddMonths(1));
            var bigTotal = 0.00;
            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles, PrevViewDate = prevMonth, CurrentViewDate = currentMonth, NextViewDate = nextMonth };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var billableDate = new DateTime();
                    foreach (var t in firstPayments)
                    {
                        if (t.Account == key.Account) { billableDate = t.Start; }
                    }

                    var agencyInfo = new AgencyPayments { AgencyName = key.Name, AccountId = key.Account, PaymentSource = key.PaymentSource, City = key.City, State = key.State, InitialSignDate = key.InitialSignDate.ToString("MM-dd-yyyy"), FirstBillableDate = billableDate.ToString("MM-dd-yyyy"), ZipCode = key.Zip, SpecialNotes = key.SpecialNotes, SalesPerson = key.SalesPerson, StartUp = key.StartUp, Status = key.Status, Payments = new List<string>() };
                    var agencyYearTotal = 0.00;
                    var i = 0;
                    var validPayments = value.Where(v => !(v.Start < start && v.End > start && v.End < end));
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l.AddDays(-1);
                        var endDay = l.AddDays(1);
                        var round = validPayments.Where(v => v.Start.Day == l.Day).Sum(s => s.Amount);
                        agencyYearTotal += round;
                        listOfAmounts[i] += round;
                        agencyInfo.Payments.Add(String.Format("{0:0.00}", round));
                        i++;
                    });
                    agencyInfo.Total = String.Format("{0:0.00}", agencyYearTotal);
                    if (agencyYearTotal > 0)
                    {
                        viewData.AgencyPaymentData.Add(agencyInfo);
                    }
                });

            }

            for (var i = 0; i < listOfAmounts.Count; i++)
            {
                bigTotal += listOfAmounts[i];
                monthTotal[i] = String.Format("{0:0.00}", listOfAmounts[i]);
            }
            viewData.ListOfTotalAmounts = monthTotal;
            viewData.BigTotal = String.Format("{0:0.00}", bigTotal);
            return viewData;
        }

        public AccountingPaymentViewData CalculateYearly(DateTime start, DateTime end)
        {
            var informations = accountingRepository.GetAllAccountingInformation(start, end);
            var firstPayments = accountingRepository.GetAllFirstPayment();
            var infoGroup = informations.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();
            var listOfAmounts = new List<double>();
            var monthTotal = new List<string>();
            var countStartDate = start;
            var titles = new List<string>();
            var endCount = start.AddYears(1);
            while (!(countStartDate.Month == endCount.Month && countStartDate.Year == endCount.Year))
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, 1));
                listOfAmounts.Add(0);
                monthTotal.Add("0.00");
                titles.Add(string.Format("{0:MMM, yyyy}", countStartDate));
                countStartDate = countStartDate.AddMonths(1);
            }
            var prevYear = start.AddYears(-1).Year.ToString();
            var currentYear = start.Year.ToString();
            var nextYear = start.AddYears(1).Year.ToString();
            var bigTotal = 0.00;
            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles, PrevViewDate = prevYear, CurrentViewDate = currentYear, NextViewDate = nextYear };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var billableDate = new DateTime();
                    foreach (var t in firstPayments)
                    {
                        if (t.Account == key.Account) { billableDate = t.Start; }
                    }
                    var agencyInfo = new AgencyPayments { AgencyName = key.Name, AccountId = key.Account, PaymentSource = key.PaymentSource, City = key.City, State = key.State, InitialSignDate = key.InitialSignDate.ToString("MM-dd-yyyy"), FirstBillableDate = billableDate.ToString("MM-dd-yyyy"), ZipCode = key.Zip, SpecialNotes = key.SpecialNotes, SalesPerson = key.SalesPerson, StartUp = key.StartUp, Status = key.Status, Payments = new List<string>() };
                    var agencyYearTotal = 0.00;
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l.AddDays(-1);
                        var endDay = l.AddMonths(1);
                        var lastPayment = value.Where(v => v.Start < endDay && startDay < v.End).OrderByDescending(v => v.Start).FirstOrDefault();
                        double round = 0;
                        if (lastPayment != null)
                        {
                            round = lastPayment.Amount;
                            agencyYearTotal += round;
                            listOfAmounts[i] += round;
                        }

                        agencyInfo.Payments.Add(String.Format("{0:0.00}", round));
                        i++;
                    });
                    agencyInfo.Total = String.Format("{0:0.00}", agencyYearTotal);
                    if (agencyYearTotal > 0)
                    {
                        viewData.AgencyPaymentData.Add(agencyInfo);
                    }
                });
            }
            for (var i = 0; i < listOfAmounts.Count; i++)
            {
                bigTotal += listOfAmounts[i];
                monthTotal[i] = String.Format("{0:0.00}", listOfAmounts[i]);
            }
            viewData.ListOfTotalAmounts = monthTotal;
            viewData.BigTotal = String.Format("{0:0.00}", bigTotal);
            return viewData;

        }

        #endregion

        #region Accounting Dashboard and Metrics

        public EndingMRRViewData CalculateEndingMRR(DateTime start, DateTime end) 
        {
            var result = new EndingMRRViewData();
            var informations = accountingRepository.GetAllAccountingInformation(start, end);
            var infoGroup = informations.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();
            var listOfAmounts = new List<double>();
            var monthTotal = new List<double>();
            var titles = new List<string>();
            var countStartDate = start;
            var countEnd = end.AddDays(1);
            while (!(countStartDate.Month == countEnd.Month && countStartDate.Year == countEnd.Year))
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, 1));
                listOfAmounts.Add(0);
                monthTotal.Add(0.00);
                titles.Add(string.Format("{0:MMM, yyyy}", countStartDate));
                countStartDate = countStartDate.AddMonths(1);
            }

            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l.AddDays(-1);
                        var endDay = l.AddMonths(1);
                        var lastPayment = value.Where(v => v.Start < endDay && startDay < v.End).OrderByDescending(v => v.Start).FirstOrDefault();

                        if (lastPayment != null)
                        {
                            listOfAmounts[i] += lastPayment.Amount;
                        }
                        i++;
                    });
                });
            }

            var row = new ChartRows();
            row.data = listOfAmounts;
            row.name = "Ending MRR";
            var rowList = new List<ChartRows>();
            rowList.Add(row);

            result.Column = viewData.Titles;
            result.Caption = "Ending MRR";
            result.Rows = rowList;
            result.StartDate = start;
            result.EndDate = countEnd.AddMonths(-1);
            return result;
        }

        public EndingMRRViewData CalculateMRRBooking(DateTime start, DateTime end)
        {
            var result = new EndingMRRViewData();
            var allPayments = accountingRepository.GetAllPayments();
            var informations = accountingRepository.GetAllAccountingInformation(start, end);
            var infoGroup = informations.GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();
            var newListOfAmounts = new List<double>();
            var churnListOfAmounts = new List<double>();
            var expandListOfAmounts = new List<double>();
            var netListOfAmounts = new List<double>();
            var titles = new List<string>();
            var countStartDate = start;
            var countEnd = end.AddDays(1);
            while (!(countStartDate.Month == countEnd.Month && countStartDate.Year == countEnd.Year))
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, 1));
                newListOfAmounts.Add(0);
                churnListOfAmounts.Add(0);
                expandListOfAmounts.Add(0);
                netListOfAmounts.Add(0);
                titles.Add(string.Format("{0:MMM, yyyy}", countStartDate));
                countStartDate = countStartDate.AddMonths(1);
            }

            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var initialDate = key.InitialSignDate.ToString("MM-dd-yyyy");
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l.AddDays(-1);
                        var endDay = l.AddMonths(1);

                        //NEW MRR
                        if (key.Start < endDay && key.Start > startDay)
                        {
                            if (key.Sequence == 1)
                            {
                                var initSignPayment = value.OrderBy(v => v.Start).FirstOrDefault();
                                if (initSignPayment != null)
                                {
                                    newListOfAmounts[i] += initSignPayment.Amount;
                                }
                            }
                        }

                        var expandPayment = value.Where(V => V.Start > startDay && V.Start < endDay).OrderByDescending(v => v.Start).FirstOrDefault();
                        if (expandPayment != null && expandPayment.Amount != 0)
                        {
                            // Expansion MRR
                            var diffAmount = 0.00;
                            var PreSequence = expandPayment.Sequence - 1;
                            if (PreSequence > 0)
                            {
                                double preAmount = 0.00;
                                var currentAmount = expandPayment.Amount;
                                foreach (var t in allPayments)
                                {
                                    if (t.Account == expandPayment.Account && t.Sequence == PreSequence)
                                    {
                                        preAmount = t.Amount;
                                    }
                                }
                                diffAmount = currentAmount - preAmount;
                            }
                            if (diffAmount >= 0.00)
                            {
                                expandListOfAmounts[i] += diffAmount;
                            }
                            else
                            {
                                churnListOfAmounts[i] += diffAmount;
                            }
                        }
                        else if (expandPayment != null && expandPayment.Amount == 0.00)
                        {
                            // CHURNED MRR
                            double churnAmount = 0.00;
                            if (expandPayment.Sequence > 1)
                            {
                                var currentAmount = expandPayment.Amount;
                                foreach (var t in allPayments)
                                {
                                    if (t.Account == expandPayment.Account && t.Sequence == expandPayment.Sequence - 1)
                                    {
                                        churnAmount = t.Amount;
                                    }
                                }
                            }
                            churnListOfAmounts[i] += -churnAmount;
                        }

                        netListOfAmounts[i] = newListOfAmounts[i] + expandListOfAmounts[i] + churnListOfAmounts[i];
                        i++;
                    });
                });
            }

            var rowList = new List<ChartRows>();

            var newRow = new ChartRows();
            newRow.data = newListOfAmounts;
            newRow.name = "NEW MRR";
            rowList.Add(newRow);

            var churnRow = new ChartRows();
            churnRow.data = churnListOfAmounts;
            churnRow.name = "Churned MRR";
            rowList.Add(churnRow);

            var expandRow = new ChartRows();
            expandRow.data = expandListOfAmounts;
            expandRow.name = "Expansion MRR";
            rowList.Add(expandRow);

            var netRow = new ChartRows();
            netRow.data = netListOfAmounts;
            netRow.name = "Net MRR";
            rowList.Add(netRow);

            result.Column = viewData.Titles;
            result.Caption = "MRR Booking";
            result.Rows = rowList;
            result.StartDate = start;
            result.EndDate = countEnd.AddMonths(-1);
            return result;
        }

        public AccountingMetricsViewData CalculateMetrics(DateTime start, DateTime end)
        {
            var startLast = start.AddYears(-1);
            var endLast = end.AddYears(-1).AddMonths(1);
            var allPayments = accountingRepository.GetAllPayments();
            var accountInfo = accountingRepository.GetAccountInfo(start, end);
            var informations = accountingRepository.GetAllAccountingInformation(start, end, startLast, endLast);

            var infoGroup = informations.Where(a=>a.Start<=end && a.End>=start).GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var infoLastGroup = informations.Where(a => a.Start <= endLast && a.End >= startLast).GroupBy(g => g.Account).ToDictionary(g => g.First(), g => g.ToList());
            var listOfDates = new List<DateTime>();

            var listofCountBegin = new List<double>();
            var listofCountNew = new List<double>();
            var listOfCountLost = new List<double>();
            var listOfCountNet = new List<double>();
            var listOfCountEnd = new List<double>();
            var listOfCountDealClosed = new List<double>();

            var beginListOfAmounts = new List<double>();
            var newListOfAmounts = new List<double>();
            var churnListOfAmounts = new List<double>();
            var downGradeListOfAmounts = new List<double>();
            var expandListOfAmounts = new List<double>();
            var netListOfAmounts = new List<double>();
            var endListOfAmounts = new List<double>();
            var avgListOfAmounts = new List<double>();

            var lastEndListOfAmounts = new List<double>();
            var lastListOfCountEnd = new List<double>();
            var lastRetentionEndListOfAmounts = new List<double>();
            var lastRetentionListOfCountEnd = new List<double>();

            var avgNewAnnualListofAmounts = new List<double>();
            var avgRevenuePerClientListofAmounts = new List<double>();
            var monthlyChurnListofAmounts = new List<double>();
            var annualizedChurnListofAmounts = new List<double>();
            var customerMonthlyChurnListofAmounts = new List<double>();
            var customerAnnualizedChurnListofAmounts = new List<double>();

            var titles = new List<string>();
            var countStartDate = start;
            var countEnd = end.AddDays(1);

            while (!(countStartDate.Month == countEnd.Month && countStartDate.Year == countEnd.Year))
            {
                listOfDates.Add(new DateTime(countStartDate.Year, countStartDate.Month, 1));

                listofCountBegin.Add(0);
                listofCountNew.Add(0);
                listOfCountLost.Add(0);
                listOfCountNet.Add(0);
                listOfCountEnd.Add(0);
                listOfCountDealClosed.Add(0);

                beginListOfAmounts.Add(0);
                newListOfAmounts.Add(0);
                churnListOfAmounts.Add(0);
                downGradeListOfAmounts.Add(0);
                expandListOfAmounts.Add(0);
                netListOfAmounts.Add(0);
                endListOfAmounts.Add(0);
                avgListOfAmounts.Add(0);

                lastEndListOfAmounts.Add(0);
                lastListOfCountEnd.Add(0);
                lastRetentionEndListOfAmounts.Add(0);
                lastRetentionListOfCountEnd.Add(0);

                avgNewAnnualListofAmounts.Add(0);
                avgRevenuePerClientListofAmounts.Add(0);
                monthlyChurnListofAmounts.Add(0);
                annualizedChurnListofAmounts.Add(0);
                customerMonthlyChurnListofAmounts.Add(0);
                customerAnnualizedChurnListofAmounts.Add(0);

                titles.Add(string.Format("{0:MMM, yyyy}", countStartDate));
                countStartDate = countStartDate.AddMonths(1);
            }
            //Deal Closed Customers
            if (accountInfo != null && accountInfo.Count > 0)
            {
                accountInfo.ForEach(key =>
                {
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l;
                        var endDay = l.AddMonths(1).AddDays(-1);
                        if (key.InitialSignDate >= startDay && key.InitialSignDate <= endDay)
                        {
                            listOfCountDealClosed[i] += 1;
                        }
                        i++;
                    });
                });
            }
            //Years on years growth
            if (infoLastGroup != null && infoLastGroup.Count > 0)
            {
                infoLastGroup.ForEach((key, value) =>
                {
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var lastStartDay = l.AddMonths(1).AddYears(-1);
                        var lastEndDay = l.AddMonths(2).AddYears(-1);
                        //var lastPayment = value.Where(V => V.Start < lastStartDay && V.End >= lastEndDay).OrderByDescending(v => v.Start).FirstOrDefault();
                        var lastPayment = value.Where(V => V.Start < lastStartDay && V.End >= lastStartDay).OrderByDescending(v => v.Start).FirstOrDefault();
                        //var lastPayment = value.Where(v => (v.Start.Year < lastStartDay.Year && v.End.Year > lastStartDay.Year) || (v.Start.Year < lastStartDay.Year && v.End.Year == lastStartDay.Year && v.End.Month >= lastStartDay.Month) || (v.Start.Year == lastStartDay.Year && v.Start.Month < lastStartDay.Month && v.End.Year > lastStartDay.Year) || (v.Start.Year == lastStartDay.Year && v.Start.Month < lastStartDay.Month && v.End.Year == lastStartDay.Year && v.End.Month >= lastStartDay.Month)).OrderByDescending(v => v.Start).FirstOrDefault();
                        if (lastPayment!=null)
                        {
                            var lastMRRPayment = 0.00;
                            lastMRRPayment = lastPayment.Amount;
                            lastEndListOfAmounts[i] += lastMRRPayment;
                            lastListOfCountEnd[i] += 1;

                            if (infoGroup != null && infoGroup.Count > 0)
                            {
                                infoGroup.ForEach((subKey, subValue) =>
                                {
                                    var startDay = l.AddMonths(1);
                                    var endDay = l.AddMonths(2);
                                    //var currentPayment = subValue.Where(V => V.Start < startDay && V.End >= endDay).OrderByDescending(v => v.Start).FirstOrDefault();
                                    var currentPayment = subValue.Where(V => V.Start < startDay && V.End >= startDay).OrderByDescending(v => v.Start).FirstOrDefault();
                                    //var currentPayment = subValue.Where(v => (v.Start.Year < startDay.Year && v.End.Year > startDay.Year) || (v.Start.Year < startDay.Year && v.End.Year == startDay.Year && v.End.Month >= startDay.Month) || (v.Start.Year == startDay.Year && v.Start.Month < startDay.Month && v.End.Year > startDay.Year) || (v.Start.Year == startDay.Year && v.Start.Month < startDay.Month && v.End.Year == startDay.Year && v.End.Month >= startDay.Month)).OrderByDescending(v => v.Start).FirstOrDefault();
                                    if (currentPayment != null && currentPayment.Account == lastPayment.Account)
                                    {
                                        var currentMRRPayment = 0.00;
                                        currentMRRPayment = currentPayment.Amount;
                                        lastRetentionEndListOfAmounts[i] += currentMRRPayment;
                                        DateTime max = new DateTime(9999, 10, 30);
                                        if (currentMRRPayment != 0.00 || (currentMRRPayment == 0.00 && currentPayment.End < max))
                                        {
                                            lastRetentionListOfCountEnd[i] += 1;
                                        }
                                    }
                                });
                            }
                        }
                        i++;
                    });
                });
            }

            var viewData = new AccountingPaymentViewData { AgencyPaymentData = new List<AgencyPayments>(), Titles = titles };
            if (infoGroup != null && infoGroup.Values.Count > 0)
            {
                infoGroup.ForEach((key, value) =>
                {
                    var i = 0;
                    listOfDates.ForEach(l =>
                    {
                        var startDay = l;
                        var endDay = l.AddMonths(1).AddDays(-1);

                        //Begin Customers and MRR
                        //var beginPayment = value.Where(V => V.Start < startDay && V.End > endDay).OrderByDescending(v => v.Start).FirstOrDefault();
                        //var beginPayment = value.Where(v => v.Start < startDay && v.End >= startDay).OrderByDescending(v => v.Start).FirstOrDefault();
                        var beginPayment = value.Where(v => (v.Start.Year < startDay.Year && v.End.Year > startDay.Year) || (v.Start.Year < startDay.Year && v.End.Year == startDay.Year && v.End.Month >= startDay.Month) || (v.Start.Year == startDay.Year && v.Start.Month < startDay.Month && v.End.Year > startDay.Year) || (v.Start.Year == startDay.Year && v.Start.Month < startDay.Month && v.End.Year == startDay.Year && v.End.Month >= startDay.Month)).OrderByDescending(v => v.Start).FirstOrDefault();
                        if (beginPayment != null)
                        {
                            var beginMRRPayment = 0.00;
                            beginMRRPayment = beginPayment.Amount;
                            beginListOfAmounts[i] += beginMRRPayment;
                            listofCountBegin[i] += 1;
                        }

                        //NEW Customers and MRR
                        if (key.Start.Year == startDay.Year && key.Start.Month == startDay.Month && key.Start.Day <= endDay.Day && key.Start.Day >= startDay.Day)
                        {
                            var initSignPayment = value.Where(v => v.Sequence == 1 && v.Start.Year == startDay.Year && v.Start.Month == startDay.Month && v.Start.Day <= endDay.Day && v.Start.Day >= startDay.Day).OrderBy(v => v.Start).FirstOrDefault();
                            if (initSignPayment != null)
                            {
                                newListOfAmounts[i] += initSignPayment.Amount;
                                listofCountNew[i] += 1;
                            }
                        }

                        var expandPayment = value.Where(v => v.Sequence > 1 && v.Start.Year == startDay.Year && v.Start.Month == startDay.Month && v.Start.Day >= startDay.Day && v.Start.Day < endDay.Day).OrderByDescending(v => v.Start).FirstOrDefault();
                        if (expandPayment != null && expandPayment.Amount != 0)
                        {
                            // Expansion and Downgrade MRR
                            double preAmount = 0.00;
                            double currentAmount = expandPayment.Amount;
                            foreach (var t in allPayments)
                            {
                                if (t.Account == expandPayment.Account && t.Sequence == expandPayment.Sequence - 1)
                                {
                                    preAmount = t.Amount;
                                }
                            }
                            var diffAmount = currentAmount - preAmount;
                            if (diffAmount >= 0.00)
                            {
                                expandListOfAmounts[i] += diffAmount;
                            }
                            else
                            {
                                downGradeListOfAmounts[i] += diffAmount;
                            }
                        }
                        else if (expandPayment != null && expandPayment.Amount == 0.00)
                        {
                            // Churned Customers and MRR
                            double churnAmount = 0.00;
                            foreach (var t in allPayments)
                            {
                                if (t.Account == expandPayment.Account && t.Sequence == expandPayment.Sequence - 1)
                                {
                                    churnAmount = t.Amount;
                                }
                            }
                            DateTime max = new DateTime(9999, 10, 30);
                            if (expandPayment.End > max)
                            {
                                listOfCountLost[i] += 1;
                                churnListOfAmounts[i] += -churnAmount;
                            }
                            else
                            {
                                downGradeListOfAmounts[i] += -churnAmount;
                            }

                        }
                        netListOfAmounts[i] = newListOfAmounts[i] + expandListOfAmounts[i] + churnListOfAmounts[i] + downGradeListOfAmounts[i];
                        i++;
                    });
                });
            }
            var beginCustomer = new List<string>();
            var newCustomer = new List<string>();
            var lostCustomer = new List<string>();
            var netCustomer = new List<string>();
            var endCustomer = new List<string>();
            var dealClosedCustomer = new List<string>();

            var beginMRR = new List<string>();
            var newMRR = new List<string>();
            var expandMRR = new List<string>();
            var churnMRR = new List<string>();
            var downgradeMRR = new List<string>();
            var netMRR = new List<string>();
            var endMRR = new List<string>();
            var avgMRR = new List<string>();

            var beginARR = new List<string>();
            var newARR = new List<string>();
            var expandARR = new List<string>();
            var churnARR = new List<string>();
            var downgradeARR = new List<string>();
            var netARR = new List<string>();
            var endARR = new List<string>();
            var avgARR = new List<string>();

            var avgNewAnnual = new List<string>();
            var avgRevenuePerClient = new List<string>();
            var monthlyChurn = new List<string>();
            var annualizedChurn = new List<string>();
            var customerMonthlyChurn = new List<string>();
            var customerAnnualizedChurn = new List<string>();

            var lastEndMRR = new List<string>();
            var lastRate = new List<string>();
            var lastEndCustomer = new List<string>();
            var lastRateCustomer = new List<string>();

            var lastRetentionEndMRR = new List<string>();
            var lastRetentionRate = new List<string>();
            var lastRetentionEndCustomer = new List<string>();
            var lastRetentionRateCustomer = new List<string>();

            var beginCustomerRow = new MetricsRows();
            var newCustomerRow = new MetricsRows();
            var lostCustomerRow = new MetricsRows();
            var netCustomerRow = new MetricsRows();
            var endCustomerRow = new MetricsRows();
            var dealClosedCustomerRow = new MetricsRows();

            var beginRow = new MetricsRows();
            var newRow = new MetricsRows();
            var expandRow = new MetricsRows();
            var churnRow = new MetricsRows();
            var downgradeRow = new MetricsRows();
            var netRow = new MetricsRows();
            var endRow = new MetricsRows();
            var avgRow = new MetricsRows();

            var beginRowARR = new MetricsRows();
            var newRowARR = new MetricsRows();
            var expandRowARR = new MetricsRows();
            var churnRowARR = new MetricsRows();
            var downgradeRowARR = new MetricsRows();
            var netRowARR = new MetricsRows();
            var endRowARR = new MetricsRows();
            var avgRowARR = new MetricsRows();

            var avgNewAnnualRow = new MetricsRows();
            var avgRevenuePerClientRow = new MetricsRows();
            var monthlyChurnRow = new MetricsRows();
            var annualizedChurnRow = new MetricsRows();
            var customerMonthlyChurnRow = new MetricsRows();
            var customerAnnualizedChurnRow = new MetricsRows();

            var lastEndMRRRow = new MetricsRows();
            var lastRateMRRRow = new MetricsRows();
            var lastEndCustomerRow = new MetricsRows();
            var lastRateCustomerRow = new MetricsRows();

            var lastRetentionEndMRRRow = new MetricsRows();
            var lastRetentionRateMRRRow = new MetricsRows();
            var lastRetentionEndCustomerRow = new MetricsRows();
            var lastRetentionRateCustomerRow = new MetricsRows();

            var totalNewCustomer = 0.00;
            var totalLostCustomer = 0.00;
            var totalNetCustomer = 0.00;
            var totalDealClosedCustomer = 0.00;

            for (var n = 0; n < listofCountNew.Count; n++)
            {
                //Customer Data
                beginCustomer.Add(string.Format("{0:0}", listofCountBegin[n]));
                beginCustomerRow.Data = beginCustomer;
               
                newCustomer.Add(string.Format("{0:0}", listofCountNew[n]));
                newCustomerRow.Data = newCustomer;
                totalNewCustomer += listofCountNew[n];

                lostCustomer.Add(string.Format("{0:0}", -listOfCountLost[n]));
                lostCustomerRow.Data = lostCustomer;
                totalLostCustomer += -listOfCountLost[n];

                listOfCountNet[n] = listofCountNew[n] - listOfCountLost[n];
                netCustomer.Add(string.Format("{0:0}", listOfCountNet[n]));
                netCustomerRow.Data = netCustomer;
                totalNetCustomer += listOfCountNet[n];

                listOfCountEnd[n] = listofCountBegin[n] + listOfCountNet[n];
                endCustomer.Add(string.Format("{0:0}", listOfCountEnd[n]));
                endCustomerRow.Data = endCustomer;

                dealClosedCustomer.Add(string.Format("{0:0}", listOfCountDealClosed[n]));
                dealClosedCustomerRow.Data = dealClosedCustomer;
                totalDealClosedCustomer += listOfCountDealClosed[n];
                

                // MRR and ARR Data
                beginMRR.Add(string.Format("{0:C}", beginListOfAmounts[n]));
                beginRow.Data = beginMRR;
                beginARR.Add(string.Format("{0:C}", beginListOfAmounts[n] * 12));
                beginRowARR.Data = beginARR;

                newMRR.Add(string.Format("{0:C}", newListOfAmounts[n]));
                newRow.Data = newMRR;
                newARR.Add(string.Format("{0:C}", newListOfAmounts[n] * 12));
                newRowARR.Data = newARR;

                expandMRR.Add(string.Format("{0:C}", expandListOfAmounts[n]));
                expandRow.Data = expandMRR;
                expandARR.Add(string.Format("{0:C}", expandListOfAmounts[n] * 12));
                expandRowARR.Data = expandARR;

                churnMRR.Add(string.Format("{0:C}", churnListOfAmounts[n]));
                churnRow.Data = churnMRR;
                churnARR.Add(string.Format("{0:C}", churnListOfAmounts[n] * 12));
                churnRowARR.Data = churnARR;

                downgradeMRR.Add(string.Format("{0:C}", downGradeListOfAmounts[n]));
                downgradeRow.Data = downgradeMRR;
                downgradeARR.Add(string.Format("{0:C}", downGradeListOfAmounts[n] * 12));
                downgradeRowARR.Data = downgradeARR;

                netMRR.Add(string.Format("{0:C}", netListOfAmounts[n]));
                netRow.Data = netMRR;
                netARR.Add(string.Format("{0:C}", netListOfAmounts[n] * 12));
                netRowARR.Data = netARR;

                endListOfAmounts[n] = beginListOfAmounts[n] + netListOfAmounts[n];
                endMRR.Add(string.Format("{0:C}", endListOfAmounts[n]));
                endRow.Data = endMRR;
                endARR.Add(string.Format("{0:C}", endListOfAmounts[n] * 12));
                endRowARR.Data = endARR;


                avgListOfAmounts[n] = (beginListOfAmounts[n] + endListOfAmounts[n]) / 2;
                avgMRR.Add(string.Format("{0:C}", avgListOfAmounts[n]));
                avgRow.Data = avgMRR;
                avgARR.Add(string.Format("{0:C}", avgListOfAmounts[n] * 12));
                avgRowARR.Data = avgARR;

                //Metrics Data
                if (listofCountNew[n] != 0.00)
                {
                    avgNewAnnualListofAmounts[n] = (newListOfAmounts[n] * 12) / listofCountNew[n];
                    avgRevenuePerClientListofAmounts[n] = newListOfAmounts[n] / listofCountNew[n];
                }
                avgNewAnnual.Add(string.Format("{0:C}", avgNewAnnualListofAmounts[n]));
                avgNewAnnualRow.Data = avgNewAnnual;
                avgRevenuePerClient.Add(string.Format("{0:C}", avgRevenuePerClientListofAmounts[n]));
                avgRevenuePerClientRow.Data = avgRevenuePerClient;

                if (beginListOfAmounts[n] != 0.00)
                {
                    monthlyChurnListofAmounts[n] = (churnListOfAmounts[n] / beginListOfAmounts[n]);
                    var temp = 1 + monthlyChurnListofAmounts[n];
                    annualizedChurnListofAmounts[n] = 1 + monthlyChurnListofAmounts[n];
                    for (var x = 0; x < 11; x++)
                    {
                        annualizedChurnListofAmounts[n] = annualizedChurnListofAmounts[n] * temp;
                    }
                    annualizedChurnListofAmounts[n] = annualizedChurnListofAmounts[n] - 1;
                }
                monthlyChurn.Add(string.Format("{0:0.00%}", monthlyChurnListofAmounts[n]));
                monthlyChurnRow.Data = monthlyChurn;

                annualizedChurn.Add(string.Format("{0:0.00%}", annualizedChurnListofAmounts[n]));
                annualizedChurnRow.Data = annualizedChurn;

                if (listofCountBegin[n] != 0.00)
                {
                    customerMonthlyChurnListofAmounts[n] = (listOfCountLost[n] / listofCountBegin[n]);
                    var temp = 1 - customerMonthlyChurnListofAmounts[n];
                    customerAnnualizedChurnListofAmounts[n] = 1 - customerMonthlyChurnListofAmounts[n];
                    for (var x = 0; x < 11; x++)
                    {
                        customerAnnualizedChurnListofAmounts[n] = customerAnnualizedChurnListofAmounts[n] * temp;
                    }
                    customerAnnualizedChurnListofAmounts[n] = customerAnnualizedChurnListofAmounts[n] - 1;
                }
                customerMonthlyChurn.Add(string.Format("{0:0.00%}", -customerMonthlyChurnListofAmounts[n]));
                customerMonthlyChurnRow.Data = customerMonthlyChurn;

                customerAnnualizedChurn.Add(string.Format("{0:0.00%}", customerAnnualizedChurnListofAmounts[n]));
                customerAnnualizedChurnRow.Data = customerAnnualizedChurn;

                //Ending MRR and Ending Customer for last year
                lastEndMRR.Add(string.Format("{0:C}", lastEndListOfAmounts[n]));
                lastEndMRRRow.Data = lastEndMRR;

                if (lastEndListOfAmounts[n] != 0.00)
                {
                    lastRate.Add(string.Format("{0:0.00%}", (endListOfAmounts[n] - lastEndListOfAmounts[n]) / lastEndListOfAmounts[n]));
                }
                else
                {
                    lastRate.Add(string.Format("{0:0.00%}", 0));
                }
                lastRateMRRRow.Data = lastRate;

               
                lastEndCustomer.Add(string.Format("{0:0}", lastListOfCountEnd[n]));
                lastEndCustomerRow.Data = lastEndCustomer;

                if (lastListOfCountEnd[n] != 0.00)
                {
                    lastRateCustomer.Add(string.Format("{0:0.00%}", (listOfCountEnd[n] - lastListOfCountEnd[n]) / lastListOfCountEnd[n]));
                }
                else 
                {
                    lastRateCustomer.Add(string.Format("{0:0.00%}", 0));
                }
                lastRateCustomerRow.Data = lastRateCustomer;

                //Retention
                lastRetentionEndMRR.Add(string.Format("{0:C}", lastRetentionEndListOfAmounts[n]));
                lastRetentionEndMRRRow.Data = lastRetentionEndMRR;

                if (lastEndListOfAmounts[n] != 0.00)
                {
                    lastRetentionRate.Add(string.Format("{0:0.00%}", (lastRetentionEndListOfAmounts[n]) / lastEndListOfAmounts[n]));
                }
                else
                {
                    lastRetentionRate.Add(string.Format("{0:0.00%}", 0));
                }
                lastRetentionRateMRRRow.Data = lastRetentionRate;


                lastRetentionEndCustomer.Add(string.Format("{0:0}", lastRetentionListOfCountEnd[n]));
                lastRetentionEndCustomerRow.Data = lastRetentionEndCustomer;

                if (lastListOfCountEnd[n] != 0.00)
                {
                    lastRetentionRateCustomer.Add(string.Format("{0:0.00%}", (lastRetentionListOfCountEnd[n]) / lastListOfCountEnd[n]));
                }
                else
                {
                    lastRetentionRateCustomer.Add(string.Format("{0:0.00%}", 0));
                }
                lastRetentionRateCustomerRow.Data = lastRetentionRateCustomer;
            }

            var result = new AccountingMetricsViewData();
            var rowList = new List<MetricsRows>();

            //Customer Data
            beginCustomerRow.Name = "Beginning Customers";
            rowList.Add(beginCustomerRow);

            newCustomerRow.Name = "Gross New Customers";
            newCustomerRow.Total = string.Format("{0:0}", totalNewCustomer);
            rowList.Add(newCustomerRow);

            lostCustomerRow.Name = "Gross Lost Customers";
            lostCustomerRow.Total = string.Format("{0:0}", totalLostCustomer);
            rowList.Add(lostCustomerRow);

            netCustomerRow.Name = "Net New Customers";
            netCustomerRow.Total = string.Format("{0:0}", totalNetCustomer);
            rowList.Add(netCustomerRow);

            endCustomerRow.Name = "Ending Customers";
            rowList.Add(endCustomerRow);

            dealClosedCustomerRow.Name = "Deal Closed Customers";
            dealClosedCustomerRow.Total = string.Format("{0:0}", totalDealClosedCustomer);
            rowList.Add(dealClosedCustomerRow);
            

            //MRR Data
            beginRow.Name = "Beginning MRR";
            rowList.Add(beginRow);

            newRow.Name = "NEW MRR";
            rowList.Add(newRow);

            expandRow.Name = "Expansion MRR";
            rowList.Add(expandRow);

            churnRow.Name = "Churned MRR";
            rowList.Add(churnRow);

            downgradeRow.Name = "Downgrade MRR";
            rowList.Add(downgradeRow);

            netRow.Name = "Net MRR";

            rowList.Add(netRow);

            endRow.Name = "Ending MRR";
            rowList.Add(endRow);

            avgRow.Name = "Avg. MRR";
            rowList.Add(avgRow);

            // ARR Data
            beginRowARR.Name = "Beginning ARR";
            rowList.Add(beginRowARR);

            newRowARR.Name = "NEW ARR";
            rowList.Add(newRowARR);

            expandRowARR.Name = "Expansion ARR";
            rowList.Add(expandRowARR);

            churnRowARR.Name = "Churned ARR";
            rowList.Add(churnRowARR);

            downgradeRowARR.Name = "Downgrade ARR";
            rowList.Add(downgradeRowARR);

            netRowARR.Name = "Net ARR";
            rowList.Add(netRowARR);

            endRowARR.Name = "Ending ARR";
            rowList.Add(endRowARR);

            avgRowARR.Name = "Avg. ARR";
            rowList.Add(avgRowARR);

            // Metrics Data
            avgNewAnnualRow.Name = "Avg. New Annual Contract Value";
            rowList.Add(avgNewAnnualRow);

            avgRevenuePerClientRow.Name = "Avg. Revenue Per New Client";
            rowList.Add(avgRevenuePerClientRow);

            monthlyChurnRow.Name = "Monthly $ Churn%";
            rowList.Add(monthlyChurnRow);

            annualizedChurnRow.Name = "Annualized $ Churn%";
            rowList.Add(annualizedChurnRow);

            customerMonthlyChurnRow.Name = "Monthly # Churn%";
            rowList.Add(customerMonthlyChurnRow);

            customerAnnualizedChurnRow.Name = "Annualized # Churn%";
            rowList.Add(customerAnnualizedChurnRow);

            lastEndMRRRow.Name = "Mothly End MRR(Last Year)";
            rowList.Add(lastEndMRRRow);

            lastRateMRRRow.Name = "Year on Year $ Growth Rate%";
            rowList.Add(lastRateMRRRow);

            lastEndCustomerRow.Name = "Mothly End Customer(Last Year)";
            rowList.Add(lastEndCustomerRow);

            lastRateCustomerRow.Name = "Year on Year # Growth Rate%";
            rowList.Add(lastRateCustomerRow);

            lastRetentionEndMRRRow.Name = "$Retention%";
            rowList.Add(lastRetentionEndMRRRow);

            lastRetentionRateMRRRow.Name = "$Retention Rate%";
            rowList.Add(lastRetentionRateMRRRow);

            lastRetentionEndCustomerRow.Name = "#Retention%";
            rowList.Add(lastRetentionEndCustomerRow);

            lastRetentionRateCustomerRow.Name = "#Retention Rate%";
            rowList.Add(lastRetentionRateCustomerRow);

            result.Months = viewData.Titles;
            result.MetricsRows = rowList;
            result.PrevViewDate = string.Format("{0:MMM yyyy}", start);
            result.NextViewDate = string.Format("{0:MMM yyyy}", countEnd.AddMonths(-1)); ;

            return result;
        }

        #endregion

       
    }
}
