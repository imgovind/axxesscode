﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using System.Threading;

    public class UserService : IUserService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IAgencyManagementMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
        }
        
        #endregion

        #region IUserService Members

        public bool CreateUser(User user, int clusterId)
        {
            try
            {
                var isNewLogin = false;

                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }

                user.LoginId = login.Id;
                user.Profile = new UserProfile();
                user.Profile.EmailWork = user.EmailAddress;
               // var agencySnapShot = loginRepository.GetAgencySnapshot(user.AgencyId);

                if (userRepository.AddUserOnly(user, clusterId))
                {
                    var userSnapShot = new UserSnapshot
                    {
                        Id = user.Id,
                        LoginId = user.LoginId,
                        AgencyId = user.AgencyId,
                        //DisplayName = user.DisplayName,
                        TitleType = user.DisplayTitle,
                        Status = user.Status,
                        Created = user.Created,
                        IsDeprecated = user.IsDeprecated
                    };
                    userRepository.LinkUserLocation(new UserLocation
                    {
                        UserId = user.Id,
                        Id = Guid.NewGuid(),
                        AgencyId = user.AgencyId,
                        AgencyLocationId = user.AgencyLocationId
                    }, clusterId);

                    if (loginRepository.AddUserSnapshot(userSnapShot))
                    {
                        if (isNewLogin)
                        {
                            ThreadPool.QueueUserWorkItem(state => SendNewUserNotifications(user));
                        }
                        else
                        {
                            ThreadPool.QueueUserWorkItem(state => SendExistingUserNotification(user));
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return false;
        }

        private static void SendNewUserNotifications(User user)
        {
            string invitationBodyText = string.Empty;
            string invitationSubject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);

            string welcomeBodyText = string.Empty;
            string welcomeSubject = "Welcome to AgencyCore, rated the Most Recommended Home Health Software Solution";

            var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
            invitationBodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, invitationSubject, invitationBodyText);

            Thread.Sleep(TimeSpan.FromMinutes(1));

            welcomeBodyText = MessageBuilder.PrepareTextFrom("NewUserWelcome");
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, welcomeSubject, welcomeBodyText, new string[] { "AgencyCore_Implementation_Checklist.docx", "New_Customer_Quick_Guide.docx", "PatientDataTemplate.xlsx" });
        }

        private static void SendExistingUserNotification(User user)
        {
            string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
            string bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
        }

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
               // var userSnapshot = userRepository.GetUserSnapshotByLoginId(login.Id);
                //var agencySnapshot = loginRepository.GetAgencySnapshot(userSnapshot.AgencyId);
               // var users = userRepository.GetUsersByLoginId(login.Id, agencySnapshot.ClusterId);
                //if (users.Count > 0)
                //{
                    result = true;
                //}
            }
            return result;
        }

        public IList<UserSelection> GetUsersWithLoginInfo(Guid agencyId)
        {
            var userSnapShots = loginRepository.GetUsersSnapShotWithLoginInfo(agencyId);
            if (userSnapShots != null && userSnapShots.Count > 0)
            {
                var userIds = userSnapShots.Select(u => u.Id).Distinct().ToList();
                var agency = loginRepository.GetAgencySnapshot(agencyId);
                if (agency!=null && agency.ClusterId > 0)
                {
                    var users = userRepository.GetUsersWithLoginInfo(agencyId, userIds, agency.ClusterId);
                    if (users != null && users.Count > 0)
                    {
                        userSnapShots.ForEach(u =>
                        {
                            var user = users.FirstOrDefault(av => av.Id == u.Id);
                            if (user != null)
                            {
                                u.FirstName = user.FirstName;
                                u.LastName = user.LastName;
                                u.Suffix = user.Suffix;
                                u.IsDeprecated = user.IsDeprecated;
                                u.Credentials = user.Credentials;
                                u.CredentialsOther = user.CredentialsOther;
                                u.Title = user.DisplayTitle;
                            }
                        });
                    }
                }
            }
            return userSnapShots ?? new List<UserSelection>();

        }

        #endregion
    }
}
