﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Log.Domain;
    using Axxess.Log.Repositories;
    using Axxess.Log.Enums;
    using Axxess.Membership.Repositories;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.SupportApp.ViewData;
    using Axxess.Membership.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.Log.Common;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.SupportApp.Services.Interfaces;

    public class LoginService : ILoginService
    {
        private readonly ISupportRepository supportRepository;
        private readonly ILogRepository logRepository;

        public LoginService(ISupportManagementDataProvider supportManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILogDataProvider logDataProvider)
        {
            Check.Argument.IsNotNull(logDataProvider, "logDataProvider");

            this.supportRepository = membershipDataProvider.SupportRepository;
            this.logRepository = logDataProvider.LogRepository;
        }


        public List<SupportAppAudit> GetSupportLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var supportLogs = new List<SupportAppAudit>();
            var logs = logRepository.GetSupportAudits(logDomain.ToString(), logType.ToString(), domainId, entityId);
            if (logs != null && logs.Count > 0)
            {
                var internalUserIds = new List<Guid>();
                logs.ForEach(n => { if (!n.UserId.IsEmpty()) { internalUserIds.Add(n.UserId); } });
                internalUserIds = internalUserIds.Distinct().ToList();
                var supports = internalUserIds.Count > 0 ? supportRepository.GetLoginDisplayNamesWithEmail(internalUserIds) ?? new List<SupportLogin>() : new List<SupportLogin>();
                logs.ForEach(log =>
                {
                    var support = supports.FirstOrDefault(s => s.Id == log.UserId);
                    if (support != null)
                    {
                        log.UserName = support.DisplayName;
                    }
                    supportLogs.Add(log);
                });
            }
            return supportLogs;
        }

        public InternalUserViewData GetUserPermissionsOnly(Guid loginId)
        {
            var viewData = new InternalUserViewData();
            var login = supportRepository.GetInternalUser(loginId);
            if (login != null)
            {
                viewData.Id = login.Id;
                viewData.LoginId = login.LoginId;
                viewData.EmailAddress = login.EmailAddress;
                viewData.DisplayName = login.DisplayName;
                viewData.PermissionData = FilterUserPermissions(login);
            }
            return viewData;
        }

        /// <summary>
        /// Filters the permissions of the site with what the user has access to and marks the ones the user has as checked
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>> FilterUserPermissions(InternalUser login)
        {
            //Deserialize the users permissions
            var userPermissions = login.Permissions.FromJson<Dictionary<int, List<int>>>() ?? new Dictionary<int, List<int>>();

            //Get the permission actions from the database
            var groupingOfPermissions = new Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>>();
            var allPermissions = supportRepository.GetPermissions();
            if (allPermissions != null)
            {
                //Group the permissions first by the category and then by the parent permission
                var groupedPermissons = from permission in allPermissions
                                        group permission by new MainCategoryPermissionGroupKey(permission.Category, permission.IsSubCategory)
                                            into categoryGroup
                                            from parentGroup in
                                                (from permission in categoryGroup
                                                 group permission by new ParentPermissionGroupKey(permission.Id))
                                            group parentGroup by categoryGroup.Key;



                //allPermissions.GroupBy(g => g.Id).ToDictionary(g => g.Key, g => g.ToList());
                foreach (var categoryGroup in groupedPermissons)
                {
                    var groupingOfParentPermissions = new Dictionary<ParentPermissionGroupKey, List<PermissionRow>>();
                    bool isAllSelected = true;
                    int numberOfPermissionsInCategory = 0;
                    foreach (var parentPermissionGroup in categoryGroup)
                    {
                        //var permissionGroup = new ParentPermissionGroupKey { Id = categoryGroup.Key };

                        var permissionList = new List<PermissionRow>();
                        //The users permissions for the group
                        List<int> userGroupPermissions;
                        userPermissions.TryGetValue(parentPermissionGroup.Key.Id, out userGroupPermissions);
                        userGroupPermissions = userGroupPermissions ?? new List<int>();

                        var count = 0;
                        foreach (var permission in parentPermissionGroup)
                        {
                            var isChecked = userGroupPermissions.Contains(permission.ChildId);
                            if (isChecked)
                            {
                                count++;
                            }
                            numberOfPermissionsInCategory++;
                            permissionList.Add(new PermissionRow() { Description = permission.Name.IsNotNullOrEmpty() ? permission.Name : (Enum.IsDefined(typeof(PermissionActions), permission.ChildId) ? ((PermissionActions)permission.ChildId).GetDescription() : string.Empty), ToolTip = permission.Description, IsChecked = isChecked, ChildId = permission.ChildId, });
                        }

                        //Add the grouping of the permission actions under a Parent Permission into permissionCategorizedList
                        if (permissionList != null && permissionList.Count > 0)
                        {
                            parentPermissionGroup.Key.IsAllSelected = (count == parentPermissionGroup.Count());
                            //If the permissions for the current main category are still all selected(isAllSelected is still true), but the permissions for this parent group are not
                            //then isAllSelected will be set to false
                            isAllSelected = isAllSelected && parentPermissionGroup.Key.IsAllSelected;
                            groupingOfParentPermissions.Add(parentPermissionGroup.Key, permissionList);
                            
                        }
                    }
                    if (groupingOfParentPermissions != null && groupingOfParentPermissions.Count > 0)
                    {
                        categoryGroup.Key.IsAllSelected = isAllSelected;
                        categoryGroup.Key.NumberOfPermissions = numberOfPermissionsInCategory;
                        groupingOfPermissions.Add(categoryGroup.Key, groupingOfParentPermissions);
                    }
                    
                }
            }
            return groupingOfPermissions;
        }

        public Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>> GetNewInternalUser()
        {
            return FilterUserPermissions(new InternalUser());
        }

        public JsonViewData UpdateInternalUser(SupportLogin login, Dictionary<int, List<int>> permissions)
        {
            var viewData = new JsonViewData();

            var existing = supportRepository.Find(login.Id);
            if (existing != null)
            {

                var internalUser = supportRepository.GetInternalUserOnly(existing.Id);
                if (internalUser != null)
                {

                    var oldPermissions = internalUser.Permissions;
                    internalUser.Permissions = permissions.ToJson();
                    if (supportRepository.UpdateInternalUserPermission(internalUser))
                    {
                        var isThereNameChange = existing.DisplayName.IsEqual(login.DisplayName);
                        if (isThereNameChange)
                        {
                            existing.DisplayName = login.DisplayName;
                            existing.IsAxxessAdmin = login.IsAxxessAdmin;
                            existing.IsAxxessSupport = login.IsAxxessSupport;
                        }
                        if (!isThereNameChange || (isThereNameChange && supportRepository.Update(existing)))
                        {
                            Auditor.AddSupportLog(Current.LoginId, LogDomain.SupportUser, login.Id, login.Id.ToString(), LogType.Login, LogAction.UserEdited, "");
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Login was updated successfully";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in updating the login.";
                            internalUser.Permissions = oldPermissions;
                            supportRepository.UpdateInternalUserPermission(internalUser);
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in updating the login.";

                    }

                }
            }
            return viewData;
        }

        public PermissionTemplateViewData GetUsePermissionsTemplates(int Id)
        {
            var viewData = new PermissionTemplateViewData();
            var template = supportRepository.GetTemplateOnly(Id);
            if (template != null)
            {
                viewData.Id = template.Id;
                viewData.Name = template.Name;
                viewData.PermissionData = FilterUserPermissionsTemplate(template);
            }
            return viewData;
        }

        private Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>> FilterUserPermissionsTemplate(PermissionTemplate template)
        {
            //Deserialize the users permissions
            var userPermissions = template.Permissions.FromJson<Dictionary<int, List<int>>>() ?? new Dictionary<int, List<int>>();

            //Get the permission actions from the database
            var groupingOfPermissions = new Dictionary<MainCategoryPermissionGroupKey, Dictionary<ParentPermissionGroupKey, List<PermissionRow>>>();
            var allPermissions = supportRepository.GetPermissions();
            if (allPermissions != null)
            {
                //Group the permissions first by the category and then by the parent permission
                var groupedPermissons = from permission in allPermissions
                                        group permission by new MainCategoryPermissionGroupKey(permission.Category, permission.IsSubCategory)
                                            into categoryGroup
                                            from parentGroup in
                                                (from permission in categoryGroup
                                                 group permission by new ParentPermissionGroupKey(permission.Id))
                                            group parentGroup by categoryGroup.Key;



                //allPermissions.GroupBy(g => g.Id).ToDictionary(g => g.Key, g => g.ToList());
                foreach (var categoryGroup in groupedPermissons)
                {
                    var groupingOfParentPermissions = new Dictionary<ParentPermissionGroupKey, List<PermissionRow>>();
                    bool isAllSelected = true;
                    int numberOfPermissionsInCategory = 0;
                    foreach (var parentPermissionGroup in categoryGroup)
                    {
                        //var permissionGroup = new ParentPermissionGroupKey { Id = categoryGroup.Key };

                        var permissionList = new List<PermissionRow>();
                        //The users permissions for the group
                        List<int> userGroupPermissions;
                        userPermissions.TryGetValue(parentPermissionGroup.Key.Id, out userGroupPermissions);
                        userGroupPermissions = userGroupPermissions ?? new List<int>();

                        var count = 0;
                        foreach (var permission in parentPermissionGroup)
                        {
                            var isChecked = userGroupPermissions.Contains(permission.ChildId);
                            if (isChecked)
                            {
                                count++;
                            }
                            numberOfPermissionsInCategory++;
                            permissionList.Add(new PermissionRow() { Description = permission.Name.IsNotNullOrEmpty() ? permission.Name : (Enum.IsDefined(typeof(PermissionActions), permission.ChildId) ? ((PermissionActions)permission.ChildId).GetDescription() : string.Empty), ToolTip = permission.Description, IsChecked = isChecked, ChildId = permission.ChildId, });
                        }

                        //Add the grouping of the permission actions under a Parent Permission into permissionCategorizedList
                        if (permissionList != null && permissionList.Count > 0)
                        {
                            parentPermissionGroup.Key.IsAllSelected = (count == parentPermissionGroup.Count());
                            //If the permissions for the current main category are still all selected(isAllSelected is still true), but the permissions for this parent group are not
                            //then isAllSelected will be set to false
                            isAllSelected = isAllSelected && parentPermissionGroup.Key.IsAllSelected;
                            groupingOfParentPermissions.Add(parentPermissionGroup.Key, permissionList);

                        }
                    }
                    if (groupingOfParentPermissions != null && groupingOfParentPermissions.Count > 0)
                    {
                        categoryGroup.Key.IsAllSelected = isAllSelected;
                        categoryGroup.Key.NumberOfPermissions = numberOfPermissionsInCategory;
                        groupingOfPermissions.Add(categoryGroup.Key, groupingOfParentPermissions);
                    }

                }
            }
            return groupingOfPermissions;
        }
    }
}
