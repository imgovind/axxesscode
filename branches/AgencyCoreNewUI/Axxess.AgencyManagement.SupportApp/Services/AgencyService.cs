﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;

    using Redmine.Net.Api;
    using Redmine.Net.Api.Types;

    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.Log.Repositories;

    using ViewData;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;
    using Axxess.Membership.Domain;
    using Axxess.LookUp.Domain;

    public class AgencyService : IAgencyService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly ISupportRepository supportRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ILogRepository logRepository;
        private readonly CustomerNotesService customerNotesService;
        private readonly ILoginRepository loginRepository;
        private readonly IAccountingRepository accountingRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider,IAgencyManagementMembershipDataProvider agencyManagementMembershipDataProvider, IMembershipDataProvider membershipDataProvider, IAccountingManagementDataProvider accountingManagementDataProvider, ILookUpDataProvider lookUpDataProvider, ILogDataProvider logDataProvider, CustomerNotesService customerNotesService)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
            Check.Argument.IsNotNull(logDataProvider, "logDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.logRepository = logDataProvider.LogRepository;
            this.customerNotesService = customerNotesService;
            this.loginRepository = agencyManagementMembershipDataProvider.LoginRepository;
            this.accountingRepository=accountingManagementDataProvider.AccountingRepository;

        }

        #endregion

        #region IAgencyService Members

        public bool CreateAgency(Agency agency)
        {
            try
            {
                var submitterInfo = agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger() ? lookupRepository.SubmitterInfo(agency.Payor.ToInteger()) : new AxxessSubmitterInfo();
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }
                if (agency.IsAxxessTheBiller)
                {
                    agency.SubmitterId = submitterInfo != null && submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : agency.SubmitterId;
                    agency.SubmitterName = submitterInfo != null && submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : agency.SubmitterId;
                    agency.SubmitterPhone = submitterInfo != null && submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : agency.SubmitterPhone;
                    agency.SubmitterFax = submitterInfo != null && submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : agency.SubmitterFax;
                }
                agency.ImplementationSpecialist = ImplementationSpecialist.Instance.Next();
                if (agency.ClusterId > 0)
                {
                    agency.Id = Guid.NewGuid();
                    if (agencyRepository.Add(agency, agency.ClusterId))
                    {
                        var agencySnapshot = new AgencySnapshot
                        {
                            Id = agency.Id,
                            Name = agency.Name,
                            IsSuspended = agency.IsSuspended,
                            IsDeprecated = agency.IsDeprecated,
                            IsFrozen = agency.IsFrozen,
                            FrozenDate = agency.FrozenDate,
                            ClusterId = agency.ClusterId,
                            Trainer = agency.Trainer,
                            BackupTrainer = agency.BackupTrainer,
                            SalesPerson = agency.SalesPerson,
                            ImplementationSpecialist = agency.ImplementationSpecialist,
                            Created = DateTime.Now,
                            AccountId = agency.AccountId
                        };
                        if (loginRepository.AddAgencySnapshot(agencySnapshot))
                        {
                            agencyRepository.InsertTemplates(agency.Id, agency.ClusterId);
                            agencyRepository.InsertSupplies(agency.Id, agency.ClusterId);
                            var conCode = new AgencyAdjustmentCode()
                            {
                                Id = Guid.NewGuid(),
                                AgencyId = agency.Id,
                                Code = "CON",
                                Description = "Contractual Obligations"
                            };

                            var bdCode = new AgencyAdjustmentCode()
                            {
                                Id = Guid.NewGuid(),
                                AgencyId = agency.Id,
                                Code = "BD",
                                Description = "Bad Debt"
                            };

                            agencyRepository.AddAdjustmentCode(conCode, agencySnapshot.ClusterId);
                            agencyRepository.AddAdjustmentCode(bdCode, agencySnapshot.ClusterId);
                            agencyRepository.InsertUploadTypes(agency.Id, agencySnapshot.ClusterId);

                            var location = new AgencyLocation();
                            location.Id = Guid.NewGuid();
                            location.Name = agency.LocationName;
                            location.AddressLine1 = agency.AddressLine1;
                            location.AddressLine2 = agency.AddressLine2;
                            location.AddressCity = agency.AddressCity;
                            location.AddressStateCode = agency.AddressStateCode;
                            location.AddressZipCode = agency.AddressZipCode;
                            location.AddressZipCodeFour = agency.AddressZipCodeFour;
                            location.AgencyId = agency.Id;
                            location.IsMainOffice = true;
                            location.IsDeprecated = false;
                            location.MedicareProviderNumber = agency.MedicareProviderNumber;
                            location.SubmitterId = agency.SubmitterId;
                            location.SubmitterName = agency.SubmitterName;
                            location.Payor = agency.Payor;
                            location.SubmitterPhone = agency.SubmitterPhone;
                            location.SubmitterFax = agency.SubmitterFax;
                            location.IsSubmitterInfoTheSame = true;
                            location.IsAxxessTheBiller = agency.IsAxxessTheBiller;
                            location.TaxId = agency.TaxId;
                            location.TaxIdType = agency.TaxIdType;

                            if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                            {
                                location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                            }
                            if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                            {
                                location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                            }


                            var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
                            if (defaultDisciplineTasks != null)
                            {
                                var costRates = new List<ChargeRate>();
                                defaultDisciplineTasks.ForEach(r =>
                                {
                                    if (r.Discipline.IsEqual("Nursing") || r.Discipline.IsEqual("PT") || r.Discipline.IsEqual("OT") || r.Discipline.IsEqual("ST") || r.Discipline.IsEqual("HHA") || r.Discipline.IsEqual("MSW"))
                                    {
                                        costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(), Charge = r.Rate });
                                    }
                                });
                                location.BillData = costRates.ToXml();
                            }

                            var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;

                            if (agencyRepository.AddLocation(location, agency.ClusterId))
                            {
                                accountingRepository.AddAccountAgency(agency.Id, location.Id, agency.ClusterId);
                                var plan = new AgencySubscriptionPlan
                                {
                                    AgencyId = agency.Id,
                                    PlanLimit = agency.PlanLimit,
                                    AgencyLocationId = location.Id,
                                    IsUserPlan = agency.IsUserPlan
                                };
                                if (accountingRepository.AddSubscriptionPlan(plan))
                                {
                                    var user = new Axxess.AgencyManagement.Domain.User
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agency.Id,
                                        AgencyName = agency.Name,
                                        AllowWeekendAccess = true,
                                        EmploymentType = "Employee",
                                        AgencyLocationId = location.Id,
                                        Status = (int)UserStatus.Active,
                                        LastName = agency.AgencyAdminLastName,
                                        FirstName = agency.AgencyAdminFirstName,
                                        PermissionsArray = GeneratePermissions(),
                                        EmailAddress = agency.AgencyAdminUsername,
                                        Credentials = CredentialTypes.None.GetDescription(),
                                        IsPrimary = true,
                                        TitleType = TitleTypes.Administrator.GetDescription(),
                                        Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                                        AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                                    };

                                    if (user.AgencyRoleList.Count > 0)
                                    {
                                        user.Roles = user.AgencyRoleList.ToArray().AddColons();
                                    }
                                    user.Status = (int)UserStatus.Active;
                                    user.ProfileData = user.Profile.ToXml();
                                    user.Messages = new List<MessageState>().ToXml();
                                    if (user.PermissionsArray.Count > 0)
                                    {
                                        user.Permissions = user.PermissionsArray.ToXml();
                                    }
                                    user.Created = DateTime.Now;
                                    user.Modified = DateTime.Now;

                                    IUserService userService = Container.Resolve<IUserService>();
                                    if (userService.CreateUser(user, agency.ClusterId))
                                    {
                                        try
                                        {
                                            string subject = string.Format("{0} needs to be trained.", agency.Name);
                                            string bodyText = MessageBuilder.PrepareTextFrom("TrainingRequest",
                                                "displayname", supportRepository.GetLoginDisplayName(agencySnapshot.Trainer),
                                                "agencyname", agency.Name,
                                                "agencyphone", location.PhoneWorkFormatted,
                                                "salesperson", supportRepository.GetLoginDisplayName(agencySnapshot.SalesPerson));

                                            Notify.User(CoreSettings.NoReplyEmail, string.Format("{0};{1};{2}", supportRepository.GetLoginEmailAddress(agencySnapshot.Trainer), supportRepository.GetLoginEmailAddress(agencySnapshot.SalesPerson), supportRepository.GetLoginEmailAddress(agencySnapshot.BackupTrainer)), subject, bodyText);

                                            string iSpecialistSubject = string.Format("You are the Implementation Specialist for {0}", agency.Name);
                                            string iSpecialistBodyText = MessageBuilder.PrepareTextFrom("ImplementationSpecialist",
                                                "displayname", supportRepository.GetLoginDisplayName(agencySnapshot.ImplementationSpecialist),
                                                "agencyname", agency.Name,
                                                "agencycitystate", string.Format("{0}, {1}", location.AddressCity, location.AddressStateCode));

                                            Notify.User(CoreSettings.NoReplyEmail, supportRepository.GetLoginEmailAddress(agencySnapshot.ImplementationSpecialist), iSpecialistSubject, iSpecialistBodyText, new string[] { "Implementation_Specialist_Checklist.docx" });
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Exception(ex);
                                        }
                                        return true;
                                      
                                    }

                                }
                                else
                                {
                                    agencyRepository.RemoveAgency(agency.Id, agency.ClusterId);
                                    agencyRepository.RemoveLocation(agency.Id, location.Id, agency.ClusterId);
                                    loginRepository.RemoveAgencySnapshot(agency.Id);
                                    accountingRepository.RemoveAccountAgency(agency.Id);
                                }
                                
                            }
                            else
                            {
                                agencyRepository.RemoveAgency(agency.Id, agency.ClusterId);
                                loginRepository.RemoveAgencySnapshot(agency.Id);
                            }
                        }
                        else
                        {
                            agencyRepository.RemoveAgency(agency.Id, agency.ClusterId);
                        }

                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        public bool UpdateAgency(Agency agency)
        {
            bool result = false;

            var agencySnapshot = loginRepository.GetAgencySnapshot(agency.Id);
            if (agencySnapshot != null)
            {
                var existingAgency = agencyRepository.GetAgencyOnly(agency.Id, agencySnapshot.ClusterId);
                if (agency != null)
                {
                    var currentPrimaryUser = userRepository.GetPrimaryUser(agency.Id, agencySnapshot.ClusterId);
                    if (currentPrimaryUser != null && agency.PrimaryUser != currentPrimaryUser.Id)
                    {
                        userRepository.TogglePrimaryUser(currentPrimaryUser.Id, agency.Id, agencySnapshot.ClusterId);
                        userRepository.TogglePrimaryUser(agency.PrimaryUser, agency.Id, agencySnapshot.ClusterId);
                    }
                    else if (currentPrimaryUser == null && !agency.PrimaryUser.IsEmpty())
                    {
                        userRepository.TogglePrimaryUser(agency.PrimaryUser, agency.Id, agencySnapshot.ClusterId);
                    }
                    var isSnapshotChange = false;
                    if (!existingAgency.Name.IsEqual(agency.Name))
                    {
                        isSnapshotChange = true;
                        existingAgency.Name = agency.Name;
                        agencySnapshot.Name = agency.Name;
                    }

                    existingAgency.TaxId = agency.TaxId;
                    existingAgency.IsAgreementSigned = agency.IsAgreementSigned;
                    existingAgency.TrialPeriod = agency.TrialPeriod;
                    existingAgency.Package = agency.Package;
                    existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                    existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                    existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                    existingAgency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                    existingAgency.TaxIdType = agency.TaxIdType;
                    existingAgency.Payor = agency.Payor;
                    existingAgency.SubmitterId = agency.SubmitterId;
                    existingAgency.SubmitterName = agency.SubmitterName;
                    existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                    existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                    existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                    existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                    existingAgency.CahpsVendor = agency.CahpsVendor;
                    existingAgency.IsAxxessTheBiller = agency.IsAxxessTheBiller;
                    existingAgency.Services = agency.Services;

                    if (agencySnapshot.SalesPerson != agency.SalesPerson)
                    {
                        isSnapshotChange = true;
                        existingAgency.SalesPerson = agency.SalesPerson;
                        agencySnapshot.SalesPerson = agency.SalesPerson;
                    }

                    if (agencySnapshot.Trainer != agency.Trainer)
                    {
                        isSnapshotChange = true;
                        existingAgency.Trainer = agency.Trainer;
                        agencySnapshot.Trainer = agency.Trainer;
                    }

                    if (agencySnapshot.BackupTrainer != agency.BackupTrainer)
                    {
                        isSnapshotChange = true;
                        existingAgency.BackupTrainer = agency.BackupTrainer;
                        agencySnapshot.BackupTrainer = agency.BackupTrainer;
                    }

                    if (agencySnapshot.ImplementationSpecialist != agency.ImplementationSpecialist)
                    {
                        isSnapshotChange = true;
                        existingAgency.ImplementationSpecialist = agency.ImplementationSpecialist;
                        agencySnapshot.ImplementationSpecialist = agency.ImplementationSpecialist;
                    }


                    existingAgency.PreviousSoftware = agency.PreviousSoftware;
                    if (!agency.IsAxxessTheBiller)
                    {
                        existingAgency.SubmitterId = agency.SubmitterId;
                        existingAgency.SubmitterName = agency.SubmitterName;
                        existingAgency.Payor = agency.Payor;
                        if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count > 0)
                        {
                            existingAgency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                        }
                        if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count > 0)
                        {
                            existingAgency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                        }
                    }
                    if (agencyRepository.Update(existingAgency, agencySnapshot.ClusterId))
                    {
                        if (isSnapshotChange)
                        {
                            loginRepository.UpdateAgencySnapshot(agencySnapshot);
                        }
                        var main = agencyRepository.GetMainLocation(agency.Id, agencySnapshot.ClusterId);
                        if (main != null)
                        {
                            main.MedicareProviderNumber = agency.MedicareProviderNumber;
                            if (agencyRepository.UpdateLocation(main.MedicareProviderNumber, main.Id, agencySnapshot.ClusterId))
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            try
            {
                var agencySnapShot = loginRepository.GetAgencySnapshot(location.AgencyId);
                if (agencySnapShot != null && agencySnapShot.ClusterId > 0)
                {
                    location.Id = Guid.NewGuid();

                    if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                    {
                        location.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                    {
                        location.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                    if (location.IsLocationStandAlone)
                    {
                        if (location.IsAxxessTheBiller)
                        {
                            var submitterInfo = location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger() ? lookupRepository.SubmitterInfo(location.Payor.ToInteger()) : new AxxessSubmitterInfo();
                            if (submitterInfo != null)
                            {
                                location.SubmitterId = submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : location.SubmitterId;
                                location.SubmitterName = submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : location.SubmitterId;
                                location.SubmitterPhone = submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : location.SubmitterPhone;
                                location.SubmitterFax = submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : location.SubmitterFax;
                            }
                            else
                            {
                                return false;
                            }
                        }

                    }
                    string cbsa = lookupRepository.CbsaCodeByZip(location.AddressZipCode);
                    location.CBSA = cbsa != null ? cbsa : string.Empty;
                    if (agencyRepository.AddLocation(location, agencySnapShot.ClusterId))
                    {
                        var subscriptionPlan = new AgencySubscriptionPlan
                        {
                            AgencyId = location.AgencyId,
                            AgencyLocationId = location.Id,
                            PlanLimit = location.PlanLimit,
                            IsUserPlan = location.IsUserPlan
                        };
                        result = accountingRepository.AddSubscriptionPlan(subscriptionPlan);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return result;
        }

        public bool UpdateLocation(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                var agencySnapshot = loginRepository.GetAgencySnapshot(location.AgencyId);
                if (agencySnapshot != null && agencySnapshot.ClusterId > 0)
                {
                    var existingLocation = agencyRepository.FindLocation(location.AgencyId, location.Id, agencySnapshot.ClusterId);
                    if (existingLocation != null)
                    {
                        if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                        {
                            existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                        }
                        if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                        {
                            existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                        }
                        existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                        if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                        {
                            existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                        }

                        if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                        {
                            existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                        }
                        if (location.ContactPhoneArray != null && location.ContactPhoneArray.Count == 3)
                        {
                            existingLocation.ContactPersonPhone = location.ContactPhoneArray.ToArray().PhoneEncode();
                        }
                        existingLocation.IsAxxessTheBiller = location.IsAxxessTheBiller;
                        if (existingLocation.IsLocationStandAlone)
                        {
                            if (existingLocation.IsAxxessTheBiller)
                            {
                                var submitterInfo = location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger() ? lookupRepository.SubmitterInfo(location.Payor.ToInteger()) : new AxxessSubmitterInfo();
                                if (submitterInfo != null)
                                {
                                    existingLocation.Payor = location.Payor;
                                    existingLocation.SubmitterId = submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : location.SubmitterId;
                                    existingLocation.SubmitterName = submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : location.SubmitterName;
                                    existingLocation.SubmitterPhone = submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : location.SubmitterPhone;
                                    existingLocation.SubmitterFax = submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : location.SubmitterFax;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                existingLocation.Payor = location.Payor;
                                existingLocation.SubmitterId = location.SubmitterId;
                                existingLocation.SubmitterName = location.SubmitterName;
                            }

                            existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                            existingLocation.TaxId = location.TaxId;
                            existingLocation.TaxIdType = location.TaxIdType;
                            existingLocation.NationalProviderNumber = location.NationalProviderNumber;
                            existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                            existingLocation.MedicaidProviderNumber = location.MedicaidProviderNumber;
                            existingLocation.HomeHealthAgencyId = location.HomeHealthAgencyId;
                            existingLocation.ContactPersonFirstName = location.ContactPersonFirstName;
                            existingLocation.ContactPersonLastName = location.ContactPersonLastName;
                            existingLocation.ContactPersonEmail = location.ContactPersonEmail;
                            existingLocation.CahpsVendor = location.CahpsVendor;
                            existingLocation.CahpsVendorId = location.CahpsVendorId;
                            existingLocation.CahpsSurveyDesignator = location.CahpsSurveyDesignator;
                            existingLocation.IsAxxessTheBiller = location.IsAxxessTheBiller;
                            existingLocation.OasisAuditVendor = location.OasisAuditVendor;
                        }
                        existingLocation.BranchId = location.BranchId;
                        existingLocation.BranchIdOther = location.BranchIdOther;
                        existingLocation.Name = location.Name;
                        existingLocation.CustomId = location.CustomId;
                        existingLocation.AddressLine1 = location.AddressLine1;
                        existingLocation.AddressLine2 = location.AddressLine2;
                        existingLocation.AddressCity = location.AddressCity;
                        existingLocation.AddressStateCode = location.AddressStateCode;
                        existingLocation.AddressZipCode = location.AddressZipCode;
                        existingLocation.AddressZipCodeFour = location.AddressZipCodeFour;
                        existingLocation.Comments = location.Comments;
                        if (agencyRepository.EditLocationModal(existingLocation, agencySnapshot.ClusterId))
                        {
                            var subscriptionPlan = accountingRepository.GetSubscriptionPlan(location.AgencyId, location.Id);
                            if (subscriptionPlan != null)
                            {
                                subscriptionPlan.IsUserPlan = location.IsUserPlan;
                                subscriptionPlan.PlanLimit = location.PlanLimit;
                                result = accountingRepository.UpdateSubscriptionPlan(subscriptionPlan);
                            }
                            else
                            {
                                subscriptionPlan = new AgencySubscriptionPlan
                                {
                                    AgencyId = location.AgencyId,
                                    AgencyLocationId = location.Id,
                                    PlanLimit = location.PlanLimit,
                                    IsUserPlan = location.IsUserPlan
                                };
                                result = accountingRepository.AddSubscriptionPlan(subscriptionPlan);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public List<SupportAppAudit> GetSupportLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var supportLogs = new List<SupportAppAudit>();
            var logs = logRepository.GetSupportAudits(logDomain.ToString(), logType.ToString(), domainId, entityId);
            if (logs != null && logs.Count > 0)
            {
                var userIds = logs.Select(l=>l.UserId).Distinct().ToList();
                var users = new List<SupportLogin>();
                if (userIds != null && userIds.Count > 0)
                {
                    users = supportRepository.GetLoginDisplayNamesWithEmail(userIds) ?? new List<SupportLogin>();
                }
                logs.ForEach(log =>
                {
                    var user = users.FirstOrDefault(u=>u.Id==log.UserId);
                    if (user != null)
                    {
                        log.UserName =user.DisplayName;
                    }
                    supportLogs.Add(log);
                });
            }
            return supportLogs;
        }

        public string AddFrozenAsset(Guid agencyId, HttpFileCollectionBase httpFiles)
        {
            Asset asset = AssetHelper.CreateAsset(agencyId, httpFiles);
            var assetBytes = new Byte[1];
            var assetId = "00000000-0000-0000-0000-000000000000";
            if (asset != null)
            {
                assetBytes = asset.Bytes;
                //customerNotesService.AddAsset(asset);

                bool result = false;
                if (SupportAppSettings.UseDBAssets)
                {
                    if (accountingRepository.Add(asset))
                    {
                        result = true;
                    }
                }
                else
                {
                     result = false;
                    byte[] temp = asset.Bytes;

                    if (!SupportAppSettings.StoreAssetBytesInDB)
                    {
                        asset.Bytes = new Byte[1];
                    }
                    if (assetRepository.Add(asset))
                    {
                        result = AssetHelper.HttpUpload(SupportAppSettings.AssetUploadURL, asset, temp);
                    }
                   
                }
                if (result)
                {
                    assetId = asset.Id.ToString();
                }
            }

            return assetId;
        }

        public List<AgencyLocationViewData> GetBranches(Guid agencyId)
        {
            var result = new List<AgencyLocationViewData>();
            var agencySnapShot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapShot != null && agencySnapShot.ClusterId > 0)
            {
                var locations = agencyRepository.GetBranchList(agencyId, agencySnapShot.ClusterId);
                if (locations != null && locations.Count > 0)
                {
                    var locationIds = locations.Select(l => l.Id).Distinct().ToList();
                    var subscriptionPlans = accountingRepository.GetLocationSubscriptionPlanByLocationId(agencySnapShot.Id, locationIds) ?? new List<AgencySubscriptionPlan>();
                    //if (subscriptionPlans != null && subscriptionPlans.Count > 0)
                    //{
                    locations.ForEach(location =>
                    {
                        var subscription = subscriptionPlans.FirstOrDefault(s => s.AgencyLocationId == location.Id);
                        //if (subscriptions != null && subscriptions.Count() > 0)
                        //{
                        //    subscriptions.ForEach(subscription =>
                        //        {
                        result.Add(new AgencyLocationViewData
                        {
                            Id = location.Id,
                            Name = location.Name,
                            AgencyId = location.AgencyId,
                            Address = location.AddressFull,
                            PlanLimit = subscription != null ? subscription.PlanLimit : 0,
                            IsUserPlan = subscription != null ? subscription.IsUserPlan : true,
                            Fax = location.FaxNumberFormatted,
                            Phone = location.PhoneWorkFormatted,
                            ProviderNumber = location.MedicareProviderNumber
                        });
                        //        });
                        //}
                    });


                    //}

                }
            }
            return result.OrderBy(l => l.Name).ToList();
        }

        public List<AgencyLite> GetAllAgencies(string stateFilter, DateTime? startDate, DateTime? endDate, int clusterId)
        {
            var agencies = new List<AgencyLite>();
            if (clusterId > 0)
            {
                agencies = agencyRepository.GetAllAgencies(stateFilter, startDate, endDate, clusterId);
            }
            else
            {
                var clusters = new int[] { 1, 2 };
                clusters.ForEach(c =>
                {
                    var list = agencyRepository.GetAllAgencies(stateFilter, startDate, endDate, c);
                    if (list != null && list.Count > 0)
                    {
                        agencies.AddRange(list);
                    }

                });
            }
            //var agencies = agencyRepository.GetAllAgencies(stateFilter, startDate, endDate, clusterId);
            if (agencies != null && agencies.Count > 0)
            {
                var loginIds = new List<Guid>();
                agencies.ForEach(a => { if (!a.TrainerId.IsEmpty()) { loginIds.Add(a.TrainerId); } if (!a.SalesPersonId.IsEmpty()) { loginIds.Add(a.SalesPersonId); } });
                loginIds = loginIds.Distinct().ToList();
                if (loginIds.Count > 0)
                {
                    var loginsWithName = supportRepository.GetLoginDisplayNamesWithEmail(loginIds);
                    if (loginsWithName != null && loginsWithName.Count > 0)
                    {
                        agencies.ForEach(a =>
                        {
                            var trainer = loginsWithName.FirstOrDefault(l=>l.Id==a.TrainerId);
                            if (trainer != null)
                            {
                                a.Trainer = trainer.DisplayName;
                            }
                            var sales = loginsWithName.FirstOrDefault(l => l.Id == a.SalesPersonId);
                            if (sales != null)
                            {
                                a.SalesPerson = sales.DisplayName;
                            }
                        });
                    }
                }
            }
            return agencies;
        }

        public IList<AgencyUpgrade> GetAgencyUpgrades(string isComplete, DateTime startDate, DateTime endDate, int clusterId)
        {
            var allList = accountingRepository.GetAgencyUpgrades(isComplete, startDate, endDate, clusterId) ?? new List<AgencyUpgrade>();
            if (allList != null && allList.Count > 0)
            {
                var users = new List<Axxess.AgencyManagement.Domain.User>();
                var locations = new List<LocationInfo>();
                var agencies = new List<AgencySnapshot>();
                if (clusterId > 0)
                {
                    var userIds = new List<Guid>();
                    var locationIds = new List<Guid>();
                    var agencyIds = new List<Guid>();
                    allList.ForEach(u => { if (u.ClusterId == clusterId) { if (!u.AgencyId.IsEmpty()) { agencyIds.Add(u.AgencyId); } if (!u.RequestedById.IsEmpty()) { userIds.Add(u.RequestedById); } if (!u.AgencyLocationId.IsEmpty()) { locationIds.Add(u.AgencyLocationId); } } });
                    userIds = userIds.Distinct().ToList();
                    locationIds = locationIds.Distinct().ToList();
                    
                    if (userIds != null && userIds.Count > 0)
                    {
                        users = userRepository.GetUsersWithLoginInfo(userIds, clusterId);
                    }
                    if (locationIds != null && locationIds.Count > 0)
                    {
                        locations = agencyRepository.GetLocationNameWithAgencyName(locationIds, clusterId);
                    }
                    agencyIds = agencyIds.Distinct().ToList();
                    if (agencyIds != null && agencyIds.Count > 0)
                    {
                        agencies = loginRepository.GetAgencySnapshotNames(agencyIds);
                    }
                }
                else
                {
                    var clusterIds = new int[] { 1, 2 };
                    clusterIds.ForEach(c =>
                    {
                        var userIds = new List<Guid>();
                        var locationIds = new List<Guid>();
                        var agencyIds = new List<Guid>();
                        allList.ForEach(u => { if (u.ClusterId == c) { if (!u.AgencyId.IsEmpty()) { agencyIds.Add(u.AgencyId); }  if (!u.RequestedById.IsEmpty()) { userIds.Add(u.RequestedById); } if (!u.AgencyLocationId.IsEmpty()) { locationIds.Add(u.AgencyLocationId); } } });
                        userIds = userIds.Distinct().ToList();
                        locationIds = locationIds.Distinct().ToList();
                        if (userIds != null && userIds.Count > 0)
                        {
                          var  clusterUsers = userRepository.GetUsersWithLoginInfo(userIds, c);
                          if (clusterUsers != null && clusterUsers.Count > 0)
                          {
                              users.AddRange(clusterUsers);
                          }
                        }

                        if (locationIds != null && locationIds.Count > 0)
                        {
                           var clusterLocations = agencyRepository.GetLocationNameWithAgencyName(locationIds, c);
                           if (clusterLocations!=null && clusterLocations.Count > 0)
                           {
                               locations.AddRange(clusterLocations);
                           }
                        }

                        agencyIds = agencyIds.Distinct().ToList();
                        if (agencyIds != null && agencyIds.Count > 0)
                        {
                            agencies = loginRepository.GetAgencySnapshotNames(agencyIds);
                        }
                    });
                }
                allList.ForEach(uc =>
                {
                    var location = locations.FirstOrDefault(l=>l.Id==uc.AgencyLocationId);
                    if (location != null)
                    {
                        uc.LocationName = location.Name;
                        uc.AgencyName = location.AgencyName;
                    }
                    else
                    {
                        var agency = agencies.FirstOrDefault(l => l.Id == uc.AgencyId);
                        if (agency != null)
                        {
                            uc.AgencyName = agency.Name;
                        }
                    }
                    var user = users.FirstOrDefault(u =>u.Id == uc.RequestedById);
                    if (user != null)
                    {
                        uc.RequestedBy = user.DisplayName;
                    }
                });
            }
            return allList;
        }

        public IList<AgencyUpgrade> GetAgencyAnnualPlan(string isComplete, DateTime startDate, DateTime endDate, int clusterId)
        {
            var allList = accountingRepository.GetAgencyAnnualPlan(isComplete, startDate, endDate, clusterId) ?? new List<AgencyUpgrade>();
            if (allList != null && allList.Count > 0)
            {
                var users = new List<Axxess.AgencyManagement.Domain.User>();
                var locations = new List<LocationInfo>();
                var agencies = new List<AgencySnapshot>();
                if (clusterId > 0)
                {
                    var userIds = new List<Guid>();
                    var locationIds = new List<Guid>();
                    var agencyIds = new List<Guid>();
                    allList.ForEach(u => { if (u.ClusterId == clusterId) { if (!u.AgencyId.IsEmpty()) { agencyIds.Add(u.AgencyId); } if (!u.RequestedById.IsEmpty()) { userIds.Add(u.RequestedById); } if (!u.AgencyLocationId.IsEmpty()) { locationIds.Add(u.AgencyLocationId); } } });
                    userIds = userIds.Distinct().ToList();
                    locationIds = locationIds.Distinct().ToList();
                    if (userIds != null && userIds.Count > 0)
                    {
                        users = userRepository.GetUsersWithLoginInfo(userIds, clusterId);
                    }
                    if (locationIds != null && locationIds.Count > 0)
                    {
                        locations = agencyRepository.GetLocationNameWithAgencyName(locationIds, clusterId);
                    }
                    agencyIds = agencyIds.Distinct().ToList();
                    if (agencyIds != null && agencyIds.Count > 0)
                    {
                        agencies = loginRepository.GetAgencySnapshotNames(agencyIds);
                    }
                }
                else
                {
                    var clusterIds = new int[] { 1, 2 };
                    clusterIds.ForEach(c =>
                    {
                        var userIds = new List<Guid>();
                        var locationIds = new List<Guid>();
                        var agencyIds = new List<Guid>();
                        allList.ForEach(u => { if (u.ClusterId == c) { if (!u.AgencyId.IsEmpty()) { agencyIds.Add(u.AgencyId); } if (!u.RequestedById.IsEmpty()) { userIds.Add(u.RequestedById); } if (!u.AgencyLocationId.IsEmpty()) { locationIds.Add(u.AgencyLocationId); } } });
                        userIds = userIds.Distinct().ToList();
                        locationIds = locationIds.Distinct().ToList();
                        if (userIds != null && userIds.Count > 0)
                        {
                            var clusterUsers = userRepository.GetUsersWithLoginInfo(userIds, c);
                            if (clusterUsers != null && clusterUsers.Count > 0)
                            {
                                users.AddRange(clusterUsers);
                            }
                        }
                        if (locationIds != null && locationIds.Count > 0)
                        {
                            var clusterLocations = agencyRepository.GetLocationNameWithAgencyName(locationIds, c);
                            if (clusterLocations != null && clusterLocations.Count > 0)
                            {
                                locations.AddRange(clusterLocations);
                            }
                        }

                        agencyIds = agencyIds.Distinct().ToList();
                        if (agencyIds != null && agencyIds.Count > 0)
                        {
                            agencies = loginRepository.GetAgencySnapshotNames(agencyIds);
                        }
                    });
                }
                allList.ForEach(uc =>
                {
                    var location = locations.FirstOrDefault(l => l.AgencyId == uc.AgencyId && l.Id == uc.AgencyLocationId);
                    if (location != null)
                    {
                        uc.LocationName = location.Name;
                        uc.AgencyName = location.AgencyName;
                    }
                    else
                    {
                        var agency = agencies.FirstOrDefault(l => l.Id == uc.AgencyId);
                        if (agency != null)
                        {
                            uc.AgencyName = agency.Name;
                        }
                    }
                    var user = users.FirstOrDefault(u => u.Id == uc.RequestedById);
                    if (user != null)
                    {
                        uc.RequestedBy = user.DisplayName;
                    }
                });
            }
            return allList;
        }

        public IList<AgencySpecialist> GetImplementationSpecialists( Guid implementationSpecialistId,  DateTime startDate,  DateTime endDate)
        {
            IList<AgencySpecialist> list = new List<AgencySpecialist>();
            var clusterIds = new int[] { 1, 2 };
            if (clusterIds != null)
            {
                 list = loginRepository.GetImplementationSpecialists(implementationSpecialistId, startDate, endDate);
                if (list != null && list.Count > 0)
                {
                    var contacts = new List<AgencyLite>();
                    foreach (var clusterId in clusterIds)
                    {
                        var agencyIds = list.Where(l => l.ClusterId == clusterId && !l.AgencyId.IsEmpty()).Select(l => l.AgencyId).Distinct().ToList();
                        if (agencyIds != null && agencyIds.Count > 0)
                        {
                            var clusterAgencyContacts = agencyRepository.GetAgencyContactInformations(agencyIds, clusterId);
                            if (clusterAgencyContacts != null && clusterAgencyContacts.Count > 0)
                            {
                                contacts.AddRange(clusterAgencyContacts);
                            }
                        }
                    }
                    var isSigle = !implementationSpecialistId.IsEmpty();
                    var users = new List<SupportLogin>();
                    var name = string.Empty;
                    var email = string.Empty;
                    if (isSigle)
                    {
                        var user = supportRepository.Find(implementationSpecialistId);
                        if (user != null)
                        {
                            name = user.DisplayName;
                            email = user.EmailAddress;
                        }

                    }
                    else
                    {
                        var userIds = list.Select(l => l.SpecialistId).Distinct().ToList();
                        if (userIds != null && userIds.Count > 0)
                        {
                            users = supportRepository.GetLoginDisplayNamesWithEmail(userIds) ?? new List<SupportLogin>();
                        }
                    }

                    list.ForEach(uc =>
                    {
                        var contact = contacts.FirstOrDefault(l => l.Id == uc.AgencyId);
                        if (contact != null)
                        {
                            uc.AgencyContactPersonPhone = contact.ContactPersonPhoneFormatted;
                            uc.AgencyContactPersonEmail = contact.ContactPersonEmail;
                            uc.AgencyContactDisplayName = contact.ContactPersonDisplayName;
                        }
                        if (isSigle)
                        {
                            uc.SpecialistName = name;
                            uc.SpecialistEmailAddress = email;
                        }
                        else
                        {
                            var user = users.FirstOrDefault(u => u.Id == uc.SpecialistId);
                            if (user != null)
                            {
                                uc.SpecialistName = user.DisplayName;
                                uc.SpecialistEmailAddress = user.EmailAddress;
                            }
                        }
                    });
                }

                }
            return list;
        }

        public IList<AgencyMarkAsReadOnly> GetAgencyMarkAsReadOnly(DateTime startDate,  DateTime endDate)
        {
           IList<AgencyMarkAsReadOnly>  list = new List<AgencyMarkAsReadOnly>();
            var clusterIds = new int[] { 1, 2 };
            if (clusterIds != null)
            {
                list = accountingRepository.GetAgencyMarkAsReadOnly(startDate, endDate);
                if (list != null && list.Count > 0)
                {
                    var contacts = new List<AgencyLite>();
                    foreach (var clusterId in clusterIds)
                    {
                        var agencyIds = list.Where(l => l.ClusterId == clusterId && !l.AgencyId.IsEmpty()).Select(l => l.AgencyId).Distinct().ToList();
                        if (agencyIds != null && agencyIds.Count > 0)
                        {
                            var clusterAgencyContacts = agencyRepository.GetAgencyContactInformations(agencyIds, clusterId);
                            if (clusterAgencyContacts != null && clusterAgencyContacts.Count > 0)
                            {
                                contacts.AddRange(clusterAgencyContacts);
                            }
                        }
                    }
                   

                    list.ForEach(uc =>
                    {

                        var contact = contacts.FirstOrDefault(l => l.Id == uc.AgencyId);
                        if (contact != null)
                        {
                            uc.AgencyName = contact.Name;
                            uc.IsFrozen = contact.IsFrozen;
                            uc.ContactPersonPhone = contact.ContactPersonPhoneFormatted;
                            uc.ContactPersonEmail = contact.ContactPersonEmail;
                            uc.ContactPersonDisplayName = contact.ContactPersonDisplayName;
                        }
                    });
                }

            }

            //var allList = new List<AgencyMarkAsReadOnly>();
            //var clusterIds = new int[] { 1, 2 };
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            //        var List = agencyRepository.GetAgencyMarkAsReadOnly(startDate, endDate, clusterId);
            //        if (List != null && List.Count > 0)
            //        {
            //            allList.AddRange(List);
            //        }
            //    }
            //}
            return list;
        }


        #endregion

        #region Private Methods

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();
            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        #endregion
    }
}
