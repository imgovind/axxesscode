﻿namespace Axxess.AgencyManagement.SupportApp.Security
{
    using System;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    
    using Security;
    using Axxess.AgencyManagement.SupportApp.ViewData;

    public interface ISupportMembershipService
    {
        void LogOff(string userName);
        bool Activate(Account account);
        bool ResendActivationLink(Guid userId, Guid agencyId);
        bool ResendPhysicianActivationLink(Guid physicianId, Guid agencyId);
        AxxessSupportPrincipal Get(string userName);
        LoginAttemptType Validate(string userName, string password);
        string GetLiveImpersonationPhysicianUrl(Guid loginId);
        string GetImpersonationPhysicianUrl(Guid loginId);
        string GetImpersonationUrl(Guid agencyId, Guid userId);
        string GetLiveImpersonationUrl(Guid agencyId, Guid userId);
        
    }
}
