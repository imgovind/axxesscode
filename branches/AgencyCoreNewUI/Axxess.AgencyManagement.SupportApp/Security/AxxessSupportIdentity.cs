﻿namespace Axxess.AgencyManagement.SupportApp.Security
{
    using System;
    using System.Security.Principal;

    using Axxess.Membership.Domain;

    using Axxess.Core.Infrastructure;

    using Domain;

    [Serializable]
    public class AxxessSupportIdentity : IIdentity
    {
        #region Constructor

        public AxxessSupportIdentity()
        {
            this.IsAuthenticated = true;
            this.Session = new AdminSession();
            this.LastSecureActivity = DateTime.Now;
        }

        public AxxessSupportIdentity(Guid id, string name) : this()
        {
            this.Id = id;
            this.Name = name;
        }

        #endregion

        #region IIdentity Members

        public string Name { get; private set; }

        public bool IsAuthenticated { get; set; }

        public AdminSession Session { get; set; }

        public string AuthenticationType
        {
            get { return SupportAppSettings.AuthenticationType; }
        }

        #endregion

        #region Axxess Identity Members

        public Guid Id { get; private set; }

        public DateTime LastSecureActivity { get; set; }

        #endregion

    }
}
