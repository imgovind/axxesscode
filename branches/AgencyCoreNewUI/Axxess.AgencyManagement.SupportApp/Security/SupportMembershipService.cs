﻿namespace Axxess.AgencyManagement.SupportApp.Security
{
    using System;
    using System.Web;
    using System.Text;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using System.Linq;
    using Axxess.AgencyManagement.SupportApp.ViewData;

    public class SupportMembershipService : ISupportMembershipService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ISupportRepository supportRepository;
        private readonly IPhysicianRepository physicianRepository;

        #endregion

        #region Constructor

        public SupportMembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider, IAgencyManagementMembershipDataProvider agencyManagementMembershipProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.userRepository = agencyManagementProvider.UserRepository;
            this.loginRepository = agencyManagementMembershipProvider.LoginRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
            this.physicianRepository = agencyManagementProvider.PhysicianRepository;
        }

        #endregion

        #region IMembershipService Members

        public bool Activate(Account account)
        {
            var login = supportRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string passwordSalt = string.Empty;
                string passwordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                login.PasswordSalt = passwordSalt;
                login.SignatureSalt = passwordSalt;
                login.PasswordHash = passwordHash;
                login.SignatureHash = passwordHash;
                login.LastLoginDate = DateTime.Now;
                if (supportRepository.Update(login))
                {
                    AxxessSupportPrincipal principal = Get(login.EmailAddress);
                    if (principal != null)
                    {
                        account.UserName = login.EmailAddress;
                        return true;
                    }
                }
            }

            return false;
        }

        public AxxessSupportPrincipal Get(string userName)
        {
            var principal = Cacher.Get<AxxessSupportPrincipal>(userName);
            if (principal == null)
            {
                var login = supportRepository.Find(userName);

                if (login != null)
                {
                    var internalUser = supportRepository.GetInternalUserOnly(login.Id);

                    if (internalUser != null)
                    {
                        var identity = new AxxessSupportIdentity(login.Id, login.EmailAddress);
                        identity.Session =
                            new AdminSession
                            {
                                LoginId = login.Id,
                                DisplayName = login.DisplayName,
                                Permissions = internalUser.Permissions.FromJson<Dictionary<int, List<int>>>() ?? new Dictionary<int, List<int>>(),
                                IsAxxessAdmin = login.IsAxxessAdmin,
                                IsAxxessSupport = login.IsAxxessSupport
                            };

                        principal = new AxxessSupportPrincipal(identity);
                        Cacher.Set<AxxessSupportPrincipal>(userName, principal);

                    }
                }
            }

            return principal;
        }

        //private AxxessSupportPrincipal GetPrincipalHelper(Agency agency, InternalUser internalUser, Login login,  bool isLink, ImpersonationLink link, string dbIp)
        //{
        //    AxxessSupportPrincipal principal = null;
        //    if (internalUser != null)
        //    {
        //        if (login != null)
        //        {
        //           if (agency != null)
        //                {
        //                    AxxessSupportIdentity identity = new AxxessSupportIdentity(login.Id, login.EmailAddress);
        //                    Dictionary<int, List<int>> permissions = internalUser.Permissions.FromJson<Dictionary<int, List<int>>>();
        //                    //var acessibleServices = user.AccessibleServices & agency.Services;
        //                    var formattedPermissions = internalUser.Permissions.FromJson<Dictionary<int, List<int>>>;// GetPermission(permissions, acessibleServices, user.ReportActions);

        //                    //IDictionary<int, Dictionary<int, List<int>>> reportPermissions = user.ReportPermissions.FromJson<Dictionary<int, Dictionary<int, List<int>>>>();
        //                    //var formattedReportPermissions = GetPermission(reportPermissions, acessibleServices);
        //                    identity.Session =
        //                        new AdminSession
        //                        {
        //                            LoginId = login.Id,
        //                            DisplayName = login.DisplayName,
        //                            Permissions = formattedPermissions
        //                            //UserId = user.Id,
        //                            //FullName = user.DisplayName,
        //                            //AgencyId = agency.Id,
        //                            //AgencyName = agency.Name,
        //                            //IsPrimary = user.IsPrimary,
        //                            //IsAgencyFrozen = agency.IsFrozen,
        //                            //Services = agency.Services,
        //                            //AcessibleServices = acessibleServices,
        //                            //PreferredService = user.PreferredService,
        //                            //LoginDay = DateTime.Today.Day,
        //                            //AccountExpirationDate = user.AccountExpireDate,
        //                            //MaxAgencyUserCount = agency.Package == 0 ? 100000000 : agency.Package,
        //                            //AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty,
        //                            //Address = user.Profile != null ? user.Profile.AddressFull : string.Empty,
        //                            //EarliestLoginTime = user.EarliestLoginTime.IsNotNullOrEmpty() ? user.EarliestLoginTime : string.Empty,
        //                            //AutomaticLogoutTime = user.AutomaticLogoutTime.IsNotNullOrEmpty() ? user.AutomaticLogoutTime : string.Empty,
        //                            //OasisVendorExist = agency.OasisAuditVendorApiKey.IsNotNullOrEmpty(),
        //                            //DBServerIp = dbIp,
        //                            //Payor = agency.Payor,
        //                            //ReportPermissions = formattedReportPermissions

        //                        };
        //                    if (isLink && link != null)
        //                    {
        //                        identity.IsImpersonated = true;
        //                        identity.Session.ImpersonatorName = link.RepName;
        //                    }
        //                    //TODO: Remove permission array
        //                    var permissionsArray = new List<string>();
        //                    if (user.Permissions.IsNotNullOrEmpty())
        //                    {
        //                        permissionsArray = user.Permissions.ToObject<List<string>>();
        //                    }
        //                    else
        //                    {
        //                        permissionsArray = new List<string>();
        //                    }


        //                    principal = new AxxessPrincipal(identity, role, permissionsArray, formattedPermissions);
        //                    Cacher.Set<AxxessPrincipal>(isLink && link != null ? string.Format("{0}:{1}", login.EmailAddress, link.Id) : login.EmailAddress, principal);
        //                }

        //        }
        //    }
        //    return principal;
        //}

        public LoginAttemptType Validate(string userName, string password)
        {
            var loginAttempt = LoginAttemptType.Failed;
            var login = supportRepository.Find(userName);

            if (login != null)
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(password.Trim(), login.PasswordHash, login.PasswordSalt))
                {
                    var internalUser = supportRepository.GetInternalUserOnly(login.Id);
                    if (internalUser != null)
                    {
                        var identity = new AxxessSupportIdentity(login.Id, login.EmailAddress);
                        identity.Session =
                            new AdminSession
                            {
                                LoginId = login.Id,
                                DisplayName = login.DisplayName,
                                Permissions = internalUser.Permissions.FromJson<Dictionary<int, List<int>>>() ?? new Dictionary<int, List<int>>(),
                                IsAxxessAdmin = login.IsAxxessAdmin,
                                IsAxxessSupport = login.IsAxxessSupport
                            };

                        login.LastLoginDate = DateTime.Now;
                        if (supportRepository.Update(login))
                        {
                            loginAttempt = LoginAttemptType.Success;
                        }

                        var principal = new AxxessSupportPrincipal(identity);
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                        Cacher.Set<AxxessSupportPrincipal>(userName, principal);
                    }
                }
            }

            if (loginAttempt == LoginAttemptType.Failed)
            {
                LoginMonitor.Instance.Track(userName, Current.IpAddress);
                if (!LoginMonitor.Instance.HasAttempts(userName, Current.IpAddress))
                {
                    loginAttempt = LoginAttemptType.TooManyAttempts;
                }
            }

            return loginAttempt;
        }

        public string GetLiveImpersonationPhysicianUrl(Guid loginId)
        {
            var url = new StringBuilder();

            if (!loginId.IsEmpty())
            {
                var link = new ImpersonationLink
                {
                    LoginId = loginId,
                    UserId = Guid.Empty,
                    AgencyId = Guid.Empty,
                    RepName = Current.DisplayName,
                    RepLoginId = Current.LoginId
                };

                if (loginRepository.AddImpersonationLink(link))
                {
                    url.Append(SupportAppSettings.LiveImpersonatePhysicianUrl);
                    url.Append(Crypto.Encrypt(string.Format("linkid={0}", link.Id.ToString())));
                }
            }

            return url.ToString();
        }

        public string GetImpersonationPhysicianUrl(Guid loginId)
        {
            var url = new StringBuilder();

            if (!loginId.IsEmpty())
            {
                var link = new ImpersonationLink
                {
                    LoginId = loginId,
                    UserId = Guid.Empty,
                    AgencyId = Guid.Empty,
                    RepName = Current.DisplayName,
                    RepLoginId = Current.LoginId
                };

                if (loginRepository.AddImpersonationLink(link))
                {
                    url.Append(SupportAppSettings.ImpersonatePhysicianUrl);
                    url.Append(Crypto.Encrypt(string.Format("linkid={0}", link.Id.ToString())));
                }
            }

            return url.ToString();
        }

        public string GetLiveImpersonationUrl(Guid agencyId, Guid userId)
        {
            var url = new StringBuilder();
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null)
            {
                var user = loginRepository.GetUserSnapshot(agencyId, userId);
                if (user != null)
                {
                    var link = new ImpersonationLink
                    {
                        UserId = userId,
                        AgencyId = agencyId,
                        LoginId = user.LoginId,
                        RepName = Current.DisplayName,
                        RepLoginId = Current.LoginId
                    };

                    if (loginRepository.AddImpersonationLink(link))
                    {
                        url.Append(string.Format(SupportAppSettings.LiveImpersonateUrl, agencySnapshot.ClusterId));
                        url.Append(Crypto.Encrypt(string.Format("linkid={0}", link.Id.ToString())));
                    }
                }
            }

            return url.ToString();
        }

        public string GetImpersonationUrl(Guid agencyId, Guid userId)
        {
            var url = new StringBuilder();
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            var user = loginRepository.GetUserSnapshot(agencyId, userId);
            if (user != null)
            {
                var link = new ImpersonationLink
                {
                    UserId = userId,
                    AgencyId = agencyId,
                    LoginId = user.LoginId,
                    RepName = Current.DisplayName,
                    RepLoginId = Current.LoginId
                };
                if (loginRepository.AddImpersonationLink(link))
                {
                    url.Append(string.Format(SupportAppSettings.ImpersonateUrl, agencySnapshot.ClusterId));
                    url.Append(Crypto.Encrypt(string.Format("linkid={0}", link.Id, link.AgencyId)));
                }
            }
            return url.ToString();
        }

        public void LogOff(string userName)
        {
            SessionStore.Abandon();
            Cacher.Remove(userName);
        }

        public bool ResendActivationLink(Guid userId, Guid agencyId)
        {
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null && agencySnapshot.ClusterId > 0)
            {
                var user = userRepository.Get(userId, agencyId, agencySnapshot.ClusterId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        string bodyText = string.Empty;
                        string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", agencySnapshot.Name);

                        // var agency = agencyRepository.GetAgencySnapshot(agencyId);
                        var agencyName = agencySnapshot.Name;
                        if (loginRepository.IsAtLeastOneUserExist(login.Id))
                        {
                            bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", agencyName);
                        }
                        else
                        {
                            var parameters = string.Format("id={0}&agencyid={1}", userId, agencyId);
                            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                            bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", agencyName, "encryptedQueryString", encryptedParameters);
                        }

                        Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, subject, bodyText);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ResendPhysicianActivationLink(Guid physicianId, Guid agencyId)
        {
            var agencySnapshot = loginRepository.GetAgencySnapshot(agencyId);
            if (agencySnapshot != null)
            {
                var physician = physicianRepository.Get(physicianId, agencyId, agencySnapshot.ClusterId);
                if (physician != null)
                {
                    var login = loginRepository.Find(physician.LoginId);
                    if (login != null)
                    {
                        string bodyText = string.Empty;
                        //var agency = agencyRepository.GetAgencySnapshot(agencyId);
                        var agencyName = agencySnapshot.Name;
                        string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", agencyName);
                        if (loginRepository.IsAtLeastOnePhysicianExist(login.Id))
                        {
                            bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", physician.FirstName, "agencyname", agencyName);
                        }
                        else
                        {
                            var parameters = string.Format("loginid={0}&type=password", physician.LoginId);
                            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                            bodyText = MessageBuilder.PrepareTextFrom("NewPhysicianConfirmation", "firstname", physician.FirstName, "agencyname", agencyName, "encryptedQueryString", encryptedParameters);
                        }

                        Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, subject, bodyText);
                        return true;
                    }
                }
            }
            return false;
        }




        #endregion
    }
}
