﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.Core.Extension;
    
    using Enums;
    using Extensions;

    [Serializable]
    public class AssessmentFactory
    {
        #region Assessment Static Method Members

        public static Assessment Create(string assessmentType)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                    assessment = new FollowUpAssessment();
                    break;
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    assessment = new StartOfCareAssessment();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathPT":
                case "OASISCDeathOT":
                    assessment = new DeathAtHomeAssessment();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                    assessment = new RecertificationAssessment();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                    assessment = new ResumptionofCareAssessment();
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    assessment = new TransferDischargeAssessment();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargePT":
                case "OASISCDischargeOT":
                    assessment = new DischargeFromAgencyAssessment();
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    assessment = new TransferNotDischargedAssessment();
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare": 
                    assessment = new NonOasisStartOfCareAssessment();
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    assessment = new NonOasisRecertificationAssessment();
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    assessment = new NonOasisDischargeAssessment();
                    break;
                default:
                    break;
            }
            return assessment;
        }

        #endregion 
    }
}
