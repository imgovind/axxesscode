﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Web.Script.Serialization;

    using Axxess.Core.Extension;
    using Axxess.OasisC.Enums;

    public class AssessmentExport
    {
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        [ScriptIgnore()]
        public int InsuranceId { get; set; }
        public string Insurance { get; set; }
        public string Identifier { get { return string.Format("{0}|{1}", this.AssessmentId, this.AssessmentType); } }
        public string PatientName { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public string AssessmentName { get { return Enum.IsDefined(typeof(AssessmentType), this.AssessmentType) ? ((AssessmentType)Enum.Parse(typeof(AssessmentType), this.AssessmentType)).GetDescription() : string.Empty; } }
        public DateTime AssessmentDate { get; set; }
        public DateTime ExportedDate { get; set; }

        public DateTime EventDate { get; set; }
        public DateTime VisitDate { get; set; }

        [ScriptIgnore()]
        public string PaymentSources { get; set; }

        //[ScriptIgnore()]
        //public string EpisodeData { get; set; }
        [ScriptIgnore()]
        public DateTime EpisodeStartDate { get; set; }
        [ScriptIgnore()]
        public DateTime EpisodeEndDate { get; set; }
        
        [ScriptIgnore()]
        public int CorrectionNumber { get; set; }

        public string AssessmentDateFormatted { get { return AssessmentDate != DateTime.MinValue ? AssessmentDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string CorrectionNumberFormat { get { return string.Format("{4:00} ( <a class=\"link\" onclick=\"OASIS.Export.EditCorrectionNumber('{0}','{1}','{2}','{3}','{4}');return false\">Edit</a>  )", this.AssessmentId, this.PatientId, this.EpisodeId, this.AssessmentType, this.CorrectionNumber); } }
        public string EpisodeRange { get { return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy")); } }
    }
}
