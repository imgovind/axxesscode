﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public class AssessmentRepository : IAssessmentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssessmentRepository Members

        public bool Add(Assessment assessment)
        {
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Created = DateTime.Now;
                assessment.Modified = DateTime.Now;
               return database.InsertAny(assessment);
            }
            return false;
        }

        public bool Update(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Modified = DateTime.Now;
                result = database.UpdateAny(assessment);
            }
            return result;
        }

        public bool UpdateModal(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                assessment.Modified = DateTime.Now;
                result = database.UpdateAny(assessment);
            }
            return result;
        }

        public Assessment Get(Guid assessmentId,Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentId, agencyId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, bool isDeprecated)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty()&& !agencyId.IsEmpty())
            {
                var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId);
                if (assessment != null)
                {
                    assessment.IsDeprecated = isDeprecated;
                    assessment.Modified = DateTime.Now;
                    result= database.UpdateAny(assessment);
                }
            }
            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid employeeId)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty()  && !employeeId.IsEmpty())
            {
                var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId);
                if (assessment != null)
                {
                    try
                    {
                        assessment.UserId = employeeId;
                        assessment.Modified = DateTime.Now;
                        result = database.UpdateAny(assessment); 
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
                else
                {
                    result = true;
                }
                
            }
            return result;
        }

        public Assessment GetWithNoList(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId)
        {
            Assessment assessment = null;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId);
            }
            return assessment;
        }

        public Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, PatientId, EpisodeId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid assessmentId, Guid patientId, Guid episodeId, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty()&& !patientId.IsEmpty() && !episodeId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId);
            }
            return assessment;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                var notIncludedAssessmentTypes = new int[] { (int)AssessmentType.PlanOfCare485, (int)AssessmentType.NonOasisDischarge, (int)AssessmentType.NonOasisRecertification, (int)AssessmentType.NonOasisStartOfCare };
                var assessmentTypes = Enum.GetValues(typeof(AssessmentType)).Cast<AssessmentType>().Where(d => !notIncludedAssessmentTypes.Contains((int)d)).Select(d => d.ToString()).ToArray();

                assessments = database.FindAnyByStatusLean(agencyId, branchId, assessmentTypes, status, patientStatus, startDate, endDate);
            }
            return assessments;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                var notIncludedAssessmentTypes = new int[] { (int)AssessmentType.PlanOfCare485, (int)AssessmentType.NonOasisDischarge, (int)AssessmentType.NonOasisRecertification, (int)AssessmentType.NonOasisStartOfCare };
                var assessmentTypes = Enum.GetValues(typeof(AssessmentType)).Cast<AssessmentType>().Where(d => !notIncludedAssessmentTypes.Contains((int)d)).Select(d => d.ToString()).ToArray();
                assessments = database.FindAnyByStatusLean(agencyId, branchId, assessmentTypes, status, paymentSources);
            }
            return assessments;
        }

        public bool UsePreviousAssessment(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid previousAssessmentId, string previousAssessmentType)
        {
            bool result = false;

            var currentAssessment = database.FindAny(agencyId, assessmentId, patientId, episodeId);
            if (currentAssessment != null)
            {
                var previousAssessment = database.FindAny( previousAssessmentId, agencyId);
                if (previousAssessment != null && previousAssessment.OasisData.IsNotNullOrEmpty())
                {
                    currentAssessment.OasisData = previousAssessment.OasisData;
                    currentAssessment.Questions = currentAssessment.OasisData.ToObject<List<Question>>();
                    return Update(currentAssessment);
                }
            }
            return result;
        }

        #endregion
    }
}
