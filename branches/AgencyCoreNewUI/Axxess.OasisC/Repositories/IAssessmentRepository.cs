﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    public interface IAssessmentRepository
    {
        bool Add(Assessment oasisAssessment);
        bool Update(Assessment oasisAssessment);
        bool UpdateModal(Assessment assessment);
        bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, bool isDeprecated);
        bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid employeeId);
        Assessment Get(Guid assessmentId, Guid agencyId);
        Assessment Get(Guid assessmentId, Guid patientId, Guid episodeId,Guid agencyId);
        Assessment GetAssessmentOnly(Guid assessmentId, Guid patientId, Guid episodeId, Guid agencyId);
        Assessment GetWithNoList(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId);
        List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate);
        List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources);
        bool UsePreviousAssessment(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid previousAssessmentId, string previousAssessmentType);
    }
}
