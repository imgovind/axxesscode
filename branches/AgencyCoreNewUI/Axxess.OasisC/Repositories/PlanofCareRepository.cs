﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;

    public class PlanofCareRepository : IPlanofCareRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PlanofCareRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IPlanofCareRepository Member

        public bool Add(PlanofCare planofCare)
        {
            if (planofCare != null)
            {
                planofCare.Created = DateTime.Now;
                planofCare.Modified = DateTime.Now;
                database.Add<PlanofCare>(planofCare);
                return true;
            }
            return false;
        }

        public bool AddStandAlone(PlanofCareStandAlone planofCare)
        {
            if (planofCare != null)
            {
                planofCare.IsDeprecated = false;
                planofCare.Created = DateTime.Now;
                planofCare.Modified = DateTime.Now;
                database.Add<PlanofCareStandAlone>(planofCare);
                return true;
            }
            return false;
        }

        public bool Update(PlanofCare planofCare)
        {
            bool result = false;

            if (planofCare != null)
            {
                planofCare.Modified = DateTime.Now;
                database.Update<PlanofCare>(planofCare);
                result = true;
            }
            return result;
        }

        public bool UpdateStandAlone(PlanofCareStandAlone planofCare)
        {
            bool result = false;

            if (planofCare != null)
            {
                planofCare.Modified = DateTime.Now;
                database.Update<PlanofCareStandAlone>(planofCare);
                result = true;
            }
            return result;
        }

        public PlanofCare Get(Guid Id)
        {
            return database.Single<PlanofCare>(p =>  p.Id == Id);
        }

        public PlanofCare Get(Guid agencyId, Guid Id)
        {
            return database.Single<PlanofCare>(p => p.Id == Id && p.AgencyId == agencyId);
        }

        public PlanofCare Get(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        }

        public PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid Id)
        {
            return database.Single<PlanofCareStandAlone>(p =>p.AgencyId == agencyId && p.Id == Id);
        }

        public PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return database.Single<PlanofCareStandAlone>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        }

        public PlanofCare Get(Guid agencyId, Guid assessmentId, string assessmentType)
        {
            return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.AssessmentId == assessmentId && p.AssessmentType == assessmentType && p.IsDeprecated == false);
        }

        public bool MarkAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            var planOfCare = database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
            if (planOfCare != null)
            {
                planOfCare.IsDeprecated = isDeprecated;
                planOfCare.Modified = DateTime.Now;
                database.Update<PlanofCare>(planOfCare);
                result = true;
            }
            return result;
        }

        public bool MarkStandAloneAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            var planOfCare = database.Single<PlanofCareStandAlone>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
            if (planOfCare != null)
            {
                planOfCare.IsDeprecated = isDeprecated;
                planOfCare.Modified = DateTime.Now;
                database.Update<PlanofCareStandAlone>(planOfCare);
                result = true;
            }
            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        {
            bool result = false;
            var planOfCare = database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
            if (planOfCare != null)
            {
                try
                {
                    planOfCare.UserId = employeeId;
                    planOfCare.Modified = DateTime.Now;
                    database.Update<PlanofCare>(planOfCare);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            if (status > 0)
            {
                return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.Status == status && p.IsDeprecated==false).ToList();
            }
            return new List<PlanofCare>();
        }

        public List<PlanofCareStandAlone> GetPlanofCareStandAloneByStatus(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            if (status > 0)
            {
                return database.Find<PlanofCareStandAlone>(p => p.AgencyId == agencyId && p.Status == status && p.IsDeprecated == false).ToList();
            }
            return new List<PlanofCareStandAlone>();
        }

        public List<PlanofCare> GetByPhysicianId(List<Guid> physicianIdentifiers, int status)
        {
            var ids = new StringBuilder();

            physicianIdentifiers.ForEach(i =>
            {
                ids.AppendFormat("'{0}',", i.ToString());
            });

            var query = new QueryBuilder("select * from `planofcares`")
                .Where(string.Format("`planofcares`.Status = {0}", status))
                .And("`planofcares`.IsDeprecated = 0")
                .And(string.Format("`planofcares`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

            return new FluentCommand<PlanofCare>(query.Build())
                .SetConnection("OasisCConnectionString")
                .AsList();

        }

        public List<PlanofCareStandAlone> GetStandAloneByPhysicianId(List<Guid> physicianIdentifiers, int status)
        {
            var ids = new StringBuilder();

            physicianIdentifiers.ForEach(i =>
            {
                ids.AppendFormat("'{0}',", i.ToString());
            });

            var query = new QueryBuilder("select * from `planofcarestandalones`")
                .Where(string.Format("`planofcarestandalones`.Status = {0}", status))
                .And("`planofcarestandalones`.IsDeprecated = 0")
                .And(string.Format("`planofcarestandalones`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

            return new FluentCommand<PlanofCareStandAlone>(query.Build())
                .SetConnection("OasisCConnectionString")
                .AsList();

        }

        public List<PlanofCare> GetPlanOfCareOrders(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.IsDeprecated == false).ToList();
        }

        public List<PlanofCare> GetPatientPlanOfCare(Guid agencyId , Guid patientId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.PatientId==patientId && p.IsDeprecated == false).ToList();
        }

        public List<PlanofCareStandAlone> GetPatientPlanOfCareStandAlone(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PlanofCareStandAlone>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.IsDeprecated == false).ToList();
        }

        public List<PlanofCare> GetPlanofCares(Guid agencyId, int status, string orderIds)
        {
            var script = string.Format(@"SELECT 
                                    oasisc.planofcares.Id as Id,
                                    oasisc.planofcares.EpisodeId as EpisodeId,
                                    oasisc.planofcares.OrderNumber as OrderNumber, 
                                    agencymanagement.patients.Id as PatientId ,
                                    agencymanagement.patients.FirstName as FirstName,
                                    agencymanagement.patients.LastName as LastName,
                                    oasisc.planofcares.Status as Status ,
                                    oasisc.planofcares.Created as Created ,
                                    oasisc.planofcares.ReceivedDate as ReceivedDate,
                                    oasisc.planofcares.SentDate as SentDate,
                                    oasisc.planofcares.PhysicianData as PhysicianData , 
                                    oasisc.planofcares.PhysicianId as PhysicianId , 
                                    oasisc.planofcares.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM 
                                            oasisc.planofcares
                                                INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id 
                                                    WHERE
                                                        oasisc.planofcares.AgencyId = @agencyid AND
                                                        agencymanagement.patients.IsDeprecated = 0 AND
                                                        agencymanagement.patients.Status IN (1,2) AND 
                                                        oasisc.planofcares.IsDeprecated = 0 AND
                                                        oasisc.planofcares.Id IN ( {0} ) AND
                                                        oasisc.planofcares.Status = @status", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCare> GetPlanofCares(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT
                                    oasisc.planofcares.Id as Id,
                                    oasisc.planofcares.EpisodeId as EpisodeId,
                                    oasisc.planofcares.OrderNumber as OrderNumber,
                                    agencymanagement.patients.Id as PatientId ,
                                    agencymanagement.patients.FirstName as FirstName,
                                    agencymanagement.patients.LastName as LastName, 
                                    oasisc.planofcares.Status as Status ,
                                    oasisc.planofcares.Created as Created ,
                                    oasisc.planofcares.ReceivedDate as ReceivedDate, 
                                    oasisc.planofcares.SentDate as SentDate, 
                                    oasisc.planofcares.PhysicianData as PhysicianData ,
                                    oasisc.planofcares.PhysicianId as PhysicianId  
                                        FROM
                                            oasisc.planofcares
                                                INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id 
                                                    WHERE
                                                        oasisc.planofcares.AgencyId = @agencyid AND
                                                        agencymanagement.patients.IsDeprecated = 0 AND
                                                        agencymanagement.patients.Status IN (1,2) AND 
                                                        oasisc.planofcares.IsDeprecated = 0 AND
                                                        oasisc.planofcares.Id IN ( {0} ) ", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid()? reader.GetStringNullable("PhysicianId").ToGuid():Guid.Empty
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCare> GetPatientPlanofCares(Guid agencyId, Guid patientId , string orderIds)
        {
            var script = string.Format(@"SELECT 
                                            oasisc.planofcares.Id as Id, 
                                            oasisc.planofcares.EpisodeId as EpisodeId,
                                            oasisc.planofcares.OrderNumber as OrderNumber,
                                            agencymanagement.patients.Id as PatientId , 
                                            oasisc.planofcares.Status as Status ,
                                            oasisc.planofcares.Created as Created ,
                                            oasisc.planofcares.ReceivedDate as ReceivedDate, 
                                            oasisc.planofcares.SentDate as SentDate,
                                            oasisc.planofcares.PhysicianData as PhysicianData ,
                                            oasisc.planofcares.PhysicianId as PhysicianId  
                                                FROM 
                                                    oasisc.planofcares 
                                                        INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id  
                                                            WHERE 
                                                                oasisc.planofcares.AgencyId = @agencyid AND
                                                                oasisc.planofcares.PatientId= @patientId AND
                                                                oasisc.planofcares.IsDeprecated = 0 AND
                                                                oasisc.planofcares.Id IN ( {0} ) ", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianData = reader.GetStringNullable("PhysicianData")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCare> GetPendingSignaturePlanofCares(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT 
                                oasisc.planofcares.Id as Id,
                                oasisc.planofcares.EpisodeId as EpisodeId,
                                oasisc.planofcares.OrderNumber as OrderNumber,
                                agencymanagement.patients.Id as PatientId ,
                                agencymanagement.patients.FirstName as FirstName,
                                agencymanagement.patients.LastName as LastName, 
                                oasisc.planofcares.Status as Status ,
                                oasisc.planofcares.Created as Created , 
                                oasisc.planofcares.ReceivedDate as ReceivedDate,
                                oasisc.planofcares.SentDate as SentDate,
                                oasisc.planofcares.PhysicianData as PhysicianData , 
                                oasisc.planofcares.PhysicianId as PhysicianId , 
                                oasisc.planofcares.PhysicianSignatureDate as PhysicianSignatureDate 
                                    FROM 
                                        oasisc.planofcares 
                                            INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id 
                                                WHERE 
                                                    oasisc.planofcares.AgencyId = @agencyid AND
                                                    agencymanagement.patients.IsDeprecated = 0 AND
                                                    agencymanagement.patients.Status IN (1,2) AND
                                                    oasisc.planofcares.IsDeprecated = 0 AND 
                                                    oasisc.planofcares.Id IN ( {0} ) AND 
                                                    ( oasisc.planofcares.Status = 130 || oasisc.planofcares.Status = 145 )", orderIds);

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")

                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCareStandAlone> GetPlanofCaresStandAloneByStatus(Guid agencyId, int status, string orderIds)
        {
            var script = string.Format(@"SELECT
                                    oasisc.planofcarestandalones.Id as Id,
                                    oasisc.planofcarestandalones.EpisodeId as EpisodeId,
                                    oasisc.planofcarestandalones.OrderNumber as OrderNumber,
                                    agencymanagement.patients.Id as PatientId ,
                                    agencymanagement.patients.FirstName as FirstName,
                                    agencymanagement.patients.LastName as LastName, 
                                    oasisc.planofcarestandalones.Status as Status ,
                                    oasisc.planofcarestandalones.Created as Created ,
                                    oasisc.planofcarestandalones.ReceivedDate as ReceivedDate, 
                                    oasisc.planofcarestandalones.SentDate as SentDate,
                                    oasisc.planofcarestandalones.PhysicianData as PhysicianData ,
                                    oasisc.planofcarestandalones.PhysicianId as PhysicianId ,
                                    oasisc.planofcarestandalones.PhysicianSignatureDate as PhysicianSignatureDate
                                        FROM 
                                            oasisc.planofcarestandalones 
                                                INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id 
                                                    WHERE
                                                        oasisc.planofcarestandalones.AgencyId = @agencyid AND 
                                                        agencymanagement.patients.IsDeprecated = 0 AND
                                                        agencymanagement.patients.Status IN (1,2) AND 
                                                        oasisc.planofcarestandalones.IsDeprecated = 0 AND
                                                        oasisc.planofcarestandalones.Id IN ( {0} ) AND
                                                        ( oasisc.planofcarestandalones.Status = @status )", orderIds);
            var list = new List<PlanofCareStandAlone>();
            using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new PlanofCareStandAlone
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCareStandAlone> GetPlanofCaresStandAlones(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT
                                    oasisc.planofcarestandalones.Id as Id, 
                                    oasisc.planofcarestandalones.EpisodeId as EpisodeId,
                                    oasisc.planofcarestandalones.OrderNumber as OrderNumber,
                                    agencymanagement.patients.Id as PatientId ,
                                    agencymanagement.patients.FirstName as FirstName,
                                    agencymanagement.patients.LastName as LastName,
                                    oasisc.planofcarestandalones.Status as Status ,
                                    oasisc.planofcarestandalones.Created as Created ,
                                    oasisc.planofcarestandalones.ReceivedDate as ReceivedDate, 
                                    oasisc.planofcarestandalones.SentDate as SentDate, 
                                    oasisc.planofcarestandalones.PhysicianData as PhysicianData ,
                                    oasisc.planofcarestandalones.PhysicianId as PhysicianId ,
                                    oasisc.planofcarestandalones.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM 
                                            oasisc.planofcarestandalones 
                                                INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id 
                                                    WHERE
                                                        oasisc.planofcarestandalones.AgencyId = @agencyid AND 
                                                        agencymanagement.patients.IsDeprecated = 0 AND 
                                                        agencymanagement.patients.Status IN (1,2) AND 
                                                        oasisc.planofcarestandalones.IsDeprecated = 0 AND
                                                        oasisc.planofcarestandalones.Id IN ( {0} )", orderIds);
            var list = new List<PlanofCareStandAlone>();
            using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PlanofCareStandAlone
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid()? reader.GetStringNullable("PhysicianId").ToGuid():Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCareStandAlone> GetPatientPlanofCaresStandAlones(Guid agencyId, Guid patientId, string orderIds)
        {
            var script = string.Format(@"SELECT
                                        oasisc.planofcarestandalones.Id as Id,
                                        oasisc.planofcarestandalones.EpisodeId as EpisodeId,
                                        oasisc.planofcarestandalones.OrderNumber as OrderNumber,
                                        agencymanagement.patients.Id as PatientId ,
                                        oasisc.planofcarestandalones.Status as Status ,
                                        oasisc.planofcarestandalones.Created as Created ,
                                        oasisc.planofcarestandalones.ReceivedDate as ReceivedDate,
                                        oasisc.planofcarestandalones.SentDate as SentDate, 
                                        oasisc.planofcarestandalones.PhysicianData as PhysicianData , 
                                        oasisc.planofcarestandalones.PhysicianId as PhysicianId 
                                            FROM 
                                                oasisc.planofcarestandalones 
                                                    INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id 
                                                        WHERE
                                                            oasisc.planofcarestandalones.AgencyId = @agencyid AND
                                                            oasisc.planofcarestandalones.PatientId = @patientId AND 
                                                            oasisc.planofcarestandalones.IsDeprecated = 0 AND
                                                            oasisc.planofcarestandalones.Id IN ( {0} )", orderIds);
            var list = new List<PlanofCareStandAlone>();
            using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new PlanofCareStandAlone
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid()? reader.GetStringNullable("PhysicianId").ToGuid():Guid.Empty
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCareStandAlone> GetPendingSignaturePlanofCaresStandAlone(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT 
                                    oasisc.planofcarestandalones.Id as Id,
                                    oasisc.planofcarestandalones.EpisodeId as EpisodeId,
                                    oasisc.planofcarestandalones.OrderNumber as OrderNumber,
                                    agencymanagement.patients.Id as PatientId ,
                                    agencymanagement.patients.FirstName as FirstName,
                                    agencymanagement.patients.LastName as LastName,
                                    oasisc.planofcarestandalones.Status as Status ,
                                    oasisc.planofcarestandalones.Created as Created ,
                                    oasisc.planofcarestandalones.ReceivedDate as ReceivedDate,
                                    oasisc.planofcarestandalones.SentDate as SentDate,
                                    oasisc.planofcarestandalones.PhysicianData as PhysicianData ,
                                    oasisc.planofcarestandalones.PhysicianId as PhysicianId ,
                                    oasisc.planofcarestandalones.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM 
                                            oasisc.planofcarestandalones
                                                INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id 
                                                    WHERE
                                                        oasisc.planofcarestandalones.AgencyId = @agencyid AND 
                                                        agencymanagement.patients.IsDeprecated = 0 AND
                                                        agencymanagement.patients.Status IN (1,2) AND
                                                        oasisc.planofcarestandalones.IsDeprecated = 0 AND
                                                        oasisc.planofcarestandalones.Id IN ( {0} ) AND 
                                                        ( oasisc.planofcarestandalones.Status = 130 || oasisc.planofcarestandalones.Status = 145 )", orderIds);

            var list = new List<PlanofCareStandAlone>();
            using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PlanofCareStandAlone
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId =reader.GetGuid("PhysicianId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PlanofCare> GetAllPlanOfCareOrders()
        {
            return database.All<PlanofCare>().ToList();
        }

        public List<PlanofCareStandAlone> GetAllPlanofCareStandAlones()
        {
            return database.All<PlanofCareStandAlone>().ToList();
        }

        #endregion

    }
}
