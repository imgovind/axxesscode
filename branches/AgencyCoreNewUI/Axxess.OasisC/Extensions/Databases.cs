﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using Domain;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    

    public static class Databases
    {
        public static bool InsertAny(this SimpleRepository repository, Assessment assessment)
        {
            var result = false;
            try
            {
                repository.Add<Assessment>(assessment); 
                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public static bool UpdateAny(this SimpleRepository repository, Assessment assessment)
        {
            var result = false;
            if (assessment != null)
            {
                try
                {
                    result = repository.Update<Assessment>(assessment) >= 1;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public static Assessment FindAny(this SimpleRepository repository, Guid assessmentId, Guid agencyId)
        {
            var assessment = repository.Single<Assessment>(e => e.Id == assessmentId && e.AgencyId == agencyId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeIdentifier(assessment.Type.ToString());
            }
            return assessment;
        }

        public static Assessment FindAny(this SimpleRepository repository, Guid agencyId, Guid assessmentId, Guid patientId, Guid episodeId)
        {
            var assessment = repository.Single<Assessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeIdentifier(assessment.Type.ToString());
            }
            return assessment;
        }

        public static List<AssessmentExport> FindAnyByStatusLean(this SimpleRepository repository, Guid agencyId, Guid branchId, string [] assessmentTypes, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            var table = "assessments";

            if (table.IsNotNullOrEmpty())
            {
                var patientStatusQuery = string.Empty; ;
                if (patientStatus <= 0)
                {
                    patientStatusQuery = " AND agencymanagement.patients.Status IN (1,2) ";
                }
                else
                {
                    patientStatusQuery = " AND agencymanagement.patients.Status = @statusid ";
                }

                var dateRange = string.Empty;
                if (status == 225 || status == 240)
                {
                    dateRange = string.Format("AND oasisc.{0}.ExportedDate between @startdate and @enddate ", table);
                }
                else if (status == 220)
                {
                    dateRange = string.Format("AND oasisc.{0}.AssessmentDate between @startdate and @enddate ", table);
                }

                var script = string.Format(@"SELECT 
                                agencymanagement.patients.Id as PatientId, 
                                agencymanagement.patients.FirstName as FirstName, 
                                agencymanagement.patients.LastName as LastName, 
                                agencymanagement.patients.PrimaryInsurance as InsuranceId , 
                                oasisc.{0}.Id  as Id , 
                                oasisc.{0}.VersionNumber as VersionNumber, 
                                oasisc.{0}.AssessmentDate as AssessmentDate , 
                                oasisc.{0}.Type as Type ,
                                oasisc.{0}.Modified as Modified , 
                                oasisc.{0}.ExportedDate as ExportedDate , 
                                oasisc.{0}.EpisodeId as EpisodeId, 
                                agencymanagement.patientepisodes.EndDate as EndDate, 
                                agencymanagement.patientepisodes.StartDate as StartDate ,
                                agencymanagement.scheduleevents.EventDate as EventDate ,
                                agencymanagement.scheduleevents.VisitDate as VisitDate 
                                        FROM oasisc.{0} 
                                                INNER JOIN agencymanagement.patients ON oasisc.{0}.PatientId = agencymanagement.patients.Id
                                                INNER JOIN agencymanagement.patientepisodes ON  oasisc.{0}.EpisodeId = agencymanagement.patientepisodes.Id 
                                                INNER JOIN agencymanagement.scheduleevents ON  oasisc.{0}.Id = agencymanagement.scheduleevents.EventId
                                                        WHERE oasisc.{0}.AgencyId = @agencyid {2} {3} {4} AND
                                                              oasisc.{0}.Type IN ( {5} ) AND 
                                                              agencymanagement.patients.IsDeprecated = 0 AND
                                                              agencymanagement.patientepisodes.IsDischarged = 0 AND
                                                              agencymanagement.patientepisodes.IsActive = 1 AND 
                                                              agencymanagement.scheduleevents.IsMissedVisit = 0 AND
                                                              agencymanagement.scheduleevents.IsDeprecated = 0 AND
                                                              DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND
                                                              oasisc.{0}.Status = @status ", table, status, patientStatusQuery, !branchId.IsEmpty() ? " AND agencymanagement.patients.AgencyLocationId = @branchId " : string.Empty, dateRange, assessmentTypes.Select(d => "\'" + d + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    assessments = cmd.SetConnection("AgencyManagementConnectionString")
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("statusid", patientStatus)
                       .AddDateTime("startDate", startDate)
                       .AddDateTime("endDate", endDate)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           EventDate = reader.GetDateTime("EventDate"),
                           VisitDate = reader.GetDateTime("VisitDate"),
                           AssessmentType = reader.GetStringNullable("Type"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }
            }
            return assessments;

        }

        public static List<AssessmentExport> FindAnyByStatusLean(this SimpleRepository repository, Guid agencyId, Guid branchId, string [] assessmentTypes, int status, List<int> paymentSources)
        {
            var results = new List<AssessmentExport>();
            var assessments = new List<AssessmentExport>();
            var table = "assessments";
            if (table.IsNotNullOrEmpty())
            {
                var script = string.Format(
                    @"SELECT 
                        agencymanagement.patients.Id as PatientId,
                        agencymanagement.patients.FirstName as FirstName, 
                        agencymanagement.patients.LastName as LastName, 
                        agencymanagement.patients.PrimaryInsurance as InsuranceId, 
                        oasisc.{0}.Id  as Id,
                        oasisc.{0}.VersionNumber as VersionNumber,
                        oasisc.{0}.AssessmentDate as AssessmentDate, 
                        oasisc.{0}.Modified as Modified, 
                        oasisc.{0}.ExportedDate as ExportedDate, 
                        oasisc.{0}.EpisodeId as EpisodeId, 
                        oasisc.{0}.Type as Type, 
                        agencymanagement.patientepisodes.EndDate as EndDate, 
                        agencymanagement.patientepisodes.StartDate as StartDate, 
                        agencymanagement.patients.PaymentSource as PaymentSources,
                        agencymanagement.scheduleevents.EventDate as EventDate ,
                        agencymanagement.scheduleevents.VisitDate as VisitDate 
                                FROM oasisc.{0} 
                                    INNER JOIN agencymanagement.patients ON oasisc.{0}.PatientId = agencymanagement.patients.Id
                                    INNER JOIN agencymanagement.patientepisodes ON  oasisc.{0}.EpisodeId = agencymanagement.patientepisodes.Id 
                                    INNER JOIN agencymanagement.scheduleevents ON  oasisc.{0}.Id = agencymanagement.scheduleevents.EventId
                                            WHERE oasisc.{0}.AgencyId = @agencyid AND 
                                                  agencymanagement.patients.Status IN (1,2) {1} AND 
                                                  agencymanagement.patients.IsDeprecated = 0 AND 
                                                  agencymanagement.patientepisodes.IsDischarged = 0 AND 
                                                  agencymanagement.patientepisodes.IsActive = 1 AND 
                                                  agencymanagement.scheduleevents.IsMissedVisit = 0 AND
                                                  agencymanagement.scheduleevents.IsDeprecated = 0 AND
                                                  DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND
                                                  oasisc.{0}.Type IN ( {2} ) AND 
                                                  oasisc.{0}.Status = @status ", table, !branchId.IsEmpty() ? " AND agencymanagement.patients.AgencyLocationId = @branchId " : string.Empty, assessmentTypes.Select(d => "\'" + d + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    assessments = cmd.SetConnection("AgencyManagementConnectionString")
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           EventDate = reader.GetDateTime("EventDate"),
                           VisitDate = reader.GetDateTime("VisitDate"),
                           AssessmentType = reader.GetString("Type"),
                           PaymentSources = reader.GetStringNullable("PaymentSources"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }

                if (assessments != null && assessments.Count > 0)
                {
                    assessments.ForEach(a =>
                    {
                        if (a.PaymentSources.IsNotNullOrEmpty())
                        {
                            paymentSources.ForEach(p =>
                            {
                                if (a.PaymentSources.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Contains(p.ToString()))
                                {
                                    results.Add(a);
                                    return;
                                }
                            });
                        }
                    });
                }
            }
            return results;
        }

        public static string AssessmentTypeIdentifier(string assessmentType)
        {
            var type = string.Empty;
            switch (assessmentType)
            {
                    
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    type = AssessmentType.StartOfCare.ToString();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    type = AssessmentType.ResumptionOfCare.ToString();
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    type = AssessmentType.FollowUp.ToString();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    type = AssessmentType.Recertification.ToString();
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    type = AssessmentType.TransferInPatientNotDischarged.ToString();
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    type = AssessmentType.TransferInPatientDischarged.ToString();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    type = AssessmentType.DischargeFromAgencyDeath.ToString();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    type = AssessmentType.DischargeFromAgency.ToString();
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    type = AssessmentType.NonOasisDischarge.ToString();
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    type = AssessmentType.NonOasisStartOfCare.ToString();
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    type = AssessmentType.NonOasisRecertification.ToString();
                    break;
                default:
                    break;
            }
            return type;
        }

    }
}
