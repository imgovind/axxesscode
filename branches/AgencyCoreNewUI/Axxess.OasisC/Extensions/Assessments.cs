﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Extension;

    public static class Assessments
    {
        public static IDictionary<string, Question> ToDictionary(this Assessment assessment)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (assessment != null && assessment.Questions != null)
            {
                var key = string.Empty;
                assessment.Questions.ForEach(question =>
                {
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                        if (!questions.ContainsKey(key))
                        {
                            questions.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                          if (!questions.ContainsKey(key))
                          {
                              questions.Add(key, question);
                          }
                    }
                    else if (question.Type == QuestionType.Generic)
                    {
                        key = string.Format("Generic{0}", question.Name);
                          if (!questions.ContainsKey(key))
                          {
                              questions.Add(key, question);
                          }
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                          if (!questions.ContainsKey(key))
                          {
                              questions.Add(key, question);
                          }
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, Question> ToDictionary(this PlanofCare planofCare)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (planofCare.Questions != null)
            {
                planofCare.Questions.ForEach(question =>
                {
                    string key = string.Empty;
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                    }
                    if (!questions.ContainsKey(key))
                    {
                        questions.Add(key, question);
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, Question> ToDictionary(this PlanofCareStandAlone planofCare)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (planofCare.Questions != null)
            {
                planofCare.Questions.ForEach(question =>
                {
                    string key = string.Empty;
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                    }
                    if (!questions.ContainsKey(key))
                    {
                        questions.Add(key, question);
                    }
                });
            }
            return questions;
        }

        public static PlanofCare ToPlanofCare(this PlanofCareStandAlone planofCareStandAlone)
        {
            var planofCare = new PlanofCare();

            planofCare.Id = planofCareStandAlone.Id;
            planofCare.AgencyId = planofCareStandAlone.AgencyId;
            planofCare.EpisodeId = planofCareStandAlone.EpisodeId;
            planofCare.PatientId = planofCareStandAlone.PatientId;
            planofCare.PhysicianId = planofCareStandAlone.PhysicianId;
            planofCare.UserId = planofCareStandAlone.UserId;
            planofCare.OrderNumber = planofCareStandAlone.OrderNumber;
            planofCare.Data = planofCareStandAlone.Data;
            planofCare.Status = planofCareStandAlone.Status;
            planofCare.SignatureText = planofCareStandAlone.SignatureText;
            planofCare.SignatureDate = planofCareStandAlone.SignatureDate;
            planofCare.PhysicianSignatureText = planofCareStandAlone.PhysicianSignatureText;
            planofCare.PhysicianSignatureDate = planofCareStandAlone.PhysicianSignatureDate;
            planofCare.ReceivedDate = planofCareStandAlone.ReceivedDate;
            planofCare.SentDate = planofCareStandAlone.SentDate;
            planofCare.Created = planofCareStandAlone.Created;
            planofCare.Modified = planofCareStandAlone.Modified;
            planofCare.IsDeprecated = planofCareStandAlone.IsDeprecated;

            return planofCare;
        }

        public static IDictionary<string, Question> Diagnosis(this Assessment assessment)
        {
            var diagnosis = new Dictionary<string, Question>();
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

        public static string AnswerOrEmptyString(this IDictionary<string, Question> questions, string key)
        {
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
            {
                return questions[key].Answer;//.Unclean();
            }

            return string.Empty;
        }

        public static Guid AnswerOrEmptyGuid(this IDictionary<string, Question> questions, string key)
        {
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
            {
                return questions[key].Answer.ToGuid();
            }

            return Guid.Empty;
        }

        public static string AnswerOrDefault(this IDictionary<string, Question> questions, string key, string defaultValue)
        {
            var answer = questions.AnswerOrEmptyString(key);
            if (answer.IsNotNullOrEmpty())
            {
                return answer;
            }

            return defaultValue;
        }

        public static string[] AnswerArray(this IDictionary<string, Question> questions, string key, char separator) {
            var answer = questions.AnswerOrEmptyString(key);
            if (answer.IsNotNullOrEmpty()) return answer.Split(separator);
            return new string[] {};
        }

        public static string[] AnswerArray(this IDictionary<string, Question> questions, string key) {
            var answer = questions.AnswerOrEmptyString(key);
            if (answer.IsNotNullOrEmpty()) return answer.Split(',');
            return new string[] { };
        }

        public static string ToChecked(this bool Checked) {
            if (Checked) return "checked='checked'";
            else return string.Empty;
        }

        public static List<Question> GetHospitalizationData(this Assessment assessment)
        {
            var transferLog = new List<Question>();
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                var names = new string[] { "M2410TypeOfInpatientFacility", "M2430ReasonForHospitalizationMed", "M2430ReasonForHospitalizationFall"
                    , "M2430ReasonForHospitalizationInfection", "M2430ReasonForHospitalizationOtherRP", "M2430ReasonForHospitalizationHeartFail"
                    , "M2430ReasonForHospitalizationCardiac", "M2430ReasonForHospitalizationMyocardial", "M2430ReasonForHospitalizationHeartDisease"
                    , "M2430ReasonForHospitalizationStroke", "M2430ReasonForHospitalizationHypo", "M2430ReasonForHospitalizationGI"
                    , "M2430ReasonForHospitalizationDehMal", "M2430ReasonForHospitalizationUrinaryInf", "M2430ReasonForHospitalizationIV"
                    , "M2430ReasonForHospitalizationWoundInf", "M2430ReasonForHospitalizationUncontrolledPain", "M2430ReasonForHospitalizationMental"
                    , "M2430ReasonForHospitalizationDVT", "M2430ReasonForHospitalizationScheduled", "M2430ReasonForHospitalizationOther"
                    , "M2440ReasonForHospitalizationUK", "M2440ReasonPatientAdmittedTherapy", "M2440ReasonPatientAdmittedRespite"
                    , "M2440ReasonPatientAdmittedHospice", "M2440ReasonPatientAdmittedPermanent", "M2440ReasonPatientAdmittedUnsafe"
                    , "M2440ReasonPatientAdmittedOther", "M2440ReasonPatientAdmittedUnknown", "GenericDischargeNarrative", "M2440ReasonPatientAdmittedTherapy", "M2440ReasonPatientAdmittedRespite"
                    , "M2440ReasonPatientAdmittedHospice", "M2440ReasonPatientAdmittedPermanent", "M2440ReasonPatientAdmittedUnsafe"
                    , "M2440ReasonPatientAdmittedOther", "M2440ReasonPatientAdmittedUnknown"};

                names.ForEach(name =>
                {
                    if (questions.ContainsKey(name) && questions[name] != null)
                    {
                        transferLog.Add(questions[name]);
                    }
                });
            }
            return transferLog;
        }
    }
}
