﻿namespace Axxess.OasisC.Enums
{
    using System.ComponentModel;

    public enum AssessmentType
    {
        [Description("OASIS-C Start Of Care")]
        StartOfCare,
        [Description("OASIS-C Resumption Of Care")]
        ResumptionOfCare,
        [Description("OASIS-C Recertification")]
        Recertification,
        [Description("OASIS-C Other Follow-up")]
        FollowUp,
        [Description("OASIS-C Transfer to inpatient facility - Not Discharged")]
        TransferInPatientNotDischarged,
        [Description("OASIS-C Transfer to inpatient facility - Discharged")]
        TransferInPatientDischarged,
        [Description("OASIS-C Death at Home")]
        DischargeFromAgencyDeath,
        [Description("OASIS-C Discharge From Agency")]
        DischargeFromAgency,
        [Description("OASIS C 485 (Plan of Care)")]
        PlanOfCare485,
        [Description("Non-OASIS Start Of Care")]
        NonOasisStartOfCare,
        [Description("Non-OASIS Recertification")]
        NonOasisRecertification,
        [Description("Non-OASIS Discharge")]
        NonOasisDischarge
    }
}
