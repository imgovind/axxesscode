﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.IO;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.iTextExtension;
    

    public static class FileGenerator
    {

        public static FileResult Pdf<T>(T inputDocument, string fileName) where T : AxxessPdf
        {
            var file = new FileStreamResult(new MemoryStream(), "application/pdf");
            try
            {
                if (inputDocument != null)
                {
                    var stream = inputDocument.GetStream();
                    if (stream != null)
                    {
                        stream.Position = 0;
                        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}_{1}.pdf", fileName, DateTime.Now.Ticks.ToString()));
                        file = new FileStreamResult(stream, "application/pdf");
                    }
                }
            }
            catch (Exception ex)
            {
                return file;
            }
            return file;
        }

        public static FileResult PlainText(string inputDocument, string fileName)
        {
            var fileStream = new MemoryStream();
            var file = new FileStreamResult(fileStream, "Text/Plain");
            try
            {
                if (inputDocument.IsNotNullOrEmpty() && inputDocument.Length > 0)
                {
                    var encoding = new UTF8Encoding();
                    byte[] buffer = encoding.GetBytes(inputDocument);
                    fileStream.Write(buffer, 0, inputDocument.Length);
                    fileStream.Position = 0;
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}{1}.txt", fileName, DateTime.Now.ToString("MMddyyyy")));
                    file = new FileStreamResult(fileStream, "Text/Plain");
                }
            }
            catch (Exception ex)
            {
                return file;
            }
            return file;
        }

    }
}
