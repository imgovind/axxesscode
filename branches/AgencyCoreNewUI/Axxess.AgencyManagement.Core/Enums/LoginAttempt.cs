﻿namespace Axxess.AgencyManagement.App.Enums
{
    public enum LoginAttempt
    {
        Failed,
        Success,
        Locked,
        Deactivated,
        TrialPeriodOver,
        AccountSuspended
    }
}
