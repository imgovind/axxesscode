﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    internal static class Reports
    {
        private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();

        #region Static Helper Methods

        internal static AgencyData GetAgency(Guid agencyId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator`  FROM `agencies` WHERE `Id` = @agencyid;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static AgencyData GetAgencyLocation(Guid agencyId, Guid agencyLocationId)
        {
            var agency = new AgencyData();
            var script = @"SELECT `Id`, `AgencyId`, `Name`,`NationalProviderNumber`,`MedicareProviderNumber`, `MedicaidProviderNumber`, `CahpsVendor`, `CahpsVendorId`, `CahpsSurveyDesignator` , `IsLocationStandAlone`, `AddressZipCode` FROM `agencylocations` WHERE `Id` = @agencylocationid  AND `AgencyId` = @agencyid ;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("AgencyId"),
                    LocationId = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    AddressZipCode = reader.GetString("AddressZipCode")
                })
                .AsSingle();
            }

            return agency;
        }

        internal static List<Patient> GetEligiblePatients(Guid agencyId, Guid agencyLocationId, DateTime sampleMonthLast)
        {
            var list = new List<Patient>();
            var script =
                @"SELECT 
                Id as Id,
                FirstName as FirstName,
                LastName as LastName, 
                MiddleInitial as MiddleInitial,
                MedicareNumber as MedicareNumber,
                MedicaidNumber as MedicaidNumber,
                Status as Status,
                PrimaryInsurance as PrimaryInsurance ,
                AddressLine1 as AddressLine1, 
                AddressLine2 as AddressLine2, 
                AddressCity as AddressCity,
                PhoneHome as PhoneHome, 
                AddressStateCode as AddressStateCode,
                AddressZipCode as AddressZipCode,
                DischargeDate as DischargeDate,
                PaymentSource as PaymentSource
                    FROM 
                         patients 
                                WHERE 
                                    AgencyId = @agencyid AND
                                    AgencyLocationId = @agencylocationid  AND
                                    IsDeprecated = 0 AND
                                    Status IN (1,2) AND
                                    PrimaryInsurance in (1,2,3,4) AND
                                    (DATE(Created) <= DATE(@month) || DATE(StartofCareDate) <= DATE(@month))";

            using (var cmd = new FluentCommand<Patient>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                 .AddDateTime("month", sampleMonthLast)
                .SetMap(reader => new Patient
                {
                    AgencyId = agencyId,
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    Status = reader.GetInt("Status"),
                    PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PaymentSource = reader.GetStringNullable("PaymentSource")
                })
                .AsList();
            }
            return list.ToList();

        }

        internal static List<Patient> GetEligiblePatientsByPaymentSource(List<Patient> allPatients, List<int> paymentSources)
        {
            var results = new List<Patient>();
            if (allPatients != null && allPatients.Count > 0)
            {
                allPatients.ForEach(i =>
                {
                    if (i.PaymentSource.IsNotNullOrEmpty())
                    {
                        paymentSources.ForEach(p =>
                        {
                            if (i.PaymentSource.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Contains(p.ToString()))
                            {
                                results.Add(i);
                                return;
                            }
                        });
                    }
                    else
                    {
                        //Windows.EventLog.WriteEntry(string.Format("Patient {0} has no Payment Sources: Id: {1}", i., i.Id), System.Diagnostics.EventLogEntryType.Information);
                    }
                });
            }
            return results;
        }

        internal static IDictionary<string, Question> GetEpisodeAssessment(List<ScheduleEvent> schedules)
        {
            IDictionary<string, Question> dictionaryResult = new Dictionary<string, Question>();
            var episodes = schedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.EventDate).First()).OrderByDescending(e=>e.StartDate).ToList();
            if (episodes != null && episodes.Count > 0)
            {
                var episode = episodes.FirstOrDefault();
                if (episode != null )
                {
                    var sampleMonthEvent = schedules.Where(s => s.EpisodeId == episode.EpisodeId && (s.IsStartofCareAssessment() ||( s.IsResumptionofCareAssessment() && s.EventDate<s.EndDate.AddDays(-5)))).OrderByDescending(e => e.EventDate).FirstOrDefault();
                    if (sampleMonthEvent != null)
                    {
                        dictionaryResult = sampleMonthEvent.ToOASISDictionary();
                    }
                    else
                    {
                        if (episodes.Count > 1)
                        {
                            var previousEpisode = episodes[1];
                            if (previousEpisode != null)
                            {
                                var samplePreviousEvent = schedules.Where(s => s.EpisodeId == previousEpisode.EpisodeId && (s.IsResumptionofCareAssessment() || s.IsRecertificationAssessment()) && s.EventDate.Date <= previousEpisode.EndDate.Date && s.EventDate >= previousEpisode.EndDate.AddDays(-5).Date).OrderByDescending(e => e.EventDate.Date).FirstOrDefault();
                                 if(sampleMonthEvent!=null)
                                 {
                                     dictionaryResult = samplePreviousEvent.ToOASISDictionary();
                                 }
                            }
                        }
                    }
                }

            }
            return dictionaryResult;
        }

        internal static IDictionary<string, Question> GetEpisodeAssessment(List<ScheduleEvent> currentSchedules, List<ScheduleEvent> previousSchedules , out string hippsCode)
        {
            IDictionary<string, Question> dictionaryResult = new Dictionary<string, Question>();
            hippsCode = string.Empty;
            if (currentSchedules != null && currentSchedules.Count > 0)
            {
                var sampleMonthEvent = currentSchedules.Where(s => (s.IsStartofCareAssessment())).OrderByDescending(e => e.EventDate).FirstOrDefault();
                if (sampleMonthEvent != null)
                {
                    hippsCode = sampleMonthEvent.HippsCode;
                    dictionaryResult = sampleMonthEvent.ToOASISDictionary();
                }
                else
                {
                    var samplePreviousEvent = previousSchedules.Where(s => (s.IsResumptionofCareAssessment() || s.IsRecertificationAssessment())).OrderByDescending(e => e.EventDate.Date).FirstOrDefault();
                    if (sampleMonthEvent != null)
                    {
                        hippsCode = samplePreviousEvent.HippsCode;
                        dictionaryResult = samplePreviousEvent.ToOASISDictionary();
                    }
                }
            }
            return dictionaryResult;
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetween(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var script =
                @"SELECT
                agencymanagement.patientepisodes.Id as EpisodeId,
                agencymanagement.patients.Id as PatientId, 
                agencymanagement.patients.DOB as DOB,
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.Status as PatientStatus,
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.DischargeDate as DischargeDate,
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patients.MedicareNumber as MedicareNumber,
                agencymanagement.patients.MedicaidNumber as MedicaidNumber,
                agencymanagement.patients.AddressLine1 as AddressLine1, 
                agencymanagement.patients.AddressLine2 as AddressLine2,
                agencymanagement.patients.AddressCity as AddressCity,
                agencymanagement.patients.PhoneHome as PhoneHome, 
                agencymanagement.patients.AddressStateCode as AddressStateCode, 
                agencymanagement.patients.AddressZipCode as AddressZipCode, 
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber, 
                agencymanagement.patients.StartofCareDate as StartofCareDate,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable ,
                agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate ,
                oasisc.assessments.OasisData as Note , 
                oasisc.assessments.HippsCode as HippsCode
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                            LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                WHERE
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate) AND
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0
                                         ORDER BY agencymanagement.patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    DOB = reader.GetDateTime("DOB"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    PatientStatus = reader.GetInt("PatientStatus"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Note = reader.GetStringNullable("Note"),
                    HippsCode = reader.GetStringNullable("HippsCode")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetEpisodeScheduleEventsExactlyBetweenWithOutOasis(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ScheduleEvent>();
            var script =
                @"SELECT
                agencymanagement.patientepisodes.Id as EpisodeId,
                agencymanagement.patients.Id as PatientId, 
                agencymanagement.patients.DOB as DOB,
                agencymanagement.patients.FirstName as FirstName,
                agencymanagement.patients.LastName as LastName, 
                agencymanagement.patients.Status as PatientStatus,
                agencymanagement.patients.MiddleInitial as MiddleInitial,
                agencymanagement.patients.DischargeDate as DischargeDate,
                agencymanagement.patientepisodes.AdmissionId as AdmissionId,
                agencymanagement.patients.MedicareNumber as MedicareNumber,
                agencymanagement.patients.MedicaidNumber as MedicaidNumber,
                agencymanagement.patients.AddressLine1 as AddressLine1, 
                agencymanagement.patients.AddressLine2 as AddressLine2,
                agencymanagement.patients.AddressCity as AddressCity,
                agencymanagement.patients.PhoneHome as PhoneHome, 
                agencymanagement.patients.AddressStateCode as AddressStateCode, 
                agencymanagement.patients.AddressZipCode as AddressZipCode, 
                agencymanagement.patientepisodes.EndDate as EndDate, 
                agencymanagement.patientepisodes.StartDate as StartDate,
                agencymanagement.patients.PatientIdNumber as PatientIdNumber, 
                agencymanagement.patients.StartofCareDate as StartofCareDate,
                agencymanagement.scheduleevents.EventId as EventId ,
                agencymanagement.scheduleevents.PatientId as PatientId ,
                agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                agencymanagement.scheduleevents.EventDate as EventDate ,
                agencymanagement.scheduleevents.VisitDate as VisitDate ,
                agencymanagement.scheduleevents.Status as Status ,
                agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                agencymanagement.scheduleevents.Discipline as Discipline ,
                agencymanagement.scheduleevents.IsBillable as IsBillable ,
                agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                agencymanagement.patientepisodes.StartDate as StartDate ,
                agencymanagement.patientepisodes.EndDate as EndDate ,
                agencymanagement.patientepisodes.Details as EpisodeNotes
                    FROM
                        agencymanagement.scheduleevents
                            INNER JOIN agencymanagement.patientepisodes ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id
                            INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id 
                                WHERE
                                    agencymanagement.patients.Status IN (1,2) AND
                                    agencymanagement.patientepisodes.AgencyId = @agencyid AND
                                    agencymanagement.patients.IsDeprecated = 0 AND
                                    agencymanagement.patients.AgencyLocationId = @agencylocationid  AND 
                                    agencymanagement.patientepisodes.IsActive = 1 AND
                                    agencymanagement.patientepisodes.IsDischarged = 0 AND
                                    DATE(agencymanagement.scheduleevents.EventDate) BETWEEN DATE(@startdate) AND DATE(@enddate) AND
                                    DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                    agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                    agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                    agencymanagement.scheduleevents.DisciplineTask > 0
                                         ORDER BY agencymanagement.patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("agencylocationid", agencyLocationId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new ScheduleEvent
                {
                    AgencyId = agencyId,
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    DOB = reader.GetDateTime("DOB"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    MedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                    DischargeDate = reader.GetDateTime("DischargeDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    PatientStatus = reader.GetInt("PatientStatus"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes")
                })
                .AsList();
            }
            return list.ToList();
        }

        internal static List<ScheduleEvent> GetPreviousEpisodeOASISAssesments(Guid agencyId, List<Guid> ids)
        {
            var oasisDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments().ToArray();// new int[] { 8, 66, 73, 90, 9, 62, 69 };
            var script = string.Format(@"SELECT 
                        agencymanagement.scheduleevents.EventId as EventId ,
                        agencymanagement.scheduleevents.PatientId as PatientId ,
                        agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                        agencymanagement.scheduleevents.EventDate as EventDate ,
                        agencymanagement.scheduleevents.VisitDate as VisitDate ,
                        agencymanagement.scheduleevents.Status as Status ,
                        agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                        agencymanagement.scheduleevents.Discipline as Discipline ,
                        agencymanagement.scheduleevents.IsBillable as IsBillable ,
                        agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                        agencymanagement.scheduleevents.TimeIn as TimeIn , 
                        agencymanagement.scheduleevents.TimeOut as TimeOut , 
                        secondaryepisode.StartDate as StartDate ,
                        secondaryepisode.EndDate as EndDate ,
                        primaryepisode.Id as NextEpisodeId ,
                        oasisc.assessments.OasisData as Note , 
                        oasisc.assessments.HippsCode as HippsCode
                            FROM
                                scheduleevents 
                                    INNER JOIN agencymanagement.patientepisodes as primaryepisode ON agencymanagement.scheduleevents.PatientId = primaryepisode.PatientId 
                                    INNER JOIN agencymanagement.patientepisodes as secondaryepisode ON agencymanagement.scheduleevents.PatientId = secondaryepisode.PatientId 
                                    LEFT JOIN oasisc.assessments ON  oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                        WHERE 
                                            agencymanagement.scheduleevents.AgencyId = @agencyid AND 
                                            primaryepisode.Id IN ( {0} )  AND 
                                            DATE(secondaryepisode.EndDate) = DATE(DATE_SUB(primaryepisode.StartDate, INTERVAL 1 DAY)) AND 
                                            secondaryepisode.IsActive = 1  AND 
                                            secondaryepisode.IsDischarged = 0  AND
                                            DATE(agencymanagement.scheduleevents.EventDate) between DATE(DATE_SUB(secondaryepisode.EndDate, INTERVAL 5 DAY)) and DATE(secondaryepisode.EndDate) AND 
                                            agencymanagement.scheduleevents.IsMissedVisit = 0  AND 
                                            agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                            agencymanagement.scheduleevents.DisciplineTask > 0  AND
                                            agencymanagement.scheduleevents.DisciplineTask IN ( {1} ) ", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), oasisDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "));


            var list = new List<ScheduleEvent>();

            return new FluentCommand<ScheduleEvent>(script)
              .SetConnection("AgencyManagementConnectionString")
              .AddGuid("agencyid", agencyId)
              .SetMap(reader => new ScheduleEvent
              {
                  EventId = reader.GetGuid("EventId"),
                  PatientId = reader.GetGuid("PatientId"),
                  EpisodeId = reader.GetGuid("EpisodeId"),
                  DisciplineTask = reader.GetInt("DisciplineTask"),
                  EventDate = reader.GetDateTime("EventDate"),
                  VisitDate = reader.GetDateTime("VisitDate"),
                  Status = reader.GetStringNullable("Status"),
                  Discipline = reader.GetStringNullable("Discipline"),
                  IsBillable = reader.GetBoolean("IsBillable"),
                  IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                  TimeIn = reader.GetStringNullable("TimeIn"),
                  TimeOut = reader.GetStringNullable("TimeOut"),
                  EndDate = reader.GetDateTime("EndDate"),
                  StartDate = reader.GetDateTime("StartDate"),
                  Note = reader.GetStringNullable("Note"),
                  HippsCode = reader.GetStringNullable("HippsCode"),
                  NewEpisodeId = reader.GetGuid("NextEpisodeId")
              })
              .AsList();
        }

        internal static CbsaCode GetCbsaCode(string zipCode)
        {
            var cbsaCode = new CbsaCode();
            var script = string.Format("SELECT * FROM `cbsacodes` WHERE `zip` = '{0}' limit 0, 1;", zipCode);

            using (var cmd = new FluentCommand<CbsaCode>(script))
            {
                cbsaCode = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new CbsaCode
                    {
                        CBSA = reader.GetStringNullable("CBSA"),
                        WITwoTen = reader.GetDouble("WITwoTen"),
                        WITwoEleven = reader.GetDouble("WITwoEleven"),
                        WITwoTwelve = reader.GetDouble("WITwoTwelve")
                    }).AsSingle();
            }

            return cbsaCode;
        }

        internal static HippsAndHhrg GetHhrgByHippsCodeAndYear(string hippsCode, int year)
        {
            var result = new HippsAndHhrg();
            var list = new List<HippsAndHhrg>();
            var script = string.Format("SELECT * FROM `hippsandhhrgs` WHERE `HIPPS` = '{0}';", hippsCode);

            using (var cmd = new FluentCommand<HippsAndHhrg>(script))
            {
                list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new HippsAndHhrg
                    {
                        HHRG = reader.GetStringNullable("HHRG"),
                        HIPPS = reader.GetStringNullable("HIPPS"),
                        HHRGWeight = reader.GetDouble("HHRGWeight"),
                        Time = reader.GetDateTime("Time")
                    }).AsList();
            }

            if (list != null && list.Count > 0)
            {
                result = list.Find(h => h.Time.Year == year);
            }
            return result;
        }

        internal static PPSStandard GetPPSStandardByYear(int year)
        {
            var result = new PPSStandard();
            var list = new List<PPSStandard>();

            var script = @"SELECT * FROM `ppsstandards`;";
            using (var cmd = new FluentCommand<PPSStandard>(script))
            {
                list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new PPSStandard
                    {
                        Time = reader.GetDateTime("Time"),
                        UrbanRate = reader.GetDouble("UrbanRate"),
                        RuralRate = reader.GetDouble("RuralRate"),
                        Labor = reader.GetDouble("Labor"),
                        NonLabor = reader.GetDouble("NonLabor"),
                        S = reader.GetDouble("S"),
                        RuralS = reader.GetDouble("RuralS"),
                        T = reader.GetDouble("T"),
                        RuralT = reader.GetDouble("RuralT"),
                        U = reader.GetDouble("U"),
                        RuralU = reader.GetDouble("RuralU"),
                        V = reader.GetDouble("V"),
                        RuralV = reader.GetDouble("RuralV"),
                        W = reader.GetDouble("W"),
                        RuralW = reader.GetDouble("RuralW"),
                        X = reader.GetDouble("X"),
                        RuralX = reader.GetDouble("RuralX")
                    }).AsList();
            }

            if (list != null && list.Count > 0)
            {
                result = list.Find(h => h.Time.Year == year);
            }

            return result;
        }

        internal static List<CbsaCode> CbsaCodesByZip(string[] zipCodes)
        {
            var list = new List<CbsaCode>();
            if (zipCodes != null && zipCodes.Length > 0)
            {
                var script = string.Format(@"SELECT 
                                        Id as Id, 
                                        Zip as Zip, 
                                        CBSA as CBSA, 
                                        WITwoSeven as WITwoSeven,
                                        WITwoEight as WITwoEight ,
                                        WITwoNine as WITwoNine , 
                                        WITwoTen as WITwoTen,
                                        WITwoEleven as WITwoEleven,
                                        WITwoTwelve as WITwoTwelve  
                                          FROM 
                                               cbsacodes 
                                                        WHERE Zip IN ( {0} ) ", zipCodes.Select(z => "\'" + z + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<CbsaCode>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new CbsaCode
                    {
                        Id = reader.GetInt("Id"),
                        Zip = reader.GetStringNullable("Zip"),
                        CBSA = reader.GetStringNullable("CBSA"),
                        WITwoSeven = reader.GetDouble("WITwoSeven"),
                        WITwoEight = reader.GetDouble("WITwoEight"),
                        WITwoNine = reader.GetDouble("WITwoNine"),
                        WITwoTen = reader.GetDouble("WITwoTen"),
                        WITwoEleven = reader.GetDouble("WITwoEleven"),
                        WITwoTwelve = reader.GetDouble("WITwoTwelve")
                    })
                    .AsList();
                }
            }
            return list;
        }

        internal static List<HippsAndHhrg> GetHhrgByHippsCodeAndYear(string[] hippsAndYears)
        {
            var list = new List<HippsAndHhrg>();
            if (hippsAndYears != null && hippsAndYears.Length > 0)
            {
                var script = string.Format(@"SELECT 
                                        Id as Id, 
                                        HHRG as HHRG, 
                                        HIPPS as HIPPS, 
                                        HHRGWeight as HHRGWeight,
                                        Time as Time 
                                          FROM 
                                               hippsandhhrgs 
                                                        WHERE concat(HIPPS,year(Time)) IN ( {0} ) ", hippsAndYears.Select(z => "\'" + z + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<HippsAndHhrg>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new HippsAndHhrg
                    {
                        Id = reader.GetInt("Id"),
                        HHRG = reader.GetStringNullable("HHRG"),
                        HIPPS = reader.GetStringNullable("HIPPS"),
                        HHRGWeight = reader.GetDouble("HHRGWeight"),
                        Time = reader.GetDateTime("Time")
                    })
                    .AsList();
                }
            }
            return list;
        }

        internal static List<PPSStandard> PPSStandards(int[] years)
        {
            var list = new List<PPSStandard>();
            if (years != null && years.Length > 0)
            {
                var script = string.Format(@"SELECT 
                                        Id as Id, 
                                        Time as Time, 
                                        UrbanRate as UrbanRate, 
                                        RuralRate as RuralRate,
                                        Labor as Labor,
                                        NonLabor as NonLabor , 
                                        S as S,
                                        RuralS as RuralS,
                                        T as T, 
                                        RuralT as RuralT,
                                        U as U,
                                        RuralU as RuralU , 
                                        V as V,
                                        RuralV as RuralV,
                                        W as W,
                                        RuralW as RuralW , 
                                        X as X,
                                        RuralX as RuralX  
                                          FROM 
                                               ppsstandards 
                                                        WHERE year(Time) IN ( {0} ) ", years.Select(z => z.ToString()).ToArray().Join(","));

                using (var cmd = new FluentCommand<PPSStandard>(script))
                {
                    list = cmd.SetConnection("AxxessLookupConnectionString")
                    .SetMap(reader => new PPSStandard
                    {
                        Id = reader.GetInt("Id"),
                        Time = reader.GetDateTime("Time"),
                        UrbanRate = reader.GetDouble("UrbanRate"),
                        RuralRate = reader.GetDouble("RuralRate"),
                        Labor = reader.GetDouble("Labor"),
                        NonLabor = reader.GetDouble("NonLabor"),
                        S = reader.GetDouble("S"),
                        RuralS = reader.GetDouble("RuralS"),
                        T = reader.GetDouble("T"),
                        RuralT = reader.GetDouble("RuralT"),
                        U = reader.GetDouble("U"),
                        RuralU = reader.GetDouble("RuralU"),
                        V = reader.GetDouble("V"),
                        RuralV = reader.GetDouble("RuralV"),
                        W = reader.GetDouble("W"),
                        RuralW = reader.GetDouble("RuralW"),
                        X = reader.GetDouble("X"),
                        RuralX = reader.GetDouble("RuralX")
                    })
                    .AsList();
                }
            }
            return list;
        }

        internal static List<Claim> GetRaps(Guid agencyId, List<Guid> rapIds)
        {
            var raps = new List<Claim>();
            var script = string.Format("SELECT * FROM `raps` WHERE `agencyid` = '{0}' AND `Id` IN ( {1}) ;", agencyId, rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<Claim>(script))
            {
                raps = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }

            return raps;
        }

        internal static List<Claim> GetFinals(Guid agencyId, List<Guid> finalIds)
        {
            var finals = new List<Claim>();
            var script = string.Format("SELECT * FROM `finals` WHERE `agencyid` = '{0}' AND `Id` IN ( {1} ) ;", agencyId, finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            using (var cmd = new FluentCommand<Claim>(script))
            {
                finals = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Claim
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        ClaimDate = reader.GetDateTime("ClaimDate"),
                        ClaimAmount = reader.GetDouble("ProspectivePay"),
                        PaymentAmount = reader.GetDouble("Payment"),
                        PaymentDate = reader.GetDateTime("PaymentDate")
                    })
                    .AsList();
            }

            return finals;
        }

        internal static List<AdmissionPeriod> GetPatientAdmissions(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var list = new List<AdmissionPeriod>();
            var script = @"select * from patientadmissiondates WHERE `AgencyId` = @agencyid and `StartOfCareDate` between @start and @end";

            using (var cmd = new FluentCommand<AdmissionPeriod>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("start", startDate)
                .AddDateTime("end", endDate)
                .AsList();
            }

            return list;
        }

        internal static List<ScheduleEvent> GetOASISSchedulesWithData(Guid agencyId, List<Guid> patientIds, DateTime startDate, DateTime endDate , bool isPastScheuldesIncluded)
        {
            var oasisDisciplineTasks = DisciplineTaskFactory.EpisodeAllAssessments().ToArray();// new int[] { 13, 61, 89, 112, 8, 66, 73, 90, 9, 62, 69 };
            var oasisScript = string.Empty;
            if (!isPastScheuldesIncluded)
            {
                oasisScript = string.Format(" AND agencymanagement.scheduleevents.DisciplineTask IN ( {0} )", oasisDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "));
            }
            var script = string.Format(@"SELECT 
                                        agencymanagement.scheduleevents.EventId as EventId ,
                                        agencymanagement.scheduleevents.PatientId as PatientId ,
                                        agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                                        agencymanagement.scheduleevents.EventDate as EventDate ,
                                        agencymanagement.scheduleevents.VisitDate as VisitDate ,
                                        agencymanagement.scheduleevents.Status as Status ,
                                        agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                                        agencymanagement.scheduleevents.Discipline as Discipline ,
                                        agencymanagement.scheduleevents.IsBillable as IsBillable ,
                                        agencymanagement.scheduleevents.IsMissedVisit as IsMissedVisit , 
                                        agencymanagement.patientepisodes.StartDate as StartDate ,
                                        agencymanagement.patientepisodes.EndDate as EndDate ,
                                        oasisc.assessments.OasisData as Note 
                                            FROM agencymanagement.scheduleevents  
                                                INNER JOIN  agencymanagement.patientepisodes ON agencymanagement.patientepisodes.Id = agencymanagement.scheduleevents.EpisodeId 
                                                LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                                    WHERE 
                                                        agencymanagement.patientepisodes.AgencyId = @agencyid AND 
                                                        agencymanagement.patientepisodes.PatientId IN ( {0} ) AND 
                                                        agencymanagement.patientepisodes.IsActive = 1  AND 
                                                        agencymanagement.patientepisodes.IsDischarged = 0  AND 
                                                        DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                                        ((DATE(agencymanagement.scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)) OR ( (DATE(agencymanagement.scheduleevents.EventDate) between DATE(DATE_SUB(@startdate, INTERVAL 65 DAY)) and DATE(@startdate)) {1})) AND
                                                        agencymanagement.scheduleevents.IsMissedVisit = 0  AND 
                                                        agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                        agencymanagement.scheduleevents.DisciplineTask > 0", patientIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), oasisScript);

            var list = new List<ScheduleEvent>();

            return new FluentCommand<ScheduleEvent>(script)
              .SetConnection("AgencyManagementConnectionString")
              .AddGuid("agencyid", agencyId)
              .AddDateTime("startdate", startDate)
              .AddDateTime("enddate", endDate)
              .SetMap(reader => new ScheduleEvent
              {
                  EventId = reader.GetGuid("EventId"),
                  PatientId = reader.GetGuid("PatientId"),
                  EpisodeId = reader.GetGuid("EpisodeId"),
                  EventDate = reader.GetDateTime("EventDate"),
                  VisitDate = reader.GetDateTime("VisitDate"),
                  Status = reader.GetStringNullable("Status"),
                  DisciplineTask = reader.GetInt("DisciplineTask"),
                  Discipline = reader.GetStringNullable("Discipline"),
                  IsBillable = reader.GetBoolean("IsBillable"),
                  IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                  StartDate = reader.GetDateTime("StartDate"),
                  EndDate = reader.GetDateTime("EndDate"),
                  Note = reader.GetStringNullable("Note")
              })
              .AsList();
        }

        #endregion

        #region Helper Functions

        internal static DateTime GetStartOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month, 1, 0, 0, 0, 0);
        }

        internal static DateTime GetEndOfMonth(int Month, int Year)
        {
            return new DateTime(Year, (int)Month,
               DateTime.DaysInMonth(Year, (int)Month), 23, 59, 59, 999);
        }

        internal static DateTime GetStartOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetStartOfMonth(12, Year - 1);
            else
                return GetStartOfMonth(Month - 1, Year);
        }

        internal static DateTime GetEndOfLastMonth(int Month, int Year)
        {
            if (Month == 1)
                return GetEndOfMonth(12, Year - 1);
            else
                return GetEndOfMonth(Month - 1, Year);
        }

        #endregion
    }
}
