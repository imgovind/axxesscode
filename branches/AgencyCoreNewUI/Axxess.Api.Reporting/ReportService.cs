﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Linq;
    using System.ServiceModel;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    public class ReportService : BaseService, IReportService
    {
        #region IReportService Members

        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear)
        {
            return CahpsExportByPaymentSources(agencyId, agencyLocationId, sampleMonth, sampleYear, new List<int>());
        }

        public List<Dictionary<string, string>> CahpsExportByPaymentSources(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var collection = new List<Dictionary<string, string>>();
            IDataSpecification dataSpecification = null;

            try
            {
                var agency = Reports.GetAgencyLocation(agencyId, agencyLocationId) ?? new AgencyData();
                if (!agency.IsLocationStandAlone)
                {
                    agency = Reports.GetAgency(agencyId) ?? new AgencyData();
                }
                agency.LocationId = agencyLocationId;

                if (agency != null)
                {
                    switch (agency.CahpsVendor)
                    {
                        case 1: // DSS Research
                            dataSpecification = new DssResearchDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)2: // Synovate Healthcare
                            dataSpecification = new SynovateDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case 3: // Novaetus
                            dataSpecification = new NovaetusDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)4: // Deyta
                        case (int)6: // Fields Research
                            dataSpecification = new DeytaDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)5: // Pinnacle
                            dataSpecification = new PinnacleDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)7: // PressGaney
                            dataSpecification = new PressGaneyDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)8: // Ocs
                            dataSpecification = new OcsDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)9: // Fazzi
                        case (int)11: // Beacon Touch
                            dataSpecification = new FazziDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)10: // Strategic Healthcare
                            dataSpecification = new ShpDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return collection;
        }

        public List<Dictionary<string, string>> PPSEpisodeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.StartDate).First()).ToList();
                        if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                        {
                            var zipCodes = scheduleFirstEpisodes.Where(s=>s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList()?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }
                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = scheduleFirstEpisodes.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears)??new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            var previousEpisodeSchedules = Reports.GetPreviousEpisodeOASISAssesments(agencyId, scheduleFirstEpisodes.Select(s => s.EpisodeId).Distinct().ToList()) ?? new List<ScheduleEvent>();
                            hippsWithYear.AddRange(previousEpisodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.EndDate.AddDays(1).Year).Distinct().ToList());
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();

                            scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                            {
                                var schedules = episodeSchedules.Where(es => es.EpisodeId == scheduleEpisode.EpisodeId).OrderByDescending(s => s.EventDate.Date).ToList() ?? new List<ScheduleEvent>();
                                var previousSchedules = previousEpisodeSchedules.Where(s => s.NewEpisodeId == scheduleEpisode.EpisodeId && s.EndDate.Date == scheduleEpisode.StartDate.AddDays(-1).Date && (s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment()) && s.EventDate.Date <= s.EndDate && s.EventDate.Date >= s.EndDate.AddDays(-5).Date).OrderByDescending(s => s.EventDate).ToList() ?? new List<ScheduleEvent>();
                                var hippsCode = string.Empty;
                                var assessmentData = Reports.GetEpisodeAssessment(schedules, previousSchedules, out hippsCode);
                                scheduleEpisode.HippsCode = hippsCode;
                                var prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == scheduleEpisode.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS == scheduleEpisode.HippsCode && h.Time.Year == scheduleEpisode.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? scheduleEpisode.AddressZipCode :agencyData.AddressZipCode )));//GetProspectivePayment(scheduleEpisode.HippsCode, scheduleEpisode.StartDate, scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? scheduleEpisode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                                var collection = new Dictionary<string, string>();
                                collection.Add("Patient ID", scheduleEpisode.PatientIdNumber);
                                collection.Add("Patient Name", string.Concat(scheduleEpisode.LastName, ", ", scheduleEpisode.FirstName, (!string.IsNullOrEmpty(scheduleEpisode.MiddleInitial) ? " " + scheduleEpisode.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", scheduleEpisode.MedicareNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicareNumber : scheduleEpisode.MedicaidNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicaidNumber : string.Empty);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Admission Date", scheduleEpisode.StartofCareDate.Date > DateTime.MinValue.Date ? scheduleEpisode.StartofCareDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Episode #", string.Empty);
                                collection.Add("Episode Type", GetEpisodeTiming(assessmentData));
                                collection.Add("NRS Severity Level", GetNrsSeverityLevel(scheduleEpisode.HippsCode));
                                collection.Add("Begin Date", scheduleEpisode.StartDate > DateTime.MinValue ? scheduleEpisode.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("End Date", scheduleEpisode.EndDate > DateTime.MinValue ? scheduleEpisode.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("First Billable", GetFirstBillableDate(schedules.Where(s=>s.IsBillable).ToList()));
                                collection.Add("Status", scheduleEpisode.PatientStatus == 1 ? "AD" : "DI");
                                collection.Add("Discharge Date", scheduleEpisode.DischargeDate > DateTime.MinValue ? scheduleEpisode.DischargeDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("Original HHRG", scheduleEpisode.HippsCode);
                                collection.Add("Original Therapy Visits", assessmentData != null && assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").IsInteger() ? assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").ToInteger().ToString() : string.Empty);
                                collection.Add("Original Case Mix", prospectivePayment != null ? prospectivePayment.Weight : string.Empty);
                                collection.Add("Original HIPPS", scheduleEpisode.HippsCode);
                                collection.Add("HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                                collection.Add("Case Mix", prospectivePayment != null ? prospectivePayment.Weight : string.Empty);
                                collection.Add("HIPPS", prospectivePayment != null ? prospectivePayment.HippsCode : string.Empty);
                                collection.Add("MSA/CBSA", prospectivePayment != null ? prospectivePayment.CbsaCode : string.Empty);
                                collection.Add("Wage Index", prospectivePayment != null ? prospectivePayment.WageIndex : string.Empty);
                                collection.Add("LUPA", "N");
                                collection.Add("PEP", "N");
                                collection.Add("SCIC", "N");
                                collection.Add("Outlier", "N");
                                collection.Add("Therapy Adj", string.Empty);

                                collectionList.Add(collection);
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return collectionList;
        }

        public List<Dictionary<string, string>> PPSVisitInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e=>e.StartDate).First()).ToList();
                        if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                        {
                            scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                            {
                                var schedules = episodeSchedules.Where(es => es.EpisodeId == scheduleEpisode.EpisodeId && es.IsBillable).OrderByDescending(s=>s.EventDate.Date).ToList()?? new List<ScheduleEvent>();
                                var collection = new Dictionary<string, string>();
                                collection.Add("Patient ID", scheduleEpisode.PatientIdNumber);
                                collection.Add("Patient Name", string.Concat(scheduleEpisode.LastName, ", ", scheduleEpisode.FirstName, (!string.IsNullOrEmpty(scheduleEpisode.MiddleInitial) ? " " + scheduleEpisode.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", scheduleEpisode.MedicareNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicareNumber : scheduleEpisode.MedicaidNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicaidNumber : string.Empty);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Begin Date", scheduleEpisode.StartDate.ToString("MM/dd/yyyy"));
                                collection.Add("End Date", scheduleEpisode.EndDate.ToString("MM/dd/yyyy"));
                                collection.Add("First Billable", GetFirstBillableDate(schedules));

                                var months = GetMonthsBetween(startDate, endDate);
                                foreach (var month in months)
                                {
                                    collection.Add(month.Value, GetVisitsInMonth(schedules, month.Key));
                                }

                                collection.Add("SN", GetVisitsByDiscipline(schedules, "Nursing"));
                                collection.Add("PT", GetVisitsByDiscipline(schedules, "PT"));
                                collection.Add("OT", GetVisitsByDiscipline(schedules, "OT"));
                                collection.Add("ST", GetVisitsByDiscipline(schedules, "ST"));
                                collection.Add("SW", GetVisitsByDiscipline(schedules, "MSW"));
                                collection.Add("HHA", GetVisitsByDiscipline(schedules, "HHA"));
                                collection.Add("Total Visits", schedules.Count.ToString());

                                collectionList.Add(collection);
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSChargeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.StartDate).First()).ToList();
                        if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                        {
                            var zipCodes = scheduleFirstEpisodes.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList() ?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }
                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = scheduleFirstEpisodes.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears) ?? new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            var previousEpisodeSchedules = Reports.GetPreviousEpisodeOASISAssesments(agencyId, scheduleFirstEpisodes.Select(s => s.EpisodeId).Distinct().ToList());
                            hippsWithYear.AddRange(previousEpisodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.EndDate.AddDays(1).Year).Distinct().ToList());
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();
                            scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                            {
                                var schedules = episodeSchedules.Where(es => es.EpisodeId == scheduleEpisode.EpisodeId ).OrderByDescending(s => s.EventDate.Date).ToList() ?? new List<ScheduleEvent>();
                                var previousSchedules = previousEpisodeSchedules.Where(s => s.NewEpisodeId == scheduleEpisode.EpisodeId && s.EndDate.Date == scheduleEpisode.StartDate.AddDays(-1).Date && (s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment()) && s.EventDate.Date <= s.EndDate && s.EventDate.Date >= s.EndDate.AddDays(-5).Date).OrderByDescending(s => s.EventDate).ToList() ?? new List<ScheduleEvent>();
                                var hippsCode = string.Empty;
                                var assessmentData = Reports.GetEpisodeAssessment(schedules, previousSchedules, out hippsCode);
                                scheduleEpisode.HippsCode = hippsCode;
                                var prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == scheduleEpisode.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS == scheduleEpisode.HippsCode && h.Time.Year == scheduleEpisode.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? scheduleEpisode.AddressZipCode : agencyData.AddressZipCode))); // var prospectivePayment = GetProspectivePayment(scheduleEpisode.HippsCode, scheduleEpisode.StartDate, scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? scheduleEpisode.AddressZipCode : scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                                var collection = new Dictionary<string, string>();
                                collection.Add("Patient ID", scheduleEpisode.PatientIdNumber);
                                collection.Add("Patient Name", string.Concat(scheduleEpisode.LastName, ", ", scheduleEpisode.FirstName, (!string.IsNullOrEmpty(scheduleEpisode.MiddleInitial) ? " " + scheduleEpisode.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", scheduleEpisode.MedicareNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicareNumber : scheduleEpisode.MedicaidNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicaidNumber : string.Empty);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Begin Date", scheduleEpisode.StartDate.Date > DateTime.MinValue.Date ? scheduleEpisode.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("End Date", scheduleEpisode.EndDate.Date > DateTime.MinValue.Date ? scheduleEpisode.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("First Billable", GetFirstBillableDate(schedules));

                                var months = GetMonthsBetween(startDate, endDate);
                                foreach (var month in months)
                                {
                                    collection.Add(string.Format("Total Charges for {0}", month.Value), GetChargesByMonth(schedules, prospectivePayment, month.Key));
                                }

                                collection.Add("Total Charges for SN", GetChargesByDiscipline(schedules, prospectivePayment, "Nursing"));
                                collection.Add("Total Charges for PT", GetChargesByDiscipline(schedules, prospectivePayment, "PT"));
                                collection.Add("Total Charges for OT", GetChargesByDiscipline(schedules, prospectivePayment, "OT"));
                                collection.Add("Total Charges for ST", GetChargesByDiscipline(schedules, prospectivePayment, "ST"));
                                collection.Add("Total Charges for SW", GetChargesByDiscipline(schedules, prospectivePayment, "MSW"));
                                collection.Add("Total Charges for HHA", GetChargesByDiscipline(schedules, prospectivePayment, "HHA"));
                                collection.Add("MS", "$0.00");
                                collection.Add("Total Charges", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                                foreach (var month in months)
                                {
                                    collection.Add(string.Format("Contractual Adjustments for {0}", month.Value), "$0.00");
                                }
                                collection.Add("Contractual Adjustment Total", "$0.00");

                                foreach (var month in months)
                                {
                                    collection.Add(string.Format("CA Reimbursement for {0}", month.Value), "N/A");
                                }
                                collection.Add("Orig Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");
                                collection.Add("Adjustment", "0");
                                collection.Add("Current Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                                collectionList.Add(collection);
                            });
                            Windows.EventLog.WriteEntry(string.Format("Collection Count: {0}", collectionList.Count), System.Diagnostics.EventLogEntryType.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSPaymentInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodeSchedules != null && episodeSchedules.Count > 0)
                    {
                        var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.StartDate).First()).ToList();
                        if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                        {
                            var ids = scheduleFirstEpisodes.Select(s => s.EpisodeId).Distinct().ToList();
                            var raps = Reports.GetRaps(agencyData.Id, ids);
                            var finals = Reports.GetRaps(agencyData.Id, ids);

                            var zipCodes = scheduleFirstEpisodes.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).ToList() ?? new List<string>();
                            if (zipCodes != null && agencyData.AddressZipCode.IsNotNullOrEmpty())
                            {
                                zipCodes.Add(agencyData.AddressZipCode);
                            }
                            var cbsaCodes = Reports.CbsaCodesByZip(zipCodes.ToArray()) ?? new List<CbsaCode>();
                            var uniqeYears = scheduleFirstEpisodes.Select(s => s.StartDate.Year).Distinct().ToArray();
                            var pps = Reports.PPSStandards(uniqeYears) ?? new List<PPSStandard>();
                            var hippsWithYear = episodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.StartDate.Year).Distinct().ToList();
                            var previousEpisodeSchedules = Reports.GetPreviousEpisodeOASISAssesments(agencyId, ids);
                            hippsWithYear.AddRange(previousEpisodeSchedules.Where(s => s.HippsCode.IsNotNullOrEmpty()).Select(s => s.HippsCode + s.EndDate.AddDays(1).Year).Distinct().ToList());
                            hippsWithYear = hippsWithYear.Distinct().ToList();
                            var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsWithYear.ToArray()) ?? new List<HippsAndHhrg>();
                            scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                            {
                                var schedules = episodeSchedules.Where(es => es.EpisodeId == scheduleEpisode.EpisodeId).OrderByDescending(s => s.EventDate.Date).ToList() ?? new List<ScheduleEvent>();
                                var previousSchedules = previousEpisodeSchedules.Where(s => s.NewEpisodeId == scheduleEpisode.EpisodeId && s.EndDate.Date == scheduleEpisode.StartDate.AddDays(-1).Date && (s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment()) && s.EventDate.Date <= s.EndDate && s.EventDate.Date >= s.EndDate.AddDays(-5).Date).OrderByDescending(s => s.EventDate).ToList() ?? new List<ScheduleEvent>();
                                var hippsCode = string.Empty;
                                var assessmentData = Reports.GetEpisodeAssessment(schedules, previousSchedules, out hippsCode);
                                scheduleEpisode.HippsCode = hippsCode;
                                var prospectivePayment = GetProspectivePayment(pps.FirstOrDefault(p => p.Time.Year == scheduleEpisode.StartDate.Year), hhrg.FirstOrDefault(h => h.HIPPS == scheduleEpisode.HippsCode && h.Time.Year == scheduleEpisode.StartDate.Year), cbsaCodes.FirstOrDefault(cb => cb.Zip.IsEqual(scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? scheduleEpisode.AddressZipCode : agencyData.AddressZipCode)));// var prospectivePayment = GetProspectivePayment(scheduleEpisode.HippsCode, scheduleEpisode.StartDate, scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? scheduleEpisode.AddressZipCode : scheduleEpisode.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);

                                var collection = new Dictionary<string, string>();
                                collection.Add("Patient ID", scheduleEpisode.PatientIdNumber);
                                collection.Add("Patient Name", string.Concat(scheduleEpisode.LastName, ", ", scheduleEpisode.FirstName, (!string.IsNullOrEmpty(scheduleEpisode.MiddleInitial) ? " " + scheduleEpisode.MiddleInitial + "." : string.Empty)));
                                collection.Add("Policy #", scheduleEpisode.MedicareNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicareNumber : scheduleEpisode.MedicaidNumber.IsNotNullOrEmpty() ? scheduleEpisode.MedicaidNumber : string.Empty);
                                collection.Add("Admission ID", string.Empty);
                                collection.Add("Begin Date", scheduleEpisode.StartDate.Date > DateTime.MinValue.Date ? scheduleEpisode.StartDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("End Date", scheduleEpisode.EndDate.Date > DateTime.MinValue.Date ? scheduleEpisode.EndDate.ToString("MM/dd/yyyy") : string.Empty);
                                collection.Add("First Billable", GetFirstBillableDate(schedules));

                                var rap = raps.FirstOrDefault(r => r.Id == scheduleEpisode.EpisodeId && r.PatientId==scheduleEpisode.PatientId && r.EpisodeId==scheduleEpisode.EpisodeId);//Reports.GetRap(agencyData.Id, episode.PatientId, episode.Id);
                                if (rap != null)
                                {
                                    collection.Add("RAP Bill Date", rap.ClaimDate.Date > DateTime.MinValue.Date ? rap.ClaimDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("RAP Bill Amount", string.Format("${0:#0.00}", rap.ClaimAmount));
                                    collection.Add("RAP Expiration Date", scheduleEpisode.EndDate.AddDays(59).ToString("MM/dd/yyyy"));
                                    collection.Add("RAP Payment Received", rap.PaymentDate.Date > DateTime.MinValue.Date ? rap.PaymentDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("RAP Payment Amount", string.Format("${0:#0.00}", rap.PaymentAmount));
                                }

                                var final = finals.FirstOrDefault(f => f.Id == scheduleEpisode.EpisodeId && f.PatientId == scheduleEpisode.PatientId && f.EpisodeId == scheduleEpisode.EpisodeId);//Reports.GetFinal(agencyData.Id, episode.PatientId, episode.Id);
                                if (final != null)
                                {
                                    collection.Add("Final Claim Bill Date", final.ClaimDate.Date > DateTime.MinValue.Date ? final.ClaimDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("Final Claim Payment Received", final.PaymentDate.Date > DateTime.MinValue.Date ? final.PaymentDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("Final Claim Payment Amount", string.Format("${0:#0.00}", final.PaymentAmount));
                                }

                                collection.Add("CA/Other Amount", "$0.00");
                                collection.Add("Current Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");
                                collection.Add("Balance Due", GetBalanceDue(prospectivePayment != null ? prospectivePayment.TotalAmount : 0, rap, final));
                                collection.Add("Cost Amount", "$0.00");
                                collection.Add("Margin", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                                collectionList.Add(collection);
                            });
                            Windows.EventLog.WriteEntry(string.Format("Collection Count: {0}", collectionList.Count), System.Diagnostics.EventLogEntryType.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<PatientsAndVisitsByAgeResult> PatientsAndVisitsByAge(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var ageRange1 = new AgeRange();
            var ageRange2 = new AgeRange();
            var ageRange3 = new AgeRange();
            var ageRange4 = new AgeRange();
            var ageRange5 = new AgeRange();
            var ageRange6 = new AgeRange();
            var ageRange7 = new AgeRange();
            var ageRange8 = new AgeRange();
            var ageRange9 = new AgeRange();
            var ageRange10 = new AgeRange();
            var list = new List<PatientsAndVisitsByAgeResult>();

            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithOutOasis(agencyId, agencyLocationId, startDate, endDate);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                   // var dateTimeNowYear= DateTime.Now.Year;
                   // var tenPatients = GetAgeRange(episodeSchedules, 0, 10);
                    var tenYearVisits = GetVisitByAgeRange(episodeSchedules, 0, 10);
                   // var tenVisits = GetVisitsByAgeRange(tenPatients.List, startDate, endDate);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "1.",
                        Description = "0-10 Years",
                        Patients = tenYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//tenPatients.List.Count,
                        Visits = tenYearVisits.Count//tenVisits
                    });

                    //var twentyPatients = GetAgeRange(episodeSchedules, 11, 20);
                    //var twentyVisits = GetVisitsByAgeRange(twentyPatients.List, startDate, endDate);
                    var twentyYearVisits = GetVisitByAgeRange(episodeSchedules, 11, 20);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "2.",
                        Description = "11-20 Years",
                        Patients = twentyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//twentyPatients.List.Count,
                        Visits = twentyYearVisits.Count//twentyVisits
                    });

                    //var thirtyPatients = GetAgeRange(episodeSchedules, 21, 30);
                    //var thirtyVisits = GetVisitsByAgeRange(thirtyPatients.List, startDate, endDate);
                    var thirtyYearVisits = GetVisitByAgeRange(episodeSchedules, 21, 30);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "3.",
                        Description = "21-30 Years",
                        Patients = thirtyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,// thirtyPatients.List.Count,
                        Visits = thirtyYearVisits.Count// thirtyVisits
                    });

                    //var fortyPatients = GetAgeRange(episodeSchedules, 31, 40);
                    //var fortyVisits = GetVisitsByAgeRange(fortyPatients.List, startDate, endDate);
                    var fortyYearVisits = GetVisitByAgeRange(episodeSchedules, 31, 40);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "4.",
                        Description = "31-40 Years",
                        Patients = fortyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,// fortyPatients.List.Count,
                        Visits = fortyYearVisits.Count //fortyVisits
                    });

                    //var fiftyPatients = GetAgeRange(episodeSchedules, 41, 50);
                    //var fiftyVisits = GetVisitsByAgeRange(fiftyPatients.List, startDate, endDate);
                    var fiftyYearVisits = GetVisitByAgeRange(episodeSchedules, 41, 50);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "5.",
                        Description = "41-50 Years",
                        Patients = fiftyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//fiftyPatients.List.Count,
                        Visits = fiftyYearVisits.Count
                    });

                    //var sixtyPatients = GetAgeRange(episodeSchedules, 51, 60);
                    //var sixtyVisits = GetVisitsByAgeRange(sixtyPatients.List, startDate, endDate);
                    var sixtyYearVisits = GetVisitByAgeRange(episodeSchedules, 51, 60);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "6.",
                        Description = "51-60 Years",
                        Patients = sixtyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//sixtyPatients.List.Count,
                        Visits = sixtyYearVisits.Count
                    });

                    //var seventyPatients = GetAgeRange(episodeSchedules, 61, 70);
                    //var seventyVisits = GetVisitsByAgeRange(seventyPatients.List, startDate, endDate);
                    var seventyYearVisits = GetVisitByAgeRange(episodeSchedules, 61, 70);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "7.",
                        Description = "61-70 Years",
                        Patients = seventyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//seventyPatients.List.Count,
                        Visits = seventyYearVisits.Count
                    });

                    //var eightyPatients = GetAgeRange(episodeSchedules, 71, 80);
                    //var eightyVisits = GetVisitsByAgeRange(eightyPatients.List, startDate, endDate);
                    var eightyYearVisits = GetVisitByAgeRange(episodeSchedules, 71, 80);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "8.",
                        Description = "71-80 Years",
                        Patients = eightyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//eightyPatients.List.Count,
                        Visits = eightyYearVisits.Count
                    });

                    //var ninetyPatients = GetAgeRange(episodeSchedules, 81, 90);
                    //var ninetyVisits = GetVisitsByAgeRange(ninetyPatients.List, startDate, endDate);
                    var ninetyYearVisits = GetVisitByAgeRange(episodeSchedules, 81, 90);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "9.",
                        Description = "81-90 Years",
                        Patients = ninetyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//ninetyPatients.List.Count,
                        Visits = ninetyYearVisits.Count
                    });

                    //var overNinetyPatients = GetAgeRange(episodeSchedules, 91, 1000);
                    //var overNinetyVisits = GetVisitsByAgeRange(overNinetyPatients.List, startDate, endDate);
                    var overNinetyYearVisits = GetVisitByAgeRange(episodeSchedules, 91, 1000);
                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "10.",
                        Description = "91 Years and Older",
                        Patients = overNinetyYearVisits.Select(v => v.AdmissionId).Distinct().ToList().Count,//overNinetyPatients.List.Count,
                        Visits = overNinetyYearVisits.Count
                    });
                    var lastData = new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "15.",
                        Description = "Total",
                        Patients = list.Sum(l => l.Patients),// tenPatients.List.Count + twentyPatients.List.Count + thirtyPatients.List.Count + fortyPatients.List.Count + fiftyPatients.List.Count + sixtyPatients.List.Count + seventyPatients.List.Count + eightyPatients.List.Count + ninetyPatients.List.Count + overNinetyPatients.List.Count,
                        Visits = list.Sum(l => l.Visits)// tenVisits + twentyVisits + thirtyVisits + fortyVisits + fiftyVisits + sixtyVisits + seventyVisits + eightyVisits + ninetyVisits + overNinetyVisits
                    };

                    list.Add(lastData);

                    //list.Add(new PatientsAndVisitsByAgeResult
                    //{
                    //    LineNumber = "15.",
                    //    Description = "Total",
                    //    Patients = tenPatients.List.Count + twentyPatients.List.Count + thirtyPatients.List.Count + fortyPatients.List.Count + fiftyPatients.List.Count + sixtyPatients.List.Count + seventyPatients.List.Count + eightyPatients.List.Count + ninetyPatients.List.Count + overNinetyPatients.List.Count,
                    //    Visits = tenVisits + twentyVisits + thirtyVisits + fortyVisits + fiftyVisits + sixtyVisits + seventyVisits + eightyVisits + ninetyVisits + overNinetyVisits
                    //});
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
        

            return list;
        }

        public List<DischargeByReasonResult> DischargesByReason(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DischargeByReasonResult>();
            try
            {
                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "41.",
                    Description = "Admitted to Hospital",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "42.",
                    Description = "Admitted to SN/IC Facility",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "43.",
                    Description = "Death",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "44.",
                    Description = "Family/Friends Assummed Responsibility",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "45.",
                    Description = "Lack of Funds",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "46.",
                    Description = "Lack of Progress",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "47.",
                    Description = "No Further Home Health Care Needed",
                    Discharges = 0
                });


                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "48.",
                    Description = "Patient Moved out of Area",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "49.",
                    Description = "Patient Refused Service",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "50.",
                    Description = "Physician Request",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "51.",
                    Description = "Transferred to Another HHA",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "52.",
                    Description = "Transferred to Home Care (Personal Care)",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "53.",
                    Description = "Transferred to Hospice",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "54.",
                    Description = "Transferred to Outpatient Rehabilitation",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "59.",
                    Description = "Other, Specify:",
                    Discharges = 0
                });

                list.Add(new DischargeByReasonResult
                {
                    LineNumber = "60.",
                    Description = "Total",
                    Discharges = 0
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<PrimaryPaymentSourceResult> VisitsByPrimaryPaymentSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var medicareVisits = 0;
            var list = new List<PrimaryPaymentSourceResult>();
            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithOutOasis(agencyId, agencyLocationId, startDate, endDate);
                 medicareVisits = GetMedicareVisits(episodeSchedules);
                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "91.",
                    Description = "Medicare",
                    Visits = medicareVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "92.",
                    Description = "Medi-Cal",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "93.",
                    Description = "TRICARE(CHAMPUS)",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "94.",
                    Description = "Other Third Party (Insurance, etc.)",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "95.",
                    Description = "Private (Self Pay)",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "96.",
                    Description = "HMO/PPO (Includes Medicare and Medi-Cal HMOs)",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "97.",
                    Description = "No Reimbursement",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "99.",
                    Description = "Other (Includes MSSP)",
                    Visits = 0
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "100.",
                    Description = "Total",
                    Visits = medicareVisits
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<VisitByStaffTypeResult> VisitsByStaffType(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var otVisits = 0;
            var ptVisits = 0;
            var snVisits = 0;
            var stVisits = 0;
            var hhaVisits = 0;
            var mswVisits = 0;
            var dietVisits = 0;

            var list = new List<VisitByStaffTypeResult>();

            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithOutOasis(agencyId, agencyLocationId, startDate, endDate);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                     hhaVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("HHA")).ToList().Count;// GetVisitsByDiscipline(episodes, "HHA");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "71.",
                        Description = "Home Health Aide",
                        Visits = hhaVisits
                    });

                     dietVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("Dietician")).ToList().Count;// GetVisitsByDiscipline(episodes, "Dietician");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "72.",
                        Description = "Nutritionist (Diet Counseling)",
                        Visits = dietVisits
                    });

                     otVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("OT")).ToList().Count;// GetVisitsByDiscipline(episodes, "OT");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "73.",
                        Description = "Occupational Therapist",
                        Visits = otVisits
                    });

                     ptVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("PT")).ToList().Count;// GetVisitsByDiscipline(episodes, "PT");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "74.",
                        Description = "Physical Therapist",
                        Visits = ptVisits
                    });

                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "75.",
                        Description = "Physician",
                        Visits = 0
                    });

                     snVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("Nursing")).ToList().Count;// GetVisitsByDiscipline(episodes, "Nursing");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "76.",
                        Description = "Skilled Nursing",
                        Visits = snVisits
                    });

                     mswVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("MSW")).ToList().Count;// GetVisitsByDiscipline(episodes, "MSW");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "77.",
                        Description = "Social Worker",
                        Visits = mswVisits
                    });

                     stVisits = episodeSchedules.Where(s => s.IsBillable && s.Discipline.IsEqual("ST")).ToList().Count;// GetVisitsByDiscipline(episodes, "ST");
                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "78.",
                        Description = "Speech Pathologist/Audiologist",
                        Visits = stVisits
                    });

                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "79.",
                        Description = "Spiritual and Pastoral Care",
                        Visits = 0
                    });

                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "84.",
                        Description = "Other",
                        Visits = 0
                    });

                    list.Add(new VisitByStaffTypeResult
                    {
                        LineNumber = "85.",
                        Description = "Total",
                        Visits = hhaVisits + otVisits + ptVisits + snVisits + stVisits + mswVisits + dietVisits
                    });
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<ReferralSourceResult> AdmissionsByReferralSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ReferralSourceResult>();
            try
            {
                var admissionPeriods = Reports.GetPatientAdmissions(agencyId, startDate, endDate);
                var anotherAgencyAdmissions = GetAdmissionCount(admissionPeriods, 5);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "21.",
                    Description = "Another Home Health Agency",
                    Admissions = anotherAgencyAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "22.",
                    Description = "Clinic",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "23.",
                    Description = "Family / Friend",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "24.",
                    Description = "Hospice",
                    Admissions = 0
                });

                var hospitalAdmissions = GetAdmissionCount(admissionPeriods, 3);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "25.",
                    Description = "Hospital (Discharge Planner, etc)",
                    Admissions = hospitalAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "26.",
                    Description = "Local Health Department",
                    Admissions = 0
                });

                var snfAdmissions = GetAdmissionCount(admissionPeriods, 4);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "27.",
                    Description = "Long Term Care Facility (SN/IC)",
                    Admissions = snfAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "28.",
                    Description = "MSSP",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "29.",
                    Description = "Payer (Insurance, HMO etc.)",
                    Admissions = 0
                });

                var phyAdmissions = GetAdmissionCount(admissionPeriods, 2);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "30.",
                    Description = "Physician",
                    Admissions = phyAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "31.",
                    Description = "Self",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "32.",
                    Description = "Social Service Agency",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "34.",
                    Description = "Other",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "35.",
                    Description = "Total",
                    Admissions = hospitalAdmissions + snfAdmissions + phyAdmissions + anotherAgencyAdmissions
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<PrincipalDiagnosisResult> PatientsVisitsByPrincipalDiagnosis(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PrincipalDiagnosisResult>();
            var line1 = new PatientVisit();
            var line2 = new PatientVisit();
            var line3 = new PatientVisit();
            var line4 = new PatientVisit();
            var line5 = new PatientVisit();
            var line6 = new PatientVisit();
            var line7 = new PatientVisit();
            var line8 = new PatientVisit();
            var line9 = new PatientVisit();
            var line10 = new PatientVisit();
            var line11 = new PatientVisit();
            var line12 = new PatientVisit();
            var line13 = new PatientVisit();
            var line14 = new PatientVisit();
            var line15 = new PatientVisit();
            var line16 = new PatientVisit();
            var line17 = new PatientVisit();
            var line18 = new PatientVisit();
            var line19 = new PatientVisit();
            var line20 = new PatientVisit();
            var line21 = new PatientVisit();
            var line22 = new PatientVisit();
            var line23 = new PatientVisit();
            var line24 = new PatientVisit();
            var line25 = new PatientVisit();
            var line26 = new PatientVisit();
            var line27 = new PatientVisit();
            var line28 = new PatientVisit();
            var line29 = new PatientVisit();
            var line30 = new PatientVisit();
            var line31 = new PatientVisit();
            var line32 = new PatientVisit();
            var line33 = new PatientVisit();
            var line34 = new PatientVisit();
            var lineTotal = new PatientVisit();

            // var episodeSchedules = 
            try
            {
                #region Loop
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetween(agencyId, agencyLocationId, startDate, endDate);//Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.StartDate).First()).ToList();
                    if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                    {
                        var previousEpisodeSchedules = Reports.GetPreviousEpisodeOASISAssesments(agencyId, scheduleFirstEpisodes.Select(s => s.EpisodeId).Distinct().ToList());
                        scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                        {
                            var schedules = episodeSchedules.Where(es => es.EpisodeId == scheduleEpisode.EpisodeId).OrderByDescending(s => s.EventDate.Date).ToList() ?? new List<ScheduleEvent>();
                            var previousSchedules = previousEpisodeSchedules.Where(s => s.NewEpisodeId == scheduleEpisode.EpisodeId && s.EndDate.Date == scheduleEpisode.StartDate.AddDays(-1).Date && (s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment()) && s.EventDate.Date <= s.EndDate && s.EventDate.Date >= s.EndDate.AddDays(-5).Date).OrderByDescending(s => s.EventDate).ToList() ?? new List<ScheduleEvent>();
                            var hippsCode = string.Empty;
                            var assessmentData = Reports.GetEpisodeAssessment(schedules, previousSchedules, out hippsCode);
                            scheduleEpisode.HippsCode = hippsCode;

                            // IDictionary<string, Question> assessmentData = null;
                            var episodeVisits = schedules.Where(s => s.IsBillable).ToList().Count;// scheduleEpisode.Schedule.IsNotNullOrEmpty() ? scheduleEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count : 0;
                            //var assessment = Reports.GetEpisodeAssessment(scheduleEpisode);
                            if (assessmentData != null && assessmentData.Count > 0)
                            {
                                //assessmentData = assessment.ToDictionary();
                                var principalDiagnosis = assessmentData.AnswerOrEmptyString("M1020ICD9M").ToLowerCase();
                                principalDiagnosis = principalDiagnosis.IsNotNullOrEmpty() ? principalDiagnosis.Trim() : string.Empty;
                                Windows.EventLog.WriteEntry(string.Format("DX: {0}", principalDiagnosis), System.Diagnostics.EventLogEntryType.Information);

                                if (principalDiagnosis.IsNotNullOrEmpty())
                                {
                                    if (!principalDiagnosis.ToLowerCase().Contains("v") && principalDiagnosis.IsDouble())
                                    {
                                        var principalDiagnosisDouble = principalDiagnosis.ToDouble();
                                        if (principalDiagnosisDouble > 0.0)
                                        {
                                            var rangeList1 = new List<DiagnosisRange> 
                                        { 
                                            new DiagnosisRange { Start = 1.0, End = 41.9 },
                                            new DiagnosisRange { Start = 45.00, End = 139.8 }
                                        };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList1))
                                            {
                                                if (!line1.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line1.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line1.Visits += episodeVisits;
                                            }

                                            if (IsDiagnosis(principalDiagnosisDouble, 42.0))
                                            {
                                                if (!line2.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line2.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line2.Visits += episodeVisits;
                                            }

                                            var rangeList3 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 162.2, End = 162.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList3)
                                                || IsDiagnosis(principalDiagnosisDouble, 197.0)
                                                || IsDiagnosis(principalDiagnosisDouble, 209.21)
                                                || IsDiagnosis(principalDiagnosisDouble, 231.2))
                                            {
                                                if (!line3.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line3.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line3.Visits += episodeVisits;
                                            }

                                            var rangeList4 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 174.0, End = 174.9 },
                                    new DiagnosisRange { Start = 175.0, End = 175.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList4)
                                                || IsDiagnosis(principalDiagnosisDouble, 198.2)
                                                || IsDiagnosis(principalDiagnosisDouble, 198.81)
                                                || IsDiagnosis(principalDiagnosisDouble, 233.0))
                                            {
                                                if (!line4.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line4.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line4.Visits += episodeVisits;
                                            }

                                            var rangeList5 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 152.0, End = 154.0 },
                                    new DiagnosisRange { Start = 209.0, End = 209.17 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList5)
                                                || IsDiagnosis(principalDiagnosisDouble, 159.0)
                                                || IsDiagnosis(principalDiagnosisDouble, 197.4)
                                                || IsDiagnosis(principalDiagnosisDouble, 197.5)
                                                || IsDiagnosis(principalDiagnosisDouble, 197.8)
                                                || IsDiagnosis(principalDiagnosisDouble, 230.3)
                                                || IsDiagnosis(principalDiagnosisDouble, 230.4)
                                                || IsDiagnosis(principalDiagnosisDouble, 230.7))
                                            {
                                                if (!line5.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line5.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line5.Visits += episodeVisits;
                                            }

                                            var rangeList6 = new List<DiagnosisRange>
                                      { 
                                        new DiagnosisRange { Start = 140.0, End = 209.36 },
                                       new DiagnosisRange { Start = 230.0, End = 234.9 }
                                         };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList6))
                                            {
                                                if (!line6.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line6.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line6.Visits += episodeVisits;
                                            }

                                            var rangeList7 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 209.40, End = 209.79 },
                                    new DiagnosisRange { Start = 210.0, End = 229.9 },
                                    new DiagnosisRange { Start = 235.0, End = 238.9 },
                                    new DiagnosisRange { Start = 239.0, End = 239.9 }
                                   };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList7))
                                            {
                                                if (!line7.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line7.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line7.Visits += episodeVisits;
                                            }

                                            var rangeList8 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 249.00, End = 250.93 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList8))
                                            {
                                                if (!line8.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line8.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line8.Visits += episodeVisits;
                                            }

                                            var rangeList9 = new List<DiagnosisRange>
                                    { 
                                    new DiagnosisRange { Start = 240.0, End = 246.9 },
                                    new DiagnosisRange { Start = 251.0, End = 279.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList9))
                                            {
                                                if (!line9.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line9.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line9.Visits += episodeVisits;
                                            }

                                            var rangeList10 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 280.0, End = 289.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList10))
                                            {
                                                if (!line10.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line10.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line10.Visits += episodeVisits;
                                            }

                                            var rangeList11 = new List<DiagnosisRange>
                                    { 
                                    new DiagnosisRange { Start = 290.0, End = 319 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList11))
                                            {
                                                if (!line11.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line11.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line11.Visits += episodeVisits;
                                            }

                                            if (IsDiagnosis(principalDiagnosisDouble, 331.0))
                                            {
                                                if (!line12.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line12.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line12.Visits += episodeVisits;
                                            }

                                            var rangeList13 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 320.0, End = 330.9 },
                                    new DiagnosisRange { Start = 331.11, End = 389.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList13))
                                            {
                                                if (!line13.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line13.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line13.Visits += episodeVisits;
                                            }

                                            var rangeList14 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 391.1, End = 392.0 },
                                    new DiagnosisRange { Start = 393.0, End = 402.91 },
                                    new DiagnosisRange { Start = 404.00, End = 429.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList14))
                                            {
                                                if (!line14.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line14.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line14.Visits += episodeVisits;
                                            }

                                            var rangeList15 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 430, End = 438.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList15))
                                            {
                                                if (!line15.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line15.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line15.Visits += episodeVisits;
                                            }

                                            var rangeList16 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 403.00, End = 403.91 },
                                    new DiagnosisRange { Start = 440, End = 459.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList16)
                                                || IsDiagnosis(principalDiagnosisDouble, 390)
                                                || IsDiagnosis(principalDiagnosisDouble, 392.9))
                                            {
                                                if (!line16.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line16.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line16.Visits += episodeVisits;
                                            }

                                            var rangeList17 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 460, End = 519.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList17))
                                            {
                                                if (!line17.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line17.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line17.Visits += episodeVisits;
                                            }

                                            var rangeList18 = new List<DiagnosisRange>
                                    { 
                                    new DiagnosisRange { Start = 520.0, End = 579.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList18))
                                            {
                                                if (!line18.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line18.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line18.Visits += episodeVisits;
                                            }

                                            var rangeList19 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 580.0, End = 608.9 },
                                    new DiagnosisRange { Start = 614.0, End = 629.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList19))
                                            {
                                                if (!line19.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line19.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line19.Visits += episodeVisits;
                                            }

                                            var rangeList20 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 610.0, End = 611.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList20))
                                            {
                                                if (!line20.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line20.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line20.Visits += episodeVisits;
                                            }

                                            var rangeList21 = new List<DiagnosisRange>
                                    { 
                                    new DiagnosisRange { Start = 630, End = 679.14 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList21))
                                            {
                                                if (!line21.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line21.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line21.Visits += episodeVisits;
                                            }

                                            var rangeList22 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 680.0, End = 709.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList22))
                                            {
                                                if (!line22.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line22.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line22.Visits += episodeVisits;
                                            }

                                            var rangeList23 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 710.0, End = 739.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList23))
                                            {
                                                if (!line23.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line23.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line23.Visits += episodeVisits;
                                            }

                                            var rangeList24 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 740.0, End = 779.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList24))
                                            {
                                                if (!line24.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line24.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line24.Visits += episodeVisits;
                                            }

                                            var rangeList25 = new List<DiagnosisRange>
                                    { 
                                    new DiagnosisRange { Start = 780.01, End = 795.6 },
                                    new DiagnosisRange { Start = 796.0, End = 799.9 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList25)
                                                || IsDiagnosis(principalDiagnosisDouble, 795.79))
                                            {
                                                if (!line25.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line25.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line25.Visits += episodeVisits;
                                            }

                                            var rangeList26 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 800.00, End = 829.1 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList26))
                                            {
                                                if (!line26.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line26.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line26.Visits += episodeVisits;
                                            }

                                            var rangeList27 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 830.0, End = 959.94 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList27))
                                            {
                                                if (!line27.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line27.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line27.Visits += episodeVisits;
                                            }

                                            var rangeList28 = new List<DiagnosisRange> 
                                    { 
                                    new DiagnosisRange { Start = 960.0, End = 995.94 }
                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList28))
                                            {
                                                if (!line28.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line28.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line28.Visits += episodeVisits;
                                            }


                                            var rangeList29 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 996.00, End = 999.9 }
                                                };

                                            if (IsInDiagnosisRange(principalDiagnosisDouble, rangeList29))
                                            {
                                                if (!line29.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                {
                                                    line29.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                }
                                                line29.Visits += episodeVisits;
                                            }
                                        }
                                    }

                                    else if (principalDiagnosis.StartsWith("v"))
                                    {

                                        principalDiagnosis = principalDiagnosis.Replace("v", "");
                                        principalDiagnosis = principalDiagnosis.IsNotNullOrEmpty() ? principalDiagnosis.Trim() : string.Empty;
                                        if (principalDiagnosis.IsNotNullOrEmpty() && principalDiagnosis.IsDouble())
                                        {
                                            var vCode = principalDiagnosis.ToDouble();
                                            if (vCode > 0.0)
                                            {
                                                var rangeList30 = new List<DiagnosisRange> 
                                                  { 
                                                    new DiagnosisRange { Start = 20.0, End = 26.9 },
                                                    new DiagnosisRange { Start = 28.0, End = 29.9 }
                                                  };

                                                if (IsInDiagnosisRange(vCode, rangeList30))
                                                {
                                                    if (!line30.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                    {
                                                        line30.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                    }
                                                    line30.Visits += episodeVisits;
                                                }

                                                if (IsDiagnosis(vCode, 30.1)
                                                    || IsDiagnosis(vCode, 30.2)
                                                    || IsDiagnosis(vCode, 31.1)
                                                    || IsDiagnosis(vCode, 31.2)
                                                    || IsDiagnosis(vCode, 32.1)
                                                    || IsDiagnosis(vCode, 32.2)
                                                    || IsDiagnosis(vCode, 33.1)
                                                    || IsDiagnosis(vCode, 33.2)
                                                    || IsDiagnosis(vCode, 34.1)
                                                    || IsDiagnosis(vCode, 34.2)
                                                    || IsDiagnosis(vCode, 35.1)
                                                    || IsDiagnosis(vCode, 35.2)
                                                    || IsDiagnosis(vCode, 36.1)
                                                    || IsDiagnosis(vCode, 36.2)
                                                    || IsDiagnosis(vCode, 37.1)
                                                    || IsDiagnosis(vCode, 37.2)
                                                    || IsDiagnosis(vCode, 39.1)
                                                    || IsDiagnosis(vCode, 39.2)
                                                    )
                                                {
                                                    if (!line31.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                    {
                                                        line31.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                    }
                                                    line31.Visits += episodeVisits;
                                                }

                                                var rangeList32 = new List<DiagnosisRange> 
                                                            { 
                                                                new DiagnosisRange { Start = 01.0, End = 07.9 },
                                                                new DiagnosisRange { Start = 09.0, End = 19.8 },
                                                                new DiagnosisRange { Start = 40.0, End = 49.9 }
                                                            };

                                                if (IsInDiagnosisRange(vCode, rangeList32))
                                                {
                                                    if (!line32.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                    {
                                                        line32.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                    }
                                                    line32.Visits += episodeVisits;
                                                }


                                                var rangeList33 = new List<DiagnosisRange> 
                                                            { 
                                                                new DiagnosisRange { Start = 50.0, End = 58.9 }
                                                            };

                                                if (IsInDiagnosisRange(vCode, rangeList33))
                                                {
                                                    if (!line33.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                    {
                                                        line33.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                    }
                                                    line33.Visits += episodeVisits;
                                                }

                                                var rangeList34 = new List<DiagnosisRange> 
                                                             { 
                                                              new DiagnosisRange { Start = 60.0, End = 91.99 }
                                                             };

                                                if (IsInDiagnosisRange(vCode, rangeList34))
                                                {
                                                    if (!line34.AdmissionIds.Contains(scheduleEpisode.AdmissionId))
                                                    {
                                                        line34.AdmissionIds.Add(scheduleEpisode.AdmissionId);
                                                    }
                                                    line34.Visits += episodeVisits;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Windows.EventLog.WriteEntry("Assessment Not Found", System.Diagnostics.EventLogEntryType.Information);
                            }

                        });
                #endregion
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "1.",
                            Description = "Infectious and parasitic diseases (exclude HIV)",
                            Visits = line1.Visits,
                            Patients = line1.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "2.",
                            Description = "HIV infections",
                            Visits = line2.Visits,
                            Patients = line2.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "3.",
                            Description = "Malignant neoplasms: Lung",
                            Visits = line3.Visits,
                            Patients = line3.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "4.",
                            Description = "Malignant neoplasms: Breast",
                            Visits = line4.Visits,
                            Patients = line4.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "5.",
                            Description = "Malignant neoplasms: Intestines",
                            Visits = line5.Visits,
                            Patients = line5.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "6.",
                            Description = "Malignant neoplasms: All other sites, excluding those in lung, breast and intestines",
                            Visits = line6.Visits,
                            Patients = line6.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "7.",
                            Description = "Non-malignant neoplasms: All sites",
                            Visits = line7.Visits,
                            Patients = line7.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "8.",
                            Description = "Diabetes mellitus",
                            Visits = line8.Visits,
                            Patients = line8.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "9.",
                            Description = "Endocrine, metabolic, and nutritional diseases; Immunity disorders",
                            Visits = line9.Visits,
                            Patients = line9.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "10.",
                            Description = "Diseases of blood and blood forming organs",
                            Visits = line10.Visits,
                            Patients = line10.AdmissionIds.Count
                        });

                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "11.",
                            Description = "Mental disorder",
                            Visits = line11.Visits,
                            Patients = line11.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "12.",
                            Description = "Alzheimer's disease",
                            Visits = line12.Visits,
                            Patients = line12.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "13.",
                            Description = "Diseases of nervous system and sense organs",
                            Visits = line13.Visits,
                            Patients = line13.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "14.",
                            Description = "Diseases of cardiovascular system",
                            Visits = line14.Visits,
                            Patients = line14.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "15.",
                            Description = "Diseases of cerebrovascular system",
                            Visits = line15.Visits,
                            Patients = line15.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "16.",
                            Description = "Diseases of all other circulatory system",
                            Visits = line16.Visits,
                            Patients = line16.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "17.",
                            Description = "Diseases of respiratory system",
                            Visits = line17.Visits,
                            Patients = line17.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "18.",
                            Description = "Diseases of digestive system",
                            Visits = line18.Visits,
                            Patients = line18.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "19.",
                            Description = "Diseases of genitourinary system",
                            Visits = line19.Visits,
                            Patients = line19.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "20.",
                            Description = "Diseases of breast",
                            Visits = line20.Visits,
                            Patients = line20.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "21.",
                            Description = "Complications of pregnancy, childbirth, and the puerperium",
                            Visits = line21.Visits,
                            Patients = line21.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "22.",
                            Description = "Diseases of skin and subcutaneous tissue",
                            Visits = line22.Visits,
                            Patients = line22.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "23.",
                            Description = "Diseases of musculoskeletal system and connective tissue (include pathological fx, malunion fx, and nonunion fx)",
                            Visits = line23.Visits,
                            Patients = line23.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "24.",
                            Description = "Congenital anomalies and perinatal conditions (include birth fractures)",
                            Visits = line24.Visits,
                            Patients = line24.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "25.",
                            Description = "Symptoms, signs, and ill-defined conditions (exclude HIV positive test)",
                            Visits = line25.Visits,
                            Patients = line25.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "26.",
                            Description = "Fractures (exclude birth fx, pathological fx, malunion fx, nonunion fx)",
                            Visits = line26.Visits,
                            Patients = line26.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "27.",
                            Description = "All other injuries",
                            Visits = line27.Visits,
                            Patients = line27.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "28.",
                            Description = "Poisonings and adverse effects of external causes",
                            Visits = line28.Visits,
                            Patients = line28.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "29.",
                            Description = "Complications of surgical and medical care",
                            Visits = line29.Visits,
                            Patients = line29.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "30.",
                            Description = "Health services related to reproduction and development",
                            Visits = line30.Visits,
                            Patients = line30.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "31.",
                            Description = "Infants born outside hospital (infant care)",
                            Visits = line31.Visits,
                            Patients = line31.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "32.",
                            Description = "Health hazards related to communicable diseases",
                            Visits = line32.Visits,
                            Patients = line32.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "33.",
                            Description = "Other health services for specific procedures and aftercare",
                            Visits = line33.Visits,
                            Patients = line33.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "34.",
                            Description = "Visits for Evaluation and Assessment",
                            Visits = line34.Visits,
                            Patients = line34.AdmissionIds.Count
                        });
                        lineTotal.Visits = line1.Visits + line2.Visits + line3.Visits + line4.Visits + line5.Visits + line6.Visits + line7.Visits + line8.Visits + line9.Visits + line10.Visits + line11.Visits + line12.Visits + line13.Visits + line14.Visits + line15.Visits + line16.Visits + line17.Visits + line18.Visits + line19.Visits + line20.Visits + line21.Visits + line22.Visits + line23.Visits + line24.Visits + line25.Visits + line26.Visits + line27.Visits + line28.Visits + line29.Visits + line30.Visits + line31.Visits + line32.Visits + line33.Visits + line34.Visits;
                        lineTotal.Patients = line1.AdmissionIds.Count + line2.AdmissionIds.Count + line3.AdmissionIds.Count + line4.AdmissionIds.Count + line5.AdmissionIds.Count + line6.AdmissionIds.Count + line7.AdmissionIds.Count + line8.AdmissionIds.Count + line9.AdmissionIds.Count + line10.AdmissionIds.Count + line11.AdmissionIds.Count + line12.AdmissionIds.Count + line13.AdmissionIds.Count + line14.AdmissionIds.Count + line15.AdmissionIds.Count + line16.AdmissionIds.Count + line17.AdmissionIds.Count + line18.AdmissionIds.Count + line19.AdmissionIds.Count + line20.AdmissionIds.Count + line21.AdmissionIds.Count + line22.AdmissionIds.Count + line23.AdmissionIds.Count + line24.AdmissionIds.Count + line25.AdmissionIds.Count + line26.AdmissionIds.Count + line27.AdmissionIds.Count + line28.AdmissionIds.Count + line29.AdmissionIds.Count + line30.AdmissionIds.Count + line31.AdmissionIds.Count + line32.AdmissionIds.Count + line33.AdmissionIds.Count + line34.AdmissionIds.Count;
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "45.",
                            Description = "TOTAL",
                            Visits = lineTotal.Visits,
                            Patients = lineTotal.Patients
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public CostReportResult CostReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var report = new CostReportResult();
            var snMedicarePatients = new List<Guid>();
            var ptMedicarePatients = new List<Guid>();
            var otMedicarePatients = new List<Guid>();
            var stMedicarePatients = new List<Guid>();
            var mswMedicarePatients = new List<Guid>();
            var hhaMedicarePatients = new List<Guid>();

            var snNonMedicarePatients = new List<Guid>();
            var ptNonMedicarePatients = new List<Guid>();
            var otNonMedicarePatients = new List<Guid>();
            var stNonMedicarePatients = new List<Guid>();
            var mswNonMedicarePatients = new List<Guid>();
            var hhaNonMedicarePatients = new List<Guid>();

            var medicareUnduplicated = new List<Guid>();
            var nonMedicareUnduplicated = new List<Guid>();

            int hhaMedicareMinutes = 0;
            int hhaNonMedicareMinutes = 0;

            try
            {
                var episodeSchedules = Reports.GetEpisodeScheduleEventsExactlyBetweenWithOutOasis(agencyId, agencyLocationId, startDate, endDate);
                if (episodeSchedules != null && episodeSchedules.Count > 0)
                {
                    var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.StartDate).First()).ToList();
                    if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                    {
                        scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                        {
                            var schedules = episodeSchedules.Where(es => es.EpisodeId == scheduleEpisode.EpisodeId && es.IsBillable).OrderByDescending(s => s.EventDate.Date).ToList() ?? new List<ScheduleEvent>();
                            var hhaVisits = schedules.Where(s => s.IsBillable && s.Discipline.IsEqual("HHA")).ToList();//episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.Discipline.IsEqual("HHA") && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList();
                            var hhaCount = hhaVisits.Count; //episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable&& s.Discipline.IsEqual("HHA")&& s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count;
                            var otCount = schedules.Where(s => s.IsBillable && s.Discipline.IsEqual("OT")).ToList().Count;// episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.Discipline.IsEqual("OT") && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count;
                            var ptCount = schedules.Where(s => s.IsBillable && s.Discipline.IsEqual("PT")).ToList().Count;//episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable&& s.Discipline.IsEqual("PT")&& s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count;
                            var snCount = schedules.Where(s => s.IsBillable && s.Discipline.IsEqual("Nursing")).ToList().Count;//episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable&& s.Discipline.IsEqual("Nursing")&& s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count;
                            var mswCount = schedules.Where(s => s.IsBillable && s.Discipline.IsEqual("MSW")).ToList().Count;// episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable&& s.Discipline.IsEqual("MSW")&& s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count;
                            var stCount = schedules.Where(s => s.IsBillable && s.Discipline.IsEqual("ST")).ToList().Count;// episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()&& s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable&& s.Discipline.IsEqual("ST")&& s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList().Count;

                            if (scheduleEpisode.EpisodeNotes.IsNotNullOrEmpty())
                            {
                                var detail = scheduleEpisode.EpisodeNotes.ToObject<EpisodeDetail>();
                                if (detail != null && detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2") || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4"))
                                {
                                    hhaVisits.ForEach(e =>
                                    {
                                        hhaMedicareMinutes += GetMinutesSpent(e);
                                    });

                                    if (!medicareUnduplicated.Contains(scheduleEpisode.PatientId))
                                    {
                                        medicareUnduplicated.Add(scheduleEpisode.PatientId);
                                    }

                                    report.SNMedicareVisits += snCount;
                                    if (snCount > 0)
                                    {
                                        if (!snMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            snMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.PTMedicareVisits += ptCount;
                                    if (ptCount > 0)
                                    {
                                        if (!ptMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            ptMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.OTMedicareVisits += otCount;
                                    if (otCount > 0)
                                    {
                                        if (!otMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            otMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.STMedicareVisits += stCount;
                                    if (stCount > 0)
                                    {
                                        if (!stMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            stMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.MSWMedicareVisits += mswCount;
                                    if (mswCount > 0)
                                    {
                                        if (!mswMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            mswMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.HHAMedicareVisits += hhaCount;
                                    if (hhaCount > 0)
                                    {
                                        if (!hhaMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            hhaMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }
                                }
                                else
                                {
                                    hhaVisits.ForEach(e =>
                                    {
                                        hhaNonMedicareMinutes += GetMinutesSpent(e);
                                    });

                                    if (!nonMedicareUnduplicated.Contains(scheduleEpisode.PatientId))
                                    {
                                        nonMedicareUnduplicated.Add(scheduleEpisode.PatientId);
                                    }

                                    report.SNNonMedicareVisits += snCount;
                                    if (snCount > 0)
                                    {
                                        if (!snNonMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            snNonMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.PTNonMedicareVisits += ptCount;
                                    if (ptCount > 0)
                                    {
                                        if (!ptNonMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            ptNonMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.OTNonMedicareVisits += otCount;
                                    if (otCount > 0)
                                    {
                                        if (!otNonMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            otNonMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.STNonMedicareVisits += stCount;
                                    if (stCount > 0)
                                    {
                                        if (!stNonMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            stNonMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.MSWNonMedicareVisits += mswCount;
                                    if (mswCount > 0)
                                    {
                                        if (!mswNonMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            mswNonMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }

                                    report.HHANonMedicareVisits += hhaCount;
                                    if (hhaCount > 0)
                                    {
                                        if (!hhaNonMedicarePatients.Contains(scheduleEpisode.AdmissionId))
                                        {
                                            hhaNonMedicarePatients.Add(scheduleEpisode.AdmissionId);
                                        }
                                    }
                                }
                            }
                        });


                        report.SNMedicarePatients = snMedicarePatients.Count;
                        report.PTMedicarePatients = ptMedicarePatients.Count;
                        report.OTMedicarePatients = otMedicarePatients.Count;
                        report.STMedicarePatients = stMedicarePatients.Count;
                        report.MSWMedicarePatients = mswMedicarePatients.Count;
                        report.HHAMedicarePatients = hhaMedicarePatients.Count;

                        report.SNNonMedicarePatients = snNonMedicarePatients.Count;
                        report.PTNonMedicarePatients = ptNonMedicarePatients.Count;
                        report.OTNonMedicarePatients = otNonMedicarePatients.Count;
                        report.STNonMedicarePatients = stNonMedicarePatients.Count;
                        report.MSWNonMedicarePatients = mswNonMedicarePatients.Count;
                        report.HHANonMedicarePatients = hhaNonMedicarePatients.Count;

                        report.SNTotalPatients = report.SNMedicarePatients + report.SNNonMedicarePatients;
                        report.PTTotalPatients = report.PTMedicarePatients + report.PTNonMedicarePatients;
                        report.OTTotalPatients = report.OTMedicarePatients + report.OTNonMedicarePatients;
                        report.STTotalPatients = report.STMedicarePatients + report.STNonMedicarePatients;
                        report.MSWTotalPatients = report.MSWMedicarePatients + report.MSWNonMedicarePatients;
                        report.HHATotalPatients = report.HHAMedicarePatients + report.HHANonMedicarePatients;

                        report.MedicareUnduplicated = medicareUnduplicated.Count;
                        report.NonMedicareUnduplicated = nonMedicareUnduplicated.Count;
                        report.TotalUnduplicated = report.MedicareUnduplicated + report.NonMedicareUnduplicated;

                        report.HHAMedicareHours = Convert.ToInt32(hhaMedicareMinutes / 60);
                        report.HHANonMedicareHours = Convert.ToInt32(hhaNonMedicareMinutes / 60);
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return report;
        }

        #endregion

        #region Private Method

        private int GetMinutesSpent(ScheduleEvent scheduleEvent)
        {
            var timeIn = DateTime.Now;
            var timeOut = DateTime.Now;
            if (scheduleEvent.TimeOut.IsNotNullOrEmpty() && scheduleEvent.TimeOut.HourToDateTime(ref timeOut) && scheduleEvent.TimeIn.IsNotNullOrEmpty() && scheduleEvent.TimeIn.HourToDateTime(ref timeIn))
            {
                if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                {
                    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    return outi + outO;
                }
                else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                {
                    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    return outi + outO + 12 * 60;

                }
                else
                {
                    if (timeOut >= timeIn)
                    {
                        return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                    }
                }
            }
            return 0;
        }

        private bool IsDiagnosis(double principalDiagnosis, double diagnosis)
        {
            bool result = false;

            if (principalDiagnosis == diagnosis)
            {
                result = true;
            }
            return result;
        }

        private bool IsInDiagnosisRange(double principalDiagnosis, List<DiagnosisRange> rangeList)
        {
            bool result = false;

            foreach (var range in rangeList)
            {
                if (principalDiagnosis >= range.Start && principalDiagnosis <= range.End)
                {
                    return true;
                }
            }
            return result;
        }

        private AgeRange GetAgeRange(List<EpisodeData> episodes, int start, int end)
        {
            var result = new AgeRange { List = new List<Guid>() };
            if (episodes != null)
            {
                episodes.ForEach(episode =>
                {
                    if (episode != null && episode.DOB != DateTime.MinValue)
                    {
                        int age = DateTime.Today.Year - episode.DOB.Year;
                        if (age >= start && age <= end)
                        {
                            if (!result.List.Contains(episode.AdmissionId))
                            {
                                result.List.Add(episode.AdmissionId);
                            }
                        }
                    }
                });
            }
            return result;
        }

        private List<ScheduleEvent> GetVisitByAgeRange(List<ScheduleEvent> scheduleEvents, int start, int end)
        {
            var list = new List<ScheduleEvent>();

            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var dateTimeNowYear = DateTime.Now.Year;
                list = scheduleEvents.Where(s => (dateTimeNowYear - s.DOB.Date.Year) >= start && (dateTimeNowYear - s.DOB.Year) <= end).ToList();
            }
            return list;
        }

        private int GetAdmissionCount(List<AdmissionPeriod> periods, int admissionSourceType)
        {
            var result = 0;
            if (periods != null)
            {
                periods.ForEach(period =>
                {
                    var patient = period.PatientData.IsNotNullOrEmpty() ? period.PatientData.ToObject<Patient>() : null;
                    if (patient != null && patient.AdmissionSource == admissionSourceType.ToString())
                    {
                        result++;
                    }
                });
            }
            return result;
        }

        private string GetEpisodeTiming(IDictionary<string, Question> assessmentData)
        {
            var timing = string.Empty;

            if (assessmentData != null)
            {
                var episodeTiming = assessmentData.AnswerOrEmptyString("M0110EpisodeTiming");
                if (episodeTiming.IsEqual("01"))
                {
                    timing = "Early";
                }
                if (episodeTiming.IsEqual("02"))
                {
                    timing = "Late";
                }
            }

            return timing;
        }

        private string GetNrsSeverityLevel(string hippsCode)
        {
            var severityLevel = string.Empty;

            if (hippsCode.IsNotNullOrEmpty())
            {
                hippsCode = hippsCode.ToLower();
                if (hippsCode.EndsWith("s"))
                {
                    severityLevel = "1";
                }
                if (hippsCode.EndsWith("t"))
                {
                    severityLevel = "2";
                }
                if (hippsCode.EndsWith("u"))
                {
                    severityLevel = "3";
                }
                if (hippsCode.EndsWith("v"))
                {
                    severityLevel = "4";
                }
                if (hippsCode.EndsWith("w"))
                {
                    severityLevel = "5";
                }
                if (hippsCode.EndsWith("x"))
                {
                    severityLevel = "6";
                }
            }

            return severityLevel;
        }

        private string GetFirstBillableDate(List<ScheduleEvent> scheduleEvents)
        {
            var date = string.Empty;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var schedule = scheduleEvents.Where(s => s.IsBillable && (s.Status == "425" || s.Status == "220" || s.Status == "225")).OrderByDescending(s => s.EventDate).FirstOrDefault();
                if (schedule != null && schedule.EventDate > DateTime.MinValue)
                {
                    return schedule.EventDate.ToString("MM/dd/yyyy");
                }
            }
            return date;
        }

        private Dictionary<int, string> GetMonthsBetween(DateTime startDate, DateTime endDate)
        {
            var list = new Dictionary<int, string>();
            if (startDate.Date < endDate.Date)
            {
                var index = 0;
                var month = startDate.Month;
                do
                {
                    list.Add(startDate.AddMonths(index).Month, startDate.AddMonths(index).ToString("MMM"));
                    index++;
                    month++;
                } while (month < endDate.Month + 3);
            }

            return list;
        }

        private string GetVisitsInMonth(List<ScheduleEvent> scheduleEvents, int month)
        {
            var result = "0";
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                return scheduleEvents.Where(s => s.EventDate.Month == month && s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private string GetVisitsByDiscipline(List<ScheduleEvent> scheduleEvents, string discipline)
        {
            var result = "0";
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                return scheduleEvents.Where(s =>  s.Discipline.IsEqual(discipline) && s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private int GetMedicareVisits(List<ScheduleEvent> episodeSchedules)
        {
            var result = 0;

            if (episodeSchedules != null && episodeSchedules.Count > 0)
            {
                var scheduleFirstEpisodes = episodeSchedules.GroupBy(s => s.EpisodeId).Select(s => s.OrderByDescending(e => e.StartDate).First()).ToList();
                if (scheduleFirstEpisodes != null && scheduleFirstEpisodes.Count > 0)
                {
                    scheduleFirstEpisodes.ForEach(scheduleEpisode =>
                    {
                        var detail = scheduleEpisode.EpisodeNotes.IsNotNullOrEmpty() ? scheduleEpisode.EpisodeNotes.ToObject<EpisodeDetail>() : null;
                        if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty() && (detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2") || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4")))
                        {
                            result += episodeSchedules.Where(s => s.IsBillable && s.EpisodeId == scheduleEpisode.EpisodeId).ToList().Count;// episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).ToList().Count;
                        }
                    });
                }
            }
            return result;
        }

        private string GetTotalEpisodeVisits(List<ScheduleEvent> scheduleEvents)
        {
            var result = "0";
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                return scheduleEvents.Where(s =>  s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private ProspectivePayment GetProspectivePayment(PPSStandard ppsStandard, HippsAndHhrg hhrg, CbsaCode cbsaCode)
        {
            var prospectivePayment = new ProspectivePayment();
            if (cbsaCode != null && hhrg != null && ppsStandard != null)
            {
                var hippsCode = hhrg.HIPPS;
                var time = ppsStandard.Time;
                Double rate = 0;
                var isRural = false;
                var wageIndex = this.WageIndex(cbsaCode, time);

                if (cbsaCode.CBSA.IsNotNullOrEmpty())
                {
                    if (cbsaCode.CBSA.StartsWith("9"))
                    {
                        isRural = true;
                        rate = ppsStandard.RuralRate;
                    }
                    else
                    {
                        isRural = false;
                        rate = ppsStandard.UrbanRate;
                    }
                }

                var rateTimesWeight = rate * hhrg.HHRGWeight;
                var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);

                prospectivePayment.Hhrg = hhrg.HHRG;
                prospectivePayment.HippsCode = hippsCode;
                prospectivePayment.CbsaCode = cbsaCode.CBSA;
                prospectivePayment.WageIndex = wageIndex.ToString();
                prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                prospectivePayment.LaborAmount = laborAmount;
                prospectivePayment.NonLaborAmount = nonLaborAmount;
                prospectivePayment.NonRoutineSuppliesAmount = supplyAmount;
                prospectivePayment.TotalAmountWithoutSupplies = totalAmountWithoutSupplies;
                prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;
            }
            return prospectivePayment;
        }

        private double WageIndex(CbsaCode cbsaCode, DateTime time)
        {
            double wageIndex = 0;
            if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }

        private double GetSupplyReimbursement(PPSStandard ppsStandard, bool isRural, char type)
        {
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        if (isRural)
                        {
                            return ppsStandard.RuralS;
                        }
                        return ppsStandard.S;
                    case 'T':
                        if (isRural)
                        {
                            return ppsStandard.RuralT;
                        }
                        return ppsStandard.T;
                    case 'U':
                        if (isRural)
                        {
                            return ppsStandard.RuralU;
                        }
                        return ppsStandard.U;
                    case 'V':
                        if (isRural)
                        {
                            return ppsStandard.RuralV;
                        }
                        return ppsStandard.V;
                    case 'W':
                        if (isRural)
                        {
                            return ppsStandard.RuralW;
                        }
                        return ppsStandard.W;
                    case 'X':
                        if (isRural)
                        {
                            return ppsStandard.RuralX;
                        }
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        private string GetChargesByMonth(List<ScheduleEvent> scheduleEvents, ProspectivePayment prospectivePayment, int month)
        {
            var result = "$0.00";
            if (scheduleEvents != null && scheduleEvents.Count>0 && prospectivePayment != null && month > 0)
            {
                var monthVisits = GetVisitsInMonth(scheduleEvents, month);
                var billableVisits = GetTotalEpisodeVisits(scheduleEvents);
                if (monthVisits.ToInteger() > 0 && billableVisits.ToInteger() > 0)
                {
                    var unitAmount = Math.Round(prospectivePayment.TotalAmount / billableVisits.ToInteger(), 2);
                    result = string.Format("${0:#0.00}", monthVisits.ToInteger() * unitAmount);
                }
            }

            return result;
        }

        private string GetChargesByDiscipline(List<ScheduleEvent> scheduleEvents, ProspectivePayment prospectivePayment, string discipline)
        {
            var result = "$0.00";
            if (scheduleEvents != null && scheduleEvents.Count>0 && prospectivePayment != null && discipline.IsNotNullOrEmpty())
            {
                var disciplineVisits = GetVisitsByDiscipline(scheduleEvents, discipline);
                var billableVisits = GetTotalEpisodeVisits(scheduleEvents);
                if (disciplineVisits.ToInteger() > 0 && billableVisits.ToInteger() > 0)
                {
                    var unitAmount = Math.Round(prospectivePayment.TotalAmount / billableVisits.ToInteger(), 2);
                    result = string.Format("${0:#0.00}", disciplineVisits.ToInteger() * unitAmount);
                }
            }
            return result;
        }

        public string GetBalanceDue(double totalAmount, Claim rap, Claim final)
        {
            var result = string.Format("${0:#0.00}", totalAmount);

            if (rap != null && totalAmount > 0 && rap.PaymentAmount > 0)
            {
                result = string.Format("${0:#0.00}", totalAmount - rap.PaymentAmount);

                if (final != null && final.PaymentAmount > 0)
                {
                    result = string.Format("${0:#0.00}", totalAmount - (rap.PaymentAmount + final.PaymentAmount));
                }
            }

            return result;
        }

        #endregion
    }
}