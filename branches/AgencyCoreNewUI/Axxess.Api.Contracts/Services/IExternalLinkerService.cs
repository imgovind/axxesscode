﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ServiceModel;
using System.ServiceModel.Web;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/")]
    public interface IExternalLinkerService : IService
    {
        [OperationContract]
        [WebInvoke(Method="POST",
            UriTemplate = "openNote/{noteId}",
            ResponseFormat=WebMessageFormat.Json,
            RequestFormat=WebMessageFormat.Json)]
        string UpdatedNote(string noteId);
    }
}
