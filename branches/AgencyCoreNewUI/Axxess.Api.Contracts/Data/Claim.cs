﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2012/01/")]
    public class Claim
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid PatientId { get; set; }
        [DataMember]
        public Guid EpisodeId { get; set; }
        [DataMember]
        public DateTime ClaimDate { get; set; }
        [DataMember]
        public double ClaimAmount { get; set; }
        [DataMember]
        public string ExpirationDate { get; set; }
        [DataMember]
        public DateTime PaymentDate { get; set; }
        [DataMember]
        public double PaymentAmount { get; set; }
    }
}
