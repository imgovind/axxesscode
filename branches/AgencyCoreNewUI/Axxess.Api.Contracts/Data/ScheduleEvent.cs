﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;
    using System.Collections.Generic;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2011/11/")]
    public class ScheduleEvent
    {
        [DataMember]
        public Guid AgencyId { get; set; }
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public int DisciplineTask { get; set; }
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public DateTime EventDate { get; set; }
        [DataMember]
        public DateTime VisitDate { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Discipline { get; set; }
        [DataMember]
        public Guid EpisodeId { get; set; }
        [DataMember]
        public Guid PatientId { get; set; }
        [DataMember]
        public bool IsBillable { get; set; }
        [DataMember]
        public bool IsMissedVisit { get; set; }
        [DataMember]
        public string TimeIn { get; set; }
        [DataMember]
        public string TimeOut { get; set; }
        [DataMember]
        public bool IsDeprecated { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public Guid NewEpisodeId { get; set; }
        [DataMember]
        public string EpisodeNotes { get; set; }
        [DataMember]
        public Guid AdmissionId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleInitial { get; set; }
        [DataMember]
        public string PatientIdNumber { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string AddressCity { get; set; }
        [DataMember]
        public string AddressStateCode { get; set; }
        [DataMember]
        public string AddressZipCode { get; set; }
        [DataMember]
        public string HippsCode { get; set; }
        [DataMember]
        public DateTime StartofCareDate { get; set; }
        [DataMember]
        public DateTime DischargeDate { get; set; }
        [DataMember]
        public DateTime DOB { get; set; }
        [DataMember]
        public string MedicareNumber { get; set; }
        [DataMember]
        public string MedicaidNumber { get; set; }
        [DataMember]
        public int PatientStatus { get; set; }
    }
}