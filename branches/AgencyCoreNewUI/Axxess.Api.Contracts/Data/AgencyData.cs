﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2011/06/")]
    public class AgencyData
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid LocationId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string MedicareProviderNumber { get; set; }
        [DataMember]
        public string MedicaidProviderNumber { get; set; }
        [DataMember]
        public string NationalProviderNumber { get; set; }
        [DataMember]
        public int CahpsVendor { get; set; }
        [DataMember]
        public string CahpsVendorClientId { get; set; }
        [DataMember]
        public string CahpsSurveyDesignator { get; set; }
        [DataMember]
        public bool IsLocationStandAlone { get; set; }
        [DataMember]
        public string AddressZipCode { get; set; }
    }
}