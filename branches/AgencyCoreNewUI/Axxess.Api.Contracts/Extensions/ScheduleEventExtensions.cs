﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Enums;
    public static class ScheduleEventExtensions
    {
        public static bool IsSkilledCare(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                return DisciplineFactory.SkilledCareDisciplines().Exists(d => d.ToString().IsEqual(scheduleEvent.Discipline));
            }
            return false;
        }

        public static bool IsHhaNote(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.Discipline.IsEqual(Disciplines.HHA.ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsMSW(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty())
            {
                if (scheduleEvent.Discipline.IsEqual(Disciplines.MSW.ToString()) && scheduleEvent.DisciplineTask != (int)DisciplineTasks.DriverOrTransportationNote)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsStartofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                return DisciplineTaskFactory.AllSOCDisciplineTasks().Exists(s => s == scheduleEvent.DisciplineTask);
            }
            return false;
        }

        public static bool IsRecertificationAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                return DisciplineTaskFactory.AllRecertDisciplineTasks().Exists(s => s == scheduleEvent.DisciplineTask);
            }
            return false;
        }

        public static bool IsResumptionofCareAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                return DisciplineTaskFactory.ROCDisciplineTasks().Exists(s => s == scheduleEvent.DisciplineTask);
            }
            return false;
        }

        public static IDictionary<string, Question> ToOASISDictionary(this ScheduleEvent scheduleEvent)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
            {
                var key = string.Empty;
                var oasisQuestions = scheduleEvent.Note.ToObject<List<Question>>();
                if (oasisQuestions != null && oasisQuestions.Count > 0)
                {
                    oasisQuestions.ForEach(question =>
                    {
                        if (question.Type == QuestionType.Moo)
                        {
                            key = string.Format("{0}{1}", question.Code, question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else if (question.Type == QuestionType.PlanofCare)
                        {
                            key = string.Format("485{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else if (question.Type == QuestionType.Generic)
                        {
                            key = string.Format("Generic{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else
                        {
                            key = string.Format("{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                    });
                }
            }
            return questions;
        }
    }
}
