﻿namespace Axxess.LookUp.Domain
{
    using System;
    public class HippsAndHhrg
    {
        public int Id { get; set; }
        public string HHRG { get; set; }
        public string HIPPS { get; set; }
        public double HHRGWeight { get; set; }
        public DateTime Time { get; set; }
    }
}
