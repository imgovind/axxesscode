﻿namespace Axxess.LookUp.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    public class Drug
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string LexiDrugId { get; set; }
        
        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0}: {1}", Code, Name);
        }
        #endregion
    }
}
