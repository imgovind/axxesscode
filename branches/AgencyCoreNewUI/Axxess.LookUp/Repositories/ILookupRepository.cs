﻿namespace Axxess.LookUp.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface ILookupRepository
    {
        IList<Supply> Supplies();
        Supply GetSupply(int Id);
        IList<EthnicRace> Races();
        IList<AmericanState> States();
        IList<PaymentSource> PaymentSources();
        IList<DiagnosisCode> DiagnosisCodes();
        IList<ProcedureCode> ProcedureCodes();
        IList<MedicareRate> DefaultMedicareRates();
        IList<Insurance> Insurances();
        IList<SupplyCategory> SupplyCategories();
        IList<ReferralSource> ReferralSources();
        IList<AdmissionSource> AdmissionSources();
        IList<Relationship> Relationships();
        IList<DrugClassification> DrugClassifications();
        IList<DisciplineTask> DisciplineTasks(string Discipline);
        IList<DisciplineTask> DisciplineTasks();
        DisciplineTask GetDisciplineTask(int disciplineTaskId);
        Relationship GetRelationship(int Id);
        Npi GetNpiData(string npi);
        AdmissionSource GetAdmissionSource(int sourceId);
        string GetAdmissionSourceCode(int sourceId);
        IList<Npi> GetNpis(string q, int limit);
        ZipCode GetZipCode(string zipCode);
        bool VerifyPecos(string npi);
        IPAddress GetIPAddress(int ipAddress);
        string GetZipCodeFromIpAddress(int ipAddress);
        IList<MedicationRoute> MedicationRoute(string q, int limit);
        IList<MedicationClassfication> MedicationClassification(string q, int limit);
        IList<MedicationDosage> MedicationDosage(string q, int limit);
        Insurance GetInsurance(int insuranceId);
        string CbsaCodeByZip(string zipCode);
        List<CBSACode> CbsaCodesByZip(string[] zipCodes);
        CBSACode CbsaCode(string zipCode);
        HippsAndHhrg GetHHRGByHIPPSCODE(string hippsCode);
        List<HippsAndHhrg> GetHhrgByHippsCodeAndYear(string[] hippsAndYears);
        PPSStandard GetPPSStandardByYear(int year);
        double ProspectivePayAmount(string hppisCode, DateTime time, string zipCode, string agencyZipCode);
        IList<CBSACode> CbsaCodes();
        IList<PPSStandard> PPSStandards();
        List<PPSStandard> PPSStandards(int[] years);
        IList<HippsAndHhrg> HippsAndHhrgs();
        double GetProspectivePaymentAmount(string hippsCode, DateTime time, string zipCode);
        ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode);

        AxxessSubmitterInfo SubmitterInfo(int payerId);
        List<AxxessSubmitterInfo> MedicareIntermediaries();
    }
}
