﻿namespace Axxess.Membership.Enums
{
    public enum ResetAttemptType
    {
        Failed,
        Success,
        Locked,
        Deactivated
    }
}
