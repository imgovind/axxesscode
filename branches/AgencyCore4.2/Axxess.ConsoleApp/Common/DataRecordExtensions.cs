﻿using System;
using System.Data;

using Axxess.Core.Extension;
using Kent.Boogaart.KBCsv;

namespace Axxess.ConsoleApp
{
    public static class DataRecordExtensions
    {
        public static string GetValue(this DataRecord dataRecord, int columnIndex)
        {
            if (dataRecord != null && columnIndex < dataRecord.Values.Count)
            {
                if (dataRecord[columnIndex] != null && dataRecord[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRecord[columnIndex].ToString().Trim().Replace("'", "").Replace("\"", "").Replace("\\", "");
                }
            }
            return string.Empty;
        }
    }  
}
