﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;
using System.Collections.Generic;

namespace Axxess.ConsoleApp.Tests
{
    public static class ManualExcelIntakeThree
    {
        private static string input = Path.Combine(App.Root, "Files\\angelwings.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\angelwings_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                    {
                                        var patientData = new PatientData();
                                        patientData.AgencyId = "b3bfe480-5062-4a75-8e1b-b401e30733b0";
                                        patientData.AgencyLocationId = "24cfbf1e-c325-4bf5-ba4c-34f742820cf8";
                                        patientData.SSN = "";
                                        patientData.MedicareNumber = "";
                                        patientData.MedicaidNumber = "";
                                        patientData.PatientStatusId = "1";
                                        patientData.MaritalStatus = "Unknown";
                                        patientData.PatientNumber = dataRow.GetValue(1);
                                        var nameArray = dataRow.GetValue(2).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (nameArray != null && nameArray.Length > 1)
                                        {
                                            var firstNameArray = nameArray[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (firstNameArray != null && firstNameArray.Length > 0)
                                            {
                                                if (firstNameArray.Length == 2)
                                                {
                                                    patientData.FirstName = firstNameArray[0].Trim();
                                                    patientData.MiddleInitial = firstNameArray[1].Trim();
                                                }
                                                else
                                                {
                                                    patientData.FirstName = firstNameArray[0].Trim();
                                                }
                                            }
                                            patientData.LastName = nameArray[0];
                                        }
                                        if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                        {
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(3))).ToString("yyyy-M-d"); 
                                        }
                                        if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                        {
                                            var addressArray = dataRow.GetValue(4).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                            if (addressArray != null)
                                            {
                                                if (addressArray.Length == 2)
                                                {
                                                    var twoWordCityArray = addressArray[0].Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (twoWordCityArray != null && twoWordCityArray.Length == 2)
                                                    {
                                                        patientData.AddressLine1 = twoWordCityArray[0].Replace("'", "");
                                                        patientData.AddressLine2 = "";
                                                        patientData.AddressCity = twoWordCityArray[1].Replace("'", "");
                                                    }
                                                    else
                                                    {
                                                        var oneWordCityArray = addressArray[0].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (oneWordCityArray != null && oneWordCityArray.Length > 1)
                                                        {
                                                            patientData.AddressCity = oneWordCityArray.Reverse()[0].Replace("'", "");

                                                            oneWordCityArray = oneWordCityArray.Reverse();
                                                            int addressLine1Count = 0;
                                                            var addressLine1Array = new StringBuilder();
                                                            do
                                                            {
                                                                addressLine1Array.AppendFormat("{0} ", oneWordCityArray[addressLine1Count]);
                                                                addressLine1Count++;
                                                            } while (addressLine1Count < oneWordCityArray.Length - 1);

                                                            patientData.AddressLine1 = addressLine1Array.ToString().TrimEnd();
                                                            patientData.AddressLine2 = "";
                                                        }
                                                    }
                                                    var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (locationArray != null && locationArray.Length == 2)
                                                    {
                                                        patientData.AddressState = locationArray[0];
                                                        patientData.AddressZipCode = locationArray[1].Substring(0, 5);
                                                    }
                                                }
                                                //        int cityNameCount = 2;
                                                //        var city = new string[addressCityArray.Length - cityNameCount];

                                                //        do
                                                //        {
                                                //            city[cityNameCount - 2] = locationArray[cityNameCount];
                                                //            cityNameCount++;
                                                //        } while (cityNameCount < locationArray.Length);

                                                //        if (city.Length > 0)
                                                //        {
                                                //            city = city.Reverse();
                                                //            city.ForEach(name =>
                                                //            {
                                                //                patientData.AddressCity += string.Format("{0} ", name.Replace(",", ""));
                                                //            });
                                                //            patientData.AddressCity = patientData.AddressCity.Trim();
                                                //        }
                                                //    }

                                                //    var locationArray = addressArray[2].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                //    if (locationArray != null && locationArray.Length == 2)
                                                //    {
                                                //        patientData.AddressState = locationArray[0];
                                                //        patientData.AddressZipCode = locationArray[1];
                                                //    }

                                                //}

                                                //if (addressArray.Length == 4)
                                                //{
                                                //    patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                //    patientData.AddressLine2 = addressArray[1].Replace("'", "");
                                                //    patientData.AddressCity = addressArray[2].Trim();

                                                //    var locationArray = addressArray[3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                //    if (locationArray != null && locationArray.Length == 2)
                                                //    {
                                                //        patientData.AddressState = locationArray[0];
                                                //        patientData.AddressZipCode = locationArray[1];
                                                //    }
                                                //}
                                            }
                                        }

                                        patientData.Phone = dataRow.GetValue(5).IsNotNullOrEmpty() ? dataRow.GetValue(5).ToPhoneDB() : "";
                                        if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                        {
                                            patientData.BirthDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(6))).ToString("yyyy-M-d"); 
                                        }

                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                        textWriter.Write(textWriter.NewLine);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
