﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class CradlePatientIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\blossom.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Blossom_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "6c5cb5d2-d448-4b43-b556-0f7a5180e83b";
                                patientData.AgencyLocationId = "4d05c77a-2a0f-4f08-940e-3c963bdc1969";
                                patientData.PatientNumber = record.GetValue(4);
                                patientData.Gender = record.GetValue(13).IsEqual("01") || record.GetValue(13).IsEqual("1") ? "Female" : "Male";
                                patientData.MedicareNumber = record.GetValue(0);
                                patientData.FirstName = record.GetValue(6).ToTitleCase();
                                patientData.LastName = record.GetValue(7).ToTitleCase();
                                patientData.MiddleInitial = "";
                                patientData.BirthDate = record.GetValue(14).ToMySqlDate(0);
                                patientData.AddressLine1 = record.GetValue(8).ToTitleCase();
                                patientData.AddressLine2 = "";
                                patientData.AddressCity = record.GetValue(9).ToTitleCase();
                                patientData.AddressState = record.GetValue(10);
                                patientData.AddressZipCode = record.GetValue(11).Substring(0, 5);
                                patientData.StartofCareDate = record.GetValue(1).ToMySqlDate(0);
                                patientData.Phone = !record.GetValue(12).Contains("_") ? record.GetValue(12).Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "") : "";
                                patientData.PatientStatusId = record.GetValue(33).IsEqual("true") ? "1" : "2";
                                if (record.GetValue(45).IsNotNullOrEmpty()) patientData.DischargeDate = record.GetValue(45).ToMySqlDate(0);
                                patientData.Comments = "Primary Diagnosis Code: " + record.GetValue(31) +  ". " + record.GetValue(32) + ". Episode Frequency: " + record.GetValue(16) + ". ";
                                patientData.Comments += "Physician: " + record.GetValue(19) + " " + record.GetValue(18) + " " + record.GetValue(17) + ". ";
                                patientData.Comments += "Assessor: " + record.GetValue(29) + " " + record.GetValue(30) + ". ";
                                patientData.Comments += "Payer Name: " + record.GetValue(43) + ". ";
                                patientData.Comments += "Triage Level: " + record.GetValue(15) + ".";

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
