﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.OasisC.Domain;
using Axxess.OasisC.Repositories;

using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.Repositories;

using Excel;
using Kent.Boogaart.KBCsv;

namespace Axxess.ConsoleApp.Tests
{
    public static class LoadPhysicians
    {
        private static Guid AgencyId = new Guid("7dd5ba4e-08dd-4c32-9709-264b280451d3");
        private static readonly IAgencyManagementDataProvider agencyDataProvider = new AgencyManagementDataProvider();
        
        private static string input = Path.Combine(App.Root, "Files\\EminentDoctors.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\EminentDoctors_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            try
            {
                using (TextWriter textWriter = new StreamWriter(output, true))
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        var physicianData = new AgencyPhysicianData();
                                        physicianData.AgencyId = AgencyId.ToString();

                                        if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                        {
                                            var nameArray = dataRow.GetValue(1).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                physicianData.LastName = nameArray[0].Trim();
                                                var firstNameArray = nameArray[1].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (firstNameArray != null && firstNameArray.Length > 1)
                                                {
                                                    physicianData.FirstName = firstNameArray[0].Trim();
                                                    physicianData.MiddleName = firstNameArray[1].Trim();
                                                }
                                                else
                                                {
                                                    physicianData.FirstName = nameArray[1].Trim();
                                                }
                                            }
                                        }

                                        physicianData.AddressLine1 = dataRow.GetValue(2);
                                        physicianData.AddressLine2 = dataRow.GetValue(3);
                                        physicianData.AddressCity = dataRow.GetValue(4);
                                        physicianData.AddressState = dataRow.GetValue(5);
                                        physicianData.AddressZipCode = dataRow.GetValue(6);
                                        physicianData.PhoneWork = dataRow.GetValue(7).ToPhoneDB();
                                        physicianData.FaxNumber = dataRow.GetValue(8).ToPhoneDB();
                                        physicianData.Credentials = dataRow.GetValue(9);
                                        physicianData.NPI = dataRow.GetValue(10);

                                        textWriter.WriteLine(new AgencyPhysicianScript(physicianData).ToString());
                                        textWriter.Write(textWriter.NewLine);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static List<string> GetPhoneArray(string phone)
        {
            var phoneArray = new List<string>();
            if (phone.IsNotNullOrEmpty() && phone.Length == 10)
            {
                phoneArray.Add(phone.Substring(0, 3));
                phoneArray.Add(phone.Substring(3, 3));
                phoneArray.Add(phone.Substring(6, 4));
            }
            return phoneArray;
        }
    }
}
