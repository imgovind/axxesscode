﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.OasisC.Domain;
using Axxess.OasisC.Repositories;

using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.Repositories;

using Axxess.LookUp.Domain;
using Axxess.LookUp.Repositories;

using Excel;
using Kent.Boogaart.KBCsv;

namespace Axxess.ConsoleApp.Tests
{
    public static class UpdatePhysicianInfo
    {
        private static Guid AgencyId = new Guid("1a4d9293-0660-419b-939c-a4b2345ae520");
        private static readonly ILookUpDataProvider lookupDataProvider = new LookUpDataProvider();
        private static readonly IAgencyManagementDataProvider agencyDataProvider = new AgencyManagementDataProvider();

        public static void Run()
        {
            try
            {
                var agencyPhysicians = agencyDataProvider.PhysicianRepository.GetAgencyPhysicians(AgencyId);
                Console.WriteLine("Agency Physician Count: {0}", agencyPhysicians != null ? agencyPhysicians.Count.ToString() : "0");

                agencyPhysicians.ForEach(p =>
                {
                    if (p.AddressLine1.IsNullOrEmpty())
                    {
                        var physicianData = lookupDataProvider.LookUpRepository.GetNpiData(p.NPI);
                        if (physicianData != null)
                        {
                            p.Credentials = physicianData.ProviderCredentialText;
                            p.AddressLine1 = physicianData.ProviderFirstLineBusinessPracticeLocationAddress;
                            p.AddressLine2 = physicianData.ProviderSecondLineBusinessPracticeLocationAddress;
                            p.AddressCity = physicianData.ProviderBusinessPracticeLocationAddressCityName;
                            p.AddressStateCode = physicianData.ProviderBusinessPracticeLocationAddressStateName;
                            p.AddressZipCode = physicianData.ProviderBusinessPracticeLocationAddressPostalCode;

                            if (agencyDataProvider.PhysicianRepository.Update(p))
                            {
                                Console.WriteLine("Physician Data Updated: {0}.", p.DisplayName);
                            }
                        }

                    }

                });
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }
    }
}
