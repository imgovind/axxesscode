﻿<%@ Page Language="C#" %><%
Response.ContentType = "text/css";
String prefix = "";
switch (HttpContext.Current.Request.Browser.Browser){
    case "AppleMAC-Safari":
        prefix = "-webkit-";
        break;
    case "Konqueror":
        prefix = "-khtml-";
        break;
} %>
/*-- gradients --*/
.note_modal{background-image:<%= prefix %>linear-gradient(300deg,#ffff87,#b8b108)}
.note_modal.blue{background-image:<%= prefix %>linear-gradient(300deg,#b6e0ff,#0d78e0)}
.note_modal.red{background-image:<%= prefix %>linear-gradient(300deg,#ff8787,#b80707)}
.modal-gray{background-image:<%= prefix %>linear-gradient(90deg,transparent,rgba(255,255,255,.4))}
#bar-bottom li.active a{background:<%= prefix %>linear-gradient(300deg,rgba(200,200,200,.9),rgba(50,73,126,.7),rgb(50,73,126))}
.agencySelection{background-image:<%= prefix %>linear-gradient(315deg,#ccc,#eee,#aaa,#eee,#bbb)}
#agencySelectionLinks li{background:transparent <%= prefix %>linear-gradient(90deg,rgba(100,100,150,.5),rgba(200,200,200,.8)) repeat scroll 0 0}
#agencySelectionLinks li a:hover{background:transparent <%= prefix %>radial-gradient(#ffc,rgba(255,255,204,.2)) repeat scroll 0 0}
ul.context-menu li.hover{background-image:<%= prefix %>linear-gradient(90deg,#7fadc7,#afd7ed,#7fadc7)}
.t-state-selected,.ui-autocomplete .ui-menu-item a.ui-state-hover,.ui-datepicker-current-day{background:#edfcff <%= prefix %>linear-gradient(90deg,#d1dcde,#edfcff,#d1dcde)}
.t-grid-content .t-state-hover{background:#ccdfe3 <%= prefix %>linear-gradient(90deg,#ccdfe3,#e8eef0,#ccdfe3)}
.ui-autocomplete{background-image:<%= prefix %>linear-gradient(315deg,#fff,#eee)}
#window_error{background-image:<%= prefix %>linear-gradient(315deg,#fcedea,#fcb6ac)}
fieldset{background-image:<%= prefix %>linear-gradient(115deg,#f5e9c4,#fffcf2,#f5e9c4)}
fieldset.oasis{background-image:<%= prefix %>linear-gradient(115deg,#d1e0d1,#ebfceb,#d1e0d1)}
fieldset.loc485{background-image:<%= prefix %>linear-gradient(115deg,#ccc,#e3e3e3,#ccc)}
.wound-note{background-image:<%= prefix %>linear-gradient(115deg,#889,#eef,#889)}
#widget-calendar{background-image:<%= prefix %>linear-gradient(80deg,#d3d3d3,#fff,#d3d3d3)}
/*-- transforms --*/
#widget-calendar .year{<%= prefix %>transform:rotate(90deg)}
#widget-calendar{<%= prefix %>transform:rotate(-15deg)}
.note_modal.bottom{<%= prefix %>transform:rotate(4deg)}
.note_modal.middle{<%= prefix %>transform:rotate(2deg)}
/*-- border radius --*/
.wound-note{<%= prefix %>border-radius: 1.5em 1.5em 3em 3em}
#window_error{<%= prefix %>border-radius:2em}
.trical .abs-left,.trical .abs-left a{<%= prefix %>border-radius:0 0 1em 0}
.trical .abs-right,.trical .abs-right a{<%= prefix %>border-radius:0 0 0 1em}
fieldset,.two-thirds,.modal-gray.window,fieldset .shade,#tooltip.calday,.tooltip-box,.page .wrapper .main,.page .wrapper .encapsulation,.agencySelection,#agencySelectionLinks li,#agencySelectionLinks li a:hover,#widget-calendar,fieldset .checkgroup .option{<%= prefix %>border-radius:.9em}
.encapsulation h4,.show-scheduler,.show-summary,.widget,.widget-more,#print-return-reason{<%= prefix %>border-radius:.9em .9em 0 0}
.vertical-tab-list li{<%= prefix %>border-radius:.9em 0 0 .9em}
.widget-head{<%= prefix %>border-radius:.7em .7em 0 0}
.wound textarea,.wound-note input,div.buttons li a,.ancillary-button a,.page .wrapper .encapsulation h5{<%= prefix %>border-radius:.6em}
.wound-note select{<%= prefix %>border-radius:.6em 0 0 .6em}
div.buttons li,.ancillary-button,.diagnosis-item,.masterCalendar .active div,#login-wrapper .button,.ui-autocomplete .ui-menu-item a{<%= prefix %>border-radius:.5em}
.remittance td,.remittance th,.tooltip_oasis,.tooltip-box .content,.nursing th,.nursing td,.nursingsub th,.nursingsub td,#bar-top ul.menu ul.menu,#bar-bottom li a,.window,.ui-dialog,.jGrowl div.jGrowl-notification,div.jGrowl div.jGrowl-closer,.standard-chart ul,.standard-chart ol,.standard-chart ul,.standard-chart ol,.medprofile ul,.medprofile ol,.billing ul,.billing ol,.payroll ul,.payroll ol,.address-block,#directions,.braden td,.braden th,.braden_score li.strong,.tinetti_score li.strong,#login-wrapper form input,#login-wrapper form textarea,#login-wrapper form select,.ui-widget-shadow,.t-pager .t-state-active,.t-pager .t-state-hover,li.token-input-token-facebook,.standard-chart li.first.last,.medprofile li.first.last,.billing li.first.last,.payroll li.first.last{<%= prefix %>border-radius:.4em}
#print-box,#validation-box,.window-top,.ui-dialog-titlebar,.standard-chart li.first,.medprofile li.first,.billing li.first,.payroll li.first,.billing .discpiline-title,.box-header,.t-tabstrip .t-item{<%= prefix %>border-radius:.4em .4em 0 0}
#print-controls,#oasis-validation-controls,#bar-top ul.menu,.window-bottom,.standard-chart li.last,.medprofile li.last,.billing li.last,.payroll li.last{<%= prefix %>border-radius:0 0 .4em .4em}
.events ul,.ui-dialog-titlebar-close,.notification,.teaser_view .replies,.teaser_view .views,.post_view .creation,.t-drag-clue,.t-grouping-header .t-group-indicator,.t-treeview .t-state-hover,.t-treeview .t-state-selected,.activity-log a{<%= prefix %>border-radius:.3em}
.t-button,.window-min,.window-resize,.window-close{<%= prefix %>border-radius:.2em}
#bar-top ul.menu li:last-child a,.box{<%= prefix %>border-radius:0 0 .2em .2em}
#bar-top ul.menu ul li:first-child a{<%= prefix %>border-radius:.2em .2em 0 0}
.remittance table td,.remittance table th,.nursing th,.vertical-tab-list,.general,.nursing table td,.nursing table th,.nursingsub table td,.nursingsub table th,#bar-top ul.menu li:last-child li a,.maximized,.maximized .window-top,.maximized .window-bottom{<%= prefix %>border-radius:0}
/*-- box shadow --*/
#window_error{<%= prefix %>box-shadow:.2em .2em 2em #aaa}
#bar-top,#bar-top ul.menu,ul.context-menu,.message-content-panel div.message-header-container,.message-content-panel div.message-body-container{<%= prefix %>box-shadow:#333 .1em .1em .4em}
.window,.ui-dialog,#tooltip.calday,.tooltip-box,.activity-log a{<%= prefix %>box-shadow:#555 .2em .2em .6em}
.window.active,.ui-dialog{<%= prefix %>box-shadow:#333 .4em .4em 1.5em}
.window-close:hover,.ui-dialog-titlebar-close:hover{<%= prefix %>box-shadow:#f00 0 0 1em}
.window-min:hover,.window-resize:hover{<%= prefix %>box-shadow:#09f 0 0 1em}
.jGrowl div.jGrowl-notification,div.jGrowl div.jGrowl-closer,.t-menu .t-group,.t-filter-options,.t-datepicker-calendar,.page .wrapper .main,.page .wrapper .encapsulation,.ui-datepicker{<%= prefix %>box-shadow:#bbb .3em .3em .5em}
.cal .active,.standard-chart li.hover,.medprofile li.hover,.billing li.hover,.payroll li.hover,.ancillary-button a:hover,div.buttons li a:hover,.ui-datepicker-current-day{<%= prefix %>box-shadow:#333 0 .1em .4em}
.t-state-selected{<%= prefix %>box-shadow:inset #555 .1em .1em .2em}
.t-grid-content .t-state-hover{<%= prefix %>box-shadow:#555 0 0 .5em}
#masterCalendarTable .active div{<%= prefix %>box-shadow:#555 .5em .5em 2em}
.widget,#print-return-reason,.address-block,#directions,.diagnosis-item,#agencySelectionLinks li{<%= prefix %>box-shadow:#333 .2em .2em .5em}
.note_modal.bottom,.modal-gray.window,.agencySelection{<%= prefix %>box-shadow:#000 1em 1em 2em}
.remittance td,.remittance th,.nursing th,.nursing td,.nursingsub th,.nursingsub td,.standard-chart ul,.standard-chart ol,.medprofile ul,.medprofile ol,.billing ul,.billing ol,.payroll ul,.payroll ol,#final-tabs fieldset,.braden td,.braden th{<%= prefix %>box-shadow:#aaa 0 .2em .3em}
.events ul{<%= prefix %>box-shadow:#333 0 .7em .7em}
.ancillary-button a:active,div.buttons li a:active{<%= prefix %>box-shadow:#000 0 0 .5em}
#login-wrapper form input.error:focus{<%= prefix %>box-shadow:inset #ea5338 0 .1em .2em}
#login-wrapper form input:focus,#login-wrapper form textarea:focus,#login-wrapper form select:focus{<%= prefix %>box-shadow:#d0e1f7 0 0 .3em}
fieldset{<%= prefix %>box-shadow:1em 1.2em .8em #eee}
.wound-note{<%= prefix %>box-shadow:#555 1em 1em 1.5em}
.wound textarea,.wound-note input,.wound-note select{<%= prefix %>box-shadow:inset #ddd .1em .1em .5em}
.remittance table td,.remittance table th,.nursing table td,.nursing table th,.nursingsub table td,.nursingsub table th,.braden .total,.orders-filter,fieldset .checkgroup .option.selected{<%= prefix %>box-shadow:none}
.tooltip_oasis{<%= prefix %>box-shadow:inset #aaa -.1em -.1em .3em}
#widget-calendar{<%= prefix %>box-shadow:#bbb 0 0 .8em}
fieldset .checkgroup .option{<%= prefix %>box-shadow:#d3d3d3 0 0 .3em}