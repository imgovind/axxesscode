﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Infections", "Export", FormMethod.Post)) { %>
<span class="wintitle">List Infections | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Infections", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Infection>().Name("List_InfectionReport").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
    columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
    columns.Bound(i => i.InfectionType).Title("Type of Infection").Sortable(true);
    columns.Bound(i => i.InfectionDateFormatted).Title("Infection Date").Sortable(true);
    columns.Bound(i => i.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
    columns.Bound(i => i.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditInfection('<#=Id#>');\">Edit</a>  | <a href=\"javascript:void(0);\" onclick=\"InfectionReport.Delete('<#=PatientId#>', '<#=EpisodeId#>', '<#=Id#>');\">Delete</a>").Title("Action").Width(200).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Infection")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_InfectionReport .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Infection Log", Click: UserInterface.ShowNewInfectionReport } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>