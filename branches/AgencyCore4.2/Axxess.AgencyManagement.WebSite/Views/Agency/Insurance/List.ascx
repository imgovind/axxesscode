﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Insurance | <%= Current.AgencyName %></span>
<div class="wrapper">
    <%= Html.Telerik().Grid<InsuranceLean>().Name("List_AgencyInsurance").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.Name).Title("Insurance Name").Sortable(true);
    columns.Bound(c => c.PayerTypeName).Title("Payer Type").Sortable(true);
    columns.Bound(c => c.PayorId).Title("Payor Id").Width(150).Sortable(true);
    columns.Bound(c => c.InvoiceTypeName).Title("Invoice Type").Width(110).Sortable(true);
    columns.Bound(c => c.PhoneNumber).Title("Phone").Width(120).Sortable(false);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Width(180).Sortable(false);
    columns.Bound(c => c.Action).Title("Action").Sortable(false).Width(120).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Insurance")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
$("#List_AgencyInsurance .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageInsurance) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Insurance", Click: UserInterface.ShowNewInsurance } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
