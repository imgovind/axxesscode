﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<% using (Html.BeginForm("EditOrders", "Agency", FormMethod.Post, new { @id = "updateOrderHistry" })) { %>
<div class="form-wrapper">
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Update <%= Model.Text %></legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Sent Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="SendDate" value="<%= Model.SendDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.SendDate.ToShortDateString() %>" /></div>
            </div><div class="row">
                <label class="float-left">Received Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="ReceivedDate" value="<%= Model.ReceivedDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.ReceivedDate.ToShortDateString() %>" /></div>
            </div><div class="row">
                <label class="float-left">Physician Signature Date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="PhysicianSignatureDate" value="<%= Model.PhysicianSignatureDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.PhysicianSignatureDate.ToShortDateString() %>" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Update</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>