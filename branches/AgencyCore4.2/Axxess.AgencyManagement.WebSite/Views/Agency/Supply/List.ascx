﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Supplies | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Supplies", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<AgencySupply>()
        .Name("List_Supply")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(t => t.Description).Sortable(true);
            columns.Bound(t => t.Code).Title("HCPCS").Sortable(true).Width(65);
            columns.Bound(t => t.RevenueCode).Title("Rev Code").Sortable(true).Width(65);
            columns.Bound(t => t.UnitCost).Format("${0:0.00}").Sortable(true).Width(70);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(100);
            columns.Bound(t => t.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditSupply('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Supply.Delete('<#=Id#>');\" class=\"deleteSupply\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Supply"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_Supply .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (!Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Supply", Click: UserInterface.ShowNewSupply } ])
)<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>