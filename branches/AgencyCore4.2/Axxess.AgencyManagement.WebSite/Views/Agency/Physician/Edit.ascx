﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<span class="wintitle">Edit Physician | <%= Model.DisplayName %></span>
<%  using (Html.BeginForm("Update", "Physician", FormMethod.Post, new { @id = "editPhysicianForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Physician_Id" })%>
<div class="wrapper main">
    
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Physician_FirstName" class="float-left">First Name:</label><div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Physician_FirstName", @class = "text input_wrapper required", @maxlength = "50" })%></div></div>
            <div class="row"><label for="Edit_Physician_MiddleIntial" class="float-left">MI:</label><div class="float-right"><%= Html.TextBox("MiddleName", Model.MiddleName, new { @id = "Edit_Physician_MiddleIntial", @class = "text input_wrapper mi", @maxlength = "1" })%></div></div>
            <div class="row"><label for="Edit_Physician_LastName" class="float-left">Last Name:</label><div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Physician_LastName", @class = "text input_wrapper required", @maxlength = "50" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="Edit_Physician_Credentials" class="float-left">Credentials:</label><div class="float-right"><%= Html.TextBox("Credentials", Model.Credentials, new { @id = "Edit_Physician_Credentials", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="Edit_Physician_NpiNumber" class="float-left">NPI No:</label><div class="float-right">  <%= Html.TextBox("NPI", Model.NPI, new { @id = "Edit_Physician_NpiNumber", @class = "text input_wrapper digits", @maxlength = "10" })%></div></div>
            <div class="row">
                <label for="Edit_Physician_PecosCheck" class="float-left">PECOS Verification:</label>
                <div id="Edit_Physician_PecosCheck" class="float-right" style="width:200px;text-align:left"><%= Model.PecosImage %></div>
            </div>
        </div>
    </fieldset>   
    <fieldset>
        <legend>Physician Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Physician_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"> <%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Physician_AddressLine1", @class = "text input_wrapper required", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Physician_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"> <%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Physician_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Physician_AddressCity" class="float-left">City:</label><div class="float-right"> <%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Physician_AddressCity", @class = "text input_wrapper required", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Physician_AddressStateCode" class="float-left"> State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Physician_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Physician_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "9" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Physician_Phone1" class="float-left">Primary Phone:</label><div class="float-right"><%= Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length>=3 ? Model.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Physician_Phone1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length >= 6 ? Model.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Physician_Phone2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhoneWorkArray", Model.PhoneWork.IsNotNullOrEmpty() && Model.PhoneWork.Length>=10 ? Model.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Physician_Phone3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Physician_AltPhone1" class="float-left">Alternate Phone:</label><div class="float-right"><%= Html.TextBox("PhoneAltArray", Model.PhoneAlternate.IsNotNullOrEmpty() && Model.PhoneAlternate.Length >= 3 ? Model.PhoneAlternate.Substring(0, 3) : "", new { @id = "Edit_Physician_AltPhone1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhoneAltArray", Model.PhoneAlternate.IsNotNullOrEmpty() && Model.PhoneAlternate.Length >= 6 ? Model.PhoneAlternate.Substring(3, 3) : "", new { @id = "Edit_Physician_AltPhone2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhoneAltArray", Model.PhoneAlternate.IsNotNullOrEmpty() && Model.PhoneAlternate.Length >= 10 ? Model.PhoneAlternate.Substring(6, 4) : "", new { @id = "Edit_Physician_AltPhone3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Physician_Fax1" class="float-left">Fax Number:</label><div class="float-right"><%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length>=3 ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Physician_Fax1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty()&& Model.FaxNumber.Length>=6 ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Physician_Fax2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() && Model.FaxNumber.Length >= 10 ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Physician_Fax3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Physician_Email" class="float-left">Email:</label><div class="float-right">  <%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Physician_Email", @class = "text email input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="Edit_Physician_PhysicianAccess" class="float-left">Grant Physician Access:</label><div class="float-right" style="width: 200px; text-align: left;"><%= Html.CheckBox("PhysicianAccess", Model.PhysicianAccess, new { @id = "Edit_Physician_PhysicianAccess", @class = "bigradio" })  %><label for="Edit_Physician_PhysicianAccess">(E-mail Address Required)</label></div></div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadPhysicianLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editphysician');">Exit</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $('#Edit_Physician_PhysicianAccess').change(function() {
        if ($('#Edit_Physician_PhysicianAccess').attr('checked')) $("#Edit_Physician_Email").addClass("required");
        else $("#Edit_Physician_Email").removeClass("required")
    });
</script>
<% Html.Telerik().TabStrip().Name("editPhysicianTabStrip").Items(tabstrip =>
   {
       tabstrip.Add().Text("Licenses").HtmlAttributes(new { id = "editPhysicianLicenses_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() =>
       { %>
            <% Html.RenderPartial("Physician/Licenses", Model.Id); %>
        <% });%>
<% }).SelectedIndex(0).Render();%>
