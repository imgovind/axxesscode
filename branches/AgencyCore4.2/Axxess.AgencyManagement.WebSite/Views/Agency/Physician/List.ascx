﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<span class="wintitle">Physician List | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Physicians", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<AgencyPhysician>()
        .Name("List_Physician")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.NPI).Title("NPI").Sortable(true).Width(3);
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(true).Width(4);
            columns.Bound(p => p.EmailAddress).Title("Email").Sortable(true).Width(4);
            columns.Bound(p => p.AddressFull).Title("Address").Sortable(true).Width(5);
            columns.Bound(p => p.PhoneWorkFormatted).Title("Phone Number").Sortable(false).Width(3);
            columns.Bound(p => p.PhoneAlternateFormatted).Title("Alternate Number").Sortable(false).Width(3);
            columns.Bound(p => p.FaxNumberFormatted).Title("Fax Number").Sortable(false).Width(3);
            columns.Bound(p => p.PhysicianAccess).ClientTemplate("<#= PhysicianAccess ? 'Yes' : 'No' #>").Title("Physician Access").Sortable(false).Width(2);
            columns.Bound(p => p.IsPecosVerified).ClientTemplate("<#= IsPecosVerified ? 'Yes' : 'No' #>").Title("PECOS").Sortable(false).Width(2);
            columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPhysician('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Physician.Delete('<#=Id#>');\" class=\"\">Delete</a>").Sortable(false).Title("Action").Width(2).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Physician"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_Physician .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManagePhysicians) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Physician", Click: UserInterface.ShowNewPhysician } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>