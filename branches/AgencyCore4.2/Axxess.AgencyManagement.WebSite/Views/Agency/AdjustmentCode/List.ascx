﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Adjustment Codes | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("AdjustmentCodes", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<AgencyAdjustmentCode>()
        .Name("List_AdjustmentCode")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(t => t.Code).Sortable(true).Width(200);
            columns.Bound(t => t.Description).Sortable(true);
            columns.Bound(t => t.CreatedDateString).Title("Created").Sortable(true).Width(120);
            columns.Bound(t => t.ModifiedDateString).Title("Modified").Width(120);
            columns.Bound(t => t.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditAdjustmentCode('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"AdjustmentCode.Delete('<#=Id#>');\" class=\"deleteAdjustmentCode\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "AdjustmentCode"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_AdjustmentCode .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (!Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Adjustment Code", Click: UserInterface.ShowNewAdjustmentCode } ])
)<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>