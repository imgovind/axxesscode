﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div id="supplyTab" class="wrapper main">
<%  using (Html.BeginForm("SupplyVerify", "Billing", FormMethod.Post, new { @id = "billingSupply" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("episodeId",Model.EpisodeId) %>
    <%= Html.Hidden("patientId",Model.PatientId) %>
    <div class="billing">
        <h3 class="align-center">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy") %> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy") %></h3>
        <div class="row">
            <%= Html.CheckBox("IsSupplyNotBillable", Model.IsSupplyNotBillable, new { @id = "Final_Supply_IsSupplyNotBillable", @class = "radio float-left" }) %>
            <label for="Final_Supply_IsSupplyNotBillable">
                Check this box if you do not want to bill for supplies.
                <em>(This removes all supplies from the claim)</em>
            </Label>
        </div>
        <div id="FinalBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>().Name("FinalBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).ToolBar(commands => {commands.Insert().ButtonType(GridButtonType.Text);}).DataBinding(dataBinding => { dataBinding.Ajax()
                    .Select("FinalSupplyBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId })
                    .Insert("FinalSupplyBillableAdd", "Billing", new { Id = Model.Id, patientId = Model.PatientId })
                    .Update("FinalSupplyBillableUpdate", "Billing", new { Id = Model.Id, patientId = Model.PatientId, IsBillable = true });
                }).HtmlAttributes(new { style = "position:relative;" }).Columns(columns => {
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='UniqueIdentifier' type='checkbox'  value='<#= UniqueIdentifier #>' />").ReadOnly().Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(90);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("${0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("${0:#0.00}").Title("Total Cost").Width(70);
                    columns.Command(commands => { commands.Edit(); }).Width(70).Title("Action");
                }).Editable(editing => editing.Mode(GridEditMode.InLine)).ClientEvents(events => events
                    .OnEdit("Supply.OnSupplyEdit")
                    .OnRowSelect("Supply.RowSelected")
                ).Sortable().Selectable().Scrollable().Footer(false) %>
        </div>
        <div id="FinalUnBillingSupplyContent">
            <div class="supplyTitle">
                <h3>Non-Billable Supplies</h3>
            </div>
            <%= Html.Telerik().Grid<Supply>().Name("FinalUnBillingSupplyGrid").DataKeys(keys => { keys.Add(c => c.UniqueIdentifier).RouteKey("UniqueIdentifier"); }).HtmlAttributes(new { style = "position:relative;" }).ToolBar(commands => { commands.Custom(); }).Columns(columns => {
                    columns.Bound(s => s.UniqueIdentifier).ClientTemplate("<input name='UniqueIdentifier' type='checkbox'  value='<#= UniqueIdentifier #>' />").ReadOnly().Title("").Width(40);
                    columns.Bound(s => s.RevenueCode).Title("Revenue Code").Width(100);
                    columns.Bound(s => s.Description).Title("Description");
                    columns.Bound(s => s.Code).Title("HCPCS").Width(70);
                    columns.Bound(s => s.DateForEdit).Format("{0:MM/dd/yyyy}").Title("Date").Width(90);
                    columns.Bound(s => s.Quantity).Title("Unit").Width(50);
                    columns.Bound(s => s.UnitCost).Format("{0:#0.00}").Title("Unit Cost").Width(70);
                    columns.Bound(s => s.TotalCost).Format("{0:#0.00}").Title("Total Cost").Width(70);
                    columns.Command(commands => { commands.Edit(); }).Width(120).Title("Action");
                }).DataBinding(dataBinding =>{ dataBinding.Ajax()
                    .Select("FinalSupplyUnBillable", "Billing", new { Id = Model.Id, patientId = Model.PatientId })
                    .Update("FinalSupplyBillableUpdate", "Billing", new { Id = Model.Id, patientId = Model.PatientId, IsBillable = false });
                }).Editable(editing => editing.Mode(GridEditMode.InLine)).ClientEvents(events => events
                    .OnEdit("Supply.OnSupplyEdit")
                    .OnRowSelect("Supply.RowSelected")
                ).Sortable().Selectable().Scrollable().Footer(false)%>
        </div>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="Billing.NavigateBack(1);">Back</a></li>
            <li><a href="javascript:void(0)" onclick="$(this).closest('form').submit();">Verify and Next</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Billing.Navigate(3, '#billingSupply');
    $("#FinalBillingSupplyGrid .t-grid-toolbar a.t-grid-add").text("Add New Supply");
    $("#FinalBillingSupplyGrid .t-grid-toolbar").append(" <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Billing.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalBillingSupplyGrid'),false);\"> Mark As Non-Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Billing.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalBillingSupplyGrid'));\"> Delete </a> <lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#FinalUnBillingSupplyGrid .t-grid-toolbar").empty().append(" <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Billing.ChangeSupplyBillableStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalUnBillingSupplyGrid'),true);\"> Mark As Billable </a> <a href=\"javascript:void(0);\" class=\"t-button float-left\" onclick=\"Billing.ChangeSupplyStatus('<%=Model.Id%>','<%=Model.PatientId%>',$('#FinalUnBillingSupplyGrid'));\"> Delete </a><lable class=\"float-left\">Note: <em>Click on the checkbox(es) and make the appropriate selection.</em> </lable>")
    $("#billingSupply #FinalBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
    $("#billingSupply #FinalUnBillingSupplyGrid .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px' });
</script>