﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
    <div class="buttons">
        <ul>
            <%= string.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/FinalPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\">Print</a></li>", Model.EpisodeId, Model.PatientId)%>
        </ul>
    </div>
    <fieldset><%= Html.Hidden("Id", Model.Id)%>
        <div class="column">
            <div class="row"><div class="float-left">Patient First Name:</div><div class="float-right light"><%= Model.FirstName%></div></div>
            <div class="row"><div class="float-left">Patient Last Name:</div><div class="float-right light"><%= Model.LastName%></div></div>
            <div class="row"><div class="float-left">Medicare #:</div><div class="float-right light"><%= Model.MedicareNumber%></div></div>
            <div class="row"><div class="float-left">Patient Record #:</div><div class="float-right light"><%= Model.PatientIdNumber%></div></div>
            <div class="row"><div class="float-left">Gender:</div><div class="float-right light"><%= Model.Gender%></div></div>
            <div class="row"><div class="float-left">Date of Birth:</div><div class="float-right light"><%= Model.DOB != null ? Model.DOB.ToShortDateString() : string.Empty %></div></div>
        </div>
        <div class="column">
            <div class="row"><div class="float-left">Episode Start Date:</div><div class="float-right light"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float-left">Admission Date:</div><div class="float-right light"><%= Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float-left">Address Line 1:</div><div class="float-right light"><%= Model.AddressLine1%></div></div>
            <div class="row"><div class="float-left">Address Line 2:</div><div class="float-right light"><%= Model.AddressLine2%></div></div>
            <div class="row"><div class="float-left">City:</div><div class="float-right light"><%= Model.AddressCity%></div></div>
            <div class="row"><div class="float-left">State, Zip Code:</div><div class="float-right light"><%= Model.AddressStateCode + Model.AddressZipCode%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column"><% var total = 0.0; %>
            <div class="row"><div class="float-left">HIPPS Code:</div><div class="float-right light"><%= Model.HippsCode%></div></div>
            <div class="row"><div class="float-left">OASIS Matching Key:</div><div class="float-right light"><%= Model.ClaimKey%></div></div>
            <div class="row"><div class="float-left">Date Of First Billable Visit:</div><div class="float-right light"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float-left">Physician Last Name, F.I.:</div><div class="float-right light"><%= (Model.PhysicianLastName )+" "+ ( Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.Substring(0, 1)+"." : "")%></div></div>
            <div class="row"><div class="float-left">Physician NPI #:</div><div class="float-right light"><%= Model.PhysicianNPI%></div></div>
            <div class="row"><div>Remark:</div><div class="margin light"><p><%= Model.Remark%></p></div></div>
        </div>
        <div class="column">
            <div class="row"><% var diagnosis = XElement.Parse(Model.DiagnosisCode); var val = (diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : ""); %>
                <div>Diagonsis Codes:</div>
                <div class="margin">
                    <div class="float-left">Primary</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Second</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Third</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Fourth</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Fifth</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float-left">Sixth</div><div class="float-right light"><%= diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="billing">
        <ul>
            <li>
                <span class="rap-checkbox"></span>
                <span class="sumdesc">Description</span>
                <span class="visittype">HCPCS/HIPPS Code</span>
                <span class="visitdate">Service Date</span>
                <span class="icon-column">Service Unit</span>
                <span class="icon-column">Total Charges</span>
            </li>
        </ul><ol>
            <li class="first <%= Model.IsSupplyNotBillable ? "odd" : "even" %>">
                    <span class="rap-checkbox">0023</span>
                    <span class="sumdesc">Home Health Services</span>
                    <span class="visittype"><%= Model.HippsCode %></span>
                    <span class="visitdate"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="icon-column"></span>
                    <span class="icon-column"></span>
            </li>
            <% var i = 0; %>
            <% if (Model.IsSupplyNotBillable) { %>
            <% } else { %>
            <% var supplies = Model.Supply.IsNotNullOrEmpty() ? Model.Supply.ToObject<List<Supply>>() : new List<Supply>();
               var serviceSupplies = supplies.Where(s => s.RevenueCode == "0270" && s.IsBillable && s.IsDeprecated == false);
               var woundSupplies = supplies.Where(s => s.RevenueCode == "0623" && s.IsBillable && s.IsDeprecated == false);
               var woundTotal = woundSupplies.Sum(s => s.TotalCost);
               var medicalSupplyTotal = serviceSupplies.Sum(s => s.TotalCost);
               total+= medicalSupplyTotal > 0 ? medicalSupplyTotal + woundTotal : Model.SupplyTotal + woundTotal;
            %>
            <li class="odd">
                    <span class="rap-checkbox"><%= medicalSupplyTotal > 0 ? "0270" : "0272" %></span>
                    <span class="sumdesc">Service Supplies</span>
                    <span class="visittype"></span>
                    <span class="visitdate"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="icon-column"></span>
                    <span class="icon-column"><%= string.Format("${0:#0.00}", medicalSupplyTotal > 0 ? medicalSupplyTotal : Model.SupplyTotal) %></span>
            </li>
                <% if (woundTotal > 0) { 
                    i = 1; %>
                <li class="even">
                    <span class="rap-checkbox">0623</span>
                    <span class="sumdesc">Wound Supplies</span>
                    <span class="visittype"></span>
                    <span class="visitdate"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToString("MM/dd/yyy") : string.Empty%></span>
                    <span class="icon-column"></span>
                    <span class="icon-column"><%= string.Format("${0:#0.00}", woundTotal)%></span>
                </li>
                <% } %>
            <% } %>
            
             <% if(Model != null && Model.BillVisitSummaryDatas != null && Model.BillVisitSummaryDatas.Count > 0) {
                  foreach (var visit in Model.BillVisitSummaryDatas) { %>
                    <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
                        <span class="rap-checkbox"><% =visit.RevenueCode%></span>
                        <span class="sumdesc">Visit</span>
                        <span class="visittype"><% =visit.HCPCSCode%></span>
                        <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : "")%></span>
                        <span class="icon-column"><% = visit.Unit%> </span>
                        <span class="icon-column"><% = string.Format("${0:#0.00}", visit.Charge)%></span>
                    </li><% i++;total +=  visit.Charge; %>
                <% } 
            }%>
        </ol>
        <ul class="total"><li class="align-right"><label for="Total">Total:</label> <label> <%= string.Format("${0:#0.00}", total)%></label></li></ul>
    </div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.NavigateBack(2);">Back</a></li>
            <%= string.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/FinalPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\">Print</a></li>", Model.EpisodeId, Model.PatientId)%>
            <li><a href="javascript:void(0);" onclick="Billing.FinalComplete('<%=Model.Id%>');">Complete</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript"> $("#final-tabs-4 ol li:last").addClass("last"); </script>