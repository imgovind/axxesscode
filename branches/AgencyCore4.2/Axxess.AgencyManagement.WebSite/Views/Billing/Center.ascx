﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle"><%= Model.ClaimType.GetDescription() %>s | <%= Current.AgencyName %></span>
<% var titleCaseType = Model.ClaimType.ToString().ToTitleCase(); %>
<div id="Billing_<%=titleCaseType%>CenterContent" class="main wrapper">
    <div class="trical">
         <span class="strong">Branch:&nbsp;</span>
         <%= Html.Hidden("Type", Model.ClaimType.ToString(), new { @id = "Billing_"+titleCaseType+ "Center_Type" })%>
         <%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "Billing_"+titleCaseType+"Center_BranchCode", @class = "report_input valid" })%>
         <span class="strong">Insurance:&nbsp;</span>
         <span><%= Html.InsurancesMedicare("PrimaryInsurance", Model.Insurance.ToString(), true, "Unassigned Insurance", new { @id = "Billing_" + titleCaseType + "Center_InsuranceId", @class = "Insurances requireddropdown" })%></span>
         <div class="buttons editeps">
            <ul>
                <li><a href="javascript:void(0);" onclick="Billing.ReLoadUnProcessedClaim('#Billing_CenterContent<%=titleCaseType%>',$('#Billing_<%=titleCaseType%>Center_BranchCode').val(), $('#Billing_<%=titleCaseType%>Center_InsuranceId').val(),'<%=titleCaseType%>Grid','<%=titleCaseType%>');">Refresh</a></li>
                <li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdf', { 'branchId': $('#Billing_<%=titleCaseType%>Center_BranchCode').val(), 'insuranceid':$('#Billing_<%=titleCaseType%>Center_InsuranceId').val(), 'parentSortType': 'branch','columnSortType':'', 'claimType': '<%=titleCaseType%>' });" >Print</a></li>
                <li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsXls', { 'branchId': $('#Billing_<%=titleCaseType%>Center_BranchCode').val(), 'insuranceid':$('#Billing_<%=titleCaseType%>Center_InsuranceId').val(), 'parentSortType': 'branch','columnSortType':'', 'claimType': '<%=titleCaseType%>' });" >Export to Excel</a></li>
            </ul>
        </div>
    </div>
    <div id="Billing_CenterContent<%=titleCaseType%>" style="min-height:200px;"><% Html.RenderPartial(titleCaseType+"Grid",  Model); %></div>
</div>
<script type="text/javascript">
    $("#Billing_<%=titleCaseType%>Center_BranchCode").change(function() {Insurance.loadMedicareWithHMOInsuarnceDropDown("Billing_<%=titleCaseType%>Center", "Unassigned Insurance", true, function() { eval("Billing.ReLoadUnProcessedClaim('#Billing_CenterContent<%=titleCaseType%>','" + $('#Billing_<%=titleCaseType%>Center_BranchCode').val() + "'," + $('#Billing_<%=titleCaseType%>Center_InsuranceId').val() + ",'<%=titleCaseType%>Grid','<%=titleCaseType%>')"); });});
    $("#Billing_<%=titleCaseType%>Center_InsuranceId").change(function() {eval("Billing.ReLoadUnProcessedClaim('#Billing_CenterContent<%=titleCaseType%>','" + $('#Billing_<%=titleCaseType%>Center_BranchCode').val() + "'," + $(this).val() + ",'<%=titleCaseType%>Grid','<%=titleCaseType%>')");});
</script>