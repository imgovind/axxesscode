﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<InsuranceAuthorizationViewData>" %>
<div class="row"><label for="HealthPlanId" class="float-left">Health Plan Id:</label><div class="float-right"><%= Html.TextBox("HealthPlanId", Model.HealthPlanId, new { @class = "" })%></div></div>
<div class="row"><label for="GroupName" class="float-left">Group Name:</label><div class="float-right"><%= Html.TextBox("GroupName", Model.GroupName, new { @class = "" })%></div></div>
<div class="row"><label for="GroupId" class="float-left">Group Id:</label><div class="float-right"><%= Html.TextBox("GroupId", Model.GroupId, new { @class = "" })%></div></div>
<div class="row" ><label for="Relationship" class="float-left">Relationship to Patient:</label><div class="float-right"><%= Html.InsuranceRelationships("Relationship", Model.Relationship, new { @class = "" })%></div></div>
<div class="row"><label for="Authorization" class="float-left">Authorizations:</label><div class="float-right"> <%= Html.DropDownList("Authorization", Model.Authorizations != null && Model.Authorizations.Count > 0 ? Model.Authorizations : new List<SelectListItem>(), new { @id = Model.ClaimTypeIdentifier+ "_Authorization" })%></div>
    <div id="<%=Model.ClaimTypeIdentifier %>_AuthorizationContent" class="margin float-right">
         <% Html.RenderPartial("~/Views/Billing/AuthorizationContent.ascx", Model.Authorization !=null ? Model.Authorization : new Authorization()); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%=Model.ClaimTypeIdentifier %>_Authorization").change(function() {
        Billing.loadAuthorizationContent("#<%=Model.ClaimTypeIdentifier %>_AuthorizationContent", $(this).val())
    })
</script>