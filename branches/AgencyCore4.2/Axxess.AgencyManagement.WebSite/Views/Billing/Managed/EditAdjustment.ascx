﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimAdjustment>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("UpdateManagedClaimAdjustmentDetails", "Billing", FormMethod.Post, new { @id = "updateManagedClaimAdjustmentForm" })) { %>
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("ClaimId", Model.ClaimId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <fieldset>
        <legend>Update Adjustment</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Adjustment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("Adjustment", String.Format("{0:0.00}",Model.Adjustment), new {@class = "text input_wrapper required currency", @maxlength = "" }) %></div>
            </div>
            <div class="row">
                <label class="float-left">Type:</label>
                <div class="float-right"><%= Html.AdjustmentCodes("TypeId", Model.TypeId.ToString(), "-- Select Code --", new { @class = "requireddropdown" }) %></div>
            </div>
             <div class="row">
                <label class="float-left">Comment:</label>
                <div class=""><%= Html.TextArea("Comments", Model.Comments) %></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update Adjustment</a></li>
            <li><a href="javascript:void(0);" class="close">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>