﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
 <% Html.Telerik().Grid<ManagedClaimAdjustment>().DataKeys(keys => { keys.Add(c => c.Id).RouteKey("Id"); })
        .Name("ManagedBillingAdjustmentsGrid").Columns(columns =>
    {
        columns.Bound(p => p.Type);
        columns.Bound(p => p.Adjustment).Format("{0:C}").Width(200);
        columns.Bound(p => p.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"> </a>");
        columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowModalUpdateAdjustmentManagedClaim('<#=Id#>');\">Update</a> | <a href=\"javascript:void(0);\" onclick=\"ManagedBilling.DeleteAdjustment('<#=PatientId#>','<#=ClaimId#>','<#=Id#>');\">Delete</a>").Title("Actions").Width(200).Visible(!Current.IsAgencyFrozen);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimAdjustmentsGrid", "Billing", new { claimId = Model }))
    .ClientEvents(evnts => evnts.OnRowDataBound("ManagedBilling.OnRowWithCommentsDataBound"))
    .Sortable().Selectable().Scrollable().Footer(false).Render(); %>
<script type="text/javascript"> $("#ManagedBillingAdjustmentsGrid .t-grid-content").css({ 'height': 'auto' }); </script>

