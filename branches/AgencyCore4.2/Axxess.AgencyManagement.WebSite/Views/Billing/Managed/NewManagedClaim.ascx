﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl <NewManagedClaimViewData>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("CreateManagedClaim", "Billing", FormMethod.Post, new { @id = "createManagedClaimForm" })) { %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <fieldset>
        <legend>Claim Information</legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Insurances:</label>
                <div class="float-right"><%= Html.DropDownList("InsuranceId", Model != null && Model.Insurances != null ? Model.Insurances : new List<SelectListItem>(), new { @class= "requireddropdown" })%></div>
            </div>
            <div ="row"></div>
            <div class="row">
                <label class="float-left">Date Range:</label>
                <div class="float-right">
                    <input type="text" class="date-picker shortdate required" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="StartDate" />
                    To
                    <input type="text" class="date-picker shortdate required" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="EndDate" />
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Add Claim</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>