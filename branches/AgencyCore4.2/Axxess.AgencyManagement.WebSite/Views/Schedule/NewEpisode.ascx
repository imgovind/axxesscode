﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<% using (Html.BeginForm("AddEpisode", "Schedule", FormMethod.Post, new { @id = "topMenuNewEpisodeForm" })){%>
<div class="wrapper main">
    <span class="bigtext align-center">New Episode</span>
    <fieldset><legend>Patient</legend><div class="column"><div class="row"><label for="TopMenuNew_Episode_PatientId" class="float-left">Patient:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", Guid.Empty.ToString(), new { @id = "TopMenuNew_Episode_PatientId", @class = "requireddropdown" })%></div></div></div></fieldset>
    <div id="topMenuNewEpisodeContent"><%Html.RenderPartial("NewEpisodeContent", Model); %></div>
    <div class="clear"></div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="">Save</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<%} %>
