﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericGU = data.AnswerArray("GenericGU"); %>
<%= Html.Hidden(Model.Type + "_GenericGU", string.Empty, new { @id = Model.Type + "_GenericGUHidden" })%>
<ul class="checkgroup bold">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU1' name='{0}_GenericGU' value='1' type='checkbox' {1} />", Model.Type, genericGU.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU1">WNL (Within Normal Limits)</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU2' name='{0}_GenericGU' value='2' type='checkbox' {1} />", Model.Type, genericGU.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU2">Incontinence</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU3' name='{0}_GenericGU' value='3' type='checkbox' {1} />", Model.Type, genericGU.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU3">Bladder Distention</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU4' name='{0}_GenericGU' value='4' type='checkbox' {1} />", Model.Type, genericGU.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU4">Discharge</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU5' name='{0}_GenericGU' value='5' type='checkbox' {1} />", Model.Type, genericGU.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU5">Frequency</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU6' name='{0}_GenericGU' value='6' type='checkbox' {1} />", Model.Type, genericGU.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU6">Dysuria</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU7' name='{0}_GenericGU' value='7' type='checkbox' {1} />", Model.Type, genericGU.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU7">Retention</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU8' name='{0}_GenericGU' value='8' type='checkbox' {1} />", Model.Type, genericGU.Contains("8").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU8">Urgency</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU9' name='{0}_GenericGU' value='9' type='checkbox' {1} />", Model.Type, genericGU.Contains("9").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU9">Oliguria</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU10' name='{0}_GenericGU' value='10' type='checkbox' {1} />", Model.Type, genericGU.Contains("10").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU10">Catheter/Device:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericGU10More">
            <%  var genericGUCatheterList = new SelectList(new[] {
                new SelectListItem { Text = "", Value = "" },
                new SelectListItem { Text = "N/A", Value = "N/A" },
                new SelectListItem { Text = "Foley Catheter ", Value = "Foley Catheter" },
                new SelectListItem { Text = "Condom Catheter", Value = "Condom Catheter" },
                new SelectListItem { Text = "Suprapubic Catheter", Value = "Suprapubic Catheter" },
                new SelectListItem { Text = "Urostomy", Value = "Urostomy" },
                new SelectListItem { Text = "Other", Value = "Other" }
            }, "Value", "Text", data.AnswerOrDefault("GenericGUCatheterList", "0")); %>
            <%= Html.DropDownList(Model.Type + "_GenericGUCatheterList", genericGUCatheterList, new { @class = "oe" }) %>
            <div class="clear"></div>
            <label for="<%= Model.Type %>_GenericGUCatheterLastChanged">Last Changed</label>
            <div>
                <%= Html.TextBox(Model.Type + "_GenericGUCatheterLastChanged", data.AnswerOrEmptyString("GenericGUCatheterLastChanged"), new { @id = Model.Type + "_GenericGUCatheterLastChanged", @class = "oe", @maxlength = "10" }) %>
                <%= Html.TextBox(Model.Type + "_GenericGUCatheterFrequency", data.AnswerOrEmptyString("GenericGUCatheterFrequency"), new { @id = Model.Type + "_GenericGUCatheterFrequency", @class = "vitals", @maxlength = "5" }) %>
                <label for="<%= Model.Type %>_GenericGUCatheterFrequency">Fr</label>
                <%= Html.TextBox(Model.Type + "_GenericGUCatheterAmount", data.AnswerOrEmptyString("GenericGUCatheterAmount"), new { @id = Model.Type + "_GenericGUCatheterAmount", @class = "vitals", @maxlength = "5" }) %>
                <label for="<%= Model.Type %>_GenericGUCatheterAmount" class="inline-radio">ml</label>
            </div>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericGU11' name='{0}_GenericGU' value='11' type='checkbox' {1} />", Model.Type, genericGU.Contains("11").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericGU11">Urine:</label>
        </div>
        <div class="extra" id="<%= Model.Type %>_GenericGU11More">
            <%  string[] genericGUUrine = data.AnswerArray("GenericGUUrine"); %>
            <%= Html.Hidden(Model.Type + "_GenericGUUrine", string.Empty, new { @id = Model.Type + "_GenericGUUrineHidden" })%>
            <ul class="checkgroup twocol">
                <li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericGUUrine6' name='{0}_GenericGUUrine' value='6' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("6").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine6">Clear</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericGUUrine1' name='{0}_GenericGUUrine' value='1' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine1">Cloudy</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericGUUrine2' name='{0}_GenericGUUrine' value='2' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine2">Odorous</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericGUUrine3' name='{0}_GenericGUUrine' value='3' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine3">Sediment</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericGUUrine4' name='{0}_GenericGUUrine' value='4' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine4">Hematuria</label>
                    </div>
                </li><li>
                    <div class="option">
                        <%= string.Format("<input id='{0}_GenericGUUrine5' name='{0}_GenericGUUrine' value='5' type='checkbox' {1} />", Model.Type, genericGUUrine.Contains("5").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericGUUrine5">Other</label>
                    </div>
                    <div class="extra" id="<%= Model.Type %>_GenericGUUrine5More">
                        <%= Html.TextBox(Model.Type + "_GenericGUOtherText", data.AnswerOrEmptyString("GenericGUOtherText"), new { @id = Model.Type + "_GenericGUOtherText", @maxlength = "20", @class = "oe" }) %>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericGenitourinaryComment" class="strong">Comments:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericGenitourinaryComment", data.AnswerOrEmptyString("GenericGenitourinaryComment"), new { @class = "fill", @id = Model.Type + "_GenericGenitourinaryComment" })%></div>
