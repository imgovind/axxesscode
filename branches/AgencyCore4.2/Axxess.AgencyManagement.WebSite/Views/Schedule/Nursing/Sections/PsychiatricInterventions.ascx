﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] interventions = data.AnswerArray("GenericPsychiatricInterventions"); %>
<%= Html.Hidden(Model.Type + "_GenericPsychiatricInterventions", string.Empty, new { @id = Model.Type + "_GenericPsychiatricInterventionsHidden" })%>
<table class="fixed"><tbody>
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions1' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='1' type='checkbox' {1} />", Model.Type, interventions.Contains("1").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions1" class="radio">Suicidal and safety precautions</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions2' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='2' type='checkbox' {1} />", Model.Type, interventions.Contains("2").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions2" class="radio">Relaxation, imagery and deep breathing exercises</label></div></td>
    </tr>    
    <tr>   
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions3' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='3' type='checkbox' {1} />", Model.Type, interventions.Contains("3").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions3" class="radio">Problem solving, positive coping, decision making and stress management technique</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions4' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='4' type='checkbox' {1} />", Model.Type, interventions.Contains("4").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions4" class="radio">Recognition of s/sx complications of crisis and when to call MD</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions5' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='5' type='checkbox' {1} />", Model.Type, interventions.Contains("5").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions5" class="radio">Reality/congruent thinking techniques</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions6' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='6' type='checkbox' {1} />", Model.Type, interventions.Contains("6").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions6" class="radio">Anger management</label></div></td>
    </tr> 
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions7' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='7' type='checkbox' {1} />", Model.Type, interventions.Contains("7").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions7" class="radio">Emergency and crisis intervention</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions8' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='8' type='checkbox' {1} />", Model.Type, interventions.Contains("8").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions8" class="radio">Recognition of thoughts and verbally express painful ones</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions9' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='9' type='checkbox' {1} />", Model.Type, interventions.Contains("9").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions9" class="radio">Recognition of cardiovascular and neurological side effects of medication</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions10' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='10' type='checkbox' {1} />", Model.Type, interventions.Contains("10").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions10" class="radio">Ability to focus thoughts on feelings and verbally express painful ones</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions11' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='11' type='checkbox' {1} />", Model.Type, interventions.Contains("11").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions11" class="radio">Positive feedback to reality and realistic feelings</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions12' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='12' type='checkbox' {1} />", Model.Type, interventions.Contains("12").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions12" class="radio">Importance of supportive thearpy, reality testing, and positive feedback, validation and confrontation</label></div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions13' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='13' type='checkbox' {1} />", Model.Type, interventions.Contains("13").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions13" class="radio">Relaxation and stress management techniques</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions14' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='14' type='checkbox' {1} />", Model.Type, interventions.Contains("14").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions14" class="radio">Relationship between feelings and behavior, impulse control behaviors</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions15' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='15' type='checkbox' {1} />", Model.Type, interventions.Contains("15").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions15" class="radio">Entry back into community and importance of interacting with others in the environment</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions16' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='16' type='checkbox' {1} />", Model.Type, interventions.Contains("16").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions16" class="radio">Grieving process and bereavement counseling</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions17' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='17' type='checkbox' {1} />", Model.Type, interventions.Contains("17").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions17" class="radio">Calming techniques for agitation</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions18' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='18' type='checkbox' {1} />", Model.Type, interventions.Contains("18").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions18" class="radio">Instructs time planning skills to prevent being overwhelmed</label></div></td>
    </tr> 
    <tr>  
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions19' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='19' type='checkbox' {1} />", Model.Type, interventions.Contains("19").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions19" class="radio">Instruct recognition of exacerbation of illness, hallucinations, and delusions, inappropriate thought patterns and disorganization</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions20' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='20' type='checkbox' {1} />", Model.Type, interventions.Contains("20").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions20" class="float-left inline-radio light">Instruct exploration of painful or anxious feelings and/or identifying ambivalent feelings</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions21' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='21' type='checkbox' {1} />", Model.Type, interventions.Contains("21").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions21" class="float-left inline-radio light">Instruct importance of providing positive reinforcement for positive actions</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions22' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='22' type='checkbox' {1} />", Model.Type, interventions.Contains("22").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions22" class="float-left inline-radio light">Instruct need for concrete realites and focus on thoughts</label></div></td>
    </tr>    
    <tr>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions23' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='23' type='checkbox' {1} />", Model.Type, interventions.Contains("23").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions23" class="float-left inline-radio light">Instruct need for supportive psychotherapist</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions25' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='24' type='checkbox' {1} />", Model.Type, interventions.Contains("24").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions25" class="float-left inline-radio light">Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria's, grandiosity, depression and/or despondence</label></div></td>
    </tr>
    <tr> 
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions26' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='25' type='checkbox' {1} />", Model.Type, interventions.Contains("25").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions26" class="float-left inline-radio light">Instruct in s/sx of lithium toxicity</label></div></td>
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions27' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='26' type='checkbox' {1} />", Model.Type, interventions.Contains("26").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions27" class="float-left inline-radio light">Instruct positive coping skills to deal with disease and symptoms</label></div></td>
    </tr>
    <tr> 
        <td colspan="2"><div class="align-left"><%= string.Format("<input id='{0}_GenericPsychiatricInterventions28' class='float-left radio' name='{0}_GenericPsychiatricInterventions' value='27' type='checkbox' {1} />", Model.Type, interventions.Contains("27").ToChecked()) %><label for="<%= Model.Type %>_GenericPsychiatricInterventions28" class="float-left inline-radio light">Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria's, grandiosity, depression and/or despondence</label></div></td>
    </tr>
    <tr>
        <td colspan="4">
            <label for="<%= Model.Type %>_GenericPsychiatricInterventionsComment" class="strong">Comments:</label>
            <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericPsychiatricInterventionsComment", data.AnswerOrEmptyString("GenericPsychiatricInterventionsComment"), new { @class = "fill", @id = Model.Type + "_GenericPsychiatricInterventionsComment" })%></div>
        </td>
    </tr>
</tbody>
</table>
