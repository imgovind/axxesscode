﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_SensoriumContainer">
    <label class="bold">Orientation Impaired</label>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumOrientation1">Time:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumOrientation1", data.AnswerOrEmptyString("GenericSensoriumOrientation1"), new { @id = Model.Type + "_GenericSensoriumOrientation1", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumOrientation2">Place:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumOrientation2", data.AnswerOrEmptyString("GenericSensoriumOrientation2"), new { @id = Model.Type + "_GenericSensoriumOrientation2", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumOrientation3">Person:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumOrientation3", data.AnswerOrEmptyString("GenericSensoriumOrientation3"), new { @id = Model.Type + "_GenericSensoriumOrientation3", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <label class="bold">Memory</label>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumMemory1">Clouding of Consiousness:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory1", data.AnswerOrEmptyString("GenericSensoriumMemory1"), new { @id = Model.Type + "_GenericSensoriumMemory1", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumMemory2">Inablity to Concentrate:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory2", data.AnswerOrEmptyString("GenericSensoriumMemory2"), new { @id = Model.Type + "_GenericSensoriumMemory2", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumMemory3">Amnesia:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory3", data.AnswerOrEmptyString("GenericSensoriumMemory3"), new { @id = Model.Type + "_GenericSensoriumMemory3", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumMemory4">Poor Recent Memory:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory4", data.AnswerOrEmptyString("GenericSensoriumMemory4"), new { @id = Model.Type + "_GenericSensoriumMemory4", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumMemory5">Poor Remote Memory:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory5", data.AnswerOrEmptyString("GenericSensoriumMemory5"), new { @id = Model.Type + "_GenericSensoriumMemory5", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <div>
        <label class="float-left normal" for="<%= Model.Type %>_GenericSensoriumMemory6">Confabulation:</label>
        <%= Html.GeneralStatusDropDown(Model.Type + "_GenericSensoriumMemory6", data.AnswerOrEmptyString("GenericSensoriumMemory6"), new { @id = Model.Type + "_GenericSensoriumMemory6", @class = "float-right sensory" })%>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericSensoriumComment" class="strong">Comment:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericSensoriumComment", data.AnswerOrEmptyString("GenericSensoriumComment"), new { @id = Model.Type + "_GenericSensoriumComment", @class = "fill" })%></div>
</div>