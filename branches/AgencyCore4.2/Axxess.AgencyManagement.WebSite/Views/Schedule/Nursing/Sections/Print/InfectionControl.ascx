﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] infectionControl = data.AnswerArray("InfectionControl"); %>
printview.checkbox("Universal Precautions Observed",<%= infectionControl.Contains("1").ToString().ToLower() %>) +
printview.checkbox("Sharps/Waste Disposal",<%= infectionControl.Contains("2").ToString().ToLower() %>) +
printview.col(2,
    printview.span("New Infection",true) +
    printview.col(2,
        printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericIsNewInfection").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericIsNewInfection").Equals("0").ToString().ToLower()%>))) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericInfectionControlComment").Clean() %>"),
"Infection Control"