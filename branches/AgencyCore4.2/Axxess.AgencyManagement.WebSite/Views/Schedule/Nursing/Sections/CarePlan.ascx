<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
<div class="buttons">
    <ul>
        <li><%= Model.CarePlanOrEvalUrl %></li>
    </ul>
</div><%
} %>
<label for="GenericCarePlan" class="float-left">Care Plan:</label>
<div class="float-right">
    <%  var carePlan = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "No changes at this time", Value = "No changes at this time" },
        new SelectListItem { Text = "Reviewed w/Pt or CG", Value = "Reviewed w/Pt or CG" },
        new SelectListItem { Text = "Revised w/Pt or CG", Value = "Revised w/Pt or CG" },
        new SelectListItem { Text = "Other", Value = "Other" }
    }, "Value", "Text", data.AnswerOrDefault("GenericCarePlan", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericCarePlan", carePlan, new { @id = Model.Type + "_GenericCarePlan", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label for="GenericCarePlanProgressTowardsGoals" class="float-left">Progress towards goals:</label>
<div class="float-right">
    <%  var progressTowardsGoals = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Good", Value = "Good" },
        new SelectListItem { Text = "Fair", Value = "Fair" },
        new SelectListItem { Text = "Poor", Value = "Poor" }
    }, "Value", "Text", data.AnswerOrDefault("GenericCarePlanProgressTowardsGoals", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericCarePlanProgressTowardsGoals", progressTowardsGoals, new { @id = Model.Type + "_GenericCarePlanProgressTowardsGoals", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label class="float-left">New/Changed Orders or Updates to Care Plan:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericCarePlanIsChanged", "1", data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("1"), new { @id = Model.Type + "_GenericCarePlanIsChanged1" }) %>
                <label for="<%= Model.Type %>_GenericCarePlanIsChanged1">Yes</label>
            </div>
        </li><li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericCarePlanIsChanged", "0", data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("0"), new { @id = Model.Type + "_GenericCarePlanIsChanged0" }) %>
                <label for="<%= Model.Type %>_GenericCarePlanIsChanged0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<%  if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
<div class="buttons">
    <ul>
        <li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%= Model.PatientId %>');" status="Add New Order">Add New Order</a></li>
    </ul>
</div>
<div class="clear"></div>
<%  } %>
<label for="<%= Model.Type %>_GenericCarePlanComment" class="strong">Comments:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericCarePlanComment", data.AnswerOrEmptyString("GenericCarePlanComment"), new { @class = "fill", @id = Model.Type + "_GenericCarePlanComment" })%></div>
<%  if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
<div class="buttons">
    <ul>
        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Patient.LoadNewIncidentReport('{0}');\" title=\"New Incident Log\">Add Incident Log</a>", Model.PatientId)%></li>
    </ul>
</div>
<% } %>