﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="buttons">
    <ul>
        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"woundCare.Load('{0}','{1}','{2}');\" title=\"{3} Wound Care Flowsheet\">{3} Wound Care Flowsheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId, Model.IsWoundCareExist ? "Edit" : "Add") %></li>
    </ul>
</div>
<label class="strong">Color:</label>
<%  string[] skinColor = data.AnswerArray("GenericSkinColor"); %>
<%= Html.Hidden(Model.Type + "_GenericSkinColor", string.Empty, new { @id = Model.Type + "_GenericSkinColorHidden" }) %>
<ul class="checkgroup twocol">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinColor1' name='{0}_GenericSkinColor' value='1' type='checkbox' {1} />", Model.Type, skinColor.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinColor1">Pink/WNL (Within Normal Limits)</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinColor2' name='{0}_GenericSkinColor' value='2' type='checkbox' {1} />", Model.Type, skinColor.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinColor2">Pallor</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinColor3' name='{0}_GenericSkinColor' value='3' type='checkbox' {1} />", Model.Type, skinColor.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinColor3">Jaundice</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinColor4' name='{0}_GenericSkinColor' value='4' type='checkbox' {1} />", Model.Type, skinColor.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinColor4">Cyanotic</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinColor5' name='{0}_GenericSkinColor' value='5' type='checkbox' {1} />", Model.Type, skinColor.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinColor5">Other</label>
        </div>
        <div class="extra">
            <%= Html.TextBox(Model.Type + "_GenericSkinColorOther", data.AnswerOrEmptyString("GenericSkinColorOther"), new { @id = Model.Type + "_GenericSkinColorOther", @class = "oe" }) %>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericSkinTemp" class="float-left">Temp:</label>
<div class="float-right">
    <%  var skinTemp = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Warm", Value = "Warm" },
            new SelectListItem { Text = "Cool", Value = "Cool" },
            new SelectListItem { Text = "Clammy", Value = "Clammy" },
            new SelectListItem { Text = "Other", Value = "Other" }
        }, "Value", "Text", data.AnswerOrDefault("GenericSkinTemp", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericSkinTemp", skinTemp, new { @id = Model.Type + "_GenericSkinTemp", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericSkinTurgor" class="float-left">Turgor:</label>
<div class="float-right">
    <%  var skinTurgor = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Good/WNL (Within Normal Limits)", Value = "Good" },
            new SelectListItem { Text = "Fair", Value = "Fair" },
            new SelectListItem { Text = "Poor", Value = "Poor" }
        }, "Value", "Text", data.AnswerOrDefault("GenericSkinTurgor", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericSkinTurgor", skinTurgor, new { @id = Model.Type + "_GenericSkinTurgor", @class = "oe" }) %>
</div>
<div class="clear"></div>
<label class="strong">Condition:</label>
<% string[] skinCondition = data.AnswerArray("GenericSkinCondition"); %>
<%= Html.Hidden(Model.Type + "_GenericSkinCondition", string.Empty, new { @id = Model.Type + "_GenericSkinConditionHidden" }) %>
<ul class="checkgroup twocol">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition1' name='{0}_GenericSkinCondition' value='1' type='checkbox' {1} />", Model.Type, skinCondition.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition1">Dry</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition2' name='{0}_GenericSkinCondition' value='2' type='checkbox' {1} />", Model.Type, skinCondition.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition2">Diaphoretic</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition3' name='{0}_GenericSkinCondition' value='3' type='checkbox' {1} />", Model.Type, skinCondition.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition3">Wound</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition4' name='{0}_GenericSkinCondition' value='4' type='checkbox' {1} />", Model.Type, skinCondition.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition4">Ulcer</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition5' name='{0}_GenericSkinCondition' value='5' type='checkbox' {1} />", Model.Type, skinCondition.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition5">Incision</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition6' name='{0}_GenericSkinCondition' value='6' type='checkbox' {1} />", Model.Type, skinCondition.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition6">Rash</label>
        </div>
    </li>
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericSkinCondition7' name='{0}_GenericSkinCondition' value='7' type='checkbox' {1} />", Model.Type, skinCondition.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericSkinCondition7">Other</label>
        </div>
        <div class="extra">
            <%= Html.TextBox(Model.Type + "_GenericSkinConditionOther", data.AnswerOrEmptyString("GenericSkinConditionOther"), new { @id = Model.Type + "_GenericSkinConditionOther", @class = "oe" }) %>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericSkinComment" class="strong">Comment:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericSkinComment", data.AnswerOrEmptyString("GenericSkinComment"), new { @id = Model.Type + "_GenericSkinComment", @class = "fill" })%></div>
