﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>LVN Supervisory Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body>
<%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.7.1.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3E" +
            "Diabetic Daily Visit" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3E" +
            "Patient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan class=%22strong%22%3E" +
            "MR:" + "%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan class=%22strong%22%3E" +
            "Visit Date:" + "%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") && data["VisitDate"].Answer.IsNotNullOrEmpty() ? data["VisitDate"].Answer.ToDateTime().ToString("MM/dd/yyy").Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan class=%22strong%22%3E" +
            "Time In:" + "%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan class=%22strong%22%3E" +
            "Time Out:" + "%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan class=%22strong%22%3E" +
            "Associated Mileage:" + "%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan class=%22strong%22%3E" +
            "Surcharge:" + "%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
            "%3C/td%3E%3Cth class=%22h1%22%3E" +
            "Diabetic Daily Visit" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3E" +
            "Patient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan class=%22strong%22%3E" +
            "Clinician Signature:" + "%3C/span%3E%3Cspan class=%22strong%22%3E" +
            "Date:" + "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3Cspan%3E" +
            "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
            "%3C/span%3E%3C/span%3E";
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/VitalSigns.ascx", Model); %>,4);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Cardiovascular.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Respiratory.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/DiabeticCare.ascx", Model); %>);
    printview.addsubsection(<% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Print/Narrative.ascx", Model); %>,1);
<%  }).Render(); %>
</body>
</html>