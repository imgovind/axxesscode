﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<span class="wintitle">Home Health Aide Visit | <%= Model.Patient.DisplayName %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHAideVisitForm" })) { %>
    <%= Html.Hidden("HHAideVisit_PatientId", Model.PatientId)%>
    <%= Html.Hidden("HHAideVisit_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("HHAideVisit_EventId", Model.EventId)%>
    <%= Html.Hidden(Model.Type + "_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>
    <%= Html.Hidden("DisciplineTask", "54")%>
    <%= Html.Hidden("Type", "HHAideVisit")%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">
                    Home Health Aide Progress Note
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
                    <a class="tooltip red-note float-right" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false"></a>
    <%  } %>
                </th>
            </tr>
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
            <tr>
                <td colspan="4" class="return-alert">
                    <div>
                        <span class="img icon error float-left"></span>
                        <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
                        <div class="buttons">
                            <ul>
                                <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.EventId %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">View Comments</a></li>
                            </ul>
                        </div>
                    </div>
                </td>            
            </tr>
    <%  } %>
            <tr>
                <td colspan="3"><span class="bigtext"><%= Model.Patient.DisplayName %> (<%= Model.Patient.PatientIdNumber %>)</span></td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl %></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="HHAideVisit_EpsPeriod" class="float-left">Episode Period:</label>
                        <div class="float-right"><%= Html.TextBox("HHAideVisit_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "HHAideVisit_EpsPeriod", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HHAideVisit_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="text" class="date-picker required required" name="HHAideVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="HHAideVisit_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HHAideVisit_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox("HHAideVisit_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = "HHAideVisit_TimeIn", @class = "time-picker" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HHAideVisit_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox("HHAideVisit_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = "HHAideVisit_TimeOut", @class = "time-picker" })%></div>
                    </div>
                     <div class="clear"></div>
                     <div>
                        <label for="HHAideVisit_HHAFrequency" class="float-left">Frequency:</label>
                        <div class="float-right"><%= Html.TextBox("HHAideVisit_HHAFrequency", data.ContainsKey("HHAFrequency") ? data["HHAFrequency"].Answer : string.Empty, new { @id = "HHAideVisit_HHAFrequency", @readonly = "readonly" })%></div>
                    </div>
                    <div class="clear"/>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                             <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                   </div>
                </td>
                <td colspan="2">
                    <div>&nbsp;
                    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <label for="HHAideVisit_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "HHAideVisit_PreviousNotes" })%></div>
                    <%  } %>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HHAideVisit_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("HHAideVisit_AssociatedMileage", data.AnswerOrEmptyString("AssociatedMileage"), new { @id = "HHAideVisit_AssociatedMileage", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="HHAideVisit_Surcharge" class="float-left">Surcharge:</label>
                        <div class="float-right"><%= Html.TextBox("HHAideVisit_Surcharge", data.AnswerOrEmptyString("Surcharge"), new { @id = "HHAideVisit_Surcharge", @class = "text number input_wrapper", @maxlength = "7" })%></div>
                    </div>
                    <div class="clear"></div>
                     <div>
                        <label for="HHAideVisit_GenericDigestiveLastBMDate" class="float-left">Last BM Date:</label>
                        <div class="float-right"><input type="text" class="date-picker" name="HHAideVisit_GenericDigestiveLastBMDate" value="<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate") %>" id="HHAideVisit_GenericDigestiveLastBMDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div style="height:26px;">
                        <label for="HHAideVisit_DNR" class="float-left">DNR:</label>
                        <div class="float-right">
                            <%= Html.RadioButton("HHAideVisit_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "HHAideVisit_DNR1", @class = "radio" })%>
                            <label for="HHAideVisit_DNR1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("HHAideVisit_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "HHAideVisit_DNR2", @class = "radio" })%>
                            <label for="HHAideVisit_DNR2" class="inline-radio">No</label>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <div class="float-right">
                            <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                            <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
                            <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                            <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                            <%  } %>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="HHAideVisit_Content"><% Html.RenderPartial("~/Views/Schedule/HHA/VisitNoteContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>       
            <tr>
                <th>Electronic Signature</th>
            </tr>
            <tr>
                <td>
                    <div class="third">
                        <label for="HHAideVisit_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password("HHAideVisit_Clinician", "", new { @id = "HHAideVisit_Clinician", @class = "complete-required" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="HHAideVisit_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="text" class="date-picker complete-required" name="HHAideVisit_SignatureDate" id="HHAideVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td>
                    <%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%>
                    <label for="HHAideVisit_ReturnForSignature">Return to Clinician for Signature</label>
                </td>
            </tr>
    <%  } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="HHAideVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Visit.HHAideVisit.Submit($(this))">Save</a></li>
            <li><a href="javascript:void(0);" onclick="Visit.HHAideVisit.Submit($(this),true)">Complete</a></li>
    <%  if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="Visit.HHAideVisit.Submit($(this))">Approve</a></li>
        <%  if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="Visit.HHAideVisit.Submit($(this))">Return</a></li>
        <%  } %>
    <%  } %>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>