﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentCodes = data.AnswerArray("GenericTreatmentCodes"); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.checkbox("B1 Evaluation",<%= genericTreatmentCodes.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("B2 Thera Ex",<%= genericTreatmentCodes.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("B3 Transfer Training",<%= genericTreatmentCodes.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("B4 Home Program",<%= genericTreatmentCodes.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("B5 Gait Training",<%= genericTreatmentCodes.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("B6 Chest PT",<%= genericTreatmentCodes.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("B7 Ultrasound",<%= genericTreatmentCodes.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("B8 Electrother",<%= genericTreatmentCodes.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("B9 Prosthetic Training",<%= genericTreatmentCodes.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("B10 Muscle Re-ed",<%= genericTreatmentCodes.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("B11 Muscle Re-ed",<%= genericTreatmentCodes.Contains("11").ToString().ToLower() %>) +
            printview.span("Other: <%= data.AnswerOrEmptyString("GenericTreatmentCodesOther").Clean() %>")),
        "Treatment Codes");
</script>