﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
<table class="fixed align-left">
    <tbody>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan1' class='radio' name='{1}_GenericTreatmentPlan' value='1' type='checkbox' {0} />", genericTreatmentPlan.Contains("1").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan1">Therapeutic exercise</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan2' class='radio' name='{1}_GenericTreatmentPlan' value='2' type='checkbox' {0} />", genericTreatmentPlan.Contains("2").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan2">Therapeutic activities (reaching, bending, etc)</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan3' class='radio' name='{1}_GenericTreatmentPlan' value='3' type='checkbox' {0} />", genericTreatmentPlan.Contains("3").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan3">Neuromuscular re-education</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan4' class='radio' name='{1}_GenericTreatmentPlan' value='4' type='checkbox' {0} />", genericTreatmentPlan.Contains("4").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan4">Teach safe and effective use of adaptive/assist device</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan5' class='radio' name='{1}_GenericTreatmentPlan' value='5' type='checkbox' {0} />", genericTreatmentPlan.Contains("5").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan5">Teach fall prevention/safety</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan6' class='radio' name='{1}_GenericTreatmentPlan' value='6' type='checkbox' {0} />", genericTreatmentPlan.Contains("6").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan6">Establish/upgrade home exercise program</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan7' class='radio' name='{1}_GenericTreatmentPlan' value='7' type='checkbox' {0} />", genericTreatmentPlan.Contains("7").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan7">Pt/caregiver education/training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan8' class='radio' name='{1}_GenericTreatmentPlan' value='8' type='checkbox' {0} />", genericTreatmentPlan.Contains("8").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan8">Sensory integrative techniques</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan9' class='radio' name='{1}_GenericTreatmentPlan' value='9' type='checkbox' {0} />", genericTreatmentPlan.Contains("9").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan9">Postural control training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan10' class='radio' name='{1}_GenericTreatmentPlan' value='10' type='checkbox' {0} />", genericTreatmentPlan.Contains("10").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan10">Teach energy conservation techniques</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan11' class='radio' name='{1}_GenericTreatmentPlan' value='11' type='checkbox' {0} />", genericTreatmentPlan.Contains("11").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan11">Wheelchair management training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan12' class='radio' name='{1}_GenericTreatmentPlan' value='12' type='checkbox' {0} />", genericTreatmentPlan.Contains("12").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan12">Teach safe and effective breathing technique</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan13' class='radio' name='{1}_GenericTreatmentPlan' value='13' type='checkbox' {0} />", genericTreatmentPlan.Contains("13").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan13">Teach work simplification</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan14' class='radio' name='{1}_GenericTreatmentPlan' value='14' type='checkbox' {0} />", genericTreatmentPlan.Contains("14").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan14">Community/work integration</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan15' class='radio' name='{1}_GenericTreatmentPlan' value='15' type='checkbox' {0} />", genericTreatmentPlan.Contains("15").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan15">Self care management training</label>
            </td>
        </tr>
        <tr>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan16' class='radio' name='{1}_GenericTreatmentPlan' value='16' type='checkbox' {0} />", genericTreatmentPlan.Contains("16").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan16">Cognitive skills development/training</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan17' class='radio' name='{1}_GenericTreatmentPlan' value='17' type='checkbox' {0} />", genericTreatmentPlan.Contains("17").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan17">Teach task segmentation</label>
            </td>
            <td>
                <%= string.Format("<input id='{1}_GenericTreatmentPlan18' class='radio' name='{1}_GenericTreatmentPlan' value='18' type='checkbox' {0} />", genericTreatmentPlan.Contains("18").ToChecked(), Model.Type)%>
                <label for="<%= Model.Type %>_GenericTreatmentPlan18">Manual therapy techniques</label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_GenericTreatmentPlan19' class='radio' name='{1}_GenericTreatmentPlan' value='19' type='checkbox' {0} />", genericTreatmentPlan.Contains("19").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericTreatmentPlan20">Electrical stimulation</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan19BodyParts" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan19Duration", data.AnswerOrEmptyString("GenericTreatmentPlan19Duration"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan19Duration" })%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div>
                    <%= string.Format("<input id='{1}_GenericTreatmentPlan20' class='radio' name='{1}_GenericTreatmentPlan' value='20' type='checkbox' {0} />", genericTreatmentPlan.Contains("20").ToChecked(), Model.Type)%>
                    <label for="<%= Model.Type %>_GenericTreatmentPlan21">Ultrasound</label>
                </div>
                <div class="margin">
                    <span>Body Parts </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20BodyParts", data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan20BodyParts" })%>
                    <span>Dosage </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20Dosage", data.AnswerOrEmptyString("GenericTreatmentPlan20Dosage"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan20Dosage" })%>
                    <span>Duration </span>
                    <%= Html.TextBox(Model.Type + "_GenericTreatmentPlan20Duration", data.AnswerOrEmptyString("GenericTreatmentPlan20Duration"), new { @class = "", @id = Model.Type + "_GenericTreatmentPlan20Duration" })%>
                </div>
            </td>
        </tr>
        
        <tr>
            <td colspan="3" class="align-center">
                <label for="<%= Model.Type %>_GenericTreatmentPlanOther">Other</label>
                <%= Html.Templates(Model.Type + "_TreatmentPlanTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericTreatmentPlanOther" })%>
                <%= Html.TextArea(Model.Type + "_GenericTreatmentPlanOther", data.AnswerOrEmptyString("GenericTreatmentPlanOther"),4,20, new { @id = Model.Type + "_GenericTreatmentPlanOther", @class="fill" })%>
            </td>
        </tr>
        
    </tbody>
</table>