﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<%  string[] genericTreatmentPlan = data.AnswerArray("GenericTreatmentPlan"); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= Model.Type == "COTAVisit" ? "COTA" : "Occupational Therapist"%> Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3E" +
            "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= Model.Type == "COTAVisit" ? "COT" : "Occupational Therapist"%> Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/VitalSigns/PrintRev2.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericSubjective").Clean() %>",0,1),
        "Subjective");
</script>

<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Homebound/PrintRev2.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/FunctionalLimitations/PrintRev3.ascx", Model); %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/Pain/PrintRev1.ascx", Model); %>
<script type="text/javascript">
printview.addsection(
        printview.col(3,
            printview.checkbox("Patient/Family",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Correct Use of Adaptive Equipment",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Safety Technique",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.checkbox("ADLs",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("5").ToString().ToLower() %>) +
            printview.checkbox("HEP",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Correct Use of Assistive Device",<%= data.AnswerOrEmptyString("GenericTeaching").Split(',').Contains("7").ToString().ToLower() %>) +
            printview.span("")+
            printview.span(""))+
            printview.span("Other(modalities,DME/AE need,consults,etc):<%= data.AnswerOrEmptyString("GenericTeachingOther").Clean() %>",0,1)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTeachingComment").Clean() %>",0,3),
        "Teaching");
</script>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/ADLs/PrintRev3.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("ROM To:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericROMTo").Clean() %> x <%= data.AnswerOrEmptyString("GenericROMToReps").Clean() %> reps",0,1)+
            printview.span("Active To:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericActiveTo").Clean() %> x <%= data.AnswerOrEmptyString("GenericActiveToReps").Clean() %> reps",0,1)+
            printview.span("Active/Assistive To:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericAssistive").Clean() %> x <%= data.AnswerOrEmptyString("GenericAssistiveReps").Clean() %> reps",0,1)+
            printview.span("Resistive, Manual, To:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericManual").Clean() %> x <%= data.AnswerOrEmptyString("GenericManualReps").Clean() %> reps",0,1)+
            printview.span("Resistive, w/Weights, To:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericResistiveWWeights").Clean() %> x <%= data.AnswerOrEmptyString("GenericResistiveWWeightsReps").Clean() %> reps",0,1)+
            printview.span("Stretching To:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericStretchingTo").Clean() %> x <%= data.AnswerOrEmptyString("GenericStretchingToReps").Clean() %> reps",0,1))+
            printview.span("Comment:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericObjectiveComment").Clean() %>",0,2),
            "Therapeutic Exercise");
</script>

<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/SupervisoryVisit/PrintRev1.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistance x Reps",true)+
            printview.span("Assistive Device",true)+
            printview.span("Bed Mobility",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistanceReps").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedMobilityRollingAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Bed/WC transfer",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericBedWCTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericBedWCTransferAssistanceReps").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericBedWCTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Toilet transfer",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericToiletTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericToiletTransferAssistanceReps").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericToiletTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Tub/Shower transfer",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTubShowerTransferAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericTubShowerTransferAssistanceReps").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTubShowerTransferAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1) +
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticOtherAssistance").StringIntToEnumDescriptionFactory("TherapyAssistance").Clean()%> x <%= data.AnswerOrEmptyString("GenericTherapeuticOtherAssistanceReps").Clean()%>",0,1) +
            printview.span("<%= data.AnswerOrEmptyString("GenericTherapeuticOtherAssistiveDevice").StringIntToEnumDescriptionFactory("TherapyAssistiveDevices").Clean() %>",0,1)),
            "Therapeutic/Dynamic Activities");
    printview.addsection(
        printview.span("<%= data.AnswerOrEmptyString("GenericAssessment").Clean() %>",0,3),
        "Assessment");
</script>
<% if (data.AnswerOrEmptyString("GenericIsWCMobilityApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsWCMobilityApplied").ToString() == "1")
   { %>
<script type="text/javascript">
    printview.addsection(printview.checkbox("N/A", true), "W/C Mobility")
</script>
<%}
   else
   { %>
<% Html.RenderPartial("~/Views/Schedule/Therapy/Shared/WCMobility/PrintRev1.ascx", Model); %>
<%} %>
<script type="text/javascript">
printview.addsection(
        printview.col(4,
            printview.span("Continue Plan:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericContinuePrescribedPlan").Clean() %>",0,1) +
            printview.span("Change Plan:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericChangePrescribedPlan").Clean() %>",0,1) +
            printview.span("Plan Discharge:", true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPlanDischarge").Clean() %>",0,1)+
            printview.span("")+
            printview.span(""))+
            printview.span("Comment:",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericPlanComment").Clean() %>",0,1),
        "Plan");
printview.addsection(
    printview.span("<%= data.AnswerOrEmptyString("GenericProgress").Clean() %>"),
    "Progress made towards goals");

printview.addsection(
        printview.col(2,
            printview.checkbox("Therapeutic exercise",<%= genericTreatmentPlan.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Therapeutic activities (reaching, bending, etc)",<%= genericTreatmentPlan.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Neuromuscular re-education",<%= genericTreatmentPlan.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective use of adaptive/assist device",<%= genericTreatmentPlan.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Teach fall prevention/safety",<%= genericTreatmentPlan.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Establish/upgrade home exercise program",<%= genericTreatmentPlan.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Pt/caregiver education/training",<%= genericTreatmentPlan.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Sensory integrative techniques",<%= genericTreatmentPlan.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Postural control training",<%= genericTreatmentPlan.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Teach energy conservation techniques",<%= genericTreatmentPlan.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Wheelchair management training",<%= genericTreatmentPlan.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective breathing technique",<%= genericTreatmentPlan.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Teach work simplification",<%= genericTreatmentPlan.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Community/work integration",<%= genericTreatmentPlan.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Self care management training",<%= genericTreatmentPlan.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Cognitive skills development/training",<%= genericTreatmentPlan.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Teach task segmentation",<%= genericTreatmentPlan.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Manual therapy techniques",<%= genericTreatmentPlan.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Electrical stimulation",<%= genericTreatmentPlan.Contains("19").ToString().ToLower() %>) +
            printview.col(2,
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19Duration").Clean() %>",0,1))+
            printview.checkbox("Ultrasound",<%= genericTreatmentPlan.Contains("20").ToString().ToLower() %>) +
            printview.col(3,
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20BodyParts").Clean() %>",0,1)+
            printview.span("Dosage: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20Dosage").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan20Duration").Clean() %>",0,1)))+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTreatmentPlanOther").Clean() %>",0,1)+
            printview.span("Frequency and Duration",true)+
            printview.span("<%= data.AnswerOrEmptyString("GenericTreatmentPlanFrequencyDuration").Clean() %>",0,1),
            "Skilled treatment provided this visit");
</script>
</body>
</html>
