﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Message | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("New", "Message", FormMethod.Post, new { @id = "newMessageForm" })) { %>
<div id="messaging-container" class="wrapper layout">
    <div class="layout-left"><%= Html.Recipients() %></div>
    <div class="new-message-content layout-main" id="new-message-content">
        <div class="inbox-subheader">
            <span id="messageTypeHeader">New Message</span>
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="Message.Cancel();">Cancel</a></li>
                </ul>
            </div>
        </div>
        <div class="buttons float-left">
            <ul>
                <li class="send">
                    <a href="javascript:void(0);" onclick="$(this).closest('form').submit();"><span class="img icon send"></span><br />Send</a>
                </li>
            </ul>
        </div>
        <div class="new-message-content-panel clear">
            <div class="new-message-row">
                <label for="New_Message_Recipents">To:</label>
                <input type="text" id="New_Message_Recipents" name="newMessageRecipents" />
            </div>
            <%--<div class="new-message-row">
                <label for="New_Message_Recipents">CC:</label>
                <input type="text" id="New_Message_CCRecipients" name="newMessageCCRecipents" />
            </div>--%>
            <div class="new-message-row">
                <label for="New_Message_Subject">Subject:</label>
                <input type="text" id="New_Message_Subject" name="Subject" maxlength="75" />
            </div>
    <%  if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA || Current.IsClinicianOrHHA) { %>
            <div class="new-message-row">
                <label for="New_Message_PatientId">Regarding:</label>
                <%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Message_PatientId", @class = "" })%>
            </div>
    <%  } %>
            <div class="new-message-row">
                <label for="New_Message_PatientId">Attach Document:</label>
                <input type="file" id="New_Message_Attachment" name="Attachment1" />
            </div>
        </div>
        <div class="new-message-row" id="new-message-body-div"></div>
    </div>
</div>
<script type="text/javascript">
    if ($('#window_messageinbox .layout').length) $('#window_messageinbox .layout').layout({ west: { paneSelector: '.layout-left', size: 275} });
    if ($('#window_newmessage .layout').length) $('#window_newmessage .layout').layout({ west: { paneSelector: '.layout-left', size: 275} });
</script>
<%  } %>