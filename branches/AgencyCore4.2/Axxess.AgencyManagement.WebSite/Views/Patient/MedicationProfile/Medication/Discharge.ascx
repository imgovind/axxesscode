﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<form id="dischargeMedicationProfileForm" action="/Patient/UpdateMedicationStatus" method="post">
    <fieldset>
        <legend>Discontinue Medication</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Discharge_Medication_Date">Medication:</label>
                <div class="float-right"><%= Model.MedicationDosage %></div>
            </div>
            <div class="row">
                <label for="Discharge_Medication_Date">D/C date:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="dischargeDate" id="Discharge_Medication_Date" /></div>
            </div>
        </div>
    </fieldset>
    <input type="hidden" id="Discharge_Medication_AssessmentType" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Medication.Discharge('<%= Model.ProfileId %>','<%= Model.Id %>', $(this));">Discontinue</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</form>
