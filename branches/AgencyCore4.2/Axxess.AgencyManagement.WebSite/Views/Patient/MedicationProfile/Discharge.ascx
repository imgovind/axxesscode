﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<form id="dischargeMedicationProfileForm" action="/Patient/UpdateMedicationStatus" method="post">
    <fieldset>
        <legend>Discharge Medication</legend>
        <div class="wide-column">
            <div class="row">
                <label for="">Enter the discharge date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="dischargeDate" id="MedicationProfile_DischargeDate" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</form>