﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Medicare Eligibility Reports | <%= Model.DisplayName %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<MedicareEligibility>()
        .Name("List_MedicareEligibilities")
        .ToolBar(commands => commands.Custom())
        .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("id"); })
        .Columns(columns => {
            columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(false).ReadOnly();
            columns.Bound(c => c.StatusName).Title("Status").Sortable(false).ReadOnly();
            columns.Bound(c => c.Id).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMedicareEligibility('<#=Id#>', '<#=PatientId#>');\">View</a>").Title("Action").Width(100);
        })
        .DataBinding(dataBinding => dataBinding.Ajax()
        .Select("MedicareEligibilityReports", "Patient", new { patientId = Model.Id }))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true))
%>
</div>
<script type="text/javascript">
    $("#List_MedicareEligibilities .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $(".t-grid-content").css("height", "auto");
</script>
