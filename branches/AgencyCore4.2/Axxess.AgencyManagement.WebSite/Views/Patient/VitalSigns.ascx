﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VitalSign>>" %>
<span class="wintitle">Patient Vital Signs | <%= ViewData.ContainsKey("DisplayName") && ViewData["DisplayName"] != null ? ViewData["DisplayName"].ToString().Clean() : string.Empty %></span>
<%var pagename = "Patient_VitalSigns";%>
<% var patientId= ViewData.ContainsKey("PatientId")? ViewData["PatientId"]:Guid.Empty;%>
<div class="wrapper">
    <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
    <fieldset>
        <%= Html.Hidden("PatientId", patientId, new { @id = pagename + "_PatientId" })%>
        <div class="wide-column"><div class="row"><label class="float-left">Date Range:</label><div class="float-left"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" />To<input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div><div class="float-left"> <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"U.RebindDataGridContent('{0}','Patient/VitalSignsContent',{{ PatientId: '{2}', StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams, patientId)%></li></ul></div></div><div class="buttons"><ul class="float-right"><li><%= Html.ActionLink("Export to Excel", "VitalSigns", "Export", new { PatientId = patientId, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = pagename + "_ExportLink", @class = "excel" })%></li></ul></div></div></div>
    </fieldset>
    <div id="<%= pagename %>GridContainer">
    <% Html.RenderPartial("VitalSignsContent", Model); %>
    </div>
</div>

