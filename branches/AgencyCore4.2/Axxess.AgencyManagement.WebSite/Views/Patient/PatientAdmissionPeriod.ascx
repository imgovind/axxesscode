﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AdmissionEpisode>>" %>
<span class="wintitle">Patient Admission Periods | <%= ViewData.ContainsKey("DisplayName") && ViewData["DisplayName"] != null ? ViewData["DisplayName"].ToString() : string.Empty %></span>
<div class="wrapper grid-bg" style="height:100%" >
   <div class="float-left">
        <div class="buttons">
            <ul><li><a id="List_PatientAdmissionPeriod_SendButton" href="javascript:void(0);" onclick="UserInterface.ShowEditPatientAdmission('<%= ViewData.ContainsKey("PatientId") ? ViewData["PatientId"] : Guid.Empty %>','<%= Guid.Empty %>','New');">Add New Period</a></li></ul>
        </div>
    </div> 
   <div id="PatientAdmissionPeriodContainer"><% Html.RenderPartial("PatientAdmissionPeriodContent", Model); %></div>
</div>

