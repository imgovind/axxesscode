﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var medicationCategory = new List<MedicationCategory>(); medicationCategory.Add(new MedicationCategory { Value = "0", Text = "Active" }); medicationCategory.Add(new MedicationCategory { Value = "1", Text = "DC" }); %>
<% ViewData["MedicationCategory"] = medicationCategory; %>
<%= Html.Telerik().DropDownList()
                    .Name("MedicationCategory")
                .BindTo(new SelectList(medicationCategory, "Value", "Text"))
%>
