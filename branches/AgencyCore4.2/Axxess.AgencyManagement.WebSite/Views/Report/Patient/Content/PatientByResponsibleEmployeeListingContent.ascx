﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByResponsibleEmployeeListing"; %>

    <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns => {
        columns.Bound(p => p.PatientId).Title("MRN").Width(80);
        columns.Bound(p => p.PatientLastName).Title("Last Name").Width(150);
        columns.Bound(p => p.PatientFirstName).Title("First Name").Width(150);
        columns.Bound(p => p.PatientMiddleInitial).Title("Middle Initial").Width(100);
        columns.Bound(p => p.AddressFull).Title("Address");
        columns.Bound(p => p.PatientSoC).Title("SOC Date").Width(80);
    })
        //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { UserId = Guid.Empty, BranchId = Guid.Empty, StatusId = 1 }))
                      .Sortable(sorting =>
                             sorting.SortMode(GridSortMode.SingleColumn)
                                 .OrderBy(order =>
                                 {
                                     var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                     var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                     if (sortName == "PatientId")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.PatientId).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.PatientId).Descending();
                                         }
                                     }
                                     else if (sortName == "PatientLastName")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.PatientLastName).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.PatientLastName).Descending();
                                         }
                                     }
                                     else if (sortName == "PatientFirstName")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.PatientFirstName).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.PatientFirstName).Descending();
                                         }
                                     }
                                     else if (sortName == "PatientSoC")
                                     {
                                         if (sortDirection == "ASC")
                                         {
                                             order.Add(o => o.PatientSoC).Ascending();
                                         }
                                         else if (sortDirection == "DESC")
                                         {
                                             order.Add(o => o.PatientSoC).Descending();
                                         }
                                     }
                                 })
                         )
                    .Scrollable()
                        .Footer(false) %>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PatientByResponsibleEmployeeListingContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", UserId : \"" + $('#<%= pagename %>_UserId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>