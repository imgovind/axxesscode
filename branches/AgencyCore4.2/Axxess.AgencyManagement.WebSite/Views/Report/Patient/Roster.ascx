﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientRoster"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Roster</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id =  pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option><option value="3">Pending</option></select></div></div>
            <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%=Html.Insurances("InsuranceId", "0", new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div></div>
            <div class="row"><input type="checkbox" id="<%= pagename %>_ActiveRangeCheckBox" class="float-left radio"><label class="float-left">Active Range:</label><div class="float-right" id="<%= pagename %>_ActiveRange">From : <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li> <%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PatientRosterContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), InsuranceId: $('#{0}_InsuranceId').val(), StartDate: $('#{0}_StartDate:visible').val(), EndDate: $('#{0}_EndDate:visible').val()}},'{1}');\">Generate Report</a>", pagename, sortParams)%> </li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientRoster", new { BranchCode = Guid.Empty, StatusId = 1, InsuranceId = 0, StartDate = DateTime.MinValue, EndDate = DateTime.MinValue }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Patient/Content/RosterContent", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>', 'All', true); });
    U.ShowIfChecked($('#<%= pagename %>_ActiveRangeCheckBox'), $('#<%= pagename %>_ActiveRange'));
 </script>