﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Revenue>>" %>
<% string pagename = "BilledRevenue"; %>
  <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient");
               columns.Bound(p => p.EpisodeRange).Sortable(false).Title("Episode").Width(150);
               columns.Bound(p => p.AssessmentTypeName).Sortable(false).Title("Assessment");
               columns.Bound(p => p.StatusName).Sortable(false).Title("Status");
               columns.Bound(p => p.ProspectivePayment).Format("{0:0.00}").Sortable(false).Title("Total Payment");
               columns.Bound(p => p.BillableVisitCount).Sortable(false).Title("Total Visits");
               columns.Bound(p => p.UnitAmount).Format("${0:0.00}").Sortable(false).Title("Unit Amount");
               columns.Bound(p => p.CompletedVisitCount).Sortable(false).Title("Earned Visits");
               columns.Bound(p => p.EarnedRevenueAmount).Format("${0:0.00}").Sortable(false).Title("Earned Amount");
               columns.Bound(p => p.UnearnedVisitCount).Sortable(false).Title("Unearned Visits");
               columns.Bound(p => p.UnearnedRevenueAmount).Format("${0:0.00}").Sortable(false).Title("Unearned Amount");
               columns.Bound(p => p.UnbilledVisitCount).Sortable(false).Title("Unbilled Visits");
               columns.Bound(p => p.UnbilledRevenueAmount).Format("${0:0.00}").Sortable(false).Title("Unbilled Amount");
               columns.Bound(p => p.RapVisitCount).Sortable(false).Title("Billed Visits");
               columns.Bound(p => p.RapAmount).Format("${0:0.00}").Sortable(false).Title("Billed Amount");
           })
            // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Insurance = 0, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                    .Sortable(sorting =>
                sorting.SortMode(GridSortMode.SingleColumn)
                    .OrderBy(order =>
                    {
                        var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                        var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                        if (sortName == "PatientIdNumber")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.PatientIdNumber).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.PatientIdNumber).Descending();
                            }
                        }
                        else if (sortName == "DisplayName")
                        {
                            if (sortDirection == "ASC")
                            {
                                order.Add(o => o.DisplayName).Ascending();
                            }
                            else if (sortDirection == "DESC")
                            {
                                order.Add(o => o.DisplayName).Descending();
                            }
                        }
                    })
            )
                 .Scrollable()
                   .Footer(false)%>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','BilledRevenueContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", InsuranceId : \"" + $('#<%= pagename %>_InsuranceId').val()  + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
