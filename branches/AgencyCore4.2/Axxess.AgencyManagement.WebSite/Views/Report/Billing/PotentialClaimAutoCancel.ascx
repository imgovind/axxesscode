﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Claim>>" %>
<% string pagename = "PotentialClaimAutoCancel"; %>
<div class="wrapper">
    <fieldset>
        <legend>Potential Claim Auto Cancel</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
        </div>
        <div class="column">
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PotentialClaimAutoCancelContent',{{ BranchCode: $('#{0}_BranchCode').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPotentialClaimAutoCancel", new { BranchCode = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/PotentialClaimAutoCancel", Model); %>
    </div>
</div>


