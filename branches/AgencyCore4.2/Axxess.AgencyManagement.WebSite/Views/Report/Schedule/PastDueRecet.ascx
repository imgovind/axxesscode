﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RecertEvent>>" %>
<% string pagename = "SchedulePastDueRecet"; %>
<div class="wrapper main">
    <fieldset>
    <legend> Past Due Recet.</legend>
         <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"]!=null ?ViewData["ManLocationId"].ToString(): Guid.Empty.ToString() , new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances("InsuranceId",ViewData.ContainsKey("Payor") && ViewData["Payor"]!=null ?ViewData["Payor"].ToString():"0", new { @id = pagename + "_InsuranceId", @class = "Insurances" })%></div></div>
            <div class="row"><label class="float-left">Due Date From:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> </div></div>
        </div>
          <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','PastDueRecetContent',{{ BranchCode: $('#{0}_BranchCode').val(), StartDate: $('#{0}_StartDate').val(), InsuranceId: $('#{0}_InsuranceId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportSchedulePastDueRecet", new { BranchCode = ViewData.ContainsKey("ManLocationId") && ViewData["ManLocationId"] != null ? ViewData["ManLocationId"].ToString() : Guid.Empty.ToString(), InsuranceId = ViewData.ContainsKey("Payor") && ViewData["Payor"] != null ? ViewData["Payor"].ToString() : "0", StartDate = DateTime.Now.AddDays(-60) }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
   <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/PastDueRecet", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>','All',true); });
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
 </script>
