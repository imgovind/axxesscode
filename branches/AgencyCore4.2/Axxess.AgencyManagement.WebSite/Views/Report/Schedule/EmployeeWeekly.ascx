﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserVisit>>" %>
<% string pagename = "ScheduleEmployeeWeekly"; %>
<div class="wrapper">
    <fieldset>
        <legend>Employee Weekly Schedule</legend>
        <div class="column">
              <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
              <div class="row"><label class="float-left">Employees:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_UserId", @class = "report_input valid" })%></div> </div>
              <div class="row"><label class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Today.AddDays(7).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
         <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','EmployeeWeeklyContent',{{ BranchCode: $('#{0}_BranchCode').val(), UserId: $('#{0}_UserId').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleEmployeeWeekly", new { BranchCode = Guid.Empty, UserId = Guid.Empty, StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(7) }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
   <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/EmployeeWeekly", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
</script>
