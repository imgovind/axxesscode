﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RecertEvent>>" %>
<% string pagename = "SchedulePastDueRecet"; %>

         <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
    {
        columns.Bound(r => r.PatientName).Title("Patient");
        columns.Bound(r => r.PatientIdNumber).Title("MRN").Width(120);
        columns.Bound(r => r.AssignedTo).Title("Employee Responsible");
        columns.Bound(r => r.Status).Title("Status").Sortable(false);
        columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120);
        columns.Bound(r => r.DateDifference).Title("Past Dates").Sortable(false).Width(60);
    })
            // .DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleRecertsPastDue", "Report", new { BranchId = Guid.Empty, InsuranceId = Model, StartDate = DateTime.Now.AddDays(-60) }))
                                   .Sortable(sorting =>
                                       sorting.SortMode(GridSortMode.SingleColumn)
                                           .OrderBy(order =>
                                           {
                                               var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                               var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                               if (sortName == "PatientIdNumber")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.PatientIdNumber).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.PatientIdNumber).Descending();
                                                   }

                                               }
                                               else if (sortName == "PatientName")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.PatientName).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.PatientName).Descending();
                                                   }

                                               }
                                               else if (sortName == "AssignedTo")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.AssignedTo).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.AssignedTo).Descending();
                                                   }

                                               }
                                               else if (sortName == "TargetDate")
                                               {
                                                   if (sortDirection == "ASC")
                                                   {
                                                       order.Add(o => o.TargetDate).Ascending();
                                                   }
                                                   else if (sortDirection == "DESC")
                                                   {
                                                       order.Add(o => o.TargetDate).Descending();
                                                   }

                                               }

                                           })
                                           )
                               .Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)
    %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','PastDueRecetContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", InsuranceId : \"" + $('#<%= pagename %>_InsuranceId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); 
 </script>
