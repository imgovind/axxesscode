﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "StatisticalAnnualAdmission"; %>
        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientDisplayName).Title("Patient");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(120);
           })
                   //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, year = DateTime.Now.Year }))
                        .Sortable(sorting =>
                                      sorting.SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                          {
                                              var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                              var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                              if (sortName == "PatientFirstName")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PatientFirstName).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PatientFirstName).Descending();
                                                  }

                                              }
                                              else if (sortName == "PatientLastName")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PatientLastName).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PatientLastName).Descending();
                                                  }

                                              }
                                              else if (sortName == "PatientSoC")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PatientSoC).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PatientSoC).Descending();
                                                  }

                                              }
                                          })
                                  )
                   .Scrollable()
                           .Footer(false)
        %>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','StatisticalAnnualAdmissionContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", Year : \"" + $('#<%= pagename %>_Year').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>

