﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Admissions By Referral Source</legend>
        <div class="column">
            <div class="row"><label for="AdmissionsByReferralSource_BranchId" class="float-left">Branch:</label><div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "AdmissionsByReferralSource_BranchId" })%></div></div>
            <div class="row"><label for="AdmissionsByReferralSource_Year" class="float-left">Sample Year:</label><div class="float-right"><select id="AdmissionsByReferralSource_Year" name="Year"><option value="2012" selected="selected">2012</option><option value="2011">2011</option><option value="2010">2010</option></select></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/AdmissionsByReferralSourceReport', '#AdmissionsByReferralSource_BranchId', '#AdmissionsByReferralSource_Year');">Request Report</a></li></ul></div>
    </fieldset>
</div>
