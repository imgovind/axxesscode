﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "StatisticalBilledEpisodesByInternalSource"; %>
<div class="wrapper">
    <fieldset>
        <legend> Billed Episodes By Internal Source </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
       <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/BilledEpisodesByInternalSource', '#BilledEpisodesByInternalSource_BranchId', '#BilledEpisodesByInternalSource_StartDate', '#BilledEpisodesByInternalSource_EndDate');">Request Report</a></li></ul></div>
    </fieldset>
</div>
