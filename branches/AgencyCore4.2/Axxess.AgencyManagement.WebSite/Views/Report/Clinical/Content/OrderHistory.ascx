﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PhysicianOrder>>" %>
<% string pagename = "ClinicalPhysicianOrderHistory"; %>

        <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(o => o.OrderNumber).Title("Order").Width(70);
               columns.Bound(o => o.DisplayName).Title("Patient");
               columns.Bound(o => o.PhysicianName).Title("Physician");
               columns.Bound(o => o.OrderDate).Template(l => string.Format("{0}", l.OrderDate > DateTime.MinValue ? l.OrderDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Order Date").Width(100);
               columns.Bound(o => o.SentDate).Template(l => string.Format("{0}", l.SentDate > DateTime.MinValue ? l.SentDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Sent Date").Width(100);
               columns.Bound(o => o.ReceivedDate).Template(l => string.Format("{0}", l.ReceivedDate > DateTime.MinValue ? l.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Received Date").Width(120);
           })
          // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 000, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                             .Sortable(sorting =>
                                      sorting.SortMode(GridSortMode.SingleColumn)
                                          .OrderBy(order =>
                                          {
                                              var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                              var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                              if (sortName == "OrderNumber")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.OrderNumber).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.OrderNumber).Descending();
                                                  }

                                              }
                                              else if (sortName == "DisplayName")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.DisplayName).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.DisplayName).Descending();
                                                  }

                                              }
                                              else if (sortName == "PhysicianName")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.PhysicianName).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.PhysicianName).Descending();
                                                  }

                                              }
                                              else if (sortName == "OrderDate")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.OrderDate).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.OrderDate).Descending();
                                                  }

                                              }
                                              else if (sortName == "SentDate")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.SentDate).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.SentDate).Descending();
                                                  }

                                              }
                                              else if (sortName == "ReceivedDate")
                                              {
                                                  if (sortDirection == "ASC")
                                                  {
                                                      order.Add(o => o.ReceivedDate).Ascending();
                                                  }
                                                  else if (sortDirection == "DESC")
                                                  {
                                                      order.Add(o => o.ReceivedDate).Descending();
                                                  }

                                              }
                                              
                                          })
                                  )
                           .Scrollable()
                                   .Footer(false)
        %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','OrderHistoryContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StatusId : \"" + $('#<%= pagename %>_StatusId').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
