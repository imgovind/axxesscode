﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="mail-controls">
    <span class="float-left">
        <a href='javascript:void(0);' onclick="UserInterface.ShowMessages();">Inbox</a> 
        <% if(!Current.IsAgencyFrozen) { %> 
        &#8211; <a href='javascript:void(0);' onclick="UserInterface.ShowNewMessage();">Compose Mail</a>
        <% } %>
    </span>
    <% if(!Current.IsAgencyFrozen) { %> 
    <select class="float-right">
        <option>Actions...</option>
        <option>Delete</option>
    </select>
    <% } %>
    <div class="clear"></div>
</div>
<div style="position: relative; height: 181px;" id="messages-widget-content"></div>
<div onclick="UserInterface.ShowMessages();" id="messageWidgetMore" class="widget-more"><a href="javascript:void(0);">More &#187;</a></div>

