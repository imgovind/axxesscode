﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("AddUserRate", "User", FormMethod.Post, new { @id = "newUserRateForm" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "New_UserRate_UserId" })%>
    <fieldset class="newmed">
        <legend>New Pay Rate</legend>
        <div class="wide_column">
            <div class="row">
                <label for="New_UserRate_Insurance" class="float-left">Insurance:</label>
                <div class="float-right"><%= Html.Insurances("Insurance", "", false, new { @id = "New_UserRate_Insurance", @class = "Insurances requireddropdown valid" })%></div>
            </div>
            <div class="row">
                <label for="New_UserRate_Task" class="float-left">Task:</label>
                <div class="float-right"><%=Html.UserDisciplineTask("Id", 0, Model, string.Empty, true, new { @id = "New_UserRate_Task", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="New_UserRate_RateType" class="float-left">Rate Type:</label>
                <div class="float-right"><%=Html.UserRateTypes("RateType", "0", new { @id = "New_UserRate_RateType", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="New_UserRate_VisitRate" class="float-left">User Rate:</label>
                <div class="float-right"><%= Html.TextBox("Rate", "", new { @id = "New_UserRate_VisitRate", @class = "text input_wrapper required loc", @maxlength = "8" })%></div>
            </div>
             <div class="row">
                <label for="New_UserRate_MileageRate" class="float-left">Mileage Rate:</label>
                <div class="float-right"><%= Html.TextBox("MileageRate", "", new { @id = "New_UserRate_MileageRate", @class = "text input_wrapper loc", @maxlength = "8" })%></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick='$(this).closest("form").submit();return false;'>Add &#38; Exit</a></li>
            <li><a onclick="$(this).closest('.window').Close();return false;">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
