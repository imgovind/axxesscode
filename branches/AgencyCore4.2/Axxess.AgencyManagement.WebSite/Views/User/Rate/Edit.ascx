﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserRate>" %>
<div class="wrapper main">
<% using (Html.BeginForm("UpdateUserRate", "User", FormMethod.Post, new { @id = "editUserRateForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_UserRate_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_UserRate_UserId" })%>
    <fieldset class="newmed">
        <legend>Edit Pay Rate</legend>
        <div class="wide_column">
        <% if (Model.Insurance.IsNullOrEmpty() && Model.RateType == 0) { %>
            <div class="row">
                <label for="Edit_UserRate_Insurance" class="float-left">Insurance:</label>
                <div class="float-right"><%= Html.Insurances("Insurance", Model.Insurance, false, new { @id = "Edit_UserRate_Insurance", @class = "Insurances requireddropdown valid" })%></div>
            </div>
        <% } else { %>
            <div class="row">
                <label class="float-left">Insurance:</label>
                <div class="float-right"><%= Model.InsuranceName %></div>
                <%= Html.Hidden("Insurance", Model.Insurance, new { @id = "Edit_UserRate_Insurance" })%>
            </div>
        <% } %>
            <div class="row">
                <label class="float-left">Task:</label>
                <div class="float-right"><%= Model.DisciplineTaskName %></div>
            </div>
            <div class="row">
                <label for="Edit_UserRate_RateType" class="float-left">Rate Type:</label>
                <div class="float-right"><%=Html.UserRateTypes("RateType", "0", new { @id = "Edit_UserRate_RateType", @class = "text input_wrapper requireddropdown" })%></div>
            </div>
            <div class="row">
                <label for="Edit_UserRate_VisitRate" class="float-left">User Rate:</label>
                <div class="float-right"><%= Html.TextBox("Rate", Model.Rate, new { @id = "Edit_UserRate_VisitRate", @class = "text input_wrapper required loc", @maxlength = "8" })%></div>
            </div>
            <div class="row">
                <label for="Edit_UserRate_MileageRate" class="float-left">Mileage Rate:</label>
                <div class="float-right"><%= Html.TextBox("MileageRate", Model.MileageRate, new { @id = "Edit_UserRate_MileageRate", @class = "text input_wrapper loc", @maxlength = "8" })%></div>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a onclick='$(this).closest("form").submit();return false;'>Save &#38; Exit</a></li>
            <li><a onclick="$(this).closest('.window').Close();return false;">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>
