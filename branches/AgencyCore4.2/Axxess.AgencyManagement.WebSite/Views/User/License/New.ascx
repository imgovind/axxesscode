<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("AddLicenseItem", "User", FormMethod.Post, new { @id = "newLicenseItemForm" })) { %>
<div class="wrapper main">
    <fieldset class="newmed">
        <legend>New License</legend>
        <div class="wide-column">
            <div class="row">
                <label for="New_LicenseItem_FirstName" class="float-left">First Name:</label>
                <div class="float-left"><%= Html.TextBox("FirstName", "", new { @id = "New_LicenseItem_FirstName", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_LicenseItem_LastName" class="float-left">Last Name:</label>
                <div class="float-left"><%= Html.TextBox("LastName", "", new { @id = "New_LicenseItem_LastName", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_LicenseItem_LicenseType" class="float-left">License Type:</label>
                <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", "", new { @id = "New_LicenseItem_Type", @class = "valid" })%><br />
                <%= Html.TextBox("OtherLicenseType", "", new { @id = "New_LicenseItem_TypeOther", @class = "valid", @maxlength = "25" }) %></div>
            </div>
            <div class="row">
                <label for="New_LicenseItem_LastName" class="float-left">Initiation Date:</label>
                <div class="float-left"><input type="text" class="date-picker" name="IssueDate" id="New_LicenseItem_InitDate" /></div>
            </div>
            <div class="row">
                <label for="New_LicenseItem_LastName" class="float-left">Expiration Date:</label>
                <div class="float-left"><input type="text" class="date-picker" name="ExpireDate" id="New_LicenseItem_ExpDate" /></div>
            </div>
            <div class="row">
                <label for="New_LicenseItem_LastName" class="float-left">File Attachment:</label>
                <div class="float-left"><input type="file" name="Attachment" id="New_LicenseItem_Attachment" /></div>
            </div>
        </div>   
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Add</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Close</a></li>
    </ul></div>
</div>
<%} %>

<script type="text/javascript">
    U.ShowIfSelectEquals(
        $("#New_LicenseItem_Type"), "Other",
        $("#New_LicenseItem_TypeOther"));
</script>