﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<div>
    <%= Html.Telerik().Grid<Medication>()
        .Name("MedicationGrid485")
            .DataKeys(keys =>  {
                keys.Add(M => M.Id);
            })
            .ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "485MedicatonGridInsertButton", style = "margin-left:0" }))
            .DataBinding(dataBinding => {
                dataBinding.Ajax()
                    .Select("Medication", "Patient", new { medId = Model.Id, medicationCategory = "Active", assessmentType = "" })
                    .Insert("InsertMedication", "Patient", new { medId = Model.Id, medicationCategory = "Active", assessmentType = "" })
                    .Update("UpdateMedication", "Patient", new { medId = Model.Id, medicationCategory = "Active", assessmentType = "" })
                    .Delete("DeleteMedication", "Patient", new { medId = Model.Id, medicationCategory = "Active", assessmentType = "" });
            })
            .Columns(columns => {
                columns.Bound(M => M.IsLongStanding).ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
                columns.Bound(M => M.StartDate).Format("{0:d}").Width(80);
                columns.Bound(M => M.MedicationDosage).Title("Name & Dose");
                columns.Bound(M => M.Frequency).Title("Frequency").Width(110);
                columns.Bound(M => M.Route).Title("Route").Width(110);
                columns.Bound(M => M.MedicationType).Title("Type").Width(70).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
                columns.Bound(M => M.Classification).Width(120);
                columns.Command(commands =>
                {
                    commands.Edit();
                    commands.Delete();
                }).Width(135).Title("Commands");
            }).ClientEvents(e => e.OnEdit("Patient.OnPlanofCareMedicationEdit")).Editable(editing => editing.Mode(GridEditMode.InLine))
            .Pageable().Scrollable().Sortable()        
    %>
    <%= Html.Hidden("IsNew", "", new { @id = "PlanofCareMedicationIsNew" })%>
</div>

<script type="text/javascript">
    $("#485MedicatonGridInsertButton").html(
        $("<span/>", { "class": "t-add t-icon" }).after("Add New Medication")
    );
    $("#MedicationGrid485 .t-grid-toolbar").append(
        $("<div/>", { "class": "buttons" }).append(
            $("<ul/>", { "class": "float-right" }).append(
                $("<li/>").append(
                    $("<a/>", { "href": "javascript:void(0)", "html": "Save &#38; Close" }).click(function() {
                        PlanOfCare.SaveMedications("<%= Model.PatientId %>")
                    })
                )
            ).append(
                $("<li/>").append(
                    $("<a/>", { "href": "javascript:void(0)", "html": "Cancel" }).click(function() {
                        $(this).closest(".window").Close()
                    })
                )
            )
        )
    );
</script>