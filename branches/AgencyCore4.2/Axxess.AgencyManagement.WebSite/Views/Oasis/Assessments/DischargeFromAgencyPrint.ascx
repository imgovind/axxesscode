<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Demographics.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/RiskAssessment.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Sensory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Pain.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Integumentary.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Respiratory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Cardiac.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Elimination.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/NeuroBehavioral.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/AdlIadl.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Medications.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/CareManagement.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/EmergentCare.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/TransferDischargeDeath.ascx", Model); %>
