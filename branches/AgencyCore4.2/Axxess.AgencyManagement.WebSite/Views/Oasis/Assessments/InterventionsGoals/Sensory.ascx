<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  string[] sensoryStatusIntervention = data.AnswerArray("485SensoryStatusIntervention"); %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485SensoryStatusIntervention", "", new { @id = Model.TypeName + "_485SensoryStatusInterventionHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485SensoryStatusIntervention1' name='{0}_485SensoryStatusIntervention' value='1' type='checkbox' {1} />", Model.TypeName, sensoryStatusIntervention.Contains("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485SensoryStatusIntervention1" class="radio">ST to evaluate.</label>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485SensoryStatusInterventionComments" class="strong">Additional Orders:</label>
            <%= Html.Templates(Model.TypeName + "_485SensoryStatusOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485SensoryStatusInterventionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485SensoryStatusInterventionComments", data.AnswerOrEmptyString("485SensoryStatusInterventionComments"), 5, 70, new { @id = Model.TypeName + "_485SensoryStatusInterventionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="loc485">
    <legend>Goals</legend>
    <div class="wide-column">
        <div class="row">
            <label for="<%= Model.TypeName %>_485SensoryStatusGoalComments" class="strong">Additional Goals:</label>
            <%= Html.Templates(Model.TypeName + "_485SensoryStatusGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485SensoryStatusGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485SensoryStatusGoalComments", data.AnswerOrEmptyString("485SensoryStatusGoalComments"), 5, 70, new { @id = Model.TypeName + "_485SensoryStatusGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>