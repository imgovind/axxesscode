<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<fieldset class="interventions loc485">
    <legend>Interventions</legend>
    <%  string[] nutritionInterventions = data.AnswerArray("485NutritionInterventions"); %>
    <%= Html.Hidden(Model.TypeName + "_485NutritionInterventions", "", new { @id = Model.TypeName + "_485NutritionInterventionsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NutritionInterventions1' name='{0}_485NutritionInterventions' value='1' type='checkbox' {1} />", Model.TypeName, nutritionInterventions.Contains("1").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NutritionInterventions1">SN to instruct</label>
                        <%  var instructOnDietPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructOnDietPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructOnDietPerson", instructOnDietPerson)%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions1">on</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructOnDietDesc", data.AnswerOrEmptyString("485InstructOnDietDesc"), new { @id = Model.TypeName + "_485InstructOnDietDesc" })%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions1">diet.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NutritionInterventions2' name='{0}_485NutritionInterventions' value='2' type='checkbox' {1} />", Model.TypeName, nutritionInterventions.Contains("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485NutritionInterventions2" class="radio">SN to assess patient for diet compliance.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NutritionInterventions3' name='{0}_485NutritionInterventions' value='3' type='checkbox' {1} />", Model.TypeName, nutritionInterventions.Contains("3").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_485NutritionInterventions3" class="radio">SN to instruct on proper technique for tube feeding, aspiration precautions and care of feeding tube site.</label>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NutritionInterventions4' name='{0}_485NutritionInterventions' value='4' type='checkbox' {1} />", Model.TypeName, nutritionInterventions.Contains("4").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NutritionInterventions4">SN to instruct the</label>
                        <%  var instructEnteralNutritionPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructEnteralNutritionPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructEnteralNutritionPerson", instructEnteralNutritionPerson)%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions4">on enteral nutrition and the care/use of equipment.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NutritionInterventions5' name='{0}_485NutritionInterventions' value='5' type='checkbox' {1} />", Model.TypeName, nutritionInterventions.Contains("5").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NutritionInterventions5">SN to instruct the</label>
                        <%  var instructCareOfTubePerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructCareOfTubePerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructCareOfTubePerson", instructCareOfTubePerson)%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions5">on proper care of</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructCareOfTubeDesc", data.AnswerOrEmptyString("485InstructCareOfTubeDesc"), new { @id = Model.TypeName + "_485InstructCareOfTubeDesc" })%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions5">tube.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 21) Orders' id='{0}_485NutritionInterventions6' name='{0}_485NutritionInterventions' value='6' type='checkbox' {1} />", Model.TypeName, nutritionInterventions.Contains("6").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NutritionInterventions6">SN to instruct the</label>
                        <%  var instructFreeWaterPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485InstructFreeWaterPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485InstructFreeWaterPerson", instructFreeWaterPerson)%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions6">to give</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructFreeWaterAmount", data.AnswerOrEmptyString("485InstructFreeWaterAmount"), new { @id = Model.TypeName + "_485InstructFreeWaterAmount", @class = "st" })%>
                        <label for="<%= Model.TypeName %>_485NutritionInterventions6">cc of free water every</label>
                        <%= Html.TextBox(Model.TypeName + "_485InstructFreeWaterEvery", data.AnswerOrEmptyString("485InstructFreeWaterEvery"), new { @id = Model.TypeName + "_485InstructFreeWaterEvery", @class = "st" })%>.
                    </span>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485NutritionComments" class="strong">Additional Orders:</label>
            <%= Html.Templates(Model.TypeName + "_485NutritionOrderTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485NutritionComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485NutritionComments", data.AnswerOrEmptyString("485NutritionComments"), 5, 70, new { @id = Model.TypeName + "_485NutritionComments", @title = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<fieldset class="goals loc485">
    <legend>Goals</legend>
    <%  string[] nutritionGoals = data.AnswerArray("485NutritionGoals"); %>
    <%= Html.Hidden(Model.TypeName + "_485NutritionGoals", "", new { @id = Model.TypeName + "_485NutritionGoalsHidden" })%>
    <div class="wide-column">
<%  if (Model.Discipline == "Nursing") { %>
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NutritionGoals1' name='{0}_485NutritionGoals' value='1' type='checkbox' {1} />", Model.TypeName, nutritionGoals.Contains("1").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NutritionGoals1">Patient will maintain</label>
                        <%= Html.TextBox(Model.TypeName + "_485MaintainDietComplianceType", data.AnswerOrEmptyString("485MaintainDietComplianceType"), new { @id = Model.TypeName + "_485MaintainDietComplianceType" })%>
                        <label for="<%= Model.TypeName %>_485NutritionGoals1">diet compliance during the episode.</label>
                    </span>
                </div>
                <div class="option">
                    <%= string.Format("<input title='(485 Locator 22) Goals' id='{0}_485NutritionGoals2' name='{0}_485NutritionGoals' value='2' type='checkbox' {1} />", Model.TypeName, nutritionGoals.Contains("2").ToChecked()) %>
                    <span class="radio">
                        <label for="<%= Model.TypeName %>_485NutritionGoals2">The</label>
                        <%  var demonstrateEnteralNutritionPerson = new SelectList(new[] {
                                new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                                new SelectListItem { Text = "Patient", Value = "Patient" },
                                new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                            }, "Value", "Text", data.AnswerOrDefault("485DemonstrateEnteralNutritionPerson", "Patient/Caregiver"));%>
                        <%= Html.DropDownList(Model.TypeName + "_485DemonstrateEnteralNutritionPerson", demonstrateEnteralNutritionPerson)%>
                        <label for="<%= Model.TypeName %>_485NutritionGoals2">will demonstrate proper care/use of enteral nutrition equipment by</label>
                        <%= Html.TextBox(Model.TypeName + "_485DemonstrateEnteralNutritionDate", data.AnswerOrEmptyString("485DemonstrateEnteralNutritionDate"), new { @id = Model.TypeName + "_485DemonstrateEnteralNutritionDate", @class = "st" })%>.
                    </span>
                </div>
            </div>
        </div>
<%  } %>
        <div class="row">
            <label for="<%= Model.TypeName %>_485NutritionGoalComments" class="strong">Additional Goals:</label>
            <%= Html.Templates(Model.TypeName + "_485NutritionGoalTemplates", new { @class = "Templates", @template = "#" + Model.TypeName + "_485NutritionGoalComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485NutritionGoalComments", data.AnswerOrEmptyString("485NutritionGoalComments"), 5, 70, new { @id = Model.TypeName + "_485NutritionGoalComments", @title = "(485 Locator 22) Goals" })%>
        </div>
    </div>
</fieldset>
<script type="text/javascript">
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>