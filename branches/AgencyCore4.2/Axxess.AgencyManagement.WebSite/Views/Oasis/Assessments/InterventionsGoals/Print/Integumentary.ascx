<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var IntegumentaryInterventions = data.AnswerArray("485IntegumentaryInterventions"); %>
<%  var IntegumentaryGoals = data.AnswerArray("485IntegumentaryGoals"); %>
<%  if (IntegumentaryInterventions.Length > 0 || (data.ContainsKey("485IntegumentaryInterventionComments") && data["485IntegumentaryInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryInterventions.Contains("1")) { %>
    printview.checkbox("SN to instruct <%= data.AnswerOrDefault("485InstructTurningRepositionPerson", "Patient/Caregiver") %> on turning/repositioning every 2 hours.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("2")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructReduceFrictionPerson", "Patient/Caregiver") %> on methods to reduce friction and shear.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("3")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructPadBonyProminencesPerson", "Patient/Caregiver") %> to pad all bony prominences.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("4")) { %>
    printview.checkbox("SN to perform/instruct on wound care as follows:<%= data.AnswerOrEmptyString("485InstructWoundCarePersonFrequency")%>",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("5")) { %>
    printview.checkbox("SN to assess skin for breakdown every visit.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("6")) { %>
    printview.checkbox("SN to instruct the <%= data.AnswerOrDefault("485InstructSignsWoundInfectionPerson", "Patient/Caregiver") %> on signs/symptoms of wound infection to report to physician, to include increased temp &#62; 100.5, chills, increase in drainage, foul odor, redness, pain and any other significant changes.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("7")) { %>
    printview.checkbox("May discontinue wound care when wound(s) have healed.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("8")) { %>
    printview.checkbox("SN to assess wound for S&#38;S of infection, healing status, wound deterioration, and complications.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("9")) { %>
    printview.checkbox("SN to perform/instruct on incision/suture site care &#38; dressing.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("10")) { %>
    printview.checkbox("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryTurningRepositionTherapyPerson", "Patient/Caregiver") %> on turning/repositioning every 2 hours.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("11")) { %>
    printview.checkbox("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryFloatHeelsPerson", "Patient/Caregiver") %> to float heels.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("12")) { %>
    printview.checkbox("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryReduceFrictionSheerPerson", "Patient/Caregiver") %> on methods to reduce friction and shear.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("13")) { %>
    printview.checkbox("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryMoistureBarrierPerson", "Patient/Caregiver") %> on proper use of moisture barrier <%= data.AnswerOrDefault("485InstructMoistureBarrier", "<span class='blank'></span>") %>.",true) +
    <%  } %>
    <%  if (IntegumentaryInterventions.Contains("14")) { %>
    printview.checkbox("Therapist to instruct <%= data.AnswerOrDefault("485IntegumentaryPadBonyPerson", "Patient/Caregiver") %> to pad all bony prominences.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryInterventionComments") && data["485IntegumentaryInterventionComments"].Answer.IsNotNullOrEmpty())
        { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryInterventionComments").Clean()%>") +
    <%  } %>
    "","Integumentary Interventions");
<%  } %>
<%  if (IntegumentaryGoals.Length > 0 || (data.ContainsKey("485IntegumentaryGoalComments") && data["485IntegumentaryGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryGoals.Contains("1")) { %>
    printview.checkbox("Wound(s) will heal without complication by the end of the episode.",true) +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("2")) { %>
    printview.checkbox("Wound(s) will be free from signs and symptoms of infection during 60 day episode.",true) +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("3")) { %>
    printview.checkbox("Wound(s) will decrease in size by <%= data.AnswerOrDefault("485WoundSizeDecreasePercent", "<span class='short blank'></span>") %>% by the end of the episode. ",true) +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("4")) { %>
    printview.checkbox("Patient skin integrity will remain intact during this episode.",true) +
    <%  } %>
    <%  if (IntegumentaryGoals.Contains("5")) { %>
    printview.checkbox("<%= data.AnswerOrDefault("485DemonstrateSterileDressingTechniquePerson", "Patient/Caregiver")%> will demonstrate understanding of changing <%= data.AnswerOrDefault("485DemonstrateSterileDressingTechniqueType", "<span class='blank'></span>")%> dressing using sterile technique.",true) +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryGoalComments") && data["485IntegumentaryGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryGoalComments").Clean()%>") +
    <%  } %>
    "","Integumentary Goals");
<%  } %>