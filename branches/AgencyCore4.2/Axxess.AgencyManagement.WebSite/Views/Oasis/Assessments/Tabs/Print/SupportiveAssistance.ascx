<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var HazardsIdentified = data.AnswerArray("GenericHazardsIdentified"); %>
<%  var UseOfInterpreter = data.AnswerArray("GenericUseOfInterpreter"); %>
<%  var HomeBoundReason = data.AnswerArray("GenericHomeBoundReason"); %>
<%  var SafetyMeasures = data.AnswerArray("485SafetyMeasures"); %>
<script type="text/javascript">
    printview.addsection(
    "","Supportive Assistance");
<%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    <%  if (Model.AssessmentTypeNum % 10 < 4) { %>
    printview.addsection(
    "","Patient Living Situation");
    printview.addsection(
        "%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%226%22%3E" +
        printview.span("(M1100) Patient Living Situation: Which of the following best describes the patient&#8217;s residential circumstance and availability of assistance?",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth style=%22width:235px;%22%3ELiving Arangement%3C/th%3E%3Cth colspan=%225%22%3EAvailability of Assistance%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3EAround the Clock%3C/th%3E%3Cth>Regular Daytime%3C/th%3E%3Cth%3ERegular Nighttime%3C/th%3E%3Cth%3EOccasional/ Short-term Assistance%3C/th%3E%3Cth%3ENo Assistance%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("a. Patient lives alone") + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("01",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("01").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("02",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("02").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("03",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("03").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("04",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("04").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("05",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("05").ToString().ToLower() %>) + "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("b. Patient lives with other person(s) in the home") + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("06",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("06").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("07",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("07").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("08",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("08").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("09",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("09").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("10",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("10").ToString().ToLower() %>) + "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("c. Patient lives in congregate situation") + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("11",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("11").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("12",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("12").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("13",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("13").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("14",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("14").ToString().ToLower() %>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("15",<%= data.AnswerOrEmptyString("M1100LivingSituation").Equals("15").ToString().ToLower() %>) + "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
     
    <%  } %>
    printview.addsection(
        printview.col(2,
            printview.span("Community resource info needed to manage care",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCommunityResourceInfoNeeded").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCommunityResourceInfoNeeded").Equals("0").ToString().ToLower() %>)) +
            printview.span("Altered affect, e.g., expressed sadness or anxiety, grief ",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericAlteredAffect").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericAlteredAffect").Equals("0").ToString().ToLower() %>)) +
            printview.span("Suicidal ideation",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericSuicidalIdeation").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericSuicidalIdeation").Equals("0").ToString().ToLower() %>))) +
        printview.span("Suspected Abuse/ Neglect",true) +
        printview.col(2,
            printview.col(2,
                printview.checkbox("Unexplained bruises",<%= data.AnswerArray("GenericSuspected").Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Inadequate food",<%= data.AnswerArray("GenericSuspected").Contains("2").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Fearful of family member",<%= data.AnswerArray("GenericSuspected").Contains("3").ToString().ToLower() %>) +
                printview.checkbox("Exploitation of funds",<%= data.AnswerArray("GenericSuspected").Contains("4").ToString().ToLower() %>)) +
            printview.col(2,
                printview.checkbox("Sexual abuse",<%= data.AnswerArray("GenericSuspected").Contains("5").ToString().ToLower() %>) +
                printview.checkbox("Neglect",<%= data.AnswerArray("GenericSuspected").Contains("6").ToString().ToLower() %>)) +
            printview.checkbox("Left unattended if constant supervision is needed",<%= data.AnswerArray("GenericSuspected").Contains("7").ToString().ToLower() %>) +
            printview.span("MSW referral indicated for ",true) +
            printview.col(2,
            printview.span("<%= data.AnswerOrEmptyString("GenericMSWIndicatedForWhat").Clean() %>",true)+
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericMSWIndicatedFor").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericMSWIndicatedFor").Equals("No").ToString().ToLower() %>))) +
            printview.span("Coordinator notified",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCoordinatorNotified").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCoordinatorNotified").Equals("No").ToString().ToLower() %>)) +
            printview.span("Ability of Patient to Handle Finances",true) +
            printview.col(3,
                printview.checkbox("Independent",<%= data.AnswerOrEmptyString("GenericAbilityHandleFinance").Equals("Independent").ToString().ToLower() %>) +
                printview.checkbox("Dependent",<%= data.AnswerOrEmptyString("GenericAbilityHandleFinance").Equals("Dependent").ToString().ToLower() %>) +
                printview.checkbox("Needs assistance",<%= data.AnswerOrEmptyString("GenericAbilityHandleFinance").Equals("Needs assistance").ToString().ToLower() %>))) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericAlteredAffectComments").Clean() %>",false,2) +
        printview.span("Supportive Assistance: Names of organizations providing assistance:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericOrgProvidingAssistanceNames").Clean() %>",false,2),
        "Community Agencies/ Social Service Screening");
    printview.addsection(
        printview.span("Safety/Sanitation Hazards affecting patient: (Select all that apply)",true)+
        printview.col(4,
            printview.checkbox("No hazards identified",<%= HazardsIdentified.Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Stairs",<%= HazardsIdentified.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Narrow or obstructed walkway",<%= HazardsIdentified.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("No gas/ electric appliance",<%= HazardsIdentified.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("No running water, plumbing",<%= HazardsIdentified.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Insect/ rodent infestation",<%= HazardsIdentified.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Cluttered/ soiled living area",<%= HazardsIdentified.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Inadequate lighting, heating and cooling",<%= HazardsIdentified.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Lack of fire safety devices",<%= HazardsIdentified.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Other <%= HazardsIdentified.Contains("9") ? data.AnswerOrEmptyString("GenericOtherHazards") : string.Empty %>",<%= HazardsIdentified.Contains("9").ToString().ToLower() %>)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericHazardsComments").Clean() %>",false,2),
        "Safety/ Sanitation Hazards");
    printview.addsection(
        printview.col(2,
            printview.span("Primary language?",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericPrimaryLanguage").Clean() %>",false,1)) +
        printview.col(5,
            printview.span("Use of interpreter",true) +
            printview.checkbox("Family",<%= UseOfInterpreter.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Friend",<%= UseOfInterpreter.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Professional",<%= UseOfInterpreter.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Other <%= UseOfInterpreter.Contains("4") ? data.AnswerOrEmptyString("GenericUseOfInterpreterOtherDetails") : string.Empty %>",<%= UseOfInterpreter.Contains("4").ToString().ToLower() %>)) +
        printview.col(2,
            printview.span("Does patient have cultural practices that influence health care?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCulturalPractices").Equals("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCulturalPractices").Equals("0").ToString().ToLower() %>))) +
        printview.span("If yes, please explain",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericCulturalPracticesDetails").Clean() %>",false,2),
        "Cultural");
    printview.addsection(
        printview.col(2,
            printview.span("Is the patient homebound?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericIsHomeBound").Equals("Yes").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericIsHomeBound").Equals("No").ToString().ToLower() %>))) +
    <%  if (!data.AnswerOrEmptyString("GenericIsHomeBound").Equals("No")) { %>
        printview.col(2,
            printview.checkbox("Exhibits considerable &#38; taxing effort to leave home",<%= HomeBoundReason.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Requires the assistance of another to get up and moving safely",<%= HomeBoundReason.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Severe Dyspnea",<%= HomeBoundReason.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Unable to safely leave home unassisted",<%= HomeBoundReason.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Unsafe to leave home due to cognitive or psychiatric impairments",<%= HomeBoundReason.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Unable to leave home due to medical restriction(s)",<%= HomeBoundReason.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Other <%= HomeBoundReason.Contains("7") ? data.AnswerOrEmptyString("GenericOtherHomeBoundDetails") : string.Empty %>",<%= HomeBoundReason.Contains("7").ToString().ToLower() %>)) +
            printview.span("Comments:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericHomeBoundComments").Clean() %>",false,2)+
    <%  } %>
        "","Homebound");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Anticoagulant Precautions",<%= SafetyMeasures.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Emergency Plan Developed",<%= SafetyMeasures.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Fall Precautions",<%= SafetyMeasures.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Keep Pathway Clear",<%= SafetyMeasures.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Keep Side Rails Up",<%= SafetyMeasures.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Neutropenic Precautions",<%= SafetyMeasures.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("O<sub>2</sub> Precautions",<%= SafetyMeasures.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Proper Position During Meals",<%= SafetyMeasures.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Safety in ADLs",<%= SafetyMeasures.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Seizure Precautions",<%= SafetyMeasures.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Sharps Safety",<%= SafetyMeasures.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Slow Position Change",<%= SafetyMeasures.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Standard Precautions/ Infection Control",<%= SafetyMeasures.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Support During Transfer and Ambulation",<%= SafetyMeasures.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Use of Assistive Devices",<%= SafetyMeasures.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Instructed on safe utilities management",<%= SafetyMeasures.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Instructed on mobility safety",<%= SafetyMeasures.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Instructed on DME &#38; electrical safety",<%= SafetyMeasures.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Instructed on sharps container",<%= SafetyMeasures.Contains("19").ToString().ToLower() %>) +
            printview.checkbox("Instructed on medical gas",<%= SafetyMeasures.Contains("20").ToString().ToLower() %>) +
            printview.checkbox("Instructed on disaster/ emergency plan",<%= SafetyMeasures.Contains("21").ToString().ToLower() %>) +
            printview.checkbox("Instructed on safety measures",<%= SafetyMeasures.Contains("22").ToString().ToLower() %>) +
            printview.checkbox("Instructed on proper handling of bio waste",<%= SafetyMeasures.Contains("23").ToString().ToLower() %>)) +
        printview.col(2,
            printview.span("Emergency Triage Code:",true) +
            printview.span("<%= data.AnswerOrEmptyString("485TriageEmergencyCode").Clean() %>",false,1)) +
        printview.span("Other:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485OtherSafetyMeasures").Clean() %>",false,2),
        "Safety Measures((Locator #15))");
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/SupportiveAssistance.ascx", Model); %>
<%  } %>
</script>