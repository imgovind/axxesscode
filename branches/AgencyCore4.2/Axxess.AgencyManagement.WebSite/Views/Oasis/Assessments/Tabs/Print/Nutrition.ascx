<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AssessmentPrint>" %>
<%  var data = Model.Data; %>
<%  var isOasis = !Model.Type.ToString().Contains("NonOasis"); %>
<%  var Nutrition = data.AnswerArray("GenericNutrition"); %>
<%  var NutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
<%  var NutritionalHealth = data.AnswerArray("GenericNutritionalHealth"); %>
<%  var NutritionDiffect = data.AnswerArray("GenericNutritionDiffect"); %>
<%  var NutritionalReqs = data.AnswerArray("485NutritionalReqs"); %>
<%  var NutritionalReqsEnteral = data.AnswerArray("485NutritionalReqsEnteral"); %>
<%  var NutritionalReqsEnteralVia = data.AnswerArray("485NutritionalReqsEnteralVia"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum % 10 < 5) { %>
    printview.addsection(
        printview.col(2,
            printview.checkbox("WNL (Within Normal Limits)",<%= Nutrition.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Dysphagia",<%= Nutrition.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Appetite",<%= Nutrition.Contains("3").ToString().ToLower() %>,true) +
        <%if(Nutrition.Contains("4")){ %>
            printview.col(3,
                printview.checkbox("Weight",true)+
                printview.checkbox("Loss",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss").ToString().ToLower() %>) +
                printview.checkbox("Gain",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain").ToString().ToLower() %>)) +
        <%}else{ %>
            printview.checkbox("Weight",false)+
        <%} %>
        <%if(Nutrition.Contains("5")){ %>
            printview.col(3,
                printview.checkbox("Diet",true)+
                printview.checkbox("Adequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate").ToString().ToLower() %>) +
                printview.checkbox("Inadequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate").ToString().ToLower() %>)) +
        <%}else{ %>
            printview.checkbox("Diet",false)+
        <%} %>
        <%if(Nutrition.Contains("9")){ %>
            printview.col(2,
                printview.checkbox("Diet Type",true)+
                printview.span("<%=data.AnswerOrEmptyString("GenericNutritionDietType").Clean() %>"))+
        <%}else{ %>
            printview.checkbox("Diet Type",false)+
        <%} %>
        <%if(Nutrition.Contains("6")){ %>
            printview.col(4,
                printview.checkbox("Enteral Feeding",true)+
                printview.checkbox("NG",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("1")).ToString().ToLower() %>) +
                printview.checkbox("PEG",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("2")).ToString().ToLower() %>) +
                printview.checkbox("Dobhoff",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("3")).ToString().ToLower() %>)) +
            
        <%}else{ %>
            printview.checkbox("Enteral Feeding",false)+
        <%} %>
            printview.checkbox("Tube Placement Checked",<%= Nutrition.Contains("7").ToString().ToLower() %>,true) +
       <%if(Nutrition.Contains("8")){ %>  
           printview.col(2,
                printview.checkbox("Residual Checked",true)+
                printview.span("Amount: <%= data.AnswerOrDefault("GenericNutritionResidualCheckedAmount", "<span class='short blank'></span>").Clean() %>ml"))+
       <%}else{ %>   
           printview.checkbox("Residual Checked",true)+
       <%} %>
        printview.span(""))+
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericNutritionComments").Clean() %>",false,2),
        "Nutrition");
    printview.addsection(
      printview.col(2,
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EWithout reason, has lost more than 10 lbs, in the last 3 months%3C/span%3E%3Cspan class=%22auto float-right%22%3E15%3C/span%3E",<%= NutritionalHealth.Contains("1").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EHas an illness/condition changed the type/amount of food eaten%3C/span%3E%3Cspan class=%22auto float-right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("2").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EHas open decubitus, ulcer, burn or wound%3C/span%3E%3Cspan class=%22auto float-right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("3").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EEats fewer than 2 meals a day%3C/span%3E%3Cspan class=%22auto float-right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("4").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EHas a tooth/mouth problem that makes it hard to eat%3C/span%3E%3Cspan class=%22auto float-right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("5").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EHas 3 or more drinks of beer, liquor or wine almost every day%3C/span%3E%3Cspan class=%22auto float-right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("6").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EDoes not always have enough money to buy foods needed%3C/span%3E%3Cspan class=%22auto float-right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("7").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EEats few fruits or vegetables, or milk products%3C/span%3E%3Cspan class=%22auto float-right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("8").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EEats alone most of the time%3C/span%3E%3Cspan class=%22auto float-right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("9").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3ETakes 3 or more prescribed or OTC medications a day%3C/span%3E%3Cspan class=%22auto float-right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("10").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3ENot always physically able to cook/feed self and no CG to assist%3C/span%3E%3Cspan class=%22auto float-right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("11").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float-left%22%3EFrequently has diarrhea or constipation%3C/span%3E%3Cspan class=%22auto float-right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("12").ToString().ToLower() %>)) +
        printview.span("%3Cspan class=%22auto strong float-left%22%3ETotal%3C/span%3E%3Cspan class=%22auto float-right%22%3E<%= data.AnswerOrEmptyString("GenericGoodNutritionScore").Clean() %>%3C/span%3E") +
        printview.span("Nutritional Status Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericNutritionalStatusComments").Clean() %>",false,2) +
        printview.col(2,
            printview.checkbox("Non-compliant with prescribed diet",<%= NutritionDiffect.Contains("1") ? "true,true" : "false,false"%>) +
            printview.checkbox("Over/under weight by 10%",<%= NutritionDiffect.Contains("2") ? "true,true" : "false,false"%>) +
            printview.span("Meals prepared by:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMealsPreparedBy").Clean() %>",false,1)),
        "Nutritional Health Screen");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Regular",<%= NutritionalReqs.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Mechanical Soft",<%= NutritionalReqs.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Heart Healthy",<%= NutritionalReqs.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Low Cholesterol",<%= NutritionalReqs.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Low Fat",<%= NutritionalReqs.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("<%= data.AnswerOrDefault("485NutritionalReqsSodiumAmount", "Low").Clean() %> Sodium",<%= NutritionalReqs.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("No Added Salt",<%= NutritionalReqs.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("<%= data.AnswerOrDefault("485NutritionalReqsCalorieADADietAmount", "<span class='short blank'></span>").Clean() %> Calorie ADA Diet",<%= NutritionalReqs.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("No Concentrated Sweets",<%= NutritionalReqs.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Coumadin Diet",<%= NutritionalReqs.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Renal Diet",<%= NutritionalReqs.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Other<%= NutritionalReqs.Contains("12") ?": "+ data.AnswerOrEmptyString("485NutritionalReqsPhyDietOtherName").Clean() : string.Empty %>",<%= NutritionalReqs.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Supplement<%= NutritionalReqs.Contains("13") ?": "+ data.AnswerOrEmptyString("485NutritionalReqsSupplementType").Clean() : string.Empty %>",<%= NutritionalReqs.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Fluid Restriction<%= NutritionalReqs.Contains("14") ?": "+ data.AnswerOrEmptyString("485NutritionalReqsFluidResAmount").Clean()+"ml/24 hours" : string.Empty %>",<%= NutritionalReqs.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("TPN<%=NutritionalReqs.Contains("16")?": "+data.AnswerOrEmptyString("485NutritionalReqsTPNAmount").Clean()+"@ml/hr via "+data.AnswerOrEmptyString("485NutritionalReqsTPNVia").Clean():string.Empty %>",<%= NutritionalReqs.Contains("16").ToString().ToLower() %>)) +
        <%if(NutritionalReqs.Contains("15")){ %>
        printview.col(5,
            printview.checkbox("Enteral Nutrition",<%= NutritionalReqs.Contains("15").ToString().ToLower() %>) +
            printview.span("") + printview.span("") +
            printview.span("Formula: <%= data.AnswerOrEmptyString("485NutritionalReqsEnteralDesc").Clean() %>") +
            printview.span("Amount: <%= data.AnswerOrDefault("485NutritionalReqsEnteralAmount", "<span class='short blank'></span>") %>ml/hr") +
            printview.span("Per:") + 
            printview.checkbox("PEG",<%= NutritionalReqsEnteral.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("NG",<%= NutritionalReqsEnteral.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Dobhoff",<%= NutritionalReqsEnteral.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Other <%= data.AnswerOrEmptyString("485NutritionalReqsEnteralOtherName").Clean() %>",<%= NutritionalReqsEnteral.Contains("4").ToString().ToLower() %>) +
            printview.span("Via:") + 
            printview.span("") +
            printview.checkbox("Pump",<%= NutritionalReqsEnteralVia.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Gravity",<%= NutritionalReqsEnteralVia.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Bolus",<%= NutritionalReqsEnteralVia.Contains("3").ToString().ToLower() %>)) ,
        <%}else{ %>
            printview.checkbox("Enteral Nutrition",false),
        <%} %>
        "Enter Physician&#8217;s Orders or Diet Requirements");
        <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Nutrition.ascx", Model); %>
<%  } %>
</script>