<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
    
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SuppliesWorksheetForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "SuppliesWorksheet")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <% Html.RenderPartial("Action", Model); %>
    <div class="medication medDiv">
        <div class="align-center strong">Supply Worksheet</div>
        <%= Html.Telerik().Grid<Supply>()
            .Name(Model.TypeName + "_SupplyGrid")
            .DataKeys(keys => { keys.Add(M => M.UniqueIdentifier).RouteKey("UniqueIdentifier"); })
            .ToolBar(commands => { commands.Insert().ButtonType(GridButtonType.Text); })
            .DataBinding(dataBinding => {
                dataBinding.Ajax()
                    .Insert("AddSupply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = Model.TypeName })
                    .Select("Supply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = Model.TypeName })
                    .Update("EditSupply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = Model.TypeName })
                    .Delete("DeleteSupply", "Oasis", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id, assessmentType = Model.TypeName });
            })
           .Columns(columns => {
                columns.Bound(s => s.Description);
                columns.Bound(s => s.Quantity).Width(95);
                columns.Bound(e => e.DateForEdit).Title("Date").Format("{0:MM/dd/yyyy}").Width(120);
                columns.Command(commands => { commands.Edit(); commands.Delete(); }).Width(150).Title("Action");
                columns.Bound(s => s.UniqueIdentifier).HeaderHtmlAttributes(new { style = "display: none" }).HtmlAttributes(new { style = "display: none" });
            })
            .Editable(editing => editing.Mode(GridEditMode.InLine))
            .ClientEvents(events => events.OnEdit("Supply.OnSupplyEdit"))
            .Sortable()
            .Footer(false) %>
    </div>
    <fieldset>
        <legend>Supplies</legend>
        <%  string[] supplies = data.AnswerArray("485Supplies"); %>
        <%= Html.Hidden(Model.TypeName + "_485Supplies", "", new { @id = Model.TypeName + "_485SuppliesHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, ABDs' id='{0}_485Supplies1' name='{0}_485Supplies' value='1' type='checkbox' {1} />", Model.TypeName, supplies.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies1" class="radio">ABDs</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Ace Wrap' id='{0}_485Supplies2' name='{0}_485Supplies' value='2' type='checkbox' {1} />", Model.TypeName, supplies.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies2" class="radio">Ace Wrap</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Alcohol Pads' id='{0}_485Supplies3' name='{0}_485Supplies' value='3' type='checkbox' {1} />", Model.TypeName, supplies.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies3" class="radio">Alcohol Pads</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Chux/Underpads' id='{0}_485Supplies4' name='{0}_485Supplies' value='4' type='checkbox' {1} />", Model.TypeName, supplies.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies4" class="radio">Chux/Underpads</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Diabetic Supplies' id='{0}_485Supplies5' name='{0}_485Supplies' value='5' type='checkbox' {1} />", Model.TypeName, supplies.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies5" class="radio">Diabetic Supplies</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Drainage Bag' id='{0}_485Supplies6' name='{0}_485Supplies' value='6' type='checkbox' {1} />", Model.TypeName, supplies.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies6" class="radio">Drainage Bag</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Dressing Supplies' id='{0}_485Supplies7' name='{0}_485Supplies' value='7' type='checkbox' {1} />", Model.TypeName, supplies.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies7" class="radio">Dressing Supplies</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Duoderm' id='{0}_485Supplies8' name='{0}_485Supplies' value='8' type='checkbox' {1} />", Model.TypeName, supplies.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies8" class="radio">Duoderm</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Exam Gloves' id='{0}_485Supplies9' name='{0}_485Supplies' value='9' type='checkbox' {1} />", Model.TypeName, supplies.Contains("9").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies9" class="radio">Exam Gloves</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Foley Catheter' id='{0}_485Supplies10' name='{0}_485Supplies' value='10' type='checkbox' {1} />", Model.TypeName, supplies.Contains("10").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies10" class="radio">Foley Catheter</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Gauze Pads' id='{0}_485Supplies11' name='{0}_485Supplies' value='11' type='checkbox' {1} />", Model.TypeName, supplies.Contains("11").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies11" class="radio">Gauze Pads</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Insertion Kit' id='{0}_485Supplies12' name='{0}_485Supplies' value='12' type='checkbox' {1} />", Model.TypeName, supplies.Contains("12").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies12" class="radio">Insertion Kit</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Irrigation Set' id='{0}_485Supplies13' name='{0}_485Supplies' value='13' type='checkbox' {1} />", Model.TypeName, supplies.Contains("13").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies13" class="radio">Irrigation Set</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Irrigation Solution' id='{0}_485Supplies14' name='{0}_485Supplies' value='14' type='checkbox' {1} />", Model.TypeName, supplies.Contains("14").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies14" class="radio">Irrigation Solution</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Kerlix Rolls' id='{0}_485Supplies15' name='{0}_485Supplies' value='15' type='checkbox' {1} />", Model.TypeName, supplies.Contains("15").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies15" class="radio">Kerlix Rolls</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Leg Bag' id='{0}_485Supplies16' name='{0}_485Supplies' value='16' type='checkbox' {1} />", Model.TypeName, supplies.Contains("16").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies16" class="radio">Leg Bag</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Needles' id='{0}_485Supplies17' name='{0}_485Supplies' value='17' type='checkbox' {1} />", Model.TypeName, supplies.Contains("17").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies17" class="radio">Needles</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, NG Tube' id='{0}_485Supplies18' name='{0}_485Supplies' value='18' type='checkbox' {1} />", Model.TypeName, supplies.Contains("18").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies18" class="radio">NG Tube</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Probe Covers' id='{0}_485Supplies19' name='{0}_485Supplies' value='19' type='checkbox' {1} />", Model.TypeName, supplies.Contains("19").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies19" class="radio">Probe Covers</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Sharps Container' id='{0}_485Supplies20' name='{0}_485Supplies' value='20' type='checkbox' {1} />", Model.TypeName, supplies.Contains("20").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies20" class="radio">Sharps Container</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Sterile Gloves' id='{0}_485Supplies21' name='{0}_485Supplies' value='21' type='checkbox' {1} />", Model.TypeName, supplies.Contains("21").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies21" class="radio">Sterile Gloves</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Syringe' id='{0}_485Supplies22' name='{0}_485Supplies' value='22' type='checkbox' {1} />", Model.TypeName, supplies.Contains("22").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies22" class="radio">Syringe</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Supplies, Tape' id='{0}_485Supplies23' name='{0}_485Supplies' value='23' type='checkbox' {1} />", Model.TypeName, supplies.Contains("23").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485Supplies23" class="radio">Tape</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485SuppliesComment">Other:</label>
                <%= Html.TextArea(Model.TypeName + "_485SuppliesComment", data.AnswerOrEmptyString("485SuppliesComment"), 5, 70, new { @id = Model.TypeName + "_485SuppliesComment", @title = "(Optional) Other Supplies" })%>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>DME (Locator #14)</legend>
        <%  string[] dME = data.AnswerArray("485DME"); %>
        <%= Html.Hidden(Model.TypeName + "_485DME", "", new { @id = Model.TypeName + "_485DMEHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Bedside Commode' id='{0}_485DME1' name='{0}_485DME' value='1' type='checkbox' {1} />", Model.TypeName, dME.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME1" class="radio">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Cane' id='{0}_485DME2' name='{0}_485DME' value='2' type='checkbox' {1} />", Model.TypeName, dME.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME2" class="radio">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Elevated Toilet Seat' id='{0}_485DME3' name='{0}_485DME' value='3' type='checkbox' {1} />", Model.TypeName, dME.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME3" class="radio">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Grab Bars' id='{0}_485DME4' name='{0}_485DME' value='4' type='checkbox' {1} />", Model.TypeName, dME.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME4" class="radio">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Hospital Bed' id='{0}_485DME5' name='{0}_485DME' value='5' type='checkbox' {1} />", Model.TypeName, dME.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME5" class="radio">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Nebulizer' id='{0}_485DME6' name='{0}_485DME' value='6' type='checkbox' {1} />", Model.TypeName, dME.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME6" class="radio">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Oxygen' id='{0}_485DME7' name='{0}_485DME' value='7' type='checkbox' {1} />", Model.TypeName, dME.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME7" class="radio">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Tub/Shower Bench' id='{0}_485DME8' name='{0}_485DME' value='8' type='checkbox' {1} />", Model.TypeName, dME.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME8" class="radio">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Walker' id='{0}_485DME9' name='{0}_485DME' value='9' type='checkbox' {1} />", Model.TypeName, dME.Contains("9").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME9" class="radio">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(485 Locator 14) DME, Wheelchair' id='{0}_485DME10' name='{0}_485DME' value='10' type='checkbox' {1} />", Model.TypeName, dME.Contains("10").ToChecked())%>
                        <label for="<%= Model.TypeName %>_485DME10" class="radio">Wheelchair</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485DMEComments">Other:</label>
                <%= Html.TextArea(Model.TypeName + "_485DMEComments", data.AnswerOrEmptyString("485DMEComments"), 5, 70, new { @id = Model.TypeName + "_485DMEComments", @title = "(485 Locator 14) DME, Other" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Provider</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDMEProviderName" class="float-left">Name:</label>
                <div class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDMEProviderName", data.AnswerOrEmptyString("GenericDMEProviderName"), new { @id = Model.TypeName + "_GenericDMEProviderName", @maxlength = "40", @title = "(Optional) DME Provider, Name" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDMEProviderPhone" class="float-left">Phone Number:</label>
                <div class="float-right"><%= Html.TextBox(Model.TypeName + "_GenericDMEProviderPhone", data.AnswerOrEmptyString("GenericDMEProviderPhone"), new { @id = Model.TypeName + "_GenericDMEProviderPhone", @maxlength = "12", @title = "(Optional) DME Provider, Phone" })%></div>
            </div>
        </div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericDMESuppliesProvided" class="strong">DME/Supplies Provided:</label>
                <%= Html.TextArea(Model.TypeName + "_GenericDMESuppliesProvided", data.AnswerOrEmptyString("GenericDMESuppliesProvided"), 5, 70, new { @id = Model.TypeName + "_GenericDMESuppliesProvided", @title = "(Optional) DME Provider, Supplies Provided" })%>
            </div>
        </div>
    </fieldset>
    <% Html.RenderPartial("Action", Model); %>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
$("#<%= Model.TypeName + "_SupplyGrid" %> .t-grid-toolbar a.t-grid-add").text("Add New Supply");
</script>