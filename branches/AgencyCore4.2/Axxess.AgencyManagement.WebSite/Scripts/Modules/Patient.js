﻿var Patient = {
    _patientId: "",
    _patientStatus: "",
    _patientName: "",
    _EpisodeId: "",
    _patientRowIndex: 0,
    _dateFilter: "",
    _showFilter: "",
    _isRebind: false,
    _fromDate: "",
    _toDate: "",
    GetId: function() {
        return Patient._patientId;
    },
    SetId: function(patientId) {
        Patient._patientId = patientId;
    },
    SetPatientRowIndex: function(patientRowIndex) {
        Patient._patientRowIndex = patientRowIndex;
    },
    GetEpisodeId: function() {
        return Patient._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        Patient._EpisodeId = EpisodeId;
    },
    RebindList: function() {
        U.RebindTGrid($('#List_Patient_Grid'));
        U.RebindTGrid($('#List_PatientPending_Grid'));
        U.RebindTGrid($('#List_Referral'));
    },
    Filter: function(text) {
        search = text.split(" ");
        $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#PatientSelectionGrid .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) {
                    $(this).parent().addClass("match");
                }
            });
        }
        $("tr.match", "#PatientSelectionGrid .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
    },
    InitNew: function() {
        var $triage = $('input.Triage');
        $triage.click(function() {
            $triage.removeAttr('checked');
            $(this).attr('checked', true);
        });
        U.PhoneAutoTab("New_Patient_HomePhone");
        U.PhoneAutoTab("New_Patient_MobilePhone");
        U.PhoneAutoTab("New_Patient_PharmacyPhone");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhonePrimary");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhoneAlternate");
        U.ShowIfChecked($("#New_Patient_DMEOther"), $("#New_Patient_OtherDME"));
        U.ShowIfChecked($("#New_Patient_PaymentSource"), $("#New_Patient_OtherPaymentSource"));
        U.InitNewTemplate("Patient", function() {
            if ($('#New_Patient_Status').val() == "1") {
                Patient.Rebind();
                UserInterface.ShowPatientPrompt();
            }
        });
        $("#window_newpatient .Physicians").PhysicianInput();
    },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Patient_HomePhone");
        U.PhoneAutoTab("Edit_Patient_MobilePhone");
        U.PhoneAutoTab("Edit_Patient_PharmacyPhone");
        $("#window_editpatient .Physicians").PhysicianInput();
        $("#EditPatient_NewPhysician").click(function() {
            var PhysId = $("#EditPatient_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Patient.AddPhysician(PhysId, $("#Edit_Patient_Id").val());
            else U.Growl("Please Select a Physician", "error");
        });
        U.InitEditTemplate("Patient");
    },
    Verify: function(medicareNumber, lastName, firstName, dob, gender) {
        var input = "medicareNumber=" + medicareNumber + "&lastName=" + lastName + "&firstName=" + firstName + "&dob=" + dob + "&gender=" + gender;
        U.PostUrl("/Patient/Verify", input, function(result) {
            alert(result);
        });
    },
    InitCenter: function() {
        $("#txtSearch_Patient_Selection").keyup(function() {
            if ($(this).val()) {
                Patient.Filter($(this).val())
                if ($("tr.match:even", "#PatientSelectionGrid .t-grid-content").length) $("tr.match:first", "#PatientSelectionGrid .t-grid-content").click();
                else $("#PatientMainResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3C" +
                    "div class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javasc" +
                    "ript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
            } else {
                $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match t-alt").show();
                $("tr:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
                $("tr:first", "#PatientSelectionGrid .t-grid-content").click();
            }
        });
        var status = Patient._patientStatus;
        $("#window_patientcenter .top select").change(function() {
            Patient._patientId = "";
            Patient._patientStatus = "";
            U.FilterResults("Patient");

        });
        $('#window_patientcenter_content .t-grid-content').css({ height: 'auto' });
        $('#window_patientcenter .layout').layout({ west__paneSelector: '.layout-left' });
        if (Patient._patientStatus == "true") {
            $("select.PatientStatusDropDown").val(2);
            $("select.PatientStatusDropDown").change();
        }
        Patient._patientStatus = status;
    },
    InitPatientReadmitted: function() { U.InitTemplate($("#readmitForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; Patient.Rebind(); }, "Patient Re-admitted successfully."); },
    InitChangePatientStatus: function() { U.InitTemplate($("#changePatientStatusForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; Patient.Rebind(); }, "Patient status updated successfully."); },
    InitDischargePatient: function() { U.InitTemplate($("#dischargePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is discharged successfully."); },
    InitActivatePatient: function() { U.InitTemplate($("#activatePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is activated successfully."); },
    InitNewPhoto: function() {
        $("#changePatientPhotoForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Patient photo updated successfully.", "success");
                            Patient.Rebind();
                            UserInterface.CloseModal();
                        }
                        else {
                            alert(result.responseText);
                        }
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Patient photo updated successfully.", "success");
                            Patient.Rebind();
                            UserInterface.CloseModal();
                        }
                        else {
                            alert(result.responseText);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RemovePhoto: function(patientId) {
        if (confirm("Are you sure you want to remove this photo?")) {
            U.PostUrl("/Patient/RemovePhoto", "patientId=" + patientId, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Patient photo has been removed successfully.", "success");
                    Patient.Rebind();
                    UserInterface.CloseModal();
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    PatientRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).html();
                    }
                });
            } else $(this).hide();
        });
    },
    ActivityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).html();
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) {
            $(e.row).addClass('darkgreen');
            if (dataItem.StatusName == "Missed Visit(complete)") {
                $(e.row).removeClass('darkgreen');
                $(e.row).addClass('darkred');
            }
        }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append($("<li/>", { "text": "Reopen Task" }).click(function() {
                $(e.row).find("a:contains('Reopen Task')").click();
            }));
            else if (!dataItem.IsOrphaned) Menu.append($("<li/>", { "text": "Edit Note" }).click(function() {
                $(e.row).find("a:first").click();
            }));
            Menu.append($("<li/>", { "text": "Details" }).click(function() {
                $(e.row).find("a:contains('Details')").click();
            })).append($("<li/>", { "text": "Delete" }).click(function() {
                $(e.row).find("a:contains('Delete')").click();
            })).append($("<li/>", { "text": "Print" }).click(function() {
                $(e.row).find(".print").parent().click();
            }));
            Menu.ContextMenu(Event);
        });
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            if (!Patient._isRebind) {
                Patient._dateFilter = "";
                Patient._showFilter = "";
                Patient._fromDate = "";
                Patient._toDate = "";
            }
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Patient.LoadInfoAndActivity(patientId);
        }
    },
    LoadInfoAndActivity: function(patientId) {
        $("#patient-info").remove();
        $('#PatientMainResult').empty().addClass('loading').load('Patient/Data', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#PatientMainResult').removeClass('loading');
            Patient.SetId(patientId);
            if (textStatus == 'error') $('#PatientMainResult').html(U.AjaxError);
            $('#window_patientcenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    LoadEditPatient: function(id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() {
            Patient.InitEdit();
            Lookup.LoadAdmissionSources();
            Lookup.LoadRaces();
            Lookup.LoadUsers();
            Lookup.LoadReferralSources();
        }, { patientId: id });
    },
    LoadEditAuthorization: function(patientId, id) {
        Acore.Open("editauthorization", 'Authorization/Edit', function() {
            Patient.InitEditAuthorization(patientId);
        }, { patientId: patientId, Id: id });
    },
    InitNewOrder: function() {
        Template.OnChangeInit();
        $("#New_Order_PhysicianDropDown").PhysicianInput();
        $("#newOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('neworder');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    SubmitOrder: function(control, page) {
        control.closest("form").validate();
        if (control.closest("form").valid()) {
            var options = {
                dataType: 'json',
                beforeSubmit: function(values, form, options) { },
                success: function(result) {
                    if (result.isSuccessful) {
                        if (control.html() == "Save") {
                            U.Growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page);
                            U.Growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page);
                            U.Growl("The Physician Order has been saved and completed successfully.", "success");
                        }
                    } else U.Growl(result.errorMessage, "error");
                }
            };
            $(control.closest("form")).ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    InitNewFaceToFaceEncounter: function() {
        $("#newFaceToFaceEncounterForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('newfacetofaceencounter');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewCommunicationNote: function() {
        Template.OnChangeInit();
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        $("#newCommunicationNoteForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        U.UnBlockButton($(form).find(".blocked-button"));
                        if (resultObject.isSuccessful) {
                            U.Growl("New Communication Note saved successfully.", "success");
                            UserInterface.CloseWindow('newcommnote');
                            Patient.Rebind();
                        } else U.Growl(result.errorMessage, "error");
                    }

                };
                $(form).ajaxSubmit(options);
                return false;
            },
            invalidHandler: function(form, validator) {
                U.UnBlockButton($(this).find(".blocked-button"));
            }
        });
    },
    InitNewAuthorization: function() {
        $(".numeric").numeric();
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        $("#newAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        U.Growl(result.errorMessage, "success");

                        if (result.isSuccessful) {
                            var patientId = $("#New_Authorization_PatientId").val();
                            UserInterface.CloseWindow('newauthorization');
                            Patient.RebindAuthorizationGrid(patientId);
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditAuthorization: function(patientId) {

        $(".numeric").numeric();
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        $("#editAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('editauthorization');
                            Patient.RebindAuthorizationGrid(patientId);
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditOrder: function() {
        Template.OnChangeInit();
        $("#Edit_Order_PhysicianDropDown").PhysicianInput();
    },
    Rebind: function() {
        Patient._isRebind = true;
        U.RebindTGrid($('#PatientSelectionGrid'));
        U.RebindTGrid($('#List_Patient_Grid'));
    },
    LoadNewOrder: function(patientId) {
        Acore.Open("neworder", 'Order/New', function() {
            Patient.InitNewOrder();
        }, { patientId: patientId });
    },
    DeleteOrder: function(id, patientId, episodeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Patient/DeleteOrder", { id: id, patientId: patientId, episodeId: episodeId }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    var grid = $('#List_PatientOrdersHistory').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId }); }

                    var grid = $('#List_EpisodeOrders').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId, episodeId: episodeId }); }

                    if ($("#Billing_CenterContent").val() != null && $("#Billing_CenterContent").val() != undefined) {
                        Billing.ReLoadUnProcessedClaim('#Billing_CenterContentFinal', $('#Billing_FinalCenter_BranchCode').val(), $('#Billing_FinalCenter_InsuranceId').val(), 'FinalGrid', 'Final');
                    }

                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    LoadNewCommunicationNote: function(patientId) {
        Acore.Open("newcommnote", 'CommunicationNote/New', function() {
            Patient.InitNewCommunicationNote();
        }, { patientId: patientId });
    },
    LoadNewAuthorization: function(patientId) {
        Acore.Open("newauthorization", 'Authorization/New', function() {
            Patient.InitNewAuthorization();
        }, { patientId: patientId });
    },
    LoadNewInfectionReport: function(patientId) {
        Acore.Open("newinfectionreport", 'Infection/New', function() {
            InfectionReport.InitNew();
        }, { patientId: patientId });
    },
    LoadNewIncidentReport: function(patientId) {
        Acore.Open("newincidentreport", 'Incident/New', function() { IncidentReport.InitNew(); }, { patientId: patientId });
    },
    InfoPopup: function(e) {
        var $dialog = $("#patient-info");
        $dialog.dialog({
            width: 500,
            position: [$(e).offset().left, $(e).offset().top],
            modal: false,
            resizable: false,
            close: function() {
                $dialog.dialog("destroy");
                $dialog.hide();
            }
        }).show();
        $('#ui-dialog-title-patientInfo').html('Patient Info');
    },
    AddPhysRow: function(el) {
        $(el).closest('.column').find('.row.hidden').first().removeClass('hidden').find("input:first").blur()
    },
    RemPhysRow: function(el) {
        $(el).closest('.row').addClass('hidden').appendTo($(el).closest('.column')).find('select').val('00000000-0000-0000-0000-000000000000')
    },
    InitMedicationProfileSnapshot: function(assessmentType) {
        U.PhoneAutoTab("MedProfile_PharmacyPhone");
        $("#window_medicationprofilesnapshot .Physicians").PhysicianInput();
        $("#newMedicationProfileSnapShotForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        if (assessmentType != undefined) {
                            var assessmentId = $(assessmentType + "_Id").val();
                            if (assessmentId != undefined && assessmentId != "") {
                                values.push({ name: "AssociatedAssessment", value: assessmentId });
                            }
                        }
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('medicationprofilesnapshot');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    LoadMedicationProfileSnapshot: function(patientId, assessmentType) {
        Acore.Open("medicationprofilesnapshot", 'Patient/MedicationProfileSnapShot', function() { Patient.InitMedicationProfileSnapshot(assessmentType); }, { patientId: patientId });
    },
    LoadMedicationProfile: function(patientId) {
        Acore.Open("medicationprofile", 'Patient/MedicationProfile', function() { }, { patientId: patientId });
    },
    LoadMedicationProfileSnapshotHistory: function(patientId) {
        Acore.Open("medicationprofilesnapshothistory", 'Patient/MedicationProfileSnapShotHistory', function() { }, { patientId: patientId });
    },
    LoadPatientCommunicationNotes: function(patientId) {
        Acore.Open("patientcommunicationnoteslist", 'Patient/PatientCommunicationNotesView', function() { }, { patientId: patientId });
    },
    OnMedicationProfileEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 7) {
                    if (e.dataItem['MedicationCategory'] != 'DC') {
                        $(this).html('');
                    }
                }
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 6) {
                    if (e.dataItem['Classification'] == null || e.dataItem['Classification'] == '') {
                        $(this).find("input[name=MedicationClassification]").val('');
                    }
                }
            });
        }
    },
    OnPlanofCareMedicationEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
        }
    },
    OnMedicationProfileRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass('t-alt').addClass('red');
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        }
        else {
            e.row.cells[7].innerHTML = '<a class="t-grid-action t-button t-state-default" href="javascript:void(0);" onclick="' + dataItem.DischargeUrl + '">D/C</a>';
        }

        if (dataItem.DrugId != null && dataItem.DrugId.length > 0) {
            var medline = 'http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.c=' + dataItem.DrugId;
            e.row.cells[2].innerHTML = '<a href="' + medline + '" target="_blank">' + dataItem.MedicationDosage + '</a>';
        }

        e.row.cells[1].innerHTML = dataItem.StartDateSortable;
    },
    OnMedicationProfileSnapShotRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined) {
            e.row.cells[1].innerHTML = dataItem.StartDateSortable;
        }
    },
    OnMedProfileSnapShotHistoryRowDataBound: function(e) {
        var dataItem = e.dataItem;
        e.row.cells[1].innerHTML = dataItem.SignedDateFormatted;
    },
    OnDataBound: function(e) {
        var id = $("#medicationProfileHistroyId").val();
        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        medicatonProfileGridDC.rebind({ medId: id, medicationCategry: "DC", assessmentType: "" });
    },

    DischargeMed: function(medId, patientId, assessmentType) {
        $("#Discharge_Medication_Container").load("/Patient/MedicationDischarge", function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("Could not load the Medication Discharge page.", "error");
            }
            else if (textStatus == "success") {
                U.ShowDialog("#Discharge_Medication_Container", function() {
                    if (assessmentType != undefined || assessmentType != null && assessmentType.length > 0) {
                        Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshOasisMedProfile(medId, assessmentType); });
                    } else {
                        Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshMedProfile(medId); });
                    }
                });
            }
        });
    },
    InitDischargeMedication: function(medId, patientId, callback) {
        $("#dischargeMedicationProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    data: { medId: medId, patientId: patientId },
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            UserInterface.CloseModal();
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.Growl(result.errorMessage, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RefreshMedProfile: function(medId) {
        var medicatonProfileGridActive = $('#MedicatonProfileGridActive').data('tGrid');
        medicatonProfileGridActive.rebind({ medId: medId, medicationCategry: "Active", assessmentType: "" });

        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        medicatonProfileGridDC.rebind({ medId: medId, medicationCategry: "DC", assessmentType: "" });
    },
    RefreshOasisMedProfile: function(medId, assessmentType) {
        var oasisMedGrid = $('#Oasis' + assessmentType + 'MedicationGrid').data('tGrid');
        if (oasisMedGrid != null) {
            oasisMedGrid.rebind({ medId: medId, medicationCategry: "Active", assessmentType: assessmentType });
        }
    },
    DateRange: function(id) {
        if (!Patient._isRebind) {
            Patient._dateFilter = id;
        }
        var data = 'dateRangeId=' + id + "&patientId=" + $("#PatientCenter_PatientId").val();
        $.ajax({
            url: '/Patient/DateRange',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                $("#date-range-text").empty();
                if (result.StartDateFormatted == "01/01/0001" || result.EndDateFormatted == "01/01/0001") {
                    $("#date-range-text").append("No Episodes Found");
                }
                else {
                    $("#date-range-text").append(result.StartDateFormatted + "-" + result.EndDateFormatted);
                }
            }
        });
    },
    CustomDateRange: function() {
        if (!Patient._isRebind) {
            Patient._fromDate = $("#patient-activity-from-date").val();
            Patient._toDate = $("#patient-activity-to-date").val();
        }
        var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
        if (PatientActivityGrid != null) {
            PatientActivityGrid.rebind({
                patientId: $("#PatientCenter_PatientId").val(),
                discipline: $("select.patient-activity-drop-down").val(),
                dateRangeId: $("select.patient-activity-date-drop-down").val(),
                rangeStartDate: $("#patient-activity-from-date").val(),
                rangeEndDate: $("#patient-activity-to-date").val()
            })
        }
    },
    DeleteSchedule: function(episodeId, patientId, eventId, employeeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId;
            U.PostUrl("/Schedule/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Patient.CustomDateRange();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                }
            });
        }
    },
    Delete: function(patientId) {
        if (confirm("Are you sure you want to delete this patient?")) {
            var input = "patientId=" + patientId;
            U.PostUrl("/Patient/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    U.Growl(resultObject.errorMessage, "success");
                    Patient.LoadPateintListContent('#Patient_List_MainContainer', '#List_Patient_BranchId', '#List_Patient_Status', 'DisplayName-ASC');
                    Patient.Rebind();
                    Schedule.Rebind();

                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    InitAdmit: function(patientId) {
        $("#newAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Patient admission successful.", "success");
                            UserInterface.CloseWindow('admitpatient');
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            Schedule.Rebind();
                            U.RebindTGrid($('#List_Patient_NonAdmit_Grid'));
                            U.RebindTGrid($('#List_PatientPending_Grid'));
                            UserInterface.ShowPatientPrompt();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Patient_Date").datepicker("hide");
        $("#newNonAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Patient non-admission successful.", "success");
                            UserInterface.CloseWindow('nonadmitpatient');
                            UserInterface.CloseModal();
                            U.RebindTGrid($('#List_PatientPending_Grid'));
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    LoadEditCommunicationNote: function(patientId, id) {
        Acore.Open("editcommunicationnote", 'Patient/EditCommunicationNote', function() {
            $("#editCommunicationNoteForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                U.Growl(resultObject.errorMessage, "success");
                                UserInterface.CloseWindow('editcommunicationnote');
                                Patient.Rebind();
                                Schedule.Rebind();
                            } else U.Growl(resultObject.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    LoadEditEmergencyContact: function(patientId, id) {
        Acore.Open("editemergencycontact", 'Patient/EditEmergencyContactContent', function() {
            U.PhoneAutoTab("Edit_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("Edit_EmergencyContact_AlternatePhoneArray");
            $("#editEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl("Edit emergency contact is successful.", "success");
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: patientId });
                                }
                                UserInterface.CloseWindow('editemergencycontact');
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    LoadNewEmergencyContact: function(id) {
        Acore.Open("newemergencycontact", 'Patient/NewEmergencyContactContent', function() {
            U.PhoneAutoTab("New_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("New_EmergencyContact_AlternatePhoneArray");
            $("#newEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl("New emergency contact added successful.", "success");
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: id });
                                }
                                UserInterface.CloseWindow('newemergencycontact');
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        }, { patientId: id });
    },
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeleteEmergencyContact",
                data: "id=" + id + "&patientId=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                        if (emergencyContact != null) {
                            emergencyContact.rebind({ PatientId: patientId });
                        }
                    }
                }
            }
            );
        }
    },
    DeleteCommunicationNote: function(Id, patientId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Patient/DeleteCommunicationNote", { Id: Id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var communicationNoteGrid = $('#List_CommunicationNote').data('tGrid');
                    if (communicationNoteGrid != null) {
                        U.Growl(result.errorMessage, "success");
                        communicationNoteGrid.rebind();
                    }
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    AddPhysician: function(id, patientId) {
        U.TGridAjax("/Patient/AddPatientPhysicain", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
        $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, patientId) {
        if (confirm("Are you sure you want to delete this physician?")) U.TGridAjax("/Patient/DeletePhysicianContact", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
    },
    DeleteAuthorization: function(patientId, id) {
        if (confirm("Are you sure you want to delete this authorization?")) {
            U.PostUrl("/Patient/DeleteAuthorization", { Id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    Patient.RebindAuthorizationGrid(patientId);
                    U.Growl(result.errorMessage, "success");
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });

        }

    },
    SetPrimaryPhysician: function(id, patientId) {
        if (confirm("Are you sure you want to set this physician as primary?")) {
            U.TGridAjax("/Physician/SetPrimary", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
            Patient.Rebind();
        }
    },
    OnSocChange: function() {
        var date = $("#New_Patient_StartOfCareDate").datepicker("getDate");
        $("#New_Patient_EpisodeStartDate").datepicker("setDate", date)
        $("#New_Patient_EpisodeStartDate").datepicker("option", "mindate", date);
    },
    PatientListDataBound: function() {
        Patient.Filter($("#txtSearch_Patient_Selection").val());
        if ($("#PatientSelectionGrid .t-grid-content tr").length) {
            if (Patient._patientId == "") $('#PatientSelectionGrid .t-grid-content tr' + ($("#txtSearch_Patient_Selection").val().length ? ".match" : "")).eq(0).click();
            else $('td:contains(' + Patient._patientId + ')', $('#PatientSelectionGrid')).closest('tr').click();
        } else $("#PatientMainResult").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
        if ($("#PatientSelectionGrid .t-state-selected").length) $("#PatientSelectionGrid .t-grid-content").scrollTop($("#PatientSelectionGrid .t-state-selected").position().top - 50);

    },
    UpdateOrderStatus: function(eventId, patientId, episodeId, orderType, actionType) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(data.errorMessage, "success");
                    } else U.Growl(data.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(data.errorMessage, "success");
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    ProcessCommunicationNote: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    RebindOrdersHistory: function() {
        var grid = $('#List_PatientOrdersHistory').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#PatientOrdersHistory_PatientId").val(), StartDate: $("#PatientOrdersHistory_StartDate-input").val(), EndDate: $("#PatientOrdersHistory_EndDate-input").val() }); }
        var $exportLink = $('#PatientOrdersHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#PatientOrdersHistory_PatientId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#PatientOrdersHistory_StartDate-input").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#PatientOrdersHistory_EndDate-input").val());
        $exportLink.attr('href', href);
    },
    LoadPatientAdmissionPeriods: function(Id) { Acore.Open("patientmanageddates", 'Patient/AdmissionPeriod', function() { }, { patientId: Id }); },
    LoadInsuranceContent: function(contentId, patientId, insuranceId, action, insuranceType) { $(contentId).load('Patient/InsuranceInfoContent', { PatientId: patientId, InsuranceId: insuranceId, Action: action, InsuranceType: insuranceType }, function(responseText, textStatus, XMLHttpRequest) { }); },
    LoadPateintListContent: function(contentId, branchId, statusId, SortParams) {
        $(contentId).empty().addClass("loading").load('Patient/ListContent', { BranchId: $(branchId).val(), Status: $(statusId).val(), SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("Could not load patient list. Try again.", "error");
            }
            else if (textStatus == "success") {
                $(contentId).removeClass("loading");
                var $exportLink = $('#List_Patient_ExportLink');
                var href = $exportLink.attr('href');
                if (href != null && href != undefined) {
                    href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $(branchId).val());
                    href = href.replace(/Status=([^&]*)/, 'Status=' + $(statusId).val());
                    $exportLink.attr('href', href);
                }
            }
        });
    },
    LoadPatientAdmissionContent: function(contentId, patientId) {
        $(contentId).empty().addClass("loading").load('Patient/AdmissionPeriodContent', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { U.Growl("Could not load this page. Try again.", "error"); }
            else if (textStatus == "success") { $(contentId).removeClass("loading"); }
        });
    },
    DeletePatientAdmission: function(patientId, Id) {
        if (confirm("Are you sure you want to delete this admission period?")) {
            U.PostUrl("/Patient/DeletePatientAdmission", { patientId: patientId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer', patientId);
                    U.Growl(result.errorMessage, "success");
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    InitAdmissionPatient: function(patientId, type) {
        U.PhoneAutoTab(type + "_Admission_HomePhone");
        U.PhoneAutoTab(type + "_Admission_MobilePhone");
        U.PhoneAutoTab(type + "_Admission_PharmacyPhone");
        $("#window_" + type.toLowerCase() + "patientadmission .Physicians").PhysicianInput();
        $("#" + type.toLowerCase() + "PatientAdmissionForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer', patientId);
                            UserInterface.CloseWindow(type.toLowerCase() + 'patientadmission');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    MarkPatientAdmissionCurrent: function(patientId, Id) {
        if (confirm("Are you sure you want to mark this admission active? This will update this admission with the current patient information.")) {
            U.PostUrl("/Patient/MarkPatientAdmissionCurrent", { patientId: patientId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    Patient.LoadPatientAdmissionContent('#PatientAdmissionPeriodContainer', patientId);
                    U.Growl(result.errorMessage, "success");
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    RestoreDeleted: function(patientId) {
        if (confirm("Are you sure you want to restore this patient?")) {
            U.PostUrl("/Patient/RestoreDeleted", { PatientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    U.RebindDataGridContent('List_Patient_Deleted', 'Patient/DeletedPatientContent', { BranchId: $('#List_Patient_Deleted_BranchId').val() }, 'DisplayName-ASC');
                    Patient.Rebind();
                    Schedule.Rebind();
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    SelectPatient: function(patientId, status) {
        var selectionFilter = $("#txtSearch_Patient_Selection");
        if (selectionFilter.val() != "") {
            $("#txtSearch_Patient_Selection").val("");
            $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match t-alt").show();
            $("tr:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
        }

        var statusNumber = !isNaN(status) ? status : (status.toLowerCase() == "false" ? "1" : "2");
        var statusDropDown = $("select.PatientStatusDropDown");
        if (statusNumber != statusDropDown.val()) {
            statusDropDown.val(statusNumber);
            Patient.SetId(patientId);
            U.RebindTGrid($('#PatientSelectionGrid'), { branchId: $("select.PatientBranchCode").val(), statusId: $("select.PatientStatusDropDown").val(), paymentSourceId: $("select.PatientPaymentDropDown").val() });
        }
        else {
            var row = $("#PatientSelectionGrid .t-grid-content tr td:contains('" + patientId + "')").parent();
            $("#PatientSelectionGrid tr.t-state-selected").removeClass("t-state-selected");
            row.addClass("t-state-selected");
            var scroll = $(row).position().top + $(row).closest(".t-grid-content").scrollTop() - 24;
            $(row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            Patient.LoadInfoAndActivity(patientId);
        }
    },
    RebindAuthorizationGrid: function(patientId) {
        var authorizationGrid = $('#List_Authorizations').data('tGrid');
        if (authorizationGrid != null) {
            var selectUrl = authorizationGrid.ajax.selectUrl;
            var match = $('#List_Authorizations').data('tGrid').ajax.selectUrl.match("patientId=(.*)");
            if (match.length > 0 && match[1] == patientId) {
                authorizationGrid.rebind({ patientId: patientId });
            }
        }
    }
}
