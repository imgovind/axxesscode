﻿var OasisValidation = {
    Validate: function(id, patientId, episodeId, assessmentType, showPpsPlus) {
        var buttonArray = [
            { Text: "OASIS Scrubber", Click: function() { U.GetAttachment("Oasis/AuditPdf", { id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType }) } },
            { Text: "OASIS File", Click: function() { U.GetAttachment("Oasis/GenerateExportFile", { assessmentId: id, assessmentType: assessmentType }) } }
        ];

        if (showPpsPlus) {
            buttonArray.push({
                Text: "PPSPlus Export",
                Click: function() {
                    U.PostUrl("Oasis/PpsExport", {
                        assessmentId: id,
                        assessmentType: assessmentType
                    }, function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                        } else U.Growl(result.errorMessage, "error");
                    });
                }
            });
        }
        Acore.OpenPrintView({
            Url: "Validate/" + id + "/" + patientId + "/" + episodeId + "/" + assessmentType,
            Buttons: buttonArray
        });
        $("#print-controls a:last").text("Return to OASIS");
        $("#print-controls li:first").addClass("red").tooltip({
            track: true,
            showURL: false,
            top: 25,
            left: 25,
            extraClass: "calday",
            bodyHandler: function() {
                return "<div class='strong'>Powered by</div><img src='/Images/hhg-logo.png'/>";
            }
        });
    },
    ExportRegenerate: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Regenerate",
         { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType },
         function(data) {
             if (data.isSuccessful) {
                 U.Growl(data.errorMessage, "success");
                 Agency.RebindOasisToExport("OasisExport");
             }
             else {
                 if (confirm("The OASIS assessment contains validation errors. Would you like to open this OASIS assessment to resolve the errors?")) {
                     eval(assessmentType + ".Load('" + id + "','" + patientId + "','" + assessmentType + "')");
                 }
             }
         },
        function() { }
        );
    },
    ConvertToTime: function(time) {
        if (time != undefined) {
            var hours = +time.substring(0, 2);
            var minutes = +time.substring(3, 5);
            var pm = time.indexOf("P") != -1;
            if (pm && hours !== 12) {
                hours += 12;
            }
            var date = new Date();
            date.setHours(hours);
            date.setMinutes(minutes);
            return date;
        }
        else
            return new Date();
    },
    CheckTimeInOut: function(prefix, showMessage) {
        var tempTimeIn = $("#printview").contents().find("#" + prefix + "_TimeIn").val();
        var tempTimeOut = $("#printview").contents().find("#" + prefix + "_TimeOut").val();
        var timeIn = OasisValidation.ConvertToTime(tempTimeIn);
        var timeOut = OasisValidation.ConvertToTime(tempTimeOut);
        var timeOutNextDay = OasisValidation.ConvertToTime(tempTimeOut);
        timeOutNextDay.setDate(timeOutNextDay.getDate() + 1);
        if (timeIn && timeOut) {
            var timeDiff = (timeOut - timeIn) / 60 / 60 / 1000;
            var timeDiffNextDay = (timeOutNextDay - timeIn) / 60 / 60 / 1000;
            var over3Hours = timeDiff > 3 || timeDiff < 0;
            var over3HoursNextDay = timeDiffNextDay > 3 || timeDiffNextDay < 0;
            if (over3Hours && !over3HoursNextDay)
                over3Hours = false;
            var defaultClick = "$('#" + prefix + "Form').focus();";
            if (over3Hours == true) {
                if (showMessage == true) {
                    var growl = $(".jGrowl-notification").text();
                    if (growl == "") {
                        U.Growl("WARNING: The Time In and Out are over 3 hours apart.", "warning");
                    }
                }
                else {
                    var answer = confirm('The Time Out is 3 hours greater than the Time In. Would you like to continue?');
                    return !answer;
                }
            } else {
                return false;
            }
        }
    }
}