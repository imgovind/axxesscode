﻿var Report = {
    Init: function() {
        $(".report-output.buttons a").click(Report.Home);
        $(".reports a.report-link").click(Report.Show);
        Report.Home();
    },
    Show: function(url) {
        if (!url.length) url = $(this).attr("href");
        $(".report-home").hide();
        $("#report-output").empty().show().addClass("loading").Load(url, function(r, t, x) {
            $(".report-output").show();
            $("#report-output").removeClass("loading");
            if (t == "success") {
                $("#report-output .report-grid").css("top", $("#window_reportcenter_content > .wrapper").height());
                var total = $("#window_reportcenter div.t-grid-content tr").not(".t-no-data").length;
                $("#window_reportcenter").Status("Total: " + total);
            }
            else $("#report-output").html(U.AjaxError);
        })
        return false;
    },
    Home: function() {
        $(".report-output").hide();
        $(".report-home").show();
        $(this).closest(".window").Status("");
    },
    loadUsersDropDown: function(reportName) {
        U.PostUrl("User/BranchList", {
            branchId: $("#" + reportName + "_BranchCode").val(),
            status: $("#" + reportName + "_StatusId").val()
        }, function(data) {
            var s = $("select#" + reportName + "_UserId");
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select User --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false)
            })
        })
    },
    loadPatientsDropDown: function(reportName) {
        U.PostUrl("Patient/BranchList", {
            branchId: $("#" + reportName + "_BranchCode").val(),
            status: $("#" + reportName + "_StatusId").val()
        }, function(data) {
            var s = $("select#" + reportName + "_PatientId");
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Patient --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false)
            })
        })
    },
    RebindReportGridContent: function(reportId, action, jsonData, sortParams) {
        var input = jsonData;
        if (sortParams != null && sortParams != undefined) {
            $.extend(input, { SortParams: sortParams });
        }
        $("#" + reportId + "GridContainer").empty().addClass("loading").load('Report/' + action, input, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.Growl("This report can't be loaded. Please close this window and try again.", 'error');
            }
            else if (textStatus == "success") {
                $(this).removeClass("loading");
                var total = $("#" + reportId + "GridContainer div.t-grid-content tr").not(".t-no-data").length;
                $(this).closest(".window").Status("Total: " + total);
                var $exportLink = $('#' + reportId + '_ExportLink');
                if ($exportLink != null && $exportLink != undefined) {
                    var href = $exportLink.attr('href');
                    if (href != null && href != undefined) {
                        $.each(jsonData, function(key, value) {
                            var filter = new RegExp(key + "=([^&]*)");
                            href = href.replace(filter, key + '=' + value);
                        });
                        $exportLink.attr('href', href);
                    }
                }
            }
        });
    },
    RebindClinicalOrders: function() {
        var grid = $('#Report_Patient_Orders_Grid').data('tGrid');
        if (grid != null) { grid.rebind({ StatusId: $("#Report_Patient_Orders_Status").val() }); }
    },
    RebindCompletedList: function() { U.RebindTGrid($('#List_CompletedReports')); },
    RequestReport: function(url, branchId, year) {
        U.PostUrl(url, { BranchId: $(branchId).val(), Year: $(year).val() }, function(result) {
            if (result.isSuccessful) U.Growl(result.errorMessage, "success");
            else U.Growl(result.errorMessage, "error");
        });
    },
    RequestReportWithRange: function(url, branchId, startDate, endDate) {
        U.PostUrl(url, { BranchId: $(branchId).val(), StartDate: $(startDate).val(), EndDate: $(endDate).val() }, function(result) {
            if (result.isSuccessful) U.Growl(result.errorMessage, "success");
            else U.Growl(result.errorMessage, "error");
        });
    },
    RequestReportWithRangeAndInsurance: function(url, branchId, insuranceId, startDate, endDate) {
        U.PostUrl(url, { BranchId: $(branchId).val(), InsuranceId: $(insuranceId).val(), StartDate: $(startDate).val(), EndDate: $(endDate).val() }, function(result) {
            if (result.isSuccessful) U.Growl(result.errorMessage, "success");
            else U.Growl(result.errorMessage, "error");
        });
    },
    Delete: function(completedReportId) {
        if (confirm("Are you sure you want to delete this Report?")) {
            U.PostUrl("Request/DeleteReport", {
                completedReportId: completedReportId
            }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Report.RebindCompletedList();
                }
                else U.Growl(result.errorMessage, "error");
            });
        }
    }
}