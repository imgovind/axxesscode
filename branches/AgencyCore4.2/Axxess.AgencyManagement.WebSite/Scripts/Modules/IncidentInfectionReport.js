﻿var IncidentReport = {
    Delete: function(patientId, episodeId, id) {
        U.DeleteTemplateWithInput("IncidentReport", { PatientId: patientId, EpisodeId: episodeId, Id: id },
        function() {
            Schedule.Rebind();
            Patient.Rebind();
        }, "Incident Log");
    },
    InitEdit: function() {
        U.InitEditTemplate("IncidentReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        }, "Incident Log");
        $("#window_editincidentreport .Physicians").PhysicianInput();
    },
    InitNew: function() {
        U.InitNewTemplate("IncidentReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        }, "incident log");
        U.ShowIfOtherChecked($("#New_Incident_IndividualInvolved4"), $("#New_Incident_IndividualInvolvedOther"));
        $("#window_newincidentreport .Physicians").PhysicianInput();
    },
    RebindList: function() { U.RebindTGrid($('#List_IncidentReport')); },
    ProcessIncident: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }

}

var InfectionReport = {
Delete: function(patientId, episodeId, id) {
U.DeleteTemplateWithInput("InfectionReport", { PatientId: patientId, EpisodeId: episodeId, Id: id }, function() { Schedule.Rebind(); Patient.Rebind(); }); },
    InitEdit: function() {
        U.InitEditTemplate("InfectionReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        $("#window_editinfectionreport .Physicians").PhysicianInput();
    },
    InitNew: function() {
        U.InitNewTemplate("InfectionReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        }, "infection report");
        U.ShowIfOtherChecked($("#New_Infection_InfectionType6"), $("#New_Infection_InfectionTypeOther"));
        $("#window_newinfectionreport .Physicians").PhysicianInput();
    },
    RebindList: function() { U.RebindTGrid($('#List_InfectionReport')); },
    ProcessInfection: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }
}