﻿$.extend(Visit, {
    HHAideVisit: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            U.ShowIfSelectEquals($(Prefix + "HomeboundStatus"), "8", $(Prefix + "HomeboundStatusOther"));
            U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
            U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
            Visit.Shared.Init(Type, Visit.HHAideVisit.Init);
        },
        Submit: function(Button, Completing) { Visit.Shared.Submit(Button, Completing, "HHAideVisit") },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("HHAideVisit", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "HHAideVisit") }
    },
    HomeMakerNote: {
        Init: function(ResponseText, TextStatus, XMLHttpRequest, Element) {
            var Type = $("input[name=Type]", Element).val(), Prefix = "#" + Type + "_";
            Visit.Shared.Init(Type, Visit.HomeMakerNote.Init);
        },
        Submit: function(Button, Completing) { Visit.Shared.Submit(Button, Completing, "HomeMakerNote") },
        Load: function(EpisodeId, PatientId, EventId) { Acore.Open("HomeMakerNote", { episodeId: EpisodeId, patientId: PatientId, eventId: EventId }) },
        Print: function(EpisodeId, PatientId, EventId, QA) { Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, "HomeMakerNote") }
    }
    
});