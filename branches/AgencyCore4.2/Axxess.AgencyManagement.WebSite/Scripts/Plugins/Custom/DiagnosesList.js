(function($) {
    $.extend($.fn, {
        DiagnosesList: function(Options) {
            return this.each(function() {
                // If control is not already enabled with this plugin
                if (!$(this).hasClass("diagnoses-list")) {
                    // Set variable for diagnoses list
                    var List = $(this),
                        NumberOfRows = 0,
                        isOasis = Options.Assessment != "Edit485";
                    $.each(Options, function(Index, Value) {
                        if (Index.match(/^_M1022PrimaryDiagnosis[0-9]*$/)) NumberOfRows = parseInt(Index.replace(/^_M1022PrimaryDiagnosis/, ""));
                    });
                    NumberOfRows++;
                    Options.List = List;
                    // Build column title row
                    List.addClass("diagnoses-list").attr("assessment", Options.Assessment).append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_title" }).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": isOasis ? "Column 1" : "Principal Diagnosis" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": isOasis ? "Column 2" : "ICD-9-C M Code" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": isOasis ? "Column 3" : "O/E" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": isOasis ? "Column 4" : "Date" })
                            )
                        )
                    );
                    // If OASIS, build instruction row
                    if (isOasis) List.append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_title" }).append(
                            $("<div/>", { "class": "diagnosis_colwidth" }).append(
                                $("<p/>", { "text": "Sequencing of diagnoses should reflect the seriousness of each condition and support the disciplines and services provided." })).append(
                                $("<label/>", { "class": "strong", "text": " Primary Diagnosis", "for": (isOasis ? Options.Assessment : "") + "_M1020PrimaryDiagnosis" }).prepend(
                                    $("<a/>", { "class": "green", "href": "javascript:void(0)", "title": "More Information about M1020", "text": "(M1020)" }).click(function() {
                                        Oasis.ToolTip('M1020')
                                    })
                                )
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth" }).append(
                                $("<p/>", { "text": "ICD-9-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses." })).append(
                                $("<label/>", { "class": "strong", "text": "ICD-9-C M/ Symptom Control Rating", "for": (isOasis ? Options.Assessment : "") + "_M1020ICD9M" })).append(
                                $("<em/>", { "text": "(V-codes are allowed)" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth" }).append(
                                $("<p/>", { "text": "Complete if a V-code is assigned under certain circumstances to Column 2 in place of a case mix diagnosis." })).append(
                                $("<label/>", { "class": "strong", "text": " Description/ ICD-9-C M", "for": (isOasis ? Options.Assessment : "") + "_M1024ICD9MA3" }).prepend(
                                    $("<a/>", { "class": "green", "href": "javascript:void(0)", "title": "More Information about M1024", "text": "(M1024)" }).click(function() {
                                        Oasis.ToolTip('M1020')
                                    })
                                )
                            ).append(
                                $("<em/>", { "text": "(V- or E-codes NOT allowed)" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth" }).append(
                                $("<p/>", { "text": "Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis that is a multiple coding situation (e.g., a manifestation code)." })).append(
                                $("<label/>", { "class": "strong", "text": " Description/ ICD-9-C M", "for": (isOasis ? Options.Assessment : "") + "_M1024ICD9MA4" }).prepend(
                                    $("<a/>", { "class": "green", "href": "javascript:void(0)", "title": "More Information about M1024", "text": "(M1024)" }).click(function() {
                                        Oasis.ToolTip('M1020')
                                    })
                                )
                            ).append(
                                $("<em/>", { "text": "(V- or E-codes NOT allowed)" })
                            )
                        )
                    );
                    // Add primary diagnosis row (M1020)
                    List.append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_input" })
                    );
                    // Build secondary title row
                    if (isOasis) List.append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_title oasis_m1022" }).append(
                            $("<div/>", { "class": "diagnosis_colwidth", "id": (isOasis ? Options.Assessment : "") + "_oasis_M1022" }).append(
                                $("<label/>", { "class": "strong", "for": (isOasis ? Options.Assessment : "") + "_M1022PrimaryDiagnosis1", "text": " Other Diagnoses" }).prepend(
                                    $("<a/>", { "class": "green", "href": "javascript:void(0)", "title": "More Information about M1022", "text": "(M1022)" }).click(function() {
                                        Oasis.ToolTip('M1020')
                                    })
                                )
                            )
                        )
                    );
                    else List.append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_title oasis_m1022" }).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": "Other Diagnosis" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": "ICD-9-C M Code" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": "O/E" })
                            )
                        ).append(
                            $("<div/>", { "class": "diagnosis_colwidth h4" }).append(
                                $("<p/>", { "text": "Date" })
                            )
                        )
                    );
                    // Cycle through adding proper number of other diagnosis lines (M1022)
                    for (var i = 0; i < NumberOfRows; i++) List.append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_input" })
                    );
                    // Build button to add new diagnosis
                    List.append(
                        $("<li/>", { "class": "oasis_diagnosis diagnosis_title" }).append(
                            $("<div/>", { "class": "buttons" }).append(
                                $("<ul/>").append(
                                    $("<li/>").append(
                                        $("<a/>", { "href": "javascript:void(0)", "text": "Add Another Diagnosis" }).click(function() {
                                            $(this).closest(".diagnoses-list").DiagnosesList("Add")
                                        })
                                    )
                                )
                            )
                        )
                    );
                    // Enable diagnosis item plugin on all affected rows
                    $(".diagnosis_input", List).DiagnosisItem(Options)
                    // Make list sortable
                    List.sortable({
                        items: ".diagnosis_input",
                        handle: ".handle.sortable",
                        stop: function() {
                            List.DiagnosesList("Reorder");
                        }
                    });
                    // Set initial names and events
                    List.DiagnosesList("Reorder");
                    // If issuing a command to an existing plugin
                } else if (typeof Options == "string" && $(this).hasClass("diagnoses-list")) {
                    // Resets all fluid features in the sortable list
                    if (Options == "Reorder") {
                        // Set variables
                        var List = $(this),
                            Assessment = List.attr("assessment");
                        // If last row has data, add a new row and return, as adding a row invokes this same process
                        if ($(".diagnosis-item:last .diagnosis", List).val().length) {
                            List.DiagnosesList("Add");
                            return;
                        }
                        // Fix all names/ids/labels on diagnosis items
                        $(".diagnosis-item", List).DiagnosisItem("Reorder");
                        // Ensure second title row is always above the secong disgnosis item row
                        $(".diagnosis_input:eq(1)", List).before($(".oasis_m1022", List));
                        // Set only last diagnosis to not sort and set change function to add a new row
                        $(".diagnosis-item", List).find(".handle").addClass("sortable").find(":input").each(function() { $(this).unbind("change") });
                        $(".diagnosis-item:last", List).find(".handle").removeClass("sortable").find(":input").each(function() {
                            $(this).change(function() {
                                if ($(this).val() && $(this).val().length) List.DiagnosesList("Add");
                            })
                        });
                        // Set only last diagnosis to not have up/down arrows and delete icon
                        $(".diagnosis_up,.diagnosis_down,.diagnosis-delete", List).show();
                        $(".diagnosis_up:last,.diagnosis_down:last,.diagnosis-delete:last,.sortable:last .diagnosis_down,.diagnosis_up:first", List).hide();
                    }
                    // Add additional diagnosis item to list
                    if (Options == "Add") {
                        // Set variables
                        var List = $(this);
                        // Add new row just before bottom row which has add new button
                        if ($(".diagnosis-item", List).length < 26) {
                            $(".diagnosis_title:last", List).before(
                                $("<li/>", { "class": "oasis_diagnosis diagnosis_input" }).DiagnosisItem({
                                    List: List
                                })
                            );
                            if ($(".diagnosis-item", List).length == 26) $(".diagnosis_title:last", List).hide();
                        }
                        // Blur ajax autocompletes elements (fix to show "Start Typing")
                        $(".diagnosis-item:last .ajax-autocomplete", List).blur();
                        // Run reorder to reset all settings
                        List.DiagnosesList("Reorder");
                    }
                    // Save current list
                    if (Options == "Save") {
                        var data = new Object(), $form = $(this).closest("form");
                        $(":input", this).add("input:eq(0),input:eq(1),input:eq(2),input:eq(3),input:eq(4),input:eq(5)", $form).each(function() {
                            data[$(this).attr("name")] = $(this).attr("value");
                        });
                        U.PostUrl("Oasis/Assessment", data);
                    }
                }
            })
        },
        DiagnosisItem: function(Options) {
            return this.each(function() {
                if (!$(this).hasClass("diagnosis-item")) {
                    // Set variables
                    var Item = $(this),
                        Order = Options.List.children(".diagnosis_input").index(Item),
                        isOasis = Options.List.attr("assessment") != "Edit485",
                        SeverityOptions = { "0": "00", "1": "01", "2": "02", "3": "03", "4": "04" },
                        OeOptions = { "Exacerbation": "1", "Onset": "2" },
                        PrimaryDiagnosis = Options ? Options["_M102" + (Order > 0 ? "2" : "0") + "PrimaryDiagnosis" + (Order > 0 ? String(Order) : "")] : "",
                        PrimaryIcd = Options ? Options["_M102" + (Order > 0 ? "2" : "0") + "ICD9M" + (Order > 0 ? String(Order) : "")] : "",
                        PaymentDiagnosis3 = Options ? Options["_M1024PaymentDiagnoses" + String.fromCharCode(65 + Order) + "3"] : "",
                        PaymentIcd3 = Options ? Options["_M1024ICD9M" + String.fromCharCode(65 + Order) + "3"] : "",
                        PaymentDiagnosis4 = Options ? Options["_M1024PaymentDiagnoses" + String.fromCharCode(65 + Order) + "4"] : "",
                        PaymentIcd4 = Options ? Options["_M1024ICD9M" + String.fromCharCode(65 + Order) + "4"] : "",
                        Severity = Options ? Options["_M102" + (Order > 0 ? "2OtherDiagnose" + String(Order) + "Rating" : "0SymptomControlRating")] : "",
                        Oe = Options ? Options["_485ExacerbationOrOnsetPrimaryDiagnosis" + (Order > 0 ? String(Order) : "")] : "",
                        Date = Options ? Options["_M102" + (Order > 0 ? "2" : "0") + "PrimaryDiagnosis" + (Order > 0 ? String(Order) : "") + "Date"] : "";
                    // Add proper classes and basic layout
                    Item.addClass("diagnosis-item oasis_diagnosis diagnosis_input").append(
                        $("<div/>", { "class": "handle" }).append(
                            $("<div/>", { "class": "diagnosis-delete" }).append(
                                $("<span/>", { "class": "img icon error-small" }))).append(
                            $("<div/>", { "class": "diagnosis_colwidth" })).append(
                            $("<div/>", { "class": "diagnosis_colwidth" })).append(
                            $("<div/>", { "class": "diagnosis_colwidth" })).append(
                            $("<div/>", { "class": "diagnosis_colwidth" })).append(
                            $("<a/>", { "class": "diagnosis_up", "href": "javascript:void(0);" })).append(
                            $("<a/>", { "class": "diagnosis_down", "href": "javascript:void(0);" })
                        )
                    );
                    // If OASIS, build layout accordingly
                    if (isOasis) {
                        $(".diagnosis_colwidth:eq(0)", Item).append(
                            $("<span/>", { "class": "alphali" })).append(
                            $("<label/>", { "class": "strong", "text": "Diagnosis" })).append(
                            $("<input/>", { "class": "diagnosis", "type": "text", "value": PrimaryDiagnosis })
                        );
                        $(".diagnosis_colwidth:eq(1)", Item).append(
                            $("<label/>", { "class": "strong", "text": "Code" })).append(
                            $("<input/>", { "class": "icd", "type": "text", "value": PrimaryIcd })
                        );
                        $(".diagnosis_colwidth:eq(2)", Item).append(
                            $("<span/>", { "class": "alphali" })).append(
                            $("<input/>", { "class": "diagnosisM1024 icdtext", "type": "text", "value": PaymentDiagnosis3 })).append(
                            $("<input/>", { "class": "ICDM1024 icdwidth", "type": "text", "value": PaymentIcd3 })
                        );
                        $(".diagnosis_colwidth:eq(3)", Item).append(
                            $("<span/>", { "class": "alphali" })).append(
                            $("<input/>", { "class": "diagnosisM1024 icdtext", "type": "text", "value": PaymentDiagnosis4 })).append(
                            $("<input/>", { "class": "ICDM1024 icdwidth", "type": "text", "value": PaymentIcd4 })
                        ).after(
                            $("<div/>", { "class": "diagnosis_colwidth_double" }).append(
                                $("<div/>", { "class": "third" }).append(
                                    $("<label/>", { "class": "strong", "text": "Severity" })).append(
                                    $("<select/>", { "class": "severity" }).append(
                                        $("<option/>", { "value": "", "text": "" })
                                    )
                                )
                            ).append(
                                $("<div/>", { "class": "third" }).append(
                                    $("<label/>", { "class": "strong", "text": "O/E" })).append(
                                    $("<select/>", { "class": "oe" }).append(
                                        $("<option/>", { "value": "", "text": "" })
                                    )
                                )
                            ).append(
                                $("<div/>", { "class": "third" }).append(
                                    $("<label/>", { "class": "strong", "text": "Date" })).append(
                                    $("<input/>", { "type": "text", "class": "date", "value": Date })
                                )
                            )
                        );
                        $(".handle", Item).append(
                            $("<div/>", { "class": "float-right oasis" }).append(
                                $("<a/>", { "class": "tooltip_oasis", "text": "?" }).click(function() { Oasis.ToolTip("M1020") })
                            )
                        );
                        // Populate severity select
                        $.each(SeverityOptions, function(Index, Value) {
                            $(".severity", Item).append(
                                $("<option/>").html(Index).attr({
                                    "value": Value,
                                    "selected": Severity === Value
                                })
                            )
                        });
                        // Set event to disable severity if v-code or e-code is used
                        $(".icd", Item).keyup(function() {
                            if ($(this).val().match(/^v/i) || $(this).val().match(/^e/i)) $(".severity", Item).attr("disabled", true);
                            else $(".severity", Item).attr("disabled", false);
                        })
                        // If 485 build layout accordingly
                    } else {
                        $(".diagnosis_colwidth:eq(0)", Item).append(
                            $("<input/>", { "class": "diagnosis", "type": "text", "value": PrimaryDiagnosis })
                        );
                        $(".diagnosis_colwidth:eq(1)", Item).append(
                            $("<input/>", { "class": "icd", "type": "text", "value": PrimaryIcd })
                        );
                        $(".diagnosis_colwidth:eq(2)", Item).append(
                            $("<select/>", { "class": "oe" }).append(
                                $("<option/>", { "value": "", "text": "" })
                            )
                        );
                        $(".diagnosis_colwidth:eq(3)", Item).append(
                            $("<input/>", { "type": "text", "class": "date", "value": Date })
                        );
                    }
                    // Enable diagnosis/ICD inputs for auto-complete functionality
                    $(".diagnosis,.icd,.diagnosisM1024,.ICDM1024", Item).IcdInput();
                    // Enable date input for date picker functionality
                    $(".date", Item).DatePicker();
                    // Populate o/e select
                    $.each(OeOptions, function(Index, Value) {
                        $(".oe", Item).append(
                            $("<option/>").html(Index).attr({
                                "value": Value,
                                "selected": Oe === Value
                            })
                        )
                    });
                    // Set events for up and down arrow icons
                    $(".diagnosis_down,.diagnosis_up", Item).click(function() {
                        var NewPosition = Item.closest(".diagnoses-list").children(".diagnosis-item").index(Item) + ($(this).hasClass("diagnosis_up") ? -1 : 2);
                        Item.closest(".diagnoses-list").find(".diagnosis-item").eq(NewPosition).before(Item);
                        Item.closest(".diagnoses-list").DiagnosesList("Reorder");
                    });
                    // Set event for delete icon click
                    $(".diagnosis-delete", Item).click(function() {
                        var Item = $(this).closest(".diagnosis-item"),
                            List = $(this).closest(".diagnoses-list"),
                            Order = List.children(".diagnosis_input").index(Item);
                        if (confirm("Are you sure you want to delete this diagnosis?")) {
                            if (Order == 0) {
                                $(".diagnosis_down", Item).click();
                                if ($(".diagnosis-item", List).length == 2) List.DiagnosesList("Add");
                            }
                            Item.remove();
                            if ($(".diagnosis-item", List).length < 26) $(".diagnosis_title:last", List).show();
                            List.DiagnosesList("Reorder");
                            List.DiagnosesList("Save");
                        }
                    })
                    // If issuing a command to an existing plugin
                } else if (typeof Options == "string" && $(this).hasClass("diagnosis-item")) {
                    // Cycle through all elements that need to be (re)named or manipulated and set variables accordingly
                    if (Options == "Reorder") {
                        var Item = $(this),
                            Order = Item.closest(".diagnoses-list").children(".diagnosis-item").index(Item),
                            Assessment = Item.closest(".diagnoses-list").attr("assessment"),
                            isOasis = Assessment != "Edit485",
                            Alpha = String.fromCharCode(97 + Order),
                            PrimaryDiagnosis = (isOasis ? Assessment + "_" : "") + "M102" + (Order > 0 ? "2" : "0") + "PrimaryDiagnosis" + (Order > 0 ? String(Order) : ""),
                            PrimaryIcd = (isOasis ? Assessment + "_" : "") + "M102" + (Order > 0 ? "2" : "0") + "ICD9M" + (Order > 0 ? String(Order) : ""),
                            PaymentDiagnosis3 = (isOasis ? Assessment + "_" : "") + "M1024PaymentDiagnoses" + String.fromCharCode(65 + Order) + "3",
                            PaymentIcd3 = (isOasis ? Assessment + "_" : "") + "M1024ICD9M" + String.fromCharCode(65 + Order) + "3",
                            PaymentDiagnosis4 = (isOasis ? Assessment + "_" : "") + "M1024PaymentDiagnoses" + String.fromCharCode(65 + Order) + "4",
                            PaymentIcd4 = (isOasis ? Assessment + "_" : "") + "M1024ICD9M" + String.fromCharCode(65 + Order) + "4",
                            Severity = (isOasis ? Assessment + "_" : "") + "M102" + (Order > 0 ? "2OtherDiagnose" + String(Order) + "Rating" : "0SymptomControlRating"),
                            Oe = (isOasis ? Assessment + "_" : "") + "485ExacerbationOrOnsetPrimaryDiagnosis" + (Order > 0 ? String(Order) : ""),
                            Date = (isOasis ? Assessment + "_" : "") + "M102" + (Order > 0 ? "2" : "0") + "PrimaryDiagnosis" + (Order > 0 ? String(Order) : "") + "Date";
                        $(".diagnosis", Item).attr({ "id": PrimaryDiagnosis, "name": PrimaryDiagnosis });
                        $(".icd", Item).attr({ "id": PrimaryIcd, "name": PrimaryIcd });
                        $(".oe", Item).attr({ "id": Oe, "name": Oe });
                        $(".date", Item).DatePicker("Rename", Date);
                        if (isOasis) {
                            $(".alphali", Item).text(Alpha + ".");
                            $(".diagnosis", Item).prev("label").attr("for", PrimaryDiagnosis);
                            $(".icd", Item).prev("label").attr("for", PrimaryIcd);
                            $(".diagnosisM1024:first", Item).attr({ "id": PaymentDiagnosis3, "name": PaymentDiagnosis3 });
                            $(".ICDM1024:first", Item).attr({ "id": PaymentIcd3, "name": PaymentIcd3 });
                            $(".diagnosisM1024:last", Item).attr({ "id": PaymentDiagnosis4, "name": PaymentDiagnosis4 });
                            $(".ICDM1024:last", Item).attr({ "id": PaymentIcd4, "name": PaymentIcd4 });
                            $(".severity", Item).attr({ "id": Severity, "name": Severity }).prev("label").attr("for", Severity);
                            $(".oe", Item).prev("label").attr("for", Oe);
                            $(".date", Item).prev("label").attr("for", Date);
                        }
                    }
                }
            })
        }
    })
})(jQuery);