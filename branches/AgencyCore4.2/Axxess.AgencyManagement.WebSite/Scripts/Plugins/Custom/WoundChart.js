(function($) {
    $.extend($.fn, {
        WoundChart: function(Options) {
            return this.each(function() {
                var Chart = $(this),
                    Num = 0;
                Chart.append(
                    $("<div/>", { "class": "wide-column" }).append(
                        $("<div/>", { "class": "row" }).append(
                            $("<div/>").Buttons([{
                                Text: "Add Wound",
                                Click: function() {
                                    Chart.find(".wound-chart").css("cursor", "crosshair").click(function(Event) {
                                        var x = parseInt(Event.pageX - Chart.find(".wound-chart").offset().left - 12),
                                            y = parseInt(Event.pageY - Chart.find(".wound-chart").offset().top - 30);
                                        Chart.find(".wound-chart").css("cursor", "default").unbind("click").append(
                                            $("<div/>", { "class": "wound-marker", "style": "left:" + x + "px;top:" + y + "px", "text": String(++Num) }).draggable({ containment: "parent" })).after(
                                            $("<div/>", { "class": "wound-note" }).WoundNote({
                                                Assessment: Options.Assessment,
                                                WoundNumber: Num,
                                                Pointer: $(".wound-marker:last", Chart)
                                            })
                                        )
                                    })
                                }
                            }])).append(
                            $("<div/>", { "class": "wound-chart" })).append(
                            $("<div/>", { "class": "clear" })).append(
                            $("<label/>", { "class": "strong", "for": Options.Assessment + "_GenericTreatmentPerformed", "text": "Treatment Performed:" })).append(
                            $("<div/>").append(
                                $("<textarea/>", { "name": Options.Assessment + "_GenericTreatmentPerformed" }))).append(
                            $("<label/>", { "class": "strong", "for": Options.Assessment + "_GenericNarrative", "text": "Narrative:" })).append(
                            $("<div/>").append(
                                $("<textarea/>", { "name": Options.Assessment + "_GenericNarrative" })
                            )
                        )
                    )
                )
            })
        },
        WoundNote: function(Options) {
            return this.each(function() {
                var Note = $(this),
                    Pointer = Options.Pointer;
                Note.append(
                    $("<h3/>", { "text": "Wound #" + Options.WoundNumber })).append(
                        $("<div/>", { "class": "inner" }).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericUploadFile" + Options.WoundNumber, Label: "Upload Photo", Type: "file", Size: 9 })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericLocation" + Options.WoundNumber, Label: "Location", Type: "text" })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericOnsetDate" + Options.WoundNumber, Label: "Onset Date", Type: "date" })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericWoundType" + Options.WoundNumber, Label: "Wound Type", Type: "text" })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericPressureUlcerStage" + Options.WoundNumber, Label: "Pressure Ulcer Stage", Type: "select",
                            Options: [
                                { Value: "I" },
                                { Value: "II" },
                                { Value: "III" },
                                { Value: "IV" },
                                { Value: "Unstageable" }
                            ]
                        })).append(
                        $("<div/>").MultiRow({
                            Prefix: Options.Assessment,
                            Label: "Measurement",
                            Subrows: [
                                { Name: "GenericMeasurementLength" + Options.WoundNumber, Label: "Length", Type: "centimeter" },
                                { Name: "GenericMeasurementWidth" + Options.WoundNumber, Label: "Width", Type: "centimeter" },
                                { Name: "GenericMeasurementDepth" + Options.WoundNumber, Label: "Depth", Type: "centimeter" }
                            ]
                        })).append(
                        $("<div/>").MultiRow({
                            Prefix: Options.Assessment,
                            Label: "Wound Bed",
                            Subrows: [
                                { Name: "GenericWoundBedGranulation" + Options.WoundNumber, Label: "Granulation", Type: "percent" },
                                { Name: "GenericWoundBedSlough" + Options.WoundNumber, Label: "Slough", Type: "percent" },
                                { Name: "GenericWoundBedEschar" + Options.WoundNumber, Label: "Eschar", Type: "percent" }
                            ]
                        })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericSurroundingTissue" + Options.WoundNumber, Label: "Surrounding Tissue", Type: "select",
                            Options: [
                                { Value: "Pink" },
                                { Value: "Dry" },
                                { Value: "Pale" },
                                { Value: "Moist" },
                                { Value: "Excoriated" },
                                { Value: "Calloused" },
                                { Value: "Normal" }
                            ]
                        })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericDrainage" + Options.WoundNumber, Label: "Drainage", Type: "select",
                            Options: [
                                { Value: "Serous" },
                                { Value: "Serosanguineous" },
                                { Value: "Purulent" },
                                { Value: "None" }
                            ]
                        })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericDrainageAmount" + Options.WoundNumber, Label: "Drainage Amount", Type: "select",
                            Options: [
                                { Value: "Minimal" },
                                { Value: "Moderate" },
                                { Value: "Heavy" },
                                { Value: "None" }
                            ]
                        })).append(
                        $("<div/>").Row({ Prefix: Options.Assessment, Name: "GenericOdor" + Options.WoundNumber, Label: "Odor", Type: "select",
                            Options: [
                                { Value: "Yes" },
                                { Value: "No" }
                            ]
                        })).append(
                        $("<div/>").MultiRow({
                            Prefix: Options.Assessment,
                            Label: "Tunneling",
                            Subrows: [
                                { Name: "GenericTunnelingLength" + Options.WoundNumber, Label: "Length", Type: "centimeter" },
                                { Name: "GenericTunnelingTime" + Options.WoundNumber, Label: "Time", Type: "time" }
                            ]
                        })).append(
                        $("<div/>").MultiRow({
                            Prefix: Options.Assessment,
                            Label: "Undermining",
                            Subrows: [
                                { Name: "GenericUnderminingLength" + Options.WoundNumber, Label: "Length", Type: "centimeter" },
                                { Name: "GenericUnderminingTime" + Options.WoundNumber, Label: "Time", Type: "timerange" }
                            ]
                        })).append(
                        $("<div/>").MultiRow({
                            Prefix: Options.Assessment,
                            Label: "Device",
                            Subrows: [
                                { Name: "GenericDeviceType" + Options.WoundNumber, Label: "Type", Type: "text" },
                                { Name: "GenericDeviceSetting" + Options.WoundNumber, Label: "Setting", Type: "text" }
                            ]
                        }))).append(
                    $("<div/>").Buttons([
                        { Text: "Save", Click: function() { Note.remove() } },
                        { Text: "Delete", Click: function() { Note.remove(); Pointer.remove() } },
                        { Text: "Cancel", Click: function() { Note.remove(); Pointer.remove() } }
                    ])
                )
            })
        }
    })
})(jQuery);