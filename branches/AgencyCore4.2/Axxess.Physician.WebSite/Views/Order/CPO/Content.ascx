﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CarePlanOversights>>" %>

<%= Html.Telerik()
           .Grid<CarePlanOversights>(Model)
           .Name("List_CPO")
           .ClientEvents(evnt => evnt.OnRowDataBound("cpo.AddCheckListOnRowDataBound()"))
           .HtmlAttributes(new { @style = "top:58px;bottom:40px;" })
           .Columns(columns =>
    {
        columns.Bound(c => c.Id).Template(t=>t.Checkbox).ClientTemplate("<#=Checkbox#>").Title("").Width(50).Sortable(false);
        columns.Bound(c => c.AgencyName).Title("Agency").Sortable(false).Width(200); ;
        columns.Bound(c => c.PatientName).Title("Patient").Sortable(false);
        columns.Bound(c => c.Duration).Title("Duration").Sortable(false).Width(80);
        columns.Bound(c => c.CptType).Title("CPT Type").Sortable(false);
        columns.Bound(c => c.LogDateFormatted).Title("Log Date").Sortable(false).Width(100);
        columns.Bound(c => c.StatusName).Title("Status").Sortable(false);
        columns.Bound(c => c.Id).Template(t=>"<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditCPO('"+t.Id+"'); \">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"cpo.Delete('"+t.Id+"');\">Delete</a>")
            .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditCPO('<#=Id#>'); \">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"cpo.Delete('<#=Id#>');\">Delete</a>")
            .Title("Action").Sortable(false).Width(150);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("CPOGrid", "Order"))
     .Scrollable().Footer(false).Sortable(sorting =>
                    sorting.SortMode(GridSortMode.SingleColumn)
                        .OrderBy(order =>
                        {
                            var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                            var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                            if (sortName == "PatientName")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.PatientName).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.PatientName).Descending();
                                }
                            }
                            else if (sortName == "AgencyName")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.AgencyName).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.AgencyName).Descending();
                                }
                            }
                            else if (sortName == "Duration")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.Duration).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.Duration).Descending();
                                }
                            }
                            else if (sortName == "CptCode")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.CptCode).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.CptCode).Descending();
                                }
                            }
                            else if (sortName == "LogDateFormatted")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.LogDateFormatted).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.LogDateFormatted).Descending();
                                }
                            }
                            else if (sortName == "Status")
                            {
                                if (sortDirection == "ASC")
                                {
                                    order.Add(o => o.Status).Ascending();
                                }
                                else if (sortDirection == "DESC")
                                {
                                    order.Add(o => o.Status).Descending();
                                }
                            }
                        }))
    .Scrollable(scrolling => scrolling.Enabled(true))%>
    
    <%= Html.Hidden("BillList", "", new { @id = "CPO_BillList" })%>
    
    
<script type="text/javascript">
    $("#List_CPO div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "/Order/CPOContent?SortParams=" + U.ParameterByName(link, 'List_CPO-orderBy'));
    });
    $("#List_CPO .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px', 'bottom': '25px' });
</script>    