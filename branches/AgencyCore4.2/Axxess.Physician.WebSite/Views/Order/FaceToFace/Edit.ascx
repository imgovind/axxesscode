﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FaceToFaceEncounter>" %>
<span class="wintitle">Face-To-Face Encounter | <%= Model.Patient != null ? Model.Patient.DisplayName : string.Empty %> | <%= Model.Agency != null ? Model.Agency.Name : string.Empty %></span>
<% using (Html.BeginForm("UpdateFaceToFace", "Order", FormMethod.Post, new { @id = "editFaceToFaceForm" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%= Html.Hidden("AgencyId", Model.AgencyId)%>
    <%= Html.Hidden("PatientId", Model.PatientId)%>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("PhysicianId", Model.PhysicianId)%>
    <% Model.EncounterDate = Model.EncounterDate != DateTime.MinValue ? Model.EncounterDate : DateTime.Now; %>
    <div class="wrapper main">
        <table class="fixed notes">
            <tbody>
                <tr><th colspan="4">Face-To-Face Encounter</th></tr>
                <tr><td colspan="4"><span class="bigtext"><%= Model.Patient != null ? Model.Patient.DisplayName + "</span>&nbsp;<span>[" + Model.Patient.PatientIdNumber + "]</span>" : "</span>"%></td></tr>
                <tr><td colspan="2">
                    <label for="FaceToFace_PatientSOC" class="float-left">Start of Care Date:</label>
                    <div class="float-left"><span id="FaceToFace_PatientSOC"><%= Model.Patient != null ? Model.Patient.StartOfCareDateFormatted : "" %></span></div>
                    <div class="clear"></div>
                </td><td colspan="2">
                    <label for="FaceToFace_EpisodeAssociated" class="float-left">Episode Associated:</label>
                    <div class="float-left"><span id="FaceToFace_EpisodeAssociated" class="legendinput"><%= (Model != null && Model.EpisodeStartDate.IsValidDate() ? Model.EpisodeStartDate + "—" : String.Empty) + (Model != null && Model.EpisodeEndDate.IsValidDate() ? Model.EpisodeEndDate : String.Empty)%></span></div>
                </td></tr>
                <tr>
                    <td colspan="4" align="left">
                    <%= string.Format("<input type=\"radio\" value=\"1\" class=\"radio float-left\" id=\"FaceToFace_CertifyingPhysician1\" name=\"Certification\" {0} />", Model.Certification.IsNotNullOrEmpty() && Model.Certification.Equals("1") ? "checked='checked'" : "")%>
                        <label for="FaceToFace_CertifyingPhysician1" class="radio float-left">POC Certifying Physician</label>
                    <%= string.Format("<input type=\"radio\" value=\"2\" class=\"radio float-left\" id=\"FaceToFace_CertifyingPhysician2\" name=\"Certification\" {0} />", Model.Certification.IsNotNullOrEmpty() && Model.Certification.Equals("2") ? "checked='checked'" : "")%>
                        <label for="FaceToFace_CertifyingPhysician2" class="radio float-left">Non POC Certifying Physician</label>
                    <div class="clear"></div><br />
                    <div>I certify that the above named patient is under my care and that I, or the nurse practitioner or physician’s assistant working with me, had the required face-to-face encounter meeting the encounter requirements on the date below.</div>
                    <div class="clear"></div><br />
                    <label for="FaceToFaceEncounter_Date" class="float-left">Face-To-Face Encounter Date:</label>
                    <div class="float-left"><input type="date" name="EncounterDate" value="<%= Model.EncounterDate.ToShortDateString() %>" maxdate="<%= DateTime.Now %>" id="FaceToFaceEncounter_Date" class="required" /></div>
                </td></tr>
                <tr>
                    <td colspan="4">
                        <label for="FaceToFace_MedicalReason" class="strong">The medical reason, diagnosis, or condition related to the primary reason for home healthcare for the encounter was:</label>
                        <div class="align_center"><%= Html.TextArea("MedicalReason", Model.MedicalReason != null ? Model.MedicalReason : "", new { @class = "fill", @id = "FaceToFace_MedicalReason", @rows = "10" })%></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <label for="FaceToFace_ClinicalFindings" class="strong">Clinical Findings that support the medical need for home health services and support home patient's homebound status are as follows:</label>
                        <div class="align_center"><%= Html.TextArea("ClinicalFinding", Model.ClinicalFinding != null ? Model.ClinicalFinding : "", new { @class = "fill", @id = "FaceToFace_ClinicalFindings", @rows = "10" })%></div>
                    </td>
                </tr>
                <tr><td colspan="4" align="left">
                    <div>I hereby certify that based on my clinical findings, the patient is homebound and the following home health services are medically necessary.</div>
                    <div class="clear"></div><br />
                     <table class="form"><tbody>
                         <tr><%string[] services = Model.Services != null && Model.Services != "" ? Model.Services.Split(';') : null;  %>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"1\" class=\"radio float-left\" id=\"FaceToFace_Services1\" name=\"ServicesArray\" {0} />", services != null && services.Contains("1") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services1" class="radio">Skilled Nursing</label></td>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"2\" class=\"radio float-left\" id=\"FaceToFace_Services2\" name=\"ServicesArray\" {0} />", services != null && services.Contains("2") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services2" class="radio">Physical Therapy</label></td>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"3\" class=\"radio float-left\" id=\"FaceToFace_Services3\" name=\"ServicesArray\" {0} />", services != null && services.Contains("3") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services3" class="radio">Occupational Therapy</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"4\" class=\"radio float-left\" id=\"FaceToFace_Services4\" name=\"ServicesArray\" {0} />", services != null && services.Contains("4") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services4" class="radio">Speech Therapy</label></td>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"5\" class=\"radio float-left\" id=\"FaceToFace_Services5\" name=\"ServicesArray\" {0} />", services != null && services.Contains("5") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services5" class="radio">Home Health Aide</label></td>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"6\" class=\"radio float-left\" id=\"FaceToFace_Services6\" name=\"ServicesArray\" {0} />", services != null && services.Contains("6") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services6" class="radio">MSW</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input type=\"checkbox\" value=\"7\" class=\"radio float-left\" id=\"FaceToFace_Services7\" name=\"ServicesArray\" {0} />", services != null && services.Contains("7") ? "checked='checked'" : "")%>
                                <label for="FaceToFace_Services7" class="radio">Other</label></td>
                            <td colspan="2"><%=Html.TextBox("ServicesOther", Model.ServicesOther.IsNotNullOrEmpty() ? Model.ServicesOther : string.Empty, new { @id = "FaceToFace_ServicesOther", @class = "text input_wrapper", @maxlength = "100" })%></td>
                        </tr>
                    </tbody></table>
                </td></tr>
            </tbody>
        </table>
        <input type="hidden" name="Status" id="Edit_FaceToFace_Status" value="150" />
        <div class="buttons">
            <ul>
                <li><a class="save">Save</a></li>
                <li><a class="complete">Complete (Approve)</a></li>
                <li><a class="close">Exit</a></li>
            </ul>
        </div>
    </div>
<% } %>


