var U = {
    TGridAjax: function(url, input, grid) {
        U.PostUrl(url, input, function(result) {
            if (result.isSuccessful) {
                U.RebindTGrid(grid);
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        });
    },
    DisplayDate: function() {
        var currentDate = new Date(),
            days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            dateSuffix = "th";
        if (currentDate.getDate() % 10 == 1 && currentDate.getDate() != 11) dateSuffix = "st";
        else if (currentDate.getDate() % 10 == 2 && currentDate.getDate() != 12) dateSuffix = "nd";
        else if (currentDate.getDate() % 10 == 3 && currentDate.getDate() != 13) dateSuffix = "rd";
        return days[currentDate.getDay()] + ", " + months[currentDate.getMonth()] + " " + currentDate.getDate() + dateSuffix + ", " + currentDate.getFullYear()
    },
    FormatMoney: function(num) {
        if (isNaN(Number(num))) return "";
        var result = String(Math.round(Math.abs(num) * 100));
        result = result.substr(0, result.length - 2) + "." + result.substr(result.length - 2);
        for (var i = 0; i * 4 + 6 < result.length; i++)
            result = result.substr(0, result.length - (i * 4 + 6)) + "," + result.substr(result.length - (i * 4 + 6));
        result = "$" + result;
        if (num < 0) result = "-" + result;
        return result;
    },
    IsGuid: function(guid) {
        return (guid && guid.match(/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/) && !guid.match(/^[0-]*$/));
    },
    IsDate: function(date) {
        if (date && date.match(/^(0?[1-9]|1[012])\/(0?[1-9]|[12]\d|3[01])\/(19|20)?\d\d$/)) {
            date = date.split("/");
            if (date[0].match(/^(0?[469]|11)$/) && parseInt(date[1]) > 30) return false; // More than 30 days in April, June, September, or November
            if (date[0].match(/^(0?2)$/)) {
                if (parseInt(date[2]) % 4 && parseInt(date[1]) > 28) return false; // More than 28 days in February on non-Leap Year
                if (parseInt(date[2]) % 4 == 0 && parseInt(date[1]) > 29) return false; // More than 29 days in February on a Leap Year
            }
            return true;
        }
        return false;
    },
    Y2KConvert: function(ShortYear) {
        var CurrentYear = String(new Date().getFullYear()),
            CurrentPrefix = parseInt(CurrentYear.substr(0, 2)),
            CurrentShortYear = parseInt(CurrentYear.substr(2, 2));
        if (parseInt(ShortYear) > CurrentShortYear) return String(--CurrentPrefix) + String(ShortYear);
        else return String(CurrentPrefix) + String(ShortYear);
    },
    EnableDatePicker: function(e) { $(".date", e.row).DatePicker(); },
    ValidationError: function(control) {
        if (control.closest(".window-content").find(".error:first").closest("fieldset").length) var scroll = control.closest(".window-content").find(".error:first").closest("fieldset").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("td").length) var scroll = control.closest(".window-content").find(".error:first").closest("td").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("div").length) var scroll = control.closest(".window-content").find(".error:first").closest("div").position().top;
        else var scroll = 0;
        control.closest(".window-content").scrollTop(scroll);
        U.Growl("Error: There was a problem validating your form, please review your information and try again.", "error");
    },
    GetAttachment: function(Url, Data) {
        $("body").append(
            $("<form/>", { "id": "AjaxAttachmentForm", "method": "post", "action": Url })
        );
        $.each(Data, function(key, val) {
            $("#AjaxAttachmentForm").append(
                $("<input/>", { "type": "hidden", "name": key, "value": val })
            );
        });
        $("#AjaxAttachmentForm").submit().remove();
    },
    HideOptions: function() {
        $(".option").each(function() {
            if ($(this).closest("li").find(".extra").length) {
                if ($(this).find("input").is(":not(:checked)")) $(this).closest("li").find(".extra").hide();
                $(this).find("input").change(function() {
                    $(this).closest("li").find(".extra").toggle().find(".checkgroup .extra").each(function() {
                        $(this).hide();
                        if ($(this).closest("li").find(".option input").is(":checked")) $(this).show();
                    });
                });
            }
        });
    },
    DeselectableRadio: function(control) {
        if (control == undefined) control = $("#desktop");
        $("input[type=radio].deselectable", control).mouseup(function() {
            if ($(this).prop("checked")) $(this).addClass("deselect");
        });
        $("input[type=radio].deselectable", control).click(function() {
            if ($(this).hasClass("deselect")) {
                $(this).removeClass("deselect").prop("checked", false).change();
            } else return true;
        });
    },
    AjaxError: function() {
        return U.Error("There was an error loading this window.", "Please try again later, if problem persists, contact Axxess for assistance.");
    },
    Error: function(title, text) {
        return $("<div/>", { "id": "window_error" }).append(
            $("<span/>", { "class": "img icon error float-left" })).append(
            $("<h1/>", { "text": title })).append(
            $("<p/>", { "text": text })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "href": "javascript:void(0)", "text": "Close Window", "onclick": "$(this).closest('.window').Close()" })
                    )
                )
            )
        )
    },
    ToolTip: function(o, c) { o.each(function() { if ($(this).attr("tooltip") != undefined) { $(this).tooltip({ track: true, showURL: false, top: 10, left: 10, extraClass: c, bodyHandler: function() { return $(this).attr("tooltip"); } }); } }); },
    RebindTGrid: function(g, a) { if (g.data('tGrid') != null) g.data('tGrid').rebind(a); },
    Growl: function(message, theme) {
        if (typeof message == "object") message = message.toSource();
        else if (typeof message != "string") message = String(message);
        $.jGrowl($.trim(message), { theme: theme, life: 5000 })
    },
    PhoneAutoTab: function(name) {
        $('#' + name + '1').autotab({ target: name + '2', format: 'numeric' });
        $('#' + name + '2').autotab({ target: name + '3', format: 'numeric', previous: name + '1' });
        $('#' + name + '3').autotab({ format: 'numeric', previous: name + '2' });
    },
    FilterResults: function(type) {
        $('#' + type + 'MainResult').empty().addClass("loading");
        U.RebindTGrid($('#' + type + 'SelectionGrid'), { branchId: $("select." + type + "BranchCode").val(), statusId: $("select." + type + "StatusDropDown").val(), paymentSourceId: $("select." + type + "PaymentDropDown").val(), name: $("#txtSearch_" + type + "_Selection").val() });
        /*setTimeout(function() {
        $('#' + type + 'MainResult').removeClass("loading");
        if ($("#" + type + "SelectionGrid .t-grid-content tr").length) $("#" + type + "SelectionGrid .t-grid-content tr:first").click();
        else $('#' + type + 'MainResult').html('<p>No patients found matching your search criteria!</p>');
        }, 5000);*/
    },
    HandlerHelperTemplate: function(AssessmentType, $form, $control, Action) {
        var options = {
            dataType: "json",
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if ($.trim(result.responseText) == "Success") {
                    if ($control.text() == "Save") {
                        U.Growl("Your assessment has been saved.", "success");
                    } else if ($control.text() == "Approve") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been approved.", "success");
                    } else if ($control.text() == "Return") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been returned.", "success");
                    } else if ($control.text() == "Save & Exit") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been saved successfully.", "success");
                    } else if ($control.text() == "Save & Continue") {
                        U.Growl("Your assessment has been saved.", "success");
                        Oasis.NextTab("#" + AssessmentType + "_Tabs");
                    } else if ($control.text() == "Save & Check for Errors") {
                        U.Growl("Your assessment has been saved.", "success");
                        if (typeof Action == "function") Action();
                    } else if ($control.text() == "Check for Errors") Action();
                    else if ($control.text() == "Save & Complete") {
                        if (typeof Action == "function") Action();
                    }
                } else U.Growl(result.responseText, "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == "Success") {
                    if ($control.text() == "Save") {
                        U.Growl("Your assessment has been saved.", "success");
                    } else if ($control.text() == "Approve") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been approved.", "success");
                    } else if ($control.text() == "Return") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been returned.", "success");
                    } else if ($control.text() == "Save & Exit") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been saved successfully.", "success");
                    } else if ($control.text() == "Save & Continue") {
                        U.Growl("Your assessment has been saved.", "success");
                        Oasis.NextTab("#" + AssessmentType + "_Tabs");
                    } else if ($control.text() == "Save & Check for Errors") {
                        U.Growl("Your assessment has been saved.", "success");
                        if (typeof Action == "function") Action();
                    } else if ($control.text() == "Check for Errors") Action();
                    else if ($control.text() == "Save & Complete") {
                        if (typeof Action == "function") Action();
                    }
                } else U.Growl(result.responseText, "error");
            }
        };
        $form.find(".form-omitted :input,.form-omitted:input").val("");
        $form.ajaxSubmit(options);
        return false;
    },
    Delete: function(name, url, data, callback, bypassAlerts) {
        if (bypassAlerts || confirm("Are you sure you want to delete this " + name.toLowerCase() + "?")) {
            U.PostUrl(url, data, function(result) {
                if (result.isSuccessful) {
                    if (callback != undefined && typeof (callback) == 'function') callback();
                    if (!bypassAlerts) U.Growl(name + " has been successfully deleted.", "success");
                } else U.Growl(resultObject.errorMessage, "error");
            });
        }
    },
    ShowIfOtherSelected: function(selectList, field) {
        var required = false;
        if ($("option:selected", selectList).val().toLowerCase() != "other") field.prop("disabled", true);
        if (field.hasClass("required")) {
            required = true;
            if (field.prop("disabled")) field.removeClass("required");
        }
        selectList.change(function() {
            if ($("option:selected", selectList).val().toLowerCase() == "other") {
                if (required) field.addClass("required");
                field.prop("disabled", false).focus();
            } else field.prop("disabled", true).val("").removeClass("required");
        });
    },
    ShowIfOtherChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) {
            field.show();
            U.ToggleValidation(field, true);
        } else {
            field.hide();
            U.ToggleValidation(field, false);
        }
        checkbox.bind('click', function() {
            if ($(this).is(":checked")) {
                field.show();
                U.ToggleValidation(field);
            } else {
                field.hide().val("");
                U.ToggleValidation(field);
            }
        });
    },
    EnableIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) {
            field.attr("disabled", false).removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.attr("disabled", true).addClass('form-omitted');
            U.ToggleValidation(field);
        }
        checkbox.bind('click', function() {
            if (checkbox.is(":checked")) {
                field.attr("disabled", false).removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.attr("disabled", true).addClass('form-omitted');
                U.ToggleValidation(field);
            }
        });
    },
    ShowIfChecked: function(checkbox, field) {
        this.IfChecked(true, checkbox, field)
    },
    HideIfChecked: function(checkbox, field) {
        this.IfChecked(false, checkbox, field)
    },
    IfChecked: function(show, checkbox, field) {
        if (checkbox.prop("checked") == show) {
            field.show().removeClass("form-omitted");
            U.ToggleValidation(field, true);
        } else {
            field.hide().addClass("form-omitted");
            U.ToggleValidation(field);
        }
        checkbox.change(function() {
            if (checkbox.prop("checked") == show) {
                field.show().removeClass("form-omitted");
                U.ToggleValidation(field, true);
            } else {
                field.hide().addClass("form-omitted");
                U.ToggleValidation(field);
            }
        });
    },
    EnableIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
            field.attr("disabled", false).removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.attr("disabled", true).addClass('form-omitted');
            U.ToggleValidation(field, false);
        }
        $(':radio[name=' + group + ']').bind('click', function() {
            if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
                field.attr("disabled", false).removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.attr("disabled", true).addClass('form-omitted');
                U.ToggleValidation(field, false);
            }
        });
    },
    ShowIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
            field.show().removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.hide().addClass('form-omitted');
            U.ToggleValidation(field, false);
        }
        $(':radio[name=' + group + ']').bind('click', function() {
            if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
                field.show().removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.hide().addClass('form-omitted');
                U.ToggleValidation(field, false);
            }
        });
    },
    HideIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
            field.hide().addClass('form-omitted');
            U.ToggleValidation(field, false);
        } else {
            field.show().removeClass('form-omitted');
            U.ToggleValidation(field, true);
        }
        $(':radio[name=' + group + ']').bind('click', function() {
            if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) {
                field.hide().addClass('form-omitted');
                U.ToggleValidation(field, false);
            } else {
                field.show().removeClass('form-omitted');
                U.ToggleValidation(field, true);
            }
        });
    },
    ShowIfSelectEquals: function(select, value, field) {
        if (select.val() == value) {
            field.show().removeClass('form-omitted');
            U.ToggleValidation(field, true);
        } else {
            field.hide().addClass('form-omitted');
            U.ToggleValidation(field, false);
        }
        select.bind('change', function() {
            if (select.val() == value) {
                field.show().removeClass('form-omitted');
                U.ToggleValidation(field, true);
            } else {
                field.hide().addClass('form-omitted');
                U.ToggleValidation(field, false);
            }
        });
    },
    NoneOfTheAbove: function(checkbox, group) {
        if (checkbox.prop("checked")) group.each(function() {
            if ($(this).prop("id") != checkbox.prop("id")) $(this).prop("checked", false).change();
        });
        group.change(function() {
            if ($(this).prop("id") != checkbox.prop("id") && $(this).prop("checked")) checkbox.prop("checked", false).change();
        });
        checkbox.change(function() {
            if ($(this).prop("checked")) group.each(function() {
                if ($(this).prop("id") != checkbox.prop("id")) $(this).prop("checked", false).change();
            });
        });
    },
    ToggleValidation: function(obj, enable) {
        if ((enable != undefined && !enable) || obj.hasClass('required-disabled') || $(".required-disabled", obj).length || obj.hasClass('requireddropdown-disabled') || $(".requireddropdown-disabled", obj).length) {
            if (obj.hasClass('required-disabled')) obj.removeClass('required-disabled').addClass('required');
            if ($(".required-disabled", obj).length) $(".required-disabled", obj).removeClass('required-disabled').addClass('required');
            if (obj.hasClass('requireddropdown-disabled')) obj.removeClass('requireddropdown-disabled').addClass('requireddropdown');
            if ($(".requireddropdown-disabled", obj).length) $(".requireddropdown-disabled", obj).removeClass('requireddropdown-disabled').addClass('requireddropdown');
        } else {
            if (obj.hasClass('required')) obj.removeClass('required').addClass('required-disabled');
            if ($(".required", obj).length) $(".required", obj).removeClass('required').addClass('required-disabled');
            if (obj.hasClass('requireddropdown')) obj.removeClass('requireddropdown').addClass('requireddropdown-disabled');
            if ($(".requireddropdown", obj).length) $(".requireddropdown", obj).removeClass('requireddropdown').addClass('requireddropdown-disabled');
        }
    },
    DeleteTemplate: function(name, id) { U.Delete(name, name + "/Delete", { Id: id }, function() { eval(name + ".RebindList()"); }); },
    InitTemplate: function(formobj, callback, message, before) {
        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        formobj.validate({
            submitHandler: function(form) {
                if (before != undefined && typeof (before) == 'function') before();
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.Growl(message, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditTemplate: function(type, callback) {
        U.InitTemplate($("#edit" + type + "Form"), function() {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
            UserInterface.CloseWindow("edit" + type.toLowerCase());
        }, type + " successfully updated");
    },
    InitNewTemplate: function(type, callback) {
        U.InitTemplate($("#new" + type + "Form"), function() {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
            UserInterface.CloseWindow("new" + type.toLowerCase());
        }, "New " + type.toLowerCase() + " successfully added");
    },
    ShowDialog: function(popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function() {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            }
        });
    },
    CloseDialog: function() {
        $.unblockUI();
    },
    GetUrl: function(url, input, onSuccess) {
        $.ajax({
            url: url,
            data: input,
            dataType: 'json',
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            }
        });
    },
    PostUrl: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    Block: function() {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: "<img src='/Images/loading.gif' style='padding:4em'>",
            css: {
                "background-color": "#fff",
                "border": "0 none",
                "top": "50%",
                "left": "50%",
                "margin": "-10em",
                "text-align": "center",
                "width": "20em",
                "height": "20em",
                "border-radius": "20em",
                "-moz-border-radius": "20em",
                "-webkit-border-radius": "20em",
                "-o-border-radius": "20em",
                "-khtml-border-radius": "20em",
                "box-shadow": "-.3em -.5em 3em #000 inset",
                "-moz-box-shadow": "-.3em -.5em 3em #000 inset",
                "-webkit-box-shadow": "-.3em -.5em 3em #000 inset",
                "-o-box-shadow": "-.3em -.5em 3em #000 inset",
                "-khtml-box-shadow": "-.3em -.5em 3em #000 inset"
            }
        });
    },
    UnBlock: function(selector) {
        $.unblockUI();
    },
    ToTitleCase: function(text) {
        var txt = '';
        var txtArray = text.toLowerCase().split(' ');
        if (txtArray.length > 1) {
            var i = 0;
            for (i = 0; i < txtArray.length; i++) {
                txt += txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
            }
        }
        else {
            txt = text.toLowerCase().substr(0, 1).toUpperCase() + text.toLowerCase().substr(1);
        }
        return txt;
    },
    ClearRadio: function(selector) {
        $("input[name=" + selector + "]").each(function() { $(this).removeAttr('checked'); });
    },
    ChangeToRadio: function(selector) {
        var $checkbox = $("input[name=" + selector + "]");
        $checkbox.click(function() {
            if ($(this).attr('checked')) {
                $checkbox.removeAttr('checked');
                $(this).attr('checked', true);
            }
        });
    },
    TimePicker: function(selector) {
        $(selector).timepicker({
            timeFormat: 'hh:mm p',
            minHour: null,
            minMinutes: null,
            minTime: null,
            maxHour: null,
            maxMinutes: null,
            maxTime: null,
            startHour: 7,
            startMinutes: 0,
            startTime: null,
            interval: 15,
            change: function(time) { }
        });
    },
    ParameterByName: function(url, name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    Message: function(Title, Text, Class) {
        return $("<div/>", { "class": "error-box" }).addClass(Class).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon float-left" }).addClass(Class)).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageError: function(Title, Text) {
        return $("<div/>", { "class": "error-box" }).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon error float-left" })).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageErrorAjax: function() {
        return U.MessageError("Request Error", "An error has been detected while procuring data. Please check your connection and try again, if problem persists, contact Axxess for assistance.");
    },
    MessageErrorJS: function(Growl) {
        var Title = "Browser Error",
            Text = "An error has been detected in your browser. Please refresh the page and try again, if problem persists, contact Axxess for assistance.";
        if (Growl) return Title + "<br />" + Text;
        else return U.MessageError(Title, Text);
    },
    MessageInfo: function(Title, Text) {
        return U.Message(Title, Text, "info")
    },
    MessageSuccess: function(title, text) {
        return U.Message(Title, Text, "success")
    },
    MessageWarn: function(Title, Text) {
        return U.Message(Title, Text, "warning")
    }
};
$.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }
