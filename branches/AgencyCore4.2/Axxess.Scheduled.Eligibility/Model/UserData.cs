﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Scheduled.Eligibility.Model
{
    public class UserData
    {
        public Guid AgencyId { get; set; }
        public string Emails { get; set; }
        public string FirstNames { get; set; }
    }
}
