﻿namespace Axxess.Log.Enums
{
    using System;

    public enum LogType : int
    {
        Patient,
        Episode,
        Physician,
        Referral,
        User,
        MedicationProfile,
        MedicationProfileHistory,
        Rap,
        Final,
        ManagedClaim,
        Authorization,
        AgencyContact,
        AgencyHospital,
        AgencyPhysician,
        AgencyLocation,
        AgencyInsurance,
        AgencyTemplate,
        AllergyProfile,
        ManagedDate,
        AgencySupply,
        NonUserLicense,
        AgencyInformation,
        AgencyIncident,
        AgencyInfection,
        AgencyAdjustmentCode
    }
}