﻿namespace Axxess.Log.Domain
{
    using System;

    using Axxess.Core.Extension;
    using System.Xml.Serialization;
    using Enums;

    public class TaskAudit
    {
        public long Id { get; set; }
        public string Log { get; set; }
        public Guid EntityId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int DisciplineTaskId { get; set; }
    }

    
}
