﻿namespace Axxess.Core
{
    using System;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Core.Infrastructure;

    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            For<IWebConfiguration>().Use<WebConfiguration>();
            For<ICache>().Use<MembaseCache>().Named("MembaseCache");
            For<ICache>().Use<HttpRuntimeCache>().Named("HttpRuntimeCache");
            For<INotification>().Use<EmailNotification>();
            For<ISessionStore>().Use<HttpContextSession>();
            For<ICryptoProvider>().Use<RijndaelProvider>();
            For<IDatabaseAdministration>().Use<MySqlAdministration>();
        }
    }
}
