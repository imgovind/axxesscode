﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Web;
    using System.Diagnostics;

    public static class ContextExtensions
    {
        //[DebuggerStepThrough]
        public static bool IsUserAuthenticated(this HttpContext context)
        {
            Check.Argument.IsNotNull(context, "context");

            bool isAuthenticated = false;

            if (context.User != null && context.User.Identity.IsAuthenticated)
            {
                isAuthenticated = true;
            }

            return isAuthenticated;
        }
    }
}
