﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public interface IDatabaseOperation
    {
        void Execute();
    }
}
