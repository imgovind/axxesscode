﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public class StringVariable
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
