﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Diagnostics;
    using System.Collections.Generic;

    public static class Container
    {
        private static IDependencyResolver resolver;

        [DebuggerStepThrough]
        public static void InitializeWith(IDependencyResolver resolver)
        {
            Check.Argument.IsNotNull(resolver, "resolver");

            Container.resolver = resolver;
        }

        [DebuggerStepThrough]
        public static void Register<T>(T instance)
        {
            Check.Argument.IsNotNull(instance, "instance");

            resolver.Register(instance);
        }

        [DebuggerStepThrough]
        public static void Inject<T>(T existing)
        {
            Check.Argument.IsNotNull(existing, "existing");

            resolver.Inject(existing);
        }

        [DebuggerStepThrough]
        public static T Resolve<T>(Type type)
        {
            Check.Argument.IsNotNull(type, "type");

            return resolver.Resolve<T>(type);
        }

        [DebuggerStepThrough]
        public static T Resolve<T>(Type type, string name)
        {
            Check.Argument.IsNotNull(type, "type");
            Check.Argument.IsNotEmpty(name, "name");

            return resolver.Resolve<T>(type, name);
        }

        [DebuggerStepThrough]
        public static T Resolve<T>()
        {
            return resolver.Resolve<T>();
        }

        [DebuggerStepThrough]
        public static T Resolve<T>(string name)
        {
            Check.Argument.IsNotEmpty(name, "name");

            return resolver.Resolve<T>(name);
        }
        [DebuggerStepThrough]
        public static IEnumerable<T> ResolveAll<T>()
        {
            return resolver.ResolveAll<T>();
        }

        [DebuggerStepThrough]
        public static void Reset()
        {
            if (resolver != null)
            {
                resolver.Dispose();
            }
        }
    }
}
