﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Text;
    using System.Xml.Serialization;

    using Extension;

    public static class Serializer
    {
        public static string Serialize<T>(T instance)
        {
            var result = string.Empty;

            try
            {
                if (instance != null)
                {
                    var stringBuilder = new StringBuilder();

                    XmlWriterSettings xmlSettings = new XmlWriterSettings();
                    xmlSettings.Indent = true;
                    xmlSettings.OmitXmlDeclaration = true;
                    xmlSettings.Encoding = Encoding.UTF8;

                    XmlSerializerNamespaces nameSpaces = new XmlSerializerNamespaces();
                    nameSpaces.Add("", "");

                    XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlSettings);

                    XmlSerializer xmlSerializer = new XmlSerializer(instance.GetType());
                    xmlSerializer.Serialize(xmlWriter, instance, nameSpaces);
                    xmlWriter.Flush();
                    result = stringBuilder.ToString();
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return result;
        }

        public static T Deserialize<T>(string xmlString)
        {
            T instance = default(T);
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));

                 instance = (T)xmlSerializer.Deserialize(memoryStream);

                if (instance == null)
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                return instance;
            }
            return instance;
        }

    }
}

