﻿namespace Axxess.Core.Infrastructure
{
    public interface IValidator
    {
        bool IsValid { get; }
        string Message { get; }
        void Validate();
    }
}
