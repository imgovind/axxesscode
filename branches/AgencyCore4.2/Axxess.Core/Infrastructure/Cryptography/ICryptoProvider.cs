﻿namespace Axxess.Core.Infrastructure
{
    public interface ICryptoProvider
    {
        string Encrypt(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize);
        string Decrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize);
    }
}
