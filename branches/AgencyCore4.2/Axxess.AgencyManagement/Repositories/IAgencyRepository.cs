﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAgencyRepository
    {
        bool ToggleDelete(Guid id);
        Agency Get(Guid id);
        Agency GetAgencyOnly(Guid id);
        List<Agency> GetPhysicianAgenies(Guid loginId);
        Agency GetWithBranches(Guid agencyId);
        Agency GetById(Guid id);
        IEnumerable<Agency> All();
        IList<Agency> AllAgencies();
        bool Add(Agency agency);
        bool Update(Agency agency);
        List<AgencyLocation> AgencyLocations(Guid agencyId, List<Guid> locationIds);

        List<AgencyUser> GetUserNames();
        List<AgencyUser> GetUserNames(Guid agencyId);

        bool AddLocation(AgencyLocation agencyLocation);
        AgencyLocation GetMainLocation(Guid agencyId);
        IList<AgencyLocation> GetBranches(Guid agencyId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id);
        AgencyLocation FindLocationOrMain(Guid agencyId, Guid Id);
        bool EditLocation(AgencyLocation location);
        bool EditLocationModal(AgencyLocation location);
        bool UpdateLocation(AgencyLocation location);
        bool EditBranchCost(AgencyLocation location);

        bool AddContact(AgencyContact contact);
        IList<AgencyContact> GetContacts(Guid agencyId);
        AgencyContact FindContact(Guid agencyId, Guid Id);
        bool EditContact(AgencyContact contact);
        bool DeleteContact(Guid agencyId, Guid id);

        bool AddHospital(AgencyHospital hospital);
        IList<AgencyHospital> GetHospitals(Guid agencyId);
        AgencyHospital FindHospital(Guid agencyId, Guid Id);
        bool EditHospital(AgencyHospital hospital);
        bool DeleteHospital(Guid agencyId, Guid Id);
        
        bool AddInsurance(AgencyInsurance insurance);
        IList<AgencyInsurance> GetInsurances(Guid agencyId);
        IList<InsuranceLean> GetLeanInsurances(Guid agencyId);
        AgencyInsurance GetInsurance(int insuranceId, Guid agencyId);
        AgencyInsurance FindInsurance(Guid agencyId, int Id);
        bool EditInsurance(AgencyInsurance insurance);
        bool EditInsuranceModal(AgencyInsurance insurance);
        bool DeleteInsurance(Guid agencyId, int Id);
        bool IsMedicareHMO(Guid agencyId, int Id);

        List<InsuranceCache> GetInsurancesForBilling(Guid agencyId, int[] insuranceIds);
        List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds);

        bool AddTemplate(AgencyTemplate template);
        IList<AgencyTemplate> GetTemplates(Guid agencyId);
        AgencyTemplate GetTemplate(Guid agencyId, Guid id);
        bool UpdateTemplate(AgencyTemplate template);
        bool DeleteTemplate(Guid agencyId, Guid id);
        void InsertTemplates(Guid agencyId);

        bool AddInfection(Infection infection);
        bool UpdateInfection(Infection infection);
        bool UpdateInfectionModal(Infection infection);
        IList<Infection> GetInfections(Guid agencyId);
        bool DeleteInfection(Guid Id);
        bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        Infection GetInfectionReport(Guid agencyId, Guid id);

        bool AddIncident(Incident incident);
        bool UpdateIncident(Incident incident);
        bool UpdateIncidentModal(Incident incident);
        IList<Incident> GetIncidents(Guid agencyId);
        bool DeleteIncident(Guid Id);
        bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        Incident GetIncidentReport(Guid agencyId, Guid id);

        bool AddSupply(AgencySupply supply);
        IList<AgencySupply> GetSupplies(Guid agencyId);
        IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit);
        AgencySupply GetSupply(Guid agencyId, Guid id);
        bool UpdateSupply(AgencySupply supply);
        bool DeleteSupply(Guid agencyId, Guid id);
        void InsertSupplies(Guid agencyId);
        AxxessSubmitterInfo SubmitterInfo(int payerId);

        bool AddReport(Report report);
        bool UpdateReport(Report report);
        Report GetReport(Guid agencyId, Guid reportId);
        IList<ReportLite> GetReports(Guid agencyId);

        bool AddCustomerNote(CustomerNote note);
        bool UpdateCustomerNote(CustomerNote note);
        CustomerNote GetCustomerNote(Guid agencyId, Guid noteId);
        IList<CustomerNote> GetCustomerNotes(Guid agencyId);

        List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(Guid agencyId, DateTime startDate, DateTime endDate);
        MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid agencyId, Guid reportId);
        AgencyMedicareInsurance FindLocationMedicareInsurance(Guid agencyId, int payor);
        bool AddAgencyLocationMedicare(AgencyMedicareInsurance medicare);
        bool UpdateAgencyLocationMedicare(AgencyMedicareInsurance medicare);

        bool AddAdjustmentCode(AgencyAdjustmentCode code);
        IList<AgencyAdjustmentCode> GetAdjustmentCodes(Guid agencyId);
        AgencyAdjustmentCode FindAdjustmentCode(Guid agencyId, Guid Id);
        bool UpdateAdjustmentCode(AgencyAdjustmentCode code);
        bool DeleteAdjustmentCode(Guid agencyId, Guid id);
    }
}
