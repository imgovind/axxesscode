﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;

    using SubSonic.Repository;
    using System.Text;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool ToggleDelete(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.IsDeprecated = !agency.IsDeprecated;
                agency.IsSuspended = !agency.IsSuspended;
                agency.Modified = DateTime.Now;
                database.Update<Agency>(agency);
                return true;
            }
            return false;
        }

        public List<AgencyUser> GetUserNames()
        {
            var script = @"SELECT agencies.Id as AgencyId, users.Id as UserId, users.FirstName, users.LastName, users.Suffix, users.Credentials, " +
                "users.CredentialsOther, users.IsDeprecated FROM agencies INNER JOIN users ON agencies.Id = users.AgencyId ORDER BY agencies.Id";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public List<AgencyUser> GetUserNames(Guid agencyId)
        {
            var script = @"SELECT Id, FirstName, LastName, Suffix, Credentials, " +
                "CredentialsOther, IsDeprecated FROM users " +
                "Where AgencyId = @agencyid";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("@agencyid", agencyId)
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("Id"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public Agency Get(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }

            return agency;
        }

        public Agency GetAgencyOnly(Guid id)
        {
            return database.Single<Agency>(a => a.Id == id);
        }

        public List<Agency> GetPhysicianAgenies(Guid loginId)
        {
            var agencies = new List<Agency>();
            var agencyIds=new List<Guid>();
            var agencyPhysicians = database.Find<AgencyPhysician>(a => a.LoginId == loginId).ToList();
            foreach (AgencyPhysician ap in agencyPhysicians)
            {
                if (!agencyIds.Contains(ap.AgencyId))
                    agencyIds.Add(ap.AgencyId);
            }
            agencyIds.ForEach(a=>
                agencies.Add(database.Find<Agency>(ag=>ag.Id==a && ag.IsDeprecated==false).SingleOrDefault()));
            return agencies;
        }
        
        public Agency GetWithBranches(Guid agencyId)
        {
            var agency = database.Single<Agency>(a => a.Id == agencyId);
            if (agency != null)
            {
                agency.Branches = database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
            }

            return agency;
        }

        public Agency GetById(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }
            return agency;
        }

        public bool Add(Agency agency)
        {
            if (agency != null)
            {
                agency.Id = Guid.NewGuid();
                agency.OasisAuditVendor = (int)OasisAuditVendors.HHG;
                agency.Created = DateTime.Now;
                agency.Modified = DateTime.Now;
                database.Add<Agency>(agency);
                return true;
            }
            return false;
        }

        public bool AddLocation(AgencyLocation agencyLocation)
        {
            if (agencyLocation != null)
            {
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }

                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                return true;
            }

            return false;
        }

        public bool UpdateLocation(AgencyLocation agencyLocation)
        {
            var result = false;
            if (agencyLocation != null)
            {
                database.Update<AgencyLocation>(agencyLocation);
                result = true;
            }
            return result;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false, "Created", 0, 20).ToList();
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id );
        }

        public AgencyLocation FindLocationOrMain(Guid agencyId, Guid Id)
        {
            AgencyLocation agencyLocation =null;
            if (!Id.IsEmpty())
            {
                agencyLocation = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id);
            }
            if (agencyLocation == null)
            {
                 agencyLocation = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
            }
            return agencyLocation;
        }

        //public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        //{
        //    var location = new AgencyLocation();
            
        //        var script = @"SELECT al.`Id`, al.`AgencyId`, al.`Name`, al.`CustomId`, al.`CBSA`, al.`MedicareProviderNumber`, al.`Cost`, al.`BillData`"+
        //                "al.`SubmitterId`, al.`SubmitterName`, al.`SubmitterPhone`, al.`SubmitterFax`, al.`Payor`, al.`BranchId`, al.`BranchIdOther`, al.`Ub04Locator81cca`," +
        //                "al.`TaxId`, `TaxIdType`, al.`NationalProviderNumber`, al.`MedicaidProviderNumber`, al.`ContactPersonFirstName`," +
        //                "al.`ContactPersonLastName`, al.`ContactPersonEmail`, al.`ContactPersonPhone`, al.`CahpsVendor`, al.`CahpsVendorId`, al.`CahpsSurveyDesignator`," +
        //                "al.`IsAxxessTheBiller`, al.`OasisAuditVendor`, al.`IsLocationStandAlone`, al.`UseServiceSupplies`, al.`UseStandardSupplyReimbursement`," +
        //                "al.`UseWoundcareSupplies`, al.`ServiceSuppliesRevenueCode`, al.`ServiceSuppliesDescription`, ai.`AddressLine1` as MedicareAddressLine1," +
        //                "ai.`AddressLine2` as MedicareAddressLine2, ai.`AddressCity` as MedicareAddressCity, ai.`AddressStateCode` as MedicareAddressStateCode, ai.`AddressZipCode` as MedicareAddressZipCode" +
        //            "FROM agencylocations al " +
        //            "LEFT JOIN agencymedicareinsurances ai ON ai.AgencyId = al.AgencyId AND ai.MedicareId = al.Payor " +
        //            "WHERE al.id = @id AND al.agencyid = @agencyid";

        //        using (var cmd = new FluentCommand<AgencyLocation>(script))
        //        {
        //            location = cmd.SetConnection("AgencyManagementConnectionString")
        //             .AddGuid("agencyid", agencyId)
        //             .AddGuid("id", Id)
        //             .SetMap(reader => new AgencyLocation
        //             {
        //                 Id = reader.GetGuid("Id"),
        //                 AgencyId = reader.GetGuid("AgencyId"),
        //                 Name = reader.GetStringNullable("Name"),
        //                 CustomId = reader.GetStringNullable("CustomId"),
        //                 CBSA = reader.GetStringNullable("CBSA"),
        //                 MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
        //                 Cost = reader.GetStringNullable("Cost"),
        //                 BillData = reader.GetStringNullable("BillData"),
        //                 SubmitterId = reader.GetStringNullable("SubmitterId"),
        //                 SubmitterName = reader.GetStringNullable("SubmitterName"),
        //                 SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
        //                 SubmitterFax = reader.GetStringNullable("SubmitterFax"),
        //                 Payor = reader.GetStringNullable("Payor"),
        //                 BranchId = reader.GetStringNullable("CustomId"),
        //                 BranchIdOther = reader.GetStringNullable("CustomId"),
        //                 Ub04Locator81cca = reader.GetStringNullable("CustomId"),
        //                 TaxId = reader.GetStringNullable("CustomId"),
        //                 TaxIdType = reader.GetStringNullable("CustomId"),
        //                 NationalProviderNumber = reader.GetStringNullable("CustomId"),
        //                 MedicaidProviderNumber = reader.GetStringNullable("CustomId"),
        //                 ContactPersonFirstName = reader.GetStringNullable("CustomId"),
        //                 ContactPersonLastName = reader.GetStringNullable("CustomId"),
        //                 ContactPersonEmail = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
                         
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 MedicareInsurance = new AgencyMedicareInsurance(){
        //                     AddressCity = reader.GetStringNullable(""),
        //                 },

        //             }).AsSingle();
        //        }
            
        //}

        public AgencyMedicareInsurance FindLocationMedicareInsurance(Guid agencyId, int payor)
        {
            return database.Single<AgencyMedicareInsurance>(l => l.AgencyId == agencyId && l.MedicareId == payor);
        }

        public bool EditLocation(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
                if (existingLocation != null)
                {
                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    //  existingLocation.IsSubmitterInfoTheSame = location.IsSubmitterInfoTheSame;
                    existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                    if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                    {
                        existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                    {
                        existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                    if (existingLocation.IsLocationStandAlone)
                    {
                        if (location.IsAxxessTheBiller)
                        {
                            var agency = this.Get(location.AgencyId);
                            if (agency != null)
                            {

                                existingLocation.SubmitterId = agency.SubmitterId;
                                existingLocation.SubmitterName = agency.SubmitterName;
                                existingLocation.Payor = agency.Payor;
                                existingLocation.SubmitterPhone = agency.SubmitterPhone;
                                existingLocation.SubmitterFax = agency.SubmitterFax;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    existingLocation.BranchId = location.BranchId;
                    existingLocation.BranchIdOther = location.BranchIdOther;
                    existingLocation.Name = location.Name;
                    existingLocation.CustomId = location.CustomId;
                    existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                    existingLocation.AddressLine1 = location.AddressLine1;
                    existingLocation.AddressLine2 = location.AddressLine2;
                    existingLocation.AddressCity = location.AddressCity;
                    existingLocation.AddressStateCode = location.AddressStateCode;
                    existingLocation.AddressZipCode = location.AddressZipCode;
                    existingLocation.Comments = location.Comments;
                    existingLocation.Modified = DateTime.Now;
                    database.Update<AgencyLocation>(existingLocation);
                    result = true;
                }
            }
            return result;
        }

        public bool EditLocationModal(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                try
                {
                    location.Modified = DateTime.Now;
                    database.Update<AgencyLocation>(location);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool EditBranchCost(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                //existingLocation.Cost = location.Cost;
                existingLocation.Ub04Locator81cca = location.Ub04Locator81cca;
                existingLocation.Modified = DateTime.Now;

                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public IEnumerable<Agency> All()
        {
            return database.All<Agency>().AsEnumerable<Agency>();
        }

        public IList<Agency> AllAgencies()
        {
            var agencies = database.All<Agency>().ToList();
            if (agencies != null && agencies.Count > 0)
            {
                agencies.ForEach(a =>
                {
                    a.MainLocation = GetMainLocation(a.Id);
                });
            }
            return agencies.OrderBy(a => a.Name).ToList();
        }

        public bool Update(Agency agency)
        {
            bool result = false;
            if (agency != null)
            {
                database.Update<Agency>(agency);
                result = true;
            }
            return result;
        }

        public List<AgencyLocation> AgencyLocations(Guid agencyId, List<Guid> locationIds)
        {
            var list = new List<AgencyLocation>();
            if (locationIds != null && locationIds.Count > 0)
            {
                var ids = locationIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name 
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<AgencyLocation>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencyLocation
                     {
                         Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name")

                     }).AsList();
                }
            }
            return list;
        }

        public bool AddAgencyLocationMedicare(AgencyMedicareInsurance medicare)
        {
            if (medicare != null)
            {
                medicare.Created = DateTime.Now;
                medicare.Id = Guid.NewGuid();
                database.Add<AgencyMedicareInsurance>(medicare);
                return true;
            }
            return false;
        }

        public bool UpdateAgencyLocationMedicare(AgencyMedicareInsurance medicare)
        {
            if (medicare != null)
            {
                medicare.Modified = DateTime.Now;
                database.Update<AgencyMedicareInsurance>(medicare);
                return true;
            }
            return false;
        }


        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {
                
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    contact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.Id == Id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(c => c.Id == contact.Id && c.AgencyId == contact.AgencyId && c.IsDeprecated == false);
            if (contact != null)
            {
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    existingContact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingContact.PhoneExtension = contact.PhoneExtension;
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;
                existingContact.Comments = contact.Comments;
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                contact.IsDeprecated = true;
                contact.Modified = DateTime.Now;
                database.Update<AgencyContact>(contact);
                return true;
            }
            return false;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (!insurance.AgencyId.IsEmpty() && insurance.OldInsuranceId > 0)
                {
                    var oldInsurance = GetInsurance(insurance.OldInsuranceId, insurance.AgencyId);
                    if (oldInsurance != null && oldInsurance.BillData.IsNotNullOrEmpty())
                    {
                        insurance.BillData = oldInsurance.BillData;
                    }
                }
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == insuranceId );
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        public IList<InsuranceLean> GetLeanInsurances(Guid agencyId)
        {
            var script = @"SELECT agencyinsurances.Id, agencyinsurances.PayorType, agencyinsurances.Name, agencyinsurances.InvoiceType, agencyinsurances.ContactPersonFirstName, agencyinsurances.ContactPersonLastName, " +
               "agencyinsurances.PhoneNumber, agencyinsurances.PayorId " +
               "FROM agencyinsurances  WHERE agencyinsurances.AgencyId = @agencyid AND agencyinsurances.IsDeprecated = 0";

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare=false
                })
                .AsList();
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id);
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I =>I.AgencyId == insurance.AgencyId && I.Id == insurance.Id );
            if (insurance != null && existingInsurance!=null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.HasContractWithAgency = insurance.HasContractWithAgency;
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderId = insurance.ProviderId;
                existingInsurance.ProviderSubscriberId = insurance.ProviderSubscriberId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.SubmitterId = insurance.SubmitterId;
                existingInsurance.BillType = insurance.BillType;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                existingInsurance.SubmitterName = insurance.SubmitterName;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(existingInsurance);
                result = true;
            }
            return result;
        }

        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id  );
            if (insurance != null)
            {
                insurance.IsDeprecated = true;
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                return true;
            }
            return false;
        }

        public bool IsMedicareHMO(Guid agencyId, int Id)
        {
            var result = false;
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id );
            if (insurance != null)
            {
                result= (insurance.PayorType == 2);
            }
            return result;
        }

        public bool EditInsuranceModal(AgencyInsurance insurance)
        {
            var result = false;
           
            if (insurance != null )
            {
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public List<InsuranceCache> GetInsurancesForBilling(Guid agencyId, int[] insuranceIds)
        {
            var list = new List<InsuranceCache>();
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND agencyinsurances.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));

                var script = string.Format(@"SELECT 
                        agencyinsurances.Id,
                        agencyinsurances.Name as Name,
                        agencyinsurances.PayorType
                                FROM agencyinsurances 
                                        WHERE agencyinsurances.AgencyId = @agencyid {0}", additionalScript);

                using (var cmd = new FluentCommand<InsuranceCache>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                      .AddGuid("agencyid", agencyId)
                      .SetMap(reader => new InsuranceCache
                      {
                          Id = reader.GetInt("Id"),
                          Name = reader.GetStringNullable("Name"),
                          PayorType = reader.GetInt("PayorType")
                      })
                      .AsList();
                }
            }
            return list;
        }

        public List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds)
        {
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND agencyinsurances.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT agencyinsurances.Id,
                        agencyinsurances.PayorType, 
                        agencyinsurances.Name,
                        agencyinsurances.InvoiceType, 
                        agencyinsurances.ContactPersonFirstName, 
                        agencyinsurances.ContactPersonLastName, 
                        agencyinsurances.PhoneNumber,
                        agencyinsurances.PayorId 
                                FROM agencyinsurances 
                                        WHERE agencyinsurances.AgencyId = @agencyid AND agencyinsurances.IsDeprecated = 0 {0}", additionalScript);

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare = false
                })
                .AsList();
        }
       

        #endregion

        #region Hospital

        public bool AddHospital(AgencyHospital hospital)
        {
            var result = false;
            if (hospital != null)
            {
               
                if (hospital.PhoneArray.Count > 0)
                {
                    hospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    hospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                hospital.Created = DateTime.Now;
                hospital.Modified = DateTime.Now;

                database.Add<AgencyHospital>(hospital);
                result = true;
            }
            return result;
        }

        public IList<AgencyHospital> GetHospitals(Guid agencyId)
        {
            return database.Find<AgencyHospital>(h => h.AgencyId == agencyId && h.IsDeprecated == false).ToList();
        }

        public AgencyHospital FindHospital(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId && h.IsDeprecated == false);
        }

        public bool EditHospital(AgencyHospital hospital)
        {
            var result = false;
            var existingHospital = database.Single<AgencyHospital>(h => h.Id == hospital.Id && h.AgencyId == hospital.AgencyId);
            if (hospital != null)
            {

                if (hospital.PhoneArray.Count > 0)
                {
                    existingHospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    existingHospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingHospital.Name = hospital.Name;
                existingHospital.ContactPersonFirstName = hospital.ContactPersonFirstName;
                existingHospital.ContactPersonLastName = hospital.ContactPersonLastName;
                existingHospital.AddressLine1 = hospital.AddressLine1;
                existingHospital.AddressLine2 = hospital.AddressLine2;
                existingHospital.AddressCity = hospital.AddressCity;
                existingHospital.AddressStateCode = hospital.AddressStateCode;
                existingHospital.AddressZipCode = hospital.AddressZipCode;
                existingHospital.EmailAddress = hospital.EmailAddress;
                existingHospital.Comment = hospital.Comment;
                existingHospital.Modified = DateTime.Now;

                database.Update<AgencyHospital>(existingHospital);
                result = true;
            }
            return result;

        }

        public bool DeleteHospital(Guid agencyId, Guid Id)
        {
            var hospital = database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
            if (hospital != null)
            {
                hospital.IsDeprecated = true;
                hospital.Modified = DateTime.Now;
                database.Update<AgencyHospital>(hospital);
                return true;
            }
            return false;
        }

        #endregion

        #region Infection Report

        public bool AddInfection(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    infection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                infection.Created = DateTime.Now;
                infection.Modified = DateTime.Now;

                database.Add<Infection>(infection);
                result = true;
            }
            return result;
        }

        public IList<Infection> GetInfections(Guid agencyId)
        {
            return database.Find<Infection>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }
        public bool DeleteInfection(Guid Id)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = true;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }
        public bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = isDeprecated;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                try
                {
                    infection.UserId = employeeId;
                    infection.Modified = DateTime.Now;
                    database.Update<Infection>(infection);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Infection GetInfectionReport(Guid agencyId, Guid infectionId)
        {
            return database.Single<Infection>(i => i.AgencyId == agencyId && i.Id == infectionId);
        }

        public bool UpdateInfection(Infection infection)
        {
            var result = false;
            var existingInfection = GetInfectionReport(infection.AgencyId, infection.Id);
            if (existingInfection != null)
            {
                existingInfection.Orders = infection.Orders;
                existingInfection.Treatment = infection.Treatment;
                existingInfection.MDNotified = infection.MDNotified;
                existingInfection.NewOrdersCreated = infection.NewOrdersCreated;
                existingInfection.TreatmentPrescribed = infection.TreatmentPrescribed;

                existingInfection.PhysicianId = infection.PhysicianId;
                existingInfection.InfectionDate = infection.InfectionDate;
                existingInfection.Status = infection.Status;
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    existingInfection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                existingInfection.InfectionTypeOther = infection.InfectionTypeOther;
                existingInfection.FollowUp = infection.FollowUp;
                existingInfection.SignatureDate = infection.SignatureDate;
                existingInfection.SignatureText = infection.SignatureText;
                existingInfection.Modified = DateTime.Now;

                database.Update<Infection>(existingInfection);
                result = true;
            }
            return result;
        }

        public bool UpdateInfectionModal(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        #endregion

        #region Incident Report

        public bool AddIncident(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    incident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                incident.Created = DateTime.Now;
                incident.Modified = DateTime.Now;

                database.Add<Incident>(incident);
                result = true;
            }
            return result;
        }

        public IList<Incident> GetIncidents(Guid agencyId)
        {
            return database.Find<Incident>(i => i.AgencyId == agencyId && i.IsDeprecated==false).ToList();
        }

        public bool DeleteIncident(Guid Id)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = true;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = isDeprecated;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                try
                {
                    incident.UserId = employeeId;
                    incident.Modified = DateTime.Now;
                    database.Update<Incident>(incident);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Incident GetIncidentReport(Guid agencyId, Guid incidentId)
        {
            return database.Single<Incident>(i => i.AgencyId == agencyId && i.Id == incidentId);
        }

        public bool UpdateIncident(Incident incident)
        {
            var result = false;
            var existingIncident = GetIncidentReport(incident.AgencyId, incident.Id);
            if (existingIncident != null)
            {
                existingIncident.Orders = incident.Orders;
                existingIncident.Description = incident.Description;
                existingIncident.ActionTaken = incident.ActionTaken;
                existingIncident.IncidentType = incident.IncidentType;
                existingIncident.MDNotified = incident.MDNotified;
                existingIncident.NewOrdersCreated = incident.NewOrdersCreated;
                existingIncident.FamilyNotified = incident.FamilyNotified;

                existingIncident.PhysicianId = incident.PhysicianId;
                existingIncident.IncidentDate = incident.IncidentDate;
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.Status = incident.Status;

                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    existingIncident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.FollowUp = incident.FollowUp;
                existingIncident.SignatureDate = incident.SignatureDate;
                existingIncident.SignatureText = incident.SignatureText;
                existingIncident.Modified = DateTime.Now;

                database.Update<Incident>(existingIncident);
                result = true;
            }
            return result;
        }

        public bool UpdateIncidentModal(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        #endregion

        #region Template Methods

        public bool AddTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
               
                template.Created = DateTime.Now;
                template.Modified = DateTime.Now;

                database.Add<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public IList<AgencyTemplate> GetTemplates(Guid agencyId)
        {
            IList<AgencyTemplate> templates = null;
            var cacheKey = string.Format("Supplies_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out templates))
            {
                templates = database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
                Cacher.Set(cacheKey, templates);
            }
            return templates;
            //return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }


        public AgencyTemplate GetTemplate(Guid agencyId, Guid id)
        {
            //return database.Single<AgencyTemplate>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
            return GetTemplates(agencyId).SingleOrDefault(c => c.Id == id && c.IsDeprecated == false);
        }

        public bool UpdateTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public bool DeleteTemplate(Guid agencyId, Guid id)
        {
            var template = database.Single<AgencyTemplate>(c => c.AgencyId == agencyId && c.Id == id );
            if (template != null)
            {
                template.IsDeprecated = true;
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                return true;
            }
            return false;
        }

        public void InsertTemplates(Guid agencyId)
        {
            var templateSQL = MessageBuilder.ReadTextFrom("Templates");

            if (templateSQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(templateSQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Billing

        public AxxessSubmitterInfo SubmitterInfo(int payerId)
        {
            return database.Single<AxxessSubmitterInfo>(s => s.Id == payerId);
        }

        #endregion

        #region Supply Methods

        public bool AddSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.IsDeprecated = false;
                supply.Created = DateTime.Now;
                supply.Modified = DateTime.Now;

                database.Add<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId)
        {
            return database.Find<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Description).ToList();
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit)
        {
            return database.GetPaged<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false && c.Description.Contains(searchTerm), "Description", 0, searchLimit);
        }

        public AgencySupply GetSupply(Guid agencyId, Guid id)
        {
            return database.Single<AgencySupply>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool UpdateSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public bool DeleteSupply(Guid agencyId, Guid id)
        {
            var supply = database.Single<AgencySupply>(c => c.AgencyId == agencyId && c.Id == id);
            if (supply != null)
            {
                supply.IsDeprecated = true;
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                return true;
            }
            return false;
        }

        public void InsertSupplies(Guid agencyId)
        {
            var supplySQL = MessageBuilder.ReadTextFrom("Supplies");

            if (supplySQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(supplySQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Reports

        public bool AddReport(Report report)
        {
            bool result = false;
            if (report != null)
            {
                report.Id = Guid.NewGuid();
                report.Created = DateTime.Now;
                database.Add<Report>(report);
                result = true;
            }

            return result;
        }

        public Report GetReport(Guid agencyId, Guid reportId)
        {
            return database.Single<Report>(r => r.Id == reportId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateReport(Report report)
        {
            var result = false;
            if (report != null)
            {
                database.Update<Report>(report);
                result = true;
            }
            return result;
        }

        public IList<ReportLite> GetReports(Guid agencyId)
        {
            var reports = new List<ReportLite>();
            var reportSQL = @"SELECT `Id`, `UserId`, `AssetId`, `Type`, `Format`, `Status`, `IsDeprecated`, `Created`, `Completed` FROM `agencymanagement`.`reports` " +
                "WHERE `AgencyId` = @agencyid AND `IsDeprecated` = 0 ORDER BY `Created` DESC LIMIT 0, 100;";

            using (var cmd = new FluentCommand<ReportLite>(reportSQL))
            {
                reports = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ReportLite {
                        Id = reader.GetGuid("Id"),
                        UserId = reader.GetGuid("UserId"),
                        AssetId = reader.GetGuid("AssetId"),
                        Name = reader.GetString("Type"),
                        Format = reader.GetString("Format"),
                        Status = reader.GetString("Status"),
                        Created = reader.GetDateTime("Created") != DateTime.MinValue ? reader.GetDateTime("Created").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        Completed = reader.GetDateTime("Completed") != DateTime.MinValue ? reader.GetDateTime("Completed").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        UserName = UserEngine.GetName(reader.GetGuid("UserId"), agencyId)
                    })
                    .AsList();
            }
            return reports;
        }

        #endregion

        #region CustomerNotes

        public bool AddCustomerNote(CustomerNote note)
        {
            bool result = false;
            if (note != null)
            {
                note.Id = Guid.NewGuid();
                note.Created = DateTime.Now;
                note.Modified = DateTime.Now;
                note.IsDeprecated = false;
                database.Add<CustomerNote>(note);
                result = true;
            }

            return result;
        }

        public CustomerNote GetCustomerNote(Guid agencyId, Guid noteId)
        {
            return database.Single<CustomerNote>(r => r.Id == noteId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateCustomerNote(CustomerNote note)
        {
            var result = false;
            if (note != null)
            {
                database.Update<CustomerNote>(note);
                result = true;
            }
            return result;
        }

        public IList<CustomerNote> GetCustomerNotes(Guid agencyId)
        {
            return database.Find<CustomerNote>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderByDescending(c => c.Created).ToList();
        }

        #endregion

        #region Medicare Eligibility Report

        public List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            List<MedicareEligibilitySummary> list = new List<MedicareEligibilitySummary>();
            var script = @"SELECT Id, AgencyId, Created " +
                "FROM medicareeligibilitysummaries me " +
                "WHERE me.Created BETWEEN @startdate AND @enddate AND me.AgencyId = @agencyid AND me.IsDeprecated = 0";

            using (var cmd = new FluentCommand<MedicareEligibilitySummary>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new MedicareEligibilitySummary
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Created = reader.GetDateTime("Created"),
                })
                .AsList();
            }
            return list;
        }

        public MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid agencyId, Guid reportId)
        {
            MedicareEligibilitySummary report = new MedicareEligibilitySummary();
            var script = @"SELECT * " +
                "FROM medicareeligibilitysummaries me " +
                "WHERE me.AgencyId = @agencyid  " +
                "AND me.Id = @id AND me.IsDeprecated = 0";

            using (var cmd = new FluentCommand<MedicareEligibilitySummary>(script))
            {
                report = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("id", reportId)
                .SetMap(reader => new MedicareEligibilitySummary
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Report = reader.GetStringNullable("Report"),
                    Created = reader.GetDateTime("Created"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                }).AsSingle();
            }
            if (report != null)
            {
                var agency = AgencyEngine.Get(agencyId);
                if (agency != null)
                {
                    report.AgencyName = agency.Name;
                }
                report.Data = report.Report.IsNotNullOrEmpty() ? report.Report.ToObject<MedicareEligibilitySummaryData>() : new MedicareEligibilitySummaryData();
                if (report.Data == null)
                {
                    report.Data = new MedicareEligibilitySummaryData();
                }
            }
            return report;
        }

        #endregion

        #region Adjustment Codes

        public bool AddAdjustmentCode(AgencyAdjustmentCode code)
        {
            bool result = false;
            if (code != null)
            {
                code.Created = DateTime.Now;
                code.Modified = DateTime.Now;
                if (code.Id.IsEmpty())
                {
                    code.Id = Guid.NewGuid();
                }
                database.Add<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        public IList<AgencyAdjustmentCode> GetAdjustmentCodes(Guid agencyId)
        {
            IList<AgencyAdjustmentCode> codes = null;
            var cacheKey = string.Format("AdjustmentCodes_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out codes))
            {
                codes = database.Find<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Code).ToList();
                Cacher.Set(cacheKey, codes);
            }
            return codes;
        }

        public AgencyAdjustmentCode FindAdjustmentCode(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.Id == Id);
        }

        public bool UpdateAdjustmentCode(AgencyAdjustmentCode code)
        {
            bool result = false;
            if (code != null)
            {
                code.Modified = DateTime.Now;
                database.Update<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        public bool DeleteAdjustmentCode(Guid agencyId, Guid id)
        {
            bool result = false;
            var code = database.Single<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.Id == id);
            if (code != null)
            {
                code.IsDeprecated = true;
                code.Modified = DateTime.Now;
                database.Update<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        #endregion
    }
}
