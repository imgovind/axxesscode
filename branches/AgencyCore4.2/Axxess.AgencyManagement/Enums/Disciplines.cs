﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    using Axxess.Core.Infrastructure;

    public enum Disciplines
    {
        [CustomDescription("Nursing", "SNV")]
        Nursing,
        [CustomDescription("Orders", "Orders")]
        Orders,
        [CustomDescription("PT", "PT")]
        PT,
        [CustomDescription("OT", "OT")]
        OT,
        [CustomDescription("ST", "ST")]
        ST,
        [CustomDescription("HHA", "HHA")]
        HHA,
        [CustomDescription("MSW", "MSW")]
        MSW,
        [CustomDescription("Dietician", "Dietician")]
        Dietician,
        [CustomDescription("Claim", "Claim")]
        Claim,
        [CustomDescription("Report & Notes", "ReportsAndNotes")]
        ReportsAndNotes
    }
}
