﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Script.Serialization;
    using Axxess.Core.Extension;
   public class SurveyCensus
    {
       [ScriptIgnore]
        public Guid Id { get; set; }
        public string CertPeriod { get; set; }
        public string PatientIdNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string Gender { get; set; }
        public DateTime SOC{ get; set; }
        public DateTime DOB { get; set; }
        public string SSN { get; set; }
        public string MedicareNumber { get; set; }
        public string PrimaryDiagnosis { get; set; }
        public string SecondaryDiagnosis { get; set; }
        public string Discipline { get; set; }
        public string Phone { get; set; }
        public string EvacuationZone { get; set; }
        [ScriptIgnore]
        public string AddressLine1 { get; set; }
        [ScriptIgnore]
        public string AddressLine2 { get; set; }
        [ScriptIgnore]
        public string AddressCity { get; set; }
        [ScriptIgnore]
        public string AddressStateCode { get; set; }
        [ScriptIgnore]
        public string AddressZipCode { get; set; }
        public int Triage { get; set; }
        [ScriptIgnore]
        public Guid PhysicianId { get; set; }
        [ScriptIgnore]
        public Guid CaseManagerId { get; set; }
        [ScriptIgnore]
        public string InsuranceId { get; set; }

        [ScriptIgnore]
        public DateTime StartDate { get; set; }
        [ScriptIgnore]
        public DateTime EndDate { get; set; }
        [ScriptIgnore]
        public string Schedule { get; set; }
        public string PatientDisplayName { get { return this.LastName + ", " + this.FirstName + " " + this.MiddleInitial; } }
        public string CaseManagerDisplayName { get; set; }
        public string InsuranceNumber { get; set; }
        public string InsuranceName { get; set; }
        public string PhysicianDisplayName { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianPhone { get; set; }
        public string PhysicianFax { get; set; }
        public string DisplayAddressLine1
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        public string DisplayAddressLine2
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.TrimWithNullable(), this.AddressStateCode.TrimWithNullable(), this.AddressZipCode.TrimWithNullable());
            }
        }
        public string PolicyNumber
        {
            get
            {
                string policyNumber = "";
                if (this.MedicareNumber.IsNotNullOrEmpty())
                {
                    policyNumber = this.MedicareNumber;
                }
                else
                {
                    policyNumber = this.InsuranceNumber;
                }
                return policyNumber;
            }
        }


    }
}
