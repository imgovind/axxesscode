﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class AgencyHospital : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string Phone { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Comment { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string ContactPerson
        {
            get
            {
                return string.Format("{0} {1}", this.ContactPersonFirstName, this.ContactPersonLastName);
            }
        }

        [SubSonicIgnore]
        public List<string> PhoneArray { get; set; }

        [SubSonicIgnore]
        public List<string> FaxNumberArray { get; set; }

        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2} ,{3}  {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} , {1} ,{2}  {3}", this.AddressLine1.Trim(),  this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public string PhoneFormatted { get { return this.Phone.ToPhone(); } }

        [SubSonicIgnore]
        public string FaxFormatted { get { return this.FaxNumber.ToPhone(); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Hospital Name is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Hospital city is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Hospital state is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Hospital zip is required.  <br />"));
            AddValidationRule(new Validation(() => (this.EmailAddress == null ? !string.IsNullOrEmpty(this.EmailAddress) : !this.EmailAddress.IsEmail()), "Contact e-mail is not in a valid format."));

        }

        #endregion
    }
}
