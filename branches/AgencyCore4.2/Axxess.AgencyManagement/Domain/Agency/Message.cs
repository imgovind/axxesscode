﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    
    public class Message : EntityBase, IMessage
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid FromId { get; set; }
        public string Body { get; set; }
        public Guid PatientId { get; set; }
        public Guid AttachmentId { get; set; }
        public string Subject { get; set; }
        public string FromName { get; set; }
        public bool MarkAsRead { get; set; }
        public bool IsDeprecated { get; set; }
        public Guid RecipientId { get; set; }
        public DateTime Created { get; set; }
        public string RecipientNames { get; set; }
        public string CarbonCopyNames { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public MessageType Type { get; set; }
        [SubSonicIgnore]
        public string PatientName { get; set; }
        [SubSonicIgnore]
        public List<Guid> Recipients { get; set; }
        [SubSonicIgnore]
        public List<Guid> CarbonCopyRecipients { get; set; }
        [SubSonicIgnore]
        public string MessageDate { get { return this.Created.ToString("F"); } }
        [SubSonicIgnore]
        public string Date { get { return string.Format("{0: MMM d, hh:mm tt}", this.Created); } }
        [SubSonicIgnore]
        public string ReplyForwardBody
        {
            get
            {
                return new StringBuilder()
                    .AppendLine("<br /><br /><br />")
                    .Append("_______________________________________________________________________________________________________________")
                    .AppendLine("<br />").AppendFormat("From: {0}", this.FromName)
                    .AppendLine("<br />").AppendFormat("Sent: {0}", this.MessageDate)
                    .AppendLine("<br />").AppendFormat("To: {0}", this.RecipientNames)
                    .AppendLine("<br />").AppendFormat("CC: {0}", this.CarbonCopyNames)
                    .AppendLine("<br />").AppendFormat("Subject: {0}", this.Subject)
                    .AppendLine("<br /><br />").Append(this.Body).ToString();
            }
        }
        [SubSonicIgnore]
        public bool HasAttachment { get { return !this.AttachmentId.IsEmpty(); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.Recipients.Count == 0, "Please select at least one recipient"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Subject), "Subject is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Body), "Body is required. <br />"));
        }

        #endregion

    }
}
