﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public sealed class PhysicianSession
    {
        public Guid LoginId { get; set; }
        public string NpiNumber { get; set; }
        public string DisplayName { get; set; }
        public string ImpersonatorName { get; set; }
        public List<Guid> AgencyPhysicianIdentifiers { get; set; }
    }
}
