﻿namespace Axxess.AgencyManagement.Domain
{
    using System.Xml.Serialization;

    [XmlRoot()]
    public class Locator
    {
        public Locator()
        {
        }

        public Locator(string id)
        {
            LocatorId = id;
        }

        [XmlElement]
        public string LocatorId { get; set; }
        [XmlElement]
        public string Code1 { get; set; }
        [XmlElement]
        public string Code2 { get; set; }
        [XmlElement]
        public string Code3 { get; set; }
    }
}
