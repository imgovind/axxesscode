﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class SystemMessage : EntityBase, IMessage
    {
        #region Members

        public Guid Id { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Members

        [SubSonicIgnore]
        public MessageType Type { get; set; }
        [SubSonicIgnore]
        public string Date { get { return string.Format("{0: MMM d, hh:mm tt}", this.Created); } }
        [SubSonicIgnore]
        public string MessageDate { get { return this.Created.ToString("F"); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Body), "Body is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Subject), "Subject is required. <br />"));
        }

        #endregion

    }
}
