﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class RemittanceLean
    {
        public Guid Id { get; set; }
        public string RemitId { get; set; }
        public int TotalClaims { get; set; }
        public double CoveredAmount { get; set; }
        public double ChargedAmount { get; set; }
        public double PaymentAmount { get; set; }
        public DateTime RemittanceDate { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}
