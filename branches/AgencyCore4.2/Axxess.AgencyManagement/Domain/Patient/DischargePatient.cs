﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;

    public class DischargePatient
    {
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public DateTime StartofCareDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public string DischargeReason { get; set; }
        public int DischargeReasonId { get; set; }

    }
}
