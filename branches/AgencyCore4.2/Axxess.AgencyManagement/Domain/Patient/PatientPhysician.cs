﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class PatientPhysician
    {
        public int Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid PhysicianId { get; set; }
        public bool IsPrimary { get; set; }
    }
}
