﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    [Serializable]
    public sealed class UserSession
    {
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string Address { get; set; }
        public string FullName { get; set; }
        public string AgencyName { get; set; }
        public string DisplayName { get; set; }
        public string AgencyRoles { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsAgencyFrozen { get; set; }
        //public int MaxAgencyUserCount { get; set; }
        public string AutomaticLogoutTime { get; set; }
        public string ImpersonatorName { get; set; }
        public bool OasisVendorExist { get; set; }
    }
}
