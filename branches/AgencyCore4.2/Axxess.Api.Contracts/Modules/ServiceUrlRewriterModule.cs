﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Web;

    class ServiceUrlRewriterModule : IHttpModule
    {
        #region IHttpModule Members

        void IHttpModule.Dispose()
        {
            return;
        }

        void IHttpModule.Init(HttpApplication context)
        {
            context.BeginRequest += (sender, e) =>
            {
                HttpContext httpContext = HttpContext.Current;
                var path = httpContext.Request.AppRelativeCurrentExecutionFilePath;
                int indexOf = path.IndexOf('/', 2);
                if (indexOf > 0)
                {
                    var serviceName = path.Substring(0, indexOf) + ".svc";
                    var pathInfo = path.Substring(indexOf, path.Length - indexOf);
                    string queryString = httpContext.Request.QueryString.ToString();
                    httpContext.Request.Headers.Add("X-REWRITE-URL", context.Request.Url.AbsolutePath);
                    httpContext.RewritePath(serviceName, pathInfo, queryString, false);
                }
            };
        }

        #endregion
    }
}

