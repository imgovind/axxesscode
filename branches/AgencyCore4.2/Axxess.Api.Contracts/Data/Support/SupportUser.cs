﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Membership/SupportUser/2012/05/")]
    public class SupportUser
    {
        [DataMember]
        public Guid LoginId { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string IpAddress { get; set; }
        [DataMember]
        public string ServerName { get; set; }
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public bool IsAuthenticated { get; set; }
        [DataMember]
        public DateTime LastSecureActivity { get; set; }
    }
}
