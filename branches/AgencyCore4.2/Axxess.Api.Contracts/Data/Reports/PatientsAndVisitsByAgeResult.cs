﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/PatientsAndVisitsByAgeResult/2012/03/")]
    public class PatientsAndVisitsByAgeResult : BaseCaliforniaReportResult
    {
        [DataMember]
        public int Visits { get; set; }
        [DataMember]
        public int Patients { get; set; }
    }
}
