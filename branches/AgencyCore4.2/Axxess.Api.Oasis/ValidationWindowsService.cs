﻿namespace Axxess.Api.Oasis
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    public partial class ValidationWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public ValidationWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "ValidationService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(OasisValidationService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("OASIS Validation Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("OASIS Validation Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
