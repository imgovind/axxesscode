﻿namespace Axxess.LookUp.Domain
{
    using System;

    public class DrugPackage
    {
        #region Members

        public int Id { get; set; }
        public string Code { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        public string SequenceNumber { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", this.SequenceNumber, this.Code, this.Size, this.Type);
        }
        #endregion
    }
}
