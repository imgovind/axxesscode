﻿var FollowUp = {
    _FollowUpId: "",
    GetFollowUpId: function() {
        return FollowUp._FollowUpId;
    },
    SetFollowUpId: function(FollowUpId) {
        FollowUp._FollowUpId = FollowUpId;
    },
    Init: function() {


        $("input[name=FollowUp_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {
                $("#followUp_M1308").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#followUp_M1308").find(':input').each(function() { $(this).val(''); });
            }
            else if ($(this).val() == 1) {
                $("#followUp_M1308").unblock();
            }
        });

        $("input[name=FollowUp_M1330StasisUlcer]").click(function() {
            if ($(this).val() == 0 || $(this).val() == 3) {
                $("#followUp_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=FollowUp_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=FollowUp_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#followUp_M1332AndM1334").unblock();
            }
        });

        $("input[name=FollowUp_M1340SurgicalWound]").click(function() {
            if ($(this).val() == 0 || $(this).val() == 2) {
                $("#followUp_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=FollowUp_M1342SurgicalWoundStatus]").attr('checked', false);
            }
            else {
                $("#followUp_M1342").unblock();
            }
        });

    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var actionType = control.val();
                    if (actionType == "Save/Continue") {
                        $("input[name='FollowUp_Id']").val(resultObject.Assessment.Id);
                        $("input[name='FollowUp_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='FollowUp_Action']").val('Edit');
                        Oasis.NextTab("#editFollowUpTabs.tabs");
                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {
                        $("input[name='FollowUp_Id']").val(resultObject.Assessment.Id);
                        $("input[name='FollowUp_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='FollowUp_Action']").val('Edit');
                    }

                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        FollowUp.HandlerHelper(form, control);
    },
    Validate: function(id) {

        OasisValidation.Validate(id, "FollowUp");

    },
    loadFollowUp: function(id, patientId, assessmentType) {
        $('#followUpResult').load('Oasis/NewFollowUpContent', { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#followUpResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#followUp');
            }
            else if (textStatus == "success") {
                JQD.open_window('#followUp');
                $("#followUpTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                $("#followUpTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
                $("#followUpTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                    FollowUp.loadFollowUpParts(event, ui);
                });
                FollowUp.Init();
            }
        }
);
    }
    ,
    loadFollowUpParts: function(event, ui) {

        $($(ui.tab).attr('href')).load('Oasis/NewFollowUpPartContent', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
            }
        }
);
    }
    ,
    FollowUp: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#editFollowUp").clearForm();
        $("#editFollowUp div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#FollowUpTitle").text("New Follow-Up - " + patientName);

                $("input[name='FollowUp_Id']").val("");
                $("input[name='FollowUp_PatientGuid']").val(id);
                $("input[name='FollowUp_Action']").val('New');
                $("#FollowUp_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#FollowUp_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#FollowUp_M0040FirstName").val(patient.FirstName);
                $("#FollowUp_M0040MI").val(patient.MiddleInitial);
                $("#FollowUp_M0040LastName").val(patient.LastName);
                $("#FollowUp_M0050PatientState").val(patient.AddressStateCode);
                $("#FollowUp_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#FollowUp_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#FollowUp_M0064PatientSSN").val(patient.SSN);
                $("#FollowUp_M0065PatientMedicaidNumber").val(patient.MedicaidNumber);
                $("#FollowUp_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=FollowUp_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='FollowUp_M0100AssessmentType'][value='05']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=FollowUp_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=FollowUp_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=FollowUp_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=FollowUp_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=FollowUp_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=FollowUp_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=FollowUp_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=FollowUp_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=FollowUp_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=FollowUp_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=FollowUp_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=FollowUp_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=FollowUp_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=FollowUp_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=FollowUp_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=FollowUp_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=FollowUp_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=FollowUp_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=FollowUp_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#FollowUp_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                    }
                }
            }
        });
    }
    ,
    EditFollowUp: function(id, patientId, assessmentType) {
        //var patientId = Oasis.GetId();
        $("#editFollowUp").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getNewRows(result);
            }
        });
        getNewRows = function(result) {
            var patient = eval(result);
            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#FollowUpTitle").text("Edit Follow Up - " + firstName + " " + lastName);
            FollowUp.SetFollowUpId(id);
            $("input[name='FollowUp_Id']").val(id);
            $("input[name='FollowUp_Action']").val('Edit');
            $("input[name='FollowUp_PatientGuid']").val(patientId);
            $("#FollowUp_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#FollowUp_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#FollowUp_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=FollowUp_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#FollowUp_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=FollowUp_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#FollowUp_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $('input[name=FollowUp_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                $("#FollowUp_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#FollowUp_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#FollowUp_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            $("#FollowUp_GenericEpisodeStartDate").val(result["GenericEpisodeStartDate"] != null && result["GenericEpisodeStartDate"] != undefined ? result["GenericEpisodeStartDate"].Answer : "");
            var rOCDateNotApplicable = result["M0032ROCDateNotApplicable"];
            if (rOCDateNotApplicable != null && rOCDateNotApplicable != undefined) {
                if (rOCDateNotApplicable.Answer == 1) {

                    $('input[name=FollowUp_M0032ROCDateNotApplicable][value=1]').attr('checked', true);
                    $("#FollowUp_M0032ROCDate").val(" ");
                }
                else {
                    $('input[name=FollowUp_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                    $("#FollowUp_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
                }
            }
            else {
                $('input[name=FollowUp_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                $("#FollowUp_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
            }
            $("#FollowUp_M0040FirstName").val(firstName);
            $("#FollowUp_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#FollowUp_M0040LastName").val(lastName);
            $("#FollowUp_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#FollowUp_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUnknown"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=FollowUp_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', true);
                    $("#FollowUp_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=FollowUp_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                    $("#FollowUp_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $('input[name=FollowUp_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                $("#FollowUp_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUnknown"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=FollowUp_M0064PatientSSNUnknown][value=1]').attr('checked', true);
                    $("#FollowUp_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=FollowUp_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                    $("#FollowUp_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $('input[name=FollowUp_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                $("#FollowUp_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUnknown"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=FollowUp_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', true);
                    $("#FollowUp_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=FollowUp_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                    $("#FollowUp_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            }
            else {
                $('input[name=FollowUp_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                $("#FollowUp_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }

            $("#FollowUp_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=FollowUp_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#FollowUp_M0090AssessmentCompleted").val(result["M0090AssessmentCompleted"] != null && result["M0090AssessmentCompleted"] != undefined ? result["M0090AssessmentCompleted"].Answer : "");
            $("input[name='FollowUp_M0100AssessmentType'][value='05']").attr('checked', true);
            var physicianOrderedDateNotApplicable = result["M0102PhysicianOrderedDateNotApplicable"];
            if (physicianOrderedDateNotApplicable != null && physicianOrderedDateNotApplicable != undefined) {
                if (physicianOrderedDateNotApplicable.Answer == 1) {

                    $('input[name=FollowUp_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', true);
                    $("#FollowUp_M0102PhysicianOrderedDate").val(" ");
                }
                else {
                    $('input[name=FollowUp_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                    $("#FollowUp_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
                }
            }
            else {
                $('input[name=FollowUp_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                $("#FollowUp_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
            }

            $("#FollowUp_M0104ReferralDate").val(result["M0104ReferralDate"] != null && result["M0104ReferralDate"] != undefined ? result["M0104ReferralDate"].Answer : "");

            $('input[name=FollowUp_M0110EpisodeTiming][value=' + (result["M0110EpisodeTiming"] != null && result["M0110EpisodeTiming"] != undefined ? result["M0110EpisodeTiming"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=FollowUp_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#FollowUp_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=FollowUp_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }

            $('input[name=FollowUp_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);

            var ICD9M = result["M1020ICD9M"];
            if (ICD9M != null && ICD9M != undefined && ICD9M.Answer != null) {
                $("#FollowUp_M1020ICD9M").val(result["M1020ICD9M"] != null && result["M1020ICD9M"] != undefined ? result["M1020ICD9M"].Answer : "");
                $("#FollowUp_M1020PrimaryDiagnosis").val(result["M1020PrimaryDiagnosis"] != null && result["M1020PrimaryDiagnosis"] != undefined ? result["M1020PrimaryDiagnosis"].Answer : "");
                $("#FollowUp_M1020SymptomControlRating").val(result["M1020SymptomControlRating"] != null && result["M1020SymptomControlRating"] != undefined ? result["M1020SymptomControlRating"].Answer : "");
            }
            var ICD9MA3 = result["M1024ICD9MA3"];
            if (ICD9MA3 != null && ICD9MA3 != undefined && ICD9MA3.Answer != null) {
                $("#FollowUp_M1024ICD9MA3").val(result["M1024ICD9MA3"] != null && result["M1024ICD9MA3"] != undefined ? result["M1024ICD9MA3"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesA3").val(result["M1024PaymentDiagnosesA3"] != null && result["M1024PaymentDiagnosesA3"] != undefined ? result["M1024PaymentDiagnosesA3"].Answer : "");
            }
            var ICD9MA4 = result["M1024ICD9MA4"];
            if (ICD9MA4 != null && ICD9MA4 != undefined && ICD9MA4.Answer != null) {
                $("#FollowUp_M1024ICD9MA4").val(result["M1024ICD9MA4"] != null && result["M1024ICD9MA4"] != undefined ? result["M1024ICD9MA4"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesA4").val(result["M1024PaymentDiagnosesA4"] != null && result["M1024PaymentDiagnosesA4"] != undefined ? result["M1024PaymentDiagnosesA4"].Answer : "");
            }

            var ICD9M1 = result["M1022ICD9M1"];
            if (ICD9M1 != null && ICD9M1 != undefined && ICD9M1.Answer != null) {
                $("#FollowUp_M1022ICD9M1").val(result["M1022ICD9M1"] != null && result["M1022ICD9M1"] != undefined ? result["M1022ICD9M1"].Answer : "");
                $("#FollowUp_M1022PrimaryDiagnosis1").val(result["M1022PrimaryDiagnosis1"] != null && result["M1022PrimaryDiagnosis1"] != undefined ? result["M1022PrimaryDiagnosis1"].Answer : "");
                $("#FollowUp_M1022OtherDiagnose1Rating").val(result["M1022OtherDiagnose1Rating"] != null && result["M1022OtherDiagnose1Rating"] != undefined ? result["M1022OtherDiagnose1Rating"].Answer : "");
            }
            var ICD9MB3 = result["M1024ICD9MB3"];
            if (ICD9MB3 != null && ICD9MB3 != undefined && ICD9MB3.Answer != null) {
                $("#FollowUp_M1024ICD9MB3").val(result["M1024ICD9MB3"] != null && result["M1024ICD9MB3"] != undefined ? result["M1024ICD9MB3"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesB3").val(result["M1024PaymentDiagnosesB3"] != null && result["M1024PaymentDiagnosesB3"] != undefined ? result["M1024PaymentDiagnosesB3"].Answer : "");
            }
            var ICD9MB4 = result["M1024ICD9MB4"];
            if (ICD9MB4 != null && ICD9MB4 != undefined && ICD9MB4.Answer != null) {
                $("#FollowUp_M1024ICD9MB4").val(result["M1024ICD9MB4"] != null && result["M1024ICD9MB4"] != undefined ? result["M1024ICD9MB4"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesB4").val(result["M1024PaymentDiagnosesB4"] != null && result["M1024PaymentDiagnosesB4"] != undefined ? result["M1024PaymentDiagnosesB4"].Answer : "");
            }


            var ICD9M2 = result["M1022ICD9M2"];
            if (ICD9M2 != null && ICD9M2 != undefined && ICD9M2.Answer != null) {
                $("#FollowUp_M1022ICD9M2").val(result["M1022ICD9M2"] != null && result["M1022ICD9M2"] != undefined ? result["M1022ICD9M2"].Answer : "");
                $("#FollowUp_M1022PrimaryDiagnosis2").val(result["M1022PrimaryDiagnosis2"] != null && result["M1022PrimaryDiagnosis2"] != undefined ? result["M1022PrimaryDiagnosis2"].Answer : "");
                $("#FollowUp_M1022OtherDiagnose2Rating").val(result["M1022OtherDiagnose2Rating"] != null && result["M1022OtherDiagnose2Rating"] != undefined ? result["M1022OtherDiagnose2Rating"].Answer : "");
            }
            var ICD9MC3 = result["M1024ICD9MC3"];
            if (ICD9MC3 != null && ICD9MC3 != undefined && ICD9MC3.Answer != null) {
                $("#FollowUp_M1024ICD9MC3").val(result["M1024ICD9MC3"] != null && result["M1024ICD9MC3"] != undefined ? result["M1024ICD9MC3"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesC3").val(result["M1024PaymentDiagnosesC3"] != null && result["M1024PaymentDiagnosesC3"] != undefined ? result["M1024PaymentDiagnosesC3"].Answer : "");
            }
            var ICD9MC4 = result["M1024ICD9MC4"];
            if (ICD9MC4 != null && ICD9MC4 != undefined && ICD9MC4.Answer != null) {
                $("#FollowUp_M1024ICD9MC4").val(result["M1024ICD9MC4"] != null && result["M1024ICD9MC4"] != undefined ? result["M1024ICD9MC4"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesC4").val(result["M1024PaymentDiagnosesC4"] != null && result["M1024PaymentDiagnosesC4"] != undefined ? result["M1024PaymentDiagnosesC4"].Answer : "");
            }

            var ICD9M3 = result["M1022ICD9M3"];
            if (ICD9M3 != null && ICD9M3 != undefined && ICD9M3.Answer != null) {
                $("#FollowUp_M1022ICD9M3").val(result["M1022ICD9M3"] != null && result["M1022ICD9M3"] != undefined ? result["M1022ICD9M3"].Answer : "");
                $("#FollowUp_M1022PrimaryDiagnosis3").val(result["M1022PrimaryDiagnosis3"] != null && result["M1022PrimaryDiagnosis3"] != undefined ? result["M1022PrimaryDiagnosis3"].Answer : "");
                $("#FollowUp_M1022OtherDiagnose3Rating").val(result["M1022OtherDiagnose3Rating"] != null && result["M1022OtherDiagnose3Rating"] != undefined ? result["M1022OtherDiagnose3Rating"].Answer : "");
            }
            var ICD9MD3 = result["M1024ICD9MD3"];
            if (ICD9MD3 != null && ICD9MD3 != undefined && ICD9MD3.Answer != null) {
                $("#FollowUp_M1024ICD9MD3").val(result["M1024ICD9MD3"] != null && result["M1024ICD9MD3"] != undefined ? result["M1024ICD9MD3"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesD3").val(result["M1024PaymentDiagnosesD3"] != null && result["M1024PaymentDiagnosesD3"] != undefined ? result["M1024PaymentDiagnosesD3"].Answer : "");
            }
            var ICD9MD4 = result["M1024ICD9MD4"];
            if (ICD9MD4 != null && ICD9MD4 != undefined && ICD9MD4.Answer != null) {
                $("#FollowUp_M1024ICD9MD4").val(result["M1024ICD9MD4"] != null && result["M1024ICD9MD4"] != undefined ? result["M1024ICD9MD4"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesD4").val(result["M1024PaymentDiagnosesD4"] != null && result["M1024PaymentDiagnosesD4"] != undefined ? result["M1024PaymentDiagnosesD4"].Answer : "");
            }

            var ICD9M4 = result["M1022ICD9M4"];
            if (ICD9M4 != null && ICD9M4 != undefined && ICD9M4.Answer != null) {
                $("#FollowUp_M1022ICD9M4").val(result["M1022ICD9M4"] != null && result["M1022ICD9M4"] != undefined ? result["M1022ICD9M4"].Answer : "");
                $("#FollowUp_M1022PrimaryDiagnosis4").val(result["M1022PrimaryDiagnosis4"] != null && result["M1022PrimaryDiagnosis4"] != undefined ? result["M1022PrimaryDiagnosis4"].Answer : "");
                $("#FollowUp_M1022OtherDiagnose4Rating").val(result["M1022OtherDiagnose4Rating"] != null && result["M1022OtherDiagnose4Rating"] != undefined ? result["M1022OtherDiagnose4Rating"].Answer : "");
            }
            var ICD9ME3 = result["M1024ICD9ME3"];
            if (ICD9ME3 != null && ICD9ME3 != undefined && ICD9ME3.Answer != null) {
                $("#FollowUp_M1024ICD9ME3").val(result["M1024ICD9ME3"] != null && result["M1024ICD9ME3"] != undefined ? result["M1024ICD9ME3"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesE3").val(result["M1024PaymentDiagnosesE3"] != null && result["M1024PaymentDiagnosesE3"] != undefined ? result["M1024PaymentDiagnosesE3"].Answer : "");
            }
            var ICD9ME4 = result["M1024ICD9ME4"];
            if (ICD9ME4 != null && ICD9ME4 != undefined && ICD9ME4.Answer != null) {
                $("#FollowUp_M1024ICD9ME4").val(result["M1024ICD9ME4"] != null && result["M1024ICD9ME4"] != undefined ? result["M1024ICD9ME4"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesE4").val(result["M1024PaymentDiagnosesE4"] != null && result["M1024PaymentDiagnosesE4"] != undefined ? result["M1024PaymentDiagnosesE4"].Answer : "");
            }

            var ICD9M5 = result["M1022ICD9M5"];
            if (ICD9M5 != null && ICD9M5 != undefined && ICD9M5.Answer != null) {
                $("#FollowUp_M1022ICD9M5").val(result["M1022ICD9M5"] != null && result["M1022ICD9M5"] != undefined ? result["M1022ICD9M5"].Answer : "");
                $("#FollowUp_M1022PrimaryDiagnosis5").val(result["M1022PrimaryDiagnosis5"] != null && result["M1022PrimaryDiagnosis5"] != undefined ? result["M1022PrimaryDiagnosis5"].Answer : "");
                $("#FollowUp_M1022OtherDiagnose5Rating").val(result["M1022OtherDiagnose5Rating"] != null && result["M1022OtherDiagnose5Rating"] != undefined ? result["M1022OtherDiagnose5Rating"].Answer : "");
            }
            var ICD9MF3 = result["M1024ICD9MF3"];
            if (ICD9MF3 != null && ICD9MF3 != undefined && ICD9MF3.Answer != null) {
                $("#FollowUp_M1024ICD9MF3").val(result["M1024ICD9MF3"] != null && result["M1024ICD9MF3"] != undefined ? result["M1024ICD9MF3"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesF3").val(result["M1024PaymentDiagnosesF3"] != null && result["M1024PaymentDiagnosesF3"] != undefined ? result["M1024PaymentDiagnosesF3"].Answer : "");
            }
            var ICD9MF4 = result["M1024ICD9MF4"];
            if (ICD9MF4 != null && ICD9MF4 != undefined && ICD9MF4.Answer != null) {
                $("#FollowUp_M1024ICD9MF4").val(result["M1024ICD9MF4"] != null && result["M1024ICD9MF4"] != undefined ? result["M1024ICD9MF4"].Answer : "");
                $("#FollowUp_M1024PaymentDiagnosesF4").val(result["M1024PaymentDiagnosesF4"] != null && result["M1024PaymentDiagnosesF4"] != undefined ? result["M1024PaymentDiagnosesF4"].Answer : "");
            }




            $('input[name=FollowUp_M1030HomeTherapiesInfusion][value=' + (result["M1030HomeTherapiesInfusion"] != null && result["M1030HomeTherapiesInfusion"] != undefined ? result["M1030HomeTherapiesInfusion"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M1030HomeTherapiesParNutrition][value=' + (result["M1030HomeTherapiesParNutrition"] != null && result["M1030HomeTherapiesParNutrition"] != undefined ? result["M1030HomeTherapiesParNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M1030HomeTherapiesEntNutrition][value=' + (result["M1030HomeTherapiesEntNutrition"] != null && result["M1030HomeTherapiesEntNutrition"] != undefined ? result["M1030HomeTherapiesEntNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M1030HomeTherapiesNone][value=' + (result["M1030HomeTherapiesNone"] != null && result["M1030HomeTherapiesNone"] != undefined ? result["M1030HomeTherapiesNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M1200Vision][value=' + (result["M1200Vision"] != null && result["M1200Vision"] != undefined ? result["M1200Vision"].Answer : "") + ']').attr('checked', true);

            $('input[name=FollowUp_M1242PainInterferingFrequency][value=' + (result["M1242PainInterferingFrequency"] != null && result["M1242PainInterferingFrequency"] != undefined ? result["M1242PainInterferingFrequency"].Answer : "") + ']').attr('checked', true);

            var unhealedPressureUlcers = "";
            if (result["M1306UnhealedPressureUlcers"] != null && result["M1306UnhealedPressureUlcers"] != undefined) {
                unhealedPressureUlcers = result["M1306UnhealedPressureUlcers"].Answer
            }
            $('input[name=FollowUp_M1306UnhealedPressureUlcers][value=' + (unhealedPressureUlcers != "" ? unhealedPressureUlcers : "") + ']').attr('checked', true);

            if (unhealedPressureUlcers == "0") {
                $("#followUp_M1308").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else if (unhealedPressureUlcers == "1") {
                $("#FollowUp_M1308NumberNonEpithelializedStageTwoUlcerCurrent").val(result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedStageTwoUlcerAdmission").val(result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedStageThreeUlcerAdmission").val(result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedStageIVUlcerAdmission").val(result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : "");
                $("#FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "");
                $("#followUp_M1308").unblock();
            }

            $('input[name=FollowUp_M1322CurrentNumberStageIUlcer][value=' + (result["M1322CurrentNumberStageIUlcer"] != null && result["M1322CurrentNumberStageIUlcer"] != undefined ? result["M1322CurrentNumberStageIUlcer"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1324MostProblematicUnhealedStage][value=' + (result["M1324MostProblematicUnhealedStage"] != null && result["M1324MostProblematicUnhealedStage"] != undefined ? result["M1324MostProblematicUnhealedStage"].Answer : "") + ']').attr('checked', true);

            var stasisUlcer = "";
            if (result["M1330StasisUlcer"] != null && result["M1330StasisUlcer"] != undefined) {
                stasisUlcer = result["M1330StasisUlcer"].Answer
            }
            $('input[name=FollowUp_M1330StasisUlcer][value=' + (stasisUlcer != "" ? stasisUlcer : "") + ']').attr('checked', true);

            if (stasisUlcer == "00" || stasisUlcer == "03") {
                $("#followUp_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=FollowUp_M1332CurrentNumberStasisUlcer][value=' + (result["M1332CurrentNumberStasisUlcer"] != null && result["M1332CurrentNumberStasisUlcer"] != undefined ? result["M1332CurrentNumberStasisUlcer"].Answer : "") + ']').attr('checked', true);
                $('input[name=FollowUp_M1334StasisUlcerStatus][value=' + (result["M1334StasisUlcerStatus"] != null && result["M1334StasisUlcerStatus"] != undefined ? result["M1334StasisUlcerStatus"].Answer : "") + ']').attr('checked', true);
                $("#followUp_M1332AndM1334").unblock();
            }

            var surgicalWound = "";
            if (result["M1340SurgicalWound"] != null && result["M1340SurgicalWound"] != undefined) {
                surgicalWound = result["M1340SurgicalWound"].Answer
            }
            $('input[name=FollowUp_M1340SurgicalWound][value=' + (surgicalWound != "" ? surgicalWound : "") + ']').attr('checked', true);

            if (surgicalWound == "00" || surgicalWound == "02") {
                $("#followUp_M1342").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=FollowUp_M1342SurgicalWoundStatus][value=' + (result["M1342SurgicalWoundStatus"] != null && result["M1342SurgicalWoundStatus"] != undefined ? result["M1342SurgicalWoundStatus"].Answer : "") + ']').attr('checked', true);
                $("#followUp_M1342").unblock();
            }

            $('input[name=FollowUp_M1350SkinLesionOpenWound][value=' + (result["M1350SkinLesionOpenWound"] != null && result["M1350SkinLesionOpenWound"] != undefined ? result["M1350SkinLesionOpenWound"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1400PatientDyspneic][value=' + (result["M1400PatientDyspneic"] != null && result["M1400PatientDyspneic"] != undefined ? result["M1400PatientDyspneic"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1610UrinaryIncontinence][value=' + (result["M1610UrinaryIncontinence"] != null && result["M1610UrinaryIncontinence"] != undefined ? result["M1610UrinaryIncontinence"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1620BowelIncontinenceFrequency][value=' + (result["M1620BowelIncontinenceFrequency"] != null && result["M1620BowelIncontinenceFrequency"] != undefined ? result["M1620BowelIncontinenceFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1630OstomyBowelElimination][value=' + (result["M1630OstomyBowelElimination"] != null && result["M1630OstomyBowelElimination"] != undefined ? result["M1630OstomyBowelElimination"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1810CurrentAbilityToDressUpper][value=' + (result["M1810CurrentAbilityToDressUpper"] != null && result["M1810CurrentAbilityToDressUpper"] != undefined ? result["M1810CurrentAbilityToDressUpper"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1820CurrentAbilityToDressLower][value=' + (result["M1820CurrentAbilityToDressLower"] != null && result["M1820CurrentAbilityToDressLower"] != undefined ? result["M1820CurrentAbilityToDressLower"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1830CurrentAbilityToBatheEntireBody][value=' + (result["M1830CurrentAbilityToBatheEntireBody"] != null && result["M1830CurrentAbilityToBatheEntireBody"] != undefined ? result["M1830CurrentAbilityToBatheEntireBody"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1840ToiletTransferring][value=' + (result["M1840ToiletTransferring"] != null && result["M1840ToiletTransferring"] != undefined ? result["M1840ToiletTransferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1850Transferring][value=' + (result["M1850Transferring"] != null && result["M1850Transferring"] != undefined ? result["M1850Transferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M1860AmbulationLocomotion][value=' + (result["M1860AmbulationLocomotion"] != null && result["M1860AmbulationLocomotion"] != undefined ? result["M1860AmbulationLocomotion"].Answer : "") + ']').attr('checked', true);
            $('input[name=FollowUp_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);
            $("#FollowUp_M2200NumberOfTherapyNeed").val(result["M2200NumberOfTherapyNeed"] != null && result["M2200NumberOfTherapyNeed"] != undefined ? result["M2200NumberOfTherapyNeed"].Answer : "");

            var therapyNeed = result["M2200TherapyNeed"];
            if (therapyNeed !== null && therapyNeed != undefined) {
                if (therapyNeed.Answer == 1) {

                    $('input[name=FollowUp_M2200TherapyNeed][value=1]').attr('checked', true);
                    $("#FollowUp_M2200NumberOfTherapyNeed").val("");
                }
                else {
                    $('input[name=FollowUp_M2200TherapyNeed][value=1]').attr('checked', false);
                    $("#FollowUp_M2200NumberOfTherapyNeed").val(result["M2200NumberOfTherapyNeed"] != null && result["M2200NumberOfTherapyNeed"] != undefined ? result["M2200NumberOfTherapyNeed"].Answer : "");
                }
            }

        };
    }

}