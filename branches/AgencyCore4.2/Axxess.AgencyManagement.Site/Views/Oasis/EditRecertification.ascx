﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editRecertification" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span id="RecertificationTitle" class="float_left">Edit Recertification</span><span
                class="float_right"><a href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);"
                    class="window_resize"> </a><a href="javascript:void(0);" class="window_close">
                </a></span>
        </div>
        <div class="abs window_content general_form oasisAssWindowContent">
            <div class="oasisAssWindowContainer">
                <div id="editRecertificationTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
                    <ul>
                        <li><a href="#editClinicalRecord_recertification">Clinical Record Items</a></li>
                        <li><a href="#editPatienthistory_recertification">Patient History & Diagnoses</a></li>
                        <li><a href="#editRiskassessment_recertification">Risk Assessment</a></li>
                        <li><a href="#editPrognosis_recertification">Prognosis</a></li>
                        <li><a href="#editSupportiveassistance_recertification">Supportive Assistance</a></li>
                        <li><a href="#editSensorystatus_recertification">Sensory Status</a></li>
                        <li><a href="#editPain_recertification">Pain</a></li>
                        <li><a href="#editIntegumentarystatus_recertification">Integumentary Status</a></li>
                        <li><a href="#editRespiratorystatus_recertification">Respiratory Status</a></li>
                        <li><a href="#editEndocrine_recertification">Endocrine</a></li>
                        <li><a href="#editCardiacstatus_recertification">Cardiac Status</a></li>
                        <li><a href="#editEliminationstatus_recertification">Elimination Status</a></li>
                        <li><a href="#editNutrition_recertification">Nutrition</a></li>
                        <li><a href="#editBehaviourialstatus_recertification">Neuro/Behaviourial Status</a></li>
                        <li><a href="#editAdl_recertification">ADL/IADLs</a></li>
                        <li><a href="#suppliesworksheet_recertification">Supplies Worksheet</a></li>
                        <li><a href="#editMedications_recertification">Medications</a></li>
                        <li><a href="#editTherapyneed_recertification">Therapy Need & Plan Of Care</a></li>
                        <li><a href="#editOrdersdisciplinetreatment_recertification">Orders for Discipline and
                            Treatment</a></li>
                    </ul>
                     <div style="width: 179px;">
                        <input id="recertificationValidation" type="button" value="Validate" onclick="Recertification.Validate(); JQD.open_window('#validation');" /></div>
                    <div id="editClinicalRecord_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationDemographicsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0010) CMS Certification Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0010CertificationNumber" name="Recertification_M0010CertificationNumber"
                                        type="text" class="text" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0014) Branch State:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="Recertification_M0014BranchState" id="Recertification_M0014BranchState">
                                        <option value=" " selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0016) Branch ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0016BranchId" name="Recertification_M0016BranchId" type="text"
                                        class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0018) National Provider Identifier (NPI)</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0018NationalProviderId" name="Recertification_M0018NationalProviderId"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="Recertification_M0018NationalProviderIdUnknown" value=" " />
                                    <input type="checkbox" id="Recertification_M0018NationalProviderIdUnknown" name="Recertification_M0018NationalProviderIdUnknown"
                                        value="1" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0020) Patient ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0020PatientIdNumber" name="Recertification_M0020PatientIdNumber"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0030) Start of Care Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0030SocDate" name="Recertification_M0030SocDate" type="text"
                                        class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        Episode Start Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_GenericEpisodeStartDate" name="Recertification_GenericEpisodeStartDate"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0032) Resumption of Care Date:
                                    </div>
                                </div>
                                <div class="padding">
                                    <input id="Recertification_M0032ROCDate" name="Recertification_M0032ROCDate" type="text"
                                        class="text" />
                                    <br />
                                    <input type="hidden" name="Recertification_M0032ROCDateNotApplicable" value="" />
                                    <input id="Recertification_M0032ROCDateNotApplicable" name="Recertification_M0032ROCDateNotApplicable"
                                        type="checkbox" value="1" />&nbsp;NA - Not Applicable
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0040) Patient Name:</div>
                                </div>
                                <div class="right marginOasis">
                                    <div class="padding">
                                        Suffix :
                                        <input id="Recertification_M0040Suffix" name="Recertification_M0040Suffix" style="width: 20px;"
                                            type="text" class="text" />
                                        &nbsp; First :
                                        <input id="Recertification_M0040FirstName" name="Recertification_M0040FirstName"
                                            type="text" class="text" />
                                        <br />
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MI :
                                        <input id="Recertification_M0040MI" name="Recertification_M0040MI" style="width: 20px;"
                                            type="text" class="text" />&nbsp; &nbsp;Last:
                                        <input id="Recertification_M0040LastName" name="Recertification_M0040LastName" type="text"
                                            class="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0050) Patient State of Residence:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="Recertification_M0050PatientState" id="Recertification_M0050PatientState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0060) Patient Zip Code:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0060PatientZipCode" name="Recertification_M0060PatientZipCode"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0063) Medicare Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0063PatientMedicareNumber" name="Recertification_M0063PatientMedicareNumber"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="Recertification_M0063PatientMedicareNumberUnknown" value=" " />
                                    <input id="Recertification_M0063PatientMedicareNumberUnknown" name="Recertification_M0063PatientMedicareNumberUnknown"
                                        type="checkbox" value="1" />&nbsp;NA – No Medicare</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0064) Social Security Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0064PatientSSN" name="Recertification_M0064PatientSSN"
                                        type="text" class="text" /><br />
                                    <input name="Recertification_M0064PatientSSNUnknown" type="hidden" value=" " />
                                    <input id="Recertification_M0064PatientSSNUnknown" name="Recertification_M0064PatientSSNUnknown"
                                        type="checkbox" value="1" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0065) Medicaid Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0065PatientMedicaidNumber" name="Recertification_M0065PatientMedicaidNumber"
                                        type="text" class="text" /><br />
                                    <input name="Recertification_M0065PatientMedicaidNumberUnknown" type="hidden" value=" " />
                                    <input id="Recertification_M0065PatientMedicaidNumberUnknown" name="Recertification_M0065PatientMedicaidNumberUnknown"
                                        type="checkbox" value="1" />&nbsp;NA – No Medicaid</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0066) Birth Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0066PatientDoB" name="Recertification_M0066PatientDoB"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow  title">
                                    <div class="padding">
                                        (M0069) Gender:</div>
                                </div>
                                <div class="right marginOasis">
                                    <%=Html.Hidden("Recertification_M0069Gender", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("Recertification_M0069Gender", "1", new { @id = "" })%>Male
                                    <%=Html.RadioButton("Recertification_M0069Gender", "2", new { @id = "" })%>Female
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0080) Discipline of Person Completing Assessment:
                                    </div>
                                </div>
                                <div class="padding">
                                    <%=Html.Hidden("Recertification_M0080DisciplinePerson", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("Recertification_M0080DisciplinePerson", "01", new { @id = "" })%>&nbsp;1
                                    - RN
                                    <%=Html.RadioButton("Recertification_M0080DisciplinePerson", "02", new { @id = "" })%>&nbsp;2
                                    - PT
                                    <%=Html.RadioButton("Recertification_M0080DisciplinePerson", "03", new { @id = "" })%>&nbsp;3
                                    - SLP/ST
                                    <%=Html.RadioButton("Recertification_M0080DisciplinePerson", "04", new { @id = "" })%>&nbsp;4
                                    - OT
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0090) Date Assessment Completed:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="Recertification_M0090AssessmentCompleted" name="Recertification_M0090AssessmentCompleted"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis assessmentType">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow margin">
                                    <u>Start/Resumption of Care</u></div>
                                <div class="insiderow margin">
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="01" />&nbsp;1
                                    – Start of care—further visits planned<br />
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="03" />&nbsp;3
                                    – Resumption of care (after inpatient stay)<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow">
                                    <u>Follow-Up</u></div>
                                <div class="insiderow">
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="04" />&nbsp;4
                                    – Recertification (follow-up) reassessment [ Go to M0110 ]<br />
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="05" />&nbsp;5
                                    – Other follow-up [ Go to M0110 ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Transfer to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="06" />&nbsp;6
                                    – Transferred to an inpatient facility—patient not discharged from agency [ Go to
                                    M1040]<br />
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="07" />&nbsp;7
                                    – Transferred to an inpatient facility—patient discharged from agency [ Go to M1040
                                    ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Discharge from Agency — Not to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="08" />&nbsp;8
                                    – Death at home [ Go to M0903 ]<br />
                                    <input name="Recertification_M0100AssessmentType" type="radio" value="09" />&nbsp;9
                                    – Discharge from agency [ Go to M1040 ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0102) Date of Physician-ordered Start of Care (Resumption of Care): If the physician
                                        indicated a specific start of care (resumption of care) date when the patient was
                                        referred for home health services, record the date specified.</div>
                                </div>
                                <div class="padding">
                                    <input id="Recertification_M0102PhysicianOrderedDate" name="Recertification_M0102PhysicianOrderedDate"
                                        type="text" class="text" />
                                    [ Go to M0110, if date entered ]<br />
                                    <input type="hidden" name="Recertification_M0102PhysicianOrderedDateNotApplicable"
                                        value="" />
                                    <input id="Recertification_M0102PhysicianOrderedDateNotApplicable" name="Recertification_M0102PhysicianOrderedDateNotApplicable"
                                        type="checkbox" value="1" />&nbsp;NA –No specific SOC date ordered by physician
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0104) Date of Referral: Indicate the date that the written or verbal referral
                                        for initiation or resumption of care was received by the HHA.
                                    </div>
                                </div>
                                <div class="padding">
                                    <input id="Recertification_M0104ReferralDate" name="Recertification_M0104ReferralDate"
                                        type="text" class="text" />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0110) Episode Timing: Is the Medicare home health payment episode for which this
                                        assessment will define a case mix group an “early” episode or a “later” episode
                                        in the patient’s current sequence of adjacent Medicare home health payment episodes?
                                    </div>
                                </div>
                                <div class="padding">
                                    <input type="hidden" name="Recertification_M0110EpisodeTiming" value="" />
                                    <input name="Recertification_M0110EpisodeTiming" type="radio" value="01" />&nbsp;1
                                    - Early
                                    <input name="Recertification_M0110EpisodeTiming" type="radio" value="02" />&nbsp;2
                                    - Later
                                    <input name="Recertification_M0110EpisodeTiming" type="radio" value="UK" />&nbsp;UK
                                    - Unknown
                                    <input name="Recertification_M0110EpisodeTiming" type="radio" value="NA" />&nbsp;NA
                                    - Not Applicable: No Medicare case mix group
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0140) Race/Ethnicity: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="Recertification_M0140RaceAMorAN" value="" />
                                    <input name="Recertification_M0140RaceAMorAN" type="checkbox" value="1" />&nbsp;1
                                    - American Indian or Alaska Native<br />
                                    <input type="hidden" name="Recertification_M0140RaceAsia" value="" />
                                    <input name="Recertification_M0140RaceAsia" type="checkbox" value="1" />&nbsp;2
                                    - Asian<br />
                                    <input type="hidden" name="Recertification_M0140RaceBalck" value="" />
                                    <input name="Recertification_M0140RaceBalck" type="checkbox" value="1" />&nbsp;3
                                    - Black or African-American<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="Recertification_M0140RaceHispanicOrLatino" value="" />
                                    <input name="Recertification_M0140RaceHispanicOrLatino" type="checkbox" value="1" />&nbsp;4
                                    - Hispanic or Latino<br />
                                    <input type="hidden" name="Recertification_M0140RaceNHOrPI" value="" />
                                    <input name="Recertification_M0140RaceNHOrPI" type="checkbox" value="1" />&nbsp;5
                                    - Native Hawaiian or Pacific Islander<br />
                                    <input type="hidden" name="Recertification_M0140RaceWhite" value="" />
                                    <input name="Recertification_M0140RaceWhite" type="checkbox" value="1" />&nbsp;6
                                    - White
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull title">
                                <div class="padding">
                                    (M0150) Current Payment Sources for Home Care: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol ">
                                <div class="padding">
                                    <input type="hidden" name="Recertification_M0150PaymentSourceNone" value="" />
                                    <input name="Recertification_M0150PaymentSourceNone" type="checkbox" value="1" />&nbsp;0
                                    - None; no charge for current services<br />
                                    <input type="hidden" name="Recertification_M0150PaymentSourceMCREFFS" value="" />
                                    <input name="Recertification_M0150PaymentSourceMCREFFS" type="checkbox" value="1" />&nbsp;1
                                    - Medicare (traditional fee-for-service)<br />
                                    <input type="hidden" name="Recertification_M0150PaymentSourceMCREHMO" value="" />
                                    <input name="Recertification_M0150PaymentSourceMCREHMO" type="checkbox" value="1" />&nbsp;2
                                    - Medicare (HMO/managed care/Advantage plan)<br />
                                    <input type="hidden" name="Recertification_M0150PaymentSourceMCAIDFFS" value="" />
                                    <input name="Recertification_M0150PaymentSourceMCAIDFFS" type="checkbox" value="1" />&nbsp;3
                                    - Medicaid (traditional fee-for-service)<br />
                                    <input type="hidden" name="Recertification_M0150PaymentSourceMACIDHMO" value="" />
                                    <input name="Recertification_M0150PaymentSourceMACIDHMO" type="checkbox" value="1" />&nbsp;4
                                    - Medicaid (HMO/managed care)
                                    <br />
                                    <input type="hidden" name="Recertification_M0150PaymentSourceWRKCOMP" value="" />
                                    <input name="Recertification_M0150PaymentSourceWRKCOMP" type="checkbox" value="1" />&nbsp;5
                                    - Workers' compensation<br />
                                    <input type="hidden" name="Recertification_M0150PaymentSourceTITLPRO" value="" />
                                    <input name="Recertification_M0150PaymentSourceTITLPRO" type="checkbox" value="1" />&nbsp;6
                                    - Title programs (e.g., Title III, V, or XX)<br />
                                </div>
                            </div>
                            <div class="insideCol adjust">
                                <input type="hidden" name="Recertification_M0150PaymentSourceOTHGOVT" value="" />
                                <input name="Recertification_M0150PaymentSourceOTHGOVT" type="checkbox" value="1" />&nbsp;7
                                - Other government (e.g., TriCare, VA, etc.)<br />
                                <input type="hidden" name="Recertification_M0150PaymentSourcePRVINS" value="" />
                                <input name="Recertification_M0150PaymentSourcePRVINS" type="checkbox" value="1" />&nbsp;8
                                - Private insurance<br />
                                <input type="hidden" name="Recertification_M0150PaymentSourcePRVHMO" value="" />
                                <input name="Recertification_M0150PaymentSourcePRVHMO" type="checkbox" value="1" />&nbsp;9
                                - Private HMO/managed care<br />
                                <input type="hidden" name="Recertification_M0150PaymentSourceSelfPay" value="" />
                                <input name="Recertification_M0150PaymentSourceSelfPay" type="checkbox" value="1" />&nbsp;10
                                - Self-pay<br />
                                <input type="hidden" name="Recertification_M0150PaymentSourceOtherSRS" value="" />
                                <input name="Recertification_M0150PaymentSourceOtherSRS" type="checkbox" value="1" />&nbsp;11
                                - Other (specify)&nbsp;&nbsp;&nbsp;<input type="text" name="Recertification_M0150PaymentSourceOther"
                                    id="Recertification_M0150PaymentSourceOther" /><br />
                                <input type="hidden" name="Recertification_M0150PaymentSourceUnknown" value="" />
                                <input name="Recertification_M0150PaymentSourceUnknown" type="checkbox" value="1" />&nbsp;UK
                                - Unknown<br />
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editPatienthistory_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationPatientHistoryForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Allergies(locator #17)
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_485Allergies" value="" />
                                        <input name="Recertification_485Allergies" value="No" type="radio" />&nbsp;NKA (Food/Drugs/Latex)
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485Allergies" value="Yes" type="radio" />&nbsp;Allergic
                                        to:<br />
                                        <textarea name="Recertification_485AllergiesDescription" id="Recertification_485AllergiesDescription"
                                            rows="4" style="width: 90%;" cols="5"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th colspan="7">
                                        Vital Signs
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="littleSpacer">Pulse: </li>
                                            <li class="littleSpacer">Apical: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericPulseApical" id="Recertification_GenericPulseApical"
                                                    size="7" maxlength="7" value="" />
                                            </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPulseApicalRegular" value="" />
                                                <input name="Recertification_GenericPulseApicalRegular" value="1" type="radio" />
                                                (Reg) </li>
                                            <li>
                                                <input name="Recertification_GenericPulseApicalRegular" value="2" type="radio" />
                                                (Irreg) </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp;</li>
                                            <li class="littleSpacer">Radial: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericPulseRadial" id="Recertification_GenericPulseRadial"
                                                    size="7" maxlength="7" value="" />
                                            </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPulseRadialRegular" value="" />
                                                <input name="Recertification_GenericPulseRadialRegular" value="1" type="radio" />
                                                (Reg) </li>
                                            <li>
                                                <input name="Recertification_GenericPulseRadialRegular" value="2" type="radio" />
                                                (Irreg) </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li class="littleSpacer">Height: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericHeight" id="Recertification_GenericHeight"
                                                    size="7" maxlength="7" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">Weight: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericWeight" id="Recertification_GenericWeight"
                                                    size="7" maxlength="7" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td rowspan="2">
                                        <ul class="columns">
                                            <li class="littleSpacer">BP </li>
                                            <li class="littleSpacer">Lying </li>
                                            <li class="littleSpacer">Sitting </li>
                                            <li class="littleSpacer">Standing </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">Left </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericBPLeftLying" id="Recertification_GenericBPLeftLying"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericBPLeftSitting" id="Recertification_GenericBPLeftSitting"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericBPLeftStanding" id="Recertification_GenericBPLeftStanding"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">Right</li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericBPRightLying" id="Recertification_GenericBPRightLying"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericBPRightSitting" id="Recertification_GenericBPRightSitting"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericBPRightStanding" id="Recertification_GenericBPRightStanding"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="littleSpacer">Temp: </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_GenericTemp" id="Recertification_GenericTemp"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                            <li class="littleSpacer">Resp: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericResp" id="Recertification_GenericResp"
                                                    size="8" maxlength="8" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_GenericVitalSignsActualStated" value="" />
                                                <input name="Recertification_GenericVitalSignsActualStated" value="1" type="radio" />
                                                Actual </li>
                                            <li>
                                                <input name="Recertification_GenericVitalSignsActualStated" value="2" type="radio" />
                                                Stated </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>
                                        Notify Physician of:
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Temperature greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericTempGreaterThan" id="Recertification_GenericTempGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericTempLessThan" id="Recertification_GenericTempLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Pulse greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericPulseGreaterThan" id="Recertification_GenericPulseGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericPulseLessThan" id="Recertification_GenericPulseLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Respirations greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericRespirationGreaterThan" id="Recertification_GenericRespirationGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericRespirationLessThan" id="Recertification_GenericRespirationLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Systolic BP greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericSystolicBPGreaterThan" id="Recertification_GenericSystolicBPGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericSystolicBPLessThan" id="Recertification_GenericSystolicBPLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Diastolic BP greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDiastolicBPGreaterThan" id="Recertification_GenericDiastolicBPGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDiastolicBPLessThan" id="Recertification_GenericDiastolicBPLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>O2 Sat less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_Generic02SatLessThan" id="Recertification_Generic02SatLessThan"
                                                    size="5" maxlength="5" value="" />
                                                % </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Fasting blood sugar greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericFastingBloodSugarGreaterThan" id="Recertification_GenericFastingBloodSugarGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericFastingBloodSugarLessThan" id="Recertification_GenericFastingBloodSugarLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Random blood sugar greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericRandomBloddSugarGreaterThan" id="Recertification_GenericRandomBloddSugarGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericRandomBloddSugarLessThan" id="Recertification_GenericRandomBloddSugarLessThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Weight greater than (>) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericWeightGreaterThan" id="Recertification_GenericWeightGreaterThan"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                            <li>lbs or less than (<) </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericWeightLessThan" id="Recertification_GenericWeightLessThan"
                                                    size="5" maxlength="5" value="" />
                                                lbs </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Past Medical History <i>(Mark all that apply)</i>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_GenericMedicalHistory" />
                                                <input name="Recertification_GenericMedicalHistory" value="1" type="checkbox" />
                                                CHF </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="2" type="checkbox" />
                                                Cardiomyopathy </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="3" type="checkbox" />
                                                Arrhythmia </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="4" type="checkbox" />
                                                Chest Pain </li>
                                            <li class="littleSpacer">
                                                <input name="Recertification_GenericMedicalHistory" value="5" type="checkbox" />
                                                MI </li>
                                            <li class="littleSpacer">
                                                <input name="Recertification_GenericMedicalHistory" value="6" type="checkbox" />
                                                CAD </li>
                                            <li class="littleSpacer">
                                                <input name="Recertification_GenericMedicalHistory" value="7" type="checkbox" />
                                                HTN </li>
                                            <li class="littleSpacer">
                                                <input name="Recertification_GenericMedicalHistory" value="8" type="checkbox" />
                                                PVD </li>
                                            <li class="littleSpacer">
                                                <input name="Recertification_GenericMedicalHistory" value="9" type="checkbox" />
                                                Murmur </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="10" type="checkbox" />
                                                Cancer &nbsp; </li>
                                            <li><i>(specify type)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryCancerType" id="Recertification_GenericMedicalHistoryCancerType"
                                                    size="40" maxlength="40" value="" />
                                                &nbsp; In remission?&nbsp;
                                                <input type="hidden" name="Recertification_GenericMedicalHistoryCancerRemission"
                                                    value=" " />
                                                <input type="radio" name="Recertification_GenericMedicalHistoryCancerRemission" value="1" />
                                                Yes &nbsp;
                                                <input type="radio" name="Recertification_GenericMedicalHistoryCancerRemission" value="0" />
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="11" type="checkbox" />
                                                Osteoarthritis/DJD </li>
                                            <li><i>(specify sites affected)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryOsteoarthritisSites"
                                                    id="Recertification_GenericMedicalHistoryOsteoarthritisSites" size="60" maxlength="60"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="12" type="checkbox" />
                                                Rheumatoid Arthritis </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="13" type="checkbox" />
                                                Gait Problems </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="14" type="checkbox" />
                                                Fractures </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="15" type="checkbox" />
                                                Falls </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="16" type="checkbox" />
                                                Joint Replacement </li>
                                            <li><i>(specify joint)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryJointReplacementLocation"
                                                    id="Recertification_GenericMedicalHistoryJointReplacementLocation" size="50"
                                                    maxlength="50" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="17" type="checkbox" />
                                                CVA </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="18" type="checkbox" />
                                                TIA </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="19" type="checkbox" />
                                                MS </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="20" type="checkbox" />
                                                Hemiplegia </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="21" type="checkbox" />
                                                Seizures </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="22" type="checkbox" />
                                                Headaches </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="23" type="checkbox" />
                                                Dizziness/Vertigo </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="24" type="checkbox" />
                                                IBS </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="25" type="checkbox" />
                                                Crohn's Disease </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="26" type="checkbox" />
                                                Diverticulitis/Diverticulosis </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="27" type="checkbox" />
                                                Constipation </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="28" type="checkbox" />
                                                Diarrhea </li>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="29" type="checkbox" />
                                                Fecal Incontinence </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="30" type="checkbox" />
                                                Liver/Gallbladder Problems </li>
                                            <li><i>(specify)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryLiverGallBladderDesc"
                                                    id="Recertification_GenericMedicalHistoryLiverGallBladderDesc" size="60" maxlength="60"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="31" type="checkbox" />
                                                Depression </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="32" type="checkbox" />
                                                Anxiety </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="33" type="checkbox" />
                                                Dementia </li>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="34" type="checkbox" />
                                                Alzheimer's </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="35" type="checkbox" />
                                                Substance Abuse </li>
                                            <li><i>(specify)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistorySubstanceAbuseDesc"
                                                    id="Recertification_GenericMedicalHistorySubstanceAbuseDesc" size="80" maxlength="80"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="36" type="checkbox" />
                                                Mental Disorder </li>
                                            <li><i>(specify)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryMentalDisorderDesc"
                                                    id="Recertification_GenericMedicalHistoryMentalDisorderDesc" size="80" maxlength="80"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="37" type="checkbox" />
                                                Pressure Ulcer </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="38" type="checkbox" />
                                                Stasis Ulcer </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="39" type="checkbox" />
                                                Diabetic Ulcer </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="40" type="checkbox" />
                                                Trauma Wound </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="41" type="checkbox" />
                                                Other </li>
                                            <li><i>(specify)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryOtherDetails" id="Recertification_GenericMedicalHistoryOtherDetails"
                                                    size="80" maxlength="80" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="42" type="checkbox" />
                                                Chronic Kidney Disease </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="43" type="checkbox" />
                                                Renal Failure </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="44" type="checkbox" />
                                                Dialysis </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="45" type="checkbox" />
                                                Anemia </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="46" type="checkbox" />
                                                Abnormal Coagulation </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="47" type="checkbox" />
                                                Blood Clots </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="48" type="checkbox" />
                                                Diabetes </li>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="49" type="checkbox" />
                                                Thyroid Problems </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="50" type="checkbox" />
                                                COPD </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="51" type="checkbox" />
                                                Asthma </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="52" type="checkbox" />
                                                Chronic Obstructive Bronchitis </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="53" type="checkbox" />
                                                Emphysema </li>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="54" type="checkbox" />
                                                Chronic Obstructive Asthma </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="55" type="checkbox" />
                                                Urinary Incontinence </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="56" type="checkbox" />
                                                Urinary Retention </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="57" type="checkbox" />
                                                BPH </li>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="58" type="checkbox" />
                                                Recent/Frequent UTI </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="59" type="checkbox" />
                                                Tuberculosis </li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="60" type="checkbox" />
                                                Hepatitis </li>
                                            <li><i>(specify)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryHepatitisDetails" id="Recertification_GenericMedicalHistoryHepatitisDetails"
                                                    size="60" maxlength="80" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="61" type="checkbox" />
                                                Infectious Disease </li>
                                            <li><i>(specify)</i>
                                                <input type="text" name="Recertification_GenericMedicalHistoryInfectiousDiseaseDetails"
                                                    id="Recertification_GenericMedicalHistoryInfectiousDiseaseDetails" size="80"
                                                    maxlength="80" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li class="spacer">
                                                <input name="Recertification_GenericMedicalHistory" value="62" type="checkbox" />
                                                Tobacco Dependence </li>
                                            <li class="spacer">Type:
                                                <input type="text" name="Recertification_GenericMedicalHistoryTobaccoDependenceType"
                                                    id="Recertification_GenericMedicalHistoryTobaccoDependenceType" size="10" maxlength="60"
                                                    value="" />
                                            </li>
                                            <li class="spacer">Amount:
                                                <input type="text" name="Recertification_GenericMedicalHistoryTobaccoDependenceAmt"
                                                    id="Recertification_GenericMedicalHistoryTobaccoDependenceAmt" size="10" maxlength="60"
                                                    value="" />
                                            </li>
                                            <li>Length of Time Used:
                                                <input type="text" name="Recertification_GenericMedicalHistoryTobaccoDependenceLength"
                                                    id="Recertification_GenericMedicalHistoryTobaccoDependenceTypeLength" size="10"
                                                    maxlength="60" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="63" type="checkbox" />
                                                Vision Problems </li>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="64" type="checkbox" />
                                                Hearing Loss </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="65" type="checkbox" />
                                                Other </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericMedicalHistoryExtraDetails" id="Recertification_GenericMedicalHistoryExtraDetails"
                                                    size="80" maxlength="80" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericMedicalHistory" value="66" type="checkbox" />
                                                Past Surgical History: </li>
                                            <li>
                                                <textarea name="Recertification_GenericMedicalHistoryPastSuguriesDetails" id="Recertification_GenericMedicalHistoryPastSuguriesDetails"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M1020/1022/1024) Diagnoses, Symptom Control, and Payment Diagnoses: List each diagnosis
                                        for which the patient is receiving home care (Column 1) and enter its ICD-9-C M
                                        code at the level of highest specificity (no surgical/procedure codes) (Column 2).
                                        Diagnoses are listed in the order that best reflect the seriousness of each condition
                                        and support the disciplines and services provided. Rate the degree of symptom control
                                        for each condition (Column 2). Choose one value that represents the degree of symptom
                                        control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes
                                        (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed
                                        if multiple coding is indicated for any diagnoses. If a V-code is reported in place
                                        of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and
                                        4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare
                                        P P S case mix group. Do not assign symptom control ratings for V- or E-codes.<br />
                                        <b>Code each row according to the following directions for each column:<br />
                                        </b>Column 1: Enter the description of the diagnosis.<br />
                                        Column 2: Enter the ICD-9-C M code for the diagnosis described in Column 1;<br />
                                        Rate the degree of symptom control for the condition listed in Column 1 using the
                                        following scale:<br />
                                        0 - Asymptomatic, no treatment needed at this time<br />
                                        1 - Symptoms well controlled with current therapy<br />
                                        2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs
                                        ongoing monitoring<br />
                                        3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and
                                        dose monitoring<br />
                                        4 - Symptoms poorly controlled; history of re-hospitalizations Note that in Column
                                        2 the rating for symptom control of each diagnosis should not be used to determine
                                        the sequencing of the diagnoses listed in Column 1. These are separate items and
                                        sequencing may not coincide. Sequencing of diagnoses should reflect the seriousness
                                        of each condition and support the disciplines and services provided.<br />
                                        Column 3: (OPTIONAL) If a V-code is assigned to any row in Column 2, in place of
                                        a case mix diagnosis, it may be necessary to complete optional item M1024 Payment
                                        Diagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.<br />
                                        Column 4: (OPTIONAL) If a V-code in Column 2 is reported in place of a case mix
                                        diagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,
                                        enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns
                                        3 and 4. For example, if the case mix diagnosis is a manifestation code, record
                                        the diagnosis description and ICD-9-C M code for the underlying condition in Column
                                        3 of that row and the diagnosis description and ICD-9-C M code for the manifestation
                                        in Column 4 of that row. Otherwise, leave Column 4 blank in that row.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th colspan="2">
                                        (M1020) Primary Diagnosis & (M1022) Other Diagnoses
                                    </th>
                                    <th colspan="2">
                                        (M1024) Payment Diagnoses (OPTIONAL)
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Column 1
                                    </th>
                                    <th>
                                        Column 2
                                    </th>
                                    <th>
                                        Column 3
                                    </th>
                                    <th>
                                        Column 4
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition
                                        and support the disciplines and services provided.)
                                    </td>
                                    <td>
                                        ICD-9-C M and symptom control rating for each condition. Note that the sequencing
                                        of these ratings may not match the sequencing of the diagnoses
                                    </td>
                                    <td>
                                        Complete if a V-code is assigned under certain circumstances to Column 2 in place
                                        of a case mix diagnosis.
                                    </td>
                                    <td>
                                        Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis
                                        that is a multiple coding situation (e.g., a manifestation code).
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description
                                    </td>
                                    <td>
                                        ICD-9-C M / Symptom Control Rating
                                    </td>
                                    <td>
                                        Description/ ICD-9-C M
                                    </td>
                                    <td>
                                        Description/ ICD-9-C M
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ICDText">
                                        <u>(M1020) Primary Diagnosis<br />
                                        </u>a.<input name="Recertification_M1020PrimaryDiagnosis" type="text" id="Recertification_M1020PrimaryDiagnosis"
                                            class="diagnosis" /><br />
                                        <br />
                                    </td>
                                    <td valign="top">
                                        <u>(V-codes are allowed)<br />
                                        </u>a.<input name="Recertification_M1020ICD9M" type="text" id="Recertification_M1020ICD9M"
                                            class="ICD" /><br />
                                        <b>Severity:</b>
                                        <select name="Recertification_M1020SymptomControlRating" id="Recertification_M1020SymptomControlRating"
                                            class="pad">
                                            <option value=" " selected="true"></option>
                                            <option value="00">0</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                        </select>
                                    </td>
                                    <td valign="top">
                                        <u>(V- or E-codes NOT allowed)<br />
                                        </u>a.<input name="Recertification_M1024PaymentDiagnosesA3" type="text" id="Recertification_M1024PaymentDiagnosesA3"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MA3" type="text" id="Recertification_M1024ICD9MA3"
                                            class="ICDM1024 pad" />
                                    </td>
                                    <td valign="top">
                                        <u>(V- or E-codes NOT allowed)</u>
                                        <br />
                                        a.<input name="Recertification_M1024PaymentDiagnosesA4" type="text" id="Recertification_M1024PaymentDiagnosesA4"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MA4" type="text" id="Recertification_M1024ICD9MA4"
                                            class="ICDM1024 pad" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ICDText">
                                        <u>(M1022) Other Diagnoses<br />
                                        </u>b.<input name="Recertification_M1022PrimaryDiagnosis1" type="text" id="Recertification_M1022PrimaryDiagnosis1"
                                            class="diagnosis" /><br />
                                    </td>
                                    <td>
                                        <u>(V- or E-codes are allowed)<br />
                                        </u>b.<input type="text" name="Recertification_M1022ICD9M1" id="Recertification_M1022ICD9M1"
                                            class="ICD" /><br />
                                        <b>Severity:</b>
                                        <select name="Recertification_M1022OtherDiagnose1Rating" id="Recertification_M1022OtherDiagnose1Rating"
                                            class="pad">
                                            <option value=" " selected="true"></option>
                                            <option value="00">0</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                        </select>
                                    </td>
                                    <td>
                                        b.<input name="Recertification_M1024PaymentDiagnosesB3" type="text" id="Recertification_M1024PaymentDiagnosesB3"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MB3" type="text" id="Recertification_M1024ICD9MB3"
                                            class="ICDM1024 pad" />
                                    </td>
                                    <td>
                                        b.<input name="Recertification_M1024PaymentDiagnosesB4" type="text" id="Recertification_M1024PaymentDiagnosesB4"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MB4" type="text" id="Recertification_M1024ICD9MB4"
                                            class="ICDM1024 pad" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ICDText">
                                        c.<input name="Recertification_M1022PrimaryDiagnosis2" type="text" id="Recertification_M1022PrimaryDiagnosis2"
                                            class="diagnosis" /><br />
                                    </td>
                                    <td>
                                        c.<input type="text" name="Recertification_M1022ICD9M2" id="Recertification_M1022ICD9M2"
                                            class="ICD" /><br />
                                        <b>Severity:</b>
                                        <select name="Recertification_M1022OtherDiagnose2Rating" id="Recertification_M1022OtherDiagnose2Rating"
                                            class="pad">
                                            <option value=" " selected="true"></option>
                                            <option value="00">0</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                        </select>
                                    </td>
                                    <td>
                                        c.<input name="Recertification_M1024PaymentDiagnosesC3" type="text" id="Recertification_M1024PaymentDiagnosesC3"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MC3" type="text" id="Recertification_M1024ICD9MC3"
                                            class="ICDM1024 pad" />
                                    </td>
                                    <td>
                                        c.<input name="Recertification_M1024PaymentDiagnosesC4" type="text" id="Recertification_M1024PaymentDiagnosesC4"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MC4" type="text" id="Recertification_M1024ICD9MC4"
                                            class="ICDM1024 pad" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ICDText">
                                        d.<input name="Recertification_M1022PrimaryDiagnosis3" type="text" id="Recertification_M1022PrimaryDiagnosis3"
                                            class="diagnosis" /><br />
                                    </td>
                                    <td>
                                        d.<input type="text" name="Recertification_M1022ICD9M3" id="Recertification_M1022ICD9M3"
                                            class="ICD" /><br />
                                        <b>Severity:</b>
                                        <select name="Recertification_M1022OtherDiagnose3Rating" id="Recertification_M1022OtherDiagnose3Rating"
                                            class="pad">
                                            <option value=" " selected="true"></option>
                                            <option value="00">0</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                        </select>
                                    </td>
                                    <td>
                                        d.<input name="Recertification_M1024PaymentDiagnosesD3" type="text" id="Recertification_M1024PaymentDiagnosesD3"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MD3" type="text" id="Recertification_M1024ICD9MD3"
                                            class="ICDM1024 pad" />
                                    </td>
                                    <td>
                                        d.<input name="Recertification_M1024PaymentDiagnosesD4" type="text" id="Recertification_M1024PaymentDiagnosesD4"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MD4" type="text" id="Recertification_M1024ICD9MD4"
                                            class="ICDM1024 pad" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ICDText">
                                        e.<input name="Recertification_M1022PrimaryDiagnosis4" type="text" id="Recertification_M1022PrimaryDiagnosis4"
                                            class="diagnosis" /><br />
                                    </td>
                                    <td>
                                        e.<input type="text" name="Recertification_M1022ICD9M4" id="Recertification_M1022ICD9M4"
                                            class="ICD" /><br />
                                        <b>Severity:</b>
                                        <select name="Recertification_M1022OtherDiagnose4Rating" id="Recertification_M1022OtherDiagnose4Rating"
                                            class="pad">
                                            <option value=" " selected="true"></option>
                                            <option value="00">0</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                        </select>
                                    </td>
                                    <td>
                                        e.<input name="Recertification_M1024PaymentDiagnosesE3" type="text" id="Recertification_M1024PaymentDiagnosesE3"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9ME3" type="text" id="Recertification_M1024ICD9ME3"
                                            class="ICDM1024 pad" />
                                    </td>
                                    <td>
                                        e.<input name="Recertification_M1024PaymentDiagnosesE4" type="text" id="Recertification_M1024PaymentDiagnosesE4"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9ME4" type="text" id="Recertification_M1024ICD9ME4"
                                            class="ICDM1024 pad" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ICDText">
                                        f.<input name="Recertification_M1022PrimaryDiagnosis5" type="text" id="Recertification_M1022PrimaryDiagnosis5"
                                            class="diagnosis" /><br />
                                    </td>
                                    <td>
                                        f.<input type="text" name="Recertification_M1022ICD9M5" id="Recertification_M1022ICD9M5"
                                            class="ICD" /><br />
                                        <b>Severity:</b>
                                        <select name="Recertification_M1022OtherDiagnose5Rating" id="Recertification_M1022OtherDiagnose5Rating"
                                            class="pad">
                                            <option value=" " selected="true"></option>
                                            <option value="00">0</option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                        </select>
                                    </td>
                                    <td>
                                        f.<input name="Recertification_M1024PaymentDiagnosesF3" type="text" id="Recertification_M1024PaymentDiagnosesF3"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MF3" type="text" id="Recertification_M1024ICD9MF3"
                                            class="ICDM1024 pad" />
                                    </td>
                                    <td>
                                        f.<input name="Recertification_M1024PaymentDiagnosesF4" type="text" id="Recertification_M1024PaymentDiagnosesF4"
                                            class="diagnosisM1024 ICDText" /><br />
                                        <input name="Recertification_M1024ICD9MF4" type="text" id="Recertification_M1024ICD9MF4"
                                            class="ICDM1024 pad" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1030) Therapies the patient receives at home: (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="padding">
                                    <input name="Recertification_M1030HomeTherapiesInfusion" value=" " type="hidden" />
                                    <input name="Recertification_M1030HomeTherapiesInfusion" value="1" type="checkbox" />&nbsp;1
                                    - Intravenous or infusion therapy (excludes TPN)<br />
                                    <input name="Recertification_M1030HomeTherapiesParNutrition" value=" " type="hidden" />
                                    <input name="Recertification_M1030HomeTherapiesParNutrition" value="1" type="checkbox" />&nbsp;2
                                    - Parenteral nutrition (TPN or lipids)<br />
                                    <input name="Recertification_M1030HomeTherapiesEntNutrition" value=" " type="hidden" />
                                    <input name="Recertification_M1030HomeTherapiesEntNutrition" value="1" type="checkbox" />&nbsp;3
                                    - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial
                                    entry into the alimentary canal)<br />
                                    <input name="Recertification_M1030HomeTherapiesNone" value=" " type="hidden" />
                                    <input name="Recertification_M1030HomeTherapiesNone" value="1" type="checkbox" />&nbsp;4
                                    - None of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editRiskassessment_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationRiskAssessmentForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="5">
                                        Most Recent Immunizations
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Pneumonia
                                    </td>
                                    <td>
                                        <input name="Recertification_485Pnemonia" value=" " type="hidden" />
                                        <input name="Recertification_485Pnemonia" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485Pnemonia" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485Pnemonia" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485PnemoniaDate" id="Recertification_485PnemoniaDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Flu
                                    </td>
                                    <td>
                                        <input name="Recertification_485Flu" value=" " type="hidden" />
                                        <input name="Recertification_485Flu" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485Flu" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485Flu" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485FluDate" id="Recertification_485FluDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tetanus
                                    </td>
                                    <td>
                                        <input name="Recertification_485Tetanus" value=" " type="hidden" />
                                        <input name="Recertification_485Tetanus" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485Tetanus" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485Tetanus" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485TetanusDate" id="Recertification_485TetanusDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TB
                                    </td>
                                    <td>
                                        <input name="Recertification_485TB" value=" " type="hidden" />
                                        <input name="Recertification_485TB" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485TB" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485TB" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485TBDate" id="Recertification_485TBDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TB Exposure
                                    </td>
                                    <td>
                                        <input name="Recertification_485TBExposure" value=" " type="hidden" />
                                        <input name="Recertification_485TBExposure" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485TBExposure" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485TBExposure" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485TBExposureDate" id="Recertification_485TBExposureDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Hepatitis B
                                    </td>
                                    <td>
                                        <input name="Recertification_485HepatitisB" value=" " type="hidden" />
                                        <input name="Recertification_485HepatitisB" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485HepatitisB" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485HepatitisB" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485HepatitisBDate" id="Recertification_485HepatitisBDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="5">
                                        Additional Immunizations
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="Recertification_485AdditionalImmunization1Name" id="Recertification_485AdditionalImmunization1Name" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdditionalImmunization1" value=" " type="hidden" />
                                        <input name="Recertification_485AdditionalImmunization1" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdditionalImmunization1" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdditionalImmunization1" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485AdditionalImmunization1Date" id="Recertification_485AdditionalImmunization1Date" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="Recertification_485AdditionalImmunization2Name" id="Recertification_485AdditionalImmunization2Name" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdditionalImmunization2" value=" " type="hidden" />
                                        <input name="Recertification_485AdditionalImmunization2" value="Yes" type="radio" />&nbsp;Yes
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdditionalImmunization2" value="No" type="radio" />&nbsp;No
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdditionalImmunization2" value="UK" type="radio" />&nbsp;Unknown
                                    </td>
                                    <td>
                                        Date:
                                        <input type="text" name="Recertification_485AdditionalImmunization2Date" id="Recertification_485AdditionalImmunization2Date" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        Comments:<br />
                                        <textarea style="width: 99%;" name="Recertification_485ImmunizationComments" id="Recertification_485ImmunizationComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Health Screening
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 40%;">
                                        Last Cholesterol Level:
                                    </td>
                                    <td>
                                        <input type="text" size="20" maxlength="20" name="Recertification_485LastCholesterolLevel"
                                            id="Recertification_485LastCholesterolLevel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Mammogram:
                                    </td>
                                    <td>
                                        <input type="text" size="20" maxlength="20" name="Recertification_485LastMammogram"
                                            id="Recertification_485LastMammogram" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient perform monthly self breast exams?
                                    </td>
                                    <td>
                                        <input name="Recertification_485PerformMonthlySelfBreastExams" value=" " type="hidden" />
                                        <input name="Recertification_485PerformMonthlySelfBreastExams" value="Yes" type="radio" />&nbsp;Yes
                                        <input name="Recertification_485PerformMonthlySelfBreastExams" value="No" type="radio" />&nbsp;No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Pap Smear:
                                    </td>
                                    <td>
                                        <input type="text" size="20" maxlength="20" name="Recertification_485LastPapSmear"
                                            id="Recertification_485LastPapSmear" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last PSA:
                                    </td>
                                    <td>
                                        <input type="text" size="20" maxlength="20" name="Recertification_485LastPSA" id="Recertification_485LastPSA" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Prostate Exam:
                                    </td>
                                    <td>
                                        <input type="text" size="20" maxlength="20" name="Recertification_485LastProstateExam"
                                            id="Recertification_485LastProstateExam" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Colonoscopy:
                                    </td>
                                    <td>
                                        <input type="text" size="20" maxlength="20" name="Recertification_485LastColonoscopy"
                                            id="Recertification_485LastColonoscopy" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485RiskAssessmentIntervention" value=" " />
                                        <input name="Recertification_485RiskAssessmentIntervention" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assist patient to obtain ERS button
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RiskAssessmentIntervention" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to develop individualized emergency plan with patient
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RiskAssessmentIntervention" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on importance of receiving influenza and pneumococcal vaccines
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RiskAssessmentIntervention" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to administer influenza vaccination as follows:
                                        <br />
                                        <input type="text" size="60" maxlength="75" name="Recertification_485AdministerVaccinationDetails"
                                            id="Recertification_485AdministerVaccinationDetails" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RiskAssessmentIntervention" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to administer pneumococcal vaccination as follows:
                                        <br />
                                        <input type="text" size="60" maxlength="75" name="Recertification_485AdministerPneumococcalVaccineDetails"
                                            id="Recertification_485AdministerPneumococcalVaccineDetails" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485RiskInterventionTemplates" name="Recertification_485RiskInterventionTemplates">
                                            <option value="0">Sample Template</option>
                                            <option value="-2" disabled="disabled">-----------------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea style="width: 99%;" name="Recertification_485RiskInterventionComments"
                                            id="Recertification_485RiskInterventionComments" rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485RiskAssessmentGoals" value=" " />
                                        <input name="Recertification_485RiskAssessmentGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        The patient will have no hospitalizations during the certification period
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RiskAssessmentGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeEmergencyPlanPerson" id="Recertification_485VerbalizeEmergencyPlanPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of individualized emergency plan by:
                                        <input type="text" size="20" maxlength="12" name="Recertification_485VerbalizeEmergencyPlanDate"
                                            id="Recertification_485VerbalizeEmergencyPlanDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485RiskGoalTemplates" name="Recertification_485RiskGoalTemplates">
                                            <option value="0">Sample Template</option>
                                            <option value="-2" disabled="disabled">-----------------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea style="width: 99%;" name="Recertification_485RiskGoalComments" id="Recertification_485RiskGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editPrognosis_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationPrognosisForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Prognosis (locator #20)
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485Prognosis" value=" " type="hidden" />
                                        <input name="Recertification_485Prognosis" value="Guarded" type="radio" />&nbsp;Guarded
                                        <input name="Recertification_485Prognosis" value="Poor" type="radio" />&nbsp;Poor
                                        <input name="Recertification_485Prognosis" value="Fair" type="radio" />&nbsp;Fair
                                        <input name="Recertification_485Prognosis" value="Good" type="radio" />&nbsp;Good
                                        <input name="Recertification_485Prognosis" value="Excellent" type="radio" />&nbsp;Excellent
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Advanced Directives
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Are there any advanced Directives?
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdvancedDirectives" value=" " type="hidden" />
                                        <input name="Recertification_485AdvancedDirectives" value="Yes" type="radio" />&nbsp;Yes
                                        <input name="Recertification_485AdvancedDirectives" value="No" type="radio" />&nbsp;No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Intent:&nbsp;
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdvancedDirectivesIntent" value=" " type="hidden" />
                                        <input name="Recertification_485AdvancedDirectivesIntent" value="DNR" type="radio" />&nbsp;DNR<br />
                                        <input name="Recertification_485AdvancedDirectivesIntent" value="Living Will" type="radio" />&nbsp;Living
                                        Will<br />
                                        <input name="Recertification_485AdvancedDirectivesIntent" value="Medical Power of Attorney"
                                            type="radio" />&nbsp;Medical Power of Attorney<br />
                                        <input name="Recertification_485AdvancedDirectivesIntent" value="Other" type="radio" />&nbsp;Other:
                                        (specify)
                                        <input id="Recertification_485AdvancedDirectivesIntentOther" name="Recertification_485AdvancedDirectivesIntentOther"
                                            maxlength="50" size="50" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Copy on file at agency?
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdvancedDirectivesCopyOnFile" value=" " type="hidden" />
                                        <input name="Recertification_485AdvancedDirectivesCopyOnFile" value="Yes" type="radio" />&nbsp;Yes
                                        <input name="Recertification_485AdvancedDirectivesCopyOnFile" value="No" type="radio" />&nbsp;No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Patient was provided written and verbal information on Advance Directives
                                    </td>
                                    <td>
                                        <input name="Recertification_485AdvancedDirectivesWrittenAndVerbal" value=" " type="hidden" />
                                        <input name="Recertification_485AdvancedDirectivesWrittenAndVerbal" value="Yes" type="radio" />&nbsp;Yes
                                        <input name="Recertification_485AdvancedDirectivesWrittenAndVerbal" value="No" type="radio" />&nbsp;No
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Is the Patient DNR (Do Not Resuscitate)?
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_GenericPatientDNR" value=" " type="hidden" />
                                        <input name="Recertification_GenericPatientDNR" value="Yes" type="radio" />&nbsp;Yes
                                        <input name="Recertification_GenericPatientDNR" value="No" type="radio" />&nbsp;No
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <th colspan="5">
                                            Functional Limitations (locator #18.A)
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value=" " type="hidden" />
                                            <input name="Recertification_485FunctionLimitations" value="Amputation" type="checkbox" />
                                            &nbsp; Amputation
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Paralysis" type="checkbox" />
                                            &nbsp; Paralysis
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Legally Blind" type="checkbox" />
                                            &nbsp; Legally Blind
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Bowel/Bladder Incontinence"
                                                type="checkbox" />
                                            &nbsp; Bowel/Bladder Incontinence
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Endurance" type="checkbox" />
                                            &nbsp; Endurance
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Dyspnea" type="checkbox" />
                                            &nbsp; Dyspnea
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Contracture" type="checkbox" />
                                            &nbsp; Contracture
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Ambulation" type="checkbox" />
                                            &nbsp; Ambulation
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Hearing" type="checkbox" />
                                            &nbsp; Hearing
                                        </td>
                                        <td>
                                            <input name="Recertification_485FunctionLimitations" value="Speech" type="checkbox" />
                                            &nbsp; Speech
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <ul>
                                                <li>
                                                    <input name="Recertification_485FunctionLimitations" value="Other" type="checkbox" />
                                                    &nbsp; Other </li>
                                                <li>
                                                    <textarea id="Recertification_485FunctionLimitationsOther" rows="5" style="width: 99.5%"
                                                        name="Recertification_485FunctionLimitationsOther"></textarea>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editSupportiveassistance_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationSupportiveAssistanceForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="5">
                                        Type of Assistance Patient Receives - other than from home health agency staff&nbsp;
                                        <i>(Select all that apply)</i>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Type of Assistance
                                    </th>
                                    <th>
                                        Family/Friends
                                    </th>
                                    <th>
                                        Provider Services
                                    </th>
                                    <th>
                                        Paid Caregiver
                                    </th>
                                    <th>
                                        Volunteer Organizations
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        ADL (bathing, dressing, toileting, bowel/bladder, eating/feeding)
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericADLTypeOfAssistance" value=" " type="hidden" />
                                        <input type="checkbox" name="Recertification_GenericADLTypeOfAssistance" value="Family/Friends" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericADLTypeOfAssistance" value="Provider Services" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericADLTypeOfAssistance" value="Paid Caregiver" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericADLTypeOfAssistance" value="Volunteer Organizations" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        IADL (meds, meals, housekeeping, laundry, telephone, shopping, finances)
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericIADLTypeOfAssistance" value=" " type="hidden" />
                                        <input type="checkbox" name="Recertification_GenericIADLTypeOfAssistance" value="Family/Friends" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericIADLTypeOfAssistance" value="Provider Services" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericIADLTypeOfAssistance" value="Paid Caregiver" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericIADLTypeOfAssistance" value="Volunteer Organizations" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Psychosocial Support
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericPsychoSupportTypeOfAssistance" value=" " type="hidden" />
                                        <input type="checkbox" name="Recertification_GenericPsychoSupportTypeOfAssistance"
                                            value="Family/Friends" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericPsychoSupportTypeOfAssistance"
                                            value="Provider Services" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericPsychoSupportTypeOfAssistance"
                                            value="Paid Caregiver" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericPsychoSupportTypeOfAssistance"
                                            value="Volunteer Organizations" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Assistance with Medical Appointments, Delivery of Medications
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericMedicalApptTypeOfAssistance" value=" " type="hidden" />
                                        <input type="checkbox" name="Recertification_GenericMedicalApptTypeOfAssistance"
                                            value="Family/Friends" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericMedicalApptTypeOfAssistance"
                                            value="Provider Services" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericMedicalApptTypeOfAssistance"
                                            value="Paid Caregiver" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericMedicalApptTypeOfAssistance"
                                            value="Volunteer Organizations" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Management of Finances
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericFinanceManagmentTypeOfAssistance" value=" " type="hidden" />
                                        <input type="checkbox" name="Recertification_GenericFinanceManagmentTypeOfAssistance"
                                            value="Family/Friends" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericFinanceManagmentTypeOfAssistance"
                                            value="Provider Services" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericFinanceManagmentTypeOfAssistance"
                                            value="Paid Caregiver" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericFinanceManagmentTypeOfAssistance"
                                            value="Volunteer Organizations" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        Comments:<br />
                                        <textarea name="Recertification_GenericTypeOfAssistanceComments" id="Recertification_GenericTypeOfAssistanceComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Supportive Assistance: Names of organizations providing assistance
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea name="Recertification_GenericOrgProvidingAssistanceNames" id="Recertification_GenericOrgProvidingAssistanceNames"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th width="45%">
                                        Community Agencies/Social Service Screening
                                    </th>
                                    <th width="5%">
                                        Yes
                                    </th>
                                    <th width="5%">
                                        No
                                    </th>
                                    <th>
                                        Ability of patient to handle finances:
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Community resource info needed to manage care
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericCommunityResourceInfoNeeded" value=" " />
                                        <input type="radio" name="Recertification_GenericCommunityResourceInfoNeeded" value="1" />
                                    </td>
                                    <td>
                                        <input type="radio" name="Recertification_GenericCommunityResourceInfoNeeded" value="0" />
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericAbilityHandleFinance" value=" " />
                                        <input type="radio" name="Recertification_GenericAbilityHandleFinance" value="Independent" />&nbsp;
                                        Independent
                                        <input type="radio" name="Recertification_GenericAbilityHandleFinance" value="Dependent" />&nbsp;
                                        Dependent
                                        <input type="radio" name="Recertification_GenericAbilityHandleFinance" value="Needs assistance" />&nbsp;
                                        Needs assistance
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Altered affect, e.g., expressed sadness or anxiety, grief
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericAlteredAffect" value=" " />
                                        <input type="radio" name="Recertification_GenericAlteredAffect" value="Yes" />
                                    </td>
                                    <td>
                                        <input type="radio" name="Recertification_GenericAlteredAffect" value="No" />
                                    </td>
                                    <td rowspan="5" valign="top">
                                        Comments:<br />
                                        <textarea name="Recertification_GenericAlteredAffectComments" id="Recertification_GenericAlteredAffectComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Suicidal ideation
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericSuicidalIdeation" value=" " />
                                        <input type="radio" name="Recertification_GenericSuicidalIdeation" value="Yes" />
                                    </td>
                                    <td>
                                        <input type="radio" name="Recertification_GenericSuicidalIdeation" value="No" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <ul>
                                            <li>Suspected Abuse/Neglect: </li>
                                            <li>&nbsp;
                                                <input type="hidden" name="Recertification_GenericSuspected" value=" " />
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="1" />&nbsp;
                                                Unexplained bruises </li>
                                            <li>&nbsp;
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="2" />&nbsp;
                                                Inadequate food </li>
                                            <li>&nbsp;
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="3" />&nbsp;
                                                Fearful of family member </li>
                                            <li>&nbsp;
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="4" />&nbsp;
                                                Exploitation of funds </li>
                                            <li>&nbsp;
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="5" />&nbsp;
                                                Sexual abuse </li>
                                            <li>&nbsp;
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="6" />&nbsp;
                                                Neglect </li>
                                            <li>&nbsp;
                                                <input type="checkbox" name="Recertification_GenericSuspected" value="7" />&nbsp;
                                                Left unattended if constant supervision is needed </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        MSW referral indicated for:
                                        <input type="text" name="Recertification_GenericMSWIndicatedForWhat" id="Recertification_GenericMSWIndicatedForWhat"
                                            size="35" maxlength="150" />
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericMSWIndicatedFor" value=" " />
                                        <input type="radio" name="Recertification_GenericMSWIndicatedFor" value="Yes" />
                                    </td>
                                    <td>
                                        <input type="radio" name="Recertification_GenericMSWIndicatedFor" value="No" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Coordinator notified
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericCoordinatorNotified" value=" " />
                                        <input type="radio" name="Recertification_GenericCoordinatorNotified" value="Yes" />
                                    </td>
                                    <td>
                                        <input type="radio" name="Recertification_GenericCoordinatorNotified" value="No" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Safety/Sanitation Hazards affecting patient: <i>(Select all that apply)</i>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericNoHazardsIdentified" value="" />
                                        <input type="checkbox" name="Recertification_GenericNoHazardsIdentified" value="1" />&nbsp;
                                        No hazards identified
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericHazardsIdentified" value="" />
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="1" />&nbsp;
                                                Stairs </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="2" />&nbsp;
                                                Narrow or obstructed walkway </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="3" />&nbsp;
                                                No gas/electric appliance </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="4" />&nbsp;
                                                No running water, plumbing </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="5" />&nbsp;
                                                Insect/rodent infestation </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="6" />&nbsp;
                                                Cluttered/soiled living area </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="7" />&nbsp;
                                                Inadequate lighting, heating and cooling </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHazardsIdentified" value="8" />&nbsp;
                                                Lack of fire safety devices </li>
                                            <li>Other: <i>(specify)</i>
                                                <input type="text" name="Recertification_GenericOtherHazards" id="Recertification_GenericOtherHazards"
                                                    size="20" maxlength="30" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comments:<br />
                                        <textarea name="Recertification_GenericHazardsComments" id="Recertification_GenericHazardsComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Fire Assessment for Patients with Oxygen
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericUsingOxygen" value="" />
                                        <input type="checkbox" name="Recertification_GenericUsingOxygen" value="1" />&nbsp;
                                        Patient not using oxygen
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient have No Smoking signs posted? &nbsp;
                                        <input type="hidden" name="Recertification_GenericNoSmokingSigns" value="" />
                                        <input type="radio" name="Recertification_GenericNoSmokingSigns" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericNoSmokingSigns" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericNoSmokingSignsPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericNoSmokingSignsPerson" value="1" />&nbsp;
                                        Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericNoSmokingSignsPerson" value="2" />&nbsp;
                                        Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient or anyone in the home smoke with oxygen in use? &nbsp;
                                        <input type="hidden" name="Recertification_GenericSmokeWithOxygenInUser" value="" />
                                        <input type="radio" name="Recertification_GenericSmokeWithOxygenInUser" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericSmokeWithOxygenInUser" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericSmokeWithOxygenInUserPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericSmokeWithOxygenInUserPerson"
                                            value="1" />&nbsp; Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericSmokeWithOxygenInUserPerson"
                                            value="2" />&nbsp; Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Are smoke detectors present and working properly? &nbsp;
                                        <input type="hidden" name="Recertification_GenericSmokeDetectorsWorking" value="" />
                                        <input type="radio" name="Recertification_GenericSmokeDetectorsWorking" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericSmokeDetectorsWorking" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericSmokeDetectorsWorkingPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericSmokeDetectorsWorkingPerson"
                                            value="1" />&nbsp; Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericSmokeDetectorsWorkingPerson"
                                            value="2" />&nbsp; Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient have a properly functioning fire extinguisher? &nbsp;
                                        <input type="hidden" name="Recertification_GenericFireExtinguisherWorking" value="" />
                                        <input type="radio" name="Recertification_GenericFireExtinguisherWorking" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericFireExtinguisherWorking" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericFireExtinguisherWorkingPerson"
                                            value="" />
                                        <input type="checkbox" name="Recertification_GenericFireExtinguisherWorkingPerson"
                                            value="1" />&nbsp; Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericFireExtinguisherWorkingPerson"
                                            value="2" />&nbsp; Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Are oxygen cylinders stored properly? &nbsp;
                                        <input type="hidden" name="Recertification_GenericOxygenCyclindersSafe" value="" />
                                        <input type="radio" name="Recertification_GenericOxygenCyclindersSafe" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericOxygenCyclindersSafe" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericOxygenCyclindersSafePerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericOxygenCyclindersSafePerson" value="1" />&nbsp;
                                        Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericOxygenCyclindersSafePerson" value="2" />&nbsp;
                                        Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Are all electrical cords near oxygen intact and free from fraying? &nbsp;
                                        <input type="hidden" name="Recertification_GenericElectricalCordsIntact" value="" />
                                        <input type="radio" name="Recertification_GenericElectricalCordsIntact" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericElectricalCordsIntact" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericElectricalCordsIntactPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericElectricalCordsIntactPerson"
                                            value="1" />&nbsp; Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericElectricalCordsIntactPerson"
                                            value="2" />&nbsp; Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient have an evacuation plan in case of fire? &nbsp;
                                        <input type="hidden" name="Recertification_GenericEvacuationPlan" value="" />
                                        <input type="radio" name="Recertification_GenericEvacuationPlan" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericEvacuationPlan" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericEvacuationPlanPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericEvacuationPlanPerson" value="1" />&nbsp;
                                        Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericEvacuationPlanPerson" value="2" />&nbsp;
                                        Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Are all cleaning fluids and aerosols stored away from oxygen, and not used while
                                        oxygen is in use? &nbsp;
                                        <input type="hidden" name="Recertification_GenericAerosolsSafeWhenOxygenInUse" value="" />
                                        <input type="radio" name="Recertification_GenericAerosolsSafeWhenOxygenInUse" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericAerosolsSafeWhenOxygenInUse" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericAerosolsSafeWhenOxygenInUsePerson"
                                            value="" />
                                        <input type="checkbox" name="Recertification_GenericAerosolsSafeWhenOxygenInUsePerson"
                                            value="1" />&nbsp; Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericAerosolsSafeWhenOxygenInUsePerson"
                                            value="2" />&nbsp; Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient refrain from using petroleum products around oxygen?&nbsp;
                                        <input type="hidden" name="Recertification_GenericPetroleumNearOxygen" value="" />
                                        <input type="radio" name="Recertification_GenericPetroleumNearOxygen" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericPetroleumNearOxygen" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericPetroleumNearOxygenPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericPetroleumNearOxygenPerson" value="1" />&nbsp;
                                        Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericPetroleumNearOxygenPerson" value="2" />&nbsp;
                                        Caregiver educated&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient only use water-based body and lip moisturizers?&nbsp;
                                        <input type="hidden" name="Recertification_GenericWaterbasedBodyLotion" value="" />
                                        <input type="radio" name="Recertification_GenericWaterbasedBodyLotion" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericWaterbasedBodyLotion" value="0" />&nbsp;
                                        No<br />
                                        <input type="hidden" name="Recertification_GenericWaterbasedBodyLotionPerson" value="" />
                                        <input type="checkbox" name="Recertification_GenericWaterbasedBodyLotionPerson" value="1" />&nbsp;
                                        Patient &nbsp;
                                        <input type="checkbox" name="Recertification_GenericWaterbasedBodyLotionPerson" value="2" />&nbsp;
                                        Caregiver educated&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="3">
                                        Safety Measures (locator #15)
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_485SafetyMeasures" value="" />
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="1" />&nbsp;
                                        Anticoagulant Precautions
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="2" />&nbsp;
                                        Emergency Plan Developed
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="3" />&nbsp;
                                        Fall Precautions
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="4" />&nbsp;
                                        Keep Pathway Clear
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="5" />&nbsp;
                                        Keep Side Rails Up
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="6" />&nbsp;
                                        Neutropenic Precautions
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="7" />&nbsp;
                                        O2 Precautions
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="8" />&nbsp;
                                        Proper Position During Meals
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="9" />&nbsp;
                                        Safety in ADLs
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="10" />&nbsp;
                                        Seizure Precautions
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="11" />&nbsp;
                                        Sharps Safety
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="12" />&nbsp;
                                        Slow Position Change
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="13" />&nbsp;
                                        Standard Precautions/Infection Control
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="14" />&nbsp;
                                        Support During Transfer and Ambulation
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="15" />&nbsp;
                                        Use of Assistive Devices
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        Other: <i>(specify)</i><br />
                                        <textarea name="Recertification_485OtherSafetyMeasures" id="Recertification_485OtherSafetyMeasures"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="16" />&nbsp;
                                        Instructed on safe utilities management
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="17" />&nbsp;
                                        Instructed on mobility safety
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="18" />&nbsp;
                                        Instructed on DME & electrical safety
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="19" />&nbsp;
                                        Instructed on sharps container
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="20" />&nbsp;
                                        Instructed on medical gas
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="21" />&nbsp;
                                        Instructed on disaster/emergency plan
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="22" />&nbsp;
                                        Instructed on safety measures
                                    </td>
                                    <td>
                                        <input type="checkbox" name="Recertification_485SafetyMeasures" value="23" />&nbsp;
                                        Instructed on proper handling of biohazard waste
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Triage/Risk Code:</span>
                                        <input type="text" name="Recertification_485TriageRiskCode" id="Recertification_485TriageRiskCode"
                                            size="5" maxlength="5" value="" />
                                    </td>
                                    <td>
                                        <span>Disaster Code:</span>
                                        <input type="text" name="Recertification_485DisasterCode" id="Recertification_485DisasterCode"
                                            size="5" maxlength="5" value="" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        Comments:<br />
                                        <textarea name="Recertification_485SafetyMeasuresComments" id="Recertification_485SafetyMeasuresComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="3">
                                        Cultural
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Primary language?&nbsp;
                                        <input type="text" name="Recertification_GenericPrimaryLanguage" id="Recertification_GenericPrimaryLanguage"
                                            size="40" maxlength="40" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Does patient have cultural practices that influence health care?&nbsp;
                                        <input type="hidden" name="Recertification_GenericCulturalPractices" value="" />
                                        <input type="radio" name="Recertification_GenericCulturalPractices" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericCulturalPractices" value="0" />&nbsp;
                                        No
                                        <br />
                                        If yes, please explain:<br />
                                        <textarea name="Recertification_GenericCulturalPracticesDetails" id="Recertification_GenericCulturalPracticesDetails"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Is religion important to the patient?&nbsp;
                                        <input type="hidden" name="Recertification_GenericReligionImportant" value="" />
                                        <input type="radio" name="Recertification_GenericReligionImportant" value="1" />&nbsp;
                                        Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericReligionImportant" value="0" />&nbsp;
                                        No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Patient's religious preference? &nbsp;
                                        <input type="text" name="Recertification_GenericReligiousPreference" id="Recertification_GenericReligiousPreference"
                                            size="40" maxlength="40" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Use of interpreter <i>(select patient preferences)</i>: </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericUseOfInterpreter" value="" />
                                                <input type="checkbox" name="Recertification_GenericUseOfInterpreter" value="1" />&nbsp;
                                                Family </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericUseOfInterpreter" value="2" />&nbsp;
                                                Friend </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericUseOfInterpreter" value="3" />&nbsp;
                                                Professional </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericUseOfInterpreter" value="4" />&nbsp;
                                                Other &nbsp;
                                                <input type="text" name="Recertification_GenericUseOfInterpreterOtherDetails" id="Recertification_GenericUseOfInterpreterOtherDetails"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Patient's primary source of emotional support:&nbsp;
                                        <input type="text" name="Recertification_GenericPrimaryEmotionalSupport" id="Recertification_GenericPrimaryEmotionalSupport"
                                            size="30" maxlength="30" value="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Homebound?
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Is Homebound? &nbsp;
                                        <input type="hidden" name="Recertification_GenericIsHomeBound" value="" />
                                        <input type="radio" name="Recertification_GenericIsHomeBound" value="Yes" />&nbsp;Yes&nbsp;
                                        <input type="radio" name="Recertification_GenericIsHomeBound" value="No" />&nbsp;No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericHomeBoundReason" value="" />
                                                <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="1" />&nbsp;
                                                Residual weakness </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="2" />&nbsp;
                                                Unable to safely leave home unassisted </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="3" />&nbsp;
                                                Requires max assistance/taxing effort to leave home </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="4" />&nbsp;
                                                Confusion, unsafe to go out of home alone </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="5" />&nbsp;
                                                Severe SOB, SOB upon exertion </li>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="6" />&nbsp;
                                                Other
                                                <input type="text" name="Recertification_GenericOtherHomeBoundDetails" id="Recertification_GenericOtherHomeBoundDetails"
                                                    size="60" maxlength="60" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Recertification_GenericHomeBoundReason" value="7" />&nbsp;
                                        Need assistance for all activities
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editSensorystatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationSensoryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th colspan="2">
                                        Sensory Status
                                    </th>
                                </tr>
                                <tr>
                                    <td rowspan="2" valign="top">
                                        <strong>Eyes:</strong>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericEyes" value="" />&nbsp;
                                                <input type="checkbox" name="Recertification_GenericEyes" value="1" />&nbsp; WNL
                                                (Within Normal Limits) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="2" />&nbsp; Glasses
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="3" />&nbsp; Contacts
                                                Left </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="4" />&nbsp; Contacts
                                                Right </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="5" />&nbsp; Blurred
                                                Vision </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="6" />&nbsp; Glaucoma
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="7" />&nbsp; Cataracts
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="8" />&nbsp; Macular
                                                Degeneration </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="9" />&nbsp; Redness
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="10" />&nbsp; Drainage
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="11" />&nbsp; Itching
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="12" />&nbsp; Watering
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEyes" value="13" />&nbsp; Other&nbsp;
                                                <input type="text" name="Recertification_GenericEyesOtherDetails" id="Recertification_GenericEyesOtherDetails"
                                                    size="10" maxlength="10" value="" />
                                            </li>
                                            <li>&nbsp;</li>
                                        </ul>
                                        <ul>
                                            <li>Date of Last Eye Exam:
                                                <input type="text" name="Recertification_GenericEyesLastEyeExamDate" id="Recertification_GenericEyesLastEyeExamDate"
                                                    size="10" maxlength="10" value="">
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <strong>Ears:</strong>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericEars" value="" />&nbsp;
                                                <input type="checkbox" name="Recertification_GenericEars" value="1" />&nbsp; WNL
                                                (Within Normal Limits) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEars" value="2" />&nbsp; Hearing
                                                Impaired&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="hidden" name="Recertification_GenericEarsHearingImpairedPosition" value="" />&nbsp;
                                                <input type="checkbox" name="Recertification_GenericEarsHearingImpairedPosition"
                                                    value="1" />&nbsp; Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="Recertification_GenericEarsHearingImpairedPosition"
                                                    value="2" />&nbsp; Right </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEars" value="3" />&nbsp; Deaf
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEars" value="4" />&nbsp; Drainage
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEars" value="5" />&nbsp; Pain
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericEars" value="6" />&nbsp; Hearing
                                                Aids&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="hidden" name="Recertification_GenericEarsHearingAidsPosition" value="" />&nbsp;
                                                <input type="checkbox" name="Recertification_GenericEarsHearingAidsPosition" value="1" />&nbsp;
                                                Left &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="Recertification_GenericEarsHearingAidsPosition" value="2" />&nbsp;
                                                Right </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Nose:</strong>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericNose" value=" " />&nbsp;
                                                <input type="checkbox" name="Recertification_GenericNose" value="1" />&nbsp; WNL
                                                (Within Normal Limits) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericNose" value="2" />&nbsp; Congestion
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericNose" value="3" />&nbsp; Loss
                                                of Smell </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericNose" value="4" />&nbsp; Nose
                                                Bleeds &nbsp;<i>How often?</i>
                                                <input type="text" name="Recertification_GenericNoseBleedsFrequency" id="Recertification_GenericNoseBleedsFrequency"
                                                    size="20" maxlength="50" value="" />
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input type="checkbox" name="Recertification_GenericNose" value="5" />&nbsp; Other
                                                &nbsp;
                                                <input type="text" name="Recertification_GenericNoseOtherDetails" id="Recertification_GenericNoseOtherDetails"
                                                    size="20" maxlength="50" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1200) Vision (with corrective lenses if the patient usually wears them):
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1200Vision" type="hidden" value=" " />
                                    <input name="Recertification_M1200Vision" type="radio" value="00" />&nbsp;0 - Normal
                                    vision: sees adequately in most situations; can see medication labels, newsprint.<br />
                                    <input name="Recertification_M1200Vision" type="radio" value="01" />&nbsp;1 - Partially
                                    impaired: cannot see medication labels or newsprint, but can see obstacles in path,
                                    and the surrounding layout; can count fingers at arm's length.<br />
                                    <input name="Recertification_M1200Vision" type="radio" value="02" />&nbsp;2 - Severely
                                    impaired: cannot locate objects without hearing or touching them or patient nonresponsive.
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485SensoryStatusIntervention" value=" " />
                                        <input name="Recertification_485SensoryStatusIntervention" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to administer ear medication as follows:
                                        <br />
                                        <input type="text" name="Recertification_485AdministerEarMedicationOrder" id="Recertification_485AdministerEarMedicationOrder"
                                            size="100" maxlength="100" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485SensoryStatusIntervention" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instill ophthalmic medication as follows:<br />
                                        <input type="text" name="Recertification_485InstillOphathalmicMedicationOrders" id="Recertification_485InstillOphathalmicMedicationOrders"
                                            size="100" maxlength="100" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485SensoryStatusIntervention" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        ST
                                        <input type="text" name="Recertification_485STEvaluateSensoryStatusFrequency" id="Recertification_485STEvaluateSensoryStatusFrequency"
                                            size="10" maxlength="10" value="" />
                                        (freq) to evaluate week of:
                                        <input type="text" name="Recertification_485STEvaluateSensoryStatusEvalDate" id="Recertification_485STEvaluateSensoryStatusEvalDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485SensoryStatusIntervention" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to provide patient with written instructions in large font
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485SensoryStatusOrderTemplates" name="Recertification_485SensoryStatusOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485SensoryStatusInterventionComments" id="Recertification_485SensoryStatusInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485SensoryStatusGoalTemplates" name="Recertification_485SensoryStatusGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485SensoryStatusGoalComments" id="Recertification_485SensoryStatusGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editPain_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationPainForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Pain Scale
                                    </th>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        Onset Date:
                                        <input type="text" name="Recertification_GenericPainOnSetDate" id="Recertification_GenericPainOnSetDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                    <td>
                                        Location of Pain:
                                        <input type="text" name="Recertification_GenericLocationOfPain" id="Recertification_GenericLocationOfPain"
                                            size="80" maxlength="80" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style='background-color: #FFFFFF;'>
                                        <div align="center">
                                            <img src="/Images/painscale.png" border="30" />
                                        </div>
                                        <div style="font-size: 10px; font-style: italic" align="center">
                                            From Hockenberry MJ, Wilson D: <a href="http://www.us.elsevierhealth.com/product.jsp?isbn=9780323053532"
                                                target="_blank">Wong's essentials of pediatric nursing</a>, ed. 8, St. Louis,
                                            2009, Mosby. Used with permission. Copyright Mosby.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Intensity of pain:
                                    </td>
                                    <td>
                                        <select name="Recertification_GenericIntensityOfPain" id="Recertification_GenericIntensityOfPain">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Duration:
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericDurationOfPain" id="Recertification_GenericDurationOfPain"
                                            size="80" maxlength="80" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Quality:
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericQualityOfPain" id="Recertification_GenericQualityOfPain"
                                            size="80" maxlength="80" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        What makes pain worse:
                                    </td>
                                    <td>
                                        <textarea name="Recertification_GenericWhatMakesPainWorse" id="Recertification_GenericWhatMakesPainWorse"
                                            rows="2" style="width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        What makes pain better:
                                    </td>
                                    <td>
                                        <textarea name="Recertification_GenericWhatMakesPainBetter" id="Recertification_GenericWhatMakesPainBetter"
                                            rows="2" style="width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Relief rating of pain, i.e., pain level after medications:
                                    </td>
                                    <td>
                                        <select name="Recertification_GenericReliefRatingOfPain" id="Recertification_GenericReliefRatingOfPain">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Medications patient takes for pain:
                                    </td>
                                    <td>
                                        <textarea name="Recertification_GenericPainMedication" id="Recertification_GenericPainMedication"
                                            rows="2" style="width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;Medication effectiveness:
                                    </td>
                                    <td>
                                        <textarea name="Recertification_GenericMedicationEffectiveness" id="Recertification_GenericMedicationEffectiveness"
                                            rows="2" style="width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;Medication adverse side effects:
                                    </td>
                                    <td>
                                        <textarea name="Recertification_GenericMedicationAdverseEffects" id="Recertification_GenericMedicationAdverseEffects"
                                            rows="2" style="width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Patient's pain goal:
                                    </td>
                                    <td>
                                        <textarea name="Recertification_GenericPatientPainGoal" id="Recertification_GenericPatientPainGoal"
                                            rows="2" style="width: 99%;"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1242) Frequency of Pain Interfering with patient's activity or movement:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1242PainInterferingFrequency" type="hidden" value=" " />
                                    <input name="Recertification_M1242PainInterferingFrequency" type="radio" value="00" />&nbsp;0
                                    - Patient has no pain<br />
                                    <input name="Recertification_M1242PainInterferingFrequency" type="radio" value="01" />&nbsp;1
                                    - Patient has pain that does not interfere with activity or movement<br />
                                    <input name="Recertification_M1242PainInterferingFrequency" type="radio" value="02" />&nbsp;2
                                    - Less often than daily<br />
                                    <input name="Recertification_M1242PainInterferingFrequency" type="radio" value="03" />&nbsp;3
                                    - Daily, but not constantly<br />
                                    <input name="Recertification_M1242PainInterferingFrequency" type="radio" value="04" />&nbsp;4
                                    - All of the time
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485PainInterventions" value=" " />
                                        <input name="Recertification_485PainInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess pain level and effectiveness of pain medications and current pain management
                                        therapy every visit
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485PainInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to take pain medication before pain becomes severe to achieve
                                        better pain control
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485PainInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on nonpharmacologic pain relief measures, including relaxation
                                        techniques, massage, stretching, positioning, and hot/cold packs
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485PainInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess patient's willingness to take pain medications and/or barriers to compliance,
                                        e.g., patient is unable to tolerate side effects such as drowsiness, dizziness,
                                        constipation
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485PainInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to report to physician if patient experiences pain level not acceptable to patient,
                                        pain level greater than
                                        <input type="text" name="Recertification_485PainTooGreatLevel" id="Recertification_485PainTooGreatLevel"
                                            size="10" maxlength="10" value="" />
                                        , pain medications not effective, patient unable to tolerate pain medications, pain
                                        affecting ability to perform patient's normal activities
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485PainOrderTemplates" name="Recertification_485PainOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485PainInterventionComments" id="Recertification_485PainInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485PainGoals" value=" " />
                                        <input name="Recertification_485PainGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will verbalize understanding of proper use of pain medication by
                                        <input type="text" name="Recertification_485PatientVerbalizeMedUseDate" id="Recertification_485PatientVerbalizeMedUseDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485PainGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will achieve pain level less than
                                        <input type="text" name="Recertification_485AchievePainLevelLessThan" id="Recertification_485AchievePainLevelLessThan"
                                            size="10" maxlength="10" value="" />
                                        within
                                        <input type="text" name="Recertification_485AchievePainLevelWeeks" id="Recertification_485AchievePainLevelWeeks"
                                            size="3" maxlength="3" value="" />
                                        weeks
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485PainGoalTemplates" name="Recertification_485PainGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485PainGoalComments" id="Recertification_485PainGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editIntegumentarystatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationIntegumentaryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table id="recetBradenScale" cellspacing="0" cellpadding="0" style="line-height: 12px">
                                <tr>
                                    <th colspan="7">
                                        Braden Scale<br />
                                        <i>for Predicting Pressure Sore Risk in Home Care</i>
                                    </th>
                                </tr>
                                <tr>
                                    <td width="19%">
                                        <strong>SENSORY PERCEPTION</strong>
                                        <br />
                                        ability to respond meaningfully to pressure-related discomfort
                                    </td>
                                    <td width="19%">
                                        <strong>1. Completely Limited</strong> Unresponsive (does not moan, flinch, or grasp)
                                        to painful stimuli, due to diminished level of consciousness or sedation <strong>OR</strong>
                                        limited ability to feel pain over most of body.
                                    </td>
                                    <td width="19%">
                                        <strong>2. Very Limited</strong> Responds only to painful stimuli. Cannot communicate
                                        discomfort except by moaning or restlessness <strong>OR</strong> has a sensory impairment
                                        which limits the ability to feel pain or discomfort over 1/2 of body.
                                    </td>
                                    <td width="19%">
                                        <strong>3. Slightly Limited</strong> Responds to verbal commands, but cannot always
                                        communicate discomfort or the need to be turned <strong>OR</strong> has some sensory
                                        impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.
                                    </td>
                                    <td width="19%">
                                        <strong>4. No Impairment</strong> Responds to verbal commands. Has no sensory deficit
                                        which would limit ability to feel or voice pain or discomfort.
                                    </td>
                                    <td width="15px">
                                        <select name="Recertification_485BradenScaleSensory" id="Recertification_485BradenScaleSensory"
                                            class="scale">
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>MOISTURE</strong>
                                        <br />
                                        degree to which skin is exposed to moisture
                                    </td>
                                    <td>
                                        <strong>1. Constantly Moist</strong> Skin is kept moist almost constantly by perspiration,
                                        urine, etc. Dampness is detected every time patient is moved or turned.
                                    </td>
                                    <td>
                                        <strong>2. Often Moist</strong> Skin is often, but not always moist. Linen must
                                        be changed as often as 3 times in 24 hours.
                                    </td>
                                    <td>
                                        <strong>3. Occasionally Moist</strong> Skin is occasionally moist, requiring an
                                        extra linen change approximately once a day.
                                    </td>
                                    <td>
                                        <strong>4. Rarely Moist</strong> Skin is usually dry; Linen only requires changing
                                        at routine intervals.
                                    </td>
                                    <td>
                                        <select name="Recertification_485BradenScaleMoisture" id="Recertification_485BradenScaleMoisture"
                                            class="scale">
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>ACTIVITY</strong>
                                        <br />
                                        degree of physical activity
                                    </td>
                                    <td>
                                        <strong>1. Bedfast</strong> Confined to bed.
                                    </td>
                                    <td>
                                        <strong>2. Chairfast</strong> Ability to walk severely limited or non-existent.
                                        Cannot bear own weight and/or must be assisted into chair or wheelchair.
                                    </td>
                                    <td>
                                        <strong>3. Walks Occasionally</strong> Walks occasionally during day, but for very
                                        short distances, with or without assistance. Spends majority of day in bed or chair.
                                    </td>
                                    <td>
                                        <strong>4. Walks Frequently</strong> Walks outside bedroom twice a day and inside
                                        room at least once every two hours during waking hours.
                                    </td>
                                    <td>
                                        <select name="Recertification_485BradenScaleActivity" id="Recertification_485BradenScaleActivity"
                                            class="scale">
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>MOBILITY</strong>
                                        <br />
                                        ability to change and control body position
                                    </td>
                                    <td>
                                        <strong>1. Completely Immobile</strong> Does not make even slight changes in body
                                        or extremity position without assistance.
                                    </td>
                                    <td>
                                        <strong>2. Very Limited</strong> Makes occasional slight changes in body or extremity
                                        position but unable to make frequent or significant changes independently.
                                    </td>
                                    <td>
                                        <strong>3. Slightly Limited</strong> Makes frequent though slight changes in body
                                        or extremity position independently.
                                    </td>
                                    <td>
                                        <strong>4. No Limitation</strong> Makes major and frequent changes in position without
                                        assistance.
                                    </td>
                                    <td>
                                        <select name="Recertification_485BradenScaleMobility" id="Recertification_485BradenScaleMobility"
                                            class="scale">
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>NUTRITION</strong>
                                        <br />
                                        usual food intake pattern
                                    </td>
                                    <td>
                                        <strong>1. Very Poor</strong> Never eats a complete meal. Rarely eats more than
                                        1/3 of any food offered. Eats 2 servings or less of protein (meat or dairy products)
                                        per day. Takes fluids poorly. Does not take a liquid dietary supplement <strong>OR</strong>
                                        is NPO and/or maintained on clear liquids or IVs for more than 5 days.
                                    </td>
                                    <td>
                                        <strong>2. Probably Inadequate</strong> Rarely eats a complete meal and generally
                                        eats only about 1/2 of any food offered. Protein intake includes only 3 servings
                                        of meat or dairy products per day. Occasionally will take a dietary supplement <strong>
                                            OR</strong> receives less than optimum amount of liquid diet or tube feeding.
                                    </td>
                                    <td>
                                        <strong>3. Adequate</strong> Eats over half of most meals. Eats a total of 4 servings
                                        of protein (meat, dairy products) per day. Occasionally will refuse a meal, but
                                        will usually take a supplement when offered <strong>OR</strong> is on a tube feeding
                                        or TPN regimen which probably meets most of nutritional needs.
                                    </td>
                                    <td>
                                        <strong>4. Excellent</strong> Eats most of every meal. Never refuses a meal. Usually
                                        eats a total of 4 or more servings of meat and dairy products. Occasionally eats
                                        between meals. Does not require supplementation.
                                    </td>
                                    <td>
                                        <select name="Recertification_485BradenScaleNutrition" id="Recertification_485BradenScaleNutrition"
                                            class="scale">
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>FRICTION & SHEAR</strong>
                                    </td>
                                    <td>
                                        <strong>1. Problem</strong> Requires moderate to maximum assistance in moving. Complete
                                        lifting without sliding against sheets is impossible. Frequently slides down in
                                        bed or chair, requiring frequent repositioning with maximum assistance. Spasticity,
                                        contractures or agitation leads to almost constant friction.
                                    </td>
                                    <td>
                                        <strong>2. Potential Problem</strong> Moves feebly or requires minimum assistance.
                                        During a move skin probably slides to some extent against sheets, chair, restraints
                                        or other devices. Maintains relatively good position in chair or bed most of the
                                        time but occasionally slides down.
                                    </td>
                                    <td>
                                        <strong>3. No Apparent Problem</strong> Moves in bed and in chair independently
                                        and has sufficient muscle strength to lift up completely during move. Maintains
                                        good position in bed or chair.
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <select name="Recertification_485BradenScaleFriction" id="Recertification_485BradenScaleFriction"
                                            class="scale">
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <strong>Total:</strong>
                                    </td>
                                    <td>
                                        <div>
                                            <input type="text" name="Recertification_485BradenScaleTotal" id="Recertification_485BradenScaleTotal"
                                                size="1" maxlength="" value="" readonly="readonly" />
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="100%">
                                        <div style="float: right" id="Recertification_485ResultBox">
                                            &nbsp;</div>
                                        &nbsp;<br />
                                        <br />
                                        <div class="textCenter">
                                            <strong>Braden Scale Scoring:</strong> Risk of developing pressure ulcers: <i><strong>
                                                15-18:</strong> At risk; <strong>13-14:</strong> Moderate risk; <strong>10-12:</strong>
                                                High risk; <strong>9 or below:</strong> Very high risk</i>
                                            <br />
                                            Copyright. Barbara Braden and Nancy Bergstrom, 1988. Reprinted with permission.
                                            All Rights Reserved.
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Integumentary Status
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Skin Turgor:</strong>
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericSkinTurgor" value="" />
                                        <input name="Recertification_GenericSkinTurgor" value="Good" type="radio" />&nbsp;
                                        Good
                                        <input name="Recertification_GenericSkinTurgor" value="Fair" type="radio" />&nbsp;
                                        Fair
                                        <input name="Recertification_GenericSkinTurgor" value="Poor" type="radio" />&nbsp;
                                        Poor
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Skin Color:</strong>
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericSkinColor" value="" />
                                        <input name="Recertification_GenericSkinColor" value="1" type="checkbox" />&nbsp;
                                        Pink/WNL
                                        <input name="Recertification_GenericSkinColor" value="2" type="checkbox" />&nbsp;
                                        Pale
                                        <input name="Recertification_GenericSkinColor" value="3" type="checkbox" />&nbsp;
                                        Jaundice
                                        <input name="Recertification_GenericSkinColor" value="4" type="checkbox" />&nbsp;
                                        Cyanotic
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Skin:</strong>
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericSkin" value=" " />
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="1" type="checkbox" />&nbsp; Dry
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="2" type="checkbox" />&nbsp; Diaphoretic
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="3" type="checkbox" />&nbsp; Warm
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="4" type="checkbox" />&nbsp; Cool
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="5" type="checkbox" />&nbsp; Wound
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="6" type="checkbox" />&nbsp; Ulcer
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="7" type="checkbox" />&nbsp; Incision
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="8" type="checkbox" />&nbsp; Rash
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="9" type="checkbox" />&nbsp; Ostomy
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericSkin" value="10" type="checkbox" />&nbsp; Other
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Instructed on measures to control infections?
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericInstructedControlInfections" value="" />
                                        <input name="Recertification_GenericInstructedControlInfections" value="1" type="radio" />&nbsp;
                                        Yes&nbsp;
                                        <input name="Recertification_GenericInstructedControlInfections" value="0" type="radio" />&nbsp;
                                        No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Nails:</strong>
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericNails" value="" />
                                        <input name="Recertification_GenericNails" value="Good" type="radio" />&nbsp; Good&nbsp;
                                        <input name="Recertification_GenericNails" value="Bad" type="radio" />&nbsp; Bad
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Is patient using pressure-relieving device(s)?
                                    </td>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericPressureRelievingDevice" value="" />
                                        <input name="Recertification_GenericPressureRelievingDevice" value="1" type="radio" />&nbsp;
                                        Yes&nbsp;
                                        <input name="Recertification_GenericPressureRelievingDevice" value="0" type="radio" />&nbsp;
                                        No
                                        <br />
                                        Type:
                                        <input type="text" name="Recertification_GenericPressureRelievingDeviceType" id="Recertification_GenericPressureRelievingDeviceType"
                                            size="40" maxlength="40" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Comments:<br />
                                        <textarea name="Recertification_GenericIntegumentaryStatusComments" id="Recertification_GenericIntegumentaryStatusComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow  title">
                                    <div class="margin">
                                        (M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II
                                        or Higher or designated as "unstageable"?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1306UnhealedPressureUlcers" type="hidden" value=" " />
                                    <input name="Recertification_M1306UnhealedPressureUlcers" type="radio" value="0" />&nbsp;0
                                    - No [ Go to M1322 ]<br />
                                    <input name="Recertification_M1306UnhealedPressureUlcers" type="radio" value="1" />&nbsp;1
                                    - Yes
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="recertification_M1308">
                            <div class="insideColFull">
                                <div class="insideColFull title">
                                    <div class="margin">
                                        (M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each
                                        Stage: (Enter "0" if none; excludes Stage I pressure ulcers)
                                    </div>
                                </div>
                                <div class="insideColFull">
                                    <div class="margin">
                                        <table class="agency-data-table" id="Table2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                    </th>
                                                    <th>
                                                        Column 1<br />
                                                        Complete at SOC/ROC/FU & D/C
                                                    </th>
                                                    <th>
                                                        Column 2<br />
                                                        Complete at FU & D/C
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        Stage description – unhealed pressure ulcers
                                                    </th>
                                                    <th>
                                                        Number Currently Present
                                                    </th>
                                                    <th>
                                                        Number of those listed in Column 1 that were present on admission (most recent SOC
                                                        / ROC)
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer
                                                        with red pink wound bed, without slough. May also present as an intact or open/ruptured
                                                        serum-filled blister.
                                                    </td>
                                                    <td>
                                                        <input name="Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent" id="Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input name="Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission"
                                                            id="Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission" type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone,
                                                        tendon, or muscles are not exposed. Slough may be present but does not obscure the
                                                        depth of tissue loss. May include undermining and tunneling.
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent" name="Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission"
                                                            name="Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission" type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough
                                                        or eschar may be present on some parts of the wound bed. Often includes undermining
                                                        and tunneling.
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent" name="Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission" name="Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or
                                                        device
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent"
                                                            name="Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent" type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission"
                                                            name="Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by
                                                        slough and/or eschar.
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"
                                                            name="Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"
                                                            name="Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.3 Unstageable: Suspected deep tissue injury in evolution.
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"
                                                            name="Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"
                                                            name="Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable
                                            redness of a localized area usually over a bony prominence. The area may be painful,
                                            firm, soft, warmer or cooler as compared to adjacent tissue.
                                        </div>
                                    </div>
                                    <div class="insideCol">
                                        <div class="margin">
                                            <input name="Recertification_M1322CurrentNumberStageIUlcer" type="hidden" value=" " />
                                            <input name="Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="00" />&nbsp;0
                                            <br />
                                            <input name="Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="01" />&nbsp;1
                                            <br />
                                            <input name="Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="02" />&nbsp;2<br />
                                        </div>
                                    </div>
                                    <div class="insideCol">
                                        <div class="margin">
                                            <input name="Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="03" />&nbsp;3<br />
                                            <input name="Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="04" />&nbsp;4
                                            or more
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer:
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1324MostProblematicUnhealedStage" type="hidden" value=" " />
                                        <input name="Recertification_M1324MostProblematicUnhealedStage" type="radio" value="01" />&nbsp;1
                                        - Stage I<br />
                                        <input name="Recertification_M1324MostProblematicUnhealedStage" type="radio" value="02" />&nbsp;2
                                        - Stage II<br />
                                        <input name="Recertification_M1324MostProblematicUnhealedStage" type="radio" value="03" />&nbsp;3
                                        - Stage III<br />
                                        <input name="Recertification_M1324MostProblematicUnhealedStage" type="radio" value="04" />&nbsp;4
                                        - Stage IV<br />
                                        <input name="Recertification_M1324MostProblematicUnhealedStage" type="radio" value="NA" />&nbsp;NA
                                        - No observable pressure ulcer or unhealed pressure ulcer
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1330) Does this patient have a Stasis Ulcer?
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1330StasisUlcer" type="hidden" value=" " />
                                        <input name="Recertification_M1330StasisUlcer" type="radio" value="00" />&nbsp;0
                                        - No [ Go to M1340 ]
                                        <br />
                                        <input name="Recertification_M1330StasisUlcer" type="radio" value="01" />&nbsp;1
                                        - Yes, patient has BOTH observable and unobservable stasis ulcers
                                        <br />
                                        <input name="Recertification_M1330StasisUlcer" type="radio" value="02" />&nbsp;2
                                        - Yes, patient has observable stasis ulcers ONLY<br />
                                        <input name="Recertification_M1330StasisUlcer" type="radio" value="03" />&nbsp;3
                                        - Yes, patient has unobservable stasis ulcers ONLY (known but not observable due
                                        to non-removable dressing) [ Go to M1340 ]
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="recertification_M1332AndM1334">
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1332) Current Number of (Observable) Stasis Ulcer(s):
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1332CurrentNumberStasisUlcer" type="hidden" value=" " />
                                        <input name="Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="01" />&nbsp;1
                                        - One<br />
                                        <input name="Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="02" />&nbsp;2
                                        - Two<br />
                                        <input name="Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="03" />&nbsp;3
                                        - Three<br />
                                        <input name="Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="04" />&nbsp;4
                                        - Four or more<br />
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1334) Status of Most Problematic (Observable) Stasis Ulcer:
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1334StasisUlcerStatus" type="hidden" value=" " />
                                        <input name="Recertification_M1334StasisUlcerStatus" type="radio" value="00" />&nbsp;0
                                        - Newly epithelialized
                                        <br />
                                        <input name="Recertification_M1334StasisUlcerStatus" type="radio" value="01" />&nbsp;1
                                        - Fully granulating
                                        <br />
                                        <input name="Recertification_M1334StasisUlcerStatus" type="radio" value="02" />&nbsp;2
                                        - Early/partial granulation<br />
                                        <input name="Recertification_M1334StasisUlcerStatus" type="radio" value="03" />&nbsp;3
                                        - Not healing
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1340) Does this patient have a Surgical Wound?
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1340SurgicalWound" type="hidden" value=" " />
                                        <input name="Recertification_M1340SurgicalWound" type="radio" value="00" />&nbsp;0
                                        - No [ Go to M1350 ]<br />
                                        <input name="Recertification_M1340SurgicalWound" type="radio" value="01" />&nbsp;1
                                        - Yes, patient has at least one (observable) surgical wound<br />
                                        <input name="Recertification_M1340SurgicalWound" type="radio" value="02" />&nbsp;2
                                        - Surgical wound known but not observable due to non-removable dressing [ Go to
                                        M1350 ]
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol" id="recertification_M1342">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1342) Status of Most Problematic (Observable) Surgical Wound:
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1342SurgicalWoundStatus" type="hidden" value=" " />
                                        <input name="Recertification_M1342SurgicalWoundStatus" type="radio" value="00" />&nbsp;0
                                        - Newly epithelialized
                                        <br />
                                        <input name="Recertification_M1342SurgicalWoundStatus" type="radio" value="01" />&nbsp;1
                                        - Fully granulating
                                        <br />
                                        <input name="Recertification_M1342SurgicalWoundStatus" type="radio" value="02" />&nbsp;2
                                        - Early/partial granulation<br />
                                        <input name="Recertification_M1342SurgicalWoundStatus" type="radio" value="03" />&nbsp;3
                                        - Not healing
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy,
                                            other than those described above that is receiving intervention by the home health
                                            agency?
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="Recertification_M1350SkinLesionOpenWound" type="hidden" value=" " />
                                        <input name="Recertification_M1350SkinLesionOpenWound" type="radio" value="0" />&nbsp;0
                                        - No &nbsp;&nbsp;
                                        <input name="Recertification_M1350SkinLesionOpenWound" type="radio" value="1" />&nbsp;1
                                        - Yes
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="6">
                                        Wound Graph
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div align="center">
                                            <img src="/Images/body_outline.jpg" alt="body outline" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="10%">
                                        &nbsp;
                                    </th>
                                    <th>
                                        Wound One
                                    </th>
                                    <th>
                                        Wound Two
                                    </th>
                                    <th>
                                        Wound Three
                                    </th>
                                    <th>
                                        Wound Four
                                    </th>
                                    <th>
                                        Wound Five
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Location:
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericWoundLocation1" id="Recertification_GenericWoundLocation1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericWoundLocation2" id="Recertification_GenericWoundLocation2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericWoundLocation3" id="Recertification_GenericWoundLocation3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericWoundLocation4" id="Recertification_GenericWoundLocation4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" name="Recertification_GenericWoundLocation5" id="Recertification_GenericWoundLocation5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Onset Date:
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOnsetDate1" name="Recertification_GenericWoundOnsetDate1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOnsetDate2" name="Recertification_GenericWoundOnsetDate2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOnsetDate3" name="Recertification_GenericWoundOnsetDate3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOnsetDate4" name="Recertification_GenericWoundOnsetDate4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOnsetDate5" name="Recertification_GenericWoundOnsetDate5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Size:
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundSize1" name="Recertification_GenericWoundSize1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundSize2" name="Recertification_GenericWoundSize2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundSize3" name="Recertification_GenericWoundSize3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundSize4" name="Recertification_GenericWoundSize4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundSize5" name="Recertification_GenericWoundSize5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Drainage:
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundDrainage1" name="Recertification_GenericWoundDrainage1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundDrainage2" name="Recertification_GenericWoundDrainage2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundDrainage3" name="Recertification_GenericWoundDrainage3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundDrainage4" name="Recertification_GenericWoundDrainage4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundDrainage5" name="Recertification_GenericWoundDrainage5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Odor:
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOdor1" name="Recertification_GenericWoundOdor1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOdor2" name="Recertification_GenericWoundOdor2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOdor3" name="Recertification_GenericWoundOdor3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOdor4" name="Recertification_GenericWoundOdor4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundOdor5" name="Recertification_GenericWoundOdor5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Etiology:
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundEtiology1" name="Recertification_GenericWoundEtiology1">
                                            <option value="0"></option>
                                            <option value="1">Burn</option>
                                            <option value="2">Infection</option>
                                            <option value="3">Pressure</option>
                                            <option value="4">Surgical</option>
                                            <option value="5">Traumatic</option>
                                            <option value="6">Diabetic</option>
                                            <option value="7">Venous Stasis</option>
                                            <option value="8">Arterial</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundEtiology2" name="Recertification_GenericWoundEtiology2">
                                            <option value="0"></option>
                                            <option value="1">Burn</option>
                                            <option value="2">Infection</option>
                                            <option value="3">Pressure</option>
                                            <option value="4">Surgical</option>
                                            <option value="5">Traumatic</option>
                                            <option value="6">Diabetic</option>
                                            <option value="7">Venous Stasis</option>
                                            <option value="8">Arterial</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundEtiology3" name="Recertification_GenericWoundEtiology3">
                                            <option value="0"></option>
                                            <option value="1">Burn</option>
                                            <option value="2">Infection</option>
                                            <option value="3">Pressure</option>
                                            <option value="4">Surgical</option>
                                            <option value="5">Traumatic</option>
                                            <option value="6">Diabetic</option>
                                            <option value="7">Venous Stasis</option>
                                            <option value="8">Arterial</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundEtiology4" name="Recertification_GenericWoundEtiology4">
                                            <option value="0"></option>
                                            <option value="1">Burn</option>
                                            <option value="2">Infection</option>
                                            <option value="3">Pressure</option>
                                            <option value="4">Surgical</option>
                                            <option value="5">Traumatic</option>
                                            <option value="6">Diabetic</option>
                                            <option value="7">Venous Stasis</option>
                                            <option value="8">Arterial</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundEtiology5" name="Recertification_GenericWoundEtiology5">
                                            <option value="0"></option>
                                            <option value="1">Burn</option>
                                            <option value="2">Infection</option>
                                            <option value="3">Pressure</option>
                                            <option value="4">Surgical</option>
                                            <option value="5">Traumatic</option>
                                            <option value="6">Diabetic</option>
                                            <option value="7">Venous Stasis</option>
                                            <option value="8">Arterial</option>
                                        </select>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Stage:
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundStage1" name="Recertification_GenericWoundStage1">
                                            <option value="0"></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundStage2" name="Recertification_GenericWoundStage2">
                                            <option value="0"></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundStage3" name="Recertification_GenericWoundStage3">
                                            <option value="0"></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundStage4" name="Recertification_GenericWoundStage4">
                                            <option value="0"></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>&nbsp;
                                    </td>
                                    <td>
                                        <select id="Recertification_GenericWoundStage5" name="Recertification_GenericWoundStage5">
                                            <option value="0"></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Undermining:
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundUndermining1" name="Recertification_GenericWoundUndermining1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundUndermining2" name="Recertification_GenericWoundUndermining2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundUndermining3" name="Recertification_GenericWoundUndermining3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundUndermining4" name="Recertification_GenericWoundUndermining4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundUndermining5" name="Recertification_GenericWoundUndermining5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inflammation:
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundInflamation1" name="Recertification_GenericWoundInflamation1"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundInflamation2" name="Recertification_GenericWoundInflamation2"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundInflamation3" name="Recertification_GenericWoundInflamation3"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundInflamation4" name="Recertification_GenericWoundInflamation4"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                    <td>
                                        <input type="text" id="Recertification_GenericWoundInflamation5" name="Recertification_GenericWoundInflamation5"
                                            value="" maxlength="17" size="17" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comments:
                                    </td>
                                    <td colspan="5">
                                        <textarea id="Recertification_GenericWoundComment" name="Recertification_GenericWoundComment"></textarea>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div>
                                            <strong>Attachments</strong>
                                        </div>
                                        <div>
                                            <ul>
                                                <li>No files uploaded</li>
                                                <li>Upload File:</li>
                                                <li>
                                                    <input id="attachment" accept="" /></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <input type="button" onclick="addFile('attachment')" value="Upload Attachment" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485IntegumentaryInterventions" value=" " />
                                        <input name="Recertification_485IntegumentaryInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructTurningRepositionPerson" id="Recertification_485InstructTurningRepositionPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on turning/repositioning every 2 hours
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructFloatHeelsPerson" id="Recertification_485InstructFloatHeelsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to float heels
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructReduceFrictionPerson" id="Recertification_485InstructReduceFrictionPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on methods to reduce friction and shear
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructPadBonyProminencesPerson" id="Recertification_485InstructPadBonyProminencesPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to pad all bony prominences
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructWoundCarePerson" id="Recertification_485InstructWoundCarePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on wound care as follows:
                                        <br />
                                        <textarea name="Recertification_485InstructWoundCarePersonFrequency" id="Recertification_485InstructWoundCarePersonFrequency"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        Other:
                                        <br />
                                        <textarea name="Recertification_485InstructWoundOtherDetails" id="Recertification_485InstructWoundOtherDetails"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess skin for breakdown every visit
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess/evaluate wound(s) at each dressing change and PRN for signs/symptoms
                                        of infection. Report to physician increased temp >100.5, chills, increase in drainage,
                                        foul odor, redness, unrelieved pain >
                                        <input type="text" name="Recertification_485AssessWoundDressingChangeScale" id="Recertification_485AssessWoundDressingChangeScale"
                                            size="3" maxlength="3" value="" />
                                        on 0-10 scale, and any other significant changes
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructSignsWoundInfectionPerson" id="Recertification_485InstructSignsWoundInfectionPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on signs/symptoms of wound infection to report to physician, to include increased
                                        temp >100.5, chills, increase in drainage, foul odor, redness, unrelieved pain >
                                        <input type="text" name="Recertification_485InstructSignsWoundInfectionScale" id="Recertification_485InstructSignsWoundInfectionScale"
                                            size="3" maxlength="3" value="" />
                                        on 0-10 scale, and any other significant changes
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryInterventions" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        May discontinue wound care when wound(s) have healed
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485IntegumentaryOrderTemplates" name="Recertification_485IntegumentaryOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485IntegumentaryInterventionComments" id="Recertification_485IntegumentaryInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485IntegumentaryGoals" value=" " />
                                        <input name="Recertification_485IntegumentaryGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Wound(s) will heal without complication by:
                                        <input type="text" name="Recertification_485HealWithoutComplicationDate" id="Recertification_485HealWithoutComplicationDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Wound(s) will be free from signs and symptoms of infection during 60 day episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Wound(s) will decrease in size by
                                        <input type="text" name="Recertification_485WoundSizeDecreasePercent" id="Recertification_485WoundSizeDecreasePercent"
                                            size="4" maxlength="4" value="" />
                                        % by
                                        <input type="text" name="Recertification_485WoundSizeDecreasePercentDate" id="Recertification_485WoundSizeDecreasePercentDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485IntegumentaryGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient skin integrity will remain intact during this episode
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485IntegumentaryGoalTemplates" name="Recertification_485IntegumentaryGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485IntegumentaryGoalComments" id="Recertification_485IntegumentaryGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editRespiratorystatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationRespiratoryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Respiratory
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" name="Recertification_GenericRespiratoryCondition" value=" " />
                                        <input name="Recertification_GenericRespiratoryCondition" value="1" type="checkbox" />&nbsp;
                                        WNL (Within Normal Limits)
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" rowspan="2">
                                        <input name="Recertification_GenericRespiratoryCondition" value="2" type="checkbox" />&nbsp;
                                        <strong>Lung Sounds:</strong>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericRespiratorySounds" value=" " />
                                                <input name="Recertification_GenericRespiratorySounds" value="1" type="checkbox" />&nbsp;
                                                CTA</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsCTAText" id="Recertification_GenericLungSoundsCTAText"
                                                    size="20" maxlength="20" value="" /></li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="2" type="checkbox" />&nbsp;
                                                Rales</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsRalesText" id="Recertification_GenericLungSoundsRalesText"
                                                    size="20" maxlength="20" value="" /></li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="3" type="checkbox" />&nbsp;
                                                Rhonchi </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsRhonchiText" id="Recertification_GenericLungSoundsRhonchiText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="4" type="checkbox" />&nbsp;
                                                Wheezes</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsWheezesText" id="Recertification_GenericLungSoundsWheezesText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="5" type="checkbox" />&nbsp;
                                                Crackles </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsCracklesText" id="Recertification_GenericLungSoundsCracklesText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="6" type="checkbox" />&nbsp;
                                                Diminished </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsDiminishedText" id="Recertification_GenericLungSoundsDiminishedText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="7" type="checkbox" />&nbsp;
                                                Absent </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsAbsentText" id="Recertification_GenericLungSoundsAbsentText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratorySounds" value="8" type="checkbox" />&nbsp;
                                                Stridor </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLungSoundsStridorText" id="Recertification_GenericLungSoundsStridorText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td width="50%">
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericRespiratoryCondition" value="3" type="checkbox" />&nbsp;
                                                <strong>Sputum:</strong>&nbsp; </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">Enter amount: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericSputumAmount" id="Recertification_GenericSputumAmount"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">Describe color, consistency, and odor: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericSputumColorConsistencyOdor" id="Recertification_GenericSputumColorConsistencyOdor"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericRespiratoryCondition" value="4" type="checkbox" />&nbsp;
                                                <strong>02</strong> </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">At: </li>
                                            <li>
                                                <input type="text" name="Recertification_Generic02AtText" id="Recertification_Generic02AtText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer"></li>
                                            <li class="spacer">LPM via:</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLPMVia" id="Recertification_GenericLPMVia"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratoryCondition" value="5" type="checkbox" />&nbsp;
                                                02 Sat:</li>
                                            <li>
                                                <input type="text" name="Recertification_Generic02SatText" id="Recertification_Generic02SatText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li></li>
                                            <li class="spacer"></li>
                                            <li>
                                                <select name="Recertification_Generic02SatList" id="Recertification_Generic02SatList">
                                                    <option value=" "></option>
                                                    <option value="Room Air">Room Air</option>
                                                    <option value="02">02</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratoryCondition" value="6" type="checkbox" />&nbsp;Nebulizer:
                                            </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericNebulizerText" id="Recertification_GenericNebulizerText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <ul class="columns">
                                            <li></li>
                                            <li class="spacer">
                                                <input name="Recertification_GenericRespiratoryCondition" value="7" type="checkbox" />&nbsp;
                                                Cough:</li>
                                            <li>
                                                <select name="Recertification_GenericCoughList" id="Recertification_GenericCoughList">
                                                    <option value=" "></option>
                                                    <option value="Productive">Productive</option>
                                                    <option value="Nonproductive">Nonproductive</option>
                                                </select>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Comments:<br />
                                        <textarea name="Recertification_GenericRespiratoryComments" id="Recertification_GenericRespiratoryComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        <div class="margin">
                                            (M1400) When is the patient dyspneic or noticeably Short of Breath?
                                        </div>
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1400PatientDyspneic" type="hidden" value=" " />
                                    <input name="Recertification_M1400PatientDyspneic" type="radio" value="00" />&nbsp;0
                                    - Patient is not short of breath<br />
                                    <input name="Recertification_M1400PatientDyspneic" type="radio" value="01" />&nbsp;1
                                    - When walking more than 20 feet, climbing stairs<br />
                                    <input name="Recertification_M1400PatientDyspneic" type="radio" value="02" />&nbsp;2
                                    - With moderate exertion (e.g., while dressing, using commode or bedpan, walking
                                    distances less than 20 feet)<br />
                                    <input name="Recertification_M1400PatientDyspneic" type="radio" value="03" />&nbsp;3
                                    - With minimal exertion (e.g., while eating, talking, or performing other ADLs)
                                    or with agitation<br />
                                    <input name="Recertification_M1400PatientDyspneic" type="radio" value="04" />&nbsp;4
                                    - At rest (during day or night)<br />
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485RespiratoryInterventions" value=" " />
                                        <input name="Recertification_485RespiratoryInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct caregiver on pulmonary toilet including percussion therapy and postural
                                        drainage (freq)
                                        <input type="text" name="Recertification_485InstructPulmonaryToiletFrequency" id="Recertification_485InstructPulmonaryToiletFrequency"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to perform pulmonary toilet including percussion therapy and postural drainage
                                        (freq)
                                        <input type="text" name="Recertification_485PerformPulmonaryToiletFrequency" id="Recertification_485PerformPulmonaryToiletFrequency"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructNebulizerUsePerson" id="Recertification_485InstructNebulizerUsePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on proper use of nebulizer/inhaler, and assess return demonstration
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess O2 saturation on room air (freq)
                                        <input type="text" name="Recertification_485AssessOxySaturationFrequency" id="Recertification_485AssessOxySaturationFrequency"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess O2 saturation on O2 @
                                        <input type="text" name="Recertification_485AssessOxySatOnOxyAt" id="Recertification_485AssessOxySatOnOxyAt"
                                            size="5" maxlength="5" value="" />
                                        LPM/
                                        <input type="text" name="Recertification_485AssessOxySatLPM" id="Recertification_485AssessOxySatLPM"
                                            size="5" maxlength="5" value="" />
                                        (freq)
                                        <input type="text" name="Recertification_485AssessOxySatOnOxyFrequency" id="Recertification_485AssessOxySatOnOxyFrequency"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructSobFactorsPerson" id="Recertification_485InstructSobFactorsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on factors that contribute to SOB, including avoiding outdoors on poor air quality
                                        days. Avoid leaving windows open when outside temperature is above
                                        <input type="text" name="Recertification_485InstructSobFactorsTemp" id="Recertification_485InstructSobFactorsTemp"
                                            size="5" maxlength="5" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructAvoidSmokingPerson" id="Recertification_485InstructAvoidSmokingPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to avoid smoking or allowing people to smoke in patient's home. Instruct patient
                                        to avoid irritants/allergens known to increase SOB
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on pursed lip breathing techniques
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on energy conserving measures including frequent rest periods,
                                        small frequent meals, avoiding large meals/overeating, controlling stress
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on proper use of nebulizer treatment with
                                        <input type="text" name="Recertification_485InstructNebulizerTreatmentType" id="Recertification_485InstructNebulizerTreatmentType"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="11" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on proper use of
                                        <input type="text" name="Recertification_485InstructProperUseOfType" id="Recertification_485InstructProperUseOfType"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="12" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct caregiver on proper suctioning technique
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="13" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485RecognizePulmonaryDysfunctionPerson" id="Recertification_485RecognizePulmonaryDysfunctionPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on methods to recognize pulmonary dysfunction and relieve complications
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryInterventions" value="14" type="checkbox" />
                                    </td>
                                    <td>
                                        Report to physician O2 saturation less than
                                        <input type="text" name="Recertification_485OxySaturationLessThanPercent" id="Recertification_485OxySaturationLessThanPercent"
                                            size="15" maxlength="15" value="" />
                                        %
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485RespiratoryOrderTemplates" name="Recertification_485RespiratoryOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485RespiratoryInterventionComments" id="Recertification_485RespiratoryInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table bprder="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485RespiratoryGoals" value=" " />
                                        <input name="Recertification_485RespiratoryGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's respiratory rate will remain within established parameters during the
                                        episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will be free from signs and symptoms of respiratory distress during the
                                        episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient and caregiver will verbalize an understanding of factors that contribute
                                        to shortness of breath by:
                                        <input type="text" name="Recertification_485VerbalizeFactorsSobDate" id="Recertification_485VerbalizeFactorsSobDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will demonstrate proper pursed lip breathing techniques by
                                        <input type="text" name="Recertification_485DemonstrateLipBreathingDate" id="Recertification_485DemonstrateLipBreathingDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will verbalize an understanding of energy conserving measures by:
                                        <input type="text" name="Recertification_485VerbalizeEnergyConserveDate" id="Recertification_485VerbalizeEnergyConserveDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeSafeOxyManagementPerson" id="Recertification_485VerbalizeSafeOxyManagementPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize and demonstrate safe management of oxygen by:
                                        <input type="text" name="Recertification_485VerbalizeSafeOxyManagementPersonDate"
                                            id="Recertification_485VerbalizeSafeOxyManagementPersonDate" size="10" maxlength="10"
                                            value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will return demonstrate proper use of nebulizer treatment by
                                        <input type="text" name="Recertification_485DemonstrateNebulizerUseDate" id="Recertification_485DemonstrateNebulizerUseDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485RespiratoryGoals" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will demonstrate proper use of
                                        <input type="text" name="Recertification_485DemonstrateProperUseOfType" id="Recertification_485DemonstrateProperUseOfType"
                                            size="15" maxlength="15" value="" />
                                        by:
                                        <input type="text" name="Recertification_485DemonstrateProperUseOfTypeDate" id="Recertification_485DemonstrateProperUseOfTypeDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485RespiratoryGoalTemplates" name="Recertification_485RespiratoryGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485RespiratoryGoalComments" id="Recertification_485RespiratoryGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editEndocrine_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationEndocrineForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Endocrine
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericEndocrineWithinNormalLimits" id="Recertification_GenericEndocrineWithinNormalLimits" />
                                        <input name="Recertification_GenericEndocrineWithinNormalLimits" value="1" type="checkbox" />
                                        WNL (Within Normal Limits)
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is patient diabetic? </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPatientDiabetic" value="" />
                                                <input type="radio" name="Recertification_GenericPatientDiabetic" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericPatientDiabetic" value="0" />&nbsp;
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Insulin dependent? </li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericInsulinDependent" value="" />
                                                <input type="radio" name="Recertification_GenericInsulinDependent" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericInsulinDependent" value="0" />&nbsp;
                                                No </li>
                                            <li class="spacer">For how long? </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericInsulinDependencyDuration" id="Recertification_GenericInsulinDependencyDuration"
                                                    size="10" maxlength="10" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is patient independently able to draw up correct dose of insulin?
                                            </li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericDrawUpInsulinDose" value="" />
                                                <input type="radio" name="Recertification_GenericDrawUpInsulinDose" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericDrawUpInsulinDose" value="0" />&nbsp;
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is patient able to properly administer own insulin? </li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericAdministerOwnInsulin" value="" />
                                                <input type="radio" name="Recertification_GenericAdministerOwnInsulin" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericAdministerOwnInsulin" value="0" />&nbsp;
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is patient taking oral hypoglycemic agent? </li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericOralHypoglycemicAgent" value="" />
                                                <input type="radio" name="Recertification_GenericOralHypoglycemicAgent" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericOralHypoglycemicAgent" value="0" />&nbsp;
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is patient independent with glucometer use? </li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericGlucometerUseIndependent" value="" />
                                                <input type="radio" name="Recertification_GenericGlucometerUseIndependent" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericGlucometerUseIndependent" value="0" />&nbsp;
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is caregiver able to correctly draw up and administer insulin?
                                            </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericCareGiverDrawUpInsulin" value="" />
                                                <input type="radio" name="Recertification_GenericCareGiverDrawUpInsulin" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericCareGiverDrawUpInsulin" value="0" />&nbsp;
                                                No &nbsp;
                                                <input type="radio" name="Recertification_GenericCareGiverDrawUpInsulin" value="2" />&nbsp;
                                                N/A, no caregiver</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Is caregiver independent with glucometer use? </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericCareGiverGlucometerUse" value="" />
                                                <input type="radio" name="Recertification_GenericCareGiverGlucometerUse" value="1" />&nbsp;
                                                Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericCareGiverGlucometerUse" value="0" />&nbsp;
                                                No &nbsp;
                                                <input type="radio" name="Recertification_GenericCareGiverGlucometerUse" value="2" />&nbsp;
                                                N/A, no caregiver</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Does patient or caregiver routinely perform inspection of the
                                                patient's lower extremities? </li>
                                            <li class="spacer">
                                                <input type="hidden" name="Recertification_GenericCareGiverInspectLowerExtremities"
                                                    value="" />
                                                <input type="radio" name="Recertification_GenericCareGiverInspectLowerExtremities"
                                                    value="1" />&nbsp; Yes&nbsp;
                                                <input type="radio" name="Recertification_GenericCareGiverInspectLowerExtremities"
                                                    value="0" />&nbsp; No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Does patient have any of the following? </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPatientEdocrineProblem" value="" />
                                                <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="1" />&nbsp;
                                                Polyuria
                                                <br />
                                                <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="2" />&nbsp;
                                                Polydipsia<br />
                                                <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="3" />&nbsp;
                                                Polyphagia
                                                <br />
                                                <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="4" />&nbsp;
                                                Neuropathy<br />
                                                <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="5" />&nbsp;
                                                Radiculopathy
                                                <br />
                                                <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="6" />&nbsp;
                                                Thyroid problems
                                                <br />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Blood Sugar&nbsp;
                                                <input type="text" name="Recertification_GenericBloodSugarLevelText" id="Recertification_GenericBloodSugarLevelText"
                                                    size="10" maxlength="10" value="" />
                                            </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericBloodSugarLevel" value=" " />
                                                <input type="radio" name="Recertification_GenericBloodSugarLevel" value="Random" />
                                                Random
                                                <br />
                                            </li>
                                            <li>
                                                <input type="radio" name="Recertification_GenericBloodSugarLevel" value="Fasting" />
                                                Fasting
                                                <br />
                                            </li>
                                            <li>
                                                <input type="radio" name="Recertification_GenericBloodSugarLevel" value="2HoursPP" />
                                                2 Hours PP
                                                <br />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Blood sugar checked by: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericBloodSugarCheckedBy" id="Recertification_GenericBloodSugarCheckedBy"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Site: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericBloodSugarSite" id="Recertification_GenericBloodSugarSite"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>Comments:<br />
                                                <textarea name="Recertification_GenericEndocrineComments" id="Recertification_GenericEndocrineComments"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485EndocrineInterventions" value=" " />
                                        <input name="Recertification_485EndocrineInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructDiabeticManagementPerson" id="Recertification_485InstructDiabeticManagementPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on all aspects of diabetic management to include disease process, foot assessments,
                                        signs and symptoms of hypo/hyperglycemia, glucometer use and preparation and administration
                                        of diabetic medications ordered by physician
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructInspectFeetDailyPerson" id="Recertification_485InstructInspectFeetDailyPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to inspect patient's feet daily and report any skin or nail problems to SN
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructWashFeetWarmPerson" id="Recertification_485InstructWashFeetWarmPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to wash patient's feet in warm (not hot) water. Wash feet gently and pat dry thoroughly
                                        making sure to dry between toes
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructMoisturizerPerson" id="Recertification_485InstructMoisturizerPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to use moisturizer daily but avoid getting between toes
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to wear clean, dry, properly-fitted socks and change them
                                        every day
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructNailCarePerson" id="Recertification_485InstructNailCarePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on appropriate nail care as follows: trim nails straight across and file rough edges
                                        with nail file
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructNeverWalkBareFootPerson" id="Recertification_485InstructNeverWalkBareFootPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        that patient should never walk barefoot
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructElevateFeetPerson" id="Recertification_485InstructElevateFeetPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        that patient should elevate feet when sitting
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructProtectFeetPerson" id="Recertification_485InstructProtectFeetPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to protect patient's feet from extreme heat or cold
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructNeverCutCornsPerson" id="Recertification_485InstructNeverCutCornsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        never to try to cut off corns, calluses, or any other lesions from lower extremities
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="11" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to perform finger stick for fasting blood sugar/random blood sugar during visit
                                        if it has not been done or if patient reports signs and symptoms of hypo/hyperglycemia
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="12" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to give patient 4 oz of fruit juice or 1 tablespoon of sugar in H2O if blood
                                        sugar is
                                        <input type="text" name="Recertification_485GiveJuiceIfBloodSugarLevel" id="Recertification_485GiveJuiceIfBloodSugarLevel"
                                            size="3" maxlength="3" value="" />
                                        mg/dl or below, and recheck blood sugar in 15 to 20 minutes. If blood sugar remains
                                        <input type="text" name="Recertification_485GiveJuiceIfBloodSugarLevelRemains" id="Recertification_485GiveJuiceIfBloodSugarLevelRemains"
                                            size="3" maxlength="3" value="" />
                                        mg/dL or below, notify physician
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="13" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to prepare and administer insulin (freq)
                                        <input type="text" name="Recertification_485PrepareAdministerInsulinType" id="Recertification_485PrepareAdministerInsulinType"
                                            size="15" maxlength="15" value="" />
                                        as follows:
                                        <input type="text" name="Recertification_485PrepareAdministerInsulinFrequency" id="Recertification_485PrepareAdministerInsulinFrequency"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="14" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess blood sugar via finger stick every visit prior to insulin administration
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="15" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to prefill insulin syringes (freq)
                                        <input type="text" name="Recertification_485PrefillInsulinSyringesType" id="Recertification_485PrefillInsulinSyringesType"
                                            size="15" maxlength="15" value="" />
                                        as follows:
                                        <input type="text" name="Recertification_485PrefillInsulinSyringesTypeFrequency"
                                            id="Recertification_485PrefillInsulinSyringesTypeFrequency" size="15" maxlength="15"
                                            value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineInterventions" value="16" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to perform inspection of patient's lower extremities every visit and report any
                                        alteration in skin integrity to physician
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485EndocrineInterventionTemplates" name="Recertification_485EndocrineInterventionTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="7975"></option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485EndocrineInterventionComments" id="Recertification_485EndocrineInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485EndocrineGoals" value=" " />
                                        <input name="Recertification_485EndocrineGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's fasting blood sugar will remain between
                                        <input type="text" name="Recertification_485FastingBloodSugarBetween" id="Recertification_485FastingBloodSugarBetween"
                                            size="4" maxlength="4" value="" />
                                        mg/dl and
                                        <input type="text" name="Recertification_485FastingBloodSugarAnd" id="Recertification_485FastingBloodSugarAnd"
                                            size="4" maxlength="4" value="" />
                                        mg/dl during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's random blood sugar will remain between
                                        <input type="text" name="Recertification_485RandomBloodSugarBetween" id="Recertification_485RandomBloodSugarBetween"
                                            size="4" maxlength="4" value="" />
                                        mg/dl and
                                        <input type="text" name="Recertification_485RandomBloodSugarAnd" id="Recertification_485RandomBloodSugarAnd"
                                            size="4" maxlength="4" value="" />
                                        mg/dl during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485GlucometerUseIndependencePerson" id="Recertification_485GlucometerUseIndependencePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be independent with glucometer use by:
                                        <input type="text" name="Recertification_485GlucometerUseIndependenceDate" id="Recertification_485GlucometerUseIndependenceDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeSkinConditionUnderstandingPerson" id="Recertification_485VerbalizeSkinConditionUnderstandingPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize an understanding of skin conditions that must be reported to SN or
                                        physician immediately
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineGoals" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485IndependentInsulinAdministrationPerson" id="Recertification_485IndependentInsulinAdministrationPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be independent with insulin administration by:
                                        <input type="text" name="Recertification_485IndependentInsulinAdministrationDate"
                                            id="Recertification_485IndependentInsulinAdministrationDate" size="10" maxlength="10"
                                            value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EndocrineGoals" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeProperFootCarePerson" id="Recertification_485VerbalizeProperFootCarePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of proper diabetic foot care by:
                                        <input type="text" name="Recertification_485VerbalizeProperFootCareDate" id="Recertification_485VerbalizeProperFootCareDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485EndocrineGoalTemplates" name="Recertification_485EndocrineGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485EndocrineGoalComments" id="Recertification_485EndocrineGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editCardiacstatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationCardiacForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Cardiovascular
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericCardioVascular" value=" " />
                                        <input name="Recertification_GenericCardioVascular" value="1" type="checkbox" />&nbsp;
                                        WNL (Within Normal Limits)
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericCardioVascular" value="2" type="checkbox" />&nbsp;
                                                Dizziness: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDizzinessDesc" id="Recertification_GenericDizzinessDesc"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericCardioVascular" value="3" type="checkbox" />&nbsp;
                                                Chest Pain: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericChestPainDesc" id="Recertification_GenericChestPainDesc"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericCardioVascular" value="4" type="checkbox" />&nbsp;
                                                Edema: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericEdemaDesc1" id="Recertification_GenericEdemaDesc1"
                                                    size="50" maxlength="50" value="" />
                                            </li>
                                            <li>
                                                <select name="Recertification_GenericEdemaList1" id="Recertification_GenericEdemaList1">
                                                    <option value=" "></option>
                                                    <option value="1+">1+</option>
                                                    <option value="2+">2+</option>
                                                    <option value="3+">3+</option>
                                                    <option value="4+">4+</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericEdemaDesc2" id="Recertification_GenericEdemaDesc2"
                                                    size="50" maxlength="50" value="" />
                                            </li>
                                            <li>
                                                <select name="Recertification_GenericEdemaList2" id="Recertification_GenericEdemaList2">
                                                    <option value=" "></option>
                                                    <option value="1+">1+</option>
                                                    <option value="2+">2+</option>
                                                    <option value="3+">3+</option>
                                                    <option value="4+">4+</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericEdemaDesc3" id="Recertification_GenericEdemaDesc3"
                                                    size="50" maxlength="50" value="" />
                                            </li>
                                            <li>
                                                <select name="Recertification_GenericEdemaList3" id="Recertification_GenericEdemaList3">
                                                    <option value=" "></option>
                                                    <option value="1+">1+</option>
                                                    <option value="2+">2+</option>
                                                    <option value="3+">3+</option>
                                                    <option value="4+">4+</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericCardioVascular" value="5" type="checkbox" />&nbsp;
                                                Dependent Edema: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPittingEdemaType" value="" />
                                                <input name="Recertification_GenericPittingEdemaType" value="1" type="checkbox" />&nbsp;
                                                Pitting </li>
                                            <li>
                                                <input name="Recertification_GenericPittingEdemaType" value="2" type="checkbox" />&nbsp;
                                                Nonpitting </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericCardioVascular" value="6" type="checkbox" />&nbsp;
                                                Heart Sounds: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericHeartSoundsType" value="" />
                                                <input name="Recertification_GenericHeartSoundsType" value="1" type="checkbox" />&nbsp;
                                                Murmur </li>
                                            <li>
                                                <input name="Recertification_GenericHeartSoundsType" value="2" type="checkbox" />&nbsp;
                                                Gallop </li>
                                            <li>
                                                <input name="Recertification_GenericHeartSoundsType" value="3" type="checkbox" />&nbsp;
                                                Click </li>
                                            <li>
                                                <input name="Recertification_GenericHeartSoundsType" value="4" type="checkbox" />&nbsp;
                                                Irregular </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericCardioVascular" value="7" type="checkbox" />&nbsp;
                                                Neck Vein Distention: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericNeckVeinDistentionDesc" id="Recertification_GenericNeckVeinDistentionDesc"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericCardioVascular" value="8" type="checkbox" />&nbsp;
                                                Peripheral Pulses: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericPeripheralPulsesDesc" id="Recertification_GenericPeripheralPulsesDesc"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericCardioVascular" value="9" type="checkbox" />&nbsp;
                                                Cap Refill: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericCapRefillLessThan3" value=" " type="hidden" />
                                                <input name="Recertification_GenericCapRefillLessThan3" value="1" type="radio" />&nbsp;
                                                <3 sec
                                                <br />
                                                <input name="Recertification_GenericCapRefillLessThan3" value="0" type="radio" />&nbsp;
                                                >3 sec </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Pacemaker:</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericPacemakerDate" id="Recertification_GenericPacemakerDate"
                                                    size="10" maxlength="10" value="" />
                                                (Insertion date) </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">AICD: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericAICDDate" id="Recertification_GenericAICDDate"
                                                    size="10" maxlength="10" value="" />
                                                (Insertion date) </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <ul>
                                            <li>Comments:<br />
                                                <textarea name="Recertification_GenericCardiovascularComments" id="Recertification_GenericCardiovascularComments"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485CardiacStatusInterventions" value=" " />
                                        <input name="Recertification_485CardiacStatusInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on daily weight self-monitoring program where the patient
                                        utilizes the same scales on a hard, flat surface each morning prior to breakfast
                                        and after urination. Report to SN weight
                                        <input name="Recertification_485WeightSelfMonitorGain" value=" " type="hidden" />
                                        <input name="Recertification_485WeightSelfMonitorGain" value="1" type="radio" />
                                        gain
                                        <input name="Recertification_485WeightSelfMonitorGain" value="0" type="radio" />
                                        loss of
                                        <input type="text" name="Recertification_485WeightSelfMonitorGainDay" id="Recertification_485WeightSelfMonitorGainDay"
                                            size="3" maxlength="3" value="" />
                                        lb/1 day,
                                        <input type="text" name="Recertification_485WeightSelfMonitorGainWeek" id="Recertification_485WeightSelfMonitorGainWeek"
                                            size="3" maxlength="3" value="" />
                                        lb/1 week
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess patient's weight log every visit
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructRecognizeCardiacDysfunctionPerson" id="Recertification_485InstructRecognizeCardiacDysfunctionPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on measures to recognize cardiac dysfunction and relieve complications
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on measures to detect and alleviate edema
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient when (s)he starts feeling chest pain, tightness, or squeezing
                                        in the chest to take nitroglycerin. Patient may take nitroglycerin one time every
                                        5 minutes. If no relief after 3 doses, call 911
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the patient the following symptoms could be signs of a heart attack:
                                        chest discomfort, discomfort in one or both arms, back, neck, jaw, stomach, shortness
                                        of breath, cold sweat, nausea, or dizziness. Instruct patient on signs and symptoms
                                        that necessitate calling 911
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        No blood pressure or venipuncture in
                                        <input type="text" name="Recertification_485NoBloodPressureArm" id="Recertification_485NoBloodPressureArm"
                                            size="10" maxlength="10" value="" />
                                        arm
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="100%">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485CardiacOrderTemplates" name="Recertification_485CardiacOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485CardiacInterventionComments" id="Recertification_485CardiacInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485CardiacStatusGoals" value=" " />
                                        <input name="Recertification_485CardiacStatusGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient weight will be maintained between
                                        <input type="text" name="Recertification_485WeightMaintainedMin" id="Recertification_485WeightMaintainedMin"
                                            size="10" maxlength="10" value="" />
                                        lbs and
                                        <input type="text" name="Recertification_485WeightMaintainedMax" id="Recertification_485WeightMaintainedMax"
                                            size="10" maxlength="10" value="" />
                                        lbs during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's blood pressure will remain within established parameters during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's pulse will remain within established parameters during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will remain free from chest pain, or chest pain will be relieved with nitroglycerin,
                                        during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeCardiacSymptomsPerson" id="Recertification_485VerbalizeCardiacSymptomsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of symptoms of cardiac complications and when to call
                                        911 by:
                                        <input type="text" name="Recertification_485VerbalizeCardiacSymptomsDate" id="Recertification_485VerbalizeCardiacSymptomsDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485CardiacStatusGoals" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeEdemaRelieverPerson" id="Recertification_485VerbalizeEdemaRelieverPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize and demonstrate edema-relieving measures by:
                                        <input type="text" name="Recertification_485VerbalizeEdemaRelieverDate" id="Recertification_485VerbalizeEdemaRelieverDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485CardiacGoalTemplates" name="Recertification_485CardiacGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485CardiacGoalComments" id="Recertification_485CardiacGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editEliminationstatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationEliminationStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th>
                                        GU
                                    </th>
                                    <th>
                                        Digestive
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericGU" value=" " />
                                                <input name="Recertification_GenericGU" value="1" type="checkbox" />&nbsp; WNL (Within
                                                Normal Limits) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="2" type="checkbox" />&nbsp; Incontinence
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="3" type="checkbox" />&nbsp; Bladder
                                                Distention </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="4" type="checkbox" />&nbsp; Burning
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="5" type="checkbox" />&nbsp; Frequency
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="6" type="checkbox" />&nbsp; Dysuria
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="7" type="checkbox" />&nbsp; Retention
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="8" type="checkbox" />&nbsp; Urgency
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGU" value="9" type="checkbox" />&nbsp; Urostomy
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericGU" value="10" type="checkbox" />&nbsp; Catheter:
                                            </li>
                                            <li>
                                                <select name="Recertification_GenericGUCatheterList" id="Recertification_GenericGUCatheterList">
                                                    <option value="0"></option>
                                                    <option value="Foley">Foley</option>
                                                    <option value=" Suprapubic">Suprapubic</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>Last Changed
                                                <input type="text" name="Recertification_GenericGUCatheterLastChanged" id="Recertification_GenericGUCatheterLastChanged"
                                                    size="10" maxlength="10" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericGUCatheterFrequency" id="Recertification_GenericGUCatheterFrequency"
                                                    size="5" maxlength="5" value="" />
                                                Fr </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericGUCatheterAmount" id="Recertification_GenericGUCatheterAmount"
                                                    size="5" maxlength="5" value="" />
                                                cc </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericGU" value="11" type="checkbox" />&nbsp; Urine:
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericGUUrine" value=" " />
                                                <input name="Recertification_GenericGUUrine" value="1" type="checkbox" />&nbsp;
                                                Cloudy
                                                <br />
                                                <input name="Recertification_GenericGUUrine" value="2" type="checkbox" />&nbsp;
                                                Odorous
                                                <br />
                                                <input name="Recertification_GenericGUUrine" value="3" type="checkbox" />&nbsp;
                                                Sediment
                                                <br />
                                                <input name="Recertification_GenericGUUrine" value="4" type="checkbox" />&nbsp;
                                                Hematuria
                                                <br />
                                                <input name="Recertification_GenericGUUrine" value="5" type="checkbox" />&nbsp;
                                                Other
                                                <input type="text" name="Recertification_GenericGUOtherText" id="Recertification_GenericGUOtherText"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericGU" value="12" type="checkbox" />&nbsp; External
                                                Genitalia: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericGUNormal" value=" " type="hidden" />
                                                <input name="Recertification_GenericGUNormal" value="1" type="radio" />
                                                Normal
                                                <br />
                                                <input name="Recertification_GenericGUNormal" value="0" type="radio" />
                                                Abnormal
                                                <br />
                                                As per:<br />
                                                <input name="Recertification_GenericGUClinicalAssessment" value=" " type="hidden" />
                                                <input name="Recertification_GenericGUClinicalAssessment" value="1" type="radio" />
                                                Clinician Assessment
                                                <br />
                                                <input name="Recertification_GenericGUClinicalAssessment" value="0" type="radio" />
                                                Pt/CG Report </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestive" name=" " />
                                                <input name="Recertification_GenericDigestive" value="1" type="checkbox" />&nbsp;
                                                WNL </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="2" type="checkbox" />&nbsp;
                                                Nausea/Vomiting </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="3" type="checkbox" />&nbsp;
                                                NPO </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="4" type="checkbox" />&nbsp;
                                                Reflux/Indigestion </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="5" type="checkbox" />&nbsp;
                                                Diarrhea </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="6" type="checkbox" />&nbsp;
                                                Constipation </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="7" type="checkbox" />&nbsp;
                                                Bowel Incontinence </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="8" type="checkbox" />&nbsp;
                                                Bowel Sounds: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveBowelSoundsType" value=" " type="hidden" />
                                                <input name="Recertification_GenericDigestiveBowelSoundsType" value="Hyperactive"
                                                    type="radio" />
                                                Hyperactive
                                                <br />
                                                <input name="Recertification_GenericDigestiveBowelSoundsType" value="Hypoactive"
                                                    type="radio" />
                                                Hypoactive
                                                <br />
                                                <input name="Recertification_GenericDigestiveBowelSoundsType" value="Normal" type="radio" />
                                                Normal
                                                <br />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericDigestive" value="9" type="checkbox" />
                                                Abd Girth: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDigestiveAbdGirthLength" id="Recertification_GenericDigestiveAbdGirthLength"
                                                    size="5" maxlength="5" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericDigestive" value="10" type="checkbox" />&nbsp;
                                                Last BM: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDigestiveLastBMDate" id="Recertification_GenericDigestiveLastBMDate"
                                                    size="10" maxlength="10" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>As per:
                                                <input type="hidden" name="Recertification_GenericDigestiveLastBMAsper" value=" " />
                                                <input type="radio" name="Recertification_GenericDigestiveLastBMAsper" value="Clinician Assessment" />
                                                Clinician Assessment
                                                <input type="radio" name="Recertification_GenericDigestiveLastBMAsper" value="Pt/CG Report" />
                                                Pt/CG Report </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestiveLastBM" value=" " />
                                                <input name="Recertification_GenericDigestiveLastBM" value="1" type="checkbox" />&nbsp;
                                                WNL </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBM" value="2" type="checkbox" />&nbsp;
                                                Abnormal Stool: </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestiveLastBMAbnormalStool" value=" " />
                                                <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="1" type="checkbox" />&nbsp;
                                                Gray </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="2" type="checkbox" />&nbsp;
                                                Tarry </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="3" type="checkbox" />&nbsp;
                                                Fresh Blood </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="4" type="checkbox" />&nbsp;
                                                Black </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBM" value="3" type="checkbox" />&nbsp;
                                                Constipation: </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestiveLastBMConstipationType"
                                                    value=" " />
                                                <input type="radio" name="Recertification_GenericDigestiveLastBMConstipationType"
                                                    value="Chronic" />
                                                Chronic </li>
                                            <li>
                                                <input type="radio" name="Recertification_GenericDigestiveLastBMConstipationType"
                                                    value="Acute" />
                                                Acute </li>
                                            <li>
                                                <input type="radio" name="Recertification_GenericDigestiveLastBMConstipationType"
                                                    value="Occasional" />
                                                Occasional </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBM" value="4" type="checkbox" />&nbsp;
                                                Lax/Enema Use: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDigestiveLaxEnemaUseDesc" id="Recertification_GenericDigestiveLaxEnemaUseDesc"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveLastBM" value="5" type="checkbox" />&nbsp;
                                                Hemorrhoids: </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestiveHemorrhoidsType" value=" " />
                                                <input type="radio" name="Recertification_GenericDigestiveHemorrhoidsType" value="Internal" />
                                                <i>Internal</i> </li>
                                            <li>
                                                <input type="radio" name="Recertification_GenericDigestiveHemorrhoidsType" value="External" />
                                                <i>External</i> </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericDigestive" value="11" type="checkbox" />&nbsp;
                                                Ostomy: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>Ostomy Type(s):
                                                <input type="text" name="Recertification_GenericDigestiveOstomyType" id="Recertification_GenericDigestiveOstomyType"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestiveOstomy" value=" " />
                                                <input name="Recertification_GenericDigestiveOstomy" value="1" type="checkbox" />&nbsp;
                                                Stoma Appearance: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDigestiveStomaType" id="Recertification_GenericDigestiveStomaType"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveOstomy" value="2" type="checkbox" />&nbsp;
                                                Stool Appearance: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDigestiveStoolAppearanceDesc" id="Recertification_GenericDigestiveStoolAppearanceDesc"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericDigestiveOstomy" value="3" type="checkbox" />&nbsp;
                                                Surrounding Skin: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDigestiveSurSkinType" id="Recertification_GenericDigestiveSurSkinType"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDigestiveSurSkinIntact" id="Recertification_GenericDigestiveSurSkinIntact" />
                                                <input name="Recertification_GenericDigestiveSurSkinIntact" value="1" type="checkbox" />&nbsp;
                                                Intact </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <ul>
                                            <li>Comments:<br />
                                                <textarea name="Recertification_GenericGUDigestiveComments" id="Recertification_GenericGUDigestiveComments"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1610) Urinary Incontinence or Urinary Catheter Presence:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1610UrinaryIncontinence" type="hidden" value=" " />
                                    <input name="Recertification_M1610UrinaryIncontinence" type="radio" value="00" />&nbsp;0
                                    - No incontinence or catheter (includes anuria or ostomy for urinary drainage) [
                                    Go to M1620 ]<br />
                                    <input name="Recertification_M1610UrinaryIncontinence" type="radio" value="01" />&nbsp;1
                                    - Patient is incontinent<br />
                                    <input name="Recertification_M1610UrinaryIncontinence" type="radio" value="02" />&nbsp;2
                                    - Patient requires a urinary catheter (i.e., external, indwelling, intermittent,
                                    suprapubic) [ Go to M1620 ]
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1620) Bowel Incontinence Frequency:
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="hidden" value=" " />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="00" />&nbsp;0
                                        - Very rarely or never has bowel incontinence<br />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="01" />&nbsp;1
                                        - Less than once weekly<br />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="02" />&nbsp;2
                                        - One to three times weekly<br />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="03" />&nbsp;3
                                        - Four to six times weekly<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="04" />&nbsp;4
                                        - On a daily basis<br />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="05" />&nbsp;5
                                        - More often than once daily<br />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="NA" />&nbsp;NA
                                        - Patient has ostomy for bowel elimination<br />
                                        <input name="Recertification_M1620BowelIncontinenceFrequency" type="radio" value="UK" />&nbsp;UK
                                        - Unknown [Omit “UK” option on FU, DC]
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel
                                        elimination that (within the last 14 days): a) was related to an inpatient facility
                                        stay, or b) necessitated a change in medical or treatment regimen?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1630OstomyBowelElimination" type="hidden" value=" " />
                                    <input name="Recertification_M1630OstomyBowelElimination" type="radio" value="00" />&nbsp;0
                                    - Patient does not have an ostomy for bowel elimination.<br />
                                    <input name="Recertification_M1630OstomyBowelElimination" type="radio" value="01" />&nbsp;1
                                    - Patient's ostomy was not related to an inpatient stay and did not necessitate
                                    change in medical or treatment regimen.<br />
                                    <input name="Recertification_M1630OstomyBowelElimination" type="radio" value="02" />&nbsp;2
                                    - The ostomy was related to an inpatient stay or did necessitate change in medical
                                    or treatment regimen.
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Dialysis
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Is patient on dialysis? </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPatientOnDialysis" value=" " />
                                                <input type="radio" name="Recertification_GenericPatientOnDialysis" value="1" />
                                                Yes </li>
                                            <li>
                                                <input type="radio" name="Recertification_GenericPatientOnDialysis" value="1" />
                                                No </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericDialysis" value=" " />
                                                <input name="Recertification_GenericDialysis" value="1" type="checkbox" />
                                                Hemodialysis </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">
                                                <input name="Recertification_GenericDialysis" value="2" type="checkbox" />
                                                AV Graft / Fistula Site: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericAVGraftSite" id="Recertification_GenericAVGraftSite"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">
                                                <input name="Recertification_GenericDialysis" value="3" type="checkbox" />
                                                Central Venous Catheter Access Site: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericVenousCatheterSite" id="Recertification_GenericVenousCatheterSite"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDialysis" value="4" type="checkbox" />
                                                Peritoneal Dialysis </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDialysis" value="5" type="checkbox" />
                                                CCPD (Continuous Cyclic Peritoneal Dialysis) </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDialysis" value="6" type="checkbox" />
                                                IPD (Intermittent Peritoneal Dialysis) </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDialysis" value="7" type="checkbox" />
                                                CAPD (Continuous Ambulatory Peritoneal Dialysis) </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericDialysis" value="8" type="checkbox" />
                                                Catheter site free from signs and symptoms of infection </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericDialysis" value="9" type="checkbox" />
                                                Other: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDialysisOtherDesc" id="Recertification_GenericDialysisOtherDesc"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Dialysis Center:</li><li>
                                                <input type="text" name="Recertification_GenericDialysisCenter" id="Recertification_GenericDialysisCenter"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Phone Number:</li><li>
                                                <input type="text" name="Recertification_GenericDialysisCenterPhone" id="Recertification_GenericDialysisCenterPhone"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Contact Person:</li><li>
                                                <input type="text" name="Recertification_GenericDialysisCenterContact" id="Recertification_GenericDialysisCenterContact"
                                                    size="25" maxlength="25" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485EliminationStatusInterventions" value=" " />
                                        <input name="Recertification_485EliminationStatusInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient on bladder training program, including timed voiding
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructUTISymptomsPerson" id="Recertification_485InstructUTISymptomsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on signs/symptoms of UTI to report to MD/SN. SN may obtain urinalysis and urine
                                        culture & sensitivity (C&S) test as needed for signs/symptoms of UTI, to include
                                        pain, foul odor, cloudy or blood-tinged urine and fever
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to change foley catheter with
                                        <input type="text" name="Recertification_485ChangeFoleyCatheterWith" id="Recertification_485ChangeFoleyCatheterWith"
                                            size="5" maxlength="5" value="" />
                                        Fr
                                        <input type="text" name="Recertification_485ChangeFoleyCatheterQuantity" id="Recertification_485ChangeFoleyCatheterQuantity"
                                            size="5" maxlength="5" value="" />
                                        cc catheter every
                                        <input type="text" name="Recertification_485ChangeFoleyCatheterFrequency" id="Recertification_485ChangeFoleyCatheterFrequency"
                                            size="5" maxlength="5" value="" />
                                        beginning on
                                        <input type="text" name="Recertification_485ChangeFoleyCatheterDate" id="Recertification_485ChangeFoleyCatheterDate"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to change suprapubic tube with
                                        <input type="text" name="Recertification_485ChangeSuprapubicTubeWith" id="Recertification_485ChangeSuprapubicTubeWith"
                                            size="5" maxlength="5" value="" />
                                        Fr
                                        <input type="text" name="Recertification_485ChangeSuprapubicTubeQuantity" id="Recertification_485ChangeSuprapubicTubeQuantity"
                                            size="5" maxlength="5" value="" />
                                        cc catheter every
                                        <input type="text" name="Recertification_485ChangeSuprapubicTubeFrequency" id="Recertification_485ChangeSuprapubicTubeFrequency"
                                            size="5" maxlength="5" value="" />
                                        beginning on
                                        <input type="text" name="Recertification_485ChangeSuprapubicTubeDate" id="Recertification_485ChangeSuprapubicTubeDate"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to irrigate suprapubic tube with 100-250cc of sterile normal saline as needed
                                        for blockage, leakage
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to irrigate foley with 100-250cc of sterile normal saline as needed for blockage,
                                        leakage
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructProperFoleyPerson" id="Recertification_485InstructProperFoleyPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on proper foley care
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to allow
                                        <input type="text" name="Recertification_485AllowFoleyVisitsAmount" id="Recertification_485AllowFoleyVisitsAmount"
                                            size="15" maxlength="15" value="" />
                                        additional visits for dislodgement, blockage, or leakage of foley or drainage system
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient/caregiver on ostomy management as follows:
                                        <input type="text" name="Recertification_485InstructOstomyManagementDesc" id="Recertification_485InstructOstomyManagementDesc"
                                            size="40" maxlength="50" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to perform ostomy care as follows:
                                        <input type="text" name="Recertification_485PerformOstomyCareDesc" id="Recertification_485PerformOstomyCareDesc"
                                            size="40" maxlength="50" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="11" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to digitally disimpact patient for constipation unrelieved by medications for
                                        <input type="text" name="Recertification_485DigitalDisimpactDays" id="Recertification_485DigitalDisimpactDays"
                                            size="3" maxlength="3" value="" />
                                        days
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="12" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructMeasureIntakeOutputPerson" id="Recertification_485InstructMeasureIntakeOutputPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on measuring and recording intake and output
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="13" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to increase activity to alleviate constipation
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="14" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to administer enema
                                        <input type="text" name="Recertification_485AdministerEnemaType" id="Recertification_485AdministerEnemaType"
                                            size="15" maxlength="15" value="" />
                                        if no bowel movement in
                                        <input type="text" name="Recertification_485AdministerEnemaDays" id="Recertification_485AdministerEnemaDays"
                                            size="3" maxlength="3" value="" />
                                        days
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="15" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructConstipationSignsPerson" id="Recertification_485InstructConstipationSignsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on signs and symptoms of constipation to report to SN or physician
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="16" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructAcidRefluxFoodsPerson" id="Recertification_485InstructAcidRefluxFoodsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on foods that contribute to acid reflux/indigestion
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusInterventions" value="17" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient not to eat 4 hours before bedtime to reduce acid reflux/indigestion
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485EliminationInterventionTemplates" name="Recertification_485EliminationInterventionTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <textarea name="Recertification_485EliminationInterventionComments" id="Recertification_485EliminationInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485EliminationStatusGoals" name=" " />
                                        <input name="Recertification_485EliminationStatusGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Foley will remain patent during this episode and patient will be free of signs and
                                        symptoms of UTI
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Suprapubic tube will remain patent during this episode and patient will be free
                                        of signs and symptoms of UTI
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will be without signs/symptoms of UTI (pain, foul odor, cloudy or blood-tinged
                                        urine and fever) during this episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485IndependentOstomyManagementPerson" id="Recertification_485IndependentOstomyManagementPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be independent in ostomy management by:
                                        <input type="text" name="Recertification_485IndependentOstomyManagementDate" id="Recertification_485IndependentOstomyManagementDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will be free from signs and symptoms of constipation during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeAcidRefluxFoodPerson" id="Recertification_485VerbalizeAcidRefluxFoodPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of foods that contribute to acid reflux/indigestion
                                        by:
                                        <input type="text" name="Recertification_485VerbalizeAcidRefluxFoodDate" id="Recertification_485VerbalizeAcidRefluxFoodDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will verbalize understanding not to eat 4 hours before bedtime to reduce
                                        acid reflux/indigestion by:
                                        <input type="text" name="Recertification_485VerbalizeReduceAcidRefluxDate" id="Recertification_485VerbalizeReduceAcidRefluxDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485EliminationStatusGoals" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will not develop any signs and symptoms of dehydration during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485EliminationGoalTemplates" name="Recertification_485EliminationGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485EliminationGoalComments" id="Recertification_485EliminationGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editNutrition_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationNutritionForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <th>
                                            Nutrition
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>
                                                    <input type="hidden" name="Recertification_GenericNutrition" value=" " />
                                                    <input name="Recertification_GenericNutrition" value="1" type="checkbox" />
                                                    WNL (Within Normal Limits) </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>
                                                    <input name="Recertification_GenericNutrition" value="2" type="checkbox" />
                                                    Dysphagia </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>
                                                    <input name="Recertification_GenericNutrition" value="3" type="checkbox" />
                                                    Decreased Appetite </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul class="columns">
                                                <li class="spacer">
                                                    <input name="Recertification_GenericNutrition" value="4" type="checkbox" />
                                                    Weight Loss/Gain </li>
                                                <li class="spacer">
                                                    <input name="Recertification_GenericNutritionWeightGainLoss" value=" " type="hidden" />
                                                    <input name="Recertification_GenericNutritionWeightGainLoss" value="Loss" type="radio" />
                                                    Loss
                                                    <input name="Recertification_GenericNutritionWeightGainLoss" value="Gain" type="radio" />
                                                    Gain </li>
                                                <li>Amount:
                                                    <input id="Recertification_GenericNutritionWeightAmount" maxlength="10" size="10"
                                                        type="text" name="Recertification_GenericNutritionWeightAmount" />
                                                </li>
                                                <li>in:
                                                    <input id="Recertification_GenericNutritionWeightAmountIn" maxlength="150" size="15"
                                                        type="text" name="Recertification_GenericNutritionWeightAmountIn" />
                                                    <i>(how long)</i> </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>
                                                    <input name="Recertification_GenericNutrition" value="5" type="checkbox" />
                                                    Meals Prepared Appropriately </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul class="columns">
                                                <li class="spacer">
                                                    <input name="Recertification_GenericNutrition" value="6" type="checkbox" />
                                                    Diet </li>
                                                <li>
                                                    <input name="Recertification_GenericNutritionDietAdequate" value=" " type="hidden" />
                                                    <input name="Recertification_GenericNutritionDietAdequate" value="Adequate" type="radio" />
                                                    Adequate
                                                    <input name="Recertification_GenericNutritionDietAdequate" value="Inadequate" type="radio" />
                                                    Inadequate </li>
                                                <li class="littleSpacer"></li>
                                                <li>
                                                    <input type="hidden" name="Recertification_GenericNutritionDiet" name=" " />
                                                    <input name="Recertification_GenericNutritionDiet" value="1" type="checkbox" />
                                                    NG </li>
                                                <li>
                                                    <input name="Recertification_GenericNutritionDiet" value="2" type="checkbox" />
                                                    PEG </li>
                                                <li>
                                                    <input name="Recertification_GenericNutritionDiet" value="3" type="checkbox" />
                                                    Dobhoff </li>
                                                <li>
                                                    <input name="Recertification_GenericNutritionDiet" value="4" type="checkbox" />
                                                    Tube Placement Checked </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericNutrition" value="7" type="checkbox" />
                                                    Residual Checked, </li>
                                                <li>Amount:
                                                    <input id="Recertification_GenericNutritionResidualCheckedAmount" maxlength="5" size="5"
                                                        type="text" name="Recertification_GenericNutritionResidualCheckedAmount" />
                                                    cc </li>
                                            </ul>
                                            <ul class="columns">
                                                <li class="littleSpacer">&nbsp; </li>
                                                <li>
                                                    <input type="hidden" name="Recertification_GenericNutritionResidualProblem" value=" " />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="1" type="checkbox" />
                                                    Throat problems?
                                                    <br />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="2" type="checkbox" />
                                                    Hoarseness?
                                                    <br />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="3" type="checkbox" />
                                                    Sore throat?
                                                    <br />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="4" type="checkbox" />
                                                    Dental problems?
                                                    <br />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="5" type="checkbox" />
                                                    Dentures?
                                                    <br />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="6" type="checkbox" />
                                                    Problems chewing?
                                                    <br />
                                                    <input name="Recertification_GenericNutritionResidualProblem" value="7" type="checkbox" />
                                                    Other: &nbsp;
                                                    <input id="Recertification_GenericNutritionOtherDetails" maxlength="15" size="15"
                                                        type="text" name="Recertification_GenericNutritionOtherDetails" />
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>Comments:<br />
                                                    <textarea id="Recertification_GenericNutritionComments" rows="5" style="width: 99%;"
                                                        name="Recertification_GenericNutritionComments"></textarea>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br />
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th width="60%">
                                        Nutritional Health Screen
                                    </th>
                                    <th width="5%">
                                        Yes
                                    </th>
                                    <th width="35%">
                                        Score
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericNutritionalHealth" value=" " />
                                                <input name="Recertification_GenericNutritionalHealth" value="1" type="checkbox" />
                                                Without reason, has lost more than 10 lbs, in the last 3 months </li>
                                        </ul>
                                    </td>
                                    <td>
                                        15
                                    </td>
                                    <td rowspan="12">
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericGoodNutritionScore" value="" />
                                                <input name="Recertification_GenericGoodNutritionScore" value="1" type="checkbox" />
                                                Good Nutritional Status (Score 0 - 25) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGoodNutritionScore" value="2" type="checkbox" />
                                                Moderate Nutritional Risk (Score 25 - 55) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericGoodNutritionScore" value="3" type="checkbox" />
                                                High Nutritional Risk (Score 55 - 100)<br />
                                                <br />
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Nutritional Status Comments:<br />
                                                <textarea id="Recertification_GenericNutritionalStatusComments" rows="10" cols="50"
                                                    name="Recertification_GenericNutritionalStatusComments"></textarea>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <br />
                                                <br />
                                                <input type="hidden" name="Recertification_GenericNutritionDiffect" value=" " />
                                                <input name="Recertification_GenericNutritionDiffect" value="1" type="checkbox" />
                                                Non-compliant with prescribed diet </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionDiffect" value="2" type="checkbox" />
                                                Over/under weight by 10%<br />
                                                <br />
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Meals prepared by:<br />
                                                <textarea id="Recertification_GenericMealsPreparedBy" rows="10" cols="50" name="Recertification_GenericMealsPreparedBy"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="2" type="checkbox" />
                                                Has an illness or condition that made pt change the type and/or amount of food eaten
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="3" type="checkbox" />
                                                Has open decubitus, ulcer, burn or wound </li>
                                        </ul>
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="4" type="checkbox" />
                                                Eats fewer than 2 meals a day </li>
                                        </ul>
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="5" type="checkbox" />
                                                Has a tooth/mouth problem that makes it hard to eat </li>
                                        </ul>
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="6" type="checkbox" />
                                                Has 3 or more drinks of beer, liquor or wine almost every day </li>
                                        </ul>
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="7" type="checkbox" />
                                                Does not always have enough money to buy foods needed </li>
                                        </ul>
                                    </td>
                                    <td>
                                        10
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="8" type="checkbox" />
                                                Eats few fruits or vegetables, or milk products </li>
                                        </ul>
                                    </td>
                                    <td>
                                        5
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="9" type="checkbox" />
                                                Eats alone most of the time </li>
                                        </ul>
                                    </td>
                                    <td>
                                        5
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="10" type="checkbox" />
                                                Takes 3 or more prescribed or OTC medications a day </li>
                                        </ul>
                                    </td>
                                    <td>
                                        5
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="11" type="checkbox" />
                                                Is not always physically able to cook and/or feed self and has no caregiver to assist
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        5
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericNutritionalHealth" value="12" type="checkbox" />
                                                Frequently has diarrhea or constipation </li>
                                        </ul>
                                    </td>
                                    <td>
                                        5
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <th colspan="2">
                                            Enter Physician's Orders or Diet Requirements (locator #16)
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul class="columns">
                                                <li>
                                                    <input type="hidden" name="Recertification_GenericDietRequirements" value=" " />
                                                    <input name="Recertification_GenericDietRequirements" value="1" type="checkbox" />
                                                </li>
                                                <li>
                                                    <input id="Recertification_GenericNutritionSodiumAmount" maxlength="5" size="5" type="text"
                                                        name="Recertification_GenericNutritionSodiumAmount" />
                                                </li>
                                                <li>Sodium </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="2" type="checkbox" />
                                                </li>
                                                <li>No Added Salt </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="3" type="checkbox" />
                                                </li>
                                                <li>
                                                    <input id="Recertification_GenericCalorieADADietAmount" maxlength="5" size="5" type="text"
                                                        name="Recertification_GenericCalorieADADietAmount" />
                                                </li>
                                                <li>Calorie ADA Diet </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="4" type="checkbox" />
                                                </li>
                                                <li>Regular </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="5" type="checkbox" />
                                                </li>
                                                <li>High Protein </li>
                                                <li>
                                                    <input id="Recertification_GenericNutritionHighProteinAmount" maxlength="5" size="5"
                                                        type="text" name="Recertification_GenericNutritionHighProteinAmount" />
                                                </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="6" type="checkbox" />
                                                </li>
                                                <li>Low Protein </li>
                                                <li>
                                                    <input id="Recertification_GenericNutritionLowProteinAmount" maxlength="5" size="5"
                                                        type="text" name="Recertification_GenericNutritionLowProteinAmount" />
                                                </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="7" type="checkbox" />
                                                </li>
                                                <li>Carbohydrate </li>
                                                <li>
                                                    <input name="Recertification_GenericCarbohydrateLevel" value=" " type="hidden" />
                                                    <input name="Recertification_GenericCarbohydrateLevel" value="1" type="radio" />
                                                    Low </li>
                                                <li>
                                                    <input name="Recertification_GenericCarbohydrateLevel" value="2" type="radio" />
                                                    High </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="8" type="checkbox" />
                                                </li>
                                                <li>Mechanical Soft </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="9" type="checkbox" />
                                                </li>
                                                <li>High Fiber </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="10" type="checkbox" />
                                                </li>
                                                <li>Supplement: </li>
                                                <li>
                                                    <input id="Recertification_GenericNutritionSupplementType" maxlength="15" size="15"
                                                        type="text" name="Recertification_GenericNutritionSupplementType" />
                                                </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="11" type="checkbox" />
                                                </li>
                                                <li>Renal Diet </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="12" type="checkbox" />
                                                </li>
                                                <li>Coumadin Diet </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="13" type="checkbox" />
                                                </li>
                                                <li>Fluid Restriction </li>
                                                <li>
                                                    <input id="Recertification_GenericFluidRestrictionAmount" maxlength="15" size="15"
                                                        type="text" name="Recertification_GenericFluidRestrictionAmount" />
                                                    cc/24 hours </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="14" type="checkbox" />
                                                </li>
                                                <li>Other: </li>
                                                <li>
                                                    <input id="Recertification_GenericPhysicianDietOtherName" maxlength="15" size="15"
                                                        type="text" name="Recertification_GenericPhysicianDietOtherName" />
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="15" type="checkbox" />
                                                </li>
                                                <li>No Concentrated Sweets </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="16" type="checkbox" />
                                                </li>
                                                <li>Heart Healthy </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="17" type="checkbox" />
                                                </li>
                                                <li>Low Cholesterol </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="18" type="checkbox" />
                                                </li>
                                                <li>Low Fat </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="19" type="checkbox" />
                                                </li>
                                                <li>Enteral Nutrition </li>
                                                <li>
                                                    <input id="Recertification_GenericEnteralNutritionDesc" maxlength="10" size="10"
                                                        type="text" name="Recertification_GenericEnteralNutritionDesc" />
                                                    (formula) </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>&nbsp; </li>
                                                <li>Amount </li>
                                                <li>
                                                    <input id="Recertification_GenericEnteralNutritionAmount" maxlength="5" size="5"
                                                        type="text" name="Recertification_GenericEnteralNutritionAmount" />
                                                    cc/day via </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>&nbsp; </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value=" " type="hidden" />
                                                    <input name="Recertification_GenericEnteralNutrition" value="1" type="checkbox" />
                                                </li>
                                                <li>
                                                    <input id="Recertification_GenericEnteralNutritionPumpType" maxlength="15" size="15"
                                                        type="text" name="Recertification_GenericEnteralNutritionPumpType" />
                                                    Pump </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value="2" type="checkbox" />
                                                    Gravity </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>&nbsp; </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value="3" type="checkbox" />
                                                    PEG </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value="4" type="checkbox" />
                                                    NG </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value="5" type="checkbox" />
                                                    Dobhoff </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>&nbsp; </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value="6" type="checkbox" />
                                                    Continuous </li>
                                                <li>
                                                    <input name="Recertification_GenericEnteralNutrition" value="7" type="checkbox" />
                                                    Bolus </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>
                                                    <input name="Recertification_GenericDietRequirements" value="20" type="checkbox" />
                                                </li>
                                                <li>TPN </li>
                                                <li>
                                                    <input id="Recertification_GenericTPNAmount" maxlength="15" size="15" type="text"
                                                        name="Recertification_GenericTPNAmount" />
                                                    @cc/hr </li>
                                            </ul>
                                            <ul class="columns">
                                                <li>&nbsp; </li>
                                                <li>via </li>
                                                <li>
                                                    <input id="Recertification_GenericTPNVia" maxlength="15" size="15" type="text" name="Recertification_GenericTPNVia" />
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br />
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <th colspan="2">
                                            Interventions
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input type="hidden" name="Recertification_485NutritionInterventions" value=" " />
                                            <input name="Recertification_485NutritionInterventions" value="1" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct
                                            <select id="Recertification_485InstructOnDietPerson" name="Recertification_485InstructOnDietPerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            on
                                            <input id="Recertification_485InstructOnDietDesc" maxlength="200" size="100" type="text"
                                                name="Recertification_485InstructOnDietDesc" />
                                            diet
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="2" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to assess patient for diet compliance
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="3" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct the
                                            <select id="Recertification_485InstructKeepDietLogPerson" name="Recertification_485InstructKeepDietLogPerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            to keep a diet log
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="4" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct the
                                            <select id="Recertification_485InstructPromoteOralIntakePerson" name="Recertification_485InstructPromoteOralIntakePerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            on methods to promote oral intake
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="5" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct the
                                            <select id="Recertification_485InstructParenteralNutritionPerson" name="Recertification_485InstructParenteralNutritionPerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            on parenteral nutrition and the care/use of equipment, to include:
                                            <input id="Recertification_485InstructParenteralNutritionInclude" maxlength="40"
                                                size="40" type="text" name="Recertification_485InstructParenteralNutritionInclude" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="6" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct the
                                            <select id="Recertification_485InstructEnteralNutritionPerson" name="Recertification_485InstructEnteralNutritionPerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            on enteral nutrition and the care/use of equipment, to include
                                            <input id="Recertification_485InstructEnteralNutritionInclude" maxlength="40" size="40"
                                                type="text" name="Recertification_485InstructEnteralNutritionInclude" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="7" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct the
                                            <select id="Recertification_485InstructCareOfTubePerson" name="Recertification_485InstructCareOfTubePerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            on proper care of
                                            <input id="Recertification_485InstructCareOfTubeDesc" maxlength="20" size="20" type="text"
                                                name="Recertification_485InstructCareOfTubeDesc">
                                            tube
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="8" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to change
                                            <input id="Recertification_485ChangeTubeEveryType" maxlength="10" size="10" type="text"
                                                name="Recertification_485ChangeTubeEveryType" />
                                            tube every
                                            <input id="Recertification_485ChangeTubeEveryFreq" maxlength="20" size="10" type="text"
                                                name="Recertification_485ChangeTubeEveryFreq" />
                                            beginning
                                            <input id="Recertification_485ChangeTubeEveryDate" maxlength="20" size="10" type="text"
                                                name="Recertification_485ChangeTubeEveryDate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="9" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to irrigate
                                            <input id="Recertification_485IrrigateTubeWithType" maxlength="10" size="10" type="text"
                                                name="Recertification_485IrrigateTubeWithType" />
                                            tube with
                                            <input id="Recertification_485IrrigateTubeWithAmount" maxlength="20" size="10" type="text"
                                                name="Recertification_485IrrigateTubeWithAmount" />
                                            cc of
                                            <input id="Recertification_485IrrigateTubeWithDesc" maxlength="20" size="10" type="text"
                                                name="Recertification_485IrrigateTubeWithDesc" />
                                            <input name="Recertification_485IrrigateTubeWithFreq" value=" " type="hidden" />
                                            <input name="Recertification_485IrrigateTubeWithFreq" value="1" type="radio" />
                                            every
                                            <input id="Recertification_485IrrigateTubeWithEvery" maxlength="20" size="10" type="text"
                                                name="Recertification_485IrrigateTubeWithEvery" />
                                            <input name="Recertification_485IrrigateTubeWithFreq" value="0" type="radio" />
                                            as needed for
                                            <input id="Recertification_485IrrigateTubeWithAsNeeded" maxlength="20" size="10"
                                                type="text" name="Recertification_485IrrigateTubeWithAsNeeded" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionInterventions" value="10" type="checkbox" />
                                        </td>
                                        <td>
                                            SN to instruct the
                                            <select id="Recertification_485InstructFreeWaterPerson" name="Recertification_485InstructFreeWaterPerson">
                                                <option selected="selected" value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            to give
                                            <input id="Recertification_485InstructFreeWaterAmount" maxlength="10" size="10" type="text"
                                                name="Recertification_485InstructFreeWaterAmount" />
                                            cc of free water every
                                            <input id="Recertification_485InstructFreeWaterEvery" maxlength="10" size="10" type="text"
                                                name="Recertification_485InstructFreeWaterEvery" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Additional Orders: &nbsp;
                                            <select id="Recertification_485NutritionOrderTemplates" name="Recertification_485NutritionOrderTemplates">
                                                <option selected="selected" value="0">&nbsp;</option>
                                                <option value="-2">-----------</option>
                                                <option value="-1">Erase</option>
                                            </select>
                                            <br />
                                            <textarea id="Recertification_485NutritionComments" rows="5" style="width: 99%;"
                                                name="Recertification_485NutritionComments"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br />
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <th colspan="2">
                                            Goals
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input type="hidden" name="Recertification_485NutritionGoals" value=" " />
                                            <input name="Recertification_485NutritionGoals" value="1" type="checkbox" />
                                        </td>
                                        <td>
                                            Patient will maintain
                                            <input id="Recertification_485MaintainDietComplianceType" maxlength="200" size="100"
                                                type="text" name="Recertification_485MaintainDietComplianceType" />
                                            diet compliance during the episode
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionGoals" value="2" type="checkbox" />
                                        </td>
                                        <td>
                                            The
                                            <select id="Recertification_485DemonstrateDietCompliancePerson" name="Recertification_485DemonstrateDietCompliancePerson">
                                                <option selected value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            will demonstrate compliance with maintaining a diet log during the episode
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionGoals" value="3" type="checkbox" />
                                        </td>
                                        <td>
                                            The
                                            <select id="Recertification_485DemonstrateEnteralNutritionPerson" name="Recertification_485DemonstrateEnteralNutritionPerson">
                                                <option selected="selected" value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            will demonstrate proper care/use of enteral nutrition equipment by
                                            <input id="Recertification_485DemonstrateEnteralNutritionDate" maxlength="20" size="10"
                                                type="text" name="Recertification_485DemonstrateEnteralNutritionDate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionGoals" value="4" type="checkbox" />
                                        </td>
                                        <td>
                                            The
                                            <select id="Recertification_485DemonstrateParenteralNutritionPerson" name="Recertification_485DemonstrateParenteralNutritionPerson">
                                                <option selected="selected" value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            will demonstrate proper care/use of parenteral nutrition equipment by
                                            <input id="Recertification_485DemonstrateParenteralNutritionDate" maxlength="20"
                                                size="10" type="text" name="Recertification_485DemonstrateParenteralNutritionDate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15px">
                                            <input name="Recertification_485NutritionGoals" value="5" type="checkbox" />
                                        </td>
                                        <td>
                                            The
                                            <select id="Recertification_485DemonstrateCareOfTubePerson" name="Recertification_485DemonstrateCareOfTubePerson">
                                                <option selected="selected" value="Patient/Caregiver">Patient/Caregiver</option>
                                                <option value="Patient">Patient</option>
                                                <option value="Caregiver">Caregiver</option>
                                            </select>
                                            will demonstrate proper care of
                                            <input id="Recertification_485DemonstrateCareOfTubeType" maxlength="10" size="10"
                                                type="text" name="Recertification_485DemonstrateCareOfTubeType" />
                                            tube by
                                            <input id="Recertification_485DemonstrateCareOfTubeDate" maxlength="20" size="10"
                                                type="text" name="Recertification_485DemonstrateCareOfTubeDate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Additional Goals: &nbsp;
                                            <select id="Recertification_485NutritionGoalTemplates" name="Recertification_485NutritionGoalTemplates">
                                                <option selected value="0">&nbsp;</option>
                                                <option value="1467">Goals</option>
                                                <option value="-2">-----------</option>
                                                <option value="-1">Erase</option>
                                            </select>
                                            <br />
                                            <textarea id="Recertification_485NutritionGoalComments" rows="5" style="width: 99%;"
                                                name="Recertification_485NutritionGoalComments"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editBehaviourialstatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationBehaviourialForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="2">
                                        Neuro/Emotional/Behavioral Status (locator #19)
                                    </th>
                                </tr>
                                <tr>
                                    <th width="50%">
                                        Neurological
                                    </th>
                                    <th width="50%">
                                        Psychosocial
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>Oriented to: </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>&nbsp; </li>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericNeurologicalOriented" value=" " />
                                                <input name="Recertification_GenericNeurologicalOriented" value="1" type="checkbox" />
                                                Person </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericNeurologicalOriented" value="2" type="checkbox" />
                                                Place </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericNeurologicalOriented" value="3" type="checkbox" />
                                                Time </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input type="hidden" name="Recertification_GenericNeurologicalStatus" value=" " />
                                                <input name="Recertification_GenericNeurologicalStatus" value="1" type="checkbox" />
                                                Disoriented </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericNeurologicalStatus" value="2" type="checkbox" />
                                                Forgetful </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericNeurologicalStatus" value="3" type="checkbox" />
                                                PERRL </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericNeurologicalStatus" value="4" type="checkbox" />
                                                Seizures </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericNeurologicalStatus" value="5" type="checkbox" />
                                                Tremors </li>
                                            <li class="span">Location(s)
                                                <input type="text" name="Recertification_GenericNeurologicalTremorsLocation" id="Recertification_GenericNeurologicalTremorsLocation"
                                                    size="30" maxlength="30" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input type="hidden" name="Recertification_GenericPsychosocial" value="" />
                                                <input name="Recertification_GenericPsychosocial" value="1" type="checkbox" />
                                                WNL (Within Normal Limits) </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="2" type="checkbox" />
                                                Poor Home Environment </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="3" type="checkbox" />
                                                Poor Coping Skills </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="4" type="checkbox" />
                                                Agitated </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="5" type="checkbox" />
                                                Depressed Mood </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="6" type="checkbox" />
                                                Impaired Decision Making </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="7" type="checkbox" />
                                                Demonstrated/Expressed Anxiety </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="8" type="checkbox" />
                                                Inappropriate Behavior </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericPsychosocial" value="9" type="checkbox" />
                                                Irritability </li>
                                        </ul>
                                        <tr>
                                            <td colspan="2">
                                                <ul class="columns">
                                                    <li>Comments:<br />
                                                        <textarea name="Recertification_GenericNeuroEmoBehaviorComments" id="Recertification_GenericNeuroEmoBehaviorComments"
                                                            rows="5" cols="70"></textarea>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485BehaviorInterventions" value=" " />
                                        <input name="Recertification_485BehaviorInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        <strong>*SN TO NOTIFY PHYSICIAN THIS PATIENT WAS SCREENED FOR DEPRESSION USING THE PHQ-2
                                            SCALE AND MEETS CRITERIA FOR FURTHER EVALUATION FOR DEPRESSION</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess for changes in neurological status every visit
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess patient's communication skills every visit
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructSeizurePrecautionPerson" id="Recertification_485InstructSeizurePrecautionPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on seizure precautions
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct caregiver on orientation techniques to use when patient becomes disoriented
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        MSW:
                                        <input name="Recertification_485MSWProvideServiceNumberVisits" value=" " type="hidden" />
                                        <input name="Recertification_485MSWProvideServiceNumberVisits" value="1" type="radio" />
                                        1-2 OR
                                        <input name="Recertification_485MSWProvideServiceNumberVisits" value="0" type="radio" />
                                        <input type="text" name="Recertification_485MSWProvideServiceVisitAmount" id="Recertification_485MSWProvideServiceVisitAmount"
                                            size="5" maxlength="5" value="" />
                                        visits, every 60 days for provider services
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        MSW:
                                        <input name="Recertification_485MSWLongTermPlanningVisits" value=" " type="hidden" />
                                        <input name="Recertification_485MSWLongTermPlanningVisits" value="1" type="radio" />
                                        1-2 OR
                                        <input name="Recertification_485MSWLongTermPlanningVisits" value="0" type="radio" />
                                        <input type="text" name="Recertification_485MSWLongTermPlanningVisitAmount" id="Recertification_485MSWLongTermPlanningVisitAmount"
                                            size="5" maxlength="5" value="" />
                                        visits, every 60 days for long term planning
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        MSW:
                                        <input name="Recertification_485MSWCommunityAssistanceVisits" value=" " type="hidden" />
                                        <input name="Recertification_485MSWCommunityAssistanceVisits" value="1" type="radio" />
                                        1-2 OR
                                        <input name="Recertification_485MSWCommunityAssistanceVisits" value="0" type="radio" />
                                        <input type="text" name="Recertification_485MSWCommunityAssistanceVisitAmount" id="Recertification_485MSWCommunityAssistanceVisitAmount"
                                            size="5" maxlength="5" value="" />
                                        visits, every 60 days for community resource assistance
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="100%">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485BehaviorOrderTemplates" name="Recertification_485BehaviorOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485BehaviorComments" id="Recertification_485BehaviorComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485BehaviorGoals" value=" " />
                                        <input name="Recertification_485BehaviorGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will remain free from increased confusion during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeSeizurePrecautionsPerson" id="Recertification_485VerbalizeSeizurePrecautionsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of seizure precautions
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Caregiver will verbalize understanding of proper orientation techniques to use when
                                        patient becomes disoriented
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485BehaviorGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's community resource needs will be met with assistance of social worker
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="100%">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485BehaviorGoalTemplates" name="Recertification_485BehaviorGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485BehaviorGoalComments" id="Recertification_485BehaviorGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editAdl_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationADLForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="4">
                                        Activities Permitted (locator #18.B)
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_485ActivitiesPermitted" value="" />
                                                <input name="Recertification_485ActivitiesPermitted" value="1" type="checkbox" />
                                                Complete bed rest </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="2" type="checkbox" />
                                                Up as tolerated </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="3" type="checkbox" />
                                                Exercise prescribed </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="4" type="checkbox" />Independent
                                                at home</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="5" type="checkbox" />
                                                Cane </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="6" type="checkbox" />
                                                Walker </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="7" type="checkbox" />
                                                Bed rest with BRP </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="8" type="checkbox" />
                                                Transfer bed-chair </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="9" type="checkbox" />Partial
                                                weight bearing</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="10" type="checkbox" />
                                                Crutches </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_485ActivitiesPermitted" value="11" type="checkbox" />
                                                Wheelchair </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>Other (specify):
                                                <input type="text" name="Recertification_485ActivitiesPermittedOther" id="Recertification_485ActivitiesPermittedOther"
                                                    size="20" maxlength="20" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Musculoskeletal
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericMusculoskeletal" value="" />
                                                <input name="Recertification_GenericMusculoskeletal" value="1" type="checkbox" />
                                                WNL (Within Normal Limits) </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericMusculoskeletal" value="2" type="checkbox" />
                                                Weakness </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericMusculoskeletal" value="3" type="checkbox" />
                                                Ambulation Difficulty </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericMusculoskeletal" value="4" type="checkbox" />
                                                Limited Mobility/ROM </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericLimitedMobilityLocation" id="Recertification_GenericLimitedMobilityLocation"
                                                    size="15" maxlength="15" value="" />
                                                <i>(location)</i> </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericMusculoskeletal" value="5" type="checkbox" />
                                                Joint Pain/Stiffness </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericJointPainLocation" id="Recertification_GenericJointPainLocation"
                                                    size="15" maxlength="15" value="" />
                                                <i>(location)</i> </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericMusculoskeletal" value="6" type="checkbox" />
                                                Poor Balance </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericMusculoskeletal" value="7" type="checkbox" />
                                                Grip Strength </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp;</li>
                                            <li>
                                                <input name="Recertification_GenericGripStrengthEqual" value="" type="hidden" />
                                                <input name="Recertification_GenericGripStrengthEqual" value="1" type="radio" />
                                                Equal
                                                <br />
                                                <input name="Recertification_GenericGripStrengthEqual" value="2" type="radio" />
                                                Unequal
                                                <br />
                                                <input type="text" name="Recertification_GenericGripStrengthDesc" id="Recertification_GenericGripStrengthDesc"
                                                    size="10" maxlength="10" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="hidden" name="Recertification_GenericBoundType" value="" />
                                                <input name="Recertification_GenericBoundType" value="1" type="checkbox" />
                                                Bedbound </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericBoundType" value="2" type="checkbox" />
                                                Chairbound </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericBoundType" value="3" type="checkbox" />
                                                Contracture: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericContractureLocation" id="Recertification_GenericContractureLocation"
                                                    size="15" maxlength="15" value="" />
                                                <i>(location)</i> </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">
                                                <input name="Recertification_GenericBoundType" value="4" type="checkbox" />
                                                Paralysis: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericParalysisLocation" id="Recertification_GenericParalysisLocation"
                                                    size="15" maxlength="15" value="" />
                                                <i>(location)</i> </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp; </li>
                                            <li>
                                                <input name="Recertification_GenericParalysisDominant" value="" type="hidden" />
                                                <input name="Recertification_GenericParalysisDominant" value="1" type="radio" />
                                                Dominant
                                                <br />
                                                <input name="Recertification_GenericParalysisDominant" value="2" type="radio" />
                                                Nondominant
                                                <br />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_GenericBoundType" value="5" type="checkbox" />
                                                Assistive Device: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericAssistiveDeviceType" id="Recertification_GenericAssistiveDeviceType"
                                                    size="15" maxlength="15" value="" />
                                                <i>(type)</i> </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <ul>
                                            <li>Comments:<br />
                                                <textarea name="Recertification_GenericMusculoskeletalComments" id="Recertification_GenericMusculoskeletalComments"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids)
                                        including undergarments, pullovers, front-opening shirts and blouses, managing zippers,
                                        buttons, and snaps:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1810CurrentAbilityToDressUpper" type="hidden" value=" " />
                                    <input name="Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="00" />&nbsp;0
                                    - Able to get clothes out of closets and drawers, put them on and remove them from
                                    the upper body without assistance.<br />
                                    <input name="Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="01" />&nbsp;1
                                    - Able to dress upper body without assistance if clothing is laid out or handed
                                    to the patient.<br />
                                    <input name="Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="02" />&nbsp;2
                                    - Someone must help the patient put on upper body clothing.<br />
                                    <input name="Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="03" />&nbsp;3
                                    - Patient depends entirely upon another person to dress the upper body.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids)
                                        including undergarments, slacks, socks or nylons, shoes:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1820CurrentAbilityToDressLower" type="hidden" value=" " />
                                    <input name="Recertification_M1820CurrentAbilityToDressLower" type="radio" value="00" />&nbsp;0
                                    - Able to obtain, put on, and remove clothing and shoes without assistance.<br />
                                    <input name="Recertification_M1820CurrentAbilityToDressLower" type="radio" value="01" />&nbsp;1
                                    - Able to dress lower body without assistance if clothing and shoes are laid out
                                    or handed to the patient.<br />
                                    <input name="Recertification_M1820CurrentAbilityToDressLower" type="radio" value="02" />&nbsp;2
                                    - Someone must help the patient put on undergarments, slacks, socks or nylons, and
                                    shoes.<br />
                                    <input name="Recertification_M1820CurrentAbilityToDressLower" type="radio" value="03" />&nbsp;3
                                    - Patient depends entirely upon another person to dress lower body.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing
                                        face, washing hands, and shampooing hair).
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="hidden"
                                        value=" " />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="00" />&nbsp;0
                                    - Able to bathe self in shower or tub independently, including getting in and out
                                    of tub/shower.<br />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="01" />&nbsp;1
                                    - With the use of devices, is able to bathe self in shower or tub independently,
                                    including getting in and out of the tub/shower.<br />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="02" />&nbsp;2
                                    - Able to bathe in shower or tub with the intermittent assistance of another person:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) for intermittent supervision or encouragement
                                    or reminders, OR
                                    <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) to get in and out of the shower or
                                    tub, OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) for washing difficult to reach areas.<br />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="03" />&nbsp;3
                                    - Able to participate in bathing self in shower or tub, but requires presence of
                                    another person throughout the bath for assistance or supervision.<br />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="04" />&nbsp;4
                                    - Unable to use the shower or tub, but able to bathe self independently with or
                                    without the use of devices at the sink, in chair, or on commode.<br />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="05" />&nbsp;5
                                    - Unable to use the shower or tub, but able to participate in bathing self in bed,
                                    at the sink, in bedside chair, or on commode, with the assistance or supervision
                                    of another person throughout the bath.<br />
                                    <input name="Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio" value="06" />&nbsp;6
                                    - Unable to participate effectively in bathing and is bathed totally by another
                                    person.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside
                                        commode safely and transfer on and off toilet/commode.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1840ToiletTransferring" type="hidden" value=" " />
                                    <input name="Recertification_M1840ToiletTransferring" type="radio" value="00" />&nbsp;0
                                    - Able to get to and from the toilet and transfer independently with or without
                                    a device.<br />
                                    <input name="Recertification_M1840ToiletTransferring" type="radio" value="01" />&nbsp;1
                                    - When reminded, assisted, or supervised by another person, able to get to and from
                                    the toilet and transfer.<br />
                                    <input name="Recertification_M1840ToiletTransferring" type="radio" value="02" />&nbsp;2
                                    - Unable to get to and from the toilet but is able to use a bedside commode (with
                                    or without assistance).<br />
                                    <input name="Recertification_M1840ToiletTransferring" type="radio" value="03" />&nbsp;3
                                    - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal
                                    independently.<br />
                                    <input name="Recertification_M1840ToiletTransferring" type="radio" value="04" />&nbsp;4
                                    - Is totally dependent in toileting.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1850) Transferring: Current ability to move safely from bed to chair, or ability
                                        to turn and position self in bed if patient is bedfast.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1850Transferring" type="hidden" value=" " />
                                    <input name="Recertification_M1850Transferring" type="radio" value="00" />&nbsp;0
                                    - Able to independently transfer.<br />
                                    <input name="Recertification_M1850Transferring" type="radio" value="01" />&nbsp;1
                                    - Able to transfer with minimal human assistance or with use of an assistive device.<br />
                                    <input name="Recertification_M1850Transferring" type="radio" value="02" />&nbsp;2
                                    - Able to bear weight and pivot during the transfer process but unable to transfer
                                    self.<br />
                                    <input name="Recertification_M1850Transferring" type="radio" value="03" />&nbsp;3
                                    - Unable to transfer self and is unable to bear weight or pivot when transferred
                                    by another person.<br />
                                    <input name="Recertification_M1850Transferring" type="radio" value="04" />&nbsp;4
                                    - Bedfast, unable to transfer but is able to turn and position self in bed.<br />
                                    <input name="Recertification_M1850Transferring" type="radio" value="05" />&nbsp;5
                                    - Bedfast, unable to transfer and is unable to turn and position self.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing
                                        position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M1860AmbulationLocomotion" type="hidden" value=" " />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="00" />&nbsp;0
                                    - Able to independently walk on even and uneven surfaces and negotiate stairs with
                                    or without railings (i.e., needs no human assistance or assistive device).<br />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="01" />&nbsp;1
                                    - With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able
                                    to independently walk on even and uneven surfaces and negotiate stairs with or without
                                    railings.<br />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="02" />&nbsp;2
                                    - Requires use of a two-handed device (e.g., walker or crutches) to walk alone on
                                    a level surface and/or requires human supervision or assistance to negotiate stairs
                                    or steps or uneven surfaces.<br />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="03" />&nbsp;3
                                    - Able to walk only with the supervision or assistance of another person at all
                                    times.<br />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="04" />&nbsp;4
                                    - Chairfast, unable to ambulate but is able to wheel self independently.<br />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="05" />&nbsp;5
                                    - Chairfast, unable to ambulate and is unable to wheel self.<br />
                                    <input name="Recertification_M1860AmbulationLocomotion" type="radio" value="06" />&nbsp;6
                                    - Bedfast, unable to ambulate or be up in a chair.
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485NursingInterventions" value=" " />
                                        <input name="Recertification_485NursingInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Physical therapy
                                        <input type="text" name="Recertification_485PhysicalTherapyFreq" id="Recertification_485PhysicalTherapyFreq"
                                            size="10" maxlength="20" value="" />
                                        (freq) to evaluate week of
                                        <input type="text" name="Recertification_485PhysicalTherapyDate" id="Recertification_485PhysicalTherapyDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485NursingInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Occupational therapy
                                        <input type="text" name="Recertification_485OccupationalTherapyFreq" id="Recertification_485OccupationalTherapyFreq"
                                            size="10" maxlength="20" value="" />
                                        (freq) to evaluate week of
                                        <input type="text" name="Recertification_485OccupationalTherapyDate" id="Recertification_485OccupationalTherapyDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485NursingInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Home Health Aide (freq)
                                        <input type="text" name="Recertification_485HomeHealthAideFreq" id="Recertification_485HomeHealthAideFreq"
                                            size="10" maxlength="10" value="" />
                                        for assistance with ADLs/IADLs
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485NursingInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess for patient adherence to appropriate activity levels
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485NursingInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess patient's compliance with home exercise program
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485NursingInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructRomExcercisePerson" id="Recertification_485InstructRomExcercisePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on proper ROM exercises and body alignment techniques
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485NursingInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to perform circulatory checks and cast care every visit
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485ADLOrderTemplates" name="Recertification_485ADLOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485ADLComments" id="Recertification_485ADLComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485NursingGoals" id="Recertification_485EstablishHomeExercisePT" />
                                        <input name="Recertification_485NursingGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Home exercise program will be established by physical therapist
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NursingGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        Home exercise program will be established by occupational therapist
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NursingGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's mobility will be improved with assistance of physical therapist
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NursingGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485DemonstrateROMExcercisePerson" id="Recertification_485DemonstrateROMExcercisePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        &nbsp; will demonstrate proper ROM exercise and body alignment techniques
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NursingGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will remain free from impaired circulation related to cast or other orthotic
                                        device
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NursingGoals" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient's ADL/IADL needs will be met with assistance of home health aide
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485ADLGoalTemplates" name="Recertification_485ADLGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485ADLGoalComments" id="Recertification_485ADLGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="3">
                                        Fall Assessment
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <i>One point is assessed for each <strong>yes</strong> selection</i>
                                    </td>
                                    <td width="15px">
                                        Yes
                                    </td>
                                    <td width="15px">
                                        No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Age 65+</strong>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericAge65Plus" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericAge65Plus" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericAge65Plus" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Diagnosis (3 or more co-existing)</strong><br />
                                        <i>Assess for hypotension.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericHypotensionDiagnosis" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericHypotensionDiagnosis" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericHypotensionDiagnosis" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Prior history of falls within 3 months</strong><br />
                                        <i>Fall definition: "An unintentional change in position resulting in coming to
                                        rest on the ground or at a lower level."
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericPriorFalls" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericPriorFalls" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericPriorFalls" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Incontinence</strong><br />
                                        <i>Inability to make it to the bathroom or commode in timely manner. Includes frequency,
                                            urgency, and/or nocturia.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericFallIncontinence" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericFallIncontinence" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericFallIncontinence" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Visual impairment</strong><br />
                                        <i>Includes macular degeneration, diabetic retinopathies, visual field loss, age related
                                            changes, decline in visual acuity, accommodation, glare tolerance, depth perception,
                                            and night vision or not wearing prescribed glasses or having the correct prescription.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericVisualImpairment" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericVisualImpairment" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericVisualImpairment" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Impaired functional mobility</strong><br />
                                        <i>May include patients who need help with IADLs or ADLs or have gait or transfer problems,
                                            arthritis, pain, fear of falling, foot problems, impaired sensation, impaired coordination
                                            or improper use of assistive devices.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericImpairedFunctionalMobility" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericImpairedFunctionalMobility" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericImpairedFunctionalMobility" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Environmental hazards</strong><br />
                                        <i>May include poor illumination, equipment tubing, inappropriate footwear, pets, hard
                                            to reach items, floor surfaces that are uneven or cluttered, or outdoor entry and
                                            exits.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericEnvHazards" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericEnvHazards" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericEnvHazards" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Poly Pharmacy (4 or more prescriptions)</strong><br />
                                        <i>Drugs highly associated with fall risk include but are not limited to, sedatives,
                                            anti-depressants, tranquilizers, narcotics, antihypertensives, cardiac meds, corticosteroids,
                                            anti-anxiety drugs, anticholinergic drugs, and hypoglycemic drugs.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericPolyPharmacy" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericPolyPharmacy" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericPolyPharmacy" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Pain affecting level of function</strong><br />
                                        <i>Pain often affects an individual's desire or ability to move or pain can be a factor
                                            in depression or compliance with safety recommendations.</i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericPainAffectingFunction" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericPainAffectingFunction" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericPainAffectingFunction" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Cognitive impairment</strong><br />
                                        <i>Could include patients with dementia, Alzheimer's or stroke patients or patients
                                            who are confused, use poor judgment, have decreased comprehension, impulsivity,
                                            memory deficits. Consider patient's ability to adhere to the plan of care. </i>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input name="Recertification_GenericCognitiveImpairment" value="" type="hidden" />
                                                <input type="radio" name="Recertification_GenericCognitiveImpairment" value="1" />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <input type="radio" name="Recertification_GenericCognitiveImpairment" value="0" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="bold" align="right">
                                            Total:</div>
                                    </td>
                                    <td colspan="2">
                                        <div id="box3">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        A score of 4 or more is considered at risk for falling
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485InstructInterventions" value=" " />
                                        <input name="Recertification_485InstructInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to wear proper footwear when ambulating
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to use prescribed assistive device when ambulating
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to change positions slowly
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructRemoveRugsPerson" id="Recertification_485InstructRemoveRugsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to remove throw rugs or use double-sided tape to secure rug in place
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructRemoveClutterPerson" id="Recertification_485InstructRemoveClutterPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to remove clutter from patient's path such as clothes, books, shoes, electrical
                                        cords, or other items that may cause patient to trip
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructContactForDizzinessPerson" id="Recertification_485InstructContactForDizzinessPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to contact agency for increased dizziness or problems with balance
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess date of patient's last eye exam
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to have annual eye exams
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct patient to use non-skid mats in tub/shower
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructAdequateLightingPerson" id="Recertification_485InstructAdequateLightingPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on importance of adequate lighting in patient area
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructInterventions" value="11" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructContactForFallPerson" id="Recertification_485InstructContactForFallPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to contact agency to report any fall with or without minor injury and to call 911
                                        for fall resulting in serious injury or causing severe pain or immobility
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485IADLOrderTemplates" name="Recertification_485IADLOrderTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485IADLComments" id="Recertification_485IADLComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485InstructGoals" value=" " />
                                        <input name="Recertification_485InstructGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        The patient will be free from falls during the certification period
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        The patient will be free from injury during the certification period
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeAnnualEyeExamPerson" id="Recertification_485VerbalizeAnnualEyeExamPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of need for annual eye examination by:
                                        <input type="text" name="Recertification_485VerbalizeAnnualEyeExamDate" id="Recertification_485VerbalizeAnnualEyeExamDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485RemoveClutterFromPathPerson" id="Recertification_485RemoveClutterFromPathPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will remove all clutter from patient's path, such as clothes, books, shoes, electrical
                                        cords, and other items, that may cause patient to trip by:
                                        <input type="text" name="Recertification_485RemoveClutterFromPathDate" id="Recertification_485RemoveClutterFromPathDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485InstructGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485RemoveThrowRugsAndSecurePerson" id="Recertification_485RemoveThrowRugsAndSecurePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will remove throw rugs or secure them with double-sided tape by:
                                        <input type="text" name="Recertification_485RemoveThrowRugsAndSecureDate" id="Recertification_485RemoveThrowRugsAndSecureDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485IADLGoalTemplates" name="Recertification_485IADLGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485IADLGoalComments" id="Recertification_485IADLGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th colspan="5">
                                        DME (locator #14)
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_485DME" value="" />
                                        <input name="Recertification_485DME" value="1" type="checkbox" />
                                        Bedside Commode
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="2" type="checkbox" />
                                        Cane
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="3" type="checkbox" />
                                        Elevated Toilet Seat
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="4" type="checkbox" />
                                        Grab Bars
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="5" type="checkbox" />
                                        Hospital Bed
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485DME" value="6" type="checkbox" />
                                        Nebulizer
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="7" type="checkbox" />
                                        Oxygen
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="8" type="checkbox" />
                                        Tub/Shower Bench
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="9" type="checkbox" />
                                        Walker
                                    </td>
                                    <td>
                                        <input name="Recertification_485DME" value="10" type="checkbox" />
                                        Wheelchair
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <ul>
                                            <li>Other:
                                                <br />
                                                <textarea name="Recertification_485DMEComments" id="Recertification_485DMEComments"
                                                    rows="5" style="width: 99%;"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="5">
                                        Supplies
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_GenericSupplies" value="" />
                                        <input name="Recertification_GenericSupplies" value="1" type="checkbox" />
                                        ABDs
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="2" type="checkbox" />
                                        Ace Wrap
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="3" type="checkbox" />
                                        Alcohol Pads
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="4" type="checkbox" />
                                        Chux/Underpads
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="5" type="checkbox" />
                                        Diabetic Supplies
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="6" type="checkbox" />
                                        Drainage Bag
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="7" type="checkbox" />
                                        Dressing Supplies
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="8" type="checkbox" />
                                        Duoderm
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="9" type="checkbox" />
                                        Exam Gloves
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="10" type="checkbox" />
                                        Foley Catheter
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="11" type="checkbox" />
                                        Gauze Pads
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="12" type="checkbox" />
                                        Insertion Kit
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="13" type="checkbox" />
                                        Irrigation Set
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="14" type="checkbox" />
                                        Irrigation Solution
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="15" type="checkbox" />
                                        Kerlix Rolls
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="16" type="checkbox" />
                                        Leg Bag
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="17" type="checkbox" />
                                        Needles
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="18" type="checkbox" />
                                        NG Tube
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="19" type="checkbox" />
                                        Probe Covers
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="20" type="checkbox" />
                                        Sharps Container
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="21" type="checkbox" />
                                        Sterile Gloves
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="22" type="checkbox" />
                                        Syringe
                                    </td>
                                    <td>
                                        <input name="Recertification_GenericSupplies" value="23" type="checkbox" />
                                        Tape
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <ul>
                                            <li>Other:
                                                <br />
                                                <textarea name="Recertification_GenericSuppliesComment" id="Recertification_GenericSuppliesComment"
                                                    rows="5" style="width: 99%;"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="5">
                                        DME Provider
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="5" class="bold">
                                        Information for company (other than home health agency) that provides supplies/DME:
                                        <ul class="columns">
                                            <li class="spacer">Name: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDMEProviderName" id="Recertification_GenericDMEProviderName"
                                                    size="40" maxlength="40" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Address: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDMEProviderAddress" id="Recertification_GenericDMEProviderAddress"
                                                    size="40" maxlength="40" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Phone Number: </li>
                                            <li>
                                                <input type="text" name="Recertification_GenericDMEProviderPhone" id="Recertification_GenericDMEProviderPhone"
                                                    size="15" maxlength="15" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Supplies/DME Provided: </li>
                                            <li>
                                                <textarea name="Recertification_GenericDMESuppliesProvided" id="Recertification_GenericDMESuppliesProvided"
                                                    rows="5" cols="100"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="suppliesworksheet_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationSuppliesWorksheetForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasisButtons">
                            <div class="row485">
                                <table id="suppliesTableRece" border="0" cellpadding="0" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th colspan="4">
                                                Current Supplies
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                Supplies Description
                                            </th>
                                            <th>
                                                Code
                                            </th>
                                            <th>
                                                Quantity
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <input type="hidden" name="Recertification_GenericSupply" id="Recertification_GenericSupply"
                                    value="" class="SupplyValue" />
                                <input value="Add Row" onclick="Oasis.addTableRow('#suppliesTableRece');" type="button" />
                            </div>
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Oasis.supplyInputFix('Recertification','#suppliesTableRece'); Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Oasis.supplyInputFix('Recertification','#suppliesTableRece'); Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%} %>
                    </div>
                    <div id="editMedications_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationMedicationsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <th colspan="5">
                                        New Medications (locator #10)
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        LS
                                    </th>
                                    <th>
                                        Start Date
                                    </th>
                                    <th>
                                        Medication/Dosage
                                    </th>
                                    <th>
                                        Classification
                                    </th>
                                    <th>
                                        Frequency/Route
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NewMedications" value=" " type="hidden" />
                                        <input name="Recertification_485NewMedications" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsLS1" id="Recertification_485NewMedicationsLS1"
                                            type="text" maxlength="10" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsDosage1" id="Recertification_485NewMedicationsDosage1"
                                            type="text" maxlength="20" />
                                    </td>
                                    <td>
                                        <select style="width: 150px;" name="Recertification_485NewMedicationsClassification1"
                                            id="Recertification_485NewMedicationsClassification1">
                                        </select>
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsFrequency1" id="Recertification_485NewMedicationsFrequency1"
                                            type="text" maxlength="50" style="width: 200px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NewMedications" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsLS2" id="Recertification_485NewMedicationsLS2"
                                            type="text" maxlength="10" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsDosage2" id="Recertification_485NewMedicationsDosage2"
                                            type="text" maxlength="20" />
                                    </td>
                                    <td>
                                        <select style="width: 150px;" name="Recertification_485NewMedicationsClassification2"
                                            id="Recertification_485NewMedicationsClassification2">
                                        </select>
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsFrequency2" id="Recertification_485NewMedicationsFrequency2"
                                            type="text" maxlength="50" style="width: 200px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485NewMedications" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsLS3" id="Recertification_485NewMedicationsLS3"
                                            type="text" maxlength="10" />
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsDosage3" id="Recertification_485NewMedicationsDosage3"
                                            type="text" maxlength="20" />
                                    </td>
                                    <td>
                                        <select style="width: 150px;" name="Recertification_485NewMedicationsClassification3"
                                            id="Recertification_485NewMedicationsClassification3">
                                        </select>
                                    </td>
                                    <td>
                                        <input name="Recertification_485NewMedicationsFrequency3" id="Recertification_485NewMedicationsFrequency3"
                                            type="text" maxlength="50" style="width: 200px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="100%">
                                        Medication Administration Record
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <strong>Time:</strong>&nbsp;<input type="text" name="Recertification_GenericMedRecTime"
                                            id="Recertification_GenericMedRecTime" value="" size="6" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Medication</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecMedication" id="Recertification_GenericMedRecMedication"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                    <td>
                                        <strong>Dose</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecDose" id="Recertification_GenericMedRecDose"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                    <td>
                                        <strong>Route</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecRoute" id="Recertification_GenericMedRecRoute"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Frequency</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecFrequency" id="Recertification_GenericMedRecFrequency"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                    <td>
                                        <strong>PRN Reason</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecPRN" id="Recertification_GenericMedRecPRN"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Location</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecLocation" id="Recertification_GenericMedRecLocation"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                    <td>
                                        <strong>Patient Response</strong><br />
                                        <input type="text" name="Recertification_GenericMedRecResponse" id="Recertification_GenericMedRecResponse"
                                            value="" size="30" maxlength="50" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="100%">
                                        <strong>Comment</strong><br />
                                        <textarea name="Recertification_GenericMedRecComments" id="Recertification_GenericMedRecComments"
                                            cols="105" rows="5"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>
                                        IV Access
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Does patient have IV access? </li>
                                            <li>
                                                <input name="Recertification_GenericIVAccess" value="" type="hidden" />
                                                <input name="Recertification_GenericIVAccess" value="1" type="radio" />&nbsp;Yes
                                            </li>
                                            <li>
                                                <input name="Recertification_GenericIVAccess" value="0" type="radio" />&nbsp;No</li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Type:</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericIVAccessType" id="Recertification_GenericIVAccessType"
                                                    size="50" maxlength="50" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Date of Insertion:</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericIVAccessDate" id="Recertification_GenericIVAccessDate"
                                                    size="11" maxlength="11" value="" />
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Date of Last Dressing Change:</li>
                                            <li>
                                                <input type="text" name="Recertification_GenericIVAccessDressingChange" id="Recertification_GenericIVAccessDressingChange"
                                                    size="11" maxlength="11" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2030) Management of Injectable Medications: Patient's current ability to prepare
                                        and take all prescribed injectable medications reliably and safely, including administration
                                        of correct dosage at the appropriate times/intervals. Excludes IV medications.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="Recertification_M2030ManagementOfInjectableMedications" type="hidden"
                                        value=" " />
                                    <input name="Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="00" />&nbsp;0 - Able to independently take the correct medication(s)
                                    and proper dosage(s) at the correct times.<br />
                                    <input name="Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="01" />&nbsp;1 - Able to take injectable medication(s) at the correct
                                    times if:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
                                    advance by another person; OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
                                    or chart.<br />
                                    <input name="Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="02" />&nbsp;2 - Able to take medication(s) at the correct times if given
                                    reminders by another person based on the frequency of the injection<br />
                                    <input name="Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="03" />&nbsp;3 - Unable to take injectable medication unless administered
                                    by another person.<br />
                                    <input name="Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="NA" />&nbsp;NA - No injectable medications prescribed.
                                </div>
                            </div>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Interventions
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485MedicationInterventions" value=" " />
                                        <input name="Recertification_485MedicationInterventions" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess patient filling medication box to determine if patient is preparing
                                        correctly
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess caregiver filling medication box to determine if caregiver is preparing
                                        correctly
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to determine if the
                                        <select name="Recertification_485DetermineFrequencEachMedPerson" id="Recertification_485DetermineFrequencEachMedPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        is able to identify the correct dose, route, and frequency of each medication
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess if the
                                        <select name="Recertification_485AssessIndicationEachMedPerson" id="Recertification_485AssessIndicationEachMedPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        can verbalize an understanding of the indication for each medication
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to establish reminders to alert patient to take medications at correct times
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="checkbox" name="Recertification_485MedicationInterventions" value="6" />
                                    </td>
                                    <td>
                                        SN to assess the
                                        <select name="Recertification_485AssessOpenMedContainersPerson" id="Recertification_485AssessOpenMedContainersPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        ability to open medication containers and determine the proper dose that should
                                        be administered
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructMedicationRegimen" id="Recertification_485InstructMedicationRegimen">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on medication regimen dose, indications, side effects, and interactions
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to remove any duplicate or expired medications to prevent confusion with medication
                                        regimen
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to observe patient drawing up injectable medications to determine if patient
                                        is able to draw up the correct dose
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485AssessAdminInjectMeds" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to assess the
                                        <select name="Recertification_485AssessAdminInjectMedsPerson" id="Recertification_485AssessAdminInjectMedsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        administering injectable medications to determine if proper technique is utilized
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="11" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to report to physician if drug therapy appears to be ineffective
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="12" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructHighRiskMedsPerson" id="Recertification_485InstructHighRiskMedsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on precautions for high risk medications, such as, hypoglycemics, anticoagulants/antiplatelets,
                                        sedative hypnotics, narcotics, antiarrhythmics, antineoplastics, skeletal muscle
                                        relaxants
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="13" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructSignsSymptomsIneffectiveDrugPerson" id="Recertification_485InstructSignsSymptomsIneffectiveDrugPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on signs and symptoms of ineffective drug therapy to report to SN or physician
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="14" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructMedSideEffectsPerson" id="Recertification_485InstructMedSideEffectsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on medication side effects to report to SN or physician
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="15" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructMedReactionsPerson" id="Recertification_485InstructMedReactionsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on medication reactions to report to SN or physician
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="16" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to administer IV
                                        <input type="text" name="Recertification_485AdministerIVType" id="Recertification_485AdministerIVType"
                                            size="15" maxlength="15" value="" />
                                        at rate of
                                        <input type="text" name="Recertification_485AdministerIVRate" id="Recertification_485AdministerIVRate"
                                            size="15" maxlength="15" value="" />
                                        via
                                        <input type="text" name="Recertification_485AdministerIVVia" id="Recertification_485AdministerIVVia"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485AdministerIVEvery" id="Recertification_485AdministerIVEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="17" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructAdministerIVPerson" id="Recertification_485InstructAdministerIVPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to administer IV at rate of
                                        <input type="text" name="Recertification_485InstructAdministerIVRate" id="Recertification_485InstructAdministerIVRate"
                                            size="15" maxlength="15" value="" />
                                        via
                                        <input type="text" name="Recertification_485InstructAdministerIVVia" id="Recertification_485InstructAdministerIVVia"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485InstructAdministerIVEvery" id="Recertification_485InstructAdministerIVEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="18" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to change peripheral IV catheter every 72 hours with
                                        <input type="text" name="Recertification_485ChangePeripheralIVGauge" id="Recertification_485ChangePeripheralIVGauge"
                                            size="15" maxlength="15" value="" />
                                        gauge
                                        <input type="text" name="Recertification_485ChangePeripheralIVWidth" id="Recertification_485ChangePeripheralIVWidth"
                                            size="15" maxlength="15" value="" />
                                        inch angiocath
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="19" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to flush peripheral IV with
                                        <input type="text" name="Recertification_485FlushPeripheralIVWith" id="Recertification_485FlushPeripheralIVWith"
                                            size="15" maxlength="15" value="" />
                                        cc of
                                        <input type="text" name="Recertification_485FlushPeripheralIVOf" id="Recertification_485FlushPeripheralIVOf"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485FlushPeripheralIVEvery" id="Recertification_485FlushPeripheralIVEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="20" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructFlushPerpheralIVPerson" id="Recertification_485InstructFlushPerpheralIVPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to flush peripheral IV with
                                        <input type="text" name="Recertification_485InstructFlushPerpheralIVWith" id="Recertification_485InstructFlushPerpheralIVWith"
                                            size="15" maxlength="15" value="" />
                                        cc of
                                        <input type="text" name="Recertification_485InstructFlushPerpheralIVOf" id="Recertification_485InstructFlushPerpheralIVOf"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485InstructFlushPerpheralIVEvery" id="Recertification_485InstructFlushPerpheralIVEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="21" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to change central line dressing every
                                        <input type="text" name="Recertification_485ChangeCentralLineEvery" id="Recertification_485ChangeCentralLineEvery"
                                            size="15" maxlength="15" value="" />
                                        using sterile technique
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="22" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructChangeCentralLinePerson" id="Recertification_485InstructChangeCentralLinePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to change central line dressing every
                                        <input type="text" name="Recertification_485InstructChangeCentralLineEvery" id="Recertification_485InstructChangeCentralLineEvery"
                                            size="15" maxlength="15" value="" />
                                        using sterile technique
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="23" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to flush central line with
                                        <input type="text" name="Recertification_485FlushCentralLineWith" id="Recertification_485FlushCentralLineWith"
                                            size="15" maxlength="15" value="" />
                                        cc of
                                        <input type="text" name="Recertification_485FlushCentralLineOf" id="Recertification_485FlushCentralLineOf"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485FlushCentralLineEvery" id="Recertification_485FlushCentralLineEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="24" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct
                                        <select name="Recertification_485InstructFlushCentralLinePerson" id="Recertification_485InstructFlushCentralLinePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to flush central line with
                                        <input type="text" name="Recertification_485InstructFlushCentralLineWith" id="Recertification_485InstructFlushCentralLineWith"
                                            size="15" maxlength="15" value="" />
                                        cc of
                                        <input type="text" name="Recertification_485InstructFlushCentralLineOf" id="Recertification_485InstructFlushCentralLineOf"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485InstructFlushCentralLineEvery" id="Recertification_485InstructFlushCentralLineEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="25" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to access
                                        <input type="text" name="Recertification_485AccessPortType" id="Recertification_485AccessPortType"
                                            size="15" maxlength="15" value="" />
                                        port every
                                        <input type="text" name="Recertification_485AccessPortTypeEvery" id="Recertification_485AccessPortTypeEvery"
                                            size="15" maxlength="15" value="" />
                                        and flush with
                                        <input type="text" name="Recertification_485AccessPortTypeWith" id="Recertification_485AccessPortTypeWith"
                                            size="15" maxlength="15" value="" />
                                        cc of
                                        <input type="text" name="Recertification_485AccessPortTypeOf" id="Recertification_485AccessPortTypeOf"
                                            size="15" maxlength="15" value="" />
                                        every
                                        <input type="text" name="Recertification_485AccessPortTypeFrequency" id="Recertification_485AccessPortTypeFrequency"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="26" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to change
                                        <input type="text" name="Recertification_485ChangePortDressingType" id="Recertification_485ChangePortDressingType"
                                            size="15" maxlength="15" value="" />
                                        port dressing using sterile technique every
                                        <input type="text" name="Recertification_485ChangePortDressingEvery" id="Recertification_485ChangePortDressingEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="27" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructPortDressingPerson" id="Recertification_485InstructPortDressingPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        to change
                                        <input type="text" name="Recertification_485InstructPortDressingType" id="Recertification_485InstructPortDressingType"
                                            size="15" maxlength="15" value="" />
                                        port dressing using sterile technique every
                                        <input type="text" name="Recertification_485InstructPortDressingEvery" id="Recertification_485InstructPortDressingEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="28" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to change IV tubing every
                                        <input type="text" name="Recertification_485ChangeIVTubingEvery" id="Recertification_485ChangeIVTubingEvery"
                                            size="15" maxlength="15" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationInterventions" value="29" type="checkbox" />
                                    </td>
                                    <td>
                                        SN to instruct the
                                        <select name="Recertification_485InstructInfectionSignsSymptomsPerson" id="Recertification_485InstructInfectionSignsSymptomsPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        on signs and symptoms of infection and infiltration
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485MedicationInterventionTemplates" name="Recertification_485MedicationInterventionTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485MedicationInterventionComments" id="Recertification_485MedicationInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="2">
                                        Goals
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input type="hidden" name="Recertification_485MedicationGoals" value=" " />
                                        <input name="Recertification_485MedicationGoals" value="1" type="checkbox" />
                                    </td>
                                    <td>
                                        Patient will remain free of adverse medication reactions during the episode
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="2" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485MedManagementIndependentPerson" id="Recertification_485MedManagementIndependentPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be independent with medication management by:
                                        <input type="text" name="Recertification_485MedManagementIndependentDate" id="Recertification_485MedManagementIndependentDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="3" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeMedRegimenUnderstandingPerson" id="Select14">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will verbalize understanding of medication regimen, dose, route, frequency, indications,
                                        and side effects by:
                                        <input type="text" name="Recertification_485VerbalizeMedRegimenUnderstandingDate"
                                            id="Recertification_485VerbalizeMedRegimenUnderstandingDate" size="10" maxlength="10"
                                            value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="4" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485MedAdminIndependentPerson" id="Recertification_485MedAdminIndependentPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be independent with
                                        <input type="text" name="Recertification_485MedAdminIndependentWith" id="Recertification_485MedAdminIndependentWith"
                                            size="10" maxlength="10" value="" />
                                        administration by:
                                        <input type="text" name="Recertification_485MedAdminIndependentDate" id="Recertification_485MedAdminIndependentDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="5" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485MedSetupIndependentPerson" id="Select16Recertification_485MedSetupIndependentPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be independent with setting up medication boxes by:
                                        <input type="text" name="Recertification_485MedSetupIndependentDate" id="Recertification_485MedSetupIndependentDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="6" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485VerbalizeEachMedIndicationPerson" id="Recertification_485VerbalizeEachMedIndicationPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be able to verbalize an understanding of the indications for each medication
                                        by:
                                        <input type="text" name="Recertification_485VerbalizeEachMedIndicationDate" id="Recertification_485VerbalizeEachMedIndicationDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="7" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485CorrectDoseIdentifyPerson" id="Recertification_485CorrectDoseIdentifyPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will be able to identify the correct dose, route, and frequency of each medication
                                        by:
                                        <input type="text" name="Recertification_485CorrectDoseIdentifyDate" id="Recertification_485CorrectDoseIdentifyDate"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="8" type="checkbox" />
                                    </td>
                                    <td>
                                        IV will remain patent and free from signs and symptoms of infection
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="9" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485DemonstrateCentralLineFlushPerson" id="Recertification_485DemonstrateCentralLineFlushPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will demonstrate understanding of flushing central line
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="10" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485DemonstratePeripheralIVLineFlushPerson" id="Recertification_485DemonstratePeripheralIVLineFlushPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will demonstrate understanding of flushing peripheral IV line
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="11" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485DemonstrateSterileDressingTechniquePerson" id="Recertification_485DemonstrateSterileDressingTechniquePerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will demonstrate understanding of changing
                                        <input type="text" name="Recertification_485DemonstrateSterileDressingTechniqueType"
                                            id="Recertification_485DemonstrateSterileDressingTechniqueType" size="10" maxlength="10"
                                            value="" />
                                        dressing using sterile technique
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15px">
                                        <input name="Recertification_485MedicationGoals" value="12" type="checkbox" />
                                    </td>
                                    <td>
                                        The
                                        <select name="Recertification_485DemonstrateAdministerIVPerson" id="Recertification_485DemonstrateAdministerIVPerson">
                                            <option value="Patient/Caregiver">Patient/Caregiver</option>
                                            <option value="Patient">Patient</option>
                                            <option value="Caregiver">Caregiver</option>
                                        </select>
                                        will demonstrate understanding of administering IV
                                        <input type="text" name="Recertification_485DemonstrateAdministerIVType" id="Recertification_485DemonstrateAdministerIVType"
                                            size="10" maxlength="10" value="" />
                                        at rate of
                                        <input type="text" name="Recertification_485DemonstrateAdministerIVRate" id="Recertification_485DemonstrateAdministerIVRate"
                                            size="10" maxlength="10" value="" />
                                        via
                                        <input type="text" name="Recertification_485DemonstrateAdministerIVVia" id="Recertification_485DemonstrateAdministerIVVia"
                                            size="10" maxlength="10" value="" />
                                        every
                                        <input type="text" name="Recertification_485DemonstrateAdministerIVEvery" id="Recertification_485DemonstrateAdministerIVEvery"
                                            size="10" maxlength="10" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Goals: &nbsp;
                                        <select id="Recertification_485MedicationGoalTemplates" name="Recertification_485MedicationGoalTemplates">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485MedicationGoalComments" id="Recertification_485MedicationGoalComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editTherapyneed_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationTherapyNeedForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode
                                        for which this assessment will define a case mix group, what is the indicated need
                                        for therapy visits (total of reasonable and necessary physical, occupational, and
                                        speech-language pathology visits combined)? (Enter zero [ “000” ] if no therapy
                                        visits indicated.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input type="text" name="Recertification_M2200NumberOfTherapyNeed" id="Recertification_M2200NumberOfTherapyNeed" />&nbsp;Number
                                    of therapy visits indicated (total of physical, occupational and speech-language
                                    pathology combined).<br />
                                    <input name="Recertification_M2200TherapyNeed" type="hidden" value=" " />
                                    <input name="Recertification_M2200TherapyNeed" type="checkbox" value="1" />&nbsp;NA
                                    - Not Applicable: No case mix group defined by this assessment.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editOrdersdisciplinetreatment_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisRecertificationOrdersDisciplineTreatmentForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "Edit")%>
                        <%= Html.Hidden("Recertification_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th colspan="2">
                                        Orders for Discipline and Treatments
                                    </th>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SN Frequency
                                    </td>
                                    <td>
                                        <input id="Recertification_485SNFrequency" maxlength="70" size="70" type="text" name="Recertification_485SNFrequency" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        PT Frequency
                                    </td>
                                    <td>
                                        <input id="Recertification_485PTFrequency" maxlength="70" size="70" type="text" name="Recertification_485PTFrequency" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        OT Frequency
                                    </td>
                                    <td>
                                        <input id="Recertification_485OTFrequency" maxlength="70" size="70" type="text" name="Recertification_485OTFrequency" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        ST Frequency
                                    </td>
                                    <td>
                                        <input id="Recertification_485STFrequency" maxlength="70" size="70" type="text" name="Recertification_485STFrequency" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        MSW Frequency
                                    </td>
                                    <td>
                                        <input id="Recertification_485MSWFrequency" maxlength="70" size="70" type="text"
                                            name="Recertification_485MSWFrequency" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        HHA Frequency
                                    </td>
                                    <td>
                                        <input id="Recertification_485HHAFrequency" maxlength="70" size="70" type="text"
                                            name="Recertification_485HHAFrequency" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" name="Recertification_485Dietician" value=" " />
                                        <input name="Recertification_485Dietician" value="1" type="checkbox" />
                                        Dietician
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Additional Orders: &nbsp;
                                        <select id="Recertification_485OrdersDisciplineInterventionTemplates" name="Recertification_485OrdersDisciplineInterventionTemplates">
                                            <option selected="selected" value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea id="Recertification_485OrdersDisciplineInterventionComments" rows="5" style="width: 99%;"
                                            name="Recertification_485OrdersDisciplineInterventionComments"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th>
                                        Rehabilitation Potential
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485RehabilitationPotential" value="" type="hidden" />
                                                <input name="Recertification_485RehabilitationPotential" value="1" type="checkbox" />
                                            </li>
                                            <li>Good to achieve stated goals with skilled intervention and patient's compliance
                                                with the plan of care </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485RehabilitationPotential" value="2" type="checkbox" />
                                            </li>
                                            <li>Fair to achieve stated goals with skilled intervention and patient's compliance
                                                with the plan of care </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485RehabilitationPotential" value="3" type="checkbox" />
                                            </li>
                                            <li>Poor to achieve stated goals with skilled intervention and patient's compliance
                                                with the plan of care </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>Other rehabilitation potential: &nbsp;
                                                <select id="Recertification_485AchieveGoalsTemplates" name="Recertification_485AchieveGoalsTemplates">
                                                    <option value="0">&nbsp;</option>
                                                    <option value="-2">-----------</option>
                                                    <option value="-1">Erase</option>
                                                </select>
                                                <br />
                                                <textarea name="Recertification_485AchieveGoalsComments" id="Recertification_485AchieveGoalsComments"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th>
                                        Discharge Plans
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input type="hidden" name="Recertification_485DischargePlans" value="" />
                                                <input name="Recertification_485DischargePlans" value="1" type="checkbox" />
                                            </li>
                                            <li>Discharge to care of physician </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="2" type="checkbox" />
                                            </li>
                                            <li>Discharge to caregiver </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="3" type="checkbox" />
                                            </li>
                                            <li>Discharge patient to self care </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="4" type="checkbox" />
                                            </li>
                                            <li>Discharge when caregiver willing and able to manage all aspects of patient's care
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="5" type="checkbox" />
                                            </li>
                                            <li>Discharge when goals met </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="6" type="checkbox" />
                                            </li>
                                            <li>Discharge when wound(s) healed </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="7" type="checkbox" />
                                            </li>
                                            <li>Discharge when reliable caregiver available to assist with patient's medical needs
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input name="Recertification_485DischargePlans" value="8" type="checkbox" />
                                            </li>
                                            <li>Discharge when patient is independent in management of medical needs </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>Additional discharge plans: &nbsp;
                                                <select id="Recertification_485DischargePlanTemplates" name="Recertification_485DischargePlanTemplates">
                                                    <option value="0">&nbsp;</option>
                                                    <option value="-2">-----------</option>
                                                    <option value="-1">Erase</option>
                                                </select>
                                                <br />
                                                <textarea name="Recertification_485DischargePlanComments" id="Recertification_485DischargePlanComments"
                                                    rows="5" cols="70"></textarea>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="3">
                                        Patient Strengths
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_485PatientStrengths" value="" />
                                        <input name="Recertification_485PatientStrengths" value="1" type="checkbox" />
                                        Motivated Learner
                                    </td>
                                    <td>
                                        <input name="Recertification_485PatientStrengths" value="2" type="checkbox" />
                                        Strong Support System
                                    </td>
                                    <td>
                                        <input name="Recertification_485PatientStrengths" value="3" type="checkbox" />
                                        Absence of Multiple Diagnosis
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input name="Recertification_485PatientStrengths" value="4" type="checkbox" />
                                        Enhanced Socioeconomic Status
                                    </td>
                                    <td colspan="2">
                                        Other:
                                        <input type="text" name="Recertification_485PatientStrengthOther" id="Recertification_485PatientStrengthOther"
                                            size="70" maxlength="70" value="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table id="Table1" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th colspan="3">
                                        Conclusions
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="Recertification_485Conclusions" value="" />
                                        <input name="Recertification_485Conclusions" value="1" type="checkbox" />
                                        Skilled Intervention Needed
                                    </td>
                                    <td>
                                        <input name="Recertification_485Conclusions" value="2" type="checkbox" />
                                        Skilled Instruction Needed
                                    </td>
                                    <td>
                                        <input name="Recertification_485Conclusions" value="3" type="checkbox" />
                                        No Skilled Service Needed
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        Other:
                                        <input type="text" name="Recertification_485ConclusionOther" id="Recertification_485ConclusionOther"
                                            size="70" maxlength="70" value="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row485">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        Skilled Intervention
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="3" class="bold">
                                        Assessment/ Instruction/ Performance:&nbsp;
                                        <select id="Recertification_485SkilledInterventionTemplate" name="Recertification_485SkilledInterventionTemplate">
                                            <option value="0">&nbsp;</option>
                                            <option value="-2">-----------</option>
                                            <option value="-1">Erase</option>
                                        </select>
                                        <br />
                                        <textarea name="Recertification_485SkilledInterventionComments" id="Recertification_485SkilledInterventionComments"
                                            rows="5" cols="70"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li>
                                                <input type="hidden" name="Recertification_485SIResponse" id="Recertification_485SIResponse" />
                                                <input name="Recertification_485SIResponse" value="1" type="checkbox" />
                                                Response to Skilled Intervention </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp;</li>
                                            <li class="spacer">Verbalized Understanding </li>
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_485SIVerbalizedUnderstandingPT" value=" " />
                                                <input name="Recertification_485SIVerbalizedUnderstandingPT" value="1" type="checkbox" />
                                                Pt </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_485SIVerbalizedUnderstandingPTPercent" id="Recertification_485SIVerbalizedUnderstandingPTPercent"
                                                    size="4" maxlength="4" value="" />
                                                % </li>
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_485SIVerbalizedUnderstandingCG" value=" " />
                                                <input name="Recertification_485SIVerbalizedUnderstandingCG" value="1" type="checkbox" />
                                                CG </li>
                                            <li>
                                                <input type="text" name="Recertification_485SIVerbalizedUnderstandingCGPercent" id="Recertification_485SIVerbalizedUnderstandingCGPercent"
                                                    size="4" maxlength="4" value="" />
                                                % </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp;</li>
                                            <li class="spacer">Return Demonstration </li>
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_485SIReturnDemonstrationPT" value=" " />
                                                <input name="Recertification_485SIReturnDemonstrationPT" value="1" type="checkbox" />
                                                Pt </li>
                                            <li class="littleSpacer">
                                                <input type="text" name="Recertification_485SIReturnDemonstrationPTPercent" id="Recertification_485SIReturnDemonstrationPTPercent"
                                                    size="4" maxlength="4" value="" />
                                                % </li>
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_485SIReturnDemonstrationCG" value=" " />
                                                <input name="Recertification_485SIReturnDemonstrationCG" value="1" type="checkbox" />
                                                CG </li>
                                            <li>
                                                <input type="text" name="Recertification_485SIReturnDemonstrationCGPercent" id="Recertification_485SIReturnDemonstrationCGPercent"
                                                    size="4" maxlength="4" value="" />
                                                % </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp;</li>
                                            <li class="spacer">Require Further Teaching </li>
                                            <li class="littleSpacer">
                                                <input type="hidden" name="Recertification_485SIFurtherTeachReqPT" value=" " />
                                                <input name="Recertification_485SIFurtherTeachReqPT" value="1" type="checkbox" />
                                                Pt </li>
                                            <li>
                                                <input type="hidden" name="Recertification_485SIFurtherTeachReqCG" value=" " />
                                                <input name="Recertification_485SIFurtherTeachReqCG" value="1" type="checkbox" />
                                                CG </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="littleSpacer">&nbsp;</li>
                                            <li class="spacer">Comments: </li>
                                            <li>
                                                <input type="text" name="Recertification_485SIResponseComments" id="Recertification_485SIResponseComments"
                                                    size="90" maxlength="90" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Title of Teaching Tool Used/Given: </li>
                                            <li>
                                                <input type="text" name="Recertification_485TeachingToolUsed" id="Recertification_485TeachingToolUsed"
                                                    size="40" maxlength="40" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="bigSpacer">Progress To Goals: </li>
                                            <li>
                                                <input type="text" name="Recertification_485ProgressToGoals" id="Recertification_485ProgressToGoals"
                                                    size="40" maxlength="40" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Conferenced With: </li>
                                            <li>
                                                <select name="Recertification_485ConferencedWith" id="Recertification_485ConferencedWith">
                                                    <option value=" "></option>
                                                    <option value="MD">MD</option>
                                                    <option value="SN">SN</option>
                                                    <option value="PT">PT</option>
                                                    <option value="OT">OT</option>
                                                    <option value="ST">ST</option>
                                                    <option value="MSW">MSW</option>
                                                    <option value="HHA">HHA</option>
                                                </select>
                                            </li>
                                        </ul>
                                        <ul class="columns">
                                            <li class="spacer">Name: </li>
                                            <li>
                                                <input type="text" name="Recertification_485ConferencedWithName" id="Recertification_485ConferencedWithName"
                                                    size="30" maxlength="30" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <ul class="columns">
                                            <li class="spacer">Regarding: </li>
                                            <li>
                                                <textarea name="Recertification_485SkilledInterventionRegarding" id="Recertification_485SkilledInterventionRegarding"
                                                    rows="5" cols="70"></textarea></li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Physician Contacted Re: </li>
                                            <li>
                                                <input type="text" name="Recertification_485SkilledInterventionPhysicianContacted"
                                                    id="Recertification_485SkilledInterventionPhysicianContacted" size="90" maxlength="90"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Order Changes: </li>
                                            <li>
                                                <input type="text" name="Recertification_485SkilledInterventionOrderChanges" id="Recertification_485SkilledInterventionOrderChanges"
                                                    size="90" maxlength="90" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Plan for Next Visit: </li>
                                            <li>
                                                <input type="text" name="Recertification_485SkilledInterventionNextVisit" id="Recertification_485SkilledInterventionNextVisit"
                                                    size="90" maxlength="90" value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Next Physician Visit: </li>
                                            <li>
                                                <input type="text" name="Recertification_485SkilledInterventionNextPhysicianVisit"
                                                    id="Recertification_485SkilledInterventionNextPhysicianVisit" size="10" maxlength="10"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">Discharge Planning: </li>
                                            <li>
                                                <input type="text" name="Recertification_485SkilledInterventionDischargePlanning"
                                                    id="Recertification_485SkilledInterventionDischargePlanning" size="90" maxlength="90"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ul class="columns">
                                            <li class="spacer">&nbsp;</li>
                                            <li>
                                                <input type="hidden" name="Recertification_485SkilledInterventionWrittenNotice" value=" " />
                                                <input name="Recertification_485SkilledInterventionWrittenNotice" value="1" type="checkbox" />
                                                Written notice of discharge provided to patient. </li>
                                            <li>Discharge scheduled for:
                                                <input type="text" name="Recertification_485SkilledInterventionWrittenNoticeDate"
                                                    id="Recertification_485SkilledInterventionWrittenNoticeDate" size="10" maxlength="10"
                                                    value="" />
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Recertification.js"))
       .OnDocumentReady(() =>
        {%>
Recertification.Init();
<%}); 
%>
