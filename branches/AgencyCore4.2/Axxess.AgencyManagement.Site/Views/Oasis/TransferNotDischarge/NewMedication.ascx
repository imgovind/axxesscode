﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientNotDischargedMedicationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
<%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
<%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2004) Medication Intervention: If there were any clinically significant medication
                issues since the previous OASIS assessment, was a physician or the physician-designee
                contacted within one calendar day of the assessment to resolve clinically significant
                medication issues, including reconciliation?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M2004MedicationIntervention", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2004MedicationIntervention", "00", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2004MedicationIntervention", "01", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2004MedicationIntervention", "NA", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No clinically significant medication issues identified since the previous OASIS
            assessment
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS
                assessment, was the patient/caregiver instructed by agency staff or other health
                care provider to monitor the effectiveness of drug therapy, drug reactions, and
                side effects, and how and when to report problems that may occur?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention", "00", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention", "01", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2015PatientOrCaregiverDrugEducationIntervention", "NA", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient not taking any drugs
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
