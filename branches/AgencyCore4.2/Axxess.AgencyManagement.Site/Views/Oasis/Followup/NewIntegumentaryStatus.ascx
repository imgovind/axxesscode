﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpIntegumentaryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">    
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II
                or Higher or designated as "unstageable"?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1306UnhealedPressureUlcers", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1306UnhealedPressureUlcers", "0", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "0" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M1322 ]<br />
            <%=Html.RadioButton("FollowUp_M1306UnhealedPressureUlcers", "1", data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insideColFull title">
            <div class="padding">
                (M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each
                Stage: (Enter “0” if none; excludes Stage I pressure ulcers)
            </div>
        </div>
    </div>
</div>
<div class="row485" id="followUp_M1308">
    <table border="0" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    Column 1<br />
                    Complete at SOC/ROC/FU & D/C
                </th>
                <th>
                    Column 2<br />
                    Complete at FU & D/C
                </th>
            </tr>
            <tr>
                <th>
                    Stage description – unhealed pressure ulcers
                </th>
                <th>
                    Number Currently Present
                </th>
                <th>
                    Number of those listed in Column 1 that were present on admission (most recent SOC
                    / ROC)
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer
                    with red pink wound bed, without slough. May also present as an intact or open/ruptured
                    serum-filled blister.
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageTwoUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageTwoUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageTwoUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageTwoUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone,
                    tendon, or muscles are not exposed. Slough may be present but does not obscure the
                    depth of tissue loss. May include undermining and tunneling.
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageThreeUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageThreeUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageThreeUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageThreeUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough
                    or eschar may be present on some parts of the wound bed. Often includes undermining
                    and tunneling.
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageFourUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageFourUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedStageIVUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedStageIVUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or
                    device
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by
                    slough and/or eschar.
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission" })%>
                </td>
            </tr>
            <tr>
                <td>
                    d.3 Unstageable: Suspected deep tissue injury in evolution.
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent" })%>
                </td>
                <td>
                    <%=Html.TextBox("FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission", data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "", new { @id = "FollowUp_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission" })%>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="rowOasis">    
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable
                redness of a localized area usually over a bony prominence. The area may be painful,
                firm, soft, warmer or cooler as compared to adjacent tissue.
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1322CurrentNumberStageIUlcer", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "00", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0
                <br />
                <%=Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "01", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? true : false, new { @id = "" })%>
                &nbsp;1
                <br />
                <%=Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "02", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "03", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3<br />
                <%=Html.RadioButton("FollowUp_M1322CurrentNumberStageIUlcer", "04", data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                or more
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1324MostProblematicUnhealedStage", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "01", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? true : false, new { @id = "" })%>
            - Stage I<br />
            <%=Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "02", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Stage II<br />
            <%=Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "03", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Stage III<br />
            <%=Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "04", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Stage IV<br />
            <%=Html.RadioButton("FollowUp_M1324MostProblematicUnhealedStage", "NA", data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No observable pressure ulcer or unhealed pressure ulcer
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1330) Does this patient have a Stasis Ulcer?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1330StasisUlcer", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1330StasisUlcer", "00", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M1340 ]
            <br />
            <%=Html.RadioButton("FollowUp_M1330StasisUlcer", "01", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes, patient has BOTH observable and unobservable stasis ulcers
            <br />
            <%=Html.RadioButton("FollowUp_M1330StasisUlcer", "02", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Yes, patient has observable stasis ulcers ONLY<br />
            <%=Html.RadioButton("FollowUp_M1330StasisUlcer", "03", data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Yes, patient has unobservable stasis ulcers ONLY (known but not observable due
            to non-removable dressing) [ Go to M1340 ]
        </div>
    </div>
</div>
<div class="rowOasis" id="followUp_M1332AndM1334">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1332) Current Number of (Observable) Stasis Ulcer(s):
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1332CurrentNumberStasisUlcer", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "01", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? true : false, new { @id = "" })%>
            &nbsp;1 - One<br />
            <%=Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "02", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Two<br />
            <%=Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "03", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Three<br />
            <%=Html.RadioButton("FollowUp_M1332CurrentNumberStasisUlcer", "04", data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Four or more<br />
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1334) Status of Most Problematic (Observable) Stasis Ulcer:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1334StasisUlcerStatus", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "00", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Newly epithelialized
            <br />
            <%=Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "01", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Fully granulating
            <br />
            <%=Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "02", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Early/partial granulation<br />
            <%=Html.RadioButton("FollowUp_M1334StasisUlcerStatus", "03", data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Not healing
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1340) Does this patient have a Surgical Wound?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1340SurgicalWound", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1340SurgicalWound", "00", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M1350 ]<br />
            <%=Html.RadioButton("FollowUp_M1340SurgicalWound", "01", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? true : false, new { @id = "" })%>
            &nbsp;1 - Yes, patient has at least one (observable) surgical wound<br />
            <%=Html.RadioButton("FollowUp_M1340SurgicalWound", "02", data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? true : false, new { @id = "" })%>
            &nbsp;2 - Surgical wound known but not observable due to non-removable dressing
            [ Go to M1350 ]
        </div>
    </div>
    <div class="insideCol" id="soc_M1342">
        <div class="insiderow title">
            <div class="padding">
                (M1342) Status of Most Problematic (Observable) Surgical Wound:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1342SurgicalWoundStatus", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "00", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Newly epithelialized
            <br />
            <%=Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "01", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Fully granulating
            <br />
            <%=Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "02", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Early/partial granulation<br />
            <%=Html.RadioButton("FollowUp_M1342SurgicalWoundStatus", "03", data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Not healing
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy,
                other than those described above that is receiving intervention by the home health
                agency?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1350SkinLesionOpenWound", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1350SkinLesionOpenWound", "0", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "0" ? true : false, new { @id = "" })%>
            &nbsp;0 - No &nbsp;&nbsp;
            <%=Html.RadioButton("FollowUp_M1350SkinLesionOpenWound", "1", data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
