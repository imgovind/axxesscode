﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ValidationInfoViewData>" %>
<div class="submitoasisCont">
    <table id="validationErrorTable">
        <thead>
            <tr>
                <th colspan="3">
                    <b>You have&nbsp;<span id="numberOfErrors">
                        <%=Model.Count %></span>&nbsp;errors.</b>
                       
                </th>
            </tr>
            <tr>
            <th>
                   Error Type
                </th>
                <th>
                    Id
                </th>
                <th>
                    Comment
                </th>
            </tr>
        </thead>
        <tbody>
            <%if (Model.Count == 0)
              { %>
            <tr>
                <td colspan="2">
                    <b>
                        <%=Model.Message %></b>
                </td>
            </tr>
            <%} %>
            <%if (Model.Count > 0 && Model.validationError != null)
              { %>
            <%foreach (var data in Model.validationError)
              { %>
            <tr>
                <td>
                    <%=data.ErrorType %>
                </td>
                <td>
                    <%=data.ErrorDup %>
                </td>
                <td>
                    <%=data.Description %>
                </td>
            </tr>
            <%} %>
            <%} %>
        </tbody>
        <tfoot>
        </tfoot>
    </table>
</div>
