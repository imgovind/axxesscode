﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<div>
    <% using (Html.BeginForm("New", "Patient", FormMethod.Post, new { @id = "admitPatientForm" }))%>
    <%  { %>
    <div id="admitPatientValidaton" class="marginBreak " style="display: none">
    </div>
    <div class="marginBreak">
        <b>Patient Demographics</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="FirstName">
                                First Name (M0040):&nbsp;&nbsp;&nbsp;</label>
                            <div class="inputs">
                                <%=Html.TextBox("FirstName", Model.FirstName, new { @id = "txtAdmit_Patient_FirstName", @class = "text  input_wrapper required", @maxlength = "100", @tabindex = "1" })%>
                            </div>
                        </div>
                        <div class="row ">
                            <label for="MiddleInitial">
                                MI:&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("MiddleInitial","", new { @id = "txtAdmit_Patient_MiddleInitial", @class = "text input_wrapper", @style = "width:20px;", @tabindex = "2" })%>
                        </div>
                        <div class="row">
                            <label for="LastName">
                                Last Name:&nbsp;&nbsp;&nbsp;</label>
                            <div class="inputs">
                                <%=Html.TextBox("LastName", Model.LastName, new { @id = "txtAdmit_Patient_LastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "3" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="Gender">
                                Gender (M0069):</label>
                            <div class="inputs">
                                <%=Html.RadioButton("Gender", "Female", Model.Gender == "Female" ? true : false, new { @id = "" })%>Female
                                <%=Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "" })%>Male
                            </div>
                        </div>
                        <div class="row">
                            <label for="DOB">
                                Date of Birth (M0066):</label>
                            <div class="inputs">
                                <%= Html.Telerik().DatePicker()
                                         .Name("DOB")
                                         .Value(Model.DOB)                                                                                                            
                                         .HtmlAttributes(new { @id = "txtAdmit_Patient_DOB", @class = "text required  date", @tabindex = "5" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="MaritalStatus">
                                Marital Status:</label>
                            <div class="inputs">
                                <%var maritalStatus = new SelectList(new[]
               { 
                   new SelectListItem { Text = "** Select **", Value = "0" },
                   new SelectListItem { Text = "Married", Value = "Married" },
                   new SelectListItem { Text = "Divorce", Value = "Divorce" },
                   new SelectListItem { Text = "Widowed", Value = "Widowed"} ,
                   new SelectListItem { Text = "Single", Value = "Single" }                 
                   
               }
                   , "Value", "Text", "0");%>
                                <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "txtAdmit_Patient_MaritalStatus", @class = "input_wrapper", @tabindex = "7" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PatientID">
                                Patient ID(M0020):</label>
                            <div class="inputs">
                                <%=Html.TextBox("PatientIdNumber", "", new { @id = "txtAdmit_Patient_PatientID", @class = "text required input_wrapper", @maxlength = "20", @tabindex = "8" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="MedicareNumber">
                                Medicare Number (M0063):</label>
                            <div class="inputs">
                                <%=Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "txtAdmit_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11", @tabindex = "9" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="MedicaidNumber">
                                Medicaid Number(M0065):</label>
                            <div class="inputs">
                                <%=Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "txtAdmit_Patient_MedicaidNumber", @class = "text digits input_wrapper", @maxlength = "9", @tabindex = "10" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="SSN">
                                SSN (M0064):</label>
                            <div class="inputs">
                                <%=Html.TextBox("SSN", Model.SSN, new { @id = "txtAdmit_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "9", @tabindex = "11" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="ESD">
                                Episode Start Date :</label>
                            <div class="inputs">
                                <%= Html.Telerik().DatePicker()
                                        .Name("EpisodeStartDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new { @id = "txtAdmit_Patient_EpisodeStartDate", @class = "text required date", @tabindex = "13" })
                                %>
                            </div>
                        </div>
                        <div class="row">
                            <label for="SOC">
                                Start of Care Date (M0030):</label>
                            <div class="inputs">
                                <%= Html.Telerik().DatePicker()
                                        .Name("StartOfCareDate")
                                        .Value(DateTime.Today)                           
                                        .HtmlAttributes(new { @id = "txtAdmit_Patient_StartOfCareDate", @class = "text required date required", @tabindex = "12" })
                                %>
                            </div>
                        </div>
                        <div class="row">
                            <label for="FirstName">
                                &nbsp;&nbsp;&nbsp; Assign to Clinician :&nbsp;&nbsp;&nbsp;</label>
                            <%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId!=null && Model.UserId!=Guid.Empty? Model.UserId.ToString():"0", new { @id = "txtAdmit_Patient_Assign", @class = "input_wrapper required selectDropDown", @tabindex = "", @style = "width: 150px;" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="margin">
                    <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                        <thead>
                            <tr>
                                <th colspan='4'>
                                    <label for="EthnicRaces">
                                        Race/Ethnicity (M0140):</label>
                                    <label class="error" for="EthnicRaces" generated="true">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" value="0" name="EthnicRaces" class="required" />
                                    American Indian or Alaska Native
                                </td>
                                <td>
                                    <input type="checkbox" value="1" name="EthnicRaces" class="required" />
                                    Asian
                                </td>
                                <td>
                                    <input type="checkbox" value="2" name="EthnicRaces" class="required" />
                                    Black or African-American
                                </td>
                                <td>
                                    <input type="checkbox" value="3" name="EthnicRaces" class="required" />
                                    Hispanic or Latino
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" value="4" name="EthnicRaces" class="required" />
                                    Native Hawaiian or Pacific Islander
                                </td>
                                <td>
                                    <input type="checkbox" value="5" name="EthnicRaces" class="required" />
                                    White
                                </td>
                                <td colspan="2">
                                    <input type="checkbox" value="6" name="EthnicRaces" class="required" />
                                    Unknown
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="margin">
                    <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                        <thead>
                            <tr>
                                <th colspan='4'>
                                    <label for="EthnicRaces">
                                        Payment Source (M0150):</label>
                                    <label class="error" for="PaymentSources" generated="true" />
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" value="0" name="PaymentSources" class="required" />
                                    None; no charge for current services
                                </td>
                                <td>
                                    <input type="checkbox" value="1" name="PaymentSources" class="required" />
                                    Mediacre (traditional fee-for-service)
                                </td>
                                <td>
                                    <input type="checkbox" value="2" name="PaymentSources" class="required" />
                                    Medicare (HMO/ managed care)
                                </td>
                                <td>
                                    <input type="checkbox" value="3" name="PaymentSources" class="required" />
                                    Madicaid (traditional fee-for-service)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" value="4" name="PaymentSources" class="required" />
                                    Madicaid (HMO/ managed care)
                                </td>
                                <td>
                                    <input type="checkbox" value="5" name="PaymentSources" class="required" />
                                    Workers' compensation
                                </td>
                                <td>
                                    <input type="checkbox" value="6" name="PaymentSources" class="required" />
                                    Title programs (e.g., Titile III,V, or XX)
                                </td>
                                <td>
                                    <input type="checkbox" value="7" name="PaymentSources" class="required" />
                                    Other government (e.g.,CHAMPUS,VA,etc)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" value="8" name="PaymentSources" class="required" />
                                    Private insurance
                                </td>
                                <td>
                                    <input type="checkbox" value="9" name="PaymentSources" class="required" />
                                    Private HMO/ managed care
                                </td>
                                <td>
                                    <input type="checkbox" value="10" name="PaymentSources" class="required" />
                                    Self-pay
                                </td>
                                <td>
                                    <input type="checkbox" value="11" name="PaymentSources" class="required" />
                                    Unknown
                                </td>
                            </tr>
                            <tr>
                                <td colspan='4'>
                                    <input type="checkbox" id="EditNewPatient_PaymentSource" value="12" name="PaymentSources"
                                        class="required" />
                                    Other (specify) &nbsp;&nbsp;&nbsp;
                                    <%=Html.TextBox("OtherPaymentSource", "", new { @id = "txtAdmit_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" })%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="marginBreak">
        <b>Patient Address</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="AddressLine1">
                                Address Line 1:</label>
                            <div class="inputs">
                                <%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "txtAdmit_Patient_AddressLine1", @class = "text required input_wrapper", @tabindex = "14" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressLine2">
                                Address Line 2:</label>
                            <div class="inputs">
                                <%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "txtAdmit_Patient_AddressLine2", @class = "text input_wrapper", tabindex = "15" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressCity">
                                City:</label>
                            <div class="inputs">
                                <%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "txtAdmit_Patient_AddressCity", @class = "text required input_wrapper", @tabindex = "16" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressStateCode">
                                State (M0050), Zip(M0060):</label>
                            <div class="inputs">
                                <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "txtAdmit_Patient_AddressStateCode", @class = "input_wrapper required selectDropDown", @tabindex = "", @style = "width: 119px;" })%>
                                &nbsp;
                                <%=Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "txtAdmit_Patient_AddressZipCode", @class = "text required digits isValidUSZip", @tabindex = "18", @style = "margin: 0px; padding: 0px; width: 54px;", @size = "5", @maxlength = "5" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PhoneHome">
                                Home Phone:</label>
                            <div class="inputs">
                                <span class="input_wrappermultible">
                                    <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome!=null&&Model.PhoneHome!=""?Model.PhoneHome.Substring(0,3):"", new { @id = "txtAdmit_Patient_HomePhone1", @class = "autotext required digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "19" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "txtAdmit_Patient_HomePhone2", @class = "autotext required digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "20" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "txtAdmit_Patient_HomePhone3", @class = "autotext required digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "21" })%>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <label for="PhoneMobile">
                                Mobile Phone:</label>
                            <div class="inputs">
                                <span class="input_wrappermultible">
                                    <%=Html.TextBox("PhoneMobileArray",  "", new { @id = "txtAdmit_Patient_MobilePhone1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "22" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("PhoneMobileArray",  "", new { @id = "txtAdmit_Patient_MobilePhone2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "23" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("PhoneMobileArray", "", new { @id = "txtAdmit_Patient_MobilePhone3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "24" })%>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressCity">
                                Email:</label>
                            <div class="inputs">
                                <%=Html.TextBox("Email", "", new { @id = "txtAdmit_Patient_Email", @class = "text input_wrapper", @tabindex = "25" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="halfRow">
        <div class="marginBreak">
            <b>Case Manager</b>
            <div class="rowBreak">
                <div class="contentDivider">
                    <div class="patientfieldset">
                        <div class="fix">
                            <div class="row">
                                <label for="PharmacyName">
                                    Name :&nbsp;&nbsp;&nbsp;</label>
                                <div class="inputs">
                                    <%= Html.LookupSelectList(SelectListTypes.Users, "CaseManagerId","0", new { @id = "txtAdmit_Patient_CaseManager", @class = "input_wrapper required selectDropDown", @tabindex = "", @style = "width: 150px;" })%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="halfRow">
        <div class="marginBreak">
            <b>Pharmacy</b>
            <div class="rowBreak">
                <div class="contentDivider">
                    <div class="patientfieldset">
                        <div class="fix">
                            <div class="row">
                                <label for="PharmacyName">
                                    Name :&nbsp;&nbsp;&nbsp;</label>
                                <%=Html.TextBox("PharmacyName", "", new { @id = "txtAdmit_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "26" })%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contentDivider">
                    <div class="patientfieldset">
                        <div class="fix">
                            <div class="row">
                                <label for="PhoneHome">
                                    Phone:</label>
                                <div class="inputs">
                                    <span class="input_wrappermultible">
                                        <%=Html.TextBox("PharmacyPhoneArray", "", new { @id = "txtAdmit_Patient_PharmacyPhone1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "27" })%>
                                    </span>- <span class="input_wrappermultible">
                                        <%=Html.TextBox("PharmacyPhoneArray", "", new { @id = "txtAdmit_Patient_PharmacyPhone2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "28" })%>
                                    </span>- <span class="input_wrappermultible">
                                        <%=Html.TextBox("PharmacyPhoneArray", "", new { @id = "txtAdmit_Patient_PharmacyPhone3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "29" })%>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="marginBreak">
        <b>Insurance</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PrimaryInsurance">
                                Primary:&nbsp;&nbsp;&nbsp;</label>
                            <select name="PrimaryInsurance" id="txtAdmit_Patient_PrimaryInsurance" class="input_wrapper"
                                tabindex="30">
                                <option value="0">** Select **</option>
                                <option value="1">Advantra Freedom</option>
                                <option value="2">AETNA HEALTH INC.</option>
                                <option value="3">CARE IMPROVEMENT PLUS OF TEXAS INSURANCE</option>
                                <option value="4">FIRST HEALTH LIFE AND HEALTH INSUR</option>
                                <option value="5">Palmetto GBA</option>
                                <option value="6">Well Care HEALT INSURANCE OF ARIZON</option>
                                <option value="7">WELLCARE OF TEXAS, INC.</option>
                            </select>
                        </div>
                        <div class="row">
                            <label for="SecondaryInsurance">
                                Secondary:&nbsp;&nbsp;&nbsp;</label>
                            <select name="SecondaryInsurance" id="txtAdmit_Patient_SecondaryInsurance" class="input_wrapper"
                                tabindex="31">
                                <option value="0">** Select **</option>
                                <option value="1">Advantra Freedom</option>
                                <option value="2">AETNA HEALTH INC.</option>
                                <option value="3">CARE IMPROVEMENT PLUS OF TEXAS INSURANCE</option>
                                <option value="4">FIRST HEALTH LIFE AND HEALTH INSUR</option>
                                <option value="5">Palmetto GBA</option>
                                <option value="6">Well Care HEALT INSURANCE OF ARIZON</option>
                                <option value="7">WELLCARE OF TEXAS, INC.</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="TertiaryInsurance">
                                Tertiary:&nbsp;&nbsp;&nbsp;</label>
                            <select name="TertiaryInsurance" id="txtAdmit_Patient_TertiaryInsurance" class="input_wrapper"
                                tabindex="32">
                                <option value="0">** Select **</option>
                                <option value="1">Advantra Freedom</option>
                                <option value="2">AETNA HEALTH INC.</option>
                                <option value="3">CARE IMPROVEMENT PLUS OF TEXAS INSURANCE</option>
                                <option value="4">FIRST HEALTH LIFE AND HEALTH INSUR</option>
                                <option value="5">Palmetto GBA</option>
                                <option value="6">Well Care HEALT INSURANCE OF ARIZON</option>
                                <option value="7">WELLCARE OF TEXAS, INC.</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="marginBreak">
        <b>Primary Emergency Contact</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row" style="vertical-align: middle;">
                            <label for="FirstName">
                                First Name:&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("EmergencyContact.FirstName", "", new { @id = "txtAdmit_Patient_EmergencyContactFirstName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "33" })%>
                        </div>
                        <div class="row">
                            <label for="LastName">
                                Last Name:&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("EmergencyContact.LastName", "", new { @id = "txtAdmit_Patient_EmergencyContactLastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "34" })%>
                        </div>
                        <div class="row">
                            <label for="PhoneHome">
                                Primary Phone:</label>
                            <div class="inputs">
                                <span class="input_wrappermultible">
                                    <%=Html.TextBox("EmergencyContact.PhonePrimaryArray", "", new { @id = "txtAdmit_Patient_EmergencyContactPrimaryPhoneArray1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "35" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("EmergencyContact.PhonePrimaryArray", "", new { @id = "txtAdmit_Patient_EmergencyContactPrimaryPhoneArray2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "36" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("EmergencyContact.PhonePrimaryArray", "", new { @id = "txtAdmit_Patient_EmergencyContactPrimaryPhoneArray3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "37" })%>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <label for="PhoneMobile">
                                Alt Phone:</label>
                            <div class="inputs">
                                <span class="input_wrappermultible">
                                    <%=Html.TextBox("EmergencyContact.PhoneAlternateArray", "", new { @id = "txtAdmit_Patient_EmergencyContactAltPhoneArray1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "38" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("EmergencyContact.PhoneAlternateArray", "", new { @id = "txtAdmit_Patient_EmergencyContactAltPhoneArray2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "39" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("EmergencyContact.PhoneAlternateArray", "", new { @id = "txtAdmit_Patient_EmergencyContactAltPhoneArray3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "40" })%>
                                </span>
                            </div>
                        </div>
                        <div class="row" style="vertical-align: middle;">
                            <label for="Email">
                                Email:&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("EmergencyContact.EmailAddress", "", new { @id = "txtAdmit_Patient_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "41" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="Relationship">
                                Relationship:</label>
                            <div class="inputs">
                                <%=Html.TextBox("EmergencyContact.Relationship", "", new { @id = "txtAdmit_Patient_Relationship", @class = "text input_wrapper required", @tabindex = "42" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressLine1">
                                Address Line 1:</label>
                            <div class="inputs">
                                <%=Html.TextBox("EmergencyContact.AddressLine1", "", new { @id = "txtAdmit_Patient_EmergencyContactAddressLine1", @class = "text input_wrapper required", @tabindex = "43" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressLine2">
                                Address Line 2:</label>
                            <div class="inputs">
                                <%=Html.TextBox("EmergencyContact.AddressLine2", "", new { @id = "txtAdmit_Patient_EmergencyContactAddressLine2", @class = "text input_wrapper", tabindex = "44" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressCity">
                                City:</label>
                            <div class="inputs">
                                <%=Html.TextBox("EmergencyContact.AddressCity", "", new { @id = "txtAdmit_Patient_EmergencyContactAddressCity", @class = "text input_wrapper required", @tabindex = "45" })%>
                            </div>
                        </div>
                        <div class="row">
                            <label for="AddressStateCode">
                                State, Zip Code :</label>
                            <div class="inputs">
                                <select id="txtAdmit_Patient_EmergencyContactAddressStateCode" name="EmergencyContact.AddressState"
                                    class="AddressStateCode input_wrapper required" style="width: 119px;" tabindex="46">
                                    <option value="0" selected>** Select State **</option>
                                </select>
                                &nbsp;
                                <%=Html.TextBox("EmergencyContact.AddressZipCode", "", new { @id = "txtAdmit_Patient_EmergencyContactAddressZipCode", @class = "text digits isValidUSZip", @tabindex = "47", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="marginBreak">
        <b>Primary Physician Contact</b>
        <div class="rowTable">
            <table border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>
                            <label>
                                Select from the list below:</label>
                            <select style="width: 150px;" class="Physicians input_wrapper " tabindex="17" name="AgencyPhysicians"
                                id="txtAdmit_Patient_PhysicianDropDown">
                                <option value="0" selected="selected">** Select Physician **</option>
                            </select>
                        </th>
                        <th>
                            <label>
                                Search by NPI Number:</label>
                            <input type="text" name="txtAdmit_NpiNumber" id="txtAdmit_NpiNumber" maxlength="10" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2">
                            <div class="rowBreak">
                                <div class="row">
                                    <div class="contentDivider">
                                        <div class="patientfieldset">
                                            <div class="fix">
                                                <div class="row">
                                                    <label for="FirstName">
                                                        First Name:&nbsp;&nbsp;&nbsp;</label>
                                                    <%=Html.TextBox("Physician.FirstName", "", new { @id = "txtAdmit_Patient_FirstNamePhysicianContact", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "48" })%>
                                                </div>
                                                <div class="row">
                                                    <label for="LastName">
                                                        Last Name:&nbsp;&nbsp;&nbsp;</label>
                                                    <%=Html.TextBox("Physician.LastName", "", new { @id = "txtAdmit_Patient_LastNamePhysicianContact", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "49" })%>
                                                </div>
                                                <div class="row">
                                                    <label for="PhoneHome">
                                                        Primary Phone:</label>
                                                    <div class="inputs">
                                                        <span class="input_wrappermultible">
                                                            <%=Html.TextBox("Physician.PhoneWorkArray", "", new { @id = "txtAdmit_Patient_PhysicianContactPrimaryPhoneArray1", @class = "input_wrappermultible autotext digits required phone_short", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "50" })%>
                                                        </span>- <span class="input_wrappermultible">
                                                            <%=Html.TextBox("Physician.PhoneWorkArray", "", new { @id = "txtAdmit_Patient_PhysicianContactPrimaryPhoneArray2", @class = "input_wrappermultible autotext digits required phone_short", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "51" })%>
                                                        </span>- <span class="input_wrappermultible">
                                                            <%=Html.TextBox("Physician.PhoneWorkArray", "", new { @id = "txtAdmit_Patient_PhysicianContactPrimaryPhoneArray3", @class = "input_wrappermultible autotext digits required phone_short", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "52" })%>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="Fax">
                                                        Fax:</label>
                                                    <div class="inputs">
                                                        <%=Html.TextBox("Physician.FaxNumberArray", "", new { @id = "txtAdmit_Patient_PhysicianContactFax1", @class = "input_wrappermultible autotext digits required phone_short", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "53" })%>
                                                        </span>- <span class="input_wrappermultible">
                                                            <%=Html.TextBox("Physician.FaxNumberArray", "", new { @id = "txtAdmit_Patient_PhysicianContactFax2", @class = "input_wrappermultible autotext digits required phone_short", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "54" })%>
                                                        </span>- <span class="input_wrappermultible">
                                                            <%=Html.TextBox("Physician.FaxNumberArray", "", new { @id = "txtAdmit_Patient_PhysicianContactFax3", @class = "input_wrappermultible autotext digits required phone_short", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "55" })%>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="FirstName">
                                                        Email:&nbsp;&nbsp;&nbsp;</label>
                                                    <%=Html.TextBox("Physician.EmailAddress", "", new { @id = "txtAdmit_Patient_PhysicianContactEmail", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "56" })%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contentDivider">
                                        <div class="patientfieldset">
                                            <div class="fix">
                                                <div class="row">
                                                    <label for="Relationship">
                                                        NPI No:</label>
                                                    <div class="inputs">
                                                        <%=Html.TextBox("Physician.NPI", "", new { @id = "txtAdmit_Patient_PhysicianContactNPINo", @class = "text input_wrapper digits", @maxlength = "10", @tabindex = "57" })%>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="AddressLine1">
                                                        Address Line 1:</label>
                                                    <div class="inputs">
                                                        <%=Html.TextBox("Physician.AddressLine1", "", new { @id = "txtAdmit_Patient_PhysicianContactAddressLine1", @class = "text input_wrapper required", @tabindex = "58" })%>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="AddressLine2">
                                                        Address Line 2:</label>
                                                    <div class="inputs">
                                                        <%=Html.TextBox("Physician.AddressLine2", "", new { @id = "txtAdmit_Patient_PhysicianContactAddressLine2", @class = "text input_wrapper", tabindex = "59" })%>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="AddressCity">
                                                        City:</label>
                                                    <div class="inputs">
                                                        <%=Html.TextBox("Physician.AddressCity", "", new { @id = "txtAdmit_Patient_PhysicianContactAddressCity", @class = "text input_wrapper required", @tabindex = "60" })%>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="AddressStateCode">
                                                        State, Zip Code :</label>
                                                    <div class="inputs">
                                                        <select id="txtAdmit_Patient_PhysicianContactAddressStateCode" name="PhysicianContact.MailingAddressStateCode"
                                                            class="AddressStateCode input_wrapper required" style="width: 119px;" tabindex="61">
                                                            <option value="0" selected>** Select State **</option>
                                                        </select>
                                                        &nbsp;
                                                        <%=Html.TextBox("Physician.AddressStateCode", "", new { @id = "txtAdmit_Patient_PhysicianContactAddressZipCode", @class = "text digits isValidUSZip", @tabindex = "62", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="margin">
            <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th colspan='6'>
                            Services Required
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %>
                    <tr>
                        <td>
                            <input type="checkbox" value="0" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("0")  ){ %>checked="checked"<% }%>'" />
                            SNV
                        </td>
                        <td>
                            <input type="checkbox" value="1" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("1")  ){ %>checked="checked"<% }%>'" />
                            HHA
                        </td>
                        <td>
                            <input type="checkbox" value="2" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("2")  ){ %>checked="checked"<% }%>'" />
                            PT
                        </td>
                        <td>
                            <input type="checkbox" value="3" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("3")  ){ %>checked="checked"<% }%>'" />
                            OT
                        </td>
                        <td>
                            <input type="checkbox" value="4" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("4")  ){ %>checked="checked"<% }%>'" />
                            SP
                        </td>
                        <td>
                            <input type="checkbox" value="5" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("5")  ){ %>checked="checked"<% }%>'" />
                            MSW
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="margin">
            <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th colspan='5'>
                            DME Needed
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %>
                    <tr>
                        <td>
                            <input type="checkbox" value="0" name="DMECollection" '<% if(  DME!=null && DME.Contains("0")  ){ %>checked="checked"<% }%>'" />
                            Bedside Commode
                        </td>
                        <td>
                            <input type="checkbox" value="1" name="DMECollection" '<% if(  DME!=null && DME.Contains("1")  ){ %>checked="checked"<% }%>'" />
                            Cane
                        </td>
                        <td>
                            <input type="checkbox" value="2" name="DMECollection" '<% if(  DME!=null && DME.Contains("2")  ){ %>checked="checked"<% }%>'" />
                            Elevated Toilet Seat
                        </td>
                        <td>
                            <input type="checkbox" value="3" name="DMECollection" '<% if(  DME!=null && DME.Contains("3")  ){ %>checked="checked"<% }%>'" />
                            Grab Bars
                        </td>
                        <td>
                            <input type="checkbox" value="4" name="DMECollection" '<% if(  DME!=null && DME.Contains("4")  ){ %>checked="checked"<% }%>'" />
                            Hospital Bed
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" value="5" name="DMECollection" '<% if(  DME!=null && DME.Contains("5")  ){ %>checked="checked"<% }%>'" />
                            Nebulizer
                        </td>
                        <td>
                            <input type="checkbox" value="6" name="DMECollection" '<% if(  DME!=null && DME.Contains("6")  ){ %>checked="checked"<% }%>'" />
                            Oxygen
                        </td>
                        <td>
                            <input type="checkbox" value="7" name="DMECollection" '<% if(  DME!=null && DME.Contains("7")  ){ %>checked="checked"<% }%>'" />
                            Tub/Shower Bench
                        </td>
                        <td>
                            <input type="checkbox" value="8" name="DMECollection" '<% if(  DME!=null && DME.Contains("8")  ){ %>checked="checked"<% }%>'" />
                            Walker
                        </td>
                        <td>
                            <input type="checkbox" value="9" name="DMECollection" '<% if(  DME!=null && DME.Contains("9")  ){ %>checked="checked"<% }%>'" />
                            Wheelchair
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <input type="checkbox" value="10" id="Edit_DME_Other" name="DMECollection" '<% if(  DME!=null && DME.Contains("10")  ){ %>checked="checked"<% }%>'" />
                            other &nbsp;&nbsp;&nbsp;
                            <%=Html.TextBox("OtherDME", Model.OtherDME, new { @id = "txtAdmit_Referral_OtherDME", @class = "text", @style = "display:none;" })%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="marginBreak">
        <b>Referral</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="AdmissionSource">
                                Admission Source:&nbsp;&nbsp;&nbsp;</label>
                            <%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.ReferralSource, new { @id = "txtAdmit_Patient_AdmissionSource", @class = "input_wrapper", @tabindex = "64" })%>
                        </div>
                        <div class="row">
                            <label for="ReferralDate">
                                Referral Date:</label>
                            <div class="inputs">
                                <%= Html.Telerik().DatePicker().Name("ReferralDate").Value(Model.ReferralDate).HtmlAttributes(new { @id = "txtAdmit_Patient_PatientReferralDate", @class = "text date", @tabindex = "65" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="FirstName">
                                FirstName:&nbsp;&nbsp;&nbsp;</label>
                        </div>
                        <div class="row">
                            <label for="FirstName">
                                LastName:&nbsp;&nbsp;&nbsp;</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="buttons buttonfix">
            <ul>
                <li>
                    <input name="" type="submit" value="Save" /></li>
                <li>
                    <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
                <li>
                    <input name="" type="reset" value="Reset" /></li>
            </ul>
        </div>
    </div>
    <%} %>
</div>
