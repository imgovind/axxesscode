﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("Edit", "Patient", FormMethod.Post, new { @id = "editPatientForm" }))%>
<%  { %>
<%=Html.Hidden("Id", Model.Id, new { @id = "txtEdit_PatientID" })%>
<div id="editPatientValidaton" class="marginBreak" style="display: none">
</div>
<div class="marginBreak">
    <b>Patient Demographics</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            First Name (M0040):&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("FirstName", Model.FirstName, new { @id = "txtEdit_Patient_FirstName", @class = "text  input_wrapper required", @maxlength = "100", @tabindex = "1" })%>
                        </div>
                    </div>
                    <div class="row ">
                        <label for="MiddleInitial">
                            MI:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = "txtEdit_Patient_MiddleInitial", @class = "text input_wrapper", @style = "width:20px;", @tabindex = "2" })%>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("LastName", Model.LastName, new { @id = "txtEdit_Patient_LastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "3" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Gender">
                            Gender (M0069):</label>
                        <div class="inputs">
                            <%=Html.RadioButton("Gender", "Female", Model.Gender == "Female" ? true : false, new { @id = "" })%>Female
                            <%=Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "" })%>Male
                        </div>
                    </div>
                    <div class="row">
                        <label for="DOB">
                            Date of Birth (M0066):</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                         .Name("DOB")
                                         .Value(Model.DOB)                                                                                                            
                                         .HtmlAttributes(new { @id = "txtEdit_Patient_DOB", @class = "text required  date", @tabindex = "5" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="MaritalStatus">
                            Marital Status:</label>
                        <div class="inputs">
                            <%var maritalStatus = new SelectList(new[]
               { 
                   new SelectListItem { Text = "** Select **", Value = "0" },
                   new SelectListItem { Text = "Married", Value = "Married" },
                   new SelectListItem { Text = "Divorce", Value = "Divorce" },
                   new SelectListItem { Text = "Widowed", Value = "Widowed"} ,
                   new SelectListItem { Text = "Single", Value = "Single" }                 
                   
               }
                   , "Value", "Text", Model.MaritalStatus);%>
                            <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "txtEdit_Patient_MaritalStatus", @class = "input_wrapper", @tabindex = "7" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="PatientID">
                            Patient ID(M0020):</label>
                        <div class="inputs">
                            <%=Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "txtEdit_Patient_PatientID", @class = "text required input_wrapper", @maxlength = "20", @tabindex = "8" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="MedicareNumber">
                            Medicare Number (M0063):</label>
                        <div class="inputs">
                            <%=Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "txtEdit_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11", @tabindex = "9" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="MedicaidNumber">
                            Medicaid Number(M0065):</label>
                        <div class="inputs">
                            <%=Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "txtEdit_Patient_MedicaidNumber", @class = "text digits input_wrapper", @maxlength = "9", @tabindex = "10" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="SSN">
                            SSN (M0064):</label>
                        <div class="inputs">
                            <%=Html.TextBox("SSN", Model.SSN, new { @id = "txtEdit_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "9", @tabindex = "11" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="SOC">
                            Start of Care Date (M0030):</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                        .Name("StartOfCareDate")
                                        .Value(Model.StartofCareDate)                           
                                        .HtmlAttributes(new { @id = "txtEdit_Patient_StartOfCareDate", @class = "text required date required", @tabindex = "12" })
                            %>
                        </div>
                    </div>
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp; Assign to Clinician :&nbsp;&nbsp;&nbsp;</label>
                        <%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId!=null && Model.UserId!=Guid.Empty? Model.UserId.ToString():"0", new { @id = "txtEdit_Patient_Assign", @class = "Employees input_wrapper required selectDropDown", @tabindex = "", @style = "width: 150px;" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="margin">
                <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th colspan='4'>
                                <label for="EthnicRaces">
                                    Race/Ethnicity (M0140):</label>
                                <label class="error" for="EthnicRaces" generated="true">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <%string[] ethnicities = Model.Ethnicities != null && Model.Ethnicities != "" ? Model.Ethnicities.Split(';') : null;  %>
                        <tr>
                            <td>
                                <input type="checkbox" value="0" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("0")  ){ %>checked="checked"<% }%>'" />
                                American Indian or Alaska Native
                            </td>
                            <td>
                                <input type="checkbox" value="1" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("1")  ){ %>checked="checked"<% }%>'" />
                                Asian
                            </td>
                            <td>
                                <input type="checkbox" value="2" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("2")  ){ %>checked="checked"<% }%>'" />
                                Black or African-American
                            </td>
                            <td>
                                <input type="checkbox" value="3" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("3")  ){ %>checked="checked"<% }%>'" />
                                Hispanic or Latino
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" value="4" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("4")  ){ %>checked="checked"<% }%>'" />
                                Native Hawaiian or Pacific Islander
                            </td>
                            <td>
                                <input type="checkbox" value="5" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("5")  ){ %>checked="checked"<% }%>'" />
                                White
                            </td>
                            <td colspan="2">
                                <input type="checkbox" value="6" name="EthnicRaces" class="required" '<% if(  ethnicities!=null && ethnicities.Contains("6")  ){ %>checked="checked"<% }%>'" />
                                Unknown
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="margin">
                <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th colspan='4'>
                                <label for="EthnicRaces">
                                    Payment Source (M0150):</label>
                                <label class="error" for="PaymentSources" generated="true">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <%string[] paymentSources = Model.PaymentSource != null && Model.PaymentSource != "" ? Model.PaymentSource.Split(';') : null;  %>
                        <tr>
                            <td>
                                <input type="checkbox" value="0" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("0")  ){ %>checked="checked"<% }%>'" />
                                None; no charge for current services
                            </td>
                            <td>
                                <input type="checkbox" value="1" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("1")  ){ %>checked="checked"<% }%>'" />
                                Mediacre (traditional fee-for-service)
                            </td>
                            <td>
                                <input type="checkbox" value="2" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("2")  ){ %>checked="checked"<% }%>'" />
                                Medicare (HMO/ managed care)
                            </td>
                            <td>
                                <input type="checkbox" value="3" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("3")  ){ %>checked="checked"<% }%>'" />
                                Madicaid (traditional fee-for-service)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" value="4" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("4")  ){ %>checked="checked"<% }%>'" />
                                Madicaid (HMO/ managed care)
                            </td>
                            <td>
                                <input type="checkbox" value="5" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("5")  ){ %>checked="checked"<% }%>'" />
                                Workers' compensation
                            </td>
                            <td>
                                <input type="checkbox" value="6" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("6")  ){ %>checked="checked"<% }%>'" />
                                Title programs (e.g., Titile III,V, or XX)
                            </td>
                            <td>
                                <input type="checkbox" value="7" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("7")  ){ %>checked="checked"<% }%>'" />
                                Other government (e.g.,CHAMPUS,VA,etc)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" value="8" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("8")  ){ %>checked="checked"<% }%>'" />
                                Private insurance
                            </td>
                            <td>
                                <input type="checkbox" value="9" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("9")  ){ %>checked="checked"<% }%>'" />
                                Private HMO/ managed care
                            </td>
                            <td>
                                <input type="checkbox" value="10" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("10")  ){ %>checked="checked"<% }%>'" />
                                Self-pay
                            </td>
                            <td>
                                <input type="checkbox" value="11" name="PaymentSources" class="required" '<% if(  paymentSources!=null && paymentSources.Contains("11")  ){ %>checked="checked"<% }%>'" />
                                Unknown
                            </td>
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <input type="checkbox" id="EditNewPatient_PaymentSource" value="12" name="PaymentSources"
                                    class="required" '<% if(  paymentSources!=null && paymentSources.Contains("12")  ){ %>checked="checked"<% }%>'" />
                                Other (specify) &nbsp;&nbsp;&nbsp;
                                <%=Html.TextBox("OtherPaymentSource", Model.OtherPaymentSource, new { @id = "txtEdit_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Patient Address</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "txtEdit_Patient_AddressLine1", @class = "text required input_wrapper", @tabindex = "14" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "txtEdit_Patient_AddressLine2", @class = "text input_wrapper", tabindex = "15" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "txtEdit_Patient_AddressCity", @class = "text required input_wrapper", @tabindex = "16" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State (M0050), Zip(M0060):</label>
                        <div class="inputs">
                            <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "txtEdit_Patient_AddressStateCode", @class = "AddressStateCode input_wrapper required selectDropDown", @tabindex = "17", @style = "width: 119px;" })%>
                            &nbsp;
                            <%=Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "txtEdit_Patient_AddressZipCode", @class = "text required digits isValidUSZip", @tabindex = "18", @style = "margin: 0px; padding: 0px; width: 54px;", @size = "5", @maxlength = "5" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="PhoneHome">
                            Home Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome!=null&&Model.PhoneHome!=""?Model.PhoneHome.Substring(0,3):"", new { @id = "txtEdit_Patient_HomePhone1", @class = "autotext required digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "19" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "txtEdit_Patient_HomePhone2", @class = "autotext required digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "20" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "txtEdit_Patient_HomePhone3", @class = "autotext required digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "21" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneMobile">
                            Mobile Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneMobileArray", Model.PhoneMobile != null && Model.PhoneMobile != "" ? Model.PhoneMobile.Substring(0, 3) : "", new { @id = "txtEdit_Patient_MobilePhone1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "22" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneMobileArray", Model.PhoneMobile != null && Model.PhoneMobile != "" ? Model.PhoneMobile.Substring(3, 3) : "", new { @id = "txtEdit_Patient_MobilePhone2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "23" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneMobileArray", Model.PhoneMobile != null && Model.PhoneMobile != "" ? Model.PhoneMobile.Substring(6, 4) : "", new { @id = "txtEdit_Patient_MobilePhone3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "24" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            Email:</label>
                        <div class="inputs">
                            <%=Html.TextBox("Email", "", new { @id = "txtEdit_Patient_Email", @class = "text input_wrapper", @tabindex = "25" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="halfRow">
    <div class="marginBreak">
        <b>Case Manager</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PharmacyName">
                                Name :&nbsp;&nbsp;&nbsp;</label>
                            <%= Html.LookupSelectList(SelectListTypes.Users, "CaseManagerId", Model.CaseManagerId != null && Model.CaseManagerId != Guid.Empty ? Model.CaseManagerId.ToString() : "0", new { @id = "txtEdit_Patient_CaseManager", @class = "text input_wrapper", @tabindex = ""})%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="halfRow">
    <div class="marginBreak">
        <b>Pharmacy</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PharmacyName">
                                Name :&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "txtEdit_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "26" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PhoneHome">
                                Phone:</label>
                            <div class="inputs">
                                <span class="input_wrappermultible">
                                    <%=Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone != null && Model.PharmacyPhone != "" ? Model.PharmacyPhone.Substring(0, 3) : "", new { @id = "txtEdit_Patient_PharmacyPhone1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "27" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone != null && Model.PharmacyPhone != "" ? Model.PharmacyPhone.Substring(3, 3) : "", new { @id = "txtEdit_Patient_PharmacyPhone2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "28" })%>
                                </span>- <span class="input_wrappermultible">
                                    <%=Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone != null && Model.PharmacyPhone != "" ? Model.PharmacyPhone.Substring(6, 4) : "", new { @id = "txtEdit_Patient_PharmacyPhone3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "29" })%>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Insurance</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="PrimaryInsurance">
                            Primary:&nbsp;&nbsp;&nbsp;</label>
                        <%var primaryInsurance = new SelectList(new[]
               { 
                   new SelectListItem { Text = "** Select **", Value = "0" },
                   new SelectListItem { Text = "Advantra Freedom", Value = "1" },
                   new SelectListItem { Text = "Aetna Health INC.", Value = "2" },
                   new SelectListItem { Text = "CARE IMPROVEMENT PLUS OF TEXAS INSURANCE", Value = "3"} ,
                   new SelectListItem { Text = "FIRST HEALTH LIFE AND HEALTH INSUR", Value = "4" } ,
                   new SelectListItem { Text = "Palmetto GBA", Value = "5" },
                   new SelectListItem { Text = "Well Care HEALT INSURANCE OF ARIZON", Value = "6"} ,
                   new SelectListItem { Text = "WELLCARE OF TEXAS, INC.", Value = "7" }                     
                   
               }
                   , "Value", "Text", Model.PrimaryInsurance);%>
                        <%= Html.DropDownList("PrimaryInsurance", primaryInsurance, new { @id = "txtEdit_Patient_PrimaryInsurance", @class = "input_wrapper", @tabindex = "30" })%>
                    </div>
                    <div class="row">
                        <label for="SecondaryInsurance">
                            Secondary:&nbsp;&nbsp;&nbsp;</label>
                        <%var secondaryInsurance = new SelectList(new[]
               { 
                   new SelectListItem { Text = "** Select **", Value = "0" },
                   new SelectListItem { Text = "Advantra Freedom", Value = "1" },
                   new SelectListItem { Text = "Aetna Health INC.", Value = "2" },
                   new SelectListItem { Text = "CARE IMPROVEMENT PLUS OF TEXAS INSURANCE", Value = "3"} ,
                   new SelectListItem { Text = "FIRST HEALTH LIFE AND HEALTH INSUR", Value = "4" } ,
                   new SelectListItem { Text = "Palmetto GBA", Value = "5" },
                   new SelectListItem { Text = "Well Care HEALT INSURANCE OF ARIZON", Value = "6"} ,
                   new SelectListItem { Text = "WELLCARE OF TEXAS, INC.", Value = "7" }                     
                   
               }
                   , "Value", "Text", Model.SecondaryInsurance);%>
                        <%= Html.DropDownList("SecondaryInsurance", secondaryInsurance, new { @id = "txtEdit_Patient_SecondaryInsurance", @class = "input_wrapper", @tabindex = "31" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="TertiaryInsurance">
                            Tertiary:&nbsp;&nbsp;&nbsp;</label>
                        <%var tertiaryInsurance = new SelectList(new[]
               { 
                   new SelectListItem { Text = "** Select **", Value = "0" },
                   new SelectListItem { Text = "Advantra Freedom", Value = "1" },
                   new SelectListItem { Text = "Aetna Health INC.", Value = "2" },
                   new SelectListItem { Text = "CARE IMPROVEMENT PLUS OF TEXAS INSURANCE", Value = "3"} ,
                   new SelectListItem { Text = "FIRST HEALTH LIFE AND HEALTH INSUR", Value = "4" } ,
                   new SelectListItem { Text = "Palmetto GBA", Value = "5" },
                   new SelectListItem { Text = "Well Care HEALT INSURANCE OF ARIZON", Value = "6"} ,
                   new SelectListItem { Text = "WELLCARE OF TEXAS, INC.", Value = "7" }                     
                   
               }
                   , "Value", "Text", Model.TertiaryInsurance);%>
                        <%= Html.DropDownList("TertiaryInsurance", tertiaryInsurance, new { @id = "txtEdit_Patient_TertiaryInsurance", @class = "input_wrapper", @tabindex = "32" })%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="margin">
        <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr>
                    <th colspan='6'>
                        <label for="EthnicRaces">
                            Services Required</label>
                        <label class="error" for="EthnicRaces" generated="true">
                    </th>
                </tr>
            </thead>
            <tbody>
                <%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %>
                <tr class="firstrow">
                    <td>
                        <input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                        <input type="checkbox" value="0" class="radio" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("0")  ){ %>checked="checked"<% }%>'" />
                        SNV
                    </td>
                    <td>
                        <input type="checkbox" value="1" class="radio" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        HHA
                    </td>
                    <td>
                        <input type="checkbox" value="2" class="radio" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        PT
                    </td>
                    <td>
                        <input type="checkbox" value="3" class="radio" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        OT
                    </td>
                    <td>
                        <input type="checkbox" value="4" class="radio" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        SP
                    </td>
                    <td>
                        <input type="checkbox" value="5" class="radio" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        MSW
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="margin">
        <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr>
                    <th colspan='5'>
                        <label for="EthnicRaces">
                            DME Needed</label>
                        <label class="error" for="EthnicRaces" generated="true">
                    </th>
                </tr>
            </thead>
            <%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %>
            <tbody>
                <tr class="firstrow">
                    <td>
                        <input type="hidden" value=" " class="radio" name="DMECollection" />
                        <input type="checkbox" value="0" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("0")  ){ %>checked="checked"<% }%>'" />
                        Bedside Commode
                    </td>
                    <td>
                        <input type="checkbox" value="1" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Cane
                    </td>
                    <td>
                        <input type="checkbox" value="2" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Elevated Toilet Seat
                    </td>
                    <td>
                        <input type="checkbox" value="3" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Grab Bars
                    </td>
                    <td>
                        <input type="checkbox" value="4" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Hospital Bed
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" value="5" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Nebulizer
                    </td>
                    <td>
                        <input type="checkbox" value="6" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("6")  ){ %>checked="checked"<% }%>'" />
                        Oxygen
                    </td>
                    <td>
                        <input type="checkbox" value="7" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("7")  ){ %>checked="checked"<% }%>'" />
                        Tub/Shower Bench
                    </td>
                    <td>
                        <input type="checkbox" value="8" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("8")  ){ %>checked="checked"<% }%>'" />
                        Walker
                    </td>
                    <td>
                        <input type="checkbox" value="9" class="radio" name="DMECollection" '<% if(  DME!=null && DME.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        Wheelchair
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <input type="checkbox" value="10" id="New_Patient_DMEOther" class="radio" name="DMECollection" />
                        Other
                        <%=Html.TextBox("OtherDME", Model.OtherDME, new { @id = "New_Patient_OtherDME", @class = "text", @style = "display:none;" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="marginBreak">
    <b>Referral</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="AdmissionSource">
                            Admission Source:&nbsp;&nbsp;&nbsp;</label>
                        <%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource, new { @id = "txtEdit_Patient_AdmissionSource", @class = "input_wrapper", @tabindex = "64" })%>
                    </div>
                    <div class="row">
                        <label for="ReferralDate">
                            Referral Date:</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                        .Name("ReferralDate")
                                        .Value(Model.ReferralDate)
                                        .HtmlAttributes(new { @id = "txtEdit_Patient_PatientReferralDate", @class = "text date", @tabindex = "65" })
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            FirstName:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("OtherSourceFirstName", Model.OtherSourceFirstName, new { @id = "txtEdit_Patient_OtherSourceFirstName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "67" })%>
                    </div>
                    <div class="row">
                        <label for="FirstName">
                            LastName:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("OtherSourceLastName", Model.OtherSourceLastName, new { @id = "txtEdit_Patient_OtherSourceLastName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "68" })%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Emergency Contact</b>&nbsp;&nbsp;&nbsp;
    <input type="button" value="Add New" onclick="Patient.NewEmergencyContact('<%=Model.Id %>');" />
    <div class="rowBreak">
        <%= Html.Telerik().Grid<PatientEmergencyContact>()
        .Name("Edit_patient_EmergencyContact_Grid")
        .Columns(columns=>
        {
            columns.Bound(c => c.FirstName);
            columns.Bound(c => c.LastName);
            columns.Bound(c => c.PrimaryPhone);
            columns.Bound(c => c.AlternatePhone);
            columns.Bound(c => c.Relationship);
            columns.Bound(c => c.EmailAddress); 
            columns.Bound(c => c.Id)
                              .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\" Patient.EditEmergencyContact('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.DeleteEmergencyContact('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a>")
                              .Title("Action").Width(100);
        })
                    .DataBinding(dataBinding => dataBinding.Ajax().Select("GetEmergencyContacts", "Patient", new { PatientId = Model.Id }))        
        .Sortable()
        .Footer(false) 
        
        %>
    </div>
</div>
<div class="marginBreak">
    <b>Physician Contact</b>&nbsp;&nbsp;&nbsp;
    <input type="button" value="Add New" onclick="Patient.NewPhysicianContact('<%=Model.Id %>'); " />
    <div class="rowBreak">
        <%= Html.Telerik().Grid<AgencyPhysician>()
        .Name("Edit_patient_PhysicianContact_Grid")
        .Columns(columns=>
        {
            columns.Bound(c => c.FirstName);
            columns.Bound(c => c.LastName);
            columns.Bound(c => c.PhoneWork).Title("Work Phone");
            columns.Bound(c => c.FaxNumber);
            columns.Bound(c => c.EmailAddress);          
                 columns.Bound(r =>r.Id)
                                   .ClientTemplate(" <a href=\"javascript:void(0);\" onclick=\"Patient.DeletePhysicianContact('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a> ")
                                   .Title("Action").Width(100);
        })
                                            .DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Patient", new { PatientId = Model.Id }))        
        .Sortable()
        .Footer(false)         
        %>
    </div>
</div>
<div class="row">
    <div class="buttons buttonfix">
        <ul>
            <li>
                <input name="" type="submit" value="Save" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input name="" type="button" value="Reset" onclick="Patient.Edit();" /></li>
        </ul>
    </div>
</div>
<%} %>
