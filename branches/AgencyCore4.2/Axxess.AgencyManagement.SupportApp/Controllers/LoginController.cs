﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class LoginController : BaseController
    {
        #region Private Members / Constructor

        private readonly ILoginRepository loginRepository;
        
        public LoginController(IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
        }

        #endregion

        #region Login Account Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] Login login)
        {
            Check.Argument.IsNotNull(login, "login");

            var viewData = new JsonViewData();

            if (loginRepository.Find(login.EmailAddress) == null)
            {
                login.Role = Roles.AxxessAdmin.ToString();
                login.IsActive = true;
                login.IsLocked = false;
                if (!loginRepository.Add(login))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in adding the login.";
                }
                else
                {
                    var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}", login.Id)));
                    var bodyText = MessageBuilder.PrepareTextFrom(
                        "NewSupportUserConfirmation",
                        "firstname", login.DisplayName,
                        "encryptedQueryString", encryptedQueryString);
                    Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Welcome to Axxess Support", bodyText);
                        
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "New Login was added successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "The Login e-mail address provided is already in use.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid loginId)
        {
            return PartialView(loginRepository.Find(loginId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Login login)
        {
            Check.Argument.IsNotNull(login, "login");

            var viewData = new JsonViewData();

            var existing = loginRepository.Find(login.Id);
            if (existing != null)
            {
                existing.DisplayName = login.DisplayName;
                existing.IsAxxessAdmin = login.IsAxxessAdmin;
                existing.IsAxxessSupport = login.IsAxxessSupport;

                if (!loginRepository.Update(existing))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in updating the login.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Login was updated successfully";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid()
        {
            return View(new GridModel(loginRepository.GetAllByRole(Roles.AxxessAdmin)));
        }

        #endregion
    }
}
