﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.Membership.Logging;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;

        private ISupportMembershipService membershipService = Container.Resolve<ISupportMembershipService>();

        public AgencyController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAgencyService agencyService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserGrid(Guid agencyId)
        {
            return PartialView("Users/List", agencyRepository.GetById(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserList(Guid agencyId)
        {
            IList<UserSelection> userList = new List<UserSelection>();
            var users = userRepository.GetAgencyUsers(agencyId);
            users.ForEach(u =>
            {
                var login = loginRepository.Find(u.LoginId);
                if (login != null)
                {
                    userList.Add(new UserSelection
                    {
                        Id = u.Id,
                        LoginId = login.Id,
                        LastName = u.LastName,
                        FirstName = u.FirstName,
                        Credential = u.DisplayTitle,
                        DisplayName = u.IsDeprecated ? u.DisplayName + " [Deprecated]" : u.DisplayName,
                        EmailAddress = login.EmailAddress,
                        LoginCreated = login.CreatedFormatted,
                        IsLoginActive = login.IsActive
                    });
                }
            });

            return View(new GridModel(userList.OrderBy(u => u.DisplayName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUser(Guid loginId)
        {
            return PartialView("Users/Edit", loginRepository.Find(loginId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateUserEmail(Guid LoginId, string EmailAddress, string DisplayName)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Information could not be saved" };

            if (EmailAddress.IsNotNullOrEmpty() && EmailAddress.IsEmail())
            {
                if (!userService.IsEmailAddressInUse(EmailAddress.Trim()))
                {
                    var login = loginRepository.Find(LoginId);
                    if (login != null)
                    {
                        login.DisplayName = DisplayName;
                        login.EmailAddress = EmailAddress;
                        if (!loginRepository.Update(login))
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in saving the User information.";
                        }
                        else
                        {
                            var users = userRepository.GetUsersByLoginId(login.Id);
                            if (users != null && users.Count > 0)
                            {
                                users.ForEach(user =>
                                {
                                    user.Profile = user.ProfileData.IsNotNullOrEmpty() ? user.ProfileData.ToObject<UserProfile>() : null;
                                    if (user.Profile != null)
                                    {
                                        user.Profile.EmailWork = EmailAddress.Trim();
                                        user.ProfileData = user.Profile.ToXml();
                                        userRepository.Refresh(user);
                                    }
                                });
                            }
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User information was saved successfully";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "User Information was not found.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "This e-mail address is already in use.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "No valid E-mail Address provided.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateUserPassword(Guid LoginId, string Password, string Signature)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User Password/Signature could not be saved" };

            var login = loginRepository.Find(LoginId);
            if (login != null)
            {
                var passSalt = string.Empty;
                var passHash = string.Empty;

                var sigSalt = string.Empty;
                var sigHash = string.Empty;

                var saltedHash = new SaltedHash();
                if (Password.IsNotNullOrEmpty())
                {
                    saltedHash.GetHashAndSalt(Password, out passHash, out passSalt);
                    login.PasswordHash = passHash;
                    login.PasswordSalt = passSalt;
                }

                if (Signature.IsNotNullOrEmpty())
                {
                    saltedHash.GetHashAndSalt(Signature, out sigHash, out sigSalt);
                    login.SignatureHash = sigHash;
                    login.SignatureSalt = sigSalt;
                }

                if (!loginRepository.Update(login))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the password or signature.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Password/Signature was saved successfully. ";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User Login was not found.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianGrid(Guid agencyId)
        {
            return PartialView("Physicians", agencyRepository.GetById(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianList(Guid agencyId)
        {
            IList<PhysicianSelection> physicianList = new List<PhysicianSelection>();
            var physicians = physicianRepository.GetAgencyPhysicians(agencyId);
            physicians.ForEach(p =>
            {
                if (!p.LoginId.IsEmpty() && p.PhysicianAccess)
                {
                    var login = loginRepository.Find(p.LoginId);
                    if (login != null)
                    {
                        physicianList.Add(new PhysicianSelection
                        {
                            Id = p.Id,
                            LoginId = login.Id,
                            LastName = p.LastName,
                            FirstName = p.FirstName,
                            Credentials = p.Credentials,
                            DisplayName = p.IsDeprecated ? p.DisplayName + " [Deprecated]" : p.DisplayName,
                            EmailAddress = login.EmailAddress,
                            IsLoginActive = login.IsActive
                        });
                    }
                }
            });

            return View(new GridModel(physicianList.OrderBy(u => u.DisplayName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid)
            {
                if (!agencyService.CreateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView(agencyRepository.Get(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Agreement()
        {
            return PartialView("Agreement", loginRepository.Find(Current.User.Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HumanResource()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid)
            {
                if (!agencyService.UpdateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid()
        {
            var agencies = new List<AgencyLite>();
            var agencyList = agencyRepository.AllAgencies();
            agencyList.ForEach(a =>
            {
                agencies.Add(new AgencyLite { 
                    Id = a.Id, 
                    Name = a.Name,
                    Date = a.Created.ToString("MM/dd/yyyy"),
                    IsSuspended = a.IsSuspended,
                    IsDeprecated = a.IsDeprecated,
                    SalesPerson = loginRepository.GetLoginDisplayName(a.SalesPerson),
                    Trainer = loginRepository.GetLoginDisplayName(a.Trainer),
                    ContactPersonEmail = a.ContactPersonEmail,
                    ContactPersonDisplayName = a.ContactPersonDisplayName,
                    ContactPersonPhoneFormatted = a.ContactPersonPhoneFormatted,
                    City = a.MainLocation != null ? a.MainLocation.AddressCity : string.Empty,
                    State = a.MainLocation != null ? a.MainLocation.AddressStateCode : string.Empty,
                    ActionText = a.IsDeprecated ? "Restore" : "Suspend"
                });
            });
            return View(new GridModel(agencies));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Impersonate(Guid agencyId, Guid userId)
        {
            var url = membershipService.GetImpersonationUrl(agencyId, userId);
            if (url.IsNotNullOrEmpty())
            {
                return Redirect(url);
            }
            return RedirectToAction("Index", "Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ImpersonatePhysician(Guid loginId)
        {
            var url = membershipService.GetImpersonationUrl(loginId);
            if (url.IsNotNullOrEmpty())
            {
                return Redirect(url);
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLocation(Guid agencyId)
        {
            return PartialView("Location/New", agencyRepository.Get(agencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationGrid(Guid agencyId)
        {
            return PartialView("Location/List", agencyRepository.Get(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationList(Guid agencyId)
        {
            return View(new GridModel(agencyRepository.GetBranches(agencyId).OrderBy(a => a.Name)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid agencyId, Guid locationId)
        {
            return PartialView("Location/Edit", agencyRepository.FindLocation(agencyId, locationId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (!agencyService.UpdateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was edited successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not complete this action for this agency." };
            if (agencyRepository.ToggleDelete(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Templates have been uploaded for this agency.";
            }
            return Json(viewData);
        }

        #endregion

        #region Templates, Supplies

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Templates(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not load templates for this agency." };
            try
            {
                agencyRepository.InsertTemplates(agencyId);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Templates have been added for this agency.";
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Supplies(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not load supplies for this agency." };
            try
            {
                agencyRepository.InsertSupplies(agencyId);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Supplies have been added for this agency.";
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StandAloneContent(Guid agencyId, Guid locationId)
        {
            var agencyLocation = new AgencyLocation();
            if (!locationId.IsEmpty() && !agencyId.IsEmpty())
            {
                agencyLocation = agencyRepository.FindLocation(agencyId, locationId) ?? new AgencyLocation();
            }
            return PartialView("Location/StandAloneInfo", agencyLocation);
        }

        #endregion

        #region Notes

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNote(Guid agencyId)
        {
            return PartialView("Note/New", agencyRepository.Get(agencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddNote([Bind] CustomerNote note)
        {
            Check.Argument.IsNotNull(note, "note");

            var viewData = new JsonViewData();

            if (note.IsValid)
            {
                if (!agencyRepository.AddCustomerNote(note))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Note.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Note was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = note.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteGrid(Guid agencyId)
        {
            return PartialView("Note/List", agencyRepository.Get(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteList(Guid agencyId)
        {
            var notes = agencyRepository.GetCustomerNotes(agencyId);
            notes.ForEach(n =>
            {
                n.RepName = loginRepository.GetLoginDisplayName(n.LoginId);
            });
            return View(new GridModel(notes));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditNote(Guid agencyId, Guid noteId)
        {
            var note = agencyRepository.GetCustomerNote(agencyId, noteId);
            if (note != null)
            {
                note.RepName = loginRepository.GetLoginDisplayName(note.LoginId);
                return PartialView("Note/Edit", note);
            }
            return PartialView("Note/Edit", new CustomerNote {} );
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewNote(Guid agencyId, Guid noteId)
        {
            var note = agencyRepository.GetCustomerNote(agencyId, noteId);
            if (note != null)
            {
                note.RepName = loginRepository.GetLoginDisplayName(note.LoginId);
                return PartialView("Note/View", note);
            }
            return PartialView("Note/View", new CustomerNote { });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateNote([Bind] CustomerNote note)
        {
            Check.Argument.IsNotNull(note, "note");

            var viewData = new JsonViewData();

            if (note.IsValid)
            {
                var existingNote = agencyRepository.GetCustomerNote(note.AgencyId, note.Id);
                if (existingNote != null)
                {
                    existingNote.Comments = note.Comments;
                    existingNote.Modified = note.Modified;
                    if (!agencyRepository.UpdateCustomerNote(existingNote))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in updating the note.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Note was updated successfully";
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = note.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteNote(Guid agencyId, Guid noteId)
        {
            Check.Argument.IsNotEmpty(noteId, "noteId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not delete this note." };
            
            var existingNote = agencyRepository.GetCustomerNote(agencyId, noteId);
            if (existingNote != null)
            {
                existingNote.IsDeprecated = true;
                if (agencyRepository.UpdateCustomerNote(existingNote))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Note has been deleted.";
                }
            }
            return Json(viewData);
        }

        #endregion
    }
}
