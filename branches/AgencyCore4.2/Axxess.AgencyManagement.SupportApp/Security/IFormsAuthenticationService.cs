﻿namespace Axxess.AgencyManagement.SupportApp.Security
{
    public interface IFormsAuthenticationService
    {
        void SignOut();
        void RedirectToLogin();
        string LoginUrl { get; }
        void SignIn(string userName, bool rememberMe);
    }
}
