﻿namespace Axxess.AgencyManagement.SupportApp.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AgencyModule : Module
    {
        public override string Name
        {
            get { return "Agency"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
              "ImpersonateUser",
              "Agency/Impersonate/{agencyId}/{userId}",
              new { controller = this.Name, action = "Impersonate", agencyId = new IsGuid(), userId = new IsGuid() }
            );

            routes.MapRoute(
              "ImpersonatePhysician",
              "Agency/ImpersonatePhysician/{loginId}",
              new { controller = this.Name, action = "ImpersonatePhysician", loginId = new IsGuid() }
            );

            routes.MapRoute(
               "NewLocation",
               "Location/New/{agencyId}",
               new { controller = this.Name, action = "NewLocation", agencyId = new IsGuid() }
            );

            routes.MapRoute(
               "AddLocation",
               "Location/Add",
               new { controller = this.Name, action = "AddLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditLocation",
              "Location/Edit/{agencyId}/{locationId}",
              new { controller = this.Name, action = "EditLocation", agencyId = new IsGuid(), locationId = new IsGuid() }
            );

            routes.MapRoute(
              "UpdateLocation",
              "Location/Update",
              new { controller = this.Name, action = "UpdateLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteLocation",
              "Location/Delete",
              new { controller = this.Name, action = "DeleteLocation", id = new IsGuid() }
            );

            routes.MapRoute(
               "LocationGrid",
               "Location/Grid",
               new { controller = this.Name, action = "Locations", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "LocationList",
               "Location/List",
               new { controller = this.Name, action = "LocationList", id = UrlParameter.Optional }
            );
        }
    }
}
