﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PhysicianExporter : BaseExporter
    {
        private IList<AgencyPhysician> physicians;
        public PhysicianExporter(IList<AgencyPhysician> physicians)
            : base()
        {
            this.physicians = physicians;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Physicians";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Physicians");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Physicians");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("NPI");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Email");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("Phone Number");
            headerRow.CreateCell(5).SetCellValue("Alternate Number");
            headerRow.CreateCell(6).SetCellValue("Fax Number");
            headerRow.CreateCell(7).SetCellValue("Physician Access");
            headerRow.CreateCell(8).SetCellValue("PECOS");

            if (this.physicians.Count > 0)
            {
                int i = 2;
                this.physicians.ForEach(p =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(p.NPI);

                    dataRow.CreateCell(1).SetCellValue(p.DisplayName);
                    dataRow.CreateCell(2).SetCellValue(p.EmailAddress);
                    dataRow.CreateCell(3).SetCellValue(p.AddressFull);
                    dataRow.CreateCell(4).SetCellValue(p.PhoneWork.ToPhone());
                    dataRow.CreateCell(5).SetCellValue(p.PhoneAlternate.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(p.FaxNumber.ToPhone());
                    dataRow.CreateCell(7).SetCellValue(p.PhysicianAccess ? "Yes" : "No");
                    dataRow.CreateCell(8).SetCellValue(p.IsPecosVerified ? "Yes" : "No");
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Physicians: {0}", physicians.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(9);
        }
    }
}
