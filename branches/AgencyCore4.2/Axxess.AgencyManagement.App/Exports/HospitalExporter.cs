﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Extensions;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class HosptialExporter : BaseExporter
    {
        private IList<AgencyHospital> hospitals;
        public HosptialExporter(IList<AgencyHospital> hospitals)
            : base()
        {
            this.hospitals = hospitals;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Hospitals";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Hospitals");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Hospitals");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Contact Last Name");
            headerRow.CreateCell(2).SetCellValue("Contact First Name");
            headerRow.CreateCell(3).SetCellValue("Address 1");
            headerRow.CreateCell(4).SetCellValue("Address 2");
            headerRow.CreateCell(5).SetCellValue("City");
            headerRow.CreateCell(6).SetCellValue("State");
            headerRow.CreateCell(7).SetCellValue("Zip Code");
            headerRow.CreateCell(8).SetCellValue("Phone Number");
            headerRow.CreateCell(9).SetCellValue("Fax Number");

            if (this.hospitals.Count > 0)
            {
                int i = 2;
                this.hospitals.ForEach(h =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(h.Name);
                    dataRow.CreateCell(1).SetCellValue(h.ContactPersonLastName);
                    dataRow.CreateCell(2).SetCellValue(h.ContactPersonFirstName);
                    dataRow.CreateCell(3).SetCellValue(h.AddressLine1);
                    dataRow.CreateCell(4).SetCellValue(h.AddressLine2);
                    dataRow.CreateCell(5).SetCellValue(h.AddressCity);
                    dataRow.CreateCell(6).SetCellValue(h.AddressStateCode);
                    dataRow.CreateCell(7).SetCellValue(h.AddressZipCode);
                    dataRow.CreateCell(8).SetCellValue(h.Phone.ToPhone());
                    dataRow.CreateCell(9).SetCellValue(h.FaxNumber.ToPhone());
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agency Hospitals: {0}", hospitals.Count));
            }

            workBook.FinishWritingToExcelSpreadsheet(10);
        }
    }
}
