﻿namespace Axxess.AgencyManagement.App.Workflows
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Services;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    public class CreateEpisodeWorkflow
    {
        #region CreateEpisodeWorkflow Members

        private Patient patient { get; set; }
        private PatientEpisode Episode { get; set; }

        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;

        public CreateEpisodeWorkflow(Patient patient, PatientEpisode episode)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;
            this.Episode = episode;
            this.patientService = Container.Resolve<IPatientService>();
            this.billingService = Container.Resolve<IBillingService>();
            this.userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
            this.patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;
            this.billingRepository = Container.Resolve<IAgencyManagementDataProvider>().BillingRepository;
            this.physicianRepository = Container.Resolve<IAgencyManagementDataProvider>().PhysicianRepository;
            this.agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = this.message.IsNullOrEmpty();
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
            };
            var patientEpisode = patientService.CreateEpisode(patient.Id, Episode, null);

            work.Add(
                () =>
                {
                    patient.AgencyId = Current.AgencyId;
                    patientEpisode.AdmissionId = Episode.AdmissionId;
                    return patientService.AddEpisode(patientEpisode);
                },
                () =>
                {
                    patientRepository.DeleteEpisode(Current.AgencyId, patient.Id, patientEpisode.Id);
                },
                "System could not save the episode information.");
            
            if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
            {
                var insurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                if ((patient.PrimaryInsurance.ToInteger() > 0 && patient.PrimaryInsurance.ToInteger() < 1000) ||
                    (insurance != null && insurance.PayorType == (int)PayerTypes.MedicareHMO))
                {
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                    var rap = patientService.CreateRap(patient, patientEpisode, 0,physician);
                    rap.Id = patientEpisode.Id;
                    work.Add(
                        () =>
                        {
                            return billingService.AddRap(rap);
                        },
                        () =>
                        {
                            billingRepository.DeleteRap(Current.AgencyId, patient.Id, rap.Id);
                        },
                        "System could not save the episode information.");
                    var final = patientService.CreateFinal(patient, patientEpisode, 0,physician);
                    final.Id = patientEpisode.Id;
                    work.Add(
                        () =>
                        {
                            return billingService.AddFinal(final);
                        },
                        () =>
                        {
                            billingRepository.DeleteFinal(Current.AgencyId, patient.Id, final.Id);
                        },
                        "System could not save the episode information.");
                }
            }

            work.Perform();
        }

        #endregion
    }
}
