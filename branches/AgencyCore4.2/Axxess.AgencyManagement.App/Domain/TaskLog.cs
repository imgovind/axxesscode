﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Log.Enums;

    public class TaskLog
    {
        public Guid UserId { get; set; }
        public DateTime Date { get; set; }
        public Guid AgencyId { get; set; }
        public int Status { get; set; }
        public string Action { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }

        [XmlIgnore]
        public string StatusName
        {
            get
            {
                var actionDescription = string.Empty;
                if (this.Status != (int)ScheduleStatus.NoStatus)
                {
                    //if (Enum.IsDefined(typeof(ScheduleStatus), this.Status))
                    //{
                    //    return ((ScheduleStatus)Enum.ToObject(typeof(DisciplineTasks), this.Status)).GetDescription();
                    //}
                    //else
                    //{
                    //    return string.Empty;
                    //}
                    return Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? ((ScheduleStatus)this.Status).GetDescription() : string.Empty;

                }
                else
                {
                    if (this.Action.IsNotNullOrEmpty() && Enum.IsDefined(typeof(Actions), this.Action))
                    {
                        var action = (Actions)Enum.Parse(typeof(Actions), this.Action);
                       
                        if (action == Actions.Reassigned)
                        {
                            return action.GetDescription() + " " + this.Description;
                        }
                        //else if (action == Actions.EditAssessment)
                        //{
                        //    return action.GetDescription() + " (" + this.Description + " )";
                        //}
                        else
                        {
                            actionDescription += action.GetDescription();
                            //return action.GetDescription() + " (" + this.Description + " )";
                        }
                    }
                    //else
                    //{
                    //    return string.Empty;
                    //}
                }
                if (actionDescription.IsNotNullOrEmpty() && this.Description.IsNotNullOrEmpty())
                {
                    return actionDescription + " (" + this.Description + " )";
                }
                else
                {
                    return actionDescription;
                }
            }
        }

    }
}
