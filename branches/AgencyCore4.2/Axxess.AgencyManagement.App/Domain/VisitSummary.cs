﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;

    public class VisitSummary
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int VisitCount { get; set; }
    }
}
