﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public class MedicationProfileSnapshotViewData
    {
        public Agency Agency { get; set; }
        public Patient Patient { get; set; }
        public MedicationProfile MedicationProfile { get; set; }

        public Guid EpisodeId { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public string Allergies { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public string PhysicianName { get; set; }
        public string SignatureText { get; set; }
        public string PrimaryDiagnosis { get; set; }
        public string SecondaryDiagnosis { get; set; }
        public DateTime SignatureDate { get; set; }
    }
}