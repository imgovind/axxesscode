﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Domain;

    public class BillingHistoryViewData
    {
        public int Count { get; set; }
        public ClaimInfoSnapShotViewData ClaimInfo { get; set; }
    }
}
