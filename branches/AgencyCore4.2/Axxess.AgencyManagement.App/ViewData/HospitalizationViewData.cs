﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public class HospitalizationViewData
    {
        public Patient Patient { get; set; }
        public List<HospitalizationLog> Logs { get; set; }
    }
}