﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public class AllergyProfileViewData
    {
        public Guid EpisodeId { get; set; }
        public Patient Patient { get; set; }
        public AllergyProfile AllergyProfile { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianDisplayName { get; set; }
        public Agency Agency { get; set; }
    }
}