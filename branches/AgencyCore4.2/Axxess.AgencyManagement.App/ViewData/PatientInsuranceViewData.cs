﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class PatientInsuranceViewData
    {
        public Guid PatientId { get; set; }
        public int InsuranceId { get; set; }
    }
}
