﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Web;
    using Axxess.AgencyManagement.Domain;

    public class PatientContactInfoViewData : JsonViewData
    {
        public AddressBookEntry PatientContactInfo { get; set; }
        public List<AddressBookEntry> PatientContactInfoList { get; set; }
    }
}
