﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.Api.Contracts;

    class OasisAuditXml : BaseXml
    {
        #region Private Members

        private List<LogicalError> data = null;
        private readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        #endregion

        #region Constructor and Methods

        public OasisAuditXml(List<LogicalError> data, Patient patient) : base(PdfDocs.OasisAudit)
        {
            this.data = data;
            var agency = agencyRepository.Get(Current.AgencyId) ?? new Agency();
            //var location = agency != null && agency.MainLocation != null ? agency.MainLocation : new AgencyLocation();
            //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, data.EpisodeId, data.PatientId) ?? new PatientEpisode();
            //var cbsa = lookupRepository.CbsaCode(patient.AddressZipCode);
            //PPSStandard pps = lookupRepository.GetPPSStandardByYear(episode.StartDate.Year);
            //var hhrg = lookupRepository.GetHHRGByHIPPSCODE(data.HippsCode);
            //var wageIndex = cbsa != null && episode != null && episode.StartDate > DateTime.MinValue ? this.WageIndex(cbsa, episode.StartDate) : 0;
            //if (!this.Data.ContainsKey("M0100AssessmentType")) this.Data.Add(new KeyValuePair<string, Question>("M0100AssessmentType", new Question()));
            //this.Data["M0100AssessmentType"].Answer = data.TypeDescription;
            //if (!this.Data.ContainsKey("EpisodeStartDate")) this.Data.Add(new KeyValuePair<string, Question>("EpisodeStartDate", new Question()));
            //this.Data["EpisodeStartDate"].Answer = episode.StartDate.ToShortDateString();
            //if (!this.Data.ContainsKey("EpisodeEndDate")) this.Data.Add(new KeyValuePair<string, Question>("EpisodeEndDate", new Question()));
            //this.Data["EpisodeEndDate"].Answer = episode.EndDate.ToShortDateString();
            //if (!this.Data.ContainsKey("CBSACode")) this.Data.Add(new KeyValuePair<string, Question>("CBSACode", new Question()));
            //this.Data["CBSACode"].Answer = cbsa != null ? cbsa.CBSA : String.Empty;
            //if (!this.Data.ContainsKey("HHRG")) this.Data.Add(new KeyValuePair<string, Question>("HHRG", new Question()));
            //this.Data["HHRG"].Answer = hhrg != null ? hhrg.HHRG : String.Empty;
            //if (!this.Data.ContainsKey("Weight")) this.Data.Add(new KeyValuePair<string, Question>("Weight", new Question()));
            //this.Data["Weight"].Answer = hhrg != null ? hhrg.HHRGWeight.ToString() : String.Empty;
            //if (!this.Data.ContainsKey("WageIndex")) this.Data.Add(new KeyValuePair<string, Question>("WageIndex", new Question()));
            //this.Data["WageIndex"].Answer = cbsa != null ? wageIndex.ToString() : String.Empty;
            //if (!this.Data.ContainsKey("LaborPortion")) this.Data.Add(new KeyValuePair<string, Question>("LaborPortion", new Question()));
            //this.Data["LaborPortion"].Answer = pps != null && hhrg != null ? String.Format("${0:#0.00}", (hhrg.HHRGWeight * pps.Rate * pps.Labor * wageIndex)) : String.Empty;
            //if (!this.Data.ContainsKey("NonLabor")) this.Data.Add(new KeyValuePair<string, Question>("NonLabor", new Question()));
            //this.Data["NonLabor"].Answer = pps != null && hhrg != null ? string.Format("${0:#0.00}", (hhrg.HHRGWeight * pps.Rate * pps.NonLabor)) : String.Empty;
            //if (!this.Data.ContainsKey("NonRoutineSuppliesAmount")) this.Data.Add(new KeyValuePair<string, Question>("NonRoutineSuppliesAmount", new Question()));
            //this.Data["NonRoutineSuppliesAmount"].Answer = hhrg != null ? String.Format("${0:#0.00}", hhrg.SupplyAddOn) : String.Empty;
            //if (!this.Data.ContainsKey("TotalPayment")) this.Data.Add(new KeyValuePair<string, Question>("TotalPayment", new Question()));
            //this.Data["TotalPayment"].Answer = cbsa != null && hhrg != null && pps != null ? String.Format("${0:#0.00}", ((pps.Rate * hhrg.HHRGWeight) * ((pps.Labor * wageIndex + pps.NonLabor)) + hhrg.SupplyAddOn)) : String.Empty;
            //if (!this.Data.ContainsKey("HIPPS")) this.Data.Add(new KeyValuePair<string, Question>("HIPPS", new Question()));
            //this.Data["HIPPS"].Answer = data.HippsCode;
            //if (!this.Data.ContainsKey("OASISMatchingKey")) this.Data.Add(new KeyValuePair<string, Question>("OASISMatchingKey", new Question()));
            //this.Data["OASISMatchingKey"].Answer = data.ClaimKey;
            this.Init();
        }

        #endregion
    }
}