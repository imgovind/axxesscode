﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Extensions;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    class HospitalizationLogXml : BaseXml
    {
        #region Private Members
        private IDictionary<string, Question> Data = null;
        #endregion

        #region Constructor and Methods

        public HospitalizationLogXml(HospitalizationLog data) : base(PdfDocs.HospitalizationLog)
        {
            this.Data = data.ToDictionary();

            this.Data.Add("M0903LastHomeVisitDate", new Question() { Answer = data.LastHomeVisitDateFormatted });
            this.Data.Add("M0906DischargeDate", new Question() { Answer = data.HospitalizationDateFormatted });

            this.Init();
        }

        public override string GetData(string index)
        {
            return this.Data.AnswerOrEmptyString(index);
        }

        #endregion
    }
}