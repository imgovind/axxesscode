﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Enums;
    abstract class VisitNotePdf : AxxessPdf {
        private VisitNoteXml xml;
        protected String DocType;
        public VisitNotePdf(VisitNoteViewData data, int rev) : base() {
            PdfDoc type;
            switch (data.Type) {
                case "MSWAssessment": case "MSWDischarge": case "MSWEvaluationAssessment":
                    type = PdfDocs.MSWEval;
                    this.Init(data, type, rev);
                    break;
                case "MSWProgressNote":
                    type = PdfDocs.MSWProgress;
                    this.Init(data, type, rev);
                    break;
                case "MSWVisit":
                    type = PdfDocs.MSWVisit;
                    this.Init(data, type, rev);
                    break;
                case "PTDischarge":
                    type = PdfDocs.PTDischarge;
                    this.Init(data, type, rev);
                    break;
                case "PTReEvaluation":
                case "PTMaintenance":
                case "PTEvaluation": 
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTEval2;
                        rev = 0;
                    }
                    else
                        type = PdfDocs.PTEval;
                    this.Init(data, type, rev);
                    break;
                case "PTReassessment":
                    type = PdfDocs.PTReassessment;
                    this.Init(data, type, rev);
                    break;
                case "PTAVisit":
                case "PTVisit": 
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTVisit2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.PTVisit;
                    }
                    this.Init(data, type, rev);
                    break;
                case "OTReEvaluation":
                case "OTEvaluation":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.OTEval2;
                        rev = 0;
                    }else
                        type = PdfDocs.OTEval;
                    this.Init(data, type, rev);
                    break;
                case "OTDischarge":
                case "OTMaintenance":
                    type = PdfDocs.OTEval;
                    this.Init(data, type, rev);
                    break;
                case "OTReassessment":
                    type = PdfDocs.OTReassessment;
                    this.Init(data, type, rev);
                    break;
                case "OTVisit":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.OTVisit2;
                        rev = 0;
                    }else
                        type = PdfDocs.OTVisit;
                    this.Init(data, type, rev);
                    break;
                case "COTAVisit":
                    type = PdfDocs.OTVisit;
                    this.Init(data, type, rev);
                    break;
                case "STEvaluation":
                case "STReEvaluation":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STEval2;
                        rev = 0;
                    }
                    else
                        type = PdfDocs.STEval;
                    this.Init(data, type, rev);
                    break;
                case "STMaintenance": 
                    type = PdfDocs.STEval;
                    this.Init(data, type, rev);
                    break;
                case "STDischarge":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STDischarge;
                    }
                    else
                    {
                        type = PdfDocs.STEval;
                    }
                    this.Init(data, type, rev);
                    break;
                case "STReassessment":
                    type = PdfDocs.STReassessment;
                    this.Init(data, type, rev);
                    break;
                case "STVisit":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STVisit2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.STVisit;
                    }
                    
                    this.Init(data, type, rev);
                    break;
                case "DriverOrTransportationNote":
                    type = PdfDocs.TransportationLog;
                    this.Init(data, type, rev);
                    break;
            }
        }
        public VisitNotePdf(VisitNoteViewData data, PdfDoc type, int rev) : base() {
            this.Init(data, type, rev);
        }
        private void Init(VisitNoteViewData data, PdfDoc type, int rev) {
            if (rev > 0) this.xml = new VisitNoteXml(data, type, rev);
            else this.xml = new VisitNoteXml(data, type);
            this.SetType(type);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.Content(this.xml));
            this.SetMargins(this.Margins(data));
            var fieldMap = this.FieldMap(data);
            fieldMap[0].Add("surcharge", data != null && data.Questions != null && data.Questions.ContainsKey("Surcharge") && data.Questions["Surcharge"].Answer.IsNotNullOrEmpty() ? data.Questions["Surcharge"].Answer : "");
            fieldMap[0].Add("mileage", data != null && data.Questions != null && data.Questions.ContainsKey("AssociatedMileage") && data.Questions["AssociatedMileage"].Answer.IsNotNullOrEmpty() ? data.Questions["AssociatedMileage"].Answer : "");
            this.SetFields(fieldMap);
        }
        protected virtual IElement[] Content(VisitNoteXml xml) {
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            return content;
        }
        protected virtual List<Dictionary<String, String>> FieldMap(VisitNoteViewData data) {
            return new List<Dictionary<string, string>>();
        }
        protected virtual float[] Margins(VisitNoteViewData data) {
            return new float[] { 0, 0, 0, 0 };
        }
    }
}