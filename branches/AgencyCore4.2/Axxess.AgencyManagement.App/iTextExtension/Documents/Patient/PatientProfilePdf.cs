﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Domain;
    class PatientProfilePdf : AxxessPdf
    {
        public PatientProfilePdf(PatientProfile data)
        {
            this.SetType(PdfDocs.PatientProfile);
            var isDataExist = data != null && data.Patient != null;
            var patient = data != null ? data.Patient : new Patient();
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(isDataExist && patient.Comments.IsNotNullOrEmpty() ? patient.Comments : " ") };
            this.SetContent(content);
            this.SetMargins(new float[] { 555, 35, 35, 35 });
            var assessment = data.CurrentAssessment != null ? data.CurrentAssessment.ToDictionary() : new Dictionary<string, Question>();
            String[] ethnicities = isDataExist && patient.Ethnicities != null && patient.Ethnicities != string.Empty ? patient.Ethnicities.Split(';') : null;
            String race = string.Empty;
            if (ethnicities != null)
            {
                foreach (String ethnic in ethnicities)
                {
                    int result;
                    if (Int32.TryParse(ethnic, out result)) race += ((Race)Enum.ToObject(typeof(Race), (result))).GetDescription() + " ";
                }
            }
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(isDataExist ? patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : string.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : string.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                    : string.Empty)
                : string.Empty));
            fieldmap[0].Add("patient", (
                isDataExist ?
                    (patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.ToTitleCase() : string.Empty) +
                    (patient.AddressLine2.IsNotNullOrEmpty() ? patient.AddressLine2.ToTitleCase() + "\n" : "\n") +
                    (patient.AddressCity.IsNotNullOrEmpty() ? patient.AddressCity.ToTitleCase() + ", " : string.Empty) +
                    (patient.AddressStateCode.IsNotNullOrEmpty() ? patient.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                    (patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty) +
                    (patient.PhoneHome.IsNotNullOrEmpty() ? "\nPhone: " + patient.PhoneHome.ToPhone() : string.Empty)
                : string.Empty));
            fieldmap[0].Add("soc", isDataExist && patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : string.Empty);
            fieldmap[0].Add("bday", isDataExist && patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("sex", isDataExist && patient.Gender.IsNotNullOrEmpty() ? patient.Gender : string.Empty);
            fieldmap[0].Add("marital", isDataExist && patient.MaritalStatus.IsNotNullOrEmpty() ? patient.MaritalStatus : string.Empty);
            fieldmap[0].Add("office", data != null && data.Agency != null && location != null && location.Name.IsNotNullOrEmpty() ? location.Name.Clean() : string.Empty);
            fieldmap[0].Add("race", race);
            fieldmap[0].Add("height", string.Format("{0} {1}", isDataExist ? patient.Height.ToString() : string.Empty, isDataExist ? (patient.HeightMetric == 0 ? "in" : (patient.HeightMetric == 1 ? "cm" : string.Empty)) : string.Empty));
            fieldmap[0].Add("weight", string.Format("{0} {1}", isDataExist ? patient.Weight.ToString() : string.Empty, isDataExist ? (patient.WeightMetric == 0 ? "lb" : (patient.WeightMetric == 1 ? "kg" : string.Empty)) : string.Empty));
            fieldmap[0].Add("ssn", isDataExist && patient.SSN.IsNotNullOrEmpty() ? patient.SSN : string.Empty);
            fieldmap[0].Add("dnr", isDataExist && patient.IsDNR ? "Yes" : "No");
            fieldmap[0].Add("mr", isDataExist && patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("mcare", isDataExist && patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : string.Empty);
            fieldmap[0].Add("cert", isDataExist && data.CurrentEpisode != null && data.CurrentEpisode.StartDateFormatted.IsNotNullOrEmpty() && data.CurrentEpisode.EndDateFormatted.IsNotNullOrEmpty() ? data.CurrentEpisode.StartDateFormatted + " - " + data.CurrentEpisode.EndDateFormatted : string.Empty);
            fieldmap[0].Add("triage", isDataExist ? (patient.Triage == 1 ? "1. Life threatening (or potential) and requires ongoing medical treatment." : string.Empty) + (patient.Triage == 2 ? "2. Not life threatening but would suffer severe adverse effects if visit postponed." : string.Empty) + (patient.Triage == 3 ? "3. Visits could be postponed 24-48 hours without adverse effects." : string.Empty) + (patient.Triage == 4 ? "4. Visits could be postponed 72-96 hours without adverse effects." : string.Empty) : string.Empty);
            fieldmap[0].Add("refdate", isDataExist && patient.ReferralDate.IsValid() ? patient.ReferralDate.ToShortDateString().Clean() : string.Empty);
            fieldmap[0].Add("priins", isDataExist && patient.PrimaryInsuranceName.IsNotNullOrEmpty() ? patient.PrimaryInsuranceName.Clean() : string.Empty);
            fieldmap[0].Add("priinsnum", isDataExist && patient.PrimaryHealthPlanId.IsNotNullOrEmpty() ? patient.PrimaryHealthPlanId.Clean() : string.Empty);
            fieldmap[0].Add("secins", isDataExist && patient.SecondaryInsuranceName.IsNotNullOrEmpty() ? patient.SecondaryInsuranceName.Clean() : string.Empty);
            fieldmap[0].Add("secinsnum", isDataExist && patient.SecondaryHealthPlanId.IsNotNullOrEmpty() ? patient.SecondaryHealthPlanId.Clean() : string.Empty);
            fieldmap[0].Add("terins", isDataExist && patient.TertiaryInsuranceName.IsNotNullOrEmpty() ? patient.TertiaryInsuranceName.Clean() : string.Empty);
            fieldmap[0].Add("terinsnum", isDataExist && patient.TertiaryHealthPlanId.IsNotNullOrEmpty() ? patient.TertiaryHealthPlanId.Clean() : string.Empty);
          
            fieldmap[0].Add("ecname", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].DisplayName.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].PrimaryPhone.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].Relationship.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].Relationship.Clean() : string.Empty);
            
            fieldmap[0].Add("ecname2", isDataExist && patient.EmergencyContacts.Count > 1 && patient.EmergencyContacts[1].DisplayName.IsNotNullOrEmpty() ? patient.EmergencyContacts[1].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone2", isDataExist && patient.EmergencyContacts.Count > 1 && patient.EmergencyContacts[1].PrimaryPhone.IsNotNullOrEmpty() ? patient.EmergencyContacts[1].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation2", isDataExist && patient.EmergencyContacts.Count > 1 && patient.EmergencyContacts[1].Relationship.IsNotNullOrEmpty() ? patient.EmergencyContacts[1].Relationship.Clean() : string.Empty);
            
            fieldmap[0].Add("ecname3", isDataExist && patient.EmergencyContacts.Count > 2 && patient.EmergencyContacts[2].DisplayName.IsNotNullOrEmpty() ? patient.EmergencyContacts[2].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone3", isDataExist && patient.EmergencyContacts.Count > 2 && patient.EmergencyContacts[2].PrimaryPhone.IsNotNullOrEmpty() ? patient.EmergencyContacts[2].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation3", isDataExist && patient.EmergencyContacts.Count > 2 && patient.EmergencyContacts[2].Relationship.IsNotNullOrEmpty() ? patient.EmergencyContacts[2].Relationship.Clean() : string.Empty);
            
            fieldmap[0].Add("ecname4", isDataExist && patient.EmergencyContacts.Count > 3 && patient.EmergencyContacts[3].DisplayName.IsNotNullOrEmpty() ? patient.EmergencyContacts[3].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone4", isDataExist && patient.EmergencyContacts.Count > 3 && patient.EmergencyContacts[3].PrimaryPhone.IsNotNullOrEmpty() ? patient.EmergencyContacts[3].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation4", isDataExist && patient.EmergencyContacts.Count > 3 && patient.EmergencyContacts[3].Relationship.IsNotNullOrEmpty() ? patient.EmergencyContacts[3].Relationship.Clean() : string.Empty);
           
            fieldmap[0].Add("allergies", data != null && data.Allergies.IsNotNullOrEmpty() ? data.Allergies : string.Empty);
            fieldmap[0].Add("pharm", isDataExist && patient.PharmacyName.IsNotNullOrEmpty() ? patient.PharmacyName : string.Empty);
            fieldmap[0].Add("pharmphone", isDataExist && patient.PharmacyPhone.IsNotNullOrEmpty() ? patient.PharmacyPhone.ToPhone() : string.Empty);
            fieldmap[0].Add("pridiag", assessment != null && assessment.ContainsKey("M1020PrimaryDiagnosis") && assessment["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? assessment["M1020PrimaryDiagnosis"].Answer : string.Empty);
            fieldmap[0].Add("secdiag", assessment != null && assessment.ContainsKey("M1022PrimaryDiagnosis1") && assessment["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? assessment["M1022PrimaryDiagnosis1"].Answer : string.Empty);
            fieldmap[0].Add("clinician", data != null && data.Clinician.IsNotNullOrEmpty() ? data.Clinician : string.Empty);
            fieldmap[0].Add("caseman", isDataExist && patient.CaseManagerName.IsNotNullOrEmpty() ? patient.CaseManagerName : string.Empty);
            fieldmap[0].Add("startofcare", isDataExist && patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : string.Empty);
            fieldmap[0].Add("freq", data != null && data.Frequencies.IsNotNullOrEmpty() ? data.Frequencies : string.Empty);
            fieldmap[0].Add("phys", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            fieldmap[0].Add("physaddress", (
                data != null && data.Physician != null ?
                    (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1.ToTitleCase() : string.Empty) +
                    (data.Physician.AddressLine2.IsNotNullOrEmpty() ? data.Physician.AddressLine2.ToTitleCase() + "\n" : "\n") +
                    (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity.ToTitleCase() + ", " : string.Empty) +
                    (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                    (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode : string.Empty)
                : string.Empty));
            fieldmap[0].Add("physphone", data != null && data.Physician != null && data.Physician.PhoneWork.IsNotNullOrEmpty() ? data.Physician.PhoneWork.ToPhone() : string.Empty);
            fieldmap[0].Add("physfax", data != null && data.Physician != null && data.Physician.FaxNumber.IsNotNullOrEmpty() ? data.Physician.FaxNumber.ToPhone() : string.Empty);
            fieldmap[0].Add("physnpi", data != null && data.Physician != null && data.Physician.NPI.IsNotNullOrEmpty() ? data.Physician.NPI : string.Empty);
            fieldmap[0].Add("patientname", isDataExist ? (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToUpper() + "\n" : "\n") : string.Empty);
            var serviceText = string.Empty;
            string[] servicesRequired = isDataExist && patient.ServicesRequired.IsNotNullOrEmpty() ? patient.ServicesRequired.Split(';') : null;
            if (servicesRequired != null && servicesRequired.Length > 0)
            {
                foreach (var service in servicesRequired)
                {
                    switch (service)
                    {
                        case "0": serviceText += "SNV, "; break;
                        case "1": serviceText += "HHA, "; break;
                        case "2": serviceText += "PT, "; break;
                        case "3": serviceText += "OT, "; break;
                        case "4": serviceText += "ST, "; break;
                        case "5": serviceText += "MSW, "; break;
                    }
                }
                if (serviceText.Length > 0) serviceText = serviceText.Substring(0, serviceText.Length - 2);
            }
            fieldmap[0].Add("services", serviceText);
            var dmeText = string.Empty;
            string[] DME = isDataExist && patient.DME.IsNotNullOrEmpty() ? patient.DME.Split(';') : null;
            if (DME != null && DME.Length > 0)
            {
                foreach (var dme in DME)
                {
                    switch (dme)
                    {
                        case "0": dmeText += "Bedside Commode, "; break;
                        case "1": dmeText += "Cane, "; break;
                        case "2": dmeText += "Elevated Toilet Seat, "; break;
                        case "3": dmeText += "Grab Bars, "; break;
                        case "4": dmeText += "Hospital Bed, "; break;
                        case "5": dmeText += "Nebulizer, "; break;
                        case "6": dmeText += "Oxygen, "; break;
                        case "7": dmeText += "Tub/Shower Bench, "; break;
                        case "8": dmeText += "Walker, "; break;
                        case "9": dmeText += "Wheelchair, "; break;
                        case "10": dmeText += "Other, "; break;
                    }
                }
                if (dmeText.Length > 0) dmeText = dmeText.Substring(0, dmeText.Length - 2);
            }
            fieldmap[0].Add("dme", dmeText);
            this.SetFields(fieldmap);
        }
    }
}