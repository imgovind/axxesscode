﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Domain;
    using ViewData;
    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;

    public class PayrollService : IPayrollService
    {
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;

        public PayrollService(IAgencyManagementDataProvider agencyManagementDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        public bool MarkAsPaid(List<string> itemList)
        {
            var result = true;

            if (itemList != null && itemList.Count > 0)
            {
                itemList.ForEach(visit =>
                {
                    var visitArray = visit.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (visitArray != null && visitArray.Length == 3)
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, visitArray[0].ToGuid(), visitArray[1].ToGuid(), visitArray[2].ToGuid());
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.IsVisitPaid = true;
                            if (!patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                result = false;
                                return;
                            }
                            else
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.MarkedPaid, (DisciplineTasks)scheduleEvent.DisciplineTask, "This visit is marked as paid.");
                                }
                            }
                        }
                    }
                });
            }
            return result;
        }

        public bool MarkAsUnpaid(List<string> itemList)
        {
            var result = true;

            if (itemList != null && itemList.Count > 0)
            {
                itemList.ForEach(visit =>
                {
                    var visitArray = visit.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (visitArray != null && visitArray.Length == 3)
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, visitArray[0].ToGuid(), visitArray[1].ToGuid(), visitArray[2].ToGuid());
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.IsVisitPaid = false;
                            if (!patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                result = false;
                                return;
                            }
                            else
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.MarkedUnpaid, (DisciplineTasks)scheduleEvent.DisciplineTask, "The visit is marked as unpaid.");
                                }
                            }
                        }
                    }
                });
            }
            return result;
        }

        public List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status)
        {
            var list = new List<VisitSummary>();
            var userVisits = new Dictionary<Guid, int>();
            var allEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            if (allEpisodes != null && allEpisodes.Count > 0) {
                allEpisodes.ForEach(episode => {
                    if (episode.Schedule.IsNotNullOrEmpty()) {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && !s.UserId.IsEmpty() && s.EventDate.IsValidDate() && !s.IsMissedVisit && !s.IsDeprecated && s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter && (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW()) && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date).ToList();
                        bool tempStatus;
                        if (bool.TryParse(Status, out tempStatus)) scheduledEvents = scheduledEvents.Where(s => s.IsVisitPaid == tempStatus).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0) {
                            scheduledEvents.ForEach(s => {
                                if (!userVisits.ContainsKey(s.UserId)) userVisits.Add(s.UserId, 1);
                                else userVisits[s.UserId]++;
                            });
                        }
                    }
                });
                if (userVisits.Count > 0) {
                    userVisits.ForEach(uv => {
                        list.Add(new VisitSummary {
                            UserId = uv.Key,
                            UserName = UserEngine.GetName(uv.Key, Current.AgencyId),
                            VisitCount = uv.Value
                        });
                    });
                }
            }
            return list;
        }

        public List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status) {
            var list = new List<PayrollDetail>();
            var userVisits = new Dictionary<Guid, List<UserVisit>>();
            var allEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            if (allEpisodes != null && allEpisodes.Count > 0) {
                allEpisodes.ForEach(episode => {
                    if (episode.Schedule.IsNotNullOrEmpty()) {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            !s.EventId.IsEmpty() &&
                            !s.UserId.IsEmpty() &&
                            s.EventDate.IsValidDate() &&
                            !s.IsMissedVisit &&
                            !s.IsDeprecated &&
                            s.IsCompleted() &&
                            s.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter &&
                            (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW()) &&
                            s.EventDate.ToDateTime().Date >= startDate.Date &&
                            s.EventDate.ToDateTime().Date <= endDate.Date).ToList();
                        bool tempStatus;
                        if (bool.TryParse(Status, out tempStatus)) scheduledEvents = scheduledEvents.Where(s => s.IsVisitPaid == tempStatus).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0) {
                            scheduledEvents.ForEach(s => {
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                if (s.Comments.IsNotNullOrEmpty()) visitNote = s.Comments.Clean();
                                if (s.StatusComment.IsNotNullOrEmpty()) statusComments = s.StatusComment;
                                var userVisit = new UserVisit {
                                    Id = s.EventId,
                                    UserId = s.UserId,
                                    VisitRate = "$0.00",
                                    Url = s.Url,
                                    UserDisplayName = UserEngine.GetName(s.UserId, Current.AgencyId),
                                    Status = s.Status,
                                    Surcharge = s.Surcharge,
                                    StatusName = s.StatusName,
                                    PatientName = episode.PatientName,
                                    StatusComment = statusComments,
                                    TaskName = s.DisciplineTaskName,
                                    EpisodeId = s.EpisodeId,
                                    PatientId = s.PatientId,
                                    ScheduleDate = s.EventDate.ToZeroFilled(),
                                    VisitDate = s.VisitDate.ToZeroFilled(),
                                    IsMissedVisit = s.IsMissedVisit,
                                    TimeIn = s.TimeIn,
                                    TimeOut = s.TimeOut,
                                    IsVisitPaid = s.IsVisitPaid,
                                    AssociatedMileage = s.AssociatedMileage
                                };
                                if (!userVisits.ContainsKey(s.UserId)) userVisits.Add(s.UserId, new List<UserVisit> { userVisit });
                                else userVisits[s.UserId].Add(userVisit);
                            });
                        }
                    }
                });
            }
            if (userVisits.Count > 0) {
                foreach (KeyValuePair<Guid, List<UserVisit>> uv in userVisits) {
                    list.Add(new PayrollDetail {
                        Id = uv.Key,
                        Name = UserEngine.GetName(uv.Key, Current.AgencyId),
                        Visits = uv.Value,
                        StartDate = startDate,
                        EndDate = endDate,
                        PayrollStatus=Status
                    });
                }
            }
            return list;
        }

        public List<UserVisit> GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status)
        {
            var userVisits = new List<UserVisit>();
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            var allEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            if (allEpisodes != null && allEpisodes.Count > 0 && user != null)
            {
                var userRates = user.Rates.IsNotNullOrEmpty() ? user.Rates.ToObject<List<UserRate>>() : new List<UserRate>();
                allEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.Details.IsNotNullOrEmpty())
                    {
                        var detail = episode.Details.ToObject<EpisodeDetail>();
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.UserId == userId &&
                            s.EventDate.IsValidDate() &&
                            !s.IsMissedVisit &&
                            !s.IsDeprecated &&
                            s.IsCompleted() &&
                            s.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter &&
                            (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW()) &&
                            s.EventDate.ToDateTime().Date >= startDate.Date &&
                            s.EventDate.ToDateTime().Date <= endDate.Date).ToList();
                        bool tempStatus;
                        if (bool.TryParse(Status, out tempStatus)) scheduledEvents = scheduledEvents.Where(s => s.IsVisitPaid == tempStatus).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(s =>
                            {
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                if (s.Comments.IsNotNullOrEmpty()) visitNote = s.Comments.Clean();
                                if (s.StatusComment.IsNotNullOrEmpty()) statusComments = s.StatusComment;

                                var userRate = new UserRate();
                                if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty())
                                {
                                    userRate = userRates.FirstOrDefault(r => r.Insurance == detail.PrimaryInsurance && r.Id == s.DisciplineTask);
                                }
                                var userVisit = new UserVisit
                                {
                                    Id = s.EventId,
                                    UserId = s.UserId,
                                    VisitRate = userRate != null ? userRate.ToString() : "none",
                                    MileageRate = userRate != null && userRate.MileageRate > 0 ? string.Format("${0:#0.00} / mile", userRate.MileageRate) : "0",
                                    Url = s.Url,
                                    UserDisplayName = user.DisplayName,
                                    Status = s.Status,
                                    Surcharge = s.Surcharge.IsNotNullOrEmpty() && s.Surcharge.IsDouble() ? string.Format("${0:#0.00}", s.Surcharge.ToDouble()) : "0",
                                    StatusName = s.StatusName,
                                    PatientName = episode.PatientName,
                                    PatientIdNumber = episode.PatientIdNumber,
                                    StatusComment = statusComments,
                                    TaskName = s.DisciplineTaskName,
                                    EpisodeId = s.EpisodeId,
                                    PatientId = s.PatientId,
                                    VisitDate = s.VisitDate.ToZeroFilled(),
                                    ScheduleDate = s.EventDate.ToZeroFilled(),
                                    IsMissedVisit = s.IsMissedVisit,
                                    TimeIn = s.TimeIn,
                                    TimeOut = s.TimeOut,
                                    IsVisitPaid = s.IsVisitPaid,
                                    AssociatedMileage = s.AssociatedMileage
                                };
                                var visitPay = GetVisitPayment(userVisit, userRate);
                                userVisit.VisitPayment = string.Format("${0:#0.00}", visitPay);

                                userVisit.Total = s.Surcharge.IsNotNullOrEmpty() && s.Surcharge.IsDouble() ? s.Surcharge.ToDouble() + visitPay : visitPay;
                                userVisit.TotalPayment = string.Format("${0:#0.00}", userVisit.Total);

                                userVisits.Add(userVisit);
                            });
                        }
                    }
                });
            }
            return userVisits.OrderByDescending(v => v.ScheduleDate).ToList();
        }

        private static double GetVisitPayment(UserVisit visit, UserRate rate)
        {
            var total = 0.0;
            var hours = (double)visit.MinSpent / 60;
            if (visit != null && rate != null)
            {
                if (rate.Rate > 0)
                {
                    if (rate.RateType == (int)UserRateTypes.Hourly)
                    {
                        total = rate.Rate * hours;
                    }
                    else if (rate.RateType == (int)UserRateTypes.PerVisit)
                    {
                        total = rate.Rate;
                    }
                }
                if (visit.AssociatedMileage.IsNotNullOrEmpty() && visit.AssociatedMileage.IsDouble() && rate.MileageRate > 0)
                {
                    total += visit.AssociatedMileage.ToDouble() * rate.MileageRate;
                }
            }
            return total;
        }
    }
}
