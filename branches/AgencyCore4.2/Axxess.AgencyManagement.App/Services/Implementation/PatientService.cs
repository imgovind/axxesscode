﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using System.Linq;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Enums;
    using Common;
    using Domain;
    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Log.Domain;

    public class PatientService : IPatientService
    {
        #region Private Members

        private readonly IDrugService drugService;
        private readonly ILogRepository logRepository;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IReferralRepository referralRepository;
        #endregion

        #region Constructor

        public PatientService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider, IOasisCDataProvider oasisDataProvider, IAssessmentService assessmentService, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            this.assessmentService = assessmentService;
            this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
        }

        #endregion

        #region IPatientService Members

        public bool AddPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            patient.AdmissionId = admissionDateId;
            if (patientRepository.Add(patient, out patientOut))
            {
                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, PatientData = patientOut != null ? patientOut.ToXml() : string.Empty, StartOfCareDate = patient.StartofCareDate, IsActive = true, IsDeprecated = false, Status = patientOut.Status }))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, (patient.ShouldCreateEpisode && !patient.UserId.IsEmpty()) && patient.Status == (int)PatientStatus.Active ? string.Empty : "Pending for admission");
                    result = true;
                }
            }
            return result;
        }

        public bool EditPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                var oldAdmissionDateId = patientToEdit.AdmissionId;
                patient.AdmissionId = patientToEdit.AdmissionId.IsEmpty() ? admissionDateId : patientToEdit.AdmissionId;
                if (patientRepository.Edit(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientToEdit.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patientToEdit);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientToEdit.Id, oldAdmissionDateId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.StartOfCareDate = patientOut.StartofCareDate;
                            if (patient.Status == (int)PatientStatus.Discharged)
                            {
                                admissionData.DischargedDate = patientOut.DischargeDate;
                            }
                            admissionData.PatientData = patientOut.ToXml();
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdatePatientForPhotoRemove(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var photoId = patient.PhotoId;
            patient.PhotoId = Guid.Empty;
            if (patientRepository.Update(patient, out patientOut))
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientOut.Id, patientOut.AdmissionId);
                if (admissionData != null && patientOut != null)
                {
                    admissionData.PatientData = patientOut.ToXml();
                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                        result = true;
                    }
                    else
                    {
                        patient.PhotoId = photoId;
                        patientRepository.Update(patient);
                    }
                }
                else
                {
                    patient.PhotoId = photoId;
                    patientRepository.Update(patient);
                }
            }
            return result;
        }

        public bool DeletePatient(Guid Id)
        {
            var result = false;
            if (patientRepository.DeprecatedPatient(Current.AgencyId, Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, Id, Id.ToString(), LogType.Patient, LogAction.PatientDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AdmitPatient(PendingPatient pending)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            if (pending.Type == NonAdmitTypes.Patient)
            {
                var patient = patientRepository.Get(pending.Id, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                    var oldAdmissionDateId = patient.AdmissionId;
                    pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                    if (patientRepository.AdmitPatient(pending, out patientOut))
                    {
                        if (patientOut != null)
                        {
                            if (!pending.PrimaryPhysician.IsEmpty())
                            {
                                List<AgencyPhysician> physicians = patient.PhysicianContacts.ToList();
                                var physician = physicians.FirstOrDefault(p => p.Id == pending.PrimaryPhysician);
                                if (physician != null)
                                {
                                    if (!physician.Primary)
                                    {
                                        physicianRepository.SetPrimary(patient.Id, pending.PrimaryPhysician);
                                    }
                                }
                                else
                                {
                                    physicianRepository.Link(patient.Id, pending.PrimaryPhysician, true);
                                    physicianRepository.SetPrimary(patient.Id, pending.PrimaryPhysician);
                                }
                            }

                            if (oldAdmissionDateId.IsEmpty())
                            {
                                if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                {
                                    var medId = Guid.NewGuid();
                                    patient.EpisodeStartDate = pending.EpisodeStartDate;
                                    if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                        result = true;
                                    }
                                    else
                                    {
                                        patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                if (admissionData != null)
                                {
                                    var oldPatientData = admissionData.PatientData;
                                    var oldSoc = admissionData.StartOfCareDate;
                                    var oldAdmissionStatus = admissionData.Status;
                                    admissionData.PatientData = patientOut.ToXml();
                                    admissionData.StartOfCareDate = pending.StartofCareDate;
                                    admissionData.Status = (int)PatientStatus.Active;
                                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                    {
                                        var medId = Guid.NewGuid();
                                        patient.EpisodeStartDate = pending.EpisodeStartDate;
                                        if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                                            result = true;
                                        }
                                        else
                                        {
                                            admissionData.Status = oldAdmissionStatus;
                                            admissionData.PatientData = oldPatientData;
                                            admissionData.StartOfCareDate = oldSoc;
                                            patientRepository.UpdatePatientAdmissionDate(admissionData);
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                    {
                                        var medId = Guid.NewGuid();
                                        patient.EpisodeStartDate = pending.EpisodeStartDate;
                                        if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                            result = true;
                                        }
                                        else
                                        {
                                            patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        patientRepository.Update(patient);
                                    }
                                }
                            }
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                }
            }
            else if (pending.Type == NonAdmitTypes.Referral)
            {
                var referral = referralRepository.Get(Current.AgencyId, pending.Id);
                if (referral != null)
                {
                    pending.AdmissionId = admissionDateId;
                    if (patientRepository.AdmitPatient(pending, out patientOut))
                    {
                        if (patientOut != null)
                        {
                            var physicains = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
                            if (!pending.PrimaryPhysician.IsEmpty() && !physicains.Exists(p => p.Id == pending.PrimaryPhysician))
                            {
                                physicains.ForEach(p => { p.IsPrimary = false; });
                                physicains.Add(new Physician() { Id = pending.PrimaryPhysician, IsPrimary = true });
                            }
                            if (physicains != null && physicains.Count > 0)
                            {
                                physicains.ForEach(p =>
                                {
                                    physicianRepository.Link(referral.Id, p.Id, p.IsPrimary);
                                });
                            }
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Referral Admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                var medId = Guid.NewGuid();
                                patientOut.EpisodeStartDate = pending.EpisodeStartDate;
                                patientOut.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                                if (this.CreateMedicationProfile(patientOut, medId) && this.CreateEpisodeAndClaims(patientOut))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, referral.Id, referral.Id.ToString(), LogType.Patient, LogAction.ReferralAdmitted, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patientRepository.DeletePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
                                    referralRepository.UpdateModal(referral);
                                }
                            }
                            else
                            {
                                referralRepository.UpdateModal(referral);
                            }
                        }
                        else
                        {
                            referralRepository.UpdateModal(referral);
                        }
                    }
                }
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            var result = false;
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.NonAdmitPatient(pending, out patientOut))
                {
                    if (patient.AdmissionId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patientOut.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {

                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool SetPatientPending(Guid patientId)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Patient patientOut = null;
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Pending;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Set Pending.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patient.Status = oldStatus;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                patient.DischargeReasonId = dischargeReasonId;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, DischargeReasonId = dischargeReasonId, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            result = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, DischargeReasonId = dischargeReasonId, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }

                        }
                    }
                }
                if (result)
                {
                    result = DischargeAllMedicationOfPatient(patientId, dischargeDate);
                }
            }
            return result;
        }

        public bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReason)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                //patient.DischargeReason = dischargeReason;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            result = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)BillingStatus.ClaimCreated || final.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
                if (result)
                {
                    result = DischargeAllMedicationOfPatient(patientId, dischargeDate);
                }
            }
            return result;
        }

        public bool ActivatePatient(Guid patientId)
        {
            var result = false;
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Active;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (patientOut != null)
                    {
                        if (oldAdmissionDateId.IsEmpty())
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                            if (admissionData != null)
                            {
                                admissionData.PatientData = patientOut.ToXml();
                                admissionData.Status = patient.Status;
                                admissionData.StartOfCareDate = patient.StartofCareDate;
                                if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                        }
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }
                }
            }
            return result;
        }

        public bool ActivatePatient(Guid patientId, DateTime startOfCareDate)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            Patient patientOut = null;
            if (patient != null)
            {
                var oldStatus = patient.Status;
                var oldStartOfCareDate = patient.StartofCareDate;
                patient.Status = (int)PatientStatus.Active;
                patient.StartofCareDate = startOfCareDate;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = admissionDateId;
                if (patientRepository.Update(patient, out patientOut))
                {

                    if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                        result = true;
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.StartofCareDate = oldStartOfCareDate;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }

                    //else
                    //{
                    //    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                    //    if (admissionData != null && patientOut != null)
                    //    {
                    //        admissionData.PatientData = patientOut.ToXml();
                    //        admissionData.Status = patient.Status;
                    //        admissionData.StartOfCareDate = patient.StartofCareDate;
                    //        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    //        {
                    //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                    //            result = true;
                    //        }
                    //        else
                    //        {
                    //            patient.Status = oldStatus;
                    //            patient.StartofCareDate = oldStartOfCareDate;
                    //            patient.AdmissionId = oldAdmissionDateId;
                    //            patientRepository.Update(patient);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        patient.Status = oldStatus;
                    //        patient.StartofCareDate = oldStartOfCareDate;
                    //        patient.AdmissionId = oldAdmissionDateId;
                    //        patientRepository.Update(patient);
                    //    }
                    //}
                }
            }
            return result;
        }

        public bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles)
        {
            var result = false;

            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (httpFiles.Count > 0)
                {
                    HttpPostedFileBase file = httpFiles.Get("Photo1");
                    if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var photo = Image.FromStream(file.InputStream);
                        var resizedImageStream = ResizeAndEncodePhoto(photo, file.ContentType, false);
                        if (resizedImageStream != null)
                        {
                            var binaryReader = new BinaryReader(resizedImageStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                Patient patientOut = null;
                                var oldPhotoId = patient.PhotoId;
                                var admissionDateId = Guid.NewGuid();
                                patient.PhotoId = asset.Id;
                                var oldAdmissionDateId = patient.AdmissionId;
                                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                                if (patientRepository.Update(patient, out patientOut))
                                {
                                    if (oldAdmissionDateId.IsEmpty())
                                    {
                                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                            result = true;
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                        if (admissionData != null && patientOut != null)
                                        {
                                            admissionData.PatientData = patientOut.ToXml();
                                            admissionData.Status = patient.Status;
                                            admissionData.StartOfCareDate = patient.StartofCareDate;
                                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                            {
                                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                                result = true;
                                            }
                                            else
                                            {
                                                patient.PhotoId = oldPhotoId;
                                                patient.AdmissionId = oldAdmissionDateId;
                                                patientRepository.Update(patient);
                                            }
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool AddPrimaryEmergencyContact(Patient patient)
        {
            var result = false;
            if (patientRepository.AddEmergencyContact(patient.EmergencyContact) && patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, patient.EmergencyContact.Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, "The Contact is set as primary.");
                result = true;
            }
            return result;
        }

        public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            var admission = new PatientAdmissionDate();
            if (patient != null)
            {
                var admissionId = Guid.NewGuid();
                var oldAdmissionId = patient.AdmissionId;
                patient.AdmissionId = admissionId;
                admission = new PatientAdmissionDate
               {
                   Id = admissionId,
                   AgencyId = Current.AgencyId,
                   PatientId = patient.Id,
                   StartOfCareDate = patient.StartofCareDate,
                   DischargedDate = patient.DischargeDate,
                   Status = patient.Status,
                   IsActive = true,
                   IsDeprecated = false,
                   Created = DateTime.Now,
                   Modified = DateTime.Now,
                   PatientData = patient.ToXml()
               };
                if (patientRepository.Update(patient))
                {
                    if (patientRepository.AddPatientAdmissionDate(admission))
                    {
                        return admission;
                    }
                    else
                    {
                        patient.AdmissionId = oldAdmissionId;
                        patientRepository.Update(patient);
                    }
                }
            }
            return admission;
        }

        public bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            bool result = false;
            if (patient != null)
            {
                emergencyContact.PatientId = patientId;
                emergencyContact.AgencyId = Current.AgencyId;
                if (patientRepository.AddEmergencyContact(emergencyContact))
                {
                    if (emergencyContact.IsPrimary)
                    {
                        patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, emergencyContact.Id);
                    }
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.EmergencyContactAdded, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool EditEmergencyContact(PatientEmergencyContact emergencyContact)
        {
            bool result = false;
            if (patientRepository.EditEmergencyContact(Current.AgencyId, emergencyContact))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, emergencyContact.PatientId, emergencyContact.PatientId.ToString(), LogType.Patient, LogAction.EmergencyContactEdited, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                result = true;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            if (patientRepository.DeleteEmergencyContact(Id, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.EmergencyContactDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public PatientProfile GetProfile(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var patientProfile = new PatientProfile();
                patientProfile.Patient = patient;
                patientProfile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

                patientProfile.Allergies = GetAllergies(patientId);
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    patientProfile.Physician = physician;
                }
                patient.EmergencyContacts = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId);
                if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
                {
                    patientProfile.EmergencyContact = patient.EmergencyContacts.OrderByDescending(e => e.IsPrimary).FirstOrDefault();
                }

                //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                //{
                //    patientProfile.Physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                //}
                //var emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patientId);
                //if (emergencyContact != null)
                //{
                //    patient.EmergencyContact = emergencyContact;
                //}
                //if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
                //{
                //    patientProfile.EmergencyContact = patient.EmergencyContacts.FirstOrDefault(p => p.IsPrimary);
                //}
                if (!patient.CaseManagerId.IsEmpty())
                {
                    patient.CaseManagerName = UserEngine.GetName(patient.CaseManagerId, Current.AgencyId);
                }
                if (!patient.UserId.IsEmpty())
                {
                    patientProfile.Clinician = UserEngine.GetName(patient.UserId, Current.AgencyId);
                }

                SetInsurance(patient);

                var episode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                if (episode != null)
                {
                    var freq = this.GetPatientEpisodeWithFrequency(episode.Id, patient.Id);
                    patientProfile.Frequencies = freq.Detail.FrequencyList;
                    patientProfile.CurrentEpisode = episode;
                    patientProfile.CurrentAssessment = assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                }
                return patientProfile;
            }

            return null;
        }

        public string GetAllergies(Guid patientId)
        {
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                return allergyProfile.ToString();
            }
            return string.Empty;
        }

        public bool CreateMedicationProfile(Patient patient, Guid medId)
        {
            var medicationProfile = new MedicationProfile { AgencyId = Current.AgencyId, Id = medId, Medication = new List<Medication>().ToXml(), PharmacyName = patient.PharmacyName, PharmacyPhone = patient.PharmacyPhone, PatientId = patient.Id, Created = DateTime.Now, Modified = DateTime.Now };
            if (patientRepository.AddNewMedicationProfile(medicationProfile))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, medicationProfile.Id.ToString(), LogType.MedicationProfile, LogAction.MedicationProfileAdded, string.Empty);
                return true;
            }
            return false;
        }

        public bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    medication.Id = Guid.NewGuid();
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                    if (medicationProfile.Medication.IsNullOrEmpty())
                    {
                        var newList = new List<Medication>() { medication };
                        medicationProfile.Medication = newList.ToXml();
                    }
                    else
                    {
                        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                        existingList.Add(medication);
                        medicationProfile.Medication = existingList.ToXml();
                    }
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
                {
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == medication.Id))
                    {
                        var exisitingMedication = existingList.Single(m => m.Id == medication.Id);
                        if (exisitingMedication != null)
                        {
                            var logAction = LogAction.MedicationUpdated;
                            if (exisitingMedication.StartDate != medication.StartDate || exisitingMedication.Route != medication.Route || exisitingMedication.Frequency != medication.Frequency || exisitingMedication.MedicationDosage != medication.MedicationDosage)
                            {
                                medication.Id = Guid.NewGuid();
                                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                                medication.MedicationType = new MedicationType { Value = "C", Text = MedicationTypeEnum.C.GetDescription() };
                                medication.MedicationCategory = exisitingMedication.MedicationCategory;
                                existingList.Add(medication);

                                exisitingMedication.MedicationCategory = "DC";
                                exisitingMedication.DCDate = DateTime.Now;
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                                logAction = LogAction.MedicationUpdatedWithDischarge;
                            }
                            else
                            {
                                exisitingMedication.IsLongStanding = medication.IsLongStanding;
                                exisitingMedication.MedicationType = medication.MedicationType;
                                exisitingMedication.Classification = medication.Classification;
                                if (exisitingMedication.MedicationCategory == "DC")
                                {
                                    exisitingMedication.DCDate = medication.DCDate;
                                    logAction = LogAction.MedicationDischarged;
                                }
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            }

                            if (patientRepository.UpdateMedication(medicationProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                var medication = existingMedications != null ? existingMedications.SingleOrDefault(m => m.Id == medicationId) : null;
                if (medication != null)
                {
                    var logAction = new LogAction();
                    if (medicationCategory.IsEqual(MedicationCategoryEnum.DC.ToString()))
                    {
                        medication.DCDate = dischargeDate;
                        medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        logAction = LogAction.MedicationDischarged;
                    }
                    else
                    {
                        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                        logAction = LogAction.MedicationActivated;
                    }
                    medication.LastChangedDate = DateTime.Now;
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool DischargeAllMedicationOfPatient(Guid patientId, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingMedications.Count > 0)
                {
                    var logAction = new LogAction();
                    existingMedications.ForEach((Medication medication) =>
                    {
                        if (medication.MedicationCategory == MedicationCategoryEnum.Active.ToString())
                        {
                            medication.DCDate = dischargeDate;
                            medication.LastChangedDate = DateTime.Now;
                            medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                            logAction = LogAction.MedicationDischarged;
                        }

                    });
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteMedication(Guid medicationProfileId, Guid medicationId)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medicationId))
                {
                    existingList.RemoveAll(m => m.Id == medicationId);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                }
                if (patientRepository.UpdateMedication(medicationProfile))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileHistory.ProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                medicationProfileHistory.Id = Guid.NewGuid();
                medicationProfileHistory.UserId = Current.UserId;
                medicationProfileHistory.AgencyId = Current.AgencyId;
                medicationProfileHistory.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                medicationProfileHistory.Created = DateTime.Now;
                medicationProfileHistory.Modified = DateTime.Now;
                medicationProfileHistory.Medication = medicationProfile.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList().ToXml();
                if (medicationProfileHistory.PharmacyPhoneArray != null && medicationProfileHistory.PharmacyPhoneArray.Count == 3)
                {
                    medicationProfileHistory.PharmacyPhone = medicationProfileHistory.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                var physician = physicianRepository.Get(medicationProfileHistory.PhysicianId, Current.AgencyId);
                if (physician != null)
                {
                    medicationProfileHistory.PhysicianData = physician.ToXml();
                }
                if (patientRepository.AddNewMedicationHistory(medicationProfileHistory))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationProfileSigned, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationHistoryForPatient(patientId, Current.AgencyId);
            if (medProfile != null)
            {
                medProfile.ForEach(m =>
                {
                    if (m.UserId != Guid.Empty)
                    {
                        m.UserName = UserEngine.GetName(m.UserId, Current.AgencyId);
                    }
                }
                );
            }
            return medProfile.OrderByDescending(m => m.SignedDate.ToShortDateString().ToOrderedDate()).ToList();
        }

        public List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory)
        {
            var medicationHistory = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var medicationList = new List<Medication>();
            if (medicationHistory != null)
            {
                medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == medicationCategory).ToList();
            }
            return medicationList;
        }

        public AllergyProfileViewData GetAllergyProfilePrint(Guid Id)
        {
            var profile = new AllergyProfileViewData();
            var allergies = patientRepository.GetAllergyProfileByPatient(Id, Current.AgencyId);
            if (allergies != null) profile.AllergyProfile = allergies;
            var patient = patientRepository.GetPatientOnly(Id, Current.AgencyId);
            if (patient != null) profile.Patient = patient;
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            if (agency != null) profile.Agency = agency;
            return profile;
        }

        public MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid patientId)
        {
            var med = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var profile = new MedicationProfileSnapshotViewData();
            if (med != null)
            {
                profile.MedicationProfile = med;
                profile.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                profile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                profile.Allergies = GetAllergies(patientId);
                if (profile.Patient != null)
                {
                    //if (profile.Patient.PhysicianContacts != null && profile.Patient.PhysicianContacts.Count > 0)
                    //{
                    //    var physician = profile.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    //    if (physician != null)
                    //    {
                    //        profile.PhysicianName = physician.DisplayName;
                    //    }
                    //}
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        profile.PhysicianName = physician.DisplayName;
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty()) profile.PharmacyName = med.PharmacyName;
                    else profile.PharmacyName = profile.Patient.PharmacyName;
                    if (med.PharmacyPhone.IsNotNullOrEmpty()) profile.PharmacyPhone = med.PharmacyPhone;
                    else profile.PharmacyPhone = profile.Patient.PharmacyPhone;
                    var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                    if (currentEpisode != null)
                    {
                        profile.EpisodeId = currentEpisode.Id;
                        profile.StartDate = currentEpisode.StartDate;
                        profile.EndDate = currentEpisode.EndDate;
                        if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                        {
                            var assessment = assessmentService.GetEpisodeAssessment(currentEpisode.Id, patientId);
                            if (assessment != null)
                            {
                                var questions = assessment.ToDictionary();
                                if (questions != null)
                                {
                                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                                    {
                                        profile.PrimaryDiagnosis = questions["M1020PrimaryDiagnosis"].Answer;
                                    }
                                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                                    {
                                        profile.SecondaryDiagnosis = questions["M1022PrimaryDiagnosis1"].Answer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return profile;
        }

        public DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected)
        {
            var viewData = new DrugDrugInteractionsViewData();
            if (drugsSelected != null && drugsSelected.Count > 0)
            {
                viewData.DrugDrugInteractions = drugService.GetDrugDrugInteractions(drugsSelected);
            }
            viewData.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

            return viewData;
        }

        public MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid Id)
        {
            var viewData = new MedicationProfileSnapshotViewData();
            var snapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (snapShot != null)
            {
                viewData.MedicationProfile = snapShot.ToProfile();
                viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                viewData.Patient = patientRepository.GetPatientOnly(snapShot.PatientId, Current.AgencyId);
                viewData.Allergies = snapShot.Allergies;
                viewData.PrimaryDiagnosis = snapShot.PrimaryDiagnosis;
                viewData.SecondaryDiagnosis = snapShot.SecondaryDiagnosis;
                AgencyPhysician physician = null;
                if (snapShot.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = snapShot.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        viewData.PhysicianName = physician.DisplayName;
                    }
                    else
                    {
                        if (!snapShot.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                viewData.PhysicianName = physician.DisplayName;
                            }
                        }
                    }
                }
                else
                {
                    if (!snapShot.PhysicianId.IsEmpty())
                    {
                        physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            viewData.PhysicianName = physician.DisplayName;
                        }
                    }
                }

                if (snapShot.SignatureText.IsNotNullOrEmpty())
                {
                    viewData.SignatureText = snapShot.SignatureText;
                    viewData.SignatureDate = snapShot.SignedDate;
                }
                else
                {
                    if (!snapShot.UserId.IsEmpty())
                    {
                        var displayName = UserEngine.GetName(snapShot.UserId, Current.AgencyId);
                        if (displayName.IsNotNullOrEmpty())
                        {
                            viewData.SignatureText = string.Format("Electronically Signed by: {0}", displayName);
                            viewData.SignatureDate = snapShot.SignedDate;
                        }
                    }
                }
                if (viewData.Patient != null)
                {
                    if (snapShot.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = snapShot.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }

                    if (snapShot.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = snapShot.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }

                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, snapShot.EpisodeId, snapShot.PatientId);
                    if (episode != null)
                    {
                        viewData.EpisodeId = episode.Id;
                        viewData.StartDate = episode.StartDate;
                        viewData.EndDate = episode.EndDate;
                    }
                }
            }
            return viewData;
        }

        public bool LinkPhysicians(Patient patient)
        {
            var result = true;
            if (patient != null && patient.AgencyPhysicians.Any())
            {
                int i = 0;
                bool isPrimary = false;
                foreach (var agencyPhysicianId in patient.AgencyPhysicians)
                {
                    if (i == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    if (!agencyPhysicianId.IsEmpty() && !physicianRepository.Link(patient.Id, agencyPhysicianId, isPrimary))
                    {
                        result = false;
                        break;
                    }
                    i++;
                }
                if (i > 0 && result)
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PhysicianLinked, "One of the physician is set as primary.");
                }
            }
            return result;
        }

        public bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary)
        {
            if (physicianRepository.Link(patientId, physicianId, isPrimary))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianLinked, isPrimary ? "Primary" : string.Empty);
                return true;
            }
            return false;
        }

        public bool UnlinkPhysician(Guid patientId, Guid physicianId)
        {
            if (patientRepository.DeletePhysicianContact(physicianId, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianUnLinked, string.Empty);
                return true;
            }
            return false;
        }

        public void AddPhysicianOrderUserAndScheduleEvent(PhysicianOrder physicianOrder, out PhysicianOrder physicianOrderOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = physicianOrder.Id,
                UserId = physicianOrder.UserId,
                PatientId = physicianOrder.PatientId,
                EpisodeId = physicianOrder.EpisodeId,
                Status = physicianOrder.Status.ToString(),
                Discipline = Disciplines.Nursing.ToString(),
                EventDate = physicianOrder.OrderDate.ToShortDateString(),
                VisitDate = physicianOrder.OrderDate.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.PhysicianOrder,
                IsOrderForNextEpisode = physicianOrder.IsOrderForNextEpisode
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, physicianOrder.EpisodeId, physicianOrder.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.PhysicianOrder);
            }

            physicianOrder.EpisodeId = newScheduleEvent.EpisodeId;
            physicianOrderOut = physicianOrder;
        }

        public bool ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType)
        {
            var result = false;
            var shouldUpdateEpisode = false;
            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PhysicianOrder physicianOrder = null;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null) userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                var description = string.Empty;
                physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                if (physicianOrder != null)
                {
                    if (actionType == "Approve")
                    {
                        physicianOrder.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                        physicianOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        physicianOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                shouldUpdateEpisode = true;
                                description = "Returned By: " + Current.UserFullName;
                            }
                        }
                    }
                    else if (actionType == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)physicianOrder.Status, DisciplineTasks.PhysicianOrder, description);
                        }
                    }
                }
            }
            return result;
        }

        public List<Order> GetOrdersFromScheduleEvents(Guid patientId, DateTime startDate, DateTime endDate, List<ScheduleEvent> schedules)
        {
            var orders = new List<Order>();
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, physicianOrdersIds, startDate, endDate);
                    physicianOrders.ForEach(po =>
                    {
                        var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                        var deleteUrl = !Current.IfOnlyRole(AgencyRoles.Auditor) ? string.Format(" | <a href=\"javaScript:void(0);\" onclick=\"Patient.DeleteOrder('{0}','{1}','{2}');\">Delete</a>", po.Id, po.PatientId, po.EpisodeId) : string.Empty;
                        orders.Add(new Order
                        {
                            Id = po.Id,
                            PatientId = po.PatientId,
                            EpisodeId = po.EpisodeId,
                            Type = OrderType.PhysicianOrder,
                            Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                            Number = po.OrderNumber,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                            PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true) + deleteUrl,
                            CreatedDate = po.OrderDateFormatted.ToZeroFilled(),
                            ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue,
                            SendDate = po.Status == (int)ScheduleStatus.OrderSentToPhysician || po.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || po.Status == (int)ScheduleStatus.OrderSavedByPhysician ? po.SentDate : DateTime.MinValue,
                            Status = po.Status
                        });
                    });
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPatientPlanofCares(Current.AgencyId, patientId, planofCareOrdersIds);
                    planofCareOrders.ForEach(poc =>
                    {
                        var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485,
                                Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() ? (poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate) : DateTime.MinValue,
                                SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? poc.SentDate : DateTime.MinValue,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPatientPlanofCaresStandAlones(Current.AgencyId, patientId, planofCareStandAloneOrdersIds);
                    planofCareStandAloneOrders.ForEach(poc =>
                    {
                        var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485StandAlone,
                                Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() ? poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate : DateTime.MinValue,
                                SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? poc.SentDate : DateTime.MinValue,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPatientFaceToFaceEncounterOrders(Current.AgencyId, patientId, faceToFaceEncounterOrdersIds);
                    faceToFaceEncounters.ForEach(ffe =>
                    {
                        var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = ffe.Id,
                                PatientId = ffe.PatientId,
                                EpisodeId = ffe.EpisodeId,
                                Type = OrderType.FaceToFaceEncounter,
                                Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                Number = ffe.OrderNumber,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() ? ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate : DateTime.MinValue,
                                SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? ffe.RequestDate : DateTime.MinValue,
                                Status = ffe.Status
                            });
                        }
                    });
                }

                var evalOrdersSchedule = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = evalOrdersSchedule.SingleOrDefault(s => s.EventId == eval.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    PatientId = eval.PatientId,
                                    EpisodeId = eval.EpisodeId,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = evnt.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() ? eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate : DateTime.MinValue,
                                    SendDate = evnt.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? eval.SentDate : DateTime.MinValue,
                                    Status = eval.Status,

                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return GetOrdersFromScheduleEvents(patientId, startDate, endDate, patientRepository.GetPatientOrderScheduleEvents(Current.AgencyId, patientId, startDate, endDate));
        }

        public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId)
        {
            var orders = new List<Order>();
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var schedules = new List<ScheduleEvent>();
                if (episode.Schedule.IsNotNullOrEmpty())
                {
                    var currentSchedules = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && !s.IsOrderForNextEpisode).ToList();
                    if (currentSchedules != null && currentSchedules.Count > 0)
                    {
                        schedules.AddRange(currentSchedules);
                    }
                    var previousEpisode = patientRepository.GetPreviousEpisodeFluent(Current.AgencyId, patientId, episode.StartDate);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var previousSchedules = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= previousEpisode.StartDate.Date && s.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && s.IsOrderForNextEpisode).ToList();
                        if (previousSchedules != null && previousSchedules.Count > 0)
                        {
                            schedules.AddRange(previousSchedules);
                        }
                    }
                }
                orders = GetOrdersFromScheduleEvents(episode.PatientId, episode.StartDate, episode.EndDate, schedules);
            }
            return orders;
        }

        public bool DeletePhysicianOrder(Guid orderId, Guid patientId, Guid episodeId)
        {
            bool result = false;
            try
            {
                var physicianOrder = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                if (physicianOrder != null && physicianOrder.EpisodeId == episodeId && physicianOrder.PatientId == patientId)
                {
                    if (patientRepository.MarkOrderAsDeleted(orderId, patientId, Current.AgencyId, true))
                    {
                        if (!physicianOrder.EpisodeId.IsEmpty())
                        {
                            var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, physicianOrder.EpisodeId, patientId, orderId);
                            if (scheduleEvent != null)
                            {
                                if (patientRepository.DeleteScheduleEvent(Current.AgencyId, physicianOrder.EpisodeId, patientId, orderId, (int)DisciplineTasks.PhysicianOrder))
                                {
                                    if (!scheduleEvent.UserId.IsEmpty())
                                    {
                                        userRepository.RemoveScheduleEvent(Current.AgencyId, patientId, orderId, scheduleEvent.UserId);
                                    }
                                    Auditor.Log(physicianOrder.EpisodeId, physicianOrder.PatientId, physicianOrder.Id, Actions.Deleted, DisciplineTasks.PhysicianOrder);
                                }
                            }
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public PhysicianOrder GetOrderPrint()
        {
            var order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrder(orderId, patientId, Current.AgencyId);
            if (order == null) order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            order.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
            {
                var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    order.Physician = physician;
                }
                else
                {
                    order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                }
            }
            else
            {
                order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
            }

            order.Allergies = GetAllergies(patientId);
            var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
            if (episode != null)
            {
                if (order.IsOrderForNextEpisode)
                {
                    order.EpisodeStartDate = episode.EndDate.AddDays(1).ToShortDateString();
                    order.EpisodeEndDate = episode.EndDate.AddDays(60).ToShortDateString();
                }
                else
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
                if (order.Patient != null)
                {
                    order.Patient.StartofCareDate = episode.StartOfCareDate;
                }
            }
            return order;
        }

        public void AddCommunicationNoteUserAndScheduleEvent(CommunicationNote communicationNote, out CommunicationNote communicationNoteOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = communicationNote.Id,
                UserId = communicationNote.UserId,
                EpisodeId = communicationNote.EpisodeId,
                PatientId = communicationNote.PatientId,
                Status = communicationNote.Status.ToString(),
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                EventDate = communicationNote.Created.ToShortDateString(),
                VisitDate = communicationNote.Created.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.CommunicationNote
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.CommunicationNote);
            }
            communicationNote.EpisodeId = newScheduleEvent.EpisodeId;
            communicationNoteOut = communicationNote;
        }

        public bool ProcessCommunicationNotes(string button, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            UserEvent userEvent = null;
            ScheduleEvent scheduleEvent = null;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (communicationNote != null)
                {
                    if (!communicationNote.EpisodeId.IsEmpty())
                    {
                        scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, communicationNote.EpisodeId, patientId, eventId);
                        if (scheduleEvent != null) userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                    }
                    var description = string.Empty;
                    var action = new Actions();
                    if (button == "Approve")
                    {
                        communicationNote.Status = ((int)ScheduleStatus.NoteCompleted);
                        communicationNote.Modified = DateTime.Now;
                        if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                                action = Actions.Approved;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        communicationNote.Status = ((int)ScheduleStatus.NoteReturned);
                        communicationNote.Modified = DateTime.Now;
                        communicationNote.SignatureText = string.Empty;
                        communicationNote.SignatureDate = DateTime.MinValue;
                        if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                shouldUpdateEpisode = true;
                                description = "Returned By:" + Current.UserFullName;
                                action = Actions.Returned;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }
                    if (scheduleEvent != null && shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, DisciplineTasks.CommunicationNote, description);
                        }
                    }
                }
            }
            return result;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var commNotes = new List<CommunicationNote>();
            var schedules = patientRepository.GetCommunicationNoteScheduleEvents(Current.AgencyId, branchId, patientStatus, startDate, endDate);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                commNotes = patientRepository.GetCommunicationNoteByIds(Current.AgencyId, physicianOrdersIds);
            }
            return commNotes;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid patientId)
        {
            var commNotes = patientRepository.GetCommunicationNotes(Current.AgencyId, patientId);
            if (commNotes != null)
            {
                commNotes.ForEach(c =>
                {
                    if (!c.UserId.IsEmpty())
                    {
                        var userName = UserEngine.GetName(c.UserId, Current.AgencyId);
                        if (userName.IsNotNullOrEmpty())
                        {
                            c.UserDisplayName = userName;
                        }
                    }
                    var physician = PhysicianEngine.Get(c.PhysicianId, Current.AgencyId);
                    c.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, c.EpisodeId, c.PatientId, c.Id);
                    if (scheduleEvent != null)
                    {
                        c.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return commNotes;
        }

        public bool DeleteCommunicationNote(Guid Id, Guid patientId)
        {
            var communicationNote = patientRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            bool result = false;
            if (communicationNote != null)
            {
                if (!communicationNote.EpisodeId.IsEmpty())
                {
                    DeleteSchedule(communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, communicationNote.UserId, (int)DisciplineTasks.CommunicationNote);
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true);
                }
                else
                {
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true);
                }
            }
            return result;
        }

        public CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId)
        {
            var note = new CommunicationNote();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {

                note = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (note != null)
                {
                    if ((note.Status == (int)ScheduleStatus.NoteCompleted || note.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !note.PhysicianId.IsEmpty() && note.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = note.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            note.Physician = physician;
                        }
                        else
                        {
                            note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                    }

                    note.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                }
            }
            return note;
        }

        public bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                faceToFaceEncounter.UserId = Current.UserId;
                faceToFaceEncounter.AgencyId = Current.AgencyId;
                faceToFaceEncounter.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                faceToFaceEncounter.OrderNumber = patientRepository.GetNextOrderNumber();
                faceToFaceEncounter.Modified = DateTime.Now;
                faceToFaceEncounter.Created = DateTime.Now;
                if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        faceToFaceEncounter.PhysicianData = physician.ToXml();
                    }
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = faceToFaceEncounter.Id,
                    UserId = faceToFaceEncounter.UserId,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Status = faceToFaceEncounter.Status.ToString(),
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = faceToFaceEncounter.RequestDate.ToShortDateString(),
                    VisitDate = faceToFaceEncounter.RequestDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.FaceToFaceEncounter
                };
                if (patientRepository.UpdateEpisode(Current.AgencyId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                {
                    Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.FaceToFaceEncounter);
                    return patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public FaceToFaceEncounter GetFaceToFacePrint()
        {
            var order = new FaceToFaceEncounter();
            order.Location = agencyRepository.GetMainLocation(Current.AgencyId);
            return order;
        }

        public FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId)
        {
            var order = new FaceToFaceEncounter();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
                if (order != null)
                {
                    order.Patient = patient;
                    if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.Physician = physician;
                        }
                        else
                        {
                            order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                    }
                    var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
                    if (episode != null)
                    {
                        order.EpisodeEndDate = episode.EndDateFormatted;
                        order.EpisodeStartDate = episode.StartDateFormatted;
                        if (order.Patient != null)
                        {
                            order.Patient.StartofCareDate = episode.StartOfCareDate;
                        }
                    }
                }
                else
                {
                    order = new FaceToFaceEncounter();
                }
                order.Location = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
            }
            // order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = startDate;
            patientEpisode.EndDate = startDate.AddDays(59);
            patientEpisode.IsActive = true;
            var events = new List<ScheduleEvent>();
            if (scheduleEvent != null && !scheduleEvent.EventId.IsEmpty())
            {
                scheduleEvent.EpisodeId = patientEpisode.Id;
                events.Add(scheduleEvent);
            }
            patientEpisode.Schedule = events.ToXml();
            return patientEpisode;
        }

        public PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode, ScheduleEvent scheduleEvent)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = episode.StartDate;
            patientEpisode.EndDate = episode.EndDate;
            patientEpisode.IsActive = true;
            patientEpisode.Detail = episode.Detail;
            patientEpisode.StartOfCareDate = episode.StartOfCareDate;
            patientEpisode.Details = patientEpisode.Detail.ToXml();
            var events = new List<ScheduleEvent>();
            if (scheduleEvent != null && !scheduleEvent.EventId.IsEmpty())
            {
                scheduleEvent.EpisodeId = patientEpisode.Id;
                events.Add(scheduleEvent);
            }
            patientEpisode.Schedule = events.ToXml();
            return patientEpisode;
        }

        public bool AddEpisode(PatientEpisode patientEpisode)
        {
            bool result = false;
            if (patientRepository.AddEpisode(patientEpisode))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool UpdateEpisode(PatientEpisode episode)
        {
            var result = false;
            episode.Details = episode.Detail.ToXml();
            var episodeToEdit = patientRepository.GetEpisodeById(Current.AgencyId, episode.Id, episode.PatientId);
            if (episodeToEdit != null)
            {
                var logAction = episodeToEdit.IsActive == episode.IsActive ? LogAction.EpisodeEdited : (!episode.IsActive ? LogAction.EpisodeDeactivated : LogAction.EpisodeActivated);
                episodeToEdit.IsActive = episode.IsActive;
                episodeToEdit.StartDate = episode.StartDate;
                episodeToEdit.EndDate = episode.EndDate;
                episodeToEdit.Detail = episodeToEdit.Details.ToObject<EpisodeDetail>();
                episode.Detail.Assets.AddRange(episodeToEdit.Detail.Assets);
                episodeToEdit.StartOfCareDate = episode.StartOfCareDate;
                episodeToEdit.Details = episode.Detail.ToXml();
                episodeToEdit.AdmissionId = episode.AdmissionId;
                if (patientRepository.UpdateEpisode(Current.AgencyId, episodeToEdit))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, episodeToEdit.PatientId, episodeToEdit.Id.ToString(), LogType.Episode, logAction, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if (e.Id == episodeId)
                    {
                        continue;
                    }
                    else
                    {
                        if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, PatientEpisode episode)
        {
            var result = false;
            try
            {
                var patientEpisodes = new List<PatientEpisode>();

                if (episode != null)
                {
                    episode.EndDate = dischargeDate;
                    episode.IsLinkedToDischarge = true;
                    episode.Modified = DateTime.Now;
                    patientEpisodes.Add(episode);
                }
                var episodes = patientRepository.EpisodesToDischarge(Current.AgencyId, patientId, dischargeDate);//  database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate.Date >= dischargeDate.Date);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(es =>
                    {
                        es.Modified = DateTime.Now;
                        es.IsDischarged = true;
                    });
                    patientEpisodes.AddRange(episodes);
                }
                if (patientEpisodes.Count > 0)
                {
                    if (patientRepository.UpdateEpisodeForDischarge(patientEpisodes))
                    {
                        if (episode != null)
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeEdited, "Updated for patient discharge");
                        }
                        if (episodes != null && episodes.Count > 0)
                        {
                            episodes.ForEach(e =>
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeDeactivatedForDischarge, string.Empty);
                            });
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool CreateEpisodeAndClaims(Patient patient)
        {
            bool result = false;
            var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                if (patient.StartofCareDate.Date == patient.EpisodeStartDate.Date)
                {
                    var newEvent = new ScheduleEvent
                    {
                        EventId = Guid.NewGuid(),
                        PatientId = patient.Id,
                        UserId = patient.UserId,
                        Discipline = Disciplines.Nursing.ToString(),
                        Status = ((int)ScheduleStatus.OasisNotYetDue).ToString(),
                        DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                        EventDate = patient.EpisodeStartDate.ToShortDateString(),
                        VisitDate = patient.EpisodeStartDate.ToShortDateString(),
                        IsBillable = true
                    };
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, newEvent);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (patient.PrimaryInsurance.IsNotNullOrEmpty())
                    {
                        episode.Detail.PrimaryInsurance = patient.PrimaryInsurance;
                    }
                    if (patient.SecondaryInsurance.IsNotNullOrEmpty())
                    {
                        episode.Detail.SecondaryInsurance = patient.SecondaryInsurance;
                    }

                    if (this.AddEpisode(episode))
                    {
                        newEvent.EpisodeId = episode.Id;
                        this.ProcessSchedule(newEvent, patientToEdit, episode);

                        if (patient.UserId != Guid.Empty)
                        {
                            userRepository.AddUserEvent(Current.AgencyId, patient.Id, patient.UserId,
                                new UserEvent
                                {
                                    EventId = newEvent.EventId,
                                    PatientId = patient.Id,
                                    UserId = patient.UserId,
                                    EpisodeId = episode.Id,
                                    DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                                    Status = ((int)ScheduleStatus.OasisNotYetDue).ToString(),
                                    EventDate = patient.EpisodeStartDate.ToShortDateString()
                                });
                        }
                        var isClaimsAdded = false;
                        var IsFaceToFaceAdded = false;
                        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
                        {
                            var insurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                            if ((patient.PrimaryInsurance.ToInteger() > 0 && patient.PrimaryInsurance.ToInteger() < 1000) || (insurance != null && insurance.PayorType == (int)PayerTypes.MedicareHMO))
                            {
                                isClaimsAdded = true;

                            }
                        }

                        if (patient.IsFaceToFaceEncounterCreated)
                        {
                            IsFaceToFaceAdded = true;
                            //if (!patientToEdit.Id.IsEmpty() && patientToEdit.PhysicianContacts.Count > 0)
                            //{
                            //    var physician = patientToEdit.PhysicianContacts.Where(p => p.Primary = true).FirstOrDefault();
                            //    if (physician != null && !physician.Id.IsEmpty())
                            //    {

                            //    }
                            //}
                        }
                        if (isClaimsAdded || IsFaceToFaceAdded)
                        {
                            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientToEdit.Id);
                            if (isClaimsAdded)
                            {
                                var rap = this.CreateRap(patientToEdit, episode, 0, physician);
                                if (rap != null)
                                {
                                    if (billingRepository.AddRap(rap))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                    }
                                }
                                var final = this.CreateFinal(patientToEdit, episode, 0, physician);
                                if (final != null)
                                {
                                    if (billingRepository.AddFinal(final))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                    }
                                }
                            }
                            if (physician != null && IsFaceToFaceAdded)
                            {
                                var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = DateTime.Now };
                                this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                            }
                        }
                        if (Enum.IsDefined(typeof(DisciplineTasks), newEvent.DisciplineTask))
                        {
                            Auditor.Log(newEvent.EpisodeId, newEvent.PatientId, newEvent.EventId, Actions.Add, (DisciplineTasks)newEvent.DisciplineTask);
                        }
                        result = true;
                    }
                }
                else if (patient.EpisodeStartDate.Date > patient.StartofCareDate.Date)
                {
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, null);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (this.AddEpisode(episode))
                    {
                        var isClaimsAdded = false;
                        var IsFaceToFaceAdded = false;
                        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
                        {
                            var insurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                            if ((patient.PrimaryInsurance.ToInteger() > 0 && patient.PrimaryInsurance.ToInteger() < 1000) ||
                                    (insurance != null && insurance.PayorType == (int)PayerTypes.MedicareHMO))
                            {
                                isClaimsAdded = true;

                            }
                        }
                        if (patient.IsFaceToFaceEncounterCreated)
                        {
                            IsFaceToFaceAdded = true;
                            //if (!patientToEdit.Id.IsEmpty() && patientToEdit.PhysicianContacts.Count > 0)
                            //{
                            //    var physician = patientToEdit.PhysicianContacts.Where(p => p.Primary = true).FirstOrDefault();
                            //    if (physician != null && !physician.Id.IsEmpty())
                            //    {
                            //        var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = patient.EpisodeStartDate };
                            //        this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                            //    }
                            //}
                        }
                        if (isClaimsAdded || IsFaceToFaceAdded)
                        {
                            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientToEdit.Id);
                            if (isClaimsAdded)
                            {
                                var rap = this.CreateRap(patientToEdit, episode, 0, physician);

                                if (rap != null)
                                {
                                    if (billingRepository.AddRap(rap))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                    }
                                }
                                var final = this.CreateFinal(patientToEdit, episode, 0, physician);
                                if (final != null)
                                {
                                    if (billingRepository.AddFinal(final))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                    }
                                }
                            }
                            if (physician != null && IsFaceToFaceAdded)
                            {
                                var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = patient.EpisodeStartDate };
                                this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                            }
                        }
                        result = true;
                    }
                }
            }

            return result;
        }

        public void DeleteEpisodeAndClaims(Patient patient)
        {
            PatientEpisode episodeDeleted = null;
            patientRepository.DeleteEpisode(Current.AgencyId, patient, out episodeDeleted);
            if (episodeDeleted != null)
            {
                billingRepository.DeleteRap(Current.AgencyId, patient.Id, episodeDeleted.Id);
                billingRepository.DeleteFinal(Current.AgencyId, patient.Id, episodeDeleted.Id);
            }
        }

        public bool AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates)
        {
            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (!episode.AdmissionId.IsEmpty())
                {
                    var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                    if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                    {
                        patient = admissinData.PatientData.ToObject<Patient>();
                    }
                }
                if (patient != null)
                {
                    episode.StartOfCareDate = patient.StartofCareDate;
                    var scheduledEvents = new List<ScheduleEvent>();
                    var disciplineTask = lookupRepository.GetDisciplineTask(disciplineTaskId);
                    var visitDateArray = visitDates.Split(',');
                    if (visitDateArray != null && visitDateArray.Length > 0)
                    {
                        visitDateArray.ForEach(date =>
                        {
                            if (date.IsDate())
                            {
                                var newScheduledEvent = new ScheduleEvent
                                {
                                    UserId = userId,
                                    IsDeprecated = false,
                                    EventId = Guid.NewGuid(),
                                    PatientId = patientId,
                                    EpisodeId = episodeId,
                                    EventDate = date.ToZeroFilled(),
                                    VisitDate = date.ToZeroFilled(),
                                    StartDate = episode.StartDate,
                                    EndDate = episode.EndDate,
                                    DisciplineTask = disciplineTaskId,
                                    PatientName = patient.DisplayName.ToUpperCase(),
                                    IsBillable = disciplineTask.IsBillable,
                                    Discipline = disciplineTask != null ? disciplineTask.Discipline : ""
                                };
                                scheduledEvents.Add(newScheduledEvent);
                                ProcessSchedule(newScheduledEvent, patient, episode);
                                if (Enum.IsDefined(typeof(DisciplineTasks), newScheduledEvent.DisciplineTask))
                                {
                                    Auditor.Log(newScheduledEvent.EpisodeId, newScheduledEvent.PatientId, newScheduledEvent.EventId, Actions.Add, (DisciplineTasks)newScheduledEvent.DisciplineTask);
                                }
                            }
                        });
                    }

                    if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, scheduledEvents))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid patientId, string jsonString)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(jsonString, "jsonString");

            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (!episode.AdmissionId.IsEmpty())
                {
                    var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                    if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                    {
                        patient = admissinData.PatientData.ToObject<Patient>();
                    }
                }
                if (patient != null)
                {
                    episode.StartOfCareDate = patient.StartofCareDate;
                    var newEvents = JsonExtensions.FromJson<List<ScheduleEvent>>(jsonString);
                    if (newEvents != null && newEvents.Count > 0)
                    {
                        newEvents.ForEach(ev =>
                        {
                            ev.EventId = Guid.NewGuid();
                            ev.PatientId = patientId;
                            ev.EpisodeId = episodeId;
                            ev.EventDate = ev.EventDate.ToZeroFilled();
                            ev.VisitDate = ev.EventDate.ToZeroFilled();
                            ev.StartDate = episode.StartDate;
                            ev.EndDate = episode.EndDate;
                            ev.IsDeprecated = false;
                            ProcessSchedule(ev, patient, episode);
                            if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
                            {
                                Auditor.Log(ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                            }
                        });
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, newEvents))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid patientId, string disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            int dateDifference = endDate.Subtract(startDate).Days + 1;
            var newEvents = new List<ScheduleEvent>();
            for (int i = 0; i < dateDifference; i++)
            {
                newEvents.Add(new ScheduleEvent { EventId = Guid.NewGuid(), PatientId = patientId, EpisodeId = episodeId, UserId = userId, DisciplineTask = int.Parse(disciplineTask), Discipline = discipline, EventDate = String.Format("{0:MM/dd/yyyy}", startDate.AddDays(i)), VisitDate = String.Format("{0:MM/dd/yyyy}", startDate.AddDays(i)), IsBillable = isBillable, IsDeprecated = false });
            }
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (!episode.AdmissionId.IsEmpty())
                {
                    var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                    if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                    {
                        patient = admissinData.PatientData.ToObject<Patient>();
                    }
                }
                if (patient != null)
                {
                    episode.StartOfCareDate = patient.StartofCareDate;
                    if (newEvents != null && newEvents.Count > 0)
                    {
                        newEvents.ForEach(ev =>
                        {
                            ProcessSchedule(ev, patient, episode);
                            if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
                            {
                                Auditor.Log(ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                            }
                        });
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, newEvents))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            var schedule = patientRepository.GetSchedule(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
            if (schedule != null)
            {
                if (httpFiles != null && httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset)) schedule.Assets.Add(asset.Id);
                            else
                            {
                                assetsSaved = false;
                                break;
                            }
                        }
                    }
                }
                Guid userId = schedule.UserId;
                schedule.UserId = scheduleEvent.UserId;
                schedule.Comments = scheduleEvent.Comments;
                schedule.Status = scheduleEvent.Status;
                schedule.EventDate = scheduleEvent.EventDate;
                schedule.VisitDate = scheduleEvent.VisitDate;
                schedule.IsBillable = scheduleEvent.IsBillable;
                schedule.Surcharge = scheduleEvent.Surcharge;
                schedule.AssociatedMileage = scheduleEvent.AssociatedMileage;
                schedule.TimeIn = scheduleEvent.TimeIn;
                schedule.TimeOut = scheduleEvent.TimeOut;
                schedule.IsMissedVisit = scheduleEvent.IsMissedVisit;
                schedule.DisciplineTask = scheduleEvent.DisciplineTask;
                schedule.PhysicianId = scheduleEvent.PhysicianId;
                if (ProcessEditDetail(schedule, schedule.EpisodeId))
                {
                    if (assetsSaved && patientRepository.UpdateEpisode(Current.AgencyId, schedule))
                    {
                        var userEvent = userRepository.GetEvent(Current.AgencyId, schedule.UserId, schedule.PatientId, schedule.EventId);
                        if (userEvent != null)
                        {
                            userEvent.IsDeprecated = schedule.IsDeprecated;
                            userEvent.EventDate = schedule.EventDate;
                            userEvent.VisitDate = schedule.VisitDate;
                            userEvent.TimeIn = schedule.TimeIn;
                            userEvent.TimeOut = schedule.TimeOut;
                            userEvent.IsMissedVisit = schedule.IsMissedVisit;
                            userEvent.Status = schedule.Status;
                            if (userId == schedule.UserId) userRepository.UpdateEvent(Current.AgencyId, userEvent);
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, userEvent);
                                userRepository.RemoveScheduleEvent(Current.AgencyId, schedule.PatientId, schedule.EventId, schedule.UserId);
                            }
                        }
                        else userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, new UserEvent { DisciplineTask = schedule.DisciplineTask, EventDate = schedule.EventDate, VisitDate = schedule.VisitDate, EventId = schedule.EventId, UserId = schedule.UserId, PatientId = schedule.PatientId, Status = schedule.Status, EpisodeId = schedule.EpisodeId, Discipline = schedule.Discipline, IsDeprecated = schedule.IsDeprecated });
                        if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask)) Auditor.Log(schedule.EpisodeId, schedule.PatientId, schedule.EventId, Actions.EditDetail, (DisciplineTasks)schedule.DisciplineTask);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
           // PatientVisitNote patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);

            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
            var newPatientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
            if (newPatientEpisode != null && episode != null)
            {
                var oldScheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                var newScheduleEvents = newPatientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                var schedule = oldScheduleEvents.FirstOrDefault(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                if (schedule != null)
                {
                    oldScheduleEvents.RemoveAll(e => e.EventId == schedule.EventId && e.EpisodeId == schedule.EpisodeId && e.PatientId == schedule.PatientId);
                    episode.Schedule = oldScheduleEvents.ToXml();
                    if (httpFiles != null && httpFiles.Count > 0)
                    {
                        foreach (string key in httpFiles.AllKeys)
                        {
                            HttpPostedFileBase file = httpFiles.Get(key);
                            if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                            {
                                var binaryReader = new BinaryReader(file.InputStream);
                                var asset = new Asset
                                {
                                    FileName = file.FileName,
                                    AgencyId = Current.AgencyId,
                                    ContentType = file.ContentType,
                                    FileSize = file.ContentLength.ToString(),
                                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                };
                                if (assetRepository.Add(asset))
                                {
                                    schedule.Assets.Add(asset.Id);
                                }
                                else
                                {
                                    assetsSaved = false;
                                    break;
                                }
                            }
                        }
                    }
                    Guid userId = schedule.UserId;
                    schedule.EpisodeId = newPatientEpisode.Id;
                    schedule.StartDate = newPatientEpisode.StartDate;
                    schedule.EndDate = newPatientEpisode.EndDate;
                    schedule.UserId = scheduleEvent.UserId;
                    schedule.Comments = scheduleEvent.Comments;
                    schedule.Status = scheduleEvent.Status;
                    schedule.EventDate = scheduleEvent.EventDate;
                    schedule.VisitDate = scheduleEvent.VisitDate;
                    schedule.IsBillable = scheduleEvent.IsBillable;
                    schedule.Surcharge = scheduleEvent.Surcharge;
                    schedule.AssociatedMileage = scheduleEvent.AssociatedMileage;
                    schedule.TimeIn = scheduleEvent.TimeIn;
                    schedule.TimeOut = scheduleEvent.TimeOut;
                    schedule.ReturnReason = scheduleEvent.ReturnReason;
                    // schedule.IsMissedVisit = scheduleEvent.IsMissedVisit;
                    schedule.DisciplineTask = scheduleEvent.DisciplineTask;
                    schedule.PhysicianId = scheduleEvent.PhysicianId;

                    if (ProcessEditDetailForReassign(schedule, episode.Id))
                    {
                        newScheduleEvents.Add(schedule);
                        newPatientEpisode.Schedule = newScheduleEvents.ToXml();

                        if (assetsSaved && patientRepository.UpdateEpisode(newPatientEpisode) && patientRepository.UpdateEpisode(episode))
                        {
                            var userEvent = userRepository.GetEvent(Current.AgencyId, schedule.UserId, schedule.PatientId, schedule.EventId);
                            if (userEvent != null)
                            {
                                userEvent.EpisodeId = schedule.EpisodeId;
                                userEvent.IsDeprecated = schedule.IsDeprecated;
                                userEvent.EventDate = schedule.EventDate;
                                userEvent.VisitDate = schedule.VisitDate;
                                userEvent.TimeIn = schedule.TimeIn;
                                userEvent.TimeOut = schedule.TimeOut;
                                userEvent.IsMissedVisit = schedule.IsMissedVisit;
                                userEvent.Status = schedule.Status;
                                if (userId == schedule.UserId)
                                {
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, userEvent);
                                    userRepository.RemoveScheduleEvent(Current.AgencyId, schedule.PatientId, schedule.EventId, schedule.UserId);
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, new UserEvent { DisciplineTask = schedule.DisciplineTask, EventDate = schedule.EventDate, VisitDate = schedule.VisitDate, EventId = schedule.EventId, UserId = schedule.UserId, PatientId = schedule.PatientId, Status = schedule.Status, EpisodeId = schedule.EpisodeId, Discipline = schedule.Discipline, IsDeprecated = schedule.IsDeprecated });
                            }
                            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
                            {
                                Auditor.Log(schedule.EpisodeId, schedule.PatientId, schedule.EventId, Actions.EditDetail, (DisciplineTasks)schedule.DisciplineTask, "Reassigned from other episode.");
                            }
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid patientId, string discipline, DateRange range)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (range.Id.IsEqual("ThisEpisode"))
            {
                var patientEpisode = patientRepository.GetCurrentEpisodeLean(Current.AgencyId, patientId);
                if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var detail = new EpisodeDetail();
                    var schedule = new List<ScheduleEvent>();
                    if (patientEpisode.Details.IsNotNullOrEmpty()) detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                    if (patientEpisode.Status == (int)PatientStatus.Discharged) schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();//&& s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.PatientDischargeDate
                    else schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                    if (schedule != null && schedule.Count > 0)
                    {
                        if (discipline == "All") schedule = schedule.Where(s => s.DisciplineTask > 0).ToList();
                        else schedule = schedule.Where(s => s.DisciplineTask > 0 && s.Discipline.IsEqual(discipline)).ToList();
                        if (schedule != null && schedule.Count > 0)
                        {

                            var userIds = new List<Guid>();
                            var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, patientEpisode.Id) ?? new List<ReturnComment>();
                            if (returnComments != null && returnComments.Count > 0)
                            {
                                var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                                if (returnUserIds != null && returnUserIds.Count > 0)
                                {
                                    userIds.AddRange(returnUserIds);
                                }
                            }
                            var scheduleUserIds = schedule.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                            if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                            {
                                userIds.AddRange(scheduleUserIds);
                            }
                            if (userIds != null && userIds.Count > 0)
                            {
                                userIds = userIds.Distinct().ToList();
                            }
                            var users = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds);
                            var eventIds = schedule.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                            var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);


                            schedule.ForEach(s =>
                            {
                                var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                                s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
                                // s.ReturnReason = this.GetReturnComments(s.EventId, s.EpisodeId, s.PatientId);
                                var hideTask = false;
                                if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
                                {
                                    hideTask = true;
                                    if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA) hideTask = false;
                                }
                                if (!hideTask)
                                {
                                    if (!s.UserId.IsEmpty())
                                    {
                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                        if (user != null)
                                        {
                                            s.UserName = user.DisplayName;
                                        }
                                    }
                                    //if (!s.UserId.IsEmpty()) s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                                    s.EpisodeNotes = detail.Comments.Clean();
                                    if (s.IsMissedVisit)
                                    {
                                        var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.EventId); // patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                                        if (missedVisit != null)
                                        {
                                            s.Status = missedVisit.Status.ToString();
                                            s.MissedVisitComments = missedVisit.ToString();
                                        }
                                    }
                                    s.EventDate = s.EventDate.ToZeroFilled();
                                    s.PatientId = patientId;
                                    s.EndDate = DateTime.Parse(patientEpisode.EndDate);
                                    s.StartDate = DateTime.Parse(patientEpisode.StartDate);
                                    Common.Url.Set(s, true, true);
                                    patientEvents.Add(s);
                                }
                            });
                        }
                    }
                }
            }
            else
            {
                if ((range.StartDate.Date > DateTime.MinValue.Date && range.EndDate.Date > DateTime.MinValue.Date) || range.Id.IsEqual("all"))
                {
                    var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
                    if (patientEpisodes != null && patientEpisodes.Count > 0)
                    {
                        var users = new List<User>();
                        patientEpisodes.ForEach(patientEpisode =>
                        {
                            if (patientEpisode.Schedule.IsNotNullOrEmpty())
                            {
                                var detail = new EpisodeDetail();
                                var schedule = new List<ScheduleEvent>();
                                if (patientEpisode.Details.IsNotNullOrEmpty()) detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                                if (patientEpisode.Status == (int)PatientStatus.Discharged) schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();//&& s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.PatientDischargeDate
                                else schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                                if (schedule.Count > 0)
                                {
                                    if (discipline == "All") schedule = schedule.Where(s => s.DisciplineTask > 0).ToList();
                                    else schedule = schedule.Where(s => s.DisciplineTask > 0 && s.Discipline.IsEqual(discipline)).ToList();
                                    if (schedule != null && schedule.Count > 0)
                                    {
                                        if (!range.Id.IsEqual("all")) schedule = schedule.Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= range.StartDate.Date && s.EventDate.ToDateTime().Date <= range.EndDate.Date).ToList();
                                        if (schedule != null && schedule.Count > 0)
                                        {
                                            var userIds = new List<Guid>();
                                            var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, patientEpisode.Id) ?? new List<ReturnComment>();
                                            if (returnComments != null && returnComments.Count > 0)
                                            {
                                                var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty() && !users.Exists(us => us.Id == r.UserId)).Select(r => r.UserId).Distinct().ToList();
                                                if (returnUserIds != null && returnUserIds.Count > 0)
                                                {
                                                    userIds.AddRange(returnUserIds);
                                                }
                                            }
                                            var scheduleUserIds = schedule.Where(s => !s.UserId.IsEmpty() && !users.Exists(us => us.Id == s.UserId)).Select(s => s.UserId).Distinct().ToList();
                                            if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                                            {
                                                userIds.AddRange(scheduleUserIds);
                                            }
                                            if (userIds != null && userIds.Count > 0)
                                            {
                                                userIds = userIds.Distinct().ToList();
                                            }
                                            if (userIds != null && userIds.Count > 0)
                                            {
                                                var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
                                                if (scheduleUsers != null && scheduleUsers.Count > 0)
                                                {
                                                    users.AddRange(scheduleUsers);
                                                }
                                            }
                                            var eventIds = schedule.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                                            var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);

                                            schedule.ForEach(s =>
                                            {
                                                var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                                                s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
                                                //s.ReturnReason = this.GetReturnComments(s.EventId, s.EpisodeId, s.PatientId);
                                                var hideTask = false;
                                                if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
                                                {
                                                    hideTask = true;
                                                    if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA) hideTask = false;
                                                }
                                                if (!hideTask)
                                                {
                                                    if (!s.UserId.IsEmpty())
                                                    {
                                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                                        if (user != null)
                                                        {
                                                            s.UserName = user.DisplayName;
                                                        }
                                                    }
                                                    s.EpisodeNotes = detail.Comments;
                                                    if (s.IsMissedVisit)
                                                    {
                                                        var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                                                        if (missedVisit != null)
                                                        {
                                                            s.Status = missedVisit.Status.ToString();
                                                            s.MissedVisitComments = missedVisit.ToString();
                                                        }
                                                    }
                                                    s.EventDate = s.EventDate.ToZeroFilled();
                                                    s.PatientId = patientId;
                                                    s.EndDate = DateTime.Parse(patientEpisode.EndDate);
                                                    s.StartDate = DateTime.Parse(patientEpisode.StartDate);
                                                    Common.Url.Set(s, true, true);
                                                    patientEvents.Add(s);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            }
            return patientEvents.OrderByDescending(e => e.EventDateSortable).ToList();
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline)
        {
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisode = patientRepository.GetPatientScheduledEvents(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
            {
                var detail = new EpisodeDetail();
                var schedule = new List<ScheduleEvent>();
                if (patientEpisode.Details.IsNotNullOrEmpty()) detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                if (discipline.IsEqual("all")) schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true).ToList();
                else if (discipline.IsEqual("Therapy")) schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).ToList();
                else schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true && e.Discipline == discipline).ToList();
                if (schedule != null && schedule.Count > 0)
                {
                    var userIds = new List<Guid>();
                    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeId) ?? new List<ReturnComment>();
                    if (returnComments != null && returnComments.Count > 0)
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds != null && returnUserIds.Count > 0)
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }
                    var scheduleUserIds = schedule.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                    {
                        userIds.AddRange(scheduleUserIds);
                    }
                    if (userIds != null && userIds.Count > 0)
                    {
                        userIds = userIds.Distinct().ToList();
                    }
                    var users = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds);
                    var eventIds = schedule.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                    var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds);

                    schedule.ForEach(s =>
                    {
                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                        s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);
                        var hideTask = false;
                        if (s.DisciplineTask == (int)DisciplineTasks.IncidentAccidentReport || s.DisciplineTask == (int)DisciplineTasks.InfectionReport)
                        {
                            hideTask = true;
                            if (Current.UserId == s.UserId || Current.IsAgencyAdmin || Current.IsCaseManager || Current.IsDirectorOfNursing || Current.IsQA) hideTask = false;
                        }
                        if (!hideTask)
                        {
                            //if (!s.UserId.IsEmpty()) s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                            //s.EpisodeNotes = detail.Comments;
                            //if (s.IsMissedVisit)
                            //{
                            //    var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                            //    if (missedVisit != null)
                            //    {
                            //        s.MissedVisitComments = missedVisit.ToString();
                            //        s.Status = missedVisit.Status.ToString();
                            //    }
                            //}

                            if (!s.UserId.IsEmpty())
                            {
                                var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                if (user != null)
                                {
                                    s.UserName = user.DisplayName;
                                }
                            }
                            s.EpisodeNotes = detail.Comments.Clean();
                            if (s.IsMissedVisit)
                            {
                                var missedVisit = missedVisits.FirstOrDefault(m => m.Id == s.EventId);
                                if (missedVisit != null)
                                {
                                    s.MissedVisitComments = missedVisit.ToString();
                                    s.Status = missedVisit.Status.ToString();
                                }
                            }

                            s.EventDate = s.EventDate.ToZeroFilled();
                            s.EndDate = DateTime.Parse(patientEpisode.EndDate);
                            s.StartDate = DateTime.Parse(patientEpisode.StartDate);
                            s.PatientId = patientId;
                            Common.Url.Set(s, true, true);
                            patientEvents.Add(s);
                        }
                    });
                }
            }
            return patientEvents.OrderByDescending(e => e.EventDateSortable).ToList();
        }

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        public List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            DateTime now = DateTime.Now;
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(agencyId, branchId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {                               

                
                var missedVisitEvents = patientEpisodes.ToDictionary(g => g.Id, g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.IsDeprecated
                            && s.EventDate.IsValidDate()
                            && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                            && s.IsMissedVisit
                        ).ToList() : new List<ScheduleEvent>()
                       );

                if (missedVisitEvents != null && missedVisitEvents.Count > 0)
                {
                    var missedVisitIds = missedVisitEvents.SelectMany(s => s.Value).Select(s => s.EventId).ToList();
                    var missedVisits = patientRepository.GetMissedVisitsByIds(agencyId, missedVisitIds);

                    foreach (var patientEpisode in patientEpisodes)
                    {
                        var scheduleEvents = missedVisitEvents[patientEpisode.Id];
                        var detail = new EpisodeDetail();
                        if (patientEpisode.Details.IsNotNullOrEmpty())
                        {
                            detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                        }
                        foreach (var missedVisit in missedVisits)
                        {
                            var scheduledEvent = scheduleEvents.FirstOrDefault(s => s.EventId == missedVisit.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.MissedVisitComments = missedVisit.ToString();
                                scheduledEvent.PatientIdNumber = patientEpisode.PatientIdNumber;
                                if (detail.Comments.IsNotNullOrEmpty())
                                {
                                    scheduledEvent.EpisodeNotes = detail.Comments;
                                }
                                scheduledEvent.EventDate = scheduledEvent.EventDate.ToZeroFilled();
                                scheduledEvent.VisitDate = scheduledEvent.VisitDate.ToZeroFilled();
                                scheduledEvent.StartDate = patientEpisode.StartDate.ToDateTime();
                                scheduledEvent.EndDate = patientEpisode.EndDate.ToDateTime();
                                scheduledEvent.PatientName = patientEpisode.PatientName;
                                scheduledEvent.UserName = !scheduledEvent.UserId.IsEmpty() ? UserEngine.GetName(scheduledEvent.UserId, agencyId).ToUpperCase() : string.Empty;
                                scheduledEvent.Status = missedVisit.Status.ToString();
                                Common.Url.Set(scheduledEvent, true, true);
                                patientEvents.Add(scheduledEvent);
                            }
                        }
                    }
                }
            }
            DateTime then = DateTime.Now;
            DateTime diff = new DateTime(then.Ticks - now.Ticks);
            return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public List<ScheduleEvent> GetDeletedTasks(Guid patientId)
        {
            var scheduledEvents = new List<ScheduleEvent>();
            var deletedItems = patientRepository.GetDeletedItems(Current.AgencyId, patientId);
            if (deletedItems != null)
            {
                deletedItems.ForEach(item =>
                {
                    if (item != null && item.Schedule.IsNotNullOrEmpty())
                    {
                        var deletedEvents = item.Schedule.ToObject<List<ScheduleEvent>>();
                        if (deletedEvents != null)
                        {
                            deletedEvents.ForEach(e =>
                            {
                                e.EventDate = e.EventDate.ToZeroFilled();
                                e.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
                                scheduledEvents.Add(e);
                            });
                        }
                    }
                });
            }
            return scheduledEvents;
        }

        public ScheduleEvent GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);

            if (scheduleEvent != null && scheduleEvent.Discipline.IsEqual(Disciplines.Orders.ToString()))
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
                {
                    var order = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                    if (order != null)
                    {
                        scheduleEvent.PhysicianId = order.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)
                {
                    var planofcare = assessmentService.GetPlanofCare(episodeId, patientId, eventId);
                    if (planofcare != null)
                    {
                        scheduleEvent.PhysicianId = planofcare.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)
                {
                    var planofcareStandalone = assessmentService.GetPlanofCareStandAlone(episodeId, patientId, eventId);
                    if (planofcareStandalone != null)
                    {
                        scheduleEvent.PhysicianId = planofcareStandalone.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter)
                {
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        scheduleEvent.PhysicianId = faceToFace.PhysicianId;
                    }
                }
            }

            return scheduleEvent;
        }

        public bool Reopen(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
            if (scheduleEvent != null)
            {
                switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
                {
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduleEvent.DisciplineTask)).ToString();
                            assessmentService.UpdateAssessmentStatus(eventId, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisReopened).ToString());
                            return true;
                        }
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.Labs:
                    case DisciplineTasks.InitialSummaryOfCare:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNDiabeticDailyVisit:
                    case DisciplineTasks.SNPediatricVisit:
                    case DisciplineTasks.SNPediatricAssessment:
                    case DisciplineTasks.SNPsychAssessment:

                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:

                    case DisciplineTasks.HomeMakerNote:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.HHAideSupervisoryVisit:

                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASTravel:
                    case DisciplineTasks.PASCarePlan:

                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTReassessment:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTSupervisoryVisit:

                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STReassessment:

                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReassessment:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                    case DisciplineTasks.OTDischarge:

                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                    case DisciplineTasks.DischargeSummary:
                    case DisciplineTasks.PTDischargeSummary:
                    case DisciplineTasks.OTDischargeSummary:

                    case DisciplineTasks.LVNSupervisoryVisit:

                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.MSWVisit:

                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                            if (patientVisitNote != null)
                            {
                                patientVisitNote.UserId = scheduleEvent.UserId;
                                patientVisitNote.Status = (int)ScheduleStatus.NoteReopened;
                                patientVisitNote.SignatureText = string.Empty;
                                patientVisitNote.SignatureDate = DateTime.MinValue;
                                patientVisitNote.PhysicianSignatureText = string.Empty;
                                patientVisitNote.ReceivedDate = DateTime.MinValue;
                                patientRepository.UpdateVisitNote(patientVisitNote);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;

                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var planOfCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
                            if (planOfCare != null)
                            {
                                planOfCare.UserId = scheduleEvent.UserId;
                                planOfCare.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCare.SignatureText = string.Empty;
                                planOfCare.SignatureDate = DateTime.MinValue;
                                planOfCare.PhysicianSignatureText = string.Empty;
                                planOfCare.ReceivedDate = DateTime.MinValue;
                                planofCareRepository.Update(planOfCare);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var planOfCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
                            if (planOfCareStandAlone != null)
                            {
                                planOfCareStandAlone.UserId = scheduleEvent.UserId;
                                planOfCareStandAlone.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCareStandAlone.SignatureText = string.Empty;
                                planOfCareStandAlone.SignatureDate = DateTime.MinValue;
                                planOfCareStandAlone.PhysicianSignatureText = string.Empty;
                                planOfCareStandAlone.ReceivedDate = DateTime.MinValue;
                                planofCareRepository.UpdateStandAlone(planOfCareStandAlone);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                physicianOrder.UserId = scheduleEvent.UserId;
                                physicianOrder.Status = (int)ScheduleStatus.OrderReopened;
                                physicianOrder.SignatureText = string.Empty;
                                physicianOrder.SignatureDate = DateTime.MinValue;
                                physicianOrder.PhysicianSignatureText = string.Empty;
                                physicianOrder.PhysicianSignatureDate = DateTime.MinValue;
                                physicianOrder.ReceivedDate = DateTime.MinValue;
                                patientRepository.UpdateOrderModel(physicianOrder);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var faceToFaceOrder = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                            if (faceToFaceOrder != null)
                            {
                                faceToFaceOrder.UserId = scheduleEvent.UserId;
                                faceToFaceOrder.Status = (int)ScheduleStatus.OrderReopened;
                                faceToFaceOrder.SignatureText = string.Empty;
                                faceToFaceOrder.SignatureDate = DateTime.MinValue;
                                faceToFaceOrder.SignatureText = string.Empty;
                                faceToFaceOrder.ReceivedDate = DateTime.MinValue;
                                patientRepository.UpdateFaceToFaceEncounter(faceToFaceOrder);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.CommunicationNote:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                            if (communicationNote != null)
                            {
                                communicationNote.UserId = scheduleEvent.UserId;
                                communicationNote.Status = (int)ScheduleStatus.NoteReopened;
                                communicationNote.SignatureText = string.Empty;
                                communicationNote.SignatureDate = DateTime.MinValue;
                                communicationNote.Modified = DateTime.Now;
                                patientRepository.UpdateCommunicationNoteModal(communicationNote);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }

                        break;

                    case DisciplineTasks.IncidentAccidentReport:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReopen).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                            if (incident != null)
                            {
                                incident.UserId = scheduleEvent.UserId;
                                incident.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                incident.SignatureText = string.Empty;
                                incident.SignatureDate = DateTime.MinValue;
                                incident.Modified = DateTime.Now;
                                agencyRepository.UpdateIncidentModal(incident);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }

                        break;

                    case DisciplineTasks.InfectionReport:
                        //scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReopen).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                            if (infection != null)
                            {
                                infection.UserId = scheduleEvent.UserId;
                                infection.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                infection.SignatureText = string.Empty;
                                infection.SignatureDate = DateTime.MinValue;
                                infection.Modified = DateTime.Now;
                                agencyRepository.UpdateInfectionModal(infection);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }

                        break;
                }
            }
            return false;
        }

        public bool RestoreTask(Guid episodeId, Guid patientId, Guid eventId)
        {
            bool result = false;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var deletedItem = patientRepository.GetDeletedItem(Current.AgencyId, episodeId, patientId);
                if (deletedItem != null && deletedItem.Schedule.IsNotNullOrEmpty())
                {
                    var deletedEvents = deletedItem.Schedule.ToObject<List<ScheduleEvent>>();
                    if (deletedEvents != null && deletedEvents.Count > 0)
                    {
                        var deletedEvent = deletedEvents.Find(e => e.EventId == eventId);

                        if (deletedEvent != null)
                        {
                            deletedEvents.RemoveAll(e => e.EventId == eventId);
                        }
                        deletedItem.Schedule = deletedEvents.ToXml();
                        if (patientRepository.UpdateDeletedItem(deletedItem))
                        {
                            if (UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)deletedEvent.DisciplineTask).ToString(), false))
                            {
                                if (patientRepository.AddNewScheduleEvent(Current.AgencyId, patientId, episodeId, deletedEvent))
                                {
                                    if (!deletedEvent.UserId.IsEmpty())
                                    {
                                        patientRepository.AddNewUserEvent(Current.AgencyId, patientId, deletedEvent.ToUserEvent());
                                    }
                                    Auditor.Log(episodeId, patientId, eventId, Actions.Restored, (DisciplineTasks)deletedEvent.DisciplineTask);
                                    result = true;
                                }
                                else
                                {
                                    UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)deletedEvent.DisciplineTask).ToString(), true);
                                    if (deletedEvent != null)
                                    {
                                        deletedEvents.Add(deletedEvent);
                                        deletedItem.Schedule = deletedEvents.ToXml();
                                        patientRepository.UpdateDeletedItem(deletedItem);
                                    }
                                }
                            }
                            else
                            {
                                if (deletedEvent != null)
                                {
                                    deletedEvents.Add(deletedEvent);
                                    deletedItem.Schedule = deletedEvents.ToXml();
                                    patientRepository.UpdateDeletedItem(deletedItem);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, int task)
        {
            bool result = false;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && task >= 0 && Enum.IsDefined(typeof(DisciplineTasks), task))
            {
                if (patientRepository.DeleteScheduleEvent(Current.AgencyId, episodeId, patientId, eventId, task))
                {
                    result = true;

                    UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)task).ToString(), true);

                    if (!employeeId.IsEmpty())
                    {
                        if (userRepository.DeleteScheduleEvent(patientId, eventId, employeeId))
                        {
                            result = true;
                        }
                    }
                    Auditor.Log(episodeId, patientId, eventId, Actions.Deleted, (DisciplineTasks)task);
                }
            }

            return result;
        }

        public bool DeleteSchedules(Guid patientId, Guid episodeId, List<Guid> eventsToBeDeleted)
        {
            bool result = false;
            if (!patientId.IsEmpty())
            {
                List<ScheduleEvent> deletedEvents;
                if (patientRepository.DeleteScheduleEvents(Current.AgencyId, episodeId, patientId, eventsToBeDeleted, out deletedEvents))
                {
                    result = true;
                    if (deletedEvents != null && deletedEvents.Count > 0)
                    {
                        foreach (var evnt in deletedEvents)
                        {
                            UpdateScheduleEntity(evnt.EventId, episodeId, patientId, ((DisciplineTasks)evnt.DisciplineTask).ToString(), true);
                            Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Deleted, (DisciplineTasks)evnt.DisciplineTask);
                        }
                    }
                    //if (!employeeId.IsEmpty())
                    //{
                    //    if (userRepository.DeleteScheduleEvent(patientId, eventId, employeeId))
                    //    {
                    //        result = true;
                    //    }
                    //}
                }
            }

            return result;
        }

        public bool DeleteScheduleEventAsset(Guid episodeId, Guid patientId, Guid eventId, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null && scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0)
                {
                    scheduleEvent.Assets.Remove(assetId);
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                    {
                        if (!assetId.IsEmpty())
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                if (scheduleEvent.IsStartofCareAssessment() || scheduleEvent.IsRecertificationAssessment() || scheduleEvent.IsResumptionofCareAssessment() || (scheduleEvent.IsOASISTransferDischarge() && scheduleEvent.Version == 2))
                                {
                                    assessmentService.DeleteOnlyWoundCareAsset(episodeId, patientId, eventId, scheduleEvent.GetAssessmentType().ToString(), assetId);
                                }
                                result = true;
                            }
                        }
                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEvent.DisciplineTask, "Uploaded asset is deleted.");
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldEmployeeId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null && !scheduleEvent.IsMissedVisit && !scheduleEvent.IsDeprecated)
                {
                    if (patientRepository.Reassign(Current.AgencyId, episodeId, patientId, eventId, employeeId) && ReassignScheduleEntity(episodeId, patientId, eventId, employeeId, ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString()))
                    {
                        userRepository.Reassign(Current.AgencyId, scheduleEvent, employeeId);
                        Auditor.Log(scheduleEvent.EpisodeId, patientId, scheduleEvent.EventId, Actions.Reassigned, (DisciplineTasks)scheduleEvent.DisciplineTask, "( From " + UserEngine.GetName(oldEmployeeId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool ReassignSchedules(Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            var userEvents = userRepository.GetSchedule(Current.AgencyId, employeeOldId);
            if (userEvents != null && userEvents.Count > 0)
            {
                userEvents.ForEach(evnt =>
                {
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.EventId);
                    if (scheduleEvent != null)
                    {
                        if (!evnt.IsMissedVisit && !evnt.IsDeprecated && evnt.EventDate.IsValidDate()
                            && evnt.EventDate.ToDateTime() >= startDate && evnt.EventDate.ToDateTime() <= endDate 
                            && (scheduleEvent.Status == ((int)ScheduleStatus.NoteNotStarted).ToString() 
                            || scheduleEvent.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() 
                            || scheduleEvent.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() 
                            || scheduleEvent.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() 
                            || scheduleEvent.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString() 
                            || scheduleEvent.Status == ((int)ScheduleStatus.OrderNotYetStarted).ToString() 
                            || scheduleEvent.Status == ((int)ScheduleStatus.ReportAndNotesCreated).ToString()))
                        {
                            if (patientRepository.Reassign(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.EventId, employeeId) && ReassignScheduleEntity(evnt.EpisodeId, evnt.PatientId, evnt.EventId, employeeId, ((DisciplineTasks)evnt.DisciplineTask).ToString()))
                            {
                                userRepository.Reassign(Current.AgencyId, scheduleEvent, employeeId);
                                Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Reassigned, (DisciplineTasks)evnt.DisciplineTask, "( From " + UserEngine.GetName(employeeOldId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            var scheduleEvents = patientRepository.GetScheduledEventsByEmployeeAssigned(Current.AgencyId, patientId, employeeOldId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(evnt =>
                {
                    if (!evnt.IsMissedVisit && !evnt.IsDeprecated && evnt.Status == ((int)ScheduleStatus.NoteNotStarted).ToString() || evnt.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || evnt.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || evnt.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || evnt.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString() || evnt.Status == ((int)ScheduleStatus.OrderNotYetStarted).ToString() || evnt.Status == ((int)ScheduleStatus.ReportAndNotesCreated).ToString())
                    {
                        if (patientRepository.Reassign(Current.AgencyId, evnt.EpisodeId, patientId, evnt.EventId, employeeId) && ReassignScheduleEntity(evnt.EpisodeId, patientId, evnt.EventId, employeeId, ((DisciplineTasks)evnt.DisciplineTask).ToString()))
                        {
                            userRepository.Reassign(Current.AgencyId, evnt, employeeId);
                            Auditor.Log(evnt.EpisodeId, patientId, evnt.EventId, Actions.Reassigned, (DisciplineTasks)evnt.DisciplineTask, "( From " + UserEngine.GetName(employeeOldId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public PatientSchedule GetPatientWithSchedule(Guid patientId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");

            var patientSchedule = patientRepository.GetPatient<PatientSchedule>(patientId, Current.AgencyId);
            if (patientSchedule != null)
            {
                patientSchedule.Episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
                return patientSchedule;
            }
            return null;
        }

        public PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId, DateTime date, string discipline)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            PatientEpisode patientEpisode = null;
            if (date.IsValid() && discipline.IsNotNullOrEmpty())
            {
                patientEpisode = patientRepository.GetEpisode(Current.AgencyId, patientId, date, discipline);
            }
            else if (discipline.IsNotNullOrEmpty())
            {
                patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline);
            }
            else if (date.IsValid())
            {
                patientEpisode = patientRepository.GetEpisode(Current.AgencyId, patientId, date);
            }
            else
            {
                patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            }

            if (patientEpisode != null)
            {
                var assessment = assessmentService.GetEpisodeAssessment(patientEpisode);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.ToDictionary();
                    var tmpString = new List<string>();
                    if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"].Answer != "") tmpString.Add("SN Frequency:" + assessmentQuestions["485SNFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"].Answer != "") tmpString.Add("PT Frequency:" + assessmentQuestions["485PTFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"].Answer != "") tmpString.Add("OT Frequency:" + assessmentQuestions["485OTFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"].Answer != "") tmpString.Add("ST Frequency:" + assessmentQuestions["485STFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"].Answer != "") tmpString.Add("MSW Frequency:" + assessmentQuestions["485MSWFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"].Answer != "") tmpString.Add("HHA Frequency:" + assessmentQuestions["485HHAFrequency"].Answer);
                    if (tmpString.Count > 0) patientEpisode.Detail.FrequencyList = tmpString.ToArray().Join(", ") + ".";
                    else patientEpisode.Detail.FrequencyList = "";
                }
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    patientEpisode.PatientIdNumber = patient.PatientIdNumber;
                    patientEpisode.DisplayName = patient.DisplayNameWithMi;
                }
            }
            return patientEpisode;
        }

        public PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId, string discipline)
        {
            return GetPatientEpisodeWithFrequency(episodeId, patientId, DateTime.MinValue, discipline);
        }

        public PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId)
        {
            return GetPatientEpisodeWithFrequency(episodeId, patientId, DateTime.MinValue, "");

        }


        public FrequenciesViewData GetPatientEpisodeFrequencyData(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            FrequenciesViewData frequencyViewData = new FrequenciesViewData();
            var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            { 
                var assessment = assessmentService.GetEpisodeAssessment(patientEpisode);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.ToDictionary();
                    var tmpString = new List<string>();

                    string[] taskTypes = new string[6] { "SN", "PT", "OT", "ST", "MSW", "HHA" };
                    foreach (string taskType in taskTypes)
                    {
                        string assessmentFrequency = string.Format("485{0}Frequency", taskType);
                        if (assessmentQuestions.ContainsKey(assessmentFrequency) && assessmentQuestions[assessmentFrequency].Answer.IsNotNullOrEmpty())
                        {
                            frequencyViewData.Visits.Add(taskType, new VisitData(assessmentQuestions[assessmentFrequency].Answer));
                        }
                        else
                        {
                            frequencyViewData.Visits.Add(taskType, new VisitData());
                        }
                    }
                }
                else
                {
                    string[] taskTypes = new string[6] { "SN", "PT", "OT", "ST", "MSW", "HHA" };
                    foreach (string taskType in taskTypes)
                    {
                        frequencyViewData.Visits.Add(taskType, new VisitData());
                    }
                }
                if (!string.IsNullOrEmpty(patientEpisode.Schedule))
                {
                    var patientActivities = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    foreach (var item in patientActivities)
                    {
                        if (!item.IsDeprecated && item.IsBillable)
                        {
                            if (item.IsSkilledNurseNote())
                            {
                                frequencyViewData.Visits["SN"].Count++;
                            }
                            else if (item.IsHhaNote())
                            {
                                frequencyViewData.Visits["HHA"].Count++;
                            }
                            else if (item.IsPTNote())
                            {
                                frequencyViewData.Visits["PT"].Count++;
                            }
                            else if (item.IsOTNote())
                            {
                                frequencyViewData.Visits["OT"].Count++;
                            }
                            else if (item.IsSTNote())
                            {
                                frequencyViewData.Visits["ST"].Count++;
                            }
                            else if (item.IsMSW())
                            {
                                frequencyViewData.Visits["MSW"].Count++;
                            }
                        }
                    }
                }
            }
            return frequencyViewData;
        }

        public bool SaveNotes(string button, FormCollection formCollection)
        {
            var result = false;

            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                string type = keys.Contains("Type") ? formCollection["Type"] : string.Empty;
                if (type.IsNotNullOrEmpty())
                {
                    Guid eventId = keys.Contains(string.Format("{0}_EventId", type)) ? formCollection.Get(string.Format("{0}_EventId", type)).ToGuid() : Guid.Empty;
                    Guid episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
                    Guid patientId = keys.Contains(string.Format("{0}_PatientId", type)) ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;
                    var timeIn = keys.Contains(string.Format("{0}_TimeIn", type)) && formCollection[string.Format("{0}_TimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeIn", type)] : string.Empty;
                    var timeOut = keys.Contains(string.Format("{0}_TimeOut", type)) && formCollection[string.Format("{0}_TimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeOut", type)] : string.Empty;
                    var sendAsOrder = keys.Contains(string.Format("{0}_SendAsOrder", type)) && formCollection[string.Format("{0}_SendAsOrder", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_SendAsOrder", type)] : string.Empty;
                    var surcharge = keys.Contains(string.Format("{0}_Surcharge", type)) && formCollection[string.Format("{0}_Surcharge", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_Surcharge", type)] : string.Empty;
                    var mileage = keys.Contains(string.Format("{0}_AssociatedMileage", type)) && formCollection[string.Format("{0}_AssociatedMileage", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_AssociatedMileage", type)] : string.Empty;
                    string date = keys.Contains(string.Format("{0}_VisitDate", type)) ? formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime().ToShortDateString() : string.Empty;
                    int disciplineTask = keys.Contains("DisciplineTask") ? formCollection.Get("DisciplineTask").ToInteger() : 0;

                    if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);

                        if (scheduleEvent != null)
                        {
                            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                            if (patientVisitNote != null)
                            {
                                scheduleEvent.SendAsOrder = sendAsOrder;
                                var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                                patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                                patientVisitNote.Note = patientVisitNote.Questions.ToXml();
                                patientVisitNote.PhysicianId = keys.Contains(string.Format("{0}_PhysicianId", type)) ? formCollection.Get(string.Format("{0}_PhysicianId", type)).ToGuid() : Guid.Empty;
                                var shouldUpdateEpisode = false;
                                var statusAction = Actions.StatusChange;
                                var description = string.Empty;
                                var oldStatus = scheduleEvent.Status;
                                if (button == "Save")
                                {
                                    if (!(Current.HasRight(Permissions.AccessCaseManagement) && oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()))
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.NoteSaved;
                                    }
                                    else
                                    {

                                    }
                                    if (type == DisciplineTasks.PTEvaluation.ToString()
                                        || type == DisciplineTasks.PTReEvaluation.ToString()
                                        || type == DisciplineTasks.OTEvaluation.ToString()
                                        || type == DisciplineTasks.OTReEvaluation.ToString()
                                        || type == DisciplineTasks.STEvaluation.ToString()
                                        || type == DisciplineTasks.STReEvaluation.ToString()
                                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || type == DisciplineTasks.PTDischarge.ToString())
                                    {
                                        var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            patientVisitNote.PhysicianData = physician.ToXml();
                                        }
                                    }
                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    if (patientRepository.UpdateVisitNote(patientVisitNote))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = patientVisitNote.Status.ToString();
                                            scheduleEvent.TimeIn = timeIn;
                                            scheduleEvent.TimeOut = timeOut;
                                            scheduleEvent.Surcharge = surcharge;
                                            scheduleEvent.AssociatedMileage = mileage;
                                            scheduleEvent.DisciplineTask = disciplineTask;
                                            if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                            {
                                                scheduleEvent.VisitDate = date;
                                            }
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = patientVisitNote.Status.ToString();
                                                userEvent.TimeIn = timeIn;
                                                userEvent.TimeOut = timeOut;
                                                userEvent.DisciplineTask = disciplineTask;
                                                if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                                {
                                                    userEvent.VisitDate = date;
                                                }
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (button == "Complete")
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.NoteSubmittedWithSignature;
                                    if (Current.HasRight(Permissions.BypassCaseManagement))
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment)
                                        {
                                            if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                            }
                                            else if (sendAsOrder.Equals("1"))
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                            }
                                            else
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                            }
                                        }
                                        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask==(int)DisciplineTasks.SixtyDaySummary)
                                        {
                                            if (sendAsOrder.Equals("1"))
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                            }
                                        }
                                    }

                                    if (type == DisciplineTasks.PTEvaluation.ToString()
                                        || type == DisciplineTasks.PTReEvaluation.ToString()
                                        || type == DisciplineTasks.OTEvaluation.ToString()
                                        || type == DisciplineTasks.OTReEvaluation.ToString()
                                        || type == DisciplineTasks.STEvaluation.ToString()
                                        || type == DisciplineTasks.STReEvaluation.ToString()
                                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || type == DisciplineTasks.PTDischarge.ToString()
                                        || type == DisciplineTasks.SixtyDaySummary.ToString())
                                    {
                                        var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            patientVisitNote.PhysicianData = physician.ToXml();
                                        }
                                    }

                                    patientVisitNote.UserId = Current.UserId;
                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                    patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                    if (patientRepository.UpdateVisitNote(patientVisitNote))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = patientVisitNote.Status.ToString();
                                            scheduleEvent.Surcharge = surcharge;
                                            scheduleEvent.AssociatedMileage = mileage;
                                            scheduleEvent.TimeIn = timeIn;
                                            scheduleEvent.TimeOut = timeOut;
                                            scheduleEvent.DisciplineTask = disciplineTask;
                                            scheduleEvent.InPrintQueue = true;
                                            scheduleEvent.SendAsOrder = sendAsOrder;
                                            if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                            {
                                                scheduleEvent.VisitDate = date;
                                            }
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = scheduleEvent.Status.ToString();
                                                userEvent.TimeIn = timeIn;
                                                userEvent.TimeOut = timeOut;
                                                userEvent.DisciplineTask = disciplineTask;
                                                if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                                {
                                                    userEvent.VisitDate = date;
                                                }
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (button == "Approve")
                                {
                                    patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                                        || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment)
                                    {
                                        if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        }
                                        else if (sendAsOrder.Equals("1"))
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        }
                                        else
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                        }
                                    }

                                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)
                                    {
                                        if (sendAsOrder.Equals("1"))
                                        {
                                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                        }
                                    }

                                    if (type == DisciplineTasks.PTEvaluation.ToString()
                                        || type == DisciplineTasks.PTReEvaluation.ToString()
                                        || type == DisciplineTasks.OTEvaluation.ToString()
                                        || type == DisciplineTasks.OTReEvaluation.ToString()
                                        || type == DisciplineTasks.STEvaluation.ToString()
                                        || type == DisciplineTasks.STReEvaluation.ToString()
                                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || type == DisciplineTasks.PTDischarge.ToString()
                                        || type == DisciplineTasks.SixtyDaySummary.ToString())
                                    {
                                        var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            patientVisitNote.PhysicianData = physician.ToXml();
                                        }
                                    }

                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                    patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                    if (patientRepository.UpdateVisitNote(patientVisitNote))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.TimeIn = timeIn;
                                            scheduleEvent.TimeOut = timeOut;
                                            scheduleEvent.AssociatedMileage = mileage;
                                            scheduleEvent.Surcharge = surcharge;
                                            scheduleEvent.InPrintQueue = true;
                                            scheduleEvent.Status = patientVisitNote.Status.ToString();
                                            scheduleEvent.ReturnReason = string.Empty;
                                            scheduleEvent.SendAsOrder = sendAsOrder;
                                            if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                            {
                                                scheduleEvent.VisitDate = date;
                                            }
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = patientVisitNote.Status.ToString();
                                                userEvent.ReturnReason = string.Empty;
                                                if (!(userEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                                {
                                                    userEvent.VisitDate = date;
                                                }
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                else if (button == "Return")
                                {
                                    var returnSignature = keys.Contains(string.Format("{0}_ReturnForSignature", type)) && formCollection[string.Format("{0}_ReturnForSignature", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_ReturnForSignature", type)] : string.Empty;
                                    patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                    if (returnSignature.IsNotNullOrEmpty())
                                    {
                                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                                    }
                                    patientVisitNote.Modified = DateTime.Now;
                                    patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                    if (patientRepository.UpdateVisitNote(patientVisitNote))
                                    {
                                        if (scheduleEvent != null)
                                        {
                                            scheduleEvent.Status = patientVisitNote.Status.ToString();
                                            scheduleEvent.ReturnReason = string.Empty;
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = patientVisitNote.Status.ToString();
                                                userEvent.ReturnReason = string.Empty;
                                            }
                                            shouldUpdateEpisode = true;
                                        }
                                    }
                                }
                                if (shouldUpdateEpisode)
                                {
                                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                    {
                                        if (userEvent != null)
                                        {
                                            if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                            {
                                                result = true;
                                            }
                                        }
                                        else
                                        {
                                            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                                            result = true;
                                        }
                                        if (scheduleEvent.Status.IsInteger())
                                        {
                                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessMissedVisitNotes(string button, Guid Id)
        {
            var mv = patientRepository.GetMissedVisit(Current.AgencyId, Id);
            if (button == "Approve")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else if (button == "Return")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
            }
            if (patientRepository.UpdateMissedVisit(mv))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessMissedVisitForm(string button, Guid eventId, string reason)
        {
            var result = false;
            var mv = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
            if (button == "Approve")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else if (button == "Return")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
            }
            if (patientRepository.UpdateMissedVisit(mv))
            {
                result = true;
            }
            return result;
        }

        public bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            var patientVisitNote = new PatientVisitNote();
            var isMissedVisit = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                    isMissedVisit = scheduleEvent.IsMissedVisit;
                }
                if (isMissedVisit)
                {// Process Missed Visit Form 
                    var mv = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
                    if (mv != null)
                    {
                        if (button == "Approve") mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                        else if (button == "Return") mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
                        if (patientRepository.UpdateMissedVisit(mv)) result = true;
                    }
                }
                else
                {// Process regular notes
                    patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                    if (patientVisitNote != null)
                    {
                        if (button == "Approve")
                        {
                            patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                            if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment) {
                                if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1) patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                else if (scheduleEvent.SendAsOrder == "1") patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                else patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                            }
                            if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)
                            {
                                if (scheduleEvent.SendAsOrder == "1") patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                            }
                            patientVisitNote.Modified = DateTime.Now;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.InPrintQueue = true;
                                    scheduleEvent.Status = patientVisitNote.Status.ToString();
                                    if (userEvent != null) userEvent.Status = patientVisitNote.Status.ToString();
                                    shouldUpdateEpisode = true;
                                }
                            }
                        }
                        else if (button == "Return")
                        {
                            patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                            patientVisitNote.Modified = DateTime.Now;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                    if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                    shouldUpdateEpisode = true;
                                }
                            }
                        }
                        else if (button == "Print")
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = false;
                                shouldUpdateEpisode = true;
                            }
                        }
                        if (shouldUpdateEpisode)
                        {
                            if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                if (userEvent != null)
                                {
                                    if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                                }
                                else
                                {
                                    userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                    result = true;
                                }
                                if (scheduleEvent.Status.IsInteger()) Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty())
            {
                Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                var patientVisitNote = new PatientVisitNote();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                    if (patientVisitNote != null)
                    {
                        patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                        if (patientVisitNote.Questions != null)
                        {
                            if (httpFiles.Count > 0)
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                var scheduleEvent = scheduleEvents.FirstOrDefault(e => e.EventId == eventId && e.EpisodeId == episodeId && e.PatientId == patientId);

                                foreach (string key in httpFiles.AllKeys)
                                {
                                    var keyArray = key.Split('_');
                                    HttpPostedFileBase file = httpFiles.Get(key);
                                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                                    {
                                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                                        var asset = new Asset
                                        {
                                            FileName = file.FileName,
                                            AgencyId = Current.AgencyId,
                                            ContentType = file.ContentType,
                                            FileSize = file.ContentLength.ToString(),
                                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                        };

                                        if (assetRepository.Add(asset))
                                        {
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = asset.Id.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = asset.Id.ToString(), Type = keyArray[0] });
                                            }
                                            if (!scheduleEvent.Assets.Contains(asset.Id))
                                            {
                                                scheduleEvent.Assets.Add(asset.Id);
                                                scheduleEvents.RemoveAll(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                                                scheduleEvents.Add(scheduleEvent);
                                                episode.Schedule = scheduleEvents.ToXml();
                                                patientRepository.UpdateEpisode(episode);
                                            }
                                        }
                                        else
                                        {
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                            }
                                            assetsSaved = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                        {
                                            patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                        }
                                        else
                                        {
                                            patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                        }
                                    }
                                }
                            }
                            patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                            patientVisitNote.IsWoundCare = true;
                            if (assetsSaved && patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                                Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care is added/updated.");
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    patientVisitNote.Questions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (patientVisitNote.Questions.Exists(q => q.Name == name))
                    {
                        patientVisitNote.Questions.SingleOrDefault(q => q.Name == name).Answer = Guid.Empty.ToString();
                        patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                                {
                                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                    var scheduleEvent = scheduleEvents.FirstOrDefault(e => e.EventId == eventId && e.EpisodeId == episodeId && e.PatientId == patientId);
                                    if (scheduleEvent!=null && scheduleEvent.Assets!=null && scheduleEvent.Assets.Contains(assetId))
                                    {
                                        scheduleEvent.Assets.Remove(assetId);
                                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                        {
                                            result = true;
                                        }
                                    }
                                    else
                                    {
                                        result = true;
                                    }
                                }
                            }
                            Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care Asset is deleted.");
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Rap CreateRap(Patient patient, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician)
        {
            var rap = new Rap
             {
                 Id = episode.Id,
                 AgencyId = patient.AgencyId,
                 PatientId = patient.Id,
                 EpisodeId = episode.Id,
                 EpisodeStartDate = episode.StartDate,
                 EpisodeEndDate = episode.EndDate,
                 IsFirstBillableVisit = false,
                 IsOasisComplete = false,
                 PatientIdNumber = patient.PatientIdNumber,
                 IsGenerated = false,
                 MedicareNumber = patient.MedicareNumber,
                 FirstName = patient.FirstName,
                 LastName = patient.LastName,
                 DOB = patient.DOB,
                 Gender = patient.Gender,
                 AddressLine1 = patient.AddressLine1,
                 AddressLine2 = patient.AddressLine2,
                 AddressCity = patient.AddressCity,
                 AddressStateCode = patient.AddressStateCode,
                 AddressZipCode = patient.AddressZipCode,
                 StartofCareDate = episode.StartOfCareDate,
                 AreOrdersComplete = false,
                 AdmissionSource = patient.AdmissionSource,
                 PatientStatus = patient.Status,
                 UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                 Status = (int)BillingStatus.ClaimCreated,
                 HealthPlanId = patient.PrimaryHealthPlanId,
                 Relationship = patient.PrimaryRelationship,
                 DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                 ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                 Created = DateTime.Now
             };
            if (insuranceId > 0)
            {
                rap.PrimaryInsuranceId = insuranceId;
            }
            else if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
            {
                rap.PrimaryInsuranceId = patient.PrimaryInsurance.ToInteger();
            }
            if (agencyPhysician != null)
            {
                rap.PhysicianNPI = agencyPhysician.NPI;
                rap.PhysicianFirstName = agencyPhysician.FirstName;
                rap.PhysicianLastName = agencyPhysician.LastName;
            }
            //if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            //{
            //    if (!patient.AgencyPhysicians[0].IsEmpty())
            //    {
            //        var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
            //        if (physician != null)
            //        {
            //            rap.PhysicianNPI = physician.NPI;
            //            rap.PhysicianFirstName = physician.FirstName;
            //            rap.PhysicianLastName = physician.LastName;
            //        }
            //    }
            //}
            return rap;
        }

        private int GetMedicareInsuranceFromPatient(Patient patient)
        {
            int insurance = -1;
            if (patient.PrimaryInsurance == "1"
                || patient.PrimaryInsurance == "2"
                || patient.PrimaryInsurance == "3"
                || patient.PrimaryInsurance == "4")
            {
                insurance = patient.PrimaryInsurance.ToInteger();
            }
            else if (patient.SecondaryInsurance == "1"
                || patient.SecondaryInsurance == "2"
                || patient.SecondaryInsurance == "3"
                || patient.SecondaryInsurance == "4")
            {
                insurance = patient.SecondaryInsurance.ToInteger();
            }
            else if (patient.TertiaryInsurance == "1"
                || patient.TertiaryInsurance == "2"
                || patient.TertiaryInsurance == "3"
                || patient.TertiaryInsurance == "4")
            {
                insurance = patient.TertiaryInsurance.ToInteger();
            }
            return insurance;
        }

        public Final CreateFinal(Patient patient, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician)
        {
            var final = new Final
            {
                Id = episode.Id,
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeId = episode.Id,
                EpisodeStartDate = episode.StartDate,
                EpisodeEndDate = episode.EndDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                MedicareNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = episode.StartOfCareDate,
                AdmissionSource = patient.AdmissionSource,
                PatientStatus = patient.Status,
                UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                AreOrdersComplete = false,
                AreVisitsComplete = false,
                Status = (int)BillingStatus.ClaimCreated,
                IsSupplyVerified = false,
                IsFinalInfoVerified = false,
                IsVisitVerified = false,
                IsRapGenerated = false,
                HealthPlanId = patient.PrimaryHealthPlanId,
                Relationship = patient.PrimaryRelationship,
                Created = DateTime.Now,
                IsSupplyNotBillable = false,
                DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                Modified = DateTime.Now
            };
            if (insuranceId > 0)
            {
                final.PrimaryInsuranceId = insuranceId;
            }
            else if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
            {
                final.PrimaryInsuranceId = patient.PrimaryInsurance.ToInteger();
            }
            if (agencyPhysician != null)
            {
                final.PhysicianNPI = agencyPhysician.NPI;
                final.PhysicianFirstName = agencyPhysician.FirstName;
                final.PhysicianLastName = agencyPhysician.LastName;
            }
            //if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            //{
            //    if (!patient.AgencyPhysicians[0].IsEmpty())
            //    {
            //        var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
            //        if (physician != null)
            //        {
            //            final.PhysicianNPI = physician.NPI;
            //            final.PhysicianFirstName = physician.FirstName;
            //            final.PhysicianLastName = physician.LastName;
            //        }
            //    }
            //}
            return final;
        }

        public bool AddClaim(Guid patientId, Guid episodeId, string type, int insuranceId)
        {
            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (episode != null && patient != null)
            {
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                if (type == "RAP" || type == "Rap")
                {
                    var rap = CreateRap(patient, episode, insuranceId, physician);
                    if (billingRepository.AddRap(rap))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                        result = true;
                    }
                }
                else if (type == "Final" || type == "FINAL")
                {
                    var final = CreateFinal(patient, episode, insuranceId, physician);
                    if (billingRepository.AddFinal(final))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool DeleteClaim(Guid patientId, Guid Id, string type)
        {
            bool result = false;
            if (type == "RAP" || type == "Rap")
            {
                if (billingRepository.DeleteRap(Current.AgencyId, patientId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Rap, LogAction.RAPDeleted, string.Empty);
                    result = true;
                }
            }
            else if (type == "Final" || type == "FINAL")
            {
                if (billingRepository.DeleteFinal(Current.AgencyId, patientId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public ManagedClaim CreateManagedClaim(Patient patient, DateTime startDate, DateTime endDate, int insuranceId)
        {
            var managedClaim = new ManagedClaim
            {
                Id = Guid.NewGuid(),
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeStartDate = startDate,
                EpisodeEndDate = endDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                IsuranceIdNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = patient.StartofCareDate,
                AreOrdersComplete = false,
                AdmissionSource = patient.AdmissionSource,
                PatientStatus = patient.Status,
                UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                Status = (int)ManagedClaimStatus.ClaimCreated,
                HealthPlanId = patient.PrimaryHealthPlanId,
                PrimaryInsuranceId = insuranceId,
                Relationship = patient.PrimaryRelationship,
                DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                Created = DateTime.Now
            };

            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        managedClaim.PhysicianNPI = physician.NPI;
                        managedClaim.PhysicianFirstName = physician.FirstName;
                        managedClaim.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return managedClaim;
        }

        public bool AddManagedClaim(Guid patientId, DateTime startDate, DateTime endDate, int insuranceId)
        {
            bool result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var managedClaim = CreateManagedClaim(patient, startDate, endDate, insuranceId);
                if (billingRepository.AddManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedClaimAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                missedVisit.AgencyId = Current.AgencyId;
                var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                var scheduledEvent = events.Find(e => e.EventId == missedVisit.Id);
                if (scheduledEvent != null)
                {
                    scheduledEvent.IsMissedVisit = true;
                    scheduledEvent.MissedVisitFormReturnReason = "";//Clean up return reason if any. 
                    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduledEvent.UserId, scheduledEvent.PatientId, scheduledEvent.EventId);
                    if (userEvent != null)
                    {
                        userEvent.IsMissedVisit = true;
                        userRepository.UpdateEvent(Current.AgencyId, userEvent);
                    }
                    var existing = patientRepository.GetMissedVisit(Current.AgencyId, scheduledEvent.EventId);
                    if (existing != null)
                    {
                        existing.EventDate = missedVisit.EventDate;
                        existing.IsOrderGenerated = missedVisit.IsOrderGenerated;
                        existing.IsPhysicianOfficeNotified = missedVisit.IsPhysicianOfficeNotified;
                        existing.Reason = missedVisit.Reason;
                        existing.SignatureDate = missedVisit.SignatureDate;
                        existing.SignatureText = missedVisit.SignatureText;
                        existing.Comments = missedVisit.Comments;
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            existing.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                        }
                        else
                        {
                            existing.Status = (int)ScheduleStatus.NoteMissedVisitPending;
                        }
                        patientRepository.UpdateMissedVisit(existing);
                    }
                    else
                    {
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                        }
                        else
                        {
                            missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitPending;
                        }
                        patientRepository.AddMissedVisit(missedVisit);
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                    {
                        result = true;
                        if (Enum.IsDefined(typeof(DisciplineTasks), scheduledEvent.DisciplineTask))
                        {
                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduledEvent.DisciplineTask), "Set as a missed visit by " + Current.UserFullName);
                        }
                    }
                }
            }
            return result;
        }

        public bool IsValidImage(HttpFileCollectionBase httpFiles)
        {
            var result = false;
            if (httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Photo1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                    if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
                    {
                        var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (allowedExtensions != null && allowedExtensions.Length > 0)
                        {
                            allowedExtensions.ForEach(extension =>
                            {
                                if (fileExtension.IsEqual(extension))
                                {
                                    result = true;
                                    return;
                                }
                            });
                        }
                    }
                }
            }
            return result;
        }

        public bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                var supplies = new List<Supply>();
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    supply.UniqueIdentifier = Guid.NewGuid();
                }
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies != null && supplies.Count > 0)
                    {
                        var existingSupply = supplies.Find(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (existingSupply != null)
                        {
                            existingSupply.Date = supply.Date;
                            existingSupply.Quantity = supply.Quantity;
                            existingSupply.Description = supply.Description;
                        }
                        else
                        {
                            supplies.Add(supply);
                        }
                    }
                    else
                    {
                        supplies = new List<Supply> { supply };
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                patientVisitNote.Supply = supplies.ToXml();
                patientVisitNote.IsSupplyExist = true;
                if (patientRepository.UpdateVisitNote(patientVisitNote))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            editSupply.UnitCost = supply.UnitCost;
                            editSupply.Description = supply.Description;
                            editSupply.DateForEdit = supply.DateForEdit;
                            editSupply.Code = supply.Code;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            var list = new List<Supply>();
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    list = patientVisitNote.Supply.ToObject<List<Supply>>();
                }
            }
            return list;
        }

        public PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility patientEligibility = null;
            try
            {
                var jsonData = new
                {
                    input_medicare_number = medicareNumber,
                    input_last_name = lastName,
                    input_first_name = firstName.Substring(0, 1),
                    input_date_of_birth = dob.ToString("MM/dd/yyyy"),
                    input_gender_id = gender.Substring(0, 1)
                };

                var javaScriptSerializer = new JavaScriptSerializer();
                var jsonRequest = javaScriptSerializer.Serialize(jsonData);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonRequest);
                byte[] requestData = encoding.GetBytes(postData);

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(AppSettings.PatientEligibilityUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                var jsonResult = string.Empty;
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jsonResult = streamReader.ReadToEnd();
                    }

                    if (jsonResult.IsNotNullOrEmpty())
                    {
                        patientEligibility = javaScriptSerializer.Deserialize<PatientEligibility>(jsonResult);
                        if (patientEligibility != null && patientEligibility.Episode != null && patientEligibility.Episode.reference_id.IsNotNullOrEmpty())
                        {
                            var npiData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(patientEligibility.Episode.reference_id.Trim());
                            if (npiData != null)
                            {
                                patientEligibility.Other_Agency_Data = new OtherAgencyData()
                                {
                                    name = npiData.ProviderOrganizationName,
                                    address1 = npiData.ProviderFirstLineBusinessMailingAddress,
                                    address2 = npiData.ProviderSecondLineBusinessMailingAddress,
                                    city = npiData.ProviderBusinessMailingAddressCityName,
                                    state = npiData.ProviderBusinessMailingAddressStateName,
                                    zip = npiData.ProviderBusinessMailingAddressPostalCode,
                                    phone = npiData.ProviderBusinessMailingAddressTelephoneNumber,
                                    fax = npiData.ProviderBusinessMailingAddressFaxNumber
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return patientEligibility;
        }

        public List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId)
        {
            var eligibilities = patientRepository.GetMedicareEligibilities(Current.AgencyId, patientId);
            if (eligibilities != null)
            {
                eligibilities.ForEach(e =>
                {
                    if (!e.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, e.EpisodeId, e.PatientId);
                        if (episode != null)
                        {
                            e.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                        }
                        else
                        {
                            e.EpisodeRange = "Not in an episode";
                        }
                    }
                    e.AssignedTo = "Axxess";
                    e.TaskName = "Medicare Eligibility Report";
                    e.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + e.PatientId + "', 'mcareEligibilityId': '" + e.Id + "' });\"><span class='img icon print'></span></a>";
                });
            }
            return eligibilities.OrderBy(e => e.Created.ToShortDateString().ToZeroFilled()).ToList();
        }

        public string GetScheduledEventUrl(PatientEpisode episode, DisciplineTasks task)
        {
            var url = string.Empty;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>().OrderBy(e => e.EventDate).ToList();
                if (schedule != null && schedule.Count > 0)
                {
                    switch (task)
                    {
                        case DisciplineTasks.HHAideCarePlan:
                            var hhaCarePlan = GetCarePlanBySelectedEpisode(episode.PatientId, episode.Id, DisciplineTasks.HHAideCarePlan);
                            if (hhaCarePlan != null)
                            {
                                url = new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({")
                                .AppendFormat("Url: '/HHACarePlan/View/{0}/{1}/{2}'", hhaCarePlan.EpisodeId, hhaCarePlan.PatientId, hhaCarePlan.Id)
                                .Append("})\">View Care Plan</a>").ToString();
                            }
                            break;
                        case DisciplineTasks.PTEvaluation:
                            var ptEval = schedule.Where(e => e.DisciplineTask == (int)task).FirstOrDefault();
                            if (ptEval != null)
                            {
                                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), ptEval.EpisodeId, ptEval.PatientId, ptEval.EventId);
                            }
                            else
                            {
                                var ptReEval = schedule.Where(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation).FirstOrDefault();
                                if (ptReEval != null)
                                {
                                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.PTReEvaluation.ToString(), ptReEval.EpisodeId, ptReEval.PatientId, ptReEval.EventId);
                                }
                            }
                            break;
                        case DisciplineTasks.STEvaluation:
                            var stEval = schedule.Where(e => e.DisciplineTask == (int)task).FirstOrDefault();
                            if (stEval != null)
                            {
                                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), stEval.EpisodeId, stEval.PatientId, stEval.EventId);
                            }
                            else
                            {
                                var stReEval = schedule.Where(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation).FirstOrDefault();
                                if (stReEval != null)
                                {
                                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.STReEvaluation.ToString(), stReEval.EpisodeId, stReEval.PatientId, stReEval.EventId);
                                }
                            }
                            break;
                        case DisciplineTasks.OTEvaluation:
                            var otEval = schedule.Where(e => e.DisciplineTask == (int)task).FirstOrDefault();
                            if (otEval != null)
                            {
                                url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), otEval.EpisodeId, otEval.PatientId, otEval.EventId);
                            }
                            else
                            {
                                var otReEval = schedule.Where(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation).FirstOrDefault();
                                if (otReEval != null)
                                {
                                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View ReEval</a>", DisciplineTasks.OTReEvaluation.ToString(), otReEval.EpisodeId, otReEval.PatientId, otReEval.EventId);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            return url;
        }

        public PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new PlanofCareViewData();
            var planofcare = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
            if (planofcare != null)
            {
                if (planofcare.Data.IsNotNullOrEmpty()) planofcare.Questions = planofcare.Data.ToObject<List<Question>>();
                viewData.EditData = planofcare.ToDictionary();
                var dictionary = new Dictionary<string, string>();
                dictionary.Add("Id", eventId.ToString());
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    dictionary.Add("PatientId", patient.Id.ToString());
                    dictionary.Add("PatientName", patient.DisplayName);
                    dictionary.Add("PatientIdNumber", patient.PatientIdNumber);
                    dictionary.Add("PatientMedicareNumber", patient.MedicareNumber);
                    dictionary.Add("PatientAddressFirstRow", patient.AddressFirstRow);
                    dictionary.Add("PatientAddressSecondRow", patient.AddressSecondRow);
                    dictionary.Add("PatientPhone", patient.PhoneHomeFormatted);
                    dictionary.Add("PatientDoB", patient.DOBFormatted);
                    dictionary.Add("PatientGender", patient.Gender);
                    if (!patient.AgencyId.IsEmpty())
                    {
                        var agency = agencyRepository.Get(patient.AgencyId);
                        if (agency != null)
                        {
                            dictionary.Add("AgencyId", agency.Id.ToString());
                            dictionary.Add("AgencyMedicareProviderNumber", agency.MedicareProviderNumber);
                            dictionary.Add("AgencyName", agency.Name);
                            dictionary.Add("AgencyAddressFirstRow", agency.MainLocation.AddressFirstRow);
                            dictionary.Add("AgencyAddressSecondRow", agency.MainLocation.AddressSecondRow);
                            dictionary.Add("AgencyPhone", agency.MainLocation.PhoneWorkFormatted);
                            dictionary.Add("AgencyFax", agency.MainLocation.FaxNumberFormatted);
                        }
                    }
                    if (planofcare.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = planofcare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            dictionary.Add("PhysicianId", physician.Id.ToString());
                            dictionary.Add("PhysicianName", physician.DisplayName);
                            dictionary.Add("PhysicianNpi", physician.NPI);
                        }
                    }
                    else
                    {
                        var primaryPhysician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (primaryPhysician != null)
                        {
                            dictionary.Add("PhysicianId", primaryPhysician.Id.ToString());
                            dictionary.Add("PhysicianName", primaryPhysician.DisplayName);
                            dictionary.Add("PhysicianNpi", primaryPhysician.NPI);
                        }
                    }
                }
                var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    dictionary.Add("PatientSoC", episode.StartOfCareDate.ToString("MM/dd/yyyy"));
                    dictionary.Add("EpisodeId", episode.Id.ToString());
                    dictionary.Add("EpisodeCertPeriod", string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted));
                }
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null && scheduledEvent.EventDate.IsValidDate()) dictionary.Add("StatusComment", scheduledEvent.StatusComment);
                viewData.DisplayData = dictionary;
            }
            return viewData;
        }

        public void AddInfectionUserAndScheduleEvent(Infection infection, out Infection infectionOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = infection.Id,
                UserId = infection.UserId,
                PatientId = infection.PatientId,
                EpisodeId = infection.EpisodeId,
                Status = infection.Status.ToString(),
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                EventDate = infection.InfectionDate.ToShortDateString(),
                VisitDate = infection.InfectionDate.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.InfectionReport
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, infection.EpisodeId, infection.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.InfectionReport);
            }
            infectionOut = infection;
        }

        public void AddIncidentUserAndScheduleEvent(Incident incident, out Incident incidentOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = incident.Id,
                UserId = incident.UserId,
                PatientId = incident.PatientId,
                EpisodeId = incident.EpisodeId,
                Status = incident.Status.ToString(),
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                EventDate = incident.IncidentDate.ToShortDateString(),
                VisitDate = incident.IncidentDate.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, incident.EpisodeId, incident.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.IncidentAccidentReport);
            }
            incident.EpisodeId = newScheduleEvent.EpisodeId;
            incidentOut = incident;
        }

        public List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId)
        {
            var visitNoteviewDatas = new List<VisitNoteViewData>();
            var visits = patientRepository.GetVisitNotesByDisciplineTaskWithStatus(Current.AgencyId, patientId, DisciplineTasks.SixtyDaySummary, (int)ScheduleStatus.NoteCompleted);
            if (visits != null && visits.Count > 0)
            {
                visits.ForEach(v =>
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, v.EpisodeId, patientId);
                    if (episode != null && episode.IsDischarged == false && episode.IsActive == true)
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        if (scheduleEvents != null && scheduleEvents.Count > 0)
                        {
                            var scheduleEvent = scheduleEvents.SingleOrDefault(s => s.EventId == v.Id);
                            if (scheduleEvent != null)
                            {
                                var visitNoteviewData = new VisitNoteViewData();
                                var user = userRepository.GetUserOnly(v.UserId, Current.AgencyId);
                                if (user != null)
                                {
                                    visitNoteviewData.UserDisplayName = user.DisplayName;
                                }
                                var questions = v.ToDictionary();
                                if (questions != null && questions.Count > 0)
                                {
                                    var physicianId = questions.ContainsKey("Physician") && questions["Physician"].Answer.IsNotNullOrEmpty() && questions["Physician"].Answer.IsGuid() ? questions["Physician"].Answer.ToGuid() : Guid.Empty;
                                    visitNoteviewData.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate.ToZeroFilled() : scheduleEvent.EventDate.ToZeroFilled();
                                    if (!physicianId.IsEmpty())
                                    {
                                        var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            visitNoteviewData.PhysicianDisplayName = physician.DisplayName;
                                        }
                                    }
                                }
                                visitNoteviewData.StartDate = episode.StartDate;
                                visitNoteviewData.EndDate = episode.EndDate;
                                visitNoteviewData.SignatureDate = questions.ContainsKey("SignatureDate") && questions["SignatureDate"].Answer.IsNotNullOrEmpty() && questions["SignatureDate"].Answer.IsValidDate() ? questions["SignatureDate"].Answer : string.Empty;
                                visitNoteviewData.PrintUrl = Url.Print(scheduleEvent, true);
                                visitNoteviewDatas.Add(visitNoteviewData);
                            }
                        }
                    }
                });
            }
            return visitNoteviewDatas;
        }

        public List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSign>();
            var scheduleEvents = this.GetScheduledEventsWithUsers(patientId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(s =>
                {
                    if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                    {
                        if (s.IsStartofCareAssessment() || s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment())
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var assessment = assessmentService.GetAssessment(s.EventId, Enum.GetName(typeof(DisciplineTasks), s.DisciplineTask));
                            if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                            {
                                var questions = assessment.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                if (questions != null)
                                {
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                    vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                    vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                    vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                    vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                    vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                    vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                    vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                    vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                    vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");
                                    var bs = new List<double>(); // var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                    var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSAM > 0)
                                    {
                                        bs.Add(BSAM);
                                    }
                                    var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSNoon > 0)
                                    {
                                        bs.Add(BSNoon);
                                    }
                                    var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSPM > 0)
                                    {
                                        bs.Add(BSPM);
                                    }
                                    var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSHS > 0)
                                    {
                                        bs.Add(BSHS);
                                    }
                                    var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                    var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                    vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                    vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                    vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                    vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                }
                                var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                if (user.IsNotNullOrEmpty())
                                {
                                    vitalSign.UserDisplayName = user;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                        else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.HHAideVisit ||
                            ((DisciplineTasks)s.DisciplineTask).GetFormGroup() == "UAP")
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var vitalSign = new VitalSign();
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var questions = visitNote.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                if (questions != null)
                                {
                                    vitalSign.BPLying = "";
                                    vitalSign.BPSitting = questions.ContainsKey("VitalSignBPVal") ? questions["VitalSignBPVal"].Answer : string.Empty;
                                    vitalSign.BPStanding = "";
                                    vitalSign.ApicalPulse = "";
                                    vitalSign.RadialPulse = "";
                                    vitalSign.PainLevel = "";
                                    vitalSign.BSMax = "";
                                    vitalSign.BSMin = "";
                                    vitalSign.Weight = questions.ContainsKey("VitalSignWeightVal") ? questions["VitalSignWeightVal"].Answer : string.Empty;
                                    vitalSign.Temp = questions.ContainsKey("VitalSignTempVal") ? questions["VitalSignTempVal"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("VitalSignRespVal") ? questions["VitalSignRespVal"].Answer : string.Empty;
                                }
                                var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                if (user.IsNotNullOrEmpty())
                                {
                                    vitalSign.UserDisplayName = user;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                        else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTVisit
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTMaintenance
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTAVisit
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STVisit
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STReEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STMaintenance
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTMaintenance
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReassessment
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTVisit)
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var vitalSign = new VitalSign();
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var questions = visitNote.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                if (questions != null)
                                {
                                    string SBP = questions.ContainsKey("GenericBloodPressure") ? questions["GenericBloodPressure"].Answer : string.Empty;
                                    string DBP = questions.ContainsKey("GenericBloodPressurePer") ? questions["GenericBloodPressurePer"].Answer : string.Empty;

                                    vitalSign.BPLying = "";
                                    vitalSign.BPSitting = SBP.IsNotNullOrEmpty() && DBP.IsNotNullOrEmpty() ? string.Format("{0}/{1}", SBP, DBP) : string.Empty;
                                    vitalSign.BPStanding = "";
                                    vitalSign.ApicalPulse = "";
                                    vitalSign.RadialPulse = "";
                                    vitalSign.PainLevel = questions.ContainsKey("GenericPainLevel") ? questions["GenericPainLevel"].Answer : string.Empty;
                                    vitalSign.OxygenSaturation = questions.ContainsKey("GenericO2Sat") ? questions["GenericO2Sat"].Answer : string.Empty;
                                    vitalSign.BSMax = questions.ContainsKey("GenericBloodSugar") ? questions["GenericBloodSugar"].Answer : string.Empty;
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;
                                }
                                var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                if (user.IsNotNullOrEmpty())
                                {
                                    vitalSign.UserDisplayName = user;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                        else if (((DisciplineTasks)s.DisciplineTask).GetCustomShortDescription() == "SNV")
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var vitalSign = new VitalSign();
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var questions = visitNote.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                if (questions != null)
                                {
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                    vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                    vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                    vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                    vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                    vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                    vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;


                                    vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                    vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                    vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");


                                    var bs = new List<double>(); //new double[] { BSAM, BSNoon, BSPM, BSHS };
                                    var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSAM > 0)
                                    {
                                        bs.Add(BSAM);
                                    }
                                    var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSNoon > 0)
                                    {
                                        bs.Add(BSNoon);
                                    }
                                    var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSPM > 0)
                                    {
                                        bs.Add(BSPM);
                                    }
                                    var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSHS > 0)
                                    {
                                        bs.Add(BSHS);
                                    }
                                    var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                    var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                    vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                    vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
                                    vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;// questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                    vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                }
                                var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                if (user.IsNotNullOrEmpty())
                                {
                                    vitalSign.UserDisplayName = user;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                    }
                });
            }
            return vitalSigns;
        }

        public List<VitalSign> GetVitalSignsForSixtyDaySummary(Guid patientId, Guid episodeId, DateTime date)
        {
            var vitalSigns = new List<VitalSign>();
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => Enum.IsDefined(typeof(DisciplineTasks), e.DisciplineTask) && e.EventDate.IsValidDate() && e.EventDate.ToDateTime() >= episode.StartDate && e.EventDate.ToDateTime() <= episode.EndDate && e.EventDate.ToDateTime().Date <= date.Date && !e.IsMissedVisit && e.IsDeprecated == false).ToList();
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    scheduleEvents.ForEach(s =>
                    {
                        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            if (((DisciplineTasks)s.DisciplineTask).GetCustomShortDescription() == "SNV")
                            {
                                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                                {
                                    var questions = visitNote.ToDictionary();

                                    vitalSign.VisitDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                    if (questions != null)
                                    {
                                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;

                                        vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                        vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                        vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                        vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                        vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                        vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                        vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                        vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
                                        var bs = new List<double>(); //var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                        var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSAM > 0)
                                        {
                                            bs.Add(BSAM);
                                        }
                                        var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSNoon > 0)
                                        {
                                            bs.Add(BSNoon);
                                        }
                                        var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSPM > 0)
                                        {
                                            bs.Add(BSPM);
                                        }
                                        var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSHS > 0)
                                        {
                                            bs.Add(BSHS);
                                        }

                                        var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                        var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                        vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;//questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                        vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                        vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                    }
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                            else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.HHAideVisit ||
                            ((DisciplineTasks)s.DisciplineTask).GetFormGroup() == "UAP")
                            {
                                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                                {
                                    var questions = visitNote.ToDictionary();
                                    vitalSign.VisitDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                    if (questions != null)
                                    {
                                        vitalSign.BPLying = "";
                                        vitalSign.BPSitting = questions.ContainsKey("VitalSignBPVal") ? questions["VitalSignBPVal"].Answer : string.Empty;
                                        vitalSign.BPStanding = "";

                                        vitalSign.ApicalPulse = questions.ContainsKey("VitalSignHRVal") ? questions["VitalSignHRVal"].Answer : string.Empty;
                                        vitalSign.RadialPulse = questions.ContainsKey("VitalSignHRVal") ? questions["VitalSignHRVal"].Answer : string.Empty;

                                        vitalSign.PainLevel = "";

                                        vitalSign.BSMax = "";
                                        vitalSign.BSMin = "";

                                        vitalSign.Weight = questions.ContainsKey("VitalSignWeightVal") ? questions["VitalSignWeightVal"].Answer : string.Empty;
                                        vitalSign.Temp = questions.ContainsKey("VitalSignTempVal") ? questions["VitalSignTempVal"].Answer : string.Empty;
                                        vitalSign.Resp = questions.ContainsKey("VitalSignRespVal") ? questions["VitalSignRespVal"].Answer : string.Empty;
                                    }
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                            else if (((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTVisit
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTMaintenance
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTReassessment
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.PTAVisit
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STVisit
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STReEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STMaintenance
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.STEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTMaintenance
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReassessment
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTReEvaluation
                            || ((DisciplineTasks)s.DisciplineTask) == DisciplineTasks.OTVisit)
                            {
                                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                                {
                                    vitalSign.DisciplineTask = s.DisciplineTaskName;
                                    var questions = visitNote.ToDictionary();
                                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                    if (questions != null)
                                    {
                                        string SBP = questions.ContainsKey("GenericBloodPressure") ? questions["GenericBloodPressure"].Answer : string.Empty;
                                        string DBP = questions.ContainsKey("GenericBloodPressurePer") ? questions["GenericBloodPressurePer"].Answer : string.Empty;

                                        vitalSign.BPLying = "";
                                        vitalSign.BPSitting = SBP.IsNotNullOrEmpty() && DBP.IsNotNullOrEmpty() ? string.Format("{0}/{1}", SBP, DBP) : string.Empty;
                                        vitalSign.BPStanding = "";
                                        vitalSign.ApicalPulse = "";
                                        vitalSign.RadialPulse = "";
                                        vitalSign.PainLevel = questions.ContainsKey("GenericPainLevel") ? questions["GenericPainLevel"].Answer : string.Empty;
                                        vitalSign.OxygenSaturation = questions.ContainsKey("GenericO2Sat") ? questions["GenericO2Sat"].Answer : string.Empty;
                                        vitalSign.BSMax = questions.ContainsKey("GenericBloodSugar") ? questions["GenericBloodSugar"].Answer : string.Empty;
                                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;
                                    }
                                    var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                    if (user.IsNotNullOrEmpty())
                                    {
                                        vitalSign.UserDisplayName = user;
                                    }
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                            else if (s.IsStartofCareAssessment() || s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment())
                            {
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var assessment = assessmentService.GetAssessment(s.EventId, Enum.GetName(typeof(DisciplineTasks), s.DisciplineTask));
                                if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                                {
                                    var questions = assessment.ToDictionary();
                                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                    if (questions != null)
                                    {
                                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                        vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                        vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                        vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                        vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                        vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                        vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                        var bs = new List<double>(); //var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                        var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSAM > 0)
                                        {
                                            bs.Add(BSAM);
                                        }
                                        var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSNoon > 0)
                                        {
                                            bs.Add(BSNoon);
                                        }
                                        var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSPM > 0)
                                        {
                                            bs.Add(BSPM);
                                        }
                                        var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                        if (BSHS > 0)
                                        {
                                            bs.Add(BSHS);
                                        }

                                        var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                        var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                        vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                        vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                        vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                        vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                        vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                    }
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                        }
                    });
                }
            }
            return vitalSigns;
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return lookupRepository.GetDisciplineTask(disciplineTaskId);
        }

        public List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetAllEpisodeAfterApril(Current.AgencyId, branchId, patientId, startDate, endDate);
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && (e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date && (e.IsDeprecated == false)) && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).OrderBy(e => e.EventDate.ToDateTime().Date).ToList();
                        if (scheduleEvents != null && scheduleEvents.Count > 0)
                        {
                            var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                            if (disciplines.Length == 1)
                            {
                                var evnt19check = true;
                                var evnt13check = true;
                                if (scheduleEvents.Count >= 13)
                                {
                                    var evnt13 = scheduleEvents[12];
                                    if (evnt13.EventDate.IsValidDate())
                                    {
                                        var date13 = evnt13.EventDate.ToDateTime();
                                        var scheduleEvents13 = scheduleEvents.Where(e => (e.EventDate.ToDateTime().Date == date13.Date)).ToList();
                                        if (scheduleEvents13 != null)
                                        {
                                            if (evnt13.Discipline == "PT")
                                            {
                                                evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                            }
                                            else if (evnt13.Discipline == "OT")
                                            {
                                                evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                            }
                                            else if (evnt13.Discipline == "ST")
                                            {
                                                evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                            }
                                        }
                                    }
                                    if (evnt13 != null)
                                    {
                                        episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate.IsValidDate() ? evnt13.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                    }
                                    if (scheduleEvents.Count >= 19)
                                    {
                                        var evnt19 = scheduleEvents[18];
                                        if (evnt19.EventDate.IsValidDate())
                                        {
                                            var date19 = evnt19.EventDate.ToDateTime();
                                            var scheduleEvents19 = scheduleEvents.Where(e => (e.EventDate.ToDateTime().Date == date19.Date)).ToList();
                                            if (scheduleEvents19 != null)
                                            {
                                                if (evnt19.Discipline == "PT")
                                                {
                                                    evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                }
                                                else if (evnt19.Discipline == "OT")
                                                {
                                                    evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                }
                                                else if (evnt19.Discipline == "ST")
                                                {
                                                    evnt13check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                }
                                            }
                                        }
                                        if (evnt19 != null)
                                        {
                                            episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate.IsValidDate() ? evnt19.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                        }

                                    }
                                    else
                                    {
                                        episode.NineteenVisit = "NA";
                                    }
                                    if (!evnt19check || !evnt13check)
                                    {
                                        episode.ScheduledTherapy = scheduleEvents.Count;
                                        var completedSchedules = scheduleEvents.Where(s => (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                                        if (completedSchedules != null)
                                        {
                                            episode.CompletedTherapy = completedSchedules.Count;
                                        }
                                        episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                        episode.Schedule = string.Empty;
                                        therapyEpisodes.Add(episode);
                                    }
                                }
                            }
                            else if (disciplines.Length > 1)
                            {
                                var discipline = string.Join(";", disciplines);
                                var evnt19check = true;
                                var evnt13check = true;
                                var first = true;
                                if (scheduleEvents.Count >= 13 && discipline.IsNotNullOrEmpty())
                                {
                                    var scheduleEvents13 = scheduleEvents.Take(13).Reverse().ToList();
                                    var scheduleEvents13End = scheduleEvents13.Take(3).ToList();
                                    if (scheduleEvents13End != null)
                                    {
                                        if (discipline.Contains("PT"))
                                        {
                                            first = false;
                                            evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                        }
                                        if (discipline.Contains("OT"))
                                        {
                                            if (first)
                                            {
                                                first = false;
                                                evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                            }
                                            else
                                            {
                                                evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                            }
                                        }
                                        if (discipline.Contains("ST"))
                                        {
                                            if (first)
                                            {
                                                first = false;
                                                evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                            }
                                            else
                                            {
                                                evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                            }
                                        }
                                    }
                                    if (!evnt13check)
                                    {
                                        var evnt13 = scheduleEvents[12];
                                        if (evnt13 != null)
                                        {
                                            episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate.IsValidDate() ? evnt13.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                        }
                                    }
                                    first = true;
                                    if (scheduleEvents.Count >= 19)
                                    {
                                        var evnt19 = scheduleEvents[18];
                                        var scheduleEvents19 = scheduleEvents.Take(19).Reverse().ToList();
                                        var scheduleEvents19End = scheduleEvents19.Take(3).ToList();
                                        if (scheduleEvents19End != null)
                                        {
                                            if (discipline.Contains("PT"))
                                            {
                                                first = false;
                                                evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);

                                            }

                                            if (discipline.Contains("OT"))
                                            {
                                                if (first)
                                                {
                                                    first = false;
                                                    evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                }
                                                else
                                                {
                                                    evnt19check = evnt19check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                }
                                            }
                                            if (discipline.Contains("ST"))
                                            {
                                                if (first)
                                                {
                                                    first = false;
                                                    evnt13check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                }
                                                else
                                                {
                                                    evnt13check = evnt13check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                }
                                            }
                                        }
                                        if (evnt19 != null)
                                        {
                                            episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate.IsValidDate() ? evnt19.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                        }
                                    }
                                    if (!evnt19check || !evnt13check)
                                    {
                                        episode.ScheduledTherapy = scheduleEvents.Count;
                                        var completedSchedules = scheduleEvents.Where(s => (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                                        if (completedSchedules != null)
                                        {
                                            episode.CompletedTherapy = completedSchedules.Count;
                                        }
                                        episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                        episode.Schedule = string.Empty;
                                        therapyEpisodes.Add(episode);
                                    }
                                }
                            }
                        }
                    }
                });
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate, int count)
        {
            var episodes = patientRepository.GetAllEpisodeAfterApril(Current.AgencyId, branchId, patientId, startDate, endDate);
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && (e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date && (e.IsDeprecated == false)) && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).OrderBy(e => e.EventDate.ToDateTime().Date).ToList();
                        if (scheduleEvents != null && scheduleEvents.Count > 0)
                        {
                            var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                            if (disciplines.Length > 0)
                            {
                                var discipline = string.Join(";", disciplines);
                                if (scheduleEvents.Count >= count && discipline.IsNotNullOrEmpty())
                                {
                                    var evntCommon = scheduleEvents[count - 1];
                                    var scheduleEventsCommon = scheduleEvents.Take(count).Reverse().ToList();
                                    var scheduleEventsCommonEnd = scheduleEventsCommon.Take(3).ToList();
                                    if (scheduleEventsCommonEnd != null)
                                    {
                                        if (discipline.Contains("PT"))
                                        {
                                            var ptEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation);
                                            if (ptEval != null && ptEval.EventDate.IsValidDate())
                                            {
                                                episode.PTEval = ptEval.EventDate.ToDateTime().ToString("MM/dd");
                                            }
                                        }
                                        else
                                        {
                                            episode.PTEval = "NA";
                                        }
                                        if (discipline.Contains("OT"))
                                        {
                                            var otEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation);
                                            if (otEval != null && otEval.EventDate.IsValidDate())
                                            {
                                                episode.OTEval = otEval.EventDate.ToDateTime().ToString("MM/dd");
                                            }
                                        }
                                        else
                                        {
                                            episode.OTEval = "NA";
                                        }
                                        if (discipline.Contains("ST"))
                                        {
                                            var stEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation);
                                            if (stEval != null && stEval.EventDate.IsValidDate())
                                            {
                                                episode.STEval = stEval.EventDate.ToDateTime().ToString("MM/dd");
                                            }
                                        }
                                        else
                                        {
                                            episode.STEval = "NA";
                                        }
                                    }
                                    episode.ScheduledTherapy = scheduleEvents.Count;
                                    var completedSchedules = scheduleEvents.Where(s => s.Status.IsInteger() && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                                    if (completedSchedules != null)
                                    {
                                        episode.CompletedTherapy = completedSchedules.Count;
                                    }
                                    episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                    therapyEpisodes.Add(episode);
                                }
                            }
                        }
                    }
                });
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task)
        {
            var taskLogs = new List<TaskLog>();
            var taskAudit = logRepository.GetTaskAudit(Current.AgencyId, patientId, eventId, task);
            if (taskAudit != null && taskAudit.Log.IsNotNullOrEmpty())
            {
                var logs = taskAudit.Log.ToObject<List<TaskLog>>();
                if (logs != null && logs.Count > 0)
                {
                    logs.ForEach(log =>
                    {
                        log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                        taskLogs.Add(log);
                    });
                }
            }
            return taskLogs;
        }

        public List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetGeneralAudits(Current.AgencyId, logDomain.ToString(), logType.ToString(), domainId, entityId.ToString());
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetMedicationAudits(Current.AgencyId, logDomain.ToString(), domainId);

            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public IDictionary<Guid, string> GetPreviousCarePlans(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId
                                   && !s.IsDeprecated && s.IsPlanofCare() && !s.IsMissedVisit
                                   && s.DisciplineTask == scheduledEvent.DisciplineTask
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });

                    previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                    previousNoteEvents.ForEach(s =>
                    {
                        previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                    });
                }
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            return GetPreviousSkilledNurseNotes(patientId, scheduledEvent, 0, -1);
        }

        public IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent, int version, int type)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId
                                   && s.Version == version && (type == -1 || (s.DisciplineTask == type))
                                   && !s.IsDeprecated && s.IsSkilledNurseNote() && !s.IsMissedVisit
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });

                    previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                    previousNoteEvents.ForEach(s =>
                    {
                        previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                    });
                }
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPediatricVisitNotes(Guid patientId, ScheduleEvent scheduledEvent, int version)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.IsPediatricVisit() && !s.IsMissedVisit
                                   && !s.IsDeprecated && s.Version == version
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date);

                            if (scheduleEvents != null)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }
            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPTNotes(Guid patientId, ScheduleEvent scheduledEvent, int version)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && !s.IsDeprecated && s.IsPTNote() //&& s.Version == version
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date);

                            if (scheduleEvents != null)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }
            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPTEvals(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);

                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            List<ScheduleEvent> scheduleEvents = null;
                            if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.PTReassessment)
                            {
                                scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s =>
                                    !s.EventId.IsEmpty()
                                   && s.EventId != scheduledEvent.EventId
                                   && !s.IsMissedVisit
                                   && !s.IsDeprecated
                                   && (s.DisciplineTask == (int)DisciplineTasks.PTReassessment)
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date).ToList();
                            }
                            else
                            {
                                scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s =>
                                    !s.EventId.IsEmpty()
                                   && s.EventId != scheduledEvent.EventId
                                   && !s.IsMissedVisit
                                   && !s.IsDeprecated
                                   && s.IsPTEval()
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date).ToList();
                            }
                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPTDischarges(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && s.IsDeprecated == false && s.IsPTDischarge()
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }
            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousOTNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && !s.IsDeprecated && s.IsOTNote()
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousISOC(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.InitialSummaryOfCare)
                            {
                                var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                    .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && s.IsDeprecated == false && (s.DisciplineTask == (int)DisciplineTasks.InitialSummaryOfCare)
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();
                            }
                        }
                    });
                }
                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }
            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousOTEvals(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            List<ScheduleEvent> scheduleEvents = null;
                            if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.OTReassessment)
                            {
                                scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && s.IsDeprecated == false && (s.DisciplineTask == (int)DisciplineTasks.OTReassessment)
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();
                            }
                            else
                            {
                                scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && s.IsDeprecated == false && s.IsOTEval()
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();
                            }
                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousSTNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && !s.IsDeprecated && s.IsSTNote()
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString()
                                       || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousSTEvals(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                     .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                        && !s.IsDeprecated && s.IsSTEval()
                        && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())
                        && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                        && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                        && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousSTReassessment(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                     .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                        && !s.IsDeprecated && s.IsSTReassessment()
                        && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString() || s.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())
                        && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                        && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                        && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousMSWProgressNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                   && !s.IsDeprecated && s.IsMSWProgressNote()
                                   && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                   && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                   && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                previousNoteEvents.AddRange(scheduleEvents);
                            }
                        }
                    });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousHHANotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            if (scheduledEvent.EventDate.IsNotNullOrEmpty() && scheduledEvent.EventDate.IsValidDate())
            {
                var patientEpisodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduledEvent.StartDate.AddMonths(-4), scheduledEvent.EventDate.ToDateTime().Date);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                        {
                            if (patientEpisode.Schedule.IsNotNullOrEmpty())
                            {
                                var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                    .Where(s => !s.EventId.IsEmpty() && s.EventId != scheduledEvent.EventId && !s.IsMissedVisit
                                       && !s.IsDeprecated && s.DisciplineTask == scheduledEvent.DisciplineTask && s.IsHhaNote()
                                       && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString())
                                       && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                       && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                       && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date).ToList();

                                if (scheduleEvents != null && scheduleEvents.Count > 0)
                                {
                                    previousNoteEvents.AddRange(scheduleEvents);
                                }
                            }
                        });
                }

                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
                previousNoteEvents.ForEach(s =>
                {
                    previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                });
            }

            return previousNotesList;
        }

        public bool UpdateAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
                {
                    var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                    if (existingList.Exists(a => a.Id == allergy.Id))
                    {
                        var exisitingAllergy = existingList.Single(m => m.Id == allergy.Id);
                        if (exisitingAllergy != null)
                        {
                            exisitingAllergy.Name = allergy.Name;
                            exisitingAllergy.Type = allergy.Type;
                            allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                            if (patientRepository.UpdateAllergyProfile(allergyProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyUpdated, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted)
        {
            var result = false;
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
            {
                var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                if (existingList.Exists(a => a.Id == allergyId))
                {
                    var allergy = existingList.Single(m => m.Id == allergyId);
                    if (allergy != null)
                    {
                        allergy.IsDeprecated = isDeleted;
                        allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                        if (patientRepository.UpdateAllergyProfile(allergyProfile))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfileId.ToString(), LogType.AllergyProfile, LogAction.AllergyDeleted, string.Empty);
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool CreateAllergyProfile(Guid patientId)
        {
            var result = false;

            return result;
        }

        public bool AddAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    allergy.Id = Guid.NewGuid();
                    if (allergyProfile.Allergies.IsNullOrEmpty())
                    {
                        var newList = new List<Allergy>() { allergy };
                        allergyProfile.Allergies = newList.ToXml();
                    }
                    else
                    {
                        var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                        existingList.Add(allergy);
                        allergyProfile.Allergies = existingList.ToXml();
                    }
                    if (patientRepository.UpdateAllergyProfile(allergyProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyAdded, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool AddHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var hospitalizationLog = new HospitalizationLog
            {
                Id = Guid.NewGuid(),
                UserId = formCollection.GetGuid("UserId"),
                PatientId = formCollection.GetGuid("PatientId"),
                EpisodeId = formCollection.GetGuid("EpisodeId"),
                HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue,
                LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue,
                Data = ProcessHospitalizationData(formCollection),
                Created = DateTime.Now,
                AgencyId = Current.AgencyId,
                SourceId = (int)TransferSourceTypes.User,
                Modified = DateTime.Now
            };

            if (patientRepository.AddHospitalizationLog(hospitalizationLog))
            {
                var patient = patientRepository.GetPatientOnly(hospitalizationLog.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsHospitalized = true;
                    patient.HospitalizationId = hospitalizationLog.Id;
                    if (patientRepository.Update(patient))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var logId = formCollection.GetGuid("Id");
            var patientId = formCollection.GetGuid("PatientId");
            var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, logId);
            if (hospitalizationLog != null)
            {
                hospitalizationLog.UserId = formCollection.GetGuid("UserId");
                hospitalizationLog.EpisodeId = formCollection.GetGuid("EpisodeId");
                hospitalizationLog.Modified = DateTime.Now;
                hospitalizationLog.HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.Data = ProcessHospitalizationData(formCollection);

                if (patientRepository.UpdateHospitalizationLog(hospitalizationLog))
                {
                    result = true;
                }
            }

            return result;
        }

        public List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId)
        {
            var logs = patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId);
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(l =>
                {
                    l.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/HospitalizationLogPdf', { 'patientId': '" + l.PatientId + "', 'hospitalizationLogId': '" + l.Id + "' });\"><span class=\"img icon print\"></span></a>";
                });
            }
            return logs;
        }

        public HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId)
        {
            var log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (log != null)
            {
                var patient = patientRepository.GetPatientOnly(log.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    log.Patient = patient;
                    log.Location = agencyRepository.FindLocation(Current.AgencyId,patient.AgencyLocationId);
                    if (!log.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeById(Current.AgencyId, log.EpisodeId, log.PatientId);
                        if (episode != null)
                        {
                            log.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                        }
                    }
                }
            }
            return log;
        }

        public List<NonAdmit> GetNonAdmits()
        {
            var list = new List<NonAdmit>();
            var patients = patientRepository.FindPatientOnly((int)PatientStatus.NonAdmission, Current.AgencyId);
            var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.NonAdmission).ToList();
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    InsuranceCache cache = null;
                    if (p.PrimaryInsurance.IsNotNullOrEmpty() && p.PrimaryInsurance.IsInteger())
                    {
                        cache = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
                    }
                    list.Add(new NonAdmit
                    {
                        Id = p.Id,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        DateOfBirth = p.DOBFormatted,
                        Phone = p.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Patient,
                        MiddleInitial = p.MiddleInitial,
                        PatientIdNumber = p.PatientIdNumber,
                        NonAdmissionReason = p.NonAdmissionReason,
                        NonAdmitDate = p.NonAdmissionDateFormatted,
                        Gender = p.Gender,
                        AddressCity = p.AddressCity,
                        AddressLine1 = p.AddressLine1,
                        AddressLine2 = p.AddressLine2,
                        AddressStateCode = p.AddressStateCode,
                        AddressZipCode = p.AddressZipCode,
                        MedicareNumber = p.MedicaidNumber,
                        InsuranceName = cache != null ? cache.Name : string.Empty,
                        InsuranceNumber = p.PrimaryHealthPlanId,
                        Comments = p.Comments
                    });
                });
            }

            if (referrals != null && referrals.Count > 0)
            {
                referrals.ForEach(r =>
                {
                    list.Add(new NonAdmit
                    {
                        Id = r.Id,
                        LastName = r.LastName,
                        FirstName = r.FirstName,
                        DateOfBirth = r.DOBFormatted,
                        Phone = r.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Referral,
                        PatientIdNumber = string.Empty,
                        NonAdmissionReason = r.NonAdmissionReason,
                        NonAdmitDate = r.NonAdmissionDateFormatted,
                        Comments = r.Comments
                    });
                });
            }

            return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        }

        public List<PatientData> GetPatients(Guid agencyId, Guid branchId, int status)
        {
            IList<PatientData> patients = patientRepository.All(agencyId, branchId, status);
            patients.ForEach((PatientData patient) =>
            {
                patient.InsuranceName = GetInsurance(patient.InsuranceId);
                if (patient.InsuranceName.IsNotNullOrEmpty())
                {
                    patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
                }
            });
            return patients.ToList();
        }

        public List<PatientData> GetDeletedPatients(Guid agencyId, Guid branchId)
        {
            IList<PatientData> patients = patientRepository.AllDeleted(agencyId, branchId);
            patients.ForEach((PatientData patient) =>
            {
                patient.InsuranceName = GetInsurance(patient.InsuranceId);
                if (patient.InsuranceName.IsNotNullOrEmpty())
                {
                    patient.InsuranceName = patient.InsuranceName.Replace("(", " (");
                }
            });
            return patients.ToList();
        }

        public List<PendingPatient> GetPendingPatients()
        {
            var patients = patientRepository.GetPendingByAgencyId(Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    if (p.PrimaryInsurance.IsNotNullOrEmpty())
                    {
                        var insurance = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
                        if (insurance != null && insurance.Name.IsNotNullOrEmpty())
                        {
                            p.PrimaryInsuranceName = insurance.Name;
                        }
                    }
                });
            }

            return patients;
        }

        public PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline)
        {
            IDictionary<string, NotesQuestion> garbageNotes = null;
            return GetCarePlanBySelectedEpisode(patientId, episodeId, discipline, out garbageNotes);
        }

        public PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline, out IDictionary<string, NotesQuestion> pocQuestions)
        {
            var pocEvents = patientRepository.GetVisitNotesByDisciplineTask(patientId, Current.AgencyId, discipline);
            foreach (var poc in pocEvents)
            {
                pocQuestions = poc.ToDictionary();
                if (pocQuestions.ContainsKey("SelectedEpisodeId") && pocQuestions["SelectedEpisodeId"] != null && pocQuestions["SelectedEpisodeId"].Answer == episodeId.ToString())
                {
                    return poc;
                }
            }
            pocQuestions = null;
            return null;
        }

        public PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType)
        {
            var viewData = new PatientInsuranceInfoViewData();
            viewData.InsuranceType = insuranceType.ToTitleCase();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    if (insuranceType.IsEqual("Primary"))
                    {
                        if (patient.PrimaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.PrimaryHealthPlanId;
                            viewData.GroupName = patient.PrimaryGroupName;
                            viewData.GroupId = patient.PrimaryGroupId;
                            viewData.Relationship = patient.PrimaryRelationship;
                        }
                        viewData.InsuranceType = "Primary";
                    }
                    else if (insuranceType.IsEqual("Secondary"))
                    {
                        if (patient.SecondaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.SecondaryHealthPlanId;
                            viewData.GroupName = patient.SecondaryGroupName;
                            viewData.GroupId = patient.SecondaryGroupId;
                            viewData.Relationship = patient.SecondaryRelationship;
                        }
                        viewData.InsuranceType = "Secondary";

                    }
                    else if (insuranceType.IsEqual("Tertiary"))
                    {
                        if (patient.TertiaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.TertiaryHealthPlanId;
                            viewData.GroupId = patient.TertiaryGroupId;
                            viewData.GroupName = patient.TertiaryGroupName;
                            viewData.Relationship = patient.TertiaryRelationship;
                        }
                        viewData.InsuranceType = "Tertiary";
                    }
                }
            }
            return viewData;
        }

        public bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                foreach (var a in admissionPeriods)
                {
                    if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    {
                        if (a.Id == admissionId)
                        {
                            continue;
                        }
                        else
                        {
                            if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return result;
        }

        public bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                foreach (var a in admissionPeriods)
                {
                    if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    {
                        if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
                        {
                            result = false;
                            break;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return result;
        }

        public bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var oldAdmissionId = patient.AdmissionId;
                    var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                    if (admission != null)
                    {
                        patient.AdmissionId = admission.Id;
                        admission.PatientData = patient.ToXml();
                        if (patientRepository.Update(patient))
                        {
                            if (patientRepository.UpdatePatientAdmissionDateModal(admission))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmissionPeriodEdited, string.Empty);
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodSetCurrent, string.Empty);

                                return true;
                            }
                            else
                            {
                                patient.AdmissionId = oldAdmissionId;
                                patientRepository.Update(patient);
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public string GetInsurance(string insurance)
        {
            var name = string.Empty;
            if (insurance.IsNotNullOrEmpty() && insurance.IsInteger())
            {
                if (insurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(insurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        name = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, insurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        name = standardInsurance.Name;
                    }
                }
            }
            return name;
        }

        public List<ScheduleEvent> GetScheduledEventsWithUsers(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (!patientId.IsEmpty())
            {
                var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, patientId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    var users = new List<User>(); //agencyRepository.GetUserNames(Current.AgencyId);
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated != true).ToList();
                            if (schedule != null && schedule.Count > 0)
                            {
                                var userIds = new List<Guid>();
                                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, patientEpisode.Id) ?? new List<ReturnComment>();
                                if (returnComments != null && returnComments.Count > 0)
                                {
                                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty() && !users.Exists(us => us.Id == r.UserId)).Select(r => r.UserId).Distinct().ToList();
                                    if (returnUserIds != null && returnUserIds.Count > 0)
                                    {
                                        userIds.AddRange(returnUserIds);
                                    }
                                }
                                var scheduleUserIds = schedule.Where(s => !s.UserId.IsEmpty() && !users.Exists(us => us.Id == s.UserId)).Select(s => s.UserId).Distinct().ToList();
                                if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                                {
                                    userIds.AddRange(scheduleUserIds);
                                }
                                if (userIds != null && userIds.Count > 0)
                                {
                                    userIds = userIds.Distinct().ToList();
                                }
                                if (userIds != null && userIds.Count > 0)
                                {
                                    var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
                                    if (scheduleUsers != null && scheduleUsers.Count > 0)
                                    {
                                        users.AddRange(scheduleUsers);
                                    }
                                }
                                //var users = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();

                                schedule.ForEach(s =>
                                {

                                    // s.ReturnReason = this.GetReturnComments(s.EventId, s.EpisodeId, s.PatientId);
                                    if (s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                    {
                                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                                        s.ReturnReason = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

                                        var userName = string.Empty;
                                        if (!s.UserId.IsEmpty())
                                        {
                                            var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                            if (user != null)
                                            {
                                                userName = user.DisplayName;
                                            }
                                        }
                                        s.EventDate = s.EventDate.ToZeroFilled();
                                        s.VisitDate = s.VisitDate.ToZeroFilled();
                                        s.StartDate = patientEpisode.StartDate.ToDateTime();
                                        s.EndDate = patientEpisode.EndDate.ToDateTime();
                                        s.PatientName = patientEpisode.PatientName;
                                        //var user = users.FirstOrDefault(u => u.UserId == s.UserId);
                                        s.UserName = userName;
                                        patientEvents.Add(s);
                                    }
                                });
                            }
                        }
                    });
                }
            }
            return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public List<ScheduleEvent> GetScheduledEventsByStatus(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int status)
        {
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranchAndPatient(Current.AgencyId, branchId, patientId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                var users = agencyRepository.GetUserNames(Current.AgencyId);

                if (status == (int)ScheduleStatus.NoteMissedVisit || status == (int)ScheduleStatus.NoteMissedVisitComplete || status == (int)ScheduleStatus.NoteMissedVisitPending || status == (int)ScheduleStatus.NoteMissedVisitReturn)
                {
                    var missedVisitEvents = patientEpisodes.ToDictionary(g => g.Id, g => g.Schedule.IsNotNullOrEmpty() ? g.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.IsDeprecated
                            && s.EventDate.IsValidDate()
                            && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                            && (clinicianId.IsEmpty() || clinicianId == s.UserId)
                            && s.IsMissedVisit
                        ).ToList() : new List<ScheduleEvent>()
                       );

                    if (missedVisitEvents != null && missedVisitEvents.Count > 0)
                    {
                        var missedVisitIds = missedVisitEvents.SelectMany(s => s.Value).Select(s => s.EventId).ToList();
                        var missedVisits = patientRepository.GetMissedVisitsByIdsAndStatus(Current.AgencyId, status, missedVisitIds);

                        foreach (var patientEpisode in patientEpisodes)
                        {
                            var scheduleEvents = missedVisitEvents[patientEpisode.Id];
                            foreach (var missedVisit in missedVisits)
                            {
                                var scheduledEvent = scheduleEvents.FirstOrDefault(s => s.EventId == missedVisit.Id);
                                if (scheduledEvent != null)
                                {
                                    scheduledEvent.PatientIdNumber = patientEpisode.PatientIdNumber;
                                    scheduledEvent.PatientName = patientEpisode.PatientName;
                                    scheduledEvent.EventDate = scheduledEvent.EventDate.ToZeroFilled();
                                    scheduledEvent.VisitDate = scheduledEvent.VisitDate.ToZeroFilled();
                                    var user = users.FirstOrDefault(u => u.UserId == scheduledEvent.UserId);
                                    scheduledEvent.UserName = user != null ? user.DisplayName : string.Empty;
                                    patientEvents.Add(scheduledEvent);
                                }
                            }
                        }
                    }
                }
                else
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().ToList();
                            if (schedule != null && schedule.Count > 0)
                            {
                                foreach (var s in schedule)
                                {
                                    if (!s.IsDeprecated
                                        && s.EventDate.IsValidDate()
                                        && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                                        && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                                        && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                                        && s.Status == status.ToString()
                                        && (clinicianId.IsEmpty() || clinicianId == s.UserId))
                                    {
                                        s.PatientIdNumber = patientEpisode.PatientIdNumber;
                                        s.PatientName = patientEpisode.PatientName;
                                        s.EventDate = s.EventDate.ToZeroFilled();
                                        s.VisitDate = s.VisitDate.ToZeroFilled();
                                        var user = users.FirstOrDefault(u => u.UserId == s.UserId);
                                        s.UserName = user != null ? user.DisplayName : string.Empty;
                                        patientEvents.Add(s);
                                    }
                                }
                            }

                        }
                    });
                }
            }
            return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public List<ScheduleEvent> GetScheduledEventsByType(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int type)
        {
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranchAndPatient(Current.AgencyId, branchId, patientId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                var users = agencyRepository.GetUserNames(Current.AgencyId);
                patientEpisodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated != true).ToList();
                        if (schedule != null && schedule.Count > 0)
                        {
                            schedule.ForEach(s =>
                            {
                                if (s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                {
                                    if (s.DisciplineTask == type && (clinicianId.IsEmpty() || clinicianId == s.UserId))
                                    {
                                        s.PatientIdNumber = patientEpisode.PatientIdNumber;
                                        s.EventDate = s.EventDate.ToZeroFilled();
                                        s.VisitDate = s.VisitDate.ToZeroFilled();
                                        s.StartDate = patientEpisode.StartDate.ToDateTime();
                                        s.EndDate = patientEpisode.EndDate.ToDateTime();
                                        s.PatientName = patientEpisode.PatientName;
                                        var user = users.FirstOrDefault(u => u.UserId == s.UserId);
                                        s.UserName = user != null ? user.DisplayName : string.Empty;
                                        patientEvents.Add(s);
                                    }
                                }
                            });
                        }
                    }
                });
            }
            return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }
        public string GetReturnComments(Guid eventId, Guid episodeId, Guid patientId) {
            string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in NewComments)
            {
                if (comment.IsDeprecated) continue;
                if (CommentString.IsNotNullOrEmpty()) CommentString += "<hr/>";
                if (comment.UserId == Current.UserId) CommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                CommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", UserEngine.GetName(comment.UserId, Current.AgencyId), comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return CommentString;
        }

        public bool AddReturnComments(Guid eventId, Guid episodeId, string comment)
        {
            return patientRepository.AddReturnComment(new ReturnComment
            {
                AgencyId = Current.AgencyId,
                Comments = comment,
                Created = DateTime.Now,
                EpisodeId = episodeId,
                EventId = eventId,
                Modified = DateTime.Now,
                UserId = Current.UserId
            });
        }

        public bool EditReturnComments(int id, string comment)
        {
            ReturnComment ExistingComment = patientRepository.GetReturnComment(Current.AgencyId, id);
            ExistingComment.Modified = DateTime.Now;
            ExistingComment.Comments = comment;
            return patientRepository.UpdateReturnComment(ExistingComment);
        }

        public bool DeleteReturnComments(int id)
        {
            return patientRepository.DeleteReturnComments(Current.AgencyId, id);
        }

        #endregion

        #region Private Methods

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        public void SetInsurance(Patient patient)
        {
            if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
            {
                if (patient.PrimaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger())
            {
                if (patient.SecondaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger())
            {
                if (patient.TertiaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
        }

        private void ProcessSchedule(ScheduleEvent scheduleEvent, Patient patient, PatientEpisode episode)
        {
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask).ToString())
            {
                case "OASISCDeath":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case "OASISCDeathOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case "OASISCDeathPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case "OASISCDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Version = 2;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case "OASISCDischargeOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case "OASISCDischargePT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case "NonOASISDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.NonOasisDischarge, episode);
                    break;

                case "OASISCFollowUp":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case "OASISCFollowupPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case "OASISCFollowupOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case "OASISCRecertification":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentRecertification = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertification);
                    break;

                case "OASISCRecertificationPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertificationPT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentRecertificationPT = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationPT);
                    break;

                case "OASISCRecertificationOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertificationOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentRecertificationOT = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationOT);
                    break;

                case "SNAssessmentRecert":
                case "NonOASISRecertification":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedNonOasisRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentNonOasisRecertification = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.NonOasisRecertification, episode, currentMedNonOasisRecertification);
                    break;

                case "OASISCResumptionofCare":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case "OASISCResumptionofCarePT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.PT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case "OASISCResumptionofCareOT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.OT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case "OASISCStartofCare":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case "OASISCStartofCarePT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.PT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case "OASISCStartofCareOT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.OT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case "SNAssessment":
                case "NonOASISStartofCare":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedNonOasisStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.NonOasisStartOfCare, episode, currentMedNonOasisStartofCare);
                    }
                    break;

                case "OASISCTransferDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                    break;
                case "OASISCTransferDischargePT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                    break;
                case "OASISCTransfer":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case "OASISCTransferPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;

                case "OASISCTransferOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(patient, scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;

                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinHS":
                case "SNInsulinNoon":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDiabeticDailyVisit":
                case "Labs":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNote);
                    break;
                case "SNPediatricVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var SNPediatricVisitNote = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(SNPediatricVisitNote);
                    break;
                case "SNPediatricAssessment":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var SNPediatricAssessment = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(SNPediatricAssessment);
                    break;
                case "SNVPsychNurse":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 3;
                    var snPsychNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable, Version = scheduleEvent.Version };
                    patientRepository.AddVisitNote(snPsychNote);
                    break;
                case "SNPsychAssessment":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var snPsychAssessment = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable, Version = scheduleEvent.Version };
                    patientRepository.AddVisitNote(snPsychAssessment);
                    break;
                case "LVNSupervisoryVisit":
                case "HHAideSupervisoryVisit":
                case "InitialSummaryOfCare":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var snNoteNonebillableNursing = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableNursing);
                    break;
                case "PTEvaluation":
                case "PTReEvaluation":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var ptEval = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(ptEval);
                    break;
                case "PTReassessment":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var ptReEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(ptReEval);
                    break;
                case "PTVisit":
                case "PTAVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var ptVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(ptVisit);
                    break;
                case "PTSupervisoryVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 1;
                    var ptSupVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(ptSupVisit);
                    break;
                case "OTSupervisoryVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 1;
                    var otSupVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(otSupVisit);
                    break;
                case "PTDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillablePT = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillablePT);
                    break;
                case "PTMaintenance":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var snNoteNonebillablePTM = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillablePTM);
                    break;
                case "OTEvaluation":
                case "OTReEvaluation":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var otEval = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(otEval);
                    break;
                case "OTReassessment":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var otReassessment = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(otReassessment);
                    break;
                case "OTVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var snNoteNonebillableOTVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableOTVisit);
                    break;
                case "OTDischarge":
                case "COTAVisit":
                case "OTMaintenance":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableOT = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableOT);
                    break;
                case "STEvaluation":
                case "STReEvaluation":
                case "STDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var stEval = new PatientVisitNote { AgencyId = Current.AgencyId, Version = scheduleEvent.Version, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(stEval);
                    break;
                case "STReassessment":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    var stReassessment = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(stReassessment);
                    break;
                case "STVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    var stNote = new PatientVisitNote { AgencyId = Current.AgencyId, Version=scheduleEvent.Version, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(stNote);
                    break;
                
                case "STMaintenance":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableST = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableST);
                    break;
                case "MSWVisit":
                case "MSWDischarge":
                case "MSWAssessment":
                case "MSWProgressNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableMSW = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableMSW);
                    break;
                case "MSWEvaluationAssessment":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableMSW2 = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableMSW2);
                    break;
                case "DriverOrTransportationNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var driverOrTransportationNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(driverOrTransportationNote);
                    break;
                case "DieticianVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Dietician.ToString();
                    scheduleEvent.IsBillable = false;
                    var snNoteNonebillableDietician = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableDietician);
                    break;
                case "HHAideVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var hhAideVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(hhAideVisit);
                    break;
                case "HHAideCarePlan":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var hhAideCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(hhAideCarePlan);
                    break;
                case "PASTravel":
                case "PASVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var pasVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(pasVisit);
                    break;
                case "PASCarePlan":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    var pasCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(pasCarePlan);
                    break;
                case "HomeMakerNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var homeMakerNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(homeMakerNote);
                    break;
                case "PTDischargeSummary":
                case "OTDischargeSummary":
                case "DischargeSummary":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                        //scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = false;
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        var dischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable, PhysicianId = physician != null ? physician.Id : Guid.Empty };
                        if (physician != null)
                        {
                            var questions = new List<NotesQuestion>();
                            questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(physician.Id), Type = "DischargeSummary" });
                            scheduleEvent.Questions = questions;
                            dischargeSummary.Note = questions.ToXml();
                            patientRepository.AddVisitNote(dischargeSummary);
                        }
                        else
                        {
                            patientRepository.AddVisitNote(dischargeSummary);
                        }
                    }
                    break;
                //case "PTDischargeSummary":
                //    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                //    scheduleEvent.Discipline = Disciplines.PT.ToString();
                //    scheduleEvent.IsBillable = false;
                //    var PTphysician = physicianRepository.GetPatientPhysicians(patient.Id, Current.AgencyId).SingleOrDefault(p => p.Primary);
                //    var PTdischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                //    if (PTphysician != null)
                //    {
                //        var questions = new List<NotesQuestion>();
                //        questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(PTphysician.Id), Type = "DischargeSummary" });
                //        scheduleEvent.Questions = questions;
                //        PTdischargeSummary.Note = questions.ToXml();
                //        patientRepository.AddVisitNote(PTdischargeSummary);
                //    }
                //    else
                //    {
                //        patientRepository.AddVisitNote(PTdischargeSummary);
                //    }
                //    break;

                case "PhysicianOrder":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Orders.ToString();
                        scheduleEvent.IsBillable = false;
                        var order = new PhysicianOrder { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, OrderDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Text = "", Summary = "", Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                        //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        //{
                        //    order.PhysicianId = patient.PhysicianContacts[0].Id;
                        //}
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            order.PhysicianId = physician.Id;
                        }
                        patientRepository.AddOrder(order);
                    }
                    break;

                case "HCFA485StandAlone":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                        scheduleEvent.Discipline = Disciplines.Orders.ToString();
                        scheduleEvent.IsBillable = false;
                        var planofCare = new PlanofCareStandAlone { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                        planofCare.Questions = new List<Question>();
                        planofCare.Data = planofCare.Questions.ToXml();
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            planofCare.PhysicianId = physician.Id;
                        }
                        planofCareRepository.AddStandAlone(planofCare);
                    }
                    break;

                case "NonOasisHCFA485":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;

                case "HCFA485":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;

                case "HCFA486":
                case "PostHospitalizationOrder":
                case "MedicaidPOC":
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                    break;
                case "IncidentAccidentReport":
                    scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCreated).ToString();
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var incidentReport = new Incident { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, IncidentDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    agencyRepository.AddIncident(incidentReport);
                    break;
                case "InfectionReport":
                    scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCreated).ToString();
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var infectionReport = new Infection { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, InfectionDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    agencyRepository.AddInfection(infectionReport);
                    break;
                //case "Rap":
                //    {
                //        var pocAssessment = assessmentService.GetEpisodeAssessment(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventDate.ToDateTime());

                //        var newRap = new Rap
                //        {
                //            Id = episode.Id,
                //            AgencyId = patient.AgencyId,
                //            PatientId = patient.Id,
                //            EpisodeId = episode.Id,
                //            EpisodeStartDate = episode.StartDate,
                //            EpisodeEndDate = episode.EndDate,
                //            IsFirstBillableVisit = false,
                //            IsOasisComplete = false,
                //            PatientIdNumber = patient.PatientIdNumber,
                //            IsGenerated = false,
                //            MedicareNumber = patient.MedicareNumber,
                //            FirstName = patient.FirstName,
                //            LastName = patient.LastName,
                //            DOB = patient.DOB,
                //            Gender = patient.Gender,
                //            AddressLine1 = patient.AddressLine1,
                //            AddressLine2 = patient.AddressLine2,
                //            AddressCity = patient.AddressCity,
                //            AddressStateCode = patient.AddressStateCode,
                //            AddressZipCode = patient.AddressZipCode,
                //            StartofCareDate = episode.StartOfCareDate,
                //            AdmissionSource = patient.AdmissionSource,
                //            PatientStatus = patient.Status,
                //            UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                //            AreOrdersComplete = false,
                //            Status = (int)BillingStatus.ClaimCreated,
                //            Created = DateTime.Now,
                //            ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>"
                //        };
                //        if (patient.PhysicianContacts != null)
                //        {
                //            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                //            if (contact != null)
                //            {
                //                newRap.PhysicianNPI = contact.NPI;
                //                newRap.PhysicianFirstName = contact.FirstName;
                //                newRap.PhysicianLastName = contact.LastName;
                //            }
                //        }
                //        if (pocAssessment != null)
                //        {
                //            var status = pocAssessment.Status;
                //            if (status == 9 && status == 10)
                //            {
                //                var assessment = assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                //                if (assessment != null)
                //                {
                //                    var assessmentQuestions = assessment.ToDictionary();
                //                    newRap.IsOasisComplete = true;
                //                    newRap.IsFirstBillableVisit = true;
                //                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                //                    {
                //                        newRap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                //                        newRap.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                //                    }
                //                    string diagnosis = "<DiagonasisCodes>";
                //                    if (assessmentQuestions["M1020ICD9M"] != null)
                //                    {
                //                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code1></code1>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M1"] != null)
                //                    {
                //                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code2></code2>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M2"] != null)
                //                    {
                //                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code3></code3>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M3"] != null)
                //                    {
                //                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code4></code4>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M4"] != null)
                //                    {
                //                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code5></code5>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M5"] != null)
                //                    {
                //                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code6></code6>";
                //                    }
                //                    diagnosis += "</DiagonasisCodes>";
                //                    newRap.DiagnosisCode = diagnosis;
                //                    newRap.HippsCode = pocAssessment.HippsCode;
                //                    newRap.ClaimKey = pocAssessment.ClaimKey;
                //                }
                //                else
                //                {
                //                    newRap.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                //                }
                //            }
                //        }
                //        else
                //        {
                //            newRap.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                //        }
                //        newRap.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

                //        billingRepository.AddRap(newRap);
                //    }
                //    break;

                //case "Final":
                //    {
                //        var pocAssessment = assessmentService.GetEpisodeAssessment(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventDate.ToDateTime());
                //        var newFinal = new Final
                //        {
                //            AgencyId = patient.AgencyId,
                //            PatientId = patient.Id,
                //            EpisodeId = episode.Id,
                //            EpisodeStartDate = episode.StartDate,
                //            EpisodeEndDate = episode.EndDate,
                //            IsFirstBillableVisit = false,
                //            IsOasisComplete = false,
                //            PatientIdNumber = patient.PatientIdNumber,
                //            IsGenerated = false,
                //            MedicareNumber = patient.MedicareNumber,
                //            FirstName = patient.FirstName,
                //            LastName = patient.LastName,
                //            DOB = patient.DOB,
                //            Gender = patient.Gender,
                //            AddressLine1 = patient.AddressLine1,
                //            AddressLine2 = patient.AddressLine2,
                //            AddressCity = patient.AddressCity,
                //            AddressStateCode = patient.AddressStateCode,
                //            AddressZipCode = patient.AddressZipCode,
                //            StartofCareDate = episode.StartOfCareDate,
                //            AdmissionSource = patient.AdmissionSource,
                //            PatientStatus = patient.Status,
                //            UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                //            AreOrdersComplete = false,
                //            Status = (int)BillingStatus.ClaimCreated,
                //            Created = DateTime.Now,
                //            ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>"
                //        };
                //        if (patient.PhysicianContacts != null)
                //        {
                //            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                //            if (contact != null)
                //            {
                //                newFinal.PhysicianNPI = contact.NPI;
                //                newFinal.PhysicianFirstName = contact.FirstName;
                //                newFinal.PhysicianLastName = contact.LastName;
                //            }
                //        }
                //        if (pocAssessment != null)
                //        {
                //            var status = pocAssessment.Status;
                //            if (status == 9 && status == 10)
                //            {
                //                var assessment = assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                //                if (assessment != null)
                //                {
                //                    var assessmentQuestions = assessment.ToDictionary();
                //                    newFinal.IsOasisComplete = true;
                //                    newFinal.IsFirstBillableVisit = true;
                //                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                //                    {
                //                        newFinal.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                //                        newFinal.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                //                    }
                //                    string diagnosis = "<DiagonasisCodes>";
                //                    if (assessmentQuestions["M1020ICD9M"] != null)
                //                    {
                //                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code1></code1>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M1"] != null)
                //                    {
                //                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code2></code2>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M2"] != null)
                //                    {
                //                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code3></code3>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M3"] != null)
                //                    {
                //                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code4></code4>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M4"] != null)
                //                    {
                //                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code5></code5>";
                //                    }
                //                    if (assessmentQuestions["M1022ICD9M5"] != null)
                //                    {
                //                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                //                    }
                //                    else
                //                    {
                //                        diagnosis += "<code6></code6>";
                //                    }
                //                    diagnosis += "</DiagonasisCodes>";
                //                    newFinal.DiagnosisCode = diagnosis;
                //                    newFinal.HippsCode = pocAssessment.HippsCode;
                //                    newFinal.ClaimKey = pocAssessment.ClaimKey;
                //                }
                //                else
                //                {
                //                    newFinal.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                //                }
                //            }
                //        }
                //        else
                //        {
                //            newFinal.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                //        }
                //        newFinal.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

                //        billingRepository.AddFinal(newFinal);
                //    }
                //    break;

                case "SixtyDaySummary":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var sixtyDaySummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, OrderNumber = patientRepository.GetNextOrderNumber(), PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(sixtyDaySummary);
                    break;

                case "TransferSummary":
                case "CoordinationOfCare":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var transferSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(transferSummary);
                    break;


                case "CommunicationNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var comNote = new CommunicationNote { Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, AgencyId = Current.AgencyId, UserId = scheduleEvent.UserId, Status = ((int)ScheduleStatus.NoteNotYetDue), Created = scheduleEvent.EventDate.ToDateTime(), Modified = DateTime.Now };
                    patientRepository.AddCommunicationNote(comNote);
                    break;
                case "FaceToFaceEncounter":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Orders.ToString();
                        scheduleEvent.IsBillable = false;
                        var faceToFaceEncounter = new FaceToFaceEncounter { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, RequestDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Modified = DateTime.Now, Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                        //if (patient.PhysicianContacts.Count > 0)
                        //{
                        //    faceToFaceEncounter.PhysicianId = patient.PhysicianContacts[0].Id;
                        //}
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
                        if (physician != null)
                        {
                            faceToFaceEncounter.PhysicianId = physician.Id;
                        }
                        patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                    }
                    break;
                case "UAPWoundCareVisit":
                case "UAPInsulinPrepAdminVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    var uapNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(uapNote);
                    break;
            }
        }

        private bool UpdateScheduleEntity(Guid eventId, Guid episodeId, Guid patientId, string taskName, bool isDeprecated)
        {
            bool result = false;
            switch (taskName)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "OASISCTransferDischargePT":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    result = assessmentService.MarkAsDeleted(eventId, episodeId, patientId, taskName, isDeprecated);
                    break;
                case "SkilledNurseVisit":
                case "Labs":
                case "InitialSummaryOfCare":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinHS":
                case "SNInsulinNoon":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "LVNSupervisoryVisit":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTReassessment":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STReassessment":
                case "STDischarge":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "PTAVisit":
                case "PTReEvaluation":
                case "PTReassessment":
                case "COTAVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "HHAideCarePlan":
                case "PASVisit":
                case "PASTravel":
                case "PASCarePlan":
                case "DischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                case "DriverOrTransportationNote":
                case "SNDiabeticDailyVisit":
                case "UAPWoundCareVisit":
                case "UAPInsulinPrepAdminVisit":
                case "HomeMakerNote":
                case "PTDischargeSummary":
                case "OTDischargeSummary":
                case "SNPediatricVisit":
                case "SNPediatricAssessment":
                case "PTSupervisoryVisit":
                case "OTSupervisoryVisit":
                case "SNPsychAssessment":
                    result = patientRepository.MarkVisitNoteAsDeleted(Current.AgencyId, episodeId, patientId, eventId, isDeprecated);
                    break;
                case "PhysicianOrder":
                    result = patientRepository.MarkOrderAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.MarkPlanOfCareAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case "HCFA485StandAlone":
                    result = assessmentService.MarkPlanOfCareStandAloneAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case "IncidentAccidentReport":
                    result = agencyRepository.MarkIncidentAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case "InfectionReport":
                    result = agencyRepository.MarkInfectionsAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case "CommunicationNote":
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, eventId, patientId, isDeprecated);
                    break;
                case "FaceToFaceEncounter":
                    result = patientRepository.DeleteFaceToFaceEncounter(Current.AgencyId, patientId, eventId, isDeprecated);
                    break;
            }
            return result;
        }

        private bool ReassignScheduleEntity(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, string taskName)
        {
            bool result = false;
            switch (taskName)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "OASISCTransferDischargePT":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    result = assessmentService.ReassignUser(episodeId, patientId, eventId, employeeId, taskName);
                    break;
                case "SkilledNurseVisit":
                case "Labs":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinHS":
                case "SNInsulinNoon":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "InitialSummaryOfCare":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNPediatricVisit":
                case "SNPediatricAssessment":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":

                case "LVNSupervisoryVisit":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTReassessment":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STReassessment":
                case "STDischarge":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "PTAVisit":
                case "PTReEvaluation":
                case "PTReassessment":
                case "COTAVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "HHAideCarePlan":
                case "DischargeSummary":
                case "PTDischargeSummary":
                case "OTDischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                case "DriverOrTransportationNote":
                case "SNDiabeticDailyVisit":
                case "PASVisit":
                case "PASTravel":
                case "PASCarePlan":
                case "UAPWoundCareVisit":
                case "UAPInsulinPrepAdminVisit":
                case "HomeMakerNote":
                case "PTSupervisoryVisit":
                case "OTSupervisoryVisit":
                case "SNPsychAssessment":
                    result = patientRepository.ReassignNotesUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case "PhysicianOrder":
                    result = patientRepository.ReassignOrdersUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.ReassignPlanOfCaresUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case "IncidentAccidentReport":
                    result = agencyRepository.ReassignIncidentUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "InfectionReport":
                    result = agencyRepository.ReassignInfectionsUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "CommunicationNote":
                    result = patientRepository.ReassignCommunicationNoteUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "FaceToFaceEncounter":
                    result = patientRepository.ReassignFaceToFaceEncounterUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
            }
            return result;
        }

        private bool ProcessEditDetail(ScheduleEvent schedule, Guid oldEpisodeId)
        {
            bool result = false;
            var type = ((DisciplineTasks)schedule.DisciplineTask).ToString();
            switch (type)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "OASISCTransferDischargePT":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification
                        || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                    {
                        if (schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()
                            || schedule.Status == ((int)ScheduleStatus.OasisExported).ToString()
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                        }
                        else
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                        }
                    }
                    result = assessmentService.UpdateAssessmentForDetail(schedule);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinHS":
                case "SNInsulinNoon":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDiabeticDailyVisit":
                case "SNPediatricVisit":
                case "SNPediatricAssessment":
                case "Labs":
                case "InitialSummaryOfCare":
                case "SNPsychAssessment":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = schedule.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = schedule.Surcharge, Name = "Surcharge", Type = type }); }
                                if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = schedule.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = schedule.AssociatedMileage, Name = "AssociatedMileage", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.NoteType = type;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "MSWEvaluationAssessment":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTReassessment":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STDischarge":
                case "STReassessment":
                case "PTAVisit":
                case "PTReEvaluation":
                case "PTReassessment":
                case "COTAVisit":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = schedule.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = schedule.Surcharge, Name = "Surcharge", Type = type }); }
                                if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = schedule.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = schedule.AssociatedMileage, Name = "AssociatedMileage", Type = type }); }
                                notesQuestions.Values.ForEach(q => { q.Type = type; });
                                visitNote.NoteType = type;
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "OTSupervisoryVisit":
                case "PTSupervisoryVisit":
                case "LVNSupervisoryVisit":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "HomeMakerNote":
                case "DriverOrTransportationNote":
                case "PASVisit":
                case "PASTravel":
                case "PASCarePlan":
                case "UAPWoundCareVisit":
                case "UAPInsulinPrepAdminVisit":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                if (notesQuestions.ContainsKey("Surcharge")) { notesQuestions["Surcharge"].Answer = schedule.Surcharge; } else { notesQuestions.Add("Surcharge", new NotesQuestion { Answer = schedule.Surcharge, Name = "Surcharge", Type = type }); }
                                if (notesQuestions.ContainsKey("AssociatedMileage")) { notesQuestions["AssociatedMileage"].Answer = schedule.AssociatedMileage; } else { notesQuestions.Add("AssociatedMileage", new NotesQuestion { Answer = schedule.AssociatedMileage, Name = "AssociatedMileage", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case "HHAideCarePlan":
                case "DischargeSummary":
                case "PTDischargeSummary":
                case "OTDischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "PhysicianOrder":
                    {
                        var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            physicianOrder.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : physicianOrder.Status;
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                physicianOrder.OrderDate = schedule.VisitDate.ToDateTime();
                            }
                            if (physicianOrder.Status.ToString() != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    physicianOrder.PhysicianData = physician.ToXml();
                                }
                            }
                            physicianOrder.PhysicianId = schedule.PhysicianId;
                            physicianOrder.IsDeprecated = schedule.IsDeprecated;
                            result = patientRepository.UpdateOrderModel(physicianOrder);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.UpdatePlanOfCareForDetail(schedule, oldEpisodeId);
                    break;
                case "HCFA485StandAlone":
                    result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                    break;
                case "FaceToFaceEncounter":
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        faceToFace.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : faceToFace.Status;
                        if (faceToFace.Status.ToString() != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFace.PhysicianData = physician.ToXml();
                            }
                        }
                        faceToFace.PhysicianId = schedule.PhysicianId;
                        faceToFace.IsDeprecated = schedule.IsDeprecated;
                        faceToFace.RequestDate = schedule.VisitDate.ToDateTime();
                        if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                        {
                            result = true;
                        }
                    }
                    break;
                case "IncidentAccidentReport":
                    {
                        var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                        if (accidentReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                accidentReport.IncidentDate = schedule.VisitDate.ToDateTime();
                            }
                            accidentReport.IsDeprecated = schedule.IsDeprecated;
                            accidentReport.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : accidentReport.Status;
                            result = agencyRepository.UpdateIncidentModal(accidentReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "InfectionReport":
                    {
                        var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                        if (infectionReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                infectionReport.InfectionDate = schedule.VisitDate.ToDateTime();
                            }
                            infectionReport.IsDeprecated = schedule.IsDeprecated;
                            infectionReport.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : infectionReport.Status;
                            result = agencyRepository.UpdateInfectionModal(infectionReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "CommunicationNote":
                    {
                        var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (comNote != null)
                        {
                            comNote.IsDeprecated = schedule.IsDeprecated;
                            comNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : comNote.Status;
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                comNote.Created = schedule.VisitDate.ToDateTime();
                            }
                            if (comNote.Status.ToString() != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    comNote.PhysicianData = physician.ToXml();
                                }
                            }
                            result = patientRepository.UpdateCommunicationNoteModal(comNote);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
            }
            return result;
        }

        private bool ProcessEditDetailForReassign(ScheduleEvent schedule, Guid oldEpisodeId)
        {
            bool result = false;
            var type = ((DisciplineTasks)schedule.DisciplineTask).ToString();
            switch (type)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "OASISCTransferDischargePT":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                    {
                        if (schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()
                            || schedule.Status == ((int)ScheduleStatus.OasisExported).ToString()
                            || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                        }
                        else
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                        }
                    }
                    result = assessmentService.UpdateAssessmentForDetail(schedule);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinHS":
                case "SNInsulinNoon":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDiabeticDailyVisit":
                case "SNPediatricVisit":
                case "SNPsychAssessment":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.NoteType = type;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case "MSWEvaluationAssessment":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "PTReassessment":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTDischarge":
                case "OTReassessment":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STReassessment":
                case "STDischarge":
                case "PTAVisit":
                case "PTReEvaluation":
                case "COTAVisit":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                notesQuestions.Values.ForEach(q => { q.Type = type; });
                                visitNote.NoteType = type;
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "LVNSupervisoryVisit":
                case "PTSupervisoryVisit":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "DriverOrTransportationNote":
                case "PASVisit":
                case "PASTravel":
                case "PASCarePlan":
                case "UAPWoundCareVisit":
                case "UAPInsulinPrepAdminVisit":
                case "HomeMakerNote":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case "HHAideCarePlan":
                case "DischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "PTDischargeSummary":
                case "OTDischargeSummary":
                case "SNDischargeSummary":
                case "CoordinationOfCare":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                visitNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : visitNote.Status;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "PhysicianOrder":
                    {
                        var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                physicianOrder.OrderDate = schedule.VisitDate.ToDateTime();
                            }
                            physicianOrder.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : physicianOrder.Status;
                            if (physicianOrder.Status.ToString() != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    physicianOrder.PhysicianData = physician.ToXml();
                                }
                            }
                            physicianOrder.PhysicianId = schedule.PhysicianId;
                            physicianOrder.IsDeprecated = schedule.IsDeprecated;
                            physicianOrder.EpisodeId = schedule.EpisodeId;
                            result = patientRepository.UpdateOrderModel(physicianOrder);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.UpdatePlanOfCareForDetail(schedule, oldEpisodeId);
                    break;
                case "HCFA485StandAlone":
                    result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                    break;
                case "FaceToFaceEncounter":
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        faceToFace.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : faceToFace.Status;
                        if (faceToFace.Status.ToString() != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFace.PhysicianData = physician.ToXml();
                            }
                        }
                        faceToFace.PhysicianId = schedule.PhysicianId;
                        faceToFace.IsDeprecated = schedule.IsDeprecated;
                        faceToFace.RequestDate = schedule.VisitDate.ToDateTime();
                        faceToFace.EpisodeId = schedule.EpisodeId;

                        if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                        {
                            result = true;
                        }
                    }
                    break;
                case "IncidentAccidentReport":
                    {
                        var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                        if (accidentReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                accidentReport.IncidentDate = schedule.VisitDate.ToDateTime();
                            }
                            accidentReport.IsDeprecated = schedule.IsDeprecated;
                            accidentReport.EpisodeId = schedule.EpisodeId;
                            accidentReport.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : accidentReport.Status;
                            result = agencyRepository.UpdateIncidentModal(accidentReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "InfectionReport":
                    {
                        var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                        if (infectionReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                infectionReport.InfectionDate = schedule.VisitDate.ToDateTime();
                            }
                            infectionReport.IsDeprecated = schedule.IsDeprecated;
                            infectionReport.EpisodeId = schedule.EpisodeId;
                            infectionReport.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : infectionReport.Status;
                            result = agencyRepository.UpdateInfectionModal(infectionReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "CommunicationNote":
                    {
                        var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (comNote != null)
                        {
                            comNote.IsDeprecated = schedule.IsDeprecated;
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                comNote.Created = schedule.VisitDate.ToDateTime();
                            }
                            comNote.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : comNote.Status;
                            if (comNote.Status.ToString() != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    comNote.PhysicianData = physician.ToXml();
                                }
                            }
                            comNote.EpisodeId = schedule.EpisodeId;
                            result = patientRepository.UpdateCommunicationNoteModal(comNote);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
            }
            return result;
        }

        private List<NotesQuestion> ProcessNoteQuestions(FormCollection formCollection)
        {
            string type = formCollection["Type"];
            formCollection.Remove("Type");
            var questions = new Dictionary<string, NotesQuestion>();
            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if ( nameArray!=null && nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(","), Type = type });
                    }
                }
            }
            return questions.Values.ToList();
        }

        public static MemoryStream ResizeAndEncodePhoto(Image original, string contentType, bool resizeIfWider)
        {
            int imageWidth = 140;
            int imageHeight = 140;
            MemoryStream memoryStream = null;

            if (original != null)
            {
                int destX = 0;
                int destY = 0;
                int sourceX = 0;
                int sourceY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                int sourceWidth = original.Width;
                int sourceHeight = original.Height;

                nPercentW = ((float)imageWidth / (float)sourceWidth);
                nPercentH = ((float)imageHeight / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                var newImage = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                newImage.SetResolution(original.HorizontalResolution, original.VerticalResolution);

                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.Clear(Color.White);
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(original,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);
                }
                memoryStream = new MemoryStream();

                ImageCodecInfo imageCodec = null;
                ImageCodecInfo[] imageCodecs = ImageCodecInfo.GetImageEncoders();

                foreach (ImageCodecInfo imageCodeInfo in imageCodecs)
                {
                    if (imageCodeInfo.MimeType == contentType)
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }

                    if (imageCodeInfo.MimeType == "image/jpeg" && contentType == "image/pjpeg")
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }
                }
                if (imageCodec != null)
                {
                    EncoderParameters encoderParameters = new EncoderParameters();
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                    newImage.Save(memoryStream, imageCodec, encoderParameters);
                    newImage.Dispose();
                    original.Dispose();
                }
            }

            memoryStream.Position = 0;
            return memoryStream;
        }

        public Guid GetPrimaryPhysicianId(Guid patientId, Guid agencyId)
        {
            var physicianId = Guid.Empty;
            var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
            if (physician != null)
            {
                physicianId = physician.Id;
            }
            //Patient patient = patientRepository.Get(patientId, agencyId);
            //Guid physicianId = new Guid();
            //if (patient.PhysicianContacts.Count > 1)
            //{
            //    foreach (AgencyPhysician p in patient.PhysicianContacts)
            //    {
            //        if (p.Primary)
            //            physicianId = p.Id;
            //    }
            //}
            //else if (patient.PhysicianContacts.Count == 1)
            //{
            //    physicianId = patient.PhysicianContacts[0].Id;
            //}
            return physicianId;
        }

        private string ProcessHospitalizationData(FormCollection formCollection)
        {
            formCollection.Remove("Id");
            formCollection.Remove("PatientId");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("UserId");
            formCollection.Remove("Templates");
            formCollection.Remove("M0903LastHomeVisitDate");
            formCollection.Remove("M0906DischargeDate");

            var questions = new List<Question>();
            foreach (var key in formCollection.AllKeys)
            {
                questions.Add(Question.Create(key, formCollection.GetValues(key).Join(",")));
            }
            return questions.ToXml();
        }


        #endregion
    }
}
