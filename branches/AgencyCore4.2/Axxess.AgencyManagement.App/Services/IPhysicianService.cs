﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;

    public interface IPhysicianService
    {
        bool CreatePhysician(AgencyPhysician physician);
        bool UpdatePhysician(AgencyPhysician physician);
        bool AddLicense(PhysicainLicense license);
        bool UpdateLicense(Guid id, Guid physicianId, DateTime ExpirationDate);
        bool DeleteLicense(Guid id, Guid physicianId);
    }
}
