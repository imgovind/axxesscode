﻿namespace Axxess.Api.Grouper
{
    using System;
    using System.Diagnostics;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    static class Program
    {
        static void Main()
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
		        { 
			        new GrouperWindowsService()
		        };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
