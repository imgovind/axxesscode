﻿namespace Axxess.Physician.App.iTextExtension.XmlParsing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Physician.App.ViewData;
    class VisitNoteXml : BaseXml {
        private IDictionary<String, NotesQuestion> Data = new Dictionary<String, NotesQuestion>();
        public VisitNoteXml(VisitNoteViewData data, PdfDoc type) : base(type) {
            this.Data = data.Questions;
            this.Init();
            if (data.Questions != null) this.FilterEmptySections();
        }
        private void FilterEmptySections() {
            this.NotaFilter();
        }
        public override String GetData(String Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}