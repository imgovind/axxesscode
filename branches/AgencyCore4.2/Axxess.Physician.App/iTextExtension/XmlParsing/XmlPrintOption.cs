﻿namespace Axxess.Physician.App.iTextExtension.XmlParsing {
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    class XmlPrintOption : XmlElement {
        public int Cols;
        public String Data;
        public String Display;
        public String Label;
        public String Type;
        public String Value;
        public int[] ColWidths;
        public List<XmlPrintQuestion> Subquestion;
        public XmlPrintOption(BaseXml xml, XElement option) : base(xml, option) {
            this.Cols = this.GetIntAttribute("cols");
            this.Data = this.GetData();
            this.Display = this.GetAttribute("display");
            this.Label = this.GetAttribute("label");
            this.Type = this.GetAttribute("type");
            this.Value = this.GetAttribute("value");
            this.ColWidths = this.GetColWidths(this.Cols);
            this.Subquestion = this.GetQuestions();
        }
        public String GetJson(String Data) {
            StringBuilder Json = new StringBuilder();
            if (this.Subquestion.Count() > 0) {
                Json.Append("<table><tbody>");
                if (this.Display == "inline") {
                    Json.Append("<tr>");
                    Json.Append("<td><span class='checkbox'>" + (this.Checked(Data) ? "X" : "&#160;") + "</span>" + this.CleanForJson(this.Label) + "</td>");
                    foreach (XmlPrintQuestion question in this.Subquestion) Json.Append("<td>" + question.GetJson() + "</td>");
                    Json.Append("</tr>");
                } else if (this.Cols > 1) {
                    Json.Append("<tr>");
                    Json.Append("<td><span class='checkbox'>" + (this.Checked(Data) ? "X" : "&#160;") + "</span>" + this.CleanForJson(this.Label) + "</td>");
                    for (int i = 0; i < this.Subquestion.Count(); i++) {
                        if (i != 0 && i % this.Cols == 0) Json.Append("</tr><tr>");
                        Json.Append("<td>" + this.Subquestion[i].GetJson() + "</td>");
                    }
                    Json.Append("</tr>");
                } else {
                    Json.Append("<tr><td><span class='checkbox'>" + (this.Checked(Data) ? "X" : "&#160;") + "</span>" + this.CleanForJson(this.Label) + "</td></tr>");
                    foreach (XmlPrintQuestion question in this.Subquestion) Json.Append("<tr><td>" + question.GetJson() + "</td></tr>");
                }
                Json.Append("</tbody></table>");
            }
            else Json.Append("<span class='checkbox'>" + (this.Checked(Data) ? "X" : "&#160;") + "</span>" + this.CleanForJson(this.Label));
            return Json.ToString();
        }
        private bool Checked(String Data) {
            return this.Data.Equals(this.Value) || Data.Equals(this.Value) || this.Data.Split(',').Contains(this.Value) || Data.Split(',').Contains(this.Value) || this.Data.Split(';').Contains(this.Value) || Data.Split(';').Contains(this.Value);
        }
    }
}