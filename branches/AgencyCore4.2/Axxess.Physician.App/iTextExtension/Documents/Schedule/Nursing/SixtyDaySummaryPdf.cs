﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Physician.App.iTextExtension;
    using Axxess.Physician.App.ViewData;
    class SixtyDaySummaryPdf : VisitNotePdf
    {
        public SixtyDaySummaryPdf(VisitNoteViewData data) : base(data, PdfDocs.SixtyDaySummary) { }
        protected override float[] Margins(VisitNoteViewData data)
        {
            return new float[] { 120, 28.3F, 125, 28.3F };
        }
        protected override List<Dictionary<string, string>> FieldMap(VisitNoteViewData data)
        {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : "");
            fieldmap[0].Add("dob", data != null && data.Patient != null && data.Patient.DOBFormatted.IsNotNullOrEmpty() ? data.Patient.DOBFormatted : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            //string notificationDate="";
            //if (data != null &&data.Questions!=null&& data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer == "1")
            //{
            //    if (data.Questions.ContainsKey("NotificationDate") && data.Questions["NotificationDate"].Answer.IsNotNullOrEmpty())
            //    {
            //        if (data.Questions["NotificationDate"].Answer == "1")
            //            notificationDate = "5 days";
            //        else if (data.Questions["NotificationDate"].Answer == "2")
            //            notificationDate = "2 days";
            //        else if (data.Questions["NotificationDate"].Answer == "3")
            //        {
            //            if (data.Questions.ContainsKey("NotificationDateOther") && data.Questions["NotificationDateOther"].Answer.IsNotNullOrEmpty())
            //                notificationDate = data.Questions["NotificationDateOther"].Answer;
            //        }
            //    }
            //}
            fieldmap[0].Add("dnr", data != null && data.Questions != null ? (data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer == "1" ? "Yes" : "No") : "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? string.Format(" {0} – {1}", data.StartDate.ToShortDateString(), data.EndDate.ToShortDateString()) : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            return fieldmap;
        }
    }
}