﻿namespace Axxess.DataLoader
{
    using System;
    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    public class DiagnosisCode_Copy
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        
        [SubSonicIgnore]
        public string FormatCode
        {
            get
            {
                return Code.IsNotNullOrEmpty() ? (Code.Trim().StartsWith("E") && Code.Trim().Length >= 0 ? string.Format("{0}.{1}", this.Code.Substring(0, 4).PadLeft(4, '0'), this.Code.Substring((4))) : (Code.Trim().Length > 0 ? string.Format("{0}.{1}", this.Code.Substring(0, 3).PadLeft(3, '0'), this.Code.Substring((3))) : string.Empty)) : string.Empty;
            }
        }
    }
}

