﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    public class CbsaEntry
    {
        public CbsaEntry() { this.Counties = new List<County>(); }
        public string CBSA { get; set; }
        public string State { get; set; }
        public double Variance { get; set; }
        public double WageIndex { get; set; }
        public List<County> Counties { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder()
            .AppendFormat("CBSA: {0} ", this.CBSA).AppendLine()
            .AppendFormat("State: {0} ", this.State).AppendLine()
            .AppendFormat("Wage Index: {0} ", this.WageIndex).AppendLine()
            .Append("Counties: ");
            this.Counties.ForEach(c =>
            {
                stringBuilder.AppendFormat("{0}, ", c.ToString());
            });
            return stringBuilder.AppendLine().AppendLine().ToString().Trim();
        }
    }

    public class County
    {
        public string Code { get; set; }
        public string Title { get; set; }

        public override string ToString()
        {
            return new StringBuilder().AppendFormat("Title: {0}", this.Title).AppendLine().ToString();
        }
    }
}
