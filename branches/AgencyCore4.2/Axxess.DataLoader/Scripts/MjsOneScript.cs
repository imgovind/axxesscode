﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class MjsOneScript
    {
        private static string input = Path.Combine(App.Root, "Files\\bchs.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\bchs_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                var i = 1;
                                csvReader.ReadHeaderRecord();
                                foreach (var dataRow in csvReader.DataRecords)
                                {
                                    var patientData = new Patient();
                                    patientData.Id = Guid.NewGuid();
                                    patientData.AgencyId = agencyId;
                                    patientData.AgencyLocationId = locationId;
                                    patientData.Status = 1;
                                    patientData.Ethnicities = string.Empty;
                                    patientData.MaritalStatus = string.Empty;
                                    patientData.IsDeprecated = false;
                                    patientData.IsHospitalized = false;

                                    patientData.LastName = dataRow.GetValue(0).ToTitleCase();
                                    patientData.FirstName = dataRow.GetValue(1).ToTitleCase();
                                    patientData.MiddleInitial = dataRow.GetValue(2).ToUpperCase();
                                    patientData.PatientIdNumber = dataRow.GetValue(3);
                                    patientData.AddressLine1 = dataRow.GetValue(4).ToTitleCase();
                                    patientData.AddressLine2 = "";
                                    patientData.AddressCity = dataRow.GetValue(5).ToTitleCase();
                                    patientData.AddressStateCode = dataRow.GetValue(6).ToUpperCase();
                                    patientData.AddressZipCode = dataRow.GetValue(7);
                                    patientData.PhoneHome = dataRow.GetValue(8).ToPhoneDB();
                                    patientData.PhoneMobile = dataRow.GetValue(9).ToPhoneDB();
                                    patientData.DOB = dataRow.GetValue(10).IsValidDate() ? dataRow.GetValue(10).ToDateTime() : DateTime.MinValue;
                                    patientData.Gender = dataRow.GetValue(11).IsEqual("F") ? "Female" : "Male";

                                    if (dataRow.GetValue(16).ToLower().Contains("medicare"))
                                    {
                                        patientData.PrimaryInsurance = "1";
                                        patientData.MedicareNumber = dataRow.GetValue(12);
                                    }
                                    else
                                    {
                                        patientData.PrimaryHealthPlanId = dataRow.GetValue(12);
                                    }

                                    if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Primary Insurance ID: {0}. ", dataRow.GetValue(12));
                                    }

                                    patientData.StartofCareDate = dataRow.GetValue(13).IsValidDate() ? dataRow.GetValue(13).ToDateTime() : DateTime.MinValue;
                                    patientData.DischargeDate = dataRow.GetValue(14).IsValidDate() ? dataRow.GetValue(14).ToDateTime() : DateTime.MinValue;

                                    if (dataRow.GetValue(15).IsEqual("discharged"))
                                    {
                                        patientData.Status = 2;
                                    }

                                    if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Primary Insurance: {0}. ", dataRow.GetValue(16));
                                    }

                                    if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Secondary Insurance: {0}. ", dataRow.GetValue(17));
                                    }

                                    if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("DX: {0}. ", dataRow.GetValue(18));
                                    }

                                    if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("DX Description: {0}. ", dataRow.GetValue(19));
                                    }

                                    if (dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(21).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician Name: {0} {1}. ", dataRow.GetValue(20), dataRow.GetValue(21));
                                    }

                                    if (dataRow.GetValue(22).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("County: {0}. ", dataRow.GetValue(22));
                                    }

                                    if (dataRow.GetValue(23).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Medications: {0}. ", dataRow.GetValue(23));
                                    }

                                    if (dataRow.GetValue(24).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("F2F Date: {0}. ", dataRow.GetValue(24));
                                    }

                                    if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("F2F Status: {0}. ", dataRow.GetValue(25));
                                    }
                                    patientData.Comments = patientData.Comments.IsNotNullOrEmpty() ? patientData.Comments.Replace("'", "").Trim() : string.Empty;

                                    patientData.Created = DateTime.Now;
                                    patientData.Modified = DateTime.Now;

                                    var medicationProfile = new MedicationProfile()
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Medication = "<ArrayOfMedication />"
                                    };

                                    var allergyProfile = new AllergyProfile()
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Allergies = "<ArrayOfAllergy />"
                                    };

                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                    {
                                        var admissionPeriod = new PatientAdmissionDate()
                                        {
                                            Id = Guid.NewGuid(),
                                            AgencyId = agencyId,
                                            Created = DateTime.Now,
                                            DischargedDate = DateTime.MinValue,
                                            IsActive = true,
                                            IsDeprecated = false,
                                            Modified = DateTime.Now,
                                            PatientData = patientData.ToXml().Replace("'", ""),
                                            PatientId = patientData.Id,
                                            Reason = string.Empty,
                                            StartOfCareDate = patientData.StartofCareDate,
                                            Status = patientData.Status
                                        };

                                        if (Database.Add(admissionPeriod))
                                        {
                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                            if (patient != null)
                                            {
                                                patient.AdmissionId = admissionPeriod.Id;
                                                if (Database.Update(patient))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                    Console.WriteLine();
                                                }
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
