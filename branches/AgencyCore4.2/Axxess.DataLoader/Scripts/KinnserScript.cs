﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class KinnserScript
    {
        private static string input = Path.Combine(App.Root, "Files\\patient_roaster.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\patient_roaster{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PrimaryRelationship = "";
                                            patientData.SecondaryRelationship = "";
                                            patientData.TertiaryRelationship = "";
                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            patientData.LastName = dataRow.GetValue(1).ToTitleCase();
                                            patientData.FirstName = dataRow.GetValue(2).ToTitleCase();
                                            patientData.Gender = dataRow.GetValue(3);
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                patientData.MedicareNumber = dataRow.GetValue(4);
                                            }
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.MedicaidNumber = dataRow.GetValue(5);
                                            }
                                            patientData.DOB = dataRow.GetValue(6).IsValidDate() ? dataRow.GetValue(6).ToDateTime() : DateTime.MinValue;
                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                patientData.PhoneHome = dataRow.GetValue(7).ToPhoneDB();
                                            }
                                            patientData.AddressLine1 = dataRow.GetValue(8).ToTitleCase();
                                            patientData.AddressCity = dataRow.GetValue(9).ToTitleCase();
                                            patientData.AddressStateCode = dataRow.GetValue(10);
                                            if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                            {
                                                patientData.AddressZipCode = dataRow.GetValue(11);
                                            }
                                            else
                                            {
                                                patientData.AddressZipCode = "00000";
                                            }
                                            patientData.EmailAddress = dataRow.GetValue(12);
                                            patientData.StartofCareDate = dataRow.GetValue(13).IsValidDate() ? dataRow.GetValue(13).ToDateTime() : DateTime.MinValue;
                                            //patientData.PrimaryInsurance = "1";
                                            //var episodeEndDate = dataRow.GetValue(14).ToDateTime();
                                            //if ( episodeEndDate.Date < DateTime.Now.AddDays(-59).Date)
                                            //{
                                            //    patientData.Status = 2;
                                            //}
                                           
                                          
                                           
                                            if (dataRow.GetValue(14).IsNotNullOrEmpty() && dataRow.GetValue(14).IsValidDate() && dataRow.GetValue(15).IsNotNullOrEmpty() && dataRow.GetValue(15).IsValidDate())
                                            {
                                                patientData.Comments += string.Format("Episode Range: {0} - {1} .", dataRow.GetValue(14).ToDateTime().ToString("MM/dd/yyyy"), dataRow.GetValue(15).ToDateTime().ToString("MM/dd/yyyy"));
                                            }
                                            if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(16));
                                            }
                                            if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Secondary Diagnosis: {0}. ", dataRow.GetValue(17));
                                            }
                                            if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Disciplines: {0}. ", dataRow.GetValue(18));
                                            }
                                            if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Frequencies: {0}. ", dataRow.GetValue(19));
                                            }
                                            if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                            {
                                                patientData.Triage = Convert.ToInt32(dataRow.GetValue(20).Trim());
                                            }

                                            //if (dataRow.GetValue(19).Trim().IsNotNullOrEmpty())
                                            //{
                                            //    switch (dataRow.GetValue(19).Trim())
                                            //    {
                                            //        case "I":
                                            //            patientData.Triage = 1;
                                            //            break;
                                            //        case "II":
                                            //            patientData.Triage = 2;
                                            //            break;
                                            //        case "III":
                                            //            patientData.Triage = 3;
                                            //            break;
                                            //        default:
                                            //            patientData.Triage = Convert.ToInt32(dataRow.GetValue(19).Trim());
                                            //            break;
                                            //    }
                                            //    patientData.Comments += string.Format("Triage Level: {0}. ", dataRow.GetValue(19));
                                            //}
                                            if (dataRow.GetValue(21).IsNotNullOrEmpty() && dataRow.GetValue(22).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician & NPI: {0} {1}. ", dataRow.GetValue(21), dataRow.GetValue(22));
                                            }
                                            if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Clinician: {0}. ", dataRow.GetValue(25));
                                            }
                                            if (dataRow.GetValue(27).IsNotNullOrEmpty())
                                            {
                                                if (dataRow.GetValue(27).Trim().ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "4";
                                                }
                                                patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(27));
                                            }
                                           
                                            if (dataRow.GetValue(31).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Therapist: {0}. ", dataRow.GetValue(31));
                                            }
                                            if (dataRow.GetValue(32).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("External Referral: {0}. ", dataRow.GetValue(32));
                                            }

                                            if (dataRow.GetValue(33).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Case Manager: {0}. ", dataRow.GetValue(33));
                                            }

                                            if (dataRow.GetValue(34).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Patient Flags: {0}. ", dataRow.GetValue(34));
                                            }


                                            patientData.Comments = patientData.Comments.Replace("'", "");

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile()
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile()
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };

                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate()
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };

                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);

                                                            var exists = true;
                                                            var npi = dataRow.GetValue(22);
                                                            if (npi.IsNotNullOrEmpty())
                                                            {
                                                                var physician = Database.GetPhysician(npi, agencyId);
                                                                if (physician == null)
                                                                {
                                                                    exists = false;
                                                                    var info = Database.GetNpiData(npi);
                                                                    if (info != null)
                                                                    {
                                                                        physician = new AgencyPhysician
                                                                        {
                                                                            Id = Guid.NewGuid(),
                                                                            AgencyId = agencyId,
                                                                            NPI = npi,
                                                                            LoginId = Guid.Empty,
                                                                            PhoneWork = dataRow.GetValue(23).ToPhoneDB(),
                                                                            FaxNumber = dataRow.GetValue(24).ToPhoneDB(),
                                                                            AddressCity = info.ProviderBusinessPracticeLocationAddressCityName,
                                                                            AddressLine1 = info.ProviderFirstLineBusinessPracticeLocationAddress,
                                                                            AddressLine2 = info.ProviderSecondLineBusinessPracticeLocationAddress,
                                                                            AddressStateCode = info.ProviderBusinessPracticeLocationAddressStateName,
                                                                            AddressZipCode = info.ProviderBusinessPracticeLocationAddressPostalCode,
                                                                            Credentials = info.ProviderCredentialText,
                                                                            Created = DateTime.Now,
                                                                            Modified = DateTime.Now,
                                                                            IsDeprecated = false,
                                                                            PhysicianAccess = false,
                                                                            Primary = true,
                                                                            LastName = info.ProviderLastName,
                                                                            FirstName = info.ProviderFirstName
                                                                        };
                                                                        var physicianNameArray = dataRow.GetValue(21).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                        if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                                        {
                                                                            physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Trim().Replace(",", "") : physician.LastName;
                                                                            physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim().Replace(",", "") : physician.FirstName;
                                                                        }
                                                                    }

                                                                    Database.Add(physician);
                                                                }

                                                                if (physician != null)
                                                                {
                                                                    var patientPhysician = new PatientPhysician
                                                                    {
                                                                        IsPrimary = true,
                                                                        PatientId = patientData.Id,
                                                                        PhysicianId = physician.Id
                                                                    };

                                                                    if (Database.Add(patientPhysician))
                                                                    {
                                                                        Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                                    }
                                                                }
                                                            }
                                                            Console.WriteLine();
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
