﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AluraPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\PhysiciansList.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PhysiciansList_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty() && dataRow.GetValue(1).IsNotNullOrEmpty())
                                        {
                                            var physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;
                                            physicianData.IsDeprecated = false;
                                            //physicianData.UPIN = dataRow.GetValue(0);
                                            physicianData.NPI = dataRow.GetValue(1);
                                            
                                            var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                physicianData.LastName = nameArray[0].Trim();
                                                physicianData.FirstName = nameArray[1].Trim();
                                            }
                                            if (dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(2).Length > 10)
                                            {
                                                physicianData.PhoneWork = dataRow.GetValue(2).ToPhoneDB();
                                            }
                                            if (dataRow.GetValue(3).IsNotNullOrEmpty() && dataRow.GetValue(3).Length > 10)
                                            {
                                                physicianData.FaxNumber = dataRow.GetValue(2).ToPhoneDB();
                                            }
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                physicianData.PhoneAlternate = dataRow.GetValue(4).ToPhoneDB();
                                            }
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                physicianData.Comments += string.Format("{0}.", dataRow.GetValue(5));
                                            }
                                            //physicianData.Credentials = dataRow.GetValue(6);
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                            {
                                                var addressArray = dataRow.GetValue(6).Trim().Split(' ');

                                                physicianData.AddressCity = addressArray[addressArray.Length - 3];
                                                physicianData.AddressStateCode = addressArray[addressArray.Length-2];
                                                physicianData.AddressZipCode = addressArray[addressArray.Length-1];
                                                int b = dataRow.GetValue(6).Trim().IndexOf(physicianData.AddressCity);
                                                physicianData.AddressLine1 = dataRow.GetValue(6).Trim().Substring(0, b - 1);
                                            }
                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                physicianData.Comments += string.Format("Other:{0}.", dataRow.GetValue(7));
                                            }
                                            if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            {
                                                physicianData.EmailAddress = dataRow.GetValue(8).Trim();
                                            }

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
