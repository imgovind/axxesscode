﻿using System;

namespace OpenForum.Core.Models
{
    [Serializable]
    public class ForumUser
    {
        public ForumUser(Guid userId, string username, string userUrl, string imageUrl)
        {
            Id = userId;
            Username = username;
            UserUrl = userUrl;
            ImageUrl = imageUrl;
        }

        public Guid Id { get; private set; }
        public string Username { get; private set; }
        public string UserUrl { get; private set;}
        public string ImageUrl { get; private set; }

        public bool HasUserUrl
        {
            get { return !string.IsNullOrEmpty(UserUrl); }
        }

        public bool HasImageUrl
        {
            get { return !string.IsNullOrEmpty(ImageUrl); }
        }
    }
}
