﻿<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css" media="screen">
		p {
			margin: 0 0 10px 0;
			font-family: arial, sans-serif;
			font-size: 12px;
		}

		body, div, td, th, textarea, input, h2, h3 {
			font-family: arial, sans-serif;
			font-size: 12px;
		}
	</style>
	<body style="font-family: arial, sans-serif; font-size: 12px;">
		<p>Hello <%=displayname%>,</p>
		<p>A new account has been created for <%=agencyname%>.</p>
		<p>Please call the agency at this telephone number <%=agencyphone%> to schedule a training within the next 2 days.</p>
		<p>If you cannot train this agency, please coordinate with your sales person <%=salesperson%> to get another trainer as soon as possible.</p>
		<p>Remember also to inquire about exporting their patient information from their previous software into Axxess.</p>
		<p>Thank you,<br />
		The Axxess&trade; team</p>
		<p>This is an automated e-mail, please do not reply.</p>
		<p>This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>