﻿var UserInterface = {
    ShowEditLogin: function(Id) {
        acore.open("editlogin", 'Login/Edit', function() { Login.InitEdit(); }, { loginId: Id });
    },
    ShowNewNote: function(agencyId) {
        acore.open("newnote", 'Agency/NewNote', function() { Note.InitNew(); }, { agencyId: agencyId });
    },
    ShowEditNote: function(agencyId, noteId) {
        acore.open("editnote", 'Agency/EditNote', function() { Note.InitEdit(); }, { agencyId: agencyId, noteId: noteId });
    },
    ShowNewLocation: function(agencyId) {
        acore.open("newlocation", 'Location/New', function() { Location.InitNew(); }, { agencyId: agencyId });
    },
    ShowEditLocation: function(agencyId, locationId) {
        acore.open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { agencyId: agencyId, locationId: locationId });
    },
    PreviewDashboardMessage: function(text) {
        U.ShowDialog("#newDashboardMessagePreview", function() {
            $("#custom-message-widget").html(text);
        });
    },
    ResendActivationLink: function(userId, agencyId) {
        if (confirm("Are you sure you want to resend the activation link for this user?")) {
            U.PostUrl("/Account/ResendLink", "userId=" + userId + "&agencyId=" + agencyId, function(result) {
                if (result.isSuccessful) U.Growl("Activation Link sent successfully", "success");
                else U.Growl(result.errorMessage, "error");
            });
        }
    },
    CloseWindow: function(window) {
        acore.close(window);
    }
}
