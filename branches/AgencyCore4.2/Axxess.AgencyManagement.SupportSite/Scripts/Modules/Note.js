﻿var Note = {
    InitEdit: function() {
        U.InitEditTemplate("Note");
    },
    InitNew: function() {
        U.InitNewTemplate("Note");
    },
    View: function(agencyId, noteId) {
        acore.open("viewnote", 'Agency/ViewNote', function() { }, { agencyId: agencyId, noteId: noteId });
    },
    RebindList: function() { U.RebindTGrid($('#List_CustomerNotes')); },
    Delete: function(agencyId, noteId) {
        if (confirm("Are you sure you want to delete this note?")) {
            U.PostUrl("Agency/DeleteNote", { agencyId: agencyId, noteId: noteId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Note.RebindList();
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }
}