﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
acore.init();
<% if (Current.IsAxxessAdmin) { %>
acore.addmenu("Create", "create", "mainmenu", 22);
acore.addwindow("newlogin", "New Login", "Login/New", function() { Login.InitNew(); }, "create");
acore.addwindow("newagency", "Agency", "Agency/New", function() { Agency.InitNew(); }, "create");
acore.addmenu("Message", "createnew", "create");
acore.addwindow("newsystemmessage", "System Message", "System/NewMessage", function() { SystemMessage.InitNew(); }, "createnew");
acore.addwindow("newdashboardmessage", "Dashboard Message", "System/NewDashboardMessage", function() { DashboardMessage.InitNew(); }, "createnew");
acore.addmenu("View", "view", "mainmenu", 43);
acore.addwindow("listlogins", "Users", "Login/List", function() { }, "view");
acore.addwindow("listagencies", "Agencies", "Agency/List", function() { }, "view");
acore.addwindow("editlogin", "Edit Login", null, null, false);
acore.addwindow("editagency", "Edit Agency", null, null, false);
acore.addwindow("listagencylocations", "Agency Locations", null, null, false);
acore.addwindow("newlocation", "New Location", null, null, false);
acore.addwindow("editlocation", "Edit Location", null, null, false);
acore.addmenu("Axxess", "axxess", "mainmenu", 0);
acore.addwindow("agreement", "Sales Agreements", "Agency/Agreement", function() {}, "axxess");
acore.addwindow("humanresource", "HR Management", "Agency/HumanResource", function() {}, "axxess");
<% } else { %>
acore.addmenu("Agency", "agency", "mainmenu", 0);
acore.addwindow("listagencies", "View Agencies", "Agency/List", function() { }, "agency");
acore.addmenu("Axxess", "axxess", "mainmenu", 0);
acore.addwindow("agreement", "Sales Agreements", "Agency/Agreement", function() {}, "axxess");
acore.addwindow("humanresource", "HR Management", "Agency/HumanResource", function() {}, "axxess");
<% } %>

acore.addwindow("listagencyusers", "Agency Users", null, null, false);
acore.addwindow("listagencyphysicians", "Agency Physicians", null, null, false);
acore.addwindow("editagencyuser", "Edit User Login Information", null, null, false);

acore.addwindow("newnote", "New Support Note", null, null, false);
acore.addwindow("editnote", "Edit Support Note", null, null, false);
acore.addwindow("viewnote", "View Support Note", null, null, false);
acore.addwindow("listagencynotes", "Agency Support Notes", null, null, false);

$('ul#mainmenu').Menu();
acore.open("listagencies");

</script>

