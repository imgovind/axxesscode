﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    public interface IAssessmentRepository
    {
        bool Add(Assessment oasisAssessment);
        bool Update(Assessment oasisAssessment);
        bool UpdateModal(Assessment assessment);
        bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated);
        bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid employeeId, string assessmentType);
        Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId);
        Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId);
        Assessment GetAssessmentOnly(Guid agencyId, Guid assessmentId, string assessmentType);
        Assessment GetAssessmentOnly(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType);
        List<Assessment> GetAllByStatus(Guid agencyId, int status);
        List<Assessment> GetOnlyCMSOasisByStatus(Guid agencyId, int status);
        List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate);
        List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources);
        List<Assessment> GetPreviousAssessments(Guid agencyId, Guid patientId, AssessmentType assessmentType);
        bool UsePreviousAssessment(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType);
    }
}
