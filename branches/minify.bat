@ECHO OFF

GOTO %1

:all
ECHO Compressing All JavaScript Files...

:acore
ECHO Compressing AgencyCore Files...
FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Modules\min\%%f
FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Visits\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Visits\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Visits\min\%%f
FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.AgencyManagement.WebSite\Scripts\Plugins\Custom\min\%%f
IF "%1"=="acore" GOTO end

:support
ECHO Compressing Axxess Support Files...
FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.AgencyManagement.SupportSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.AgencyManagement.SupportSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.AgencyManagement.SupportSite\Scripts\Modules\min\%%f
REM FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.AgencyManagement.SupportSite\Scripts\Plugins\Custom\min\%%f
IF "%1"=="support" GOTO end

:md
ECHO Compressing Axxess MD Files...
FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.Physician.WebSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.Physician.WebSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.Physician.WebSite\Scripts\Modules\min\%%f
FOR /f %%f IN ('dir /b .\AgencyCore4.1\Axxess.Physician.WebSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCore4.1\Axxess.Physician.WebSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore4.1\Axxess.Physician.WebSite\Scripts\Plugins\Custom\min\%%f

:end
del ~compile.tmp
ECHO Complete
pause