﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Security;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    [Compress]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountController : BaseController
    {
        #region Constructor

        public AccountController()
            : this(null, null)
        {
        }

        public AccountController(IFormsAuthenticationService formsAuthentication, ISupportMembershipService membershipService)
        {
            this.MembershipService = membershipService;
            this.AuthenticationService = formsAuthentication;
        }

        #endregion

        #region Properties

        public ISupportMembershipService MembershipService
        {
            get;
            private set;
        }

        public IFormsAuthenticationService AuthenticationService
        {
            get;
            private set;
        }

        #endregion

        #region AccountController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Activate()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["loginid"] != null)
            {
                var loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
                var login = loginRepository.Find(HttpContext.Request.QueryString["loginid"].ToGuid());
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.Name = login.DisplayName;
                    account.EmailAddress = login.EmailAddress;
                }
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Activate([Bind] Account account)
        {
            var viewData = Validate<AccountViewData>(
                            new Validation(() => string.IsNullOrEmpty(account.Password), "You must specify a password. <br/>"),
                            new Validation(() => account.Password.Length < 8, "The minimum password length is 8 characters.")
                            );

            if (viewData.isSuccessful)
            {
                if (this.MembershipService.Activate(account))
                {
                    this.AuthenticationService.SignIn(account.UserName, false);
                    viewData.redirectUrl = "/";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Account Activation failed. Please try again.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOn()
        {
            var logon = new Logon();

            if ((HttpContext.Request.Cookies.Count > 0) && (HttpContext.Request.Cookies[SupportAppSettings.RememberMeCookie] != null))
            {
                var cookie = HttpContext.Request.Cookies[SupportAppSettings.RememberMeCookie];
                logon.UserName = Crypto.Decrypt(cookie.Value);
            }

            return View(logon);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public JsonResult LogOn([Bind] Logon logon)
        {
            var viewData = Validate<AccountViewData>(
                             new Validation(() => string.IsNullOrEmpty(logon.UserName.Trim()), "You must specify a username. <br/>"),
                             new Validation(() => string.IsNullOrEmpty(logon.Password.Trim()), "You must specify a password.  <br/>"),
                             new Validation(() => !logon.UserName.Trim().IsEmail(), "E-mail Address is not valid.<br/>")
                        );

            if (viewData.isSuccessful)
            {
                LoginAttemptType loginAttempt = LoginAttemptType.Failed;
                if (LoginMonitor.Instance.IsUserLocked(logon.UserName, Current.IpAddress))
                {
                    loginAttempt = LoginAttemptType.TooManyAttempts;
                }
                else
                {
                    loginAttempt = MembershipService.Validate(logon.UserName.Trim(), logon.Password.Trim());
                }

                switch (loginAttempt)
                {
                    case LoginAttemptType.Success:
                        this.AuthenticationService.SignIn(logon.UserName.Trim(), logon.RememberMe);
                        viewData.redirectUrl = "/";
                        break;
                    case LoginAttemptType.TooManyAttempts:
                        viewData.isLocked = true;
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "You exceeded the maximum number of login attempts. Please note that your online access to Support will be disabled for 5 minutes.";
                        break;
                    default:
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Username / Password combination failed.";
                        break;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOff()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(Current.User.Name);
                this.AuthenticationService.SignOut();
            }

            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ResendLink(Guid userId, Guid agencyId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Activation Link could not be resent." };

            if (MembershipService.ResendActivationLink(userId, agencyId))
            {
                viewData.isSuccessful = true;
            }

            return Json(viewData);
        }

        #endregion
    }

}
