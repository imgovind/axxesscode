﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Add", "Agency", FormMethod.Post, new { @id = "newAgencyForm" })) { %>
<div class="form-wrapper">
    <fieldset>
        <legend>Agreement Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_IsAgreementSignedYes">Agreement Signed?</label><div class="float-right"><input type="radio" class="radio required" id="New_Agency_IsAgreementSignedYes" name="IsAgreementSigned" value="true" /> Yes <input type="radio" class="radio required" id="New_Agency_IsAgreementSignedNo" value="false" name="IsAgreementSigned" /> No&nbsp;&nbsp;</div></div>
            <div class="row"><label for="New_Agency_TrialPeriod">Trial Period:</label>
                <div class="float-right">
                    <% var trialPeriod = new SelectList(new[] {
                       new SelectListItem { Text = "-- Select Trial Period --", Value = "" },
                       new SelectListItem { Text = "30 Days", Value = "30" },
                       new SelectListItem { Text = "60 Days", Value = "60" },
                       new SelectListItem { Text = "90 Days", Value = "90" }
                       }, "Value", "Text"); %>
                    <%= Html.DropDownList("TrialPeriod", trialPeriod, new { @id = "New_Agency_TrialPeriod", @class = "input_wrapper" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_Package">Package:</label>
                <div class="float-right">
                    <% var package = new SelectList(new[] {
                       new SelectListItem { Text = "Bronze (1 to 5 users)", Value = "5" },
                       new SelectListItem { Text = "Silver (6 to 10 users)", Value = "10" },
                       new SelectListItem { Text = "Gold (11 to 20 users)", Value = "20" },
                       new SelectListItem { Text = "Platinum (21 to 40 users)", Value = "40" },
                       new SelectListItem { Text = "Titanium (41 to unlimited users)", Value = "0" }
                       }, "Value", "Text"); %>
                    <%= Html.DropDownList("Package", package, new { @id = "New_Agency_Package", @class = "input_wrapper", @style="width: 235px;" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Admin Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_AdminUsername">E-mail:</label><div class="float-right"><%=Html.TextBox("AgencyAdminUsername", "", new { @id = "New_Agency_AdminUsername", @class = "text required", @maxlength = "50" })%></div></div>
            <div class="row"><label for="New_Agency_AdminFirstName">First Name:</label><div class="float-right"><%=Html.TextBox("AgencyAdminFirstName", "", new { @id = "New_Agency_AdminFirstName", @class = "text required input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_Agency_AdminLastName">Last Name:</label><div class="float-right"><%=Html.TextBox("AgencyAdminLastName", "", new { @id = "New_Agency_AdminLastName", @class = "text required input_wrapper", @maxlength = "20" })%></div></div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Audit Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_SalesPerson">Sales Person:</label>
                <div class="float-right"><%= Html.Users("SalesPerson", "", new { @id = "New_Agency_SalesPerson", @class = "requireddropdown" })%></div>
            </div>
            <div class="row"><label for="New_Agency_Trainer">Trainer:</label>
                <div class="float-right"><%= Html.Users("Trainer", "", new { @id = "New_Agency_Trainer", @class = "requireddropdown" })%></div>
            </div>
            <div class="row"><label for="New_Agency_BackupTrainer">Backup Trainer:</label>
                <div class="float-right"><%= Html.Users("BackupTrainer", "", new { @id = "New_Agency_BackupTrainer", @class = "requireddropdown" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_CompanyName">Company Name:</label><div class="float-right"><%=Html.TextBox("Name", "", new { @id = "New_Agency_CompanyName", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_TaxId">Tax Id:</label><div class="float-right"><%=Html.TextBox("TaxId", "", new { @id = "New_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_TaxIdType">Tax Id Type:</label><div class="float-right"><select id="New_Agency_TaxIdType" name="TaxIdType"><option value="0">-- Select Tax ID Type --</option><option value="1">EIN (Employer Identification Number)</option><option value="2">SSN (Social Security Number)</option></select></div></div>
            <div class="row"><label for="New_Agency_Payor">Payor:</label><div class="float-right"><select id="New_Agency_Payor" name="Payor"><option value="0">-- Select Payor --</option><option value="1">Palmetto GBA</option><option value="2">National Government Services</option><option value="3">Cigna Government Services</option><option value="4">Anthem Health Plans of Maine</option></select></div></div>
            <div class="row"><div class="float-right"><input type="checkbox" class="radio" name="New_Agency_SameAsAdmin" id="New_Agency_SameAsAdmin" />&nbsp;Check here if the Admin is same as the Contact Person</div></div>
            <div class="row"><label for="New_Agency_ContactPersonEmail">Contact Person E-mail:</label><div class="float-right"><%=Html.TextBox("ContactPersonEmail", "", new { @id = "New_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Agency_ContactPhone1">Contact Person Phone:</label><div class="float-right"><input type="text" class="autotext required numeric phone_short" name="ContactPhoneArray" id="New_Agency_ContactPhone1" maxlength="3" /> - <input type="text" class="autotext required numeric phone_short" name="ContactPhoneArray" id="New_Agency_ContactPhone2" maxlength="3" /> - <input type="text" class="autotext required numeric phone_long" name="ContactPhoneArray" id="New_Agency_ContactPhone3" maxlength="4"/></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_NationalProviderNo">National Provider Number:</label><div class="float-right"><%=Html.TextBox("NationalProviderNumber", "", new { @id = "New_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_MedicareProviderNo">Medicare Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicareProviderNumber", "", new { @id = "New_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float-right"><%=Html.TextBox("MedicaidProviderNumber", "", new { @id = "New_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_HomeHealthAgencyId">Unique Agency OASIS ID Code:</label><div class="float-right"><%=Html.TextBox("HomeHealthAgencyId", "", new { @id = "New_Agency_HomeHealthAgencyId", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_CahpsVendor">CAHPS Vendor:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.CahpsVendors, "CahpsVendor", "", new { @id = "New_Agency_CahpsVendor", @class = "valid" })%></div></div>
            <div class="row"><label for="New_Agency_ContactPersonFirstName">Contact Person First Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_ContactPersonLastName">Contact Person Last Name:</label><div class="float-right"><%=Html.TextBox("ContactPersonLastName", "", new { @id = "New_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "30" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Submitter Information (Medicare)</legend>
        <div class="float-left"><%=Html.CheckBox("IsAxxessTheBiller", false, new { @id = "New_Agency_AxxessBiller", @class = "radio" })%>&nbsp;<label for="New_Agency_AxxessBiller">Check here if Axxess&trade; HealthCare Consult handles billing for this Agency.</label></div>
        <div class="clear"></div>
        <div class="column">
            <div class="row"><label for="New_Agency_SubmitterId">Submitter Id:</label><div class="float-right"><%=Html.TextBox("SubmitterId", "", new { @id = "New_Agency_SubmitterId", @maxlength = "15", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Agency_SubmitterName">Submitter Name:</label><div class="float-right"><%=Html.TextBox("SubmitterName", "", new { @id = "New_Agency_SubmitterName", @maxlength = "50", @class = "text input_wrapper required" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_SubmitterPhone1">Submitter Phone Number:</label><div class="float-right"><input type="text" class="autotext numeric phone_short required" name="SubmitterPhoneArray" id="New_Agency_SubmitterPhone1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short required" name="SubmitterPhoneArray" id="New_Agency_SubmitterPhone2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long required" name="SubmitterPhoneArray" id="New_Agency_SubmitterPhone3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Agency_SubmitterFax1">Submitter Fax Number:</label><div class="float-right"><input type="text" class="autotext numeric phone_short required" name="SubmitterFaxArray" id="New_Agency_SubmitterFax1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short required" name="SubmitterFaxArray" id="New_Agency_SubmitterFax2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long required" name="SubmitterFaxArray" id="New_Agency_SubmitterFax3" maxlength="4" /></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_LocationName">Location Name: &nbsp;(e.g. Headquarters)</label><div class="float-right"><%=Html.TextBox("LocationName", "", new { @id = "New_Agency_LocationName", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_Phone1">Phone Number:</label><div class="float-right"><input type="text" class="autotext numeric required phone_short" name="PhoneArray" id="New_Agency_Phone1" maxlength="3" /> - <input type="text" class="autotext numeric required phone_short" name="PhoneArray" id="New_Agency_Phone2" maxlength="3" /> - <input type="text" class="autotext numeric required phone_long" name="PhoneArray" id="New_Agency_Phone3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Agency_Fax1">Fax Number:</label><div class="float-right"><input type="text" class="autotext numeric phone_short" name="FaxArray" id="New_Agency_Fax1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short" name="FaxArray" id="New_Agency_Fax2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long" name="FaxArray" id="New_Agency_Fax3" maxlength="4" /></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_AddressLine1">Address Line 1:</label><div class="float-right"><%=Html.TextBox("AddressLine1", "", new { @id = "New_Agency_AddressLine1", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_AddressLine2">Address Line 2:</label><div class="float-right"><%=Html.TextBox("AddressLine2", "", new { @id = "New_Agency_AddressLine2", @maxlength = "50", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_AddressCity">City:</label><div class="float-right"><%=Html.TextBox("AddressCity", "", new { @id = "New_Agency_AddressCity", @maxlength = "40", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_AddressStateCode">State:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Agency_AddressStateCode", @class = "AddressStateCode required valid" }) %></div></div>
            <div class="row"><label for="New_Agency_AddressZipCode" class="float-left">Zip:</label><div class="float-right"><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Agency_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%> - <%= Html.TextBox("AddressZipCodeFour", "", new { @id = "New_Agency_AddressZipCodeFour", @class = "text required digits isValidUSZip zip-small", @maxlength = "4" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('newagency');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
