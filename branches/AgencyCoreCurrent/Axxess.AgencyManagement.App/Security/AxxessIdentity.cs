﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Text;
    using System.Security.Principal;

    using Axxess.Membership.Domain;
    using Axxess.AgencyManagement.Domain;

    using Axxess.Core.Infrastructure;

    using Domain;

    [Serializable]
    public class AxxessIdentity : IIdentity
    {
        #region Constructor

        public AxxessIdentity()
        {
            this.IsAuthenticated = true;
            this.Session = new UserSession();
            this.LastSecureActivity = DateTime.Now;
        }

        public AxxessIdentity(Guid id, string name) : this()
        {
            this.Id = id;
            this.Name = name;
        }

        #endregion

        #region IIdentity Members

        public string Name { get; private set; }

        public bool IsAuthenticated { get; set; }

        public bool IsImpersonated { get; set; }

        public UserSession Session { get; set; }

        public string AuthenticationType
        {
            get { return AppSettings.AuthenticationType; }
        }

        #endregion

        #region Axxess Identity Members

        public Guid Id { get; private set; }

        public DateTime LastSecureActivity { get; set; }

        #endregion
       
    }
}
