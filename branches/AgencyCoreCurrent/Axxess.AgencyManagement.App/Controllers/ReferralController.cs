﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;
    using Security;
    using iTextExtension;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    
    using Axxess.Log.Enums;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReferralController : BaseController
    {
        #region Private Members/Constructor

        private readonly IReferralService referralService;
        private readonly IReferralRepository referralRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IUserRepository userRepository;
        private readonly IPatientService patientService;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;

        public ReferralController(IAgencyManagementDataProvider agencyManagementDataProvider, IReferralService referralService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.referralService = referralService;
            this.patientService = patientService;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
        }

        #endregion

        #region ReferralController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Get(Guid id)
        {
            return Json(referralRepository.Get( Current.AgencyId,id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid()
        {
            return View(new GridModel(referralService.GetPending(Current.AgencyId)));
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add([Bind]Referral referral)
        {
            Check.Argument.IsNotNull(referral, "referral");

            var viewData = new JsonViewData();

            if (referral.IsValid && (referral.EmergencyContact == null || (referral.EmergencyContact != null && referral.EmergencyContact.IsValid)))
            {
                if (!referralService.AddReferral(referral))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the referral.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Referral was created successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                if (!referral.IsValid)
                {
                    viewData.errorMessage = referral.ValidationMessage;
                }
                else
                {
                    viewData.errorMessage = referral.EmergencyContact.ValidationMessage;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNonAdmit(Guid referralId)
        {
            return PartialView("NonAdmit", referralRepository.Get(Current.AgencyId, referralId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNonAdmit([Bind] PendingPatient referral)
        {
            Check.Argument.IsNotNull(referral, "PendingReferral");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Non-Admission of referral could not be saved." };
            referral.AgencyId = Current.AgencyId;
            if (referralRepository.NonAdmitReferral(referral))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralNonAdmitted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Referral non-admission successful.";
            }
            return Json(viewData);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if(referral != null)
            {
                referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, id);
            }
            return PartialView("Edit", referral);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ReferralPdf(Guid id)
        {
            var referral = referralRepository.Get(Current.AgencyId, id);
            if(referral != null)
            {
                if (referral.Physicians.IsNotNullOrEmpty())
                {
                    var physicians = referral.Physicians.ToObject<List<Physician>>();
                    if (physicians != null && physicians.Count > 0)
                    {
                        var primary = physicians.FirstOrDefault(p => p.IsPrimary);
                        if (primary != null)
                        {
                            referral.PrimaryPhysician = physicianRepository.Get(primary.Id, Current.AgencyId);
                        }
                    }
                }

                if (referral.ReferrerPhysician != null && referral.ReferrerPhysician.IsNotEmpty())
                {
                    var referralDoctor = physicianRepository.Get(referral.ReferrerPhysician, Current.AgencyId);
                    if (referralDoctor != null)
                    {
                        referral.ReferralPhysicianName = referralDoctor.DisplayName;
                    }
                }
                if (referral.InternalReferral != null && referral.InternalReferral.IsNotEmpty())
                {
                    var internalReferral = userRepository.GetUserOnly(referral.InternalReferral,Current.AgencyId);
                    if (internalReferral != null)
                    {
                        referral.InternalReferralName = internalReferral.DisplayName;
                    }
                }
                if (referral.UserId != null && referral.UserId.IsNotEmpty())
                {
                    var assignedClinician = userRepository.GetUserOnly(referral.UserId, Current.AgencyId);
                    if (assignedClinician != null)
                    {
                        referral.ClinicianName = assignedClinician.DisplayName;
                    }
                }
                referral.EmergencyContact = referralRepository.GetFirstEmergencyContactByReferral(Current.AgencyId, id);
                if (referral.PrimaryInsurance.IsNotNullOrEmpty())
                {
                    referral.PrimaryInsuranceName = patientService.GetInsurance(referral.PrimaryInsurance);
                    if (referral.PrimaryRelationship.IsNotNullOrEmpty())
                    {
                        referral.PrimaryRelationshipDescription = patientService.GetRelationshipDescription(referral.PrimaryRelationship);
                    }
                }
                if (referral.SecondaryInsurance.IsNotNullOrEmpty())
                {
                    referral.SecondaryInsuranceName = patientService.GetInsurance(referral.SecondaryInsurance);
                    if (referral.SecondaryRelationship.IsNotNullOrEmpty())
                    {
                        referral.SecondaryRelationshipDescription = patientService.GetRelationshipDescription(referral.SecondaryRelationship);
                    }
                }
                if (referral.TertiaryInsurance.IsNotNullOrEmpty())
                {
                    referral.TertiaryInsuranceName = patientService.GetInsurance(referral.TertiaryInsurance);
                    if (referral.TertiaryRelationship.IsNotNullOrEmpty())
                    {
                        referral.TertiaryRelationshipDescription = patientService.GetRelationshipDescription(referral.TertiaryRelationship);
                    }
                }
            }
            ReferralPdf doc = new ReferralPdf(referral, agencyRepository.GetWithBranches(Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Referral_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [GridAction]
        public ActionResult GetPhysicians(Guid ReferralId)
        {
            Check.Argument.IsNotEmpty(ReferralId, "ReferralId");
            
            var list = new List<ReferalPhysician>();
            var referral = referralRepository.Get(Current.AgencyId, ReferralId);
            if (referral != null && referral.Physicians.IsNotNullOrEmpty())
            {
                var physicians = referral.Physicians.ToObject<List<Physician>>();
                foreach (var physician in physicians)
                {
                    var agencyPhysician = physicianRepository.Get(physician.Id, Current.AgencyId);
                    if (agencyPhysician != null)
                    {
                        list.Add(new ReferalPhysician
                        {
                            Id = agencyPhysician.Id,
                            FirstName = agencyPhysician.FirstName,
                            LastName = agencyPhysician.LastName,
                            WorkPhone = agencyPhysician.PhoneWorkFormatted,
                            FaxNumber = agencyPhysician.FaxNumberFormatted,
                            EmailAddress = agencyPhysician.EmailAddress,
                            IsPrimary = physician.IsPrimary
                        });
                    }
                }
            }
            return Json(new GridModel(list));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddPhysician(Guid id, Guid referralId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(referralId, "referralId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician could not be added. Please try again." };
            var referral = referralRepository.Get(Current.AgencyId, referralId);
            if (referral != null && referral.Physicians.IsNotNullOrEmpty())
            {
                var physicians = referral.Physicians.ToObject<List<Physician>>();
                referral.AgencyPhysicians = new List<Guid>();
                foreach (var physician in physicians)
                {
                    referral.AgencyPhysicians.Add(physician.Id);
                }
                if (!referral.AgencyPhysicians.Contains(id))
                {
                    referral.AgencyPhysicians.Add(id);
                    if (referralRepository.Update(referral))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The physician has been added successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The physician already exists in the list.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePhysician(Guid id, Guid referralId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(referralId, "referralId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician could not be deleted. Please try again." };
            var referral = referralRepository.Get(Current.AgencyId, referralId);
            if (referral != null && referral.Physicians.IsNotNullOrEmpty())
            {
                var physicians = referral.Physicians.ToObject<List<Physician>>();
                for (int i = 0; i < physicians.Count(); i++)
                {
                    if (physicians[i].Id.Equals(id))
                    {
                        physicians.RemoveAt(i--);
                    }
                }
                referral.AgencyPhysicians = new List<Guid>();
                if (physicians.Count > 0)
                {
                    foreach (var physician in physicians)
                    {
                        referral.AgencyPhysicians.Add(physician.Id);
                    }
                }
                else
                {
                    referral.AgencyPhysicians.Add(Guid.Empty);
                }
                if (referralRepository.Update(referral))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician was removed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetPrimaryPhysician(Guid id, Guid referralId) {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(referralId, "referralId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The physician was not set as primary. Try Again." };
            var Referral = referralRepository.Get(Current.AgencyId, referralId);
            if (Referral != null && Referral.Physicians.IsNotNullOrEmpty()) {
                var Physicians = Referral.Physicians.ToObject<List<Physician>>();
                for (int i = 0; i < Physicians.Count(); i++) if (Physicians[i].Id.Equals(id)) Physicians.RemoveAt(i--);
                Referral.AgencyPhysicians = new List<Guid>();
                Referral.AgencyPhysicians.Add(id);
                foreach (var Physician in Physicians) Referral.AgencyPhysicians.Add(Physician.Id);
                if (referralRepository.Update(Referral)) {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The physician was successfully set as primary.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in setting physician as primary.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Referral referral) {
            Check.Argument.IsNotNull(referral, "referral");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(referral.FirstName), ". Patient first name is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.LastName), ". Patient last name is required. <br>"),
                           new Validation(() => string.IsNullOrEmpty(referral.DOB.ToString()), ". Patient date of birth is required.<br/>"),
                           new Validation(() => !referral.DOB.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.Gender), ". Patient gender has to be selected.<br/>"),
                           new Validation(() => (referral.EmailAddress == null ? !string.IsNullOrEmpty(referral.EmailAddress) : !referral.EmailAddress.IsEmail()), ". Patient e-mail is not in a valid  format.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressLine1), ". Patient address line is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressCity), ". Patient city is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressStateCode), ". Patient state is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(referral.AddressZipCode), ". Patient zip is required.<br/>"),
                           new Validation(() => referral.EmergencyContact != null && referral.EmergencyContact.HasValue &&
                               (string.IsNullOrEmpty(referral.EmergencyContact.FirstName) || 
                               string.IsNullOrEmpty(referral.EmergencyContact.LastName) || 
                               !referral.EmergencyContact.PrimaryPhoneHasValue), "Patient emergency contact requires a first name, last name, and a primary phone number.<br/>")
                      );

            if (viewData.isSuccessful) {
                referral.AgencyId = Current.AgencyId;
                if (referralRepository.Update(referral)) {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralUpdated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your referral has been updated.";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Could not update the referral. Please try again.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this Referral. Please try again." };
            if (referralRepository.Delete(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.Referral, LogAction.ReferralDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Referral has been deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReferralLogs(Guid referralId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.Referral, Current.AgencyId, referralId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Verify(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            return PartialView("Eligibility", patientService.VerifyEligibility(medicareNumber, lastName, firstName, dob, gender));
        }

        #endregion
    }
}
