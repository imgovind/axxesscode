﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Exports;
    using Security;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;

    using Axxess.OasisC.Repositories;
    using Axxess.OasisC.Domain;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IReportService reportService;
        private readonly IPayrollService payrollService;
        private readonly IPhysicianService physicianService;

        public ExportController(IAgencyManagementDataProvider agencyManagementDataProvider, IOasisCDataProvider oasisCDataProvider, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService, IPatientService patientService, IReportService reportService, IPayrollService payrollService, IBillingService billingService, IPhysicianService physicianService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.reportService = reportService;
            this.payrollService = payrollService;
            this.billingService = billingService;
            this.physicianService = physicianService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Contacts()
        {
            var contacts = agencyRepository.GetContacts(Current.AgencyId);
            var export = new ContactExporter(contacts);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Contacts_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Hospitals()
        {
            var hospitals = agencyRepository.GetHospitals(Current.AgencyId);
            var export = new HosptialExporter(hospitals);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Hospitals_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pharmacies()
        {
            var pharmacies = agencyRepository.GetPharmacies(Current.AgencyId);
            var export = new PharmacyExporter(pharmacies);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Pharmacies_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult Physicians()
        {
            var physicians = physicianService.GetAgencyPhysiciansWithPecosVerification();
            var export = new PhysicianExporter(physicians);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Physicians_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Referrals()
        {
            IEnumerable<Referral> referrals = null;
            if (Current.IsCommunityLiason || Current.IsInRole(AgencyRoles.ExternalReferralSource))
            {
                referrals = referralRepository.GetAllByCreatedUser(Current.AgencyId, Current.UserId, ReferralStatus.Pending);
            }
            else
            {
                referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.Pending);
            }
            var export = new ReferralExporter(referrals.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Referrals_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult Patients(Guid BranchId, int Status)
        {
            var patients = patientService.GetPatients(Current.AgencyId, BranchId, Status); // patientRepository.GetAllByAgencyId(Current.AgencyId);
            var export = new PatientExporter(patients.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Patients_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult DeletedPatients(Guid BranchId)
        {
            var patients = patientService.GetDeletedPatients(Current.AgencyId, BranchId); 
            var export = new PatientExporter(patients.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("DeletedPatients_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ActiveUsers()
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Active);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ActiveUsers_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult InactiveUsers()
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Inactive);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("InactiveUsers_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult DeleteUsers()
        {
            var users = userRepository.GetDeletedUsers(Current.AgencyId, true);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("DeletedUsers_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CahpsReport(Guid BranchId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var export = new CahpsExporter(BranchId, sampleMonth, sampleYear, paymentSources);
            return File(export.Process().ToArray(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleList()
        {
            var export = new UserScheduleExporter(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ScheduleList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supplies()
        {
            var export = new SupplyExporter(agencyRepository.GetSupplies(Current.AgencyId));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Supplies_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAScheduleList(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var export = new QAScheduleExporter(agencyService.GetCaseManagerSchedule(BranchId, Status, StartDate, EndDate).OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("QAScheduleList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLog()
        {
            var patientList = patientRepository.GetHospitalizedPatients(Current.AgencyId);
            if (patientList != null && patientList.Count > 0)
            {
                patientList.ForEach(d =>
                {
                    d.User = !d.UserId.IsEmpty() ? UserEngine.GetName(d.UserId, Current.AgencyId) : string.Empty;
                });
            }
            var export = new HospitalizationLogExporter(patientList);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("HospitalizationLog_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHospitalizationLog(Guid patientId)
        {
            var logs = patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId);
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            var export = new PatientHospitalizationLogExporter(logs, patient!=null? patient.DisplayName : string.Empty);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("{0}_HospitalizationLog_{1}.xls", patient != null ? patient.DisplayName : string.Empty, DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueueList(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            string branchName = "All branches";
            if (BranchId != Guid.Empty)
            {
                branchName = agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            }
            var export = new PrintQueueExporter(branchName, agencyService.GetPrintQueue(BranchId, StartDate, EndDate).OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PrintQueue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ExportedOasis(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var assessments = assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisExported, Status, StartDate, EndDate);
            var export = new ExportedOasis(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ExportedOasis_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult NotExportedOasis(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var assessments = assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedNotExported, Status, StartDate, EndDate);
            var export = new ExportedOasis(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("NotExportedOasis_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OasisExport(Guid BranchId, [ModelBinder(typeof(CommaSeparatedModelBinder))]List<int> paymentSources)
        {
            var assessments = assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedExportReady, paymentSources);
            var export = new OasisExporter(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OASISToExport_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Authorizations(Guid patientId)
        {
            IList<Authorization> authorizations = patientRepository.GetAuthorizations(Current.AgencyId, patientId);
            var export = new AuthorizationsExporter(authorizations);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Authorizations_{0}.xls", DateTime.Now.Ticks));
        }

        

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrdersHistory(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var orders = agencyService.GetProcessedOrders(BranchId, StartDate, EndDate, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature });
            var export = new OrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Orders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PatientOrdersHistory(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            var orders = patientService.GetPatientOrders(patientId, StartDate, EndDate);
            var export = new PatientOrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PatientOrders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrdersPendingSignature(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var orders = agencyService.GetOrdersPendingSignature(BranchId, StartDate, EndDate);
            var export = new PendingSignatureOrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersPendingPhysicianSignature_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime StartDate, DateTime EndDate)
        {
            var orders = agencyService.GetOrdersToBeSent(BranchId, sendAutomatically, StartDate, EndDate);
            var export = new OrdersToBeSentExporter(orders.ToList(), sendAutomatically, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersToBeSent_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult RecertsPastDue(Guid BranchId, int insuranceId, DateTime StartDate)
        {
            var recertEvents = agencyService.GetRecertsPastDue(BranchId, insuranceId, StartDate, DateTime.Now);
            var export = new PastDueRecertExporter(recertEvents.ToList(), StartDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PastDueRecerts_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult RecertsUpcoming(Guid BranchId, int InsuranceId)
        {
            var recertEvents = agencyService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24));
            var export = new UpcomingRecertExporter(recertEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("UpcomingRecerts_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Templates()
        {
            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            var export = new TemplateExporter(templates.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Templates_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult NonVisitTasks()
        {
            var nonVisitTasks = agencyRepository.GetNonVisitTasks(Current.AgencyId);
            if (!nonVisitTasks.IsNull()) 
            {
                var export = new NonVisitTaskExporter(nonVisitTasks.ToList());
                if (export != null) 
                {
                    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AgencyNonVisits_{0}.xls", DateTime.Now.Ticks));
                }
            }
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UserNonVisitTasks()
        {
            var userNonVisitTasks = agencyService.GetUserNonVisitTasks(Current.AgencyId);
            if (!userNonVisitTasks.IsNull())
            {
                var export = new UserNonVisitTaskExporter(userNonVisitTasks.ToList());
                if (export != null)
                {
                    return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("UserNonVisitTasks_{0}.xls", DateTime.Now.Ticks));
                }
            }
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Incidents()
        {
            var incidents = agencyService.GetIncidents(Current.AgencyId);
            var export = new IncidentExporter(incidents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Incident_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Infections()
        {
            var infections = agencyService.GetInfections(Current.AgencyId);
            var export = new InfectionExporter(infections.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Infections_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult CommunicationNotes(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var communicationNotes = patientService.GetCommunicationNotes(BranchId, Status, StartDate, EndDate);
            var export = new CommunicationNoteExporter(communicationNotes.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("CommunicationNotes_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ScheduleDeviations(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var scheduleEvents = reportService.GetScheduleDeviation(BranchCode, StartDate, EndDate);
            var export = new ScheduleDeviationExporter(scheduleEvents.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("ScheduleDeviations_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PayrollSummary(DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var payroll = payrollService.GetSummary(StartDate, EndDate, payrollStatus);
            var export = new PayrollSummaryExport(payroll.OrderBy(e => e.LastName).ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollSummary_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PayrollDetail(Guid UserId, DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            detail.StartDate = StartDate;
            detail.EndDate = EndDate;
            detail.PayrollStatus = payrollStatus;
            if (!UserId.IsEmpty())
            {
                detail.Id = UserId;
                detail.Name = UserEngine.GetName(UserId, Current.AgencyId);
                detail.Visits = payrollService.GetVisits(UserId, StartDate, EndDate, payrollStatus, true);
                detail.Visits = detail.Visits.OrderBy(e => e.PatientName).ToList();
            }
            var export = new PayrollDetailExport(detail);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetail_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PayrollDetails(DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var details = payrollService.GetDetails(StartDate, EndDate, payrollStatus, true);
            var export = new PayrollDetailsExport(details, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetails_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult SubmittedBatchClaims(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            var claims = billingRepository.ClaimDatas(Current.AgencyId, StartDate, EndDate, ClaimType);
            var export = new SubmittedBatchClaimsExport(claims.ToList(), StartDate, EndDate, ClaimType);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("SubmittedBatchClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSEpisodeInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsEpisodeExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSVisitInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsVisitExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSPaymentInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsPaymentExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult PPSChargeInformationReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var export = new PpsChargeExporter(BranchId, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult SubmittedClaimDetails(int Id, DateTime StartDate, DateTime EndDate)
        {
            var claims = billingService.GetSubmittedBatchClaims(Id);
            var export = new SubmittedClaimDetailsExporter(claims, StartDate, EndDate);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var visits = patientService.GetMissedScheduledEvents(Current.AgencyId, BranchId, StartDate, EndDate).OrderBy(p => p.PatientName).ToList();
            var export = new MissedVisitsExporter(visits, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("MissedVisits_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult AdjustmentCodes()
        {
            var codes = agencyRepository.GetAdjustmentCodes(Current.AgencyId);
            var export = new AdjustmentCodesExporter(codes.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("AdjustmentCodes_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult UploadTypes()
        {
            var classes = agencyRepository.GetUploadType(Current.AgencyId);
            var export = new UploadTypesExporter(classes.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("UploadTypes_{0}.xls", DateTime.Now.Ticks));
        }


        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult PatientsAndVisitsByAgeReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new PatientsAndVisitsByAgeExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult DischargesByReasonReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new DischargesByReasonExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult VisitsByPrimaryPaymentSourceReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new VisitsByPrimaryPaymentSourceExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult VisitsByStaffTypeReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new VisitsByStaffTypeExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult AdmissionsByReferralSourceReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new AdmissionsByReferralSourceExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public FileResult PatientsVisitsByPrincipalDiagnosisReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        //{
        //    var export = new PatientsVisitsByPrincipalDiagnosisExporter(Current.AgencyId, BranchId, StartDate, EndDate);
        //    return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        //}

        #endregion
    }
}
