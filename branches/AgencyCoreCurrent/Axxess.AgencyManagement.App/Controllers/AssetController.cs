﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Domain;
    using Services;
    using Security;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;


    [Audit]
    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AssetController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAssetService assetService;
        private readonly IAssetRepository assetRepository;

        public AssetController(IAgencyManagementDataProvider agencyManagementDataProvider, IAssetService assetService)
        {
            Check.Argument.IsNotNull(assetService, "assetService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assetService = assetService;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
        }

        #endregion

        #region AssetController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Serve(Guid assetId)
        {
            var asset = assetRepository.Get(assetId, Current.AgencyId);
            if (asset != null)
            {
                asset.FileName = asset.FileName.Replace(",", "-").Replace(";", "-");
                if (CoreSettings.UseDBAssets)
                {
                    return File(asset.Bytes, asset.ContentType, asset.FileName);
                }
                else
                {
                    string key = CoreSettings.AssetKey.ToLower();
                    var date = DateTime.Now.AddMinutes(5).ToString("yyyyMMddHHmmss");
                    var hash = AssetHelper.CreateHash(date);
                    var url = string.Format(CoreSettings.AssetServeURL, asset.Id.ToString(), key, Convert.ToBase64String(Encoding.UTF8.GetBytes(asset.AgencyId.ToString())), date, hash, asset.FileName);
                    return Redirect(url);
                }
            }
            return Json(new JsonViewData { isSuccessful = false, errorMessage = "Asset could not be served" });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Delete(Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "This asset could not be deleted." };
            if (assetService.DeleteAsset(assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The asset was successfully deleted.";
            }
            return Json(viewData);
        }
        
        #endregion
    }
}
