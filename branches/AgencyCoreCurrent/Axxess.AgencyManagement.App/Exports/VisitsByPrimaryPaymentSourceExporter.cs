﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class VisitsByPrimaryPaymentSourceExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid branchId;
        private Guid agencyId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public VisitsByPrimaryPaymentSourceExporter(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
            : base()
        {
            this.branchId = branchId;
            this.agencyId = agencyId;
            this.endDate = endDate;
            this.startDate = startDate;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "VisitsByPrimaryPaymentSource.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Visits By Primary Payment Source";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("VisitsByPrimaryPaymentSource");

            var agency = agencyRepository.GetAgencyOnly(agencyId);
            var agencyName = agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty;

            List<PrimaryPaymentSourceResult> data = reportAgent.VisitsByPrimaryPaymentSource(agencyId, branchId, startDate, endDate);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 2;

                var titleRow = sheet.CreateRow(0);
                titleRow.CreateCell(0).SetCellValue(agencyName);
                titleRow.CreateCell(1).SetCellValue("Visits By Primary Payment Source Report");
                titleRow.CreateCell(2).SetCellValue(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

                var headerRow = sheet.CreateRow(1);
                headerRow.CreateCell(0).SetCellValue("Line No.");
                headerRow.CreateCell(1).SetCellValue("Reimbursement Source");
                headerRow.CreateCell(2).SetCellValue("Visits");

                data.ForEach(item =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    dataRow.CreateCell(0).SetCellValue(item.LineNumber);
                    dataRow.CreateCell(1).SetCellValue(item.Description);
                    dataRow.CreateCell(2).SetCellValue(item.Visits);
                    rowIndex++;
                });

                workBook.FinishWritingToExcelSpreadsheet(3);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Visits By Primary Payment Source found!");
            }
        }

        #endregion

    }
}
