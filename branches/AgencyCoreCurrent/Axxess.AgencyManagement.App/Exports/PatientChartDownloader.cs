﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.IO;
    using System.Collections.Generic;

    using Enums;
    using Services;
    using Extensions;
    using iTextExtension;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Ionic.Zip;

    public class PatientChartDownloader : BaseExporter
    {
        private Guid agencyId;
        private Guid episodeId;
        private Guid patientId;


        private readonly IAssessmentService assessmentService = Container.Resolve<IAssessmentService>();
        private readonly IPatientService patientService = Container.Resolve<IPatientService>();
        private readonly IAgencyService agencyService = Container.Resolve<IAgencyService>();
        private readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
        private readonly IPatientRepository patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;
        public PatientChartDownloader(Guid agencyId, Guid episodeId, Guid patientId)
            : base()
        {
            this.agencyId = agencyId;
            this.episodeId = episodeId;
            this.patientId = patientId;
            this.FormatType = ExportFormatType.ZIP;
            this.FileName = string.Format("{0}.zip", DateTime.Now.Ticks);
        }

        protected override void WriteToArchive()
        {
            var patientEpisode = patientRepository.GetPatientScheduledEvents(agencyId, episodeId, patientId);
            if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
            {
                var tasks = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                if (tasks != null && tasks.Count > 0)
                {
                    tasks.ForEach(task =>
                    {
                        ProcessDownload(task);
                    });
                }
            }
        }

        private void ProcessDownload(ScheduleEvent task)
        {
            string zipEntryName = string.Format("{0}-{1}-{2}.pdf",
                task.DisciplineTaskName.Replace(" ", "-").Replace("/", ""),
                task.EventDate.ToOrderedDate().Replace("/", "-"),
                task.EventId.ToString().Substring(0, 5).Replace("-", ""));
            AxxessPdf pdf = ProcessDownloadHelper(task);
            if (pdf != null)
            {
                var stream = pdf.GetStream();
                if (stream != null)
                {
                    stream.Position = 0;
                    this.zipFile.AddEntry(zipEntryName, stream.ToArray());
                    stream.Close();
                }
            }
        }

        private AxxessPdf ProcessDownloadHelper(ScheduleEvent task)
        {
            AxxessPdf pdf = null;
            if (this.zipFile != null)
                switch (task.DisciplineTask)
                {
                    case (int)DisciplineTasks.OASISCDeath:
                    case (int)DisciplineTasks.OASISCDeathOT:
                    case (int)DisciplineTasks.OASISCDeathPT:
                    case (int)DisciplineTasks.OASISCDischarge:
                    case (int)DisciplineTasks.OASISCDischargeOT:
                    case (int)DisciplineTasks.OASISCDischargePT:
                    case (int)DisciplineTasks.OASISCDischargeST:
                    case (int)DisciplineTasks.NonOASISDischarge:
                    case (int)DisciplineTasks.OASISCFollowUp:
                    case (int)DisciplineTasks.OASISCFollowupPT:
                    case (int)DisciplineTasks.OASISCFollowupOT:
                    case (int)DisciplineTasks.OASISCRecertification:
                    case (int)DisciplineTasks.OASISCRecertificationPT:
                    case (int)DisciplineTasks.OASISCRecertificationOT:
                    case (int)DisciplineTasks.OASISCRecertificationST:
                    case (int)DisciplineTasks.SNAssessmentRecert:
                    case (int)DisciplineTasks.NonOASISRecertification:
                    case (int)DisciplineTasks.OASISCResumptionofCare:
                    case (int)DisciplineTasks.OASISCResumptionofCarePT:
                    case (int)DisciplineTasks.OASISCResumptionofCareOT:
                    case (int)DisciplineTasks.OASISCStartofCare:
                    case (int)DisciplineTasks.OASISCStartofCarePT:
                    case (int)DisciplineTasks.OASISCStartofCareOT:
                    case (int)DisciplineTasks.SNAssessment:
                    case (int)DisciplineTasks.NonOASISStartofCare:
                    case (int)DisciplineTasks.OASISCTransferDischarge:
                    case (int)DisciplineTasks.OASISCTransferDischargePT:
                    case (int)DisciplineTasks.OASISCTransfer:
                    case (int)DisciplineTasks.OASISCTransferPT:
                    case (int)DisciplineTasks.OASISCTransferOT:
                        {
                            pdf = new OasisPdf(assessmentService.GetAssessmentPrint(agencyId, episodeId, patientId, task.EventId));
                            break;
                        }
                    case (int)DisciplineTasks.SkilledNurseVisit:
                    case (int)DisciplineTasks.SNVisitPM:
                    case (int)DisciplineTasks.SNInsulinAM:
                    case (int)DisciplineTasks.SNInsulinPM:
                    case (int)DisciplineTasks.SNInsulinHS:
                    case (int)DisciplineTasks.SNInsulinNoon:
                    case (int)DisciplineTasks.FoleyCathChange:
                    case (int)DisciplineTasks.SNB12INJ:
                    case (int)DisciplineTasks.SNBMP:
                    case (int)DisciplineTasks.SNCBC:
                    case (int)DisciplineTasks.SNHaldolInj:
                    case (int)DisciplineTasks.PICCMidlinePlacement:
                    case (int)DisciplineTasks.PRNFoleyChange:
                    case (int)DisciplineTasks.PRNSNV:
                    case (int)DisciplineTasks.PRNVPforCMP:
                    case (int)DisciplineTasks.PTWithINR:
                    case (int)DisciplineTasks.PTWithINRPRNSNV:
                    case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case (int)DisciplineTasks.SNDC:
                    case (int)DisciplineTasks.SNEvaluation:
                    case (int)DisciplineTasks.SNFoleyLabs:
                    case (int)DisciplineTasks.SNFoleyChange:
                    case (int)DisciplineTasks.LVNVisit:
                    case (int)DisciplineTasks.SNInjection:
                    case (int)DisciplineTasks.SNInjectionLabs:
                    case (int)DisciplineTasks.SNLabsSN:
                    case (int)DisciplineTasks.SNVwithAideSupervision:
                    case (int)DisciplineTasks.SNVDCPlanning:
                    case (int)DisciplineTasks.SNVTeachingTraining:
                    case (int)DisciplineTasks.SNVManagementAndEvaluation:
                    case (int)DisciplineTasks.SNVObservationAndAssessment:
                    case (int)DisciplineTasks.SNWoundCare:
                        {
                            pdf = new SNVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                            break;
                        }
                    case (int)DisciplineTasks.SNDiabeticDailyVisit:
                        pdf = new DiabeticDailyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.Labs:
                        pdf = new LabsPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.SNPediatricVisit:
                        var snPediatricData = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId);
                        if (snPediatricData.Version == 2)
                        {
                            pdf = new PediatricPdf(snPediatricData, PdfDocs.Pediatric2);
                        }
                        else
                        {
                            pdf = new PediatricPdf(snPediatricData, PdfDocs.Pediatric);
                        }
                        break;
                    case (int)DisciplineTasks.SNPediatricAssessment:
                        pdf = new PediatricPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.PediatricAssessment);
                        break;
                    case (int)DisciplineTasks.SNVPsychNurse:
                        pdf = new PsychPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.Psych, task.Version);
                        break;
                    case (int)DisciplineTasks.SNPsychAssessment:
                        pdf = new PsychAssessmentPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.PsychAssessment, task.Version);
                        break;
                    case (int)DisciplineTasks.LVNSupervisoryVisit:
                        pdf = new LVNSVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.HHAideSupervisoryVisit:
                        pdf = new HHASVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.InitialSummaryOfCare:
                        pdf = new InitialSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.PTSupervisoryVisit:
                    case (int)DisciplineTasks.OTSupervisoryVisit:
                        pdf = new TherapySupervisoryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.NutritionalAssessment:
                        pdf = new DieticianPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.OTMaintenance:
                    case (int)DisciplineTasks.OTVisit:
                    case (int)DisciplineTasks.COTAVisit:
                    case (int)DisciplineTasks.OTDischarge:
                    case (int)DisciplineTasks.OTReassessment:
                    case (int)DisciplineTasks.OTEvaluation:
                    case (int)DisciplineTasks.OTReEvaluation:
                    case (int)DisciplineTasks.STEvaluation:
                    case (int)DisciplineTasks.STReEvaluation:
                    case (int)DisciplineTasks.STDischarge:
                    case (int)DisciplineTasks.STReassessment:
                    case (int)DisciplineTasks.STVisit:
                    case (int)DisciplineTasks.STMaintenance:
                    case (int)DisciplineTasks.PTEvaluation:
                    case (int)DisciplineTasks.PTReEvaluation:
                    case (int)DisciplineTasks.PTDischarge:
                    case (int)DisciplineTasks.PTMaintenance:
                    case (int)DisciplineTasks.PTReassessment:
                    case (int)DisciplineTasks.PTVisit:
                    case (int)DisciplineTasks.PTAVisit:
                        pdf = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.MSWVisit:
                    case (int)DisciplineTasks.MSWAssessment:
                    case (int)DisciplineTasks.MSWProgressNote:
                    case (int)DisciplineTasks.MSWDischarge:
                    case (int)DisciplineTasks.MSWEvaluationAssessment:
                        pdf = new MSWPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.DriverOrTransportationNote:
                        pdf = new TransportationPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.HHAideVisit:
                        pdf = new HHAVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.HHAideCarePlan:
                        pdf = new HHACarePlanPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.PASTravel:
                        pdf = new PASTravelPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.PASVisit:
                        pdf = new PASVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.PASCarePlan:
                        pdf = new PASCarePlanPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.HomeMakerNote:
                        pdf = new HomeMakerNotePdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.OTDischargeSummary:
                        pdf = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.OTDischargeSummary);
                        break;
                    case (int)DisciplineTasks.PTDischargeSummary:
                        var ptDischargeSummaryData = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId);
                        if (ptDischargeSummaryData.Version == 2)
                        {
                            pdf = new DischargeSummaryPdf(ptDischargeSummaryData, PdfDocs.PTDischargeSummary2);
                        }
                        else
                        {
                            pdf = new DischargeSummaryPdf(ptDischargeSummaryData, PdfDocs.PTDischargeSummary);
                        }

                        break;
                    case (int)DisciplineTasks.STDischargeSummary:
                        pdf = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.STDischargeSummary);
                        break;
                    case (int)DisciplineTasks.DischargeSummary:
                        pdf = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.DischargeSummary);
                        break;
                    case (int)DisciplineTasks.PhysicianOrder:
                        pdf = new PhysicianOrderPdf(patientService.GetOrderPrint(patientId, task.EventId, agencyId));
                        break;
                    case (int)DisciplineTasks.HCFA485StandAlone:
                    case (int)DisciplineTasks.HCFA485:
                        pdf = new PlanOfCarePdf(assessmentService.GetPlanOfCarePrint(episodeId, patientId, task.EventId, agencyId));
                        break;
                    case (int)DisciplineTasks.NonOasisHCFA485:

                        break;
                    case (int)DisciplineTasks.HCFA486:
                    case (int)DisciplineTasks.PostHospitalizationOrder:
                    case (int)DisciplineTasks.MedicaidPOC:
                        break;
                    case (int)DisciplineTasks.IncidentAccidentReport:
                        pdf = new IncidentReportPdf(agencyService.GetIncidentReportPrint(episodeId, patientId, task.EventId, agencyId));
                        break;
                    case (int)DisciplineTasks.InfectionReport:
                        pdf = new InfectionReportPdf(agencyService.GetInfectionReportPrint(episodeId, patientId, task.EventId, agencyId));
                        break;
                    case (int)DisciplineTasks.SixtyDaySummary:
                        pdf = new SixtyDaySummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.TransferSummary:
                    case (int)DisciplineTasks.CoordinationOfCare:
                        pdf = new TransferSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.CommunicationNote:
                        pdf = new ComNotePdf(patientService.GetCommunicationNotePrint(task.EventId, patientId, agencyId));
                        break;
                    case (int)DisciplineTasks.FaceToFaceEncounter:
                        pdf = new PhysFaceToFacePdf(patientService.GetFaceToFacePrint(patientId, task.EventId, agencyId));
                        break;
                    case (int)DisciplineTasks.UAPWoundCareVisit:
                        pdf = new WoundCarePdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                        pdf = new UAPInsulinPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
                        break;
                    case (int)DisciplineTasks.MedicareEligibilityReport:
                        var eligibility = patientRepository.GetMedicareEligibility(agencyId, patientId, task.EventId);
                        var patientEligibility = eligibility.Result.FromJson<PatientEligibility>();
                        pdf = new MedicareEligibilityPdf(patientEligibility, agencyRepository.GetWithBranches(agencyId), patientRepository.GetPatientOnly(patientId, agencyId));
                        break;
                }
            return pdf;
        }


        //private AxxessPdf ProcessDownloadHelper(ScheduleEvent task)
        //{
        //    string zipEntryName = string.Format("{0}-{1}-{2}.pdf",
        //        task.DisciplineTaskName.Replace(" ", "-").Replace("/", ""),
        //        task.EventDate.ToOrderedDate().Replace("/", "-"),
        //        task.EventId.ToString().Substring(0, 5).Replace("-", ""));
        //    if (this.zipFile != null)
        //        switch (task.DisciplineTask)
        //        {
        //            case (int)DisciplineTasks.OASISCDeath:
        //            case (int)DisciplineTasks.OASISCDeathOT:
        //            case (int)DisciplineTasks.OASISCDeathPT:
        //            case (int)DisciplineTasks.OASISCDischarge:
        //            case (int)DisciplineTasks.OASISCDischargeOT:
        //            case (int)DisciplineTasks.OASISCDischargePT:
        //            case (int)DisciplineTasks.OASISCDischargeST:
        //            case (int)DisciplineTasks.NonOASISDischarge:
        //            case (int)DisciplineTasks.OASISCFollowUp:
        //            case (int)DisciplineTasks.OASISCFollowupPT:
        //            case (int)DisciplineTasks.OASISCFollowupOT:
        //            case (int)DisciplineTasks.OASISCRecertification:
        //            case (int)DisciplineTasks.OASISCRecertificationPT:
        //            case (int)DisciplineTasks.OASISCRecertificationOT:
        //            case (int)DisciplineTasks.SNAssessmentRecert:
        //            case (int)DisciplineTasks.NonOASISRecertification:
        //            case (int)DisciplineTasks.OASISCResumptionofCare:
        //            case (int)DisciplineTasks.OASISCResumptionofCarePT:
        //            case (int)DisciplineTasks.OASISCResumptionofCareOT:
        //            case (int)DisciplineTasks.OASISCStartofCare:
        //            case (int)DisciplineTasks.OASISCStartofCarePT:
        //            case (int)DisciplineTasks.OASISCStartofCareOT:
        //            case (int)DisciplineTasks.SNAssessment:
        //            case (int)DisciplineTasks.NonOASISStartofCare:
        //            case (int)DisciplineTasks.OASISCTransferDischarge:
        //            case (int)DisciplineTasks.OASISCTransferDischargePT:
        //            case (int)DisciplineTasks.OASISCTransfer:
        //            case (int)DisciplineTasks.OASISCTransferPT:
        //            case (int)DisciplineTasks.OASISCTransferOT:
        //                {
        //                    var oasisDocument = new OasisPdf(assessmentService.GetAssessmentPrint(agencyId, episodeId, patientId, task.EventId));
        //                    var oasisStream = oasisDocument.GetFormattedStream();
        //                    oasisStream.Position = 0;
        //                    this.zipFile.AddEntry(zipEntryName, oasisStream);
        //                    break;
        //                }
        //            case (int)DisciplineTasks.SkilledNurseVisit:
        //            case (int)DisciplineTasks.SNInsulinAM:
        //            case (int)DisciplineTasks.SNInsulinPM:
        //            case (int)DisciplineTasks.SNInsulinHS:
        //            case (int)DisciplineTasks.SNInsulinNoon:
        //            case (int)DisciplineTasks.FoleyCathChange:
        //            case (int)DisciplineTasks.SNB12INJ:
        //            case (int)DisciplineTasks.SNBMP:
        //            case (int)DisciplineTasks.SNCBC:
        //            case (int)DisciplineTasks.SNHaldolInj:
        //            case (int)DisciplineTasks.PICCMidlinePlacement:
        //            case (int)DisciplineTasks.PRNFoleyChange:
        //            case (int)DisciplineTasks.PRNSNV:
        //            case (int)DisciplineTasks.PRNVPforCMP:
        //            case (int)DisciplineTasks.PTWithINR:
        //            case (int)DisciplineTasks.PTWithINRPRNSNV:
        //            case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
        //            case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
        //            case (int)DisciplineTasks.SNDC:
        //            case (int)DisciplineTasks.SNEvaluation:
        //            case (int)DisciplineTasks.SNFoleyLabs:
        //            case (int)DisciplineTasks.SNFoleyChange:
        //            case (int)DisciplineTasks.LVNVisit:
        //            case (int)DisciplineTasks.SNInjection:
        //            case (int)DisciplineTasks.SNInjectionLabs:
        //            case (int)DisciplineTasks.SNLabsSN:
        //            case (int)DisciplineTasks.SNVwithAideSupervision:
        //            case (int)DisciplineTasks.SNVDCPlanning:
        //            case (int)DisciplineTasks.SNVTeachingTraining:
        //            case (int)DisciplineTasks.SNVManagementAndEvaluation:
        //            case (int)DisciplineTasks.SNVObservationAndAssessment:
        //            case (int)DisciplineTasks.SNWoundCare:
        //                {
        //                    var snVisitDocument = new SNVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                    var snVisitStream = snVisitDocument.GetStream();
        //                    snVisitStream.Position = 0;
        //                    this.zipFile.AddEntry(zipEntryName, snVisitStream);
        //                    break;
        //                }
        //            case (int)DisciplineTasks.SNDiabeticDailyVisit:
        //                var diabeticDailyVisitDocument = new DiabeticDailyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                var diabeticDailyVisitStream = diabeticDailyVisitDocument.GetStream();
        //                diabeticDailyVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, diabeticDailyVisitStream);
        //                break;
        //            case (int)DisciplineTasks.Labs:
        //                var labsVisitDocument = new LabsPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                var labsVisitStream = labsVisitDocument.GetStream();
        //                labsVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, labsVisitStream);
        //                break;
        //            case (int)DisciplineTasks.SNPediatricVisit:
        //                Stream snPediatricStream = null;
        //                var snPediatricData = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId);
        //                if (snPediatricData.Version == 2)
        //                {
        //                    var snPediatricDocument = new PediatricPdf(snPediatricData, PdfDocs.Pediatric2);
        //                    snPediatricStream = snPediatricDocument.GetStream();
        //                    snPediatricStream.Position = 0;
        //                }
        //                else
        //                {
        //                    var snPediatricDocument = new PediatricPdf(snPediatricData, PdfDocs.Pediatric);
        //                    snPediatricStream = snPediatricDocument.GetStream();
        //                    snPediatricStream.Position = 0;
        //                }
        //                this.zipFile.AddEntry(zipEntryName, snPediatricStream);
        //                break;
        //            case (int)DisciplineTasks.SNPediatricAssessment:
        //                var PediatricAssessmentVisitDocument = new PediatricPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.PediatricAssessment);
        //                var PediatricAssessmentVisitStream = PediatricAssessmentVisitDocument.GetStream();
        //                PediatricAssessmentVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, PediatricAssessmentVisitStream);
        //                break;
        //            case (int)DisciplineTasks.SNVPsychNurse:
        //                var PsychVisitDocument = new PsychPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.Psych, task.Version);
        //                var PsychVisitStream = PsychVisitDocument.GetStream();
        //                PsychVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, PsychVisitStream);
        //                break;
        //            case (int)DisciplineTasks.SNPsychAssessment:
        //                var psychAssessmentVisitDocument = new PsychAssessmentPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.PsychAssessment, task.Version);
        //                var psychAssessmentVisitStream = psychAssessmentVisitDocument.GetStream();
        //                psychAssessmentVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, psychAssessmentVisitStream);
        //                break;
        //            case (int)DisciplineTasks.LVNSupervisoryVisit:
        //                var lvnsVisitDocument = new LVNSVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                var lvnsVisitStream = lvnsVisitDocument.GetStream();
        //                lvnsVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, lvnsVisitStream);
        //                break;
        //            case (int)DisciplineTasks.HHAideSupervisoryVisit:
        //                var hhasVisitDocument = new HHASVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                var hhasVisitStream = hhasVisitDocument.GetStream();
        //                hhasVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, hhasVisitStream);
        //                break;
        //            //case (int)DisciplineTasks.InitialSummaryOfCare:
        //            //    var initialSummaryVisitDocument = new InitialSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var initialSummaryVisitStream = initialSummaryVisitDocument.GetStream();
        //            //    initialSummaryVisitStream.Position = 0;

        //            //    ZipEntry initialSummaryEntry = this.zipFile.AddEntry(zipEntryName, initialSummaryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.PTSupervisoryVisit:
        //            //case (int)DisciplineTasks.OTSupervisoryVisit:
        //            //    var therapySupervisoryVisitDocument = new TherapySupervisoryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var therapySupervisoryVisitStream = therapySupervisoryVisitDocument.GetStream();
        //            //    therapySupervisoryVisitStream.Position = 0;

        //            //    ZipEntry therapySupervisoryEntry = this.zipFile.AddEntry(zipEntryName, therapySupervisoryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.NutritionalAssessment:
        //            //    var dieticianVisitDocument = new DieticianPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var dieticianVisitStream = dieticianVisitDocument.GetStream();
        //            //    dieticianVisitStream.Position = 0;

        //            //    ZipEntry dieticianEntry = this.zipFile.AddEntry(zipEntryName, dieticianVisitStream);
        //            //    break;
        //            case (int)DisciplineTasks.OTMaintenance:
        //            case (int)DisciplineTasks.OTVisit:
        //            case (int)DisciplineTasks.COTAVisit:
        //            case (int)DisciplineTasks.OTDischarge:
        //            case (int)DisciplineTasks.OTReassessment:
        //            case (int)DisciplineTasks.OTEvaluation:
        //            case (int)DisciplineTasks.OTReEvaluation:
        //            case (int)DisciplineTasks.STEvaluation:
        //            case (int)DisciplineTasks.STReEvaluation:
        //            case (int)DisciplineTasks.STDischarge:
        //            case (int)DisciplineTasks.STReassessment:
        //            case (int)DisciplineTasks.STVisit:
        //            case (int)DisciplineTasks.STMaintenance:
        //            case (int)DisciplineTasks.PTEvaluation:
        //            case (int)DisciplineTasks.PTReEvaluation:
        //            case (int)DisciplineTasks.PTDischarge:
        //            case (int)DisciplineTasks.PTMaintenance:
        //            case (int)DisciplineTasks.PTReassessment:
        //            case (int)DisciplineTasks.PTVisit:
        //            case (int)DisciplineTasks.PTAVisit:
        //                var therapyVisitDocument = new TherapyPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                var therapyVisitStream = therapyVisitDocument.GetStream();
        //                therapyVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, therapyVisitStream);
        //                break;
        //            case (int)DisciplineTasks.MSWVisit:
        //            case (int)DisciplineTasks.MSWAssessment:
        //            case (int)DisciplineTasks.MSWProgressNote:
        //            case (int)DisciplineTasks.MSWDischarge:
        //            case (int)DisciplineTasks.MSWEvaluationAssessment:
        //                var mswVisitDocument = new MSWPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //                var mswVisitStream = mswVisitDocument.GetStream();
        //                mswVisitStream.Position = 0;
        //                this.zipFile.AddEntry(zipEntryName, mswVisitStream);
        //                break;
        //            //case (int)DisciplineTasks.DriverOrTransportationNote:
        //            //    var transportationVisitDocument = new TransportationPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var transportationVisitStream = transportationVisitDocument.GetStream();
        //            //    transportationVisitStream.Position = 0;

        //            //    ZipEntry transportationEntry = this.zipFile.AddEntry(zipEntryName, transportationVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.HHAideVisit:
        //            //    var hhaVisitDocument = new HHAVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var hhaVisitStream = hhaVisitDocument.GetStream();
        //            //    hhaVisitStream.Position = 0;

        //            //    ZipEntry hhaEntry = this.zipFile.AddEntry(zipEntryName, hhaVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.HHAideCarePlan:
        //            //    var hhaCarePlanVisitDocument = new HHACarePlanPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var hhaCarePlanVisitStream = hhaCarePlanVisitDocument.GetStream();
        //            //    hhaCarePlanVisitStream.Position = 0;

        //            //    ZipEntry hhaCarePlanEntry = this.zipFile.AddEntry(zipEntryName, hhaCarePlanVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.PASTravel:
        //            //    var pasTravelVisitDocument = new PASTravelPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var pasTravelVisitStream = pasTravelVisitDocument.GetStream();
        //            //    pasTravelVisitStream.Position = 0;

        //            //    ZipEntry pasTravelEntry = this.zipFile.AddEntry(zipEntryName, pasTravelVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.PASVisit:
        //            //    var pasVisitDocument = new PASVisitPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var pasVisitStream = pasVisitDocument.GetStream();
        //            //    pasVisitStream.Position = 0;

        //            //    ZipEntry pasEntry = this.zipFile.AddEntry(zipEntryName, pasVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.PASCarePlan:
        //            //    var pasCarePlanVisitDocument = new PASCarePlanPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var pasCarePlanVisitStream = pasCarePlanVisitDocument.GetStream();
        //            //    pasCarePlanVisitStream.Position = 0;

        //            //    ZipEntry pasCarePlanEntry = this.zipFile.AddEntry(zipEntryName, pasCarePlanVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.HomeMakerNote:
        //            //    var HomeMakerNoteVisitDocument = new HomeMakerNotePdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var HomeMakerNoteVisitStream = HomeMakerNoteVisitDocument.GetStream();
        //            //    HomeMakerNoteVisitStream.Position = 0;

        //            //    ZipEntry HomeMakerNoteEntry = this.zipFile.AddEntry(zipEntryName, HomeMakerNoteVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.OTDischargeSummary:
        //            //    var otDischargeSummaryVisitDocument = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.OTDischargeSummary);
        //            //    var otDischargeSummaryVisitStream = otDischargeSummaryVisitDocument.GetStream();
        //            //    otDischargeSummaryVisitStream.Position = 0;

        //            //    ZipEntry otDischargeSummaryEntry = this.zipFile.AddEntry(zipEntryName, otDischargeSummaryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.PTDischargeSummary:
        //            //    var ptDischargeSummaryData = assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId);
        //            //    Stream ptDischargeSummaryVisitStream = null;
        //            //    if (ptDischargeSummaryData.Version == 2)
        //            //    {
        //            //        var ptDischargeSummaryVisitDocument = new DischargeSummaryPdf(ptDischargeSummaryData, PdfDocs.PTDischargeSummary2);
        //            //        ptDischargeSummaryVisitStream = ptDischargeSummaryVisitDocument.GetStream();
        //            //        ptDischargeSummaryVisitStream.Position = 0;
        //            //    }
        //            //    else
        //            //    {
        //            //        var ptDischargeSummaryVisitDocument = new DischargeSummaryPdf(ptDischargeSummaryData, PdfDocs.PTDischargeSummary);
        //            //        ptDischargeSummaryVisitStream = ptDischargeSummaryVisitDocument.GetStream();
        //            //        ptDischargeSummaryVisitStream.Position = 0;
        //            //    }

        //            //    ZipEntry ptDischargeSummaryEntry = this.zipFile.AddEntry(zipEntryName, ptDischargeSummaryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.STDischargeSummary:
        //            //    var stDischargeSummaryVisitDocument = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.STDischargeSummary);
        //            //    var stDischargeSummaryVisitStream = stDischargeSummaryVisitDocument.GetStream();
        //            //    stDischargeSummaryVisitStream.Position = 0;

        //            //    ZipEntry stDischargeSummaryEntry = this.zipFile.AddEntry(zipEntryName, stDischargeSummaryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.DischargeSummary:
        //            //    var dischargeSummaryVisitDocument = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId), PdfDocs.DischargeSummary);
        //            //    var dischargeSummaryVisitStream = dischargeSummaryVisitDocument.GetStream();
        //            //    dischargeSummaryVisitStream.Position = 0;

        //            //    ZipEntry dischargeSummaryEntry = this.zipFile.AddEntry(zipEntryName, dischargeSummaryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.PhysicianOrder:
        //            //    var physicianOrderVisitDocument = new PhysicianOrderPdf(patientService.GetOrderPrint(patientId, task.EventId, agencyId));
        //            //    var physicianOrderVisitStream = physicianOrderVisitDocument.GetStream();
        //            //    physicianOrderVisitStream.Position = 0;

        //            //    ZipEntry physicianOrderEntry = this.zipFile.AddEntry(zipEntryName, physicianOrderVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.HCFA485StandAlone:
        //            //case (int)DisciplineTasks.HCFA485:
        //            //    var planOfCareVisitDocument = new PlanOfCarePdf(assessmentService.GetPlanOfCarePrint(episodeId, patientId, task.EventId, agencyId));
        //            //    var planOfCareVisitStream = planOfCareVisitDocument.GetStream();
        //            //    planOfCareVisitStream.Position = 0;

        //            //    ZipEntry planOfCareEntry = this.zipFile.AddEntry(zipEntryName, planOfCareVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.NonOasisHCFA485:

        //            //    break;
        //            //case (int)DisciplineTasks.HCFA486:
        //            //case (int)DisciplineTasks.PostHospitalizationOrder:
        //            //case (int)DisciplineTasks.MedicaidPOC:
        //            //    break;
        //            //case (int)DisciplineTasks.IncidentAccidentReport:
        //            //    var incidentAccidentDocument = new IncidentReportPdf(agencyService.GetIncidentReportPrint(episodeId, patientId, task.EventId, agencyId));
        //            //    var incidentAccidentStream = incidentAccidentDocument.GetStream();
        //            //    incidentAccidentStream.Position = 0;

        //            //    ZipEntry incidentAccidentEntry = this.zipFile.AddEntry(zipEntryName, incidentAccidentStream);
        //            //    break;
        //            //case (int)DisciplineTasks.InfectionReport:
        //            //    var infectionDocument = new InfectionReportPdf(agencyService.GetInfectionReportPrint(episodeId, patientId, task.EventId, agencyId));
        //            //    var infectionStream = infectionDocument.GetStream();
        //            //    infectionStream.Position = 0;

        //            //    ZipEntry infectionEntry = this.zipFile.AddEntry(zipEntryName, infectionStream);
        //            //    break;
        //            //case (int)DisciplineTasks.SixtyDaySummary:
        //            //    var sixtyDaySummaryVisitDocument = new SixtyDaySummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var sixtyDaySummaryVisitStream = sixtyDaySummaryVisitDocument.GetStream();
        //            //    sixtyDaySummaryVisitStream.Position = 0;

        //            //    ZipEntry sixtyDaySummaryEntry = this.zipFile.AddEntry(zipEntryName, sixtyDaySummaryVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.TransferSummary:
        //            //case (int)DisciplineTasks.CoordinationOfCare:
        //            //    var coordinationOfCareVisitDocument = new TransferSummaryPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var coordinationOfCareVisitStream = coordinationOfCareVisitDocument.GetStream();
        //            //    coordinationOfCareVisitStream.Position = 0;

        //            //    ZipEntry coordinationOfCareEntry = this.zipFile.AddEntry(zipEntryName, coordinationOfCareVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.CommunicationNote:
        //            //    var comNoteVisitDocument = new ComNotePdf(patientService.GetCommunicationNotePrint(task.EventId, patientId, agencyId));
        //            //    var comNoteVisitStream = comNoteVisitDocument.GetStream();
        //            //    comNoteVisitStream.Position = 0;

        //            //    ZipEntry comNoteEntry = this.zipFile.AddEntry(zipEntryName, comNoteVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.FaceToFaceEncounter:
        //            //    var physFaceToFaceVisitDocument = new PhysFaceToFacePdf(patientService.GetFaceToFacePrint(patientId, task.EventId, agencyId));
        //            //    var physFaceToFaceVisitStream = physFaceToFaceVisitDocument.GetStream();
        //            //    physFaceToFaceVisitStream.Position = 0;

        //            //    ZipEntry physFaceToFaceEntry = this.zipFile.AddEntry(zipEntryName, physFaceToFaceVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.UAPWoundCareVisit:
        //            //    var uapWoundCareVisitDocument = new WoundCarePdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var uapWoundCareVisitStream = uapWoundCareVisitDocument.GetStream();
        //            //    uapWoundCareVisitStream.Position = 0;

        //            //    ZipEntry uapWoundCareEntry = this.zipFile.AddEntry(zipEntryName, uapWoundCareVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
        //            //    var uapInsulinVisitDocument = new UAPInsulinPdf(assessmentService.GetVisitNotePrint(agencyId, episodeId, patientId, task.EventId));
        //            //    var uapInsulinVisitStream = uapInsulinVisitDocument.GetStream();
        //            //    uapInsulinVisitStream.Position = 0;

        //            //    ZipEntry uapInsulinEntry = this.zipFile.AddEntry(zipEntryName, uapInsulinVisitStream);
        //            //    break;
        //            //case (int)DisciplineTasks.MedicareEligibilityReport:
        //            //    var eligibility = patientRepository.GetMedicareEligibility(agencyId, patientId, task.EventId);
        //            //    var patientEligibility = eligibility.Result.FromJson<PatientEligibility>();
        //            //    var medicareEligibilityDocument = new MedicareEligibilityPdf(patientEligibility, agencyRepository.GetWithBranches(agencyId), patientRepository.GetPatientOnly(patientId, agencyId));
        //            //    var medicareEligibilityStream = medicareEligibilityDocument.GetStream();
        //            //    medicareEligibilityStream.Position = 0;

        //            //    ZipEntry medicareEligibilityEntry = this.zipFile.AddEntry(zipEntryName, medicareEligibilityStream);
        //            //    break;
        //        }
        //}
    }
}
