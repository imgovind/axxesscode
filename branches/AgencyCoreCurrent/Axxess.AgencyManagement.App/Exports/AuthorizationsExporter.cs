﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AuthorizationsExporter : BaseExporter
    {
        private IList<Authorization> authorizations;
        public AuthorizationsExporter(IList<Authorization> authorizations) : base() {
            this.authorizations = authorizations;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Authorizations";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Authorizations");

            var normalFont = base.workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            dateStyle.Alignment = HorizontalAlignment.LEFT;
            dateStyle.SetFont(normalFont); 

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Authorizations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Authorization #1");
            headerRow.CreateCell(1).SetCellValue("Authorization #2");
            headerRow.CreateCell(2).SetCellValue("Authorization #3");
            headerRow.CreateCell(3).SetCellValue("Branch");
            headerRow.CreateCell(4).SetCellValue("Start Date");
            headerRow.CreateCell(5).SetCellValue("End Date");
            headerRow.CreateCell(6).SetCellValue("Status");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            headerRow.CreateCell(8).SetCellValue("SN Visit");
            headerRow.CreateCell(9).SetCellValue("PT Visit");
            headerRow.CreateCell(10).SetCellValue("OT Visit");
            headerRow.CreateCell(11).SetCellValue("ST Visit");
            headerRow.CreateCell(12).SetCellValue("MSW Visit");
            headerRow.CreateCell(13).SetCellValue("HHA Visit");
            headerRow.CreateCell(14).SetCellValue("Dietitian Visit");
            headerRow.CreateCell(15).SetCellValue("RN Visit");
            headerRow.CreateCell(16).SetCellValue("LVN Visit");

            int count = 2;
            if (this.authorizations.Count > 0) {
                
                this.authorizations.ForEach(a =>
                {
                    var dataRow = sheet.CreateRow(count);
                    dataRow.CreateCell(0).SetCellValue(a.Number1);
                    dataRow.CreateCell(1).SetCellValue(a.Number2);
                    dataRow.CreateCell(2).SetCellValue(a.Number3);
                    dataRow.CreateCell(3).SetCellValue(a.Branch);

                    if (a.StartDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(a.StartDate);
                    }
                    else
                    {
                        dataRow.CreateCell(4).SetCellValue(string.Empty);
                    }


                    if (a.EndDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(a.EndDate);
                    }
                    else
                    {
                        dataRow.CreateCell(5).SetCellValue(string.Empty);
                    }   

                    dataRow.CreateCell(6).SetCellValue(a.Status);
                    dataRow.CreateCell(7).SetCellValue(a.Insurance);
                    dataRow.CreateCell(8).SetCellValue(a.SNVisit.IsNotNullOrEmpty() ? a.SNVisit + " " + (a.SNVisitCountType == "1" ? "Visits" : string.Empty) + (a.SNVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(9).SetCellValue(a.PTVisit.IsNotNullOrEmpty() ? a.PTVisit + " " + (a.PTVisitCountType == "1" ? "Visits" : string.Empty) + (a.PTVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(10).SetCellValue(a.OTVisit.IsNotNullOrEmpty() ? a.OTVisit + " " + (a.OTVisitCountType == "1" ? "Visits" : string.Empty) + (a.OTVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(11).SetCellValue(a.STVisit.IsNotNullOrEmpty() ? a.STVisit + " " + (a.STVisitCountType == "1" ? "Visits" : string.Empty) + (a.STVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(12).SetCellValue(a.MSWVisit.IsNotNullOrEmpty() ? a.MSWVisit + " " + (a.MSWVisitCountType == "1" ? "Visits" : string.Empty) + (a.MSWVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(13).SetCellValue(a.HHAVisit.IsNotNullOrEmpty() ? a.HHAVisit + " " + (a.HHAVisitCountType == "1" ? "Visits" : string.Empty) + (a.HHAVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(14).SetCellValue(a.DieticianVisit.IsNotNullOrEmpty() ? a.DieticianVisit + " " + (a.DieticianVisitCountType == "1" ? "Visits" : string.Empty) + (a.DieticianVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(15).SetCellValue(a.RNVisit.IsNotNullOrEmpty() ? a.RNVisit + " " + (a.RNVisitCountType == "1" ? "Visits" : string.Empty) + (a.RNVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(16).SetCellValue(a.LVNVisit.IsNotNullOrEmpty() ? a.LVNVisit + " " + (a.LVNVisitCountType == "1" ? "Visits" : string.Empty) + (a.LVNVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    count++;
                });
                var totalRow = sheet.CreateRow(count + 2);
                var totalCell = totalRow.CreateCell(0);
                totalCell.SetCellValue(string.Format("Total Number Of Authorizations: {0}", authorizations.Count));
            }

            workBook.FinishWritingToExcelSpreadsheet(22);
        }
    }
}
