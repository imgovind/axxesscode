﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using NPOI.HPSF;

    public class UploadTypesExporter : BaseExporter
    {
        private IList<UploadType> types;
        public UploadTypesExporter(IList<UploadType> types)
            : base()
        {
            this.types = types;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Upload Types";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("AgencyUploadTypes");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(1);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Upload Types");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Type");
            headerRow.CreateCell(1).SetCellValue("Created Date");
            headerRow.CreateCell(2).SetCellValue("Modified Date");

            if (this.types.Count > 0)
            {
                int i = 2;
                this.types.ForEach(type =>
                {
                    var datarow = sheet.CreateRow(i);
                    datarow.CreateCell(0).SetCellValue(type.Type);
                    if (type.Created != DateTime.MinValue)
                    {
                        var createdDateCell = datarow.CreateCell(1);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(type.Created);
                    }
                    else
                    {
                        datarow.CreateCell(1).SetCellValue(string.Empty);
                    }
                    if (type.Modified != DateTime.MinValue)
                    {
                        var createdDateCell = datarow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(type.Modified);
                    }
                    else
                    {
                        datarow.CreateCell(3).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Agency Upload Types: {0}", types.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(4);
        }
    }
}
