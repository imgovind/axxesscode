﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;

    public class AccountViewData : JsonViewData
    {
        public string id { get; set; }
        public string userId { get; set; }
        public string agencyId { get; set; }
        public string email { get; set; }
        public bool isLocked { get; set; }
        public bool hasMultiple { get; set; }
        public string redirectUrl { get; set; }
        public bool isAccountInUse { get; set; }
    }
}
