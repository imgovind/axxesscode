﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class OpenOasis
    {
        public string Date { get; set; }
        public string Status { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public string AssessmentName { get; set; }
        public string CurrentlyAssigned { get; set; }
    }
}
