﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class UserSubscriptionPlanViewData
    {
        public UserSubscriptionPlanViewData()
        {
            this.Plans = new Dictionary<Guid,UserPlanViewData>();
        }
        public Dictionary<Guid, UserPlanViewData> Plans { get; set; }
    }

    public class UserPlanViewData
    {
        public int Max { get; set; }
        public int Count { get; set; }
        public string NextPlan { get; set; }
        public string CurrentPlan { get; set; }
    }
}
