﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    public class HiqhViewData
    {
        public string XmlResult { get; set; }
        public string MedicareNumber { get; set; }
        public  string LastName { get; set; } 
        public string FirstName { get; set; }
        public DateTime Dob { get; set; } 
        public string Gender { get; set; }
    }
}
