﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.LookUp.Domain;

    public class ClaimSupplyViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsSupplyNotBillable { get; set; }
        public List<Supply> BilledSupplies { get; set; }
        public List<Supply> UnbilledSupplies { get; set; }
        public List<Supply> ExcludedSupplies { get; set; }
    }
}
