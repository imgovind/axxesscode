﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class SubscriptionPlanViewData
    {
        public SubscriptionPlanViewData()
        {
            this.Plans = new Dictionary<Guid, LocationPlanViewData>();
        }
        public Dictionary<Guid, LocationPlanViewData> Plans { get; set; }
    }

    public class LocationPlanViewData
    {
        public Guid Id { get; set; }
        public int Max { get; set; }
        public int Count { get; set; }
        public int NextPlan { get; set; }
        public string NextPlanDescription { get; set; }
        public bool IsUserPlan { get; set; }
        public int CurrentPlan { get; set; }
        public string CurrentPlanDescription { get; set; }
    }
}
