﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using Axxess.Core.Infrastructure;

    public class OasisPlanOfCareJson : JsonViewData
    {
        public Guid episodeId { get; set; }
        public Guid patientId { get; set; }
        public Guid eventId { get; set; }
    }
}