﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    using LexiData;

    public class DrugDrugInteractionsViewData
    {
        public Patient Patient { get; set; }
        public Agency Agency { get; set; }
        public List<DrugDrugInteractionResult> DrugDrugInteractions { get; set; }
    }
}
