﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Text;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Api;
    using Axxess.AgencyManagement.App.ViewData;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Extensions;
    using Exports;

    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.App.Domain;

    public class AssessmentService : IAssessmentService
    {
        #region Constructor / Members

        private static readonly GrouperAgent grouperAgent = new GrouperAgent();
        private static readonly ValidationAgent validationAgent = new ValidationAgent();

        private readonly ILookupService lookupService;
        private readonly IUserRepository userRepository;
        private readonly IAssetService assetService;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IOasisCDataProvider oasisDataProvider;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentRepository oasisAssessmentRepository;

        public AssessmentService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILookupService lookupService, IAssetService assetService)
        {
            Check.Argument.IsNotNull(assetService, "assetService");
            Check.Argument.IsNotNull(lookupService, "lookupService");
            Check.Argument.IsNotNull(oasisDataProvider, "oasisDataProvider");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupService = lookupService;
            this.oasisDataProvider = oasisDataProvider;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.oasisAssessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.assetService = assetService;
        }

        #endregion

        #region IAssessmentService Members

        public OasisViewData SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles, string assessmentType)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            string action = "{0}_Action".FormatWith(assessmentType);
            string assessmentAction = formCollection.Get(action);
            if (assessmentAction.IsNotNullOrEmpty())
            {
                switch (assessmentAction)
                {
                    case "New":
                        oasisViewData = AddAssessment(formCollection, assessmentType);
                        break;
                    case "Edit":
                        oasisViewData = UpdateAssessment(formCollection, httpFiles, assessmentType);
                        break;
                }
            }
            return oasisViewData;
        }

        public Assessment AddAssessment(Patient patient,ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode)
        {
            Assessment assessment = AssessmentFactory.Create(((DisciplineTasks)oasisSchedule.DisciplineTask).ToString());
            if (assessment != null)
            {
                if (oasisSchedule.Version != 0)
                    assessment.Version = oasisSchedule.Version;
                assessment.Status = (int)ScheduleStatus.NoteNotYetDue;
                assessment.Id = oasisSchedule.EventId;
                assessment.AgencyId = Current.AgencyId;
                assessment.EpisodeId = episode.Id;
                assessment.UserId = oasisSchedule.UserId;
                assessment.PatientId = oasisSchedule.PatientId;
                assessment.AssessmentDate = DateTime.Parse(oasisSchedule.EventDate);
                assessment.Questions = ProcessPatientDemographics(patient, oasisSchedule, assessmentType, episode).Values.ToList();
                oasisDataProvider.OasisAssessmentRepository.Add(assessment);
                //  Auditor.Log(oasisSchedule.PatientId, oasisSchedule.EventId, Actions.Add, oasisSchedule.DisciplineTask.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline));
            }
            return assessment;
        }

        public Assessment AddAssessment(Patient patient,ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode, string medicationProfile)
        {
            Assessment assessment = AssessmentFactory.Create(((DisciplineTasks)oasisSchedule.DisciplineTask).ToString());
            if (assessment != null)
            {
                assessment.Status = (int)ScheduleStatus.OasisNotYetDue;
                assessment.Id = oasisSchedule.EventId;
                assessment.AgencyId = Current.AgencyId;
                assessment.EpisodeId = episode.Id;
                assessment.PatientId = oasisSchedule.PatientId;
                assessment.UserId = oasisSchedule.UserId;
                assessment.MedicationProfile = medicationProfile;
                assessment.AssessmentDate = DateTime.Parse(oasisSchedule.EventDate);
                assessment.Questions = ProcessPatientDemographics(patient, oasisSchedule, assessmentType, episode).Values.ToList();
                oasisDataProvider.OasisAssessmentRepository.Add(assessment);
                //Auditor.Log(oasisSchedule.PatientId, oasisSchedule.EventId, Actions.Add, oasisSchedule.DisciplineTask.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline));
            }
            return assessment;
        }

        public bool MarkAsDeleted(Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated)
        {
            return oasisDataProvider.OasisAssessmentRepository.MarkAsDeleted(Current.AgencyId, assessmentId, episodeId, patientId, assessmentType, isDeprecated);

        }

        public bool ReassignUser(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, string taskName)
        {
            return oasisDataProvider.OasisAssessmentRepository.ReassignUser(Current.AgencyId, episodeId, patientId, eventId, employeeId, taskName);
        }

        public Assessment GetAssessment(Guid assessmentId, string assessmentType)
        {
            return oasisDataProvider.OasisAssessmentRepository.Get(assessmentId, assessmentType, Current.AgencyId);
        }

        public Assessment GetAssessment(Guid episodeId, Guid patientId, Guid eventId, string Type)
        {
            return  oasisAssessmentRepository.Get(eventId, patientId, episodeId, Type, Current.AgencyId);
        }

        public Assessment GetAssessmentWithDisciplineTask(Guid episodeId, Guid patientId, Guid eventId, int disciplineTask)
        {
            var type = ((DisciplineTasks)disciplineTask).ToString();
            return GetAssessment(episodeId, patientId, eventId, type);
        }

        public Assessment GetAssessmentWithScheduleType(Guid assessmentId, string assessmentType)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentId, assessmentType, Current.AgencyId);
            if (assessment != null)
            {
                assessment.ShowOasisVendorButton = Current.OasisVendorExist;
                var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, assessment.EpisodeId, assessment.PatientId, assessment.Id);
                if (scheduleEvent != null)
                {
                    assessment.Status = scheduleEvent.Status.IsNotNullOrEmpty() && scheduleEvent.Status.IsInteger() ? scheduleEvent.Status.ToInteger() : assessment.Status;
                    if (scheduleEvent.EventDate.IsValidDate())
                    {
                        assessment.ScheduleDate = scheduleEvent.EventDate.ToDateTime();
                    }
                    if (scheduleEvent.VisitDate.IsValidDate())
                    {
                        assessment.VisitDate = scheduleEvent.VisitDate.ToDateTime();
                    }
                    assessment.Version = scheduleEvent.Version;
                    if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                    {
                        switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
                        {
                            case DisciplineTasks.OASISCStartofCare:
                            case DisciplineTasks.SNAssessment:
                            case DisciplineTasks.NonOASISStartofCare:
                            case DisciplineTasks.OASISCResumptionofCare:
                            case DisciplineTasks.OASISCFollowUp:
                            case DisciplineTasks.OASISCRecertification:
                            case DisciplineTasks.SNAssessmentRecert:
                            case DisciplineTasks.NonOASISRecertification:
                            case DisciplineTasks.OASISCTransfer:
                            case DisciplineTasks.OASISCTransferDischarge:
                            case DisciplineTasks.OASISCDeath:
                            case DisciplineTasks.OASISCDischarge:
                            case DisciplineTasks.NonOASISDischarge:
                                assessment.Discipline = "Nursing";
                                break;
                            case DisciplineTasks.OASISCStartofCarePT:
                            case DisciplineTasks.OASISCResumptionofCarePT:
                            case DisciplineTasks.OASISCFollowupPT:
                            case DisciplineTasks.OASISCRecertificationPT:
                            case DisciplineTasks.OASISCTransferPT:
                            case DisciplineTasks.OASISCDeathPT:
                            case DisciplineTasks.OASISCDischargePT:
                            case DisciplineTasks.OASISCTransferDischargePT:
                                assessment.Discipline = "PT";
                                break;
                            case DisciplineTasks.OASISCStartofCareOT:
                            case DisciplineTasks.OASISCResumptionofCareOT:
                            case DisciplineTasks.OASISCFollowupOT:
                            case DisciplineTasks.OASISCRecertificationOT:
                            case DisciplineTasks.OASISCTransferOT:
                            case DisciplineTasks.OASISCDeathOT:
                            case DisciplineTasks.OASISCDischargeOT:
                                assessment.Discipline = "OT";
                                break;
                            case DisciplineTasks.OASISCDischargeST:
                            case DisciplineTasks.OASISCRecertificationST:
                                assessment.Discipline = "ST";
                                break;
                        }
                    }
                    else
                    {
                        assessment.Discipline = scheduleEvent.Discipline;
                    }
                }
            }
            return assessment;
        }

        public string GetOasisSubmissionFormatNew(IDictionary<string, Question> assessmentQuestions, int versionNumber, AgencyLocation patientLocation)
        {
            var submissionGuide = GetOasisSubmissionFormatInstructionsNew();
            if (submissionGuide != null && submissionGuide.Count > 0)
            {
                var submissionFormat = new StringBuilder();
                submissionFormat.Capacity = 1446;
                string type = assessmentQuestions.AnswerOrEmptyString("M0100AssessmentType");
                submissionFormat.Append("B1"); //REC_ID
                submissionFormat.Append(string.Empty.PadRight(2)); //REC_TYPE
                submissionFormat.Append(string.Empty.PadRight(8)); //LOCK_DATE
                submissionFormat.Append(versionNumber.ToString().PadLeft(2, '0')); //CORRECTION_NUM submissionFormat.Append("00"); //
                submissionFormat.Append(string.Empty.PadRight(8)); //ACY_DOC_CD
                submissionFormat.Append("C-072009".PadRight(12)); //VERSION_CD1
                submissionFormat.Append("02.00".PadRight(5)); //VERSION_CD2
                submissionFormat.Append("364649717".PadRight(9)); //SFTW_ID
                submissionFormat.Append("1.0".PadRight(5)); //SFT_VER
                if (patientLocation != null && patientLocation.HomeHealthAgencyId.IsNotNullOrEmpty()) //HHA_AGENCY_ID
                {
                    submissionFormat.Append(patientLocation.HomeHealthAgencyId.Trim().ToUpper().PartOfString(0, 16).PadRight(16));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(16));
                }
                submissionFormat.Append(string.Empty.PadRight(14)); //PAT_ID
                submissionFormat.Append(string.Empty.PadRight(2));  //ST_CODE
                submissionFormat.Append(string.Empty.PadRight(4));  //ST_ERR_CNT
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_PMT_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_KEY_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //ST_DELETE
                submissionFormat.Append(string.Empty.PadRight(1));  //MC_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //MC_PMT_COR
                submissionFormat.Append(string.Empty.PadRight(1));  //MC_KEY_COR
                submissionFormat.Append(string.Empty.PadRight(20)); //MASK_VERSION_CD
                submissionFormat.Append(string.Empty.PadRight(7));  //CNT_FILLER

                if (patientLocation != null && patientLocation.MedicareProviderNumber.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(patientLocation.MedicareProviderNumber.Trim().ToUpper().PartOfString(0, 6).PadRight(6));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(6));
                }

                submissionFormat.Append(string.Empty.PadRight(15)); //M0014_BRANCH_STATE

                if (submissionGuide.ContainsKey("M0014_BRANCH_STATE") && patientLocation != null && patientLocation.AddressStateCode.IsNotNullOrEmpty()) //M0014_BRANCH_STATE
                {
                    submissionFormat.Append(patientLocation.AddressStateCode.Trim().ToUpper().PartOfString(0, 2).PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M0016_BRANCH_ID") && patientLocation != null && patientLocation.BranchId.IsNotNullOrEmpty()) //M0016_BRANCH_ID
                {
                    if (patientLocation.BranchId.Trim().IsEqual("N") || patientLocation.BranchId.Trim().IsEqual("P"))
                    {
                        submissionFormat.Append(patientLocation.BranchId.Trim().ToUpperCase().PartOfString(0, 1).PadRight(1));
                        submissionFormat.Append(string.Empty.PadRight(9));
                    }
                    else if (patientLocation.BranchId.Trim().IsEqual("Other") && patientLocation.BranchIdOther.IsNotNullOrEmpty() && patientLocation.BranchIdOther.Length == 10)
                    {
                        submissionFormat.Append(patientLocation.BranchIdOther.Trim().ToUpper().PartOfString(0, 10).PadRight(10));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append("N");
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("M0020_PAT_ID") && assessmentQuestions.ContainsKey("M0020PatientIdNumber") && assessmentQuestions["M0020PatientIdNumber"].Answer.IsNotNullOrEmpty()) //M0020_PAT_ID
                {
                    submissionFormat.Append(assessmentQuestions["M0020PatientIdNumber"].Answer.Trim().ToUpper().PartOfString(0, 20).PadRight(20));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("M0030_START_CARE_DT") && assessmentQuestions.ContainsKey("M0030SocDate") && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0030SocDate"].Answer.IsValidDate()) //M0030_START_CARE_DT
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));

                }

                if ((submissionGuide.ContainsKey("M0032_ROC_DT_NA") && assessmentQuestions.ContainsKey("M0032ROCDateNotApplicable") && !assessmentQuestions["M0032ROCDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0032_ROC_DT_NA") && !assessmentQuestions.ContainsKey("M0032ROCDateNotApplicable"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0032_ROC_DT") && assessmentQuestions.ContainsKey("M0032ROCDate") && assessmentQuestions["M0032ROCDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0032ROCDate"].Answer.IsValidDate()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0032ROCDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                    submissionFormat.Append("1".PadRight(1));
                }
                if (submissionGuide.ContainsKey("M0040_PAT_FNAME") && assessmentQuestions.ContainsKey("M0040FirstName") && assessmentQuestions["M0040FirstName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_FNAME
                {

                    submissionFormat.Append(assessmentQuestions["M0040FirstName"].Answer.Trim().ToUpper().PartOfString(0, 12).PadRight(12));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(12));
                }

                if (submissionGuide.ContainsKey("M0040_PAT_MI") && assessmentQuestions.ContainsKey("M0040MI") && assessmentQuestions["M0040MI"].Answer.IsNotNullOrEmpty()) //M0040_PAT_MI
                {

                    submissionFormat.Append(assessmentQuestions["M0040MI"].Answer.Trim().ToUpper().PartOfString(0, 1).PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (submissionGuide.ContainsKey("M0040_PAT_LNAME") && assessmentQuestions.ContainsKey("M0040LastName") && assessmentQuestions["M0040LastName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_LNAME
                {

                    submissionFormat.Append(assessmentQuestions["M0040LastName"].Answer.Trim().ToUpper().PartOfString(0, 18).PadRight(18));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(18));
                }

                if (submissionGuide.ContainsKey("M0040_PAT_SUFFIX") && assessmentQuestions.ContainsKey("M0040Suffix") && assessmentQuestions["M0040Suffix"].Answer.IsNotNullOrEmpty()) //M0040_PAT_SUFFIX
                {

                    submissionFormat.Append(assessmentQuestions["M0040Suffix"].Answer.Trim().ToUpper().PartOfString(0, 3).PadRight(3));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(3));
                }

                if (submissionGuide.ContainsKey("M0050_PAT_ST") && assessmentQuestions.ContainsKey("M0050PatientState") && assessmentQuestions["M0050PatientState"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {

                    submissionFormat.Append(assessmentQuestions["M0050PatientState"].Answer.Trim().ToUpper().PartOfString(0, 2).PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M0060_PAT_ZIP") && assessmentQuestions.ContainsKey("M0060PatientZipCode") && assessmentQuestions["M0060PatientZipCode"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {

                    submissionFormat.Append(assessmentQuestions["M0060PatientZipCode"].Answer.Trim().ToUpper().PartOfString(0, 11).PadRight(11));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if ((submissionGuide.ContainsKey("M0063_MEDICARE_NA") && assessmentQuestions.ContainsKey("M0063PatientMedicareNumberUnknown") && !assessmentQuestions["M0063PatientMedicareNumberUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0032_ROC_DT_NA") && !assessmentQuestions.ContainsKey("M0063PatientMedicareNumberUnknown"))) // M0063_MEDICARE_NUM and M0063_MEDICARE_NA
                {

                    if (submissionGuide.ContainsKey("M0063_MEDICARE_NUM") && assessmentQuestions.ContainsKey("M0063PatientMedicareNumber") && assessmentQuestions["M0063PatientMedicareNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M0063PatientMedicareNumber"].Answer.Replace("-", "").Trim().ToUpper().PartOfString(0, 12).PadRight(12));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(12));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(12));
                    submissionFormat.Append("1".PadRight(1));
                }


                if ((submissionGuide.ContainsKey("M0064_SSN_UK") && assessmentQuestions.ContainsKey("M0064PatientSSNUnknown") && !assessmentQuestions["M0064PatientSSNUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0064_SSN_UK") && !assessmentQuestions.ContainsKey("M0064PatientSSNUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0064_SSN") && assessmentQuestions.ContainsKey("M0064PatientSSN") && assessmentQuestions["M0064PatientSSN"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M0064PatientSSN"].Answer.Trim().ToUpper().PartOfString(0, 9).PadRight(9));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(9));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                    submissionFormat.Append("1".PadRight(1));
                }

                if ((submissionGuide.ContainsKey("M0065_MEDICAID_NA") && assessmentQuestions.ContainsKey("M0065PatientMedicaidNumberUnknown") && !assessmentQuestions["M0065PatientMedicaidNumberUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0065_MEDICAID_NA") && !assessmentQuestions.ContainsKey("M0065PatientMedicaidNumberUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0065_MEDICAID_NUM") && assessmentQuestions.ContainsKey("M0065PatientMedicaidNumber") && assessmentQuestions["M0065PatientMedicaidNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M0065PatientMedicaidNumber"].Answer.Trim().ToUpper().PartOfString(0, 14).PadRight(14));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(14));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(14));
                    submissionFormat.Append("1".PadRight(1));
                }


                if (submissionGuide.ContainsKey("M0066_PAT_BIRTH_DT") && assessmentQuestions.ContainsKey("M0066PatientDoB") && assessmentQuestions["M0066PatientDoB"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0066PatientDoB"].Answer.IsValidDate()) //M0050_PAT_ST
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0066PatientDoB"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                }
                submissionFormat.Append(string.Empty.PadRight(1));

                if (submissionGuide.ContainsKey("M0069_PAT_GENDER") && assessmentQuestions.ContainsKey("M0069Gender") && assessmentQuestions["M0069Gender"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0069Gender"].Answer.Trim().ToUpper().PartOfString(0, 1).PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }



                if ((submissionGuide.ContainsKey("M0018_PHYSICIAN_UK") && assessmentQuestions.ContainsKey("M0018NationalProviderIdUnknown") && !assessmentQuestions["M0018NationalProviderIdUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0018_PHYSICIAN_UK") && !assessmentQuestions.ContainsKey("M0018NationalProviderIdUnknown")))
                {

                    if (submissionGuide.ContainsKey("M0018_PHYSICIAN_ID") && assessmentQuestions.ContainsKey("M0018NationalProviderId") && assessmentQuestions["M0018NationalProviderId"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(assessmentQuestions["M0018NationalProviderId"].Answer.Trim().ToUpper().PartOfString(0, 10).PadRight(10));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                    submissionFormat.Append("0".PadRight(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                    submissionFormat.Append("1".PadRight(1));
                }

                if (submissionGuide.ContainsKey("M0080_ASSESSOR_DISCIPLINE") && assessmentQuestions.ContainsKey("M0080DisciplinePerson") && assessmentQuestions["M0080DisciplinePerson"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0080DisciplinePerson"].Answer.Trim().ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M0090_INFO_COMPLETED_DT") && assessmentQuestions.ContainsKey("M0090AssessmentCompleted") && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsValidDate()) //M0050_PAT_ST
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0090AssessmentCompleted"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadLeft(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }

                if (submissionGuide.ContainsKey("M0100_ASSMT_REASON") && assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0100AssessmentType"].Answer.Trim().ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M0140_ETHNIC_AI_AN") && assessmentQuestions.ContainsKey("M0140RaceAMorAN") && assessmentQuestions["M0140RaceAMorAN"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceAMorAN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_ASIAN") && assessmentQuestions.ContainsKey("M0140RaceAsia") && assessmentQuestions["M0140RaceAsia"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceAsia"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_BLACK") && assessmentQuestions.ContainsKey("M0140RaceBalck") && assessmentQuestions["M0140RaceBalck"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceBalck"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_HISP") && assessmentQuestions.ContainsKey("M0140RaceHispanicOrLatino") && assessmentQuestions["M0140RaceHispanicOrLatino"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceHispanicOrLatino"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_NH_PI") && assessmentQuestions.ContainsKey("M0140RaceNHOrPI") && assessmentQuestions["M0140RaceNHOrPI"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceNHOrPI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M0140_ETHNIC_WHITE") && assessmentQuestions.ContainsKey("M0140RaceWhite") && assessmentQuestions["M0140RaceWhite"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0140RaceWhite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                submissionFormat.Append(string.Empty.PadLeft(1));


                if (submissionGuide.ContainsKey("M0150_CPAY_NONE") && assessmentQuestions.ContainsKey("M0150PaymentSourceNone") && assessmentQuestions["M0150PaymentSourceNone"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceNone"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCARE_FFS") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCREFFS") && assessmentQuestions["M0150PaymentSourceMCREFFS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCREFFS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCARE_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCREHMO") && assessmentQuestions["M0150PaymentSourceMCREHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCREHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCAID_FFS") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCAIDFFS") && assessmentQuestions["M0150PaymentSourceMCAIDFFS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCAIDFFS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_MCAID_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourceMACIDHMO") && assessmentQuestions["M0150PaymentSourceMACIDHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMACIDHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }


                if (submissionGuide.ContainsKey("M0150_CPAY_WRKCOMP") && assessmentQuestions.ContainsKey("M0150PaymentSourceWRKCOMP") && assessmentQuestions["M0150PaymentSourceWRKCOMP"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceWRKCOMP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }


                if (submissionGuide.ContainsKey("M0150_CPAY_TITLEPGMS") && assessmentQuestions.ContainsKey("M0150PaymentSourceTITLPRO") && assessmentQuestions["M0150PaymentSourceTITLPRO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceTITLPRO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_OTH_GOVT") && assessmentQuestions.ContainsKey("M0150PaymentSourceOTHGOVT") && assessmentQuestions["M0150PaymentSourceOTHGOVT"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceOTHGOVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_PRIV_INS") && assessmentQuestions.ContainsKey("M0150PaymentSourcePRVINS") && assessmentQuestions["M0150PaymentSourcePRVINS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourcePRVINS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_PRIV_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourcePRVHMO") && assessmentQuestions["M0150PaymentSourcePRVHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourcePRVHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0150_CPAY_SELFPAY") && assessmentQuestions.ContainsKey("M0150PaymentSourceSelfPay") && assessmentQuestions["M0150PaymentSourceSelfPay"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceSelfPay"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }


                if (submissionGuide.ContainsKey("M0150_CPAY_OTHER") && assessmentQuestions.ContainsKey("M0150PaymentSourceOtherSRS") && assessmentQuestions["M0150PaymentSourceOtherSRS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceOtherSRS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }
                if (type.Contains("03") || type.Contains("01"))
                {

                    if (submissionGuide.ContainsKey("M0150_CPAY_UK") && assessmentQuestions.ContainsKey("M0150PaymentSourceUnknown") && assessmentQuestions["M0150PaymentSourceUnknown"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0150PaymentSourceUnknown"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadRight(6));
                submissionFormat.Append(string.Empty.PadRight(5));
                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }
                    else
                    {
                        if ((submissionGuide.ContainsKey("M1005_INP_DSCHG_UNKNOWN") && assessmentQuestions.ContainsKey("M1005InpatientDischargeDateUnknown") && !assessmentQuestions["M1005InpatientDischargeDateUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M1005_INP_DSCHG_UNKNOWN") && !assessmentQuestions.ContainsKey("M1005InpatientDischargeDateUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                        {

                            if (submissionGuide.ContainsKey("M1005_INP_DISCHARGE_DT") && assessmentQuestions.ContainsKey("M1005InpatientDischargeDate") && assessmentQuestions["M1005InpatientDischargeDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1005InpatientDischargeDate"].Answer.IsValidDate()) //M0010_CCN
                            {
                                submissionFormat.Append(DateTime.Parse(assessmentQuestions["M1005InpatientDischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(8));
                            }
                            submissionFormat.Append("0".PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                            submissionFormat.Append("1".PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                    {
                        submissionFormat.Append(string.Empty.PadLeft(14));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP1_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode1") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP2_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode2") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                submissionFormat.Append(string.Empty.PadLeft(1));

                if (type.Contains("03") || type.Contains("01"))
                {
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD_NA") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(28));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD1") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode1") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD2") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode2") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD3") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode3") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD4") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode4") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }

                    }

                    if (submissionGuide.ContainsKey("M1018_PRIOR_UNKNOWN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUK")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000001");
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NOCHG_14D") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNA"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000010");
                            }
                            else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append("000000100");
                                }
                                else
                                {
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                    {
                                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                        {
                                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                        }
                                        else
                                        {
                                            submissionFormat.Append("0");
                                        }
                                    }
                                    else
                                    {
                                        submissionFormat.Append(string.Empty.PadLeft(1));
                                    }
                                    submissionFormat.Append("0");
                                    submissionFormat.Append("0");
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append(string.Empty.PadLeft(1));
                                submissionFormat.Append("0");
                                submissionFormat.Append("0");
                            }
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000100");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                                submissionFormat.Append(string.Empty.PadLeft(1));
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");

                        }
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NOCHG_14D") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNA"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000010");
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000100");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                submissionFormat.Append("0");
                                submissionFormat.Append("0");
                                submissionFormat.Append(string.Empty.PadLeft(1));

                            }

                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000100");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(28));
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1020ICD9M"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1020ICD9M"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        if (!assessmentQuestions["M1020ICD9M"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_SEVERITY") && assessmentQuestions.ContainsKey("M1020SymptomControlRating") && assessmentQuestions["M1020SymptomControlRating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1020SymptomControlRating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }


                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M1") && assessmentQuestions["M1022ICD9M1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose1Rating") && assessmentQuestions["M1022OtherDiagnose1Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose1Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }


                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M2") && assessmentQuestions["M1022ICD9M2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        if (!assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().Trim().StartsWith("V"))
                        {

                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose2Rating") && assessmentQuestions["M1022OtherDiagnose2Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose2Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M3") && assessmentQuestions["M1022ICD9M3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {

                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose3Rating") && assessmentQuestions["M1022OtherDiagnose3Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose3Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M4") && assessmentQuestions["M1022ICD9M4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose4Rating") && assessmentQuestions["M1022OtherDiagnose4Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose4Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M5") && assessmentQuestions["M1022ICD9M5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().Trim().StartsWith("E"))
                        {
                            submissionFormat.Append(string.Format("{0}", assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().PadRight(7, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }

                        if (!assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().Trim().StartsWith("V"))
                        {
                            if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose5Rating") && assessmentQuestions["M1022OtherDiagnose5Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose5Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(9));
                    }

                    if (submissionGuide.ContainsKey("M1030_THH_NONE_ABOVE") && assessmentQuestions.ContainsKey("M1030HomeTherapiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1030HomeTherapiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1030_THH_IV_INFUSION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesInfusion")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1030_THH_PAR_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesParNutrition")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1030_THH_ENT_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesEntNutrition")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1030_THH_IV_INFUSION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesInfusion")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1030_THH_PAR_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesParNutrition")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1030_THH_ENT_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesEntNutrition")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(54));
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }

                submissionFormat.Append(string.Empty.PadLeft(6));

                if (type.Contains("01") || type.Contains("03"))
                {

                    if (submissionGuide.ContainsKey("M1036_RSK_NONE") && assessmentQuestions.ContainsKey("M1036RiskFactorsNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1036RiskFactorsNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000010");
                        }
                        else if (submissionGuide.ContainsKey("M1036_RSK_UNKNOWN") && assessmentQuestions.ContainsKey("M1036RiskFactorsUnknown"))
                        {
                            if (assessmentQuestions["M1036RiskFactorsUnknown"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000001");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("00");
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                    }
                    else if (submissionGuide.ContainsKey("M1036_RSK_UNKNOWN") && assessmentQuestions.ContainsKey("M1036RiskFactorsUnknown"))
                    {
                        if (assessmentQuestions["M1036RiskFactorsUnknown"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                submissionFormat.Append(string.Empty.PadLeft(55));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M1200_VISION") && assessmentQuestions.ContainsKey("M1200Vision") && assessmentQuestions["M1200Vision"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1200Vision"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1230_SPEECH") && assessmentQuestions.ContainsKey("M1230SpeechAndOral") && assessmentQuestions["M1230SpeechAndOral"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1230SpeechAndOral"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1322_NBR_PRSULC_STG1") && assessmentQuestions.ContainsKey("M1322CurrentNumberStageIUlcer") && assessmentQuestions["M1322CurrentNumberStageIUlcer"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1322CurrentNumberStageIUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(7));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1324_STG_PRBLM_ULCER") && assessmentQuestions.ContainsKey("M1324MostProblematicUnhealedStage") && assessmentQuestions["M1324MostProblematicUnhealedStage"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1324MostProblematicUnhealedStage"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                submissionFormat.Append(string.Empty.PadLeft(14));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1400_WHEN_DYSPNEIC") && assessmentQuestions.ContainsKey("M1400PatientDyspneic") && assessmentQuestions["M1400PatientDyspneic"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1400PatientDyspneic"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1410_RESPTX_NONE") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1410HomeRespiratoryTreatmentsNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1410_RESPTX_OXYGEN") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1410_RESPTX_VENTILATOR") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1410_RESPTX_AIRPRESS") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1410_RESPTX_OXYGEN") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1410_RESPTX_VENTILATOR") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1410_RESPTX_AIRPRESS") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1600_UTI") && assessmentQuestions.ContainsKey("M1600UrinaryTractInfection") && assessmentQuestions["M1600UrinaryTractInfection"].Answer.IsNotNullOrEmpty()) //M1600_UTI
                    {
                        submissionFormat.Append(assessmentQuestions["M1600UrinaryTractInfection"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1610UrinaryIncontinence"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                submissionFormat.Append(string.Empty.PadLeft(2));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1620_BWL_INCONT") && assessmentQuestions.ContainsKey("M1620BowelIncontinenceFrequency") && assessmentQuestions["M1620BowelIncontinenceFrequency"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1620BowelIncontinenceFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M1630_OSTOMY") && assessmentQuestions.ContainsKey("M1630OstomyBowelElimination") && assessmentQuestions["M1630OstomyBowelElimination"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1630OstomyBowelElimination"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1700_COG_FUNCTION") && assessmentQuestions.ContainsKey("M1700CognitiveFunctioning") && assessmentQuestions["M1700CognitiveFunctioning"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1700CognitiveFunctioning"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1710_WHEN_CONFUSED") && assessmentQuestions.ContainsKey("M1710WhenConfused") && assessmentQuestions["M1710WhenConfused"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1710WhenConfused"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1720_WHEN_ANXIOUS") && assessmentQuestions.ContainsKey("M1720WhenAnxious") && assessmentQuestions["M1720WhenAnxious"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1720WhenAnxious"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(13));

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1740_BD_NONE") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0000001");
                        }

                        else
                        {
                            if (submissionGuide.ContainsKey("M1740_BD_MEM_DEFICIT") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_IMP_DECISN") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_VERBAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_PHYSICAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_SOC_INAPPRO") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1740_BD_DELUSIONS") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1740_BD_MEM_DEFICIT") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_IMP_DECISN") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_VERBAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_PHYSICAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_SOC_INAPPRO") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_DELUSIONS") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }


                    if (submissionGuide.ContainsKey("M1745_BEH_PROB_FREQ") && assessmentQuestions.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && assessmentQuestions["M1745DisruptiveBehaviorSymptomsFrequency"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1745DisruptiveBehaviorSymptomsFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1750_REC_PSYCH_NURS") && assessmentQuestions.ContainsKey("M1750PsychiatricNursingServicing") && assessmentQuestions["M1750PsychiatricNursingServicing"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1750PsychiatricNursingServicing"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));


                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1800_CUR_GROOMING") && assessmentQuestions.ContainsKey("M1800Grooming") && assessmentQuestions["M1800Grooming"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1800Grooming"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1810_CUR_DRESS_UPPER") && assessmentQuestions.ContainsKey("M1810CurrentAbilityToDressUpper") && assessmentQuestions["M1810CurrentAbilityToDressUpper"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1810CurrentAbilityToDressUpper"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1820_CUR_DRESS_LOWER") && assessmentQuestions.ContainsKey("M1820CurrentAbilityToDressLower") && assessmentQuestions["M1820CurrentAbilityToDressLower"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1820CurrentAbilityToDressLower"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(18));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1870_CUR_FEEDING") && assessmentQuestions.ContainsKey("M1870FeedingOrEating") && assessmentQuestions["M1870FeedingOrEating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1870FeedingOrEating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1880_CUR_PREP_LT_MEALS") && assessmentQuestions.ContainsKey("M1880AbilityToPrepareLightMeal") && assessmentQuestions["M1880AbilityToPrepareLightMeal"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1880AbilityToPrepareLightMeal"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(18));
                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1890_CUR_PHONE_USE") && assessmentQuestions.ContainsKey("M1890AbilityToUseTelephone") && assessmentQuestions["M1890AbilityToUseTelephone"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1890AbilityToUseTelephone"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(21));
                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2310_ECR_MEDICATION") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMed")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_MEDICATION") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMed")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));
                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2310_ECR_HYPOGLYC") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHypo")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.IsNotNullOrEmpty())
                                    {

                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_HYPOGLYC") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHypo")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.IsNotNullOrEmpty())
                                {

                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    //if (type.Contains("09"))
                    //{
                    //    submissionFormat.Append("NA");
                    //}
                    //else
                    //{
                        if (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                   // }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(7));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_MED") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMed")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_MED") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMed")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(3));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_HYPOGLYC") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHypo")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_HYPOGLYC") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHypo")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_UR_TRACT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUrinaryInf")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_UR_TRACT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUrinaryInf")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(1));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("00");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDVT")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDVT")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "01") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2440_NH_UNKNOWN") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnknown")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedUnknown"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0000001");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2440_NH_THERAPY") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedTherapy")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_RESPITE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedRespite")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_HOSPICE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedHospice")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_PERMANENT") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedPermanent")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_UNSAFE_HOME") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnsafe")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2440_NH_OTHER") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedOther")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M2440_NH_THERAPY") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedTherapy")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_RESPITE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedRespite")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_HOSPICE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedHospice")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_PERMANENT") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedPermanent")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_UNSAFE_HOME") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnsafe")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_OTHER") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedOther")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("08") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M0903_LAST_HOME_VISIT") && assessmentQuestions.ContainsKey("M0903LastHomeVisitDate") && assessmentQuestions["M0903LastHomeVisitDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0903LastHomeVisitDate"].Answer.IsValidDate()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0903LastHomeVisitDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    if (submissionGuide.ContainsKey("M0906_DC_TRAN_DTH_DT") && assessmentQuestions.ContainsKey("M0906DischargeDate") && assessmentQuestions["M0906DischargeDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0906DischargeDate"].Answer.IsValidDate()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0906DischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }
                submissionFormat.Append(string.Empty.PadLeft(6));

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1000_DC_SNF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesSNF")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1000_DC_SNF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesSNF")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(2));
                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                submissionFormat.Append(string.Empty.PadLeft(14));
                if (submissionGuide.ContainsKey("NATL_PROV_ID") && patientLocation.NationalProviderNumber.IsNotNullOrEmpty() && patientLocation.NationalProviderNumber.Length == 10) //M0050_PAT_ST
                {
                    submissionFormat.Append(patientLocation.NationalProviderNumber.ToUpper().PartOfString(0, 10).PadLeft(10));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    if (submissionGuide.ContainsKey("M0110_EPISODE_TIMING") && assessmentQuestions.ContainsKey("M0110EpisodeTiming") && assessmentQuestions["M0110EpisodeTiming"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M0110EpisodeTiming"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_A3") && assessmentQuestions.ContainsKey("M1024ICD9MA3") && assessmentQuestions["M1024ICD9MA3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MA3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M1") && assessmentQuestions["M1022ICD9M1"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_B3") && assessmentQuestions.ContainsKey("M1024ICD9MB3") && assessmentQuestions["M1024ICD9MB3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MB3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M2") && assessmentQuestions["M1022ICD9M2"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_C3") && assessmentQuestions.ContainsKey("M1024ICD9MC3") && assessmentQuestions["M1024ICD9MC3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MC3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M3") && assessmentQuestions["M1022ICD9M3"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_D3") && assessmentQuestions.ContainsKey("M1024ICD9MD3") && assessmentQuestions["M1024ICD9MD3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MD3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M4") && assessmentQuestions["M1022ICD9M4"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_E3") && assessmentQuestions.ContainsKey("M1024ICD9ME3") && assessmentQuestions["M1024ICD9ME3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9ME3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M5") && assessmentQuestions["M1022ICD9M5"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_F3") && assessmentQuestions.ContainsKey("M1024ICD9MF3") && assessmentQuestions["M1024ICD9MF3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MF3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1020ICD9M"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {

                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_A4") && assessmentQuestions.ContainsKey("M1024ICD9MA4") && assessmentQuestions["M1024ICD9MA4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MA4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M1") && assessmentQuestions["M1022ICD9M1"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M1"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_B4") && assessmentQuestions.ContainsKey("M1024ICD9MB4") && assessmentQuestions["M1024ICD9MB4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MB4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M2") && assessmentQuestions["M1022ICD9M2"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M2"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_C4") && assessmentQuestions.ContainsKey("M1024ICD9MC4") && assessmentQuestions["M1024ICD9MC4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MC4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M3") && assessmentQuestions["M1022ICD9M3"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M3"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_D4") && assessmentQuestions.ContainsKey("M1024ICD9MD4") && assessmentQuestions["M1024ICD9MD4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MD4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M4") && assessmentQuestions["M1022ICD9M4"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M4"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_E4") && assessmentQuestions.ContainsKey("M1024ICD9ME4") && assessmentQuestions["M1024ICD9ME4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9ME4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1022ICD9M5") && assessmentQuestions["M1022ICD9M5"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("V") && assessmentQuestions["M1022ICD9M5"].Answer.ToUpper().StartsWith("E")) //M0050_PAT_ST
                    {
                        if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_F4") && assessmentQuestions.ContainsKey("M1024ICD9MF4") && assessmentQuestions["M1024ICD9MF4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MF4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M2200_THER_NEED_NA") && assessmentQuestions.ContainsKey("M2200TherapyNeedNA") && assessmentQuestions["M2200TherapyNeedNA"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2200TherapyNeedNA"].Answer == "1")
                    {
                        submissionFormat.Append("000");
                        submissionFormat.Append("0");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2200_THER_NEED_NUM") && assessmentQuestions.ContainsKey("M2200NumberOfTherapyNeed") && assessmentQuestions["M2200NumberOfTherapyNeed"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2200NumberOfTherapyNeed"].Answer.PartOfString(0, 3).PadLeft(3, '0'));
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(3));
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(90));
                }
                submissionFormat.Append(string.Empty.PadLeft(1));
                if (type.Contains("01") || type.Contains("03"))
                {
                    if ((submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && !assessmentQuestions["M0102PhysicianOrderedDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && !assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                    {

                        if (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDate") && assessmentQuestions["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0102PhysicianOrderedDate"].Answer.IsValidDate()) //M0010_CCN
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0102PhysicianOrderedDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                        submissionFormat.Append("0");
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                        submissionFormat.Append("1");
                    }


                    if ((submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && !assessmentQuestions["M0102PhysicianOrderedDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && !assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable"))) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M0104_PHYSN_RFRL_DT") && assessmentQuestions.ContainsKey("M0104ReferralDate") && assessmentQuestions["M0104ReferralDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0104ReferralDate"].Answer.IsValidDate()) //M0010_CCN
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0104ReferralDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                    }

                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1000_DC_LTC_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTC")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_IPPS_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIPPS")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_LTCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTCH")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_IRF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIRF")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_PSYCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesPhych")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1000_DC_OTH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesOTHR")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {


                        if (submissionGuide.ContainsKey("M1000_DC_LTC_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTC")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_IPPS_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIPPS")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_LTCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTCH")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_IRF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIRF")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_PSYCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesPhych")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_OTH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesOTHR")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                    {
                        submissionFormat.Append(string.Empty.PadLeft(58));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP3_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode3") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP4_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode4") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP5_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode5") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1010_14_DAY_INP6_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode6") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode6"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode6"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1012_INP_NA_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCodeNotApplicable") && assessmentQuestions["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer == "1") //M0010_CCN
                        {
                            submissionFormat.Append(string.Empty.PadLeft(28));
                            submissionFormat.Append("10");
                        }
                        else if ((submissionGuide.ContainsKey("M1012_INP_UK_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCodeUnknown") && assessmentQuestions["M1012InpatientFacilityProcedureCodeUnknown"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1012InpatientFacilityProcedureCodeUnknown"].Answer == "1"))
                        {
                            submissionFormat.Append(string.Empty.PadLeft(28));
                            submissionFormat.Append("01");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR1_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode1") && assessmentQuestions["M1012InpatientFacilityProcedureCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode1"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR2_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode2") && assessmentQuestions["M1012InpatientFacilityProcedureCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode2"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR3_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode3") && assessmentQuestions["M1012InpatientFacilityProcedureCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode3"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            if (submissionGuide.ContainsKey("M1012_INP_PRCDR4_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode4") && assessmentQuestions["M1012InpatientFacilityProcedureCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                            {
                                submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode4"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(7));
                            }
                            submissionFormat.Append("00");
                        }
                    }
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD_NA") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(14));
                        submissionFormat.Append("1");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD5") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode5") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1016_CHGREG_ICD6") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode6") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode6"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode6"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        submissionFormat.Append("0");
                    }

                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_NONE_ABOVE") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskNone")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_RCNT_DCLN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskRecentDecline")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_MLTPL_HOSPZTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMultipleHosp")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_HSTRY_FALLS") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskHistoryOfFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_5PLUS_MDCTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMedications")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_FRAILTY") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskFrailty")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1032_HOSP_RISK_OTHR") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1032HospitalizationRiskOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_RCNT_DCLN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskRecentDecline")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_MLTPL_HOSPZTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMultipleHosp")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_HSTRY_FALLS") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskHistoryOfFall")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_5PLUS_MDCTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMedications")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_FRAILTY") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskFrailty")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_OTHR") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskOther")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskOther"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1034_PTNT_OVRAL_STUS") && assessmentQuestions.ContainsKey("M1034OverallStatus") && assessmentQuestions["M1034OverallStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1034OverallStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(6));
                    submissionFormat.Append(string.Empty.PadLeft(56));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(14));
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append(string.Empty.PadLeft(2));

                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1040InfluenzaVaccine"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if ((submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1040InfluenzaVaccine"].Answer == "01") || (submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1040InfluenzaVaccine"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1045_INFLNZ_RSN_NOT_RCVD") && assessmentQuestions.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && assessmentQuestions["M1045InfluenzaVaccineNotReceivedReason"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1045InfluenzaVaccineNotReceivedReason"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1050_PPV_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1050PneumococcalVaccine") && assessmentQuestions["M1050PneumococcalVaccine"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1050PneumococcalVaccine"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1050_PPV_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1050PneumococcalVaccine") && assessmentQuestions["M1050PneumococcalVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1050PneumococcalVaccine"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1055_PPV_RSN_NOT_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1055PPVNotReceivedReason") && assessmentQuestions["M1055PPVNotReceivedReason"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1055PPVNotReceivedReason"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1100_PTNT_LVG_STUTN") && assessmentQuestions.ContainsKey("M1100LivingSituation") && assessmentQuestions["M1100LivingSituation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1100LivingSituation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1210_HEARG_ABLTY") && assessmentQuestions.ContainsKey("M1210Hearing") && assessmentQuestions["M1210Hearing"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1210Hearing"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1220_UNDRSTG_VERBAL_CNTNT") && assessmentQuestions.ContainsKey("M1220VerbalContent") && assessmentQuestions["M1220VerbalContent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1220VerbalContent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1240_FRML_PAIN_ASMT") && assessmentQuestions.ContainsKey("M1240FormalPainAssessment") && assessmentQuestions["M1240FormalPainAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1240FormalPainAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1242_PAIN_FREQ_ACTVTY_MVMT") && assessmentQuestions.ContainsKey("M1242PainInterferingFrequency") && assessmentQuestions["M1242PainInterferingFrequency"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1242PainInterferingFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1300_PRSR_ULCR_RISK_ASMT") && assessmentQuestions.ContainsKey("M1300PressureUlcerAssessment") && assessmentQuestions["M1300PressureUlcerAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1300PressureUlcerAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1300_PRSR_ULCR_RISK_ASMT") && assessmentQuestions.ContainsKey("M1300PressureUlcerAssessment") && assessmentQuestions["M1300PressureUlcerAssessment"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1300PressureUlcerAssessment"].Answer == "00") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M1302_RISK_OF_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1302RiskDevelopingPressureUlcers") && assessmentQuestions["M1302RiskDevelopingPressureUlcers"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1302RiskDevelopingPressureUlcers"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                if (type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(10));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1307_OLDST_STG2_AT_DSCHRG") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcer") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer == "02") //M0010_CCN
                        {
                            if (submissionGuide.ContainsKey("M1307_OLDST_STG2_ONST_DT") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.IsValidDate()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.ToDateTime().ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(8));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                        if (submissionGuide.ContainsKey("M1307_OLDST_STG2_AT_DSCHRG") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcer") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG2") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_STG2_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG3") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_STG3_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG4") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NBR_STG4_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DRSG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DRSG_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_CVRG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_CVRG_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DEEP_TISUE") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1308_NSTG_DEEP_TISUE_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(14));
                    }
                    else
                    {
                        if ((submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG3") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer == "0") && (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG4") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer == "0") && (submissionGuide.ContainsKey("M1308_NSTG_CVRG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer == "0"))
                        {
                            submissionFormat.Append(string.Empty.PadLeft(12));
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M1310_PRSR_ULCR_LNGTH") && assessmentQuestions.ContainsKey("M1310PressureUlcerLength") && assessmentQuestions["M1310PressureUlcerLength"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1310PressureUlcerLength"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                                submissionFormat.Append(".");
                                if (assessmentQuestions.ContainsKey("M1310PressureUlcerLengthDecimal"))
                                {
                                    submissionFormat.Append(assessmentQuestions["M1310PressureUlcerLengthDecimal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1, '0'));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(4));
                            }
                            if (submissionGuide.ContainsKey("M1312_PRSR_ULCR_WDTH") && assessmentQuestions.ContainsKey("M1312PressureUlcerWidth") && assessmentQuestions["M1312PressureUlcerWidth"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1312PressureUlcerWidth"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                                submissionFormat.Append(".");
                                if (assessmentQuestions.ContainsKey("M1312PressureUlcerWidthDecimal"))
                                {
                                    submissionFormat.Append(assessmentQuestions["M1312PressureUlcerWidthDecimal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1, '0'));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(4));
                            }
                            if (submissionGuide.ContainsKey("M1314_PRSR_ULCR_DEPTH") && assessmentQuestions.ContainsKey("M1314PressureUlcerDepth") && assessmentQuestions["M1314PressureUlcerDepth"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M1314PressureUlcerDepth"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                                submissionFormat.Append(".");
                                if (assessmentQuestions.ContainsKey("M1314PressureUlcerDepthDecimal"))
                                {
                                    submissionFormat.Append(assessmentQuestions["M1314PressureUlcerDepthDecimal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1, '0'));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(4));
                            }

                        }
                        if (submissionGuide.ContainsKey("M1320_STUS_PRBLM_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1320MostProblematicPressureUlcerStatus") && assessmentQuestions["M1320MostProblematicPressureUlcerStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1320MostProblematicPressureUlcerStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1330StasisUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1330StasisUlcer"].Answer == "00") || (submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1330StasisUlcer"].Answer == "03")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(4));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1332_NUM_STAS_ULCR") && assessmentQuestions.ContainsKey("M1332CurrentNumberStasisUlcer") && assessmentQuestions["M1332CurrentNumberStasisUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1332CurrentNumberStasisUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                        if (submissionGuide.ContainsKey("M1334_STUS_PRBLM_STAS_ULCR") && assessmentQuestions.ContainsKey("M1334StasisUlcerStatus") && assessmentQuestions["M1334StasisUlcerStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1334StasisUlcerStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1340SurgicalWound"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1340SurgicalWound"].Answer == "00") || (submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1340SurgicalWound"].Answer == "02")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1342_STUS_PRBLM_SRGCL_WND") && assessmentQuestions.ContainsKey("M1342SurgicalWoundStatus") && assessmentQuestions["M1342SurgicalWoundStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1342SurgicalWoundStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                    if (submissionGuide.ContainsKey("M1350_LESION_OPEN_WND") && assessmentQuestions.ContainsKey("M1350SkinLesionOpenWound") && assessmentQuestions["M1350SkinLesionOpenWound"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1350SkinLesionOpenWound"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1500HeartFailureSymptons"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if ((submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "00") || (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "02") || (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(6));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_NO_ACTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupNoAction")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupNoAction"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("100000");
                            }
                            else
                            {
                                submissionFormat.Append("0");
                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_CNTCT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupPhysicianCon")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_ER_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupAdvisedEmg")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupParameters")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CLNCL_INTRVTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupInterventions")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CARE_PLAN_CHG") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupChange")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_CNTCT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupPhysicianCon")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_ER_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupAdvisedEmg")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupParameters")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CLNCL_INTRVTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupInterventions")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CARE_PLAN_CHG") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupChange")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1610UrinaryIncontinence"].Answer == "00") || (submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1610UrinaryIncontinence"].Answer == "02")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1615_INCNTNT_TIMING") && assessmentQuestions.ContainsKey("M1615UrinaryIncontinenceOccur") && assessmentQuestions["M1615UrinaryIncontinenceOccur"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M1615UrinaryIncontinenceOccur"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1730_STDZ_DPRSN_SCRNG") && assessmentQuestions.ContainsKey("M1730DepressionScreening") && assessmentQuestions["M1730DepressionScreening"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1730DepressionScreening"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1730_PHQ2_LACK_INTRST") && assessmentQuestions.ContainsKey("M1730DepressionScreeningInterest") && assessmentQuestions["M1730DepressionScreeningInterest"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1730DepressionScreeningInterest"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1730_PHQ2_DPRSN") && assessmentQuestions.ContainsKey("M1730DepressionScreeningHopeless") && assessmentQuestions["M1730DepressionScreeningHopeless"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1730DepressionScreeningHopeless"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1830_CRNT_BATHG") && assessmentQuestions.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && assessmentQuestions["M1830CurrentAbilityToBatheEntireBody"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1830CurrentAbilityToBatheEntireBody"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1840_CUR_TOILTG") && assessmentQuestions.ContainsKey("M1840ToiletTransferring") && assessmentQuestions["M1840ToiletTransferring"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1840ToiletTransferring"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1845_CUR_TOILTG_HYGN") && assessmentQuestions.ContainsKey("M1845ToiletingHygiene") && assessmentQuestions["M1845ToiletingHygiene"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1845ToiletingHygiene"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }


                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M1850_CUR_TRNSFRNG") && assessmentQuestions.ContainsKey("M1850Transferring") && assessmentQuestions["M1850Transferring"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1850Transferring"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1860_CRNT_AMBLTN") && assessmentQuestions.ContainsKey("M1860AmbulationLocomotion") && assessmentQuestions["M1860AmbulationLocomotion"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1860AmbulationLocomotion"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    //if (submissionGuide.ContainsKey("SUBM_HIPPS_CODE") && assessmentQuestions.ContainsKey("HIIPSCODE") && assessmentQuestions["HIIPSCODE"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    //{
                    //    submissionFormat.Append(assessmentQuestions["HIIPSCODE"].Answer.ToUpper().PartOfString(0, 5).PadLeft(5));
                    //}
                    //else
                    if (submissionGuide.ContainsKey("M0110_EPISODE_TIMING") && assessmentQuestions.ContainsKey("M0110EpisodeTiming") && assessmentQuestions["M0110EpisodeTiming"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0110EpisodeTiming"].Answer == "NA") //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(5));
                    }
                    else
                    {
                        submissionFormat.Append("0".PadLeft(5, '0'));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(5));
                }
                submissionFormat.Append(string.Empty.PadLeft(5));

                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
                {
                    //if (submissionGuide.ContainsKey("SUBM_HIPPS_VERSION") && assessmentQuestions.ContainsKey("HIIPSVERSION") && assessmentQuestions["HIIPSVERSION"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    //{
                    //    submissionFormat.Append(assessmentQuestions["HIPPSVERSION"].Answer.ToUpper().PartOfString(0, 5).PadLeft(5));
                    //}
                    //else
                    {
                        submissionFormat.Append("0".PadLeft(5, '0'));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(5));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_SELF") && assessmentQuestions.ContainsKey("M1900SelfCareFunctioning") && assessmentQuestions["M1900SelfCareFunctioning"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900SelfCareFunctioning"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_AMBLTN") && assessmentQuestions.ContainsKey("M1900Ambulation") && assessmentQuestions["M1900Ambulation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900Ambulation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_TRNSFR") && assessmentQuestions.ContainsKey("M1900Transfer") && assessmentQuestions["M1900Transfer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900Transfer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_HSEHOLD") && assessmentQuestions.ContainsKey("M1900HouseHoldTasks") && assessmentQuestions["M1900HouseHoldTasks"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1900HouseHoldTasks"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M1910_MLT_FCTR_FALL_RISK_ASMT") && assessmentQuestions.ContainsKey("M1910FallRiskAssessment") && assessmentQuestions["M1910FallRiskAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1910FallRiskAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2000DrugRegimenReview"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "00") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "01") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2002_MDCTN_FLWP") && assessmentQuestions.ContainsKey("M2002MedicationFollowup") && assessmentQuestions["M2002MedicationFollowup"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2002MedicationFollowup"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(12));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    //if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "00") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "01") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    //{
                    //    submissionFormat.Append(string.Empty.PadLeft(2));
                    //}
                    //else
                    //{
                    if (submissionGuide.ContainsKey("M2004_MDCTN_INTRVTN") && assessmentQuestions.ContainsKey("M2004MedicationIntervention") && assessmentQuestions["M2004MedicationIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2004MedicationIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    // }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2010_HIGH_RISK_DRUG_EDCTN") && assessmentQuestions.ContainsKey("M2010PatientOrCaregiverHighRiskDrugEducation") && assessmentQuestions["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2015_DRUG_EDCTN_INTRVTN") && assessmentQuestions.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && assessmentQuestions["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2020_CRNT_MGMT_ORAL_MDCTN") && assessmentQuestions.ContainsKey("M2020ManagementOfOralMedications") && assessmentQuestions["M2020ManagementOfOralMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2020ManagementOfOralMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
                {
                    if (type.Contains("01") || type.Contains("03"))
                    {
                        if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2030_CRNT_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2030ManagementOfInjectableMedications") && assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                            {
                                submissionFormat.Append(assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(2));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2030_CRNT_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2030ManagementOfInjectableMedications") && assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M2040_PRIOR_MGMT_ORAL_MDCTN") && assessmentQuestions.ContainsKey("M2040PriorMedicationInject") && assessmentQuestions["M2040PriorMedicationInject"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2040PriorMedicationInject"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2040_PRIOR_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2040PriorMedicationInject") && assessmentQuestions["M2040PriorMedicationInject"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2040PriorMedicationInject"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }

                if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_ADL") && assessmentQuestions.ContainsKey("M2100ADLAssistance") && assessmentQuestions["M2100ADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100ADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_IADL") && assessmentQuestions.ContainsKey("M2100IADLAssistance") && assessmentQuestions["M2100IADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100IADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_MDCTN") && assessmentQuestions.ContainsKey("M2100MedicationAdministration") && assessmentQuestions["M2100MedicationAdministration"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100MedicationAdministration"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_PRCDR") && assessmentQuestions.ContainsKey("M2100MedicalProcedures") && assessmentQuestions["M2100MedicalProcedures"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100MedicalProcedures"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_EQUIP") && assessmentQuestions.ContainsKey("M2100ManagementOfEquipment") && assessmentQuestions["M2100ManagementOfEquipment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100ManagementOfEquipment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_SPRVSN") && assessmentQuestions.ContainsKey("M2100SupervisionAndSafety") && assessmentQuestions["M2100SupervisionAndSafety"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100SupervisionAndSafety"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_ADVCY") && assessmentQuestions.ContainsKey("M2100FacilitationPatientParticipation") && assessmentQuestions["M2100FacilitationPatientParticipation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2100FacilitationPatientParticipation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2110_ADL_IADL_ASTNC_FREQ") && assessmentQuestions.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && assessmentQuestions["M2110FrequencyOfADLOrIADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2110FrequencyOfADLOrIADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }

                if (type.Contains("01") || type.Contains("03"))
                {
                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PTNT_SPECF") && assessmentQuestions.ContainsKey("M2250PatientParameters") && assessmentQuestions["M2250PatientParameters"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250PatientParameters"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_DBTS_FT_CARE") && assessmentQuestions.ContainsKey("M2250DiabeticFoot") && assessmentQuestions["M2250DiabeticFoot"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250DiabeticFoot"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_FALL_PRVNT") && assessmentQuestions.ContainsKey("M2250FallsPrevention") && assessmentQuestions["M2250FallsPrevention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250FallsPrevention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_DPRSN_INTRVTN") && assessmentQuestions.ContainsKey("M2250DepressionPrevention") && assessmentQuestions["M2250DepressionPrevention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250DepressionPrevention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PAIN_INTRVTN") && assessmentQuestions.ContainsKey("M2250MonitorMitigatePainIntervention") && assessmentQuestions["M2250MonitorMitigatePainIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250MonitorMitigatePainIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PRSULC_PRVNT") && assessmentQuestions.ContainsKey("M2250PressureUlcerIntervention") && assessmentQuestions["M2250PressureUlcerIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250PressureUlcerIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PRSULC_TRTMT") && assessmentQuestions.ContainsKey("M2250PressureUlcerTreatment") && assessmentQuestions["M2250PressureUlcerTreatment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2250PressureUlcerTreatment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }

                if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
                {
                    if (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2300EmergentCare"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(17));
                    }

                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("00000000000000000");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2310_ECR_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareFall")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareResInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOtherResInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_HRT_FAILR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartFail")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareCardiac")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMyocardial")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartDisease")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_STROKE_TIA") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareStroke")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_GI_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareGI")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDehMal")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_UTI") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUrinaryInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareIV")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_WND_INFCTN_DTRORTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareWoundInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_UNCNTLD_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMental")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2310_ECR_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDVT")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2310_ECR_OTHER") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOther")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                            }
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M2310_ECR_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareResInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOtherResInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_HRT_FAILR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartFail")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareCardiac")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMyocardial")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartDisease")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_STROKE_TIA") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareStroke")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_GI_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareGI")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDehMal")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_UTI") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUrinaryInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareIV")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_WND_INFCTN_DTRORTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareWoundInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_UNCNTLD_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMental")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDVT")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_OTHER") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_DBTS_FT") && assessmentQuestions.ContainsKey("M2400DiabeticFootCare") && assessmentQuestions["M2400DiabeticFootCare"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400DiabeticFootCare"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_FALL_PRVNT") && assessmentQuestions.ContainsKey("M2400FallsPreventionInterventions") && assessmentQuestions["M2400FallsPreventionInterventions"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400FallsPreventionInterventions"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_DPRSN") && assessmentQuestions.ContainsKey("M2400DepressionIntervention") && assessmentQuestions["M2400DepressionIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400DepressionIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PAIN_MNTR") && assessmentQuestions.ContainsKey("M2400PainIntervention") && assessmentQuestions["M2400PainIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400PainIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PRSULC_PRVN") && assessmentQuestions.ContainsKey("M2400PressureUlcerIntervention") && assessmentQuestions["M2400PressureUlcerIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400PressureUlcerIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PRSULC_WET") && assessmentQuestions.ContainsKey("M2400PressureUlcerTreatment") && assessmentQuestions["M2400PressureUlcerTreatment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2400PressureUlcerTreatment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }

                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                    submissionFormat.Append(string.Empty.PadLeft(17));
                    submissionFormat.Append(string.Empty.PadLeft(12));
                }
                if (type.Contains("09"))
                {
                    //if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "01") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    //{
                    //    submissionFormat.Append(string.Empty.PadLeft(2));
                    //}
                    //else
                    //{
                        if (submissionGuide.ContainsKey("M2420_DSCHRG_DISP") && assessmentQuestions.ContainsKey("M2420DischargeDisposition") && assessmentQuestions["M2420DischargeDisposition"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(assessmentQuestions["M2420DischargeDisposition"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(2));
                        }
                    //}
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (type.Contains("06") || type.Contains("07"))
                {
                    if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Empty.PadLeft(16));
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("0000000000000001");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M2430_HOSP_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationFall")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationInfection")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOtherRP")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2430_HOSP_HRT_FAILR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartFail")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }

                                if (submissionGuide.ContainsKey("M2430_HOSP_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationCardiac")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMyocardial")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }

                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartDisease")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }

                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_STROKE_TIA") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationStroke")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_GI_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationGI")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDehMal")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationIV")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_WND_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationWoundInf")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }

                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMental")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_SCHLD_TRTMT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationScheduled")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M2430_HOSP_OTHER") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOther")) //M0010_CCN
                                {
                                    if (assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {

                            if (submissionGuide.ContainsKey("M2430_HOSP_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationInfection")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOtherRP")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_HRT_FAILR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartFail")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationCardiac")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMyocardial")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartDisease")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_STROKE_TIA") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationStroke")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_GI_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationGI")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDehMal")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationIV")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_WND_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationWoundInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMental")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_SCHLD_TRTMT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationScheduled")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_OTHER") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }
                submissionFormat.Append(string.Empty.PadLeft(244));
                submissionFormat.Append("%");
                return submissionFormat.ToString().ToUpper();
            }
            else
            {
                return string.Empty;
            }
        }
       
        public string OasisHeader(AgencyLocation agencyLocation)
        {
            var submissionGuide = GetOasisHeaderInstructionsNew();
            var submissionFormat = new StringBuilder();
            submissionFormat.Capacity = 1448;
            if (submissionGuide != null && submissionGuide.Count > 0)
            {
                submissionFormat.Append("A1"); //REC_ID :2

                if (submissionGuide.ContainsKey("FED_ID")) //FED_ID :8
                {
                    if (agencyLocation != null && agencyLocation.MedicareProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.MedicareProviderNumber.PadRight(6).Trim().PartOfString(0, 6).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(6));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(6));
                }

                submissionFormat.Append(string.Empty.PadRight(4)); //FILLER1 :12

                if (submissionGuide.ContainsKey("ST_ID")) //ST_ID :27
                {
                    if (agencyLocation.MedicaidProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.MedicaidProviderNumber.Trim().PadRight(15).PartOfString(0, 15).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(15));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(15));
                }

                if (submissionGuide.ContainsKey("HHA_AGENCY_ID")) //HHA_AGENCY_ID :43
                {
                    if (agencyLocation.HomeHealthAgencyId.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.HomeHealthAgencyId.Trim().PadRight(16).PartOfString(0, 16).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(16));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(16));
                }

                if (submissionGuide.ContainsKey("ACY_NAME")) //ACY_NAME :73
                {
                    if (agencyLocation.Name.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.Name.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_ADDR_1")) //ACY_ADDR_1 :103
                {
                    if (agencyLocation != null && agencyLocation.AddressLine1.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressLine1.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_ADDR_2")) //ACY_ADDR_2 :133
                {
                    if (agencyLocation != null && agencyLocation.AddressLine2.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressLine2.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(30));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }


                if (submissionGuide.ContainsKey("ACY_CITY")) //ACY_CITY :153
                {
                    if (agencyLocation != null && agencyLocation.AddressCity.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressCity.Trim().PadRight(20).PartOfString(0, 20).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(20));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("ACY_ST")) //ACY_ST :155
                {
                    if (agencyLocation != null && agencyLocation.AddressStateCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressStateCode.Trim().PadRight(2).PartOfString(0, 2).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("ACY_ZIP")) //ACY_ZIP :166
                {
                    if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.AddressZipCode.Trim().PadRight(11).PartOfString(0, 11).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(11));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if (submissionGuide.ContainsKey("ACY_CNTCT")) //ACY_CNTCT :196
                {
                    submissionFormat.Append(string.Format("{0}, {1}", agencyLocation.ContactPersonLastName.IsNotNullOrEmpty() ? agencyLocation.ContactPersonLastName : string.Empty, agencyLocation.ContactPersonFirstName.IsNotNullOrEmpty() ? agencyLocation.ContactPersonFirstName : string.Empty).Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("ACY_PHONE")) //ACY_PHONE :206
                {
                    if (agencyLocation.ContactPersonPhone.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.ContactPersonPhone.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("ACY_EXTEN")) //ACY_EXTEN :211
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("AGT_ID")) //AGT_ID :220
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("AGT_NAME")) //AGT_NAME :250
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_ADDR_1")) //AGT_ADDR_1 :280
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_ADDR_2")) //AGT_ADDR_2 :310
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_CITY")) //AGT_CITY :330
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("AGT_ST")) //AGT_ST :332
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("AGT_ZIP")) //AGT_ZIP :343
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }

                if (submissionGuide.ContainsKey("AGT_CNTCT")) //AGT_CNTCT :373
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("AGT_PHONE")) //AGT_PHONE :383
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("AGT_EXTEN")) //AGT_EXTEN :388
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("SFW_ID") && submissionGuide["SFW_ID"] != null && submissionGuide["SFW_ID"].DefaultValue.IsNotNullOrEmpty())//SFW_ID :397
                {
                    submissionFormat.Append(submissionGuide["SFW_ID"].DefaultValue.Trim().Trim().PadRight(9).PartOfString(0, 9).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(9));
                }

                if (submissionGuide.ContainsKey("SFW_NAME") && submissionGuide["SFW_NAME"] != null && submissionGuide["SFW_NAME"].DefaultValue.IsNotNullOrEmpty()) //SFW_NAME : 427
                {
                    submissionFormat.Append(submissionGuide["SFW_NAME"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_ADDR_1") && submissionGuide["SFW_ADDR_1"] != null && submissionGuide["SFW_ADDR_1"].DefaultValue.IsNotNullOrEmpty()) //SFW_ADDR_1 : 457
                {
                    submissionFormat.Append(submissionGuide["SFW_ADDR_1"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_ADDR_2") && submissionGuide["SFW_ADDR_2"] != null && submissionGuide["SFW_ADDR_2"].DefaultValue.IsNotNullOrEmpty()) //SFW_ADDR_1 :487
                {
                    submissionFormat.Append(submissionGuide["SFW_ADDR_2"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_CITY") && submissionGuide["SFW_CITY"] != null && submissionGuide["SFW_CITY"].DefaultValue.IsNotNullOrEmpty()) //SFW_CITY : 507
                {
                    submissionFormat.Append(submissionGuide["SFW_CITY"].DefaultValue.Trim().PadRight(20).PartOfString(0, 20).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(20));
                }

                if (submissionGuide.ContainsKey("SFW_ST") && submissionGuide["SFW_ST"] != null && submissionGuide["SFW_ST"].DefaultValue.IsNotNullOrEmpty()) //SFW_ST : 509
                {
                    submissionFormat.Append(submissionGuide["SFW_ST"].DefaultValue.Trim().PadRight(2).PartOfString(0, 2).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(2));
                }

                if (submissionGuide.ContainsKey("SFW_ZIP") && submissionGuide["SFW_ZIP"] != null && submissionGuide["SFW_ZIP"].DefaultValue.IsNotNullOrEmpty()) //SFW_ZIP : 520
                {
                    submissionFormat.Append(submissionGuide["SFW_ZIP"].DefaultValue.Trim().PadRight(11).PartOfString(0, 11).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(11));
                }


                if (submissionGuide.ContainsKey("SFW_CNTCT") && submissionGuide["SFW_CNTCT"] != null && submissionGuide["SFW_CNTCT"].DefaultValue.IsNotNullOrEmpty()) //SFW_CNTCT : 550
                {
                    submissionFormat.Append(submissionGuide["SFW_CNTCT"].DefaultValue.Trim().PadRight(30).PartOfString(0, 30).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(30));
                }

                if (submissionGuide.ContainsKey("SFW_PHONE") && submissionGuide["SFW_PHONE"] != null && submissionGuide["SFW_PHONE"].DefaultValue.IsNotNullOrEmpty()) //SFW_PHONE :560
                {
                    submissionFormat.Append(submissionGuide["SFW_PHONE"].DefaultValue.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("SFW_EXTEN")) //SFW_EXTEN :565
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(5));
                }

                if (submissionGuide.ContainsKey("FILE_DT")) //FILE_DT :573
                {
                    submissionFormat.Append(DateTime.Now.ToString("yyyyMMdd").Trim().ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(8));
                }

                if (submissionGuide.ContainsKey("TEST_SW") && submissionGuide["TEST_SW"] != null && submissionGuide["TEST_SW"].DefaultValue.IsNotNullOrEmpty()) //TEST_SW :574
                {
                    submissionFormat.Append(submissionGuide["TEST_SW"].DefaultValue.PadRight(1).PartOfString(0, 1).ToUpper());
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(1));
                }


                if (submissionGuide.ContainsKey("NATL_PROV_ID")) //NATL_PROV_ID :584
                {
                    if (agencyLocation.NationalProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agencyLocation.NationalProviderNumber.Trim().PadRight(10).PartOfString(0, 10).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(10));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(10));
                }

                if (submissionGuide.ContainsKey("HDR_FL")) //HDR_FL : 1445
                {
                    submissionFormat.Append(string.Empty.PadRight(861));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadRight(861));
                }

                if (submissionGuide.ContainsKey("DATA_END")) //HDR_FL :1446
                {
                    submissionFormat.Append("%".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("%".PadRight(1));
                }

                if (submissionGuide.ContainsKey("CRG_RTN")) //CRG_RTN :1447
                {
                    submissionFormat.Append("\r".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("\r".PadRight(1));
                }

                if (submissionGuide.ContainsKey("LN_FD")) //LN_FD :1448
                {
                    submissionFormat.Append("\n".PadRight(1));
                }
                else
                {
                    submissionFormat.Append("\n".PadRight(1));
                }

            }
            return submissionFormat.ToString();
        }

        public string OasisFooter(int totalNumberOfRecord)
        {
            var footerString = new string(' ', 1446);
            var footerStringWithRecId = footerString.Remove(0, 2).Insert(0, "Z1");
            var footerStringWithTotalRec = footerStringWithRecId.Remove(2, 6).Insert(2, totalNumberOfRecord.ToString().PadLeft(6, '0'));
            var footerStringWithDataEnd = footerStringWithTotalRec.Remove(1445, 1).Insert(1445, "%");
            return footerStringWithDataEnd;
        }

        public string OasisInactivateBody(IDictionary<string, Question> assessmentQuestions , Guid agencyLocationId)
        {
            var submissionFormat = new StringBuilder();
            submissionFormat.Capacity = 1446;
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                if (assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"] != null && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty())
                {
                    string type = assessmentQuestions["M0100AssessmentType"].Answer;

                    var submissionGuide = GetInactivateOasisSubmissionFormatInstructionsNew();

                    var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, agencyLocationId);
                    if (agencyLocation != null && !agencyLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            agencyLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            agencyLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }
                    
                    //var agency = agencyRepository.Get(Current.AgencyId);
                    //if (agency != null)
                    //{
                    //    agency.MainLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                    //}
                    submissionFormat.Append("X1"); //REC_ID
                    submissionFormat.Append(string.Empty.PadRight(2)); //ITEM_FILLER1
                    submissionFormat.Append(string.Empty.PadRight(8)); //LOCK_DATE
                    submissionFormat.Append(string.Empty.PadRight(41)); //ITEM_FILLER2
                    if (submissionGuide.ContainsKey("HHA_AGENCY_ID")) //HHA_AGENCY_ID
                    {
                        if (agencyLocation!=null && agencyLocation.HomeHealthAgencyId.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(agencyLocation.HomeHealthAgencyId.Trim().PadRight((16)).PartOfString(0, (16)).ToUpper());
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadRight(16));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(16));
                    }
                    submissionFormat.Append(string.Empty.PadRight(27)); //ITEM_FILLER3
                    submissionFormat.Append(string.Empty.PadRight(20)); //MASK_VERSION_CD
                    submissionFormat.Append(string.Empty.PadRight(60)); //ITEM_FILLER4
                    if (submissionGuide.ContainsKey("M0030_START_CARE_DT") && assessmentQuestions.ContainsKey("M0030SocDate") && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0030SocDate"].Answer.IsValidDate()) //M0030_START_CARE_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8));

                    }
                    if (submissionGuide.ContainsKey("M0032_ROC_DT") && assessmentQuestions.ContainsKey("M0032ROCDate") && assessmentQuestions["M0032ROCDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0032ROCDate"].Answer.IsValidDate()) //M0032_ROC_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0032ROCDate"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8));
                    }
                    submissionFormat.Append(string.Empty.PadRight(1)); //ITEM_FILLER5
                    if (submissionGuide.ContainsKey("M0040_PAT_FNAME") && assessmentQuestions.ContainsKey("M0040FirstName") && assessmentQuestions["M0040FirstName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_FNAME
                    {

                        submissionFormat.Append(assessmentQuestions["M0040FirstName"].Answer.Trim().ToUpper().PartOfString(0, 12).PadRight(12));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(12));
                    }
                    submissionFormat.Append(string.Empty.PadRight(1)); //ITEM_FILLER6
                    if (submissionGuide.ContainsKey("M0040_PAT_LNAME") && assessmentQuestions.ContainsKey("M0040LastName") && assessmentQuestions["M0040LastName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_LNAME
                    {

                        submissionFormat.Append(assessmentQuestions["M0040LastName"].Answer.Trim().ToUpper().PartOfString(0, 18).PadRight(18));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(18));
                    }
                    submissionFormat.Append(string.Empty.PadRight(29)); //ITEM_FILLER7
                    if (submissionGuide.ContainsKey("M0064_SSN") && assessmentQuestions.ContainsKey("M0064PatientSSN") && assessmentQuestions["M0064PatientSSN"].Answer.IsNotNullOrEmpty()) //M0064_SSN
                    {
                        submissionFormat.Append(assessmentQuestions["M0064PatientSSN"].Answer.Trim().ToUpper().PartOfString(0, 9).PadRight(9));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(9));
                    }
                    submissionFormat.Append(string.Empty.PadRight(16)); //ITEM_FILLER8
                    if (submissionGuide.ContainsKey("M0066_PAT_BIRTH_DT") && assessmentQuestions.ContainsKey("M0066PatientDoB") && assessmentQuestions["M0066PatientDoB"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0066PatientDoB"].Answer.IsValidDate()) //M0066_PAT_BIRTH_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0066PatientDoB"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadRight(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadRight(8, '-'));
                    }
                    submissionFormat.Append(string.Empty.PadRight(1)); //ITEM_FILLER9
                    if (submissionGuide.ContainsKey("M0069_PAT_GENDER") && assessmentQuestions.ContainsKey("M0069Gender") && assessmentQuestions["M0069Gender"].Answer.IsNotNullOrEmpty()) //M0069_PAT_GENDER
                    {
                        submissionFormat.Append(assessmentQuestions["M0069Gender"].Answer.Trim().ToUpper().PartOfString(0, 1).PadRight(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadRight(13)); //ITEM_FILLER10
                    if (submissionGuide.ContainsKey("M0090_INFO_COMPLETED_DT") && assessmentQuestions.ContainsKey("M0090AssessmentCompleted") && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsValidDate()) //M0090_INFO_COMPLETED_DT
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0090AssessmentCompleted"].Answer).ToString("yyyyMMdd").Trim().PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }

                    if (submissionGuide.ContainsKey("M0100_ASSMT_REASON") && assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty()) //M0100_ASSMT_REASON
                    {
                        submissionFormat.Append(assessmentQuestions["M0100AssessmentType"].Answer.Trim().ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    submissionFormat.Append(string.Empty.PadRight(425)); //ITEM_FILLER11
                    if (type.Contains("06") || type.Contains("07") || type.Contains("08") || type.Contains("09"))
                    {
                        if (submissionGuide.ContainsKey("M0906_DC_TRAN_DTH_DT") && assessmentQuestions.ContainsKey("M0906DischargeDate") && assessmentQuestions["M0906DischargeDate"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M0906DischargeDate"].Answer.IsValidDate()) //M0906_DC_TRAN_DTH_DT
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0906DischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    submissionFormat.Append(string.Empty.PadRight(701)); //ITEM_FILLER12
                    submissionFormat.Append("%"); //DATA_END
                }
            }
            return submissionFormat.ToString();
        }

        public ScheduleEvent GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId)
        {
            ScheduleEvent result = null;
            var episode = patientRepository.GetEpisodeOnlyWithPreviousAndAfter(Current.AgencyId, episodeId, patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit);
                if (scheduleEvents != null)
                {
                    var socSchedule = scheduleEvents.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISStartofCare);
                    if (socSchedule != null)
                    {
                        var status = socSchedule.Status;
                        if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString() || status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            result = socSchedule;
                        }
                    }
                    else
                    {
                        var previousEpisode = episode.PreviousEpisode;
                        if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var previousSchedules = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= previousEpisode.StartDate.Date && s.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date) && (s.EventDate.ToDateTime().Date >= episode.StartDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date < episode.EndDate.Date) && !s.IsDeprecated && !s.IsMissedVisit).OrderByDescending(s => s.EventDate.ToDateTime().Date).ToList();
                            if (previousSchedules != null)
                            {
                                var recetSchedule = previousSchedules.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification);
                                if (recetSchedule != null)
                                {
                                    var status = recetSchedule.Status;
                                    if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString() || status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                                    {
                                        result = recetSchedule;
                                    }
                                }
                                else
                                {
                                    var rocSchedule = previousSchedules.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                                    if (rocSchedule != null)
                                    {
                                        var status = rocSchedule.Status;
                                        if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString() || status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                                        {
                                            result = rocSchedule;
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            return result;

        }

        public Assessment GetEpisodeAssessment(Guid agencyId, PatientEpisode episode)
        {
            Assessment assessment = null;
            if (episode != null && episode.IsActive == true && episode.Schedule.IsNotNullOrEmpty())
            {
                var assessmentEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                   .Where(e => e.IsDeprecated == false && e.IsMissedVisit == false
                      && e.EventDate.IsValidDate() && e.EventDate.ToDateTime() >= episode.StartDate
                      && e.EventDate.ToDateTime().Date < episode.EndDate.AddDays(-5).Date
                      && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment()))
                   .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();

                if (assessmentEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentEvent.EventId, assessmentEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                }
                else
                {
                    if (episode.HasPrevious && episode.PreviousEpisode != null && episode.PreviousEpisode.IsActive == true && episode.PreviousEpisode.IsDischarged == false && episode.PreviousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var recertEvent = episode.PreviousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => e.IsDeprecated == false && e.IsMissedVisit == false
                               && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.PreviousEpisode.EndDate.AddDays(-5).Date
                               && e.EventDate.ToDateTime().Date <= episode.PreviousEpisode.EndDate.Date && e.IsRecertificationAssessment())
                            .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();

                        if (recertEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(recertEvent.EventId, recertEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                        }
                        else
                        {
                            var rocEvent = episode.PreviousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                                 .Where(e => e.IsDeprecated == false && e.IsMissedVisit == false
                                    && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.PreviousEpisode.EndDate.AddDays(-5).Date
                                    && e.EventDate.ToDateTime().Date <= episode.PreviousEpisode.EndDate.Date && e.IsResumptionofCareAssessment())
                                 .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();

                            if (rocEvent != null)
                            {
                                assessment = oasisDataProvider.OasisAssessmentRepository.Get(rocEvent.EventId, rocEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                            }
                        }
                    }
                }
            }
            return assessment;
        }

        public Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId)
        {
            var episode = patientRepository.GetEpisodeOnlyWithPreviousAndAfter(Current.AgencyId, episodeId, patientId);
            return GetEpisodeAssessment(Current.AgencyId, episode); 
        }

        public Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate)
        {
            return GetEpisodeAssessment(Current.AgencyId, episodeId, patientId, eventDate);
        }

        public Assessment GetEpisodeAssessment(Guid agencyId, Guid episodeId, Guid patientId, DateTime eventDate)
        {
            Assessment assessment = null;
            var episode = patientRepository.GetEpisodeOnlyWithPreviousAndAfter(agencyId, episodeId, patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var assessmentEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && !e.IsMissedVisit && !e.IsDeprecated
                            && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date
                            && (e.IsStartofCareAssessment()))
                    .OrderBy(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                if (assessmentEvent != null)
                {
                    assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentEvent.EventId, assessmentEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                }
                else
                {
                    if (episode.HasPrevious && episode.PreviousEpisode != null && episode.PreviousEpisode.IsActive && !episode.PreviousEpisode.IsDischarged && episode.PreviousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var previousEvent = episode.PreviousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && !e.IsMissedVisit && !e.IsDeprecated
                                && e.EventDate.ToDateTime().Date >= episode.StartDate.AddDays(-6).Date
                                && e.EventDate.ToDateTime().Date <= episode.StartDate.AddDays(-1).Date
                                && episode.PreviousEpisode.EndDate.AddDays(1).Date == episode.StartDate.Date
                                && (e.IsResumptionofCareAssessment() || e.IsRecertificationAssessment()))
                            .OrderByDescending(e => e.EventDate.ToDateTime().ToString("yyyyMMdd")).FirstOrDefault();

                        if (previousEvent != null)
                        {
                            assessment = oasisDataProvider.OasisAssessmentRepository.Get(previousEvent.EventId, previousEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                        }
                    }
                }
            }
            return assessment;
        }

        public DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId)
        {
            var dateRange = new DateRange();
            dateRange.IsLinkedToAssessment = false;
            var episode = patientRepository.GetEpisodeOnlyWithPreviousAndAfter(Current.AgencyId, episodeId, patientId);
            if (episode != null && episode.IsActive == true && episode.Schedule.IsNotNullOrEmpty())
            {
                dateRange.EpisodeId = episode.Id;
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e=>!e.IsDeprecated && !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date<=episode.EndDate && e.EventDate.ToDateTime().Date>=episode.StartDate.Date).OrderByDescending(e => e.EventDate.ToDateTime().Date).ToList();
                if (scheduleEvents.Count > 0)
                {
                    var assessmentEvent = scheduleEvents.FirstOrDefault(e => e.EventId == assessmentId);
                    if (assessmentEvent != null)
                    {
                        if (assessmentEvent.IsRecertificationAssessment() && assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5))
                        {
                            if (episode.NextEpisode != null)
                            {
                                dateRange.EpisodeId = episode.NextEpisode.Id;
                                dateRange.StartDate = episode.NextEpisode.StartDate;
                                dateRange.EndDate = episode.NextEpisode.EndDate;
                            }
                            else
                            {
                                dateRange.EpisodeId = Guid.Empty;
                                dateRange.StartDate = episode.EndDate.AddDays(1);
                                dateRange.EndDate = episode.EndDate.AddDays(60);
                            }
                        }
                        else if (assessmentEvent.IsResumptionofCareAssessment() && assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5)
                                                    && !scheduleEvents.Exists(e => (e.IsRecertificationAssessment() || e.IsResumptionofCareAssessment()) && e.EventId != assessmentEvent.EventId && e.EventDate.ToDateTime().Date > assessmentEvent.EventDate.ToDateTime().Date))
                        {
                            if (episode.NextEpisode != null)
                            {
                                dateRange.EpisodeId = episode.NextEpisode.Id;
                                dateRange.StartDate = episode.NextEpisode.StartDate;
                                dateRange.EndDate = episode.NextEpisode.EndDate;
                            }
                            else
                            {
                                dateRange.EpisodeId = Guid.Empty;
                                dateRange.StartDate = episode.EndDate.AddDays(1);
                                dateRange.EndDate = episode.EndDate.AddDays(60);
                            }
                        }
                        else if (assessmentEvent.IsSNRecertificationAssessment() && assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5))
                        {
                            if (episode.NextEpisode != null)
                            {
                                dateRange.EpisodeId = episode.NextEpisode.Id;
                                dateRange.StartDate = episode.NextEpisode.StartDate;
                                dateRange.EndDate = episode.NextEpisode.EndDate;
                            }
                            else
                            {
                                dateRange.EpisodeId = Guid.Empty;
                                dateRange.StartDate = episode.EndDate.AddDays(1);
                                dateRange.EndDate = episode.EndDate.AddDays(60);
                            }
                        }
                        else
                        {
                            dateRange.StartDate = episode.StartDate;
                            dateRange.EndDate = episode.EndDate;
                        }
                        dateRange.IsLinkedToAssessment = true;
                    }
                    else
                    {
                        dateRange.StartDate = episode.StartDate;
                        dateRange.EndDate = episode.EndDate;
                    }
                }
                else
                {
                    dateRange.StartDate = episode.StartDate;
                    dateRange.EndDate = episode.StartDate.AddDays(59);
                }

            }
            return dateRange;
        }
        public DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId, Guid agencyId)
        {
            var dateRange = new DateRange();
            dateRange.IsLinkedToAssessment = false;
            var episode = patientRepository.GetEpisodeOnlyWithPreviousAndAfter(agencyId, episodeId, patientId);
            if (episode != null && episode.IsActive == true && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => !e.IsDeprecated && !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= episode.EndDate && e.EventDate.ToDateTime().Date >= episode.StartDate.Date).OrderByDescending(e => e.EventDate.ToDateTime().Date).ToList();
                if (scheduleEvents.Count > 0)
                {
                    var assessmentEvent = scheduleEvents.FirstOrDefault(e => e.EventId == assessmentId);
                    if (assessmentEvent != null)
                    {
                        if (assessmentEvent.IsRecertificationAssessment() && assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5))
                        {
                            if (episode.NextEpisode != null)
                            {
                                dateRange.StartDate = episode.NextEpisode.StartDate;
                                dateRange.EndDate = episode.NextEpisode.EndDate;
                            }
                            else
                            {
                                dateRange.StartDate = episode.EndDate.AddDays(1);
                                dateRange.EndDate = episode.EndDate.AddDays(60);
                            }
                        }
                        else if (assessmentEvent.IsResumptionofCareAssessment() && assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5)
                                                    && !scheduleEvents.Exists(e => (e.IsRecertificationAssessment() || e.IsResumptionofCareAssessment()) && e.EventId != assessmentEvent.EventId && e.EventDate.ToDateTime().Date > assessmentEvent.EventDate.ToDateTime().Date))
                        {
                            if (episode.NextEpisode != null)
                            {
                                dateRange.StartDate = episode.NextEpisode.StartDate;
                                dateRange.EndDate = episode.NextEpisode.EndDate;
                            }
                            else
                            {
                                dateRange.StartDate = episode.EndDate.AddDays(1);
                                dateRange.EndDate = episode.EndDate.AddDays(60);
                            }
                        }
                        else if (assessmentEvent.IsSNRecertificationAssessment() && assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5))
                        {
                            if (episode.NextEpisode != null)
                            {
                                dateRange.StartDate = episode.NextEpisode.StartDate;
                                dateRange.EndDate = episode.NextEpisode.EndDate;
                            }
                            else
                            {
                                dateRange.StartDate = episode.EndDate.AddDays(1);
                                dateRange.EndDate = episode.EndDate.AddDays(60);
                            }
                        }
                        else
                        {
                            dateRange.StartDate = episode.StartDate;
                            dateRange.EndDate = episode.EndDate;
                        }
                        dateRange.IsLinkedToAssessment = true;
                    }
                    else
                    {
                        dateRange.StartDate = episode.StartDate;
                        dateRange.EndDate = episode.EndDate;
                    }
                }
                else
                {
                    dateRange.StartDate = episode.StartDate;
                    dateRange.EndDate = episode.StartDate.AddDays(59);
                }

            }
            return dateRange;
        }

        public string GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType)
        {
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, episodeId, assessmentId, assessmentType);
            if (planofCare != null)
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                if (scheduleEvent != null)
                {
                    return new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({")
                        .AppendFormat("Url: '/485/PrintPreview/{0}/{1}/{2}',", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                        .Append("PdfUrl: '/Oasis/PlanOfCarePdf',")
                        .AppendFormat("PdfData: {{ 'episodeId': '{0}', 'patientId': '{1}', 'eventId': '{2}' }}", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                        .Append("})\">View Plan of Care</a>").ToString();
                }
            }
            return string.Empty;
        }

        public ScheduleEvent GetPlanofCareScheduleEvent(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType)
        {
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, episodeId, assessmentId, assessmentType);
            if (planofCare != null)
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                if (scheduleEvent != null)
                {
                    return scheduleEvent;
                }
            }
            return null;
        }

        public PlanofCare GetPlanofCare(Guid episodeId, Guid patientId, Guid planofCareId)
        {
            return oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, episodeId, patientId, planofCareId);
        }

        public PlanofCareStandAlone GetPlanofCareStandAlone(Guid episodeId, Guid patientId, Guid planofCareId)
        {
            return oasisDataProvider.PlanofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, planofCareId);
        }

        public bool UpdatePlanofCare(FormCollection formCollection)
        {
            var result = false;
            var planofCareId = formCollection.Get("Id").ToGuid();
            var episodeId = formCollection.Get("EpisodeId").ToGuid();
            var patientId = formCollection.Get("PatientId").ToGuid();

            var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, episodeId, patientId, planofCareId);
            if (planofCare != null)
            {
                Guid physicianGuidId;
                var physicianStringId = formCollection.Get("PhysicianId");
                if (physicianStringId.IsNotNullOrEmpty() && physicianStringId.GuidTryParse(out physicianGuidId))
                {
                    planofCare.PhysicianId = physicianGuidId;
                }

                var status = formCollection.Get("Status");
                if (status.IsNotNullOrEmpty() && status.IsInteger())
                {
                    planofCare.Status = int.Parse(status);
                    if (planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                    {
                        planofCare.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        DateTime dTOutput;
                        if (DateTime.TryParse(formCollection.Get("SignatureDate"), out dTOutput)) { 
                            planofCare.SignatureDate = dTOutput;
                        } else {
                            return false;
                        }
                        //planofCare.SignatureDate = DateTime.Parse(formCollection.Get("SignatureDate"));
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            planofCare.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        }
                        if (!planofCare.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianData = physician.ToXml();
                            }
                        }
                    }
                    else
                    {
                        planofCare.SignatureText = string.Empty;
                        planofCare.SignatureDate = DateTime.MinValue;
                    }

                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, planofCareId);
                    if (scheduleEvent != null)
                    {
                        scheduleEvent.Status = planofCare.Status.ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            ProcessPlanofCare(formCollection, planofCare);
                            planofCare.Data = planofCare.Questions.ToXml();
                            if (planofCareId.IsEmpty())
                            {
                                result = oasisDataProvider.PlanofCareRepository.Add(planofCare);
                                if (result)
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
                                }
                            }
                            else
                            {
                                result = oasisDataProvider.PlanofCareRepository.Update(planofCare);
                                if (result)
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)planofCare.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }

                        }
                    }
                }
            }
            return result;
        }

        public bool UpdatePlanofCareStandAlone(FormCollection formCollection)
        {
            var result = false;
            var planofCareId = formCollection.Get("Id").ToGuid();
            var episodeId = formCollection.Get("EpisodeId").ToGuid();
            var patientId = formCollection.Get("PatientId").ToGuid();

            var planofCare = oasisDataProvider.PlanofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, planofCareId);
            if (planofCare != null)
            {
                Guid physicianGuidId;
                var physicianStringId = formCollection.Get("PhysicianId");
                if (physicianStringId.IsNotNullOrEmpty() && physicianStringId.GuidTryParse(out physicianGuidId))
                {
                    planofCare.PhysicianId = physicianGuidId;
                }

                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    planofCare.PatientData = patient.ToXml();
                }

                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    planofCare.AgencyId = agency.Id;
                    planofCare.AgencyData = agency.ToXml();
                }
                var status = formCollection.Get("Status");
                if (status.IsNotNullOrEmpty() && status.IsInteger())
                {
                    planofCare.Status = int.Parse(status);
                    if (planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                    {
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            planofCare.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        }
                        if (!planofCare.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianData = physician.ToXml();
                            }
                        }

                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, planofCareId);
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.Status = planofCare.Status.ToString();
                            if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                planofCare.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                planofCare.SignatureDate = DateTime.Parse(formCollection.Get("SignatureDate"));
                            }
                        }
                    }
                }

                ProcessPlanofCare(formCollection, planofCare);
                planofCare.Data = planofCare.Questions.ToXml();

                result = oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCare);
                if (result)
                {
                    Auditor.Log(planofCare.EpisodeId, planofCare.PatientId, planofCare.Id, Actions.StatusChange, (ScheduleStatus)planofCare.Status, DisciplineTasks.HCFA485StandAlone, string.Empty);
                }
            }
            return result;
        }

        public bool UpdatePlanOfCareForDetail(ScheduleEvent schedule, Guid oldEpisodeId)
        {
            bool result = false;
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, oldEpisodeId, schedule.PatientId, schedule.EventId);
            if (planofCare != null)
            {
                planofCare.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : planofCare.Status;
                if (planofCare.Status.ToString() != schedule.Status && planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                    if (physician != null )
                    {
                        planofCare.PhysicianData = physician.ToXml();
                    }
                }
                planofCare.EpisodeId = schedule.EpisodeId;
                planofCare.PhysicianId = schedule.PhysicianId;
                planofCare.IsDeprecated = schedule.IsDeprecated;
                result = oasisDataProvider.PlanofCareRepository.Update(planofCare);
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdatePlanOfCareStandAloneForDetail(ScheduleEvent schedule)
        {
            bool result = false;
            var planofCare = oasisDataProvider.PlanofCareRepository.GetStandAlone(Current.AgencyId,schedule.EpisodeId,schedule.PatientId, schedule.EventId);
            if (planofCare != null)
            {
                planofCare.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : planofCare.Status;
                if (planofCare.Status.ToString() != schedule.Status && planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        planofCare.PhysicianData = physician.ToXml();
                    }
                }
                planofCare.PhysicianId = schedule.PhysicianId;
                planofCare.EpisodeId = schedule.EpisodeId;
                planofCare.IsDeprecated = schedule.IsDeprecated;
                result = oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCare);
            }
            else
            {
                result = true;
            }
            return result;
        }

        public void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis)
        {
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId,scheduleEvent.EpisodeId, assessment.Id, assessment.Type.ToString());
            if (planofCare == null)
            {
                planofCare = new PlanofCare();
                planofCare.Id = scheduleEvent.EventId;
                planofCare.AssessmentId = assessment.Id;
                planofCare.AgencyId = Current.AgencyId;
                planofCare.PatientId = scheduleEvent.PatientId;
                planofCare.EpisodeId = scheduleEvent.EpisodeId;
                planofCare.Status = scheduleEvent.Status.ToInteger();
                planofCare.AssessmentType = assessment.Type.ToString();
                planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
                planofCare.IsNonOasis = isNonOasis;
                var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patient.Id);
                if (physician != null)
                {
                    planofCare.PhysicianId = physician.Id;
                }
                //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                //{
                //    var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                //    if (primaryPhysician == null)
                //    {
                //        primaryPhysician = patient.PhysicianContacts.First();
                //    }
                //    planofCare.PhysicianId = primaryPhysician.Id;
                //}
                planofCare.UserId = scheduleEvent.UserId;
                planofCare.Questions = Get485FromAssessment(assessment);
                planofCare.Data = planofCare.Questions.ToXml();
                oasisDataProvider.PlanofCareRepository.Add(planofCare);
            }
        }

        public bool MarkPlanOfCareAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated)
        {
            return oasisDataProvider.PlanofCareRepository.MarkAsDeleted(Current.AgencyId, episodeId, patientId, eventId, isDeprecated);
        }

        public bool MarkPlanOfCareStandAloneAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated)
        {
            return oasisDataProvider.PlanofCareRepository.MarkStandAloneAsDeleted(Current.AgencyId, episodeId, patientId, eventId, isDeprecated);
        }

        public bool ReassignPlanOfCaresUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        {
            return oasisDataProvider.PlanofCareRepository.ReassignUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
        }

        public bool UpdateAssessmentStatus(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string status) {
            bool result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, Id, assessmentType);
            if (assessment != null) {
                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                if (episode != null) {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().ToList();
                    if (scheduleEvents != null && scheduleEvents.Count > 0) {
                        var scheduleEvent = scheduleEvents.Where(s => s.EpisodeId == episodeId && s.PatientId == patientId && s.EventId == Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, Id);
                        if (scheduleEvent != null) {
                            if (status == ((int)ScheduleStatus.OasisReopened).ToString() && assessment.Status == (int)ScheduleStatus.OasisExported) assessment.VersionNumber += 1;
                            if (status == ((int)ScheduleStatus.OasisReopened).ToString() || status == ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString()) {
                                assessment.SignatureText = string.Empty;
                                assessment.SignatureDate = DateTime.MinValue;
                            }
                            assessment.Status = int.Parse(status);
                            if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview && Current.HasRight(Permissions.BypassCaseManagement)) assessment.Status = (int)ScheduleStatus.OasisCompletedExportReady;
                            if (scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate()) assessment.AssessmentDate = scheduleEvent.EventDate.ToDateTime();
                            if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment)) {
                                if (assessment.Type == AssessmentType.Recertification || assessment.Type == AssessmentType.NonOasisRecertification) {
                                    if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview || assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady) patientRepository.SetRecertFlag(Current.AgencyId, episodeId, patientId, true);
                                    else patientRepository.SetRecertFlag(Current.AgencyId, episodeId, patientId, false);
                                }
                                if (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady) scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = assessment.Status.ToString();
                                scheduleEvent.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent)) {
                                    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, scheduleEvent.EventId);
                                    if (userEvent != null) {
                                        userEvent.Status = scheduleEvent.Status;
                                        userEvent.VisitDate = scheduleEvent.VisitDate;
                                        userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                        result = true;
                                    } else {
                                        userEvent = new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason };
                                        userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, userEvent);
                                        result = true;
                                    }
                                    if ((status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady && Current.HasRight(Permissions.BypassCaseManagement))) && (assessment.Type == AssessmentType.StartOfCare || assessment.Type == AssessmentType.Recertification || (assessment.Type == AssessmentType.ResumptionOfCare && scheduleEvent.EventDate.IsValidDate() && scheduleEvent.EventDate.ToDateTime().Date >= scheduleEvent.EndDate.AddDays(-5).Date && scheduleEvent.EventDate.ToDateTime().Date <= scheduleEvent.EndDate.Date) || assessment.Type == AssessmentType.NonOasisStartOfCare || assessment.Type == AssessmentType.NonOasisRecertification)) {
                                        var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId,assessment.EpisodeId, assessment.Id, assessment.Type.ToString());
                                        if (planofCare == null) {
                                            var isNonOasis = assessment.Type == AssessmentType.NonOasisStartOfCare || assessment.Type == AssessmentType.NonOasisRecertification;
                                            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                                            if (patient != null) {
                                                var pocScheduleEvent = new ScheduleEvent {
                                                    EventId = Guid.NewGuid(),
                                                    EndDate = episode.EndDate,
                                                    EventDate = scheduleEvent.EventDate,
                                                    VisitDate = scheduleEvent.EventDate,
                                                    UserId = scheduleEvent.UserId,
                                                    StartDate = episode.StartDate,
                                                    PatientId = patientId,
                                                    EpisodeId = episodeId,
                                                    Discipline = Disciplines.Orders.ToString(),
                                                    DisciplineTask = isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
                                                    Status = ((int)ScheduleStatus.OrderSaved).ToString(),
                                                    IsOrderForNextEpisode = assessment.Type == AssessmentType.Recertification ? true : false
                                                };
                                                if (!CreatePlanofCare(pocScheduleEvent, patient, assessment, isNonOasis)) result = false;
                                            }
                                        } else if (planofCare != null) {
                                            var planofCareScheduleEvent = scheduleEvents.Where(s => s.EpisodeId == planofCare.EpisodeId && s.PatientId == planofCare.PatientId && s.EventId == planofCare.Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                                            if (planofCareScheduleEvent == null) {
                                                var pocScheduleEvent = new ScheduleEvent {
                                                    EventId = planofCare.Id,
                                                    EndDate = episode.EndDate,
                                                    EventDate = scheduleEvent.EventDate,
                                                    VisitDate = scheduleEvent.EventDate,
                                                    UserId = scheduleEvent.UserId,
                                                    StartDate = episode.StartDate,
                                                    PatientId = patientId,
                                                    EpisodeId = episodeId,
                                                    Discipline = Disciplines.Orders.ToString(),
                                                    DisciplineTask = (assessment.Type == AssessmentType.NonOasisStartOfCare || assessment.Type == AssessmentType.NonOasisRecertification) ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
                                                    Status = ((int)ScheduleStatus.OrderSaved).ToString(),
                                                    IsOrderForNextEpisode = assessment.Type == AssessmentType.Recertification ? true : false
                                                };
                                                scheduleEvents.Add(pocScheduleEvent);
                                                episode.Schedule = scheduleEvents.ToXml();
                                                if (patientRepository.UpdateEpisode(episode)) {
                                                    result = true;
                                                    Auditor.Log(pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.EventId, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
                                                }
                                            } else result = true;
                                        }
                                    }
                                    if (scheduleEvent.Status.IsInteger()) Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateAssessmentCorrectionNumber(Guid Id, Guid patientId, Guid episodeId, string assessmentType, int correctionNumber)
        {
            bool result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, Id, assessmentType);
            if (assessment != null && assessment.SubmissionFormat.IsNotNullOrEmpty() && assessment.SubmissionFormat.Length >= 1446)
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, Id);
                if (scheduleEvent != null)
                {
                    assessment.VersionNumber = correctionNumber;
                    assessment.SubmissionFormat = assessment.SubmissionFormat.Trim().Remove(12, 2).Insert(12, correctionNumber.ToString().PadLeft(2, '0'));
                    if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                    {
                        if (scheduleEvent.Status.IsInteger())
                        {
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.Edit, (DisciplineTasks)scheduleEvent.DisciplineTask,"The correction number of the assessment edited.");
                        }
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateAssessmentStatusForSubmit(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string gcode, string status, string signature, DateTime date, string timeIn, string timeOut)
        {
            bool result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, Id, assessmentType);
            if (assessment != null)
            {
                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().ToList();
                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                    {
                        var scheduleEvent = scheduleEvents.Where(s => s.EpisodeId == episodeId && s.PatientId == patientId && s.EventId == Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, Id);
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.GCode = gcode;
                            assessment.Status = int.Parse(status);
                            if (Current.HasRight(Permissions.BypassCaseManagement))
                            {
                                assessment.Status = (int)ScheduleStatus.OasisCompletedExportReady;
                            }
                            assessment.SignatureDate = date;
                            assessment.SignatureText = signature;
                            assessment.TimeIn = timeIn;
                            assessment.TimeOut = timeOut;
                            if (scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate())
                            {
                                assessment.AssessmentDate = scheduleEvent.EventDate.ToDateTime();
                            }
                            if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                            {
                                if (assessment.Type == AssessmentType.TransferInPatientNotDischarged)
                                {
                                    var assessmentData = assessment.ToDictionary();
                                    var hospitalizationData = assessment.GetHospitalizationData();
                                    var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, assessment.Id);
                                    if (hospitalizationLog == null)
                                    {
                                        hospitalizationLog = new HospitalizationLog()
                                        {
                                            Id = assessment.Id,
                                            PatientId = patientId,
                                            EpisodeId = episodeId,
                                            UserId = Current.UserId,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            AgencyId = Current.AgencyId,
                                            SourceId = (int)TransferSourceTypes.Oasis,
                                            Data = hospitalizationData != null ? hospitalizationData.ToXml() : new List<Question>().ToXml(),
                                            HospitalizationDate = assessmentData != null && assessmentData.AnswerOrEmptyString("M0906DischargeDate").IsNotNullOrEmpty() ? assessmentData.AnswerOrEmptyString("M0906DischargeDate").ToDateTime() : DateTime.MinValue,
                                            LastHomeVisitDate = assessmentData != null && assessmentData.AnswerOrEmptyString("M0903LastHomeVisitDate").IsNotNullOrEmpty() ? assessmentData.AnswerOrEmptyString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue
                                        };
                                        patientRepository.AddHospitalizationLog(hospitalizationLog);
                                    }
                                    else
                                    {
                                        hospitalizationLog.EpisodeId = episodeId;
                                        hospitalizationLog.UserId = Current.UserId;
                                        hospitalizationLog.Modified = DateTime.Now;
                                        hospitalizationLog.Data = hospitalizationData != null ? hospitalizationData.ToXml() : new List<Question>().ToXml();
                                        hospitalizationLog.HospitalizationDate = assessmentData != null && assessmentData.AnswerOrEmptyString("M0906DischargeDate").IsNotNullOrEmpty() ? assessmentData.AnswerOrEmptyString("M0906DischargeDate").ToDateTime() : DateTime.MinValue;
                                        hospitalizationLog.LastHomeVisitDate = assessmentData != null && assessmentData.AnswerOrEmptyString("M0903LastHomeVisitDate").IsNotNullOrEmpty() ? assessmentData.AnswerOrEmptyString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue;

                                        patientRepository.UpdateHospitalizationLog(hospitalizationLog);
                                    }

                                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                                    if (patient != null)
                                    {
                                        patient.IsHospitalized = true;
                                        patient.HospitalizationId = hospitalizationLog.Id;
                                        patientRepository.Update(patient);
                                    }
                                }

                                if (assessment.Type == AssessmentType.Recertification || assessment.Type == AssessmentType.StartOfCare)
                                {
                                    if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview || assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady || assessment.Status == (int)ScheduleStatus.OasisExported || assessment.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                    {
                                        patientRepository.SetRecertFlag(Current.AgencyId, episodeId, patientId, true);
                                    }
                                    else
                                    {
                                        patientRepository.SetRecertFlag(Current.AgencyId, episodeId, patientId, false);
                                    }
                                }

                                scheduleEvent.Status = assessment.Status.ToString();
                                scheduleEvent.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
                                scheduleEvent.TimeIn = timeIn;
                                scheduleEvent.TimeOut = timeOut;
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                {
                                    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, scheduleEvent.EventId);
                                    if (userEvent != null)
                                    {
                                        userEvent.Status = scheduleEvent.Status;
                                        userEvent.VisitDate = scheduleEvent.VisitDate;
                                        userEvent.TimeIn = timeIn;
                                        userEvent.TimeOut = timeOut;
                                        userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                        result = true;
                                    }
                                    else
                                    {
                                        userEvent = new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason };
                                        userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, userEvent);
                                        result = true;
                                    }

                                    if ((status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()|| (assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady && Current.HasRight(Permissions.BypassCaseManagement))) 
                                        &&(assessment.Type == AssessmentType.StartOfCare
                                        || assessment.Type == AssessmentType.Recertification
                                        || (assessment.Type == AssessmentType.ResumptionOfCare && scheduleEvent.EventDate.IsValidDate() && scheduleEvent.EventDate.ToDateTime().Date >= scheduleEvent.EndDate.AddDays(-5).Date && scheduleEvent.EventDate.ToDateTime().Date <= scheduleEvent.EndDate.Date)
                                        || assessment.Type == AssessmentType.NonOasisStartOfCare
                                        || assessment.Type == AssessmentType.NonOasisRecertification))
                                    {
                                        var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, assessment.EpisodeId, assessment.Id, assessment.Type.ToString());
                                        if (planofCare == null)
                                        {
                                            var isNonOasis = assessment.Type == AssessmentType.NonOasisStartOfCare || assessment.Type == AssessmentType.NonOasisRecertification;
                                            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                                            if (patient != null)
                                            {
                                                var pocScheduleEvent = new ScheduleEvent
                                                {
                                                    EventId = Guid.NewGuid(),
                                                    EndDate = episode.EndDate,
                                                    EventDate = scheduleEvent.EventDate,
                                                    VisitDate = scheduleEvent.EventDate,
                                                    UserId = scheduleEvent.UserId,
                                                    StartDate = episode.StartDate,
                                                    PatientId = patientId,
                                                    EpisodeId = episodeId,
                                                    Discipline = Disciplines.Orders.ToString(),
                                                    DisciplineTask = isNonOasis ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
                                                    Status = ((int)ScheduleStatus.OrderSaved).ToString(),
                                                    IsOrderForNextEpisode = assessment.Type == AssessmentType.Recertification ? true : false
                                                };

                                                if (!CreatePlanofCare(pocScheduleEvent, patient, assessment, isNonOasis))
                                                {
                                                    result = false;
                                                }
                                            }
                                        }
                                        else if (planofCare != null)
                                        {
                                            var planofCareScheduleEvent = scheduleEvents.Where(s => s.EpisodeId == planofCare.EpisodeId && s.PatientId == planofCare.PatientId && s.EventId == planofCare.Id).FirstOrDefault();// patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                                            if (planofCareScheduleEvent == null)
                                            {
                                                var pocScheduleEvent = new ScheduleEvent
                                                {
                                                    EventId = planofCare.Id,
                                                    EndDate = episode.EndDate,
                                                    EventDate = scheduleEvent.EventDate,
                                                    VisitDate = scheduleEvent.EventDate,
                                                    UserId = scheduleEvent.UserId,
                                                    StartDate = episode.StartDate,
                                                    PatientId = patientId,
                                                    EpisodeId = episodeId,
                                                    Discipline = Disciplines.Orders.ToString(),
                                                    DisciplineTask = (assessment.Type == AssessmentType.NonOasisStartOfCare || assessment.Type == AssessmentType.NonOasisRecertification) ? (int)DisciplineTasks.NonOasisHCFA485 : (int)DisciplineTasks.HCFA485,
                                                    Status = ((int)ScheduleStatus.OrderSaved).ToString(),
                                                    IsOrderForNextEpisode = assessment.Type == AssessmentType.Recertification ? true : false
                                                };
                                                scheduleEvents.Add(pocScheduleEvent);
                                                episode.Schedule = scheduleEvents.ToXml();
                                                if (patientRepository.UpdateEpisode(episode))
                                                {
                                                    result = true;
                                                    Auditor.Log(pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.EventId, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);
                                                }

                                            }
                                            else
                                            {
                                                result = true;
                                            }
                                        }
                                    }

                                    if (scheduleEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateAssessmentForDetail(ScheduleEvent schedule)
        {
            bool result = false;
            if (schedule != null)
            {
                var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, schedule.EventId, ((DisciplineTasks)schedule.DisciplineTask).ToString());
                if (assessment != null)
                {
                    assessment.TimeIn = schedule.TimeIn;
                    assessment.TimeOut = schedule.TimeOut;
                    if (schedule.EventDate.IsNotNullOrEmpty() && schedule.EventDate.IsValidDate())
                    {
                        assessment.AssessmentDate = schedule.EventDate.ToDateTime();
                    }
                    assessment.IsDeprecated = schedule.IsDeprecated;
                    assessment.EpisodeId = schedule.EpisodeId;
                    assessment.UserId = schedule.UserId;
                    assessment.Status = schedule.Status.IsNotNullOrEmpty() && schedule.Status.IsInteger() ? schedule.Status.ToInteger() : assessment.Status;
                    result = oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment);
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdatePlanofCareStatus(Guid episodeId, Guid patientId, Guid eventId, string actionType)
        {
            var result = false;
            var shouldUpdateEpisode = false;
            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PlanofCare planofCare = null;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null) userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                var description = string.Empty;
                var planofCareStandAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
                if (planofCareStandAlone != null)
                {
                    if (actionType == "Approve")
                    {
                        planofCareStandAlone.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                        if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                shouldUpdateEpisode = true;
                                description = "Returned By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (actionType == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }
                }
                else
                {
                    planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
                    if (planofCare != null)
                    {
                        if (actionType == "Approve")
                        {
                            planofCare.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.InPrintQueue = true;
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                    if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                    shouldUpdateEpisode = true;
                                    description = "Approved By:" + Current.UserFullName;
                                }
                            }
                        }
                        else if (actionType == "Return")
                        {
                            planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    if (userEvent != null) userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    shouldUpdateEpisode = true;
                                    description = "Returned By:" + Current.UserFullName;
                                }
                            }
                        }
                        else if (actionType == "Print")
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = false;
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                }
                if (shouldUpdateEpisode)
                {
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                    {
                        if (userEvent != null)
                        {
                            if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                        }
                        else
                        {
                            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                            result = true;
                        }
                        if (scheduleEvent.Status.IsInteger()) Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                    }
                }
            }
            return result;
        }

        public bool MarkAsExportedOrCompleted(List<string> OasisSelected, int status)
        {
            bool result = false;
            if (OasisSelected != null && OasisSelected.Count > 0)
            {
                OasisSelected.ForEach(o =>
                {
                    string[] data = o.Split('|');
                    if (data != null && data.Length > 1)
                    {
                        if (data[0].IsGuid() && !data[0].ToGuid().IsEmpty() && data[1].IsNotNullOrEmpty())
                        {
                            var assessment = GetAssessment(data[0].ToGuid(), data[1]);
                            if (assessment != null)
                            {
                                assessment.Status = status;
                                assessment.ExportedDate = DateTime.Now;
                                if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                {
                                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, assessment.EpisodeId, assessment.PatientId, assessment.Id);
                                    if (scheduleEvent != null)
                                    {
                                        scheduleEvent.Status = status.ToString();
                                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                        {
                                            var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, assessment.Id);
                                            if (userEvent != null)
                                            {
                                                userEvent.Status = status.ToString();
                                                userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                                result = true;
                                            }
                                            else
                                            {
                                                userEvent = new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason };
                                                userRepository.AddUserEvent(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.UserId, userEvent);
                                                result = true;
                                            }
                                            if (scheduleEvent.Status.IsInteger())
                                            {
                                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                });
            }
            return result;
        }

        public OasisAudit Audit(Guid assessmentId, Guid patientId, Guid episodeId, string assessmentType)
        {
            var audit = new OasisAudit();
            audit.LogicalErrors = new List<Axxess.Api.Contracts.LogicalError>();
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                if (agency != null)
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var assessment = GetAssessment(assessmentId, assessmentType);
                        if (assessment != null)
                        {
                            var patientLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                            if (patientLocation != null)
                            {
                                if (!patientLocation.IsLocationStandAlone)
                                {
                                    patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                    patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                    patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                }
                                var assessmentQuestions = assessment.ToDictionary();
                                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                {
                                    if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                                    {
                                        assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                                    }
                                    else
                                    {
                                        assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                                    }

                                    if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                                    {
                                        assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                                    }
                                    else
                                    {
                                        assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                                    }

                                    //var submissionBodyFormat = GetOasisSubmissionFormatInstructionsNew();
                                    var oasisFormatString = GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                    if (oasisFormatString!=null && oasisFormatString.IsNotNullOrEmpty())
                                    {
                                        audit.LogicalErrors = validationAgent.LogicalInconsistencyCheck(oasisFormatString);
                                    }
                                }
                                audit.Assessment = assessment;
                                audit.Agency = agency;
                                audit.Location = patientLocation;
                                audit.Patient = patient;
                            }
                        }
                    }
                }
            }
            return audit;
        }

        public ValidationInfoViewData Validate(Guid assessmentId, Guid patientId, Guid episodeId, string assessmentType)
        {
            var validationInfo = new ValidationInfoViewData();
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                var assessment = GetAssessment(assessmentId, assessmentType);
                if (assessment != null)
                {
                    validationInfo.AssessmentId = assessmentId;
                    validationInfo.AssessmentType = assessmentType;
                    validationInfo.EpisodeId = episodeId;
                    validationInfo.PatientId = patientId;
                    var validationErrors = new List<Axxess.Api.Contracts.ValidationError>();
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            var schedules = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (schedules != null && schedules.Count > 0)
                            {
                                var scheduleEvent = schedules.FirstOrDefault(s=>s.EpisodeId==episode.Id && s.EventId==assessment.Id);
                                if (scheduleEvent != null)
                                {
                                    validationInfo.Version = scheduleEvent.Version;
                                    validationInfo.Status = scheduleEvent.Status;
                                    var patientLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                                    if (patientLocation != null)
                                    {
                                        if (!patientLocation.IsLocationStandAlone)
                                        {
                                            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                                            if (agency != null)
                                            {
                                                patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                                patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                                patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                            }
                                            
                                        }
                                        bool medicareOrMedicareHMO = false;
                                        int primaryInsurance=patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger()?patient.PrimaryInsurance.ToInteger():0;
                                        if (primaryInsurance > 0 && primaryInsurance < 1000)
                                        {
                                            medicareOrMedicareHMO = true;
                                        }
                                        else
                                        {
                                            var patientInsurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
                                            if (patientInsurance != null)
                                            {
                                                if (patientInsurance.PayorType == 2)
                                                {
                                                    medicareOrMedicareHMO = true;
                                                }
                                            }
                                        }
                                        if (medicareOrMedicareHMO)
                                        {
                                            if (scheduleEvent.GCode.IsNotNullOrEmpty())
                                            {
                                                validationInfo.GCode = scheduleEvent.GCode;
                                            }
                                            else
                                            {
                                                var agencyChargeRates = new List<ChargeRate>();
                                                if (patientLocation.IsLocationStandAlone)
                                                {
                                                    if (patientLocation.BillData.IsNotNullOrEmpty())
                                                    {
                                                        agencyChargeRates = patientLocation.BillData.ToObject<List<ChargeRate>>();
                                                    }
                                                }
                                                else
                                                {
                                                    var mainLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                                    if (mainLocation != null && mainLocation.BillData.IsNotNullOrEmpty())
                                                    {
                                                        agencyChargeRates = mainLocation.BillData.ToObject<List<ChargeRate>>();
                                                    }
                                                }
                                                var rate = agencyChargeRates.SingleOrDefault(r => r.Id == scheduleEvent.DisciplineTask);
                                                if (rate != null && rate.Code.IsNotNullOrEmpty())
                                                {
                                                    validationInfo.GCode = rate.Code;
                                                }
                                                else
                                                {
                                                    var disciplineTasks = lookupRepository.DisciplineTasks() ?? new List<DisciplineTask>();
                                                    var data = disciplineTasks.SingleOrDefault(d => d.Id == scheduleEvent.DisciplineTask);
                                                    if (data != null)
                                                    {
                                                        validationInfo.GCode = data.GCode;
                                                    }
                                                }
                                            }
                                        }
                                        var assessmentQuestions = assessment.ToDictionary();
                                        if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                        {
                                            if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                                            {
                                                assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                                            }
                                            else
                                            {
                                                assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                                            }

                                            if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                                            {
                                                assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                                            }
                                            else
                                            {
                                                assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                                            }

                                            var oasisFormatString = this.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                            if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                                            {
                                                validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                                if (validationErrors.Count > 0)
                                                {
                                                    validationErrors.RemoveAt(0);
                                                }
                                                validationErrors.AddRange(CustomValidation(assessmentQuestions));
                                                if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString())
                                                {
                                                    var signedMedicationProfile = patientRepository.GetSignedMedicationAssocatiedToAssessment(assessment.PatientId, assessment.Id);
                                                    if (signedMedicationProfile == null)
                                                    {
                                                        validationErrors.Add(new Axxess.Api.Contracts.ValidationError() { ErrorDup = "M0001", ErrorType = "WARNING", Description = "WARNING: You have not signed the medication profile" });
                                                    }
                                                }
                                                int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                                if (error == 0)
                                                {
                                                    validationInfo.EpisodeStartDate = episode.StartDate;
                                                    validationInfo.EpisodeEndDate = episode.EndDate;
                                                    validationInfo.TimeIn = assessment.TimeIn;
                                                    validationInfo.TimeOut = assessment.TimeOut;
                                                    validationInfo.IsErrorFree = true;
                                                    if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                                                    {
                                                        var hipps = grouperAgent.GetHippsCode(oasisFormatString);
                                                        if (hipps != null)
                                                        {
                                                            if (hipps.ClaimMatchingKey != string.Empty && hipps.ClaimMatchingKey.Length == 18 && hipps.Code != string.Empty && hipps.Code.Length == 5 && hipps.Version != string.Empty && hipps.Version.Length == 5)
                                                            {
                                                                var hippsCode = assessment.Questions.Find(q => q.Name == "HIPPSCODE");
                                                                if (hippsCode == null)
                                                                {
                                                                    assessment.Questions.Add(new Question { Name = "HIPPSCODE", Answer = hipps.Code });
                                                                }
                                                                else
                                                                {
                                                                    hippsCode.Answer = hipps.Code;
                                                                }
                                                                var hippsVersion = assessment.Questions.Find(q => q.Name == "HIPPSVERSION");
                                                                if (hippsVersion == null)
                                                                {
                                                                    assessment.Questions.Add(new Question { Name = "HIPPSVERSION", Answer = hipps.Version });
                                                                }
                                                                else
                                                                {
                                                                    hippsVersion.Answer = hipps.Version;
                                                                }
                                                            }

                                                            var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                                            var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                                            assessment.HippsCode = hipps.Code;
                                                            assessment.HippsVersion = hipps.Version;
                                                            assessment.ClaimKey = hipps.ClaimMatchingKey;
                                                            assessment.SubmissionFormat = oasisFormatStringComplete;
                                                            assessment.IsValidated = true;
                                                            if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                            {
                                                                //if (scheduleEvent.Status != ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() && scheduleEvent.Status != ((int)ScheduleStatus.OasisCompletedExportReady).ToString())
                                                                //{
                                                                validationInfo.HIPPSCODE = hipps.Code;
                                                                validationInfo.HIPPSKEY = hipps.ClaimMatchingKey;
                                                                var currentEpisode = new PatientEpisode();
                                                                var rap = new Rap();
                                                                if (assessmentType.IsEqual(AssessmentType.StartOfCare.ToString()))
                                                                {
                                                                    currentEpisode = episode;
                                                                    rap = billingRepository.GetRap(Current.AgencyId, patientId, episode.Id);
                                                                }
                                                                else if (assessmentType.IsEqual(AssessmentType.Recertification.ToString()))
                                                                {
                                                                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => !e.IsDeprecated && !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= episode.EndDate && e.EventDate.ToDateTime().Date >= episode.StartDate.Date).OrderByDescending(e => e.EventDate.ToDateTime().Date).ToList();
                                                                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                                                                    {
                                                                        var assessmentEvent = scheduleEvents.FirstOrDefault(e => e.EventId == assessmentId);
                                                                        if (assessmentEvent != null)
                                                                        {
                                                                            if (assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5))
                                                                            {
                                                                                bool ifLastAssessment = !scheduleEvents.Exists(s => !s.IsMissedVisit && s.IsResumptionofCareAssessment() && s.EventDate.ToDateTime().Date > assessmentEvent.EventDate.ToDateTime().Date && s.IsDeprecated == false);
                                                                                if (ifLastAssessment)
                                                                                {
                                                                                    currentEpisode = patientRepository.GetEpisodeLeanByStartDate(Current.AgencyId, patientId, episode.EndDate.AddDays(1));
                                                                                    if (currentEpisode != null)
                                                                                    {
                                                                                        rap = billingRepository.GetRap(Current.AgencyId, patientId, currentEpisode.Id);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else if (assessmentType.IsEqual(AssessmentType.ResumptionOfCare.ToString()))
                                                                {
                                                                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => !e.IsDeprecated && !e.EventId.IsEmpty() && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= episode.EndDate && e.EventDate.ToDateTime().Date >= episode.StartDate.Date).OrderByDescending(e => e.EventDate.ToDateTime().Date).ToList();
                                                                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                                                                    {
                                                                        var assessmentEvent = scheduleEvents.FirstOrDefault(e => e.EventId == assessmentId);
                                                                        if (assessmentEvent != null)
                                                                        {
                                                                            if (assessmentEvent.EventDate.ToDateTime().Date <= episode.EndDate && assessmentEvent.EventDate.ToDateTime().Date >= episode.EndDate.AddDays(-5))
                                                                            {
                                                                                bool ifLastAssessment = !scheduleEvents.Exists(s => !s.IsMissedVisit && s.IsRecertificationAssessment() && s.EventDate.ToDateTime().Date > assessmentEvent.EventDate.ToDateTime().Date && s.IsDeprecated == false);
                                                                                if (ifLastAssessment)
                                                                                {
                                                                                    currentEpisode = patientRepository.GetEpisodeLeanByStartDate(Current.AgencyId, patientId, episode.EndDate.AddDays(1));
                                                                                    if (currentEpisode != null)
                                                                                    {
                                                                                        rap = billingRepository.GetRap(Current.AgencyId, patientId, currentEpisode.Id);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if (currentEpisode != null && currentEpisode.StartDate.IsValid())
                                                                {
                                                                    var prospectivePayment = Math.Round(lookupService.GetProspectivePaymentAmount(hipps.Code, currentEpisode.StartDate, patient != null && patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty, patientLocation.AddressZipCode, patientLocation.AddressStateCode));
                                                                    validationInfo.StandardPaymentRate = prospectivePayment;
                                                                    if (rap != null && !rap.Id.IsEmpty() && (rap.Status == (int)BillingStatus.ClaimCreated || rap.Status == (int)BillingStatus.ClaimReOpen))
                                                                    {
                                                                        rap.ClaimKey = hipps.ClaimMatchingKey;
                                                                        rap.HippsCode = hipps.Code;

                                                                        if (assessmentType.IsEqual(AssessmentType.StartOfCare.ToString()))
                                                                        {
                                                                            rap.ProspectivePay = 0.6 * prospectivePayment;
                                                                        }
                                                                        else
                                                                        {
                                                                            rap.ProspectivePay = 0.5 * prospectivePayment;
                                                                        }

                                                                        billingRepository.UpdateRap(rap);
                                                                    }
                                                                }
                                                                if (hipps.Code.IsNotNullOrEmpty())
                                                                {
                                                                    var hhrg = lookupRepository.GetHHRGByHIPPSCODE(hipps.Code.Trim());
                                                                    validationInfo.HHRG = hhrg != null ? hhrg.HHRG : string.Empty;
                                                                }
                                                                // }
                                                                validationInfo.Count = validationErrors.Count;
                                                                validationInfo.ValidationErrors = validationErrors;
                                                                validationInfo.Message = "Your HIPPS Code generated successfully.";
                                                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                            }
                                                        }
                                                    }
                                                    else if (assessmentType == AssessmentType.DischargeFromAgency.ToString() || assessmentType == AssessmentType.DischargeFromAgencyDeath.ToString() || assessmentType == AssessmentType.TransferInPatientDischarged.ToString() || assessmentType == AssessmentType.TransferInPatientNotDischarged.ToString())
                                                    {
                                                        assessment.SubmissionFormat = oasisFormatString;
                                                        assessment.IsValidated = true;
                                                        if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                        {
                                                            validationInfo.Count = validationErrors.Count;
                                                            validationInfo.ValidationErrors = validationErrors;
                                                            validationInfo.Message = "Your OASIS assessment was successfully saved.";
                                                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                                                    {
                                                        assessment.ClaimKey = string.Empty;
                                                        assessment.HippsCode = string.Empty;
                                                        assessment.HippsVersion = string.Empty;
                                                        assessment.SubmissionFormat = string.Empty;
                                                        assessment.IsValidated = false;
                                                        if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                        {
                                                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not error free");
                                                        }
                                                    }
                                                    else if (assessmentType == AssessmentType.DischargeFromAgency.ToString() || assessmentType == AssessmentType.DischargeFromAgencyDeath.ToString() || assessmentType == AssessmentType.TransferInPatientDischarged.ToString() || assessmentType == AssessmentType.TransferInPatientNotDischarged.ToString())
                                                    {
                                                        assessment.SubmissionFormat = string.Empty;
                                                        assessment.IsValidated = false;
                                                        if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                        {
                                                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not error free");
                                                        }
                                                    }
                                                    validationInfo.Count = validationErrors.Count;
                                                    validationInfo.ValidationErrors = validationErrors.OrderBy(m => m.ErrorType).ToList();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return validationInfo;
        }

        public bool Validate(Assessment assessment)
        {
            var result = false;
            if (assessment != null)
            {
                var validationErrors = new List<Axxess.Api.Contracts.ValidationError>();
                var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, assessment.EpisodeId, assessment.PatientId);
                    if (episode != null)
                    {
                        var schedules = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedules != null && schedules.Count > 0)
                        {
                            var scheduleEvent = schedules.FirstOrDefault(s => s.EpisodeId == episode.Id && s.EventId == assessment.Id);
                            if (scheduleEvent != null)
                            {
                                var patientLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                                if (patientLocation != null)
                                {
                                    if (!patientLocation.IsLocationStandAlone)
                                    {
                                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                                        if (agency != null)
                                        {
                                            patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                                            patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                                            patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                                        }
                                    }
                                    var assessmentQuestions = assessment.ToDictionary();
                                    if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                                    {
                                        if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                                        {
                                            assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                                        }
                                        else
                                        {
                                            assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                                        }

                                        if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                                        {
                                            assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                                        }
                                        else
                                        {
                                            assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                                        }
                                        var oasisFormatString = this.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                                        if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                                        {
                                            validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                            if (validationErrors.Count > 0)
                                            {
                                                validationErrors.RemoveAt(0);
                                            }
                                            validationErrors.AddRange(CustomValidation(assessmentQuestions));
                                            int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                            if (error == 0)
                                            {
                                                if (assessment.TypeName == AssessmentType.StartOfCare.ToString() || assessment.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessment.TypeName == AssessmentType.Recertification.ToString() || assessment.TypeName == AssessmentType.FollowUp.ToString())
                                                {
                                                    var hipps = grouperAgent.GetHippsCode(oasisFormatString);
                                                    if (hipps != null && hipps.ClaimMatchingKey != string.Empty && hipps.ClaimMatchingKey.Length == 18 && hipps.Code != string.Empty && hipps.Code.Length == 5 && hipps.Version != string.Empty && hipps.Version.Length == 5)
                                                    {
                                                        var hippsCode = assessment.Questions.Find(q => q.Name == "HIPPSCODE");
                                                        if (hippsCode == null)
                                                        {
                                                            assessment.Questions.Add(new Question { Name = "HIPPSCODE", Answer = hipps.Code });
                                                        }
                                                        else
                                                        {
                                                            hippsCode.Answer = hipps.Code;
                                                        }
                                                        var hippsVersion = assessment.Questions.Find(q => q.Name == "HIPPSVERSION");
                                                        if (hippsVersion == null)
                                                        {
                                                            assessment.Questions.Add(new Question { Name = "HIPPSVERSION", Answer = hipps.Version });
                                                        }
                                                        else
                                                        {
                                                            hippsVersion.Answer = hipps.Version;
                                                        }
                                                    }

                                                    var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                                    var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                                    assessment.HippsCode = hipps.Code;
                                                    assessment.HippsVersion = hipps.Version;
                                                    assessment.ClaimKey = hipps.ClaimMatchingKey;
                                                    assessment.SubmissionFormat = oasisFormatStringComplete;
                                                    assessment.IsValidated = true;
                                                    if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                    {
                                                        result = true;
                                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                    }
                                                }
                                                else if (assessment.TypeName == AssessmentType.DischargeFromAgency.ToString() || assessment.TypeName == AssessmentType.DischargeFromAgencyDeath.ToString() || assessment.TypeName == AssessmentType.TransferInPatientDischarged.ToString() || assessment.TypeName == AssessmentType.TransferInPatientNotDischarged.ToString())
                                                {

                                                    assessment.SubmissionFormat = oasisFormatString;
                                                    assessment.IsValidated = true;
                                                    if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                    {
                                                        result = true;
                                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Error free");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (assessment.TypeName == AssessmentType.StartOfCare.ToString() || assessment.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessment.TypeName == AssessmentType.Recertification.ToString() || assessment.TypeName == AssessmentType.FollowUp.ToString())
                                                {
                                                    assessment.ClaimKey = string.Empty;
                                                    assessment.HippsCode = string.Empty;
                                                    assessment.HippsVersion = string.Empty;
                                                    assessment.SubmissionFormat = string.Empty;
                                                    assessment.IsValidated = false;
                                                    if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                    {
                                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not Error free");
                                                    }

                                                }
                                                else if (assessment.TypeName == AssessmentType.DischargeFromAgency.ToString() || assessment.TypeName == AssessmentType.DischargeFromAgencyDeath.ToString() || assessment.TypeName == AssessmentType.TransferInPatientDischarged.ToString() || assessment.TypeName == AssessmentType.TransferInPatientNotDischarged.ToString())
                                                {
                                                    assessment.SubmissionFormat = string.Empty;
                                                    assessment.IsValidated = false;
                                                    if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
                                                    {
                                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.AssessmentCheckError, (DisciplineTasks)scheduleEvent.DisciplineTask, "Not Error free");
                                                    }
                                                }
                                                result = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool Validate(Assessment assessment, out Assessment assessmentOut)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
            if (patient != null)
            {
                var patientLocation = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                if (patientLocation != null)
                {
                    if (!patientLocation.IsLocationStandAlone)
                    {
                        var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                        if (agency != null)
                        {
                            patientLocation.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                            patientLocation.MedicareProviderNumber = agency.MedicareProviderNumber;
                            patientLocation.NationalProviderNumber = agency.NationalProviderNumber;
                        }
                    }
                    var assessmentQuestions = assessment.ToDictionary();
                    if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                    {
                        if (assessmentQuestions.ContainsKey("HIPPSCODE"))
                        {
                            assessmentQuestions["HIPPSCODE"] = new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) };
                        }
                        else
                        {
                            assessmentQuestions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = string.Empty.PadLeft(5) });
                        }

                        if (assessmentQuestions.ContainsKey("HIPPSVERSION"))
                        {
                            assessmentQuestions["HIPPSVERSION"] = new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) };
                        }
                        else
                        {
                            assessmentQuestions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = string.Empty.PadLeft(5) });
                        }
                        var oasisFormatString = this.GetOasisSubmissionFormatNew(assessmentQuestions, assessment.VersionNumber, patientLocation);
                        if (oasisFormatString != null && oasisFormatString.IsNotNullOrEmpty())
                        {
                            var validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                            if (validationErrors.Count > 0)
                            {
                                validationErrors.RemoveAt(0);
                            }
                            validationErrors.AddRange(CustomValidation(assessmentQuestions));
                            int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                            if (error == 0)
                            {
                                if (assessment.TypeName == AssessmentType.StartOfCare.ToString() || assessment.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessment.TypeName == AssessmentType.Recertification.ToString() || assessment.TypeName == AssessmentType.FollowUp.ToString())
                                {
                                    var hipps = grouperAgent.GetHippsCode(oasisFormatString);
                                    if (hipps != null && hipps.ClaimMatchingKey != string.Empty && hipps.ClaimMatchingKey.Length == 18 && hipps.Code != string.Empty && hipps.Code.Length == 5 && hipps.Version != string.Empty && hipps.Version.Length == 5)
                                    {
                                        var hippsCode = assessment.Questions.Find(q => q.Name == "HIPPSCODE");
                                        if (hippsCode == null)
                                        {
                                            assessment.Questions.Add(new Question { Name = "HIPPSCODE", Answer = hipps.Code });
                                        }
                                        else
                                        {
                                            hippsCode.Answer = hipps.Code;
                                        }
                                        var hippsVersion = assessment.Questions.Find(q => q.Name == "HIPPSVERSION");
                                        if (hippsVersion == null)
                                        {
                                            assessment.Questions.Add(new Question { Name = "HIPPSVERSION", Answer = hipps.Version });
                                        }
                                        else
                                        {
                                            hippsVersion.Answer = hipps.Version;
                                        }
                                    }
                                    var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                    var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                    assessment.HippsCode = hipps.Code;
                                    assessment.HippsVersion = hipps.Version;
                                    assessment.ClaimKey = hipps.ClaimMatchingKey;
                                    assessment.SubmissionFormat = oasisFormatStringComplete;
                                    assessment.IsValidated = true;
                                    result = true;

                                }
                                else if (assessment.TypeName == AssessmentType.DischargeFromAgency.ToString() || assessment.TypeName == AssessmentType.DischargeFromAgencyDeath.ToString() || assessment.TypeName == AssessmentType.TransferInPatientDischarged.ToString() || assessment.TypeName == AssessmentType.TransferInPatientNotDischarged.ToString())
                                {
                                    assessment.SubmissionFormat = oasisFormatString;
                                    assessment.IsValidated = true;
                                    result = true;
                                }
                            }
                            else
                            {
                                if (assessment.TypeName == AssessmentType.StartOfCare.ToString() || assessment.TypeName == AssessmentType.ResumptionOfCare.ToString() || assessment.TypeName == AssessmentType.Recertification.ToString() || assessment.TypeName == AssessmentType.FollowUp.ToString())
                                {
                                    assessment.ClaimKey = string.Empty;
                                    assessment.HippsCode = string.Empty;
                                    assessment.HippsVersion = string.Empty;
                                    assessment.SubmissionFormat = string.Empty;
                                    assessment.IsValidated = false;
                                    result = false;
                                }
                                else if (assessment.TypeName == AssessmentType.DischargeFromAgency.ToString() || assessment.TypeName == AssessmentType.DischargeFromAgencyDeath.ToString() || assessment.TypeName == AssessmentType.TransferInPatientDischarged.ToString() || assessment.TypeName == AssessmentType.TransferInPatientNotDischarged.ToString())
                                {
                                    assessment.ClaimKey = string.Empty;
                                    assessment.HippsCode = string.Empty;
                                    assessment.HippsVersion = string.Empty;
                                    assessment.SubmissionFormat = string.Empty;
                                    assessment.IsValidated = false;
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }
            assessmentOut = assessment;
            return result;
        }

        public ValidationInfoViewData ValidateInactivate(Guid assessmentId, string assessmentType)
        {
            var validationInfo = new ValidationInfoViewData();
            validationInfo.isValid = false;
            var validationErrors = new List<Axxess.Api.Contracts.ValidationError>();
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                var assessment = GetAssessment(assessmentId, assessmentType);
                if (assessment != null)
                {
                    var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                    if (patient != null)
                    {
                        validationInfo.AssessmentId = assessmentId;
                        validationInfo.AssessmentType = assessmentType;
                        var assessmentQuestions = assessment.ToDictionary();
                        if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                        {
                            var oasisFormatString = OasisInactivateBody(assessmentQuestions , patient.AgencyLocationId);
                            if (oasisFormatString.IsNotNullOrEmpty() && oasisFormatString.Length == 1446)
                            {
                                validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                                if (validationErrors.Count > 0)
                                {
                                    validationErrors.RemoveAt(0);
                                }
                                int error = validationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count();
                                if (error == 0)
                                {
                                    assessment.CancellationFormat = oasisFormatString;
                                    oasisDataProvider.OasisAssessmentRepository.Update(assessment);
                                    validationInfo.Count = validationErrors.Count;
                                    validationInfo.ValidationErrors = validationErrors;
                                    validationInfo.Message = "Your OASIS Export Cancellation File generated successfully.";
                                    validationInfo.isValid = true;
                                }
                                else
                                {
                                    assessment.CancellationFormat = string.Empty;
                                    oasisDataProvider.OasisAssessmentRepository.Update(assessment);
                                    validationInfo.Count = validationErrors.Count;
                                    validationInfo.ValidationErrors = validationErrors.OrderBy(m => m.ErrorType).ToList();
                                    validationInfo.isValid = false;
                                }
                            }
                            else
                            {
                                validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment submittion format  is not correct. Try again or contact us." });
                                validationInfo.isValid = false;
                                validationInfo.ValidationErrors = validationErrors;
                            }
                        }
                        else
                        {
                            validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment data is not found . Try again or contact us." });
                            validationInfo.isValid = false;
                            validationInfo.ValidationErrors = validationErrors;
                        }
                    }
                    else
                    {
                        validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment information is not found . Try again or contact us." });
                        validationInfo.isValid = false;
                        validationInfo.ValidationErrors = validationErrors;

                    }
                }
                else
                {
                    validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment patient information is not found . Try again or contact us." });
                    validationInfo.isValid = false;
                    validationInfo.ValidationErrors = validationErrors;
                }
            }
            else
            {
                validationErrors.Add(new Axxess.Api.Contracts.ValidationError { ErrorType = "FATAL", ErrorDup = "", Description = "The assessment information is not correct . Try again or contact us." });
                validationInfo.isValid = false;
                validationInfo.ValidationErrors = validationErrors;
            }
            return validationInfo;
        }

        public IDictionary<string, Question> Allergies(Guid assessmentId, string AssessmentType)
        {
            var allergies = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions != null && questions.Count > 0)
                {
                    if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                    {
                        allergies.Add("485Allergies", questions["485Allergies"]);
                    }
                    if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                    {
                        allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
                    }
                }
            }
            return allergies;
        }

        public IDictionary<string, Question> Allergies(Assessment assessment)
        {
            var allergies = new Dictionary<string, Question>();
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions != null && questions.Count > 0)
                {
                    if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                    {
                        allergies.Add("485Allergies", questions["485Allergies"]);
                    }
                    if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                    {
                        allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
                    }
                }
            }
            return allergies;
        }

        public IDictionary<string, Question> Diagnosis(Guid assessmentId, string AssessmentType)
        {
            var diagnosis = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions != null && questions.Count > 0)
                {
                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                    {
                        diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                    }
                    if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                    {
                        diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                    {
                        diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                    {
                        diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                    {
                        diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                    {
                        diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                    {
                        diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                    }
                }

            }
            return diagnosis;
        }

        public IDictionary<string, Question> Diagnosis(Assessment assessment)
        {
            var diagnosis = new Dictionary<string, Question>();
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions != null && questions.Count > 0)
                {
                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                    {
                        diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                    }
                    if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                    {
                        diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                    {
                        diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                    {
                        diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                    {
                        diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                    {
                        diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                    {
                        diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                    {
                        diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                    }
                }

            }
            return diagnosis;
        }

        public IDictionary<string, Question> LocatorQuestions(Assessment assessment)
        {
            IDictionary<string, Question> locators = new Dictionary<string, Question>();
            if (assessment != null)
            {
                var planofCare = new PlanofCare();
                planofCare.Questions = Get485FromAssessment(assessment);
                locators = planofCare.ToDictionary();
            }
            return locators;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, eventId, assessmentType);
                if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                    if (assessment.Questions.Exists(q => string.Format("{0}{1}", q.Type, q.Name) == name))
                    {
                        assessment.Questions.SingleOrDefault(q => string.Format("{0}{1}", q.Type, q.Name) == name).Answer = Guid.Empty.ToString();
                        assessment.OasisData = assessment.Questions.ToXml();
                        if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                        {
                            if (assetService.DeleteAsset(assetId))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                var scheduleEvent = scheduleEvents.FirstOrDefault(e => e.EventId == eventId && e.EpisodeId == episodeId && e.PatientId == patientId);
                                if (scheduleEvent != null && scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0 && scheduleEvent.Assets.Contains(assetId))
                                {
                                    scheduleEvent.Assets.Remove(assetId);
                                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                    {
                                        result = true;
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool DeleteOnlyWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, eventId, assessmentType);
                if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                    if (assessment.Questions.Exists(q => q.Answer == assetId.ToString()))
                    {
                        assessment.Questions.SingleOrDefault(q => q.Answer == assetId.ToString()).Answer = Guid.Empty.ToString();
                        assessment.OasisData = assessment.Questions.ToXml();
                        if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                        {
                            result = true;
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            var result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, eventId, assessmentType);
            if (assessment != null)
            {
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    supply.UniqueIdentifier = Guid.NewGuid();
                }
                var supplies = new List<Supply>();
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies != null && supplies.Count > 0)
                    {
                        supplies.Add(supply);

                    }
                    else
                    {
                        supplies = new List<Supply> { supply };
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                assessment.Supply = supplies.ToXml();
                if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                {
                    result = true;
                }
            }

            return result;
        }

        public bool UpdateSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            var result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, eventId, assessmentType);
            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            editSupply.UnitCost = supply.UnitCost;
                            editSupply.Description = supply.Description;
                            editSupply.DateForEdit = supply.DateForEdit;
                            editSupply.Code = supply.Code;
                            assessment.Supply = supplies.ToXml();
                            if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            var result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, eventId, assessmentType);
            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        assessment.Supply = supplies.ToXml();
                        if (oasisDataProvider.OasisAssessmentRepository.UpdateModal(assessment))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public List<Supply> GetAssessmentSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, episodeId, patientId, eventId, assessmentType);
            var list = new List<Supply>();
            if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
            {
                list = assessment.Supply.ToObject<List<Supply>>();
            }
            return list;
        }

        public AssessmentPrint GetAssessmentPrint(AssessmentType Type)
        {
            var assessment = new AssessmentPrint();
            assessment.Type = Type;
            assessment.Location = agencyRepository.GetMainLocation(Current.AgencyId);
            return assessment;
        }

        public AssessmentPrint GetAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return GetAssessmentPrint(Current.AgencyId, episodeId, patientId, eventId);
        }

        public AssessmentPrint GetAssessmentPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var assessmentPrint = new AssessmentPrint();
            var scheduleEvent = patientRepository.GetScheduleOnly(agencyId, episodeId, patientId, eventId);
            if (scheduleEvent != null)
            {
                assessmentPrint.GCode = scheduleEvent.GCode.IsNotNullOrEmpty() ? scheduleEvent.GCode : string.Empty;
                var taskType = Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask) ? (DisciplineTasks)scheduleEvent.DisciplineTask : DisciplineTasks.NoDiscipline;
                var assessment = oasisAssessmentRepository.GetAssessmentOnly(agencyId, episodeId, patientId, eventId, taskType.ToString());
                if (assessment != null)
                {
                    assessmentPrint.AssessmentDate = assessment.AssessmentDate;
                    assessmentPrint.SignatureText = assessment.SignatureText;
                    assessmentPrint.SignatureDate = assessment.SignatureDate;
                    assessmentPrint.Discipline = scheduleEvent.Discipline;
                    assessmentPrint.Type = assessment.Type;
                    var patient = patientRepository.GetPatientOnly(assessment.PatientId, agencyId);
                    assessmentPrint.Location = agencyRepository.FindLocationOrMain(agencyId, patient != null ? patient.AgencyLocationId : Guid.Empty);
                    var questions = assessment.ToDictionary();
                    assessmentPrint.TimeIn = scheduleEvent.TimeIn;
                    assessmentPrint.TimeOut = scheduleEvent.TimeOut;
                    assessmentPrint.Version = scheduleEvent.Version;
                    if (scheduleEvent.VisitDate.IsValidDate())
                    {
                        assessmentPrint.VisitDate = scheduleEvent.VisitDate.ToDateTime();
                    }
                    var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId);
                    if (allergyProfile != null)
                    {
                        questions.Remove("AllergiesDescription");
                        questions.Add("AllergiesDescription", new Question { Name = "AllergiesDescription", Answer = allergyProfile.ToString(), Type = QuestionType.PlanofCare });
                    }
                    assessmentPrint.Data = questions;
                }
            }
            return assessmentPrint;
        }

        public PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return GetPlanOfCarePrint(episodeId, patientId, eventId, Current.AgencyId);
        }

        public PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId, Guid agencyId)
        {
            var isStandAlone = false;
            PlanofCare planofCare = null;
            var planofCareStandAlone = planofCareRepository.GetStandAlone(agencyId, episodeId, patientId, eventId);
            if (planofCareStandAlone != null)
            {
                isStandAlone = true;
                planofCare = planofCareStandAlone.ToPlanofCare();
            }
            else
            {
                planofCare = planofCareRepository.Get(agencyId, episodeId, patientId, eventId);
            }

            if (planofCare != null && planofCare.Data.IsNotNullOrEmpty())
            {
                planofCare.EpisodeId = episodeId;
                var agency = agencyRepository.GetWithBranches(planofCare.AgencyId);
                planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId) ?? new Patient();
                if (!planofCare.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(agencyId, planofCare.EpisodeId, planofCare.PatientId);
                    if (episode != null && !episode.AdmissionId.IsEmpty())
                    {
                        var admission = patientRepository.GetPatientAdmissionDate(agencyId, episode.AdmissionId);
                        if (admission != null && admission.PatientData.IsNotNullOrEmpty() && admission.StartOfCareDate > DateTime.MinValue)
                        {
                            if (planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || planofCare.Status == (int)ScheduleStatus.OrderSavedByPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || planofCare.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                            {
                                patient = admission.PatientData.ToObject<Patient>();
                            }
                            if (patient != null)
                            {
                                patient.StartofCareDate = admission.StartOfCareDate;
                            }
                        }
                    }
                }
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
                if (!isStandAlone)
                {
                    var episodeRange = GetPlanofCareCertPeriod(episodeId, patientId, planofCare.AssessmentId, agencyId);
                    if (episodeRange != null & episodeRange.StartDate != DateTime.MinValue & episodeRange.EndDate != DateTime.MinValue)
                    {
                        if (episodeRange.DateRangeDays > 59)
                        {
                            planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                        }
                        else
                        {
                            planofCare.EpisodeEnd = episodeRange.StartDate.AddDays(59).ToString("MM/dd/yyyy");
                        }
                        planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                    }
                }
                else
                {
                    var answers = planofCare.ToDictionary();
                    if (answers != null)
                    {
                        var episodeAssociatedId = answers.AnswerOrEmptyGuid("EpisodeAssociated");
                        if (!episodeAssociatedId.IsEmpty())
                        {
                            var episode = patientRepository.GetEpisodeOnly(agencyId, episodeAssociatedId, patientId);
                            if (episode != null & episode.StartDate != DateTime.MinValue & episode.EndDate != DateTime.MinValue)
                            {
                                if ((episode.EndDate - episode.StartDate).Days > 59)
                                {
                                    planofCare.EpisodeEnd = episode.EndDateFormatted;
                                }
                                else
                                {
                                    planofCare.EpisodeEnd = episode.StartDate.AddDays(59).ToString("MM/dd/yyyy");
                                }
                                planofCare.EpisodeStart = episode.StartDateFormatted;
                            }
                        }
                    }
                }
                if ((planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || planofCare.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
                {
                }
                else
                {
                    var physician = PhysicianEngine.Get(planofCare.PhysicianId, agencyId);
                    if (physician != null)
                    {
                        planofCare.PhysicianData = physician.ToXml();
                    }
                }
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId);
                if (allergyProfile != null)
                {
                    planofCare.Allergies = allergyProfile.ToString();
                }
                bool ifAllergies = false;
                foreach (Question q in planofCare.Questions)
                {
                    if (q.Name == "AllergiesDescription" && q.Answer.Trim().IsNotNullOrEmpty())
                    {
                        ifAllergies = true;
                        break;
                    }
                }
                if (!ifAllergies)
                {
                    planofCare.Questions.RemoveAll(x => x.Name == "AllergiesDescription");
                    Question allergiesDescription = new Question { Name = "AllergiesDescription", Answer = allergyProfile.ToString(), Type = QuestionType.PlanofCare };
                    planofCare.Questions.Add(allergiesDescription);
                }
            }
            return planofCare;
        }

        private IDictionary<string, NotesQuestion> CombineNoteQuestionsAndOasisQuestions(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            var questions = noteQuestions;
            if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                {
                    noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis"))
                {
                    noteQuestions.Add("PrimaryDiagnosis", oasisQuestions["PrimaryDiagnosis"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                {
                    noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M"))
                {
                    noteQuestions.Add("ICD9M", oasisQuestions["ICD9M"]);
                }
            }

            if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                {
                    noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                {
                    noteQuestions.Add("PrimaryDiagnosis1", oasisQuestions["PrimaryDiagnosis1"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                {
                    noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M1"))
                {
                    noteQuestions.Add("ICD9M1", oasisQuestions["ICD9M1"]);
                }
            }
            return questions;
        }

        public MissedVisit GetMissedVisitPrint()
        {
            var note = new MissedVisit();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId)
        {
            var note = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
            if (note != null)
            {
                note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                note.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    note.PhysicianName = (physician.LastName + ", " + physician.FirstName + " " + physician.MiddleName).Trim();
                    note.PhysicianPhone = physician.PhoneWork;
                    note.PhysicianFax = physician.FaxNumber;
                }
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, note.EpisodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    note.EventDate = scheduleEvent != null && scheduleEvent.EventDate.IsNotNullOrEmpty() ? scheduleEvent.EventDate : string.Empty;
                    note.DisciplineTaskName = scheduleEvent != null && scheduleEvent.DisciplineTaskName.IsNotNullOrEmpty() ? scheduleEvent.DisciplineTaskName : string.Empty;
                }
            }
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint()
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(string type)
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            note.Type = type;
            return note;
        }

        public VisitNoteViewData GetTransportationNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var Note = new VisitNoteViewData();
            Note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (visitNote != null)
            {
                var scheduledEvent = patientRepository.GetScheduleOnly(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    Note.StatusComment = scheduledEvent.StatusComment;
                    var noteQuestions = visitNote.ToDictionary();
                    Note.Questions = noteQuestions;
                    Note.EndDate = scheduledEvent.EndDate;
                    Note.StartDate = scheduledEvent.StartDate;
                    Note.VisitDate = scheduledEvent.VisitDate;
                    Note.DisciplineTask = scheduledEvent.DisciplineTask;
                    Note.PatientId = visitNote.PatientId;
                    Note.EpisodeId = visitNote.EpisodeId;
                    Note.EventId = visitNote.Id;
                    Note.UserId = scheduledEvent.UserId;
                    Note.Type = visitNote.NoteType.IsNotNullOrEmpty() ? visitNote.NoteType.Trim() : string.Empty;
                    Note.TypeName = visitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), visitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), visitNote.NoteType)).GetDescription() : string.Empty;
                    Note.Version = visitNote.Version;
                }
                else Note.Questions = new Dictionary<string, NotesQuestion>();
            }
            else Note.Questions = new Dictionary<string, NotesQuestion>();
            var Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            Note.Patient = Patient;
            return Note;
        }

        public VisitNoteViewData GetVisitNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return GetVisitNotePrint(Current.AgencyId, episodeId, patientId, eventId);
        }

        public VisitNoteViewData GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(agencyId);
            var patientvisitNote = patientRepository.GetVisitNote(agencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId);
                if (allergyProfile != null) note.Allergies = allergyProfile.ToString();
                note.SignatureText = patientvisitNote.SignatureText;
                note.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                note.PhysicianSignatureText = patientvisitNote.PhysicianSignatureText;
                note.PhysicianSignatureDate = patientvisitNote.PhysicianSignatureDate;
                var scheduledEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    note.DisciplineTask = scheduledEvent.DisciplineTask;
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                    
                        var assessment = GetEpisodeAssessment(agencyId, episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            IDictionary<string, NotesQuestion> assementQuestions = null;
                            if (patientvisitNote.IsSkilledNurseNote())
                            {
                                assementQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses | SectionQuestionType.HomeBoundStatus);
                            }
                            else
                            {
                                assementQuestions = assessment.ToDiagnosisQuestionDictionary();
                            }
                            if (patientvisitNote.Note != null)
                            {
                                var noteQuestions = patientvisitNote.ToDictionary();
                                note.Questions = assementQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                            }
                            else
                            {
                                note.Questions = assementQuestions;
                            }
                        }
                        else if (patientvisitNote.Note != null)
                        {
                            note.Questions = patientvisitNote.ToDictionary();
                        }
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if ((noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"].Answer.IsNullOrEmpty())
                            || !noteQuestions.ContainsKey("PrimaryDiagnosis"))
                        {
                            var assessment = GetEpisodeAssessment(agencyId, episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                            if (assessment != null)
                            {
                                assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                var assementQuestions = assessment.ToNotesQuestionDictionary();
                                note.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, assementQuestions);
                            }
                            else
                            {
                                note.Questions = noteQuestions;
                            }
                        }
                        else
                        {
                            note.Questions = noteQuestions;
                        }
                        if (note.DisciplineTask == (int)DisciplineTasks.PASVisit)
                        {
                            if (!noteQuestions.ContainsKey("TravelTimeIn"))
                            {
                                note.Questions.Add("TravelTimeIn", new NotesQuestion { Name = "TravelTimeIn", Answer = scheduledEvent.TravelTimeIn });
                                note.Questions.Add("TravelTimeOut", new NotesQuestion { Name = "TravelTimeOut", Answer = scheduledEvent.TravelTimeOut });
                            }
                        }
                        
                    }
                }
                else if (patientvisitNote.Note != null)
                {
                    note.Questions = patientvisitNote.ToDictionary();
                }
                if (note.DisciplineTask == (int)DisciplineTasks.HHAideVisit)
                {
                    if (scheduledEvent.EventDate.IsValidDate())
                    {
                        IDictionary<string, NotesQuestion> pocQuestions = null;
                        var pocEvent = this.GetCarePlanBySelectedEpisode(agencyId, patientId, episodeId, DisciplineTasks.HHAideCarePlan, out pocQuestions, scheduledEvent.EventDate.ToDateTime());
                        if (pocEvent != null)
                        {
                            if (note.Questions.ContainsKey("HHAFrequency"))
                            {
                                note.Questions.Remove("HHAFrequency");
                                note.Questions.Add("HHAFrequency", pocQuestions["HHAFrequency"]);
                            }
                            if (note.Questions.ContainsKey("PrimaryDiagnosis"))
                            {
                                note.Questions.Remove("PrimaryDiagnosis");
                                note.Questions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                            }
                            if (note.Questions.ContainsKey("PrimaryDiagnosis1"))
                            {
                                note.Questions.Remove("PrimaryDiagnosis1");
                                note.Questions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                            }
                        }
                    }
                }
                note.Questions.Remove("Allergies");
                note.Questions.Add("Allergies", new NotesQuestion { Name = "Allergies", Answer = allergyProfile.ToString() });
                var selectedEpisodeId = note.Questions != null && note.Questions.ContainsKey("SelectedEpisodeId") && note.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? note.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
                if (!selectedEpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(agencyId, selectedEpisodeId, patientId);
                    if (episode != null)
                    {
                        note.EndDate = episode.EndDate;
                        note.StartDate = episode.StartDate;
                    }
                }
                else
                {
                    var episode = patientRepository.GetEpisodeById(agencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        note.EndDate = episode.EndDate;
                        note.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null) note.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                    }
                }
                note.IsWoundCareExist = patientvisitNote.IsWoundCare;
                if (note.IsWoundCareExist) note.WoundCare = patientvisitNote.ToWoundCareDictionary();
                note.PatientId = patientvisitNote.PatientId;
                note.EpisodeId = patientvisitNote.EpisodeId;
                note.EventId = patientvisitNote.Id;
                note.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                note.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                note.Version = patientvisitNote.Version;
            }
            else note.Questions = new Dictionary<string, NotesQuestion>();

            var patient = patientRepository.GetPatientOnly(patientId, agencyId);
            note.Patient = patient;
            if (patient != null)
            {
                if (note.Type != null && (note.Type == DisciplineTasks.PTEvaluation.ToString()
                                            || note.Type == DisciplineTasks.PTReEvaluation.ToString()
                                            || note.Type == DisciplineTasks.OTEvaluation.ToString()
                                            || note.Type == DisciplineTasks.OTReEvaluation.ToString()
                                            || note.Type == DisciplineTasks.STEvaluation.ToString()
                                            || note.Type == DisciplineTasks.STReEvaluation.ToString()
                                            || note.Type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                            || note.Type == DisciplineTasks.PTDischarge.ToString()
                                            || note.Type == DisciplineTasks.SixtyDaySummary.ToString()))
                {
                    AgencyPhysician physician = patientvisitNote.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        note.PhysicianId = physician.Id;
                        note.PhysicianDisplayName = physician.DisplayName;
                        note.PhysicianPhone = physician.PhoneWorkFormatted;
                        note.PhysicianFax = physician.FaxNumberFormatted;
                    }
                    else if (!patientvisitNote.PhysicianId.IsEmpty())
                    {
                        physician = physicianRepository.Get(patientvisitNote.PhysicianId, agencyId);
                        if (physician != null)
                        {
                            note.PhysicianDisplayName = physician.DisplayName;
                            note.PhysicianPhone = physician.PhoneWorkFormatted;
                            note.PhysicianFax = physician.FaxNumberFormatted;
                        }
                    }
                }
                else
                {
                    if (note.Questions != null && note.Questions.ContainsKey("PhysicianId") && note.Questions["PhysicianId"].Answer.IsNotNullOrEmpty() && note.Questions["PhysicianId"].Answer.IsGuid())
                    {
                        var physician = physicianRepository.Get(note.Questions["PhysicianId"].Answer.ToGuid(), agencyId);
                        if (physician != null)
                        {
                            note.PhysicianId = physician.Id;
                            note.PhysicianDisplayName = physician.DisplayName;
                            note.PhysicianPhone = physician.PhoneWorkFormatted;
                            note.PhysicianFax = physician.FaxNumberFormatted;
                        }
                    }
                    else
                    {
                        var physician = physicianRepository.GetPatientPrimaryPhysician(agencyId, patientId);
                        if (physician != null)
                        {
                            note.PhysicianId = physician.Id;
                            note.PhysicianDisplayName = physician.DisplayName;
                            note.PhysicianPhone = physician.PhoneWorkFormatted;
                            note.PhysicianFax = physician.FaxNumberFormatted;
                        }
                        //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        //{
                        //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        //    if (physician != null)
                        //    {
                        //        note.PhysicianId = physician.Id;
                        //        note.PhysicianDisplayName = physician.DisplayName;
                        //    }
                        //}
                    }
                }
                if (note.Type != null && (note.Type == DisciplineTasks.PTPlanOfCare.ToString() || note.Type == DisciplineTasks.OTPlanOfCare.ToString() || note.Type == DisciplineTasks.STPlanOfCare.ToString()
                    || note.Type == DisciplineTasks.SNPsychAssessment.ToString()))
                {
                    note.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
                }
            }
            return note;
        }

        private PatientVisitNote GetCarePlanBySelectedEpisode(Guid agencyId, Guid patientId, Guid episodeId, DisciplineTasks discipline, out IDictionary<string, NotesQuestion> pocQuestions, DateTime eventDate)
        {
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                var episode = patientRepository.GetEpisodeById(agencyId, episodeId, patientId);
                if (episode != null)
                {
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime() <= eventDate && s.EventDate.ToDateTime() >= episode.StartDate && s.EventDate.ToDateTime() <= episode.EndDate && s.DisciplineTask == (int)discipline && !s.IsMissedVisit).OrderBy(s => s.EventDate.ToDateTime()).FirstOrDefault();
                    if (evnt != null)
                    {
                        var poc = patientRepository.GetVisitNote(agencyId, patientId, evnt.EventId);
                        pocQuestions = poc.ToDictionary();
                        if (pocQuestions != null && pocQuestions.Count != 0)
                        {
                            if (pocQuestions.ContainsKey("SelectedEpisodeId") && pocQuestions["SelectedEpisodeId"] != null && pocQuestions["SelectedEpisodeId"].Answer == episodeId.ToString())
                            {
                                return poc;
                            }
                            else
                            {
                                pocQuestions = null;
                            }
                        }
                    }
                    else
                    {
                        var previousEpisode = patientRepository.GetEpisodeLean(agencyId, patientId, episode.StartDate.AddDays(-1));
                        if (previousEpisode != null)
                        {
                            if (previousEpisode.Schedule.IsNotNullOrEmpty())
                            {
                                var preEvnt = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime() >= previousEpisode.StartDate && s.EventDate.ToDateTime() <= previousEpisode.EndDate && s.EventDate.ToDateTime() >= previousEpisode.EndDate.AddDays(-5) && s.DisciplineTask == (int)discipline && !s.IsMissedVisit).OrderBy(s => s.EventDate.ToDateTime()).FirstOrDefault();
                                if (preEvnt != null)
                                {
                                    var poc = patientRepository.GetVisitNote(agencyId, patientId, preEvnt.EventId);
                                    pocQuestions = poc.ToDictionary();
                                    if (pocQuestions != null && pocQuestions.Count != 0)
                                    {
                                        if (pocQuestions.ContainsKey("SelectedEpisodeId") && pocQuestions["SelectedEpisodeId"] != null && pocQuestions["SelectedEpisodeId"].Answer == episodeId.ToString())
                                        {
                                            return poc;
                                        }
                                        else
                                        {
                                            pocQuestions = null;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            pocQuestions = null;
            return null;
        }



        public List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var result = new List<AssessmentExport>();
            var assessments = oasisAssessmentRepository.GetOnlyCMSOasisByStatusLean(Current.AgencyId,branchId, ((int)status),patientStatus,  startDate,  endDate);

            if (assessments != null && assessments.Count > 0)
            {
                assessments.ForEach(a =>
                {
                    if (a.EpisodeData != null && a.EpisodeData.IsNotNullOrEmpty())
                    {
                        var assessmentEvent = a.EpisodeData.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == a.AssessmentId && e.EventDate.IsValidDate() && e.IsDeprecated == false);

                        if (assessmentEvent != null)
                        {
                            var insurance = InsuranceEngine.Instance.Get(a.InsuranceId, Current.AgencyId);
                            if (insurance != null)
                            {
                                a.Insurance = insurance.Name;
                            }
                            if ((assessmentEvent.IsRecertificationAssessment() || assessmentEvent.IsResumptionofCareAssessment())
                               && (assessmentEvent.EventDate.ToDateTime().Date >= a.EpisodeEndDate.AddDays(-5).Date && assessmentEvent.EventDate.ToDateTime().Date <= a.EpisodeEndDate.Date))
                            {
                                a.EpisodeStartDate = a.EpisodeEndDate.AddDays(1);
                                a.EpisodeEndDate = a.EpisodeEndDate.AddDays(60);
                            }
                            result.Add(a);
                        }
                    }
                });
            }

            return result;
        }

        public List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, List<int> paymentSources)
        {
            var result = new List<AssessmentExport>();
            var assessments = oasisAssessmentRepository.GetOnlyCMSOasisByStatusLean(Current.AgencyId, branchId, ((int)status), paymentSources);
            if (assessments != null && assessments.Count > 0)
            {
                int count = 1;
                assessments.ForEach(a =>
                {
                    if (a.EpisodeData != null && a.EpisodeData.IsNotNullOrEmpty())
                    {
                        var assessmentEvent = a.EpisodeData.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == a.AssessmentId && e.EventDate.IsValidDate() && e.IsDeprecated == false);

                        if (assessmentEvent != null)
                        {
                            var insurance = InsuranceEngine.Instance.Get(a.InsuranceId, Current.AgencyId);
                            if (insurance != null)
                            {
                                a.Insurance = insurance.Name;
                            }
                            if ((assessmentEvent.IsRecertificationAssessment() || assessmentEvent.IsResumptionofCareAssessment())
                               && (assessmentEvent.EventDate.ToDateTime().Date >= a.EpisodeEndDate.AddDays(-5).Date && assessmentEvent.EventDate.ToDateTime().Date <= a.EpisodeEndDate.Date))
                            {
                                a.EpisodeStartDate = a.EpisodeEndDate.AddDays(1);
                                a.EpisodeEndDate = a.EpisodeEndDate.AddDays(60);
                            }
                            a.Index = count;
                            result.Add(a);
                            count++;
                        }
                    }
                });
            }
            return result;
        }

        public AssessmentPrint OASISProfileData(Guid assessmentId, string assessmentType)
        {
            var assessmentPrint = new AssessmentPrint();                        
            var assessment = oasisAssessmentRepository.GetAssessmentOnly(Current.AgencyId, assessmentId, assessmentType);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();                
                var patient = patientRepository.GetPatientOnly(assessment.PatientId, Current.AgencyId);
                var location = agencyRepository.FindLocationOrMain(Current.AgencyId, patient != null ? patient.AgencyLocationId : Guid.Empty);
                var episodeRange = this.GetPlanofCareCertPeriod(assessment.EpisodeId, assessment.PatientId, assessment.Id);
                if (questions != null && patient != null && location != null)
                {
                    if (string.IsNullOrEmpty(assessment.HippsCode))
                    {
                        if (!string.IsNullOrEmpty(assessment.SubmissionFormat))
                        {
                            var hipps = grouperAgent.GetHippsCode(assessment.SubmissionFormat);
                            if (hipps != null)
                            {
                                assessment.HippsCode = hipps.Code ?? string.Empty;
                                assessment.HippsVersion = hipps.Version ?? string.Empty;
                                assessment.ClaimKey = hipps.ClaimMatchingKey ?? string.Empty;
                                oasisDataProvider.OasisAssessmentRepository.Update(assessment);
                            }
                        }
                    }
                    var prospectivePayment = lookupService.GetProspectivePayment(assessment.HippsCode, episodeRange.StartDate, patient != null && patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty, location != null && patient.AgencyLocationId == location.Id ? location.AddressZipCode : string.Empty, location != null ? location.AddressStateCode : string.Empty);

                    if (!questions.ContainsKey("M0100AssessmentType"))
                    {
                        questions.Add("M0100AssessmentType", new Question() { Answer = assessment.TypeDescription });
                    }
                    else
                    {
                        questions["M0100AssessmentType"].Answer = assessment.TypeDescription;
                    }

                    if (!questions.ContainsKey("HIPPS"))
                    {
                        questions.Add("HIPPS", new Question() { Answer = assessment.HippsCode });
                    }
                    else
                    {
                        questions["HIPPS"].Answer = assessment.HippsCode;
                    }

                    if (!questions.ContainsKey("OASISMatchingKey"))
                    {
                        questions.Add("OASISMatchingKey", new Question() { Answer = assessment.ClaimKey });
                    }
                    else
                    {
                        questions["OASISMatchingKey"].Answer = assessment.ClaimKey;
                    }

                    if (!questions.ContainsKey("EpisodeStartDate"))
                    {
                        questions.Add("EpisodeStartDate", new Question() { Answer = episodeRange.StartDate.ToShortDateString() });
                    }
                    else
                    {
                        questions["EpisodeStartDate"].Answer = episodeRange.StartDate.ToShortDateString();
                    }

                    if (!questions.ContainsKey("EpisodeEndDate"))
                    {
                        questions.Add("EpisodeEndDate", new Question() { Answer = episodeRange.EndDate.ToShortDateString() });
                    }
                    else
                    {
                        questions["EpisodeEndDate"].Answer = episodeRange.EndDate.ToShortDateString();
                    }

                    if (!questions.ContainsKey("CBSACode"))
                    {
                        questions.Add("CBSACode", new Question() { Answer = prospectivePayment != null ? prospectivePayment.CbsaCode : string.Empty });
                    }
                    else
                    {
                        questions["CBSACode"].Answer = prospectivePayment != null ? prospectivePayment.CbsaCode : string.Empty;
                    }

                    if (!questions.ContainsKey("HHRG"))
                    {
                        questions.Add("HHRG", new Question() { Answer = prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty });
                    }
                    else
                    {
                        questions["HHRG"].Answer = prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty;
                    }

                    if (!questions.ContainsKey("Weight"))
                    {
                        questions.Add("Weight", new Question() { Answer = prospectivePayment != null ? prospectivePayment.Weight : string.Empty });
                    }
                    else
                    {
                        questions["Weight"].Answer = prospectivePayment != null ? prospectivePayment.Weight : string.Empty;
                    }

                    if (!questions.ContainsKey("WageIndex"))
                    {
                        questions.Add("WageIndex", new Question() { Answer = prospectivePayment != null ? prospectivePayment.WageIndex : string.Empty });
                    }
                    else
                    {
                        questions["WageIndex"].Answer = prospectivePayment != null ? prospectivePayment.WageIndex : string.Empty;
                    }

                    if (!questions.ContainsKey("LaborPortion"))
                    {
                        questions.Add("LaborPortion", new Question() { Answer = prospectivePayment != null ? prospectivePayment.LaborAmount : string.Empty });
                    }
                    else
                    {
                        questions["LaborPortion"].Answer = prospectivePayment != null ? prospectivePayment.LaborAmount : string.Empty;
                    }

                    if (!questions.ContainsKey("NonLabor"))
                    {
                        questions.Add("NonLabor", new Question() { Answer = prospectivePayment != null ? prospectivePayment.NonLaborAmount : string.Empty });
                    }
                    else
                    {
                        questions["NonLabor"].Answer = prospectivePayment != null ? prospectivePayment.NonLaborAmount : string.Empty;
                    }

                    if (!questions.ContainsKey("NonRoutineSuppliesAmount"))
                    {
                        questions.Add("NonRoutineSuppliesAmount", new Question() { Answer = prospectivePayment != null ? prospectivePayment.NonRoutineSuppliesAmount : string.Empty });
                    }
                    else
                    {
                        questions["NonRoutineSuppliesAmount"].Answer = prospectivePayment != null ? prospectivePayment.NonRoutineSuppliesAmount : string.Empty;
                    }

                    if (!questions.ContainsKey("TotalPayment"))
                    {
                        questions.Add("TotalPayment", new Question() { Answer = prospectivePayment != null ? prospectivePayment.TotalProspectiveAmount : string.Empty });
                    }
                    else
                    {
                        questions["TotalPayment"].Answer = prospectivePayment != null ? prospectivePayment.TotalProspectiveAmount : string.Empty;
                    }

                    assessmentPrint.Data = questions;
                    assessmentPrint.Location = location;
                }

            }
            return assessmentPrint;
        }

        #endregion

        #region Private Members

        private OasisViewData AddAssessment(FormCollection formCollection, string assessmentType)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            Assessment assessment = AssessmentFactory.Create(assessmentType);
            assessment.Id = Guid.NewGuid();
            assessment.AgencyId = Current.AgencyId;
            assessment.Status = (int)ScheduleStatus.OasisSaved;
            assessment.Type = (AssessmentType)Enum.Parse(typeof(AssessmentType), assessmentType);
            assessment.PatientId = formCollection.Get(string.Format("{0}_PatientGuid", assessmentType)).ToGuid();
            this.Process(formCollection, assessment, out assessment);
            if (oasisDataProvider.OasisAssessmentRepository.Add(assessment))
            {
                oasisViewData.isSuccessful = true;
                oasisViewData.assessmentId = assessment.Id;
                oasisViewData.Assessment = assessment;
            }
            return oasisViewData;
        }

        private OasisViewData UpdateAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles, string assessmentType)
        {
            var oasisViewData = new OasisViewData { isSuccessful = false };
            string assessmentId = formCollection.Get(string.Format("{0}_Id", assessmentType));
            string episodeId = formCollection.Get(string.Format("{0}_EpisodeId", assessmentType));
            string category = formCollection["categoryType"];
            string action = formCollection.Get(string.Format("{0}_Button", assessmentType));
            Guid patientId = formCollection.Get(string.Format("{0}_PatientGuid", assessmentType)).ToGuid();

            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentId.ToGuid(), patientId, episodeId.ToGuid(), assessmentType, Current.AgencyId);
            if (assessment != null)
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, assessment.EpisodeId, assessment.PatientId, assessment.Id);
                if (scheduleEvent != null)
                {
                    assessment.PatientId = patientId;
                    assessment.Type = (AssessmentType)Enum.Parse(typeof(AssessmentType), assessmentType);
                    var isActionForStatusChange = false;
                    var oldStatus = scheduleEvent.Status.IsNotNullOrEmpty() && scheduleEvent.Status.IsInteger() ? scheduleEvent.Status.ToInteger() : assessment.Status;
                    switch (action)
                    {
                        case "Approve":
                            {
                                var aproveValidation = true;
                                assessment.Status = (int)ScheduleStatus.OasisCompletedExportReady;
                                if (assessment.SignatureText.IsNullOrEmpty() || assessment.SignatureDate <= DateTime.MinValue)
                                {
                                    assessment.Status = oldStatus;
                                    oasisViewData.errorMessage = string.Format("This OASIS Assessment could not be approved because the Electronic Signature is missing.{0}", assessment.TypeName.Contains("NonOasis") ? string.Empty : " Click on 'Check on Errors' to complete the OASIS Assessment. ");
                                    aproveValidation = false;
                                }
                                if (!assessment.TypeName.Contains("NonOasis"))
                                {
                                    var checkError = this.Validate(assessment, out assessment);
                                    if (!checkError)
                                    {
                                        assessment.Status = oldStatus;
                                        oasisViewData.errorMessage += "This OASIS Assessment could not be approved due to validation errors. Click on 'Check on Errors' and resolve all errors before continuing. ";
                                        aproveValidation = false;
                                    }
                                }
                                isActionForStatusChange = true;
                                if (!aproveValidation)
                                {
                                    oasisViewData.isSuccessful = false;
                                    return oasisViewData;
                                }
                            }
                            break;
                        case "Return":
                            {
                                assessment.Status = (int)ScheduleStatus.OasisReturnedForClinicianReview;
                                assessment.SignatureText = string.Empty;
                                assessment.SignatureDate = DateTime.MinValue;
                                isActionForStatusChange = true;
                            }
                            break;
                        default:
                            {
                                this.Process(formCollection, assessment, out assessment);
                                if (oldStatus == (int)ScheduleStatus.OasisCompletedPendingReview || oldStatus == (int)ScheduleStatus.OasisCompletedExportReady)
                                {
                                    assessment.IsValidated = false;
                                }
                                else
                                {
                                    assessment.Status = (int)ScheduleStatus.OasisSaved;
                                }
                                isActionForStatusChange = false;
                            }
                            break;
                    }
                    scheduleEvent.Status = assessment.Status.ToString();
                    if (category.IsNotNullOrEmpty() && category.IsEqual(AssessmentCategory.Integumentary.ToString()) && !isActionForStatusChange)
                    {
                        if (SaveAsset(httpFiles, assessment, out assessment, ref scheduleEvent))
                        {
                            scheduleEvent.Status = assessment.Status.ToString();
                            if (CompleteAssessmentUpdate(assessment, scheduleEvent, assessmentType, category, isActionForStatusChange,oldStatus))
                            {
                                oasisViewData.isSuccessful = true;
                                oasisViewData.assessmentId = assessment.Id;
                                oasisViewData.Assessment = assessment;
                            }
                        }
                    }
                    else
                    {
                        if (CompleteAssessmentUpdate(assessment, scheduleEvent, assessmentType, category, isActionForStatusChange,oldStatus))
                        {
                            oasisViewData.isSuccessful = true;
                            oasisViewData.assessmentId = assessment.Id;
                            oasisViewData.Assessment = assessment;
                        }
                    }
                }
                else
                {
                    oasisViewData.errorMessage = "Schedule Data is not found. Try again.";
                }
            }
            else
            {
                oasisViewData.errorMessage = "Assessment Data is not found. Try again.";
            }
            return oasisViewData;
        }

        private bool CompleteAssessmentUpdate(Assessment assessment, ScheduleEvent scheduleEvent, string assessmentType, string category, bool isActionForStatusChange, int oldStatus)
        {
            bool result = false;
            if (oasisDataProvider.OasisAssessmentRepository.Update(assessment))
            {
                scheduleEvent.Status = assessment.Status.ToString();
                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                {
                    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, assessment.Id);
                    if (userEvent != null)
                    {
                        userEvent.Status = scheduleEvent.Status;
                        userEvent.ReturnReason = scheduleEvent.ReturnReason;
                        userRepository.UpdateEvent(Current.AgencyId, userEvent);
                        result = true;
                    }
                    else
                    {
                        userRepository.AddUserEvent(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                        result = true;
                    }
                    if (isActionForStatusChange)
                    {
                        if (scheduleEvent.Status.IsInteger())
                        {
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                        }
                    }
                    else
                    {
                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, oldStatus == (int)ScheduleStatus.OasisCompletedPendingReview ? Actions.EditAssessmentForQAReview : (oldStatus == (int)ScheduleStatus.OasisCompletedExportReady ? Actions.EditAssessmentForExportReady : Actions.EditAssessment), (DisciplineTasks)scheduleEvent.DisciplineTask, (category.IsNotNullOrEmpty() && Enum.IsDefined(typeof(AssessmentCategory), category) ? ((AssessmentCategory)Enum.Parse(typeof(AssessmentCategory), category)).GetDescription() : string.Empty));
                    }
                }
            }
            return result;
        }

        private bool SaveAsset(HttpFileCollectionBase httpFiles, Assessment assessment, out Assessment recentAssessment, ref ScheduleEvent scheduleEvent)
        {
            bool assetSaved = false;
            recentAssessment = assessment;
            IDictionary<string, Question> questions = assessment.ToDictionary();
            if (httpFiles.Count > 0)
            {
                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, assessment.EpisodeId, assessment.PatientId);
                if (episode != null)
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvents != null && scheduleEvents.Count > 0)
                    {
                        scheduleEvent = scheduleEvents.FirstOrDefault(e => e.EventId == assessment.Id && e.EpisodeId == assessment.EpisodeId && e.PatientId == assessment.PatientId);
                        foreach (string key in httpFiles.AllKeys)
                        {
                            var keyArray = key.Split('_');
                            HttpPostedFileBase file = httpFiles.Get(key);
                            if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                            {
                                using (var binaryReader = new BinaryReader(file.InputStream))
                                {
                                    var asset = new Asset
                                    {
                                        FileName = file.FileName,
                                        AgencyId = Current.AgencyId,
                                        ContentType = file.ContentType,
                                        FileSize = file.ContentLength.ToString(),
                                        Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                    };
                                    if (assetService.AddAsset(asset))
                                    {
                                        if (questions.ContainsKey(keyArray[1]))
                                        {
                                            questions[keyArray[1]] = new Question { Name = keyArray[1].Remove(0, 7), Answer = asset.Id.ToString(), Type = QuestionType.Generic };
                                        }
                                        else
                                        {
                                            questions.Add(keyArray[1], new Question { Name = keyArray[1].Remove(0, 7), Answer = asset.Id.ToString(), Type = QuestionType.Generic });
                                        }
                                        assetSaved = true;
                                        if (!scheduleEvent.Assets.Contains(asset.Id))
                                        {
                                            var tempEvent = scheduleEvent;
                                            scheduleEvent.Assets.Add(asset.Id);
                                            scheduleEvents.RemoveAll(e => e.EventId == tempEvent.EventId && e.EpisodeId == tempEvent.EpisodeId && e.PatientId == tempEvent.PatientId);
                                            scheduleEvents.Add(scheduleEvent);
                                            episode.Schedule = scheduleEvents.ToXml();
                                            patientRepository.UpdateEpisode(episode);
                                        }
                                    }
                                    else
                                    {
                                        assetSaved = false;
                                        if (questions.ContainsKey(keyArray[1]))
                                        {
                                            questions[keyArray[1]] = new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic };
                                        }
                                        else
                                        {
                                            questions.Add(keyArray[1], new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic });
                                        }
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                assetSaved = true;
                                if (questions.ContainsKey(keyArray[1]))
                                {
                                    questions[keyArray[1]] = new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic };
                                }
                                else
                                {
                                    questions.Add(keyArray[1], new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic });
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                assetSaved = true;
            }
            recentAssessment.Questions = questions.Values.ToList();
            return assetSaved;
        }

        private void Process(FormCollection formCollection, Assessment assessment, out Assessment recentAssessment)
        {
            formCollection.Remove(formCollection["assessment"] + "_Id");
            formCollection.Remove(formCollection["assessment"] + "_Action");
            formCollection.Remove(formCollection["assessment"] + "_PatientGuid");
            formCollection.Remove(formCollection["assessment"] + "_EpisodeId");
            formCollection.Remove(formCollection["assessment"] + "_Button");
            formCollection.Remove("assessment");
            formCollection.Remove("categoryType");
            recentAssessment = assessment;

            IDictionary<string, Question> questions = assessment.ToDictionary();

            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray != null && nameArray.Length == 3 && nameArray[2] == "text") continue;
                if (nameArray != null && nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (questions.ContainsKey(name))
                    {
                        questions[name] = Question.Create(name, formCollection.GetValues(key).Join(","));
                    }
                    else
                    {
                        questions.Add(name, Question.Create(name, formCollection.GetValues(key).Join(",")));
                    }
                }
            }
            recentAssessment.Questions = questions.Values.ToList();
        }

        private void ProcessPlanofCare(FormCollection formCollection, PlanofCare planofCare)
        {
            formCollection.Remove("Id");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("PatientId");
            formCollection.Remove("Status");
            formCollection.Remove("SignatureText");
            formCollection.Remove("SignatureDate");
            formCollection.Remove("PhysicianId_text");
            formCollection.Remove("PhysicianId");

            if (planofCare.Data.IsNotNullOrEmpty())
            {
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                IDictionary<string, Question> questions = planofCare.ToDictionary();
                questions = RemovePrimaryDiagnosis(questions);

                foreach (var key in formCollection.AllKeys)
                {
                    string keyName = key;
                    string[] nameArray = key.Split('_');
                    if (nameArray != null && nameArray.Length > 2)
                    {
                        nameArray.Reverse();
                        keyName = nameArray[0];
                    }

                    string answer = formCollection.GetValues(key).Join(",");
                    if (questions.ContainsKey(keyName))
                    {
                        questions[keyName].Answer = answer;
                    }
                    else
                    {
                        questions.Add(keyName, Question.Create(key, answer));
                    }
                }
                planofCare.Questions = questions.Values.ToList();
            }
        }

        private void ProcessPlanofCare(FormCollection formCollection, PlanofCareStandAlone planofCare)
        {
            formCollection.Remove("Id");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("PatientId");
            formCollection.Remove("PhysicianId");
            formCollection.Remove("Status");
            formCollection.Remove("SignatureText");
            formCollection.Remove("SignatureDate");
            formCollection.Remove("PhysicianId_text");

            if (planofCare.Data.IsNotNullOrEmpty())
            {
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                IDictionary<string, Question> questions = planofCare.ToDictionary();
                questions = RemovePrimaryDiagnosis(questions);

                foreach (var key in formCollection.AllKeys)
                {
                    string nonGenericKey = key.Replace("Generic", "");
                    string answer = formCollection.GetValues(key).Join(",");
                    if (questions.ContainsKey(key))
                    {
                        questions[key].Answer = answer;
                    }
                    else if (questions.ContainsKey(nonGenericKey))
                    {
                        questions[nonGenericKey].Answer = answer;
                    }
                    else
                    {
                        questions.Add(key, Question.Create(key, answer));
                    }
                }
                planofCare.Questions = questions.Values.ToList();
            }
        }

        private IDictionary<string, Question> RemovePrimaryDiagnosis(IDictionary<string, Question> questions)
        {
            if (questions != null && questions.Count > 0)
            {
                questions.RemoveAll<string, Question>(i => i.Code == "M1020" || i.Code == "M1022");
                return questions;
            }
            else
            {
                return new Dictionary<string, Question>();
            }
        }

        public List<Question> Get485FromAssessment(Assessment assessment)
        {
            var planofCareQuestions = new List<Question>();
            if (assessment != null)
            {
                var assessmentQuestions = assessment.ToDictionary();
                if (assessmentQuestions != null && assessmentQuestions.Count > 0)
                {
                    // 10. Medications
                    var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                    if (medicationProfile != null)
                    {
                        planofCareQuestions.Add(Question.Create("485Medications", medicationProfile.ToString()));
                    }

                    planofCareQuestions.Add(GetQuestion("M0102PhysicianOrderedDate", assessmentQuestions));

                    // 11. Principal Diagnosis
                    planofCareQuestions.Add(GetQuestion("M1020PrimaryDiagnosis", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("M1020ICD9M", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485ExacerbationOrOnsetPrimaryDiagnosis", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("M1020PrimaryDiagnosisDate", assessmentQuestions));

                    // 12. Surgical Procedure
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureDescription1", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode1", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode1Date", assessmentQuestions));

                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureDescription2", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode2", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SurgicalProcedureCode2Date", assessmentQuestions));

                    // 13. Other Pertinent Diagnosis
                    for (int count = 1; count <= 14; count++)
                    {
                        planofCareQuestions.Add(GetQuestion(string.Format("M1022PrimaryDiagnosis{0}", count), assessmentQuestions));
                        planofCareQuestions.Add(GetQuestion(string.Format("M1022ICD9M{0}", count), assessmentQuestions));
                        planofCareQuestions.Add(GetQuestion(string.Format("485ExacerbationOrOnsetPrimaryDiagnosis{0}", count), assessmentQuestions));
                        planofCareQuestions.Add(GetQuestion(string.Format("M1022PrimaryDiagnosis{0}Date", count), assessmentQuestions));
                    }

                    // 14. DME and Supplies
                    planofCareQuestions.Add(GetQuestion("485DME", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485DMEComments", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485Supplies", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485SuppliesComment", assessmentQuestions));

                    // 15. Safety Measures
                    planofCareQuestions.Add(GetQuestion("485SafetyMeasures", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485OtherSafetyMeasures", assessmentQuestions));

                    // 16. Nutritional Requirements
                    if (assessmentQuestions.ContainsKey("485NutritionalReqs") && assessmentQuestions["485NutritionalReqs"] != null)
                    {
                        planofCareQuestions.Add(Question.Create("485NutritionalReqs", PlanofCareXml.ExtractText("NutritionalRequirements", assessmentQuestions)));
                    }
                    
                    // 17. Allergies
                    var allergyProfile = patientRepository.GetAllergyProfileByPatient(assessment.PatientId, Current.AgencyId);
                    planofCareQuestions.Add(GetQuestion("485Allergies", assessmentQuestions));
                    if (assessmentQuestions.ContainsKey("485AllergiesDescription") && assessmentQuestions["485AllergiesDescription"] != null)
                    {
                        planofCareQuestions.Add(GetQuestion("485AllergiesDescription", assessmentQuestions));
                    }
                    else
                    {
                        planofCareQuestions.Add(Question.Create("485AllergiesDescription", allergyProfile != null ? allergyProfile.ToString() : string.Empty));
                    }

                    // 18.A. Functional Limitations
                    planofCareQuestions.Add(GetQuestion("485FunctionLimitations", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485FunctionLimitationsOther", assessmentQuestions));

                    // 18.B. Activities Permitted
                    planofCareQuestions.Add(GetQuestion("485ActivitiesPermitted", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485ActivitiesPermittedOther", assessmentQuestions));


                    // 19. Mental Status
                    planofCareQuestions.Add(GetQuestion("485MentalStatus", assessmentQuestions));
                    planofCareQuestions.Add(GetQuestion("485MentalStatusOther", assessmentQuestions));

                    // 20. Prognosis
                    planofCareQuestions.Add(GetQuestion("485Prognosis", assessmentQuestions));

                    // 21. Interventions
                    var interventions = string.Empty;
                    if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"] != null && assessmentQuestions["485SNFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("SN Frequency: {0}. ", assessmentQuestions["485SNFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"] != null && assessmentQuestions["485PTFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("PT Frequency: {0}. ", assessmentQuestions["485PTFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"] != null && assessmentQuestions["485OTFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("OT Frequency: {0}. ", assessmentQuestions["485OTFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"] != null && assessmentQuestions["485STFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("ST Frequency: {0}. ", assessmentQuestions["485STFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"] != null && assessmentQuestions["485MSWFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("MSW Frequency: {0}. ", assessmentQuestions["485MSWFrequency"].Answer);
                    }
                    if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"] != null && assessmentQuestions["485HHAFrequency"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += string.Format("HHA Frequency: {0}. ", assessmentQuestions["485HHAFrequency"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("485OrdersDisciplineInterventionComments") && assessmentQuestions["485OrdersDisciplineInterventionComments"] != null && assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer.IsNotNullOrEmpty())
                    {
                        interventions += assessmentQuestions["485OrdersDisciplineInterventionComments"].Answer;
                    }

                    var baseVitalSignFormat = "{0} greater than (>) {1} or less than (<) {2}. ";

                    var vitalSignParameters = string.Empty;
                    if (assessmentQuestions.ContainsKey("GenericTempGreaterThan")
                       && assessmentQuestions["GenericTempGreaterThan"] != null
                       && assessmentQuestions["GenericTempGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericTempLessThan")
                       && assessmentQuestions["GenericTempLessThan"] != null
                       && assessmentQuestions["GenericTempLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Temperature", assessmentQuestions["GenericTempGreaterThan"].Answer, assessmentQuestions["GenericTempLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericPulseGreaterThan")
                       && assessmentQuestions["GenericPulseGreaterThan"] != null
                       && assessmentQuestions["GenericPulseGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericPulseLessThan")
                       && assessmentQuestions["GenericPulseLessThan"] != null
                       && assessmentQuestions["GenericPulseLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Pulse", assessmentQuestions["GenericPulseGreaterThan"].Answer, assessmentQuestions["GenericPulseLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericRespirationGreaterThan")
                       && assessmentQuestions["GenericRespirationGreaterThan"] != null
                       && assessmentQuestions["GenericRespirationGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericRespirationLessThan")
                       && assessmentQuestions["GenericRespirationLessThan"] != null
                       && assessmentQuestions["GenericRespirationLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Respirations", assessmentQuestions["GenericRespirationGreaterThan"].Answer, assessmentQuestions["GenericRespirationLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericSystolicBPGreaterThan")
                       && assessmentQuestions["GenericSystolicBPGreaterThan"] != null
                       && assessmentQuestions["GenericSystolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericSystolicBPLessThan")
                       && assessmentQuestions["GenericSystolicBPLessThan"] != null
                       && assessmentQuestions["GenericSystolicBPLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Systolic BP", assessmentQuestions["GenericSystolicBPGreaterThan"].Answer, assessmentQuestions["GenericSystolicBPLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericDiastolicBPGreaterThan")
                       && assessmentQuestions["GenericDiastolicBPGreaterThan"] != null
                       && assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericDiastolicBPLessThan")
                       && assessmentQuestions["GenericDiastolicBPLessThan"] != null
                       && assessmentQuestions["GenericDiastolicBPLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Diastolic BP", assessmentQuestions["GenericDiastolicBPGreaterThan"].Answer, assessmentQuestions["GenericDiastolicBPLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("Generic02SatLessThan")
                       && assessmentQuestions["Generic02SatLessThan"] != null
                       && assessmentQuestions["Generic02SatLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format("O2 Sat (percent) less than (<) {0}. ", assessmentQuestions["Generic02SatLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericFastingBloodSugarGreaterThan")
                       && assessmentQuestions["GenericFastingBloodSugarGreaterThan"] != null
                       && assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericFastingBloodSugarLessThan")
                       && assessmentQuestions["GenericFastingBloodSugarLessThan"] != null
                       && assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Fasting blood sugar", assessmentQuestions["GenericFastingBloodSugarGreaterThan"].Answer, assessmentQuestions["GenericFastingBloodSugarLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericRandomBloddSugarGreaterThan")
                       && assessmentQuestions["GenericRandomBloddSugarGreaterThan"] != null
                       && assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer.IsNotNullOrEmpty()
                       && assessmentQuestions.ContainsKey("GenericRandomBloodSugarLessThan")
                       && assessmentQuestions["GenericRandomBloodSugarLessThan"] != null
                       && assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format(baseVitalSignFormat, "Random blood sugar", assessmentQuestions["GenericRandomBloddSugarGreaterThan"].Answer, assessmentQuestions["GenericRandomBloodSugarLessThan"].Answer);
                    }

                    if (assessmentQuestions.ContainsKey("GenericWeightGreaterThan")
                       && assessmentQuestions["GenericWeightGreaterThan"] != null
                       && assessmentQuestions["GenericWeightGreaterThan"].Answer.IsNotNullOrEmpty())
                    {
                        vitalSignParameters += string.Format("Weight Gain/Loss (lbs/7 days) Greater than {0}. ", assessmentQuestions["GenericWeightGreaterThan"].Answer);
                    }

                    if (vitalSignParameters.IsNotNullOrEmpty())
                    {
                        interventions += "SN to notify MD of: " + vitalSignParameters;
                    }

                    interventions += PlanofCareXml.ExtractText("Intervention", assessmentQuestions);
                    var interventionQuestion = Question.Create("485Interventions", interventions);
                    planofCareQuestions.Add(interventionQuestion);

                    var goals = string.Empty;
                    goals += PlanofCareXml.ExtractText("Goal", assessmentQuestions);
                    if (assessmentQuestions.ContainsKey("485SupportiveAssistanceGoalsComments") && assessmentQuestions["485SupportiveAssistanceGoalsComments"] != null)
                    {
                        goals += assessmentQuestions["485SupportiveAssistanceGoalsComments"].Answer;
                    }
                    if (assessmentQuestions.ContainsKey("485SensoryStatusGoalComments") && assessmentQuestions["485SensoryStatusGoalComments"] != null)
                    {
                        goals += assessmentQuestions["485SensoryStatusGoalComments"].Answer;
                    }
                    if (assessmentQuestions.ContainsKey("485RehabilitationPotential") && assessmentQuestions["485RehabilitationPotential"] != null)
                    {
                        if (assessmentQuestions["485RehabilitationPotential"].Answer == "1")
                        {
                            goals += "Rehab Potential: Good for stated goals.";
                        }
                        if (assessmentQuestions["485RehabilitationPotential"].Answer == "2")
                        {
                            goals += "Rehab Potential: Fair for stated goals.";
                        }
                        if (assessmentQuestions["485RehabilitationPotential"].Answer == "3")
                        {
                            goals += "Rehab Potential: Poor for stated goals.";
                        }
                    }

                    if (assessmentQuestions.ContainsKey("485AchieveGoalsComments") && assessmentQuestions["485AchieveGoalsComments"] != null && assessmentQuestions["485AchieveGoalsComments"].Answer.IsNotNullOrEmpty())
                    {
                        goals += assessmentQuestions["485AchieveGoalsComments"].Answer;
                    }

                    if (assessmentQuestions.ContainsKey("485DischargePlans") && assessmentQuestions["485DischargePlans"] != null)
                    {
                        if (assessmentQuestions["485DischargePlans"].Answer == "1")
                        {
                            goals += "Discharge Plan: Patient to be discharged to the care of Physician. ";
                        }
                        if (assessmentQuestions["485DischargePlans"].Answer == "2")
                        {
                            goals += "Discharge Plan: Patient to be discharged to the care of Caregiver. ";
                        }
                        if (assessmentQuestions["485DischargePlans"].Answer == "3")
                        {
                            goals += "Discharge Plan: Patient to be discharged to Self care. ";
                        }
                    }

                    if (assessmentQuestions.ContainsKey("485DischargePlansReason") && assessmentQuestions["485DischargePlansReason"] != null && assessmentQuestions["485DischargePlansReason"].Answer.IsNotNullOrEmpty())
                    {
                        var reasons = assessmentQuestions["485DischargePlansReason"].Answer.Split(',');
                        foreach (string reason in reasons)
                        {
                            if (reason.IsEqual("1"))
                            {
                                goals += "Discharge when caregiver willing and able to manage all aspects of patient's care. ";
                            }
                            if (reason.IsEqual("2"))
                            {
                                goals += "Discharge when goals met. ";
                            }
                            if (reason.IsEqual("3"))
                            {
                                goals += "Discharge when wound(s) healed. ";
                            }
                        }
                    }

                    if (assessmentQuestions.ContainsKey("485DischargePlanComments") && assessmentQuestions["485DischargePlanComments"] != null && assessmentQuestions["485DischargePlanComments"].Answer.IsNotNullOrEmpty())
                    {
                        goals += assessmentQuestions["485DischargePlanComments"].Answer;
                    }

                    var goalQuestion = Question.Create("485Goals", goals);
                    planofCareQuestions.Add(goalQuestion);
                }
            }
            return planofCareQuestions;
        }

        private Question GetQuestion(string questionName, IDictionary<string, Question> assessmentQuestions)
        {
            var question = new Question();

            if (assessmentQuestions.ContainsKey(questionName) && assessmentQuestions[questionName] != null)
            {
                question = assessmentQuestions[questionName];
            }
            return question;
        }

        private IDictionary<string, Question> ProcessPatientDemographics(Patient patient, ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode)
        {
            var questions = new Dictionary<string, Question>();
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);// AgencyEngine.Get(Current.AgencyId);
            questions.Add("M0020PatientIdNumber", new Question { Name = "PatientIdNumber", Code = "M0020", Answer = patient.PatientIdNumber, Type = QuestionType.Moo });
            if (assessmentType == AssessmentType.StartOfCare)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "01", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = "00000" });
                questions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.NonOasisStartOfCare)
            {
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.ResumptionOfCare)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "03", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = "00000" });
                questions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.Recertification)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "04", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.AddDays(60).ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIPPSCODE", new Question { Name = "HIPPSCODE", Answer = "00000" });
                questions.Add("HIPPSVERSION", new Question { Name = "HIPPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.DischargeFromAgency)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "09", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.DischargeFromAgencyDeath)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "08", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.FollowUp)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "05", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = "00000" });
                questions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.TransferInPatientDischarged)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "07", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.TransferInPatientNotDischarged)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "06", Type = QuestionType.Moo });

                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            questions.Add("M0010CertificationNumber", new Question { Name = "CertificationNumber", Code = "M0010", Answer = agency.MedicareProviderNumber, Type = QuestionType.Moo });
            //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
            //{
            //    patient.PhysicianContacts.ForEach(p =>
            //    {
            //        if (p.Primary && p.NPI.IsNotNullOrEmpty() && p.NPI.Length == 10)
            //        {
            //            questions.Add("M0018NationalProviderId", new Question { Name = "NationalProviderId", Code = "M0018", Answer = p.NPI, Type = QuestionType.Moo });
            //        }
            //    });
            //}
            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patient.Id);
            if (physician != null)
            {
                if (physician.NPI.IsNotNullOrEmpty() && physician.NPI.Length == 10)
                {
                    questions.Add("M0018NationalProviderId", new Question { Name = "NationalProviderId", Code = "M0018", Answer = physician.NPI, Type = QuestionType.Moo });
                }
            }

            questions.Add("M0030SocDate", new Question { Name = "SocDate", Code = "M0030", Answer = episode.StartOfCareDate.ToString("MM/dd/yyyy"), Type = QuestionType.Moo });
            questions.Add("M0040FirstName", new Question { Name = "FirstName", Code = "M0040", Answer = patient.FirstName, Type = QuestionType.Moo });
            string val = patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToString() : "";
            questions.Add("M0040MI", new Question { Name = "MI", Code = "M0040", Answer = val, Type = QuestionType.Moo });
            questions.Add("M0040LastName", new Question { Name = "LastName", Code = "M0040", Answer = patient.LastName, Type = QuestionType.Moo });
            questions.Add("M0050PatientState", new Question { Name = "PatientState", Code = "M0050", Answer = patient.AddressStateCode, Type = QuestionType.Moo });
            questions.Add("M0060PatientZipCode", new Question { Name = "PatientZipCode", Code = "M0060", Answer = patient.AddressZipCode, Type = QuestionType.Moo });
            questions.Add("M0063PatientMedicareNumber", new Question { Name = "PatientMedicareNumber", Code = "M0063", Answer = patient.MedicareNumber, Type = QuestionType.Moo });
            questions.Add("M0064PatientSSN", new Question { Name = "PatientSSN", Code = "M0064", Answer = patient.SSN, Type = QuestionType.Moo });
            questions.Add("M0065PatientMedicaidNumber", new Question { Name = "PatientMedicaidNumber", Code = "M0065", Answer = patient.MedicaidNumber, Type = QuestionType.Moo });
            questions.Add("M0066PatientDoB", new Question { Name = "PatientDoB", Code = "M0066", Answer = patient.DOBFormatted, Type = QuestionType.Moo });
            if (patient.Gender == "Male")
            {
                questions.Add("M0069Gender", new Question { Name = "Gender", Code = "M0069", Answer = "1", Type = QuestionType.Moo });
            }
            else if (patient.Gender == "Female")
            {
                questions.Add("M0069Gender", new Question { Name = "Gender", Code = "M0069", Answer = "2", Type = QuestionType.Moo });
            }

            if (patient.ReferralDate > DateTime.MinValue)
            {
                questions.Add("M0104ReferralDate", new Question { Name = "ReferralDate", Code = "M0104", Answer = patient.ReferralDateFormatted, Type = QuestionType.Moo });
            }

            if (patient.Ethnicities != null)
            {
                var races = patient.Ethnicities.Split(';');
                if (races != null && races.Length > 0)
                {
                    //foreach (var race in races)
                    //{
                    if (races.Contains("0"))
                    {
                        questions.Add("M0140RaceAMorAN", new Question { Name = "RaceAMorAN", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("1"))
                    {
                        questions.Add("M0140RaceAsia", new Question { Name = "RaceAsia", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("2"))
                    {
                        questions.Add("M0140RaceBalck", new Question { Name = "RaceBalck", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("3"))
                    {
                        questions.Add("M0140RaceHispanicOrLatino", new Question { Name = "RaceHispanicOrLatino", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("4"))
                    {
                        questions.Add("M0140RaceNHOrPI", new Question { Name = "RaceNHOrPI", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (races.Contains("5"))
                    {
                        questions.Add("M0140RaceWhite", new Question { Name = "RaceWhite", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    //}
                }
            }

            if (patient.PaymentSource != null)
            {
                var paymentSources = patient.PaymentSource.Split(';');
                if (paymentSources != null && paymentSources.Length > 0)
                {
                    //foreach (var paymentSource in paymentSources)
                    //{
                    if (paymentSources.Contains("0"))
                    {
                        questions.Add("M0150PaymentSourceNone", new Question { Name = "PaymentSourceNone", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("1"))
                    {
                        questions.Add("M0150PaymentSourceMCREFFS", new Question { Name = "PaymentSourceMCREFFS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("2"))
                    {
                        questions.Add("M0150PaymentSourceMCREHMO", new Question { Name = "PaymentSourceMCREHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("3"))
                    {
                        questions.Add("M0150PaymentSourceMCAIDFFS", new Question { Name = "PaymentSourceMCAIDFFS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("4"))
                    {
                        questions.Add("M0150PaymentSourceMACIDHMO", new Question { Name = "PaymentSourceMACIDHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("5"))
                    {
                        questions.Add("M0150PaymentSourceWRKCOMP", new Question { Name = "PaymentSourceWRKCOMP", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("6"))
                    {
                        questions.Add("M0150PaymentSourceTITLPRO", new Question { Name = "PaymentSourceTITLPRO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("7"))
                    {
                        questions.Add("M0150PaymentSourceOTHGOVT", new Question { Name = "PaymentSourceOTHGOVT", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("8"))
                    {
                        questions.Add("M0150PaymentSourcePRVINS", new Question { Name = "PaymentSourcePRVINS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("9"))
                    {
                        questions.Add("M0150PaymentSourcePRVHMO", new Question { Name = "PaymentSourcePRVHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("10"))
                    {
                        questions.Add("M0150PaymentSourceSelfPay", new Question { Name = "PaymentSourceSelfPay", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("11"))
                    {
                        questions.Add("M0150PaymentSourceUnknown", new Question { Name = "PaymentSourceUnknown", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSources.Contains("12"))
                    {
                        questions.Add("M0150PaymentSourceOtherSRS", new Question { Name = "PaymentSourceOtherSRS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                        questions.Add("M0150PaymentSourceOther", new Question { Name = "PaymentSourceOther", Code = "M0150", Answer = patient.OtherPaymentSource, Type = QuestionType.Moo });
                    }
                    // }
                }
            }


            questions.Add("GenericPatientDNR", new Question { Name = "PatientDNR", Answer = patient != null ? (patient.IsDNR ? "Yes" : "No") : "No", Type = QuestionType.Generic });
            
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patient.Id, Current.AgencyId);
            if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
            {
                var allergyList = allergyProfile.Allergies.ToObject<List<Allergy>>().Where(a => a.IsDeprecated == false).ToList();
                if (allergyList != null && allergyList.Count > 0)
                {
                    questions.Add("485Allergies", new Question { Name = "Allergies", Answer = "Yes", Type = QuestionType.PlanofCare });
                    questions.Add("485AllergiesDescription", new Question { Name = "AllergiesDescription", Answer = allergyProfile.ToString(), Type = QuestionType.PlanofCare });
                }
            }
            return questions;
        }

        private List<Axxess.Api.Contracts.ValidationError> CustomValidation(IDictionary<string, Question> assessmentQuestions)
        {
            var submissionFormat = new StringBuilder();
            var oasisValidationRules = new List<OasisValidation>();
            string type = assessmentQuestions["M0100AssessmentType"].Answer;
            if (assessmentQuestions.ContainsKey("M0016BranchId") && assessmentQuestions["M0016BranchId"].Answer.IsNotNullOrEmpty() && (assessmentQuestions["M0016BranchId"].Answer != "N" || assessmentQuestions["M0016BranchId"].Answer != "P"))
            {
                oasisValidationRules.Add(new OasisValidation(() => (assessmentQuestions.ContainsKey("M0014BranchState") && !assessmentQuestions["M0014BranchState"].Answer.IsNotNullOrEmpty()) || (!assessmentQuestions.ContainsKey("M0014BranchState")) ? true : false, new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If the Branch Id(M0060) code  exist branch state needs to be filled.", ErrorDup = "M0014_BRANCH_STATE" }));
            }
            if (type.Contains("01") || type.Contains("03"))
            {
                if (assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening").IsEqual("00")
                    || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening").IsEqual("02")
                    || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreening").IsEqual("03"))
                {
                    oasisValidationRules.Add(new OasisValidation(() => assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreeningInterest").IsNotNullOrEmpty() || assessmentQuestions.AnswerOrEmptyString("M1730DepressionScreeningHopeless").IsNotNullOrEmpty(), new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If Depression Screening (M1730) was not done using the PHQ-2 Scale, then PHQ Scale (M1730_PHQ2) must be left blank.", ErrorDup = "M1730_PHQ2" }));
                }
            }

            if (type.Contains("01"))
            {
                if (!assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDateUnknown").IsEqual("1"))
                {
                    var startofCareDate = assessmentQuestions.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentQuestions.AnswerOrEmptyString("M0030SocDate").ToDateTime() : DateTime.MinValue;
                    var assessmentDate = assessmentQuestions.AnswerOrEmptyString("M0090AssessmentCompleted").IsDate() ? assessmentQuestions.AnswerOrEmptyString("M0090AssessmentCompleted").ToDateTime() : DateTime.MinValue;
                    var inpatientDischargeDate = assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDate").IsDate() ? assessmentQuestions.AnswerOrEmptyString("M1005InpatientDischargeDate").ToDateTime() : DateTime.MinValue;

                    oasisValidationRules.Add(new OasisValidation(() => inpatientDischargeDate.Date > startofCareDate.Date || inpatientDischargeDate.Date > assessmentDate.Date, new Axxess.Api.Contracts.ValidationError { ErrorType = "ERROR", Description = "If (M1005) Most recent inpatient discharge date is completed, then M1005 inpatient discharge must be prior to or the same as M0030 (Start of Care Date) and M0090 (Completion Date).", ErrorDup = "M1005_INP_DISCHARGE_DT" }));
                }
            }

            if (assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderId").IsEqual(string.Empty) &&
                !assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderIdUnknown").IsEqual("1"))
            {
                oasisValidationRules.Add(new OasisValidation(() => assessmentQuestions.AnswerOrEmptyString("M0018NationalProviderId").IsNullOrEmpty(), new Axxess.Api.Contracts.ValidationError { ErrorType = "WARNING", Description = "No NPI Information provided. Enter the NPI of the physician who will sign the Plan of Care or check the Unknown or Not Available option.", ErrorDup = "M0018_PHYSICIAN_ID" }));
            }

            var oasisEntityValidator = new OasisEntityValidator(oasisValidationRules.ToArray());
            oasisEntityValidator.Validate();

            return oasisEntityValidator.ValidationError;
        }

        private Dictionary<string, SubmissionBodyFormat> GetOasisSubmissionFormatInstructionsNew()
        {
            var dictionaryFormat = new Dictionary<string, SubmissionBodyFormat>();
            var format = oasisDataProvider.CachedDataRepository.GetSubmissionFormatInstructions();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private Dictionary<string, SubmissionHeaderFormat> GetOasisHeaderInstructionsNew()
        {
            var format = oasisDataProvider.CachedDataRepository.GetSubmissionHeaderFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionHeaderFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        private Dictionary<string, SubmissionInactiveBodyFormat> GetInactivateOasisSubmissionFormatInstructionsNew()
        {
            var format = oasisDataProvider.CachedDataRepository.GetSubmissionInactiveBodyFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionInactiveBodyFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        //only used when there is no plan of care exist
        private bool CreatePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis)
        {
            var result = false;
            var planofCare = new PlanofCare();
            planofCare.Id = scheduleEvent.EventId;
            planofCare.AssessmentId = assessment.Id;
            planofCare.AgencyId = Current.AgencyId;
            planofCare.PatientId = scheduleEvent.PatientId;
            planofCare.EpisodeId = scheduleEvent.EpisodeId;
            planofCare.Status = scheduleEvent.Status.ToInteger();
            planofCare.AssessmentType = assessment.Type.ToString();
            planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
            planofCare.IsNonOasis = isNonOasis;
            //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
            //{
            //    var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
            //    if (primaryPhysician == null)
            //    {
            //        primaryPhysician = patient.PhysicianContacts.First();
            //    }
            //    planofCare.PhysicianId = primaryPhysician.Id;
            //}
            var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId,patient.Id);
            if (physician != null)
            {
                planofCare.PhysicianId = physician.Id;
            }
            planofCare.UserId = scheduleEvent.UserId;
            planofCare.Questions = Get485FromAssessment(assessment);
            planofCare.Data = planofCare.Questions.ToXml();
            //if (oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId,scheduleEvent.EpisodeId, assessment.Id, assessment.Type.ToString()) != null)
            //{
            //    return result;
            //}
            if (oasisDataProvider.PlanofCareRepository.Add(planofCare))
            {
                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, new List<ScheduleEvent> { scheduleEvent }))
                {
                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
                    result = true;
                }
            }
            return result;
        }

        #endregion
    }
}
