﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    public class DiagnosisRange
    {
        public double Start { get; set; }
        public double End { get; set; }
    }
}
