﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web;

    using Axxess.AgencyManagement.Domain;

    public interface IAssetService
    {
        bool AddAsset(Asset asset);
        bool RemoveAsset(Guid assetId);
        bool DeleteAsset(Guid assetId);
        bool AddAsset(HttpPostedFileBase file);
        bool AddAsset(string fileName, string contentType, byte[] bytes);
        bool AddPatientDocument(Asset asset, PatientDocument patientDocument);
    }
}
