﻿namespace Axxess.AgencyManagement.App.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using System.Reflection;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using System.Web;

    public class DatabaseLog : ILog
    {
        #region ILog Members

        public void Info(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Info.ToString()
                });
        }

        public void Warning(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Warn.ToString()
                });
        }

        public void Error(string message)
        {
            LogEngine.Instance.Add(
                new Error()
                {
                    Message = message,
                    Created = DateTime.Now,
                    Server = Environment.MachineName,
                    Type = LogPriority.Error.ToString()
                });
        }

        public void Exception(Exception exception)
        {
            if (exception != null)
            {
                var error = new Error
                {
                    Created = DateTime.Now,
                    Message = exception.Message,
                    Server = Environment.MachineName,
                    Type = LogPriority.Fatal.ToString()
                };

                if (exception is HttpException)
                {
                    HttpException httpException = exception as HttpException;
                    error.Type = httpException.GetHttpCode().ToString();
                }

                if (exception.InnerException != null)
                {
                    error.Detail.InnerError = exception.InnerException.ToString();
                }

                StackTrace stackTrace = new StackTrace();
                if (stackTrace.FrameCount > 0)
                {
                    int frameIndex = stackTrace.FrameCount - 1;
                    StackFrame stackFrame = stackTrace.GetFrame(frameIndex);
                    MethodBase methodBase = stackFrame.GetMethod();
                    error.Detail.MethodName = methodBase.Name;
                    error.Detail.FileName = stackFrame.GetFileName();
                    error.Detail.LineNumber = stackFrame.GetFileLineNumber();
                    error.Detail.ExceptionType = exception.GetType().ToString();
                }

                LogEngine.Instance.Add(error);
            }
        }

        #endregion
    }
}
