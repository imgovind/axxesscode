﻿namespace Axxess.AgencyManagement.App.Logging
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Timers;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    public class LogEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly LogEngine instance = new LogEngine();
        }

        #endregion

        #region Private Members

        private Timer updateTimer;
        private List<Error> newErrors;
        private List<Error> tempErrors;

        private readonly System.Threading.ReaderWriterLockSlim readWriteLock = new System.Threading.ReaderWriterLockSlim();
        
        #endregion

        #region Public Instance

        public static LogEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private LogEngine()
        {
            newErrors = new List<Error>();
        }

        private void StartTimer()
        {
            this.updateTimer = new System.Timers.Timer(TimeSpan.FromSeconds(30).TotalMilliseconds);
            this.updateTimer.Enabled = true;
            this.updateTimer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
            this.updateTimer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            using (readWriteLock.AcquireUpgradeableLock())
            {
                if (this.newErrors.Count > 0)
                {
                    using (readWriteLock.AcquireWriteLock())
                    {
                        this.tempErrors = new List<Error>(this.newErrors);
                        this.newErrors = new List<Error>();
                        this.ProcessExceptions();
                        this.tempErrors = new List<Error>();
                    }
                }
            }
        }

        private void ProcessExceptions()
        {
            if (this.tempErrors != null && this.tempErrors.Count > 0)
            {
                var uniques = new List<Error>();

                this.tempErrors.ForEach( error => {
                    if (!uniques.Exists(e => e.Message.IsEqual(error.Message) || e.Detail.ToString().IsEqual(error.Detail.ToString())))
                    {
                        error.Detail.Impression++;
                        uniques.Add(error);
                    }
                    else
                    {
                        var existing = uniques.Find(e => e.Message.IsEqual(error.Message) || e.Detail.ToString().IsEqual(error.Detail.ToString()));
                        if (existing != null)
                        {
                            existing.Detail.Impression++;
                        }
                    }
                });

                IErrorRepository errorRepository = Container.Resolve<IMembershipDataProvider>().ErrorRepository;
                uniques.ForEach(error =>
                {
                    errorRepository.Add(error);
                });
            }
        }
        #endregion

        #region Public Methods

        public void Add(Error error)
        {
            using (readWriteLock.AcquireWriteLock())
            {
                this.newErrors.Add(error);
            }
        }

        #endregion

    }
}
