﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    class ClaimResponsePdf : AxxessPdf
    {
        private ClaimResponseXml xml;
        public ClaimResponsePdf(ClaimData data)
        {
            this.xml = new ClaimResponseXml(data);
            this.SetType(PdfDocs.ClaimResponse);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans); 
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in xml.GetLayout())
            {
                if (section.Subsection.Count > 0)
                {
                    content[count] = new AxxessTable(section.Cols > 0 ? section.Cols : 1);
                }
                else
                {
                    content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                }
                foreach (XmlPrintSection subsection in section.Subsection)
                {
                    AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessContentSection contentSection = new AxxessContentSection(subsection, this.GetFonts(), true, 7.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                    content[count].AddCell(sectionCell);
                }
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 28, 30, 25, 100 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            this.SetMargins(margins);
            this.SetFields(fieldmap);
        }
    }
}
