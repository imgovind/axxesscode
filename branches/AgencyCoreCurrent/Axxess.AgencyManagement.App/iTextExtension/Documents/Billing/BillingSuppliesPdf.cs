﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.LookUp.Domain;

    class BillingSuppliesPdf : AxxessPdf
    {
        public BillingSuppliesPdf(Final data)
        {
            this.SetType(PdfDocs.BillingSupplies);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[2] ;
            float[] padding = new float[] { 1, 2, 8, 2 }, borders = new float[] { .2F, 0, .2F, 0 };

            content[0] = new AxxessTable(1);
            AxxessCell datacell = new AxxessCell(padding, borders);
            datacell.AddElement(new AxxessContentSection("Certification Period", fonts[1], data.EpisodeRange, fonts[0], 12));
            content[0].AddCell(datacell);

            var supplies = data != null ? data.Supply.IsNotNullOrEmpty() ? data.Supply.ToObject<List<Supply>>() : new List<Supply>() : null;
            if (data == null || supplies == null || supplies.Count == 0)
            {
                content[1] = new AxxessTable(1);
                AxxessCell contentcell = new AxxessCell(padding, borders);
                contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], "There are no supplies from this claim.", fonts[0], 12));
                content[1].AddCell(contentcell);
            }
            else
            {
                content[1] = new AxxessTable(new float[] { 15, 3, 1 }, false);
                content[1].AddCell("Description", fonts[0], padding, borders);
                content[1].AddCell("Date", fonts[0], padding, borders);
                content[1].AddCell("Qty", fonts[0], padding, borders);
                content[1].HeaderRows = 1;
                foreach (var supply in supplies)
                {
                    content[1].AddCell(supply.Description, fonts[0], padding, borders);
                    content[1].AddCell(supply.Date, fonts[0], padding, borders);
                    content[1].AddCell(supply.Quantity.ToString(), fonts[0], padding, borders);
                }
            }
            this.SetContent(content);
            float[] margins = new float[] { 100, 28, 28, 29 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            if (data.Agency != null)
            {
                var location = data.Agency.GetBranch(data.BranchId);
                fieldmap[0].Add("agency", (
                    data != null && data.Agency != null ?
                        (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : string.Empty) +
                        (location != null ?
                            (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) +
                            (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                            (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) +
                            (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                            (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : string.Empty) +
                            (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                            (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                        : string.Empty)
                    : string.Empty));
            }
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[1].Add("patientname", data != null ?
                (data.LastName.IsNotNullOrEmpty() ? data.LastName.ToLower().ToUpper() + ", " : string.Empty) +
                (data.FirstName.IsNotNullOrEmpty() ? data.FirstName.ToLower().ToUpper() : string.Empty) : string.Empty);
            fieldmap[1].Add("AssessmentType", "Final Claim Supplies");
            this.SetFields(fieldmap);
        }
    }
}