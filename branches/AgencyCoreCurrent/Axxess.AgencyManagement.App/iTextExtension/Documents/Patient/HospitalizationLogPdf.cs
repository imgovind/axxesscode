﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using iTextSharp.text;

    using XmlParsing;
    using ViewData;

    public class HospitalizationLogPdf : AxxessPdf
    {
        private HospitalizationLogXml xml;
        public HospitalizationLogPdf(HospitalizationLog data)
        {
            this.xml = new HospitalizationLogXml(data);
            this.SetType(PdfDocs.HospitalizationLog);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout())
            {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 102, 28.8F, 70, 28.8F };
            this.SetMargins(margins);
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<string, string>() { });
            var location = data.Location;
            fieldmap[0].Add("agency",(Current.AgencyName + "\n" ) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : string.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                    : string.Empty)
                );
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : string.Empty);
            fieldmap[0].Add("patientname", data != null && data.Patient != null && data.Patient.DisplayName.IsNotNullOrEmpty() ? data.Patient.DisplayName : string.Empty);
            fieldmap[0].Add("user", data.User);
            this.SetFields(fieldmap);
        }
    }
}
