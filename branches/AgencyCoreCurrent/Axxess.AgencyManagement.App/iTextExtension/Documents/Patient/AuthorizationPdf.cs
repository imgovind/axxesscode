﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.Core.Infrastructure;
    using Services;

    class AuthorizationPdf : AxxessPdf {
        public AuthorizationPdf(Authorization authorization, Patient patient, AgencyLocation location)
        {
            this.SetType(PdfDocs.Authorization);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(authorization != null && authorization.Comments.IsNotNullOrEmpty() ? authorization.Comments : " ") };
            this.SetContent(content);
            this.SetMargins(new float[] { 240, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            //var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency",
                  (Current.AgencyName + "\n") +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : string.Empty)
                );
            fieldmap[0].Add("patientname",
                patient != null ?
                    (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.ToUpper() + ", " : string.Empty) +
                    (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.ToUpper() + " " : string.Empty) +
                    (patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToUpper() + "\n" : "\n")
                : string.Empty);
            fieldmap[0].Add("mr", patient != null && patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("dob", patient != null && patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("start", authorization != null && authorization.StartDateFormatted.IsNotNullOrEmpty() ? authorization.StartDateFormatted : string.Empty);
            fieldmap[0].Add("end", authorization != null && authorization.EndDateFormatted.IsNotNullOrEmpty() ? authorization.EndDateFormatted : string.Empty);
            //var branch = agency.GetBranch(authorization != null && !authorization.AgencyLocationId.IsEmpty() ? authorization.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("branch", authorization != null ? authorization.Branch : string.Empty);
            fieldmap[0].Add("status", authorization != null && authorization.Status.IsNotNullOrEmpty() ? authorization.Status : string.Empty);
            fieldmap[0].Add("insurance", authorization != null ? authorization.InsuranceName: string.Empty);
            fieldmap[0].Add("auth1", authorization != null && authorization.Number1.IsNotNullOrEmpty() ? authorization.Number1 : string.Empty);
            fieldmap[0].Add("auth2", authorization != null && authorization.Number2.IsNotNullOrEmpty() ? authorization.Number2 : string.Empty);
            fieldmap[0].Add("auth3", authorization != null && authorization.Number3.IsNotNullOrEmpty() ? authorization.Number3 : string.Empty);
            fieldmap[0].Add("sn", authorization != null && authorization.SNVisit.IsNotNullOrEmpty() ? authorization.SNVisit + " " + (authorization.SNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.SNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("pt", authorization != null && authorization.PTVisit.IsNotNullOrEmpty() ? authorization.PTVisit + " " + (authorization.PTVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.PTVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("ot", authorization != null && authorization.OTVisit.IsNotNullOrEmpty() ? authorization.OTVisit + " " + (authorization.OTVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.OTVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("st", authorization != null && authorization.STVisit.IsNotNullOrEmpty() ? authorization.STVisit + " " + (authorization.STVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.STVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("msw", authorization != null && authorization.MSWVisit.IsNotNullOrEmpty() ? authorization.MSWVisit + " " + (authorization.MSWVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.MSWVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("hha", authorization != null && authorization.HHAVisit.IsNotNullOrEmpty() ? authorization.HHAVisit + " " + (authorization.HHAVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.HHAVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("dietician", authorization != null && authorization.DieticianVisit.IsNotNullOrEmpty() ? authorization.DieticianVisit + " " + (authorization.DieticianVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.DieticianVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("rn", authorization != null && authorization.RNVisit.IsNotNullOrEmpty() ? authorization.RNVisit + " " + (authorization.RNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.RNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("lvn", authorization != null && authorization.LVNVisit.IsNotNullOrEmpty() ? authorization.LVNVisit + " " + (authorization.LVNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (authorization.LVNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}