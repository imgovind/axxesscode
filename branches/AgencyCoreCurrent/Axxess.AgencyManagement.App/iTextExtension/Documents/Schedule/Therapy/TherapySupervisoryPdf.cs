﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;

    class TherapySupervisoryPdf : VisitNotePdf {
        public TherapySupervisoryPdf(VisitNoteViewData data) : base(data, PdfDocs.SupervisoryVisit, 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 120, 28.3F, 60, 28.3F };
        }

        protected override List<Dictionary<string, string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : "");
            fieldmap[0].Add("aide", data != null && data.Questions != null && data.Questions.ContainsKey("Therapist") && data.Questions["Therapist"].Answer.IsNotNullOrEmpty() ? UserEngine.GetName(data.Questions["Therapist"].Answer.ToGuid(), data.Agency.Id) : "");
            fieldmap[0].Add("aidepresent", data != null && data.Questions != null && data.Questions.ContainsKey("TherapistPresent") && data.Questions["TherapistPresent"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["TherapistPresent"].Answer == "1" ? "Yes" : "") +
                (data.Questions["TherapistPresent"].Answer == "0" ? "No" : "")
            : "");
            fieldmap[0].Add("milage", data != null && data.Questions != null && data.Questions.ContainsKey("AssociatedMileage") && data.Questions["AssociatedMileage"].Answer.IsNotNullOrEmpty() ? data.Questions["AssociatedMileage"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            switch (data.Type)
            {
                case "PTSupervisoryVisit": fieldmap[1].Add("doctype", "PTA SUPERVISORY VISIT"); break;
                case "OTSupervisoryVisit": fieldmap[1].Add("doctype", "COTA SUPERVISORY VISIT"); break;
            }
            return fieldmap;
        }
    }
}