﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ReportModule : Module
    {
        public override string Name
        {
            get { return "Report"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Patient
            routes.MapRoute(
              "ReportPatientSurveyCensusByDateRange",
              "Report/Patient/SurveyCensusByDateRange",
              new { controller = this.Name, action = "PatientSurveyCensusByDateRange", id = UrlParameter.Optional });

            routes.MapRoute(
                "ReportPatientRoster",
                "Report/Patient/Roster",
                new { controller = this.Name, action = "PatientRoster", id = UrlParameter.Optional });

            routes.MapRoute(
                "ReportPatientList",
                "Report/Patient/PatientList",
                new { controller = this.Name, action = "PatientList", id = UrlParameter.Optional });

            routes.MapRoute(
                "ReportPatientReferral",
                "Report/Patient/Referral",
                new { controller = this.Name, action = "ReferralLog", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportCAHPSReport",
                  "Report/Patient/Cahps",
                  new { controller = this.Name, action = "Cahps", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientEmergencyList",
               "Report/Patient/EmergencyList",
               new { controller = this.Name, action = "PatientEmergencyList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientEmergencyPreparednessList",
               "Report/Patient/EmergencyPreparednessList",
               new { controller = this.Name, action = "PatientEmergencyPreparednessList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientBirthdayList",
               "Report/Patient/Birthdays",
               new { controller = this.Name, action = "PatientBirthdayList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientAddressList",
               "Report/Patient/AddressList",
               new { controller = this.Name, action = "PatientAddressList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientPhysicians",
               "Report/Patient/Physician",
               new { controller = this.Name, action = "PatientByPhysicians", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportPatientSocCertPeriod",
             "Report/Patient/SocCertPeriod",
             new { controller = this.Name, action = "PatientSocCertPeriodListing", id = UrlParameter.Optional });

            routes.MapRoute(
             "PatientByResponsibleEmployee",
             "Report/Patient/ResponsibleEmployee",
             new { controller = this.Name, action = "PatientByResponsibleEmployeeListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "PatientByResponsibleCaseManager",
            "Report/Patient/ResponsibleCaseManager",
            new { controller = this.Name, action = "PatientByResponsibleCaseManagerListing", id = UrlParameter.Optional });


            routes.MapRoute(
            "ReportStatisticalCensusByPrimaryInsurance",
            "Report/Statistical/CensusByPrimaryInsurance",
            new { controller = this.Name, action = "StatisticalCensusByPrimaryInsurance", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientExpiringAuthorizations",
               "Report/Patient/ExpiringAuthorizations",
               new { controller = this.Name, action = "PatientExpiringAuthorizations", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportPatientSurveyCensus",
              "Report/Patient/SurveyCensus",
              new { controller = this.Name, action = "PatientSurveyCensus", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportPatientVitalSigns",
            "Report/Patient/VitalSigns",
            new { controller = this.Name, action = "PatientVitalSigns", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportPatientSixtyDaySummary",
            "Report/Patient/SixtyDaySummary",
            new { controller = this.Name, action = "PatientSixtyDaySummary", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportPatientDischargePatients",
            "Report/Patient/DischargePatients",
            new { controller = this.Name, action = "DischargePatients", id = UrlParameter.Optional });

            routes.MapRoute(
            "VitalSigns",
            "Report/VitalSigns/{PatientId}/{StartDate}/{EndDate}",
            new { controller = this.Name, action = "VitalSigns", PatientId = new Guid(), StartDate = new DateTime(), EndDate = new DateTime() });
            #endregion

            #region Clinical

            routes.MapRoute(
               "ReportClinicalOpenOasis",
               "Report/Clinical/OpenOasis",
               new { controller = this.Name, action = "ClinicalOpenOasis", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportClinicalOrders",
               "Report/Clinical/Orders",
               new { controller = this.Name, action = "ClinicalOrders", id = UrlParameter.Optional });
            routes.MapRoute(
              "ReportClinicalMissedVisits",
              "Report/Clinical/MissedVisits",
              new { controller = this.Name, action = "ClinicalMissedVisit", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportClinicalPhysicianOrderHistory",
             "Report/Clinical/PhysicianOrderHistory",
             new { controller = this.Name, action = "ClinicalPhysicianOrderHistory", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportClinicalPlanOfCareHistory",
              "Report/Clinical/PlanOfCareHistory",
              new { controller = this.Name, action = "ClinicalPlanOfCareHistory", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportClinicalTherapyManagement",
             "Report/Clinical/TherapyManagement",
             new { controller = this.Name, action = "ClinicalTherapyManagement", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportClinicalThirteenAndNineteenVisitException",
            "Report/Clinical/ThirteenAndNineteenVisitException",
            new { controller = this.Name, action = "ClinicalThirteenAndNineteenVisitException", id = UrlParameter.Optional });

            routes.MapRoute(
          "ReportClinicalThirteenTherapyReevaluationException",
          "Report/Clinical/ThirteenTherapyReevaluationException",
          new { controller = this.Name, action = "ClinicalThirteenTherapyReevaluationException", id = UrlParameter.Optional });

            routes.MapRoute(
          "ReportClinicalNineteenTherapyReevaluationException",
          "Report/Clinical/NineteenTherapyReevaluationException",
          new { controller = this.Name, action = "ClinicalNineteenTherapyReevaluationException", id = UrlParameter.Optional });


            #endregion

            #region Schedule            

            routes.MapRoute(
             "ReportSchedulePatientWeekly",
             "Report/Schedule/PatientWeekly",
             new { controller = this.Name, action = "SchedulePatientWeeklySchedule", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportSchedulePatientMonthlySchedule",
            "Report/Schedule/PatientMonthlySchedule",
            new { controller = this.Name, action = "PatientMonthlyScheduleListing", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportScheduleEmployeeWeekly",
             "Report/Schedule/EmployeeWeekly",
             new { controller = this.Name, action = "ScheduleEmployeeWeekly", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportScheduleMonthlyWork",
               "Report/Schedule/MonthlySchedule",
               new { controller = this.Name, action = "ScheduleMonthlyWork", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportSchedulePastDueVisits",
              "Report/Schedule/PastDueVisits",
              new { controller = this.Name, action = "SchedulePastDueVisits", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportScheduleVisitsByStatus",
              "Report/Schedule/VisitsByStatus",
              new { controller = this.Name, action = "ScheduleVisitsByStatus", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportScheduleVisitsByType",
              "Report/Schedule/VisitsByType",
              new { controller = this.Name, action = "ScheduleVisitsByType", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportSchedulePastDueVisitsByDiscipline",
            "Report/Schedule/PastDueVisitsByDiscipline",
            new { controller = this.Name, action = "SchedulePastDueVisitsByDiscipline", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportScheduleDailyWork",
             "Report/Schedule/DailySchedule",
             new { controller = this.Name, action = "ScheduleDailyWork", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportScheduleCaseManagerTask",
            "Report/Schedule/CaseManagerTask",
            new { controller = this.Name, action = "ScheduleCaseManagerTask", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportScheduleDeviation",
            "Report/Schedule/Deviation",
            new { controller = this.Name, action = "ScheduleDeviation", id = UrlParameter.Optional });

            routes.MapRoute(
           "ReportSchedulePastDueRecet",
           "Report/Schedule/PastDueRecet",
           new { controller = this.Name, action = "SchedulePastDueRecet", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportScheduleUpcomingRecet",
            "Report/Schedule/UpcomingRecet",
            new { controller = this.Name, action = "ScheduleUpcomingRecet", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportScheduleElectronicVisitVerificationLog",
             "Report/Schedule/ElectronicVisitVerificationLog",
             new { controller = this.Name, action = "ScheduleElectronicVisitVerificationLog", id = UrlParameter.Optional });



            #endregion

            #region Billing

            routes.MapRoute(
            "ReportSubmittedClaims",
            "Report/Billing/SubmittedClaims",
            new { controller = this.Name, action = "SubmittedClaims", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportExpectedSubmittedClaims",
            "Report/Billing/ExpectedSubmittedClaims",
            new { controller = this.Name, action = "ExpectedSubmittedClaims", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingOutstandingClaims",
             "Report/Billing/OutstandingClaims",
             new { controller = this.Name, action = "OutstandingClaims", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportBillingByStatusSummary",
              "Report/Billing/ByStatusSummary",
              new { controller = this.Name, action = "ClaimsByStatus", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingAccountsReceivable",
             "Report/Billing/AccountsReceivable",
             new { controller = this.Name, action = "AccountsReceivable", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingBillingBatch",
             "Report/Billing/BillingBatch",
             new { controller = this.Name, action = "BillingBatch", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportBillingAgedAccountsReceivable",
            "Report/Billing/AgedAccountsReceivable",
            new { controller = this.Name, action = "AgedAccountsReceivable", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingPPSRAPClaims",
             "Report/Billing/PPSRAPClaims",
             new { controller = this.Name, action = "PPSRAPClaims", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingPPSFinalClaims",
             "Report/Billing/PPSFinalClaims",
             new { controller = this.Name, action = "PPSFinalClaims", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportBillingPotentialClaimAutoCancel",
            "Report/Billing/PotentialClaimAutoCancel",
            new { controller = this.Name, action = "PotentialClaimAutoCancel", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingUnearnedRevenue",
             "Report/Billing/UnearnedRevenue",
             new { controller = this.Name, action = "UnearnedRevenue", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingUnbilledRevenue",
             "Report/Billing/UnbilledRevenue",
             new { controller = this.Name, action = "UnbilledRevenue", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingEarnedRevenue",
             "Report/Billing/EarnedRevenue",
             new { controller = this.Name, action = "EarnedRevenue", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingEarnedRevenueByEpisodeDays",
             "Report/Billing/EarnedRevenueByEpisodeDays",
             new { controller = this.Name, action = "EarnedRevenueByEpisodeDays", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportHHRG",
             "Report/Billing/HHRG",
             new { controller = this.Name, action = "HHRG", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingBilledRevenue",
             "Report/Billing/BilledRevenue",
             new { controller = this.Name, action = "BilledRevenue", id = UrlParameter.Optional });

             routes.MapRoute(
             "ReportBillingManagedAccountsReceivable",
             "Report/Billing/ManagedAccountsReceivable",
             new { controller = this.Name, action = "ManagedAccountsReceivable", id = UrlParameter.Optional });

             routes.MapRoute(
            "ReportBillingManagedClaimsByStatus",
            "Report/Billing/ManagedClaimsByStatus",
            new { controller = this.Name, action = "ManagedClaimsByStatus", id = UrlParameter.Optional });

             routes.MapRoute(
           "ReportBillingUnbilledManagedClaims",
           "Report/Billing/UnbilledManagedClaims",
           new { controller = this.Name, action = "UnbilledManagedClaims", id = UrlParameter.Optional });

             routes.MapRoute(
             "ReportBillingUnbilledVisitsForManagedClaims",
             "Report/Billing/UnbilledVisitsForManagedClaims",
             new { controller = this.Name, action = "UnbilledVisitsForManagedClaims", id = UrlParameter.Optional });
            
            #endregion

            #region Employee

            routes.MapRoute(
               "ReportEmployeeRoster",
               "Report/Employee/Roster",
               new { controller = this.Name, action = "EmployeeRoster", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportEmployeeBirthdayList",
              "Report/Employee/Birthdays",
              new { controller = this.Name, action = "EmployeeBirthdayList", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportEmployeeLicenseListing",
             "Report/Employee/License",
             new { controller = this.Name, action = "EmployeeLicenseListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportAllEmployeeLicenseListing",
            "Report/Employee/AllEmployeeLicense",
            new { controller = this.Name, action = "AllEmployeeLicenseListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportEmployeeExpiringLicense",
            "Report/Employee/ExpiringLicense",
            new { controller = this.Name, action = "EmployeeExpiringLicense", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportEmployeeNonUserExpiringLicense",
            "Report/Employee/NonUserExpiringLicense",
            new { controller = this.Name, action = "NonUserExpiringLicense", id = UrlParameter.Optional });

            routes.MapRoute(
           "ReportEmployeeVisitByDateRange",
           "Report/Employee/VisitByDateRange",
           new { controller = this.Name, action = "EmployeeVisitByDateRange", id = UrlParameter.Optional });

            routes.MapRoute(
           "ReportEmployeePermissions",
           "Report/Employee/Permissions",
           new { controller = this.Name, action = "EmployeePermissions", id = UrlParameter.Optional });

            routes.MapRoute(
                "PayrollDetailSummary",
                "Report/Employee/PayrollDetailSummary",
                new { controller = this.Name, action = "PayrollDetailSummary", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportEmployeeList",
              "Report/Employee/List",
              new { controller = this.Name, action = "EmployeeList", id = UrlParameter.Optional });

            #endregion

            #region Statistical

            routes.MapRoute(
                "VisitsByPayor",
                "Report/Statistical/VisitsByPayor",
                new { controller = this.Name, action = "StatisticalVisitsByPayor", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportStatisticalPatientVisitHistory",
             "Report/Statistical/PatientVisits",
             new { controller = this.Name, action = "StatisticalPatientVisitHistory", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportStatisticalEmployeeVisitHistory",
            "Report/Statistical/EmployeeVisits",
            new { controller = this.Name, action = "StatisticalEmployeeVisitHistory", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportStatisticalMonthlyAdmission",
             "Report/Statistical/MonthlyAdmission",
             new { controller = this.Name, action = "StatisticalMonthlyAdmissionListing", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportStatisticalAnnualAdmission",
             "Report/Statistical/AnnualAdmission",
             new { controller = this.Name, action = "StatisticalAnnualAdmissionListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportStatisticalUnduplicatedCensusReport",
            "Report/Statistical/UnduplicatedCensusReport",
            new { controller = this.Name, action = "StatisticalUnduplicatedCensusReportListing", id = UrlParameter.Optional });
            routes.MapRoute(
          "ReportStatisticalUnduplicatedServiceCensusReport",
          "Report/Statistical/UnduplicatedServiceCensusReport",
          new { controller = this.Name, action = "StatisticalUnduplicatedServiceCensusReportListing", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportStatisticalPPSEpisodeInformation",
                  "Report/Statistical/PPSEpisodeInformation",
                  new { controller = this.Name, action = "PPSEpisodeInformationReport", id = UrlParameter.Optional });
            routes.MapRoute(
                  "ReportStatisticalPPSVisitInformation",
                  "Report/Statistical/PPSVisitInformation",
                  new { controller = this.Name, action = "PPSVisitInformationReport", id = UrlParameter.Optional });
            routes.MapRoute(
                  "ReportStatisticalPPSPaymentInformation",
                  "Report/Statistical/PPSPaymentInformation",
                  new { controller = this.Name, action = "PPSPaymentInformationReport", id = UrlParameter.Optional });
            routes.MapRoute(
                  "ReportStatisticalPPSChargeInformation",
                  "Report/Statistical/PPSChargeInformation",
                  new { controller = this.Name, action = "PPSChargeInformationReport", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportDischargesByReason",
                  "Report/Statistical/DischargesByReason",
                  new { controller = this.Name, action = "DischargesByReasonReport", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportVisitsByPrimaryPaymentSource",
                  "Report/Statistical/VisitsByPrimaryPaymentSource",
                  new { controller = this.Name, action = "VisitsByPrimaryPaymentSourceReport", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportVisitsByStaffType",
                  "Report/Statistical/VisitsByStaffType",
                  new { controller = this.Name, action = "VisitsByStaffTypeReport", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportAdmissionsByReferralSource",
                  "Report/Statistical/AdmissionsByReferralSource",
                  new { controller = this.Name, action = "AdmissionsByReferralSourceReport", id = UrlParameter.Optional });
            
            routes.MapRoute(
                  "ReportPatientsVisitsByPrincipalDiagnosis",
                  "Report/Statistical/PatientsVisitsByPrincipalDiagnosis",
                  new { controller = this.Name, action = "PatientsVisitsByPrincipalDiagnosisReport", id = UrlParameter.Optional });
            
            routes.MapRoute(
                  "ReportPatientsAndVisitsByAge",
                  "Report/Statistical/PatientsAndVisitsByAge",
                  new { controller = this.Name, action = "PatientsAndVisitsByAgeReport", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportCostReport",
                  "Report/Statistical/CostReport",
                  new { controller = this.Name, action = "CostReport", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportStatisticalPatientAdmissionsByInternalReferralSource",
                  "Report/Statistical/PatientAdmissionsByInternalReferralSource",
                  new { controller = this.Name, action = "StatisticalPatientAdmissionsByInternalReferralSource", id = UrlParameter.Optional });

            #endregion

        }
    }
}
