﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ScheduleModule : Module
    {
        public override string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "OTPlanOfCarePrint",
                "OTPlanOfCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTPlanOfCarePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            
            routes.MapRoute(
                "MissedVisit",
                "Visit/Miss",
                new { controller = this.Name, action = "MissedVisit", id = UrlParameter.Optional });

            routes.MapRoute(
                "MissedVisitPrint",
                "MissedVisit/View/{patientId}/{eventId}",
                new { controller = this.Name, action = "MissedVisitPrint", patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "HHACarePlanPrint",
                "HHACarePlan/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "HHACarePlanPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
           
            routes.MapRoute(
                "CoordinationOfCarePrint",
                "CoordinationOfCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "CoordinationOfCarePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            
            routes.MapRoute(
                "TransferSummaryPrint",
                "TransferSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "TransferSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SixtyDaySummaryPrint",
                "SixtyDaySummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SixtyDaySummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "LVNSVisitPrint",
                "LVNSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "LVNSVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "HHASVisitPrint",
                "HHAideSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "HHASVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "HomeMakerNotePrint",
               "HomeMakerNote/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "HomeMakerNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DischargeSummaryPrint",
                "DischargeSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "DischargeSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTDischargeSummaryPrint",
                "PTDischargeSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTDischargeSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTDischargeSummaryPrint",
                "OTDischargeSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTDischargeSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STDischargeSummaryPrint",
                "STDischargeSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STDischargeSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "NutritionalAssessmentPrint",
                "NutritionalAssessment/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "NutritionalAssessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "LabsPrint",
                "Labs/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "LabsPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNVisitPrint",
                "SNVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNPediatricVisitPrint",
                "SNPediatricVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNPediatricVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNPediatricAssessmentPrint",
                "SNPediatricAssessment/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNPediatricAssessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "SNPsychVisitPrint",
                "SNPsychVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNPsychVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNPsychAssessmentPrint",
                "SNPsychAssessment/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNPsychAssessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "MSWVisitPrint",
                "MSWVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "MSWVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWProgressNotePrint",
               "MSWProgressNote/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "MSWProgressNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "TransportationNotePrint",
               "TransportationNote/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "TransportationNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNDiabeticDailyVisitPrint",
                "SNDiabeticDailyVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNDiabeticDailyVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWEvaluationAssessmentPrint",
               "MSWEvaluationAssessment/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "MSWEvaluationAssessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWDischargePrint",
               "MSWDischarge/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "MSWEvaluationAssessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWAssessmentPrint",
               "MSWAssessment/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "MSWEvaluationAssessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "HHAideVisitPrint",
                "HHAideVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "HHAideVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTVisitPrint",
                "OTVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "COTAVisitPrint",
                "COTAVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTEvaluationPrint",
                "OTEvaluation/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTDischargePrint",
                "OTDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });


            routes.MapRoute(
                "OTReassessmentPrint",
                "OTReassessment/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTReassessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTReEvaluationPrint",
                "OTReEvaluation/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            
            routes.MapRoute(
                "OTMaintenancePrint",
                "OTMaintenance/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PASCarePlanPrint",
                "PASCarePlan/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PASCarePlanPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PASVisitPrint",
                "PASVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PASVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PASTravelPrint",
                "PASTravel/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PASTravelPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTDischargePrint",
                "PTDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTEvaluationPrint",
                "PTEvaluation/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            
            routes.MapRoute(
                "PTPlanOfCarePrint",
                "PTPlanOfCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTPlanOfCarePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTReEvaluationPrint",
                "PTReEvaluation/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTReassessmentPrint",
                "PTReassessment/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTReassessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTMaintenancePrint",
                "PTMaintenance/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTVisitPrint",
                "PTVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTSupervisoryVisitPrint",
                "PTSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTSupervisoryVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTSupervisoryVisitPrint",
                "OTSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTSupervisoryVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTAVisitPrint",
                "PTAVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STPlanOfCarePrint",
                "STPlanOfCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STPlanOfCarePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "STEvaluationPrint",
                "STEvaluation/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STReEvaluationPrint",
                "STReEvaluation/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STReassessmentPrint",
                "STReassessment/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STReassessmentPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STMaintenancePrint",
                "STMaintenance/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STDischargePrint",
                "STDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STVisitPrint",
                "STVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "UAPInsulinPrepAdminVisitPrint",
                "UAPInsulinPrepAdminVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "UAPInsulinPrepAdminVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "UAPWoundCareVisitPrint",
                "UAPWoundCareVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "UAPWoundCareVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "InitialSummaryOfCarePrint",
                "ISOC/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "InitialSummaryOfCarePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "VisitVerificationInformation",
                "Verification/Info/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "VerificationInformation", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

        }
    }
}
