﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class UserModule : Module
    {
        public override string Name
        {
            get { return "User"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "EditProfile",
               "Profile/Edit",
               new { controller = this.Name, action = "Profile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ForgotSignature",
               "Signature/Forgot",
               new { controller = this.Name, action = "ForgotSignature", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "EmailSignature",
               "Signature/Email",
               new { controller = this.Name, action = "EmailSignature", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "UserLicenseAdd",
                "User/License/Add",
                new { controller = this.Name, action = "LicenseAdd", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                "UserLicenseNew",
                "User/License/New",
                new { controller = this.Name, action = "LicenseNew", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                "UserLicenseEdit",
                "User/License/Edit",
                new { controller = this.Name, action = "LicenseEdit", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                "UserLicenseUpdate",
                "User/License/Update",
                new { controller = this.Name, action = "LicenseUpdate", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                "UserLicenseDelete",
                "User/License/Delete",
                new { controller = this.Name, action = "LicenseDelete", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                "UserLicenseList",
                "User/License/List",
                new { controller = this.Name, action = "LicenseList", id = UrlParameter.Optional }
             );
            #region Rates
            routes.MapRoute(
                "UserRateAdd",
                "User/Rate/Add",
                new { controller = this.Name, action = "RateAdd", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserRateNew",
                "User/Rate/New",
                new { controller = this.Name, action = "RateNew", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserRateEdit",
                "User/Rate/Edit",
                new { controller = this.Name, action = "RateEdit", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserRateUpdate",
                "User/Rate/Update",
                new { controller = this.Name, action = "RateUpdate", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserRateDelete",
                "User/Rate/Delete",
                new { controller = this.Name, action = "RateDelete", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserRateList",
                "User/Rate/List",
                new { controller = this.Name, action = "RateList", id = UrlParameter.Optional }
             );
            #endregion
            #region NonVisitRates
            routes.MapRoute(
                "UserNonVisitRateList",
                "User/NonVisitRate/List",
                new { controller = this.Name, action = "NonVisitRateList", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                "UserNonVisitRateAdd",
                "User/NonVisitRate/Add",
                new { controller = this.Name, action = "NonVisitRateAdd", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserNonVisitRateNew",
                "User/NonVisitRate/New",
                new { controller = this.Name, action = "NonVisitRateNew", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserNonVisitRateEdit",
                "User/NonVisitRate/Edit",
                new { controller = this.Name, action = "NonVisitRateEdit", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserNonVisitRateUpdate",
                "User/NonVisitRate/Update",
                new { controller = this.Name, action = "NonVisitRateUpdate", id = UrlParameter.Optional }
             );
            routes.MapRoute(
                "UserNonVisitRateDelete",
                "User/NonVisitRate/Delete",
                new { controller = this.Name, action = "NonVisitRateDelete", id = UrlParameter.Optional }
             );
            #endregion
        }
    }
}
