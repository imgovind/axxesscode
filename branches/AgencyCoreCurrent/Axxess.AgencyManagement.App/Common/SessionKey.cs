﻿namespace Axxess.AgencyManagement.App
{
    public enum SessionKey
    {
        UserSession,
        AdministratorSession
    }
}
