﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    public class PayrollDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PayrollStatus { get; set; }
        public List<UserVisit> Visits { get; set; }
    }

    public class PayrollDetailSummaryItem
    {
        public string TaskName { get; set; }
        public string TaskCount { get; set; }
        public string TotalTaskTime { get; set; }
        public double TotalMileage { get; set; }
        public double TotalSurcharges { get; set; }
        public string TotalSurchargesFormat
        {
            get
            {
                return string.Format("{0:C}", this.TotalSurcharges);
            }
        }
        public string Insurance { get; set; }
        public string PayRate { get; set; }
        public string MileageRage { get; set; }
        public double TotalPayment { get; set; }
        public string TotalPaymentFormat
        {
            get 
            {
                return string.Format("{0:C}", this.TotalPayment);
            }
        }
    }
}
