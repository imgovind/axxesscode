﻿namespace Axxess.Accounts.Website.Models
{
    public abstract class BaseViewData
    {
        public BaseViewData()
        {
            this.isSuccessful = false;
            this.exception = string.Empty;
            this.errorMessage = string.Empty;
        }

        public string exception { get; set; }
        public bool isSuccessful { get; set; }
        public string errorMessage { get; set; }
    }
}