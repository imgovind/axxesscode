﻿namespace Axxess.Accounts.Website.Models
{
    using System;
    using Axxess.Accounts.Entities.Enums;
    
    public class ResetViewData : BaseViewData
    {
        public Guid LoginId { get; set; }
        public Change ResetType { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
    }
}