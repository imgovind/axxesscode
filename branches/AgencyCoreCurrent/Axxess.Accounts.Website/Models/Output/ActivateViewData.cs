﻿namespace Axxess.Accounts.Website.Models
{
    using System;
    using Axxess.Accounts.Entities.Enums;
    
    public class ActivateViewData : BaseViewData
    {
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string DisplayName { get; set; }
        public string redirectUrl { get; set; }
        public string EmailAddress { get; set; }
    }
}