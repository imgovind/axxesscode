﻿namespace Axxess.Accounts.Website.Models
{
    using System;

    public abstract class BaseInputData
    {
        public BaseInputData() { }

        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
    }
}