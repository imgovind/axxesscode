﻿namespace Axxess.Accounts.Website.Models
{
    using System;
    using Axxess.Framework.DataTypes;

    public class Token
    {
        public Token()
        {
            this.IsChanged = true;
            this.Id = ShortGuid.NewId().ToString();
        }

        public string Id { get; set; }
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public bool IsChanged { get; set; }
        public string DeviceId { get; set; }
        public DateTime Expires { get; set; }
        public string AgencyName { get; set; }
        public string DisplayName { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsExpired { get { return this.Expires < DateTime.Now; } }
    }
}