﻿namespace Axxess.Accounts.Website.Models
{
    using System.Collections.Generic;

    public class AccountViewData : BaseViewData
    {
        public List<dynamic> Agencies { get; set; }
        public string redirectUrl { get; set; }
        public bool hasMultipleAccounts { get; set; }
    }
}