﻿namespace Axxess.Accounts.Website.Models
{
    using System;

    using Axxess.Accounts.Entities.Enums;

    public class ActivateInputData : BaseInputData
    {
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}