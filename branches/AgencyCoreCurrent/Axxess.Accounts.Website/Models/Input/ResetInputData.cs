﻿namespace Axxess.Accounts.Website.Models
{
    using System;

    using Axxess.Accounts.Entities.Enums;

    public class ResetInputData : BaseInputData
    {
        public string Password { get; set; }
        public Change ResetType { get; set; }
        public string Signature { get; set; }
    }
}