﻿namespace Axxess.Accounts.Website.Models
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class LoginInputData : BaseInputData
    {
        public bool RememberMe { get; set; }
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
        public string EmailAddress { get; set; }
    }
}