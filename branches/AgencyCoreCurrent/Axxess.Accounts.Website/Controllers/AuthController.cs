﻿namespace Axxess.Accounts.Website.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Collections.Generic;

    using Models;

    using Axxess.Framework.Extensions;
    using Axxess.Framework.Validation;

    using Axxess.Accounts.Data;
    using Axxess.Accounts.Components;
    using Axxess.Accounts.Entities.Enums;
    using Axxess.Accounts.Website.Security;

    public class AuthController : BaseApiController
    {
        #region Private Members / Contructors

        private readonly IMembershipProvider membershipProvider;
        private readonly ILoginValidation loginValidation;

        public AuthController()
        {
            this.membershipProvider = BusinessFactory.Instance.Get<IMembershipProvider>();
            this.loginValidation = BusinessFactory.Instance.Get<ILoginValidation>();
        }

        #endregion

        #region AuthController Implementation

        [ActionName("RESTAction")]
        public HttpResponseMessage Post([FromUri]string emailAddress, [FromUri]string password)
        {
            var result = this.loginValidation.Validate(emailAddress, password);
            try
            {
                if (result.isSuccessful)
                {
                    if (result.Agencies != null && result.Agencies.Count >= 1)
                    {
                        var userAccounts = result.Agencies;
                        if (userAccounts != null && userAccounts.Count() > 0)
                        {
                            result.Agencies = new List<dynamic>();
                            userAccounts.ForEach(userAccount =>
                            {
                                result.Agencies.Add(new
                                {
                                    Title = userAccount.Title,
                                    UserId = userAccount.UserId,
                                    AgencyId = userAccount.AgencyId,
                                    AgencyName = userAccount.AgencyName,
                                    Created = userAccount.Created.ToShortDateString()
                                });
                            });
                            if (result.Agencies != null && result.Agencies.Count >= 1)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, result);
                            }
                        }
                    }
                    else
                    {
                        result.errorMessage = "There are no active accounts associated with this login. Please try again.";
                    }
                }
            }
            catch (Exception exception)
            {
                result.errorMessage = exception.ToString();
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result.errorMessage.IsNotNullOrEmpty() ? result.errorMessage : "This account could not be verified at this time. Please try again.");

        }

        #endregion
    }
}