﻿namespace Axxess.Accounts.Website.Controllers
{
    using System.Web.Mvc;

    using Axxess.Framework.Validation;

    using Models;

    public class BaseController : Controller
    {
        public T Validate<T>(params Validation[] validations) where T : BaseViewData, new()
        {
            var entityValidator = new EntityValidator(validations);
            entityValidator.Validate();

            if (entityValidator.IsValid)
            {
                return new T { isSuccessful = true };
            }
            else
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }
        }
    }
}