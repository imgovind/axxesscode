﻿namespace Axxess.Accounts.Website.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.Accounts.Data;
    using Axxess.Accounts.Entities;
    using Axxess.Accounts.Components;
    using Axxess.Framework.Extensions;
    using Axxess.Framework.Cryptography;

    [Authorize]
    public class HomeController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            if (System.Web.HttpContext.Current.User != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var userAgencyIdentifier = System.Web.HttpContext.Current.User.Identity.Name;
                var userAgencyArray = userAgencyIdentifier.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                if (userAgencyArray != null && userAgencyArray.Length > 1)
                {
                    var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                    if (readAccountRepository != null)
                    {
                        var userAccount = readAccountRepository.GetUserAccount(userAgencyArray[0].ToGuid(), userAgencyArray[1].ToGuid());
                        if (userAccount != null)
                        {
                            var token = new LoginToken
                            {
                                IsUsed = false,
                                Id = Guid.NewGuid(),
                                Created = DateTime.Now,
                                UserId = userAgencyArray[0].ToGuid(),
                                AgencyId = userAgencyArray[1].ToGuid()
                            };
                            var writeAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("WriteMembership");
                            if (writeAccountRepository != null)
                            {
                                if (writeAccountRepository.Add<LoginToken>(token))
                                {
                                    string redirectUrl = string.Format(
                                        ApplicationSettings.AgencyHomeSiteUrlFormat,
                                        userAccount.ClusterId,
                                        Crypto.Encrypt(string.Format("tokenId={0}", token.Id.ToString()))
                                    );

                                    return Redirect(redirectUrl);
                                }
                            }
                        }
                    }
                }
            }
            return RedirectToAction("Login", "Account");
        }
    }
}
