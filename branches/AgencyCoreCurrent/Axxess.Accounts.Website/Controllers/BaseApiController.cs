﻿namespace Axxess.Accounts.Website.Controllers
{
    using System;
    using System.Web.Http;

    using Axxess.Framework.Extensions;
    using Axxess.Framework.Validation;

    using Filters;
    using Models;

    /// <summary>
    /// Base Controller Class. Provides base functions/members for deriving classes.
    /// </summary>
    [AllowCrossSite]
    public abstract class BaseApiController : ApiController
    {
        public T Validate<T>(params Validation[] validations) where T : BaseViewData, new()
        {
            var entityValidator = new EntityValidator(validations);
            entityValidator.Validate();

            if (entityValidator.IsValid)
            {
                return new T { isSuccessful = true };
            }
            else
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }
        }

        /// <summary>
        /// Authentication Token
        /// </summary>
        //public Token Token { get; set; }

        //internal bool TryVerify(string tokenId, string deviceId, out Token token)
        //{
        //    var result = false;
        //    token = SessionMonitor.Instance.Get(tokenId);
        //    if (token != null)
        //    {
        //        if (token.IsExpired)
        //        {
        //            token.ErrorMessage = "Session has Expired, Please login to continue.";
        //        }
        //        else
        //        {
        //            if (deviceId.IsEqual(token.DeviceId))
        //            {
        //                result = true;
        //                token.ErrorMessage = string.Empty;
        //            }
        //            else
        //            {
        //                token.ErrorMessage = "Wrong Device ID provided. Please login to continue.";
        //            }
        //        }
        //    }
        //    else
        //    {
        //        token = new Token
        //        {
        //            ErrorMessage = "Session Is Invalid. Please login to continue."
        //        };
        //    }

        //    return result;
        //}

        ///// <summary>
        ///// Attempts to update the token with the selected user identifier and agency identifier.
        ///// </summary>
        ///// <param name="tokenId">Authorization Token Identifier</param>
        ///// <param name="deviceId">Device Identifier</param>
        ///// <param name="agencyId">Agency Identifier</param>
        ///// <param name="userId">User Identifier</param>
        ///// <param name="token">Session Token containing authorizing information</param>
        ///// <returns></returns>
        //internal bool TryUpdate(string tokenId, string deviceId, Guid agencyId, Guid userId, out Token token)
        //{
        //    var result = false;
        //    token = SessionMonitor.Instance.Get(tokenId);
        //    if (token != null)
        //    {
        //        if (token.IsExpired)
        //        {
        //            token.ErrorMessage = "Session has Expired, Please login to continue.";
        //        }
        //        else
        //        {
        //            if (deviceId.IsEqual(token.DeviceId))
        //            {
        //                if (userId.IsNotEmpty() && agencyId.IsNotEmpty())
        //                {
        //                    token.UserId = userId;
        //                    token.AgencyId = agencyId;
        //                    token.IsChanged = true;
        //                    token.ErrorMessage = string.Empty;
        //                    result = true;
        //                }
        //                else
        //                {
        //                    token.ErrorMessage = "Agency/User account cannot be identified.";
        //                }
        //            }
        //            else
        //            {
        //                token.ErrorMessage = "Wrong Device ID provided. Please login to continue.";
        //            }
        //        }
        //    }
        //    else
        //    {
        //        token = new Token
        //        {
        //            ErrorMessage = "Session is no longer valid. Please login to continue."
        //        };
        //    }

        //    return result;
        //}
    }
}