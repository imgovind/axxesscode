﻿namespace Axxess.Accounts.Website.Controllers
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Axxess.Accounts.Data;
    using Axxess.Accounts.Components;
    using Axxess.Accounts.Entities;
    using Axxess.Accounts.Entities.Enums;

    using Axxess.Framework;
    using Axxess.Framework.DataTypes;
    using Axxess.Framework.Extensions;
    using Axxess.Framework.Validation;
    using Axxess.Framework.Extensibility;
    using Axxess.Framework.Cryptography;
    using Axxess.Framework.Notification;
    using Axxess.Framework.Configuration;

    using Models;
    using Axxess.Accounts.Website.Security;

    public class AccountController : BaseController
    {
        #region Private Members / Contructors

        private readonly IMembershipProvider membershipProvider;
        private readonly IFormsAuthenticationService formsAuthenticationService;
        private readonly ILoginValidation loginValidation;

        public AccountController()
        {
            this.membershipProvider = BusinessFactory.Instance.Get<IMembershipProvider>();
            this.formsAuthenticationService = BusinessFactory.Instance.Get<IFormsAuthenticationService>();
            this.loginValidation = BusinessFactory.Instance.Get<ILoginValidation>();
        }

        #endregion

        #region AccountController Implementation

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            //if (System.Web.HttpContext.Current.User != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //}
            this.formsAuthenticationService.SignOut();

            return View();
        }

        [HttpPost]
        public JsonResult Login([Bind] LoginInputData loginInput)
        {
            var result = this.loginValidation.Validate(loginInput.EmailAddress, loginInput.Password);
            try
            {
                if (result.isSuccessful)
                {
                    result.isSuccessful = false;
                    result.errorMessage = "The system could not authenticate your account at this time. Please try again.";
                    if (result.Agencies != null && result.Agencies.Count >=1)
                    {
                        if (!result.hasMultipleAccounts && result.Agencies.Count == 1)
                        {
                            var userAccount = result.Agencies[0];
                            if (userAccount != null)
                            {
                                if (userAccount.Status != (int)UserStatus.Active)
                                {
                                    result.errorMessage = "This account is deactivated. Please contact your Agency/Company's Administrator.";
                                }
                                else
                                {
                                    if (userAccount.IsAgencySuspended)
                                    {
                                        result.errorMessage = "You do not have access to your account at this time. Please contact your Agency/Company's Administrator.";
                                    }
                                    else
                                    {
                                        var token = new LoginToken
                                        {
                                            IsUsed = false,
                                            Id = Guid.NewGuid(),
                                            Created = DateTime.Now,
                                            UserId = userAccount.UserId,
                                            AgencyId = userAccount.AgencyId
                                        };
                                        var writeAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("WriteMembership");
                                        if (writeAccountRepository != null)
                                        {
                                            if (writeAccountRepository.Add<LoginToken>(token))
                                            {
                                                result.redirectUrl = string.Format(
                                                    ApplicationSettings.AgencyHomeSiteUrlFormat,
                                                    userAccount.ClusterId,
                                                    Crypto.Encrypt(string.Format("tokenId={0}", token.Id.ToString()))
                                                );

                                                this.formsAuthenticationService.SignIn(string.Format("{0}_{1}", userAccount.UserId, userAccount.AgencyId), loginInput.RememberMe);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (result.hasMultipleAccounts && result.Agencies.Count > 1)
                        {
                            var userAccounts = result.Agencies;
                            if (userAccounts != null && userAccounts.Count() > 0)
                            {
                                result.Agencies = new List<dynamic>();
                                var selectionList = new Dictionary<string, string>();
                                userAccounts.ForEach(userAccount =>
                                {
                                    var id = ShortGuid.NewId().ToString();
                                    var val = string.Format("{0}_{1}", userAccount.UserId, userAccount.AgencyId);
                                    selectionList.Add(id, val);
                                    result.Agencies.Add(new
                                    {
                                        Id = id,
                                        Title = userAccount.Title,
                                        AgencyName = userAccount.AgencyName,
                                        Created = userAccount.Created.ToShortDateString()
                                    });
                                });
                                Session["AgencySelection"] = selectionList;
                            }
                        }                       
                        result.isSuccessful = true;
                        result.errorMessage = "";
                    }
                    else
                    {
                        result.errorMessage = "There are no active accounts associated with this login. Please try again.";
                    }
                }            
            }
            catch (Exception exception)
            {
                result.exception = exception.ToString();
            }

            return Json(result);

        }

        [HttpPost]
        public JsonResult Select(string id)
        {
            var result = this.Validate<LoginViewData>(
                            new Validation(() => id.IsNullOrEmpty(), "You must select an Agency. ")
                        );
            try
            {
                if (result.isSuccessful)
                {
                    result.isSuccessful = false;
                    result.errorMessage = "Login failed. Please try again.";

                    var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                    if (readAccountRepository != null)
                    {
                        if (Session["AgencySelection"] != null && Session["AgencySelection"] is Dictionary<string, string>)
                        {
                            var selectionList = (Dictionary<string, string>)Session["AgencySelection"];
                            if (selectionList.ContainsKey(id))
                            {
                                var selection = selectionList[id];
                                if (selection.IsNotNullOrEmpty())
                                {
                                    var userAgencyArray = selection.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                                    if (userAgencyArray != null && userAgencyArray.Length > 1)
                                    {
                                        var userAccount = readAccountRepository.GetUserAccount(userAgencyArray[0].ToGuid(), userAgencyArray[1].ToGuid());
                                        if (userAccount != null)
                                        {
                                            if (userAccount.Status != (int)UserStatus.Active)
                                            {
                                                result.errorMessage = "This account is deactivated. Please contact your Agency/Company's Administrator.";
                                            }
                                            else
                                            {
                                                if (userAccount.IsAgencySuspended)
                                                {
                                                    result.errorMessage = "You do not have access to your account at this time. Please contact your Agency/Company's Administrator.";
                                                }
                                                else
                                                {
                                                    var token = new LoginToken
                                                    {
                                                        IsUsed = false,
                                                        Id = Guid.NewGuid(),
                                                        Created = DateTime.Now,
                                                        UserId = userAccount.UserId,
                                                        AgencyId = userAccount.AgencyId
                                                    };
                                                    var writeAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("WriteMembership");
                                                    if (writeAccountRepository != null)
                                                    {
                                                        if (writeAccountRepository.Add<LoginToken>(token))
                                                        {
                                                            Session["AgencySelection"] = null;
                                                            result.redirectUrl = string.Format(
                                                                ApplicationSettings.AgencyHomeSiteUrlFormat,
                                                                userAccount.ClusterId,
                                                                Crypto.Encrypt(string.Format("tokenId={0}", token.Id.ToString()))
                                                            );

                                                            result.isSuccessful = true;
                                                            this.formsAuthenticationService.SignIn(string.Format("{0}_{1}", userAccount.UserId, userAccount.AgencyId), false);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.exception = exception.ToString();
            }

            return Json(result);
        }

        [HttpGet]
        public ActionResult LogOff()
        {
            if (System.Web.HttpContext.Current.User != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.formsAuthenticationService.SignOut();
            }

            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult Forgot()
        {
            if (System.Web.HttpContext.Current.User != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.membershipProvider.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.formsAuthenticationService.SignOut();
            }
            return View();
        }

        [HttpPost]
        [Recaptcha.RecaptchaControlMvc.CaptchaValidator]
        public JsonResult Forgot(string EmailAddress, bool captchaValid)
        {
            var result = Validate<JsonViewData>(
                            new Validation(() => EmailAddress.Trim().IsNullOrEmpty(), "You must specify your E-mail Address. <br/>"),
                            new Validation(() => !EmailAddress.IsEmail(), "E-mail Address is not valid.<br/>"),
                            new Validation(() => !(captchaValid), "The Captcha text you entered didn't match the security check. Please try again. <br/>")
                       );
            try
            {
                if (result.isSuccessful)
                {
                    result.isSuccessful = false;
                    result.errorMessage = "Forgot Password was unsuccessful. Please try again.";

                    var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                    if (readAccountRepository != null)
                    {
                        var login = readAccountRepository.GetByEmailAddress(EmailAddress);
                        if (login != null)
                        {
                            if (login.IsLocked)
                            {
                                result.errorMessage = "This account has been locked. Please contact your Agency/Company's Administrator.";
                            }
                            else
                            {
                                if (!login.IsActive)
                                {
                                    result.errorMessage = "This account is not active at this time. Please contact your Administrator.";
                                }
                                else
                                {
                                    ApplicationRoles role = login.Role.ToEnum<ApplicationRoles>(ApplicationRoles.None);
                                    if (role != ApplicationRoles.ApplicationUser)
                                    {
                                        result.errorMessage = "This account type is not authorized to use this application. Please try again.";
                                    }
                                    else
                                    {
                                        var userAccounts = readAccountRepository.GetUserAccounts(login.Id);
                                        if (userAccounts != null)
                                        {
                                            var activeAccount = userAccounts.Where(a => a.Status == (int)UserStatus.Active).FirstOrDefault();
                                            if (activeAccount != null)
                                            {
                                                result.isSuccessful = true;
                                                string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, UrlHelper.GenerateContentUrl("~", HttpContext));
                                                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=password", login.Id)));
                                                var bodyText = MessageBuilder.PrepareTextFrom(
                                                    "PasswordResetInstructions",
                                                    "firstname", login.DisplayName,
                                                    "baseurl", baseUrl,
                                                    "encryptedQueryString", encryptedQueryString);
                                                Notify.User(FrameworkSettings.NoReplyEmail, login.EmailAddress, "Reset Password - Axxess Home Health Management Software", bodyText);
                                                result.errorMessage = "A link to reset your password has been sent to your e-mail address.<br />Please check your e-mail to complete the <a href=\"/login\">Login</a> process.";
                                            }
                                            else
                                            {
                                                result.isSuccessful = false;
                                                result.errorMessage = "This user is deactivated. Please contact your Agency/Company's Administrator.";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.exception = exception.Message;
            }

            return Json(result);
        }

        [HttpGet]
        public ActionResult Reset()
        {
            var resetViewData = new ResetViewData();
            if (HttpContext.Request.QueryString["loginid"] != null && HttpContext.Request.QueryString["type"] != null)
            {
                var resetType = HttpContext.Request.QueryString["type"];
                var loginId = HttpContext.Request.QueryString["loginid"].ToGuid();

                var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                if (readAccountRepository != null)
                {
                    var login = readAccountRepository.GetById(loginId);
                    if (login != null)
                    {
                        resetViewData.LoginId = login.Id;
                        resetViewData.DisplayName = login.DisplayName;
                        resetViewData.EmailAddress = login.EmailAddress;
                        resetViewData.ResetType = resetType.IsEqual("password") ? Change.Password : Change.Signature;
                    }
                }
            }
            return View(resetViewData);
        }

        [HttpPost]
        public JsonResult Reset([Bind] ResetInputData reset)
        {
            var result = Validate<ResetViewData>(
                        new Validation(() => reset.ResetType == Change.Password ? reset.Password.IsNullOrEmpty() : reset.Signature.IsNullOrEmpty(), string.Format("You must specify a new {0}. <br/>", reset.ResetType == Change.Password ? "password" : "signature")),
                        new Validation(() => reset.ResetType == Change.Password ? reset.Password.Length < 8 : reset.Signature.Length < 8, string.Format("The minimum {0} length is 8 characters.", reset.ResetType == Change.Password ? "password" : "signature"))
                    );

            try
            {
                if (result.isSuccessful)
                {
                    result.isSuccessful = false;
                    result.errorMessage = string.Format("Reset {0} was unsuccessful. Please try again.", reset.ResetType.ToString());

                    string newHash = string.Empty;
                    string newSalt = string.Empty;
                    var saltedHash = new SaltedHash();
                    if (reset.ResetType == Change.Password)
                    {
                        saltedHash.GetHashAndSalt(reset.Password, out newHash, out newSalt);

                        var writeAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("WriteMembership");
                        if (writeAccountRepository != null)
                        {
                            if (writeAccountRepository.UpdatePasswordAndHash(reset.LoginId, newHash, newSalt))
                            {
                                result.isSuccessful = true;
                                result.errorMessage = "Your password has been successfully changed. Please click to <a href=\"/login\">Login</a>";
                            }
                            else
                            {
                                result.errorMessage = "Change password attempt failed.";
                            }
                        }
                    }
                    else
                    {
                        saltedHash.GetHashAndSalt(reset.Signature, out newHash, out newSalt);

                        var writeAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("WriteMembership");
                        if (writeAccountRepository != null)
                        {
                            if (writeAccountRepository.UpdateSignatureAndHash(reset.LoginId, newHash, newSalt))
                            {
                                result.isSuccessful = true;
                                result.errorMessage = "Your signature has been successfully changed. Please click to <a href=\"/login\">Login</a>";
                            }
                            else
                            {
                                result.errorMessage = "Change signature attempt failed.";
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.exception = exception.Message;
            }

            return Json(result);
        }

        [HttpGet]
        public ActionResult Activate()
        {
            var activateViewData = new ActivateViewData();
            if (HttpContext.Request.QueryString["id"] != null && HttpContext.Request.QueryString["agencyid"] != null)
            {
                var userId = HttpContext.Request.QueryString["id"].ToGuid();
                var agencyId = HttpContext.Request.QueryString["agencyid"].ToGuid();

                var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                if (readAccountRepository != null)
                {
                    var userAccount = readAccountRepository.GetUserAccount(userId, agencyId);
                    if (userAccount != null)
                    {
                        activateViewData.UserId = userAccount.UserId;
                        activateViewData.AgencyId = userAccount.AgencyId;
                        activateViewData.LoginId = userAccount.LoginId;
                        activateViewData.AgencyName = userAccount.AgencyName;
                    }
                }
            }
            return View(activateViewData);
        }

        [HttpPost]
        public JsonResult Activate([Bind] ActivateInputData activate)
        {
            var result = Validate<ActivateViewData>(
                            new Validation(() => activate.Password.IsNullOrEmpty(), "You must specify a password. <br/>"),
                            new Validation(() => activate.Password.IsNotNullOrEmpty() && activate.Password.Length < 8, "The minimum password length is 8 characters."),
                            new Validation(() => activate.Password.IsNotNullOrEmpty() && activate.ConfirmPassword.IsNotNullOrEmpty() && !activate.Password.IsEqual(activate.ConfirmPassword), "Confirm that both passwords match")
                        );

            try
            {
                if (result.isSuccessful)
                {
                    result.isSuccessful = false;
                    result.errorMessage = "Account Activation failed. Please try again.";

                    var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                    if (readAccountRepository != null)
                    {
                        var login = readAccountRepository.GetUserAccount(activate.UserId, activate.AgencyId);
                        if (login != null)
                        {
                            string newHash = string.Empty;
                            string newSalt = string.Empty;
                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(activate.Password, out newHash, out newSalt);

                            var writeAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("WriteMembership");
                            if (writeAccountRepository != null)
                            {
                                if (writeAccountRepository.UpdatePasswordAndSignature(activate.LoginId, newHash, newSalt))
                                {
                                    var token = new LoginToken
                                    {
                                        IsUsed = false,
                                        Id = Guid.NewGuid(),
                                        Created = DateTime.Now,
                                        UserId = activate.UserId,
                                        AgencyId = activate.AgencyId
                                    };
                                    if (writeAccountRepository.Add<LoginToken>(token))
                                    {
                                        result.redirectUrl = string.Format(
                                            ApplicationSettings.AgencyHomeSiteUrlFormat,
                                            login.ClusterId,
                                            Crypto.Encrypt(string.Format("tokenId={0}", token.Id.ToString()))
                                        );

                                        result.isSuccessful = true;
                                        this.formsAuthenticationService.SignIn(string.Format("{0}_{1}", login.UserId, login.AgencyId), false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.exception = exception.Message;
            }

            return Json(result);
        }

        #endregion
    }
}
