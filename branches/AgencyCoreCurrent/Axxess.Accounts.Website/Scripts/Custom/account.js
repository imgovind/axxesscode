﻿var Logon = {
    Init: function () {
        // Setup for form submission
        this.Validate();

        // Focus on the E-mail Address field
        $("#Login_UserName").focus();

        // Mobile Viewport
        if ($(window).width() < 767) {
            viewport = document.querySelector("meta[name=viewport]");
            viewport.setAttribute('content', 'width=640, initial-scale=0.5, maximum-scale=0.5, user-scalable=no');
        }

        // Upcoming Events
        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: 'https://axxessweb.com/seminar/upcomingEvents',
            jsonpCallback: 'mycallback',
            cache: true,
            success: function (data) {
                var $container = $('.upcoming-events').html('<h1>Training Opportunities</h1>');
                $.each(data, function (i, object) {
                    var series = '';
                    $.each(object, function (property, value) {
                        series += '<li><a href="http://axxessweb.com/seminar/' + value.series + '/' + value.slug + '" target="_blank">'
		                    	+ value.location + '<span class="date">' + value.date + '</span></a></li>';
                    });
                    $container.append('<div class="series">'
		                	+ '<h4><a href="http://axxessweb.com/seminar/' + object[0].series + '" target="_blank">' + object[0].title + ' &raquo;</a></h4>'
		                	+ '<ul id="' + object[0].series + '">' + series + '</ul>'
		                	+ '</div>');
                });
                $container.append('<div class="btn-wrapper"><a href="http://axxessweb.com/seminar" class="btn-custom">SEE MORE</a></div>');
            }
        });

        // Did You Know? Ads
        var dyk = $('.didyouknow p');
        dyk.eq(Math.floor(Math.random() * (dyk.length))).show();
        window.setInterval(function () {
            var current = $('.didyouknow p:visible');
            current.animate({ top: '2em', opacity: 0 }, 800, function () { $(this).hide() });
            var next = current.next();
            if (next.length === 0) next = current.siblings('p').first();
            next.css({ top: '-2em', opacity: 0 }).show().animate({ top: 0, opacity: 1 }, 800, function () { $(this).show() });
        }, 7000);
    },
    Validate: function () {
        $("#login-form")
            .on('submit', function (event) { event.preventDefault(); })
            .validate({
                messages: { UserName: "Please enter your e-mail address.", Password: "Please enter your password." },
                ignore: ":hidden",
                submitHandler: function (form) {
                    var options = {
                        dataType: "json",
                        beforeSubmit: function (values, form, options) {
                            $('.form-wrapper').hide();
                            Logon.LoadingAnimationStart();
                        },
                        success: function (result) {
                            if (result.isSuccessful) {
                                Logon.LoadingAnimationStop();
                                if (result.hasMultipleAccounts) {
                                    // Multiple Agencies
                                    var ul = $('#agencySelectionLinks ul');
                                    for (var agency in result.Agencies) {
                                        agency = result.Agencies[agency];
                                        ul.append(
                                            '<li>' +
                                                '<a href="javascript:void(0);" onclick="Logon.Select(\'' +
                                                    agency.Id + '\');" title="' + agency.AgencyName + '">' +
                                                    '<span class="agency-name">' + agency.AgencyName + '</span>' +
                                                    '<span>' + agency.Title + ' profile created on ' + agency.Created + '</span>' +
                                                '</a>' +
                                            '</li>');
                                    }
                                    $('.login-agency-selection').show();
                                } else {
                                    // Complete, Successful Login
                                    Logon.Agreement(result);
                                }
                            } else {
                                Logon.LoadingAnimationStop();
                                if (result.isAccountInUse) {
                                    // Already logged in elsewhere
                                    Logon.AlreadyLoggedIn(result);
                                } else {
                                    // Failed login
                                    if (result.isLocked) {
                                        Logon.Lock(result);
                                    } else {
                                        $('.form-wrapper').show();
                                        U.Growl(result.errorMessage, "error");
                                    }
                                }
                            }
                        },
                        error: function (response) {
                            Logon.LoadingAnimationStop();
                            U.Growl('A communication error happened while trying to verify your credentials. Please try again or contact Axxess support if it persists.', "error");
                            $('.form-wrapper').show();
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            })
    },
    Select: function (id) {
        // Hide Agency Selection pop-up
        $('.login-agency-selection').hide();

        // Start Loading animation
        Logon.LoadingAnimationStart();

        // Post Agency Selection
        U.PostUrl("/Account/Select", "id=" + id, function (result) {
            Logon.LoadingAnimationStop();

            if (result.isSuccessful) {
                Logon.Agreement(result);
            } else {
                if (result.isAccountInUse) {
                    // Already Logged In Elsewhere
                    Logon.AlreadyLoggedIn(result);
                } else {
                    // Failed login
                    if (result.isLocked) {
                        Logon.Lock(result);
                    } else {
                        $('.login-agency-selection').show();
                        U.Growl(result.errorMessage, "error");
                    }
                }
            }
        });
    },
    Lock: function (result) {
        // Show Error
        Logon.Message(result.errorMessage + '<div class="countdown"></div>');
        // Countdown
        $('.login-message .countdown').data('seconds', 5 * 60).text('5:00');
        Logon.MessageCountdown = window.setInterval(function () {
            var c = $('.login-message .countdown');
            var seconds = c.data('seconds') - 1;
            if (seconds === 0) {
                window.clearInterval(Logon.MessageCountdown);
                $('.login-message').hide();
                $('.login-form-wrapper').show();
            } else {
                c.data('seconds', seconds);
                var minutes = Math.floor(seconds / 60);
                var seconds = seconds - (minutes * 60);
                if (seconds < 10) { seconds = "0" + seconds; }
                c.text(minutes + ':' + seconds);
            }
        }, 1000);
    },
    AlreadyLoggedIn: function (result) {
        Logon.Message(
            '<h3>User Already Logged In</h3>' +
            '<p>This user is already logged in on another computer. If you choose to proceed, the user will automatically be logged off the other computer and their work will not be saved. Are you sure you want to continue?</p>',
            function () {
                $('.login-message').hide();
                $('.login-form-wrapper').show();
            },
            function () {
                U.PostUrl("/Account/Kick", "userId=" + result.userId + "&agencyId=" + result.agencyId, function (result) {
                    if (result.isSuccessful) {
                        $('.login-message').hide();
                        Logon.LoadingAnimationStart();
                        window.location.href = result.redirectUrl;
                    }
                    else { U.Growl(result.errorMessage, "error"); }
                });
            }
        );
    },
    Agreement: function (result) {
        Logon.Message(
            '<h3>Login Successful</h3>' +
            '<p>This system and all its components and contents (collectively, the "System") are intended for authorized business use only. All data within is considered confidential and proprietary. Unauthorized access, use, modification, destruction, disclosure or copy of this system is prohibited and will result in prosecution. Click OK to continue.</p>',
            function () { window.location.href = '/logout'; },
            function () {
                $('.login-message').hide()
                Logon.LoadingAnimationStart();
                window.location.href = result.redirectUrl;
            }
        );
    },
    // Login Message
    MessageCountdown: null,
    Message: function (message, cancel, ok) {
        var loginMessage = $('.login-message');
        loginMessage.find('.message').html(message);
        if (cancel === undefined)
            loginMessage.find('.btn-cancel').hide();
        else
            loginMessage.find('.btn-cancel').unbind('click').click(cancel).show();
        if (ok === undefined)
            loginMessage.find('.btn-ok').hide();
        else
            loginMessage.find('.btn-ok').unbind('click').click(ok).show();
        loginMessage.show();
    },
    // Loading Animation
    LoadingStep: 0,
    LoadingInterval: null,
    LoadingAnimationStart: function () {
        $('.login-loading').fadeIn();
        Logon.LoadingInterval = window.setInterval(Logon.LoadingAnimationStep, 200);
    },
    LoadingAnimationStop: function () {
        window.clearInterval(Logon.LoadingInterval);
        $('.login-loading').stop(true, true).hide();
    },
    LoadingAnimationStep: function () {
        var dots = $('.login-loading i');
        dots.eq(Logon.LoadingStep).animate({ 'font-size': '2em' }, 300);
        dots.eq((Logon.LoadingStep - 1) % 10).animate({ 'font-size': '1em' }, 300);
        Logon.LoadingStep = (Logon.LoadingStep + 1) % 10;
    }
}
var ResetPassword = {
    InitMobile: function () {
        this.Init("Mobile");
        $("input[type=button]", "#login_wrapper").click(function () {
            $("#login_wrapper").fadeOut(500, function () {
                $(location).attr("href", "/Login")
            })
        });
        $("#login_wrapper").fadeIn(500)
    },
    Init: function (Mobile) {
        $("#forgotPasswordForm").validate({
            rules: {
                EmailAddress: { required: true, email: true }
            },
            messages: {
                EmailAddress: { required: "* required", email: "E-mail Address is not valid." }
            },
            submitHandler: function (form) {
                var options = {
                    dataType: 'json',
                    success: function (result) {
                        if (result.isSuccessful) {
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        } else $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                    },
                    error: function (response) {
                        U.Growl('A communication error happened while trying to reset your password. Please try again or contact Axxess support if it persists.', "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Activate = {
    Init: function () {
        $("#activateAccountForm").validate({
            rules: {
                Password: { required: true, minlength: 8 }
            },
            messages: {
                Password: { required: "* required", minlength: "8 characters minimum" }
            },
            submitHandler: function (form) {
                var options = {
                    dataType: 'json',
                    success: function (result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        }
                    },
                    error: function (response) {
                        U.Growl('A communication error happened while trying to activate your account. Please try again and contact Axxess support if the problem persists.', "error");
                        $('#activateAccountForm').show();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var ChangePassword = {
    Init: function () {
        $("#changePasswordForm").validate({
            submitHandler: function (form) {
                var options = {
                    dataType: 'json',
                    success: function (result) {
                        if (result.isSuccessful) {
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        }
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        }
                    },
                    error: function (response) {
                        U.Growl('A communication error happened while trying to change your password. Please try again and contact Axxess support if the problem persists.', "error");
                        $('#activateAccountForm').show();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Link = {
    Init: function () {
        $("#linkAccountForm").validate({
            submitHandler: function (form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function (values, form, options) { U.Block(); },
                    success: function (result) {
                        if (result.isSuccessful) {
                            $("#messages").empty().removeClass().addClass("notification success").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                            $("#Agency_Selection_Container").load("/Account/Agencies", { loginId: $("#Link_User_LoginId").val() }, function (responseText, textStatus, XMLHttpRequest) {
                                if (textStatus == 'error') {
                                }
                                else if (textStatus == "success") {
                                    U.ShowDialog("#Agency_Selection_Container", function () { });
                                }
                            });
                        }
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span class="icon"></span><span>' + result.errorMessage + '</span>');
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}