﻿namespace Axxess.Accounts.Website
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.Accounts.Components;

    public class WebApiApplication : System.Web.HttpApplication
    {
        public override void Init()
        {
            base.Init();
            MvcHandler.DisableMvcResponseHeader = true;
            this.AcquireRequestState += new EventHandler(AxxessApplication_AcquireRequestState);
        }

        protected void Application_Start()
        {
            Bootstrapper.Run();
        }

        protected void AxxessApplication_AcquireRequestState(object sender, EventArgs e)
        {
            if (Context.Handler is MvcHandler
               && Context.User != null
               && Context.User.Identity.IsAuthenticated)
            {
                
            }
        }
    }
}