﻿namespace Axxess.Accounts.Website.Filters
{
    using System;
    using System.Web.Http.Filters;

    public class AllowCrossSiteAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionContext)
        {
            if (actionContext.Response != null)
            {
                actionContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "X-Api-Version, Authorization-Token, Device-Id, Origin, X-Requested-With, Content-Type, Accept");
            }

            base.OnActionExecuted(actionContext);
        }
    }
}