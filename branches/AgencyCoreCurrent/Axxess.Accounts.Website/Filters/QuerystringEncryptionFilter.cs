﻿namespace Axxess.Accounts.Website.Filters
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.Framework.Extensions;
    using Axxess.Framework.Configuration;
    using Axxess.Framework.Cryptography;

    using Axxess.Accounts.Components;

    public class QuerystringEncryptionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.HttpContext.Request.RawUrl.Contains("?"))
            {
                string query = filterContext.HttpContext.Request.Url.Query.Replace("?", "");
                string path = filterContext.HttpContext.Request.Url.AbsolutePath.Substring(1);

                if (!path.ContainsAny(ApplicationSettings.DoNotEncryptFiles) &&
                    !path.StartsWithAny(ApplicationSettings.DoNotEncryptPaths))
                {
                    if (query.StartsWith("enc=", StringComparison.OrdinalIgnoreCase))
                    {
                        string decryptedQuery = Crypto.Decrypt(query.Replace("enc=", ""));
                        if (!string.IsNullOrEmpty(decryptedQuery))
                        {
                            filterContext.HttpContext.RewritePath(string.Format("/{0}", path), string.Empty, decryptedQuery);
                        }
                    }
                }
            }
        }
    }
}