﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Axxess.Accounts.Website.Security" %>

<%@ Import Namespace="Axxess.Framework.Caching" %>
<%@ Import Namespace="Axxess.Framework.Extensions" %>

<script runat="server" type="text/C#">
    private class webinarUser
    {
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AgencyName { get; set; }
    }

    public void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["u"].IsNotNullOrEmpty())
        {
            var principal = OutProcCacher.Get<AxxessPrincipal>(Request.QueryString["u"]);
            if (principal != null)
            {
                if (Request.QueryString["q"].IsNotNullOrEmpty())
                {
                    if (Request.QueryString["q"].IsEqual("training"))
                    {
                        Response.Write(Newtonsoft.Json.JsonConvert.True);
                    }
                    else if (Request.QueryString["q"].IsEqual("webinar"))
                    {
                        AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                        if (identity != null && identity.Session != null)
                        {
                            var nameArray = identity.Session.FullName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            var lastName = string.Empty;
                            var firstName = string.Empty;
                            if (nameArray != null && nameArray.Length >= 2)
                            {
                                firstName = nameArray[0];
                                lastName = nameArray[1];

                                var webinarUser = new webinarUser
                                {
                                    FirstName = firstName,
                                    LastName = lastName,
                                    AgencyName = identity.Session.AgencyName,
                                    EmailAddress = identity.Session.EmailAddress
                                };

                                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(webinarUser));
                            }
                            else
                            {
                                Response.Write(Newtonsoft.Json.JsonConvert.False);
                            }
                        }
                        else
                        {
                            Response.Write(Newtonsoft.Json.JsonConvert.False);
                        }
                    }
                    else
                    {
                        Response.Write(Newtonsoft.Json.JsonConvert.False);
                    }
                }
                else
                {
                    Response.Write(Newtonsoft.Json.JsonConvert.False);
                }
            }
            else
            {
                Response.Write(Newtonsoft.Json.JsonConvert.False);
            }
        }
        else
        {
            Response.Write(Newtonsoft.Json.JsonConvert.False);
        }
        Response.End();
    }
</script>