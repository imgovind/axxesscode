﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="header">Axxess Diagnostic Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="body">
        <% System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
           string browserInfo = "<strong>Your Browser Information</strong><br />"
               + "Name = " + browser.Browser + "<br />"
               + "Version = " + browser.Version + "<br />"
               + "Is Beta = " + browser.Beta + "<br />"
               + "Is Mobile = " + browser.IsMobileDevice + "<br />"
               + "User Agent = " + Request.UserAgent.ToLower() + "<br />";
        %>
        <%= browserInfo %>
            
        <% string ipAddressInfo = "<br /><strong>IP Address</strong><br />"
               + "Current IP = " + HttpContext.Current.Request.UserHostAddress; %>
        <%= ipAddressInfo%>
        
        <% string remoteIpAddressInfo = "<br />"
               + "Remote IP = " + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]; %>
        <%= remoteIpAddressInfo%>
        
        <% string forwardedIpAddressInfo = "<br />"
               + "Forwarded IP = " + HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]; %>
        <%= forwardedIpAddressInfo%>
        
        <% string isLocal = "<br />"
               + "Is Local = " + HttpContext.Current.Request.IsLocal; %>
        <%= isLocal %>
        
        <% string serverInfo = "<br /><br /><strong>Server Information</strong><br />"
               + "Name = " + Environment.MachineName + "<br />"
               + "IP Address = " + HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"]; %>
        <%= serverInfo%>
        
         <% System.Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            var softwareVersion = string.Format("<br /><br /><strong>Software Version</strong><br />Version: {0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision); %>
        <%= softwareVersion %>


</asp:Content>
    