﻿namespace Axxess.Accounts.Website.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Axxess.Accounts.Website.Models;

    public interface ILoginValidation
    {
        LoginViewData Validate(string emailAddress, string password);
    } 
}
