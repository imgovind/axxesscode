﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Axxess.Accounts.Website.Security
{
    using System;

    [Flags]
    public enum Permissions : ulong
    {
        None = 0,
        [Permission("Add/Edit/Delete Users", "Manage Users", "Administration", "This permission allows a user to add, edit or delete user account.")]
        ManageUsers = 1,
        [Permission("Delete Any Task", "Delete scheduled tasks", "Administration", "This permission allows a user the ability to delete any task or documents from a patient’s chart or schedule center.")]
        DeleteTasks = 1 << 1,
        [Permission("Manage Payroll", "Run Payroll Reports", "Administration", "This permission allows a user access to the payroll center and in order to perform payroll functions.")]
        ManagePayroll = 1 << 2,
        [Permission("Access Billing Center", "Access Billing Center", "Billing", "This permission allows user access to the billing center where you can create and manage insurance claims and invoices for Medicare, private insurances or private pay.")]
        AccessBillingCenter = 1 << 3,
        [Permission("Receive Eligibility Report", "Receive Eligibility Report", "Billing", "This permission allows a user to view the Medicare eligibility reports for all of the patients admitted within an agency.")]
        ReceiveEligibilityReport = 1 << 4,
        [Permission("Manage Claims", "Manage Claims (RAP & Final)", "Billing", "This permission allows a user the ability to manage claims by updating the status of billed claims as they are processed, accepted and paid.  A user will also be able to change the status of a claim for resubmission.")]
        ManageClaims = 1 << 5,
        [Permission("Generate Claim files", "Generate Claim files", "Billing", "This permission allows a user create electronic claims files for submission to insurance payers.")]
        GenerateClaimFiles = 1 << 6,
        [Permission("View HHRG Calculations", "View HHRG Calculations", "Billing", "This permission allows a user the ability to see the HHRG calculations for a Medicare claim.  This permission also allows a user to view the payment details of an OASIS assessment from the patient’s charts or schedule center.")]
        ViewHHRGCalculations = 1 << 7,
        [Permission("Access Quality Assurance (QA) Center", "Access Quality Assurance (QA) Center", "QA", "This permission allows a user access to the QA center, to perform quality assurance tasks like reviewing submitted documents. This permission is typically granted to case managers, QA nurses or nursing directors.")]
        AccessCaseManagement = 1 << 8,
        [Permission("Re-open Previously Approved Documents", "Re-open Previously Approved Documents", "QA", "This permission allows a user to reopen previously completed or approved tasks/documents.")]
        ReOpenPreviousDocuments = 1 << 9,
        [Permission("Schedule PRN", "Schedule PRN", "QA", "This permission allows a user to schedule PRN visits, without giving them access to the entire scheduling center.")]
        SchedulePRN = 1 << 10,
        [Permission("Add/Edit/Delete Patients", "Manage Patients", "Clerical", "This permission allows a user to add new patients, edit or delete patient information.")]
        ManagePatients = 1 << 11,
        [Permission("Add/Edit/Delete Referrals", "Manage Referral", "Clerical", "This permission allows a user to add new referrals, edit or delete the referral information.")]
        ManageReferrals = 1 << 12,
        [Permission("View Existing Referrals", "View Existing Referrals", "Clerical", "This permission allows a user to view existing patient referrals.")]
        ViewExisitingReferrals = 1 << 13,
        [Permission("Add/Edit/Delete Hospital Information", "Manage Hospital Information", "Clerical", "This permission allows a user to add new hospitals and edit or delete listed hospitals.")]
        ManageHospital = 1 << 14,
        [Permission("Add/Edit/Delete Insurance Information", "Manage Insurance Information", "Clerical", "This permission allows a user to add insurances and edit or delete insurance information.")]
        ManageInsurance = 1 << 15,
        [Permission("Add/Edit/Delete Contact Information", "Manage Contact Information", "Clerical", "This permission will allow a user to add new contacts and edit or delete contacts and contact information (i.e. a CPA, Therapy group, bank, social worker, moving company, restaurant, etc.)")]
        ManageContact = 1 << 16,
        [Permission("View Lists", "View lists of  patients, referrals, contacts, users, etc", "Clerical", "This permission will allow a user the ability to view list of patients, referrals, physicians, hospitals, etc.")]
        ViewLists = 1 << 17,
        [Permission("Access Patient Charts", "Access Patient Charts", "None", "")]
        AccessPatientCenter = 1 << 18,
        [Permission("Access Print Queue", "Access Print Queue", "Clerical", "This permission allows a user access to the print queue where completed documents are listed to make for easy printing of the documents.")]
        PrintClinicalDocuments = 1 << 19,
        [Permission("Export List to Excel", "Export List to Excel", "Clerical", "This permission allows a user to export reports to Excel for printing or editing.")]
        ExportListToExcel = 1 << 20,
        [Permission("Edit Task Details", "Edit Task Details", "Clerical", "This permission allows a user the ability to edit task details, such as times and dates of schedules task, also allows a user to add attachments to task, view attachments add comments(sticky notes) to documents/tasks.")]
        EditTaskDetails = 1 << 21,
        [Permission("View Completed Documents/Tasks", "View Completed Documents/Tasks", "Clerical", "This permission allows a user the ability to view tasks/documents that have been completed and approved through the Quality Assurance (QA) center.")]
        ViewCompletedDocumentsTasks = 1 << 22,
        [Permission("Create/Edit Physician Orders", "Create/Edit Physician Orders", "Clinical", "This permission will allow a user to the ability create and edit physician orders. Clinicians (e.g. nurses, therapists) generally need this permission.")]
        ManagePhysicianOrders = 1 << 23,
        [Permission("Order Management Center", "Order Management Center", "Clerical", "This permission allows a user access to and the ability to manage and track physician orders and care plans (i.e. mark orders as sent, received and complete/returned with physician signature.)")]
        AccessOrderManagementCenter = 1 << 24,
        [Permission("Manage Medication Profile/History/Snapshot/Pharmacies", "Manage Medication Profile/History/Snapshot/Pharmacies", "Clinical", "This permission allows a user access to create, edit and delete patient medication/pharmacies information.")]
        ViewMedicationProfile = 1 << 25,
        [Permission("Dashboard", "Dashboard", "General", "")]
        Dashboard = 1 << 26,
        [Permission("Messaging", "Messaging", "General", "")]
        Messaging = 1 << 27,
        [Permission("Access Personal Profile", "Access Personal Profile", "General", "")]
        AccessPersonalProfile = 1 << 28,
        [Permission("View Scheduled Tasks", "View Scheduled Tasks", "General", "")]
        ViewScheduledTasks = 1 << 29,
        [Permission("View Patient Profile", "View Patient Profile", "General", "")]
        ViewPatientProfile = 1 << 30,
        [Permission("View Exported OASIS", "View Exported OASIS", "OASIS", "This permission allows a user to see OASIS assessments that have been exported and also to cancel OASIS assessments that are already submitted.")]
        ViewExportedOasis = (ulong)1 << 31,
        [Permission("Create OASIS Export file", "Create OASIS Export file", "OASIS", "This permission allows a user to create the OASIS export data file that is submitted to CMS or the designated OASIS office.")]
        CreateOasisSubmitFile = (ulong)1 << 32,
        [Permission("Approve Completed Documents", "Approve Completed Documents", "QA", "This permission allows a user the ability to review, edit and approve documents that have been submitted by users. This permission is typically granted to case managers, QA nurses or nursing directors.")]
        ApproveCompletedDocuments = (ulong)1 << 33,
        [Permission("Bypass QA Center", "Bypass Quality Assurance Center", "Clinical", "This permission will allow your clinical staff to submit documents that bypass the quality assurance review process. This permission can be reserved for Directors of Nursing or Case Managers. Documents submitted by persons with this permission will be marked as “completed” instead of “submitted pending QA review”.")]
        BypassCaseManagement = (ulong)1 << 34,
        [Permission("Reopen Documents", "Reopen Documents", "QA", "This permission allows a user to reopen completed tasks/documents.")]
        ReopenDocuments = (ulong)1 << 35,
        [Permission("Access Reports Center", "Access Reports Center", "Reporting", "This permission will allows a user to access to the report center, where they will be able to run a number of reports, such as patient roster, CAHPS reports, aged account receivable, survey census, unduplicated census report.  Reports can be exported to excel for additional options.")]
        AccessReports = (ulong)1 << 36,
        [Permission("Access Schedule Center", "Access Schedule Center", "None", "")]
        AccessScheduleCenter = (ulong)1 << 37,
        [Permission("Schedule Visits/Activites", "Schedule Visits/Activites", "Schedule Management", "This permission allows a user to schedule visits and other activities. Examples of tasks/activities that can be scheduled include: nursing, therapy, social work or home health aide visits. OASIS assessments, care summaries, etc are among other documents that can be scheduled.")]
        ScheduleVisits = (ulong)1 << 38,
        [Permission("Edit Episode Information", "Edit Episode Information", "Schedule Management", "This permission allows a user to edit the episode information i.e. change episode dates or adjust the length of an episode.")]
        EditEpisode = (ulong)1 << 39,
        [Permission("Add/Edit/Delete Physician Information", "Manage Physician Information", "Clerical", "This permission allows a user to add, edit or delete physicians and physician information.")]
        ManagePhysicians = (ulong)1 << 40,
        [Permission("Create Incident/Accident & Infection Log", "Create Incident/Accident & Infection Log", "Clerical", "This permission allows a user to create incidents, accident and infection logs.")]
        ManageIncidentAccidentInfectionReport = (ulong)1 << 41,
        [Permission("View Previous Notes", "View Previous Notes", "Clinical", "This permission allows a user the ability to view and copy previous documentation. This permission helps users compare documentation and facilitate faster documentation and helps improve compliance.")]
        ViewPreviousNotes = (ulong)1 << 42,
        [Permission("Access Patient Reports", "Access Patient Reports", "Reporting", "This permission grants a user access to be able to view and generate reports on patients. Reports can be exported to excel for additional options.")]
        AccessPatientReports = (ulong)1 << 43,
        [Permission("Access Clinical Reports", "Access Clinical Reports", "Reporting", "This permission grants a user access to be able to view and generate clinical reports such as missed visit reports, orders, assessments etc. Reports can be exported to excel for additional options.")]
        AccessClinicalReports = (ulong)1 << 44,
        [Permission("Access Schedule Reports", "Access Schedule Reports", "Reporting", "This permission grants a user access to be able to view and generate scheduling reports. Reports can be exported to excel for additional options.")]
        AccessScheduleReports = (ulong)1 << 45,
        [Permission("Access Billing/Financial Reports", "Access Billing/Financial Reports", "Reporting", "This permission grants a user access to be able to view and generate billing and financial reports. Reports can be exported to excel for additional options.")]
        AccessBillingReports = (ulong)1 << 46,
        [Permission("Access Employee Reports", "Access Employee Reports", "Reporting", "This permission grants a user access to view and generate employee reports.")]
        AccessEmployeeReports = (ulong)1 << 47,
        [Permission("Access Statistical Reports", "Access Statistical Reports", "Reporting", "This permission grants a user access to view and generate statistical reports.")]
        AccessStatisticalReports = (ulong)1 << 48,
        [Permission("Manage Company Information", "Manage Company Information", "Administration", "This permission grants a user access to modify company information such as address, phone and fax.")]
        ManageAgencyInformation = (ulong)1 << 49,
        [Permission("State Surveyor/Auditor", "State Surveyor/Auditor", "State Survey/Audit", "This permission grants access to a state surveyor/auditor to review patient records. After granting this access, you must go edit individual patient charts to grant the surveyor/auditor access to that particular patient record.")]
        StateSurveyorAuditorAccess = (ulong)1 << 50,
        [Permission("Remittance Advice", "Remittance Advice", "Billing", "This permission will allow a user access to Remittance Advice.  The permission will also allow a user the ability to post Medicare payments directly to the corresponding claims.")]
        RemittanceAdvice = (ulong)1 << 51,
        [Permission("Manage Access to Patient Charts", "Manage Access to Patient Charts", "Clerical", "This permission will allow a user to grant or deny access to patient charts.")]
        ManagePatientAccess = (ulong)1 << 52,
        [Permission("Access Payroll Reports", "Access Payroll Reports", "Reporting", "This permission grants a user access to view and generate payroll reports.")]
        AccessPayrollDetailSummaryReports = (ulong)1 << 53,
    }
}