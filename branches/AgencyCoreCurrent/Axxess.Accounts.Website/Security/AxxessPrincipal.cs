﻿namespace Axxess.Accounts.Website.Security
{
    using System;
    using System.Security.Principal;
    using System.Collections.Generic;

    using Axxess.Framework.Extensions;

    using Axxess.Accounts.Entities;
    using Axxess.Accounts.Entities.Enums;

    [Serializable]
    public class AxxessPrincipal : IPrincipal
    {
        #region Private Members

        private Roles roleId;
        private AxxessIdentity identity;
        private Permissions permissions;

        #endregion

        #region Constructor

        public AxxessPrincipal(AxxessIdentity identity)
            : this(identity, Roles.ApplicationUser, new List<string>())
        {
        }

        public AxxessPrincipal(AxxessIdentity identity, Roles roleId)
            : this(identity, roleId, new List<string>())
        {
        }

        public AxxessPrincipal(AxxessIdentity identity, List<string> rights)
            : this(identity, Roles.ApplicationUser, rights)
        {
        }

        public AxxessPrincipal(AxxessIdentity identity, Roles roleId, List<string> rights)
        {
            this.roleId = roleId;
            this.identity = identity;

            rights.ForEach(i =>
            {
                this.permissions |= (Permissions)Enum.Parse(typeof(Permissions), i);
            });
        }

        #endregion

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return identity; }
        }

        public bool IsInRole(Roles roleId)
        {
            return this.roleId == roleId;
        }

        public bool IsInRole(string role)
        {
            return (Roles)Enum.Parse(typeof(Roles), role, true) == this.roleId;
        }

        public bool HasPermission(Permissions permission)
        {
            return this.permissions.Has(permission);
        }

        #endregion
    }
}