﻿namespace Axxess.Accounts.Website.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Axxess.Accounts.Components;
    using Axxess.Accounts.Data;
    using Axxess.Accounts.Entities.Enums;
    using Axxess.Framework.Extensions;
    using Axxess.Framework.Validation;
    using Models;

    public class LoginValidation : ILoginValidation
    {
        #region Private Members / Contructors

        private readonly IMembershipProvider membershipProvider;

        public LoginValidation()
        {
            this.membershipProvider = BusinessFactory.Instance.Get<IMembershipProvider>();
        }

        private T Validate<T>(params Validation[] validations) where T : BaseViewData, new()
        {
            var entityValidator = new EntityValidator(validations);
            entityValidator.Validate();

            if (entityValidator.IsValid)
            {
                return new T { isSuccessful = true };
            }
            else
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }
        }

        #endregion

        #region IAuthorizationService Members

        public LoginViewData Validate(string emailAddress, string password)
        {
            LoginViewData result = this.Validate<LoginViewData>(
                            new Validation(() => emailAddress.IsNullOrEmpty(), "You must specify an E-mail Address. "),
                            new Validation(() => password.IsNullOrEmpty(), "You must specfiy a Password. "),
                            new Validation(() => !emailAddress.IsEmail(), "The E-mail Address is not valid.<br/>")
                        );
            try
            {
                if (result.isSuccessful)
                {
                    result.isSuccessful = false;
                    result.errorMessage = "E-mail Address/Password could not be verified at this time. Please try again.";

                    var readAccountRepository = RepositoryFactory.Instance.Get<IAccountRepository>("ReadMembership");
                    if (readAccountRepository != null)
                    {
                        var login = readAccountRepository.GetByEmailAddress(emailAddress.Trim());
                        if (login != null)
                        {
                            if (login.IsLocked)
                            {
                                result.errorMessage = "This account has been locked. Please contact your Agency/Company's Administrator.";
                            }
                            else
                            {
                                if (!login.IsActive)
                                {
                                    result.errorMessage = "This account is not active at this time. Please contact your Administrator.";
                                }
                                else
                                {
                                    if (!this.membershipProvider.VerifyPassword(password.Trim(), login.PasswordHash, login.PasswordSalt))
                                    {
                                        result.errorMessage = "The E-mail Address/Password combination failed. Please try again.";
                                    }
                                    else
                                    {
                                        ApplicationRoles role = login.Role.ToEnum<ApplicationRoles>(ApplicationRoles.None);
                                        if (role != ApplicationRoles.ApplicationUser)
                                        {
                                            result.errorMessage = "This account type is not authorized to use this application. Please try again.";
                                        }
                                        else
                                        {
                                            var userAccounts = readAccountRepository.GetUserAccounts(login.Id);
                                            result.Agencies = new List<dynamic>();
                                            if (userAccounts != null && userAccounts.Count() >= 1)
                                            {
                                                result.Agencies = userAccounts.ToList<dynamic>();
                                                if (result.Agencies != null)
                                                {
                                                    if (result.Agencies.Count() > 1)
                                                    {
                                                        result.hasMultipleAccounts = true;
                                                    }
                                                    result.errorMessage = "";
                                                    result.isSuccessful = true;
                                                }                                               
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.exception = exception.ToString();
            }

            return result;
        }

        #endregion
    }
}