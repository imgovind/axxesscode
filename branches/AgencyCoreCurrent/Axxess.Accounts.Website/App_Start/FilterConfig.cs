﻿namespace Axxess.Accounts.Website
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Http.Filters;

    using Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new QuerystringEncryptionFilter());
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterHttpFilters(HttpFilterCollection filters)
        {
            filters.Add(new AllowCrossSiteAttribute());
        }
    }
}