﻿namespace Axxess.Accounts.Website
{
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/plugins/jquery.form.js",
                "~/Scripts/plugins/jquery.validate.js",
                "~/Scripts/plugins/jquery.jgrowl.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                "~/Scripts/custom/account.js",
                "~/Scripts/custom/utility.js"
            ));

            bundles.Add(new StyleBundle("~/Content/Auth").Include(
                "~/Content/account.css",
                "~/Content/font-awesome/css/font-awesome.min.css"
            ));
            BundleTable.EnableOptimizations = true;
        }
    }
}