﻿namespace Axxess.Accounts.Website
{
    using System.Web.Mvc;
    using System.Web.Http;
    using System.Web.Routing;
    using System.Web.Optimization;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Description;

    using Axxess.Framework;
    using Axxess.Framework.Extensibility;

    using Filters;
    using Controllers;

    using Axxess.Accounts.Entities;
    using Axxess.Accounts.Data;

    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Framework.Initialize();
            EntityMap.Register();
            DataRegister.Initialize();
        }

        public static void Run()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            FilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //Exposing API to other origins
            GlobalConfiguration.Configuration.MessageHandlers.Add(new OptionsHttpMessageHandler());
        }
    }
}
