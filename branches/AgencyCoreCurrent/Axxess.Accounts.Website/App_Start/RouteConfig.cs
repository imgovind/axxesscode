﻿namespace Axxess.Accounts.Website
{
    using System.Web.Mvc;
    using System.Web.Http;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{file}.png");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            routes.IgnoreRoute("Images/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");
            routes.IgnoreRoute("Tool/{*pathInfo}");

            routes.MapRoute(
                name: "Login",
                url: "Login",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Forgot",
                url: "Forgot",
                defaults: new { controller = "Account", action = "Forgot", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Reset",
               "Reset",
               new { controller = "Account", action = "Reset", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Activate",
               "Activate",
               new { controller = "Account", action = "Activate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Logout",
                "Logout",
                new { controller = "Account", action = "LogOff", id = UrlParameter.Optional }
            );

            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{action}",
                defaults: new { id = RouteParameter.Optional, action = "RESTAction" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}