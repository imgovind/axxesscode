﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Data;

    using HtmlAgilityPack;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;

    public static class DataExtensions
    {
        public static string GetValue(this DataRow dataRow, int columnIndex)
        {
            if (dataRow != null && columnIndex < dataRow.ItemArray.Length)
            {
                if (dataRow[columnIndex] != null && dataRow[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRow[columnIndex].ToString().Trim();
                }
            }
            return string.Empty;
        }

        public static string ToText(this DataRecord dataRow)
        {
            if (dataRow != null && dataRow.Values.Count > 0)
            {
                var text = new StringBuilder();
                dataRow.Values.ForEach(value =>
                {
                    text.AppendFormat("{0} ", value);
                });
                return text.ToString().Trim();
            }
            return string.Empty;
        }

        public static string ToText(this DataRow dataRow)
        {
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                var i = 0;
                var text = new StringBuilder();
                dataRow.ItemArray.ForEach(item =>
                {
                    text.AppendFormat("{0} ", dataRow.GetValue(i));
                    i++;
                });
                return text.ToString().Trim();
            }
            return string.Empty;
        }

        public static string GetValue(this DataRecord dataRecord, int columnIndex)
        {
            if (dataRecord != null && columnIndex < dataRecord.Values.Count)
            {
                if (dataRecord[columnIndex] != null && dataRecord[columnIndex].ToString().IsNotNullOrEmpty())
                {
                    return dataRecord[columnIndex].ToString().Trim().Replace("'", "").Replace("\"", "").Replace("\\", "");
                }
            }
            return string.Empty;
        }

        public static bool IsEmpty(this DataRow dataRow)
        {
            var result = true;
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                {
                    if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public static bool WithAtLeastXColumn(this DataRow dataRow, int columns)
        {
            var counter = 0;
            var result = false;
            if (dataRow != null && dataRow.ItemArray.Length > 0)
            {
                for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                {
                    if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                    {
                        counter++;
                    }
                }
            }
            if (counter >= columns)
            {
                result = true;
            }
            return result;
        }

        public static string ToPhoneDB(this string text)
        {
            if (text.IsNotNullOrEmpty() && text.Length >= 10)
            {
                return text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Replace("\r","").Replace("\n","");
            }
            return string.Empty;
        }

        public static void SetVisiTrakPatient(this HtmlNode node, int tableNumber, int columnNumber, Patient patient, string patientPrimaryInsurance)
        {

            if (node != null && patient != null)
            {
                switch (tableNumber)
                {
                    case 1:
                        if (columnNumber == 2)
                        {
                            patient.LastName = node.InnerText.Split(',')[0].Trim();

                            var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (firstName.Length > 1)
                            {
                                patient.FirstName = firstName[0].Trim();
                                patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                            }
                            else
                            {
                                patient.FirstName = node.InnerText.Split(',')[1].Trim();
                            }
                        }
                        if (columnNumber == 3)
                        {
                            if (node.InnerText.IsNotNullOrEmpty())
                            {
                                patient.StartofCareDate = node.InnerText.ToMySqlDate();
                            }
                        }
                        if (columnNumber == 4)
                        {
                            if (node.InnerText.IsNotNullOrEmpty() && !node.InnerText.Trim().IsEqual("-"))
                            {
                                //var dateRange = node.InnerText.Trim().Split('-');
                                //if (dateRange.Length > 1)
                                //{
                                //    var episodeStart = dateRange[0].ToMySqlDate();
                                //    var episodeEnd = dateRange[1].ToMySqlDate();

                                //    if (episodeEnd.Year < 2011)
                                //    {
                                //        patient.Status = 2;
                                //    }
                                //}
                                patient.Comments += string.Format("Last Certification Period: {0}. ", node.InnerText.Replace("\r","").Replace("\n","").Replace(" ",""));
                            }
                        }
                        if (columnNumber == 5)
                        {
                            patient.PhoneHome = node.InnerText.ToPhoneDB();
                        }
                        if (columnNumber == 7)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                                {
                                    patient.PrimaryInsurance = patientPrimaryInsurance;
                                    patient.Comments += string.Format("Insurance => : {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                                }
                                else if (node.InnerText.Trim().ToLower().StartsWith("medicaid"))
                                {
                                    patient.PrimaryInsurance = patientPrimaryInsurance;
                                    patient.Comments += string.Format("Insurance => : {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                                }
                                else
                                {
                                    patient.PrimaryInsurance = null;
                                    patient.Comments += string.Format("Insurance: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                                }
                            }
                        }

                        if (columnNumber == 8)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Acuity Level: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Replace(" ", "").Trim());
                            }
                        }

                        if (columnNumber == 9)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                //New - Data Load - KELLY HOME HEALTH
                                var mdNameArray = node.InnerText.Split(',');
                                //string mdName = default(string);
                                //StringBuilder sb = new StringBuilder();
                                //var temp = mdNameArray[1].Replace("\r", "").Trim();
                                //temp = temp.Replace("\n", "").Trim();
                                
                                //var temp2 = mdNameArray[0].Replace("DR:", "").Trim();
                                //temp2 = temp2.Replace("\n", "").Trim();
                                //temp2 = temp2.Replace("\r", "").Trim();
                                //sb.Append("DR: ");
                                //sb.Append(temp);
                                //sb.Append(" ");
                                //sb.Append(temp2);
                                //mdName = sb.ToString();
                                //sb.Length = 0;
                                //if (mdNameArray.Count() == 2)
                                //{

                                //}
                                //else if (mdNameArray.Count() == 1)
                                //{
                                    
                                //}
                                //else if (mdNameArray.Count() == 0)
                                //{
                                //    mdName = mdNameArray[0].Replace("\r", "").Trim();
                                //    mdName += " ";
                                //    if (mdNameArray.Count() > 1)
                                //    {
                                //        int gogog = 0;
                                //        mdName += mdNameArray[1].Trim();
                                //        mdName += ". ";
                                //    }
                                //    else
                                //    {
                                //        int bobob = 0;
                                //    }
                                //    patient.Comments += mdName;
                                //}
                                //Old
                                string mdName = default(string);
                                if (mdNameArray.Count() > 0)
                                {
                                    mdNameArray[0] = mdNameArray[0].Replace("\r", "").Trim();
                                    mdNameArray[0] = mdNameArray[0].Replace("\n", "").Trim();
                                    mdName += mdNameArray[0] + ", ";
                                    if (mdNameArray.Count() > 1)
                                    {
                                        mdNameArray[1] = mdNameArray[1].Replace("\r", "").Trim();
                                        mdNameArray[1] = mdNameArray[1].Replace("\n", "").Trim();
                                        mdName += mdNameArray[1];
                                    }
                                    patient.Comments += mdName;
                                }
                                else
                                {
                                    patient.Comments += string.Format("{0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                                }
                                patient.Comments += " ;";
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Replace(" ", "").Trim());
                            }
                        }

                        break;
                    case 2:
                        if (columnNumber == 2)
                        {
                            patient.PatientIdNumber = node.InnerText.Trim().Replace("MR# ", "").Trim();
                        }
                        if (columnNumber == 5)
                        {
                            var address = node.InnerHtml.IsNotNullOrEmpty()? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase):string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Replace("<Font Color=\"#000000\" Face=\"Arial\" Size=\"1\">", "").Replace("<font color=\"#000000\" face=\"ARIAL\" size=\"1\">", "").Replace("<Font Size=\"1\" Color=\"#000000\" Face=\"Arial\">", "");

                            var addressArray = address.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                            if (addressArray.Length > 1)
                            {
                                //New - Added while doing data load for KELLY HOME HEALTH
                                var addressLine1Array = addressArray[0].Split('\n');
                                if (addressLine1Array.Count() > 0)
                                {
                                    string addrLine1 = addressLine1Array[0].Replace("\r", "").Trim();
                                    addrLine1 += " ";
                                    if (addressLine1Array.Count() > 1)
                                    {
                                        addrLine1 += addressLine1Array[1].Trim();
                                    }
                                    patient.AddressLine1 = addrLine1;
                                }
                                //Old
                                //patient.AddressLine1 = addressArray[0].Replace("\r", "").Replace("\n", "");
                                var cityStateZip = addressArray[1].Split(',');
                                if (cityStateZip.Length > 1)
                                {
                                    patient.AddressCity = cityStateZip[0];

                                    var stateZip = cityStateZip[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                    if (stateZip.Length > 1)
                                    {
                                        patient.AddressStateCode = stateZip[0];
                                        patient.AddressZipCode = stateZip[1];
                                    }
                                }
                            }
                        }
                        if (columnNumber == 6)
                        {
                            if (patient.PrimaryInsurance.IsEqual(patientPrimaryInsurance))
                            {
                                if(patient.Comments.Contains("Medicare"))
                                {
                                    patient.MedicareNumber = node.InnerText.Trim();
                                }
                                else if (patient.Comments.Contains("Medicaid"))
                                {
                                    patient.MedicaidNumber = node.InnerText.Trim();
                                }
                                else 
                                {
                                    patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                                }
                            }
                            else
                            {
                                patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                            }
                        }

                        if (columnNumber == 7)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Code: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                            }
                        }

                        if (columnNumber == 8)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                //New - Data Load - KELLY HOME HEALTH
                                var diagDescrArray = node.InnerText.Split('\n');
                                string diagDescr = default(string);
                                if (diagDescrArray.Length > 1)
                                {
                                    diagDescr = diagDescrArray[0].Replace("\r", "").Trim();
                                    diagDescr += " ";
                                    diagDescr += diagDescrArray[1].Trim();
                                    diagDescr += ". ";
                                }
                                else 
                                {
                                    diagDescr = diagDescrArray[0].Replace("\r", "").Trim();
                                    diagDescr += ". ";
                                }
                                patient.Comments += diagDescr;

                                //Old
                                //patient.Comments += string.Format("Diagnosis Description: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                            }
                        }
                        if (columnNumber == 9)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("CM Phone: {0}. ", node.InnerText.Replace("\r", "").Replace("\n", "").Replace(" ", "").Trim());
                            }
                        }

                        break;
                    case 3:
                        if (columnNumber == 2)
                        {
                            patient.DOB = node.InnerText.Replace("DOB: ", "").ToMySqlDate();
                        }

                        break;
                }
            }
        }

        public static void SetVisiTrakPatientTwo(this HtmlNode node, int tableNumber, int columnNumber, Patient patient)
        {
            if (node != null && patient != null)
            {
                switch (tableNumber)
                {
                    case 1:
                        if (columnNumber == 2)
                        {
                            patient.LastName = node.InnerText.Split(',')[0].Trim();

                            var firstName = node.InnerText.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (firstName.Length > 1)
                            {
                                patient.FirstName = firstName[0].Trim();
                                patient.MiddleInitial = firstName[1].Trim().Replace(".", "");
                            }
                            else
                            {
                                patient.FirstName = node.InnerText.Split(',')[1].Trim();
                            }
                        }
                        if (columnNumber == 4)
                        {
                            if (node.InnerText.IsNotNullOrEmpty())
                            {
                                patient.StartofCareDate = node.InnerText.ToMySqlDate();
                            }
                        }
                        if (columnNumber == 6)
                        {
                            if (node.InnerText.IsNotNullOrEmpty() && !node.InnerText.Trim().IsEqual("-"))
                            {
                                //var dateRange = node.InnerText.Trim().Split('-');
                                //if (dateRange.Length > 1)
                                //{
                                //    var episodeStart = dateRange[0].ToMySqlDate();
                                //    var episodeEnd = dateRange[1].ToMySqlDate();

                                //    if (episodeEnd.Year < 2011)
                                //    {
                                //        patient.Status = 2;
                                //    }
                                //}
                                patient.Comments += string.Format("Last Certification Period: {0}. ", node.InnerText);
                            }
                        }
                        if (columnNumber == 8)
                        {
                            patient.PhoneHome = node.InnerText.ToPhoneDB();
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                if (node.InnerText.Trim().ToLower().StartsWith("medicare"))
                                {
                                    patient.PrimaryInsurance = "1";
                                }
                                else
                                {
                                    patient.Comments += string.Format("Insurance: {0}. ", node.InnerText.Trim());
                                }
                            }
                        }

                        if (columnNumber == 12)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Acuity Level: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 14)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 15)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }
                        break;
                    case 2:
                        if (columnNumber == 2)
                        {
                            patient.PatientIdNumber = node.InnerText.Trim().Replace("MR# ", "");
                        }
                        if (columnNumber == 6)
                        {
                            var address = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "");
                            var addressArray = address.Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                            if (addressArray.Length > 1)
                            {
                                patient.AddressLine1 = addressArray[0];
                                var cityStateZip = addressArray[1].Split(',');
                                if (cityStateZip.Length > 1)
                                {
                                    patient.AddressCity = cityStateZip[0];

                                    var stateZip = cityStateZip[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                    if (stateZip.Length > 1)
                                    {
                                        patient.AddressStateCode = stateZip[0];
                                        patient.AddressZipCode = stateZip[1];
                                    }
                                }
                            }
                        }
                        if (columnNumber == 8)
                        {
                            if (patient.PrimaryInsurance.IsEqual("1"))
                            {
                                patient.MedicareNumber = node.InnerText.Trim();
                            }
                            else
                            {
                                patient.Comments += string.Format("Health Insurance Number: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 10)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Code: {0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 11)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("Diagnosis Description: {0}. ", node.InnerText.Trim());
                            }
                        }
                        if (columnNumber == 13)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("{0}. ", node.InnerText.Trim());
                            }
                        }

                        if (columnNumber == 14)
                        {
                            if (node.InnerText.Trim().IsNotNullOrEmpty())
                            {
                                patient.Comments += string.Format("CM Phone: {0}. ", node.InnerText.Trim());
                            }
                        }
                        break;
                    case 3:
                        if (columnNumber == 2)
                        {
                            patient.DOB = node.InnerText.Replace("DOB: ", "").ToMySqlDate();
                        }
                        break;
                }
            }
        }

        public static bool DoesHtmlRowHaveData(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.ToLower().IsEqual("table"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {

                    if (rowNode.Name.ToLower().IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.ToLower().IsEqual("td"))
                            {
                                if (count == 1)
                                {
                                    name = dataNode.InnerText;
                                }
                                count++;
                            }
                        });
                    }
                });

                if (count >= 10 && !name.IsEqual("Name"))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool DoesHtmlRowHaveDataSecond(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("table"))
            {
                var tbody = node.SelectSingleNode("tbody");
                if (tbody != null && tbody.Name.IsEqual("tbody"))
                {
                    tbody.ChildNodes.ToList().ForEach(rowNode =>
                    {

                        if (rowNode.Name.IsEqual("tr"))
                        {
                            rowNode.ChildNodes.ToList().ForEach(dataNode =>
                            {
                                if (dataNode.Name.IsEqual("td"))
                                {
                                    if (count == 1)
                                    {
                                        name = dataNode.InnerText;
                                    }
                                    count++;
                                }
                            });
                        }
                    });

                    if (count >= 10 && !name.IsEqual("Name"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool DoesHtmlRowHaveTBody(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("tbody"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    if (rowNode.Name.IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.IsEqual("td"))
                            {
                                if (count == 1)
                                {
                                    name = dataNode.InnerText;
                                }
                                count++;
                            }
                        });
                    }
                });

                if (count >= 10 && !name.IsEqual("Name"))
                {
                    return true;
                }
            }
            return false;
        }

        public static void SetVisiTrakPhysician(this HtmlNode node, int columnNumber, AgencyPhysician physician)
        {
            if (node != null && physician != null)
            {
                var nodeValue = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Trim();
                if (columnNumber == 2)
                {
                    var physicianName = nodeValue;
                    if (physicianName.ToUpper().Contains("GRAND") && physicianName.ToUpper().Contains("MEDICAL") && physicianName.ToUpper().Contains("CLINIC")) 
                    {
                        physician.LastName = nodeValue.Split(',')[0].Trim();

                        var firstName = nodeValue.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        if (firstName.Length > 1)
                        {
                            physician.FirstName = firstName[0].Trim();
                            physician.MiddleName = firstName[1].Trim().Replace(".", "");
                        }
                        else
                        {
                            physician.FirstName = nodeValue.Split(',')[1].Trim();
                        }
                    }
                    else
                    {
                        var physicianArray = nodeValue.Split(',');
                        if (physicianArray.Length > 1)
                        {
                            physicianArray[0] = physicianArray[0].Replace("\n", "");
                            physicianArray[0] = physicianArray[0].Replace("\r", "");
                            physicianArray[0] = physicianArray[0].Trim();
                            physician.LastName = physicianArray[0];

                            var physicianFiMiArray1 = physicianArray[1].Split('\n');
                            for (int i = 0; i < physicianFiMiArray1.Length; i++)
                            {
                                physicianFiMiArray1[i] = physicianFiMiArray1[i].Trim();
                            }
                            if (physicianFiMiArray1.Length > 1)
                            {
                                var physicianFiMiArray = physicianFiMiArray1[1].Split(' ');
                                if (physicianFiMiArray.Length > 1)
                                {
                                    physician.FirstName = physicianFiMiArray[0];
                                    physician.MiddleName = physicianFiMiArray[1];
                                }
                                else
                                {
                                    physician.FirstName = physicianFiMiArray[0];
                                }
                            }
                            else 
                            {
                                physician.FirstName = physicianFiMiArray1[0].Trim();
                            }
                        }
                    }
                    
                }
                if (columnNumber == 4)
                {
                    physician.AddressLine1 = nodeValue;
                }
                if (columnNumber == 5)
                {
                    physician.AddressCity = nodeValue;
                }
                if (columnNumber == 6)
                {
                    physician.AddressStateCode = nodeValue;
                }
                if (columnNumber == 7)
                {
                    physician.AddressZipCode = nodeValue;
                }
                if (columnNumber == 8)
                {
                    physician.PhoneWork = nodeValue.ToPhoneDB();
                }
                if (columnNumber == 9)
                {
                    physician.FaxNumber = nodeValue.ToPhoneDB();
                }

                if (columnNumber == 10 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("UPIN: {0}. ", nodeValue.Trim());
                }

                if (columnNumber == 11 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.NPI = nodeValue;
                }

                if (columnNumber == 12 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.LicenseNumber = nodeValue;
                }

                if (columnNumber == 13 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("Tax ID: {0}. ", nodeValue);
                }

            }
        }

        public static void SetVisiTrakPhysicianTwo(this HtmlNode node, int columnNumber, AgencyPhysician physician)
        {
            if (node != null && physician != null)
            {
                var nodeValue = node.InnerHtml.IsNotNullOrEmpty() ? Regex.Replace(node.InnerHtml, @"\<\/?font[^>]*\>", "", RegexOptions.IgnoreCase) : string.Empty;// node.InnerHtml.Replace("<font face=\"ARIAL\" size=\"1\" color=\"#000000\">", "").Replace("</font>", "").Trim();
                if (columnNumber == 2)
                {
                    physician.LastName = nodeValue.Split(',')[0].Trim();

                    var firstName = nodeValue.Split(',')[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (firstName.Length > 1)
                    {
                        physician.FirstName = firstName[0].Trim();
                        physician.MiddleName = firstName[1].Trim().Replace(".", "");
                    }
                    else
                    {
                        physician.FirstName = nodeValue.Split(',')[1].Trim();
                    }
                }
                if (columnNumber == 4)
                {
                    physician.AddressLine1 = nodeValue;
                }
                if (columnNumber == 6)
                {
                    physician.AddressCity = nodeValue;
                }
                if (columnNumber == 8)
                {
                    physician.AddressStateCode = nodeValue;
                }
                if (columnNumber == 10)
                {
                    physician.AddressZipCode = nodeValue;
                }
                if (columnNumber == 12)
                {
                    physician.PhoneWork = nodeValue.ToPhoneDB();
                }
                if (columnNumber == 14)
                {
                    physician.FaxNumber = nodeValue.ToPhoneDB();
                }

                if (columnNumber == 16 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("UPIN: {0}. ", nodeValue.Trim());
                }

                if (columnNumber == 18)
                {
                    physician.NPI = nodeValue;
                }

                if (columnNumber == 20 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.LicenseNumber = nodeValue;
                }

                if (columnNumber == 22 && nodeValue.Trim().IsNotNullOrEmpty())
                {
                    physician.Comments += string.Format("Tax ID: {0}. ", nodeValue);
                }

            }
        }

        public static bool DoesHtmlRowHavePhysicianData(this HtmlNode node)
        {
            int count = 0;
            var name = string.Empty;
            if (node != null && node.Name.IsEqual("table"))
            {
                node.ChildNodes.ToList().ForEach(rowNode =>
                {
                    if (rowNode.Name.IsEqual("tr"))
                    {
                        rowNode.ChildNodes.ToList().ForEach(dataNode =>
                        {
                            if (dataNode.Name.IsEqual("td"))
                            {
                                if (count == 1)
                                {
                                    name = dataNode.InnerText;
                                }
                                count++;
                            }
                        });
                    }
                });

                if (count >= 10 && !name.IsEqual("Doctor"))
                {
                    return true;
                }
            }
            return false;
        }

        public static IDictionary<string, NotesQuestion> ToDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.Note != null)
            {
                var noteQuestions = patientVisitNote.Note.ToObject<List<NotesQuestion>>();
                noteQuestions.ForEach(n =>
                {
                    questions.Add(n.Name, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToWoundDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.WoundNote != null)
            {
                var noteQuestions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                noteQuestions.ForEach(n =>
                {
                    questions.Add(n.Name, n);
                });
            }
            return questions;
        }

        public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        {
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty()) return questions[key].Answer.Unclean();
            else return string.Empty;
        }

        public static Guid AnswerOrEmptyGuid(this IDictionary<string, NotesQuestion> questions, string key)
        {
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty() && questions[key].Answer.IsGuid())
            {
                return questions[key].Answer.ToGuid();
            }

            return Guid.Empty;
        }
    }
}
