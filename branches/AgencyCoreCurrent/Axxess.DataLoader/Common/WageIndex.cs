﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    public class WageIndex
    {
        public WageIndex() { this.Counties = new List<string>(); }
        public string CBSA { get; set; }
        public string MetroArea { get; set; }
        public double TwentyEleven { get; set; }
        public List<string> Counties { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder()
            .AppendFormat("CBSA: {0}{1}", this.CBSA, Environment.NewLine)
            .AppendFormat("Wage Index: {0}{1}", this.TwentyEleven, Environment.NewLine);
            this.Counties.ForEach(c =>
            {
                stringBuilder.AppendFormat("{0}{1}", c, Environment.NewLine);
            });
            stringBuilder.AppendFormat("{0}", Environment.NewLine);
            return stringBuilder.ToString();
        }
    }
}
