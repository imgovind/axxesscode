﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    public class ZipCbsaEntry
    {
        public int Id { get; set; }
        public string Zip { get; set; }
        public string CBSA { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CountyCode { get; set; }
        public string CountyTitle { get; set; }
        public double WageIndex2011 { get; set; }
        public double WageIndex2012 { get; set; }
        public double WageIndex2013 { get; set; }
        public bool IsFound { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
            .AppendFormat("Zip: {0}{1}", this.Zip, Environment.NewLine)
            .AppendFormat("CBSA: {0}{1}", this.CBSA, Environment.NewLine)
            .AppendFormat("CityName: {0}{1}", this.CityName, Environment.NewLine)
            .AppendFormat("State Code: {0}{1}", this.StateCode, Environment.NewLine)
            .AppendFormat("State Name: {0}{1}", this.StateName, Environment.NewLine)
            .AppendFormat("CountyCode: {0}{1}", this.CountyCode, Environment.NewLine)
            .AppendFormat("CountyTitle: {0}{1}", this.CountyTitle, Environment.NewLine)
            .AppendFormat("Wage Index: {0}{1}", this.WageIndex2011, Environment.NewLine)
            .AppendFormat("{0}", Environment.NewLine).ToString();
        }

        public string ToSQL()
        {
            return new StringBuilder("INSERT INTO `cbsacodes_new` (`Zip`, `CityName`, `CountyCode`, `CountyTitle`, `StateCode`, ")
            .Append("`StateName`, `CBSA`, `WITwoSeven`, `WITwoEight`, `WITwoNine`, `WITwoTen`, `WITwoEleven`, `Variance`) ")
            .AppendFormat("VALUES ('{0}', '{1}', '{2}', '{3}', ", Zip, CityName, CountyCode, CountyTitle)
            .AppendFormat("'{0}', '{1}', '{2}', 0, 0, 0, 0, {3}, 0);", StateCode, StateName, CBSA, WageIndex2011)
            .ToString();
        }

        public string ToUpdateSQL()
        {
            return new StringBuilder("UPDATE `cbsacodes` SET ")
            .AppendFormat("`CBSA` = {0}, ", this.CBSA)
            .AppendFormat("`WITwoThirteen` = {0} WHERE `Id` = {1};", this.WageIndex2013, this.Id)
            .ToString();
        }
    }
}
