﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class GenericExcelScript
    {
        private static string input = Path.Combine(App.Root, "Files\\assuring.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\assuring_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Status = 1;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            patientData.Gender = "";
                                            
                                            var nameArray = dataRow.GetValue(1).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                patientData.LastName = nameArray[0];
                                                var firstNameArray = nameArray[1].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (firstNameArray != null && firstNameArray.Length == 2)
                                                {
                                                    patientData.FirstName = firstNameArray[0].Trim();
                                                    patientData.MiddleInitial = firstNameArray[1].Trim();
                                                }
                                                else
                                                {
                                                    patientData.FirstName = nameArray[1].Trim();
                                                }
                                            }
                                            if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(2)));
                                            }
                                            if (dataRow.GetValue(3).IsNotNullOrEmpty())
                                            {
                                                var addressArray = dataRow.GetValue(3).Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                if (addressArray != null && addressArray.Length > 1)
                                                {
                                                    patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                    patientData.AddressLine2 = "";

                                                    var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (locationArray != null && locationArray.Length > 2)
                                                    {
                                                        locationArray = locationArray.Reverse();
                                                        patientData.AddressZipCode = locationArray[0];
                                                        patientData.AddressStateCode = "TX";

                                                        if (locationArray.Length > 3)
                                                        {
                                                            int cityNameCount = 2;
                                                            var city = new string[locationArray.Length - cityNameCount];

                                                            do
                                                            {
                                                                city[cityNameCount - 2] = locationArray[cityNameCount];
                                                                cityNameCount++;
                                                            } while (cityNameCount < locationArray.Length);

                                                            if (city.Length > 0)
                                                            {
                                                                city = city.Reverse();
                                                                city.ForEach(name =>
                                                                {
                                                                    patientData.AddressCity += string.Format("{0} ", name.Replace(",", ""));
                                                                });
                                                                patientData.AddressCity = patientData.AddressCity.Trim();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            patientData.AddressCity = locationArray[2].Replace(",", "");
                                                        }
                                                    }
                                                }
                                            }
                                            patientData.PhoneHome = dataRow.GetValue(4).ToPhoneDB();
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(5)));
                                            }

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId,
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };


                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                var admissionPeriod = new PatientAdmissionDate
                                                {
                                                    Id = Guid.NewGuid(),
                                                    AgencyId = agencyId,
                                                    Created = DateTime.Now,
                                                    DischargedDate = DateTime.MinValue,
                                                    IsActive = true,
                                                    IsDeprecated = false,
                                                    Modified = DateTime.Now,
                                                    PatientData = patientData.ToXml().Replace("'", ""),
                                                    PatientId = patientData.Id,
                                                    Reason = string.Empty,
                                                    StartOfCareDate = patientData.StartofCareDate,
                                                    Status = patientData.Status
                                                };
                                                if (Database.Add(admissionPeriod))
                                                {
                                                    var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    if (patient != null)
                                                    {
                                                        patient.AdmissionId = admissionPeriod.Id;
                                                        if (Database.Update(patient))
                                                        {
                                                            Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                        }
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
