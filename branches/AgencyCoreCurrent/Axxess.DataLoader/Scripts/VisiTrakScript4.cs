﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using HtmlAgilityPack;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    public static class VisiTrakScript4
    {
        private static string input = Path.Combine(App.Root, "Files\\Patient-Roster-Discharged.HTM");
        private static string output = Path.Combine(App.Root, string.Format("Files\\PatientRoster_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\LOG{0}.txt", DateTime.Now.Ticks.ToString()));

        private static readonly SubSonic.Repository.SimpleRepositoryOptions dataOptions = new SubSonic.Repository.SimpleRepositoryOptions();

        private static readonly SubSonic.Repository.SimpleRepository dataSS = new SubSonic.Repository.SimpleRepository("AgencyManagementConnectionString", dataOptions);

        private static readonly PatientRepository patientRepository = new PatientRepository(dataSS);


        public static void Run(Guid agencyId, Guid locationId)
        {
            string agencyPayorId = "1";
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.Load(input);
                    Console.WriteLine(htmlDocument.ToString());
                    int i = 1;
                    int counter = 1;
                    Patient patientData = null;
                    int gcounter = 0;
                    htmlDocument.DocumentNode.ChildNodes.ToList().ForEach(htmlNode =>
                    {
                        if (htmlNode.Name.IsEqual("html"))
                        {
                            htmlNode.ChildNodes.ToList().ForEach(bodyNode =>
                            {
                                if (bodyNode.Name.IsEqual("body"))
                                {
                                    bodyNode.ChildNodes.ToList().ForEach(tableNode =>
                                    {
                                        if (tableNode.DoesHtmlRowHaveData())
                                        {
                                            TextWriter txtWrtr = new StreamWriter(log, true);
                                            StringBuilder sb = new StringBuilder();
                                            string consolePatientName = default(string);
                                            string consolePatientMRN = default(string);
                                            bool checkPatientExisting = default(bool);
                                            int dataCounter = 1;
                                            if (counter % 3 == 1)
                                            {
                                                // Table 1
                                                patientData = new Patient();
                                                patientData.Id = Guid.NewGuid();
                                                patientData.AgencyId = agencyId;
                                                patientData.AgencyLocationId = locationId;
                                                patientData.Status = 1;
                                                patientData.PrimaryInsurance = "1";
                                                patientData.Ethnicities = string.Empty;
                                                patientData.MaritalStatus = string.Empty;
                                                patientData.IsDeprecated = false;
                                                patientData.IsHospitalized = false;
                                                patientData.Gender = "";

                                                tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                                {
                                                    if (tRowNode.Name.IsEqual("tr"))
                                                    {
                                                        tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                        {
                                                            if (dataNode.Name.IsEqual("td"))
                                                            {
                                                                dataNode.SetVisiTrakPatient(1, dataCounter, patientData, agencyPayorId);
                                                                dataCounter++;
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else if (counter % 3 == 0)
                                            {
                                                // Table 3
                                                tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                                {
                                                    if (tRowNode.Name.IsEqual("tr"))
                                                    {
                                                        tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                        {
                                                            if (dataNode.Name.IsEqual("td"))
                                                            {
                                                                dataNode.SetVisiTrakPatient(3, dataCounter, patientData, agencyPayorId);
                                                                dataCounter++;
                                                            }
                                                        });
                                                    }
                                                });

                                                patientData.Comments = patientData.Comments.Trim();

                                                patientData.Created = DateTime.Now;
                                                patientData.Modified = DateTime.Now;

                                                Patient patrettaor = Database.GetPatientByMR(patientData.PatientIdNumber, agencyId);

                                                if (patrettaor != null) 
                                                {
                                                    checkPatientExisting = true;
                                                } 
                                                else 
                                                {
                                                    checkPatientExisting = false;
                                                }

                                                gcounter++;
                                                consolePatientName = patientData.LastName + ", " + patientData.FirstName;
                                                consolePatientMRN = patientData.PatientIdNumber;
                                                if (checkPatientExisting != true)
                                                {
                                                    //#region DBWrite
                                                    //var medicationProfile = new MedicationProfile
                                                    //{
                                                    //    Id = Guid.NewGuid(),
                                                    //    AgencyId = agencyId,
                                                    //    PatientId = patientData.Id,
                                                    //    Created = DateTime.Now,
                                                    //    Modified = DateTime.Now,
                                                    //    Medication = "<ArrayOfMedication />"
                                                    //};

                                                    //var allergyProfile = new AllergyProfile
                                                    //{
                                                    //    Id = Guid.NewGuid(),
                                                    //    AgencyId = agencyId,
                                                    //    PatientId = patientData.Id,
                                                    //    Created = DateTime.Now,
                                                    //    Modified = DateTime.Now,
                                                    //    Allergies = "<ArrayOfAllergy />"
                                                    //};



                                                    //if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                    //{
                                                    //    var admissionPeriod = new PatientAdmissionDate
                                                    //    {
                                                    //        Id = Guid.NewGuid(),
                                                    //        AgencyId = agencyId,
                                                    //        Created = DateTime.Now,
                                                    //        DischargedDate = DateTime.MinValue,
                                                    //        IsActive = true,
                                                    //        IsDeprecated = false,
                                                    //        Modified = DateTime.Now,
                                                    //        PatientData = patientData.ToXml().Replace("'", ""),
                                                    //        PatientId = patientData.Id,
                                                    //        Reason = string.Empty,
                                                    //        StartOfCareDate = patientData.StartofCareDate,
                                                    //        Status = patientData.Status
                                                    //    };
                                                    //    if (Database.Add(admissionPeriod))
                                                    //    {
                                                    //        var patient = Database.GetPatient(patientData.Id, agencyId);
                                                    //        if (patient != null)
                                                    //        {
                                                    //            patient.AdmissionId = admissionPeriod.Id;
                                                    //            if (Database.Update(patient))
                                                    //            {
                                                    //                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}
                                                    //#endregion
                                                    sb.Length = 0;
                                                    Console.WriteLine("Table {0} : {1} : {2} : Added : {3}", consolePatientMRN, counter, gcounter, consolePatientName);
                                                    sb.AppendLine(string.Format("Table {0} : {1} : {2} : Not Added : {3}", consolePatientMRN, counter, gcounter, consolePatientName));
                                                    txtWrtr.WriteLine(sb.ToString());
                                                }
                                                else
                                                {
                                                    sb.Length = 0;
                                                    Console.WriteLine("Table {0} : {1} : {2} : Not Added : {3}", consolePatientMRN, counter, gcounter, consolePatientName);
                                                    sb.AppendLine(string.Format("Table {0} : {1} : {2} : Not Added : {3}", consolePatientMRN, counter, gcounter, consolePatientName));
                                                    txtWrtr.WriteLine(sb.ToString());
                                                }
                                                i++;
                                            }
                                            else if (counter % 3 == 2)
                                            {
                                                // Table 2
                                                tableNode.ChildNodes.ToList().ForEach(tRowNode =>
                                                {
                                                    if (tRowNode.Name.IsEqual("tr"))
                                                    {
                                                        tRowNode.ChildNodes.ToList().ForEach(dataNode =>
                                                        {
                                                            if (dataNode.Name.IsEqual("td"))
                                                            {
                                                                dataNode.SetVisiTrakPatient(2, dataCounter, patientData, agencyPayorId);
                                                                dataCounter++;
                                                            }
                                                        });
                                                    }
                                                });

                                            }
                                            txtWrtr.Close();
                                            counter++;
                                        }
                                    });

                                }
                            });
                        }
                    });
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
