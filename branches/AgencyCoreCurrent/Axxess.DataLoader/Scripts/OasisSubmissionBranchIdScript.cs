﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public static class OasisSubmissionBranchIdScript
    {
        public static void Run(Guid agencyId)
        {
            var socList = Database.GetAssessments<StartOfCareAssessment>(agencyId);
            var locations = Database.GetAgencyLocations(agencyId);
            if (socList != null && socList.Count > 0)
            {
                FixLocationId<StartOfCareAssessment>(socList, locations);
            }

            var rocList = Database.GetAssessments<ResumptionofCareAssessment>(agencyId);
            if (rocList != null && rocList.Count > 0)
            {
                FixLocationId<ResumptionofCareAssessment>(rocList, locations);
            }

            var recertList = Database.GetAssessments<RecertificationAssessment>(agencyId);
            if (recertList != null && recertList.Count > 0)
            {
                FixLocationId<RecertificationAssessment>(recertList, locations);
            }

            var followupList = Database.GetAssessments<FollowUpAssessment>(agencyId);
            if (followupList != null && followupList.Count > 0)
            {
                FixLocationId<FollowUpAssessment>(followupList, locations);
            }

            var deathList = Database.GetAssessments<DeathAtHomeAssessment>(agencyId);
            if (deathList != null && deathList.Count > 0)
            {
                FixLocationId<DeathAtHomeAssessment>(deathList, locations);
            }

            var dischargeList = Database.GetAssessments<DischargeFromAgencyAssessment>(agencyId);
            if (dischargeList != null && dischargeList.Count > 0)
            {
                FixLocationId<DischargeFromAgencyAssessment>(dischargeList, locations);
            }

            var transferList = Database.GetAssessments<TransferDischargeAssessment>(agencyId);
            if (transferList != null && transferList.Count > 0)
            {
                FixLocationId<TransferDischargeAssessment>(transferList, locations);
            }

            var transferNotList = Database.GetAssessments<TransferNotDischargedAssessment>(agencyId);
            if (transferNotList != null && transferNotList.Count > 0)
            {
                FixLocationId<TransferNotDischargedAssessment>(transferNotList, locations);
            }
        }

        private static void FixLocationId<T>(List<T> list, List<AgencyLocation> locations) where T : Assessment, new()
        {
            Console.WriteLine("Item Count: {0}", list.Count);
            list.ForEach(a =>
            {
                var patient = Database.GetPatient(a.PatientId, a.AgencyId);
                if (patient != null && !patient.AgencyLocationId.IsEmpty())
                {
                    var location = locations.FirstOrDefault(l => l.Id == patient.AgencyLocationId);
                    if (location != null)
                    {
                        if (a.SubmissionFormat.IsNotNullOrEmpty())
                        {
                            var locationId = location.BranchIdOther.IsNotNullOrEmpty() ? location.BranchIdOther : location.BranchId.PadRight(10, ' ');
                            //var result = ReplaceDashesInPatientId(a.SubmissionFormat);
                            var result = ReplaceWrongBranchId(a.SubmissionFormat, locationId);

                            if (!result.IsEqual(a.SubmissionFormat))
                            {
                                a.SubmissionFormat = result;
                                Database.UpdateForOasisC<T>(a);
                                Console.WriteLine("{0} updated", a.ToString());
                                Console.WriteLine();
                            }
                            else
                            {
                                Console.WriteLine("Nothing to change");
                                Console.WriteLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("No Submission Format");
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No Agency Location");
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("No Patient Information");
                    Console.WriteLine();
                }
            });
        }

        private static string ReplaceWrongBranchId(string submissionFormat, char branchId)
        {
            var result = submissionFormat;
            if (submissionFormat.IsNotNullOrEmpty() && submissionFormat.Length == 1446)
            {
                result = submissionFormat.ReplaceAt(146, branchId);
                Console.WriteLine("Result Length == SubmissionFormat : {0}", result.Length == submissionFormat.Length);
                Console.WriteLine("Result Lenght: {0}", result.Length);
            }
            return result;
        }

        private static string ReplaceWrongBranchId(string submissionFormat, string branchId)
        {
            var result = submissionFormat;
            if (submissionFormat.IsNotNullOrEmpty() && submissionFormat.Length == 1446)
            {
                result = submissionFormat.Remove(146, 10).Insert(146, branchId);
                Console.WriteLine("Result Length == SubmissionFormat : {0}", result.Length == submissionFormat.Length);
                Console.WriteLine("Result Lenght: {0}", result.Length);
                Console.WriteLine();
            }
            return result;
        }

        private static string ReplaceDashesInPatientId(string submissionFormat)
        {
            var result = submissionFormat;
            if (submissionFormat.IsNotNullOrEmpty() && submissionFormat.Length == 1446)
            {
                var medicareNum = submissionFormat.Substring(240, 12);
                result = submissionFormat.Remove(240, 12).Insert(240, medicareNum.IsNotNullOrEmpty() ? medicareNum.Replace("-","").PadRight(12, ' ') : medicareNum);
                Console.WriteLine("Result Length == SubmissionFormat : {0}", result.Length == submissionFormat.Length);
                Console.WriteLine("Result Lenght: {0}", result.Length);
                Console.WriteLine();
            }
            return result;
        }
    }
}
