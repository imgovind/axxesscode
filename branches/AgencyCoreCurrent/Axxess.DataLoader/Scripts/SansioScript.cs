﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class SansioScript
    {
        private static string input = Path.Combine(App.Root, "Files\\Paitents.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Paitents_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static int rowCounter = 1;
        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int numberOfRows = 3;
                                //int rowCounter = 1;
                                bool cont = false;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (dataRow.GetValue(0).ToLower().Contains("patient"))
                                            continue;
                                        if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                        {
                                            cont = true;
                                            continue;
                                        }
                                        if (cont)
                                        {
                                            if (dataRow.GetValue(1).ToLower().Contains("medicaid"))
                                            {
                                                continue;
                                            }
                                            cont = false;
                                            continue;
                                        }
                                        patientData = FillPatient(dataRow, numberOfRows, patientData, agencyId, locationId, textWriter, i);
                                        rowCounter++;
                                    }
                                }
                                Console.WriteLine(rowCounter);
                            }
                        }
                    }
                }
            }
        }

        private static Patient FillPatient(DataRow dataRow, int numberOfRows, Patient patientData, string agencyId, string locationId, TextWriter textWriter, int i)
        {
            if (rowCounter % numberOfRows == 1)
            {
                patientData = new Patient();
                patientData.Id = Guid.NewGuid();
                patientData.AgencyId = agencyId.ToGuid();
                patientData.AgencyLocationId = locationId.ToGuid();
                patientData.Ethnicities = string.Empty;
                patientData.MaritalStatus = string.Empty;
                patientData.IsDeprecated = false;
                patientData.IsHospitalized = false;
                patientData.Status = 1;
                patientData.AddressLine1 = string.Empty;
                patientData.AddressLine2 = string.Empty;
                patientData.AddressCity = string.Empty;
                patientData.AddressStateCode = string.Empty;
                patientData.AddressZipCode = string.Empty;

                var patientNameID = dataRow.GetValue(0).Split('-');
                patientData.PatientIdNumber = patientNameID[1].Trim();
                var patientName = patientNameID[0].Split(',');
                patientData.LastName = patientName[0].Trim();
                patientData.FirstName = patientName[1].Trim();

                patientData.Comments += string.Format("Admit: {0}. ", dataRow.GetValue(3));

                patientData.StartofCareDate = dataRow.GetValue(4).IsValidDate() ? dataRow.GetValue(4).ToDateTime() : DateTime.MinValue;

                patientData.SSN = dataRow.GetValue(8).Replace("-","");

                patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(11));

                patientData.Gender = dataRow.GetValue(16).IsEqual("F") ? "Female" : "Male";

                patientData.Comments += string.Format("Age: {0}. ", dataRow.GetValue(17));

                patientData.Comments += string.Format("DOS: {0}. ", dataRow.GetValue(19));

                patientData.Comments += string.Format("Visits: {0}. ", dataRow.GetValue(21));
            }
            else if (rowCounter % numberOfRows == 2)
            {
                if (dataRow.GetValue(1).ToLower().Contains("medicaid"))
                {
                    patientData.MedicaidNumber = dataRow.GetValue(7);
                }
                else if (dataRow.GetValue(1).ToLower().Contains("palmetto"))
                {
                    patientData.MedicareNumber = dataRow.GetValue(7);
                    patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(1).Split(':')[1].Trim());
                }
            }
            else if (rowCounter % numberOfRows == 0)
            {
                if (dataRow.GetValue(1).ToLower().Contains("payer"))
                {
                    if (dataRow.GetValue(1).ToLower().Contains("palmetto"))
                    {
                        patientData.MedicareNumber = dataRow.GetValue(7);
                        patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(1).Split(':')[1].Trim());
                    }
                    FinishPatient(agencyId, patientData, textWriter, i);
                    i++;
                }
                else
                {
                    FinishPatient(agencyId, patientData, textWriter, i);
                    i++;
                    rowCounter++;
                    patientData = FillPatient(dataRow, numberOfRows, new Patient(), agencyId, locationId, textWriter, i);
                    //rowCounter++;
                }
            }
            return patientData;
        }

        private static void FinishPatient(string agencyId, Patient patientData, TextWriter textWriter, int i)
        {
            if (patientData.Comments.IsNotNullOrEmpty())
            {
                patientData.Comments = patientData.Comments.Replace("'", "");
            }

            patientData.Created = DateTime.Now;
            patientData.Modified = DateTime.Now;

            var medicationProfile = new MedicationProfile
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId.ToGuid(),
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Medication = "<ArrayOfMedication />"
            };

            var allergyProfile = new AllergyProfile
            {
                Id = Guid.NewGuid(),
                AgencyId = agencyId.ToGuid(),
                PatientId = patientData.Id,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Allergies = "<ArrayOfAllergy />"
            };
            var a = 5;
            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
            {
                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
            }
            textWriter.Write(textWriter.NewLine);
        }
    }
}
