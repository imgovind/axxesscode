﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class CradleCsvScript
    {
        private static string input = Path.Combine(App.Root, "Files\\View.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\view.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                var i = 1;
                                csvReader.ReadHeaderRecord();
                                foreach (var dataRow in csvReader.DataRecords)
                                {
                                    var patientData = new Patient();
                                    patientData.Id = Guid.NewGuid();
                                    patientData.AgencyId = agencyId.ToGuid();
                                    patientData.AgencyLocationId = locationId.ToGuid();
                                    patientData.Status = 1;
                                    patientData.Ethnicities = string.Empty;
                                    patientData.MaritalStatus = string.Empty;
                                    patientData.IsDeprecated = false;
                                    patientData.IsHospitalized = false;
                                    patientData.PrimaryRelationship = "";
                                    patientData.SecondaryRelationship = "";
                                    patientData.TertiaryRelationship = "";

                                    patientData.MedicareNumber = dataRow.GetValue(0).Replace("-","");
                                    patientData.StartofCareDate = dataRow.GetValue(1).IsValidDate() ? dataRow.GetValue(1).ToDateTime() : DateTime.MinValue;
                                    if (dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(3).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("EpisodeFrom - EpisodeTo: {0} - {1}. ", dataRow.GetValue(2), dataRow.GetValue(3));
                                    }
                                    patientData.PatientIdNumber = dataRow.GetValue(4);
                                    patientData.MiddleInitial = "";
                                    patientData.FirstName = dataRow.GetValue(6).ToTitleCase();
                                    patientData.LastName = dataRow.GetValue(7).ToTitleCase();
                                    patientData.AddressLine1 = dataRow.GetValue(8).ToTitleCase();
                                    patientData.AddressLine2 = "";
                                    patientData.AddressCity = dataRow.GetValue(9).ToTitleCase();
                                    patientData.AddressStateCode = dataRow.GetValue(10);
                                    patientData.AddressZipCode = dataRow.GetValue(11).Substring(0, 5);
                                    patientData.PhoneHome = !dataRow.GetValue(12).Contains("_") ? dataRow.GetValue(12).ToPhoneDB() : "";
                                    patientData.DOB = dataRow.GetValue(13).IsValidDate() ? dataRow.GetValue(13).ToDateTime() : DateTime.MinValue;
                                    patientData.Gender = dataRow.GetValue(14).IsEqual("01") || dataRow.GetValue(14).IsEqual("1") ? "Female" : "Male";
                                    if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("EpisodeId:{0}.", dataRow.GetValue(15));
                                    }
                                    if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Emergency/Triage Level: {0}. ", dataRow.GetValue(16));
                                    }
                                    if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Episode Frequency: {0}. ", dataRow.GetValue(17));
                                    }
                                    if (dataRow.GetValue(30).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Assessor: {0} {1}. ", dataRow.GetValue(30), dataRow.GetValue(31));
                                    }
                                    if (dataRow.GetValue(32).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Primary Diagnosis/Code: {0} {1}. ", dataRow.GetValue(32), dataRow.GetValue(33));
                                    }
                                    patientData.Status = dataRow.GetValue(34).IsEqual("1") ? 1 : 2;
                                    if (dataRow.GetValue(35).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Discharge Type: {0}. ", dataRow.GetValue(35));
                                    }
                                    if (patientData.Status == 2 && dataRow.GetValue(36).IsNotNullOrEmpty())
                                    {
                                        patientData.DischargeDate = dataRow.GetValue(36).IsDate() ? dataRow.GetValue(36).ToDateTime() : DateTime.MinValue;
                                        patientData.Comments += string.Format("Discharge Date: {0}. ", patientData.DischargeDate.ToShortDateString());
                                    }
                                    if (dataRow.GetValue(38).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("PatientId: {0}. ", dataRow.GetValue(38));
                                    }
                                    if (dataRow.GetValue(39).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Payer Id: {0}. ", dataRow.GetValue(39));
                                    }
                                    if (dataRow.GetValue(40).IsNotNullOrEmpty())
                                    {
                                        if (dataRow.GetValue(40).ToLower().Contains("medicare"))
                                        {
                                            patientData.PrimaryInsurance = "1";
                                        }
                                        else
                                        {
                                            patientData.PrimaryHealthPlanId = patientData.MedicareNumber;
                                            patientData.MedicareNumber = string.Empty;
                                        }
                                        patientData.Comments += string.Format("Payer Name: {0}. ", dataRow.GetValue(40));
                                    }
                                    if (dataRow.GetValue(41).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Payer Type: {0}. ", dataRow.GetValue(41));
                                    }
                                    if (dataRow.GetValue(42).IsNotNullOrEmpty())
                                    {
                                        patientData.DischargeReason = dataRow.GetValue(42);
                                        patientData.Comments += string.Format("RFD: {0}. ", dataRow.GetValue(42));
                                    }

                                    patientData.Comments = patientData.Comments.Replace("'", "");

                                    patientData.Created = DateTime.Now;
                                    patientData.Modified = DateTime.Now;

                                    var medicationProfile = new MedicationProfile()
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId.ToGuid(),
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Medication = "<ArrayOfMedication />"
                                    };

                                    var allergyProfile = new AllergyProfile()
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId.ToGuid(),
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Allergies = "<ArrayOfAllergy />"
                                    };

                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                    {
                                        var admissionPeriod = new PatientAdmissionDate()
                                        {
                                            Id = Guid.NewGuid(),
                                            AgencyId = agencyId.ToGuid(),
                                            Created = DateTime.Now,
                                            DischargedDate = DateTime.MinValue,
                                            IsActive = true,
                                            IsDeprecated = false,
                                            Modified = DateTime.Now,
                                            PatientData = patientData.ToXml().Replace("'", ""),
                                            PatientId = patientData.Id,
                                            Reason = string.Empty,
                                            StartOfCareDate = patientData.StartofCareDate,
                                            Status = patientData.Status
                                        };

                                        if (Database.Add(admissionPeriod))
                                        {
                                            var patient = Database.GetPatient(patientData.Id, agencyId.ToGuid());
                                            if (patient != null)
                                            {
                                                patient.AdmissionId = admissionPeriod.Id;
                                                if (Database.Update(patient))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);

                                                    var exists = true;
                                                    if (dataRow.GetValue(18).IsNotNullOrEmpty() && dataRow.GetValue(23).IsNotNullOrEmpty())
                                                    {
                                                        var physician = Database.GetPhysician(dataRow.GetValue(18), agencyId.ToGuid());
                                                        if (physician == null)
                                                        {
                                                            exists = false;
                                                            physician = new AgencyPhysician
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = agencyId.ToGuid(),
                                                                NPI = dataRow.GetValue(18),
                                                                LoginId = Guid.Empty,
                                                                LastName = dataRow.GetValue(20),
                                                                FirstName = dataRow.GetValue(22),
                                                                PhoneWork = dataRow.GetValue(28).ToPhoneDB(),
                                                                FaxNumber = dataRow.GetValue(29).ToPhoneDB(),

                                                                AddressLine1 = dataRow.GetValue(24),
                                                                AddressCity = dataRow.GetValue(25),
                                                                AddressLine2 = string.Empty,
                                                                AddressStateCode = dataRow.GetValue(26),
                                                                AddressZipCode = dataRow.GetValue(27),
                                                                Credentials = dataRow.GetValue(19),
                                                                Created = DateTime.Now,
                                                                Modified = DateTime.Now,
                                                                IsDeprecated = false,
                                                                PhysicianAccess = false,
                                                                Primary = true
                                                            };

                                                            Database.Add(physician);
                                                        }

                                                        if (physician != null)
                                                        {
                                                            var patientPhysician = new PatientPhysician
                                                            {
                                                                IsPrimary = true,
                                                                PatientId = patientData.Id,
                                                                PhysicianId = physician.Id
                                                            };

                                                            if (Database.Add(patientPhysician))
                                                            {
                                                                Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                            }
                                                        }
                                                    }
                                                    Console.WriteLine();
                                                }
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
