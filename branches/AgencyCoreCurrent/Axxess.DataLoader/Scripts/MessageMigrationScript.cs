﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public static class MessageMigrationScript
    {
        private static TextWriter textWriter = null;
        private const int USER_MIGRATION_THREAD_COUNT = 20;
        private static string output = Path.Combine(App.Root, string.Format("MessageLog_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (textWriter = new StreamWriter(output, true))
            {
                try
                {
                    var startTime = DateTime.Now;
                    var stopWatch = new Stopwatch();
                    stopWatch.Start();

                    var agencies = Database.GetAgencies();
                    if (agencies != null && agencies.Count > 0)
                    {
                        agencies.ForEach(agency =>
                        {
                            Console.WriteLine("Migrating Agency: {0}", agency.Name);
                            var users = Database.GetUsers(agency.Id);
                            if (users != null && users.Count > 0)
                            {
                                var userMigrations = new List<UserMigration>(users.Count);
                                if (users.Count < USER_MIGRATION_THREAD_COUNT)
                                {
                                    ManualResetEvent[] manualEvents = new ManualResetEvent[users.Count];
                                    for (int i = 0; i < users.Count; i++)
                                    {
                                        Console.WriteLine("Migrating User: {0}", users[i].DisplayName);
                                        manualEvents[i] = new ManualResetEvent(false);
                                        UserMigration userMigration = new UserMigration(users[i], manualEvents[i]);
                                        userMigrations.Add(userMigration);
                                        ThreadPool.QueueUserWorkItem(userMigration.ThreadPoolCallback, i);
                                    }

                                    WaitHandle.WaitAll(manualEvents);
                                    userMigrations.ForEach(userMigration =>
                                    {
                                        if (userMigration.ErrorMessage.IsNotNullOrEmpty())
                                        {
                                            textWriter.WriteLine(userMigration.ErrorMessage);
                                            textWriter.WriteLine();
                                        }
                                    });
                                }
                                else
                                {
                                    var counter = 0;
                                    var chunk = users.Count;
                                    do
                                    {
                                        var userChunk = users.Skip(counter * USER_MIGRATION_THREAD_COUNT).Take(USER_MIGRATION_THREAD_COUNT).ToList();
                                        if (userChunk.Count > 0)
                                        {
                                            ManualResetEvent[] manualEvents = new ManualResetEvent[userChunk.Count];
                                            for (int i = 0; i < userChunk.Count; i++)
                                            {
                                                Console.WriteLine("Migrating User: {0}", userChunk[i].DisplayName);
                                                manualEvents[i] = new ManualResetEvent(false);
                                                UserMigration userMigration = new UserMigration(userChunk[i], manualEvents[i]);
                                                userMigrations.Add(userMigration);
                                                ThreadPool.QueueUserWorkItem(userMigration.ThreadPoolCallback, i);
                                            }

                                            WaitHandle.WaitAll(manualEvents);

                                            chunk -= USER_MIGRATION_THREAD_COUNT;
                                            counter++;
                                        }
                                    } while (chunk > 0);

                                    userMigrations.ForEach(userMigration =>
                                    {
                                        if (userMigration.ErrorMessage.IsNotNullOrEmpty())
                                        {
                                            textWriter.WriteLine(userMigration.ErrorMessage);
                                            textWriter.WriteLine();
                                        }
                                    });
                                }
                            }
                            Console.WriteLine("Migration Complete for {0}.", agency.Name);
                            Console.WriteLine();
                        });
                    }

                    stopWatch.Stop();
                    Console.WriteLine("Started @: {0}", startTime.ToString("MM/dd/yyyy @ hh:mm:ss tt"));
                    Console.WriteLine("Finished @: {0}", DateTime.Now.ToString("MM/dd/yyyy @ hh:mm:ss tt"));
                    Console.WriteLine("Script Completed in {0} minutes or {1} hour(s)", stopWatch.Elapsed.TotalMinutes, Convert.ToDouble(stopWatch.Elapsed.TotalMinutes / 60));
                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
                Console.ReadLine();
            }
        }
    }

    public class UserMigration
    {
        private User user;
        private ManualResetEvent manualEvent;
        public string ErrorMessage { get; set; }

        public UserMigration(User user, ManualResetEvent manualEvent)
        {
            this.user = user;
            this.manualEvent = manualEvent;
        }

        public void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = (int)threadContext;
            Console.WriteLine("Starting {0} on thread {1}", this.user.DisplayName, threadIndex);
            Migrate();
            Console.WriteLine("Ended {0} on thread {1}", this.user.DisplayName, threadIndex);

            this.manualEvent.Set();
        }

        public void Migrate()
        {
            try
            {
                var lastMessage = new Message();
                var messages = Database.GetUserMessages(user.Id, user.AgencyId);

                messages.ForEach(message =>
                {
                    if (lastMessage.Subject.IsEqual(message.Subject)
                        && lastMessage.RecipientNames.IsEqual(message.RecipientNames)
                        && lastMessage.Body.IsEqual(message.Body))
                    {
                        var userMessage = new UserMessage()
                        {
                            MessageType = 0,
                            Id = Guid.NewGuid(),
                            FolderId = Guid.Empty,
                            MessageId = lastMessage.Id,
                            AgencyId = message.AgencyId,
                            IsDeprecated = message.IsDeprecated,
                            IsRead = message.MarkAsRead,
                            UserId = message.RecipientId
                        };

                        Database.Add<UserMessage>(userMessage);
                        Console.WriteLine("New Message Association: {0} to {1}", lastMessage.Id, userMessage.Id);
                    }
                    else
                    {
                        var messageDetail = new MessageDetail()
                        {
                            Id = message.Id,
                            FromId = message.FromId,
                            FromName = message.FromName,
                            AgencyId = message.AgencyId,
                            AttachmentId = message.AttachmentId,
                            RecipientNames = message.RecipientNames,
                            CarbonCopyNames = message.CarbonCopyNames,
                            PatientId = message.PatientId,
                            Subject = message.Subject,
                            Body = message.Body,
                            Created = message.Created
                        };

                        var userMessage = new UserMessage()
                        {
                            MessageType = 0,
                            Id = Guid.NewGuid(),
                            FolderId = Guid.Empty,
                            MessageId = message.Id,
                            AgencyId = message.AgencyId,
                            IsDeprecated = message.IsDeprecated,
                            IsRead = message.MarkAsRead,
                            UserId = message.RecipientId
                        };

                        Database.Add<MessageDetail>(messageDetail);
                        Console.WriteLine("New Message: {0}", message.Id);
                        Database.Add<UserMessage>(userMessage);
                        Console.WriteLine("New Message Association: {0} to {1}", message.Id, userMessage.Id);

                        lastMessage = message;
                    }
                });

                if (user.Messages.IsNotNullOrEmpty())
                {
                    var systemMessages = user.Messages.ToObject<List<MessageState>>();

                    if (systemMessages != null && systemMessages.Count > 0)
                    {
                        Console.WriteLine("System Message Count: {0}", systemMessages.Count);
                        systemMessages.ForEach(systemMessage =>
                        {
                            var userMessage = new UserMessage()
                            {
                                UserId = user.Id,
                                Id = Guid.NewGuid(),
                                FolderId = Guid.Empty,
                                AgencyId = user.AgencyId,
                                MessageId = systemMessage.Id,
                                IsRead = systemMessage.IsRead,
                                IsDeprecated = systemMessage.IsDeprecated,
                                MessageType = (int)MessageType.System
                            };

                            Database.Add<UserMessage>(userMessage);
                            Console.WriteLine("System Message Associated: {0} to {1}", systemMessage.Id, userMessage.Id);
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.ToString();
            }
        }
    }
}
