﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public class DeveroColumnPhysician
    {
        public int FirstName { get; set; }
        public int MiddleName { get; set; }
        public int LastName { get; set; }
        public int Title { get; set; }
        public int Address1 { get; set; }
        public int Address2 { get; set; }
        public int City { get; set; }
        public int State { get; set; }
        public int Zip { get; set; }
        public int Phone { get; set; }
        public int Fax { get; set; }
        public int Email { get; set; }
        public int UPIN { get; set; }
        public int NPI { get; set; }
        public int PecosStatus { get; set; }
        public int PecosLastChecked { get; set; }
        public int Protocol { get; set; }
        public int LicenseNumber { get; set; }
        public int LicenseState { get; set; }
        public int LicenseExpiration { get; set; }
        public int PhysicianCode { get; set; }
        public int SynergyAgencyID { get; set; }
        public int Visibility { get; set; }

        public bool DBWritePermission { get; set; }

        public Guid AgencyId { get; set; }

        public string FirstColumn { get; set; }
    }

    public static class DeveroPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\elsmar-doctor.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\elsmar-doctor-event-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\elsmar-doctor-data-{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            DeveroColumnPhysician columnvalue = new DeveroColumnPhysician 
            {
                FirstName = 0,
                MiddleName = 1,
                LastName = 2,
                Title = 3,
                Address1 = 4,
                Address2 = 5,
                City = 6,
                State = 7,
                Zip = 8,
                Phone = 9,
                Fax = 10,
                Email = 11,
                NPI = 13,
                DBWritePermission = true,
                AgencyId = agencyId
            };

            foreach (var item in typeof(PromptColumnPhysician).GetProperties())
            {
                if ((int)item.GetValue(columnvalue, null) == 0)
                {
                    columnvalue.FirstColumn = item.Name;
                    break;
                }
            }

            Process(columnvalue);
        }

        public static void Process(DeveroColumnPhysician column)
        {
            int addedCounter = default(int), cannotAddCounter = default(int), skippedCounter = default(int), noNPICounter = default(int);
            using (TextWriter errorWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                        {
                            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                            {
                                if (excelReader != null && excelReader.IsValid)
                                {
                                    excelReader.IsFirstRowAsColumnNames = true;
                                    DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                    if (dataTable != null && dataTable.Rows.Count > 0)
                                    {
                                        var i = 1;
                                        foreach (DataRow dataRow in dataTable.Rows)
                                        {
                                            if (!dataRow.IsEmpty())
                                            {
                                                StringBuilder logOutput = new StringBuilder();
                                                StringBuilder commentOutput = new StringBuilder();
                                                var npi = dataRow.GetValue(column.NPI);
                                                var physician = Database.GetPhysician(npi, column.AgencyId);
                                                if (physician == null)
                                                { 
                                                    var info = Database.GetNpiData(npi);
                                                    if (info != null)
                                                    {
                                                        physician = new AgencyPhysician
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = column.AgencyId,
                                                            NPI = npi,
                                                            LoginId = Guid.Empty
                                                        };

                                                        #region Name
                                                        if (column.FirstName != default(int) || (column.FirstColumn.Equals("FirstName")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.FirstName);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.FirstName = Staging.Trim().Replace("DO", string.Empty).Replace("DR", string.Empty).Replace("MD", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("FN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.MiddleName != default(int) || (column.FirstColumn.Equals("MiddleName")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.MiddleName);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.MiddleName = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("MN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.LastName != default(int) || (column.FirstColumn.Equals("LastName")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.LastName);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.LastName = Staging.Trim().Replace("DO", string.Empty).Replace("DR", string.Empty).Replace("MD", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("LN:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        #endregion
                                                        #region Address
                                                        if (column.Address1 != default(int) || (column.FirstColumn.Equals("Address1")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Address1);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressLine1 = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("Address1:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.Address2 != default(int) || (column.FirstColumn.Equals("Address2")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Address2);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressLine2 = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("Address2:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.City != default(int) || (column.FirstColumn.Equals("City")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.City);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressCity = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("City:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.State != default(int) || (column.FirstColumn.Equals("State")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.State);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressStateCode = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("State:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.Zip != default(int) || (column.FirstColumn.Equals("Zip")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Zip);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressZipCode = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("Zip:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        #endregion
                                                        #region PhoneFax
                                                        if (column.Phone != default(int) || (column.FirstColumn.Equals("Phone")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Phone);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.PhoneWork = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("Phone:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.Fax != default(int) || (column.FirstColumn.Equals("Fax")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Fax);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.FaxNumber = Staging.Trim();
                                                                ForComment = Staging.Trim();
                                                            }
                                                            logOutput.Append("Fax:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        #endregion
                                                        #region Others
                                                        if (column.PecosStatus != default(int) || (column.FirstColumn.Equals("PecosStatus")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.PecosStatus);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("Pecos Status:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("Pecos Status:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.PecosLastChecked != default(int) || (column.FirstColumn.Equals("PecosLastChecked")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.PecosLastChecked);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("Pecos Last Checked:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("Pecos Last Checked:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.Protocol != default(int) || (column.FirstColumn.Equals("Protocol")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Protocol);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("Protocol:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("Protocol:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.LicenseNumber != default(int) || (column.FirstColumn.Equals("LicenseNumber")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.LicenseNumber);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("License Number:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("License Number:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.LicenseState != default(int) || (column.FirstColumn.Equals("LicenseState")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.LicenseState);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("License State:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("License State:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.LicenseExpiration != default(int) || (column.FirstColumn.Equals("LicenseExpiration")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.LicenseExpiration);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("License Expiration:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("License Expiration:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.PhysicianCode != default(int) || (column.FirstColumn.Equals("PhysicianCode")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.PhysicianCode);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("Physician Code:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("Physician Code:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.SynergyAgencyID != default(int) || (column.FirstColumn.Equals("SynergyAgencyID")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.SynergyAgencyID);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("Synergy Agency ID:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("Synergy Agency ID:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        if (column.Visibility != default(int) || (column.FirstColumn.Equals("Visibility")))
                                                        {
                                                            var ForComment = default(string);
                                                            var Staging = dataRow.GetValue(column.Visibility);
                                                            if (Staging.IsNotNullOrEmpty())
                                                            {
                                                                ForComment = Staging.Trim();
                                                            }
                                                            commentOutput.Append("Visibility:"); commentOutput.Append(": "); commentOutput.Append(ForComment); commentOutput.Append("\t\t");
                                                            logOutput.Append("Visibility:"); logOutput.Append(": "); logOutput.Append(ForComment); logOutput.Append("\t\t");
                                                        }
                                                        physician.Comments = commentOutput.ToString();
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Physician Not Added - No NPI Info");
                                                        noNPICounter++;
                                                    }
                                                    if (column.DBWritePermission == true)
                                                    {
                                                        if (Database.Add(physician))
                                                        {
                                                            Console.WriteLine("(" + i.ToString() + ")" + "Physician Added: " + physician.LastName + ", " + physician.FirstName);
                                                            addedCounter++;
                                                            logWriter.WriteLine(logOutput.ToString());
                                                            
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("(" + i.ToString() + ")" + "Physician NOT Added: ");
                                                            cannotAddCounter++;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("(" + i.ToString() + ")" + "Physician PRESENT: " + physician.LastName + ", " + physician.FirstName);
                                                    skippedCounter++;
                                                }
                                                logOutput.Length = 0;
                                                commentOutput.Length = 0;
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorWriter.WriteLine(ex);
                }
            }
        }
    }
}
