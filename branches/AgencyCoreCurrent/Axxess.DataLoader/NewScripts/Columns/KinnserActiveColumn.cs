﻿namespace Axxess.DataLoader.Columns
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class KinnserActiveColumn
    {
        
        public int FirstName { get; set; }
        public int LastName { get; set; }
        public int Gender { get; set; }
        public int SSN { get; set; }
        public int MRN { get; set; }
        public int HIC { get; set; }
        public int SOC { get; set; }
        public int EpisodeSD { get; set; }
        public int Address1 { get; set; }
        public int City { get; set; }
        public int State { get; set; }
        public int Zip { get; set; }
        public int Phone { get; set; }
        public int PhysicianName { get; set; }
        public int PhysicianAddress1 { get; set; }
        public int PhysicianCity { get; set; }
        public int PhysicianState { get; set; }
        public int PhysicianZip { get; set; }
        public int PhysicianPhone { get; set; }
        public int PhysicianFax { get; set; }
        public int PhysicianNPI { get; set; }
        public int MedicaidNumber { get; set; }
        public int DOB { get; set; }
        public int EmergencyContactName { get; set; }
        public int EmergencyContactRelationship { get; set; }
        public int EmergencyContactPhone { get; set; }
        public int EmergencyContactAddress { get; set; }
        public int Pharmacy { get; set; }
        public int PharmacyPhone { get; set; }
        public int EpisodeED { get; set; }
        public int PrimaryDiagnosis { get; set; }
        public int SecondaryDiagnosis { get; set; }
        public int Disciplines { get; set; }
        public int DisciplineFrequency { get; set; }
        public int Triage { get; set; }
        public int PrimaryClinician { get; set; }
        public int Insurance { get; set; }
        public int Therapist { get; set; }
        public int ExternalReferral { get; set; }
        public int CaseManager { get; set; }
        public int PatientStatusFromXLS { get; set; }
        public Guid agencyId { get; set; }
        public Guid locationId { get; set; }
        public int MDSUBFirst { get; set; }
        public int MDSUBLast { get; set; }
        public int MDSUBMiddle { get; set; }
        public char MDSplitChar { get; set; }
        public string DefaultPayor { get; set; }
        public string FirstColumn { get; set; }
        public int PatientStatus { get; set; }
        public string ServiceLocation { get; set; }
    }
}
