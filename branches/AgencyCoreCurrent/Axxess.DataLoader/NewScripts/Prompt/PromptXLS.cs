﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public class PromptXLSActive
    {
        public int MRN { get; set; }
        public int LastName { get; set; }
        public int FirstName { get; set; }
        public int MiddleInitial { get; set; }
        public int Gender { get; set; }
        public int DOB { get; set; }
        public int MaritalStatus { get; set; }
        public int Street { get; set; }
        public int City { get; set; }
        public int State { get; set; }
        public int Zip { get; set; }
        public int HICNumber { get; set; }
        public int MedicaidNum { get; set; }
        public int SSN { get; set; }
        public int Telephone { get; set; }
        public int SOC { get; set; }
        public int EpisodeSD { get; set; }
        public int EpisodeED { get; set; }
        public int MyProperty { get; set; }
        public int PhysicianName { get; set; }
        public int Pharmacy { get; set; }
        public int PharmacyPhone { get; set; }
        public int DXCode1 { get; set; }
        public int DXCode2 { get; set; }
        public int DXCode3 { get; set; }
        public int DXCode4 { get; set; }
        public int DXCode5 { get; set; }
        public int DXDesc1 { get; set; }
        public int DXDesc2 { get; set; }
        public int DXDesc3 { get; set; }
        public int DXDesc4 { get; set; }
        public int DXDesc5 { get; set; }
        public int ReferName { get; set; }
        public int PayorName1 { get; set; }

        public string CommentsDelimiter { get; set; }
        public bool canWriteToDB { get; set; }
        public int patientStatus { get; set; }
        public string firstColumn { get; set; }
        public string agencyPayor { get; set; }
        public Guid agencyId { get; set; }
        public Guid locationId { get; set; }
    }

    public static class PromptXLS
    {
        private static string input = Path.Combine(App.Root, "Files\\vida-active.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\vida-active-EventLog{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {

            PromptXLSActive columnValue = new PromptXLSActive();
            columnValue.MRN = 0;
            columnValue.LastName = 1;
            columnValue.FirstName = 2;
            columnValue.MiddleInitial = 3;
            columnValue.Gender = 4;
            columnValue.DOB = 5;
            columnValue.MaritalStatus = 6;
            columnValue.Street = 7;
            columnValue.City = 8;
            columnValue.State = 9;
            columnValue.Zip = 10;
            columnValue.HICNumber = 11;
            columnValue.MedicaidNum = 12;
            columnValue.SSN = 13;
            columnValue.Telephone = 14;
            columnValue.SOC = 16;
            columnValue.EpisodeSD = 17;
            columnValue.EpisodeED = 18;
            columnValue.PhysicianName = 30;
            columnValue.Pharmacy = 31;
            columnValue.PharmacyPhone = 32;
            columnValue.DXCode1 = 33;
            columnValue.DXCode2 = 34;
            columnValue.DXCode3 = 35;
            columnValue.DXCode4 = 36;
            columnValue.DXCode5 = 37;
            columnValue.DXDesc1 = 43;
            columnValue.DXDesc2 = 44;
            columnValue.DXDesc3 = 45;
            columnValue.DXDesc4 = 46;
            columnValue.DXDesc5 = 47;
            columnValue.ReferName = 70;
            columnValue.PayorName1 = 73;
            columnValue.agencyId = agencyId;
            columnValue.locationId = locationId;
            columnValue.agencyPayor = "1";
            columnValue.canWriteToDB = true;
            columnValue.patientStatus = 1;
            columnValue.CommentsDelimiter = "; \n\r";
            foreach (var item in typeof(PromptXLSActive).GetProperties())
            {
                if ((int)item.GetValue(columnValue, null) == 0)
                {
                    columnValue.firstColumn = item.Name;
                    break;
                }
            }

            Process(columnValue);
        }

        public static void Process(PromptXLSActive columnValue)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    var totalpatients = default(int);
                    var totalCounter = default(int);
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        totalpatients = dataTable.Rows.Count;
                                        totalCounter++;
                                        if (!dataRow.IsEmpty())
                                        {
                                            StringBuilder sb = new StringBuilder();

                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = columnValue.agencyId;
                                            patientData.AgencyLocationId = columnValue.locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = columnValue.patientStatus;
                                            patientData.ServiceLocation = "Q5001";

                                            #region PROCESS
                                            #region MRN
                                            string typenamecolumnspacerwower = default(string);
                                            if (columnValue.MRN != default(int) || columnValue.firstColumn.Equals("MRN"))
                                            {
                                                patientData.PatientIdNumber = dataRow.GetValue(columnValue.MRN).IsNotNullOrEmpty() ? dataRow.GetValue(columnValue.MRN) : "";
                                            }
                                            #endregion
                                            #region Name
                                            if (columnValue.FirstName != default(int) || columnValue.firstColumn.Equals("FirstName"))
                                            {
                                                patientData.FirstName = dataRow.GetValue(columnValue.FirstName).IsNotNullOrEmpty() ? dataRow.GetValue(columnValue.FirstName) : "";
                                            }
                                            if (columnValue.LastName != default(int) || columnValue.firstColumn.Equals("LastName"))
                                            {
                                                patientData.LastName = dataRow.GetValue(columnValue.LastName).IsNotNullOrEmpty() ? dataRow.GetValue(columnValue.LastName) : "";
                                            }
                                            if (columnValue.MiddleInitial != default(int) || columnValue.firstColumn.Equals("MiddleInitial"))
                                            {
                                                patientData.MiddleInitial = dataRow.GetValue(columnValue.MiddleInitial).IsNotNullOrEmpty() ? dataRow.GetValue(columnValue.MiddleInitial) : "";
                                            }
                                            #endregion
                                            #region Sex
                                            if (columnValue.Gender != default(int) || (columnValue.firstColumn.Equals("Gender")))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.Gender);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    if (Staging.ToUpper().Contains("M"))
                                                    {
                                                        patientData.Gender = "Male";
                                                    }
                                                    else if (Staging.ToUpper().Contains("F"))
                                                    {
                                                        patientData.Gender = "Female";
                                                    }
                                                }
                                            }
                                            #endregion
                                            #region DOB
                                            if (columnValue.DOB != default(int) || columnValue.firstColumn.Equals("DOB"))
                                            {
                                                string DOBStaging = dataRow.GetValue(columnValue.DOB);
                                                if (DOBStaging.IsNotNullOrEmpty())
                                                {
                                                    if (DOBStaging.IsDouble())
                                                    {
                                                        patientData.DOB = DateTime.FromOADate(double.Parse(DOBStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.DOB = DateTime.Parse(DOBStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }
                                            }
                                            else
                                            {
                                                patientData.DOB = DateTime.MinValue;
                                            }
                                            #endregion
                                            #region MaritalStatus
                                            if (columnValue.MaritalStatus != default(int) || (columnValue.firstColumn.Equals("MaritalStatus")))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.MaritalStatus);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    Staging = Staging.ToUpperCase();
                                                    if (Staging.StartsWith("M"))
                                                    {
                                                        patientData.MaritalStatus = "Married";
                                                    }
                                                    else if (Staging.StartsWith("S"))
                                                    {
                                                        patientData.MaritalStatus = "Single";
                                                    }
                                                    else if (Staging.StartsWith("W"))
                                                    {
                                                        patientData.MaritalStatus = "Widowed";
                                                    }
                                                    else if (Staging.StartsWith("D"))
                                                    {
                                                        patientData.MaritalStatus = "Divorced";
                                                    }
                                                    else
                                                    {
                                                        patientData.MaritalStatus = "Unknown";
                                                    }
                                                }
                                            }
                                            #endregion
                                            #region Address
                                            if (columnValue.Street != default(int) || (columnValue.firstColumn.Equals("Street")))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.Street);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressLine1 = Staging.Trim();
                                                }
                                            }
                                            if (columnValue.City != default(int) || (columnValue.firstColumn.Equals("City")))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.City);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressCity = Staging.Trim();
                                                }
                                            }
                                            if (columnValue.State != default(int) || (columnValue.firstColumn.Equals("State")))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.State);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressStateCode = Staging.Trim();
                                                }
                                            }
                                            if (columnValue.Zip != default(int) || (columnValue.firstColumn.Equals("Zip")))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.Zip);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressZipCode = Staging.Trim();
                                                }
                                            }
                                            #endregion
                                            #region HIC+Medicaid
                                            if (columnValue.PayorName1 != default(int) || columnValue.firstColumn.Equals("PayorName1"))
                                            {
                                                if (dataRow.GetValue(columnValue.PayorName1).IsNotNullOrEmpty())
                                                {
                                                    if (dataRow.GetValue(columnValue.PayorName1).ToUpper().Contains("PALMETTO"))
                                                    {
                                                        patientData.PrimaryInsurance = columnValue.agencyPayor;
                                                        if (columnValue.HICNumber != default(int) || columnValue.firstColumn.Equals("HICNumber"))
                                                        {
                                                            if (dataRow.GetValue(columnValue.HICNumber).IsNotNullOrEmpty())
                                                            {
                                                                patientData.MedicareNumber = dataRow.GetValue(columnValue.HICNumber);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (columnValue.MedicaidNum != default(int) || columnValue.firstColumn.Equals("MedicaidNum"))
                                            {
                                                if (dataRow.GetValue(columnValue.MedicaidNum).IsNotNullOrEmpty())
                                                {
                                                    patientData.MedicaidNumber = dataRow.GetValue(columnValue.MedicaidNum);
                                                    patientData.Comments += "Medicaid Number: " + dataRow.GetValue(columnValue.MedicaidNum) + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.HICNumber != default(int) || columnValue.firstColumn.Equals("HICNumber"))
                                            {
                                                if (dataRow.GetValue(columnValue.HICNumber).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "HICNumber: " + dataRow.GetValue(columnValue.HICNumber) + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region SSN
                                            if (columnValue.SSN != default(int) || columnValue.firstColumn.Equals("SSN"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.SSN).Replace("-", "").Trim();
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.SSN = Staging;
                                                }
                                            }
                                            #endregion
                                            #region SOC
                                            if (columnValue.SOC != default(int) || columnValue.firstColumn.Equals("SOC"))
                                            {
                                                string SOCStaging = dataRow.GetValue(columnValue.SOC);
                                                if (SOCStaging.IsNotNullOrEmpty())
                                                {
                                                    if (SOCStaging.IsDouble())
                                                    {
                                                        patientData.StartofCareDate = DateTime.FromOADate(double.Parse(SOCStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.StartofCareDate = DateTime.Parse(SOCStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.StartofCareDate = DateTime.MinValue;
                                                }
                                            }
                                            else
                                            {
                                                patientData.StartofCareDate = DateTime.MinValue;
                                            }
                                            #endregion
                                            #region EpisodeSD + EpisodeED
                                            if (columnValue.EpisodeSD != default(int) || columnValue.firstColumn.Equals("EpisodeSD"))
                                            {
                                                string SDStaging = dataRow.GetValue(columnValue.EpisodeSD);
                                                DateTime Staging = DateTime.MinValue;
                                                if (SDStaging.IsNotNullOrEmpty())
                                                {
                                                    if (SDStaging.IsDouble())
                                                    {
                                                        Staging = DateTime.FromOADate(double.Parse(SDStaging));
                                                    }
                                                    else
                                                    {
                                                        Staging = DateTime.Parse(SDStaging);
                                                    }
                                                }
                                                patientData.Comments += "Episode Start Date:" + Staging.ToShortDateString() + columnValue.CommentsDelimiter;
                                            }
                                            if (columnValue.EpisodeED != default(int) || columnValue.firstColumn.Equals("EpisodeED"))
                                            {
                                                string EDStaging = dataRow.GetValue(columnValue.EpisodeED);
                                                DateTime Staging = DateTime.MinValue;
                                                if (EDStaging.IsNotNullOrEmpty())
                                                {
                                                    if (EDStaging.IsDouble())
                                                    {
                                                        Staging = DateTime.FromOADate(double.Parse(EDStaging));
                                                    }
                                                    else
                                                    {
                                                        Staging = DateTime.Parse(EDStaging);
                                                    }
                                                }
                                                patientData.Comments += "Episode End Date:" + Staging.ToShortDateString() + columnValue.CommentsDelimiter;
                                            }
                                            #endregion
                                            #region PhysicianName
                                            if (columnValue.PhysicianName != default(int) || columnValue.firstColumn.Equals("PhysicianName"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.PhysicianName);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Physician Name: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region Pharmacy
                                            if (columnValue.Pharmacy != default(int) || columnValue.firstColumn.Equals("Pharmacy"))
                                            {
                                                var pharmacyStaging = dataRow.GetValue(columnValue.Pharmacy);
                                                if (pharmacyStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Diagnosis: " + pharmacyStaging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.PharmacyPhone != default(int) || columnValue.firstColumn.Equals("PharmacyPhone"))
                                            {
                                                var pharmacyPhoneStaging = dataRow.GetValue(columnValue.PharmacyPhone);
                                                if (pharmacyPhoneStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Diagnosis: " + pharmacyPhoneStaging.ToPhoneDB() + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region DiagnosisCodes
                                            if (columnValue.DXCode1 != default(int) || columnValue.firstColumn.Equals("DXCode1"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXCode1);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Code1: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXCode2 != default(int) || columnValue.firstColumn.Equals("DXCode2"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXCode2);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Code2: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXCode3 != default(int) || columnValue.firstColumn.Equals("DXCode3"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXCode3);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Code3: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXCode4 != default(int) || columnValue.firstColumn.Equals("DXCode4"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXCode4);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Code4: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXCode5 != default(int) || columnValue.firstColumn.Equals("DXCode5"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXCode5);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Code5: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region DiagnosisDescription
                                            if (columnValue.DXDesc1 != default(int) || columnValue.firstColumn.Equals("DXDesc1"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXDesc1);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Description1: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXDesc2 != default(int) || columnValue.firstColumn.Equals("DXDesc2"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXDesc2);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Description2: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXDesc3 != default(int) || columnValue.firstColumn.Equals("DXDesc3"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXDesc3);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Description3: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXDesc4 != default(int) || columnValue.firstColumn.Equals("DXDesc4"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXDesc4);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Description4: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            if (columnValue.DXDesc5 != default(int) || columnValue.firstColumn.Equals("DXDesc5"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.DXDesc5);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Diagnosis Description5: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region ReferralName
                                            if (columnValue.ReferName != default(int) || columnValue.firstColumn.Equals("ReferName"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.ReferName);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Referral Name: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #region Payor
                                            if (columnValue.PayorName1 != default(int) || columnValue.firstColumn.Equals("PayorName1"))
                                            {
                                                var Staging = dataRow.GetValue(columnValue.PayorName1);
                                                if (Staging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Payor Name: " + Staging + columnValue.CommentsDelimiter;
                                                }
                                            }
                                            #endregion
                                            #endregion

                                            //Check if writing to Dev Database.
                                            //Check the Program.cs for AgencyId, AgencyLocationId
                                            if (columnValue.canWriteToDB)
                                            {
                                                bool checkExists = default(bool);
                                                Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, columnValue.agencyId);

                                                if (patientExisting != null)
                                                {
                                                    checkExists = true;
                                                }

                                                if (checkExists != true)
                                                {
                                                    #region DBWrite
                                                    var medicationProfile = new MedicationProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = columnValue.agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Medication = "<ArrayOfMedication />"
                                                    };

                                                    var allergyProfile = new AllergyProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = columnValue.agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Allergies = "<ArrayOfAllergy />"
                                                    };

                                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                    {
                                                        var admissionPeriod = new PatientAdmissionDate
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = columnValue.agencyId,
                                                            Created = DateTime.Now,
                                                            DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                            IsActive = true,
                                                            IsDeprecated = false,
                                                            Modified = DateTime.Now,
                                                            PatientData = patientData.ToXml().Replace("'", ""),
                                                            PatientId = patientData.Id,
                                                            Reason = string.Empty,
                                                            StartOfCareDate = patientData.StartofCareDate,
                                                            Status = patientData.Status
                                                        };
                                                        if (Database.Add(admissionPeriod))
                                                        {
                                                            var patient = Database.GetPatient(patientData.Id, columnValue.agencyId);
                                                            if (patient != null)
                                                            {
                                                                patient.AdmissionId = admissionPeriod.Id;
                                                                if (Database.Update(patient))
                                                                {
                                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine("\nTotal Patients:" + totalpatients);
                    Console.WriteLine("\nTotal Counted:" + totalCounter);
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}