﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using StructureMap;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.DataLoader.Domain;

    using SubSonic.Repository;
    using Axxess.LookUp.Domain;

    class Program
    {
        static void Main(string[] args)
        {
            #region update scripts
            //Console.WriteLine("Schedule Event Update Started");
            //DateTime startTimer = DateTime.Now;
            ////ScheduleEventExportToTable.Run(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            ////ScheduleEventUpdate.Run();
            //ScheduleEventUpdate.Run(new Guid("b33582bb-e3b1-4bee-adb5-d1edd8657042"));
            ////ScheduleEventUpdate.Run(new Guid("b8391e3d-f5bb-4139-856e-38c376269340"));
            //TimeSpan endTimer = DateTime.Now - startTimer;
            //Console.WriteLine("Total Time =>" + "Mins:" + endTimer.TotalMinutes);
            //Console.WriteLine("Total Time =>" + "Hours:" + endTimer.TotalHours);
            #endregion

            #region frequent scripts
            //Console.WriteLine("Dataload for Ardent");
            //AxxessActive.Run(new Guid("3e5ab5bc-a898-4944-8139-52c243401788"), new Guid("95123f1f-385d-4902-94b2-dc9195fa6d0f"));

            //Console.WriteLine("Dataload for Glow");
            //CradleActive.Run(new Guid("9268fa08-9bad-454f-8da2-2bac1301ea68"), new Guid("93e6cac2-a752-4dd7-a4d5-b8fbf8799f6a"));

            //Console.WriteLine("Physician Dataload for Incare");
            //AxxessPhysicianNew2.Run(new Guid("9859e6f8-821a-4161-ae37-88bdc8d30a76"));

            AxxessActive.Run(new Guid("9859e6f8-821a-4161-ae37-88bdc8d30a76"), new Guid("4434b742-060d-4e64-950c-8ec52a77bd40"), "incare-active.xls");

            //Console.WriteLine("DataLoad for Columbus");
            //KinnserActive2.Run(new Guid("9901fd07-bb09-4ca8-8b73-b1b3c3cc1621"), new Guid("384f3569-30dc-4dd9-9783-97ab54686222"));
            //AxxessPhysicianNew.Run(new Guid("9901fd07-bb09-4ca8-8b73-b1b3c3cc1621"));
            //Console.WriteLine("DataLoad for CCS");
            //KinnserActive.Run(new Guid("685c47fe-cb72-40aa-8eec-60d671a5aa1f"), new Guid("51a35b43-078d-4a65-8b1d-a33f78bd583d"));
            //Console.WriteLine("DataLoad for Get Well DBA");
            //KinnserActive3.Run(new Guid("2418ba97-aca1-49e3-9da9-d0f907d5c643"), new Guid("69f18d4f-ea2a-4ff2-bbdb-88ebc15ab181"));
            //VisiTrakACTIVEPatientScript.Run(new Guid("083ccb62-78f3-4fdf-a5a1-a10fe72dee80"), new Guid("dcc06eb3-7934-4f30-875d-3c2cd766770a"));
            //KinnserActivePhysicianUpFront.Run(new Guid("231bfb3d-4928-43dc-9788-3bfabc0b7531"), new Guid("a1914e6c-75dd-4af1-9799-9eaf8e11a267"));
            //KinnserPhysicianNew.Run(new Guid("231bfb3d-4928-43dc-9788-3bfabc0b7531"));
            //VisitracActive.Run(new Guid("dc145f92-fb88-41f3-b29e-7bd9a66da429"), new Guid("22f85ee2-acf5-4ffb-82a5-95ff9d90e658"));
            //VisitracPhysician.Run(new Guid("dc145f92-fb88-41f3-b29e-7bd9a66da429"));
            //Console.WriteLine("DataLoad for Alphonsa Home Health");
            //VisitracActive2.Run(new Guid("8d1fea54-f595-42d9-a1c2-c4aa7b0bca75"), new Guid("7ad1897f-57f3-4a1b-a89e-094ad484640c"));
            //VisitracPhysician2.Run(new Guid("8d1fea54-f595-42d9-a1c2-c4aa7b0bca75"));
            //Console.WriteLine("DataLoad for Manos de oro");
            //KinnserActive.Run(new Guid("0d0d83a9-6a61-4b71-9efa-88fe2cd71b2e"), new Guid("c69e5ffe-ac73-4ba2-9f87-1544f6167f7e"));
            //KinnserPhysicianNew.Run(new Guid("0d0d83a9-6a61-4b71-9efa-88fe2cd71b2e"));
            //Console.WriteLine("DataLoad for Hannah Home Health");
            //SynergyMutipleRowActive.Run(new Guid("7705531b-40cb-4bf2-a42f-1375f37d9fe1"), new Guid("fc90b43a-5108-47ab-b412-40066d57c197"));
            //AxxessPhysicianNew.Run(new Guid("7705531b-40cb-4bf2-a42f-1375f37d9fe1"));
            //AxxessPhysicianNew2.Run(new Guid("0411015b-75f2-43e8-ab2d-0f6e95969212"));
            //VisiTrakPhysicianScript.Run(new Guid("717313af-6bfa-4bae-99b9-13729e09d06d"));
            //PromptXLS.Run(new Guid("3c7d4d71-52fb-406b-b0bb-ed33540e688e"), new Guid("3aabf589-789c-46c6-a318-30a3c78254db"));
            //PromptPhysician.Run(new Guid("3c7d4d71-52fb-406b-b0bb-ed33540e688e"));
            //SynergyPhysician.Run(new Guid("df559fd3-26c3-422c-bf6a-7104c2b3fcbb"));
            //SynergyTwoCRowActive.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"), new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //SynergyTwoCRowPending.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"), new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //SynergyTwoCRowDischarged.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"), new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //PromptActive.Run(new Guid("31926308-773a-406e-aaa8-3c493faf595d"), new Guid("89f85303-d5fa-4fec-a572-4c970d05408a"));
            //PromptPhysician.Run(new Guid("31926308-773a-406e-aaa8-3c493faf595d"));
            //DeveroActive.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"), new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //DeveroDischarged.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"), new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //DeveroPhysician.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"));
            //SynergyTwoScript.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"),new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //SynergyTwoRowD.Run("c9101e56-05cb-4ee7-9662-69c0f8ff6f37", "599168b6-37ba-4b15-8c74-a0148aa5d557");
            //Axxess20Column2.Run(new Guid("e6bf3dc6-0c31-4683-8c61-a0ec95d520d2"), new Guid("ca599c43-463f-4e03-91a6-6926f7944cb5"));
            //Axxess20Column.Run(new Guid("d2f89333-d809-4df0-bcfe-eae0026988a3"), new Guid("a51badb6-241e-48d4-b5f9-80a087c205f0"));
            //SynergyMutipleRow.Run(new Guid("a3c6764e-6923-43a0-b2cf-b3c33b5a42e0"), new Guid("dec84a6e-0f06-4d97-bd4e-f0e9a79572e2"));
            //SynergyTwoRow.Run("1dd83491-dc13-4374-be70-a12c1389cb5e", "d3fe7e8c-8360-4f28-8646-e0a2b4344070");
            //AxxessPhysicianNew3.Run(new Guid("05abfa82-9703-4e71-9262-c8c06d896e84"));
            //Axxess40Column2.Run(new Guid("02ee76a4-ebe8-47e9-9c60-252e3d7a32a5"), new Guid("3f43c278-30c6-41cc-9f7b-51a524f8b468"));
            //Axxess20Column3.Run(new Guid("96c46586-eccb-4391-a348-a593498caa8b"), new Guid("23e61db8-ff74-4e02-b3a9-b6169eef5387"));
            //Axxess40ColumnD.Run(new Guid("1dcf6166-8e67-4042-8405-14b47e2c609d"), new Guid("66f94d76-4029-47d0-bed9-b31aa984d57e"));
            //VisiTrakDISCHARGEDPatientScript.Run(new Guid("090570b8-5c22-4492-b970-9f7846f50ba2"), new Guid("1477a7ea-33ee-4c57-adaf-d01969c40afd"));
            //Axxess40Column2.Run(new Guid("5a92d937-cd5e-4e4c-8bee-b438803bf4fc"), new Guid("6ad99608-c347-4a9f-9ca7-3a872e34379c"));
            //AxxessPhysicianTwoRow.Run(new Guid("d26c70b7-714c-4e20-b65f-e95c30e9db75"));
            //AluraPhysician.Run(new Guid("d26c70b7-714c-4e20-b65f-e95c30e9db75"));
            #endregion

            #region All The Scripts
            //OasisSubmissionBranchIdScript.Run(new Guid("aa6a38f2-364b-4d8e-b418-29bff888be51"));
            //FixOasisSubmissionBranchIdScript.Run(new Guid("f5e28cb7-4b72-4332-a98a-1e748dc95c21"));
            //FixOasisSubmissionBranchIdScript.Run(new Guid("f5e28cb7-4b72-4332-a98a-1e748dc95c21"));
            //AgencyAddAdjustmentsScript.Run();
            //UpdatePaymentsScript.Run();
            //AllegenyIntake.Run(new Guid("f3c587a1-6303-4ebf-ac16-403ee70b9aa7"), new Guid("efe80c4b-cd75-45d7-98ef-8be524263bd7"));
            //UsingSimpleRepo();
            //UsingFluentAdoNet();
            //Alura.Run("fc7c6c62-eee6-4186-84ba-25105ebca2f2", "20882903-4608-4c3f-993c-82d2d5132fbf");
            //IDatabaseAdministration databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=agencymanagement;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);
            //databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=oasisc;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);
            //databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=axxessmembership;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);
            //AloraHealthIntake.Run(new Guid("a84be1d0-d5f7-42f5-95ee-d10d255cb3f1"), new Guid("c87a57b0-24bc-4d26-a126-f8dc5e407075"));
            //AssetScript.Run(new Guid("153e863d-669a-4edd-851a-4088890fd9a1"));
            //Wizard.Run(new Guid("d544542f-75da-4ade-9c84-5e4aa4d2fb59"), new Guid("411ac04a-3868-40b4-99a2-bb4d18021f03"));
            //WizardPhysician.Run(new Guid("a4f28d28-dc0f-44bb-9174-50daf9193eb5"));
            //AssetScript.Run(new Guid("153e863d-669a-4edd-851a-4088890fd9a1"));
            //VestaScript.Run("565e9603-d887-407b-8404-ffe9a8b7b92f", "145bd87c-9e3e-47d9-8bb0-1130620d9422");
            //ColoradoScript.Run();
            //ArizonaScript.Run();
            //MinnesotaScript.Run();
            //CaliforniaHHAScript.Run();
            //SponsorScript.Run();
            //var stopWatch = new Stopwatch();
            //stopWatch.Start();
            //CradleMedPointScript.Run(new Guid("5d50ae56-5e62-46cf-9b7f-40f5ca8202b3"), new Guid("4bab3fe9-51dc-4e90-9262-32575e505349"));
            //KinnserScript.Run(new Guid("e9a50d43-6465-474e-a3cf-bb8e926789b4"), new Guid("31904bd4-cbfe-4612-b36f-60a5bca23ed1"));
            //KinnserPhysician.Run(new Guid("e9a50d43-6465-474e-a3cf-bb8e926789b4"));
            //KinnserCsvScript.Run(new Guid("3a1b0087-1de8-4235-aa0e-97f7b67f022b"), new Guid("b445bad7-a293-4bc8-9d3e-88dfa62baee3"));
            //AxxessScript.Run("24711294-bff1-4f6b-ae58-5fa2bbeae342", "c060d570-deb6-45f1-a7cc-7edc127b528d");
            //MedicationProfileScript.Run(new Guid("6d80ae1f-410d-408f-b342-051e956b037f"));
            //GenericExcelScript.Run(new Guid("8bf20512-359a-4fe5-9438-966dfaa5a34c"), new Guid("8885e4f3-36ce-4a26-a08a-0b691e89ccbe"));
            //SynergyScript.Run(new Guid("726f5594-8754-42d9-a421-d52d35d0fa82"), new Guid("e4834fa2-7c96-4879-80d1-e2b632f320ce"));
            //VisiTrakScript.Run(new Guid("1c099985-2664-4fa3-bdaf-349ecda4b263"), new Guid("18dae740-31fb-4547-b2fa-6e50b556f13d"));
            //GenericExcelScript3.Run(new Guid("271a571f-3d00-4044-932a-d164b9e97a44"), new Guid("1950f21c-15fa-4174-935f-85f33314f8bc"));
            //VisiTrakScript3.Run(new Guid("908c335b-e0cb-406a-916e-ecdd92d8379a"), new Guid("2de44ff4-b71a-4bae-9611-82e1321ba192"));
            //VisitrackExcelPhysician2.Run(new Guid("271a571f-3d00-4044-932a-d164b9e97a44"));
            //CopyLogs.Run();
            //SynergyThreeScript.Run(new Guid("cd7ca5bb-44bc-4390-bb4e-a2dfa80ca1d2"), new Guid("3e76f5b7-6af9-4f95-8a38-b09c75bd2d9a"));
            //SynergyTwoScript.Run(new Guid("824e13c1-7cc6-43f1-aa5c-81190c45cb54"), new Guid("7e59d3dc-1e11-4957-ab81-f4f83a49df9e"));
            //SynergyPhysicianScript.Run(new Guid("824e13c1-7cc6-43f1-aa5c-81190c45cb54"));
            //SynergyTwoPhysicianScript.Run(new Guid("ce218bb3-7b0e-41e3-9842-59e9947d1000"));
            //HealthMedXPhysicianScript.Run(new Guid("abc2fbb2-807e-4dd0-987f-608e5d75558a"));
            //HealthCareFirstScript.Run(new Guid("93a811f1-ffda-43f9-a844-b2a0f9db81f4"), new Guid("9a41ef5a-f55c-4f98-b1de-84a094b91354"));
            //Icd9Script.Run(true);
            //Icd9Script.Run(false);
            //GenericExcelTwoScript.Run(new Guid("4a318889-50b7-4ba2-90d2-bb259a663411"), new Guid("e4556fd3-e9e9-42e3-a801-06ddd9da77ee"));
            //WageIndexScript.Run();
            //CradleCsvScript.Run("15260aab-b081-4318-8e56-6a6b879f32ce", "987dcc9a-053c-4bf3-b372-28b4859c456a");
            //OasisSubmissionBranchIdScript.Run(new Guid("ea9c9ab1-d667-4c1d-8d74-1436344666a8"));
            //HHCenterScript.Run(new Guid("9d5afae5-6127-4c58-b457-c5185273ad4a"), new Guid("044ec64d-118e-43df-85f1-9c772a004c86"));
            //MjsOneScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"), new Guid("e8460caf-8a63-42b2-89e7-058bf9b8be15"));
            //MsjOnePhysicianScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"));
            //IgeaHomeHealthIntake.Run("5ccbf159-dc60-4b5a-a6aa-ade0ca60a1be", "80404746-3140-46c9-9ea8-4c68ee2bdb47");
            //PermissionScript.Run();
            //AgencySupplies.Run();
            //PhysicianOrders.Run();
            //AgencyMergeScript.Run("4da8bb1c-0b75-415c-862d-c36ff673619d", new List<string> { "949dfac1-8bdb-4097-868b-a7729cc991b4", "c65edc26-12be-43d1-b71c-5074d16213e8" });
            //AxxessScript4.Run(new Guid("01d5f8ea-3713-4d5a-8c9e-3c539fb574fa"), new Guid("9b0f3104-5313-4cc2-9dee-277904313071"));
            //int i = PatinetAddressScript.Run(new Guid("0bd5ffc9-61c1-495a-9df5-7b93885ab30d"));
            //AxxessScript3.Run(new Guid("10655a61-5d3d-440f-bf13-522111069f8e"), new Guid("2b2d5dd3-1cd1-42b1-8da6-de0cc13011ce"));
            //AxxessScript2.Run(new Guid("e9a50d43-6465-474e-a3cf-bb8e926789b4"), new Guid("31904bd4-cbfe-4612-b36f-60a5bca23ed1"), new Guid("ee56f594-fb77-4d1c-9c2c-b04317c731e1"));
            //SchduleLoadScript.Run(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //SchduleLoadScript.Run(new Guid("d0307ff6-9d69-435a-a411-f737b1980bfb"));
            //SchduleLoadScript.Run(new Guid("86493a0e-0fc0-4260-998b-442dd17f1a6e"));
            //PhysicianCsvScript.Run(new Guid("7596d33b-a56a-49e0-bc27-fe11d76fffec"));
            //SansioScript.Run("bada3b4f-2e32-4c1b-9eea-55a080ffa627", "28b948b1-49e1-4557-b1ed-91abe32f0de6");
            //SansioPhysicansScript.Run("bada3b4f-2e32-4c1b-9eea-55a080ffa627");
            //Genie.Run("6aa2ce5c-dede-4fc8-93cd-7cf64bb520f5", "00956c1b-3e09-40c7-a790-a969e35884e3");
            //SynergyTwoRow.Run("b93c9148-a694-4cd5-9f9c-ef6ea62e1d98", "4872c996-709f-4af4-b9bd-45eda86b4800");
            //GeniePhysician.Run(new Guid("69d47957-7a55-41a5-84c0-f0002f9deca2"));
            //DataSoftLogic.Run("8ef53ea3-ca70-4a4c-b56d-5f1c2f366e26", "cdd48dbd-bfb5-4d17-bfc8-37d7b433fb5b");
            //DataSoftLogicTwoRow.Run("8ef53ea3-ca70-4a4c-b56d-5f1c2f366e26", "cdd48dbd-bfb5-4d17-bfc8-37d7b433fb5b");
            //SynergyFourRows.Run(new Guid("b5c94ce3-b731-43d9-b09f-399b136771a4"), new Guid("a0604cb1-87ba-4243-a29c-3f279ccac2f0"));
            //LoadAgencyLocationChargeRates.Run();
            //SynergyThree.Run("b5c94ce3-b731-43d9-b09f-399b136771a4", "a0604cb1-87ba-4243-a29c-3f279ccac2f0");
            //LoadAgencyLocationChargeRates.RunToUpdateTheDisciplineTasks();
            //AgencyAddUploadTypeScript.Run();
            //OasisSubmissionIdScript.Run(new Guid("824e13c1-7cc6-43f1-aa5c-81190c45cb54"), new List<string> { "1011602" }, "IL1011602");
            //MigrateToMessageFolders2.Run();
            //VisitrackExcelPhysician.Run(new Guid("45292c51-da6c-44f8-963d-87744f364345"));
            //LoadSystemMessage.Run();
            //GenericExcelPatientScript.Run(new Guid("cd7ca5bb-44bc-4390-bb4e-a2dfa80ca1d2"), new Guid("3e76f5b7-6af9-4f95-8a38-b09c75bd2d9a"));
            //GenericExcelPatientScriptWiseCounty.Run(new Guid("45292c51-da6c-44f8-963d-87744f364345"), new Guid("9aa60845-0217-480e-b07b-fd205580ba56"));
            #endregion

            Console.WriteLine("[Done. Press any key to exit...]");
            Console.ReadLine();
        }
    }
}
