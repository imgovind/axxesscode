﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Enums;

    using SubSonic.Repository;

    public static class Database
    {
        #region Private Members

        private static readonly SimpleRepository database = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);

        private const string SELECT_AGENCY_CONTACTS = @"SELECT * FROM `agencycontacts` WHERE IsDeprecated = 0 and AgencyId = @agencyid;";
        private const string SELECT_AGENCY_PHYSICIANS = @"SELECT * FROM `agencyphysicians` WHERE IsDeprecated = 0 and AgencyId = @agencyid;";
        private const string SELECT_AGENCY_REFERRALS = @"SELECT * FROM `referrals` WHERE IsDeprecated = 0 and AgencyId = @agencyid;";
        
        #endregion

        #region Internal Methods

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                database.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                database.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<AgencyPhysician> GetAgencyPhysicians(Guid agencyId)
        {
            var list = new List<AgencyPhysician>();

            using (var cmd = new FluentCommand<AgencyPhysician>(SELECT_AGENCY_PHYSICIANS))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString").AddGuid("agencyid", agencyId).AsList();
            }
            return list;
        }

        internal static List<AgencyContact> GetAgencyContacts(Guid agencyId)
        {
            var list = new List<AgencyContact>();

            using (var cmd = new FluentCommand<AgencyContact>(SELECT_AGENCY_CONTACTS))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString").AddGuid("agencyid", agencyId).AsList();
            }
            return list;
        }

        internal static List<Referral> GetAgencyReferals(Guid agencyId)
        {
            var list = new List<Referral>();

            using (var cmd = new FluentCommand<Referral>(SELECT_AGENCY_REFERRALS))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString").AddGuid("agencyid", agencyId).AsList();
            }
            return list;
        }

        #endregion
    }
}
