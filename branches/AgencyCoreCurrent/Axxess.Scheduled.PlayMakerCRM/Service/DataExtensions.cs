﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System;
    using System.Collections.Generic;

    public static class DataExtensions
    {
        public static void AddRecord(this Data data, DataRequestRecordsRecord[] record)
        {
            data.Request[0].Records[0].Record = record;
        }

        public static void AddField(this List<DataRequestRecordsRecordFieldsField> list, string key, string name, string value)
        {
            list.Add(new DataRequestRecordsRecordFieldsField
            {
                Key = key,
                Name = name,
                Value = value
            });
        }
    }
}
