﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System;
    using System.Diagnostics;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public abstract class BaseApiEntity
    {
    }
}
