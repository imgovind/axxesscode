﻿namespace Axxess.Api.AbilityNetwork
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    public partial class AbilityNetworkWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public AbilityNetworkWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "AbilityNetworkService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(AbilityNetworkService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("AbilityNetwork Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("AbilityNetwork Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
