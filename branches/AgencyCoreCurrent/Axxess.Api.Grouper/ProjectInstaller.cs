﻿namespace Axxess.Api.Grouper
{
    using System;
    using System.ServiceProcess;
    using System.ComponentModel;
    using System.Configuration.Install;

    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller grouperInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.grouperInstaller = new ServiceInstaller();
            this.grouperInstaller.StartType = ServiceStartMode.Automatic;
            this.grouperInstaller.ServiceName = "GrouperService";
            this.grouperInstaller.DisplayName = "Grouper Service";
            this.grouperInstaller.Description = "Provides Grouper Information to the AgencyCore Application.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.grouperInstaller });
        }
    }
}
