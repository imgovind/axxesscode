﻿namespace Axxess.AgencyManagement.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;


    public enum ReportCategory
    {
        [Description("Patient Reports")]
        Patient = 1,
        [Description("Clinical Reports")]
        Clinical = 2,
        [Description("Billing/Financial Reports")]
        BillingFinancial = 3,
        [Description("Schedule Reports")]
        Schedule = 4,
        [Description("Statistical Reports")]
        Statistical = 5,
        [Description("Annual Survey Report (Missouri)")]
        AnnualSurveyReportMissouri = 6,
        [Description("Annual Utilization Report (California)")]
        AnnualUtilizationReportCalifornia = 7,
        [Description("Employee Reports")]
        Employee = 8,
        [Description("Payroll Reports")]
        PayrollDetailSummary = 9,
        //temp testing report category
        [Description("Testing Reports")]
        Testing = 10,
        
    }
}
