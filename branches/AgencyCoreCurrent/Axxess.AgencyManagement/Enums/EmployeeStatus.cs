﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum NonAdmitTypes 
    {
        [Description("Patient")]
        Patient = 1,
        [Description("Referal")]
        Referral = 2
    }
}
