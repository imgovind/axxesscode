﻿namespace Axxess.AgencyManagement.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum UB4PatientStatus
    {
        [Description("30 - Still a patient. Services continue to be provided")]
        StillPatient = 30,
        [Description("01 - Discharge to home or self-care (routine discharge)")]
        DischargeToHomeOrSelfCare = 01,
        [Description("02 - Discharge/transfer to short-term general Hospital")]
        DischargeToShortTermHospital = 02,
        [Description("03 - Discharge/transfer to SNF")]
        DischargeToSNF = 03,
        [Description("04 - Discharge/transfer to Intermediate Care Facility")]
        DischargeToICF = 04,
        [Description("05 - Discharge to designated cancer center or children's hospital")]
        DischargeToAnotherTypeOfInstitution = 05,
        [Description("06 - Discharge to home under care of HHS")]
        DischargeToHomeUnderCareOfHHS = 06,
        [Description("07 - Left against medical advice or discontinued care")]
        LeftAgainstMedicalAdviceOrDiscontinuedCare = 07,
        [Description("20 - Expired")]
        Expired = 20,
        [Description("21 - Discharge/transfer to court/law enforcement")]
        DischargeToCourtEnforcement = 21,
        //[Description("Expired at home")]
        //ExpiredAtHome = 40,
        //[Description("Expired in a medical facility")]
        //ExpiredInMedicalFacility = 41,
        //[Description("Expired, place unknown")]
        //ExpiredPlaceUnknown = 42,
        [Description("43 - Discharge/transfer to a Federal Hospital")]
        DischargeToFHCF = 43,
        [Description("50 - Discharge/transfer for Hospice in the home")]
        DischargeToHospiceHome = 50,
        [Description("51 - Discharge/transfer to Hospice services in a medical facility")]
        DischargeToHospiceMedicareFacility = 51,
        [Description("61 - Discharge/transfer to hospital-based Medicare approved swing bed")]
        DischargeToSwingBed = 61,
        [Description("62 - Discharge/transfer to Inpatient Rehabilitation Facility")]
        DischargeToIRF = 62,
        [Description("63 - Discharge/transfer to long-term care hospital")]
        DischargeToLongTermHospital = 63,
        [Description("64 - Discharge/transfer to Medicaid certified, but non-Medicare certified nursing facility")]
        DischargeToMedicaidCertifiedNursingFacility = 64,
        [Description("65 - Discharge/transfer to psychiatric hospital or psychiatric part unit of a hospital")]
        DischargeToPsychiatricHospital = 65,
        [Description("66 - Discharge/transfer to Critial Access Hospital(CAH)")]
        DischargeToCAH = 66,
        [Description("70 - Discharge/transfer to another type of health care institution not defined elsewhere in code list")]
        DischargeToNotDefiendElsewhere = 70
    }
}
