﻿namespace Axxess.AgencyManagement.Domain
{
    using System;


    public class Payment
    {
        #region Members

        public int Id { get; set; }
        public string Account { get; set; }
        public int Sequence { get; set; }
        public double Amount { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Package { get; set; }

        #endregion
    }
}
