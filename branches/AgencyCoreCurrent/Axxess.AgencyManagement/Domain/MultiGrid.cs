﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    public class MultiGrid
    {
        public List<SelectableItem> Left { get; set; }
        public List<SelectableItem> Right { get; set; }
    }
}
