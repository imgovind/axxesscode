﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;

    public class UserNonVisitTask : EntityBase
    {

        #region Members
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }
        public DateTime TaskDate { get; set; }
        public bool PaidStatus { get; set; }
        public DateTime PaidDate { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        [SubSonicIgnore]
        public string TaskTitle { get; set; }
        [SubSonicIgnore]
        public string TaskText { get; set; }
        [SubSonicIgnore]
        public string UserDisplayName { get; set; }
        #endregion

        #region Domain
        [SubSonicIgnore]
        public string TaskDateDateString { get { return this.TaskDate.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public string PaidDateDateString { get { if (this.PaidStatus) { return this.PaidDate.ToShortDateString().ToZeroFilled(); } else { return string.Empty; } } }
        [SubSonicIgnore]
        public string TimeInDateString { get { return this.TimeIn.ToShortTimeString(); } }
        [SubSonicIgnore]
        public string TimeOutDateString { get { return this.TimeOut.ToShortTimeString(); } }
        [SubSonicIgnore]
        public string PaidStatusString 
        { 
            get 
            {
                StringBuilder sb = new StringBuilder();
                string returnVariable = default(string);
                if (this.PaidStatus) 
                {
                    sb.Append("   <span class=");sb.Append('"');sb.Append("img icon success-small");sb.Append('"');sb.Append("></span>");
                    returnVariable = sb.ToString();
                } else 
                {
                    sb.Append("   <span class="); sb.Append('"'); sb.Append("img icon error-small"); sb.Append('"'); sb.Append("></span>");
                    returnVariable = sb.ToString();
                }
                return returnVariable;
            }
        }
        [SubSonicIgnore]
        public int MinSpent
        {
            get
            {                
                if (this.TimeOut.ToString().IsNotNullOrEmpty() && this.TimeIn.ToString().IsNotNullOrEmpty())
                {
                    if (TimeOut >= TimeIn)
                    {
                        return (TimeOut.Hour * 60 + TimeOut.Minute) - (TimeIn.Hour * 60 + TimeIn.Minute);
                    }
                    else
                    {
                        return 24 * 60 - (TimeIn.Hour * 60 + TimeIn.Minute) + (TimeOut.Hour * 60 + TimeOut.Minute);
                    }
                }
                return 0;
            }
            set { }
        }
        #endregion

        #region Validation Rules
        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.TaskId.ToString()), "Task is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.UserId.ToString()), "User is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.TaskDate.ToString()), "TaskDate is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.TimeIn.ToString()), "TimeIn is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.TimeOut.ToString()), "TimeOut is required. <br />"));
        }
        #endregion

    }
}
