﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;

    public class AgencyNonVisit : EntityBase
    {
        #region Members
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        #endregion

        #region Domain
        [SubSonicIgnore]
        public string CreatedDateString { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public string ModifiedDateString { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }
        #endregion

        #region Validation Rules
        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Title), "Title is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Text is required. <br />"));
        }
        #endregion
    }
}
