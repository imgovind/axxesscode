﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Enums;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;
    public class CarePlanOversights : EntityBase
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid PhysicianLoginId { get; set; }
        public string CptCode { get; set; }
        public int Duration { get; set; }
        public int Activity { get; set; }
        public string Comments { get; set; }
        public DateTime LogDate { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        [SubSonicIgnore]
        public string AgencyName { get; set; }
        [SubSonicIgnore]
        public string PatientName { get; set; }
        [SubSonicIgnore]
        public string LogDateFormatted { get; set; }
        [SubSonicIgnore]
        public string Checkbox
        {
            get
            {
                if (this.Status == 425)
                {
                    return "<input name='CpoSelected' class='BillChecked' type='checkbox' value='"+this.Id+"'/>";
                }
                else
                    return "";
            }
        }
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                ScheduleStatus status = Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status.ToString()) : ScheduleStatus.NoStatus;
                return status.GetDescription();
            }
        }
        [SubSonicIgnore]
        public string CptType
        {
            get
            {
                CpoCode cpoCode = Enum.IsDefined(typeof(CpoCode), this.CptCode) ? (CpoCode)Enum.Parse(typeof(CpoCode), this.CptCode) : CpoCode.NoCode;
                return cpoCode.GetDescription();
            }
        }
        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.CptCode), "Category is required. <br />"));
            AddValidationRule(new Validation(() => this.Duration <= 0, "Duration is required."));
                
        }
    }
}
