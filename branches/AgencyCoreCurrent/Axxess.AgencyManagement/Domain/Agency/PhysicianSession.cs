﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core;

    [Serializable]
    public sealed class PhysicianSession
    {
        public Guid LoginId { get; set; }
        public string NpiNumber { get; set; }
        public string DisplayName { get; set; }
        public string ImpersonatorName { get; set; }
        //public IDictionary<Guid,Guid> AgencyPhysicianIdentifiers { get; set; }
        public IDictionary<string,NameIdPair> Agencies { get; set; }
        public Guid DefaultAgencyId { get; set; }
    }

   
}
