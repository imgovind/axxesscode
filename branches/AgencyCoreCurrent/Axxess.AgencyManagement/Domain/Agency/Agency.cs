﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class Agency : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid Trainer { get; set; }
        public Guid BackupTrainer { get; set; }
        public Guid SalesPerson { get; set; }
        public Guid ImplementationSpecialist { get; set; }
        public string Name { get; set; }
        public string TaxId { get; set; }
        public string TaxIdType { get; set; }
        public string Payor { get; set; }
        public string NationalProviderNumber { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string MedicaidProviderNumber { get; set; }
        public string HomeHealthAgencyId { get; set; }
        public string SubmitterId { get; set; }
        public string SubmitterName { get; set; }
        public string SubmitterPhone { get; set; }
        public string SubmitterFax { get; set; }
        public bool IsAgreementSigned { get; set; }
        public bool IsSuspended { get; set; }
        public int TrialPeriod { get; set; }
        public int Package { get; set; }
        public int AnnualPlanId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public bool IsDeprecated { get; set; }
        public int CahpsVendor { get; set; }
        public string CahpsVendorId { get; set; }
        public string CahpsSurveyDesignator { get; set; }
        public bool IsAxxessTheBiller { get; set; }
        public int OasisAuditVendor { get; set; }
        public string OasisAuditVendorApiKey { get; set; }
        public DateTime FrozenDate { get; set; }
        public bool IsFrozen { get; set; }
        public string AccountId { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public string ContactPersonDisplayName
        {
            get
            {
                return string.Concat(this.ContactPersonFirstName, " ", this.ContactPersonLastName);
            }
        }
        [SubSonicIgnore]
        public string ContactPersonPhoneFormatted
        {
            get
            {
                return ContactPersonPhone.ToPhone();
            }
        }
        [ScriptIgnore]
        [SubSonicIgnore]
        public AgencyLocation MainLocation
        {
            get;
            set;
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminUsername { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminPassword { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminFirstName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminLastName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ContactPhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string LocationName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressLine1 { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressLine2 { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressCity { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressStateCode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressZipCode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressZipCodeFour { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string Phone { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> PhoneArray { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> FaxArray { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> SubmitterPhoneArray { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> SubmitterFaxArray { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public string PackageDescription
        {
            get
            {
                if (Package == 100000)
                {
                    return "Unlimited users";
                }
                else if (Package == 100001)
                {
                    return "Unlimited users (Premier Plan)";
                }
                else if (Package == 100002)
                {
                    return "Unlimited users (Enterprise Plan)";
                }
                return string.Format("{0} users", this.Package.ToString());
            }
        }

        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public string AnnualPlanDescription
        {
            get
            {
                var annualPlan = string.Empty;

                if (this.AnnualPlanId == 1)
                {
                    annualPlan = "5% Discount for 6 month payment";
                }
                else if (this.AnnualPlanId == 2)
                {
                    annualPlan = "10% Discount for full year payment";
                }
                else if (this.AnnualPlanId == 3)
                {
                    annualPlan = "15% Discount for 2 years payment";
                }
                
                return annualPlan;
            }
        }

        [SubSonicIgnore]
        public List<AgencyLocation> Branches { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Agency Name is required. <br />"));
            if (this.Id.IsEmpty())
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminUsername), "Admin E-mail is required.  <br />"));
                AddValidationRule(new Validation(() => !this.AgencyAdminUsername.IsEmail(), "Admin E-mail is not in a valid  format.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminFirstName), "Admin FirstName is required. <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminLastName), "Admin LastName is required. <br />"));
               

                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.TaxId), "Tax Id is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Payor), "Payor is required."));
                AddValidationRule(new Validation(() => this.Payor.IsNotNullOrEmpty() && !this.Payor.IsInteger(), "Payor is not in the right format."));
                AddValidationRule(new Validation(() => this.Payor.IsNotNullOrEmpty() && this.Payor.IsInteger() && this.Payor.ToInteger() <= 0, "Payor is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.NationalProviderNumber), "National Provider Number is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.ContactPersonFirstName), "Contact Person First Name is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.ContactPersonLastName), "Contact Person Last Name is required."));
                if (!this.IsAxxessTheBiller)
                {
                    AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.SubmitterId), "Submitter Id is required."));
                    AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.SubmitterName), "Submitter Name is required."));
                }

                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Agency Address line is required.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Agency city is required.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Agency state is required.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Agency zip is required.  <br />"));

                AddValidationRule(new Validation(() => this.SalesPerson.IsEmpty(), "Sales Person is required.  <br />"));
                AddValidationRule(new Validation(() => this.Trainer.IsEmpty(), "Trainer is required.  <br />"));
            }
        }

        #endregion
    }
}
