﻿namespace Axxess.AgencyManagement.Domain
{
    using System.Xml.Serialization;
    using Axxess.AgencyManagement.Enums;

    [XmlRoot()]
    public class Locator
    {
        public Locator()
        {
            Type = (int)LocatorType.String;
        }

        public Locator(string id)
        {
            LocatorId = id;
            Type = (int)LocatorType.String;
        }

        [XmlElement]
        public string LocatorId { get; set; }
        [XmlElement]
        public string Code1 { get; set; }
        [XmlElement]
        public string Code2 { get; set; }
        [XmlElement]
        public string Code3 { get; set; }
        [XmlElement]
        public string PhysicianType { get; set; }
        [XmlElement]
        public bool Customized { get; set; }
        [XmlElement]
        public int Type { get; set; }

    }
}
