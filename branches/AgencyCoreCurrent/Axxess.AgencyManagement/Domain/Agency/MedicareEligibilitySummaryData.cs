﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using System.Xml.Serialization;

namespace Axxess.AgencyManagement.Domain
{
    public class MedicareEligibilitySummaryData
    {
        public List<MedicareEligibilityError> Errors { get; set; }
        public List<MedicareEligibilityPayor> Payors { get; set; }
    }

    public class MedicareEligibilityError
    {
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string MedicareNumber { get; set; }
        public string Error { get; set; }
        [XmlIgnore]
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }
    }

    public class MedicareEligibilityPayor
    {
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string MedicareNumber { get; set; }
        public string PlanNumber { get; set; }
        public string Payor { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        [XmlIgnore]
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }
        [XmlIgnore]
        public string PayorWithHtmlAddress { get { return Payor != null && Payor != "" ? string.Format("{0}{1}{2}", this.Payor, this.AddressLineOne.IsNotNullOrEmpty() ? "<br/>" + this.AddressLineOne : "", this.AddressLineTwo.IsNotNullOrEmpty() ? "<br/>" + this.AddressLineTwo : "") : ""; } }
    }
}
