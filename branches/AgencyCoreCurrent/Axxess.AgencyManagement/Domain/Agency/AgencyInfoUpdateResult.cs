﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class AgencyInfoUpdateResult
    {
        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
    }
}
