﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    public class AgencySubscriptionPlan
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public bool IsUserPlan { get; set; }
        public int PlanLimit { get; set; }

        [SubSonicIgnore]
        public int CurrentPlanCount { get; set; }
    }
}