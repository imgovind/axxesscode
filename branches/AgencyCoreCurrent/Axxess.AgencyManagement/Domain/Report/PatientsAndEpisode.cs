﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "ArrayOfScheduleEvent")]
    public class PatientsAndEpisode
    {
        //Patients Table
        public Guid Id { get; set; }
        public string PrimaryInsurance  { get; set; }
        public string SecondaryInsurance { get; set; }
        public string TertiaryInsurance { get; set; }
        public string Schedule { get; set; }
        public List<ScheduleEvent> ScheduleEvents { get; set; }
    }
}
