﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class VitalSign
    {
        public double BSMaxGraph { get; set; }
        public double BSMinGraph { get; set; }
        public double TempGraph { get; set; }
        public double RespGraph { get; set; }
        public double WeightGraph { get; set; }
        public int PainLevelGraph { get; set; }
        public DateTime VisitDateGraph { get; set; }
        public int ApicalPulseGraph { get; set; }
        public int RadialPulseGraph { get; set; }
        public int BPSystolic { get; set; }
        public int BPDiastolic { get; set; }

        public string BSMax { get; set; }
        public string BSMin { get; set; }
        public string Temp { get; set; }
        public string Resp { get; set; }
        public string Weight { get; set; }
        public string PainLevel { get; set; }
        public string VisitDate { get; set; }
        public string ApicalPulse { get; set; }
        public string RadialPulse { get; set; }
        public string DisciplineTask { get; set; }
        public string UserDisplayName { get; set; }
        public string PatientDisplayName { get; set; }

        public string BPLyingLeft { get; set; }
        public string BPLyingRight { get; set; }
        public string BPSittingLeft { get; set; }
        public string BPSittingRight { get; set; }
        public string BPStandingLeft { get; set; }
        public string BPStandingRight { get; set; }

        public string BPLying { get; set; }
        public string BPSitting { get; set; }
        public string BPStanding { get; set; }

        public string OxygenSaturation { get; set; }

       



    }
}
