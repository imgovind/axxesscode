﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using SubSonic.SqlGeneration.Schema;

    public class OrderLean
    {
        public Guid Id { get; set; }
        public string NoteType { get; set; }
        public long OrderNumber { get; set; }
        public int Status { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public DateTime SentDate { get; set; }
        public string Schedule { get; set; }


        #region Domain

        [SubSonicIgnore]
        public string PatientName { get; set; }

        #endregion
    }
}
