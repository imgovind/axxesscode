﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class HomeHealthGoldIdentity
    {
        public int Id { get; set; }
        public Guid EntityId { get; set; }
    }
}
