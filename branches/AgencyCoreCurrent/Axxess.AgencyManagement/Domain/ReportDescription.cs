﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;


    public class ReportDescription 
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string Parameters { get; set; }
        public string InformationInclude { get; set; }
        public string Comments { get; set; }
        public bool IsOnclick { get; set; }
        public bool IsProductionReady { get; set; }
    }
}
