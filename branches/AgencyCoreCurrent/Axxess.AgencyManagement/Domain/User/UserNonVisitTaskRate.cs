﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    [XmlRoot()]
    public class UserNonVisitTaskRate : EntityBase
    {
        #region Members
        [XmlElement]
        [UIHint("Non-Visit Task")]
        public Guid Id { get; set; }
        [XmlElement]
        public int RateType { get; set; }
        [XmlElement]
        public double Rate { get; set; }
        #endregion

        #region Domain
        [XmlIgnore]
        public Guid UserId { get; set; }
        [XmlIgnore]
        public string RateTypeName
        {
            get
            {
                return RateType > 0 ? RateType.ToEnum<UserRateTypes>(UserRateTypes.None).GetDescription() : string.Empty;
            }
        }
        [XmlIgnore]
        public string TaskTitle { get; set; }
        [XmlIgnore]
        public string TaskText { get; set; }
        [XmlIgnore]
        public string RateString 
        { 
            get 
            {
                if (this.Rate > 0)
                {
                    if (this.RateType == (int)UserRateTypes.Hourly)
                    {
                        return String.Format("{0:C} / hr", this.Rate);
                    }
                    else if (this.RateType == (int)UserRateTypes.PerVisit)
                    {
                        return String.Format("{0:C} / visit", this.Rate);
                    }
                }
                return string.Empty;
            }
        }
        #endregion

        #region Validation Rules
        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.Id.IsEmpty(), "Task is required."));
            AddValidationRule(new Validation(() => this.Rate < 0, "Rate is required."));
            AddValidationRule(new Validation(() => this.RateType < 0, "Rate Type is required."));
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            if (this.Rate > 0)
            {
                if (this.RateType == (int)UserRateTypes.Hourly)
                {
                    return String.Format("{0:C} / hr", this.Rate);
                }
                else if (this.RateType == (int)UserRateTypes.PerVisit)
                {
                    return String.Format("{0:C} / visit", this.Rate);
                }
            }
            return string.Empty;
        }
        #endregion
    }
}
