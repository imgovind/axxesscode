﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class EpisodeLean
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public bool IsDischarged { get; set; }
        public bool IsActive { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }

        public string Range { get { return string.Format("{0} - {1}", this.StartDate.ToString("MM/dd/yyyy"), this.EndDate.ToString("MM/dd/yyyy")); } }
    }
}
