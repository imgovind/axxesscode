﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class ReferralEmergencyContact : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternatePhone { get; set; }
        public string EmailAddress { get; set; }
        public string Relationship { get; set; }
        public bool IsPrimary { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public List<string> PhonePrimaryArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneAlternateArray { get; set; }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        [SubSonicIgnore]
        public string PrimaryPhoneFormatted { get { return this.PrimaryPhone.ToPhone(); } }

        //Checks if the PrimaryPhone has a value or if the values in the PhonePrimaryArray are not empty
        [SubSonicIgnore]
        public bool PrimaryPhoneHasValue
        {
            get
            {
                bool result = false;
                if (PrimaryPhone.IsNotNullOrEmpty())
                {
                    result = true;
                }
                else if (PhonePrimaryArray.IsNotNullOrEmpty())
                {
                    var phoneStringJoined = string.Join("", PhonePrimaryArray.ToArray());
                    if (phoneStringJoined.IsNotNullOrEmpty())
                    {
                        result = true;
                    }
                }
                return result;
            }
        }

        [SubSonicIgnore]
        public bool HasValue 
        { 
            get 
            {
                if (this.FirstName.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.LastName.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.PrimaryPhoneHasValue)
                {
                    return true;
                }
                else if (this.Relationship.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.AlternatePhone.IsNotNullOrEmpty())
                {
                    return true;
                }
                else if (this.EmailAddress.IsNotNullOrEmpty())
                {
                    return true;
                }
                else
                { 
                    return false; 
                }
            } 
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.HasValue &&
                               (string.IsNullOrEmpty(this.FirstName) || string.IsNullOrEmpty(this.LastName) || !this.PrimaryPhoneHasValue),
                                "Patient emergency contact requires a first name, last name, and a primary phone number.<br/>"));
        }

        #endregion

    }
}
