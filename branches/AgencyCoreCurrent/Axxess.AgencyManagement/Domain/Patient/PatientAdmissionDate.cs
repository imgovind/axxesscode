﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using System.ComponentModel.DataAnnotations;

    public class PatientAdmissionDate
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartOfCareDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime DischargedDate { get; set; }
        public string PatientData { get; set; }
        public int Status { get; set; }
        public int DischargeReasonId { get; set; }
        public string Reason { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
