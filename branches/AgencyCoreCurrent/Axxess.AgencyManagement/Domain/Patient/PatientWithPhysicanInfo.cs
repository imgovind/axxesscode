﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PatientWithPhysicanInfo
    {
        public Guid Id { get; set; }
        public string PatientIdNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string PhysicianNPI { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public Guid CaseManagerId { get; set; }
        public DateTime Created { get; set; }

    }
}
