﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Axxess.AgencyManagement.Enums;
    using System.ComponentModel.DataAnnotations;

    public class ManagedClaimPayment : EntityBase
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ClaimId { get; set; }
        public Guid PatientId { get; set; }
        public double Payment { get; set; }
        public DateTime PaymentDate { get; set; }
        [SubSonicIgnore]
        public string PaymentDateFormatted { 
            get {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToZeroFilled() : "";
            } 
        }
        public int Payor { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        [SubSonicIgnore]
        public string PaymentAmount { get; set; }
        [SubSonicIgnore]
        public string PayorName { get; set; }
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [SubSonicIgnore]
        public DateTime EpisodeEndDate { get; set; }
        [SubSonicIgnore]
        public string ClaimDateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.Payor == 0, "Payor is required."));
        }
    }
}
