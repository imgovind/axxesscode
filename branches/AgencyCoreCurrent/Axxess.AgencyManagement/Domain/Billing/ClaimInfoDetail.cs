﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class ClaimInfoDetail
    {
        public long BatchId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BillType { get; set; }
        public double ClaimAmount { get; set; }
        public double ProspectivePay { get; set; }
        public string HippsCode { get; set; }
        public string AddressZipCode { get; set; }

        public string DisplayName 
        { 
            get 
            {
                var name = string.Empty;
                if (this.LastName.IsNotNullOrEmpty() && this.FirstName.IsNotNullOrEmpty())
                {
                    name = string.Format("{0}, {1}", this.LastName.ToUpperCase(), this.FirstName.ToUpperCase());
                    if (this.MiddleInitial.IsNotNullOrEmpty())
                    {
                        name = string.Format(name + " {0}", this.MiddleInitial.ToInitial());
                    }
                }
                return name; 
            } 
        }
        
        public string Range 
        { 
            get 
            {
                var range = string.Empty;
                if (this.StartDate != DateTime.MinValue && this.EndDate != DateTime.MinValue)
                {
                    range = string.Format("{0} - {1}", this.StartDate.ToString("MM/dd/yyyy"), this.EndDate.ToString("MM/dd/yyyy"));
                }
                return range; 
            } 
        }
    }
}
