﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Extensions;
    using Axxess.AgencyManagement.Enums;
    using System.ComponentModel.DataAnnotations;

    public class ManagedClaimAdjustment
    {
        #region Members
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ClaimId { get; set; }
        public Guid PatientId { get; set; }
        public double Adjustment { get; set; }
        public Guid TypeId { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        #endregion

        #region Domain

        [SubSonicIgnore]
        public string Type { get; set; }
        [SubSonicIgnore]
        public string Description { get; set; }

        #endregion
    }
}
