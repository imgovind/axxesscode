﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
   
    using System.ComponentModel.DataAnnotations;

    public class SecondaryClaimLean
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid PrimaryClaimId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public bool IsVerified { get; set; }
        public DateTime Modified { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string VerifiedVisit { get; set; }
        public int SecondaryInsuranceId { get; set; }

        public bool IsVisitVerified { get; set; }
        public bool IsInsuranceVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsInfoVerified { get; set; }
        public bool IsRemittanceVerified { get; set; }

        public int Status { get; set; }

        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return this.PaymentDate != DateTime.MinValue ? this.PaymentDate.ToZeroFilled() : string.Empty;
            }
        }
        public DateTime ClaimDate { get; set; }
        public string ClaimDateFormatted
        {
            get
            {
                return this.ClaimDate != DateTime.MinValue ? this.ClaimDate.ToZeroFilled() : string.Empty;
            }
        }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public double Balance
        {
            get
            {
                return ClaimAmount - PaymentAmount;
            }
        }

        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus), this.Status) ? ((BillingStatus)this.Status).GetDescription() : (Enum.IsDefined(typeof(ManagedClaimStatus), this.Status) ? ((ManagedClaimStatus)this.Status).GetDescription() : string.Empty);
            }
        }

        public string ClaimRange
        {
            get
            {
                return string.Format("{0} - {1}", this.StartDate != null ? this.StartDate.ToString("MM/dd/yyyy") : "", this.EndDate != null ? this.EndDate.ToString("MM/dd/yyyy") : "");
            }
        }


        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        public bool IsHMO { get; set; }
        public int InvoiceType { get; set; }

        public string PrintUrl 
        {
            get
            {
                var url = string.Empty;
                if (this.IsVisitVerified && this.IsInfoVerified && this.IsSupplyVerified && this.IsRemittanceVerified && this.IsInsuranceVerified)
                {
                    if (this.InvoiceType == (int)Enums.InvoiceType.UB)
                    {
                        //url += string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/SecondaryUB04Pdf', {{ 'patientId': '{0}', 'Id': '{1}' }});\">UB-04</a>", this.PatientId, this.Id);
                    }
                    else if (this.InvoiceType == (int)Enums.InvoiceType.HCFA)
                    {
                        //url += string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/SecondaryHCFA1500Pdf', {{ 'patientId': '{0}', 'Id': '{1}' }});\">HCFA-1500</a> ", this.PatientId, this.Id);
                    }
                    else if (this.InvoiceType == (int)Enums.InvoiceType.Invoice)
                    {
                        //url += string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/InvoicePdf', {{ 'patientId': '{1}', 'Id': '{0}', 'isForPatient': '{2}' }});\" >Invoice</a>", this.Id, this.PatientId, false);
                    }

                    string downloadLink = string.Format("<a href=\"javascript:void(0);\" onclick=\"SecondaryBilling.LoadGeneratedSingleSecondaryClaim('{0}');\">Download</a>", this.Id);
                    if (url.IsNotNullOrEmpty())
                    {
                        url += " | " + downloadLink;
                    }
                    else
                    {
                        url = downloadLink;
                    }
                }
                return url;
            }
        }
    }
}
