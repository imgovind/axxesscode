﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;

    public static class PatientVistNoteExtensions
    {
        public static bool IsSkilledNurseNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (IsSkilledNurseNote(note.NoteType))
                {
                    result = true;
                }
            }

            return result;
        }

        private static bool IsSkilledNurseNote(string noteType)
        {
            var result = false;

            switch (noteType)
            {
                case "SkilledNurseVisit":
                case "SNVisitPM":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinNoon":
                case "SNInsulinHS":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNAssessment":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "LVNVisit":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNPsychAssessment":
                case "SNWoundCare":
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }

        public static bool IsHHANote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("HHAideVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsPASNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual(DisciplineTasks.PASVisit.ToString()))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsPTNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("PTVisit") || note.NoteType.IsEqual("PTAVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsSTNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("STVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsOTNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("OTVisit") || note.NoteType.IsEqual("COTAVisit"))
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
