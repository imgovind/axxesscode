﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using Repositories;

    public interface IAccountingManagementDataProvider
    {
        IAccountingRepository AccountingRepository
        {
            get;
        }
    }
}
