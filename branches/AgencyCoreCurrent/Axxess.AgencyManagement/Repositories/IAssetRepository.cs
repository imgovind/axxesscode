﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using Domain;

    public interface IAssetRepository
    {
        bool Delete(Guid id);
        bool Remove(Guid id);
        bool Add(Asset asset);
        Asset Get(Guid id, Guid agencyId);

        bool DeletePatientDocument(Guid id, Guid patientId);
        bool AddPatientDocument(Asset asset, PatientDocument patientDocument);
        bool UpdatePatientDocument(PatientDocument patientDocument);
        PatientDocument GetPatientDocument(Guid id, Guid patientId, Guid agencyId);
        List<PatientDocument> GetPatientDocuments(Guid patientId);
    }
}
