﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    using Enums;
    using Domain;

    public class ReferralRepository : IReferralRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ReferralRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IReferralRepository Members

        public bool Add(Referral referral)
        {
            var result = false;
            try
            {
                if (referral != null)
                {
                    if (referral.PhoneHomeArray != null && referral.PhoneHomeArray.Count >= 2)
                    {
                        referral.PhoneHome = referral.PhoneHomeArray.ToArray().PhoneEncode();
                    }
                    if (referral.ServicesRequiredCollection != null && referral.ServicesRequiredCollection.Count > 0)
                    {
                        referral.ServicesRequired = referral.ServicesRequiredCollection.ToArray().AddColons();
                    }
                    if (referral.DMECollection != null && referral.DMECollection.Count > 0)
                    {
                        referral.DME = referral.DMECollection.ToArray().AddColons();
                    }
                    if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                    {
                        var physicianList = new List<Physician>();

                        int i = 0;
                        foreach (var guid in referral.AgencyPhysicians)
                        {
                            if (!guid.IsEmpty())
                            {
                                physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                                i++;
                            }
                        }
                        referral.Physicians = physicianList.ToXml();
                    }
                    referral.Status = (byte)ReferralStatus.Pending;
                    referral.Created = DateTime.Now;
                    referral.Modified = DateTime.Now;
                    database.Add<Referral>(referral);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool NonAdmitReferral(PendingPatient pending)
        {
            bool result = false;

            if (pending != null)
            {
                var referral = database.Single<Referral>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId && p.IsDeprecated == false);
                if (referral != null)
                {
                    referral.NonAdmissionDate = pending.NonAdmitDate;
                    referral.NonAdmissionReason = pending.Reason;
                    referral.Comments = pending.Comments;
                    referral.Status = (int)ReferralStatus.NonAdmission;
                    referral.Modified = DateTime.Now;

                    database.Update<Referral>(referral);
                    result = true;
                }
            }
            return result;
        }

        public IEnumerable<Referral> GetAll(Guid agencyId, ReferralStatus status)
        {
            return database.Find<Referral>(r => r.AgencyId == agencyId && r.Status == (int)status && r.IsDeprecated == false);
        }

        public IEnumerable<Referral> GetAllByCreatedUser(Guid agencyId, Guid createdById, ReferralStatus status)
        {
            return database.Find<Referral>(r => r.AgencyId == agencyId && r.CreatedById == createdById && r.Status == (int)status && r.IsDeprecated == false);
        }

        public List<ReferralData> All(Guid agencyId, ReferralStatus referralStatus)
        {
            var query = new QueryBuilder("SELECT Id, UserId, FirstName, LastName, DOB, Gender, Status, CreatedById, AdmissionSource, ReferralDate FROM referrals")
                .Where("AgencyId = @agencyid").And("Status = @status").And("IsDeprecated = 0").OrderBy("LastName", true);

            var list = new List<ReferralData>();
            using (var cmd = new FluentCommand<ReferralData>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)referralStatus)
                .SetMap(reader => new ReferralData
                {
                    Id = reader.GetGuid("Id"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AdmissionSourceId = reader.GetInt("AdmissionSource"),
                    DateOfBirth = reader.GetDateTime("DOB").ToShortDateString().ToZeroFilled(),
                    ReferralDate = reader.GetDateTime("ReferralDate").ToShortDateString().ToZeroFilled(),
                    CreatedBy = reader.GetGuid("CreatedById").IsEmpty() ? string.Empty : UserEngine.GetName(reader.GetGuid("CreatedById"), agencyId)
                })
                .AsList();
            }
            return list;
        }

        public List<ReferralData> AllByUser(Guid agencyId, Guid createdById, ReferralStatus status)
        {
            var query = new QueryBuilder("SELECT Id, UserId, FirstName, LastName, DOB, Gender, Status, CreatedById, AdmissionSource, ReferralDate FROM referrals")
                .Where("AgencyId = @agencyid").And("CreatedById = @createdbyid").And("IsDeprecated = 0").And("Status = @status").OrderBy("LastName", true);

            var list = new List<ReferralData>();
            using (var cmd = new FluentCommand<ReferralData>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("createdbyid", createdById)
                .AddGuid("agencyid", agencyId)
                .AddInt("status", (int)status)
                .SetMap(reader => new ReferralData
                {
                    Id = reader.GetGuid("Id"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AdmissionSourceId = reader.GetInt("AdmissionSource"),
                    DateOfBirth = reader.GetDateTime("DOB").ToShortDateString().ToZeroFilled(),
                    ReferralDate = reader.GetDateTime("ReferralDate").ToShortDateString().ToZeroFilled(),
                    CreatedBy = !reader.GetGuid("CreatedById").IsEmpty() ? UserEngine.GetName(reader.GetGuid("CreatedById"), agencyId) : string.Empty
                })
                .AsList();
            }
            return list;
        }

        public Referral Get(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
        }

        public bool SetStatus(Guid agencyId, Guid id, ReferralStatus status)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var result = false;
            var referral = Get(agencyId, id);

            if (referral != null)
            {
                referral.Status = (int)status;
                database.Update<Referral>(referral);
                result = true;
            }
            return result;
        }

        public bool Update(Referral referral)
        {
            bool result = false;
            if (referral != null)
            {
                if (referral.ServicesRequiredCollection != null && referral.ServicesRequiredCollection.Count > 0)
                {
                    referral.ServicesRequired = referral.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (referral.DMECollection != null && referral.DMECollection.Count > 0)
                {
                    referral.DME = referral.DMECollection.ToArray().AddColons();
                }
                if (referral.PhoneHomeArray != null && referral.PhoneHomeArray.Count > 0)
                {
                    referral.PhoneHome = referral.PhoneHomeArray.ToArray().PhoneEncode();
                }

                var editReferral = Get(referral.AgencyId, referral.Id);
                if (editReferral != null)
                {
                    editReferral.AdmissionSource = referral.AdmissionSource;
                    editReferral.ReferrerPhysician = referral.ReferrerPhysician;
                    editReferral.ReferralDate = referral.ReferralDate;
                    editReferral.OtherReferralSource = referral.OtherReferralSource;
                    editReferral.InternalReferral = referral.InternalReferral;
                    editReferral.FirstName = referral.FirstName;
                    editReferral.MiddleInitial = referral.MiddleInitial;
                    editReferral.LastName = referral.LastName;
                    editReferral.MedicaidNumber = referral.MedicaidNumber;
                    editReferral.MedicareNumber = referral.MedicareNumber;
                    editReferral.SSN = referral.SSN;
                    editReferral.DOB = referral.DOB;
                    editReferral.Gender = referral.Gender;
                    editReferral.Height = referral.Height;
                    editReferral.HeightMetric = referral.HeightMetric;
                    editReferral.Weight = referral.Weight;
                    editReferral.WeightMetric = referral.WeightMetric;
                    editReferral.MaritalStatus = referral.MaritalStatus;
                    editReferral.PhoneHome = referral.PhoneHome;
                    editReferral.EmailAddress = referral.EmailAddress;
                    editReferral.AddressLine1 = referral.AddressLine1;
                    editReferral.AddressLine2 = referral.AddressLine2;
                    editReferral.AddressCity = referral.AddressCity;
                    editReferral.AddressStateCode = referral.AddressStateCode;
                    editReferral.AddressZipCode = referral.AddressZipCode;
                    editReferral.ServicesRequired = referral.ServicesRequired;
                    editReferral.PrimaryInsurance = referral.PrimaryInsurance;
                    editReferral.PrimaryHealthPlanId = referral.PrimaryHealthPlanId;
                    editReferral.PrimaryGroupName = referral.PrimaryGroupName;
                    editReferral.PrimaryGroupId = referral.PrimaryGroupId;
                    editReferral.PrimaryRelationship = referral.PrimaryRelationship;
                    editReferral.SecondaryInsurance = referral.SecondaryInsurance;
                    editReferral.SecondaryHealthPlanId = referral.SecondaryHealthPlanId;
                    editReferral.SecondaryGroupName = referral.SecondaryGroupName;
                    editReferral.SecondaryGroupId = referral.SecondaryGroupId;
                    editReferral.SecondaryRelationship = referral.SecondaryRelationship;
                    editReferral.TertiaryInsurance = referral.TertiaryInsurance;
                    editReferral.TertiaryHealthPlanId = referral.TertiaryHealthPlanId;
                    editReferral.TertiaryGroupName = referral.TertiaryGroupName;
                    editReferral.TertiaryGroupId = referral.TertiaryGroupId;
                    editReferral.TertiaryRelationship = referral.TertiaryRelationship;
                    editReferral.DME = referral.DME;
                    editReferral.OtherDME = referral.OtherDME;
                    editReferral.UserId = referral.UserId;
                    editReferral.Comments = referral.Comments;
                    editReferral.IsDNR = referral.IsDNR;
                    if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                    {
                        var physicianList = new List<Physician>();

                        int i = 0;
                        foreach (var guid in referral.AgencyPhysicians)
                        {
                            if (!guid.IsEmpty())
                            {
                                physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                                i++;
                            }
                        }
                        editReferral.Physicians = physicianList.ToXml();
                    }
                    var emergencyContact = GetFirstEmergencyContactByReferral(referral.AgencyId, referral.Id);
                    if (emergencyContact != null)
                    {
                        if(referral.EmergencyContact != null)
                        {
                            referral.EmergencyContact.AgencyId = referral.AgencyId;
                            referral.EmergencyContact.ReferralId = referral.Id;
                            if (referral.EmergencyContact.FirstName.IsNullOrEmpty() && referral.EmergencyContact.LastName.IsNullOrEmpty() &&
                                referral.EmergencyContact.Relationship.IsNullOrEmpty() && referral.EmergencyContact.PrimaryPhone.IsNullOrEmpty() &&
                                referral.EmergencyContact.AlternatePhone.IsNullOrEmpty() && referral.EmergencyContact.EmailAddress.IsNullOrEmpty())
                            {
                                DeleteEmergencyContact(emergencyContact.Id, referral.Id);
                            }
                            else
                            {
                                EditEmergencyContact(referral.AgencyId, referral.EmergencyContact);
                            }
                        }
                    }
                    else if(referral.EmergencyContact != null)
                    {
                        referral.EmergencyContact.AgencyId = referral.AgencyId;
                        referral.EmergencyContact.ReferralId = referral.Id;
                        referral.EmergencyContact.IsPrimary = true;
                        AddEmergencyContact(referral.EmergencyContact);
                    }
                    editReferral.Modified = DateTime.Now;
                    database.Update<Referral>(editReferral);
                    result = true;
                }
            }

            return result;
        }

        public bool Delete(Guid agencyId, Guid id)
        {
            var referral = database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
            if (referral != null)
            {
                referral.IsDeprecated = true;
                referral.Modified = DateTime.Now;
                database.Update<Referral>(referral);
                return true;
            }
            return false;
        }


        public bool UpdateModal(Referral referral)
        {
            bool result = false;
            try
            {
                if (referral != null)
                {
                    database.Update<Referral>(referral);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AddEmergencyContact(ReferralEmergencyContact emergencyContactInfo)
        {
            var result = false;
            try
            {
                if (emergencyContactInfo != null)
                {
                    emergencyContactInfo.Id = Guid.NewGuid();
                    if (emergencyContactInfo.PhonePrimaryArray.Count >= 2)
                    {
                        emergencyContactInfo.PrimaryPhone = emergencyContactInfo.PhonePrimaryArray.ToArray().PhoneEncode();
                    }
                    if (emergencyContactInfo.PhoneAlternateArray.Count >= 2)
                    {
                        emergencyContactInfo.AlternatePhone = emergencyContactInfo.PhoneAlternateArray.ToArray().PhoneEncode();
                    }
                    database.Add<ReferralEmergencyContact>(emergencyContactInfo);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public IList<ReferralEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid referralId)
        {
            Check.Argument.IsNotEmpty(referralId, "referralId");
            return database.Find<ReferralEmergencyContact>(p => p.AgencyId == agencyId && p.ReferralId == referralId).OrderBy(p => p.IsPrimary == false).ToList();
        }


        public ReferralEmergencyContact GetEmergencyContact(Guid referralId, Guid emergencyContactId)
        {
            Check.Argument.IsNotEmpty(emergencyContactId, "emergencyContactId");
            return database.Single<ReferralEmergencyContact>(e => e.ReferralId == referralId && e.Id == emergencyContactId);
        }

        public bool EditEmergencyContact(Guid agencyId, ReferralEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            bool result = false;
            try
            {
                if (emergencyContact != null)
                {
                    if (emergencyContact.PhonePrimaryArray.Count >= 2)
                    {
                        emergencyContact.PrimaryPhone = emergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
                    }
                    if (emergencyContact.PhoneAlternateArray.Count >= 2)
                    {
                        emergencyContact.AlternatePhone = emergencyContact.PhoneAlternateArray.ToArray().PhoneEncode();
                    }
                    var emergencyContactToEdit = database.Single<ReferralEmergencyContact>(e => e.ReferralId == emergencyContact.ReferralId && e.Id == emergencyContact.Id);
                    if (emergencyContactToEdit != null)
                    {
                        emergencyContactToEdit.FirstName = emergencyContact.FirstName;
                        emergencyContactToEdit.LastName = emergencyContact.LastName;
                        emergencyContactToEdit.PrimaryPhone = emergencyContact.PrimaryPhone;
                        emergencyContactToEdit.AlternatePhone = emergencyContact.AlternatePhone;
                        emergencyContactToEdit.EmailAddress = emergencyContact.EmailAddress;
                        emergencyContactToEdit.Relationship = emergencyContact.Relationship;
                        if (emergencyContact.IsPrimary)
                        {
                            if (SetPrimaryEmergencyContact(agencyId, emergencyContact.ReferralId, emergencyContact.Id))
                            {
                                emergencyContactToEdit.IsPrimary = true;
                            }
                        }
                        database.Update<ReferralEmergencyContact>(emergencyContactToEdit);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid referralId)
        {
            bool result = false;
            try
            {
                if (!Id.IsEmpty() && !referralId.IsEmpty())
                {
                    var emergencyContact = database.Single<ReferralEmergencyContact>(r => r.ReferralId == referralId && r.Id == Id);
                    if (emergencyContact != null)
                    {
                        database.Delete<ReferralEmergencyContact>(emergencyContact.Id);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool SetPrimaryEmergencyContact(Guid agencyId, Guid referralId, Guid emergencyContactId)
        {
            bool result = false;
            try
            {
                var referral = database.Single<Referral>(p => p.Id == referralId);
                if (referral != null)
                {
                    var emergencyContacts = database.Find<ReferralEmergencyContact>(p => p.ReferralId == referralId);
                    bool flag = false; ;
                    foreach (ReferralEmergencyContact contat in emergencyContacts)
                    {
                        if (contat.Id == emergencyContactId)
                        {
                            contat.IsPrimary = true;
                            flag = true;
                        }
                        else
                        {
                            contat.IsPrimary = false;
                        }
                    }
                    if (flag)
                    {
                        database.UpdateMany<ReferralEmergencyContact>(emergencyContacts);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public ReferralEmergencyContact GetFirstEmergencyContactByReferral(Guid agencyId, Guid referralId)
        {
            var emergencyContact = new ReferralEmergencyContact();
            var script = string.Format(@"SELECT 
                                Id as Id,
                                FirstName as FirstName,
                                LastName as LastName ,
                                Relationship as Relationship ,
                                PrimaryPhone as PrimaryPhone ,
                                AlternatePhone as AlternatePhone ,
                                EmailAddress as EmailAddress 
                                FROM
                                    referralemergencycontacts 
                                        WHERE 
                                            AgencyId = @agencyId  AND 
                                            ReferralId = @referralId 
                                            Order BY IsPrimary DESC 
                                            Limit 1");
            using (var cmd = new FluentCommand<ReferralEmergencyContact>(script))
            {
                emergencyContact = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("referralId", referralId)
                    .SetMap(reader => new ReferralEmergencyContact
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        Relationship = reader.GetStringNullable("Relationship"),
                        PrimaryPhone = reader.GetStringNullable("PrimaryPhone"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        AlternatePhone = reader.GetStringNullable("AlternatePhone")
                    })
                    .AsSingle();
            }
            return emergencyContact;
        }

        #endregion
    }
}
