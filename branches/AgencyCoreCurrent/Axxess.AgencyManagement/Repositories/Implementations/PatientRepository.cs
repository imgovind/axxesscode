﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Transactions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using AutoMapper;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class PatientRepository : IPatientRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PatientRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IPatientRepository Members

        public string LastPatientId(Guid agencyId)
        {
            Patient patient = null;
            var query = new QueryBuilder("SELECT PatientIdNumber FROM patients").Where("patients.AgencyId = @agencyid").OrderBy("Created", false).LimitTo(0, 1);

            using (var cmd = new FluentCommand<Patient>(query.Build()))
            {
                patient = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Patient
                {
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsSingle();
            }

            if (patient != null && patient.PatientIdNumber.IsNotNullOrEmpty())
            {
                return patient.PatientIdNumber.Trim();
            }

            return string.Empty;
        }

        public bool Delete(Guid agencyId, Guid Id)
        {
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == Id);
                if (patient != null)
                {
                    database.Delete<Patient>(patient.Id);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool DeprecatedPatient(Guid agencyId, Guid Id)
        {
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == Id);
                if (patient != null)
                {
                    patient.IsDeprecated = true;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public IList<Patient> GetAllByAgencyId(Guid agencyId)
        {
            var patients = database.Find<Patient>(p => p.AgencyId == agencyId && p.IsDeprecated == false);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    p.EmergencyContacts = GetEmergencyContacts(agencyId, p.Id);
                    p.PhysicianContacts = GetPatientPhysicians(p.Id);
                    p.Physician = p.PhysicianContacts.SingleOrDefault(pc => pc.Primary);
                    p.Branch = GetBranchName(agencyId, p.AgencyLocationId);
                });
            }
            return patients.OrderBy(p => p.LastName).ToList();
        }

        public IList<PatientData> All(Guid agencyId, Guid branchId, int status)
        {
            //var query = new QueryBuilder("SELECT Id, PatientIdNumber, FirstName, LastName, MiddleInitial, DOB, Gender, Status, " +
            //    "AddressLine1, AddressLine2, AddressCity, AddressStateCode, AddressZipCode, PhoneHome FROM patients");
            //query.Where("patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 ").OrderBy("patients.LastName", true);
            var query = string.Format("SELECT Id, PatientIdNumber, PrimaryHealthPlanId, MedicareNumber, PrimaryInsurance, FirstName, LastName, MiddleInitial, DOB, Gender, Status, AddressLine1, AddressLine2, AddressCity, AddressStateCode, AddressZipCode, PhoneHome, PhoneMobile , EmailAddress, IsDNR  FROM patients Where patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 {0} {1} ", !branchId.IsEmpty() ? " AND AgencyLocationId = @branchId " : string.Empty, status == 0 ? " " : " AND Status = " + status);
            var list = new List<PatientData>();
            using (var cmd = new FluentCommand<PatientData>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new PatientData
                {
                    Id = reader.GetGuid("Id"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PhoneMobile = reader.GetStringNullable("PhoneMobile").ToPhone(),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsDNR = reader.GetBoolean("IsDNR")
                })
                .AsList();
            }
            return list;
        }

        public IList<PatientData> AllDeleted(Guid agencyId, Guid branchId)
        {
            var query = string.Format("SELECT Id, PatientIdNumber ,PrimaryHealthPlanId, MedicareNumber, PrimaryInsurance, FirstName, LastName, MiddleInitial, DOB, Gender, Status, AddressLine1, AddressLine2, AddressCity, AddressStateCode, AddressZipCode, PhoneHome, PhoneMobile , EmailAddress, IsDNR  FROM patients Where patients.AgencyId = @agencyid AND patients.IsDeprecated = 1 {0} ", !branchId.IsEmpty() ? " AND AgencyLocationId = @branchId " : string.Empty);
            var list = new List<PatientData>();
            using (var cmd = new FluentCommand<PatientData>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new PatientData
                {
                    Id = reader.GetGuid("Id"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    StatusId = reader.GetInt("Status"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PhoneMobile = reader.GetStringNullable("PhoneMobile").ToPhone(),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    DateOfBirth = reader.GetDateTime("DOB"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsDNR = reader.GetBoolean("IsDNR")
                })
                .AsList();
            }
            return list;
        }

        //Do not use it . It is not agency specific
        public IList<Patient> All()
        {
            return database.All<Patient>().ToList();
        }

        public List<PatientSelection> GetPatientSelection(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name)
        {
            var query = new QueryBuilder("SELECT patients.Id, patients.PatientIdNumber, patients.FirstName, patients.LastName, patients.MiddleInitial FROM patients");
            query.Where("patients.AgencyId = @agencyid").And("patients.Status = @statusid");

            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId ");
            }

            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("(patients.FirstName like '%{0}%' OR patients.LastName like '%{0}%')", name));
            }
            if (paymentSourceId > 0)
            {
                query.And(string.Format("patients.PaymentSource like '%{0};%'", paymentSourceId));
            }
            query.And("patients.IsDeprecated = 0").OrderBy("patients.LastName", true);

            return new FluentCommand<PatientSelection>(query.Build())
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("statusid", statusId)
                .AddInt("paymentsourceid", paymentSourceId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new PatientSelection
                {
                    Id = reader.GetGuid("Id"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                    LastName = reader.GetString("LastName").Trim().ToUpper(),
                    MI = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
        }

        public List<PatientSelection> GetPatientSelection(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name, Guid userId)
        {
            var query = new QueryBuilder("SELECT patients.Id, patients.PatientIdNumber, patients.FirstName, patients.LastName, patients.MiddleInitial FROM patients");
            query.Where("patients.AgencyId = @agencyid").And("patients.Status = @statusid");

            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId ");
            }

            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("(patients.FirstName like '%{0}%' OR patients.LastName like '%{0}%')", name));
            }
            if (paymentSourceId > 0)
            {
                query.And(string.Format("patients.PaymentSource like '%{0};%'", paymentSourceId));
            }
            if (!userId.IsEmpty())
            {
                query.And(string.Format("patients.UserId = '{0}' OR patients.CaseManagerId = '{0}'", userId));
            }
            query.And("patients.IsDeprecated = 0").OrderBy("patients.LastName", true);

            return new FluentCommand<PatientSelection>(query.Build())
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("statusid", statusId)
                .AddInt("paymentsourceid", paymentSourceId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new PatientSelection
                {
                    Id = reader.GetGuid("Id"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                    LastName = reader.GetString("LastName").Trim().ToUpper(),
                    MI = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
        }

        public List<PatientSelection> GetAuditorPatientSelection(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name, Guid userId)
        {
            var query = new QueryBuilder("SELECT patients.Id, patients.PatientIdNumber, patients.FirstName, patients.LastName, patients.MiddleInitial FROM patients");
            query.Where("patients.AgencyId = @agencyid").And("patients.Status = @statusid");

            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId ");
            }

            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("(patients.FirstName like '%{0}%' OR patients.LastName like '%{0}%')", name));
            }
            if (paymentSourceId > 0)
            {
                query.And(string.Format("patients.PaymentSource like '%{0};%'", paymentSourceId));
            }
            if (!userId.IsEmpty())
            {
                query.And(string.Format("patients.AuditorId = '{0}'", userId));
            }
            query.And("patients.IsDeprecated = 0").OrderBy("patients.LastName", true);

            return new FluentCommand<PatientSelection>(query.Build())
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("statusid", statusId)
                .AddInt("paymentsourceid", paymentSourceId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new PatientSelection
                {
                    Id = reader.GetGuid("Id"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                    LastName = reader.GetString("LastName").Trim().ToUpper(),
                    MI = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
        }

        public List<PatientSelection> GetPatientSelectionMedicare(Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name)
        {
            var list = new List<PatientSelection>();
            var query = new QueryBuilder("SELECT patients.Id, patients.FirstName, patients.LastName, patients.MiddleInitial FROM  patients");
            query.Where("patients.AgencyId = @agencyid").And("patients.Status = @statusid");
            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId ");
            }
            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("(patients.FirstName like '%{0}%' OR patients.LastName like '%{0}%')", name));
            }
            if (paymentSourceId > 0)
            {
                if (paymentSourceId > 0 && paymentSourceId < 1000)
                {
                    query.And("(patients.PrimaryInsurance = @paymentSourceId OR patients.SecondaryInsurance = @paymentSourceId OR patients.TertiaryInsurance = @paymentSourceId )");
                }
                else
                {
                    query.And("patients.PrimaryInsurance = @paymentSourceId");
                }
            }
            else
            {
                return list;
            }

            query.And("patients.IsDeprecated = 0").OrderBy("patients.LastName", true);

            using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("statusid", statusId)
                 .Add("paymentsourceid", paymentSourceId)
                 .SetMap(reader => new PatientSelection
                 {
                     Id = reader.GetGuid("Id"),
                     FirstName = reader.GetString("FirstName").ToUpper(),
                     LastName = reader.GetString("LastName").ToUpper(),
                     MI = reader.GetStringNullable("MiddleInitial").ToUpper()
                 })
                 .AsList();
            }

            return list;
        }

        public List<PatientSelection> GetPatientSelectionAllInsurance(Guid agencyId, Guid branchId, int statusId, string name, int insurance)
        {
            var list = new List<PatientSelection>();
            var query = new QueryBuilder("SELECT patients.Id, patients.FirstName, patients.LastName, patients.MiddleInitial FROM  patients");
            query.Where("patients.AgencyId = @agencyid").And("patients.Status = @statusid");
            //if (insurance > 0)
            //{
            //    query.And("(patients.PrimaryInsurance = @insurnace ||patients.SecondaryInsurance = @insurnace || patients.TertiaryInsurance = @insurnace )");
            //}

            if (insurance < 0)
            {
                return list;
            }
            else if (insurance == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            query.And(string.Format("( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", location.Payor));
                        }
                        else
                        {
                            query.And("( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ");
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            query.And(string.Format("( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", agency.Payor));
                        }
                        else
                        {
                            query.And("( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ");
                        }
                    }
                }
            }
            else
            {
                query.And(string.Format("( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} ) ", insurance));
            }


            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId ");
            }
            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("(patients.FirstName like '%{0}%' OR patients.LastName like '%{0}%')", name));
            }
            query.And("patients.IsDeprecated = 0").OrderBy("patients.LastName", true);
            using (var cmd = new FluentCommand<PatientSelection>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", statusId)
                    // .AddInt("insurnace", insurance)
                .SetMap(reader => new PatientSelection
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetString("FirstName").ToUpper(),
                    LastName = reader.GetString("LastName").ToUpper(),
                    MI = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
            }
            return list;
        }

        public int GetPatientStatusCount(Guid agencyId, int statusId)
        {
            var script = @"SELECT count(*) FROM patients WHERE patients.AgencyId = @agencyid AND patients.Status = @statusid AND patients.IsDeprecated = 0";
            return new FluentCommand<int>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId).AddInt("statusid", statusId).AsScalar();
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid admissionId)
        {
            return database.Single<PatientAdmissionDate>(a => a.AgencyId == agencyId && a.Id == admissionId);
        }

        public PatientAdmissionDate GetPatientLatestAdmissionDate(Guid agencyId, Guid patientId, DateTime date)
        {
            PatientAdmissionDate managedDate = null;
            var script = @"SELECT * FROM patientadmissiondates WHERE patientadmissiondates.AgencyId = @agencyId AND patientadmissiondates.PatientId = @patientId  AND patientadmissiondates.StartOfCareDate <= @date AND  patientadmissiondates.IsDeprecated = 0  ORDER BY patientadmissiondates.StartOfCareDate DESC LIMIT 1";
            using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
            {
                managedDate = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddDateTime("date", date)
                .SetMap(reader => new PatientAdmissionDate
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    DischargedDate = reader.GetDateTime("DischargedDate"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }

            return managedDate;
        }

        public List<PendingPatient> GetPendingByAgencyId(Guid agencyId)
        {
            var pending = new List<PendingPatient>();
            var patients = database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == (int)PatientStatus.Pending && p.IsDeprecated == false);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    pending.Add(new PendingPatient
                    {
                        Id = p.Id,
                        Created = p.Created,
                        DisplayName = p.DisplayName,
                        Type = NonAdmitTypes.Patient,
                        PatientIdNumber = p.PatientIdNumber,
                        PrimaryInsurance = p.PrimaryInsurance,
                        Branch = GetBranchName(agencyId, p.AgencyLocationId),

                    });
                });
            }

            return pending.OrderBy(p => p.DisplayName).ToList();
        }

        public List<PatientHospitalizationData> GetHospitalizedPatients(Guid agencyId)
        {
            var list = new List<PatientHospitalizationData>();
            var script = @"SELECT patients.FirstName, patients.LastName, patients.PatientIdNumber, hospitalizationlogs.HospitalizationDate, hospitalizationlogs.SourceId, " +
                "hospitalizationlogs.Id, hospitalizationlogs.LastHomeVisitDate, hospitalizationlogs.UserId, hospitalizationlogs.PatientId FROM `patients` " +
                "INNER JOIN hospitalizationlogs ON patients.HospitalizationId = hospitalizationlogs.Id WHERE patients.AgencyId = @agencyid " +
                "AND patients.IsHospitalized = 1 AND patients.IsDeprecated = 0 AND hospitalizationlogs.IsDeprecated = 0";

            using (var cmd = new FluentCommand<PatientHospitalizationData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientHospitalizationData
                {
                    Id = reader.GetGuid("Id"),
                    UserId = reader.GetGuid("UserId"),
                    SourceId = reader.GetInt("SourceId"),
                    PatientId = reader.GetGuid("PatientId"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    LastHomeVisitDate = reader.GetDateTime("LastHomeVisitDate") != DateTime.MinValue ? reader.GetDateTime("LastHomeVisitDate").ToString("MM/dd/yyyy") : string.Empty,
                    HospitalizationDate = reader.GetDateTime("HospitalizationDate") != DateTime.MinValue ? reader.GetDateTime("HospitalizationDate").ToString("MM/dd/yyyy") : string.Empty
                })
                .AsList();
            }
            return list;
        }

        public string GetBranchName(Guid agencyId, Guid locationId)
        {
            var location = database.Single<AgencyLocation>(l => l.Id == locationId && l.AgencyId == agencyId && l.IsDeprecated == false);
            if (location != null)
            {
                return location.Name;
            }
            return string.Empty;
        }

        public IList<Patient> FindPatientOnly(int statusId, Guid agencyId)
        {
            return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.IsDeprecated == false);
        }

        public IList<Patient> Find(int statusId, Guid branchCode, Guid agencyId)
        {
            if (statusId == 0 && branchCode.IsEmpty())
            {
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.IsDeprecated == false);
            }
            if (branchCode.IsEmpty() && statusId > 0)
            {
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.IsDeprecated == false);
            }
            if (!branchCode.IsEmpty() && statusId == 0)
            {
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.AgencyLocationId == branchCode && p.IsDeprecated == false);
            }
            return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.AgencyLocationId == branchCode && p.IsDeprecated == false);
        }

        public IList<Patient> FindByUser(int statusId, Guid branchCode, Guid agencyId, Guid userId)
        {
            if (userId.IsEmpty())
            {
                if (statusId == 0 && branchCode.IsEmpty())
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.IsDeprecated == false);
                }
                if (branchCode.IsEmpty() && statusId > 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.IsDeprecated == false);
                }
                if (!branchCode.IsEmpty() && statusId == 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.AgencyLocationId == branchCode && p.IsDeprecated == false);
                }
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.AgencyLocationId == branchCode && p.IsDeprecated == false);
            }
            else
            {
                if (statusId == 0 && branchCode.IsEmpty())
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.UserId == userId && p.IsDeprecated == false);
                }
                if (branchCode.IsEmpty() && statusId > 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.UserId == userId && p.IsDeprecated == false);
                }
                if (!branchCode.IsEmpty() && statusId == 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.AgencyLocationId == branchCode && p.UserId == userId && p.IsDeprecated == false);
                }
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.AgencyLocationId == branchCode && p.UserId == userId && p.IsDeprecated == false);
            }
        }

        public IList<Patient> FindByCaseManager(Guid agencyId, Guid branchCode, int statusId, Guid caseManagerId)
        {
            if (caseManagerId.IsEmpty())
            {
                if (statusId == 0 && branchCode.IsEmpty())
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.IsDeprecated == false);
                }
                if (branchCode.IsEmpty() && statusId > 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.IsDeprecated == false);
                }
                if (!branchCode.IsEmpty() && statusId == 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.AgencyLocationId == branchCode && p.IsDeprecated == false);
                }
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.AgencyLocationId == branchCode && p.IsDeprecated == false);
            }
            else
            {
                if (statusId == 0 && branchCode.IsEmpty())
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.CaseManagerId == caseManagerId && p.IsDeprecated == false);
                }
                if (branchCode.IsEmpty() && statusId > 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.CaseManagerId == caseManagerId && p.IsDeprecated == false);
                }
                if (!branchCode.IsEmpty() && statusId == 0)
                {
                    return database.Find<Patient>(p => p.AgencyId == agencyId && p.AgencyLocationId == branchCode && p.CaseManagerId == caseManagerId && p.IsDeprecated == false);
                }
                return database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == statusId && p.AgencyLocationId == branchCode && p.CaseManagerId == caseManagerId && p.IsDeprecated == false);
            }
        }

        public bool Add(Patient patient, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (patient != null)
                {
                    patient.IsDeprecated = false;
                    patient.Created = DateTime.Now;
                    patient.Modified = DateTime.Now;
                    //patient.PrimaryPharmacyId = Guid.NewGuid();
                    database.Add<Patient>(patient);
                    patientOut = patient;
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AdmitPatient(PendingPatient pending, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (pending != null)
                {
                    if (pending.Type == NonAdmitTypes.Patient)
                    {
                        var patient = database.Single<Patient>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId && p.IsDeprecated == false);
                        if (patient != null)
                        {
                            patient.AdmissionId = pending.AdmissionId;
                            patient.PatientIdNumber = pending.PatientIdNumber;
                            patient.StartofCareDate = pending.StartofCareDate;
                            patient.ReferralDate = pending.ReferralDate;
                            patient.CaseManagerId = pending.CaseManagerId;
                            patient.UserId = pending.UserId;
                            patient.AgencyLocationId = pending.AgencyLocationId;
                            patient.PrimaryInsurance = pending.PrimaryInsurance;
                            patient.SecondaryInsurance = pending.SecondaryInsurance;
                            patient.TertiaryInsurance = pending.TertiaryInsurance;
                            patient.Payer = pending.Payer;
                            patient.ShouldCreateEpisode = pending.ShouldCreateEpisode;
                            patient.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;

                            patient.Status = (int)PatientStatus.Active;
                            patient.Modified = DateTime.Now;

                            database.Update<Patient>(patient);
                            
                            patientOut = patient;
                            patientOut.EpisodeStartDate = pending.EpisodeStartDate;
                            result = true;
                        }
                    }
                    else
                    {
                        var referral = database.Single<Referral>(r => r.Id == pending.Id && r.AgencyId == pending.AgencyId);
                        if (referral != null)
                        {
                            var patient = new Patient
                            {
                                Id = referral.Id,
                                PatientIdNumber = pending.PatientIdNumber,
                                AdmissionId = pending.AdmissionId,
                                AgencyId = referral.AgencyId,
                                AgencyLocationId = pending.AgencyLocationId,
                                AdmissionSource = referral.AdmissionSource.ToString(),
                                ReferrerPhysician = referral.ReferrerPhysician,
                                OtherReferralSource = referral.OtherReferralSource,
                                InternalReferral = referral.InternalReferral,
                                FirstName = referral.FirstName,
                                MiddleInitial = referral.MiddleInitial,
                                LastName = referral.LastName,
                                Gender = referral.Gender,
                                DOB = referral.DOB,
                                MaritalStatus = referral.MaritalStatus,
                                Height = referral.Height,
                                HeightMetric = referral.HeightMetric,
                                Weight = referral.Weight,
                                WeightMetric = referral.WeightMetric,
                                UserId = pending.UserId,
                                CaseManagerId = pending.CaseManagerId,
                                MedicareNumber = referral.MedicareNumber,
                                MedicaidNumber = referral.MedicaidNumber,
                                SSN = referral.SSN,
                                AddressLine1 = referral.AddressLine1,
                                AddressLine2 = referral.AddressLine2,
                                AddressCity = referral.AddressCity,
                                AddressStateCode = referral.AddressStateCode,
                                AddressZipCode = referral.AddressZipCode,
                                PhoneHome = referral.PhoneHome,
                                EmailAddress = referral.EmailAddress,
                                ServicesRequired = referral.ServicesRequired,
                                Ethnicities = string.Empty,
                                DME = referral.DME,
                                OtherDME = referral.OtherDME,
                                Comments = referral.Comments,
                                StartofCareDate = pending.StartofCareDate,
                                ReferralDate = pending.ReferralDate,
                                PrimaryInsurance = pending.PrimaryInsurance,
                                SecondaryInsurance = pending.SecondaryInsurance,
                                TertiaryInsurance = pending.TertiaryInsurance,
                                Payer = pending.Payer,
                                Status = (int)PatientStatus.Active,
                                Modified = DateTime.Now
                            };
                            database.Add<Patient>(patient);
                            patientOut = patient;

                            referral.Status = (int)ReferralStatus.Admitted;
                            database.Update<Referral>(referral);
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (pending != null)
                {
                    var patient = database.Single<Patient>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId && p.IsDeprecated == false);
                    if (patient != null)
                    {
                        patient.NonAdmissionDate = pending.NonAdmitDate;
                        patient.NonAdmissionReason = pending.Reason;
                        patient.Comments = pending.Comments;
                        patient.Status = (int)PatientStatus.NonAdmission;
                        patient.AdmissionId = pending.AdmissionId;
                        patient.Modified = DateTime.Now;

                        database.Update<Patient>(patient);
                        patientOut = patient;
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AddPatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            var result = false;
            try
            {
                if (managedDate != null)
                {
                    managedDate.Created = DateTime.Now;
                    managedDate.Modified = DateTime.Now;
                    database.Add<PatientAdmissionDate>(managedDate);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdatePatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            var result = false;
            var managedDateToEdit = database.Single<PatientAdmissionDate>(s => s.AgencyId == managedDate.AgencyId && s.PatientId == managedDate.PatientId && s.Id == managedDate.Id);
            try
            {
                if (managedDateToEdit != null && managedDate != null)
                {
                    managedDateToEdit.StartOfCareDate = managedDate.StartOfCareDate;
                    managedDateToEdit.DischargedDate = managedDate.DischargedDate;
                    managedDateToEdit.PatientData = managedDate.PatientData;
                    managedDateToEdit.Status = managedDate.Status;
                    managedDateToEdit.Reason = managedDate.Reason;
                    managedDateToEdit.DischargeReasonId = managedDate.DischargeReasonId;
                    managedDateToEdit.Modified = DateTime.Now;
                    database.Update<PatientAdmissionDate>(managedDateToEdit);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdatePatientAdmissionDateModal(PatientAdmissionDate managedDate)
        {
            var result = false;
            if (managedDate != null)
            {
                try
                {
                    managedDate.Modified = DateTime.Now;
                    database.Update<PatientAdmissionDate>(managedDate);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            return database.Single<PatientAdmissionDate>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.Id == Id);
        }

        public IList<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            var dates = database.Find<PatientAdmissionDate>(d => d.AgencyId == agencyId && d.PatientId == patientId && d.IsDeprecated == false);
            return dates != null ? dates.OrderByDescending(d => d.StartOfCareDate.Date).ToList() : new List<PatientAdmissionDate>();
        }

        public bool DeprecatedPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            var managedDate = database.Single<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId && s.Id == Id);
            try
            {
                if (managedDate != null)
                {
                    managedDate.IsDeprecated = true;
                    managedDate.Modified = DateTime.Now;
                    database.Update<PatientAdmissionDate>(managedDate);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeletePatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            var managedDate = database.Single<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId && s.Id == Id);
            try
            {
                if (managedDate != null)
                {
                    database.Delete<PatientAdmissionDate>(managedDate.Id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeletePatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            var result = false;
            var managedDates = database.Find<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId);
            try
            {
                if (managedDates != null && managedDates.Count > 0)
                {
                    database.DeleteMany<PatientAdmissionDate>(managedDates);
                    result = true;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeprecatedPatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            var result = false;
            var managedDates = database.Find<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId);
            try
            {
                if (managedDates != null && managedDates.Count > 0)
                {
                    managedDates.ForEach(m => { m.IsDeprecated = true; m.Modified = DateTime.Now; });
                    database.UpdateMany<PatientAdmissionDate>(managedDates);
                    result = true;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DischargePatient(Guid agencyId, Guid patientId, DateTime dischargeDate, int dischargeReasonId, string dischargeReason)
        {
            var result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    patient.DischargeDate = dischargeDate;
                    patient.Status = (int)PatientStatus.Discharged;
                    patient.DischargeReasonId = dischargeReasonId;
                    patient.DischargeReason = dischargeReason;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool Update(Patient patient, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (patient != null)
                {
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    patientOut = patient;
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool Update(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    patient.Modified = DateTime.Now;
                    this.database.Update<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool Edit(Patient patient, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (patient != null)
                {
                    var patientToEdit = database.Single<Patient>(p => p.Id == patient.Id && p.AgencyId == patient.AgencyId);
                    //var pharmacy = database.Single<AgencyPharmacy>(h => h.Id == patient.PrimaryPharmacyId && h.AgencyId == patient.AgencyId);
                    if (patientToEdit != null)
                    {
                        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
                        {
                            patientToEdit.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
                            patientToEdit.PrimaryGroupName = patient.PrimaryGroupName;
                            patientToEdit.PrimaryGroupId = patient.PrimaryGroupId;
                            patientToEdit.PrimaryRelationship = patient.PrimaryRelationship;
                        }
                        else patientToEdit.PrimaryHealthPlanId = patientToEdit.PrimaryGroupName = patientToEdit.PrimaryGroupId = string.Empty;

                        if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
                        {
                            patientToEdit.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
                            patientToEdit.SecondaryGroupName = patient.SecondaryGroupName;
                            patientToEdit.SecondaryGroupId = patient.SecondaryGroupId;
                            patientToEdit.SecondaryRelationship = patient.SecondaryRelationship;
                        }
                        else patientToEdit.SecondaryHealthPlanId = patientToEdit.SecondaryGroupName = patientToEdit.SecondaryGroupId = string.Empty;

                        if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
                        {
                            patientToEdit.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
                            patientToEdit.TertiaryGroupName = patient.TertiaryGroupName;
                            patientToEdit.TertiaryGroupId = patient.TertiaryGroupId;
                            patientToEdit.TertiaryRelationship = patient.TertiaryRelationship;
                        }
                        else patientToEdit.TertiaryHealthPlanId = patientToEdit.TertiaryGroupName = patientToEdit.TertiaryGroupId = string.Empty;

                        patientToEdit.AgencyLocationId = patient.AgencyLocationId;
                        patientToEdit.FirstName = patient.FirstName;
                        patientToEdit.LastName = patient.LastName;
                        patientToEdit.MiddleInitial = patient.MiddleInitial;
                        patientToEdit.Gender = patient.Gender;
                        patientToEdit.DOB = patient.DOB;
                        patientToEdit.MaritalStatus = patient.MaritalStatus;
                        patientToEdit.Height = patient.Height;
                        patientToEdit.HeightMetric = patient.HeightMetric;
                        patientToEdit.Weight = patient.Weight;
                        patientToEdit.WeightMetric = patient.WeightMetric;
                        patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                        patientToEdit.MedicareNumber = patient.MedicareNumber;
                        patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                        patientToEdit.SSN = patient.SSN;
                        patientToEdit.IsDNR = patient.IsDNR;
                        patientToEdit.StartofCareDate = patient.StartofCareDate;
                        if (patient.Status == (int)PatientStatus.Discharged)
                        {
                            patientToEdit.DischargeDate = patient.DischargeDate;
                            patientToEdit.DischargeReason = patient.DischargeReason;
                            patientToEdit.DischargeReasonId = patient.DischargeReasonId;
                        }
                        patientToEdit.Ethnicities = patient.Ethnicities;
                        patientToEdit.PaymentSource = patient.PaymentSource;
                        patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                        patientToEdit.AddressLine1 = patient.AddressLine1;
                        patientToEdit.AddressLine2 = patient.AddressLine2;
                        patientToEdit.AddressCity = patient.AddressCity;
                        patientToEdit.AddressStateCode = patient.AddressStateCode;
                        patientToEdit.AddressZipCode = patient.AddressZipCode;
                        patientToEdit.EvacuationAddressLine1 = patient.EvacuationAddressLine1;
                        patientToEdit.EvacuationAddressLine2 = patient.EvacuationAddressLine2;
                        patientToEdit.EvacuationAddressCity = patient.EvacuationAddressCity;
                        patientToEdit.EvacuationAddressStateCode = patient.EvacuationAddressStateCode;
                        patientToEdit.EvacuationAddressZipCode = patient.EvacuationAddressZipCode;
                        patientToEdit.PhoneHome = patient.PhoneHome;
                        patientToEdit.PhoneMobile = patient.PhoneMobile;
                        patientToEdit.EvacuationPhoneHome = patient.EvacuationPhoneHome;
                        patientToEdit.EvacuationPhoneMobile = patient.EvacuationPhoneMobile;
                        patientToEdit.EmailAddress = patient.EmailAddress;
                        patientToEdit.PrimaryPharmacyId = patient.PrimaryPharmacyId;
                        patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                        patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                        patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                        patientToEdit.AdmissionSource = patient.AdmissionSource;
                        patientToEdit.ReferralDate = patient.ReferralDate;
                        patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                        patientToEdit.InternalReferral = patient.InternalReferral;
                        patientToEdit.OtherReferralSource = patient.OtherReferralSource;

                        patientToEdit.ServicesRequired = patient.ServicesRequired;
                        patientToEdit.DME = patient.DME;
                        patientToEdit.Payer = patient.Payer;
                        patientToEdit.Triage = patient.Triage;
                        patientToEdit.EvacuationZone = patient.EvacuationZone;
                        patientToEdit.Comments = patient.Comments;
                        patientToEdit.CaseManagerId = patient.CaseManagerId;
                        patientToEdit.AuditorId = patient.AuditorId;
                        patientToEdit.UserId = patient.UserId;
                        patientToEdit.AdmissionId = patient.AdmissionId;
                        patientToEdit.ServiceLocation = patient.ServiceLocation;
                        patientToEdit.Modified = DateTime.Now;
                        database.Update<Patient>(patientToEdit);
                        patientOut = patientToEdit;
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool PatientAdmissionEdit(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    var admissionToEdit = database.Single<PatientAdmissionDate>(p => p.AgencyId == patient.AgencyId && p.Id == patient.AdmissionId && p.PatientId == patient.Id);

                    if (admissionToEdit != null)
                    {
                        var patientToEdit = admissionToEdit.PatientData.ToObject<Patient>() ?? new Patient();
                        if (patientToEdit != null)
                        {
                            patientToEdit.FirstName = patient.FirstName;
                            patientToEdit.MiddleInitial = patient.MiddleInitial;
                            patientToEdit.LastName = patient.LastName;
                            patientToEdit.Gender = patient.Gender;
                            patientToEdit.DOB = patient.DOB;
                            patientToEdit.MaritalStatus = patient.MaritalStatus;
                            patientToEdit.Height = patient.Height;
                            patientToEdit.HeightMetric = patient.HeightMetric;
                            patientToEdit.Weight = patient.Weight;
                            patientToEdit.WeightMetric = patient.WeightMetric;
                            patientToEdit.AgencyLocationId = patient.AgencyLocationId;
                            patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                            patientToEdit.MedicareNumber = patient.MedicareNumber;
                            patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                            patientToEdit.SSN = patient.SSN;
                            patientToEdit.StartofCareDate = patient.StartofCareDate;
                            patientToEdit.DischargeDate = patient.DischargeDate;
                            patientToEdit.CaseManagerId = patient.CaseManagerId;
                            patientToEdit.UserId = patient.UserId;
                            patientToEdit.Ethnicities = patient.Ethnicities;
                            patientToEdit.PaymentSource = patient.PaymentSource;
                            patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                            patientToEdit.AddressLine1 = patient.AddressLine1;
                            patientToEdit.AddressLine2 = patient.AddressLine2;
                            patientToEdit.AddressCity = patient.AddressCity;
                            patientToEdit.AddressStateCode = patient.AddressStateCode;
                            patientToEdit.AddressZipCode = patient.AddressZipCode;
                            patientToEdit.PhoneHome = patient.PhoneHome;
                            patientToEdit.PhoneMobile = patient.PhoneMobile;
                            patientToEdit.EvacuationAddressLine1 = patient.EvacuationAddressLine1;
                            patientToEdit.EvacuationAddressLine2 = patient.EvacuationAddressLine2;
                            patientToEdit.EvacuationAddressCity = patient.EvacuationAddressCity;
                            patientToEdit.EvacuationAddressStateCode = patient.EvacuationAddressStateCode;
                            patientToEdit.EvacuationAddressZipCode = patient.EvacuationAddressZipCode;
                            patientToEdit.EvacuationPhoneHome = patient.EvacuationPhoneHome;
                            patientToEdit.EvacuationPhoneMobile = patient.EvacuationPhoneMobile;
                            patientToEdit.EmailAddress = patient.EmailAddress;
                            patientToEdit.PharmacyName = patient.PharmacyName;
                            patientToEdit.PharmacyPhone = patient.PharmacyPhone;
                            patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                            if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
                            {
                                patientToEdit.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
                                patientToEdit.PrimaryGroupName = patient.PrimaryGroupName;
                                patientToEdit.PrimaryGroupId = patient.PrimaryGroupId;
                            }
                            patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                            if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
                            {
                                patientToEdit.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
                                patientToEdit.SecondaryGroupName = patient.SecondaryGroupName;
                                patientToEdit.SecondaryGroupId = patient.SecondaryGroupId;
                            }
                            patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                            if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
                            {
                                patientToEdit.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
                                patientToEdit.TertiaryGroupName = patient.TertiaryGroupName;
                                patientToEdit.TertiaryGroupId = patient.TertiaryGroupId;
                            }
                            patientToEdit.Triage = patient.Triage;
                            patientToEdit.EvacuationZone = patient.EvacuationZone;
                            patientToEdit.ServicesRequired = patient.ServicesRequired;
                            patientToEdit.DME = patient.DME;
                            patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                            patientToEdit.AdmissionSource = patient.AdmissionSource;
                            patientToEdit.OtherReferralSource = patient.OtherReferralSource;

                            patientToEdit.ReferralDate = patient.ReferralDate;
                            patientToEdit.InternalReferral = patient.InternalReferral;
                            patientToEdit.IsDNR = patient.IsDNR;
                            patientToEdit.Comments = patient.Comments;
                            patientToEdit.DischargeReasonId = patient.DischargeReasonId;
                            patientToEdit.DischargeReason = patient.DischargeReason;
                            patientToEdit.AdmissionId = patient.AdmissionId;
                            patientToEdit.Modified = DateTime.Now;
                            admissionToEdit.PatientData = patientToEdit.ToXml();
                            admissionToEdit.DischargeReasonId = patient.DischargeReasonId;
                            admissionToEdit.Reason = patient.DischargeReason;
                            admissionToEdit.StartOfCareDate = patient.StartofCareDate;
                            admissionToEdit.DischargedDate = patient.DischargeDate;
                            database.Update<PatientAdmissionDate>(admissionToEdit);
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool PatientAdmissionAdd(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    var admissionToAdd = new PatientAdmissionDate { Id = Guid.NewGuid(), PatientId = patient.Id, AgencyId = patient.AgencyId, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, Created = DateTime.Now, Modified = DateTime.Now, IsDeprecated = false, IsActive = true, Status = patient.Status };
                    var patientToEdit = database.Single<Patient>(p => p.AgencyId == patient.AgencyId && p.Id == patient.Id) ?? new Patient();
                    if (patientToEdit != null)
                    {
                        patientToEdit.FirstName = patient.FirstName;
                        patientToEdit.MiddleInitial = patient.MiddleInitial;
                        patientToEdit.LastName = patient.LastName;
                        patientToEdit.Gender = patient.Gender;
                        patientToEdit.DOB = patient.DOB;
                        patientToEdit.IsDNR = patient.IsDNR;
                        patientToEdit.MaritalStatus = patient.MaritalStatus;
                        patientToEdit.Height = patient.Height;
                        patientToEdit.HeightMetric = patient.HeightMetric;
                        patientToEdit.Weight = patient.Weight;
                        patientToEdit.WeightMetric = patient.WeightMetric;
                        patientToEdit.AgencyLocationId = patient.AgencyLocationId;
                        patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                        patientToEdit.MedicareNumber = patient.MedicareNumber;
                        patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                        patientToEdit.SSN = patient.SSN;
                        patientToEdit.StartofCareDate = patient.StartofCareDate;
                        patientToEdit.DischargeDate = patient.DischargeDate;
                        patientToEdit.DischargeReasonId = patient.DischargeReasonId;
                        patientToEdit.DischargeReason = patient.DischargeReason;
                        patientToEdit.CaseManagerId = patient.CaseManagerId;
                        patientToEdit.UserId = patient.UserId;
                        patientToEdit.Ethnicities = patient.Ethnicities;
                        patientToEdit.PaymentSource = patient.PaymentSource;
                        patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                        patientToEdit.AddressLine1 = patient.AddressLine1;
                        patientToEdit.AddressLine2 = patient.AddressLine2;
                        patientToEdit.AddressCity = patient.AddressCity;
                        patientToEdit.AddressStateCode = patient.AddressStateCode;
                        patientToEdit.AddressZipCode = patient.AddressZipCode;
                        patientToEdit.PhoneHome = patient.PhoneHome;
                        patientToEdit.PhoneMobile = patient.PhoneMobile;
                        patientToEdit.EvacuationAddressLine1 = patient.EvacuationAddressLine1;
                        patientToEdit.EvacuationAddressLine2 = patient.EvacuationAddressLine2;
                        patientToEdit.EvacuationAddressCity = patient.EvacuationAddressCity;
                        patientToEdit.EvacuationAddressStateCode = patient.EvacuationAddressStateCode;
                        patientToEdit.EvacuationAddressZipCode = patient.EvacuationAddressZipCode;
                        patientToEdit.EvacuationPhoneHome = patient.EvacuationPhoneHome;
                        patientToEdit.EvacuationPhoneMobile = patient.EvacuationPhoneMobile;
                        patientToEdit.EmailAddress = patient.EmailAddress;
                        patientToEdit.PharmacyName = patient.PharmacyName;
                        patientToEdit.PharmacyPhone = patient.PharmacyPhone;
                        patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
                        {
                            patientToEdit.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
                            patientToEdit.PrimaryGroupName = patient.PrimaryGroupName;
                            patientToEdit.PrimaryGroupId = patient.PrimaryGroupId;
                        }
                        patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                        if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
                        {
                            patientToEdit.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
                            patientToEdit.SecondaryGroupName = patient.SecondaryGroupName;
                            patientToEdit.SecondaryGroupId = patient.SecondaryGroupId;
                        }
                        patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                        if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
                        {
                            patientToEdit.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
                            patientToEdit.TertiaryGroupName = patient.TertiaryGroupName;
                            patientToEdit.TertiaryGroupId = patient.TertiaryGroupId;
                        }
                        patientToEdit.Triage = patient.Triage;
                        patientToEdit.EvacuationZone = patient.EvacuationZone;
                        patientToEdit.ServicesRequired = patient.ServicesRequired;
                        patientToEdit.DME = patient.DME;
                        patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                        patientToEdit.AdmissionSource = patient.AdmissionSource;
                        patientToEdit.OtherReferralSource = patient.OtherReferralSource;

                        patientToEdit.ReferralDate = patient.ReferralDate;
                        patientToEdit.InternalReferral = patient.InternalReferral;
                        patientToEdit.Comments = patient.Comments;
                        patientToEdit.AdmissionId = admissionToAdd.Id;
                        admissionToAdd.PatientData = patientToEdit.ToXml();
                        admissionToAdd.StartOfCareDate = patient.StartofCareDate;
                        admissionToAdd.DischargedDate = patient.DischargeDate;
                        admissionToAdd.Reason = patient.DischargeReason;
                        admissionToAdd.DischargeReasonId = patient.DischargeReasonId;
                        database.Add<PatientAdmissionDate>(admissionToAdd);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId, out Patient patientOut)
        {
            bool result = false;
            patientOut = null;
            try
            {
                if (!patientId.IsEmpty() && !agencyId.IsEmpty() && !assetId.IsEmpty())
                {
                    var patient = database.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId && p.IsDeprecated == false);

                    if (patient != null)
                    {
                        patient.PhotoId = assetId;
                        patient.Modified = DateTime.Now;
                        database.Update<Patient>(patient);
                        patientOut = patient;
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool SetStatus(Guid agencyId, Guid patientId, PatientStatus status)
        {
            bool result = false;
            try
            {
                if (!patientId.IsEmpty())
                {
                    var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId && p.IsDeprecated == false);
                    if (patient != null)
                    {
                        patient.Status = (int)status;
                        patient.Modified = DateTime.Now;
                        database.Update<Patient>(patient);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AddEmergencyContact(PatientEmergencyContact emergencyContactInfo)
        {
            var result = false;
            try
            {
                if (emergencyContactInfo != null)
                {
                    emergencyContactInfo.Id = Guid.NewGuid();
                    if (emergencyContactInfo.PhonePrimaryArray.Count >= 2)
                    {
                        emergencyContactInfo.PrimaryPhone = emergencyContactInfo.PhonePrimaryArray.ToArray().PhoneEncode();
                    }
                    if (emergencyContactInfo.PhoneAlternateArray.Count >= 2)
                    {
                        emergencyContactInfo.AlternatePhone = emergencyContactInfo.PhoneAlternateArray.ToArray().PhoneEncode();
                    }
                    database.Add<PatientEmergencyContact>(emergencyContactInfo);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        private IList<AgencyPhysician> GetPatientPhysicians(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var physicians = new List<AgencyPhysician>();
            var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            foreach (var physician in patientPhysician)
            {
                var agencyPhysician = database.Single<AgencyPhysician>(p => p.Id == physician.PhysicianId);
                if (agencyPhysician != null)
                {
                    agencyPhysician.Primary = physician.IsPrimary;
                    physicians.Add(agencyPhysician);
                }
            }
            return physicians;
        }

        public Patient Get(Guid Id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "patientId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var patient = GetPatientOnly(Id, agencyId);
            if (patient != null)
            {
                patient.EmergencyContacts = GetEmergencyContacts(agencyId, Id);
                patient.PhysicianContacts = GetPatientPhysicians(Id);
                return patient;
            }
            return null;
        }

        public Patient GetPatientOnly(Guid Id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "patientId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == Id);
        }

        public T GetPatient<T>(Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = new Patient();
            try
            {
                Mapper.CreateMap<Patient, T>();
                patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
            }
            catch (Exception ex)
            {
                // TODO Log
                return Mapper.Map<Patient, T>(patient);
            }
            return Mapper.Map<Patient, T>(patient);
        }

        public IList<Birthday> GetPatientBirthdays(Guid agencyId, Guid branchId, int month)
        {
            var birthdays = new List<Birthday>();
            var script = string.Format(@"SELECT FirstName , LastName , MiddleInitial, PatientIdNumber , DOB , " +
              " AddressLine1, AddressLine2 , AddressCity , AddressStateCode , AddressZipCode, PhoneHome , PhoneMobile , EmailAddress   FROM patients " +
              "  WHERE  AgencyId = @agencyId  AND month( DOB )= @month " +
              " {0} AND Status = 1 AND IsDeprecated = 0 ORDER BY LastName ASC , FirstName ASC ", !branchId.IsEmpty() ? " AND AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<Birthday>(script))
            {
                birthdays = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddInt("month", month)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new Birthday
                    {
                        Name = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial") + "." : ""),
                        IdNumber = reader.GetStringNullable("PatientIdNumber"),
                        Date = reader.GetDateTime("DOB"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome"),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                    })
                    .AsList();
            }

            return birthdays;

        }

        public List<AddressBookEntry> GetPatientAddressListing(Guid agencyId, Guid branchId, int status)
        {
            var adressBookEntrys = new List<AddressBookEntry>();
            var script = string.Format(@"SELECT FirstName , LastName , MiddleInitial, PatientIdNumber ,  " +
              " AddressLine1, AddressLine2 , AddressCity , AddressStateCode , AddressZipCode, PhoneHome , PhoneMobile , EmailAddress  FROM patients " +
              "  WHERE  AgencyId = @agencyId  " +
              " {0} {1}  AND IsDeprecated = 0 ORDER BY LastName ASC , FirstName ASC", status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)" : " AND patients.Status = " + status, !branchId.IsEmpty() ? " AND AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<AddressBookEntry>(script))
            {
                adressBookEntrys = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new AddressBookEntry
                    {
                        Name = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial") + "." : ""),
                        IdNumber = reader.GetStringNullable("PatientIdNumber"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneHome = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PhoneMobile = reader.GetStringNullable("PhoneMobile").ToPhone(),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                    })
                    .AsList();
            }
            return adressBookEntrys;
        }

        public IList<PatientEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEmergencyContact>(p => p.AgencyId == agencyId && p.PatientId == patientId).OrderBy(p => p.IsPrimary == false).ToList();
        }

        public PatientEmergencyContact GetFirstEmergencyContactByPatient(Guid agencyId, Guid patientId)
        {
            var emergencyContact = new PatientEmergencyContact();
            var script = string.Format(@"SELECT 
                                FirstName as FirstName,
                                LastName as LastName ,
                                Relationship as Relationship ,
                                PrimaryPhone as PrimaryPhone ,
                                EmailAddress as EmailAddress 
                                FROM
                                    patientemergencycontacts 
                                        WHERE 
                                            AgencyId = @agencyId  AND 
                                            PatientId = @patientId 
                                            Order BY IsPrimary DESC 
                                            Limit 1");
            using (var cmd = new FluentCommand<PatientEmergencyContact>(script))
            {
                emergencyContact = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .SetMap(reader => new PatientEmergencyContact
                    {
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        Relationship = reader.GetStringNullable("Relationship"),
                        PrimaryPhone = reader.GetStringNullable("PrimaryPhone"),
                        EmailAddress = reader.GetStringNullable("EmailAddress")
                    })
                    .AsSingle();
            }
            return emergencyContact;
        }

        public List<ReferralInfo> GetReferralInfos(Guid agencyId, int status, DateTime startDate, DateTime endDate)
        {
            var referralInfos = new List<ReferralInfo>();
            var script=string.Format(@"SELECT referrals.ReferralDate as ReferralDate, agencyphysicians.FirstName as FirstName,"+
                "agencyphysicians.LastName as LastName, agencyphysicians.MiddleName as MiddleName, referrals.FirstName as rFirstName,"+
                "referrals.LastName as rLastName, referrals.AddressLine1 as AddressLine1, referrals.AddressLine2 as AddressLine2,"+
                "referrals.AddressCity as AddressCity, referrals.AddressStateCode as AddressStateCode, referrals.AddressZipCode as AddressZipCode,referrals.ServicesRequired as ServicesRequired," +
                "referrals.Gender as Gender, referrals.DOB as DOB, referrals.OtherReferralSource as OtherReferralSource, referrals.Status as Status "+                
                "FROM referrals LEFT JOIN agencyphysicians ON referrals.ReferrerPhysician = agencyphysicians.Id "+
                "WHERE referrals.AgencyId=@AgencyId {0} and DATE(referrals.ReferralDate) between DATE(@StartDate) and DATE(@EndDate) "+
                "and referrals.IsDeprecated=0",status>0?"AND referrals.Status=@Status":string.Empty);
            using (var cmd = new FluentCommand<ReferralInfo>(script))
            {
                referralInfos = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("AgencyId", agencyId)
                    .AddInt("Status",status)
                    .AddDateTime("StartDate",startDate)
                    .AddDateTime("EndDate",endDate)
                    .SetMap(reader => new ReferralInfo
                    {
                        ReferralDate=reader.GetDateTime("ReferralDate"),
                        FirstName=reader.GetStringNullable("FirstName"),
                        LastName=reader.GetStringNullable("LastName"),
                        MiddleName=reader.GetStringNullable("MiddleName"),
                        ReferralFirstName=reader.GetStringNullable("rFirstName"),
                        ReferralLastName=reader.GetStringNullable("rLastName"),
                        AddressLine1=reader.GetStringNullable("AddressLine1"),
                        AddressLine2=reader.GetStringNullable("AddressLine2"),
                        AddressCity=reader.GetStringNullable("AddressCity"),
                        AddressStateCode=reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode=reader.GetStringNullable("AddressZipCode"),
                        ServicesRequired=reader.GetStringNullable("ServicesRequired"),
                        Gender=reader.GetStringNullable("Gender"),
                        DOB=reader.GetDateTime("DOB"),
                        OtherReferralSource=reader.GetStringNullable("OtherReferralSource"),
                        Status=reader.GetInt("Status")
                    })
                    .AsList();
            }
            return referralInfos;
        }

        public AgencyPhysician GetPatientReferralPhysician(Guid agencyId, Guid patientId)
        {
            var physician = new AgencyPhysician();
            var script = string.Format(@"SELECT agencyphysicians.FirstName, agencyphysicians.LastName, agencyphysicians.NPI "
                                    + "FROM agencyphysicians INNER JOIN patients ON agencyphysicians.Id = patients.ReferrerPhysician "
                                    + "WHERE patients.agencyId=@agencyid AND patients.Id=@patientId limit 1");
            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                physician = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new AgencyPhysician
                    {
                        FirstName=reader.GetStringNullable("FirstName"),
                        LastName=reader.GetStringNullable("LastName"),
                        NPI=reader.GetStringNullable("NPI")
                    }).AsSingle();
            }
            return physician;                            
        }

        public List<EmergencyContactInfo> GetEmergencyContactInfos(Guid agencyId, Guid branchId, int status)
        {
            var emergencyContactInfos = new List<EmergencyContactInfo>();
            var script = string.Format(@"SELECT patients.FirstName as FirstName , patients.LastName as LastName , patients.MiddleInitial as MiddleInitial, patients.PatientIdNumber as PatientIdNumber , patients.Triage as Triage , " +
              "patientemergencycontacts.FirstName as ContactFirstName, patientemergencycontacts.LastName as ContactLastName , patientemergencycontacts.Relationship as Relationship , patientemergencycontacts.PrimaryPhone as PrimaryPhone , patientemergencycontacts.EmailAddress as EmailAddress  FROM patientemergencycontacts " +
              "  INNER JOIN patients ON patientemergencycontacts.PatientId = patients.Id WHERE  patients.AgencyId = @agencyId  AND patientemergencycontacts.IsPrimary = 1 " +
              " {0} {1} AND patients.IsDeprecated = 0 ORDER BY patients.LastName ASC , patients.FirstName ASC ", status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)  " : " AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<EmergencyContactInfo>(script))
            {
                emergencyContactInfos = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new EmergencyContactInfo
                    {
                        PatientName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial").ToUpperCase() + "." : string.Empty),
                        Triage = reader.GetInt("Triage"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        ContactName = string.Format("{0}, {1}", reader.GetStringNullable("ContactLastName").ToUpperCase(), reader.GetStringNullable("ContactFirstName").ToUpperCase()),
                        ContactRelation = reader.GetStringNullable("Relationship"),
                        ContactPhoneHome = reader.GetStringNullable("PrimaryPhone").ToPhone(),
                        ContactEmailAddress = reader.GetStringNullable("EmailAddress")
                    })
                    .AsList();
            }
            return emergencyContactInfos;
        }

        public List<PatientEvacuation> GetEmergencyPreparednessInfos(Guid agencyId, Guid branchId, int status)
        {
            var emergencyContactInfos = new List<PatientEvacuation>();
            var script = string.Format(@"SELECT patients.FirstName as FirstName , patients.LastName as LastName , patients.MiddleInitial as MiddleInitial, patients.PatientIdNumber as PatientIdNumber , patients.Triage as Triage , " +
                "patients.EvacuationAddressLine1 as EvacuationAddressLine1, patients.EvacuationAddressLine2 as EvacuationAddressLine2, patients.EvacuationAddressCity as EvacuationAddressCity, patients.EvacuationAddressStateCode as EvacuationAddressStateCode, patients.EvacuationAddressZipCode as EvacuationAddressZipCode, patients.EvacuationPhoneHome as EvacuationPhoneHome, patients.EvacuationPhoneMobile as EvacuationPhoneMobile, patients.EvacuationZone as EvacuationZone, " +
              "patientemergencycontacts.FirstName as ContactFirstName, patientemergencycontacts.LastName as ContactLastName , patientemergencycontacts.Relationship as Relationship , patientemergencycontacts.PrimaryPhone as PrimaryPhone , patientemergencycontacts.EmailAddress as EmailAddress  FROM patients " +
              "LEFT JOIN patientemergencycontacts ON patients.Id = patientemergencycontacts.PatientId WHERE  patients.AgencyId = @agencyId" +
              " {0} {1} AND patients.IsDeprecated = 0 ORDER BY patients.LastName ASC , patients.FirstName ASC ", status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)  " : " AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientEvacuation>(script))
            {
                emergencyContactInfos = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new PatientEvacuation
                    {
                        PatientName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? reader.GetStringNullable("MiddleInitial").ToUpperCase() + "." : string.Empty),
                        Triage = reader.GetInt("Triage"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        ContactName = string.Format("{0} {1}", reader.GetStringNullable("ContactFirstName").ToUpperCase(), reader.GetStringNullable("ContactLastName").ToUpperCase()),
                        ContactRelation = reader.GetStringNullable("Relationship"),
                        ContactPhoneHome = reader.GetStringNullable("PrimaryPhone").ToPhone(),
                        ContactEmailAddress = reader.GetStringNullable("EmailAddress"),
                        EvacuationAddressLine1 = reader.GetStringNullable("EvacuationAddressLine1"),
                        EvacuationAddressLine2 = reader.GetStringNullable("EvacuationAddressLine2"),
                        EvacuationAddressCity = reader.GetStringNullable("EvacuationAddressCity"),
                        EvacuationAddressStateCode = reader.GetStringNullable("EvacuationAddressStateCode"),
                        EvacuationAddressZipCode = reader.GetStringNullable("EvacuationAddressZipCode"),
                        EvacuationZone = reader.GetStringNullable("EvacuationZone"),
                        EvacuationPhoneHome = reader.GetStringNullable("EvacuationPhoneHome"),
                        EvacuationPhoneMobile = reader.GetStringNullable("EvacuationPhoneMobile")
                    })
                    .AsList();
            }
            return emergencyContactInfos;
        }

        public PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactId)
        {
            Check.Argument.IsNotEmpty(emergencyContactId, "emergencyContactId");
            return database.Single<PatientEmergencyContact>(e => e.PatientId == patientId && e.Id == emergencyContactId);
        }

        public bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            bool result = false;
            try
            {
                if (emergencyContact != null)
                {
                    if (emergencyContact.PhonePrimaryArray.Count >= 2)
                    {
                        emergencyContact.PrimaryPhone = emergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
                    }
                    if (emergencyContact.PhoneAlternateArray.Count >= 2)
                    {
                        emergencyContact.AlternatePhone = emergencyContact.PhoneAlternateArray.ToArray().PhoneEncode();
                    }
                    var emergencyContactToEdit = database.Single<PatientEmergencyContact>(e => e.PatientId == emergencyContact.PatientId && e.Id == emergencyContact.Id);
                    if (emergencyContactToEdit != null)
                    {
                        emergencyContactToEdit.FirstName = emergencyContact.FirstName;
                        emergencyContactToEdit.LastName = emergencyContact.LastName;
                        emergencyContactToEdit.PrimaryPhone = emergencyContact.PrimaryPhone;
                        emergencyContactToEdit.AlternatePhone = emergencyContact.AlternatePhone;
                        emergencyContactToEdit.EmailAddress = emergencyContact.EmailAddress;
                        emergencyContactToEdit.Relationship = emergencyContact.Relationship;
                        if (emergencyContact.IsPrimary)
                        {
                            if (SetPrimaryEmergencyContact(agencyId, emergencyContact.PatientId, emergencyContact.Id))
                            {
                                emergencyContactToEdit.IsPrimary = true;
                            }
                        }
                        database.Update<PatientEmergencyContact>(emergencyContactToEdit);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            try
            {
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    var emergencyContact = database.Single<PatientEmergencyContact>(r => r.PatientId == patientId && r.Id == Id);
                    if (emergencyContact != null)
                    {
                        database.Delete<PatientEmergencyContact>(emergencyContact.Id);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEmergencyContacts(Guid patientId)
        {
            bool result = false;
            try
            {
                if (!patientId.IsEmpty())
                {
                    database.DeleteMany<PatientEmergencyContact>(pec => pec.PatientId == patientId);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeletePhysicianContact(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            bool result = false;
            try
            {
                var patientPhysician = database.Single<PatientPhysician>(p => p.PatientId == patientId && p.PhysicianId == Id);
                if (patientPhysician != null)
                {
                    database.Delete<PatientPhysician>(patientPhysician.Id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId)
        {
            bool result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.Id == patientId);
                if (patient != null)
                {
                    var emergencyContacts = database.Find<PatientEmergencyContact>(p => p.PatientId == patientId);
                    bool flag = false; ;
                    foreach (PatientEmergencyContact contat in emergencyContacts)
                    {
                        if (contat.Id == emergencyContactId)
                        {
                            contat.IsPrimary = true;
                            flag = true;
                        }
                        else
                        {
                            contat.IsPrimary = false;
                        }
                    }
                    if (flag)
                    {
                        database.UpdateMany<PatientEmergencyContact>(emergencyContacts);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Add<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Update<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public MissedVisit GetMissedVisit(Guid agencyId, Guid Id)
        {
            var missedVisit = database.Single<MissedVisit>(m => m.AgencyId == agencyId && m.Id == Id);
            if (missedVisit != null)
            {
                var patient = database.Single<Patient>(p => p.Id == missedVisit.PatientId && p.AgencyId == agencyId);
                if (patient != null)
                {
                    missedVisit.PatientName = patient.DisplayName;
                }
                var scheduledEvent = GetSchedule(agencyId, missedVisit.EpisodeId, missedVisit.PatientId, Id);
                if (scheduledEvent != null)
                {
                    missedVisit.VisitType = scheduledEvent.DisciplineTaskName;
                    var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == scheduledEvent.UserId);
                    if (user != null)
                    {
                        missedVisit.UserName = user.DisplayName;
                    }
                }
            }
            return missedVisit;
        }

        public List<MissedVisit> GetMissedVisitsOnly(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var missedVisits = database.Find<MissedVisit>(m => m.AgencyId == agencyId && m.Date >= startDate && m.Date <= endDate).ToList();
            return missedVisits;
        }

        public bool AddOrder(PhysicianOrder order)
        {
            var result = false;
            try
            {
                if (order != null)
                {
                    order.Modified = DateTime.Now;
                    database.Add<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AddAuthorization(Authorization authorization)
        {
            var result = false;
            try
            {
                if (authorization != null)
                {
                    authorization.Created = DateTime.Now;
                    authorization.Modified = DateTime.Now;
                    database.Add<Authorization>(authorization);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool EditAuthorization(Authorization authorization)
        {
            var result = false;
            try
            {
                var authorizationToEdit = database.Single<Authorization>(a => a.AgencyId == authorization.AgencyId && a.PatientId == authorization.PatientId && a.Id == authorization.Id);
                if (authorizationToEdit != null)
                {
                    authorizationToEdit.StartDate = authorization.StartDate;
                    authorizationToEdit.EndDate = authorization.EndDate;
                    authorizationToEdit.AgencyLocationId = authorization.AgencyLocationId;
                    authorizationToEdit.Status = authorization.Status;
                    authorizationToEdit.Insurance = authorization.Insurance;
                    authorizationToEdit.Number1 = authorization.Number1;
                    authorizationToEdit.Number2 = authorization.Number2;
                    authorizationToEdit.Number3 = authorization.Number3;
                    authorizationToEdit.SNVisit = authorization.SNVisit;
                    authorization.SNVisitCountType = authorization.SNVisitCountType;
                    authorizationToEdit.PTVisit = authorization.PTVisit;
                    authorizationToEdit.PTVisitCountType = authorization.PTVisitCountType;
                    authorizationToEdit.OTVisit = authorization.OTVisit;
                    authorizationToEdit.OTVisitCountType = authorization.OTVisitCountType;
                    authorizationToEdit.STVisit = authorization.STVisit;
                    authorizationToEdit.STVisitCountType = authorization.STVisitCountType;
                    authorizationToEdit.MSWVisit = authorization.MSWVisit;
                    authorizationToEdit.MSWVisitCountType = authorization.MSWVisitCountType;
                    authorizationToEdit.HHAVisit = authorization.HHAVisit;
                    authorizationToEdit.HHAVisitCountType = authorization.HHAVisitCountType;

                    authorizationToEdit.DieticianVisit = authorization.DieticianVisit;
                    authorizationToEdit.DieticianVisitCountType = authorization.DieticianVisitCountType;

                    authorizationToEdit.RNVisit = authorization.RNVisit;
                    authorizationToEdit.RNVisitCountType = authorization.RNVisitCountType;

                    authorizationToEdit.LVNVisit = authorization.LVNVisit;
                    authorizationToEdit.LVNVisitCountType = authorization.LVNVisitCountType;
                    authorizationToEdit.Comments = authorization.Comments;

                    authorizationToEdit.Modified = DateTime.Now;
                    database.Update<Authorization>(authorizationToEdit);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var authorization = database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
                if (authorization != null)
                {

                    authorization.IsDeprecated = true;
                    authorization.Modified = DateTime.Now;
                    database.Update<Authorization>(authorization);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public IList<Authorization> GetAuthorizations(Guid agencyId, Guid patientId)
        {
            var authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false);
            if (authorizations != null && authorizations.Count > 0)
            {
                authorizations.ForEach(a =>
                {
                    a.Branch = GetBranchName(agencyId, a.AgencyLocationId);
                });
            }
            return authorizations.OrderBy(a => a.StartDate).ToList();
        }

        public IList<Authorization> GetAuthorizationsByStatusAndDate(Guid agencyId, Guid patientId, string status, DateTime startDate, DateTime endDate)
        {
            var authorizations =new List<Authorization>();
            if (status.Equals("All"))
            {
                authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.EndDate >= startDate && a.EndDate <= endDate && a.IsDeprecated == false).ToList();
            
            }
            else
            {

                authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Status == status && a.EndDate >= startDate && a.EndDate <= endDate && a.IsDeprecated == false).ToList();
            }
            if (authorizations != null && authorizations.Count > 0)
            {
                authorizations.ForEach(a =>
                {
                    a.Branch = GetBranchName(agencyId, a.AgencyLocationId);
                });
            }
            return authorizations.OrderBy(a => a.StartDate).ToList();
        }


        public IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, string insuranceId, string status, DateTime startDate, DateTime endDate)
        {
            var authorizations = new List<Authorization>();
            var script = string.Format(@"SELECT Id , Number1 , Number2 ,  " +
              " Number3 , StartDate , EndDate   FROM authorizations " +
              " WHERE  AgencyId = @agencyId AND PatientId = @patientId  AND  Insurance = @insuranceId " +
              "  AND Status = @status  AND ( (DATE( @startdate) between DATE(StartDate) and Date (EndDate)) OR ( DATE(@enddate) between DATE(StartDate) and Date (EndDate)) OR (DATE(StartDate) between  DATE(@startdate) and Date (@enddate)) OR (DATE(EndDate) between  DATE(@startdate) and Date (@enddate)))  ORDER BY StartDate ASC");
            using (var cmd = new FluentCommand<Authorization>(script))
            {
                authorizations = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddString("insuranceId", insuranceId)
                    .AddString("status", status)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new Authorization
                    {
                        Id = reader.GetGuid("Id"),
                        Number1 = reader.GetStringNullable("Number1"),
                        Number2 = reader.GetStringNullable("Number2"),
                        Number3 = reader.GetStringNullable("Number3"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate")
                    })
                    .AsList();
            }
            return authorizations;
        }

        public Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id)
        {
            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
        }

        public Authorization GetAuthorization(Guid agencyId, Guid Id)
        {
            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.Id == Id);
        }

        public PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var physicianOrder = database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id && o.IsDeprecated == false);
            if (physicianOrder != null)
            {
                var patient = GetPatientOnly(patientId, agencyId);
                physicianOrder.DisplayName = patient != null ? patient.DisplayName : string.Empty;
            }
            return physicianOrder;
        }

        public PhysicianOrder GetOrder(Guid Id, Guid agencyId)
        {
            var physicianOrder = database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.Id == Id && o.IsDeprecated == false);
            if (physicianOrder != null)
            {
                var patient = GetPatientOnly(physicianOrder.PatientId, physicianOrder.AgencyId);
                physicianOrder.DisplayName = patient != null ? patient.DisplayName : string.Empty;
            }
            return physicianOrder;
        }

        public PhysicianOrder GetOrderOnly(Guid Id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.Id == Id);
        }

        public PhysicianOrder GetOrderOnly(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id && o.IsDeprecated == false);
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status)
        {
            if (!agencyId.IsEmpty() && status > 0)
            {
                var orders = database.Find<PhysicianOrder>(o => o.AgencyId == agencyId && o.Status == status && o.IsDeprecated == false).ToList();
                orders.ForEach(o =>
                {
                    var patient = GetPatientOnly(o.PatientId, agencyId);
                    if (patient != null)
                    {
                        o.DisplayName = patient.DisplayName;
                    }

                    var physician = database.Single<AgencyPhysician>(p => p.Id == o.PhysicianId && p.AgencyId == agencyId);
                    if (physician != null)
                    {
                        o.PhysicianName = physician.DisplayName;
                    }
                });
                return orders;
            }
            return new List<PhysicianOrder>();
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT physicianorders.Id as Id, physicianorders.PhysicianId as PhysicianId, physicianorders.EpisodeId as EpisodeId, physicianorders.OrderNumber as OrderNumber, patients.Id as PatientId , patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.LastName as PhysicianLastName, physicianorders.Status as Status, physicianorders.OrderDate as OrderDate, physicianorders.ReceivedDate as ReceivedDate, physicianorders.SentDate as SentDate, agencyphysicians.Credentials as PhysicianCredentials , physicianorders.PhysicianSignatureDate as PhysicianSignatureDate " +
               "FROM physicianorders INNER JOIN patients ON physicianorders.PatientId = patients.Id INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id  " +
               "WHERE physicianorders.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) " +
               "AND physicianorders.IsDeprecated = 0 AND physicianorders.Id IN ( {0} )  AND physicianorders.OrderDate >= @startDate AND physicianorders.OrderDate <= @endDate", orderIds); //AND  physicianorders.Status = @status

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientVisitNote> GetEvalOrders(Guid agencyId, string evalIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patientvisitnotes.Id as Id, patientvisitnotes.PhysicianId as PhysicianId, patientvisitnotes.EpisodeId as EpisodeId, patientvisitnotes.OrderNumber as OrderNumber, " +
               "patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.LastName as PhysicianLastName, patientvisitnotes.NoteType as NoteType, " +
               "patientvisitnotes.Status as Status, patientvisitnotes.Note as Note, patientvisitnotes.ReceivedDate as ReceivedDate, patientvisitnotes.SentDate as SentDate, agencyphysicians.Credentials as PhysicianCredentials , patientvisitnotes.PhysicianSignatureDate as PhysicianSignatureDate " +
               "FROM patientvisitnotes INNER JOIN patients ON patientvisitnotes.PatientId = patients.Id INNER JOIN agencyphysicians ON patientvisitnotes.PhysicianId = agencyphysicians.Id " +
               "WHERE patientvisitnotes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2) " +
               "AND patientvisitnotes.IsDeprecated = 0 AND patientvisitnotes.Id IN ({0}) ", evalIds);

            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = GetNoteVisitDate(reader.GetStringNullable("Note")),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    NoteType = reader.GetStringNullable("NoteType"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();

            }
            var query = (from p in list
                         where p.OrderDate >= startDate && p.OrderDate <= endDate
                         select p).ToList();
            return query;
        }

        private DateTime GetNoteVisitDate(string note)
        {
            XElement rootElement = XElement.Parse(note);
            IDictionary<string, NotesQuestion> dict = new Dictionary<string, NotesQuestion>();
            foreach (XElement el in rootElement.Elements())
            {
                if (el.Attribute("Name") != null && el.Attribute("Answer") != null && el.Attribute("Type") != null)
                {
                    dict.Add(el.Attribute("Name").Value, new NotesQuestion { Name = el.Attribute("Name").Value, Answer = el.Attribute("Answer").Value, Type = el.Attribute("Type").Value });
                }
            }
            if (dict != null && dict.ContainsKey("VisitDate"))
            {
                return dict["VisitDate"].Answer.ToDateTime();
            }
            else
                return DateTime.MinValue;
        }

        public List<OrderLean> GetEvalOrdersByPhysician(Guid agencyId, Guid physicianId, DateTime startDate, DateTime endDate, int[] status)
        {
            var list = new List<OrderLean>();
            var allStatus = status.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");

            var query = string.Format(@"select
                                        pn.Id,
                                        pn.NoteType,
                                        pn.OrderNumber,
                                        pn.Status, 
                                        pn.AgencyId, 
                                        pn.PatientId,
                                        pn.EpisodeId, 
                                        pn.SentDate ,
                                        pa.LastName,
                                        pa.FirstName,
                                        pe.Schedule 
                                            from patients pa  
                                                 INNER JOIN patientepisodes pe ON pe.PatientId= pa.Id
                                                 INNER JOIN  patientvisitnotes pn ON pe.Id = pn.EpisodeId 
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    pe.AgencyId = @agencyid AND
                                                    pn.AgencyId = @agencyid AND
                                                    DATE(pe.StartDate) <=DATE(@enddate) AND DATE(pe.EndDate)>= DATE(@startdate) AND
                                                    pn.PhysicianId = @physicianid AND
                                                    pn.Status in ({0}) AND 
                                                    pn.IsDeprecated = 0 ", allStatus);

            using (var cmd = new FluentCommand<OrderLean>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("physicianid", physicianId)
                    .AddDateTime("startdate",startDate)
                    .AddDateTime("enddate",endDate)
                         .SetMap(reader => new OrderLean
                         {
                             Id = reader.GetGuid("Id"),
                             NoteType = reader.GetStringNullable("NoteType"),
                             OrderNumber = reader.GetInt("OrderNumber"),
                             Status = reader.GetInt("Status"),
                             AgencyId = reader.GetGuid("AgencyId"),
                             PatientId = reader.GetGuid("PatientId"),
                             EpisodeId = reader.GetGuid("EpisodeId"),
                             SentDate = reader.GetDateTime("SentDate"),
                             PatientName=reader.GetStringNullable("LastName")+", "+reader.GetStringNullable("FirstName"),
                             Schedule = reader.GetStringNullable("Schedule")
                         })
                    .AsList();
            }

            return list;
        }

        public List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<int> status, string evalIds, DateTime startDate, DateTime endDate)
        {
            var list = GetEvalOrders(agencyId, evalIds, startDate, endDate);
            return list.Where(p => status.Contains(p.Status)).ToList();
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, Guid branchId, string orderIds, DateTime startDate, DateTime endDate)
        {
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND patients.AgencyLocationId = @branchId ";
            }

            var script = string.Format(@"SELECT physicianorders.Id as Id, physicianorders.PhysicianId as PhysicianId, physicianorders.EpisodeId as EpisodeId, physicianorders.OrderNumber as OrderNumber, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.LastName as PhysicianLastName, physicianorders.Status as Status, physicianorders.OrderDate as OrderDate, physicianorders.ReceivedDate as ReceivedDate, physicianorders.SentDate as SentDate, agencyphysicians.Credentials as PhysicianCredentials " +
               "FROM physicianorders INNER JOIN patients ON physicianorders.PatientId = patients.Id INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id " +
               "WHERE physicianorders.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) {0}" +
               "AND physicianorders.IsDeprecated = 0 AND physicianorders.Id IN ( {1} )  AND physicianorders.OrderDate >= @startDate AND physicianorders.OrderDate <= @endDate", branchScript, orderIds);

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase()

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT physicianorders.Id as Id, physicianorders.PhysicianId, physicianorders.EpisodeId as EpisodeId, physicianorders.OrderNumber as OrderNumber, patients.Id as PatientId , patients.FirstName as FirstName, patients.LastName as LastName, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.LastName as PhysicianLastName, physicianorders.Status as Status , physicianorders.OrderDate as OrderDate , physicianorders.ReceivedDate as ReceivedDate, physicianorders.SentDate as SentDate " +
               "FROM physicianorders INNER JOIN patients ON physicianorders.PatientId = patients.Id INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id " +
               "WHERE physicianorders.AgencyId = @agencyid AND physicianorders.PatientId = @patientId  " +
               "AND physicianorders.IsDeprecated = 0 AND physicianorders.Id IN ( {0} )  AND physicianorders.OrderDate >= @startDate AND physicianorders.OrderDate <= @endDate", orderIds);

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase()

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPendingPhysicianSignatureOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT physicianorders.Id as Id, physicianorders.PhysicianId, physicianorders.EpisodeId as EpisodeId, physicianorders.OrderNumber as OrderNumber, patients.Id as PatientId , patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.LastName as PhysicianLastName, physicianorders.Status as Status , physicianorders.OrderDate as OrderDate , physicianorders.ReceivedDate as ReceivedDate, physicianorders.SentDate as SentDate , physicianorders.PhysicianSignatureDate as PhysicianSignatureDate " +
               "FROM physicianorders INNER JOIN patients ON physicianorders.PatientId = patients.Id INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id " +
               "WHERE physicianorders.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) " +
               "AND physicianorders.IsDeprecated = 0 AND physicianorders.Id IN ( {0} ) AND physicianorders.OrderDate >= @startDate AND physicianorders.OrderDate <= @endDate", orderIds);// AND  (physicianorders.Status = 130 || physicianorders.Status = 145 )

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase(),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.IsDeprecated == false).ToList();
        }

        public List<PhysicianOrder> GetAllPhysicianOrders()
        {
            return database.All<PhysicianOrder>().ToList();
        }

        public bool MarkOrderAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var physicianOrder = database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id);
                if (physicianOrder != null)
                {
                    physicianOrder.IsDeprecated = isDeprecated;
                    database.Update<PhysicianOrder>(physicianOrder);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignOrdersUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var physicianOrder = database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id);
            if (physicianOrder != null)
            {
                try
                {
                    physicianOrder.UserId = employeeId;
                    database.Update<PhysicianOrder>(physicianOrder);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived)
        {
            Check.Argument.IsNotEmpty(orderId, "orderId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            bool result = false;
            try
            {
                var order = database.Single<PhysicianOrder>(o => o.Id == orderId && o.AgencyId == agencyId);
                if (order != null)
                {
                    if (dateReceived != DateTime.MinValue)
                    {
                        var physician = database.Single<AgencyPhysician>(p => p.Id == order.PhysicianId && p.AgencyId == agencyId);
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", physician.DisplayName);
                        }
                        order.ReceivedDate = dateReceived;
                    }
                    order.Status = status;
                    if (status == (int)ScheduleStatus.OrderSentToPhysician)
                    {
                        order.SentDate = DateTime.Now;
                    }
                    database.Update<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived, DateTime dateSend)
        {
            bool result = false;
            try
            {
                var order = database.Single<PhysicianOrder>(o => o.Id == orderId && o.AgencyId == agencyId);
                if (order != null)
                {
                    order.ReceivedDate = dateReceived;
                    order.SentDate = dateSend;
                    order.Status = status;
                    database.Update<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                var editOrder = database.Single<PhysicianOrder>(o => o.Id == order.Id && o.PatientId == order.PatientId);
                if (order != null && editOrder != null)
                {
                    editOrder.Modified = DateTime.Now;
                    editOrder.OrderDate = order.OrderDate;
                    editOrder.Summary = order.Summary;
                    editOrder.Text = order.Text;
                    editOrder.PhysicianId = order.PhysicianId;
                    editOrder.Status = order.Status;
                    editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    editOrder.IsOrderReadAndVerified = order.IsOrderReadAndVerified;
                    editOrder.SignatureDate = order.SignatureDate;
                    editOrder.SignatureText = order.SignatureText;
                    editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    if (order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                    {
                        editOrder.PhysicianSignatureText = order.PhysicianSignatureText;
                        editOrder.PhysicianSignatureDate = order.PhysicianSignatureDate;
                        editOrder.ReceivedDate = order.ReceivedDate;
                    }
                    editOrder.PhysicianData = order.PhysicianData;
                    database.Update<PhysicianOrder>(editOrder);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrderModel(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                if (order != null)
                {
                    order.Modified = DateTime.Now;
                    database.Update<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public long GetNextOrderNumber()
        {
            var orderNumber = new OrderNumber();
            orderNumber.Created = DateTime.Now;
            database.Add<OrderNumber>(orderNumber);
            return orderNumber.Id;
        }

        public bool AddCommunicationNote(CommunicationNote communicationNote)
        {
            try
            {
                if (communicationNote != null)
                {
                    database.Add<CommunicationNote>(communicationNote);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public CommunicationNote GetCommunicationNote(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
        }

        public bool EditCommunicationNote(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            Check.Argument.IsNotEmpty(communicationNote.Id, "communicationNote.Id");

            bool result = false;
            try
            {
                var communicationNoteToEdit = database.Single<CommunicationNote>(c => c.AgencyId == communicationNote.AgencyId && c.Id == communicationNote.Id && c.PatientId == communicationNote.PatientId);
                if (communicationNoteToEdit != null)
                {
                    communicationNoteToEdit.Created = communicationNote.Created;
                    communicationNoteToEdit.Text = communicationNote.Text;
                    communicationNoteToEdit.EpisodeId = communicationNote.EpisodeId;
                    communicationNoteToEdit.PhysicianId = communicationNote.PhysicianId;
                    communicationNoteToEdit.SignatureText = communicationNote.SignatureText;
                    communicationNoteToEdit.SignatureDate = communicationNote.SignatureDate;
                    communicationNoteToEdit.Status = communicationNote.Status;
                    communicationNoteToEdit.PhysicianData = communicationNote.PhysicianData;
                    database.Update<CommunicationNote>(communicationNoteToEdit);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateCommunicationNoteModal(CommunicationNote communicationNote)
        {
            bool result = false;
            try
            {
                if (communicationNote != null)
                {
                    database.Update<CommunicationNote>(communicationNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteCommunicationNote(Guid agencyId, Guid Id, Guid patientId, bool isDeprecated)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            try
            {
                var commNote = database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id);
                if (commNote != null)
                {
                    commNote.IsDeprecated = isDeprecated;
                    database.Update<CommunicationNote>(commNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignCommunicationNoteUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var commNote = database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
            if (commNote != null)
            {
                try
                {
                    commNote.UserId = employeeId;
                    database.Update<CommunicationNote>(commNote);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId)
        {
            return database.Find<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.IsDeprecated == false).ToList();
        }

        public List<CommunicationNote> GetCommunicationNotesByEpisode(Guid agencyId, Guid patientId,Guid episodeId)
        {
            return database.Find<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.EpisodeId == episodeId && c.IsDeprecated == false).ToList();
        }

        public List<CommunicationNote> GetCommunicationNoteByIds(Guid agencyId, string orderIds)
        {

            var script = string.Format(@"SELECT communicationnotes.Id as Id, communicationnotes.EpisodeId as EpisodeId, communicationnotes.UserId as UserId, communicationnotes.Status as Status, communicationnotes.Created as Created , patients.Id as PatientId , patients.FirstName as FirstName, patients.LastName as LastName  " +
               "FROM communicationnotes INNER JOIN patients ON communicationnotes.PatientId = patients.Id  " +
               "WHERE communicationnotes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) " +
               "AND communicationnotes.IsDeprecated = 0 AND communicationnotes.Id IN ( {0} ) ", orderIds);

            var list = new List<CommunicationNote>();
            using (var cmd = new FluentCommand<CommunicationNote>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new CommunicationNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    UserDisplayName = UserEngine.GetName(reader.GetGuid("UserId"), agencyId),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    Created = reader.GetDateTime("Created")
                })
                .AsList();
            }
            return list;
        }

        public List<CommunicationNote> GetAllCommunicationNotes()
        {
            return database.All<CommunicationNote>().ToList();
        }

        public PatientNote GetNote(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<PatientNote>(n => n.PatientId == patientId && n.IsDeprecated == false);
        }

        public Guid Note(Guid patientId, string patientNote)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var exstingNote = database.Single<PatientNote>(n => n.PatientId == patientId && n.IsDeprecated == false);
            PatientNote note = new PatientNote();
            if (exstingNote != null)
            {
                exstingNote.Note = patientNote;
                database.Update<PatientNote>(exstingNote);
                return exstingNote.Id;
            }
            else
            {
                try
                {
                    note.Id = Guid.NewGuid();
                    note.PatientId = patientId;
                    note.Note = patientNote;
                    database.Add<PatientNote>(note);
                }
                catch (Exception ex)
                {
                    return Guid.Empty;
                }
            }
            return note.Id;
        }

        public List<BirthdayWidget> GetCurrentPatientBirthdays(Guid agencyId)
        {
            var script = @"SELECT `Id`,`FirstName`,`LastName`,`MiddleInitial`,`DOB`,`PhoneHome`,(YEAR(CURDATE())-YEAR(`DOB`)) - (RIGHT(CURDATE(),5)<RIGHT(`DOB`,5)) AS Age, `Status`  FROM patients WHERE `AgencyId` = @agencyid AND `Status` = @status AND `IsDeprecated` = 0 AND MONTH(`DOB`) = @month ORDER BY Age DESC LIMIT 0, 5";
            return new FluentCommand<BirthdayWidget>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("month", DateTime.Now.Month)
                .AddInt("status", (int)PatientStatus.Active)
                .SetMap(reader => new BirthdayWidget
                {
                    Id = reader.GetGuid("Id"),
                    Date = reader.GetDateTime("DOB"),
                    IsDischarged = reader.GetInt("Status") == (int)PatientStatus.Discharged,
                    PhoneHome = reader.GetStringNullable("PhoneHome"),
                    Name = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial() 
                })
                .AsList();
        }

        public List<PatientEpisode> GetPatientAllEpisodes(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).ToList();
        }

        public List<PatientEpisode> GetPatientActiveEpisodes(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).ToList();
        }

        public List<PatientEpisode> GetPatientAllEpisodesWithNoException(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).ToList();
        }

        public List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT  patientepisodes.Id, patientepisodes.PatientId, patientepisodes.IsActive, patientepisodes.IsDischarged, patientepisodes.EndDate, " +
                "patientepisodes.StartDate FROM patientepisodes " +
                "WHERE patientepisodes.AgencyId = @agencyid " +
                "AND patientepisodes.PatientId = @patientId AND (patientepisodes.IsDischarged = 1 || patientepisodes.IsActive = 0)";

            return new FluentCommand<EpisodeLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new EpisodeLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
        }

        public List<PatientEpisodeTherapyException> GetAllEpisodeAfterApril(Guid agencyId, Guid branchId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT patients.FirstName, patients.LastName, patients.MiddleInitial, patients.PatientIdNumber, patientepisodes.EndDate, " +
               "patientepisodes.StartDate, patientepisodes.Schedule FROM patientepisodes " +
               "INNER JOIN patients ON patientepisodes.PatientId = patients.Id WHERE patientepisodes.AgencyId = @agencyid " +
               "AND (patients.Status = 1) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
               "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate " +
               "OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) " +
               "OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and @enddate )) ";
            if (!branchId.IsEmpty())
            {
                script += " AND patients.AgencyLocationId=@branchId";
            }
            if (!patientId.IsEmpty())
            {
                script += " AND patients.Id=@patientId";
            }

            return new FluentCommand<PatientEpisodeTherapyException>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PatientEpisodeTherapyException
                {
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
        }

        public List<RecertEvent> GetUpcomingRecertsLean(Guid agencyId)
        {
            var script = @"SELECT patients.PatientIdNumber, patientepisodes.Schedule, patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id WHERE patientepisodes.AgencyId = @agencyid  AND patients.AgencyId = @agencyid " +
                "AND patients.Status = 1 AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patients.PrimaryInsurance = 1 || patients.PrimaryInsurance = 2 || patients.PrimaryInsurance = 3 || patients.PrimaryInsurance = 4) " +
                "AND patientepisodes.EndDate between ADDDATE(curdate(), 5) and ADDDATE(curdate(), 24) ORDER BY patientepisodes.EndDate ASC";

            return new FluentCommand<RecertEvent>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new RecertEvent
                {
                    Schedule = reader.GetString("Schedule"),
                    TargetDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
        }

        public List<RecertEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var insuranceFilter = string.Empty;
            var branchFilter = string.Empty;
            var list = new List<RecertEvent>();

            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId != 0)
            {
                insuranceFilter = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            if (branchId != Guid.Empty)
            {
                branchFilter = string.Format(" AND patients.AgencyLocationId = '{0}' ", branchId);

            }
            var script = string.Format(@"SELECT patients.Id, 
                                patients.PatientIdNumber,
                                patientepisodes.Schedule,
                                patientepisodes.StartDate, 
                                patientepisodes.EndDate, 
                                patients.FirstName, 
                                patients.LastName, 
                                patients.MiddleInitial 
                        FROM patients INNER JOIN patientepisodes 
                        ON patientepisodes.PatientId = patients.Id 
                        WHERE patients.AgencyId = @agencyid  
                        AND patients.Status = 1 
                        AND patients.IsDeprecated = 0 
                        AND patientepisodes.IsActive = 1 
                        AND patientepisodes.IsLinkedToDischarge = 0 {0} {1}
                AND DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) 
                ORDER BY patientepisodes.EndDate", branchFilter, insuranceFilter);
            using (var cmd = new FluentCommand<RecertEvent>(script))
            {

                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddDateTime("startdate", startDate)
                 .AddDateTime("enddate", endDate)
                 .SetMap(reader => new RecertEvent
                 {
                     Schedule = reader.GetString("Schedule"),
                     TargetDate = reader.GetDateTime("EndDate"),
                     StartDate = reader.GetDateTime("StartDate"),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                 })
                 .AsList();
            }
            return list;
        }
        public List<RecertEvent> GetUpcomingRecertsWidgetLean(Guid agencyId)
        {
            var script = @"SELECT patients.Id, 
                                patients.PatientIdNumber,
                                patientepisodes.StartDate, 
                                patientepisodes.EndDate, 
                                patients.FirstName, 
                                patients.LastName, 
                                patients.MiddleInitial 
                        FROM patients INNER JOIN patientepisodes 
                        ON patientepisodes.PatientId = patients.Id 
                        WHERE patients.AgencyId = @agencyid  
                        AND patients.Status = 1 
                        AND patients.IsDeprecated = 0 
                        AND patientepisodes.IsActive = 1 
                        AND patientepisodes.IsLinkedToDischarge = 0 
                        AND DATE(patientepisodes.EndDate) between curdate() AND ADDDATE(curdate(), 24) 
                        ORDER BY patientepisodes.EndDate LIMIT 5";

            return new FluentCommand<RecertEvent>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new RecertEvent
                {
                    Id = reader.GetGuid("Id"),
                    StartDate = reader.GetDateTime("StartDate"),
                    TargetDate = reader.GetDateTime("EndDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
        }

        public List<RecertEvent> GetPastDueRecertsWidgetLean(Guid agencyId)
        {
            //var script = @"SELECT agencymanagement.patients.PatientIdNumber, agencymanagement.patientepisodes.StartDate, agencymanagement.patientepisodes.EndDate, agencymanagement.patients.FirstName, agencymanagement.patients.LastName " +
            //    "FROM oasisc.recertificationassessments " +
            //    "INNER JOIN agencymanagement.patientepisodes ON oasisc.recertificationassessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.recertificationassessments.PatientId = agencymanagement.patientepisodes.PatientId AND agencymanagement.patientepisodes.IsActive = 1 AND agencymanagement.patientepisodes.IsDischarged = 0 AND agencymanagement.patientepisodes.AgencyId = @agencyid AND  agencymanagement.patientepisodes.EndDate < curdate() " +
            //    "INNER JOIN agencymanagement.patients ON agencymanagement.patientepisodes.PatientId = agencymanagement.patients.Id AND agencymanagement.patients.AgencyId = @agencyid AND agencymanagement.patients.Status = 1 AND agencymanagement.patients.IsDeprecated = 0 " +
            //    "WHERE  oasisc.recertificationassessments.IsDeprecated = 0  AND oasisc.recertificationassessments.Status IN ( '200', '205')  ORDER BY agencymanagement.patientepisodes.EndDate ASC LIMIT 0, 5";

            var list = new List<RecertEvent>();
            var startDate = DateTime.Now.AddDays(-59);
            var startDateProcessed = string.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            var endDate = DateTime.Now.AddDays(5);
            var endDateProcessed = string.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);
            var script = string.Format
                (
                    "SELECT patients.Id, patients.PatientIdNumber, patientepisodes.Schedule,patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName , patients.MiddleInitial " +
                    "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                    "WHERE patients.AgencyId = '{0}' " +
                    "AND patients.Status = '1' " +
                    "AND patients.IsDeprecated = '0' " +
                    "AND patientepisodes.IsActive = '1' " +
                    "AND patientepisodes.IsDischarged = '0' " +
                    "AND patientepisodes.IsLinkedToDischarge = '0' " +
                    "AND DATE(patientepisodes.EndDate) between DATE('{1}') and DATE('{2}') " +
                    "ORDER BY patientepisodes.EndDate ASC; "
                    , agencyId, startDateProcessed, endDateProcessed
                );

            //var script2 = @"SELECT patients.Id, patients.PatientIdNumber, patientepisodes.Schedule,patientepisodes.StartDate, patientepisodes.EndDate, " +
            //    "patients.FirstName, patients.LastName " +
            //    "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
            //    "WHERE patientepisodes.AgencyId = @agencyid AND patients.AgencyId = @agencyid " +
            //    "AND patients.Status = 1 AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 " +
            //    "AND patientepisodes.IsDischarged = 0 AND patientepisodes.IsLinkedToDischarge = 0 " +
            //    "AND patientepisodes.EndDate < curdate() ORDER BY patientepisodes.EndDate ASC LIMIT 0, 5;";

            using (var cmd = new FluentCommand<RecertEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new RecertEvent
                {
                    Id = reader.GetGuid("Id"),
                    TargetDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }

            return list;
        }

        public List<RecertEvent> GetPastDueRecertsLean(Guid agencyId)
        {
            var script = @"SELECT patients.PatientIdNumber, patientepisodes.Schedule, patientepisodes.EndDate, patients.FirstName, patients.LastName " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id WHERE patientepisodes.AgencyId = @agencyid AND patients.AgencyId = @agencyid " +
                "AND patients.Status = 1 AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patients.PrimaryInsurance = 1 || patients.PrimaryInsurance = 2 || patients.PrimaryInsurance = 3 || patients.PrimaryInsurance = 4) " +
                "AND patientepisodes.EndDate < curdate() ORDER BY patientepisodes.EndDate ASC";

            return new FluentCommand<RecertEvent>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new RecertEvent
                {
                    Schedule = reader.GetString("Schedule"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    TargetDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
        }

        public List<RecertEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var insuranceString = string.Empty;
            var branchString = string.Empty;
            var list = new List<RecertEvent>();

            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId != 0)
            {
                insuranceString = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            if (branchId != null && branchId != Guid.Empty)
            {
                branchString = string.Format("AND patients.AgencyLocationId = '{0}' ", branchId);

            }

            var startDateProcessed = string.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            var endDateProcessed = string.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);
            var script = string.Format
                (
                    "SELECT patients.PatientIdNumber, patientepisodes.Schedule,patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName, patients.MiddleInitial " +
                    "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                    "WHERE patients.AgencyId = '{0}' " +
                    "{1} " +
                    "AND patients.Status = '1' " +
                    "AND patients.IsDeprecated = '0' " +
                    "AND patientepisodes.IsActive = '1' " +
                    "AND patientepisodes.IsDischarged = '0' " +
                    "AND patientepisodes.IsLinkedToDischarge = '0' " +
                    "{2} " +
                    "AND DATE(patientepisodes.EndDate) between DATE('{3}') and DATE('{4}') " +
                    "ORDER BY patientepisodes.EndDate ASC; "
                    , agencyId, branchString, insuranceString, startDateProcessed, endDateProcessed
                );

            //var script2 = string.Format(@"SELECT patients.PatientIdNumber, patientepisodes.Schedule,patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName, patients.MiddleInitial " +
            //    "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id WHERE patientepisodes.AgencyId = @agencyid  AND patients.AgencyId = @agencyid  {0} " +
            //    "AND patients.Status = 1 AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0  AND patientepisodes.IsLinkedToDischarge = 0 " +
            //    " {1}  AND DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) ORDER BY patientepisodes.EndDate ASC", !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty, insuranceString);
            ////( patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate )) ORDER BY patientepisodes.EndDate ASC
            using (var cmd = new FluentCommand<RecertEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                  .AddGuid("agencyid", agencyId)
                  .AddGuid("branchid", branchId)
                  .AddDateTime("startdate", startDate)
                  .AddDateTime("enddate", endDate)
                  .SetMap(reader => new RecertEvent
                  {
                      Schedule = reader.GetStringNullable("Schedule"),
                      PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                      StartDate = reader.GetDateTime("StartDate"),
                      TargetDate = reader.GetDateTime("EndDate"),
                      PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                  })
                  .AsList();
            }
            return list;
        }

        public string GetPatientNameById(Guid patientId, Guid agencyId)
        {
            string name = string.Empty;
            var patient = database.Find<Patient>(p => p.Id == patientId).FirstOrDefault();
            if (patient != null)
            {
                name = patient.DisplayNameWithMi;
            }
            return name;
        }

        public List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId)
        {
            var script = @"SELECT patients.FirstName, patients.LastName, patientepisodes.Schedule FROM patients " +
                "INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id WHERE patients.AgencyId = @agencyid " +
                "AND ( patients.Status = 1 ||  patients.Status = 2 ) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0";

            return new FluentCommand<PatientEpisodeData>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Schedule = reader.GetString("Schedule"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                }).AsList();
        }

        public List<PatientEpisodeData> GetAllPatientEpisodeData(Guid agencyId)
        {
            var script = @"SELECT patients.FirstName, patients.LastName, patients.Status, patients.DischargeDate, patientepisodes.EndDate, " +
                "patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details FROM patientepisodes " +
                "INNER JOIN patients ON patientepisodes.PatientId = patients.Id WHERE patientepisodes.AgencyId = @agencyid " +
                "AND (patients.Status = 1 || patients.Status = 2) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1";

            return new FluentCommand<PatientEpisodeData>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
        }

        public PatientEpisode GetPreviousEpisode(Guid agencyId, Guid patientId, DateTime startDate)
        {
            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId
               && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
            return allEpisodes.Where(e => e.EndDate < startDate).FirstOrDefault();
        }

        public PatientEpisode GetPreviousEpisodeFluent(Guid agencyId, Guid patientId, DateTime startDate)
        {
            var script = @"SELECT patientepisodes.Id as Id , patientepisodes.PatientId as PatientId ,  patientepisodes.Details as Details , patientepisodes.StartDate as StartDate , patientepisodes.EndDate as EndDate , patientepisodes.Schedule as Schedule , " +
                 " patientepisodes.IsActive as IsActive , patientepisodes.IsRecertCompleted as IsRecertCompleted , patientepisodes.IsDischarged as IsDischarged , patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge ,patientepisodes.AssessmentId as AssessmentId , patientepisodes.AssessmentType as AssessmentType , patientepisodes.Created as Created , patientepisodes.Modified as Modified , patientepisodes.AdmissionId as AdmissionId  " +
                 "FROM patientepisodes  " +
                  "WHERE (patientepisodes.IsDischarged = 0 && patientepisodes.IsActive = 1) && patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientid AND DATE(patientepisodes.StartDate) < DATE(@startdate) ORDER BY  patientepisodes.StartDate DESC LIMIT 1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    Details = reader.GetStringNullable("Details"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsRecertCompleted = reader.GetBoolean("IsRecertCompleted"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                    AssessmentId = reader.GetGuid("AssessmentId"),
                    AssessmentType = reader.GetStringNullable("AssessmentType"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    AdmissionId = reader.GetGuid("AdmissionId")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT patientepisodes.Id as Id , patients.Id as PatientId, patients.FirstName, patients.PatientIdNumber , patients.LastName, patients.MiddleInitial, patientepisodes.EndDate, patientepisodes.StartDate " +
              "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
              "WHERE (patientepisodes.IsDischarged = 0 && patientepisodes.IsActive = 1) && patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientId AND patientepisodes.Id = @episodeId ";
            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeId", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    DisplayName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName"), reader.GetStringNullable("FirstName"), reader.GetStringNullable("MiddleInitial").ToInitial())

                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetCurrentEpisode(Guid agencyId, Guid patientId)
        {
            PatientEpisode episode = null;
            if (!patientId.IsEmpty())
            {
                var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
                episode = allEpisodes.Where(e => e.StartDate <= DateTime.Now).FirstOrDefault();
                if (episode != null)
                {
                    var previousEpisode = allEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate > episode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        episode.NextEpisode = nextEpisode;
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        episode.PreviousEpisode = previousEpisode;
                    }
                }
                else if (allEpisodes != null)
                {
                    var futureEpisode = allEpisodes.LastOrDefault();
                    if (futureEpisode != null)
                    {
                        var previousEpisode = allEpisodes.Where(e => e.StartDate < futureEpisode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate > futureEpisode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            futureEpisode.NextEpisode = nextEpisode;
                        }
                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            futureEpisode.PreviousEpisode = previousEpisode;
                        }
                    }
                    episode = futureEpisode;
                }
            }
            return episode;
        }

        public PatientEpisodeData GetCurrentEpisodeLean(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT patientepisodes.Id as Id, patients.FirstName, patients.LastName, patients.Status, patients.DischargeDate, patientepisodes.EndDate, " +
                "patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id WHERE patientepisodes.AgencyId = @agencyid " +
                "AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND patients.Id = @patientid AND curdate() BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate limit 0,1";

            var currentEpisode = new PatientEpisodeData();

            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                currentEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsSingle();
            }

            return currentEpisode;
        }

        public PatientEpisodeData GetCurrentEpisodeId(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT patientepisodes.Id as Id, patientepisodes.EndDate, " +
                "patientepisodes.StartDate " +
                "FROM patientepisodes  WHERE patientepisodes.AgencyId = @agencyid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND patientepisodes.PatientId = @patientid ORDER BY patientepisodes.StartDate DESC";

            var allEpisodes = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                allEpisodes = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                })
                .AsList();
            }

            PatientEpisodeData episode = null;
            if (allEpisodes != null && allEpisodes.Count > 0)
            {
                episode = allEpisodes.Where(e => e.StartDate.ToDateTime() <= DateTime.Now.Date).FirstOrDefault();
                if (episode != null)
                {
                    return episode;
                }
                else if (allEpisodes != null)
                {
                    var futureEpisode = allEpisodes.LastOrDefault();
                    episode = futureEpisode;
                }
            }
            return episode;
        }

        public PatientEpisode GetCurrentEpisodeOnly(Guid agencyId, Guid patientId)
        {
            PatientEpisode episode = null;
            if (!patientId.IsEmpty())
            {
                var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate).ToList();
                if (allEpisodes != null && allEpisodes.Count > 0)
                {
                    episode = allEpisodes.Where(e => e.StartDate.Date <= DateTime.Now.Date).FirstOrDefault();
                    if (episode != null)
                    {
                        return episode;
                    }
                    else if (allEpisodes != null)
                    {
                        var futureEpisode = allEpisodes.LastOrDefault();
                        episode = futureEpisode;
                    }
                }
            }
            return episode;
        }

        public NewEpisodeData GetLastEpisode(Guid agencyId, Guid patientId)
        {
            NewEpisodeData newEpisodeDate = null;
            if (!patientId.IsEmpty())
            {
                var script = @"SELECT patientepisodes.Id as Id , patients.Id as PatientId , patients.FirstName as FirstName , patients.LastName as LastName , patients.CaseManagerId as CaseManagerId , patients.PrimaryInsurance as PrimaryInsurance , patients.SecondaryInsurance as SecondaryInsurance , " +
                  "patientepisodes.EndDate as EndDate , patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge, patientepisodes.StartDate as StartDate , patientepisodes.StartOfCareDate as StartOfCareDate , patientepisodes.AdmissionId as AdmissionId  " +
                  "FROM patientepisodes  INNER JOIN patients ON patients.Id = patientepisodes.PatientId " +
                  "WHERE patientepisodes.AgencyId = @agencyid  AND patientepisodes.PatientId = @patientid " +
                  "AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0 " +
                  " ORDER BY patientepisodes.StartDate DESC LIMIT 1 ";
                using (var cmd = new FluentCommand<NewEpisodeData>(script))
                {
                    newEpisodeDate = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new NewEpisodeData
                    {
                        PatientId = reader.GetGuid("PatientId"),
                        CaseManager = reader.GetStringNullable("CaseManagerId"),
                        IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                        PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                        SecondaryInsurance = reader.GetStringNullable("SecondaryInsurance"),
                        EndDate = reader.GetDateTime("EndDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                        AdmissionId = reader.GetGuid("AdmissionId")

                    })
                    .AsSingle();
                }
                //if (newEpisodeDate != null)
                //{
                //    var managedDate = this.GetPatientLatestAdmissionDate(agencyId, patientId, (int)PatientDateType.StartOfCareDate);
                //    if (managedDate != null && managedDate.Date.Date > DateTime.MinValue.Date)
                //    {
                //        newEpisodeDate.StartOfCareDate = managedDate.Date;
                //    }
                //}
            }
            return newEpisodeDate;
        }

        public DateRange GetCurrentEpisodeDate(Guid agencyId, Guid patientId)
        {
            DateRange dateRange = new DateRange();
            if (!patientId.IsEmpty())
            {
                var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate <= DateTime.Today && e.EndDate >= DateTime.Today && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate).FirstOrDefault();
                if (episode != null)
                {
                    dateRange.StartDate = (DateTime)episode.StartDate;
                    dateRange.EndDate = (DateTime)episode.EndDate;
                }
            }
            return dateRange;
        }

        public DateRange GetNextEpisode(Guid agencyId, Guid patientId)
        {
            DateRange dateRange = new DateRange();
            if (!patientId.IsEmpty())
            {
                var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate >= DateTime.Now && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate).FirstOrDefault();
                if (episode != null)
                {
                    dateRange.StartDate = (DateTime)episode.StartDate;
                    dateRange.EndDate = (DateTime)episode.EndDate;
                }
            }
            return dateRange;
        }

        public DateRange GetPreviousEpisode(Guid agencyId, Guid patientId)
        {
            DateRange dateRange = new DateRange();
            if (!patientId.IsEmpty())
            {
                var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EndDate < DateTime.Now && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.EndDate).FirstOrDefault();
                if (episode != null)
                {
                    dateRange.StartDate = (DateTime)episode.StartDate;
                    dateRange.EndDate = (DateTime)episode.EndDate;
                }
            }
            return dateRange;
        }

       

        public PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
            var episode = allEpisodes.Where(e => e.StartDate.Date <= date.Date).FirstOrDefault();
            if (episode != null)
            {
                //var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId && p.IsDeprecated == false);
                //if (patient != null)
                //{
                //    episode.DisplayName = patient.DisplayName;
                //}
                var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > episode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();

                List<ScheduleEvent> events = null;
                if (episode.Schedule.IsNotNullOrEmpty())
                {
                    if (discipline.IsEqual("all"))
                    {
                        events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date).ToList();
                    }
                    else if (discipline.IsEqual("Nursing"))
                    {
                        events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == "Nursing" || e.Discipline == "ReportsAndNotes" && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date).ToList();
                    }
                    else if (discipline.IsEqual("Therapy"))
                    {
                        events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST") && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date).ToList();
                    }
                    else
                    {
                        events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == discipline && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date).ToList();
                    }
                }
                else
                {
                    var tempList = new List<ScheduleEvent>();
                    episode.Schedule = tempList.ToXml();
                }
                if (events != null && events.Count > 0)
                {
                    events.ForEach(evnt =>
                    {
                        if (evnt.Url != "0" && evnt.Url.IsNotNullOrEmpty())
                        {
                            var user = database.Single<User>(evnt.UserId);
                            if (user != null)
                            {
                                evnt.UserName = user.DisplayName;
                            }
                        }
                    });
                    episode.Schedule = events.ToXml();
                }
                if (episode.Details.IsNotNullOrEmpty())
                {
                    episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                }
                if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                {
                    episode.NextEpisode = nextEpisode;
                    if (episode.NextEpisode.Details.IsNotNullOrEmpty())
                    {
                        episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
                    }
                }
                if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                {
                    episode.PreviousEpisode = previousEpisode;
                    if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
                    {
                        episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                    }
                }
            }
            else if (allEpisodes != null)
            {
                var futureEpisode = allEpisodes.Where(e => e.IsActive).LastOrDefault();
                if (futureEpisode != null)
                {
                    //var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId && p.IsDeprecated == false);
                    //if (patient != null)
                    //{
                    //    futureEpisode.DisplayName = patient.DisplayName;
                    //}
                    var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < futureEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > futureEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                    List<ScheduleEvent> events = null;
                    if (discipline.IsEqual("all"))
                    {
                        events = futureEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= futureEpisode.StartDate.Date && e.EventDate.ToDateTime().Date <= futureEpisode.EndDate.Date).ToList();
                    }
                    else if (discipline.IsEqual("Nursing"))
                    {
                        events = futureEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == "Nursing" || e.Discipline == "ReportsAndNotes" && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= futureEpisode.StartDate.Date && e.EventDate.ToDateTime().Date <= futureEpisode.EndDate.Date).ToList();
                    }
                    else if (discipline.IsEqual("Therapy"))
                    {
                        events = futureEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST") && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= futureEpisode.StartDate.Date && e.EventDate.ToDateTime().Date <= futureEpisode.EndDate.Date).ToList();
                    }
                    else
                    {
                        events = futureEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == discipline && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= futureEpisode.StartDate.Date && e.EventDate.ToDateTime().Date <= futureEpisode.EndDate.Date).ToList();
                    }
                    if (events != null && events.Count > 0)
                    {
                        events.ForEach(evnt =>
                        {
                            if (evnt.Url != "0" && evnt.Url.IsNotNullOrEmpty())
                            {
                                var user = database.Single<User>(evnt.UserId);
                                if (user != null)
                                {
                                    evnt.UserName = user.DisplayName;
                                }
                            }
                        });
                    }
                    futureEpisode.Schedule = events.ToXml();
                    if (futureEpisode.Details.IsNotNullOrEmpty())
                    {
                        futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
                    }
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        futureEpisode.NextEpisode = nextEpisode;
                        if (futureEpisode.NextEpisode.Details.IsNotNullOrEmpty())
                        {
                            futureEpisode.NextEpisode.Detail = futureEpisode.NextEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        futureEpisode.PreviousEpisode = previousEpisode;
                        if (futureEpisode.PreviousEpisode.Details.IsNotNullOrEmpty())
                        {
                            futureEpisode.PreviousEpisode.Detail = futureEpisode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                    episode = futureEpisode;
                }
            }
            return episode;
        }

        public List<PatientEpisode> GetPatientScheduledEvents(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            Id,
                            EndDate, 
                            StartDate,
                            Schedule,
                            Details 
                                FROM 
                                    patientepisodes
                                            WHERE 
                                                AgencyId = @agencyid AND 
                                                IsActive = 1 AND
                                                IsDischarged = 0 AND 
                                                PatientId = @patientid";
            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details")
                })
                .AsList();
            }
            return list;

        }

        public List<PatientEpisode> GetPatientScheduledEventsByRange(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT 
                            Id,
                            EndDate,
                            StartDate, 
                            Schedule,
                            Details 
                                FROM 
                                    patientepisodes 
                                        WHERE 
                                            AgencyId = @agencyid  AND
                                            IsActive = 1  AND
                                            IsDischarged = 0 AND 
                                            PatientId = @patientid AND 
                                            (DATE(StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(StartDate) and DATE(EndDate) AND DATE(@enddate)  between DATE(StartDate) and DATE(EndDate)) OR ( DATE(StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(EndDate)  between DATE(@startdate) and  DATE(@enddate) ))";

            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT patientepisodes.Id as Id, patients.Id as PatientId, patients.FirstName, patients.PatientIdNumber , patients.LastName, patients.MiddleInitial, patients.Status, patients.DischargeDate, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                "WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND (patients.Status = 1 || patients.Status = 2)" +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND ( patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))";

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeDataSchduleWidget(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT
                            patients.Id as PatientId, 
                            patients.FirstName,
                            patients.LastName, 
                            patients.MiddleInitial,
                            patients.Status, 
                            patientepisodes.Schedule,
                            patientepisodes.EndDate,
                            patientepisodes.StartDate
                                FROM 
                                   patients 
                                        INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id 
                                            WHERE 
                                                 patients.AgencyId = @agencyid AND
                                                 patients.IsDeprecated = 0 AND
                                                (patients.Status = 1 || patients.Status = 2) AND
                                                 patientepisodes.IsActive = 1 AND
                                                 patientepisodes.IsDischarged = 0 AND
                                                (DATE(patientepisodes.StartDate) BETWEEN DATE(@startdate) AND DATE(@enddate) || DATE(patientepisodes.EndDate) BETWEEN " +
                                                                "DATE(@startdate) AND DATE(@enddate) OR (DATE(@startdate) BETWEEN DATE(patientepisodes.StartDate) AND DATE(patientepisodes.EndDate) " +
                                                                "AND DATE(@enddate) BETWEEN DATE(patientepisodes.StartDate) AND DATE(patientepisodes.EndDate)) OR (DATE(patientepisodes.StartDate) " +
                                                                "BETWEEN DATE(@startdate) AND DATE(@enddate) AND DATE(patientepisodes.EndDate) BETWEEN DATE(@startdate) AND DATE(@enddate)))";

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    PatientId = reader.GetGuid("PatientId"),
                    Status = reader.GetInt("Status"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            return list;
        }


        public List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, byte statusId)
        {
            var script = string.Format(@"SELECT patients.Id as PatientId, patients.FirstName, patients.LastName, " +
                "patients.Status, patients.MiddleInitial, patients.DOB, patients.PhoneHome, patientepisodes.Schedule " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                "WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Status = @statusid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0;");

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("statusid", statusId)
                .SetMap(reader => new PatientEpisodeData
                {
                    DOB = reader.GetDateTime("DOB"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PhoneNumber = reader.GetStringNullable("PhoneHome"),
                    LastName = reader.GetStringNullable("LastName").ToTitleCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToTitleCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid branchId, byte statusId)
        {
            var script = string.Format(@"SELECT patients.Id as PatientId, patients.FirstName, patients.LastName, patients.IsDeprecated, " +
                "patients.Status, patients.MiddleInitial, patients.DOB, patients.PhoneHome, patientepisodes.Schedule " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                "WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Status = @statusid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 {0} ;", !branchId.IsEmpty() ? "AND patients.AgencyLocationId = '" + branchId.ToString() + "' " : string.Empty);

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("statusid", statusId)
                .SetMap(reader => new PatientEpisodeData
                {
                    DOB = reader.GetDateTime("DOB"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PhoneNumber = reader.GetStringNullable("PhoneHome"),
                    LastName = reader.GetStringNullable("LastName").ToTitleCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToTitleCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisode> GetPatientEpisodeDataForSchedule(Guid agencyId, Guid branchId, int statusId, DateTime startDate, DateTime endDate)
        {
            var status = string.Empty;
            if (statusId <= 0)
            {
                status = " AND ( patients.Status = 1 OR patients.Status = 2 OR patients.Status = 4 )";
            }
            else
            {
                status = " AND patients.Status = @statusid";
            }
            var script = string.Format(@"SELECT 
                                    patientepisodes.Id as Id,
                                    patients.FirstName,
                                    patients.LastName, 
                                    patients.MiddleInitial,
                                    patients.PatientIdNumber,
                                    patientepisodes.EndDate as EndDate, 
                                    patientepisodes.StartDate as StartDate,
                                    patientepisodes.Schedule,
                                    patientepisodes.Details 
                                        FROM 
                                            patients 
                                                INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id
                                                    WHERE
                                                        patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 {1} AND
                                                        ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) )) AND 
                                                        patientepisodes.IsActive = 1 AND
                                                        patientepisodes.IsDischarged = 0 {0} ;", !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId " : string.Empty, status);

            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", statusId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    Details = reader.GetStringNullable("Details"),
                    PatientIdNumber=reader.GetStringNullable("PatientIdNumber"),
                    DisplayName = string.Concat(reader.GetStringNullable("LastName").ToUpperCase(), ", ", reader.GetStringNullable("FirstName").ToUpperCase(), (reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? " " + reader.GetStringNullable("MiddleInitial").ToUpperCase() + "." : string.Empty))
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeDataForSchedule(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT patients.FirstName, patients.LastName, patients.MiddleInitial, patientepisodes.Schedule, patientepisodes.Details, " +
                "patientepisodes.StartDate, patientepisodes.EndDate " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                "WHERE patients.AgencyId = @agencyid AND patients.Id = @patientid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0;";

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = string.Concat(reader.GetStringNullable("LastName").ToUpperCase(), ", ", reader.GetStringNullable("FirstName").ToUpperCase(), (reader.GetStringNullable("MiddleInitial").IsNotNullOrEmpty() ? " " + reader.GetStringNullable("MiddleInitial").ToUpperCase() + "." : string.Empty))
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid branchId, byte statusId, int insuranceId)
        {
            var insurance = string.Empty;
            var list = new List<PatientEpisodeData>();
            //if (insuranceId.IsInteger() && insuranceId.ToInteger() > 0)
            //{
            //    insurance = " AND ( patients.PrimaryInsurance = @insuranceid ||patients.SecondaryInsurance = @insuranceid || patients.TertiaryInsurance = @insuranceid )";
            //}

            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", location.Payor);
                        }
                        else
                        {
                            insurance = " AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format(" AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance => 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 ) ", agency.Payor);
                        }
                        else
                        {
                            insurance = " AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 ) ";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} ) ", insuranceId);
            }


            var script = string.Format(@"SELECT patients.Id as PatientId, patients.FirstName, patients.LastName, " +
                " patients.Status, patients.MiddleInitial, patientepisodes.Schedule " +
                " FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                " WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Status = @statusid " +
                " AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 {0} {1} ", insurance, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = '" + branchId.ToString() + "' " : string.Empty);


            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddInt("statusid", statusId)
                .AddGuid("agencyid", agencyId)
                    //.AddString("insuranceid", insuranceId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    LastName = reader.GetStringNullable("LastName").ToTitleCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToTitleCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToUpper()
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            string patientScript = "";
            if (!patientId.IsEmpty())
            {
                patientScript = " AND patients.Id = @patientid ";
            }

            var script = string.Format(@"SELECT
                                patients.FirstName,
                                patients.LastName ,
                                patients.MiddleInitial,
                                patients.PatientIdNumber , 
                                patients.Status,
                                patients.DischargeDate,
                                patientepisodes.Id, 
                                patientepisodes.EndDate,
                                patientepisodes.StartDate,
                                patientepisodes.Schedule,
                                patientepisodes.Details 
                                    FROM 
                                    patients
                                        INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id 
                                            WHERE
                                                patients.AgencyId = @agencyid  AND
                                                patientepisodes.IsActive = 1  AND
                                                patientepisodes.IsDischarged = 0 {0}  AND
                                                ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))", patientScript);
            //AND patients.IsDeprecated = 0 AND ( patients.Status = 1 OR patients.Status = 2)
            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetEpisodeData(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT  patientepisodes.Id, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                "WHERE patients.AgencyId = @agencyid  AND patients.AgencyId = @agencyid " +//AND patients.IsDeprecated = 0 AND ( patients.Status = 1 OR patients.Status = 2)
                " AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0 AND patients.Id = @patientid" +
                " AND (( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate)) OR (DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate)) OR ( DATE(patientepisodes.StartDate) < DATE(@startdate) and DATE(patientepisodes.EndDate) > DATE(@enddate)) OR ( DATE(patientepisodes.StartDate)  >  DATE(@startdate) and  DATE(patientepisodes.EndDate) <DATE(@enddate)) )";

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled()
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeDataByBranch(Guid agencyId, Guid locationId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT patientepisodes.Id, patients.FirstName, patients.LastName ,  patients.PatientIdNumber , patients.MiddleInitial, patients.Status, patients.DischargeDate, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id " +
                "WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) " +
                (!locationId.IsEmpty() ? "AND patients.AgencyLocationId = '" + locationId.ToString() + "' " : string.Empty) +
                "AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0 " +
                "AND (DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))";

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                    StartDate = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy"),
                    PatientName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").ToInitial()),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisodeData> GetPatientEpisodeDataByBranchAndPatient(Guid agencyId, Guid locationId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            string patientScript = "";
            if (!patientId.IsEmpty())
            {
                patientScript = " AND patients.Id = @patientid ";
            }
            var script = string.Format(@"SELECT patientepisodes.Id  as Id,
                                            patients.FirstName,
                                            patients.LastName , 
                                            patients.PatientIdNumber ,
                                            patients.MiddleInitial, 
                                            patients.Status, patients.DischargeDate, 
                                            patientepisodes.EndDate,
                                            patientepisodes.StartDate,
                                            patientepisodes.Schedule,
                                            patientepisodes.Details 
                                                FROM
                                                    patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id 
                                                WHERE 
                                                    patients.AgencyId = @agencyid AND
                                                    patients.IsDeprecated = 0 AND 
                                                    (patients.Status = 1 || patients.Status = 2) {1} AND 
                                                    patientepisodes.IsActive = 1 AND 
                                                    patientepisodes.IsDischarged = 0 {0} AND 
                                                    (DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) 
                                                        OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) 
                                                        OR (DATE(@startdate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) 
                                                        OR (DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) AND DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate)))",
                                                                patientScript, !locationId.IsEmpty() ? "AND patients.AgencyLocationId = '" + locationId.ToString() + "' " : string.Empty);
            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                    StartDate = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy"),
                    PatientName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase(), reader.GetStringNullable("MiddleInitial").ToInitial()),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }

        public PatientEpisodeData GetPatientScheduledEvents(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT patients.FirstName, patients.LastName, patients.MiddleInitial, patients.Status, patients.DischargeDate, patientepisodes.EndDate, " +
                "patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details " +
                "FROM patients INNER JOIN patientepisodes ON patientepisodes.PatientId = patients.Id WHERE patients.AgencyId = @agencyid " +
                "AND  patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND patients.Id = @patientid AND patientepisodes.Id = @episodeid";

            return new FluentCommand<PatientEpisodeData>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Status = reader.GetInt("Status"),
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsSingle();
        }

        public PatientEpisodeData GetPatientEpisodeWithSchedule(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule, patientepisodes.Details " +
                "FROM patientepisodes WHERE patientepisodes.AgencyId = @agencyid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND patientepisodes.PatientId = @patientid AND patientepisodes.Id = @episodeid";

            return new FluentCommand<PatientEpisodeData>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Details = reader.GetStringNullable("Details"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsSingle();
        }

        public List<ScheduleEvent> GetScheduledEventsByEmployeeAssigned(Guid agencyId, Guid patientId, Guid employeeId, DateTime startDate, DateTime endDate)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (!patientId.IsEmpty())
            {
                var patientEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false);
                patientEpisodes.ForEach(patientEpisode =>
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated != true
                        && s.EventDate.IsValidDate() && s.UserId == employeeId && s.EventDate.ToDateTime() >= startDate && s.EventDate.ToDateTime() <= endDate).ToList();
                    if (schedule != null && schedule.Count > 0)
                    {
                        patientEvents.AddRange(schedule);
                    }
                });
            }
            return patientEvents;
        }



        public List<ScheduleEvent> GetEpisodeSchedulesByEmployee(Guid agencyId, Guid patientId, Guid episodeId, Guid employeeId)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                var patientEpisode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId).SingleOrDefault();
                if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    patientEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated != true && s.UserId == employeeId).ToList();
                }
            }
            return patientEvents;
        }

        public PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
        }

        public PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT patientepisodes.Id as Id , patientepisodes.PatientId as PatientId ,  patientepisodes.Details as Details , patientepisodes.StartDate as StartDate , patientepisodes.EndDate as EndDate , patientepisodes.Schedule as Schedule , " +
                " patientepisodes.IsActive as IsActive , patientepisodes.IsRecertCompleted as IsRecertCompleted , patientepisodes.IsDischarged as IsDischarged , patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge ,patientepisodes.AssessmentId as AssessmentId , patientepisodes.AssessmentType as AssessmentType , patientepisodes.Created as Created , patientepisodes.Modified as Modified , patientepisodes.AdmissionId as AdmissionId , patientadmissiondates.StartOfCareDate as StartOfCareDate " +
                "FROM patientepisodes INNER JOIN patientadmissiondates ON patientepisodes.AdmissionId = patientadmissiondates.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patientadmissiondates.AgencyId = @agencyid AND patientepisodes.Id = @episodeid AND patientepisodes.PatientId = @patientid LIMIT 1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    Details = reader.GetStringNullable("Details"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsRecertCompleted = reader.GetBoolean("IsRecertCompleted"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                    AssessmentId = reader.GetGuid("AssessmentId"),
                    AssessmentType = reader.GetStringNullable("AssessmentType"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId)
        {
            PatientEpisode patientEpisode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (patientEpisode != null)
                {
                    var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                    if (patient != null)
                    {
                        patientEpisode.DisplayName = patient.DisplayName;
                        patientEpisode.StartOfCareDateFormatted = patient.StartOfCareDateFormatted;
                    }
                    var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                    if (allEpisodes != null)
                    {
                        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < patientEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > patientEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            patientEpisode.NextEpisode = nextEpisode;
                            if (patientEpisode.NextEpisode.Details.IsNotNullOrEmpty())
                            {
                                patientEpisode.NextEpisode.Detail = patientEpisode.NextEpisode.Details.ToObject<EpisodeDetail>();
                            }
                        }
                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            patientEpisode.PreviousEpisode = previousEpisode;
                            if (patientEpisode.PreviousEpisode.Details.IsNotNullOrEmpty())
                            {
                                patientEpisode.PreviousEpisode.Detail = patientEpisode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                            }
                        }
                    }
                    var schedule = patientEpisode.Schedule.IsNotNullOrEmpty() ? patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.Date && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.Date).ToList() : new List<ScheduleEvent>();
                    schedule.ForEach(s =>
                    {
                        if (!s.UserId.IsEmpty())
                        {
                            var user = database.Single<User>(s.UserId);
                            if (user != null)
                            {
                                s.UserName = user.DisplayName;
                            }
                        }
                    });
                    patientEpisode.Schedule = schedule.ToXml();
                    if (patientEpisode.Details.IsNotNullOrEmpty())
                    {
                        patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                    }
                }
            }
            return patientEpisode;
        }

        //TODO Add logic to not get the patient name based on a parameter
        public PatientEpisode GetEpisodeOnlyWithPreviousAndAfter(Guid agencyId, Guid episodeId, Guid patientId)
        {
            PatientEpisode patientEpisode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (patientEpisode != null)
                {
                    var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                    if (patient != null)
                    {
                        patientEpisode.DisplayName = patient.DisplayName;
                    }
                    var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                    if (allEpisodes != null)
                    {
                        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < patientEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > patientEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            patientEpisode.NextEpisode = nextEpisode;
                        }
                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            patientEpisode.PreviousEpisode = previousEpisode;
                        }
                    }
                }
            }
            return patientEpisode;
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline)
        {
            PatientEpisode episode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty() && discipline.IsNotNullOrEmpty())
            {
                try
                {
                    var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                    if (allEpisodes != null)
                    {
                        episode = allEpisodes.Where(e => e.Id == episodeId).SingleOrDefault();
                        if (episode != null)
                        {
                            var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                            if (patient != null)
                            {
                                episode.DisplayName = patient.DisplayName;
                            }
                            var previousEpisode = allEpisodes.Where(e => (e.StartDate < episode.StartDate) && e.IsActive == true).OrderByDescending(e => e.StartDate).FirstOrDefault();
                            var nextEpisode = allEpisodes.Where(e => (e.StartDate > episode.StartDate) && e.IsActive == true).OrderBy(e => e.StartDate).FirstOrDefault();

                            List<ScheduleEvent> events = null;
                            if (discipline.IsEqual("all"))
                            {
                                events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.ToDateTime() >= episode.StartDate && e.EventDate.ToDateTime() <= episode.EndDate).ToList();
                            }
                            else if (discipline.IsEqual("Therapy"))
                            {
                                events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST") && e.EventDate.ToDateTime() >= episode.StartDate && e.EventDate.ToDateTime() <= episode.EndDate).ToList();
                            }
                            else
                            {
                                events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == discipline && e.EventDate.ToDateTime() >= episode.StartDate && e.EventDate.ToDateTime() <= episode.EndDate).ToList();
                            }
                            if (events != null && events.Count > 0)
                            {
                                events.ForEach(evnt =>
                                {
                                    if (!evnt.UserId.IsEmpty() && evnt.Url != "0")
                                    {
                                        var user = database.Single<User>(evnt.UserId);
                                        if (user != null)
                                        {
                                            evnt.UserName = user.DisplayName;
                                        }
                                    }
                                });
                            }
                            episode.Schedule = events.ToXml();
                            if (episode.Details.IsNotNullOrEmpty())
                            {
                                episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                            }

                            if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                            {
                                episode.NextEpisode = nextEpisode;
                                if (episode.NextEpisode.Details.IsNotNullOrEmpty())
                                {
                                    episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
                                }
                            }
                            if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                            {
                                episode.PreviousEpisode = previousEpisode;
                                if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
                                {
                                    episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return episode;

        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            PatientEpisode episode = null;
            try
            {
                episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => se.StartDate.Date <= date.Date && se.EndDate.Date >= date.Date).OrderByDescending(se => se.StartDate.Date).ThenByDescending(se => se.EndDate.Date).FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
            return episode;
        }

        public PatientEpisode GetEpisodeLean(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT 
                            patientepisodes.Id as Id ,
                            patientepisodes.PatientId as PatientId ,
                            patientepisodes.StartDate as StartDate ,
                            patientepisodes.EndDate as EndDate ,
                            patientepisodes.Schedule as Schedule 
                                FROM
                                    patientepisodes 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND 
                                            patientepisodes.PatientId = @patientid AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(patientepisodes.EndDate) = DATE(@date)  
                                            ORDER BY patientepisodes.StartDate ASC LIMIT 0,1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetEpisodeLeanByStartDate(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT 
                            patientepisodes.Id as Id ,
                            patientepisodes.PatientId as PatientId ,
                            patientepisodes.StartDate as StartDate ,
                            patientepisodes.EndDate as EndDate 
                                FROM
                                    patientepisodes 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND 
                                            patientepisodes.PatientId = @patientid AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(patientepisodes.StartDate) = DATE(@date)  
                                            ORDER BY patientepisodes.StartDate ASC LIMIT 0,1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public bool AddEpisode(PatientEpisode patientEpisode)
        {
            var result = false;
            if (patientEpisode != null)
            {
                try
                {
                    patientEpisode.Details = patientEpisode.Detail.ToXml();
                    patientEpisode.Created = DateTime.Now;
                    patientEpisode.Modified = DateTime.Now;
                    database.Add<PatientEpisode>(patientEpisode);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid agencyId, PatientEpisode patientEpisode)
        {
            var result = false;
            try
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientEpisode.PatientId && e.Id == patientEpisode.Id);
                if (episode != null)
                {
                    episode.StartDate = patientEpisode.StartDate;
                    episode.EndDate = patientEpisode.EndDate;
                    episode.Schedule = patientEpisode.Schedule;
                    episode.IsActive = patientEpisode.IsActive;
                    episode.Details = patientEpisode.Details;
                    //episode.StartOfCareDate = patientEpisode.StartOfCareDate;
                    episode.AdmissionId = patientEpisode.AdmissionId;
                    episode.Modified = DateTime.Now;
                    database.Update<PatientEpisode>(episode);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateEpisode(PatientEpisode episode)
        {
            var result = false;
            try
            {
                if (episode != null)
                {
                    database.Update<PatientEpisode>(episode);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate)
        {
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).Where(e => e.StartDate.Date >= dischargeDate.Date).ToList();
        }

        public List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate, Guid admissionId)
        {
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.AdmissionId == admissionId).Where(e => e.StartDate.Date >= dischargeDate.Date).ToList();
        }

        public bool UpdateEpisodeForDischarge(List<PatientEpisode> patientEpisodes)
        {
            var result = false;
            try
            {
                if (patientEpisodes.Count > 0)
                {
                    database.UpdateMany<PatientEpisode>(patientEpisodes);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool Activate(Guid agencyId, Guid patientId)
        {
            var result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    patient.Status = (int)PatientStatus.Active;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool ActivateWithNewSOC(Guid agencyId, Guid patientId, DateTime startOfCareDate)
        {
            var result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    patient.Status = (int)PatientStatus.Active;
                    patient.StartofCareDate = startOfCareDate;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete)
        {
            var result = false;
            try
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (episode != null)
                {
                    episode.IsRecertCompleted = isRecertComplete;
                    episode.Modified = DateTime.Now;
                    database.Update<PatientEpisode>(episode);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool AddNewScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, ScheduleEvent newScheduledEvent)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(newScheduledEvent, "newScheduledEvent");
            var result = false;
            var episode = this.GetEpisodeOnly(agencyId, episodeId, patientId);
            if (episode != null)
            {
                var events = episode.Schedule.IsNotNullOrEmpty() ? episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()).ToList() : new List<ScheduleEvent>();
                newScheduledEvent.EpisodeId = episode.Id;
                newScheduledEvent.EndDate = episode.EndDate;
                newScheduledEvent.StartDate = episode.StartDate;
                events.Add(newScheduledEvent);
                episode.Modified = DateTime.Now;
                episode.Schedule = events.ToXml();
                database.Update<PatientEpisode>(episode);
                result = true;
            }
            return result;
        }

        public void AddNewUserEvent(Guid agencyId, Guid patientId, UserEvent newUserEvent)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(newUserEvent, "newUserEvent");

            var employeeEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == newUserEvent.UserId);
            if (employeeEpisode != null)
            {
                var employeeEvents = employeeEpisode.Visits.ToObject<List<UserEvent>>().Where(s => !s.EventId.IsEmpty()).ToList();
                employeeEvents.Add(newUserEvent);
                employeeEpisode.Visits = employeeEvents.ToXml();
                database.Update<UserSchedule>(employeeEpisode);
            }
            else
            {
                var userSchedule = new UserSchedule();
                userSchedule.AgencyId = agencyId;
                userSchedule.Id = Guid.NewGuid();
                userSchedule.PatientId = patientId;
                userSchedule.UserId = newUserEvent.UserId;

                var newEventList = new List<UserEvent>();
                newEventList.Add(newUserEvent);
                userSchedule.Visits = newEventList.ToXml();
                userSchedule.Created = DateTime.Now;
                userSchedule.Modified = DateTime.Now;
                database.Add<UserSchedule>(userSchedule);
            }
        }

        public bool UpdateEpisode(Guid agencyId, ScheduleEvent scheduleEvent)
        {
            bool result = false;
            try
            {
                if (scheduleEvent != null)
                {
                    var patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                    if (patientEpisode != null && !string.IsNullOrEmpty(patientEpisode.Schedule))
                    {
                        var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                        events.ForEach(e =>
                        {
                            if (e.EventId == scheduleEvent.EventId)
                            {
                                e.Assets = scheduleEvent.Assets;
                                e.TimeIn = scheduleEvent.TimeIn;
                                e.Status = scheduleEvent.Status;
                                e.UserId = scheduleEvent.UserId;
                                e.TimeOut = scheduleEvent.TimeOut;
                                e.Comments = scheduleEvent.Comments;
                                e.IsPayable = scheduleEvent.IsPayable;
                                e.EventDate = scheduleEvent.EventDate;
                                e.Surcharge = scheduleEvent.Surcharge;
                                e.VisitDate = scheduleEvent.VisitDate;
                                e.Discipline = scheduleEvent.Discipline;
                                e.IsBillable = scheduleEvent.IsBillable;
                                e.IsPayable = scheduleEvent.IsPayable;
                                e.GCode = scheduleEvent.GCode;
                                e.IsVisitPaid = scheduleEvent.IsVisitPaid;
                                e.SendAsOrder = scheduleEvent.SendAsOrder;
                                e.ReturnReason = scheduleEvent.ReturnReason;
                                e.TravelTimeIn = scheduleEvent.TravelTimeIn;
                                e.IsDeprecated = scheduleEvent.IsDeprecated;
                                e.InPrintQueue = scheduleEvent.InPrintQueue;
                                e.IsMissedVisit = scheduleEvent.IsMissedVisit;
                                e.TravelTimeOut = scheduleEvent.TravelTimeOut;
                                e.DisciplineTask = scheduleEvent.DisciplineTask;
                                e.ServiceLocation = scheduleEvent.ServiceLocation;
                                e.PersonalComments = scheduleEvent.PersonalComments;
                                e.AssociatedMileage = scheduleEvent.AssociatedMileage;
                                e.MissedVisitFormReturnReason = scheduleEvent.MissedVisitFormReturnReason;
                            }
                        });
                        patientEpisode.Modified = DateTime.Now;
                        patientEpisode.Schedule = events.ToXml();
                        database.Update<PatientEpisode>(patientEpisode);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateEpisode(Guid agencyId, Guid episodeId, Guid patientId, List<ScheduleEvent> newEvents)
        {
            bool result = false;
            try
            {
                if (!agencyId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && newEvents != null)
                {
                    var patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId && e.PatientId == patientId);
                    if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => !e.EventId.IsEmpty()).ToList();
                        events.ForEach(e =>
                        {
                            e.EpisodeId = episodeId;
                            e.PatientId = patientId;
                            e.StartDate = patientEpisode.StartDate;
                            e.EndDate = patientEpisode.EndDate;
                        });

                        newEvents.ForEach(ev =>
                        {
                            ev.EpisodeId = episodeId;
                            ev.PatientId = patientId;
                            ev.StartDate = patientEpisode.StartDate;
                            ev.EndDate = patientEpisode.EndDate;
                            events.Add(ev);
                        });

                        patientEpisode.Modified = DateTime.Now;
                        patientEpisode.Schedule = events.ToXml();
                        database.Update<PatientEpisode>(patientEpisode);
                        newEvents.ForEach(ev =>
                        {
                            var userEvent = new UserEvent { DisciplineTask = ev.DisciplineTask, EventDate = ev.EventDate, VisitDate = ev.VisitDate, EventId = ev.EventId, UserId = ev.UserId, PatientId = patientId, Status = ev.Status, EpisodeId = ev.EpisodeId, Discipline = ev.Discipline, IsDeprecated = ev.IsDeprecated };
                            var userEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == ev.UserId);

                            if (userEpisode != null)
                            {
                                var employeeEvents = userEpisode.Visits.ToObject<List<UserEvent>>().Where(e => e.EventId != Guid.Empty).ToList();
                                employeeEvents.Add(userEvent);
                                userEpisode.Visits = employeeEvents.ToXml();
                                database.Update<UserSchedule>(userEpisode);
                            }
                            else
                            {
                                var userSchedule = new UserSchedule();
                                userSchedule.Id = Guid.NewGuid();
                                userSchedule.AgencyId = agencyId;
                                userSchedule.PatientId = patientId;
                                userSchedule.UserId = ev.UserId;
                                var newEventList = new List<UserEvent>();
                                newEventList.Add(userEvent);
                                userSchedule.Visits = newEventList.ToXml();
                                userSchedule.Created = DateTime.Now;
                                userSchedule.Modified = DateTime.Now;
                                database.Add<UserSchedule>(userSchedule);
                            }
                        });
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    scheduleEvents.ForEach(scheduleEvent =>
                    {
                        var patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                        if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (events != null && events.Count > 0)
                            {
                                events.FirstOrDefault(e => e.EventId == scheduleEvent.EventId).IsBillable = scheduleEvent.IsBillable;
                                patientEpisode.Modified = DateTime.Now;
                                patientEpisode.Schedule = events.ToXml();
                                database.Update<PatientEpisode>(patientEpisode);
                            }
                        }
                    }
                    );
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool Reassign(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid userId)
        {
            bool result = false;
            try
            {
                if (!agencyId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && !userId.IsEmpty())
                {
                    var patientEpisode = database.Single<PatientEpisode>(pe => pe.AgencyId == agencyId && pe.PatientId == patientId && pe.Id == episodeId);
                    if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                        events.ForEach(ev =>
                        {
                            if (ev.EventId == eventId)
                            {
                                ev.UserId = userId;
                            }
                        });
                        patientEpisode.Schedule = events.ToXml();
                        database.Update<PatientEpisode>(patientEpisode);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public ScheduleEvent GetSchedule(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            ScheduleEvent scheduleEvent = null;
            if (!agencyId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (episode != null)
                {
                    scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                    if (scheduleEvent != null)
                    {
                        var user = database.Single<User>(scheduleEvent.UserId);
                        if (user != null)
                        {
                            scheduleEvent.UserName = user.DisplayName;
                            if (user.IsDeprecated)
                            {
                                scheduleEvent.IsUserDeleted = true;
                            }
                        }
                        scheduleEvent.StartDate = episode.StartDate;
                        scheduleEvent.EndDate = episode.EndDate;
                        scheduleEvent.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
                        var patient = database.Single<Patient>(p => p.Id == patientId);
                        if (patient != null)
                        {
                            scheduleEvent.PatientName = patient.DisplayName;
                            scheduleEvent.PatientIdNumber = patient.PatientIdNumber;
                            if (scheduleEvent.ServiceLocation.IsNullOrEmpty())
                            {
                                scheduleEvent.ServiceLocation = patient.ServiceLocation;
                            }
                        }
                    }
                }
            }
            return scheduleEvent;
        }

        public ScheduleEvent GetScheduleOnly(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            ScheduleEvent scheduleEvent = null;
            if (!agencyId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
                    if (scheduleEvent != null)
                    {
                        scheduleEvent.StartDate = episode.StartDate;
                        scheduleEvent.EndDate = episode.EndDate;
                        scheduleEvent.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
                    }
                }
            }
            return scheduleEvent;
        }

        public bool DeleteEpisode(Guid agencyId, Patient patient, out PatientEpisode episodeDeleted)
        {
            try
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patient.Id && e.StartDate <= DateTime.Now && e.EndDate > DateTime.Now);
                episodeDeleted = episode;
                if (episode != null)
                {
                    database.Delete<PatientEpisode>(episode);
                }
                var userSchedule = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patient.Id && e.UserId == patient.UserId);
                if (userSchedule != null)
                {
                    database.Delete<UserSchedule>(userSchedule);
                }
            }
            catch (Exception ex)
            {
                episodeDeleted = null;
                return false;
            }
            return true;
        }

        public bool DeleteEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            try
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (episode != null)
                {
                    database.Delete<PatientEpisode>(episode.Id);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool DeleteScheduleEvent(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, int task)
        {
            bool result = false;
            try
            {
                var patientEpisode = database.Single<PatientEpisode>(pe => pe.AgencyId == agencyId && pe.PatientId == patientId && pe.Id == episodeId);
                if (patientEpisode != null)
                {
                    List<ScheduleEvent> events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    var scheduleEvent = events.Find(e => e.EventId == eventId && e.DisciplineTask == task);
                    if (scheduleEvent != null)
                    {
                        var deletedItem = GetDeletedItem(agencyId, episodeId, patientId);
                        if (deletedItem == null)
                        {
                            deletedItem = new DeletedItem
                            {
                                Id = Guid.NewGuid(),
                                AgencyId = agencyId,
                                EpisodeId = episodeId,
                                Schedule = new List<ScheduleEvent>() { scheduleEvent }.ToXml(),
                                PatientId = patientId,
                                Created = DateTime.Now,
                                Modified = DateTime.Now
                            };
                            this.AddDeletedItem(deletedItem);
                        }
                        else
                        {
                            var deletedSchedule = deletedItem.Schedule.ToObject<List<ScheduleEvent>>();
                            if (deletedSchedule != null && !deletedSchedule.Exists(s => s.EventId == eventId))
                            {
                                deletedSchedule.Add(scheduleEvent);
                                deletedItem.Schedule = deletedSchedule.ToXml();
                                this.UpdateDeletedItem(deletedItem);
                            }
                        }
                        events.RemoveAll(evnt => evnt.EventId == eventId && evnt.DisciplineTask == task);
                        patientEpisode.Schedule = events.ToXml();
                        database.Update<PatientEpisode>(patientEpisode);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteScheduleEvents(Guid agencyId, Guid episodeId, Guid patientId, List<Guid> eventsToBeDeleted, out List<ScheduleEvent> deletedEvents)
        {
            bool result = false;
            deletedEvents = new List<ScheduleEvent>();
            try
            {
                var patientEpisode = database.Single<PatientEpisode>(pe => pe.AgencyId == agencyId && pe.PatientId == patientId && pe.Id == episodeId);
                if (patientEpisode != null)
                {
                    List<ScheduleEvent> events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    List<ScheduleEvent> eventsToBeKept = new List<ScheduleEvent>();
                    foreach (var evnt in events)
                    {
                        if (eventsToBeDeleted.Exists(e => e == evnt.EventId))
                        {
                            deletedEvents.Add(evnt);
                        }
                        else
                        {
                            eventsToBeKept.Add(evnt);
                        }
                    }
                    var deletedItem = GetDeletedItem(agencyId, episodeId, patientId);
                    if (deletedItem == null)
                    {
                        deletedItem = new DeletedItem
                        {
                            Id = Guid.NewGuid(),
                            AgencyId = agencyId,
                            EpisodeId = patientEpisode.Id,
                            Schedule = deletedEvents.ToXml(),
                            PatientId = patientId,
                            Created = DateTime.Now,
                            Modified = DateTime.Now
                        };
                        this.AddDeletedItem(deletedItem);
                    }
                    else
                    {
                        var deletedSchedule = deletedItem.Schedule.IsNotNullOrEmpty() ? deletedItem.Schedule.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                        if (deletedSchedule == null)
                        {
                            deletedSchedule = new List<ScheduleEvent>();
                        }
                        if (deletedSchedule != null)
                        {
                            foreach (var evnt in deletedEvents)
                            {
                                if (!deletedSchedule.Exists(s => s.EventId == evnt.EventId))
                                {
                                    deletedSchedule.Add(evnt);
                                }
                            }
                            deletedItem.Schedule = deletedSchedule.ToXml();
                            this.UpdateDeletedItem(deletedItem);
                        }
                        else
                        {
                            return false;
                        }
                    }

                    patientEpisode.Schedule = eventsToBeKept.ToXml();
                    database.Update<PatientEpisode>(patientEpisode);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId)
        {
            return database.Single<MedicationProfileHistory>(m => m.AgencyId == AgencyId && m.Id == Id);
        }

        public bool AddNewMedicationProfile(MedicationProfile medication)
        {
            var result = false;
            try
            {
                database.Add<MedicationProfile>(medication);
                result = true;
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory)
        {
            try
            {
                database.Add<MedicationProfileHistory>(medicationHistory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string medicationType)
        {
            var medicationProfile = this.GetMedicationProfile(Id, agencyId);
            if (medicationProfile != null)
            {
                medication.Id = Guid.NewGuid();
                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                if (medicationProfile.Medication.IsNullOrEmpty())
                {
                    var newList = new List<Medication>() { medication };
                    medicationProfile.Medication = newList.ToXml();
                    database.Update<MedicationProfile>(medicationProfile);
                }
                else
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    existingList.Add(medication);
                    medicationProfile.Medication = existingList.ToXml();
                    database.Update<MedicationProfile>(medicationProfile);
                }
            }
            return medicationProfile;
        }

        public bool UpdateMedication(MedicationProfile medicationProfile)
        {
            var result = false;
            try
            {
                if (medicationProfile != null)
                {
                    database.Update<MedicationProfile>(medicationProfile);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public List<Patient> GetPatientByAgencyPhysician(Guid agencyId, Guid loginId)
        {
            Guid physicianId = database.Find<AgencyPhysician>(ag => ag.AgencyId == agencyId && ag.LoginId == loginId && ag.IsDeprecated == false).FirstOrDefault().Id;
            List<PatientPhysician> pp = database.Find<PatientPhysician>(p => p.PhysicianId == physicianId).ToList();
            if (pp != null && pp.Count > 0)
            {
                var patientIds = new StringBuilder();
                pp.ForEach(p =>
                    {
                        patientIds.AppendFormat("'{0}',", p.PatientId);
                    });
                string ids = patientIds.ToString().Remove(patientIds.ToString().Length - 1, 1);
                var query = new QueryBuilder("select * from patients")
                          .Where(string.Format("patients.Id in ({0})", ids))
                          .And("patients.IsDeprecated=0");
                using (var cmd = new FluentCommand<Patient>(query.Build()))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString").AsList();
                }
            }
            else
            {
                return new List<Patient>();
            }
        }

        public bool AddCPO(CarePlanOversight cpo)
        {
            var result = false;
            if (cpo != null)
            {
                cpo.Id = Guid.NewGuid();
                cpo.Created = DateTime.Now;
                cpo.Modified = DateTime.Now;
                try
                {
                    database.Add<CarePlanOversight>(cpo);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteCPO(Guid id)
        {
            var cpo = database.Single<CarePlanOversight>(c => c.Id == id);
            if (cpo != null)
            {
                cpo.IsDeprecated = true;
                cpo.Modified = DateTime.Now;
                database.Update<CarePlanOversight>(cpo);
                return true;
            }
            return false;
        }

        public bool UpdateCPO(CarePlanOversight cpo)
        {
            var result = false;
            if (cpo != null)
            {
                cpo.Modified = DateTime.Now;
                try
                {
                    database.Update<CarePlanOversight>(cpo);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string medicationType)
        {
            try
            {
                var medicationProfile = this.GetMedicationProfile(Id, agencyId);
                string medicationString = string.Empty;
                if (medicationProfile != null)
                {
                    medicationString = medicationProfile.Medication;
                }
                if (medicationString.IsNotNullOrEmpty() && medicationString != "")
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList != null && existingList.Count > 0 && existingList.Exists(m => m.Id == medication.Id))
                    {
                        var med = existingList.Single(m => m.Id == medication.Id);
                        if (med.StartDate.Date != medication.StartDate.Date || med.Route != medication.Route || med.Frequency != medication.Frequency || med.MedicationDosage != medication.MedicationDosage)
                        {
                            medication.Id = Guid.NewGuid();
                            medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                            medication.MedicationType = new MedicationType { Value = "C", Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                            existingList.Add(medication);
                            med.MedicationCategory = "DC";
                            med.DCDate = DateTime.Now;
                            medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            database.Update<MedicationProfile>(medicationProfile);
                            return true;
                        }
                        else
                        {
                            med.IsLongStanding = medication.IsLongStanding;
                            med.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() }; ;
                            med.Classification = medication.Classification;
                            if (med.MedicationCategory == "DC")
                            {
                                med.DCDate = medication.DCDate;
                            }
                            medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            database.Update<MedicationProfile>(medicationProfile);
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMedicationForDischarge(Guid medId, Guid agencyId, Guid Id, DateTime dischargeDate)
        {
            bool result = false;
            try
            {
                var medicationProfile = this.GetMedicationProfile(medId, agencyId);
                string medicationString = "";
                if (medicationProfile != null)
                {
                    medicationString = medicationProfile.Medication;
                }
                if (medicationString.IsNotNullOrEmpty() && medicationString != "")
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == Id))
                    {
                        var med = existingList.Single(m => m.Id == Id);
                        med.DCDate = dischargeDate;
                        med.LastChangedDate = DateTime.Now;
                        med.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                        database.Update<MedicationProfile>(medicationProfile);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public MedicationProfile DeleteMedication(Guid medId, Guid agencyId, Medication medication)
        {
            var medicationProfile = this.GetMedicationProfile(medId, agencyId);
            string medicationString = "";
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            if (!medicationString.IsNotNullOrEmpty())
            {
                return medicationProfile;
            }
            else
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medication.Id))
                {
                    existingList.RemoveAll(m => m.Id == medication.Id);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                    database.Update<MedicationProfile>(medicationProfile);
                }
            }
            return medicationProfile;
        }

        public bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfileHistory)
        {
            try
            {
                if (medicationProfileHistory != null)
                {
                    medicationProfileHistory.Modified = DateTime.Now;
                    database.Update<MedicationProfileHistory>(medicationProfileHistory);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool DeleteMedicationProfile(Guid Id, Guid agencyId)
        {
            try
            {
                var medication = this.GetMedicationProfile(Id, agencyId);
                if (medication != null)
                {
                    database.Delete<MedicationProfile>(medication.Id);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId)
        {
            return database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AgencyId == agencyId && m.IsDeprecated == false);
        }

        public List<MedicationProfileHistory> GetMedicationHistoryForPatientLean(Guid patientId, Guid agencyId)
        {
            var list = new List<MedicationProfileHistory>();
            var script = string.Format(@"SELECT 
                                            Id,
                                            UserId,
                                            SignedDate
                                                FROM 
                                                    medicationprofilehistories 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            PatientId = @patientid AND 
                                                            IsDeprecated = 0");
            using (var cmd = new FluentCommand<MedicationProfileHistory>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                 .SetMap(reader => new MedicationProfileHistory
                 {
                     Id = reader.GetGuid("Id"),
                     SignedDate = reader.GetDateTime("SignedDate"),
                     UserId = reader.GetGuid("UserId")
                 }).AsList();
                return list;
            }
        }


        public MedicationProfileHistory GetSignedMedicationProfileForPatientByEpisode(Guid patientId, Guid agencyId, Guid episodeId)
        {
            return database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AgencyId == agencyId && m.EpisodeId == episodeId && m.IsDeprecated == false && m.SignedDate > DateTime.MinValue).SingleOrDefault();
        }

        public MedicationProfileHistory GetSignedMedicationAssocatiedToAssessment(Guid patientId, Guid assessmentId)
        {
            var medProfileHistories = database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AssociatedAssessment == assessmentId && m.IsDeprecated == false && m.SignedDate > DateTime.MinValue);
            DateTime greatestDate = DateTime.MinValue;
            MedicationProfileHistory newestMedProfileHistory = null;
            medProfileHistories.ForEach((m) =>
            {
                if (m.SignedDate > greatestDate)
                {
                    greatestDate = m.SignedDate;
                    newestMedProfileHistory = m;
                }
            });
            return newestMedProfileHistory;
        }

        public IList<MedicationProfileHistory> GetAllMedicationProfileHistory()
        {
            return database.All<MedicationProfileHistory>().ToList();
        }

        public MedicationProfile GetMedicationProfileByPatient(Guid patientId, Guid AgencyId)
        {
            var medProfiles = database.Find<MedicationProfile>(m => m.AgencyId == AgencyId && m.PatientId == patientId);
            if (medProfiles != null)
            {
                return medProfiles.FirstOrDefault();
            }
            return null;
        }

        public MedicationProfile GetMedicationProfile(Guid Id, Guid agencyId)
        {
            return database.Single<MedicationProfile>(m => m.AgencyId == agencyId && m.Id == Id);
        }

        public bool SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            try
            {
                var medication = this.GetMedicationProfile(medicationProfile.Id, medicationProfile.AgencyId);
                if (medication != null)
                {
                    medication.PharmacyName = medicationProfile.PharmacyName;
                    medication.PharmacyPhone = medicationProfile.PharmacyPhone;
                    medication.Modified = DateTime.Now;
                    database.Update<MedicationProfile>(medication);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool AddVisitNote(PatientVisitNote patientVisitNote)
        {
            try
            {
                database.Add<PatientVisitNote>(patientVisitNote);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddNoteRelation(NoteRelation relation)
        {
            try
            {
                database.Add<NoteRelation>(relation);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateVisitNote(PatientVisitNote patientVisitNote)
        {
            var result = false;
            try
            {
                if (patientVisitNote != null)
                {
                    if (patientVisitNote.Note.IsNotNullOrEmpty())
                    {
                        patientVisitNote.Modified = DateTime.Now;
                        database.Update<PatientVisitNote>(patientVisitNote);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid noteId)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == noteId && p.PatientId == patientId && p.IsDeprecated == false);
        }
        
        public NoteRelation GetPlanOfCare(Guid parentId, Guid agencyId)
        {
            return database.Single<NoteRelation>(p => p.AgencyId == agencyId && p.ParentId == parentId && p.IsDeprecated == false);
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
        }

        public PatientVisitNote GetVisitNoteByPlanOfCare(Guid pocId, Guid agencyId)
        {
            PatientVisitNote note=null;
            var script = "SELECT patientvisitnotes.Note from patientvisitnotes inner join noterelations "+
            "on patientvisitnotes.Id=noterelations.ParentId "+
            "WHERE noterelations.Id=@id and noterelations.AgencyId=@agencyId and noterelations.IsDeprecated=0";
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                note = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("id", pocId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new PatientVisitNote
                    {
                        Note = reader.GetStringNullable("Note")
                    })
                    .AsSingle();
            }
            return note;
        }

        public List<PatientVisitNote> GetPreviousNotes(Guid patientId, Guid agencyId)
        {
            return database.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.IsDeprecated == false).OrderByDescending(p => p.Created).ToList();
        }

        public List<PatientVisitNote> GetVisitNotesByDisciplineTask(Guid patientId, Guid agencyId, DisciplineTasks task)
        {
            return database.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.NoteType == task.ToString() && p.PatientId == patientId && p.IsDeprecated == false).OrderByDescending(p => p.Created).ToList();
        }

        public List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, DisciplineTasks task, int status)
        {
            return database.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.NoteType == task.ToString() && p.PatientId == patientId && p.Status == status && p.IsDeprecated == false).OrderByDescending(p => p.Created).ToList();
        }

        public bool MarkVisitNoteAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var visitNote = database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
                if (visitNote != null)
                {
                    visitNote.IsDeprecated = isDeprecated;
                    visitNote.Modified = DateTime.Now;
                    database.Update<PatientVisitNote>(visitNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool MarkPOCAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var relation = database.Single<NoteRelation>(r => r.AgencyId == agencyId && r.Id == eventId);
                if (relation != null)
                {
                    relation.IsDeprecated = isDeprecated;
                    database.Update<NoteRelation>(relation);
                    result = true;
                }
                var visitNote = database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
                if (visitNote != null)
                {
                    visitNote.IsDeprecated = isDeprecated;
                    visitNote.Modified = DateTime.Now;
                    database.Update<PatientVisitNote>(visitNote);
                    result =result && true;
                }
                
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignNotesUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var visitNote = database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
                if (visitNote != null)
                {
                    try
                    {
                        visitNote.UserId = employeeId;
                        visitNote.Modified = DateTime.Now;
                        database.Update<PatientVisitNote>(visitNote);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId)
        {
            var noteType = DisciplineTasks.HHAideCarePlan.ToString();
            return database.Find<PatientVisitNote>(p => p.EpisodeId == episodeId && p.PatientId == patientId && p.NoteType == noteType && p.IsDeprecated == false).LastOrDefault();
        }

        public PatientVisitNote GetVisitNoteByType(Guid episodeId, Guid patientId, DisciplineTasks disciplineTask)
        {
            var noteType = disciplineTask.ToString();
            var notes = database.Find<PatientVisitNote>(p => p.EpisodeId == episodeId && p.PatientId == patientId && p.NoteType == noteType && p.IsDeprecated == false);
            return notes.LastOrDefault();
        }

        public ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId)
        {
            ScheduleEvent scheduleEvent = null;
            var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId && e.PatientId == patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var status = new string[] { 
                    //((int)ScheduleStatus.NoteNotStarted).ToString(),
                    //((int)ScheduleStatus.NoteNotYetDue).ToString(),
                    //((int)ScheduleStatus.NoteSaved).ToString(),
                    //((int)ScheduleStatus.NoteSubmittedWithSignature).ToString(),
                    ((int)ScheduleStatus.NoteCompleted).ToString(),
                    //((int)ScheduleStatus.NoteReopened).ToString(),
                    //((int)ScheduleStatus.NoteReturned).ToString(),
                    //((int)ScheduleStatus.AwaitingClinicianSignature).ToString(),

                    //((int)ScheduleStatus.NoteReturnedForClinicianSignature).ToString(),

                    //((int)ScheduleStatus.EvalReturnedByPhysician).ToString(),

                    ((int)ScheduleStatus.OasisCompletedExportReady).ToString(),
                    ((int)ScheduleStatus.OasisExported).ToString(),
                    ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                    //((int)ScheduleStatus.OasisNotYetDue).ToString(),
                    //((int)ScheduleStatus.OasisNotStarted).ToString(),
                    //((int)ScheduleStatus.OasisSaved).ToString(),
                    //((int)ScheduleStatus.OasisCompletedPendingReview).ToString(),
                    //((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString(),
                    //((int)ScheduleStatus.OasisReopened).ToString(),

                    ((int)ScheduleStatus.EvalToBeSentToPhysician).ToString(),
                    ((int)ScheduleStatus.EvalSentToPhysician).ToString(),
                    ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                    ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString()
                };
                scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && s.IsBillable && status.Contains(s.Status) && !s.IsDeprecated && !s.IsMissedVisit).OrderBy(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
            }
            return scheduleEvent;
        }

        public bool IsFirstScheduledVisit(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId && e.PatientId == patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit).OrderBy(s=>s.EventDate.ToDateTime()).FirstOrDefault();
                if (scheduleEvent != null)
                {
                    if (scheduleEvent.EventId == eventId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsFirstBillableVisit(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var result = false;
            try
            {
                var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId && e.PatientId == patientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit);
                    if (scheduleEvents != null)
                    {
                        foreach (var scheduleEvent in scheduleEvents)
                        {
                            var status = scheduleEvent.Status;
                            if (scheduleEvent.IsBillable)
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool IsPatientIdExist(Guid agencyId, string patientIdNumber)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.PatientIdNumber == patientIdNumber && p.IsDeprecated == false);
        }

        public bool IsMedicareExist(Guid agencyId, string medicareNumber)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.MedicareNumber == medicareNumber && p.IsDeprecated == false);
        }

        public bool IsMedicaidExist(Guid agencyId, string medicaidNumber)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.MedicaidNumber == medicaidNumber && p.IsDeprecated == false);
        }

        public bool IsSSNExist(Guid agencyId, string ssn)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.SSN == ssn && p.IsDeprecated == false);
        }

        public bool IsPatientIdExistForEdit(Guid agencyId, Guid patientId, string patientIdNumber)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.Id != patientId && p.PatientIdNumber == patientIdNumber && p.IsDeprecated == false);
        }

        public bool IsMedicareExistForEdit(Guid agencyId, Guid patientId, string medicareNumber)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.Id != patientId && p.MedicareNumber == medicareNumber && p.IsDeprecated == false);
        }

        public bool IsMedicaidExistForEdit(Guid agencyId, Guid patientId, string medicaidNumber)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.Id != patientId && p.MedicaidNumber == medicaidNumber && p.IsDeprecated == false);
        }

        public bool IsSSNExistForEdit(Guid agencyId, Guid patientId, string ssn)
        {
            return database.Exists<Patient>(p => p.AgencyId == agencyId && p.Id != patientId && p.SSN == ssn && p.IsDeprecated == false);
        }

        public bool IsEpisodeExist(Guid agencyId, Guid episodeId)
        {
            var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId);
            if (episode != null)
            {
                return true;
            }
            return false;
        }

        public bool IsPatientExist(Guid agencyId, Guid patientId)
        {
            var patient = database.Single<Patient>(e => e.AgencyId == agencyId && e.Id == patientId);
            if (patient != null)
            {
                return true;
            }
            return false;
        }

        public List<PatientEpisodeData> GetEpisodeByBranch(Guid branchCode, Guid agencyId)
        {

            var script = @"SELECT patients.FirstName, patients.LastName, patients.MiddleInitial, patients.PatientIdNumber, " +
               "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
               "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
               "WHERE patientepisodes.AgencyId = @agencyid AND patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND (patients.Status = 1 || patients.Status = 2) " +
               (!branchCode.IsEmpty() ? "AND patients.AgencyLocationId = '" + branchCode.ToString() + "' " : string.Empty) +
               "AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0 ";

            var list = new List<PatientEpisodeData>();
            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientEpisodeData
                {
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                    StartDate = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            return list;
        }

        public bool AddFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            try
            {
                if (faceToFaceEncounter != null)
                {
                    database.Add<FaceToFaceEncounter>(faceToFaceEncounter);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid agencyId)
        {
            return database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.Id == Id);
        }

        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId)
        {
            return database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
        }

        public List<FaceToFaceEncounter> GetAllFaceToFaceEncounters()
        {
            return database.All<FaceToFaceEncounter>().ToList();
        }

        public bool UpdateFaceToFaceEncounterForRequest(Guid agencyId, Guid orderId, int status, DateTime dateSent)
        {
            Check.Argument.IsNotEmpty(orderId, "orderId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            bool result = false;
            try
            {
                var order = database.Single<FaceToFaceEncounter>(o => o.Id == orderId && o.AgencyId == agencyId && o.IsDeprecated == false);
                if (order != null)
                {
                    order.Status = status;
                    order.SentDate = dateSent;
                    database.Update<FaceToFaceEncounter>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                if (faceToFaceEncounter != null)
                {
                    try
                    {
                        faceToFaceEncounter.Modified = DateTime.Now;
                        database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool RemoveFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
                if (faceToFaceEncounter != null)
                {
                    database.Delete<FaceToFaceEncounter>(faceToFaceEncounter.Id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id, bool IsDeprecated)
        {
            var result = false;
            try
            {
                var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.IsDeprecated = IsDeprecated;
                    faceToFaceEncounter.Modified = DateTime.Now;
                    database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignFaceToFaceEncounterUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
            if (faceToFaceEncounter != null)
            {
                try
                {
                    faceToFaceEncounter.UserId = employeeId;
                    database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public List<FaceToFaceEncounter> GetFaceToFaceEncounterOrders(Guid agencyId, int status, string orderIds)
        {

            var script = string.Format(@"SELECT facetofaceencounters.Id as Id, facetofaceencounters.EpisodeId as EpisodeId, facetofaceencounters.OrderNumber as OrderNumber, facetofaceencounters.PhysicianId as PhysicianId , patients.Id as PatientId , patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, facetofaceencounters.Status as Status ,  facetofaceencounters.ReceivedDate as ReceivedDate, facetofaceencounters.RequestDate as RequestDate ,facetofaceencounters.SentDate as SentDate, facetofaceencounters.SignatureDate as SignatureDate " +
               "FROM facetofaceencounters INNER JOIN patients ON facetofaceencounters.PatientId = patients.Id  " +
               "WHERE facetofaceencounters.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) " +
               "AND facetofaceencounters.IsDeprecated = 0 AND facetofaceencounters.Id IN ( {0} ) AND facetofaceencounters.Status = @status ", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    SentDate = reader.GetDateTime("SentDate"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                })
                .AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetPatientFaceToFaceEncounterOrders(Guid agencyId, Guid patientId, string orderIds)
        {

            var script = string.Format(@"SELECT facetofaceencounters.Id as Id, facetofaceencounters.EpisodeId as EpisodeId, facetofaceencounters.OrderNumber as OrderNumber, facetofaceencounters.PhysicianId as PhysicianId , patients.Id as PatientId ,  facetofaceencounters.Status as Status ,  facetofaceencounters.ReceivedDate as ReceivedDate, facetofaceencounters.RequestDate as RequestDate " +
               "FROM facetofaceencounters INNER JOIN patients ON facetofaceencounters.PatientId = patients.Id  " +
               "WHERE facetofaceencounters.AgencyId = @agencyid AND facetofaceencounters.PatientId = @patientId " +
               "AND facetofaceencounters.IsDeprecated = 0 AND facetofaceencounters.Id IN ( {0} ) ", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    PhysicianId = reader.GetGuid("PhysicianId")
                })
                .AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetPendingSignatureFaceToFaceEncounterOrders(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT facetofaceencounters.Id as Id, facetofaceencounters.EpisodeId as EpisodeId, facetofaceencounters.OrderNumber as OrderNumber, facetofaceencounters.PhysicianId as PhysicianId , patients.Id as PatientId , patients.FirstName as FirstName, patients.LastName as LastName,  facetofaceencounters.Status as Status ,  facetofaceencounters.ReceivedDate as ReceivedDate, facetofaceencounters.RequestDate as RequestDate, facetofaceencounters.SentDate as SentDate " +
               "FROM facetofaceencounters INNER JOIN patients ON facetofaceencounters.PatientId = patients.Id  " +
               "WHERE facetofaceencounters.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND ( patients.Status = 1 || patients.Status = 2 ) " +
               "AND facetofaceencounters.IsDeprecated = 0 AND facetofaceencounters.Id IN ( {0} ) AND ( facetofaceencounters.Status = 130 || facetofaceencounters.Status = 145 )", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianId = reader.GetGuid("PhysicianId")
                }).AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetCommunicationNoteScheduleEvents(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patients.Id as PatientId , " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 {0} {1} " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))", status <= 0 ? " AND (patients.Status = 1 || patients.Status = 2)" : " AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);

            var list = new List<EpisodeFiltered>();
            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<EpisodeFiltered>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new EpisodeFiltered
                {
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule"),
                })
                .AsList();
            }
            if (list != null && list.Count > 0)
            {
                list.ForEach(l =>
                {
                    if (l.Schedule.IsNotNullOrEmpty())
                    {
                        var events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && (s.DisciplineTask == (int)DisciplineTasks.CommunicationNote)).ToList();
                        if (events != null && events.Count > 0)
                        {
                            scheduleList.AddRange(events);
                        }
                    }
                });
            }
            return scheduleList;
        }
        public List<Patient> GetPatientByPhysician(Guid physicianId)
        {
            List<PatientPhysician> patientPhysicians = database.Find<PatientPhysician>(p => p.PhysicianId == physicianId).ToList();
            var patients = new List<Patient>();
            patientPhysicians.ForEach(pa =>
                {
                    var patient = database.Find<Patient>(p => p.Id == pa.PatientId).SingleOrDefault();
                    patients.Add(patient);
                });
            return patients;
        }

        public List<ScheduleEvent> GetPhysicianOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int status)
        {
            var script = @"SELECT patients.Id as PatientId , patients.PatientIdNumber as PatientIdNumber , patients.FirstName, patients.LastName, patients.MiddleInitial, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND (patients.Status = 1 || patients.Status = 2) " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))";

            var list = new List<EpisodeFiltered>();
            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<EpisodeFiltered>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new EpisodeFiltered
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            if (list != null && list.Count > 0)
            {
                list.ForEach(l =>
                {
                    if (l.Schedule.IsNotNullOrEmpty())
                    {
                        var events = new List<ScheduleEvent>();
                        if (status == 000)
                        {
                            events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && (s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)).ToList();
                        }
                        else
                        {
                            events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && (s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && s.Status == status.ToString()).ToList();
                        }
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(ev => { ev.StartDate = l.StartDate; ev.EndDate = l.EndDate; ev.PatientName = l.PatientName; });
                            scheduleList.AddRange(events);
                        }
                    }

                });
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int status)
        {
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND patients.AgencyLocationId = @branchId ";
            }

            var script = string.Format(@"SELECT patients.Id as PatientId , patients.PatientIdNumber as PatientIdNumber , patients.FirstName, patients.LastName, patients.MiddleInitial, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND (patients.Status = 1 || patients.Status = 2) " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 {0}" +
                "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))"
                , branchScript);

            var list = new List<EpisodeFiltered>();
            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<EpisodeFiltered>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new EpisodeFiltered
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            if (list != null && list.Count > 0)
            {
                list.ForEach(l =>
                {
                    if (l.Schedule.IsNotNullOrEmpty())
                    {
                        var events = new List<ScheduleEvent>();
                        if (status == 000)
                        {
                            events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && (s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)).ToList();
                        }
                        else
                        {
                            events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && (s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone) && s.Status == status.ToString()).ToList();
                        }
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(ev => { ev.StartDate = l.StartDate; ev.EndDate = l.EndDate; ev.PatientName = l.PatientName; });
                            scheduleList.AddRange(events);
                        }
                    }

                });
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var script = string.Format(@"SELECT patients.Id as PatientId , patients.PatientIdNumber as PatientIdNumber , patients.FirstName, patients.LastName, patients.MiddleInitial, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND (patients.Status = 1 || patients.Status = 2) " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0  {0} " +
                "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate " +
                "OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate " +
                "and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))", !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId " : string.Empty);

            var list = new List<EpisodeFiltered>();
            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<EpisodeFiltered>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new EpisodeFiltered
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            if (list != null && list.Count > 0)
            {
                list.ForEach(l =>
                {
                    if (l.Schedule.IsNotNullOrEmpty())
                    {
                        var events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                            && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter
                            || s.DisciplineTask == (int)DisciplineTasks.HCFA485
                            || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485
                            || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder
                            || s.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.STEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.PTReassessment
                            || s.DisciplineTask == (int)DisciplineTasks.OTReassessment
                            || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment
                            || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone
                            || s.DisciplineTask == (int)DisciplineTasks.PTDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.OTDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.MSWDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.STDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary
                            || s.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary
                            || s.DisciplineTask == (int)DisciplineTasks.TenDaySummary
                            || s.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.STPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment)
                            && s.Status.IsNotNullOrEmpty() && s.Status.IsInteger() && status.Contains(s.Status.ToInteger())).ToList();
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(ev => { ev.StartDate = l.StartDate; ev.EndDate = l.EndDate; ev.PatientName = l.PatientName; });
                            scheduleList.AddRange(events);
                        }
                    }

                });
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT patients.Id as PatientId , " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientId " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))";

            var list = new List<EpisodeFiltered>();
            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<EpisodeFiltered>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new EpisodeFiltered
                {
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsList();
            }
            if (list != null && list.Count > 0)
            {
                list.ForEach(l =>
                {
                    if (l.Schedule.IsNotNullOrEmpty())
                    {
                        var events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate()
                            && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                            && s.EventDate.ToDateTime().Date >= l.StartDate.Date
                            && s.EventDate.ToDateTime().Date <= l.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter
                            || s.DisciplineTask == (int)DisciplineTasks.HCFA485
                            || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485
                            || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder
                            || s.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.STEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.PTReassessment
                            || s.DisciplineTask == (int)DisciplineTasks.OTReassessment
                            || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment
                            || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone
                            || s.DisciplineTask == (int)DisciplineTasks.PTDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.OTDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.MSWDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.STDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.STPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment
                            || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)).ToList();
                        if (events != null && events.Count > 0)
                        {
                            scheduleList.AddRange(events);
                        }
                    }

                });
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patients.Id as PatientId , patients.PatientIdNumber as PatientIdNumber , patients.FirstName, patients.LastName, patients.MiddleInitial, " +
                "patientepisodes.EndDate, patientepisodes.StartDate, patientepisodes.Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND (patients.Status = 1 || patients.Status = 2) {0} " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate between @startdate and @enddate || patientepisodes.EndDate between @startdate and @enddate OR (@startdate between  patientepisodes.StartDate and patientepisodes.EndDate AND @enddate  between patientepisodes.StartDate and patientepisodes.EndDate) OR ( patientepisodes.StartDate  between  @startdate and  @enddate  AND patientepisodes.EndDate  between @startdate and  @enddate ))", !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty);

            var list = new List<EpisodeFiltered>();
            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<EpisodeFiltered>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new EpisodeFiltered
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            if (list != null && list.Count > 0)
            {
                list.ForEach(l =>
                {
                    if (l.Schedule.IsNotNullOrEmpty())
                    {
                        var events = l.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                            && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter
                            || s.DisciplineTask == (int)DisciplineTasks.HCFA485
                            || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485
                            || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder
                            || s.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.STEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                            || s.DisciplineTask == (int)DisciplineTasks.PTReassessment
                            || s.DisciplineTask == (int)DisciplineTasks.OTReassessment
                            || s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment
                            || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone
                            || s.DisciplineTask == (int)DisciplineTasks.PTDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.OTDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.MSWDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.STDischarge
                            || s.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.STPlanOfCare
                            || s.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment
                            || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary
                            || s.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary
                            || s.DisciplineTask == (int)DisciplineTasks.TenDaySummary)
                            && (s.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString()
                            || s.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString()
                            || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString()
                            || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString())).ToList();
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(ev => { ev.StartDate = l.StartDate; ev.EndDate = l.EndDate; ev.PatientName = l.PatientName; });
                            scheduleList.AddRange(events);
                        }
                    }

                });
            }
            return scheduleList;
        }

        public List<PatientSocCertPeriod> PatientSocCertPeriods(Guid agencyId, string patientIds, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientSocCertPeriod>();
            var script = string.Format(@"SELECT patientepisodes.PatientId as PatientId , patientadmissiondates.PatientData as PatientData, " +
                 " patientepisodes.EndDate as EndDate, patientepisodes.StartDate as StartDate, patientepisodes.Details as Details " +
                 " FROM  patientepisodes " +
                 " LEFT JOIN patientadmissiondates ON patientepisodes.PatientId = patientadmissiondates.PatientId AND patientepisodes.AdmissionId = patientadmissiondates.Id  " +
                 " WHERE patientepisodes.PatientId IN ( {0} ) AND patientepisodes.AgencyId = @agencyid  AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0 AND DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate)", patientIds.ToString());

            using (var cmd = new FluentCommand<PatientSocCertPeriod>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientSocCertPeriod
                {
                    Id = reader.GetGuid("PatientId"),
                    SocCertPeriod = string.Format("{0} - {1}", reader.GetDateTime("StartDate").ToString("MM/dd/yyyy"), reader.GetDateTime("EndDate").ToString("MM/dd/yyyy")),
                    details = reader.GetStringNullable("Details").ToObject < EpisodeDetail>(),
                    PatientData = reader.GetStringNullable("PatientData"),
                }).AsList();
            }
            return list;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT patients.FirstName , patients.LastName , patients.MiddleInitial ,patients.PatientIdNumber ,  " +
              " patients.AddressLine1 , patients.AddressLine2 , patients.AddressCity , patients.AddressStateCode , patients.AddressZipCode , patients.PhoneHome , patients.Gender  FROM patients INNER JOIN patientphysicians " +
              " ON patientphysicians.PatientId = patients.Id WHERE  patients.AgencyId = @agencyId AND patientphysicians.PhysicianId = @agencyPhysicianId " +
              " AND (patients.Status = 1 OR patients.Status = 2) AND patients.IsDeprecated = 0 ORDER BY LastName ASC , FirstName ASC");
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("agencyPhysicianId", agencyPhysicianId)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PatientGender = reader.GetStringNullable("Gender")
                    }).AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId, int status)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT patients.FirstName , patients.LastName , patients.MiddleInitial ,patients.PatientIdNumber ,  " +
              " patients.AddressLine1 , patients.AddressLine2 , patients.AddressCity , patients.AddressStateCode , patients.AddressZipCode , patients.PhoneHome , patients.Gender  FROM patients INNER JOIN patientphysicians " +
              " ON patientphysicians.PatientId = patients.Id WHERE  patients.AgencyId = @agencyId AND patientphysicians.PhysicianId = @agencyPhysicianId" +
              " AND patients.Status = @status AND patients.IsDeprecated = 0 ORDER BY LastName ASC , FirstName ASC");
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("agencyPhysicianId", agencyPhysicianId)
                    .AddInt("status", status)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PatientGender = reader.GetStringNullable("Gender")
                    }).AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<PatientRoster> GetPatientByResponsiableEmployee(Guid agencyId, Guid userId, Guid branchId, int status)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT FirstName , LastName , MiddleInitial, PatientIdNumber ,  " +
              " AddressLine1 , AddressLine2 , AddressCity , AddressStateCode , AddressZipCode , StartofCareDate   FROM patients " +
              " WHERE  AgencyId = @agencyId AND UserId = @userId " +
              " {0} {1} AND IsDeprecated = 0 ORDER BY LastName ASC , FirstName ASC ", status == 0 ? " AND (Status = 1 || Status = 2)" : " AND Status = " + status, !branchId.IsEmpty() ? "AND AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddGuid("userId", userId)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientSoC = reader.GetDateTime("StartofCareDate").ToString("MM/dd/yyyy")
                    }).AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid agencyId, Guid caseManagerId, Guid branchId, int status)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT FirstName , LastName , MiddleInitial, PatientIdNumber ,  " +
              " AddressLine1 , AddressLine2 , AddressCity , AddressStateCode , AddressZipCode , StartofCareDate , PhoneHome  FROM patients " +
              " WHERE  AgencyId = @agencyId AND CaseManagerId = @caseManagerId " +
              " {0} {1} AND IsDeprecated = 0 ORDER BY LastName ASC , FirstName ASC", status == 0 ? " AND (Status = 1 || Status = 2)" : " AND Status = " + status, !branchId.IsEmpty() ? "AND AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddGuid("caseManagerId", caseManagerId)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientSoC = reader.GetDateTime("StartofCareDate").ToString("MM/dd/yyyy"),
                        PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone()
                    }).AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<PatientRoster> GetPatientByInsurance(Guid agencyId, Guid branchId, int insuranceId, int status)
        {
            var agencyPhysicianPatients = new List<PatientRoster>();
            var script = string.Format(@"SELECT FirstName , LastName , MiddleInitial, PatientIdNumber ,  " +
              " AddressLine1 , AddressLine2 , AddressCity , AddressStateCode , AddressZipCode , patients.PrimaryInsurance, patients.PrimaryHealthPlanId, patients.PhoneHome , patients.Gender  FROM patients " +
              " WHERE  AgencyId = @agencyId AND PrimaryInsurance = @insuranceId " +
              " {0} {1} AND IsDeprecated = 0", status == 0 ? " AND (Status = 1 || Status = 2)" : " AND Status = " + status, !branchId.IsEmpty() ? "AND AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                agencyPhysicianPatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("insuranceId", insuranceId)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                        PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                        PatientAddressCity = reader.GetStringNullable("AddressCity"),
                        PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                        PatientGender = reader.GetStringNullable("Gender"),
                        PatientInsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                        PatientInsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    })
                    .AsList();
            }
            return agencyPhysicianPatients;
        }

        public List<PatientRoster> GetPatientByAdmissionMonthYear(Guid agencyId, Guid branchId, int status, int month, int year)
        {
            var patientsByAdmission = new List<PatientRoster>();
            var script = string.Format(
                    @"SELECT patientadmissiondates.PatientData, patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial, patients.PatientIdNumber as PatientIdNumber, " +
                    "patients.AdmissionSource as AdmissionSource, patientadmissiondates.StartOfCareDate as StartOfCareDate, " +
                    "patients.OtherReferralSource, patients.InternalReferral, patients.ReferrerPhysician, patients.ReferralDate, patients.Status " +
                    "FROM patientadmissiondates INNER JOIN patients ON patientadmissiondates.PatientId = patients.Id " +
                    "WHERE patients.AgencyId = @agencyId AND month(patientadmissiondates.StartOfCareDate) = @month AND " +
                    "year(patientadmissiondates.StartOfCareDate) = @year {0} {1} AND patients.IsDeprecated = 0  AND " +
                    "patientadmissiondates.IsDeprecated = 0 GROUP BY patients.Id, patientadmissiondates.StartOfCareDate ORDER BY " +
                    "patients.LastName ASC, patients.FirstName ASC, patientadmissiondates.StartOfCareDate DESC ", status == 0 ? "AND " +
                    "(patients.Status = 1 || patients.Status = 2)" : "AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                patientsByAdmission = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("month", month)
                    .AddInt("year", year)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientStatus = reader.GetInt("Status"),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        OtherReferralSource = reader.GetStringNullable("OtherReferralSource"),
                        InternalReferral = reader.GetGuid("InternalReferral") != Guid.Empty ? UserEngine.GetName(reader.GetGuid("InternalReferral"), agencyId) : string.Empty,
                        ReferrerPhysician = reader.GetGuid("ReferrerPhysician") != Guid.Empty ? PhysicianEngine.GetName(reader.GetGuid("ReferrerPhysician"), agencyId) : string.Empty,
                        //ReferralDate = reader.GetDateTime("ReferralDate") != DateTime.MinValue ? reader.GetDateTime("ReferralDate").ToString("MM/dd/yyyy") : string.Empty,
                        ReferralDate = reader.GetStringNullable("PatientData").ToObject<Patient>().ReferralDate != DateTime.MinValue ? reader.GetStringNullable("PatientData").ToObject<Patient>().ReferralDate.ToString("MM/dd/yyyy") : string.Empty,
                        PatientSoC = reader.GetDateTime("StartOfCareDate").ToString("MM/dd/yyyy")
                    }).AsList();
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionMonthYearUnduplicated(Guid agencyId, Guid branchId, int status, int year)
        {
            var patientsByAdmission = new List<PatientRoster>();
            var script = string.Format(@"SELECT patients.Id , patients.FirstName as FirstName , patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, patients.PatientIdNumber as PatientIdNumber  ,  " +
              " patients.AdmissionSource as AdmissionSource , max(patientadmissiondates.StartOfCareDate) as StartOfCareDate  FROM patientadmissiondates  INNER JOIN patients  ON patientadmissiondates.PatientId = patients.Id " +
              " WHERE  patients.AgencyId = @agencyId  AND year(patientadmissiondates.StartOfCareDate) = @year " +
              " {0} {1} AND patients.IsDeprecated = 0 AND patientadmissiondates.IsDeprecated = 0 GROUP BY patients.Id  ORDER BY patients.LastName ASC , patients.FirstName ASC , patientadmissiondates.StartOfCareDate DESC ", status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)" : " AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                patientsByAdmission = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("year", year)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                        PatientSoC = reader.GetDateTime("StartOfCareDate").ToString("MM/dd/yyyy")
                    }).AsList();
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionUnduplicatedByDateRange(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var patientsByAdmission = new List<PatientRoster>();
            var script = string.Format(@"SELECT 
                                        patients.Id ,
                                        patients.FirstName as FirstName , 
                                        patients.LastName as LastName , 
                                        patients.MiddleInitial as MiddleInitial,
                                        patients.PatientIdNumber as PatientIdNumber  , 
                                        patients.Status as Status ,  
                                        patients.AdmissionSource as AdmissionSource  ,
                                        patientadmissiondates.DischargedDate as DischargeDate ,
                                        max(patientadmissiondates.StartOfCareDate) as StartOfCareDate 
                                            FROM patientadmissiondates  
                                                INNER JOIN patients  ON patientadmissiondates.PatientId = patients.Id 
                                                    WHERE 
                                                        patients.AgencyId = @agencyId  AND
                                                        DATE(patientadmissiondates.StartOfCareDate) between DATE(@startDate) and DATE(@enddate)  {0} {1} AND
                                                        patients.IsDeprecated = 0 AND
                                                        patientadmissiondates.IsDeprecated = 0 
                                                            GROUP BY patients.Id 
                                                                ORDER BY patients.LastName ASC , patients.FirstName ASC , patientadmissiondates.StartOfCareDate DESC ",
                                                                                                          status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)" : " AND patients.Status = " + status,
                                                                                                      !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                patientsByAdmission = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                        PatientSoC = reader.GetDateTime("StartOfCareDate").ToString("MM/dd/yyyy"),
                        PatientStatus = reader.GetInt("Status"),
                        PatientDischargeDate = reader.GetInt("Status") == 2 && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate").ToString("MM/dd/yyyy") : string.Empty
                    }).AsList();
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionUnduplicatedServiceByDateRange(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var patientsByAdmission = new List<PatientRoster>();
            var script = string.Format(@"SELECT 
                                        patients.Id ,
                                        patients.FirstName as FirstName , 
                                        patients.LastName as LastName , 
                                        patients.MiddleInitial as MiddleInitial,
                                        patients.PatientIdNumber as PatientIdNumber  , 
                                        patients.Status as Status ,   
                                        patients.AdmissionSource as AdmissionSource  ,
                                        patientadmissiondates.DischargedDate as DischargeDate ,
                                        max(patientadmissiondates.StartOfCareDate) as StartOfCareDate 
                                            FROM patientadmissiondates  
                                                INNER JOIN patients  ON patientadmissiondates.PatientId = patients.Id 
                                                    WHERE 
                                                        patients.AgencyId = @agencyId  AND
                                                        (DATE(patientadmissiondates.StartOfCareDate) between DATE(@startDate) and DATE(@enddate) || (DATE(patientadmissiondates.DischargedDate) between DATE(@startDate) and DATE(@enddate) && DATE(patientadmissiondates.DischargedDate) >DATE(patientadmissiondates.StartOfCareDate)) || ( DATE(patientadmissiondates.StartOfCareDate)<DATE(@startDate) AND patients.Status = 1 )||(DATE(patientadmissiondates.StartOfCareDate)<DATE(@startDate) && DATE(patientadmissiondates.DischargedDate)>DATE(@enddate) && DATE(patientadmissiondates.DischargedDate)>DATE(patientadmissiondates.StartOfCareDate)&& patients.Status = 1)) {0} {1} AND
                                                        patients.IsDeprecated = 0 AND
                                                        patientadmissiondates.IsDeprecated = 0 
                                                            GROUP BY patients.Id 
                                                                ORDER BY patients.LastName ASC , patients.FirstName ASC , patientadmissiondates.StartOfCareDate DESC ",
                                                                                                          status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)" : " AND patients.Status = " + status,
                                                                                                      !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                patientsByAdmission = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                        PatientSoC = reader.GetDateTime("StartOfCareDate").ToString("MM/dd/yyyy"),
                        PatientStatus = reader.GetInt("Status"),
                        PatientDischargeDate = reader.GetInt("Status") == 2 && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate").ToString("MM/dd/yyyy") : string.Empty
                    }).AsList();
            }
            return patientsByAdmission;
        }

        public List<PatientRoster> GetPatientByAdmissionYear(Guid agencyId, Guid branchId, int status, int year)
        {
            var patientsByAdmission = new List<PatientRoster>();
            var script = string.Format(@"SELECT patients.FirstName as FirstName , patients.LastName as LastName , patients.MiddleInitial as MiddleInitial, patients.PatientIdNumber as PatientIdNumber  ,  " +
              " patients.AdmissionSource as AdmissionSource , patientadmissiondates.StartOfCareDate as StartOfCareDate  FROM patientadmissiondates  INNER JOIN patients  ON patientadmissiondates.PatientId = patients.Id " +
              " WHERE  patients.AgencyId = @agencyId  AND year(patientadmissiondates.StartOfCareDate) = @year " +
              " {0} {1} AND patients.IsDeprecated = 0 AND patientadmissiondates.IsDeprecated = 0 GROUP BY patients.Id, patientadmissiondates.StartOfCareDate ORDER BY patients.LastName ASC , patients.FirstName ASC , patientadmissiondates.StartOfCareDate DESC ", status == 0 ? " AND (patients.Status = 1 || patients.Status = 2)" : " AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                patientsByAdmission = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("year", year)
                    .SetMap(reader => new PatientRoster
                    {
                        PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientId = reader.GetStringNullable("PatientIdNumber"),
                        AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                        PatientSoC = reader.GetDateTime("StartOfCareDate").ToString("MM/dd/yyyy")
                    }).AsList();
            }
            return patientsByAdmission;
        }

        public List<DischargePatient> GetDischargePatients(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var dischargePatients = new List<DischargePatient>();
            var script = string.Format(@"SELECT p.FirstName, p.LastName, p.MiddleInitial, p.PatientIdNumber, p.DischargeReasonId as CurrentReasonId ,p.DischargeReason as CurrentReason, pa.DischargeReasonId as DischargeReasonId , " +
                " pa.StartofCareDate, pa.DischargedDate, pa.PatientData, pa.Reason, p.AdmissionId = pa.Id as IsTheSameAdmission " +
                " FROM patients p " +
                " JOIN patientadmissiondates pa on p.Id = pa.PatientId " +
                " WHERE pa.AgencyId = @agencyId AND (pa.Status = 2) AND p.IsDeprecated = 0 AND pa.IsDeprecated = 0 " +
                " AND DATE(pa.DischargedDate) between DATE(@startdate) and DATE(@enddate) {0} " +
                " ORDER BY p.LastName ASC, p.FirstName ASC ", !branchId.IsEmpty() ? "AND p.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<DischargePatient>(script))
            {
                dischargePatients = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new DischargePatient
                    {
                        FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                        LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        StartofCareDate = reader.GetDateTime("StartofCareDate"),
                        PatientData = reader.GetStringNullable("PatientData"),
                        DischargeReason = reader.GetStringNullable("Reason"),
                        CurrentDischargeReason = reader.GetStringNullable("CurrentReason"),
                        DischargeDate = reader.GetDateTime("DischargedDate"),
                        DischargeReasonId = reader.GetInt("DischargeReasonId"),
                        CurrentDischargeReasonId = reader.GetInt("CurrentReasonId"),
                        IsTheSameAdmission = reader.GetBoolean("IsTheSameAdmission")
                    }).AsList();
            }

            foreach (var patient in dischargePatients)
            {
                if (patient.IsTheSameAdmission)
                {
                    if (patient.CurrentDischargeReasonId != (int)DischargeReasons.Other)
                    {
                        patient.DischargeReason = string.Empty;
                        patient.DischargeReasonId = patient.CurrentDischargeReasonId;
                    }
                    else 
                    {
                        patient.DischargeReason = patient.CurrentDischargeReason;
                        patient.DischargeReasonId = patient.CurrentDischargeReasonId;
                    }
                }
                else
                {
                    var patientData = patient.PatientData.ToObject<Patient>();
                    if (patientData != null)
                    {
                        if (patientData.DischargeReasonId > 0)
                        {
                            patient.DischargeReason = patientData.DischargeReason;
                            patient.DischargeReasonId = patientData.DischargeReasonId;
                            if (patientData.DischargeReasonId != (int)DischargeReasons.Other)
                            {
                                patient.DischargeReason = string.Empty;
                            }
                        }
                    }
                    else
                    {
                        patient.DischargeReasonId = (int)DischargeReasons.None;
                        patient.DischargeReason = string.Empty;
                    }
                }
            }
            return dischargePatients;
        }

        public List<PatientSelection> GetUserPatients(Guid agencyId, Guid userId, byte statusId)
        {
            var patients = new List<PatientSelection>();
            if (!userId.IsEmpty() && statusId > 0)
            {
                var patientEpisodes = GetPatientEpisodeData(agencyId, statusId);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleList = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.UserId == userId && e.IsDeprecated == false).ToList();
                            if (scheduleList != null && scheduleList.Count > 0)
                            {
                                scheduleList.ForEach(scheduleEvent =>
                                {
                                    if (!patients.Exists(p => p.Id == patientEpisode.PatientId))
                                    {
                                        if (!patientEpisode.IsDischarged)
                                        {
                                            patients.Add(new PatientSelection
                                            {
                                                DOB = patientEpisode.DOB,
                                                Id = patientEpisode.PatientId,
                                                MI = patientEpisode.MiddleInitial.ToInitial(),
                                                LastName = patientEpisode.LastName.ToUpperCase(),
                                                FirstName = patientEpisode.FirstName.ToUpperCase(),
                                                PhoneNumber = patientEpisode.PhoneNumber,
                                                IsDischarged = patientEpisode.IsDischarged
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
            return patients.OrderBy(p => p.LastName).ToList();
        }

        public List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId)
        {
            var patients = new List<PatientSelection>();
            if (!userId.IsEmpty() && statusId > 0)
            {
                var patientEpisodes = GetPatientEpisodeData(agencyId, branchId, statusId);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleList = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.UserId == userId && e.IsDeprecated == false).ToList();
                            if (scheduleList != null && scheduleList.Count > 0)
                            {
                                scheduleList.ForEach(scheduleEvent =>
                                {
                                    if (!patients.Exists(p => p.Id == patientEpisode.PatientId))
                                    {
                                        patients.Add(new PatientSelection
                                        {
                                            DOB = patientEpisode.DOB,
                                            Id = patientEpisode.PatientId,
                                            MI = patientEpisode.MiddleInitial,
                                            LastName = patientEpisode.LastName.ToUpperCase(),
                                            FirstName = patientEpisode.FirstName.ToUpperCase(),
                                            PhoneNumber = patientEpisode.PhoneNumber,
                                            IsDischarged = patientEpisode.IsDischarged
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
            return patients.OrderBy(p => p.LastName).ToList();
        }

        public List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId)
        {
            var patients = new List<PatientSelection>();
            if (!userId.IsEmpty() && statusId > 0)
            {
                var patientEpisodes = GetPatientEpisodeData(agencyId, branchId, statusId, insuranceId);
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleList = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.UserId == userId && e.IsDeprecated == false).ToList();
                            if (scheduleList != null && scheduleList.Count > 0)
                            {
                                scheduleList.ForEach(scheduleEvent =>
                                {
                                    if (!patients.Exists(p => p.Id == patientEpisode.PatientId))
                                    {
                                        patients.Add(new PatientSelection
                                        {
                                            Id = patientEpisode.PatientId,
                                            MI = patientEpisode.MiddleInitial,
                                            LastName = patientEpisode.LastName,
                                            FirstName = patientEpisode.FirstName,
                                            IsDischarged = patientEpisode.IsDischarged
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
            return patients.OrderBy(p => p.LastName).ToList();
        }

        public List<PatientList> GetPatientList(Guid agencyId, Guid branchId, int statusId)
        {
            var list = new List<PatientList>();
            try
            {
                var status = "AND ( patients.Status = 1 OR patients.Status = 2 OR patients.Status = 3 )";
                if (statusId != null)
                {
                    if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged || statusId == (int)PatientStatus.Pending)
                    {
                        status = string.Format(" AND patients.Status = {0} ", statusId);
                    }
                }
                var insurance = string.Empty;
                if (agencyId != null && branchId != null)
                {
                    if (!agencyId.IsEmpty() && !branchId.IsEmpty())
                    {
                        var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                        if (location != null && location.IsLocationStandAlone)
                        {

                        }
                        else
                        {
                            var agency = database.Single<Agency>(l => l.Id == agencyId);

                        }

                        var script = string.Format(@"
                        SELECT patients.Id as Id, patients.FirstName as FirstName, patients.LastName as LastName, patients.PatientIdNumber as PatientIdNumber, patients.Comments as Comments, patients.Status as PatientStatus" +
                        "patients.SSN as SSN, patients.DOB as DOB, patients.Gender as Gender, patients.Ethnicities as Ethnicities, patients.MaritalStatus as MaritalStatus ,patients.IsDNR as IsDNR," +
                        "patients.AddressLine1 as AddressLine1, patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.AddressStateCode as AddressStateCode, patients.PrimaryHealthPlanId as PrimaryHealthPlanId," +
                        "patients.AddressZipCode as AddressZipCode, patients.PhoneHome as PhoneHome, patients.PhoneMobile as PhoneMobile, patients.EmailAddress as EmailAddress, patients.Payer as Payer, patients.MedicaidNumber as MedicaidNumber" +
                        "patients.CaseManagerId as CaseManagerId, patients.StartofCareDate as StartofCareDate,  patients.PrimaryInsurance as PrimaryInsurance, patients.Services as Services, patients.MedicareNumber as MedicareNumber" +
                        "patients.DME as DME, patients.ServicesRequired as ServicesRequired, patients.PaymentSource as PaymentSource, patients.ReferralName as ReferralName, patients.MedicareNumber as MedicareNumber, patients.MedicaidNumber as MedicaidNumber" +
                        "patientphysicians.PhysicianId as PatientPhysicianId, patientepisodes.StartDate as EpisodeStartDate, patientepisodes.EndDate as EpisodeEndDate" +
                        "FROM patients" +
                        "LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id AND patientphysicians.IsPrimary = 1" +
                        "LEFT JOIN patientepisodes ON patientepisodes.PatientId = patients.Id AND patientepisodes.AgencyId = patients.AgencyId" +
                        "WHERE patients.AgencyId = {0} and patients.AgencyLocationId = {1}", agencyId, branchId);

                        using (var cmd = new FluentCommand<PatientList>(script))
                        {
                            list = cmd.SetConnection("AgencyManagementConnectionString")
                            .AddGuid("agencyId", agencyId)
                            .AddGuid("branchId", branchId)
                            .AddDateTime("startdate", DateTime.Now.Date)
                            .SetMap(reader => new PatientList
                            {
                                Id = reader.GetGuid("Id"),
                                PatientId = reader.GetStringNullable("PatientIdNumber"),
                                patientMRN = reader.GetStringNullable("PatientIdNumber"),
                                PatientFirstName = reader.GetStringNullable("FirstName"),
                                PatientLastName = reader.GetStringNullable("LastName"),
                                patientComments = reader.GetStringNullable("Comments"),
                                patientSSN = reader.GetStringNullable("SSN"),
                                PatientDOB = reader.GetDateTime("DOB"),
                                PatientGender = reader.GetStringNullable("Gender"),
                                patientEthnicity = reader.GetStringNullable("Ethnicities"),
                                patientMaritalStatus = reader.GetStringNullable("MaritalStatus"),
                                patientDNR = reader.GetInt("IsDNR") == 0 ? "Yes" : "No",//tinyint
                                PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                                PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                                PatientAddressCity = reader.GetStringNullable("AddressCity"),
                                PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                                PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                                patientPrimaryHealthPlanId = reader.GetStringNullable("PrimaryHealthPlanId"),
                                PatientPhone = reader.GetStringNullable("PhoneHome"),
                                patientMobile = reader.GetStringNullable("PhoneMobile"),
                                patientEmail = reader.GetStringNullable("EmailAddress"),
                                patientPayer = reader.GetStringNullable("Payer"),
                                patientCaseManagerId = reader.GetGuid("CaseManagerId"),
                                PatientSoC = reader.GetStringNullable("StartofCareDate"),
                                PatientInsurance = reader.GetStringNullable("PrimaryInsurance"),
                                patientDME = reader.GetStringNullable("DME"),
                                patientServicesRequired = reader.GetStringNullable("ServicesRequired"),
                                patientPaymentSource = reader.GetStringNullable("PaymentSource"),
                                patientReferralName = reader.GetStringNullable("ReferralName"),
                                PhysicianId = reader.GetGuid("PatientPhysicianId"),
                                patientServices = reader.GetInt("Services"),
                                patientMedicaidNumber = reader.GetStringNullable("MedicaidNumber"),
                                PatientMedicareNumber = reader.GetStringNullable("MedicareNumber"),
                                PatientStatus = reader.GetInt("PatientStatus"),
                                patientEpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                                patientEpisodeEndDate = reader.GetDateTime("EpisodeEndDate")
                            }).AsList();
                        }

                    }
                    else
                    {
                        //Else part if agency Id and branch Id are empty.
                    }
                }
                else
                {
                    //Else part if agency Id and branch Id are null
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception" + ex.Message);
            }
            return list;
        }

        public List<PatientRoster> GetPatientRoster(Guid agencyId, Guid branchId, int statusId, int insuranceId)
        {
            var list = new List<PatientRoster>();
            var status = " AND (patients.Status = 1 OR patients.Status = 2 OR patients.Status = 3 ) ";
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged || statusId == (int)PatientStatus.Pending)
            {
                status = string.Format(" AND patients.Status = {0} ", statusId);
            }
            var insurance = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }
            var script = string.Format(@"SELECT patients.Id as Id, patients.FirstName as FirstName , patients.MiddleInitial, patients.PrimaryInsurance, patients.LastName as LastName , patients.PatientIdNumber as PatientIdNumber,  patients.MedicareNumber as MedicareNumber, patients.DOB as DOB, patients.PhoneHome as PhoneHome , patients.AddressLine1 as AddressLine1, patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.AddressStateCode as AddressStateCode , patients.AddressZipCode as AddressZipCode, patients.StartofCareDate as StartofCareDate ,  patients.DischargeDate as DischargeDate,  patients.Status as Status  , patients.Triage as Triage ,  " +
             "patients.Gender as Gender, patients.PaymentSource as PaymentSource, patients.PrimaryHealthPlanId as PrimaryHealthPlanId,  patientphysicians.PhysicianId as PhysicianId " +
             "FROM patients  LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1 " +
             "WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 {0} {1} {2} ", status, !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId " : string.Empty, insurance);

            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", DateTime.Now.Date)
                .SetMap(reader => new PatientRoster
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetStringNullable("PatientIdNumber"),
                    PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientGender = reader.GetStringNullable("Gender"),
                    PatientMedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    PatientDOB = reader.GetDateTimeNullable("DOB")??DateTime.MinValue,
                    PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                    PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                    PatientAddressCity = reader.GetStringNullable("AddressCity"),
                    PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientSoC = reader.GetDateTime("StartofCareDate").ToShortDateString().ToZeroFilled(),
                    PatientInsurance = reader.GetStringNullable("PaymentSource"),
                    PatientInsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PatientInsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetGuid("PhysicianId") : Guid.Empty,
                    PatientDischargeDate = reader.GetInt("Status") == 2 && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate").ToString("MM/dd/yyyy") : string.Empty,
                    Triage = reader.GetInt("Triage")
                }).AsList();
            }
            return list;
        }


        public List<PatientRoster> GetPatientRosterByDateRange(Guid agencyId, Guid branchId, int statusId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientRoster>();
            var status = " AND (patientadmissiondates.Status = 1 OR patientadmissiondates.Status = 2 OR patientadmissiondates.Status = 3 ) ";
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged || statusId == (int)PatientStatus.Pending)
            {
                status = string.Format(" AND patientadmissiondates.Status = {0} ", statusId);
            }
            var insurance = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }
            var script = string.Format(@"SELECT DISTINCT patients.Id as Id, patients.FirstName as FirstName , patients.MiddleInitial, patients.PrimaryInsurance, patients.LastName as LastName , patients.PatientIdNumber as PatientIdNumber, " +
                "patients.MedicareNumber as MedicareNumber, patients.DOB as DOB, patients.PhoneHome as PhoneHome , patients.AddressLine1 as AddressLine1, patients.AddressLine2 as AddressLine2, " +
                "patients.AddressCity as AddressCity, patients.AddressStateCode as AddressStateCode , patients.AddressZipCode as AddressZipCode, patientadmissiondates.Status as Status  , patients.Triage as Triage , " +
                "patients.Gender as Gender, patients.PaymentSource as PaymentSource, patients.PrimaryHealthPlanId as PrimaryHealthPlanId,  patientphysicians.PhysicianId as PhysicianId, " +
                "patientadmissiondates.StartOfCareDate as StartOfCareDate, patientadmissiondates.DischargedDate as DischargeDate " +
                "FROM patientadmissiondates " +
                "INNER JOIN patients ON patientadmissiondates.PatientId = patients.Id  " +
                "LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1 " +
                "WHERE patients.IsDeprecated = 0 AND patientadmissiondates.IsDeprecated = 0 " +
                "AND ((DATE(patientadmissiondates.StartOfCareDate) BETWEEN DATE(@startdate) AND DATE(@enddate)) OR DATEDIFF(patientadmissiondates.StartOfCareDate, @startdate) < 0) " +
                "AND ((patientadmissiondates.DischargedDate != '0001-01-01' AND DATEDIFF(patientadmissiondates.StartOfCareDate, patientadmissiondates.DischargedDate) < 0 AND (DATEDIFF(patientadmissiondates.DischargedDate, @startdate) > 0 OR patientadmissiondates.Status = 1)) " +
                    "OR patientadmissiondates.DischargedDate = '0001-01-01') AND " +
                "patients.AgencyId = @agencyid {0} {1} {2} ", status, !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId " : string.Empty, insurance);

            using (var cmd = new FluentCommand<PatientRoster>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientRoster
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetStringNullable("PatientIdNumber"),
                    PatientFirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientLastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    PatientMiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientGender = reader.GetStringNullable("Gender"),
                    PatientMedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    PatientDOB = reader.GetDateTime("DOB"),
                    PatientPhone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    PatientAddressLine1 = reader.GetStringNullable("AddressLine1"),
                    PatientAddressLine2 = reader.GetStringNullable("AddressLine2"),
                    PatientAddressCity = reader.GetStringNullable("AddressCity"),
                    PatientAddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    PatientAddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PatientSoC = reader.GetDateTime("StartofCareDate").ToShortDateString().ToZeroFilled(),
                    PatientInsurance = reader.GetStringNullable("PaymentSource"),
                    PatientInsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PatientInsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetGuid("PhysicianId") : Guid.Empty,
                    PatientDischargeDate = reader.GetInt("Status") == 2 && reader.GetDateTime("DischargeDate").Date > DateTime.MinValue.Date && reader.GetDateTime("DischargeDate").Date >= reader.GetDateTime("StartOfCareDate").Date ? reader.GetDateTime("DischargeDate").ToString("MM/dd/yyyy") : string.Empty,
                    Triage = reader.GetInt("Triage")
                }).AsList();
            }
            return list;
        }

        public List<PatientEpisode> GetSurveyCensesPatientEpisodes(Guid agencyId, string patientIds)
        {
            var list = new List<PatientEpisode>();
            var script = string.Format(@"SELECT patientepisodes.PatientId as Id, patientepisodes.StartDate , patientepisodes.EndDate , patientepisodes.Schedule " +
             "FROM  patientepisodes " +
             "WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId IN ( {0} ) AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND DATE(patientepisodes.StartDate) < Date(@startdate)  ORDER BY patientepisodes.StartDate ", patientIds);

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", DateTime.Now)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    // PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    // FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    // LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    // Gender = reader.GetStringNullable("Gender"),
                    // MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    // SSN = reader.GetStringNullable("SSN"),
                    // DOB = reader.GetDateTime("DOB"),
                    // Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    // AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    // AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    // AddressCity = reader.GetStringNullable("AddressCity"),
                    // AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    // AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    //SOC = reader.GetDateTime("StartofCareDate"),
                    // InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    //PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetGuid("PhysicianId") : Guid.Empty,
                    // CaseManagerId = reader.GetStringNullable("CaseManagerId").IsNotNullOrEmpty() && reader.GetStringNullable("CaseManagerId").IsGuid() ? reader.GetGuid("CaseManagerId") : Guid.Empty

                }).AsList();
            }
            return list;
        }

        public List<PatientWithPhysicanInfo> GetPatientPhysicianInfos(Guid agencyId, Guid branchId, int statusId)
        {
            var list = new List<PatientWithPhysicanInfo>();
            var status = " AND (patients.Status = 1 OR patients.Status = 2) ";
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged)
            {
                status = string.Format(" AND patients.Status = {0} ", statusId);
            }
            var script = string.Format(@"SELECT patients.Id as Id , patients.PatientIdNumber as PatientIdNumber ,patients.FirstName as FirstName , patients.LastName as LastName , patients.MiddleInitial as MiddleInitial, patients.CaseManagerId as CaseManagerId , patients.UserId as  UserId, patients.Created as Created ,agencyphysicians.FirstName as PhysicianFirstName , patientphysicians.PhysicianId as  PhysicianId , agencyphysicians.LastName as PhysicianLastName , agencyphysicians.NPI as PhysicianNPI " +
                " FROM patients  LEFT JOIN patientphysicians ON (patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1) LEFT JOIN agencyphysicians ON  patientphysicians.PhysicianId = agencyphysicians.Id " +
                " WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0  {0} {1} ORDER BY patients.Created DESC ", status, !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<PatientWithPhysicanInfo>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusId", statusId)
                .SetMap(reader => new PatientWithPhysicanInfo
                {
                    Id = reader.GetGuid("Id"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                    PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                    PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                    UserId = reader.GetStringNullable("UserId").IsNotNullOrEmpty() && reader.GetStringNullable("UserId").IsGuid() ? reader.GetGuid("UserId") : Guid.Empty,
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetGuid("PhysicianId") : Guid.Empty,
                    CaseManagerId = reader.GetStringNullable("CaseManagerId").IsNotNullOrEmpty() && reader.GetStringNullable("CaseManagerId").IsGuid() ? reader.GetGuid("CaseManagerId") : Guid.Empty,
                    Created = reader.GetDateTime("Created")
                }).AsList();
            }
            return list;
        }

        public List<SurveyCensus> GetSurveyCensesByStatus(Guid agencyId, Guid branchId, int statusId, int insuranceId)
        {
            var list = new List<SurveyCensus>();
            var status = " AND (patients.Status = 1 OR patients.Status = 2) ";
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged)
            {
                status = string.Format(" AND patients.Status = {0} ", statusId);
            }

            var insurance = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            var script = string.Format(@"SELECT patients.Id as Id, patients.FirstName as FirstName , patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, patients.PrimaryHealthPlanId as PrimaryHealthPlanId, patients.PatientIdNumber as PatientIdNumber,  patients.MedicareNumber as MedicareNumber, patients.DOB as DOB, patients.PhoneHome as PhoneHome , patients.AddressLine1 as AddressLine1, patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.AddressStateCode as AddressStateCode , patients.AddressZipCode as AddressZipCode, patients.StartofCareDate as StartofCareDate ,  patients.CaseManagerId as CaseManagerId , patients.SSN as SSN ,  patients.Triage as Triage ,  " +
              "patients.Gender as Gender, patients.EvacuationZone as EvacuationZone, patients.Triage as Triage, patients.PrimaryInsurance as PrimaryInsurance,  patientphysicians.PhysicianId as PhysicianId, agencyphysicians.FirstName as PhysicianFirstName , agencyphysicians.LastName as PhysicianLastName , agencyphysicians.NPI as PhysicianNPI , agencyphysicians.Credentials as Credentials, agencyphysicians.PhoneWork as PhoneWork , agencyphysicians.FaxNumber as FaxNumber " +
              "FROM patients  LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1 LEFT JOIN agencyphysicians ON  patientphysicians.PhysicianId = agencyphysicians.Id  " +
              "WHERE patients.AgencyId = @agencyid AND patients.IsDeprecated = 0 {0} {1} {2} ORDER BY patients.LastName ASC , patients.FirstName ASC ", status, !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId " : string.Empty, insurance);

            using (var cmd = new FluentCommand<SurveyCensus>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusId", statusId)
                .SetMap(reader => new SurveyCensus
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    DOB = reader.GetDateTime("DOB"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    Gender = reader.GetStringNullable("Gender"),
                    SOC = reader.GetDateTime("StartofCareDate"),
                    Triage = reader.GetInt("Triage"),
                    SSN = reader.GetStringNullable("SSN"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PhysicianDisplayName = string.Concat(reader.GetStringNullable("PhysicianLastName"), ", ", reader.GetStringNullable("PhysicianFirstName"), " ", reader.GetStringNullable("Credentials")),
                    PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                    PhysicianPhone = reader.GetStringNullable("PhoneWork").ToPhone(),
                    PhysicianFax = reader.GetStringNullable("FaxNumber"),
                    EvacuationZone = reader.GetStringNullable("EvacuationZone"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    CaseManagerId = reader.GetStringNullable("CaseManagerId").IsNotNullOrEmpty() && reader.GetStringNullable("CaseManagerId").IsGuid() ? reader.GetStringNullable("CaseManagerId").ToGuid() : Guid.Empty
                }).AsList();
            }
            return list;
        }

        public List<SurveyCensus> GetSurveyCensesByStatusByDateRange(Guid agencyId, Guid branchId, int statusId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var list = new List<SurveyCensus>();
            var statusString = " AND (patients.Status = 1 OR patients.Status = 2) ";
            if (statusId == (int)PatientStatus.Active || statusId == (int)PatientStatus.Discharged)
            {
                statusString = string.Empty;
                statusString = string.Format(" AND patients.Status = {0} ", statusId);
            }

            var dateRangeString = string.Empty;
            string ulStartDate = default(string); string ulEndDate = default(string);
            ulStartDate = startDate.Year.ToString() + "-" + startDate.Month.ToString() + "-" + startDate.Day.ToString();
            ulEndDate = endDate.Year.ToString() + "-" + endDate.Month.ToString() + "-" + endDate.Day.ToString();
            if (statusId == (int)PatientStatus.Active) 
            {
                dateRangeString = string.Format("AND patientadmissiondates.StartOfCareDate BETWEEN ('{0}') AND ('{1}') ",ulStartDate,ulEndDate);
            }
            else if (statusId == (int)PatientStatus.Discharged)
            {
                dateRangeString = string.Format("AND patientadmissiondates.DischargedDate BETWEEN ('{0}') AND ('{1}') ", ulStartDate, ulEndDate);
            }
            else
            {
                dateRangeString = string.Format("AND (( patientadmissiondates.StartOfCareDate BETWEEN ('{0}') AND ('{1}') &&  patients.Status = 1) || (patientadmissiondates.DischargedDate BETWEEN ('{0}') AND ('{1}') &&  patients.Status = 2) )", ulStartDate, ulEndDate);
            }
           

            var agencyLocationString = string.Empty;
            if (branchId.IsNotEmpty() && !branchId.IsNull()) 
            {
                agencyLocationString = string.Format("AND patients.AgencyLocationId = '{0}' ", branchId);
            }

            var insuranceString = string.Empty;
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insuranceString = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insuranceString = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insuranceString = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insuranceString = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insuranceString = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            var script = string.Format(
                "SELECT CONCAT(patientadmissiondates.StartOfCareDate, ' - ', patientadmissiondates.DischargedDate) as `Admission Period (YYYY-MM-DD)`,  patients.Id as Id, patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, patients.PatientIdNumber as PatientIdNumber, patients.PrimaryHealthPlanId as PrimaryHealthPlanId, patients.MedicareNumber as MedicareNumber, patients.DOB as DOB, patients.PhoneHome as PhoneHome, patients.AddressLine1 as AddressLine1, patients.AddressLine2 as AddressLine2, patients.AddressCity as AddressCity, patients.AddressStateCode as AddressStateCode, patients.AddressZipCode as AddressZipCode, patients.Gender as Gender, patients.StartofCareDate as StartofCareDate, patientadmissiondates.DischargedDate as DischargedDate, patients.Triage as Triage, patients.SSN as SSN, patients.PrimaryInsurance as PrimaryInsurance, agencyphysicians.LastName as PhysicianLastName, agencyphysicians.FirstName as PhysicianFirstName, agencyphysicians.Credentials as Credentials, agencyphysicians.NPI as PhysicianNPI, agencyphysicians.PhoneWork as PhoneWork, agencyphysicians.FaxNumber as FaxNumber, patients.EvacuationZone as EvacuationZone, patientphysicians.PhysicianId as PhysicianId, patients.CaseManagerId as CaseManagerId " +
                "FROM patients "+
                "LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1 "+
                "LEFT JOIN agencyphysicians ON  patientphysicians.PhysicianId = agencyphysicians.Id AND agencyphysicians.IsDeprecated = '0' "+
                "LEFT JOIN patientadmissiondates ON patientadmissiondates.PatientId = patients.Id AND patientadmissiondates.IsActive = 1 AND patientadmissiondates.IsDeprecated = '0' AND patientadmissiondates.`Status` = patients.`Status` "+
                "WHERE patients.AgencyId = '{0}' "+
                "AND patients.IsDeprecated = '0' "+
                "{1} "+ //agencyLocation
                "{2} "+ //Status
                "{3} "+ //Insurance
                "{4} "  //DateRange
                ,agencyId, agencyLocationString, statusString, insuranceString, dateRangeString);

            using (var cmd = new FluentCommand<SurveyCensus>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusId", statusId)
                .SetMap(reader => new SurveyCensus
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    InsuranceNumber = reader.GetStringNullable("PrimaryHealthPlanId"),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    DOB = reader.GetDateTime("DOB"),
                    Phone = reader.GetStringNullable("PhoneHome").ToPhone(),
                    AddressLine1 = reader.GetString("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetString("AddressCity"),
                    AddressStateCode = reader.GetString("AddressStateCode"),
                    AddressZipCode = reader.GetString("AddressZipCode"),
                    Gender = reader.GetStringNullable("Gender"),
                    SOC = reader.GetDateTime("StartofCareDate"),
                    DischargedDate = reader.GetDateTime("DischargedDate"),
                    Triage = reader.GetInt("Triage"),
                    SSN = reader.GetStringNullable("SSN"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PhysicianDisplayName = string.Concat(reader.GetStringNullable("PhysicianLastName"), ", ", reader.GetStringNullable("PhysicianFirstName"), " ", reader.GetStringNullable("Credentials")),
                    PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                    PhysicianPhone = reader.GetStringNullable("PhoneWork").ToPhone(),
                    PhysicianFax = reader.GetStringNullable("FaxNumber"),
                    EvacuationZone = reader.GetStringNullable("EvacuationZone"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    CaseManagerId = reader.GetStringNullable("CaseManagerId").IsNotNullOrEmpty() && reader.GetStringNullable("CaseManagerId").IsGuid() ? reader.GetStringNullable("CaseManagerId").ToGuid() : Guid.Empty
                }).AsList();
            }
            return list;
        }

        public List<AdmissionEpisode> PatientAdmissonPeriods(Guid agencyId, Guid patientId)
        {
            var list = new List<AdmissionEpisode>();
            var script = string.Format(@"SELECT patientadmissiondates.Id as Id,  patientadmissiondates.PatientId as PatientId , patientadmissiondates.StartOfCareDate as StartOfCareDate, patientadmissiondates.DischargedDate as DischargedDate,  patientadmissiondates.Created as Created ,  patientadmissiondates.Modified as Modified , " +
                 " patientepisodes.Id as EpisodeId , patientepisodes.EndDate as EndDate, patientepisodes.StartDate as StartDate " +
                 " FROM patientadmissiondates   " +
                 " LEFT JOIN patientepisodes ON ( patientadmissiondates.PatientId = patientepisodes.PatientId AND patientadmissiondates.Id = patientepisodes.AdmissionId AND patientadmissiondates.IsDeprecated = 0  AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0 ) " +
                 " WHERE patientadmissiondates.PatientId = @patientid AND patientadmissiondates.AgencyId = @agencyid   AND patientadmissiondates.IsActive = 1   ");

            using (var cmd = new FluentCommand<AdmissionEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AdmissionEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuidIncludeEmpty("EpisodeId"),
                    StartOfCareDate = reader.GetDateTimeWithMin("StartOfCareDate"),
                    DischargedDate = reader.GetDateTimeWithMin("DischargedDate"),
                    EpisodeStartDate = reader.GetDateTimeWithMin("StartDate"),
                    EpisodeEndDate = reader.GetDateTimeWithMin("EndDate"),
                    Created = reader.GetDateTimeWithMin("Created"),
                    Modified = reader.GetDateTimeWithMin("Modified"),
                }).AsList();
            }
            return list;
        }

        public string GetReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId)
        {
            ScheduleEvent task = this.GetSchedule(agencyId, episodeId, patientId, eventId);
            return task.ReturnReason.IsNotNullOrEmpty() ? task.ReturnReason : string.Empty;
        }

        public bool AddReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId, string reason, string user)
        {
            bool result = false;
            ScheduleEvent task = this.GetSchedule(agencyId, episodeId, patientId, eventId);
            if (task != null)
            {
                task.ReturnReason +=
                    (task.ReturnReason.IsNotNullOrEmpty() ? "<hr/>" : string.Empty) +
                    "<span class='user'>" + user + "</span>" +
                    "<span class='time'>" + DateTime.Now.ToString("g") + "</span>" +
                    "<span class='reason'>" + reason + "</span>";
                result = this.UpdateEpisode(agencyId, task);
            }
            return result;
        }

        public Dictionary<string, int> GetPatientAdmits(Guid agencyId, Guid branchId)
        {
            var list = new Dictionary<string, int>();
            var script = string.Format(@"SELECT DISTINCT 
	                                patients.Id as Id,
	                                count(patientadmissiondates.Id) as Admit 
		                                FROM 
		                                patientadmissiondates 
			                                INNER JOIN patients ON patientadmissiondates.PatientId = patients.Id 
				                                WHERE patients.IsDeprecated = 0 AND 
                                                    patients.InternalReferral != '00000000-0000-0000-0000-000000000000' AND
				                                    patientadmissiondates.IsDeprecated = 0 AND
		                                            patients.AgencyId = @agencyid AND {0}
		                                            (patientadmissiondates.`Status` = 3 OR  patientadmissiondates.`Status` = 4 OR
		                                            patientadmissiondates.`Status` = 1 OR  patientadmissiondates.`Status` = 2)
		       	                                        GROUP BY patientadmissiondates.PatientId",
                        !branchId.IsEmpty() ? " patients.AgencyLocationId = @branchId AND" : string.Empty);

            using (var cmd = new FluentCommand<int>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetDictonaryId("Id")
                .SetMap(reader => reader.GetInt("Admit")).AsDictionary();
            }
            return list;
        }

        public List<PatientAdmission> GetPatientAdmissionsByDateRange(Guid agencyId, Guid branchId, int statusId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientAdmission>();
            var prescript = string.Format(@"SELECT DISTINCT 
	                                patients.Id as Id,
	                                patients.FirstName as FirstName,
	                                patients.MiddleInitial, 
	                                patients.PrimaryInsurance,
	                                patients.LastName as LastName,
	                                patients.PatientIdNumber as PatientIdNumber,
	                                patients.PhoneHome as PhoneHome,
	                                patientadmissiondates.`Status` as `Status`,
	                                patients.Gender as Gender, 
	                                patientphysicians.PhysicianId as PhysicianId,
	                                patientadmissiondates.StartOfCareDate as StartOfCareDate,
	                                patientadmissiondates.DischargedDate as DischargeDate,
	                                patients.InternalReferral as InternalReferral
		                                FROM 
		                                patientadmissiondates 
			                                INNER JOIN patients ON patientadmissiondates.PatientId = patients.Id 
			                                LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1
				                                WHERE patients.IsDeprecated = 0 AND 
                                                    patients.InternalReferral != '00000000-0000-0000-0000-000000000000' AND
				                                    patientadmissiondates.IsDeprecated = 0 AND
		                                            patients.AgencyId = @agencyid  {0}",
		       	                                    !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId " : string.Empty);
            var script = "";
            if (statusId == 0)
            {
                script = prescript + "AND ((patientadmissiondates.`Status` = 3 OR  patientadmissiondates.`Status` = 4) OR ((patientadmissiondates.`Status` = 1 OR  patientadmissiondates.`Status` = 2) AND patientadmissiondates.StartOfCareDate between @startDate AND @endDate))";
            }
            else if (statusId == (int)PatientStatus.Active ||statusId == (int)PatientStatus.Discharged)
            {
                script = prescript + "AND patientadmissiondates.`Status` = @statusId AND patientadmissiondates.StartOfCareDate between @startDate AND @endDate";
            }
            else
            {
                script = prescript + "AND patientadmissiondates.`Status` = @statusId";
            }

//            var script = string.Format(@"SELECT DISTINCT 
//	                                patients.Id as Id,
//	                                patients.FirstName as FirstName,
//	                                patients.MiddleInitial, 
//	                                patients.PrimaryInsurance,
//	                                patients.LastName as LastName,
//	                                patients.PatientIdNumber as PatientIdNumber,
//	                                patients.PhoneHome as PhoneHome,
//	                                patientadmissiondates.`Status` as `Status`,
//	                                patients.Gender as Gender, 
//	                                patientphysicians.PhysicianId as PhysicianId,
//	                                patientadmissiondates.StartOfCareDate as StartOfCareDate,
//	                                patientadmissiondates.DischargedDate as DischargeDate,
//	                                patients.InternalReferral as InternalReferral
//		                                FROM 
//		                                patientadmissiondates 
//			                                INNER JOIN patients ON patientadmissiondates.PatientId = patients.Id 
//			                                LEFT JOIN patientphysicians ON patientphysicians.PatientId = patients.Id  AND  patientphysicians.IsPrimary = 1
//				                                WHERE patients.IsDeprecated = 0 AND 
//                                                    patients.InternalReferral != '00000000-0000-0000-0000-000000000000' AND
//				                                    patientadmissiondates.IsDeprecated = 0 AND
//		                                            patients.AgencyId = @agencyid AND 
//		                                            patientadmissiondates.StartOfCareDate between @startDate AND @endDate AND 
//                                                     (patientadmissiondates.`Status`=@statusId) ");
            //((patientadmissiondates.`Status` = 3 OR  patientadmissiondates.`Status` = 4) OR ((patientadmissiondates.`Status` = 1 OR  patientadmissiondates.`Status` = 2) AND patientadmissiondates.StartOfCareDate between @startDate AND @endDate))

            using (var cmd = new FluentCommand<PatientAdmission>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid",statusId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientAdmission
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleInitial = reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhoneHome = reader.GetStringNullable("PhoneHome").ToPhone(),
                    StartOfCareDate = reader.GetDateTime("StartofCareDate"),
                    InsuranceId = reader.GetStringNullable("PrimaryInsurance"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetGuid("PhysicianId") : Guid.Empty,
                    //Admit = reader.GetInt("Admit"),
                    Status = reader.GetInt("Status"),
                    DischargedDate = reader.GetDateTime("DischargeDate"),
                    InternalReferral = reader.GetGuidIncludeEmpty("InternalReferral"),
                }).AsList();
            }
            return list;
        }

        public List<MissedVisit> GetMissedVisitsByStatus(Guid agencyId, DateTime startDate, DateTime endDate, int status)
        {
            var list = new List<MissedVisit>();
            var script = @"SELECT * 
                           FROM missedvisits m 
                           WHERE 			                                 
                           m.Date between @startDate AND @endDate AND
                           m.Status = @status";

            using (var cmd = new FluentCommand<MissedVisit>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddInt("status", status)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new MissedVisit
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Date = reader.GetDateTime("Created"),
                    Reason = reader.GetStringNullable("Reason"),
                    Comments = reader.GetStringNullable("Comments"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                    IsOrderGenerated = reader.GetBoolean("IsOrderGenerated"),
                    SignatureText = reader.GetStringNullable("SignatureText"),
                    IsPhysicianOfficeNotified = reader.GetBoolean("IsPhysicianOfficeNotified")
                }).AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (!patientId.IsEmpty())
            {
                var patientEpisodes = this.GetPatientEpisodeData(agencyId, patientId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
                if (patientEpisodes != null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(patientEpisode =>
                    {
                        if (patientEpisode.StartDate.IsValidDate() && patientEpisode.EndDate.IsValidDate() && patientEpisode.Schedule.IsNotNullOrEmpty())
                        {
                            var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false).ToList();
                            if (schedule != null && schedule.Count > 0)
                            {
                                schedule.ForEach(s =>
                                {
                                    if (s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                    {
                                        s.EventDate = s.EventDate.ToZeroFilled();
                                        s.VisitDate = s.VisitDate.ToZeroFilled();
                                        s.StartDate = patientEpisode.StartDate.ToDateTime();
                                        s.EndDate = patientEpisode.EndDate.ToDateTime();
                                        s.PatientName = patientEpisode.PatientName;
                                        patientEvents.Add(s);
                                    }
                                });
                            }
                        }
                    });
                }
            }
            return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var ids = missedVisitIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            Id,
                                            Status,
                                            Reason,
                                            Comments
                                                FROM 
                                                    missedvisits 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                         Reason = reader.GetStringNullable("Reason"),
                         Comments = reader.GetStringNullable("Comments")
                     }).AsList();
                }
            }
            return list;
        }

        public List<MissedVisit> GetMissedVisitsByIdsAndStatus(Guid agencyId, List<int> statuses, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var ids = missedVisitIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var statusString = statuses.Select(s => string.Format("{0}", s)).ToArray().Join(",");
                var script = string.Format(@"SELECT 
                                            Id,
                                            Status
                                                FROM 
                                                    missedvisits 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            Status IN ({1}) AND
                                                            Id IN ( {0} )", ids,statusString);
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status")
                     }).AsList();
                }
            }
            return list;
        }


        public List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, Guid episodeId)
        {
            var list = new List<ReturnComment>();
            var script = string.Format(@"SELECT 
                                            Id,
                                            EpisodeId,
                                            EventId,
                                            UserId,
                                            Comments,
                                            Modified
                                                FROM 
                                                    returncomments 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            EpisodeId = @episodeId AND
                                                            IsDeprecated = 0");
           
            
            using (var cmd = new FluentCommand<ReturnComment>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("episodeId", episodeId)
                 .SetMap(reader => new ReturnComment
                 {
                     Id = reader.GetInt("Id"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     EventId = reader.GetGuid("EventId"),
                     UserId = reader.GetGuid("UserId"),
                     Comments = reader.GetStringNullable("Comments"),
                     Modified = reader.GetDateTime("Modified")
                 }).AsList();
            }
            return list;
        }

        public List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, List<Guid> episodeIds)
        {
            var list = new List<ReturnComment>();
            if (episodeIds != null && episodeIds.Count > 0)
            {
                var ids = episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            Id,
                                            EpisodeId,
                                            EventId,
                                            UserId,
                                            Comments,
                                            Modified
                                                FROM 
                                                    returncomments 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            EpisodeId IN ( {0} )AND
                                                            IsDeprecated = 0", ids);
                using (var cmd = new FluentCommand<ReturnComment>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ReturnComment
                     {
                         Id = reader.GetInt("Id"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         EventId = reader.GetGuid("EventId"),
                         UserId = reader.GetGuid("UserId"),
                         Comments = reader.GetStringNullable("Comments"),
                         Modified = reader.GetDateTime("Modified")
                     }).AsList();
                }
            }
            return list;
        }


        #endregion

        #region Deleted Task Methods

        public List<DeletedItem> GetDeletedItems(Guid agencyId, Guid patientId)
        {
            return database.Find<DeletedItem>(d => d.AgencyId == agencyId && d.PatientId == patientId).ToList();
        }

        public DeletedItem GetDeletedItem(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return database.Single<DeletedItem>(d => d.AgencyId == agencyId && d.PatientId == patientId && d.EpisodeId == episodeId);
        }

        public bool AddDeletedItem(DeletedItem deletedItem)
        {
            var result = false;
            if (deletedItem != null)
            {
                deletedItem.Created = DateTime.Now;
                deletedItem.Modified = DateTime.Now;

                database.Add<DeletedItem>(deletedItem);
                result = true;
            }
            return result;
        }

        public bool UpdateDeletedItem(DeletedItem deletedItem)
        {
            var result = false;
            if (deletedItem != null)
            {
                deletedItem.Modified = DateTime.Now;
                database.Update<DeletedItem>(deletedItem);
                result = true;
            }
            return result;
        }

        #endregion

        #region Medicare Eligibility

        public MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid patientId, Guid id)
        {
            return database.Single<MedicareEligibility>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.Id == id);
        }

        public MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid episodeId, Guid patientId, Guid id)
        {
            return database.Single<MedicareEligibility>(m => m.AgencyId == agencyId && m.EpisodeId == episodeId && m.PatientId == patientId && m.Id == id);
        }

        public bool UpdateMedicareEligibility(MedicareEligibility medicareEligibility)
        {
            var result = false;
            if (medicareEligibility != null)
            {
                database.Update<MedicareEligibility>(medicareEligibility);
                result = true;
            }
            return result;
        }

        public List<MedicareEligibility> GetMedicareEligibilities(Guid agencyId, Guid patientId)
        {
            return database.Find<MedicareEligibility>(m => m.AgencyId == agencyId && m.PatientId == patientId).ToList();
        }

        #endregion

        #region Allergies

        public AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId)
        {
            var profile = database.Single<AllergyProfile>(m => m.AgencyId == agencyId && m.PatientId == patientId);

            if (profile == null)
            {
                profile = new AllergyProfile
                {
                    Id = Guid.NewGuid(),
                    AgencyId = agencyId,
                    PatientId = patientId,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Allergies = new List<Allergy>().ToXml()
                };
                database.Add<AllergyProfile>(profile);
            }

            return profile;
        }

        public AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId)
        {
            return database.Single<AllergyProfile>(a => a.AgencyId == agencyId && a.Id == profileId);
        }

        public bool UpdateAllergyProfile(AllergyProfile allergyProfile)
        {
            if (allergyProfile != null)
            {
                allergyProfile.Modified = DateTime.Now;
                database.Update<AllergyProfile>(allergyProfile);
                return true;
            }
            return false;
        }

        public bool AddAllergyProfile(AllergyProfile allergyProfile)
        {
            if (allergyProfile != null)
            {
                allergyProfile.Created = DateTime.Now;
                allergyProfile.Modified = DateTime.Now;
                database.Add<AllergyProfile>(allergyProfile);
                return true;
            }
            return false;
        }

        public List<PatientEpisodeData> GetEpisodeDatasBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientEpisodeData>();
            var script = @"SELECT " +
                "patientepisodes.Id as EpisodeId, patients.Id as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, " +
                "patients.Status as Status, patients.DischargeDate as DischargeDate, " +
                "patientepisodes.EndDate as EndDate, patientepisodes.StartDate as StartDate, patientepisodes.Schedule as Schedule " +
                "FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id " +
                "WHERE (patients.Status = 1 || patients.Status = 2) AND patientepisodes.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Id = @patientid " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate " +
                "|| @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND " +
                "patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate " +
                "BETWEEN @startdate AND @enddate)) ORDER BY patientepisodes.StartDate ASC";

            using (var cmd = new FluentCommand<PatientEpisodeData>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                    EndDate = reader.GetDateTime("EndDate").ToShortDateString().ToZeroFilled(),
                    StartDate = reader.GetDateTime("StartDate").ToShortDateString().ToZeroFilled(),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return list.ToList();
        }

        public List<PatientEpisode> GetEpisodesBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientEpisode>();
            var script = string.Format(@"SELECT patientepisodes.Id as Id, 
                                    patientepisodes.AgencyId as AgencyId,  
                                    patientepisodes.PatientId as PatientId,  
                                    patientepisodes.Details as Details, 
                                    patientepisodes.StartDate as StartDate, 
                                    patientepisodes.EndDate as EndDate, 
                                    patientepisodes.Schedule as Schedule,
                                    patientepisodes.IsActive as IsActive, 
                                    patientepisodes.IsRecertCompleted as IsRecertCompleted, 
                                    patientepisodes.IsDischarged as IsDischarged,  
                                    patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge,
                                    patientepisodes.AssessmentId as AssessmentId, 
                                    patientepisodes.AssessmentType as AssessmentType, 
                                    patientepisodes.Created as Created, 
                                    patientepisodes.Modified as Modified, 
                                    patientepisodes.AdmissionId as AdmissionId,
                                    patientepisodes.EndDate as EndDate,
                                    patientepisodes.StartDate as StartDate,
                                    patientepisodes.Schedule as Schedule,
                                    patientepisodes.StartOfCareDate as StartOfCareDate
                                        FROM patientepisodes 
                                            WHERE patientepisodes.AgencyId = @agencyid AND
                                                patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND
                                                patientepisodes.PatientId = @patientId AND
                                                (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate 
                                                || @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND 
                                                patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate 
                                                BETWEEN @startdate AND @enddate)) 
                                                    ORDER BY patientepisodes.StartDate ASC");

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Details = reader.GetStringNullable("Details"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsRecertCompleted = reader.GetBoolean("IsRecertCompleted"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                    AssessmentId = reader.GetGuid("AssessmentId"),
                    AssessmentType = reader.GetStringNullable("AssessmentType"),
                    Created = reader.GetDateTime("Created"),
                    Modified = reader.GetDateTime("Modified"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                })
                .AsList();
            }
            return list.ToList();
        }

        #endregion

        #region Hospitalization Log

        public HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId)
        {
            return database.Single<HospitalizationLog>(t => t.AgencyId == agencyId && t.PatientId == patientId && t.Id == transferLogId);
        }

        public bool UpdateHospitalizationLog(HospitalizationLog transferLog)
        {
            var result = false;
            if (transferLog != null)
            {
                database.Update<HospitalizationLog>(transferLog);
                result = true;
            }
            return result;
        }

        public bool AddHospitalizationLog(HospitalizationLog transferLog)
        {
            var result = false;
            if (transferLog != null)
            {
                database.Add<HospitalizationLog>(transferLog);
                result = true;
            }
            return result;
        }

        public List<HospitalizationLog> GetHospitalizationLogs(Guid patientId, Guid agencyId)
        {
            return database.Find<HospitalizationLog>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.IsDeprecated == false).ToList();
        }

        #endregion

        #region Md.Axxessweb.com

//        public List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysician(Guid agencyId, List<Guid> physicianIdentifiers, int[] status)
//        {
//            var list = new List<FaceToFaceEncounter>();
//            var ids = physicianIdentifiers.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");
//            var allStatus = status.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");

//            //var query = new QueryBuilder("select Id, AgencyId, EpisodeId, PatientId, OrderNumber, Status, SentDate, RequestDate from `facetofaceencounters`")
//            //    .Where(string.Format("`facetofaceencounters`.AgencyId in ({0})", agencyIds))
//            //    .And(string.Format("`facetofaceencounters`.PhysicianId in ({0})", ids))
//            //    .And(string.Format("(`facetofaceencounters`.Status = {0} || `facetofaceencounters`.Status = {1})"
//            //    , (int)ScheduleStatus.OrderSentToPhysicianElectronically, status))
//            //    .And("`facetofaceencounters`.IsDeprecated = 0");

//            var query = string.Format(@"select
//                                        ftf.Id,
//                                        ftf.Status,
//                                        ftf.AgencyId, 
//                                        ftf.OrderNumber,
//                                        ftf.PatientId,
//                                        ftf.RequestDate,
//                                        ftf.EpisodeId,                            
//                                        pa.LastName,
//                                        pa.FirstName
//                                            from facetofaceencounters ftf
//                                                 INNER JOIN patients pa ON ftf.PatientId= pa.Id
//                                                    where 
//                                                    ftf.AgencyId = '{0}' AND
//                                                    ftf.PhysicianId in ({1}) AND
//                                                    ftf.Status in ({2}) AND 
//                                                    ftf.IsDeprecated = 0 ", agencyId, ids, allStatus);
            
//            using (var cmd = new FluentCommand<FaceToFaceEncounter>(query))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                    .SetMap(reader=>new FaceToFaceEncounter
//                {
//                    Id=reader.GetGuid("Id"),
//                    AgencyId = reader.GetGuid("AgencyId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId=reader.GetGuid("EpisodeId"),
//                    OrderNumber=reader.GetInt("OrderNumber"),
//                    Status=reader.GetInt("Status"),
//                    RequestDate=reader.GetDateTime("RequestDate"),
//                    PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName")
//                })
//                    .AsList();
//            }

//            return list;
//        }

        public List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysicianAndDate(Guid agencyId, Guid physicianId, int[] status, DateTime startDate, DateTime endDate)
        {
            var list = new List<FaceToFaceEncounter>();
            var allStatus = status.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");

            var query = string.Format(@"select
                                        ftf.Id,
                                        ftf.Status,
                                        ftf.AgencyId, 
                                        ftf.OrderNumber,
                                        ftf.PatientId,
                                        ftf.RequestDate,
                                        ftf.EpisodeId,                            
                                        pa.LastName,
                                        pa.FirstName
                                            from patients pa 
                                                 INNER JOIN facetofaceencounters ftf ON ftf.PatientId= pa.Id
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    ftf.AgencyId = @agencyid AND
                                                    ftf.PhysicianId = @physicianid AND
                                                    ftf.Status in ({0}) AND 
                                                    DATE(ftf.RequestDate) BETWEEN DATE(@startDate) AND DATE(@endDate) AND
                                                    ftf.IsDeprecated = 0 ", allStatus);


            using (var cmd = new FluentCommand<FaceToFaceEncounter>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid",agencyId)
                    .AddGuid("physicianid", physicianId)
                    .AddDateTime("startDate",startDate)
                    .AddDateTime("endDate",endDate)
                    .SetMap(reader => new FaceToFaceEncounter
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        OrderNumber = reader.GetInt("OrderNumber"),
                        Status = reader.GetInt("Status"),
                        RequestDate = reader.GetDateTime("RequestDate"),
                        PatientName = reader.GetStringNullable("LastName") + ", " + reader.GetStringNullable("FirstName")
                    })
                    .AsList();
            }

            return list;
        }

//        public List<PhysicianOrder> GetPhysicianOrdersByPhysician(Guid agencyId, List<Guid> physicianIdentifiers, int status)
//        {
//            var list = new List<PhysicianOrder>();
//            var ids = physicianIdentifiers.Select(s => "'" + s.ToString() + "'").ToArray().Join(",");

//            var query = string.Format(@"select
//                                        po.Id,
//                                        po.Status,
//                                        po.AgencyId, 
//                                        po.OrderNumber,
//                                        po.PatientId,
//                                        po.SentDate ,
//                                        po.OrderDate,
//                                        po.EpisodeId,                            
//                                        pa.LastName,
//                                        pa.FirstName
//                                            from physicianorders po
//                                                 INNER JOIN patients pa ON pa.Id = po.PatientId
//                                                    where 
//                                                    po.AgencyId = '{0}' AND
//                                                    po.PhysicianId in ({1}) AND
//                                                    po.Status = {2} AND 
//                                                    po.IsDeprecated = 0 ", agencyId, ids, status);

//            using (var cmd = new FluentCommand<PhysicianOrder>(query))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                     .SetMap(reader => new PhysicianOrder
//                     {
//                         Id = reader.GetGuid("Id"),
//                         Status = reader.GetInt("Status"),
//                         AgencyId = reader.GetGuid("AgencyId"),
//                         OrderNumber = reader.GetInt("OrderNumber"),
//                         PatientId = reader.GetGuid("PatientId"),
//                         EpisodeId = reader.GetGuid("EpisodeId"),
//                         SentDate = reader.GetDateTime("SentDate"),
//                         OrderDate = reader.GetDateTime("OrderDate"),
//                         PatientName = reader.GetStringNullable("LastName")+", "+reader.GetStringNullable("FirstName")
//                     })
//                    .AsList();
//            }
//            return list;
//        }

        public List<PhysicianOrder> GetPhysicianOrdersByPhysicianAndDate(Guid agencyId, Guid physicianId, int status, DateTime startDate, DateTime endDate)
        {
            var list = new List<PhysicianOrder>();
            var query = string.Format(@"select
                                        po.Id,
                                        po.Status,
                                        po.AgencyId, 
                                        po.OrderNumber,
                                        po.PatientId,
                                        po.OrderDate,
                                        po.EpisodeId,                            
                                        pa.LastName,
                                        pa.FirstName
                                            from  patients pa
                                                 INNER JOIN physicianorders  po ON pa.Id = po.PatientId
                                                    where 
                                                    pa.AgencyId = @agencyid AND
                                                    po.AgencyId = @agencyid AND
                                                    po.PhysicianId = @physicianid AND
                                                    po.Status = {0} AND 
                                                    DATE(po.OrderDate) between DATE(@startdate) and DATE(@enddate) AND
                                                    po.IsDeprecated = 0 ", status);
            
            using (var cmd = new FluentCommand<PhysicianOrder>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("physicianid", physicianId)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate",endDate)
                    .SetMap(reader => new PhysicianOrder
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         OrderNumber = reader.GetInt("OrderNumber"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         OrderDate = reader.GetDateTime("OrderDate"),
                         PatientName = reader.GetStringNullable("LastName")+", "+reader.GetStringNullable("FirstName")
                     })
                    .AsList();
            }


            return list;
        }

        #endregion

        #region Return Comments

        public List<ReturnComment> GetReturnComments(Guid agencyId, Guid episodeId, Guid eventId)
        {
            return database.Find<ReturnComment>(a => a.AgencyId == agencyId && a.EpisodeId == episodeId && a.EventId == eventId && a.IsDeprecated == false).ToList();
        }

        public ReturnComment GetReturnComment(Guid agencyId, int id)
        {
            return database.Find<ReturnComment>(a => a.AgencyId == agencyId && a.Id == id).FirstOrDefault();
        }

        public bool DeleteReturnComments(Guid agencyId, int id)
        {
            var returnComment = database.Single<ReturnComment>(a => a.AgencyId == agencyId && a.Id == id);
            if (returnComment != null)
            {
                returnComment.IsDeprecated = true;
                returnComment.Modified = DateTime.Now;
                database.Update<ReturnComment>(returnComment);
                return true;
            }
            return false;
        }

        public bool AddReturnComment(ReturnComment returnComment)
        {
            bool result = false;

            if (returnComment != null)
            {
                returnComment.Created = DateTime.Now;
                returnComment.Modified = DateTime.Now;

                database.Add<ReturnComment>(returnComment);
                result = true;
            }

            return result;
        }

        public bool UpdateReturnComment(ReturnComment returnComment)
        {
            var result = false;
            try
            {
                if (returnComment != null)
                {
                    returnComment.Modified = DateTime.Now;
                    database.Update<ReturnComment>(returnComment);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        #endregion

        #region UserAccess
        public List<PatientUser> GetPatientUsersByPatient(Guid patientId)
        {
            return database.Find<PatientUser>(pu => pu.PatientId == patientId).ToList();
        }

        public List<SelectedUser> GetUserAccessList(Guid agencyId, Guid patientId)
        {
            var query = "SELECT users.id as UserId, users.FirstName as UserFirstName, users.LastName as UserLastName, users.Credentials as UserCredentials, users.CredentialsOther as UserCredentialsOther, users.Suffix as UserSuffix, users.Roles as UserRoles, COALESCE(pu.id, '" + Guid.Empty.ToString() + "') as PatientUserId FROM users " +
                "LEFT JOIN (SELECT patientusers.Id, patientusers.userid FROM patientusers WHERE patientusers.patientid = @patientid) pu " +
                "ON users.id = pu.userid " +
                "WHERE users.agencyid = @agencyid " +
                "AND users.IsDeprecated = false " +
                "AND pu.id IS null";
            var list = new List<SelectedUser>();
            using (var cmd = new FluentCommand<SelectedUser>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("patientid", patientId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new SelectedUser
                    {
                        UserId = reader.GetGuid("UserId"),
                        FirstName = reader.GetString("UserFirstName"),
                        LastName = reader.GetString("UserLastName"),
                        Suffix = reader.GetStringNullable("UserSuffix"),
                        Credentials = reader.GetStringNullable("UserCredentials"),
                        CredentialsOther = reader.GetStringNullable("UserCredentialsOther"),
                        Roles = reader.GetString("UserRoles"),
                        PatientUserId = reader.GetGuid("PatientUserId")
                    }).AsList();
            }
            //limit list to clinician
            var clinician = new List<SelectedUser>();
            foreach (var user in list)
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    clinician.Add(user);
                }
            }
            clinician = clinician.OrderBy(a => a.DisplayName).ToList();
            return clinician;
        }

        public List<SelectedUser> GetUserAccessibleList(Guid agencyId, Guid patientId)
        {
            var query = "SELECT users.id as UserId, users.FirstName as UserFirstName, users.LastName as UserLastName, users.Credentials as UserCredentials, users.CredentialsOther as UserCredentialsOther, users.Suffix as UserSuffix, users.Roles as UserRoles, COALESCE(pu.id, '" + Guid.Empty.ToString() + "') as PatientUserId FROM users " +
                "RIGHT JOIN (SELECT patientusers.Id, patientusers.userid FROM patientusers WHERE patientusers.patientid = @patientid) pu " +
                "ON users.id = pu.userid " +
                "WHERE users.agencyid = @agencyid " +
                "AND users.IsDeprecated = false";
            var list = new List<SelectedUser>();
            using (var cmd = new FluentCommand<SelectedUser>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("patientid", patientId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new SelectedUser
                    {
                        UserId = reader.GetGuid("UserId"),
                        FirstName = reader.GetString("UserFirstName"),
                        LastName = reader.GetString("UserLastName"),
                        Suffix = reader.GetStringNullable("UserSuffix"),
                        Credentials = reader.GetStringNullable("UserCredentials"),
                        CredentialsOther = reader.GetStringNullable("UserCredentialsOther"),
                        Roles = reader.GetString("UserRoles"),
                        PatientUserId = reader.GetGuid("PatientUserId")
                    }).AsList();
            }
            //limit list to clinician
            var clinician = new List<SelectedUser>();
            foreach (var user in list)
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    clinician.Add(user);
                }
            }
            clinician = clinician.OrderBy(a => a.DisplayName).ToList();
            return clinician;
        }

        public PatientUser GetPatientUser(Guid patientUserId)
        {
            return database.Find<PatientUser>(pu => pu.Id == patientUserId).First();
        }

        public bool AddPatientUser(PatientUser patientUser)
        {
            patientUser.Id = Guid.NewGuid();
            database.Add<PatientUser>(patientUser);
            return true;
        }

        public bool RemovePatientUser(PatientUser patientUser)
        {
            database.Delete<PatientUser>(this.GetPatientAccess(patientUser.PatientId, patientUser.UserId).Id);
            return true;
        }

        public PatientUser GetPatientAccess(Guid patientId, Guid userId)
        {
            var query = new QueryBuilder("SELECT * FROM patientusers").Where("userid = @userId").And("patientid = @patientId").LimitTo(0, 1);
            return new FluentCommand<PatientUser>(query.Build())
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("userId", userId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new PatientUser
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    UserId = reader.GetGuid("UserId")
                }
            ).AsList().First();
        }

        public List<PatientSelection> GetPatientsWithUserAccess(Guid userId, Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name)
        {
            var query = new QueryBuilder("SELECT patients.Id, patients.PatientIdNumber, patients.FirstName, patients.LastName, patients.MiddleInitial FROM patientusers " +
                "JOIN patients on patientusers.patientid = patients.id");
            query.Where("patientusers.userid = @userId").And("patients.AgencyId = @agencyId").And("patients.status = @statusid");

            if (!branchId.IsEmpty())
            {
                query.And("patients.AgencyLocationId = @branchId");
            }
            if (name.IsNotNullOrEmpty())
            {
                query.And(string.Format("patients.PaymentSource like '%{0};%'", paymentSourceId));
            }
            query.And("patients.IsDeprecated = 0").OrderBy("patients.LastName", true);

            return new FluentCommand<PatientSelection>(query.Build())
            .SetConnection("AgencyManagementConnectionString")
            .AddGuid("userId", userId)
            .AddGuid("agencyId", agencyId)
            .AddInt("statusId", statusId)
            .AddInt("paymentSouceId", paymentSourceId)
            .AddGuid("branchId", branchId)
            .SetMap(reader => new PatientSelection
            {
                Id = reader.GetGuid("Id"),
                PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                FirstName = reader.GetString("FirstName").Trim().ToUpper(),
                LastName = reader.GetString("LastName").Trim().ToUpper(),
                MI = reader.GetStringNullable("MiddleInitial").ToUpper()
            })
            .AsList();
        }
        #endregion

        #region PatientAccess

        public MultiGrid GetPatientAccessData(Guid agencyId, Guid userId)
        {
            var access = new MultiGrid();
            var query = "SELECT patients.id as PatientId, patients.FirstName as PatientFirstName, patients.LastName as PatientLastName, COALESCE(pu.id, '" + Guid.Empty.ToString() + "') as PatientUserId FROM patients " +
                "RIGHT JOIN (SELECT patientusers.Id, patientusers.patientid FROM patientusers WHERE patientusers.userid = @userid) pu " +
                "ON patients.id = pu.patientid " +
                "WHERE patients.AgencyId = @agencyid " +
                "AND patients.IsDeprecated = false";
            var list = new List<SelectableItem>();
            using (var cmd = new FluentCommand<SelectableItem>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("userid", userId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new SelectableItem
                    {
                        Id = reader.GetGuid("PatientId"),
                        DisplayName = reader.GetString("PatientLastname") + ", " + reader.GetString("PatientFirstName")
                    }).AsList();
            }
            access.Left = list.OrderBy(p => p.DisplayName).ToList();
            query = "SELECT patients.id as PatientId, patients.FirstName as PatientFirstName, patients.LastName as PatientLastName, COALESCE(pu.id, '" + Guid.Empty.ToString() + "') as PatientUserId FROM patients " +
                "LEFT JOIN (SELECT patientusers.Id, patientusers.patientid FROM patientusers WHERE patientusers.userid = @userid) pu " +
                "ON patients.id = pu.patientid " +
                "WHERE patients.AgencyId = @agencyid " +
                "AND patients.IsDeprecated = false " +
                "AND pu.id IS null";
            list = new List<SelectableItem>();
            using (var cmd = new FluentCommand<SelectableItem>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("userid", userId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new SelectableItem
                    {
                        Id = reader.GetGuid("PatientId"),
                        DisplayName = reader.GetString("PatientLastname") + ", " + reader.GetString("PatientFirstName")
                    }).AsList();
            }
            access.Right = list.OrderBy(p => p.DisplayName).ToList();
            return access;
        }

        #endregion

        #region AgencyTeams

        public List<PatientSelection> GetAgencyTeamAccessPatients(Guid agencyId, Guid currentUserId)
        {
            return null;
        }

        #endregion
        
        #region Cahps Report
        public List<Patient> GetPatientsAdmissionSourceByIds(Guid agencyId, List<Guid> patientIds)
        {
            var list = new List<Patient>();
            if (patientIds != null && patientIds.Count > 0)
            {
                var ids = patientIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format
                    (
                        "SELECT patients.Id, patients.AdmissionSource " +
                        "FROM agencymanagement.patients "+
                        "WHERE patients.AgencyId = '{0}' "+
                        "AND patients.Id IN ( {1} ) ", agencyId, ids);
                using (var cmd = new FluentCommand<Patient>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Patient
                     {
                         Id = reader.GetGuid("Id"),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource")
                     }).AsList();
                }
            }
            return list;
        }
        #endregion

        #region VisitsByPayorReports

        public List<PatientsAndEpisode> GetPatientAndEpisodesByBranchStatusInsurance(Guid agencyId, Guid branchId, int statusId, string insuranceIds, DateTime startDate, DateTime endDate)
        {
            List<PatientsAndEpisode> outPatientEpisodes = null;
            if (startDate != null && endDate != null) { } else { startDate = DateTime.Now.AddDays(-59); endDate = DateTime.Now; }
            string branchString = default(string);
            if (!branchId.IsNull() && branchId.IsNotEmpty()) { branchString = " AND patients.AgencyLocationId = @branchId "; }
            string statusString = default(string);
            if (!statusId.IsNull() && statusId != 0) { statusString = " AND patients.`Status` = @statusid "; }
            string insuranceString = default(string);
            if (insuranceIds == string.Empty)
            {
                insuranceString = string.Empty;
            }
            else if (insuranceIds.IsNotNullOrEmpty())
            {
                insuranceString = string.Format(" AND (patients.PrimaryInsurance IN ( {0} ) || patients.SecondaryInsurance IN ( {0} ) || patients.TertiaryInsurance IN ( {0} )) ", insuranceIds);
            }
            var script = string.Format(@"SELECT
                                patients.Id as patientId,
                                patients.PrimaryInsurance as patientPrimaryInsurance,
                                patients.SecondaryInsurance as patientSecondaryInsurance,
                                patients.TertiaryInsurance as patientTertiaryInsurance,
                                patientepisodes.`Schedule` as patientEpisodeSchedule 
                                    FROM 
                                        agencymanagement.patients 
                                            INNER JOIN agencymanagement.patientepisodes ON patients.Id = patientepisodes.PatientId 
                                                WHERE 
                                                    patients.AgencyId = @agencyid  {0} {1} {2} AND
                                                    DATE(patientepisodes.StartDate) <= DATE(@enddate) AND 
                                                    DATE(patientepisodes.EndDate) >= DATE(@startdate)"
                                                                            , branchString, statusString, insuranceString);
            using (var cmd = new FluentCommand<PatientsAndEpisode>(script))
            {
                outPatientEpisodes = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                 .AddDateTime("startdate", startDate)
                 .AddDateTime("enddate", endDate)
                 .AddInt("statusid", statusId)
                 .AddString("insuranceids", insuranceIds)
                 .SetMap(reader => new PatientsAndEpisode
                 {
                     Id = reader.GetGuidIncludeEmpty("patientId"),
                     PrimaryInsurance = reader.GetStringNullable("patientPrimaryInsurance"),
                     SecondaryInsurance = reader.GetStringNullable("patientSecondaryInsurance"),
                     TertiaryInsurance = reader.GetStringNullable("patientTertiaryInsurance"),
                     ///Schedule = reader.GetStringNullable("patientEpisodeSchedule"),
                     ScheduleEvents = reader.GetStringNullable("patientEpisodeSchedule").ToObject<List<ScheduleEvent>>()
                 }).AsList();
            }
            return outPatientEpisodes;
        }

        #endregion

        #region Visit Verify

        public List<PatientVisitLog> GetVisitVerificationLogs(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientVisitLog>();
            var script = @"SELECT patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, patientvisitlogs.Id, " +
                "patientvisitlogs.TaskId, patientvisitlogs.UserId, patientvisitlogs.AgencyId, patientvisitlogs.PatientId, patientvisitlogs.EpisodeId, " +
                "patientvisitlogs.VerifiedLatitude, patientvisitlogs.VerifiedLongitude, patientvisitlogs.Created, patientvisitlogs.VerifiedAddress, " +
                "patientvisitlogs.VerifiedDateTime, patientvisitlogs.IsPatientUnableToSign, patientvisitlogs.PatientUnableToSignReason, patientvisitlogs.PatientSignatureAssetId, " +
                "users.FirstName as UserFirstName, users.LastName as UserLastName, users.MiddleName as UserMiddleName, " +
                "patientepisodes.Schedule, patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate " +
                "FROM patientvisitlogs INNER JOIN patients ON patientvisitlogs.PatientId = patients.Id " +
                "INNER JOIN users ON patientvisitlogs.UserId = users.Id " +
                "INNER JOIN patientepisodes ON patientvisitlogs.EpisodeId = patientepisodes.Id " +
                "WHERE patientvisitlogs.AgencyId = @agencyid AND patientvisitlogs.VerifiedDateTime BETWEEN @startDate AND @endDate;";

            using (var cmd = new FluentCommand<PatientVisitLog>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientVisitLog
                {
                    Id = reader.GetGuid("Id"),
                    TaskId = reader.GetGuid("TaskId"),
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Created = reader.GetDateTime("Created"),
                    TaskName = reader.GetString("Schedule"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    VerifiedLatitude = reader.GetDecimal("VerifiedLatitude"),
                    VerifiedDateTime = reader.GetDateTime("VerifiedDateTime"),
                    VerifiedLongitude = reader.GetDecimal("VerifiedLongitude"),
                    VerifiedAddress = reader.GetStringNullable("VerifiedAddress"),
                    PatientSignatureAssetId = reader.GetGuid("PatientSignatureAssetId"),
                    Signature = (reader.GetBoolean("IsPatientUnableToSign") ? "No" : "Yes"),
                    PatientUnableToSignReason = reader.GetStringNullable("PatientUnableToSignReason"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    UserName = reader.GetString("UserLastName").ToUpperCase() + ", " + reader.GetString("UserFirstName").ToUpperCase() + " " + reader.GetStringNullable("UserMiddleName").ToInitial()

                }).AsList();
            }
            return list;
        }

        public PatientVisitLog GetVisitVerificationLog(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId)
        {
            PatientVisitLog log = null;
            var script = @"SELECT patients.FirstName as FirstName, patients.LastName as LastName, patients.MiddleInitial as MiddleInitial, patients.PatientIdNumber as PatientMRN, " +
                "patients.DOB, patients.StartofCareDate, patients.AddressLine1, patients.AddressLine2, patients.AddressCity, patients.AddressStateCode, patients.AddressZipCode, " +
                "patientvisitlogs.Id, patientvisitlogs.TaskId, patientvisitlogs.UserId, patientvisitlogs.AgencyId, patientvisitlogs.PatientId, patientvisitlogs.EpisodeId, " +
                "patientvisitlogs.VerifiedLatitude, patientvisitlogs.VerifiedLongitude, patientvisitlogs.Created, patientvisitlogs.VerifiedAddress, " +
                "patientvisitlogs.VerifiedDateTime, patientvisitlogs.IsPatientUnableToSign, patientvisitlogs.PatientUnableToSignReason, patientvisitlogs.PatientSignatureAssetId, " +
                "users.FirstName as UserFirstName, users.LastName as UserLastName, users.MiddleName as UserMiddleName, " +
                "patientepisodes.Schedule, patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate " +
                "FROM patientvisitlogs INNER JOIN patients ON patientvisitlogs.PatientId = patients.Id " +
                "INNER JOIN users ON patientvisitlogs.UserId = users.Id " +
                "INNER JOIN patientepisodes ON patientvisitlogs.EpisodeId = patientepisodes.Id " +
                "WHERE patientvisitlogs.AgencyId = @agencyid AND patientvisitlogs.PatientId = @patientid " +
                "AND patientvisitlogs.EpisodeId = @episodeid AND patientvisitlogs.TaskId = @eventId LIMIT 0, 1;";

            using (var cmd = new FluentCommand<PatientVisitLog>(script))
            {
                log = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddGuid("eventId", eventId)
                .SetMap(reader => new PatientVisitLog
                {
                    Id = reader.GetGuid("Id"),
                    DOB = reader.GetDateTime("DOB"),
                    TaskId = reader.GetGuid("TaskId"),
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Created = reader.GetDateTime("Created"),
                    TaskName = reader.GetString("Schedule"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    StartofCareDate = reader.GetDateTime("StartofCareDate"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    PatientMRN = reader.GetStringNullable("PatientMRN"),
                    VerifiedLatitude = reader.GetDecimal("VerifiedLatitude"),
                    VerifiedDateTime = reader.GetDateTime("VerifiedDateTime"),
                    VerifiedLongitude = reader.GetDecimal("VerifiedLongitude"),
                    VerifiedAddress = reader.GetStringNullable("VerifiedAddress"),
                    PatientSignatureAssetId = reader.GetGuid("PatientSignatureAssetId"),
                    Signature = (reader.GetBoolean("IsPatientUnableToSign") ? "No" : "Yes"),
                    PatientUnableToSignReason = reader.GetStringNullable("PatientUnableToSignReason"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    UserName = reader.GetString("UserLastName").ToUpperCase() + ", " + reader.GetString("UserFirstName").ToUpperCase() + " " + reader.GetStringNullable("UserMiddleName").ToInitial()

                }).AsSingle();
            }
            return log;
        }

        #endregion

    }
}
