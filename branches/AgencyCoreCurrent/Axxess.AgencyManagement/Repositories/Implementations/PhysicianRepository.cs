﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Domain;
    using Axxess.Core.Infrastructure;

    public class PhysicianRepository : IPhysicianRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PhysicianRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }
        #endregion

        #region IPhysicianRepository Members

        public bool Add(AgencyPhysician physician)
        {
            var result = false;
            if (physician != null)
            {
                if (physician.PhoneWorkArray != null && physician.PhoneWorkArray.Count == 3)
                {
                    physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                }
                if (physician.PhoneAltArray != null && physician.PhoneAltArray.Count == 3)
                {
                    physician.PhoneAlternate = physician.PhoneAltArray.ToArray().PhoneEncode();
                }
                if (physician.FaxNumberArray != null && physician.FaxNumberArray.Count == 3)
                {
                    physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                }
                physician.Created = DateTime.Now;
                physician.Modified = DateTime.Now;
                database.Add<AgencyPhysician>(physician);
                result = true;
                PhysicianEngine.Refresh(physician.AgencyId);
            }
            return result;
        }

        //public List<CarePlanOversight> GetCPO(Guid agencyId,Guid physicianLoginId)
        //{
        //    return database.Find<CarePlanOversight>(c => c.AgencyId==agencyId && c.PhysicianLoginId == physicianLoginId && c.IsDeprecated == false).OrderByDescending(p => p.LogDate).ToList();
        //}

        public List<CarePlanOversight> GetCPO(Guid agencyId, Guid physicianLoginId, DateTime startDate, DateTime endDate)
        {
            return database.Find<CarePlanOversight>(c => c.AgencyId == agencyId && c.PhysicianLoginId == physicianLoginId && c.LogDate >= startDate && c.LogDate <= endDate && c.IsDeprecated == false).OrderByDescending(p => p.LogDate).ToList();
        }

        public CarePlanOversight GetCPOById(Guid id)
        {
            return database.Find<CarePlanOversight>(c => c.Id == id).FirstOrDefault();
        }

        public bool Edit(AgencyPhysician physician)
        {
            bool result = false;
            if (physician != null)
            {
                physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                physician.PhoneAlternate = physician.PhoneAltArray.ToArray().PhoneEncode();
                physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                var physicianInfo = database.Single<AgencyPhysician>(p => p.Id == physician.Id);
                if (physicianInfo != null)
                {
                    physicianInfo.FirstName = physician.FirstName;
                    physicianInfo.LastName = physician.LastName;
                    physicianInfo.PhoneWork = physician.PhoneWork;
                    physicianInfo.PhoneAlternate = physician.PhoneAlternate;
                    physicianInfo.FaxNumber = physician.FaxNumber;
                    physicianInfo.EmailAddress = physician.EmailAddress;
                    physicianInfo.NPI = physician.NPI;
                    physicianInfo.LoginId = physician.LoginId;
                    physicianInfo.PhysicianAccess = physician.PhysicianAccess;
                    physicianInfo.AddressLine1 = physician.AddressLine1;
                    physicianInfo.AddressLine2 = physician.AddressLine2;
                    physicianInfo.AddressCity = physician.AddressCity;
                    physicianInfo.AddressStateCode = physician.AddressStateCode;
                    physicianInfo.AddressZipCode = physician.AddressZipCode;
                    database.Update<AgencyPhysician>(physicianInfo);
                    result = true;
                    PhysicianEngine.Refresh(physicianInfo.AgencyId);
                }
            }
            return result;
        }

        public bool Delete(Guid agencyId, Guid id)
        {
            var physician = database.Single<AgencyPhysician>(p => p.AgencyId == agencyId && p.Id == id);
            if (physician != null)
            {
                physician.IsDeprecated = true;
                physician.Modified = DateTime.Now;
                database.Update<AgencyPhysician>(physician);
                PhysicianEngine.Refresh(physician.AgencyId);
                return true;
            }
            return false;
        }

        public bool Update(AgencyPhysician physician)
        {
            bool result = false;
            if (physician != null)
            {
                var physicianInfo = database.Single<AgencyPhysician>(p => p.Id == physician.Id);
                if (physicianInfo != null)
                {
                    physicianInfo.LastName = physician.LastName;
                    physicianInfo.FirstName = physician.FirstName;
                    physicianInfo.MiddleName = physician.MiddleName;
                    physicianInfo.Credentials = physician.Credentials;

                    if (physician.PhoneWorkArray != null && physician.PhoneWorkArray.Count > 2)
                    {
                        physicianInfo.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                    }
                    if (physician.PhoneAltArray != null && physician.PhoneAltArray.Count > 2)
                    {
                        physicianInfo.PhoneAlternate = physician.PhoneAltArray.ToArray().PhoneEncode();
                    }
                    if (physician.FaxNumberArray != null && physician.FaxNumberArray.Count > 2)
                    {
                        physicianInfo.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    physicianInfo.NPI = physician.NPI;
                    physicianInfo.EmailAddress = physician.EmailAddress;
                    physicianInfo.AddressLine1 = physician.AddressLine1;
                    physicianInfo.AddressLine2 = physician.AddressLine2;
                    physicianInfo.AddressCity = physician.AddressCity;
                    physicianInfo.AddressStateCode = physician.AddressStateCode;
                    physicianInfo.AddressZipCode = physician.AddressZipCode;
                    physicianInfo.PhysicianAccess = physician.PhysicianAccess;
                    physicianInfo.LoginId = physician.LoginId;
                    if (physician.Licenses.IsNotNullOrEmpty())
                    {
                        physicianInfo.Licenses = physician.Licenses;
                    }
                    physicianInfo.Modified = DateTime.Now;
                    database.Update<AgencyPhysician>(physicianInfo);
                    result = true;
                    PhysicianEngine.Refresh(physicianInfo.AgencyId);
                }
            }
            return result;
        }

        public bool Link(Guid patientId, Guid physicianId, bool isPrimary) 
        {
            var result = false;
            if (!patientId.IsEmpty() && !physicianId.IsEmpty())
            {
                try
                {
                    var patientPhysician = new PatientPhysician
                    {
                        PatientId = patientId,
                        PhysicianId = physicianId,
                        IsPrimary = isPrimary
                    };
                    database.Add<PatientPhysician>(patientPhysician);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool Unlink(Guid patientId, Guid physicianId)
        {
            var result = false;
            if (!patientId.IsEmpty() && !physicianId.IsEmpty())
            {
                database.DeleteMany<PatientPhysician>(pp => pp.PatientId == patientId && pp.PhysicianId == physicianId);
                result = true;
            }
            return result;
        }

        public bool UnlinkAll(Guid patientId)
        {
            var result = false;
            if (!patientId.IsEmpty())
            {
                database.DeleteMany<PatientPhysician>(pp => pp.PatientId == patientId);
                result = true;
            }
            return result;
        }

        public AgencyPhysician Get(Guid physicianId, Guid agencyId)
        {
            return database.Single<AgencyPhysician>(p => p.Id == physicianId && p.AgencyId == agencyId);
        }

        public IList<AgencyPhysician> GetByLoginId(Guid loginId)
        {
            return database.Find<AgencyPhysician>(p => p.LoginId == loginId && p.PhysicianAccess == true && p.IsDeprecated == false).ToList();
        }

        public IDictionary<string, NameIdPair> GetAgencyPhysiciansByLoginIdLean(Guid LoginId)
        {
            IDictionary<string, NameIdPair> list = new Dictionary<string, NameIdPair>();

            var script = @"SELECT 
                    ap.`Id`, 
                    ap.`AgencyId` ,
                    ag.Name
                        FROM
                           agencies ag INNER JOIN
                            agencymanagement.agencyphysicians ap 
                                ON ag.Id = ap.AgencyId
                                    WHERE
                                        ap.LoginId = @loginid AND
                                        ap.PhysicianAccess = 1 AND
                                        ap.IsDeprecated = 0";

            using (var cmd = new FluentCommand<NameIdPair>(script))
                    {
                        list = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("loginid", LoginId)
                        .SetDictonaryId("AgencyId")
                        .SetMap(reader => new NameIdPair
                        {
                            Id = reader.GetGuid("Id"),
                            Name = reader.GetStringNullable("Name"),

                        }).AsDictionary();
                    }
            return list;
        }

        public IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId)
        {
            return database.Find<AgencyPhysician>(p => p.AgencyId == agencyId && p.IsDeprecated == false).OrderBy(p => p.FirstName).ToList();
        }

        public IList<AgencyPhysician> GetAgencyPhysiciansLean(Guid agencyId)
        {
            List<AgencyPhysician> list = new List<AgencyPhysician>();

            var script = @"SELECT ap.`Id`, ap.`NPI`, ap.`Credentials`, ap.`FirstName`, ap.`LastName`, ap.`MiddleName`, ap.`Gender`, " + 
                                "ap.`AddressLine1`, ap.`AddressLine2`, ap.`AddressCity`, ap.`AddressStateCode`, ap.`AddressZipCode`, ap.`PhoneWork`, ap.`PhoneAlternate`, " + 
                                "ap.`FaxNumber`, ap.`EmailAddress`, ap.`PhysicianAccess` " + 
                            "FROM agencymanagement.agencyphysicians ap " +
                            "WHERE ap.AgencyId = @agencyid AND ap.IsDeprecated = 0";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    NPI = reader.GetStringNullable("NPI"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Credentials = reader.GetStringNullable("Credentials"),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    PhysicianAccess = reader.GetBoolean("PhysicianAccess")
                }).AsList();
            }
            return list;
        }

        public IList<AgencyPhysician> GetAllPhysicians()
        {
            return database.Find<AgencyPhysician>(p => p.IsDeprecated == false).OrderBy(p => p.FirstName).ToList();
        }

        public IList<Patient> GetPhysicanPatients(Guid physicianId, Guid agencyId)
        {
            var patients = new List<Patient>();
            var patientPhysicians = database.Find<PatientPhysician>(p => p.PhysicianId == physicianId);
            if (patientPhysicians!=null && patientPhysicians.Count > 0)
            {
                patientPhysicians.ForEach(pp =>
                {
                    var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == pp.PatientId);
                    if (patient != null)
                    {
                        patients.Add(patient);
                    }
                });
            }
            return patients;
        }

        public bool DoesPhysicianExist(Guid patientId, Guid physicianId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            bool result = false;
            var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            foreach (PatientPhysician physician in patientPhysician)
            {
                if (physician.PhysicianId == physicianId)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public bool DoesPhysicianExistInAgency(Guid agencyId, string NPI, string zipcode)
        {
            bool result = false;
            if (zipcode.IsNotNullOrEmpty())
            {
                var agencyPhysician = database.Find<AgencyPhysician>(a => a.AgencyId == agencyId && a.NPI == NPI && a.AddressZipCode == zipcode && a.IsDeprecated == false);
                if (agencyPhysician.Count > 0) result = true;
            }
            return result;
        }

        public IList<AgencyPhysician> GetPatientPhysicians(Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            IList<AgencyPhysician> physicians = new List<AgencyPhysician>();
            var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            foreach (PatientPhysician physician in patientPhysician)
            {
                var agencyPhysician = database.Single<AgencyPhysician>(p => p.AgencyId == agencyId && p.Id == physician.PhysicianId);
                if (agencyPhysician != null)
                {
                    agencyPhysician.Primary = physician.IsPrimary;
                    physicians.Add(agencyPhysician);
                }
            }

            return physicians;
        }

        public bool SetPrimary(Guid patientId, Guid physicianId)
        {
            bool result = false;
            bool flag = false;
            var patientPhysicians = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            if (patientPhysicians != null)
            {
                foreach (PatientPhysician contat in patientPhysicians)
                {
                    if (contat.PhysicianId == physicianId)
                    {
                        contat.IsPrimary = true;
                        flag = true;
                    }
                    else
                    {
                        contat.IsPrimary = false;
                    }
                }
                if (flag)
                {
                    database.UpdateMany<PatientPhysician>(patientPhysicians);
                    result = true;
                }
            }
            return result;
        }

        public AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var physicianContact = database.Single<AgencyPhysician>(p => p.AgencyId == agencyId && p.Id == physicianId);
            var patientPhysician = database.Single<PatientPhysician>(p => p.PatientId == patientId && p.PhysicianId == physicianId);
            physicianContact.Primary = patientPhysician.IsPrimary;

            return physicianContact;
        }

        public IList<PhysicainLicense> GeAgencyPhysicianLicenses(Guid agencyId, Guid physicianId)
        {
            var physician = Get(physicianId, agencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    return physician.LicensesArray;
                }
            }
            return new List<PhysicainLicense>();
        }

        public AgencyPhysician GetPatientPrimaryPhysician(Guid agencyId, Guid patientId)
        {
            AgencyPhysician patientPrimaryPhysician = null;
            var script = @"SELECT 
                        agencyphysicians.Id as Id ,
                        agencyphysicians.FirstName as FirstName ,
                        agencyphysicians.LastName as LastName , 
                        agencyphysicians.MiddleName as MiddleName ,
                        agencyphysicians.Gender as Gender ,
                        agencyphysicians.AddressLine1 as AddressLine1 ,
                        agencyphysicians.AddressLine2 as AddressLine2 ,
                        agencyphysicians.AddressCity as AddressCity ,
                        agencyphysicians.AddressStateCode as AddressStateCode ,
                        agencyphysicians.AddressZipCode as AddressZipCode ,
                        agencyphysicians.PhoneWork as PhoneWork  ,
                        agencyphysicians.FaxNumber as FaxNumber,
                        agencyphysicians.NPI as NPI  
                            FROM
                                agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                    WHERE 
                                        agencyphysicians.AgencyId = @agencyid  AND
                                        patientphysicians.PatientId = @patientid AND
                                        patientphysicians.IsPrimary = 1  AND
                                        agencyphysicians.IsDeprecated = 0
                                           LIMIT 1 ";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                patientPrimaryPhysician = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    NPI = reader.GetStringNullable("NPI"),
                    FaxNumber = reader.GetStringNullable("FaxNumber")
                })
                .AsSingle();
            }
            return patientPrimaryPhysician;
        }

        public AgencyPhysician GetPatientPrimaryOrFirstPhysician(Guid agencyId, Guid patientId)
        {
            AgencyPhysician patientPrimaryPhysician = null;
            var script = @"SELECT 
                        agencyphysicians.Id as Id ,
                        agencyphysicians.FirstName as FirstName ,
                        agencyphysicians.LastName as LastName , 
                        agencyphysicians.MiddleName as MiddleName ,
                        agencyphysicians.Gender as Gender ,
                        agencyphysicians.AddressLine1 as AddressLine1 ,
                        agencyphysicians.AddressLine2 as AddressLine2 ,
                        agencyphysicians.AddressCity as AddressCity ,
                        agencyphysicians.AddressStateCode as AddressStateCode ,
                        agencyphysicians.AddressZipCode as AddressZipCode ,
                        agencyphysicians.PhoneWork as PhoneWork,
                        agencyphysicians.FaxNumber as FaxNumber,
                        agencyphysicians.NPI as NPI  
                            FROM
                                agencyphysicians INNER JOIN patientphysicians ON agencyphysicians.Id = patientphysicians.PhysicianId  
                                    WHERE 
                                        agencyphysicians.AgencyId = @agencyid  AND
                                        patientphysicians.PatientId = @patientid AND
                                        agencyphysicians.IsDeprecated = 0 ORDER BY  patientphysicians.IsPrimary DESC
                                           LIMIT 1 ";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                patientPrimaryPhysician = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new AgencyPhysician
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MiddleName = reader.GetStringNullable("MiddleName").ToUpperCase(),
                    Gender = reader.GetStringNullable("Gender"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    NPI = reader.GetStringNullable("NPI"),
                    FaxNumber = reader.GetStringNullable("FaxNumber")
                })
                .AsSingle();
            }
            return patientPrimaryPhysician;
        }

        #endregion

        #region patientListExporterHelperMethods

        public Guid getPrimaryPhysicianId(Guid PatientId) 
        {
            Guid outResultPhysicianId = Guid.Empty;

            Guid EmptyGuid = new Guid();
            PatientPhysician ppp = new PatientPhysician();
            List<PatientPhysician> list = new List<PatientPhysician>();

            var script = string.Format(
                "select patientphysicians.`Id` as Id, patientphysicians.`PatientId` as PatientId, patientphysicians.`PhysicianId` as PhysicianId "+
                "from agencymanagement.patientphysicians "+
                "where patientphysicians.PatientId='{0}' "+
                "And patientphysicians.IsPrimary='1'; ",PatientId);

            using (var cmd = new FluentCommand<PatientPhysician>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("goguid", EmptyGuid)
                .SetMap(reader => new PatientPhysician
                {
                    Id = reader.GetInt("Id"),
                    PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                    PhysicianId = reader.GetGuidIncludeEmpty("PhysicianId")
                }).AsList();
            }

            foreach (var item in list) 
            {
                outResultPhysicianId = item.PhysicianId;
            }
            return outResultPhysicianId;
        }

        #endregion

    }
}
