﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;
    using Extensions;

    using SubSonic.Repository;
    using System.Text;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool ToggleDelete(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.IsDeprecated = !agency.IsDeprecated;
                agency.IsSuspended = !agency.IsSuspended;
                agency.Modified = DateTime.Now;
                database.Update<Agency>(agency);
                return true;
            }
            return false;
        }

        public List<AgencyLite> GetAllAgencies()
        {
            var list = new List<AgencyLite>();
            var sql = new StringBuilder()
                .Append("SELECT agencymanagement.agencies.Id, agencymanagement.agencies.Name, agencymanagement.agencies.Created, ")
                .Append("agencymanagement.agencies.IsFrozen, agencymanagement.agencies.IsSuspended, agencymanagement.agencies.IsDeprecated, ")
                .Append("L1.DisplayName AS SalesPerson, L2.DisplayName AS Trainer, agencymanagement.agencies.ContactPersonEmail, ")
                .Append("CONCAT_WS(' ', IFNULL(agencymanagement.agencies.ContactPersonFirstName, ''), IFNULL(agencymanagement.agencies.ContactPersonLastName, '')) AS ContactPerson, ")
                .Append("agencymanagement.agencies.ContactPersonPhone, agencymanagement.agencylocations.AddressCity, agencymanagement.agencylocations.AddressStateCode ")
                .Append("FROM agencymanagement.agencies ")
                .Append("INNER JOIN agencymanagement.agencylocations on agencymanagement.agencies.Id = agencymanagement.agencylocations.AgencyId ")
                .Append("LEFT JOIN axxessmembership.logins L1 on agencymanagement.agencies.SalesPerson = L1.Id ")
                .Append("LEFT JOIN axxessmembership.logins L2 on agencymanagement.agencies.Trainer = L2.Id ")
                .Append("WHERE agencymanagement.agencylocations.IsMainOffice = 1 ORDER BY agencymanagement.agencies.Created asc")
                .ToString();

            using (var cmd = new FluentCommand<AgencyLite>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyLite
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    City = reader.GetString("AddressCity"),
                    Created = reader.GetDateTime("Created"),
                    IsFrozen = reader.GetBoolean("IsFrozen"),
                    State = reader.GetString("AddressStateCode"),
                    Trainer = reader.GetStringNullable("Trainer"),
                    IsSuspended = reader.GetBoolean("IsSuspended"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    SalesPerson = reader.GetStringNullable("SalesPerson"),
                    ContactPersonEmail = reader.GetString("ContactPersonEmail"),
                    ContactPersonDisplayName = reader.GetString("ContactPerson"),
                    ContactPersonPhoneFormatted = reader.GetStringNullable("ContactPersonPhone").ToPhone()
                }).AsList();
            }

            return list;
        }

        public List<AgencyUser> GetUserNames()
        {
            var script = @"SELECT agencies.Id as AgencyId, users.Id as UserId, users.FirstName, users.LastName, users.Suffix, users.Credentials, " +
                "users.CredentialsOther, users.IsDeprecated FROM agencies INNER JOIN users ON agencies.Id = users.AgencyId ORDER BY agencies.Id";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public List<AgencyUser> GetUserNames(Guid agencyId)
        {
            var script = @"SELECT Id, FirstName, LastName, Suffix, Credentials, " +
                "CredentialsOther, IsDeprecated FROM users " +
                "Where AgencyId = @agencyid";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("@agencyid", agencyId)
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("Id"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public Agency Get(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }

            return agency;
        }

        public Agency GetAgencyContactInformation(Guid agencyId)
        {
            var script = @"SELECT Name, AccountId, ContactPersonFirstName, ContactPersonLastName, ContactPersonEmail, ContactPersonPhone FROM agencies Where Id = @agencyid";
            return new FluentCommand<Agency>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("@agencyid", agencyId)
                .SetMap(reader => new Agency
                {
                    Name = reader.GetStringNullable("Name"),
                    AccountId = reader.GetStringNullable("AccountId"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone")
                }).AsSingle();
        }

        public Agency GetAgencyOnly(Guid id)
        {
            return database.Single<Agency>(a => a.Id == id);
        }

        public List<Agency> GetPhysicianAgenies(Guid loginId)
        {
            var agencies = new List<Agency>();
            var agencyIds=new List<Guid>();
            var agencyPhysicians = database.Find<AgencyPhysician>(a => a.LoginId == loginId).ToList();
            foreach (AgencyPhysician ap in agencyPhysicians)
            {
                if (!agencyIds.Contains(ap.AgencyId))
                    agencyIds.Add(ap.AgencyId);
            }
            agencyIds.ForEach(a=>
                agencies.Add(database.Find<Agency>(ag=>ag.Id==a && ag.IsDeprecated==false).SingleOrDefault()));
            return agencies;
        }
        
        public Agency GetWithBranches(Guid agencyId)
        {
            var agency = database.Single<Agency>(a => a.Id == agencyId);
            if (agency != null)
            {
                agency.Branches = database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
            }

            return agency;
        }

        public Agency GetById(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }
            return agency;
        }

        public bool Add(Agency agency)
        {
            if (agency != null)
            {
                agency.Id = Guid.NewGuid();
                agency.OasisAuditVendor = (int)OasisAuditVendors.HHG;
                agency.Created = DateTime.Now;
                agency.Modified = DateTime.Now;
                database.Add<Agency>(agency);
                return true;
            }
            return false;
        }

        public bool AddUpgrade(AgencyUpgrade agencyUpgrade)
        {
            if (agencyUpgrade != null)
            {
                database.Add<AgencyUpgrade>(agencyUpgrade);
                return true;
            }
            return false;
        }

        public bool AddChange(AgencyChange agencyChange)
        {
            if (agencyChange != null)
            {
                agencyChange.Id = Guid.NewGuid();
                agencyChange.Created = DateTime.Now;
                database.Add<AgencyChange>(agencyChange);
                return true;
            }
            return false;
        }

        public bool AddLocation(AgencyLocation agencyLocation)
        {
            if (agencyLocation != null)
            {
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }

                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                return true;
            }

            return false;
        }

        public bool UpdateLocation(AgencyLocation agencyLocation)
        {
            var result = false;
            if (agencyLocation != null)
            {
                agencyLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(agencyLocation);
                result = true;
            }
            return result;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
        }



        public int GetBranchesCount(Guid agencyId)
        {
            try
            {
                string script = @"SELECT COUNT(Id) FROM agencylocations WHERE AgencyId = @agencyid ";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .AsScalar();
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id );
        }

        public AgencyLocation FindLocationOrMain(Guid agencyId, Guid id)
        {
            AgencyLocation agencyLocation = null;
            if (!id.IsEmpty())
            {
                agencyLocation = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == id);
            }
            if (agencyLocation == null)
            {
                agencyLocation = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
            }
            if (agencyLocation != null)
            {
                var agency = database.Single<Agency>(a => a.Id == agencyId);
                if (agency != null)
                {
                    agencyLocation.AgencyName = agency.Name;
                }
            }
            return agencyLocation;
        }

        //public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        //{
        //    var location = new AgencyLocation();
            
        //        var script = @"SELECT al.`Id`, al.`AgencyId`, al.`Name`, al.`CustomId`, al.`CBSA`, al.`MedicareProviderNumber`, al.`Cost`, al.`BillData`"+
        //                "al.`SubmitterId`, al.`SubmitterName`, al.`SubmitterPhone`, al.`SubmitterFax`, al.`Payor`, al.`BranchId`, al.`BranchIdOther`, al.`Ub04Locator81cca`," +
        //                "al.`TaxId`, `TaxIdType`, al.`NationalProviderNumber`, al.`MedicaidProviderNumber`, al.`ContactPersonFirstName`," +
        //                "al.`ContactPersonLastName`, al.`ContactPersonEmail`, al.`ContactPersonPhone`, al.`CahpsVendor`, al.`CahpsVendorId`, al.`CahpsSurveyDesignator`," +
        //                "al.`IsAxxessTheBiller`, al.`OasisAuditVendor`, al.`IsLocationStandAlone`, al.`UseServiceSupplies`, al.`UseStandardSupplyReimbursement`," +
        //                "al.`UseWoundcareSupplies`, al.`ServiceSuppliesRevenueCode`, al.`ServiceSuppliesDescription`, ai.`AddressLine1` as MedicareAddressLine1," +
        //                "ai.`AddressLine2` as MedicareAddressLine2, ai.`AddressCity` as MedicareAddressCity, ai.`AddressStateCode` as MedicareAddressStateCode, ai.`AddressZipCode` as MedicareAddressZipCode" +
        //            "FROM agencylocations al " +
        //            "LEFT JOIN agencymedicareinsurances ai ON ai.AgencyId = al.AgencyId AND ai.MedicareId = al.Payor " +
        //            "WHERE al.id = @id AND al.agencyid = @agencyid";

        //        using (var cmd = new FluentCommand<AgencyLocation>(script))
        //        {
        //            location = cmd.SetConnection("AgencyManagementConnectionString")
        //             .AddGuid("agencyid", agencyId)
        //             .AddGuid("id", Id)
        //             .SetMap(reader => new AgencyLocation
        //             {
        //                 Id = reader.GetGuid("Id"),
        //                 AgencyId = reader.GetGuid("AgencyId"),
        //                 Name = reader.GetStringNullable("Name"),
        //                 CustomId = reader.GetStringNullable("CustomId"),
        //                 CBSA = reader.GetStringNullable("CBSA"),
        //                 MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
        //                 Cost = reader.GetStringNullable("Cost"),
        //                 BillData = reader.GetStringNullable("BillData"),
        //                 SubmitterId = reader.GetStringNullable("SubmitterId"),
        //                 SubmitterName = reader.GetStringNullable("SubmitterName"),
        //                 SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
        //                 SubmitterFax = reader.GetStringNullable("SubmitterFax"),
        //                 Payor = reader.GetStringNullable("Payor"),
        //                 BranchId = reader.GetStringNullable("CustomId"),
        //                 BranchIdOther = reader.GetStringNullable("CustomId"),
        //                 Ub04Locator81cca = reader.GetStringNullable("CustomId"),
        //                 TaxId = reader.GetStringNullable("CustomId"),
        //                 TaxIdType = reader.GetStringNullable("CustomId"),
        //                 NationalProviderNumber = reader.GetStringNullable("CustomId"),
        //                 MedicaidProviderNumber = reader.GetStringNullable("CustomId"),
        //                 ContactPersonFirstName = reader.GetStringNullable("CustomId"),
        //                 ContactPersonLastName = reader.GetStringNullable("CustomId"),
        //                 ContactPersonEmail = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
                         
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 ContactPersonPhone = reader.GetStringNullable("CustomId"),
        //                 MedicareInsurance = new AgencyMedicareInsurance(){
        //                     AddressCity = reader.GetStringNullable(""),
        //                 },

        //             }).AsSingle();
        //        }
            
        //}

        public AgencyMedicareInsurance FindLocationMedicareInsurance(Guid agencyId, int payor)
        {
            return database.Single<AgencyMedicareInsurance>(l => l.AgencyId == agencyId && l.MedicareId == payor);
        }

        public bool EditLocation(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
                if (existingLocation != null)
                {
                    if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                    {
                        existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                    {
                        existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    //  existingLocation.IsSubmitterInfoTheSame = location.IsSubmitterInfoTheSame;
                    existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                    if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                    {
                        existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                    {
                        existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                    if (existingLocation.IsLocationStandAlone)
                    {
                        if (location.IsAxxessTheBiller)
                        {
                            var agency = this.Get(location.AgencyId);
                            if (agency != null)
                            {

                                existingLocation.SubmitterId = agency.SubmitterId;
                                existingLocation.SubmitterName = agency.SubmitterName;
                                existingLocation.Payor = agency.Payor;
                                existingLocation.SubmitterPhone = agency.SubmitterPhone;
                                existingLocation.SubmitterFax = agency.SubmitterFax;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    existingLocation.BranchId = location.BranchId;
                    existingLocation.BranchIdOther = location.BranchIdOther;
                    existingLocation.Name = location.Name;
                    existingLocation.CustomId = location.CustomId;
                    existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                    existingLocation.AddressLine1 = location.AddressLine1;
                    existingLocation.AddressLine2 = location.AddressLine2;
                    existingLocation.AddressCity = location.AddressCity;
                    existingLocation.AddressStateCode = location.AddressStateCode;
                    existingLocation.AddressZipCode = location.AddressZipCode;
                    existingLocation.Comments = location.Comments;
                    existingLocation.Modified = DateTime.Now;
                    database.Update<AgencyLocation>(existingLocation);
                    result = true;
                }
            }
            return result;
        }

        public bool EditLocationModal(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                try
                {
                    location.Modified = DateTime.Now;
                    database.Update<AgencyLocation>(location);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool EditBranchCost(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                //existingLocation.Cost = location.Cost;
                existingLocation.Ub04Locator81cca = location.Ub04Locator81cca;
                existingLocation.Modified = DateTime.Now;

                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public IEnumerable<Agency> All()
        {
            return database.All<Agency>().AsEnumerable<Agency>();
        }

        public IList<Agency> AllAgencies()
        {
            var agencies = database.All<Agency>().ToList();
            if (agencies != null && agencies.Count > 0)
            {
                agencies.ForEach(a =>
                {
                    a.MainLocation = GetMainLocation(a.Id);
                });
            }
            return agencies.OrderBy(a => a.Name).ToList();
        }

        public bool Update(Agency agency)
        {
            bool result = false;
            if (agency != null)
            {
                agency.Modified = DateTime.Now;
                database.Update<Agency>(agency);
                result = true;
            }
            return result;
        }

        public List<AgencyLocation> AgencyLocations(Guid agencyId, List<Guid> locationIds)
        {
            var list = new List<AgencyLocation>();
            if (locationIds != null && locationIds.Count > 0)
            {
                var ids = locationIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name ,
                        IsMainOffice, 
                        IsLocationStandAlone
                            FROM
                                agencylocations 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<AgencyLocation>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencyLocation
                     {
                         Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name"),
                         IsMainOffice = reader.GetBoolean("IsMainOffice"),
                         IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone")

                     }).AsList();
                }
            }
            return list.OrderBy(l => l.IsMainOffice).ThenBy(l => l.IsLocationStandAlone).ToList();
        }



        public bool AddAgencyLocationMedicare(AgencyMedicareInsurance medicare)
        {
            if (medicare != null)
            {
                medicare.Created = DateTime.Now;
                medicare.Id = Guid.NewGuid();
                database.Add<AgencyMedicareInsurance>(medicare);
                return true;
            }
            return false;
        }

        public bool UpdateAgencyLocationMedicare(AgencyMedicareInsurance medicare)
        {
            if (medicare != null)
            {
                medicare.Modified = DateTime.Now;
                database.Update<AgencyMedicareInsurance>(medicare);
                return true;
            }
            return false;
        }


        public List<Agency> AgencyWithNameOnly(List<Guid> agencyIds)
        {
            var list = new List<Agency>();
            if (agencyIds != null && agencyIds.Count > 0)
            {
                var ids = agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                        Id as Id ,
                        Name as Name 
                            FROM
                                agencies 
                                    WHERE 
                                        Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Agency>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .SetMap(reader => new Agency
                     {
                         Id = reader.GetGuid("Id"),
                         Name = reader.GetStringNullable("Name")
                     }).AsList();
                }
            }
            return list;
        }


        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {
                
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    contact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.Id == Id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(c => c.Id == contact.Id && c.AgencyId == contact.AgencyId && c.IsDeprecated == false);
            if (contact != null)
            {
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    existingContact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingContact.PhoneExtension = contact.PhoneExtension;
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;
                existingContact.Comments = contact.Comments;
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                contact.IsDeprecated = true;
                contact.Modified = DateTime.Now;
                database.Update<AgencyContact>(contact);
                return true;
            }
            return false;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (!insurance.AgencyId.IsEmpty() && insurance.OldInsuranceId > 0)
                {
                    if (insurance.OldInsuranceId < 1000)
                    {
                        var location = GetMainLocation(insurance.AgencyId);
                        if (location != null && location.BillData.IsNotNullOrEmpty())
                        {
                            insurance.BillData = location.BillData;
                        }
                    }
                    else
                    {
                        var oldInsurance = GetInsurance(insurance.OldInsuranceId, insurance.AgencyId);
                        if (oldInsurance != null && oldInsurance.BillData.IsNotNullOrEmpty())
                        {
                            insurance.BillData = oldInsurance.BillData;
                        }
                    }
                }
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == insuranceId );
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        public IList<InsuranceLean> GetLeanInsurances(Guid agencyId)
        {
            var script = @"SELECT agencyinsurances.Id, agencyinsurances.PayorType, agencyinsurances.Name, agencyinsurances.InvoiceType, agencyinsurances.ContactPersonFirstName, agencyinsurances.ContactPersonLastName, " +
               "agencyinsurances.PhoneNumber, agencyinsurances.PayorId " +
               "FROM agencyinsurances  WHERE agencyinsurances.AgencyId = @agencyid AND agencyinsurances.IsDeprecated = 0";

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare=false
                })
                .AsList();
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id);
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I =>I.AgencyId == insurance.AgencyId && I.Id == insurance.Id );
            if (insurance != null && existingInsurance!=null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.RequireServiceLocation = insurance.RequireServiceLocation;
                existingInsurance.HasContractWithAgency = insurance.HasContractWithAgency;
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderId = insurance.ProviderId;
                existingInsurance.ProviderSubscriberId = insurance.ProviderSubscriberId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.PrintContentOnly = insurance.PrintContentOnly;
                existingInsurance.HCFALocators = insurance.HCFALocators;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.SubmitterId = insurance.SubmitterId;
                existingInsurance.BillType = insurance.BillType;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                existingInsurance.SubmitterName = insurance.SubmitterName;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(existingInsurance);
                result = true;
            }
            return result;
        }

        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id  );
            if (insurance != null)
            {
                insurance.IsDeprecated = true;
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                return true;
            }
            return false;
        }

        public bool IsMedicareHMO(Guid agencyId, int Id)
        {
            var result = false;
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id );
            if (insurance != null)
            {
                result= (insurance.PayorType == 2);
            }
            return result;
        }

        public bool EditInsuranceModal(AgencyInsurance insurance)
        {
            var result = false;
           
            if (insurance != null )
            {
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public List<InsuranceCache> GetInsurancesForBilling(Guid agencyId, int[] insuranceIds)
        {
            var list = new List<InsuranceCache>();
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND agencyinsurances.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));

                var script = string.Format(@"SELECT 
                        agencyinsurances.Id,
                        agencyinsurances.Name as Name,
                        agencyinsurances.PayorType
                                FROM agencyinsurances 
                                        WHERE agencyinsurances.AgencyId = @agencyid {0}", additionalScript);

                using (var cmd = new FluentCommand<InsuranceCache>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                      .AddGuid("agencyid", agencyId)
                      .SetMap(reader => new InsuranceCache
                      {
                          Id = reader.GetInt("Id"),
                          Name = reader.GetStringNullable("Name"),
                          PayorType = reader.GetInt("PayorType")
                      })
                      .AsList();
                }
            }
            return list;
        }

        public List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds)
        {
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND agencyinsurances.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT agencyinsurances.Id,
                        agencyinsurances.PayorType, 
                        agencyinsurances.Name,
                        agencyinsurances.InvoiceType, 
                        agencyinsurances.ContactPersonFirstName, 
                        agencyinsurances.ContactPersonLastName, 
                        agencyinsurances.PhoneNumber,
                        agencyinsurances.PayorId 
                                FROM agencyinsurances 
                                        WHERE agencyinsurances.AgencyId = @agencyid AND agencyinsurances.IsDeprecated = 0 {0}", additionalScript);

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare = false
                })
                .AsList();
        }
       

        #endregion

        #region Hospital

        public bool AddHospital(AgencyHospital hospital)
        {
            var result = false;
            if (hospital != null)
            {
               
                if (hospital.PhoneArray.Count > 0)
                {
                    hospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    hospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                hospital.Created = DateTime.Now;
                hospital.Modified = DateTime.Now;

                database.Add<AgencyHospital>(hospital);
                result = true;
            }
            return result;
        }

        public IList<AgencyHospital> GetHospitals(Guid agencyId)
        {
            return database.Find<AgencyHospital>(h => h.AgencyId == agencyId && h.IsDeprecated == false).ToList();
        }

        public AgencyHospital FindHospital(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId && h.IsDeprecated == false);
        }

        public bool EditHospital(AgencyHospital hospital)
        {
            var result = false;
            var existingHospital = database.Single<AgencyHospital>(h => h.Id == hospital.Id && h.AgencyId == hospital.AgencyId);
            if (hospital != null)
            {

                if (hospital.PhoneArray.Count > 0)
                {
                    existingHospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    existingHospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingHospital.Name = hospital.Name;
                existingHospital.ContactPersonFirstName = hospital.ContactPersonFirstName;
                existingHospital.ContactPersonLastName = hospital.ContactPersonLastName;
                existingHospital.AddressLine1 = hospital.AddressLine1;
                existingHospital.AddressLine2 = hospital.AddressLine2;
                existingHospital.AddressCity = hospital.AddressCity;
                existingHospital.AddressStateCode = hospital.AddressStateCode;
                existingHospital.AddressZipCode = hospital.AddressZipCode;
                existingHospital.EmailAddress = hospital.EmailAddress;
                existingHospital.Comment = hospital.Comment;
                existingHospital.Modified = DateTime.Now;

                database.Update<AgencyHospital>(existingHospital);
                result = true;
            }
            return result;

        }

        public bool DeleteHospital(Guid agencyId, Guid Id)
        {
            var hospital = database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
            if (hospital != null)
            {
                hospital.IsDeprecated = true;
                hospital.Modified = DateTime.Now;
                database.Update<AgencyHospital>(hospital);
                return true;
            }
            return false;
        }

        #endregion

        #region Pharmacy

        public bool AddPharmacy(AgencyPharmacy pharmacy)
        {
            var result = false;
            if (pharmacy != null)
            {

                if (pharmacy.PhoneArray.Count > 0)
                {
                    pharmacy.Phone = pharmacy.PhoneArray.ToArray().PhoneEncode();
                }
                if (pharmacy.FaxNumberArray.Count > 0)
                {
                    pharmacy.FaxNumber = pharmacy.FaxNumberArray.ToArray().PhoneEncode();
                }
                pharmacy.Created = DateTime.Now;
                pharmacy.Modified = DateTime.Now;

                database.Add<AgencyPharmacy>(pharmacy);
                result = true;
            }
            return result;
        }

        public IList<AgencyPharmacy> GetPharmacies(Guid agencyId)
        {
            return database.Find<AgencyPharmacy>(h => h.AgencyId == agencyId && h.IsDeprecated == false).ToList();
        }

        public AgencyPharmacy FindPharmacy(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyPharmacy>(h => h.Id == Id && h.AgencyId == agencyId && h.IsDeprecated == false);
        }

        public bool EditPharmacy(AgencyPharmacy pharmacy)
        {
            var result = false;
            var existingPharmacy = database.Single<AgencyPharmacy>(p => p.Id == pharmacy.Id && p.AgencyId == pharmacy.AgencyId);
            if (pharmacy != null)
            {

                if (pharmacy.PhoneArray.Count > 0)
                {
                    existingPharmacy.Phone = pharmacy.PhoneArray.ToArray().PhoneEncode();
                }
                if (pharmacy.FaxNumberArray.Count > 0)
                {
                    existingPharmacy.FaxNumber = pharmacy.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingPharmacy.Name = pharmacy.Name;
                existingPharmacy.ContactPersonFirstName = pharmacy.ContactPersonFirstName;
                existingPharmacy.ContactPersonLastName = pharmacy.ContactPersonLastName;
                existingPharmacy.AddressLine1 = pharmacy.AddressLine1;
                existingPharmacy.AddressLine2 = pharmacy.AddressLine2;
                existingPharmacy.AddressCity = pharmacy.AddressCity;
                existingPharmacy.AddressStateCode = pharmacy.AddressStateCode;
                existingPharmacy.AddressZipCode = pharmacy.AddressZipCode;
                existingPharmacy.EmailAddress = pharmacy.EmailAddress;
                existingPharmacy.Comment = pharmacy.Comment;
                existingPharmacy.Modified = DateTime.Now;

                database.Update<AgencyPharmacy>(existingPharmacy);
                result = true;
            }
            return result;

        }

        public bool DeletePharmacy(Guid agencyId, Guid Id)
        {
            var pharmacy = database.Single<AgencyPharmacy>(h => h.Id == Id && h.AgencyId == agencyId);
            if (pharmacy != null)
            {
                pharmacy.IsDeprecated = true;
                pharmacy.Modified = DateTime.Now;
                database.Update<AgencyPharmacy>(pharmacy);
                return true;
            }
            return false;
        }

        #endregion

        #region Infection Report

        public bool AddInfection(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    infection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                infection.Created = DateTime.Now;
                infection.Modified = DateTime.Now;

                database.Add<Infection>(infection);
                result = true;
            }
            return result;
        }

        public IList<Infection> GetInfections(Guid agencyId)
        {
            var sql = string.Format(@"SELECT infections.Id, infections.PatientId,CONCAT_WS(', ', patients.LastName, patients.FirstName) as 'PatientName', 
                                    infections.EpisodeId,CONCAT_WS(',',agencyphysicians.LastName, agencyphysicians.FirstName) as 'PhysicianName', infections.InfectionDate, infections.InfectionType
                                    FROM infections INNER JOIN patients ON infections.PatientId=patients.Id
                                    INNER JOIN agencyphysicians ON infections.PhysicianId=agencyphysicians.Id
                                    WHERE infections.AgencyId=@agencyid 
                                    AND infections.IsDeprecated=0");
            return new FluentCommand<Infection>(sql)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Infection
                {
                    Id = reader.GetGuid("Id"),
                    PatientName = reader.GetStringNullable("PatientName"),
                    PhysicianName = reader.GetStringNullable("PhysicianName"),
                    InfectionDate = reader.GetDateTimeWithMin("InfectionDate"),
                    PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                    EpisodeId = reader.GetGuidIncludeEmpty("EpisodeId"),
                    InfectionType = reader.GetStringNullable("InfectionType")
                })
                .AsList();
        }

        public List<Infection> GetInfectionsByDateRange(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var sql=string.Format(@"SELECT infections.Id, infections.PatientId,CONCAT_WS(', ', patients.LastName, patients.FirstName) as 'PatientName', 
                                    infections.EpisodeId,CONCAT_WS(',',agencyphysicians.LastName, agencyphysicians.FirstName) as 'PhysicianName', infections.InfectionDate, infections.InfectionType
                                    FROM infections INNER JOIN patients ON infections.PatientId=patients.Id 
                                    INNER JOIN agencyphysicians ON infections.PhysicianId=agencyphysicians.Id 
                                    WHERE infections.AgencyId=@agencyid 
                                    AND Date(infections.InfectionDate) >= Date(@startdate) 
                                    AND Date(infections.InfectionDate) <= Date(@enddate) 
                                    AND infections.IsDeprecated=0");
            return new FluentCommand<Infection>(sql)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate",startDate)
                .AddDateTime("enddate",endDate)
                .SetMap(reader => new Infection
                {
                    Id=reader.GetGuid("Id"),
                    PatientName=reader.GetStringNullable("PatientName"),
                    PhysicianName=reader.GetStringNullable("PhysicianName"),
                    InfectionDate=reader.GetDateTimeWithMin("InfectionDate"),
                    PatientId=reader.GetGuidIncludeEmpty("PatientId"),
                    EpisodeId=reader.GetGuidIncludeEmpty("EpisodeId"),
                    InfectionType=reader.GetStringNullable("InfectionType")
                })
                .AsList();

        }

        public bool DeleteInfection(Guid Id)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = true;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }
        public bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = isDeprecated;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                try
                {
                    infection.UserId = employeeId;
                    infection.Modified = DateTime.Now;
                    database.Update<Infection>(infection);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Infection GetInfectionReport(Guid agencyId, Guid infectionId)
        {
            return database.Single<Infection>(i => i.AgencyId == agencyId && i.Id == infectionId);
        }

        public bool UpdateInfection(Infection infection)
        {
            var result = false;
            var existingInfection = GetInfectionReport(infection.AgencyId, infection.Id);
            if (existingInfection != null)
            {
                existingInfection.Orders = infection.Orders;
                existingInfection.Treatment = infection.Treatment;
                existingInfection.MDNotified = infection.MDNotified;
                existingInfection.NewOrdersCreated = infection.NewOrdersCreated;
                existingInfection.TreatmentPrescribed = infection.TreatmentPrescribed;

                existingInfection.PhysicianId = infection.PhysicianId;
                existingInfection.InfectionDate = infection.InfectionDate;
                existingInfection.Status = infection.Status;
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    existingInfection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                existingInfection.InfectionTypeOther = infection.InfectionTypeOther;
                existingInfection.FollowUp = infection.FollowUp;
                existingInfection.SignatureDate = infection.SignatureDate;
                existingInfection.SignatureText = infection.SignatureText;
                existingInfection.Modified = DateTime.Now;

                database.Update<Infection>(existingInfection);
                result = true;
            }
            return result;
        }

        public bool UpdateInfectionModal(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        #endregion

        #region Incident Report

        public bool AddIncident(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    incident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                incident.Created = DateTime.Now;
                incident.Modified = DateTime.Now;

                database.Add<Incident>(incident);
                result = true;
            }
            return result;
        }

        public IList<Incident> GetIncidents(Guid agencyId)
        {
            var sql = string.Format(@"SELECT incidents.Id, incidents.PatientId,CONCAT_WS(', ', patients.LastName, patients.FirstName) as 'PatientName', 
                                    incidents.EpisodeId,CONCAT_WS(',',agencyphysicians.LastName, agencyphysicians.FirstName) as 'PhysicianName', incidents.IncidentDate, incidents.IncidentType
                                    FROM incidents INNER JOIN patients ON incidents.PatientId=patients.Id
                                    INNER JOIN agencyphysicians ON incidents.PhysicianId=agencyphysicians.Id
                                    WHERE incidents.AgencyId=@agencyid 
                                    AND incidents.IsDeprecated = 0");
            return new FluentCommand<Incident>(sql)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Incident
                {
                    Id = reader.GetGuid("Id"),
                    PatientName = reader.GetStringNullable("PatientName"),
                    PhysicianName = reader.GetStringNullable("PhysicianName"),
                    IncidentDate = reader.GetDateTimeWithMin("IncidentDate"),
                    PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                    EpisodeId = reader.GetGuidIncludeEmpty("EpisodeId"),
                    IncidentType = reader.GetStringNullable("IncidentType")
                })
                .AsList();
        }

        public bool DeleteIncident(Guid Id)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = true;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = isDeprecated;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                try
                {
                    incident.UserId = employeeId;
                    incident.Modified = DateTime.Now;
                    database.Update<Incident>(incident);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Incident GetIncidentReport(Guid agencyId, Guid incidentId)
        {
            return database.Single<Incident>(i => i.AgencyId == agencyId && i.Id == incidentId);
        }

        public bool UpdateIncident(Incident incident)
        {
            var result = false;
            var existingIncident = GetIncidentReport(incident.AgencyId, incident.Id);
            if (existingIncident != null)
            {
                existingIncident.Orders = incident.Orders;
                existingIncident.Description = incident.Description;
                existingIncident.ActionTaken = incident.ActionTaken;
                existingIncident.IncidentType = incident.IncidentType;
                existingIncident.MDNotified = incident.MDNotified;
                existingIncident.NewOrdersCreated = incident.NewOrdersCreated;
                existingIncident.FamilyNotified = incident.FamilyNotified;

                existingIncident.PhysicianId = incident.PhysicianId;
                existingIncident.IncidentDate = incident.IncidentDate;
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.Status = incident.Status;

                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    existingIncident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.FollowUp = incident.FollowUp;
                existingIncident.SignatureDate = incident.SignatureDate;
                existingIncident.SignatureText = incident.SignatureText;
                existingIncident.Modified = DateTime.Now;

                database.Update<Incident>(existingIncident);
                result = true;
            }
            return result;
        }

        public bool UpdateIncidentModal(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        #endregion

        #region Non Visit Task methods

        public bool AddNonVisitTask(AgencyNonVisit agencyNonVisit) 
        {
            var result = default(bool);
            if (agencyNonVisit != null) 
            {
                agencyNonVisit.Created = DateTime.Now;
                agencyNonVisit.Modified = DateTime.Now;
                database.Add<AgencyNonVisit>(agencyNonVisit);
                result = true; 
            }
            return result;
        }

        public IList<AgencyNonVisit> GetNonVisitTasks(Guid agencyId)
        {
            IList<AgencyNonVisit> result = null;
            var cacheKey = string.Format("NonVisitTasks_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out result))
            {
                result = database.Find<AgencyNonVisit>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
                Cacher.Set(cacheKey, result);
            }
            return result;
            //return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }

        public AgencyNonVisit GetNonVisitTask(Guid agencyId, Guid id)
        {
            AgencyNonVisit result = null;
            result = database.Single<AgencyNonVisit>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
            if (result == null) 
            {
                result = GetNonVisitTasks(agencyId).SingleOrDefault(c => c.Id == id && c.IsDeprecated == false);
            }
            if (result != null)
            {
                return result;
            }
            else 
            {
                result = new AgencyNonVisit();
                return result;
            }
            //return database.Single<AgencyTemplate>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
            return GetNonVisitTasks(agencyId).SingleOrDefault(c => c.Id == id && c.IsDeprecated == false);
        }

        public bool UpdateNonVisitTask(AgencyNonVisit nonVisitTask)
        {
            var result = false;
            if (nonVisitTask != null)
            {
                nonVisitTask.Modified = DateTime.Now;
                database.Update<AgencyNonVisit>(nonVisitTask);
                result = true;
            }
            return result;
        }

        public bool DeleteNonVisitTask(Guid agencyId, Guid id)
        {
            var nonVisitTask = database.Single<AgencyNonVisit>(c => c.AgencyId == agencyId && c.Id == id);
            if (nonVisitTask != null)
            {
                nonVisitTask.IsDeprecated = true;
                nonVisitTask.Modified = DateTime.Now;
                database.Update<AgencyNonVisit>(nonVisitTask);
                return true;
            }
            return false;
        }
        #endregion

        #region Non-Visit Task Manager Methods
        public List<UserNonVisitTask> GetUserNonVisitTasksByUserStatusAndDateRange(Guid agencyId,Guid UserId, DateTime startDate, DateTime endDate, bool Status)
        {
            var result = new List<UserNonVisitTask>();
            string startDateFilter = string.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            string endDateFilter = string.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            var script = string.Format
                (
                    "SELECT agencymanagement.usernonvisittasks.Id as Id, agencymanagement.usernonvisittasks.AgencyId as AgencyId, agencymanagement.usernonvisittasks.TaskId as TaskId, agencymanagement.usernonvisittasks.UserId as UserId, agencymanagement.usernonvisittasks.TaskDate as TaskDate, agencymanagement.usernonvisittasks.PaidStatus as PaidStatus, agencymanagement.usernonvisittasks.PaidDate as PaidDate, agencymanagement.usernonvisittasks.TimeIn as TimeIn, agencymanagement.usernonvisittasks.TimeOut as TimeOut, agencymanagement.usernonvisittasks.Comments as Comments, agencymanagement.usernonvisittasks.IsDeprecated as IsDeprecated " +
                    "FROM agencymanagement.usernonvisittasks " +
                    "WHERE agencymanagement.usernonvisittasks.AgencyId = '{0}' " +
                    "AND agencymanagement.usernonvisittasks.TaskDate BETWEEN DATE('{1}') AND DATE('{2}') " +
                    "AND agencymanagement.usernonvisittasks.PaidStatus = {3} " +
                    "AND agencymanagement.usernonvisittasks.UserId = '{4}' " +
                    "AND agencymanagement.usernonvisittasks.IsDeprecated = false "
                    , agencyId, startDateFilter, endDateFilter, Status, UserId
                );

            using (var cmd = new FluentCommand<UserNonVisitTask>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new UserNonVisitTask
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    TaskId = reader.GetGuid("TaskId"),
                    UserId = reader.GetGuid("UserId"),
                    TaskDate = reader.GetDateTimeWithMin("TaskDate"),
                    PaidStatus = reader.GetBoolean("PaidStatus"),
                    PaidDate = reader.GetDateTimeWithMin("PaidDate"),
                    TimeIn = reader.GetDateTimeWithMin("TimeIn"),
                    TimeOut = reader.GetDateTimeWithMin("TimeOut"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated")
                })
                .AsList();
            }
            return result;
        }

        public List<UserNonVisitTask> GetUserNonVisitTasksByUserAndDateRange(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate)
        {
            var result = new List<UserNonVisitTask>();
            string startDateFilter = string.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            string endDateFilter = string.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            var script = string.Format
                (
                    "SELECT agencymanagement.usernonvisittasks.Id as Id, agencymanagement.usernonvisittasks.AgencyId as AgencyId, agencymanagement.usernonvisittasks.TaskId as TaskId, agencymanagement.usernonvisittasks.UserId as UserId, agencymanagement.usernonvisittasks.TaskDate as TaskDate, agencymanagement.usernonvisittasks.PaidStatus as PaidStatus, agencymanagement.usernonvisittasks.PaidDate as PaidDate, agencymanagement.usernonvisittasks.TimeIn as TimeIn, agencymanagement.usernonvisittasks.TimeOut as TimeOut, agencymanagement.usernonvisittasks.Comments as Comments, agencymanagement.usernonvisittasks.IsDeprecated as IsDeprecated " +
                    "FROM agencymanagement.usernonvisittasks " +
                    "WHERE agencymanagement.usernonvisittasks.AgencyId = '{0}' " +
                    "AND agencymanagement.usernonvisittasks.TaskDate BETWEEN DATE('{1}') AND DATE('{2}') " +
                    "AND agencymanagement.usernonvisittasks.UserId = '{3}' " +
                    "AND agencymanagement.usernonvisittasks.IsDeprecated = false "
                    , agencyId, startDateFilter, endDateFilter, userId
                );

            using (var cmd = new FluentCommand<UserNonVisitTask>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new UserNonVisitTask
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    TaskId = reader.GetGuid("TaskId"),
                    UserId = reader.GetGuid("UserId"),
                    TaskDate = reader.GetDateTimeWithMin("TaskDate"),
                    PaidStatus = reader.GetBoolean("PaidStatus"),
                    PaidDate = reader.GetDateTimeWithMin("PaidDate"),
                    TimeIn = reader.GetDateTimeWithMin("TimeIn"),
                    TimeOut = reader.GetDateTimeWithMin("TimeOut"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated")
                })
                .AsList();
            }
            return result;
        }

        public List<UserNonVisitTask> GetUserNonVisitTasksByStatusAndDateRange(Guid agencyId, DateTime startDate, DateTime endDate, bool Status)
        {
            var result = new List<UserNonVisitTask>();
            string startDateFilter = string.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            string endDateFilter = string.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);            

            var script = string.Format
                (
                    "SELECT agencymanagement.usernonvisittasks.Id as Id, agencymanagement.usernonvisittasks.AgencyId as AgencyId, agencymanagement.usernonvisittasks.TaskId as TaskId, agencymanagement.usernonvisittasks.UserId as UserId, agencymanagement.usernonvisittasks.TaskDate as TaskDate, agencymanagement.usernonvisittasks.PaidStatus as PaidStatus, agencymanagement.usernonvisittasks.PaidDate as PaidDate, agencymanagement.usernonvisittasks.TimeIn as TimeIn, agencymanagement.usernonvisittasks.TimeOut as TimeOut, agencymanagement.usernonvisittasks.Comments as Comments, agencymanagement.usernonvisittasks.IsDeprecated as IsDeprecated " +
                    "FROM agencymanagement.usernonvisittasks " +
                    "WHERE agencymanagement.usernonvisittasks.AgencyId = '{0}' " +
                    "AND agencymanagement.usernonvisittasks.TaskDate BETWEEN DATE('{1}') AND DATE('{2}') " +
                    "AND agencymanagement.usernonvisittasks.PaidStatus = {3} " +
                    "AND agencymanagement.usernonvisittasks.IsDeprecated = false "
                    ,agencyId,startDateFilter,endDateFilter,Status
                );

            using (var cmd = new FluentCommand<UserNonVisitTask>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new UserNonVisitTask
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    TaskId = reader.GetGuid("TaskId"),
                    UserId = reader.GetGuid("UserId"),
                    TaskDate = reader.GetDateTimeWithMin("TaskDate"),
                    PaidStatus = reader.GetBoolean("PaidStatus"),
                    PaidDate = reader.GetDateTimeWithMin("PaidDate"),
                    TimeIn = reader.GetDateTimeWithMin("TimeIn"),
                    TimeOut = reader.GetDateTimeWithMin("TimeOut"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated")
                })
                .AsList();
            }
            return result;
        }

        public List<UserNonVisitTask> GetUserNonVisitTasksByDateRange(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var result = new List<UserNonVisitTask>();
            string startDateFilter = string.Format("{0}-{1}-{2}", startDate.Year, startDate.Month, startDate.Day);
            string endDateFilter = string.Format("{0}-{1}-{2}", endDate.Year, endDate.Month, endDate.Day);

            var script = string.Format
                (
                    "SELECT agencymanagement.usernonvisittasks.Id as Id, agencymanagement.usernonvisittasks.AgencyId as AgencyId, agencymanagement.usernonvisittasks.TaskId as TaskId, agencymanagement.usernonvisittasks.UserId as UserId, agencymanagement.usernonvisittasks.TaskDate as TaskDate, agencymanagement.usernonvisittasks.PaidStatus as PaidStatus, agencymanagement.usernonvisittasks.PaidDate as PaidDate, agencymanagement.usernonvisittasks.TimeIn as TimeIn, agencymanagement.usernonvisittasks.TimeOut as TimeOut, agencymanagement.usernonvisittasks.Comments as Comments, agencymanagement.usernonvisittasks.IsDeprecated as IsDeprecated " +
                    "FROM agencymanagement.usernonvisittasks " +
                    "WHERE agencymanagement.usernonvisittasks.AgencyId = '{0}' " +
                    "AND agencymanagement.usernonvisittasks.TaskDate BETWEEN DATE('{1}') AND DATE('{2}') " +
                    "AND agencymanagement.usernonvisittasks.IsDeprecated = false "
                    , agencyId, startDateFilter, endDateFilter
                );

            using (var cmd = new FluentCommand<UserNonVisitTask>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new UserNonVisitTask
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    TaskId = reader.GetGuid("TaskId"),
                    UserId = reader.GetGuid("UserId"),
                    TaskDate = reader.GetDateTimeWithMin("TaskDate"),
                    PaidStatus = reader.GetBoolean("PaidStatus"),
                    PaidDate = reader.GetDateTimeWithMin("PaidDate"),
                    TimeIn = reader.GetDateTimeWithMin("TimeIn"),
                    TimeOut = reader.GetDateTimeWithMin("TimeOut"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated")
                })
                .AsList();
            }
            return result;
        }

        public IList<UserNonVisitTask> GetUserNonVisitTasksByUser(Guid agencyId, Guid UserId)
        {
            IList<UserNonVisitTask> result = null;
            result = database.Find<UserNonVisitTask>(c => c.UserId == UserId && c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
            return result;
        }

        public UserNonVisitTask GetUserNonVisitTask(Guid agencyId, Guid id)
        {
            UserNonVisitTask result = null;
            result = database.Single<UserNonVisitTask>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
            return result;
        }

        public bool UpdateUserNonVisitTask(UserNonVisitTask userNonVisitTask)
        {
            var result = false;
            if (userNonVisitTask != null)
            {
                database.Update<UserNonVisitTask>(userNonVisitTask);
                result = true;
            }
            return result;
        }

        public IList<UserNonVisitTask> GetUserNonVisitTasks(Guid agencyId)
        {
            IList<UserNonVisitTask> result = null;
            var cacheKey = string.Format("UserNonVisitTasks_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out result))
            {
                result = database.Find<UserNonVisitTask>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderByDescending(c => c.TaskDate).ToList();
                Cacher.Set(cacheKey, result);
            }
            return result;
            //return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }

        public bool AddUserNonVisitTask(UserNonVisitTask userNonVisitTask)
        {
            var result = default(bool);
            if (userNonVisitTask != null)
            {
                database.Add<UserNonVisitTask>(userNonVisitTask);
                result = true;
            }
            return result;
        }

        public bool DeleteUserNonVisitTask(Guid agencyId, Guid id)
        {
            var userNonVisitTask = database.Single<UserNonVisitTask>(c => c.AgencyId == agencyId && c.Id == id);
            if (userNonVisitTask != null)
            {
                userNonVisitTask.IsDeprecated = true;
                database.Update<UserNonVisitTask>(userNonVisitTask);
                return true;
            }
            return false;
        }
        #endregion

        #region Template Methods

        public bool AddTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
               
                template.Created = DateTime.Now;
                template.Modified = DateTime.Now;

                database.Add<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public IList<AgencyTemplate> GetTemplates(Guid agencyId)
        {
            IList<AgencyTemplate> templates = null;
            var cacheKey = string.Format("Supplies_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out templates))
            {
                templates = database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
                Cacher.Set(cacheKey, templates);
            }
            return templates;
            //return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }


        public AgencyTemplate GetTemplate(Guid agencyId, Guid id)
        {
            //return database.Single<AgencyTemplate>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
            return GetTemplates(agencyId).SingleOrDefault(c => c.Id == id && c.IsDeprecated == false);
        }

        public bool UpdateTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public bool DeleteTemplate(Guid agencyId, Guid id)
        {
            var template = database.Single<AgencyTemplate>(c => c.AgencyId == agencyId && c.Id == id );
            if (template != null)
            {
                template.IsDeprecated = true;
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                return true;
            }
            return false;
        }

        public void InsertTemplates(Guid agencyId)
        {
            var templateSQL = MessageBuilder.ReadTextFrom("Templates");

            if (templateSQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(templateSQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Billing

        public AxxessSubmitterInfo SubmitterInfo(int payerId)
        {
            return database.Single<AxxessSubmitterInfo>(s => s.Id == payerId);
        }

        #endregion

        #region Supply Methods

        public bool AddSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.IsDeprecated = false;
                supply.Created = DateTime.Now;
                supply.Modified = DateTime.Now;

                database.Add<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId)
        {
            return database.Find<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Description).ToList();
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit)
        {
            var supplies = new List<AgencySupply>();
            var suppliesSQL = string.Format(@"SELECT 
                            `Id`, 
                            `Code`, 
                            `UnitCost`,
                            `RevenueCode`, 
                            `Description`, 
                            `IsDeprecated`
                                FROM
                                     `agencysupplies`
                                            WHERE 
                                                `AgencyId` = @agencyid AND
                                                `IsDeprecated` = 0 AND
                                                `Description` LIKE '%{0}%' 
                                                 LIMIT 0, {1};", searchTerm, searchLimit);

            using (var cmd = new FluentCommand<AgencySupply>(suppliesSQL))
            {
                supplies = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencySupply
                    {
                        Id = reader.GetGuid("Id"),
                        Code = reader.GetStringNullable("Code"),
                        UnitCost = reader.GetDouble("UnitCost"),
                        RevenueCode = reader.GetStringNullable("RevenueCode"),
                        Description = reader.GetStringNullable("Description"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated")

                    })
                    .AsList();
            }
            if (supplies.IsNotNullOrEmpty())
            {
                supplies = supplies.OrderBy(s => s.Description).ToList();
            }
            return supplies;
        }


        public AgencySupply GetSupply(Guid agencyId, Guid id)
        {
            return database.Single<AgencySupply>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public IList<AgencySupply> GetSuppliesByIds(Guid agencyId, List<Guid> supplyIds)
        {
            var list = new List<AgencySupply>();
            if (supplyIds != null && supplyIds.Count > 0)
            {
                var ids = supplyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                string script = string.Format(@"SELECT Id, RevenueCode, UnitCost, Code
                                                FROM 
                                                    agencysupplies
                                                        WHERE 
                                                            AgencyId = @agencyid AND
                                                            Id IN ( {0} )", ids);

                using (var cmd = new FluentCommand<AgencySupply>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new AgencySupply
                     {
                         Id = reader.GetGuid("Id"),
                         RevenueCode = reader.GetStringNullable("RevenueCode"),
                         UnitCost = reader.GetDouble("UnitCost"),
                         Code = reader.GetStringNullable("Code")
                     }).AsList();
                }
            }
            return list;
        }

        public bool UpdateSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public bool DeleteSupply(Guid agencyId, Guid id)
        {
            var supply = database.Single<AgencySupply>(c => c.AgencyId == agencyId && c.Id == id);
            if (supply != null)
            {
                supply.IsDeprecated = true;
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                return true;
            }
            return false;
        }

        public void InsertSupplies(Guid agencyId)
        {
            var supplySQL = MessageBuilder.ReadTextFrom("Supplies");

            if (supplySQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(supplySQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        #endregion

        #region Reports

        public bool AddReport(Report report)
        {
            bool result = false;
            if (report != null)
            {
                report.Id = Guid.NewGuid();
                report.Created = DateTime.Now;
                database.Add<Report>(report);
                result = true;
            }

            return result;
        }

        public List<ReportDescription> GetDescriptions(bool IsProduction)
        {
            if (IsProduction)
            {

                return database.Find<ReportDescription>(r => r.IsProductionReady == true).ToList();

            }
            else
            return database.All<ReportDescription>().ToList();
        }


        public Report GetReport(Guid agencyId, Guid reportId)
        {
            return database.Single<Report>(r => r.Id == reportId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateReport(Report report)
        {
            var result = false;
            if (report != null)
            {
                database.Update<Report>(report);
                result = true;
            }
            return result;
        }

        public bool RemoveReport(Guid reportId)
        {
            var result = false;
            try
            {
                return database.Delete<Report>(reportId) > 0;
            }
            catch
            {
                return false;
            }
            return result;
        }


        public IList<ReportLite> GetReports(Guid agencyId)
        {
            var reports = new List<ReportLite>();
            var reportSQL = @"SELECT `Id`, `UserId`, `AssetId`, `Type`, `Format`, `Status`, `IsDeprecated`, `Created`, `Completed` FROM `agencymanagement`.`reports` " +
                "WHERE `AgencyId` = @agencyid AND `IsDeprecated` = 0 ORDER BY `Created` DESC LIMIT 0, 100;";

            using (var cmd = new FluentCommand<ReportLite>(reportSQL))
            {
                reports = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ReportLite {
                        Id = reader.GetGuid("Id"),
                        UserId = reader.GetGuid("UserId"),
                        AssetId = reader.GetGuid("AssetId"),
                        Name = reader.GetString("Type"),
                        Format = reader.GetString("Format"),
                        Status = reader.GetString("Status"),
                        Created = reader.GetDateTime("Created") != DateTime.MinValue ? reader.GetDateTime("Created").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        Completed = reader.GetDateTime("Completed") != DateTime.MinValue ? reader.GetDateTime("Completed").ToString("MM/dd/yyyy hh:mm:ss tt") : ""
                        
                    })
                    .AsList();
            }
            

            return reports;
        }

        public IList<ReportLite> GetReportsByUserId(Guid agencyId, Guid userId)
        {
            var reports = new List<ReportLite>();

            var reportSQL = string.Format
                (
                    "SELECT reports.`Id` AS ReportId, reports.`UserId` AS ReportUserId, reports.`AssetId` AS ReportAssetId, reports.`Type` AS ReportType, reports.`Format` AS ReportFormat, reports.`Status` AS ReportStatus, reports.`IsDeprecated` AS ReportIsDeprecated, reports.`Created` AS ReportCreated, reports.`Completed` AS ReportCompleted " +
                    "FROM `agencymanagement`.`reports` " +
                    "WHERE reports.`AgencyId`  = '{0}' " +
                    "AND reports.`UserId` = '{1}' " +
                    "AND reports.`IsDeprecated` = '0' " +
                    "ORDER BY reports.`Created` DESC " +
                    "LIMIT 0, 100; "
                    , agencyId, userId
                );

            using (var cmd = new FluentCommand<ReportLite>(reportSQL))
            {
                reports = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ReportLite
                    {
                        Id = reader.GetGuid("ReportId"),
                        UserId = reader.GetGuid("ReportUserId"),
                        AssetId = reader.GetGuid("ReportAssetId"),
                        Name = reader.GetString("ReportType"),
                        Format = reader.GetString("ReportFormat"),
                        Status = reader.GetString("ReportStatus"),
                        Created = reader.GetDateTime("ReportCreated") != DateTime.MinValue ? reader.GetDateTime("ReportCreated").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        Completed = reader.GetDateTime("ReportCompleted") != DateTime.MinValue ? reader.GetDateTime("ReportCompleted").ToString("MM/dd/yyyy hh:mm:ss tt") : ""
                    })
                    .AsList();
            }
            return reports;
        }

        #endregion

        #region CustomerNotes

        public bool AddCustomerNote(CustomerNote note)
        {
            bool result = false;
            if (note != null)
            {
                note.Id = Guid.NewGuid();
                note.Created = DateTime.Now;
                note.Modified = DateTime.Now;
                note.IsDeprecated = false;
                database.Add<CustomerNote>(note);
                result = true;
            }

            return result;
        }

        public CustomerNote GetCustomerNote(Guid agencyId, Guid noteId)
        {
            return database.Single<CustomerNote>(r => r.Id == noteId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public bool UpdateCustomerNote(CustomerNote note)
        {
            var result = false;
            if (note != null)
            {
                database.Update<CustomerNote>(note);
                result = true;
            }
            return result;
        }

        public IList<CustomerNote> GetCustomerNotes(Guid agencyId)
        {
            return database.Find<CustomerNote>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderByDescending(c => c.Created).ToList();
        }

        #endregion

        #region Medicare Eligibility Report

        public List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            List<MedicareEligibilitySummary> list = new List<MedicareEligibilitySummary>();
            var script = @"SELECT Id, AgencyId, Created " +
                "FROM medicareeligibilitysummaries me " +
                "WHERE me.Created BETWEEN @startdate AND @enddate AND me.AgencyId = @agencyid AND me.IsDeprecated = 0";

            using (var cmd = new FluentCommand<MedicareEligibilitySummary>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new MedicareEligibilitySummary
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Created = reader.GetDateTime("Created"),
                })
                .AsList();
            }
            return list;
        }

        public MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid agencyId, Guid reportId)
        {
            MedicareEligibilitySummary report = new MedicareEligibilitySummary();
            var script = @"SELECT * " +
                "FROM medicareeligibilitysummaries me " +
                "WHERE me.AgencyId = @agencyid  " +
                "AND me.Id = @id AND me.IsDeprecated = 0";

            using (var cmd = new FluentCommand<MedicareEligibilitySummary>(script))
            {
                report = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("id", reportId)
                .SetMap(reader => new MedicareEligibilitySummary
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Report = reader.GetStringNullable("Report"),
                    Created = reader.GetDateTime("Created"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                }).AsSingle();
            }
            if (report != null)
            {
                var agency = AgencyEngine.Get(agencyId);
                if (agency != null)
                {
                    report.AgencyName = agency.Name;
                }
                report.Data = report.Report.IsNotNullOrEmpty() ? report.Report.ToObject<MedicareEligibilitySummaryData>() : new MedicareEligibilitySummaryData();
                if (report.Data == null)
                {
                    report.Data = new MedicareEligibilitySummaryData();
                }
            }
            return report;
        }

        #endregion

        #region Adjustment Codes

        public bool AddAdjustmentCode(AgencyAdjustmentCode code)
        {
            bool result = false;
            if (code != null)
            {
                code.Created = DateTime.Now;
                code.Modified = DateTime.Now;
                if (code.Id.IsEmpty())
                {
                    code.Id = Guid.NewGuid();
                }
                database.Add<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        public IList<AgencyAdjustmentCode> GetAdjustmentCodes(Guid agencyId)
        {
            IList<AgencyAdjustmentCode> codes = null;
            var cacheKey = string.Format("AdjustmentCodes_{0}", agencyId);
            if (!Cacher.TryGet(cacheKey, out codes))
            {
                codes = database.Find<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Code).ToList();
                Cacher.Set(cacheKey, codes);
            }
            return codes;
        }

        public AgencyAdjustmentCode FindAdjustmentCode(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.Id == Id);
        }

        public bool UpdateAdjustmentCode(AgencyAdjustmentCode code)
        {
            bool result = false;
            if (code != null)
            {
                code.Modified = DateTime.Now;
                database.Update<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        public bool DeleteAdjustmentCode(Guid agencyId, Guid id)
        {
            bool result = false;
            var code = database.Single<AgencyAdjustmentCode>(c => c.AgencyId == agencyId && c.Id == id);
            if (code != null)
            {
                code.IsDeprecated = true;
                code.Modified = DateTime.Now;
                database.Update<AgencyAdjustmentCode>(code);
                result = true;
            }
            return result;
        }

        #endregion

        #region Upload Type

        public bool AddUploadType(UploadType uploadType)
        {
            bool result = false;
            if (uploadType != null)
            {
                uploadType.Created = DateTime.Now;
                uploadType.Modified = DateTime.Now;
                uploadType.IsDeprecated = false;

                if (uploadType.Id.IsEmpty())
                {
                    uploadType.Id = Guid.NewGuid();
                }
                database.Add<UploadType>(uploadType);
                result = true;
            }
            return result;
        }

        public IList<UploadType> GetUploadType(Guid agencyId)
        {
            IList<UploadType> uploadTypes = null;
            var chacheKey = string.Format("UploadType_{0}", agencyId);
            if (!Cacher.TryGet(chacheKey, out uploadTypes))
            {
                uploadTypes = database.Find<UploadType>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Type).ToList();
                Cacher.Set(chacheKey, uploadTypes);
            }
            return uploadTypes;
        }

        public UploadType FindUploadType(Guid agencyId, Guid Id)
        {
            return database.Single<UploadType>(c => c.AgencyId == agencyId && c.Id == Id);
        }

        public bool UpdateUploadType(UploadType uploadType)
        {
            bool result = false;
            if (uploadType != null)
            {
                uploadType.Modified = DateTime.Now;
                database.Update<UploadType>(uploadType);
                result = true;
            }
            return result;
        }

        public bool DeleteUploadType(Guid agencyId, Guid id)
        {
            bool result = false;
            var uploadType = database.Single<UploadType>(c => c.AgencyId == agencyId && c.Id == id);
            if (uploadType != null)
            {
                uploadType.IsDeprecated = true;
                uploadType.Modified = DateTime.Now;
                database.Update<UploadType>(uploadType);
                result = true;
            }
            return result;
        }

        #endregion

        #region Teams
        public IList<AgencyTeam> GetTeams(Guid agencyId)
        {
            IList<AgencyTeam> teams = null;
            teams = database.Find<AgencyTeam>(t => t.AgencyId == agencyId && t.IsDeprecated == false).OrderBy(t => t.Name).ToList();
            return teams;
        }

        public IList<AgencyTeam> GetTeamsWithUserToolTip(Guid agencyId)
        {
            IList<AgencyTeam> teams = null;
            teams = database.Find<AgencyTeam>(t => t.AgencyId == agencyId && t.IsDeprecated == false).OrderBy(t => t.Name).ToList();
            foreach (var team in teams)
            {
                var userIds = team.Users.IsNotNullOrEmpty() ? team.Users.ToObject<List<Guid>>() : new List<Guid>();
                team.UserToolTip = string.Empty;
                foreach (var userId in userIds)
                {
                    team.UserToolTip += UserEngine.GetName(userId, agencyId) + "<br/>";
                }
            }
            return teams;
        }

        public AgencyTeam GetTeam(Guid agencyid, Guid id)
        {
            return database.Single<AgencyTeam>(t => t.AgencyId == agencyid && t.Id == id && t.IsDeprecated == false);
        }

        public bool AddTeam(AgencyTeam team)
        {
            if (team != null)
            {
                database.Add<AgencyTeam>(team);
            }
            return true;
        }

        public bool UpdateTeam(AgencyTeam team)
        {
            bool result = false;
            if (team != null)
            {
                database.Update<AgencyTeam>(team);
                result = true;
            }
            return result;
        }

        public bool DeleteTeam(Guid agencyId, Guid id)
        {
            bool result = false;
            var team = GetTeam(agencyId, id);
            if (team != null)
            {
                team.IsDeprecated = true;
                database.Update<AgencyTeam>(team);
                result = true;
            }
            return result;
        }

        public MultiGrid GetTeamAccessUserData(Guid agencyId, Guid teamId, List<User> agencyUsers)
        {
            var grid = new MultiGrid() { Left = new List<SelectableItem>(), Right = new List<SelectableItem>() };
            var team = GetTeam(agencyId, teamId);
            var teamUsers = team.Users.IsNotNullOrEmpty() ? team.Users.ToObject<List<Guid>>() : new List<Guid>();
            foreach (var user in agencyUsers)
            {
                if (teamUsers.Contains(user.Id))
                {
                    grid.Left.Add(new SelectableItem
                    {
                        Id = user.Id,
                        DisplayName = user.LastName + ", " + user.FirstName
                    });
                }
                else
                {
                    grid.Right.Add(new SelectableItem
                    {
                        Id = user.Id,
                        DisplayName = user.LastName + ", " + user.FirstName
                    });
                }
            }
            return grid;
        }

        public IList<SelectedUser> GetTeamUsers(Guid agencyId, Guid teamId, List<User> agencyUsers)
        {
            var users = GetTeam(agencyId, teamId).Users;
            var selectedUsers = users.IsNotNullOrEmpty() ? users.ToObject<List<Guid>>() : new List<Guid>();
            var selectedList = new List<SelectedUser>();
            foreach (var user in agencyUsers)
            {
                var selectedUser = new SelectedUser
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Credentials = user.Credentials,
                    CredentialsOther = user.CredentialsOther,
                    Suffix = user.Suffix,
                    UserId = user.Id,
                    Roles = user.Roles
                };
                if (selectedUsers.Contains(user.Id))
                {
                    selectedList.Add(selectedUser);
                }
            }
            //limit list to clinicians
            var clinician = new List<SelectedUser>();
            foreach (var user in selectedList)
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    clinician.Add(user);
                }
            }
            clinician = clinician.OrderBy(a => a.DisplayName).ToList();
            return clinician;
        }

        public IList<SelectedUser> GetUserNotInTeam(Guid agencyId, Guid agencyTeamId, List<User> agencyUsers)
        {
            var list = new List<SelectedUser>();
            var agencyTeam = GetTeam(agencyId, agencyTeamId);
            string users = "";
            if (agencyTeam != null)
            {
                users = agencyTeam.Users;
            }
            var selectedUsers = users.IsNotNullOrEmpty() ? users.ToObject<List<Guid>>() : new List<Guid>();
            foreach (var user in agencyUsers)
            {
                var selectedUser = new SelectedUser
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Credentials = user.Credentials,
                    CredentialsOther = user.CredentialsOther,
                    Suffix = user.Suffix,
                    UserId = user.Id,
                    Roles = user.Roles
                };
                if (!selectedUsers.Contains(user.Id))
                {
                    list.Add(selectedUser);
                }
            }

            //limit list to clinicians
            var clinicians = new List<SelectedUser>();
            foreach (var user in list)
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    clinicians.Add(user);
                }
            }
            clinicians = clinicians.OrderBy(a => a.DisplayName).ToList();
            return clinicians;
        }

        public bool AddUsersToTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers)
        {
            var agencyTeam = GetTeam(agencyId, agencyTeamId);
            if (selectedUsers != null && selectedUsers.Count > 0)
            {
                var teamUsers = agencyTeam.Users.ToObject<List<Guid>>()??new List<Guid>();
                foreach (var user in selectedUsers)
                {
                    if (!teamUsers.Contains(user))
                    {
                        teamUsers.Add(user);
                    }
                }
                agencyTeam.Users = teamUsers.ToXml();
            }
            database.Update<AgencyTeam>(agencyTeam);
            return true;
        }

        public bool RemoveUsersFromTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers)
        {
            var agencyTeam = GetTeam(agencyId, agencyTeamId);
            if (selectedUsers != null && selectedUsers.Count > 0)
            {
                var teamUsers = agencyTeam.Users.ToObject<List<Guid>>()??new List<Guid>();
                foreach (var user in selectedUsers)
                {
                    if (teamUsers.Contains(user))
                    {
                        teamUsers.Remove(user);
                    }
                }
                agencyTeam.Users = teamUsers.ToXml();
            }
            database.Update<AgencyTeam>(agencyTeam);
            return true;
        }

        public MultiGrid GetTeamAccessPatientData(Guid agencyId, Guid teamId) {
            var grid = new MultiGrid();
            var query = "SELECT p.Id as PatientId, p.FirstName as PatientFirstName, p.LastName as PatientLastName, COALESCE(pt.Id, '" + Guid.Empty.ToString() + "') as AgencyTeamId FROM patients p " +
                "RIGHT JOIN (SELECT patientteams.Id, patientteams.PatientId FROM patientteams WHERE patientteams.TeamId = @teamId) pt " +
                "ON p.Id = pt.PatientId " +
                "WHERE p.AgencyId = @agencyId " +
                "AND p.IsDeprecated = false";
            var list = new List<SelectableItem>();
            using (var cmd = new FluentCommand<SelectableItem>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("teamId", teamId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new SelectableItem
                    {
                        Id = reader.GetGuid("PatientId"),
                        DisplayName = reader.GetString("PatientLastName") + ", " + reader.GetString("PatientFirstName")
                    }).AsList();
            }
            list = list.OrderBy(p => p.DisplayName).ToList();
            grid.Left = list;

            query = "SELECT p.Id as PatientId, p.FirstName as PatientFirstName, p.LastName as PatientLastName, COALESCE(pt.Id, '" + Guid.Empty.ToString() + "') as AgencyTeamId FROM patients p " +
                "LEFT JOIN (SELECT patientteams.Id, patientteams.PatientId FROM patientteams WHERE patientteams.TeamId = @teamId) pt " +
                "ON p.Id = pt.PatientId " +
                "WHERE p.AgencyId = @agencyId " +
                "AND p.IsDeprecated = false " +
                "AND pt.Id is NULL";
            list = new List<SelectableItem>();
            using (var cmd = new FluentCommand<SelectableItem>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("teamId", teamId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new SelectableItem
                    {
                        Id = reader.GetGuid("PatientId"),
                        DisplayName = reader.GetString("PatientLastName") + ", " + reader.GetString("PatientFirstName")
                    }).AsList();
            }
            list = list.OrderBy(p => p.DisplayName).ToList();
            grid.Right = list;
            return grid;
        }

        public List<SelectedPatient> GetPatientsInTeam(Guid agencyId, Guid teamId)
        {
            var query = "SELECT p.Id as PatientId, p.FirstName as PatientFirstName, p.LastName as PatientLastName, COALESCE(pt.Id, '" + Guid.Empty.ToString() + "') as AgencyTeamId FROM patients p " +
                "RIGHT JOIN (SELECT patientteams.Id, patientteams.PatientId FROM patientteams WHERE patientteams.TeamId = @teamId) pt " +
                "ON p.Id = pt.PatientId " +
                "WHERE p.AgencyId = @agencyId " +
                "AND p.IsDeprecated = false";
            var list = new List<SelectedPatient>();
            using (var cmd = new FluentCommand<SelectedPatient>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("teamId", teamId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new SelectedPatient
                    {
                        PatientId = reader.GetGuid("PatientId"),
                        FirstName = reader.GetString("PatientFirstName"),
                        LastName = reader.GetString("PatientLastName"),
                        AgencyTeamId = reader.GetGuid("AgencyTeamId")
                    }).AsList();
            }
            list = list.OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
            return list;
        }

        public List<SelectedPatient> GetPatientsNotInTeam(Guid agencyId, Guid teamId)
        {
            var query = "SELECT p.Id as PatientId, p.FirstName as PatientFirstName, p.LastName as PatientLastName, COALESCE(pt.Id, '" + Guid.Empty.ToString() + "') as AgencyTeamId FROM patients p " +
                "LEFT JOIN (SELECT patientteams.Id, patientteams.PatientId FROM patientteams WHERE patientteams.TeamId = @teamId) pt " +
                "ON p.Id = pt.PatientId " +
                "WHERE p.AgencyId = @agencyId " +
                "AND p.IsDeprecated = false " +
                "AND pt.Id is NULL";
            var list = new List<SelectedPatient>();
            using (var cmd = new FluentCommand<SelectedPatient>(query))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("teamId", teamId)
                    .AddGuid("agencyId", agencyId)
                    .SetMap(reader => new SelectedPatient
                    {
                        PatientId = reader.GetGuid("PatientId"),
                        FirstName = reader.GetString("PatientFirstName"),
                        LastName = reader.GetString("PatientLastName"),
                        AgencyTeamId = reader.GetGuid("AgencyTeamId")
                    }).AsList();
            }
            list = list.OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
            return list;
        }

        public bool AddPatientsToTeam(Guid agencyTeamId, List<Guid> patientIds)
        {
            foreach (var patientId in patientIds)
            {
                var patientTeam = new PatientTeam
                {
                    Id = Guid.NewGuid(),
                    PatientId = patientId,
                    TeamId = agencyTeamId
                };
                database.Add<PatientTeam>(patientTeam);
            }
            return true;
        }
        public bool RemovePatientsFromTeam(Guid agencyTeamId, List<Guid> patientIds)
        {
            foreach (var patientId in patientIds)
            {
                var patientTeam = database.Single<PatientTeam>(x => x.TeamId == agencyTeamId && x.PatientId == patientId);
                if (patientTeam != null)
                {
                    database.Delete<PatientTeam>(patientTeam.Id);
                }
            }
            return true;
        }

        public List<SelectedPatient> GetTeamAccess(Guid agencyId, Guid currentUserId)
        {
            var teams = GetTeams(agencyId);
            var userTeams = new List<AgencyTeam>();
            var accessablePatients = new List<SelectedPatient>();
            foreach (var team in teams)
            {
                var users = team.Users.ToObject<List<Guid>>();
                if (users != null && users.Contains(currentUserId))
                {
                    userTeams.Add(team);
                }
            }
            foreach (var team in userTeams)
            {
                accessablePatients.AddRange(GetPatientsInTeam(agencyId, team.Id));
            }
            return accessablePatients.Distinct().ToList();
        }

        
        #endregion

        #region Subscription Plan

        public bool AddSubscriptionPlan(AgencySubscriptionPlan plan)
        {
            if (plan != null)
            {
                plan.Id = Guid.NewGuid();
                database.Add<AgencySubscriptionPlan>(plan);
                return true;
            }
            return false;
        }

        public List<AgencySubscriptionPlan> GetSubscriptionPlans(Guid agencyId)
        {
            return database.Find<AgencySubscriptionPlan>(p => p.AgencyId == agencyId).ToList();
       }

        public AgencySubscriptionPlan GetSubscriptionPlan(Guid agencyId, Guid locationId)
        {
            return database.Single<AgencySubscriptionPlan>(p => p.AgencyId == agencyId && p.AgencyLocationId == locationId);
        }

        public List<AgencySubscriptionPlan> GetUserSubscriptionPlans(Guid agencyId, List<Guid> locationIds)
        {
            var locationScript = string.Empty;
            if (locationIds != null && locationIds.Count > 0)
            {
                if (locationIds.Count == 1)
                {
                    locationScript = " AND asp.AgencyLocationId = '" + locationIds[0] + "'";
                }
                else
                {
                    locationScript = " AND asp.AgencyLocationId in (" + locationIds.Select(s => "'" + s.ToString() + "'").ToArray().Join(",") + ")";
                }
            }
            var list = new List<AgencySubscriptionPlan>();
            try
            {
                string[] arrStrings = { @"SELECT asp.Id, asp.AgencyLocationId, asp.IsUserPlan, asp.PlanLimit, ul.UserCount
                                    FROM agencysubscriptionplans asp
                                    JOIN (SELECT userlocations.AgencyLocationId, count(userlocations.UserId) as UserCount
                                         FROM userlocations
                                             LEFT JOIN users on userlocations.UserId = users.Id
                                                 WHERE
                                                     userlocations.AgencyId = @agencyid AND users.`Status` = 1 AND
                                                     users.IsDeprecated = 0
                                                         GROUP BY userlocations.AgencyLocationId) as ul on asp.AgencyLocationId = ul.AgencyLocationId
                                    WHERE
                                        asp.AgencyId = @agencyid AND 
                                        asp.IsUserPlan = 1 ", locationScript, ";" };
                using (var cmd = new FluentCommand<AgencySubscriptionPlan>(arrStrings.Join("")))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                            .AddGuid("agencyid", agencyId)
                            .SetMap(reader => new AgencySubscriptionPlan() { Id = reader.GetGuid("Id"), AgencyId = agencyId, AgencyLocationId = reader.GetGuid("AgencyLocationId"), PlanLimit = reader.GetInt("PlanLimit"), IsUserPlan = reader.GetBoolean("IsUserPlan"), CurrentPlanCount = reader.GetInt("UserCount") })
                            .AsList();
                }
            }
            catch (Exception)
            {
                return null;
            }
            return list;
        }

        public bool UpdateSubscriptionPlan(AgencySubscriptionPlan plan)
        {
            var result = false;
            if (plan != null)
            {
                result = database.Update<AgencySubscriptionPlan>(plan) > 0;
            }
            return result;
        }

        public int GetPatientCountPerLocation(Guid agencyId, Guid locationId)
        {
            int count = 0;
            var script = @"SELECT count(id) FROM patients WHERE AgencyId = @agencyid AND AgencyLocationId = @locationId AND status = 1 AND IsDeprecated = 0;";
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("locationId", locationId)
                .AsScalar();
            }
            return count;
        }

        public int GetUserCountPerLocation(Guid agencyId, Guid locationId)
        {
            int count = 0;
            var script = @"SELECT count(users.id) FROM users, userlocations WHERE users.Id = userlocations.UserId AND users.`Status` = 1 AND users.IsDeprecated = 0 AND userlocations.AgencyId = @agencyid AND userlocations.AgencyLocationId = @locationId;";
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("locationId", locationId)
                .AsScalar();
            }
            return count;
        }

        #endregion
    }
}
