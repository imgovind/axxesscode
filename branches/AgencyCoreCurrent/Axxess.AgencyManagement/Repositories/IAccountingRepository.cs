﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAccountingRepository
    {
        Payment GetAccountingPayment(string accountId);
        double GetAmount(bool IsUserPlan, int PreviousPackageId);
    }
}