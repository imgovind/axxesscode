﻿using System;
using System.ServiceProcess;
using System.ComponentModel;
using System.Configuration.Install;

namespace Axxess.Api.Services
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller cacheInstaller;
        private ServiceInstaller reportInstaller;
        private ServiceInstaller grouperInstaller;
        private ServiceInstaller validationInstaller;
        private ServiceInstaller authenticationInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.validationInstaller = new ServiceInstaller();
            this.validationInstaller.StartType = ServiceStartMode.Automatic;
            this.validationInstaller.ServiceName = "ValidationService";
            this.validationInstaller.DisplayName = "Validation Service";
            this.validationInstaller.Description = "Provides OASIS Validation to the AgencyCore Application.";

            this.grouperInstaller = new ServiceInstaller();
            this.grouperInstaller.StartType = ServiceStartMode.Automatic;
            this.grouperInstaller.ServiceName = "GrouperService";
            this.grouperInstaller.DisplayName = "Grouper Service";
            this.grouperInstaller.Description = "Provides Grouper HIPPS Code to the AgencyCore Application.";

            this.reportInstaller = new ServiceInstaller();
            this.reportInstaller.StartType = ServiceStartMode.Automatic;
            this.reportInstaller.ServiceName = "ReportService";
            this.reportInstaller.DisplayName = "Report Service";
            this.reportInstaller.Description = "Retrieves various reports for the AgencyCore Application.";

            this.cacheInstaller = new ServiceInstaller();
            this.cacheInstaller.StartType = ServiceStartMode.Automatic;
            this.cacheInstaller.ServiceName = "CacheService";
            this.cacheInstaller.DisplayName = "Cache Service";
            this.cacheInstaller.Description = "Caches Agency Info, Physician Info, and User Info for the AgencyCore Application.";

            this.authenticationInstaller = new ServiceInstaller();
            this.authenticationInstaller.StartType = ServiceStartMode.Automatic;
            this.authenticationInstaller.ServiceName = "AuthenticationService";
            this.authenticationInstaller.DisplayName = "Authentication Service";
            this.authenticationInstaller.Description = "Provides Single Sign-on services for Axxess Applications.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.validationInstaller, this.grouperInstaller, this.reportInstaller, this.authenticationInstaller, this.cacheInstaller });
        }
    }
}
