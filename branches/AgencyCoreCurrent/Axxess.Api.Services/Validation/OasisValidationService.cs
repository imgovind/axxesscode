﻿namespace Axxess.Api.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    public class OasisValidationService : BaseService, IValidationService
    {
        #region IValidationService Members

        public List<ValidationError> ValidateAssessment(string oasisDataString)
        {
            var validationErrors = new List<ValidationError>();
            try
            {
                validationErrors = OasisValidator.CheckDataString(oasisDataString);
            }
            catch (Exception ex)
            {
                var reason = ex.Message;
                if (ex.InnerException != null)
                {
                    reason += " Inner Exception: " + ex.InnerException.Message;
                }
                throw GetFault<DefaultFault>(reason);
            }
            return validationErrors;
        }

        public List<LogicalError> LogicalInconsistencyCheck(string oasisDataString)
        {
            var logicalErrors = new List<LogicalError>();
            try
            {
                logicalErrors = OasisValidator.CheckLogicalInconsistencies(oasisDataString);
            }
            catch (Exception ex)
            {
                var reason = ex.Message;
                if (ex.InnerException != null)
                {
                    reason += " Inner Exception: " + ex.InnerException.Message;
                }
                throw GetFault<DefaultFault>(reason);
            }
            return logicalErrors;
        }

        #endregion
    }
}