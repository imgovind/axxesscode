﻿namespace Axxess.Framework.Cryptography
{
    using Validation;
    using Configuration;
    using Extensibility;

    public static class Crypto
    {
        private static ICryptoProvider InternalCryptoProvider = Container.Resolve<ICryptoProvider>();

        public static string Encrypt(string plainText)
        {
            Check.Argument.IsNotEmpty(plainText, "plainText");

            return InternalCryptoProvider.Encrypt(plainText,
                FrameworkSettings.CryptoPassPhrase,
                FrameworkSettings.CryptoSaltValue,
                FrameworkSettings.CryptoHashAlgorithm,
                FrameworkSettings.CryptoPasswordIterations,
                FrameworkSettings.CryptoInitVector,
                FrameworkSettings.CryptoKeySize);
        }

        public static string Decrypt(string cypherText)
        {
            Check.Argument.IsNotEmpty(cypherText, "cypherText");

            return InternalCryptoProvider.Decrypt(cypherText,
                FrameworkSettings.CryptoPassPhrase,
                FrameworkSettings.CryptoSaltValue,
                FrameworkSettings.CryptoHashAlgorithm,
                FrameworkSettings.CryptoPasswordIterations,
                FrameworkSettings.CryptoInitVector,
                FrameworkSettings.CryptoKeySize);
        }
    }
}
