﻿namespace Axxess.Framework.Cryptography
{
    using System;
    using System.Security.Cryptography;

    public static class TokenFactory
    {
        public static string GetNewToken()
        {
            byte[] random = new byte[20];
            var randonNumberProvider = new RNGCryptoServiceProvider();
            randonNumberProvider.GetBytes(random);
            return Convert.ToBase64String(random);
        }
    }
}
