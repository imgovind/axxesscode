﻿namespace Axxess.Framework.Extensions
{
    using System;
    using System.Diagnostics;

    using Axxess.Framework.Validation;
    using Axxess.Framework.Serialization;

    public static class JsonExtension
    {
        [DebuggerStepThrough]
        public static T FromJson<T>(this string json)
        {
            Check.Argument.IsNotEmpty(json, "json");

            return JSerializer.Deserialize<T>(json);
        }

        [DebuggerStepThrough]
        public static string ToJson<T>(this T instance)
        {
            Check.Argument.IsNotNull(instance, "instance");

            return JSerializer.Serialize(instance);
        }
    }
}
