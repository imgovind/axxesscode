﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;
    using System.Collections.Generic;

    public static class Mapper
    {
        private static IDictionary<System.Type, object> register;

        private static void Initialize()
        {
            if (register == null)
            {
                register = new Dictionary<System.Type, object>();
            }
        }

        public static void Register<T>(ISelectable<T> converter)
        {
            Initialize();
            register[typeof(T)] = converter;
        }

        public static ISelectable<T> Resolve<T>()
        {
            return register.ContainsKey(typeof(T)) ? register[typeof(T)] as ISelectable<T> : null;
        }
    }
}
