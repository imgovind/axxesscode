﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Text;
    using System.Data;
    using System.Linq;
    using System.Dynamic;
    using System.Reflection;
    using System.Linq.Expressions;
    using System.Collections.Generic;

    using Extensions;

    public static class DynamicQuery
    {
        #region Constructor

        private static Dictionary<Type, DbType> typeMap;

        static DynamicQuery()
        {
            Initialize();
        }

        #endregion

        #region Public Methods

        public static QueryResult GetInsertQuery<T>(string tableName, T item)
        {
            PropertyInfo[] props = PropertyCache.Get<T>();
            string[] columns = props.Select(p => p.Name).ToArray();

            var statement = string.Format("INSERT INTO {0} ({1}) VALUES (@{2})",
                                 tableName,
                                 string.Join(", ", columns),
                                 string.Join(", @", columns));

            IList<Parameter> parameters = new List<Parameter>();
            columns.ForEach(column =>
            {
                var prop = props.FirstOrDefault(p => p.Name.IsEqual(column));
                parameters.Add(new Parameter
                {
                    Name = column,
                    Value = prop.GetValue(item),
                    Type = typeMap[prop.PropertyType]
                });
            });

            return new QueryResult(statement, parameters);
        }

        public static QueryResult GetUpdateQuery<T>(string tableName, T item)
        {
            PropertyInfo[] props = PropertyCache.Get<T>();
            string[] columns = props.Select(p => p.Name).ToArray();

            var parameterNames = columns.Select(name => name + "=@" + name.ToLower()).ToList();
            var statement = string.Format("UPDATE {0} SET {1} WHERE Id=@id", tableName, string.Join(", ", parameterNames));

            IList<Parameter> parameters = new List<Parameter>();
            columns.ForEach(column =>
            {
                var prop = props.FirstOrDefault(p => p.Name.IsEqual(column));
                parameters.Add(new Parameter { 
                    Name = column.ToLower(), 
                    Value = prop.GetValue(item),
                    Type = typeMap[prop.PropertyType]
                });
            });

            return new QueryResult(statement, parameters);
        }

        public static QueryResult GetDynamicQuery<T>(string tableName, Expression<Func<T, bool>> expression)
        {
            var builder = new StringBuilder();
            var queryProperties = new List<QueryParameter>();
            IList<Parameter> parameters = new List<Parameter>();

            var body = (BinaryExpression)expression.Body;
            WalkTree(body, ExpressionType.Default, ref queryProperties);

            builder.Append("SELECT * FROM ");
            builder.Append(tableName);
            builder.Append(" WHERE ");

            for (int i = 0; i < queryProperties.Count(); i++)
            {
                QueryParameter item = queryProperties[i];

                if (item.LinkingOperator.IsNotNullOrEmpty() && i > 0)
                {
                    builder.Append(string.Format("{0} {1} {2} @{1} ", item.LinkingOperator, item.PropertyName, item.QueryOperator, item.PropertyValue));
                }
                else
                {
                    builder.Append(string.Format("{0} {1} @{0} ", item.PropertyName, item.QueryOperator));
                }

                parameters.Add(new Parameter { Name = item.PropertyName, Value = item.PropertyValue, Type = item.DatabaseType });
            }

            return new QueryResult(builder.ToString().TrimEnd(), parameters);
        }

        #endregion

        #region Private Methods

        private static void WalkTree(BinaryExpression body, ExpressionType linkingType, ref List<QueryParameter> queryProperties)
        {
            if (body.NodeType != ExpressionType.AndAlso && body.NodeType != ExpressionType.OrElse)
            {
                string link = GetOperator(linkingType);
                string propertyName = GetPropertyName(body);
                dynamic propertyValue = GetPropertyValue(body.Right);
                string databaseOperator = GetOperator(body.NodeType);
                DbType databaseType = typeMap[propertyValue.GetType()];

                queryProperties.Add(new QueryParameter(link, propertyName, propertyValue, databaseOperator, databaseType));
            }
            else
            {
                WalkTree((BinaryExpression)body.Left, body.NodeType, ref queryProperties);
                WalkTree((BinaryExpression)body.Right, body.NodeType, ref queryProperties);
            }
        }

        private static string GetPropertyName(BinaryExpression body)
        {
            string propertyName = body.Left.ToString().Split(new char[] { '.' })[1];

            if (body.Left.NodeType == ExpressionType.Convert)
            {
                propertyName = propertyName.Replace(")", string.Empty);
            }

            return propertyName;
        }

        private static string GetOperator(ExpressionType type)
        {
            switch (type)
            {
                case ExpressionType.Equal:
                    return "=";
                case ExpressionType.NotEqual:
                    return "!=";
                case ExpressionType.LessThan:
                    return "<";
                case ExpressionType.GreaterThan:
                    return ">";
                case ExpressionType.AndAlso:
                case ExpressionType.And:
                    return "AND";
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                    return "OR";
                case ExpressionType.Default:
                    return string.Empty;
                default:
                    throw new NotImplementedException();
            }
        }

        private static object GetPropertyValue(Expression body)
        {
            var objectMember = Expression.Convert(body, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(objectMember);
            var getter = getterLambda.Compile();
            return getter();
        }

        private static void Initialize()
        {
            typeMap = new Dictionary<Type, DbType>();
            typeMap[typeof(byte)] = DbType.Byte;
            typeMap[typeof(sbyte)] = DbType.SByte;
            typeMap[typeof(short)] = DbType.Int16;
            typeMap[typeof(ushort)] = DbType.UInt16;
            typeMap[typeof(int)] = DbType.Int32;
            typeMap[typeof(uint)] = DbType.UInt32;
            typeMap[typeof(long)] = DbType.Int64;
            typeMap[typeof(ulong)] = DbType.UInt64;
            typeMap[typeof(float)] = DbType.Single;
            typeMap[typeof(double)] = DbType.Double;
            typeMap[typeof(decimal)] = DbType.Decimal;
            typeMap[typeof(bool)] = DbType.Boolean;
            typeMap[typeof(string)] = DbType.String;
            typeMap[typeof(char)] = DbType.StringFixedLength;
            typeMap[typeof(Guid)] = DbType.Guid;
            typeMap[typeof(DateTime)] = DbType.DateTime;
            typeMap[typeof(DateTimeOffset)] = DbType.DateTimeOffset;
            typeMap[typeof(byte[])] = DbType.Binary;
            typeMap[typeof(byte?)] = DbType.Byte;
            typeMap[typeof(sbyte?)] = DbType.SByte;
            typeMap[typeof(short?)] = DbType.Int16;
            typeMap[typeof(ushort?)] = DbType.UInt16;
            typeMap[typeof(int?)] = DbType.Int32;
            typeMap[typeof(uint?)] = DbType.UInt32;
            typeMap[typeof(long?)] = DbType.Int64;
            typeMap[typeof(ulong?)] = DbType.UInt64;
            typeMap[typeof(float?)] = DbType.Single;
            typeMap[typeof(double?)] = DbType.Double;
            typeMap[typeof(decimal?)] = DbType.Decimal;
            typeMap[typeof(bool?)] = DbType.Boolean;
            typeMap[typeof(char?)] = DbType.StringFixedLength;
            typeMap[typeof(Guid?)] = DbType.Guid;
            typeMap[typeof(DateTime?)] = DbType.DateTime;
        }

        #endregion
    }
}
