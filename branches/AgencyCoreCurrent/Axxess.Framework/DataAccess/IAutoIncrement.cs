﻿namespace Axxess.Framework.DataAccess
{
    using System;

    public interface IAutoIncrement<T> : IConvertable<T>
    {
        void ApplyAutoNumber(T instance, int newId);
    }
}
