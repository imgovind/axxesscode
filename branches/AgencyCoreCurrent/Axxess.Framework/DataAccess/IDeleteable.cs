﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;

    public interface IDeleteable<T> : IConvertable<T>
    {
        string DeleteSql();
        void ApplyDelete(T instance, IDbCommand command);
    }
}
