﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;

    public interface IInsertable<T> : IConvertable<T>
    {
        string InsertSql();
        void ApplyInsert(T instance, IDbCommand command);
    }
}
