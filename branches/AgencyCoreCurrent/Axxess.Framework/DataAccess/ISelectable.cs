﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;

    public interface ISelectable<T> : IConvertable<T>
    {
        T ApplySelect(DataReader reader);
    }
}
