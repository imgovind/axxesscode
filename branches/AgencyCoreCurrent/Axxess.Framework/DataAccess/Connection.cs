﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Collections.Generic;

    using Axxess.Framework;
    using Axxess.Framework.Extensions;

    public class Connection<T> : Disposable where T : class, new()
    {
        #region Constructors/Members

        private IDbConnection connection;

        public delegate bool DelegatePredicate(T input);
        public delegate T DelegateReader(DataReader dataReader);

        public Connection(IDbConnection connection)
        {
            this.connection = connection;
            if (this.connection.State != ConnectionState.Open)
            {
                this.connection.Open();
            }
        }

        #endregion Constructors/Members

        #region Private Methods

        private int GetLastInsertRowId()
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = GetLastInsertedAutoNumberStatement();
                using (IDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    return reader.Read() ? Int32.Parse(reader[0].ToString()) : 0;
                }
            }
        }

        private string GetLastInsertedAutoNumberStatement()
        {
            switch (connection.GetType().Name.ToUpper().Replace("CONNECTION", ""))
            {
                case "SQLITE": return "select last_insert_rowid()";
                case "SQL": return "select @@identity";
                case "NPGSQL": return "select lastval()";
                case "MYSQL": return "SELECT LAST_INSERT_ID();";
            }
            throw new ApplicationException("Unknown autonumber statement");
        }

        private IDbDataParameter CreateParameter(IDbCommand command, Parameter parameter)
        {
            var result = command.CreateParameter();
            result.DbType = parameter.Type;
            result.Value = parameter.Value;
            result.ParameterName = parameter.Name;
            return result;
        }

        private void AddParameters(IDbCommand command, IEnumerable<Parameter> parameters)
        {
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    command.Parameters.Add(CreateParameter(command, parameter));
                }
            }
        }

        #endregion Private Methods

        #region SELECT Methods

        public IEnumerable<T> Select(string statement)
        {
            return Select(statement, Mapper.Resolve<T>(), null);
        }

        public IEnumerable<T> Select(string statement, DelegateReader readMapper)
        {
            return Select(statement, readMapper, null);
        }

        public IEnumerable<T> Select(string statement, IEnumerable<Parameter> parameters)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = statement;
                AddParameters(command, parameters);
                using (IDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var customReader = new DataReader(dataReader);
                    while (dataReader.Read())
                    {
                        T instance = new T();
                        dataReader.Hydrate(instance);
                        yield return instance;
                    }
                }
            }
        }

        private IEnumerable<T> Select(string statement, ISelectable<T> traits, IEnumerable<Parameter> parameters)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = statement;
                AddParameters(command, parameters);
                using (IDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var customReader = new DataReader(dataReader);
                    while (dataReader.Read())
                    {
                        T instance = default(T);
                        if (traits != null)
                        {
                            instance = traits.ApplySelect(customReader);
                        }
                        else
                        {
                            instance = new T();
                            dataReader.Hydrate(instance);
                        }
                        yield return instance;
                    }
                }
            }
        }

        private IEnumerable<T> Select(string statement, DelegateReader readMapper, IEnumerable<Parameter> parameters)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = statement;
                AddParameters(command, parameters);
                using (IDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    var customReader = new DataReader(dataReader);
                    while (dataReader.Read())
                    {
                        T instance = default(T);
                        if (readMapper != null)
                        {
                            yield return readMapper(customReader);
                        }
                        else
                        {
                            instance = new T();
                            dataReader.Hydrate(instance);
                        }
                        yield return instance;
                    }
                }
            }
        }

        #endregion SELECT Methods

        #region INSERT Methods

        public bool Insert(T model)
        {
            var result = false;
            QueryResult queryResult = DynamicQuery.GetInsertQuery(typeof(T).Name.ToLowerInvariant().Pluralize(), model);
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = queryResult.Statement;
                AddParameters(command, queryResult.Parameters);
                if (command.ExecuteNonQuery() > 0)
                {
                    result = true;
                }
            }

            return result;
        }

        #endregion INSERT Methods

        #region DELETE Methods

        public bool Delete(T model)
        {
            var result = false;
            QueryResult queryResult = DynamicQuery.GetInsertQuery(typeof(T).Name.ToLowerInvariant().Pluralize(), model);
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = queryResult.Statement;
                AddParameters(command, queryResult.Parameters);
                if (command.ExecuteNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public void ApplyUpdates(IEnumerable<T> items, IList<T> deletedItems, DelegatePredicate insertCriterium, DelegatePredicate updateCriterium)
        {
            if (deletedItems != null)
            {
                foreach (T item in deletedItems)
                {
                    this.Delete(item);
                }
                deletedItems.Clear();
            }

            if (items != null)
            {
                foreach (T item in items)
                {
                    if (insertCriterium(item))
                    {
                        this.Insert(item);
                    }
                    else if (updateCriterium(item))
                    {
                        this.Update(item);
                    }
                }
            }
        }

        #endregion DELETE Methods

        #region UPDATE Methods

        public bool Update(T model)
        {
            var result = false;
            QueryResult queryResult = DynamicQuery.GetUpdateQuery(typeof(T).Name.ToLowerInvariant().Pluralize(), model);
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = queryResult.Statement;
                AddParameters(command, queryResult.Parameters);
                if (command.ExecuteNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion UPDATE Methods

        #region EXECUTE Methods

        public bool Execute(string statement)
        {
            return Execute(statement, null);
        }
        
        public bool Execute(string statement, IEnumerable<Parameter> parameters)
        {
            var result = false;
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = statement;
                AddParameters(command, parameters);
                if (command.ExecuteNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion Execute Methods

        #region Disposable Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.connection != null)
            {
                if (this.connection.State != ConnectionState.Closed)
                {
                    this.connection.Close();
                    this.connection.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion  Disposable Overrides
    }
}