﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Collections.Generic;

    public class QueryResult
    {
        private readonly Tuple<string, IList<Parameter>> result;

        public QueryResult(string sql, IList<Parameter> parameters)
        {
            result = new Tuple<string, IList<Parameter>>(sql, parameters);
        }

        public string Statement
        {
            get
            {
                return result.Item1;
            }
        }

        public IList<Parameter> Parameters
        {
            get
            {
                return result.Item2;
            }
        }

        
    }
}
