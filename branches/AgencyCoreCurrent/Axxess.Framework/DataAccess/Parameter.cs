﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;

    public class Parameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public DbType Type { get; set; }
    }
}
