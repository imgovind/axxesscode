﻿namespace Axxess.Framework.DataAccess
{
    using System;
    using System.Data;

    public interface IUpdateable<T> : IConvertable<T>
    {
        string UpdateSql();
        void ApplyUpdate(T instance, IDbCommand command);
    }
}
