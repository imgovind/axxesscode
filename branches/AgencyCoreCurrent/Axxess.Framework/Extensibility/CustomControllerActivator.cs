﻿namespace Axxess.Framework.Extensibility
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Framework.Extensibility;

    public class CustomControllerActivator : IControllerActivator
    {
        IController IControllerActivator.Create(RequestContext requestContext, Type controllerType)
        {
            return Container.Resolve(controllerType) as IController;
        }
    }
}
