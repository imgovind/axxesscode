﻿namespace Axxess.Framework.Extensibility
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http.Dependencies;
    
    public class CustomWebApiDependencyResolver : IDependencyResolver
    {
        public IDependencyScope BeginScope()
        {
            return this;
        }

        public object GetService(Type serviceType)
        {
            return Container.Resolve(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Container.ResolveAll<object>(serviceType);
        }

        public void Dispose()
        {
        }
    }
}
