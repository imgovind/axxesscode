﻿namespace Axxess.Framework.Attributes
{
    using System;
    using System.ComponentModel;

    public class GroupDescriptionAttribute : DescriptionAttribute
    {
        public GroupDescriptionAttribute(string description, string group)
            : base(description)
        {
            this.Group = group;
        }

        public string Group { get; private set; }
    }
}
