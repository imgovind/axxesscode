﻿namespace Axxess.Framework.Session
{
    using System.Configuration;

    using Caching;
    using Extensibility;

    using Enyim.Caching;
    using Couchbase.Configuration;

    public static class SessionBucketActivator
    {
        #region Members

        private static IMemcachedClient memcachedClient;

        public static IMemcachedClient Cache
        {
            get
            {
                if (memcachedClient == null)
                {
                    Initialize();
                }
                return memcachedClient;
            }
        }

        private static void Initialize()
        {
            ICouchbaseClientFactory factory = Container.Resolve<ICouchbaseClientFactory>();
            if (factory != null)
            {
                memcachedClient = factory.Create((ICouchbaseClientConfiguration)ConfigurationManager.GetSection("session-bucket"));
            }
        }

        #endregion
    }
}
