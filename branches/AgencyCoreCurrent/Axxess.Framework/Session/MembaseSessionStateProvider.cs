﻿namespace Axxess.Framework.Session
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.SessionState;
    using System.Web.Configuration;

    using Configuration;

    using Couchbase;

    using Enyim.Caching;
    using Enyim.Caching.Memcached;

    public class MembaseSessionStateProvider : SessionStateStoreProviderBase
    {
        #region Private Members

        private static readonly IMemcachedClient cache = SessionBucketActivator.Cache;

        #endregion

        #region SessionStateStoreProviderBase Members

        public override SessionStateStoreData CreateNewStoreData(HttpContext context, int timeout)
        {
            return SessionActor.CreateNewStoreData(context, timeout);
        }

        public override void CreateUninitializedItem(HttpContext context, string id, int timeout)
        {
            cache.Store(StoreMode.Set, id, new MembaseHolder(null, false, DateTime.Now, 0, 1), new TimeSpan(0, timeout, 0));
        }

        public override void Dispose()
        {
        }

        public override void EndRequest(HttpContext context)
        {
        }

        public override SessionStateStoreData GetItem(HttpContext context, string id, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actionFlags)
        {
            DateTime setTime = DateTime.Now;
            SessionStateStoreData item = null;

            lockId = null;
            locked = false;
            actionFlags = 0;
            lockAge = TimeSpan.Zero;

            MembaseHolder holder = cache.Get<MembaseHolder>(id);
            if (holder != null)
            {
                if (!holder.Locked)
                {
                    holder.LockId++;
                    holder.SetTime = setTime;
                    cache.Store(StoreMode.Set, id, holder, new TimeSpan(0, 0, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes), 0, 0));

                    lockId = holder.LockId;
                    lockAge = holder.LockAge;
                    actionFlags = (SessionStateActions)holder.ActionFlag;

                    if (actionFlags == SessionStateActions.InitializeItem)
                    {
                        item = SessionActor.CreateNewStoreData(context, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes));
                    }
                    else
                    {
                        item = SessionActor.Deserialize(context, holder.Content, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes));
                    }
                }
                else
                {
                    locked = true;
                    lockId = holder.LockId;
                    lockAge = holder.LockAge;
                    actionFlags = (SessionStateActions)holder.ActionFlag;
                }
            }
            return item;
        }

        public override SessionStateStoreData GetItemExclusive(HttpContext context, string id, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actionFlags)
        {
            DateTime setTime = DateTime.Now;
            SessionStateStoreData item = null;

            lockId = null;
            locked = false;
            actionFlags = 0;
            lockAge = TimeSpan.Zero;

            MembaseHolder holder = cache.Get<MembaseHolder>(id);
            if (holder != null)
            {
                if (!holder.Locked)
                {
                    holder.LockId++;
                    holder.SetTime = setTime;
                    holder.Locked = true;
                    cache.Store(StoreMode.Set, id, holder, new TimeSpan(0, 0, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes), 0, 0));

                    locked = true;
                    lockId = holder.LockId;
                    lockAge = holder.LockAge;
                    actionFlags = (SessionStateActions)holder.ActionFlag;
                }
                else
                {
                    locked = true;
                    lockId = holder.LockId;
                    lockAge = holder.LockAge;
                    actionFlags = (SessionStateActions)holder.ActionFlag;
                }

                if (actionFlags == SessionStateActions.InitializeItem)
                {
                    item = SessionActor.CreateNewStoreData(context, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes));
                }
                else
                {
                    item = SessionActor.Deserialize(context, holder.Content, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes));
                }

            }
            return item;
        }

        public override void InitializeRequest(HttpContext context)
        {
        }

        public override void ReleaseItemExclusive(HttpContext context, string id, object lockId)
        {
            MembaseHolder holder = cache.Get<MembaseHolder>(id);

            if (holder != null)
            {
                holder.Locked = false;
                holder.LockId = (int)lockId;
                cache.Store(StoreMode.Set, id, holder);
            }
        }

        public override void RemoveItem(HttpContext context, string id, object lockId, SessionStateStoreData item)
        {
            cache.Remove(id);
        }

        public override void ResetItemTimeout(HttpContext context, string id)
        {
            object item = cache.Get(id);

            if (item != null)
            {
                cache.Store(StoreMode.Set, id, item, new TimeSpan(0, Convert.ToInt32(FrameworkSettings.MembaseSessionTimeoutInMinutes), 0));
            }
        }

        public override void SetAndReleaseItemExclusive(HttpContext context, string id, SessionStateStoreData item, object lockId, bool newItem)
        {
            byte[] content = null;
            DateTime setTime = DateTime.Now;

            content = SessionActor.Serialize((SessionStateItemCollection)item.Items);

            if (newItem == true)
            {
                MembaseHolder holder = new MembaseHolder(content, false, setTime, 0, 0);
                cache.Store(StoreMode.Add, id, holder, new TimeSpan(0, item.Timeout, 0));
            }
            else
            {
                MembaseHolder holder = new MembaseHolder(content, false, setTime, 0, 0);
                cache.Store(StoreMode.Set, id, holder, new TimeSpan(0, item.Timeout, 0));
            }
        }

        public override bool SetItemExpireCallback(SessionStateItemExpireCallback expireCallback)
        {
            return false;
        }

        #endregion
    }
}
