﻿namespace Axxess.Framework.Session
{
    using System;
    using System.Web;

    using Validation;
    using Extensibility;

    public static class SessionStore
    {
        private static ISessionStore InternalSession
        {
            get
            {
                return Container.Resolve<ISessionStore>();
            }
        }

        public static string SessionId
        {
            get
            {
                return InternalSession.Id;
            }
        }

        public static T Get<T>(string key)
        {
            Check.Argument.IsNotEmpty(key, "key");

            return InternalSession.Get<T>(key);
        }

        public static void Add<T>(string key, T value)
        {
            Check.Argument.IsNotEmpty(key, "key");
            Check.Argument.IsNotNull(value, "value");

            RemoveIfExists(key);

            InternalSession.Add(key, value);
        }

        public static void Remove(string key)
        {
            Check.Argument.IsNotEmpty(key, "key");

            InternalSession.Remove(key);
        }

        public static bool Contains(string key)
        {
            Check.Argument.IsNotEmpty(key, "key");

            return InternalSession.Contains(key);
        }

        public static void Abandon()
        {
            InternalSession.Abandon();
        }

        public static void Clear()
        {
            InternalSession.Clear();
        }

        internal static void RemoveIfExists(string key)
        {
            if (InternalSession.Contains(key))
            {
                InternalSession.Remove(key);
            }
        }

        public static bool IsUserSessionActive()
        {
            return InternalSession != null;
        }
    }
}
