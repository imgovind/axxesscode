﻿namespace Axxess.Framework.Session
{
    public interface ISessionStore
    {
        string Id { get; }
        T Get<T>(string key);
        bool Contains(string key);
        void Add<T>(string key, T value);
        void Remove(string key);
        void Abandon();
        void Clear();
    }
}
