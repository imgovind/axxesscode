﻿namespace Axxess.Framework.Session
{
    using System.Configuration;

    using Caching;
    using Extensibility;

    using Enyim.Caching;
    using Couchbase.Configuration;

    public static class DefaultBucketActivator
    {
        #region Members

        private static IMemcachedClient memcachedClient;

        public static IMemcachedClient Cache
        {
            get
            {
                if (memcachedClient == null)
                {
                    Initialize();
                }
                return memcachedClient;
            }
        }

        private static void Initialize()
        {
            ICouchbaseClientFactory factory = Container.Resolve<ICouchbaseClientFactory>();
            if (factory != null)
            {
                memcachedClient = factory.Create((ICouchbaseClientConfiguration)ConfigurationManager.GetSection("default-bucket"));
            }
        }

        #endregion
    }
}
