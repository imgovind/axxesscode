﻿namespace Axxess.Framework.Caching
{
    using System;
    using System.Collections.Specialized;

    using Enyim.Caching;

    using Couchbase;
    using Couchbase.Configuration;

    public sealed class CouchbaseClientFactory : ICouchbaseClientFactory
    {
        public IMemcachedClient Create(ICouchbaseClientConfiguration config)
        {
            if (config == null)
                throw new InvalidOperationException("Invalid Couchbase Configuration section: ");
            return new CouchbaseClient(config);
        }
    }
}
