﻿namespace Axxess.Framework.Caching
{
    using System;
    using System.Net;

    using Couchbase;
    using Couchbase.Configuration;

    using Enyim.Caching.Memcached;

    public static class CouchbaseClientActivator
    {
        #region Members

        private static CouchbaseClient couchbaseClient;

        public static CouchbaseClient Cache
        {
            get
            {
                if (couchbaseClient == null)
                {
                    Initialize();
                }
                return couchbaseClient;
            }
        }

        private static void Initialize()
        {
            var couchbaseClientConfiguration = new CouchbaseClientConfiguration();
            //couchbaseClientConfiguration.SocketPool.DeadTimeout = new TimeSpan(0, 0, 10);
            //couchbaseClientConfiguration.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, 2);
            //couchbaseClientConfiguration.Bucket = "default";
            //couchbaseClientConfiguration.BucketPassword = "";

            ////Quick test of Store/Get operations
            //if (CoreSettings.MemcacheServerUriArray != null && CoreSettings.MemcacheServerUriArray.Length > 0)
            //{
            //    foreach (var serverUri in CoreSettings.MemcacheServerUriArray)
            //    {
            //        couchbaseClientConfiguration.Urls.Add(new Uri(serverUri));
            //    }
            couchbaseClient = new CouchbaseClient(couchbaseClientConfiguration);
            //}
        }

        #endregion
    }
}
