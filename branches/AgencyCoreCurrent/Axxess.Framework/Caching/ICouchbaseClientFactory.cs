﻿namespace Axxess.Framework.Caching
{
    using System.Collections.Specialized;
    
    using Enyim.Caching;

    using Couchbase.Configuration;

    public interface ICouchbaseClientFactory
    {
        IMemcachedClient Create(ICouchbaseClientConfiguration config);
    }
}
