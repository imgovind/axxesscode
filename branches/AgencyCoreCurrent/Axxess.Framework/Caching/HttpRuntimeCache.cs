﻿namespace Axxess.Framework.Caching
{
    using System;
    using System.Web;
    using System.Web.Caching;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Framework.Configuration;

    public sealed class HttpRuntimeCache : ICache
    {
        #region Members

        private static System.Web.Caching.Cache cache;

        #endregion

        #region Constructors

        static HttpRuntimeCache()
        {
            cache = HttpRuntime.Cache;
        }

        #endregion

        #region ICache Members

        public int Count
        {
            get { return cache.Count; }
        }

        public void Remove(string key)
        {
            cache.Remove(key);
        }

        public bool Contains(string key)
        {
            return cache[key] != null;
        }

        public void Set<T>(string key, T value)
        {
            cache.Insert(key, value, null, DateTime.Now.AddMinutes(FrameworkSettings.CachingIntervalInMinutes), Cache.NoSlidingExpiration);
        }

        public void Set<T>(string key, T value, DateTime absoluteExpiration)
        {
            cache.Insert(key, value, null, absoluteExpiration, Cache.NoSlidingExpiration);
        }

        public void Set<T>(string key, T value, TimeSpan slidingExpiration)
        {
            cache.Insert(key, value, null, DateTime.Now.AddMinutes(FrameworkSettings.CachingIntervalInMinutes), slidingExpiration);
        }

        public bool TryGet<T>(string key, out T value)
        {
            value = default(T);

            if (Contains(key))
            {
                object cached = cache.Get(key);

                if (cached != null)
                {
                    value = (T)cached;

                    return true;
                }
            }
            return false;
        }

        public T Get<T>(string key)
        {
            T result = default(T);

            if (Contains(key))
            {
                object cached = cache.Get(key);
                if (cached != null)
                {
                    result = (T)cached;
                }
            }
            return result;
        }

        public List<string> CachedKeys
        {
            get
            {
                var list = new List<string>();
                IDictionaryEnumerator enumerator = cache.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    list.Add(enumerator.Key.ToString());
                }
                return list;
            }
        }

        #endregion
    }
}
