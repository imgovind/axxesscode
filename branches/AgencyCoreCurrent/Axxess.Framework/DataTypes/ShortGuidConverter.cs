﻿namespace Axxess.Framework.DataTypes
{
    using System;
    using System.Globalization;
    using System.ComponentModel;

    public class ShortGuidConverter : TypeConverter
    {
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
            {
                return ShortGuid.NewId();
            }
            if (value is string)
            {
                return new ShortGuid(value.ToString());
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            else
            {
                return base.CanConvertFrom(context, sourceType);
            }
        }

    }
}
