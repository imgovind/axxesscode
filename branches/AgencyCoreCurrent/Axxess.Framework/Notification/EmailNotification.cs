﻿namespace Axxess.Framework.Notification
{
    using System;
    using System.Net.Mail;
    using System.Threading;

    using Extensions;
    using Axxess.Framework.Configuration;

    public class EmailNotification : INotification
    {
        #region INotification Members

        public void Send(string fromAddress, string toAddress, string ccAddress, string subject, string body)
        {
            using (MailMessage mail = CreateMessage(fromAddress, toAddress, ccAddress, subject, body))
            {
                SendMail(mail);
            }
        }

        public void Send(string fromAddress, string toAddress, string subject, string body)
        {
            using (MailMessage mail = CreateMessage(fromAddress, toAddress, subject, body))
            {
                SendMail(mail);
            }
        }

        public void SendAsync(string fromAddress, string toAddress, string subject, string body)
        {
            ThreadPool.QueueUserWorkItem(state => Send(fromAddress, toAddress, subject, body));
        }

        #endregion

        #region Static Methods

        private static void SendMail(MailMessage mail)
        {
            try
            {
                SmtpClient smtp = new SmtpClient
                {
                    EnableSsl = FrameworkSettings.EnableSSLMail
                };

                smtp.Send(mail);
            }
            catch (Exception)
            {
            }
        }

        private static MailMessage CreateMessage(string fromAddress, string toAddress, string subject, string body)
        {
            return CreateMessage(fromAddress, toAddress, string.Empty, subject, body);
        }

        private static MailMessage CreateMessage(string fromAddress, string toAddress, string ccAddress, string subject, string body)
        {
            MailMessage message = new MailMessage
            {
                Body = body,
                Subject = subject,
                IsBodyHtml = true,
                Sender = new MailAddress(FrameworkSettings.NoReplyEmail),
                From = new MailAddress(fromAddress, FrameworkSettings.NoReplyDisplayName)
            };

            string[] tos = toAddress.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string to in tos)
            {
                message.To.Add(new MailAddress(to));
            }

            if (ccAddress.IsNotNullOrEmpty())
            {
                string[] ccList = ccAddress.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string cc in ccList)
                {
                    message.CC.Add(new MailAddress(cc));
                }
            }

            return message;
        }

        #endregion
    }
}
