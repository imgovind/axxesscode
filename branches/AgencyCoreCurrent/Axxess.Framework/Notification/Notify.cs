﻿namespace Axxess.Framework.Notification
{
    using Axxess.Framework.Extensibility;
    using System;

    public static class Notify
    {
        #region Static Methods

        public static void User(string fromAddress, string toAddress, string subject, string body)
        {
            INotification notification = Container.Resolve<INotification>();
            notification.Send(fromAddress, toAddress, subject, body);
        }

        #endregion
    }
}
