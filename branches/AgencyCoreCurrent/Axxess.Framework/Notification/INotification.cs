﻿namespace Axxess.Framework.Notification
{
    using System;

    public interface INotification
    {
        void Send(string fromAddress, string toAddress, string subject, string body);
    }
}
