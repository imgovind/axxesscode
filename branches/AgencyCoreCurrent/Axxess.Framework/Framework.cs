﻿namespace Axxess.Framework
{
    using Caching;
    using Session;
    using Cryptography;
    using Configuration;
    using Extensibility;
    using Notification;

    public static class Framework
    {
        public static void Initialize()
        {
            Container.InitializeWith(new MunqTypeResolver());
            Container.Register<ICryptoProvider, RijndaelProvider>(ObjectLifeSpans.Singleton);
            Container.Register<IWebConfiguration, WebConfiguration>(ObjectLifeSpans.Singleton);
            Container.Register<INotification, EmailNotification>(ObjectLifeSpans.Singleton);

            Container.Register<ICouchbaseClientFactory, CouchbaseClientFactory>(ObjectLifeSpans.Singleton);

            Container.Register<ICache, MembaseCache>("MembaseCache", ObjectLifeSpans.Singleton);
            Container.Register<ICache, HttpRuntimeCache>("HttpRuntimeCache", ObjectLifeSpans.Singleton);

            Container.Register<ISessionStore, HttpContextSession>(ObjectLifeSpans.Singleton);
        }
    }
}