(function($) {
    $.extend($.fn, {
        Zebra: function(selector) {
            return this.each(function() {
                if (selector == undefined) var even = "tr:even,li:even", odd = "tr:odd,li:odd";
                else var even = selector + ":even", odd = selector + ":odd";
                $(this).children(even).addClass("t-alt even").removeClass("odd");
                $(this).children(odd).addClass("odd").removeClass("t-alt even");
            })
        }
    })
})(jQuery);