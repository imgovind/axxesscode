(function($) {
    $.extend($.fn, {
        ReturnComments: function(Arguments) {
            return this.each(function() {
                if (!$(this).hasClass("return-comments-container")) {
                    var Container = $(this), ButtonArray = new Array();
                    if (typeof Arguments.ReturnFunction == "function") ButtonArray.push({
                        Text: "Return",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("textarea", Container).val().length) {
                                U.PostUrl("Order/AddReturnReason", { agencyId: Arguments.AgencyId, orderId: Arguments.EventId, episodeId: Arguments.EpisodeId, patientId: Arguments.PatientId, comment: $("textarea", Container).val() }, function(Result) {
                                    U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                    if (Result.isSuccessful) {
                                        Arguments.ReturnFunction();
//                                        Agency.RebindCaseManagement();
//                                        Patient.Rebind();
//                                        Schedule.Rebind();
//                                        User.RebindScheduleList();
                                        Container.closest(".window").Close();
                                    }
                                })
                            } else U.Growl("Please give a reason for returning this document.", "error");
                        }
                    });
                    else ButtonArray.push({
                        Text: "Add Comment",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("textarea", Container).val().length) {
                                U.PostUrl("Order/AddReturnReason", { agencyId: Arguments.AgencyId, orderId: Arguments.EventId, episodeId: Arguments.EpisodeId, patientId: Arguments.PatientId, comment: $("textarea", Container).val() }, function(Result) {
                                    U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                    if (Result.isSuccessful) {
                                        Container.ReturnComments("Refresh");
                                        $("textarea", Container).val("");
//                                        Agency.RebindCaseManagement();
//                                        Patient.Rebind();
//                                        Schedule.Rebind();
                                        //                                        User.RebindScheduleList();
                                        Container.closest(".window").Close();
                                    }
                                })
                            } else U.Growl("Please enter your comments that you would like to add.", "error");
                        }
                    });
                    if (Container.hasClass("main")) ButtonArray.push({
                        Text: "Close",
                        Click: function() {
                            var Container = $(this).closest(".return-comments-container");
                            if ($("#print-controls").length) $("ul", "#print-controls").show();
                            Container.closest(".window").Close();
                        }
                    });
                    Arguments.ButtonText = "Refresh";
                    Container.addClass("return-comments-container").attr({ EventId: Arguments.EventId, EpisodeId: Arguments.EpisodeId, PatientId: Arguments.PatientId }).append(
                        $("<div/>", { "class": "return-comments" })).append(
                        $("<textarea/>", { "class": "tall" })).append(
                        $("<div/>").Buttons(ButtonArray)).ReturnComments(Arguments);
                } else if (Arguments.ButtonText == "Refresh") {
                    var Container = $(this);
                    U.PostUrl("Order/ReturnReason", { agencyId: Arguments.AgencyId, orderId: Arguments.EventId, episodeId: Arguments.EpisodeId, patientId: Arguments.PatientId }, function(Result) {
                        $(".return-comments", Container).html(Result.errorMessage).scrollTop($(".return-comments", Container).prop("scrollHeight")); ;
                        $(".return-comments .edit-controls", Container).each(function() {
                            var CommentId = $(this).text(), Element = $(this).closest(".main");
                            $(this).html(
                                $("<span/>").addClass("img icon edit-comment").click(function() {
                                    UserInterface.EditReturnComments(CommentId, $(this).parent().next().next().next().text(), Element)
                                })
                            ).append(
                                $("<span/>").addClass("img icon delete-comment").click(function() {
                                    if (confirm("Are you sure you want to delete this comment?")) U.PostUrl("Schedule/DeleteReturnReason", { id: CommentId }, function(Result) {
                                        U.Growl(Result.errorMessage, Result.isSuccessful ? "success" : "error");
                                        if (Result.isSuccessful) Container.ReturnComments("Refresh");
                                    })
                                })
                            )
                        })
                    })
                }
            })
        }
    })
})(jQuery);