﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Trim() %>&#8217;s Signed Orders</span>
<fieldset class="orders-filter align-center">
    <div class="buttons float-right">
        <ul>
            <li><a href="javascript:void(0);" onclick="Order.GetCompletedList();">Generate</a></li>
        </ul>
    </div>
    <label class="float-left">Agency:</label>
    <div class="" style="display:inline-block">
    <%=Html.LookupSelectList("agencies", "CompleteList_AgencyId", Current.DefaultAgencyId.ToString(), new { @id = "CompleteList_AgencyId", @class = "" })%>
    </div>
    <div class="clear" />
    <label class="float-left">Date Range:</label>
    <input type="text" name="StartDate" class="date-picker shortdate" value="<%= DateTime.Now.AddDays(-60).ToShortDateString() %>" id="CompleteOrder_StartDate" />
    <input type="text" name="EndDate" class="date-picker shortdate" value="<%= DateTime.Now.ToShortDateString() %>" id="CompleteOrder_EndDate" />
</fieldset>
<div class="wrapper">
    <%= Html.Telerik().Grid<Order>().Name("List_CompleteOrders").HtmlAttributes(new { style = "top:72px;" }).Columns(columns =>
{
    columns.Bound(o => o.Number).Title("Order #").Sortable(true).Width(10);
    columns.Bound(o => o.TypeDescription).Title("Document").Sortable(true).Width(25);
    columns.Bound(o => o.AgencyName).Title("Agency").Sortable(true).Width(25);
    columns.Bound(o => o.PatientName).Title("Patient").Sortable(true).Width(25);
    columns.Bound(o => o.OrderDate).Title("Order Date").Sortable(true).Width(10);
    columns.Bound(o => o.PrintUrl).Title(" ").Width(3);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("CompleteGrid", "Order", new { agencyId=Current.DefaultAgencyId, startDate=DateTime.Now.AddDays(-60).ToShortDateString(), endDate=DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>