﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
    $("body").AcoreDesktop();
    Acore.AddMenu({ Name: "Create", Id: "create", IconX: "140", IconY: "108" });
    Acore.AddMenu({ Name: "View", Id: "view", IconX: "161", IconY: "108" });

    Acore.AddWindow({ Name: "Orders Pending Review", Id: "listorders", Url: "Order/List", Menu: "view" });
    Acore.AddWindow({ Name: "Face-To-Face Encounters", Id: "listfacetoface", Url: "Order/FaceToFaceList", Menu: "view" });
    Acore.AddWindow({ Name: "Orders Completed", Id: "listpastorders", Url: "Order/CompleteList", Menu: "view" });
    Acore.AddWindow({ Name: "Face-To-Face Encounter", Id: "faceToFace", Url: "Order/FaceToFaceEncounter" });
    
    //Acore.AddMenu({ Name: "Care Plan Oversight", Id: "cpo", IconX: "153", IconY: "87" });
    Acore.AddWindow({ Name: "Care Plan Oversight Log", Id: "newcareplanoversight", Url: "Order/CPO/New", Menu: "create" });
    Acore.AddWindow({ Name: "Care Plan Oversight Logs", Id: "listcpo", Url: "Order/CPO/List", Menu: "view" });
    Acore.AddWindow({ Name: "Edit Care Plan Oversight Log", Id: "editcpo" });
    $("ul#mainmenu").superfish();

    Acore.Open("listorders");
</script>

