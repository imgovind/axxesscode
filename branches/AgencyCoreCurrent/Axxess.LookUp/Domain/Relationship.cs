﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class Relationship
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
