﻿namespace Axxess.LookUp.Domain
{
    public interface ICbsaCode
    {
        int Id { get; set; }
        string CBSA { get; set; }
        string StateCode { get; set; }
        string StateName { get; set; }
        double WITwoSeven { get; set; }
        double WITwoEight { get; set; }
        double WITwoNine { get; set; }
        double WITwoTen { get; set; }
        double WITwoEleven { get; set; }
        double WITwoTwelve { get; set; }
        double WITwoThirteen { get; set; }
        double WITwoFourteen { get; set; }
    }
}
