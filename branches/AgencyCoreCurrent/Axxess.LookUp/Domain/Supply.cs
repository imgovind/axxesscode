﻿
namespace Axxess.LookUp.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;

    [KnownType(typeof(Supply))]
    [XmlRoot()]
    public class Supply
    {
        public Supply() { this.Quantity = 1; }
        [XmlElement]
        public int Id { get; set; }
        [XmlElement]
        [SubSonicIgnore]
        public string RevenueCode { get; set; }
        [XmlElement]
        public string Code { get; set; }
        [XmlElement]
        public string Description { get; set; }
        [XmlElement]
        public int CategoryId { get; set; }
        [XmlElement]
        [SubSonicIgnore]
        public int Quantity { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public string Date { get; set; }

        [UIHint("Date")]
        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime DateForEdit { 
            get
            { 
                return this.Date.IsNotNullOrEmpty() && this.Date.IsValidDate() ? this.Date.ToDateTime() : DateTime.Today; 
            }
            set
            {
                this.Date = value.ToString("MM/dd/yyyy");
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string IdCheckbox
        {
            get
            {
                return string.Format("<input name='BillingId' type='checkbox'  value='{0}' />", this.BillingId);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Url
        {
            get
            {
                return null;
            }
        }

        [XmlElement]
        [SubSonicIgnore]
        public string Modifier { get; set; }

        [UIHint("Guid")]
        [XmlElement]
        [SubSonicIgnore]
        public Guid UniqueIdentifier { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public bool IsBillable { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public bool IsDeprecated { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public int BillingId { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public double UnitCost { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public double Total { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public bool IsAddedFromBilling { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public double TotalCost { get { return this.UnitCost * this.Quantity; } }
    }
}
