﻿namespace Axxess.LookUp.Repositories
{
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class LookUpDataProvider : ILookUpDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public LookUpDataProvider()
        {
            this.database = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        #region ILookUpDataContext Members

        private ILookupRepository lookUpRepository;
        public ILookupRepository LookUpRepository
        {
            get
            {
                if (lookUpRepository == null)
                {
                    lookUpRepository = new LookupRepository(this.database);
                }
                return lookUpRepository;
            }
        }

        #endregion
    }
}
