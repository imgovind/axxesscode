﻿namespace Axxess.Scheduled.Eligibility.Service
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Security.Cryptography.X509Certificates;

    using Axxess.Core.Extension;

    using Extensions;

    public static class AbilityService
    {
        #region Public Methods

        public static hiqhRequest CreateHiqhRequest(bool isMockRequest)
        {
            return new hiqhRequest
            {
                mockResponse = isMockRequest ? "true" : "false",
                details = new hiqhRequestDetails[1]
                {
                    new hiqhRequestDetails 
                    { 
                        detail = "ALL"
                    }
                },
                medicareMainframe = new hiqhRequestMedicareMainframe[1]
                {
                   new hiqhRequestMedicareMainframe
                   {
                       application = new hiqhRequestMedicareMainframeApplication[1]
                       {
                           new hiqhRequestMedicareMainframeApplication
                           {
                               name = "DDE",
                               pptnRegion = "",
                               dataCenter = "CDS",
                               appId = "ACPFA391",
                               facilityState = "TX",
                               lineOfBusiness = "PartA"
                           }
                       },
                       credential = new hiqhRequestMedicareMainframeCredential[1]
                       {
                           new hiqhRequestMedicareMainframeCredential
                           {
                               userId = "we72165",
                               password = "$9Cheeks"
                           }
                       }
                   }
                }
            };
        }

        public static hiqhRequestSearchCriteria[] CreateSearchCriteria(string hic, string lastName, string firstInitial, string dateOfBirth, string sex, string providerId)
        {
            return new hiqhRequestSearchCriteria[1]
            {
                new hiqhRequestSearchCriteria
                {
                    hic = hic,
                    sex = sex,
                    requestorId = "1",
                    lastName = lastName,
                    dateOfBirth = dateOfBirth,
                    firstInitial = firstInitial,
                    intermediaryNumber = "11004",
                    providerId = providerId
                }
            };
        }

        public static hiqhResponse GetHiqhResponse(hiqhRequest request)
        {
            hiqhResponse response = null;

            string[] hostList = { "SW", "SE", "GL", "GW", "KS", "MA", "PA", "NE", "SO" };

            foreach (var host in hostList)
            {
                try
                {
                    response = SendHiqhRequest(request.UseHost(host).ToXml());
                    if (response != null)
                    {
                        break;
                    }
                }
                catch (WebException webEx)
                {
                    using (var responseStream = new StreamReader(webEx.Response.GetResponseStream()))
                    {
                        var responseText = responseStream.ReadToEnd();
                        Console.WriteLine("ResponseText: {0}", responseText);
                    }
                    Console.WriteLine("WebException: {0}", webEx.ToString());
                }

            }

            return response;
        }

        #endregion

        #region Private Members and Methods

        private static string[] UnwantedNodes = 
        {
            "preventativeServices",
            "smokingCessation",
            "rehabilitationSessions",
            "telehealthServices",
            "behavioralServices",
            "highIntensityBehavioralCounseling"
        };
        private static hiqhResponse SendHiqhRequest(string hiqhRequestXml)
        {
            hiqhResponse response = null;

            var encoding = new ASCIIEncoding();
            byte[] requestData = encoding.GetBytes(hiqhRequestXml);
            string eligibilityUrl = @"https://access.abilitynetwork.com:443/access/cwf/hiqh/";
            Uri eligibilityUri = new Uri(eligibilityUrl, UriKind.Absolute);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(eligibilityUri);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "text/xml";
            httpWebRequest.Headers.Add("X-Access-Version", "1");
            httpWebRequest.ContentLength = requestData.Length;

            X509Store certStore = new X509Store(StoreName.My);
            certStore.Open(OpenFlags.OpenExistingOnly & OpenFlags.ReadOnly);
            X509Certificate certificate = certStore.Certificates.Get("Adeniyi.Olajide@012");

            httpWebRequest.ClientCertificates.Add(certificate);

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
            }

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var responseStream = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    XDocument xDocument = XDocument.Load(responseStream);
                    var unwantedNodes = from node in xDocument.Descendants()
                            where UnwantedNodes.Contains(node.Name.LocalName)
                            select node;

                    unwantedNodes.ToList().ForEach(x => x.Remove());

                    
                    //var responseText = responseStream.ReadToEnd();
                    //if (responseText.IsNotNullOrEmpty())
                    //{
                    //    response = responseText.ToObject<hiqhResponse>();

                    //}
                }
            }

            return response;
        }

        #endregion
    }
}
