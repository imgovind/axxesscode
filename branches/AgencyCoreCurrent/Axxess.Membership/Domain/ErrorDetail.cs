﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Text;
    using Axxess.Core.Extension;

    public class ErrorDetail
    {
        public string Text { get; set; }
        public int Impression { get; set; }
        public string UserName { get; set; }
        public string InnerError { get; set; }
        public string ExceptionType { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                .AppendFormat("Text: {0}<br />", this.Text)
                .AppendFormat("Impressions: {0}<br />", this.Impression)
                .AppendFormat("UserName: {0}<br />", this.UserName)
                .AppendFormat("InnerError: {0}<br />", this.InnerError)
                .AppendFormat("ExceptionType: {0}", this.ExceptionType)
                .ToString().Clean();
        }
    }
}
