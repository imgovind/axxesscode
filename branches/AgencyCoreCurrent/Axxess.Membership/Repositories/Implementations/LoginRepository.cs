﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class LoginRepository : ILoginRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LoginRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ILoginRepository Members

        public bool Add(Login login)
        {
            if (login != null)
            {
                login.Id = Guid.NewGuid();
                login.Created = DateTime.Now;
                database.Add<Login>(login);
                return true;
            }
            return false;
        }

        public Login Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<Login>(l => l.Id == id);
        }

        public Login Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<Login>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(Login login)
        {
            if (login != null)
            {
                database.Update<Login>(login);
                return true;
            }
            return false;
        }

        public bool Delete(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);

            if (login != null)
            {
                login.IsActive = false;
                database.Update<Login>(login);
                result = true;
            }
            return result;
        }

        public ICollection<Login> GetAll()
        {
            return database.All<Login>().ToList().AsReadOnly();
        }

        public IList<Login> GetAllByRole(Roles role)
        {
            return database.Find<Login>(l => l.Role == role.ToString() && l.IsActive == true).ToList();
        }

        public ICollection<Login> AutoComplete(string searchString)
        {
            return database.Find<Login>(u => u.EmailAddress.StartsWith(searchString)).ToList();
        }

        public string GetLoginDisplayName(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.DisplayName;
                }
            }

            return name;
        }

        public string GetLoginEmailAddress(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.EmailAddress;
                }
            }

            return name;
        }

        #endregion

        #region DDE Credentials Members

        public DdeCredential GetDdeCredential(Guid id)
        {
            return database.Single<DdeCredential>(d => d.Id == id);
        }

        public IList<DdeCredential> GetDdeCredentials()
        {
            return database.All<DdeCredential>().ToList();
        }

        public bool UpdateDdeCredential(DdeCredential ddeCredential)
        {
            if (ddeCredential != null)
            {
                database.Update<DdeCredential>(ddeCredential);
                return true;
            }
            return false;
        }

        #endregion
    }
}
