﻿namespace Axxess.Api
{
    using System.Collections.Generic;
    using Axxess.Api.Contracts;
    using System;

    public class EligibilityAgent : BaseAgent<IEligibilityService>
    {
        #region Overrides

        public override string ToString()
        {
            return "EligibilityService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Eligibility Methods

        public PatientEligibility EligibilityCheck(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility PatientEligibility = null;
            BaseAgent<IEligibilityService>.Call(e => PatientEligibility = e.EligibilityCheck(medicareNumber, lastName, firstName, dob, gender), this.ToString());
            return PatientEligibility;
        }

        #endregion
    }
}
