﻿namespace Axxess.Scheduled.HomeHealthGold
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.OasisC.Domain;
    using System.Web.Script.Serialization;
    using System.Net;
    
    class Program
    {
        #region Private Members

        private static Guid AgencyId;
        private static string reportDate;
        private static string oasisOutput;
        private static string servicesOutput;
        private static string admissionsOutput;
        private static TextWriter textWriter = null;
        private static Dictionary<Guid, string> assessmentStatusList = null;
        private static Dictionary<string, string> cbsaLookup = new Dictionary<string, string>();
        private static string AppRoot = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
        private static string output = Path.Combine(AppRoot, string.Format("log_{0}.txt", DateTime.Now.Ticks.ToString()));

        #endregion

        #region Main Methods

        static void Main(string[] args)
        {
            using (textWriter = new StreamWriter(output, true))
            {
                try
                {
                    Bootstrapper.Run();
                    LoopThroughAgencyData();
                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
            }
        }

        private static void Initialize(Guid agencyId)
        {
            AgencyId = agencyId;

            assessmentStatusList = new Dictionary<Guid, string>();

            reportDate = DateTime.Now.ToString("yyyy/MM/dd");

            oasisOutput = "oasis.txt";
            servicesOutput = "services.txt";
            admissionsOutput = "admissions.txt";
        }

        private static void LoopThroughAgencyData()
        {
            var customers = Database.GetCustomers();
            if (customers != null && customers.Count > 0)
            {
                customers.ForEach(customer =>
                {
                    Initialize(customer.AgencyId);

                    var agency = Database.GetAgency(AgencyId);
                    if (agency != null)
                    {
                        var users = Database.GetAgencyUsers(AgencyId);
                        var patients = Database.GetAgencyPatients(AgencyId);
                        var physicians = Database.GetAgencyPhysicians(AgencyId);
                        if (patients != null && patients.Count > 0)
                        {
                            var oasisBuilder = new StringBuilder();
                            var servicesBuilder = new StringBuilder();
                            var admissionBuilder = new StringBuilder();

                            using (var oasisWriter = new StringWriter(oasisBuilder))
                            {
                                using (TextWriter servicesWriter = new StringWriter(servicesBuilder))
                                {
                                    using (TextWriter admissionDataWriter = new StringWriter(admissionBuilder))
                                    {
                                        int admissionCounter = 1;
                                        patients.ForEach(patient =>
                                        {
                                            if (patient != null)
                                            {
                                                var patientAdmissions = Database.GetPatientAdmissions(AgencyId, patient.Id);
                                                if (patientAdmissions != null && patientAdmissions.Count > 0)
                                                {
                                                    patientAdmissions.ForEach(patientAdmission =>
                                                    {
                                                        var primaryPhysician = Database.GetPatientPhysician(patient.Id);

                                                        var physician = physicians != null
                                                            && primaryPhysician != null
                                                            ? physicians.Find(u => u.Id == primaryPhysician.PhysicianId)
                                                            : null;

                                                        var admissionNumber = GetAdmissionId(patientAdmissions, patient.Id, patient.AdmissionId);

                                                        var clinician = users != null
                                                            && !patient.UserId.IsEmpty()
                                                            ? users.Find(u => u.Id == patient.UserId)
                                                            : null;

                                                        var identity = Database.GetEntityId(patient.Id);
                                                        if (identity != null)
                                                        {
                                                            patient.PatientIdNumber = identity.Id.ToString();
                                                        }
                                                        else
                                                        {
                                                            identity = new HomeHealthGoldIdentity
                                                            {
                                                                EntityId = patient.Id
                                                            };
                                                            if (Database.Add<HomeHealthGoldIdentity>(identity))
                                                            {
                                                                patient.PatientIdNumber = identity.Id.ToString();
                                                            }
                                                        }

                                                        var admissionLine = CreatePatientAdmissionLine(admissionNumber, patient.PatientIdNumber, patientAdmission, clinician, physician);

                                                        var adminBatch = Database.GetDataBatch(patientAdmission.Id, AgencyId, patient.Id);
                                                        if (adminBatch != null)
                                                        {
                                                            if (patientAdmission.IsDeprecated)
                                                            {
                                                                adminBatch.IsDeprecated = true;
                                                                adminBatch.LastModified = patientAdmission.Modified;
                                                                if (Database.Update<HomeHealthGoldBatch>(adminBatch))
                                                                {
                                                                    Console.WriteLine("Deleted Admin Batch");
                                                                    admissionDataWriter.WriteLine("D{0}{1}", adminBatch.Id.ToString().PadRight(20), admissionLine);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (patientAdmission.Modified != adminBatch.LastModified
                                                                        || patientAdmission.Status != adminBatch.Status)
                                                                {
                                                                    adminBatch.Status = patientAdmission.Status;
                                                                    adminBatch.LastModified = patientAdmission.Modified;
                                                                    if (Database.Update<HomeHealthGoldBatch>(adminBatch))
                                                                    {
                                                                        Console.WriteLine("Updated Admin Batch");
                                                                        admissionDataWriter.WriteLine("C{0}{1}", adminBatch.Id.ToString().PadRight(20), admissionLine);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            adminBatch = new HomeHealthGoldBatch
                                                            {
                                                                PatientId = patient.Id,
                                                                AgencyId = customer.AgencyId,
                                                                EntityId = patientAdmission.Id,
                                                                Status = patientAdmission.Status,
                                                                LastModified = patientAdmission.Modified,
                                                                Created = DateTime.Now
                                                            };
                                                            if (Database.Add<HomeHealthGoldBatch>(adminBatch))
                                                            {
                                                                Console.WriteLine("New Admin Batch");
                                                                admissionDataWriter.WriteLine("N{0}{1}", adminBatch.Id.ToString().PadRight(20), admissionLine);
                                                            }
                                                        }

                                                        var admissionEpisodes = Database.GetPatientAdmissionEpisodes(AgencyId, patient.Id, patientAdmission.Id);
                                                        if (admissionEpisodes != null && admissionEpisodes.Count > 0)
                                                        {
                                                            admissionEpisodes.ForEach(admissionEpisode =>
                                                            {
                                                                if (admissionEpisode != null && admissionEpisode.Schedule.IsNotNullOrEmpty())
                                                                {
                                                                    var schedule = admissionEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                                                                    if (schedule != null && schedule.Count > 0)
                                                                    {
                                                                        var billableVisits = schedule.Where(s =>
                                                                            !s.EventId.IsEmpty() && s.VisitDate.IsDate()
                                                                            && !s.IsMissedVisit && s.IsBillable).ToList();
                                                                        if (billableVisits != null && billableVisits.Count > 0)
                                                                        {
                                                                            billableVisits.ForEach(visit =>
                                                                            {
                                                                                var serviceLine = CreateServiceLine(patient.PatientIdNumber, admissionNumber, visit.VisitDate, Clean(visit.DisciplineTaskName));
                                                                                var serviceBatch = Database.GetDataBatch(visit.EventId, AgencyId, patient.Id);
                                                                                if (serviceBatch != null)
                                                                                {
                                                                                    if (visit.IsDeprecated)
                                                                                    {
                                                                                        serviceBatch.IsDeprecated = true;
                                                                                        if (Database.Update<HomeHealthGoldBatch>(serviceBatch))
                                                                                        {
                                                                                            Console.WriteLine("Deleted Service Batch");
                                                                                            servicesWriter.WriteLine("D{0}{1}", serviceBatch.Id.ToString().PadRight(20), serviceLine);
                                                                                            assessmentStatusList.Add(visit.EventId, string.Format("D{0}", serviceBatch.Id.ToString().PadRight(20)));
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (visit.Status != serviceBatch.Status.ToString())
                                                                                        {
                                                                                            serviceBatch.Status = visit.Status.ToInteger();
                                                                                            if (Database.Update<HomeHealthGoldBatch>(serviceBatch))
                                                                                            {
                                                                                                Console.WriteLine("Updated Service Batch");
                                                                                                servicesWriter.WriteLine("C{0}{1}", serviceBatch.Id.ToString().PadRight(20), serviceLine);
                                                                                                assessmentStatusList.Add(visit.EventId, string.Format("C{0}", serviceBatch.Id.ToString().PadRight(20)));
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (!visit.IsDeprecated)
                                                                                    {
                                                                                        serviceBatch = new HomeHealthGoldBatch
                                                                                        {
                                                                                            PatientId = patient.Id,
                                                                                            EntityId = visit.EventId,
                                                                                            AgencyId = customer.AgencyId,
                                                                                            Status = visit.Status.ToInteger(),
                                                                                            LastModified = DateTime.Now,
                                                                                            Created = DateTime.Now
                                                                                        };
                                                                                        if (Database.Add<HomeHealthGoldBatch>(serviceBatch))
                                                                                        {
                                                                                            Console.WriteLine("New Service Batch");
                                                                                            servicesWriter.WriteLine("N{0}{1}", serviceBatch.Id.ToString().PadRight(20), serviceLine);
                                                                                            assessmentStatusList.Add(visit.EventId, string.Format("N{0}", serviceBatch.Id.ToString().PadRight(20)));
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        }

                                                                        var assessmentEvents = schedule.Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate()
                                                                            && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.IsAssessment()
                                                                            && (s.Status.ToInteger() == (int)ScheduleStatus.OasisCompletedExportReady
                                                                            || s.Status.ToInteger() == (int)ScheduleStatus.OasisExported
                                                                            || s.Status.ToInteger() == (int)ScheduleStatus.OasisCompletedNotExported)).ToList();
                                                                        if (assessmentEvents != null && assessmentEvents.Count > 0)
                                                                        {
                                                                            assessmentEvents.ForEach(assessmentVisit =>
                                                                            {
                                                                                var status = assessmentStatusList != null && assessmentStatusList.ContainsKey(assessmentVisit.EventId) ? assessmentStatusList[assessmentVisit.EventId] : string.Empty.PadRight(21);
                                                                                if (status.IsNotNullOrEmpty())
                                                                                {
                                                                                    var assessingClinician = users != null
                                                                                        && !assessmentVisit.UserId.IsEmpty()
                                                                                        ? users.Find(u => u.Id == assessmentVisit.UserId)
                                                                                        : null;
                                                                                    var assessment = Database.GetAssessment(AgencyId, patient.Id, assessmentVisit.EventId, assessmentVisit.GetAssessmentType());
                                                                                    var oasisLine = CreateOasisLine(patient.PatientIdNumber, admissionNumber, assessingClinician, assessment);

                                                                                    Console.WriteLine("Assessment Batch");
                                                                                    oasisWriter.WriteLine("{0}{1}", status, oasisLine);
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        admissionCounter++;
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            if (oasisBuilder.ToString().IsNotNullOrEmpty())
                            {
                                textWriter.WriteLine(oasisBuilder.ToString());
                                var oasisBatchResponse = GetPostReportResponse(AgencyId, oasisOutput, reportDate, oasisBuilder.ToString());
                                if (oasisBatchResponse != null && oasisBatchResponse.result.success)
                                {
                                    Console.WriteLine("OASIS Batch Sent to FTP");
                                }
                            }
                            else
                            {
                                textWriter.WriteLine("OASIS BATCH EMPTY");
                            }

                            if (servicesBuilder.ToString().IsNotNullOrEmpty())
                            {
                                textWriter.WriteLine(servicesBuilder.ToString());
                                var servicesBatchResponse = GetPostReportResponse(AgencyId, servicesOutput, reportDate, servicesBuilder.ToString());
                                if (servicesBatchResponse != null && servicesBatchResponse.result.success)
                                {
                                    Console.WriteLine("Services Batch Sent to FTP");
                                }
                            }
                            else
                            {
                                textWriter.WriteLine("SERVICES BATCH EMPTY");
                            }

                            if (admissionBuilder.ToString().IsNotNullOrEmpty())
                            {
                                textWriter.WriteLine(admissionBuilder.ToString());
                                var admissionBatchResponse = GetPostReportResponse(AgencyId, admissionsOutput, reportDate, admissionBuilder.ToString());
                                if (admissionBatchResponse != null && admissionBatchResponse.result.success)
                                {
                                    Console.WriteLine("Admission Batch Sent to FTP");
                                }
                            }
                            else
                            {
                                textWriter.WriteLine("ADMISSION BATCH EMPTY");
                            }

                        }
                    }
                });
            }
        }

        #endregion

        #region Helper Functions

        private static string CreatePatientAdmissionLine(string patientId, string admissionNumber, PatientAdmissionDate patientAdmission, User clinician, AgencyPhysician physician)
        {
            if (patientAdmission != null && patientAdmission.PatientData.IsNotNullOrEmpty())
            {
                var patient = patientAdmission.PatientData.ToObject<Patient>();
                if (patient != null)
                {
                    var cbsaCode = GetCbsaCode(patient.AddressZipCode);
                    var admissionSource = patient.AdmissionSource.IsNotNullOrEmpty() && !patient.AdmissionSource.IsEqual("0") ? patient.AdmissionSource.ToInteger().ToEnum<AdmissionSource>(AdmissionSource.Source_1).GetDescription().PartOfString(0, 25).PadRight(25) : string.Empty.PadRight(25);

                    return new StringBuilder(185)
                        .Append(patientId.IsNotNullOrEmpty() ? patientId.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(admissionNumber.IsNotNullOrEmpty() ? admissionNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(patient.LastName.IsNotNullOrEmpty() ? patient.LastName.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(string.Empty.PadRight(25))
                        .Append((clinician != null ? clinician.DisplayName : string.Empty).Trim().PadRight(25).PartOfString(0, 25))
                        .Append((physician != null ? physician.DisplayName : string.Empty).Trim().PadRight(25).PartOfString(0, 25))
                        .Append(cbsaCode.Trim().PadRight(5).PartOfString(0, 5))
                        .Append(admissionSource)
                        .ToString();
                }
            }
            return string.Empty;
        }

        private static string CreateServiceLine(string patientId, string admissionNumber, string visitDate, string taskName)
        {
            return new StringBuilder(80)
                        .Append(patientId.IsNotNullOrEmpty() ? patientId.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(admissionNumber.IsNotNullOrEmpty() ? admissionNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(visitDate.IsNotNullOrEmpty() && visitDate.IsDate() ? visitDate.ToZeroFilled().Trim().PadRight(10).PartOfString(0, 10) : string.Empty.PadRight(10))
                        .Append(taskName.IsNotNullOrEmpty() ? taskName.ToUpperCase().Trim().PadRight(30).PartOfString(0, 30) : string.Empty.PadRight(30))
                        .ToString();
        }

        private static string CreateOasisLine(string patientId, string admissionNumber, User clinician, Assessment assessment)
        {
            return new StringBuilder(1513)
                        .Append(patientId.IsNotNullOrEmpty() ? patientId.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append(admissionNumber.IsNotNullOrEmpty() ? admissionNumber.Trim().PadRight(20).PartOfString(0, 20) : string.Empty.PadRight(20))
                        .Append((clinician != null ? clinician.DisplayName : string.Empty).Trim().PadRight(25).PartOfString(0, 25))
                        .Append((assessment != null && assessment.SubmissionFormat.IsNotNullOrEmpty() ? assessment.SubmissionFormat : string.Empty).Trim().PadRight(1448).PartOfString(0, 1448))
                        .ToString();
        }

        private static string GetAdmissionId(List<PatientAdmissionDate> patientAdmissions, Guid patientId, Guid patientAdmissionId)
        {
            string admissionNumber = string.Empty;
            if (patientAdmissions != null && patientAdmissions.Count > 0)
            {
                int index = patientAdmissions.FindIndex(p => p.Id == patientAdmissionId);
                if (index >= 0)
                {
                    admissionNumber = (index + 1).ToString().PadLeft(4, '0');
                }
            }

            return admissionNumber;
        }

        private static string GetCbsaCode(string zipCode)
        {
            var cbsa = string.Empty;
            if (zipCode.IsNotNullOrEmpty())
            {
                if (cbsaLookup.ContainsKey(zipCode))
                {
                    cbsa = cbsaLookup[zipCode];
                }
                else
                {
                    var cbsaCode = Database.GetCbsaCode(zipCode);
                    if (cbsaCode != null && cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        cbsa = cbsaCode.CBSA;
                        cbsaLookup.Add(zipCode, cbsa);
                    }
                }
            }
            return cbsa;
        }

        private static string Clean(string taskName)
        {
            var text = taskName;
            if (taskName.IsNotNullOrEmpty())
            {
                if (taskName.ToLowerInvariant().StartsWith("oasis", StringComparison.CurrentCultureIgnoreCase)
                    && !taskName.ToLowerInvariant().Contains("(pt)")
                    && !taskName.ToLowerInvariant().Contains("(ot)"))
                {
                    text = string.Format("SN {0}", taskName);
                }
            }
            return text;
        }

        private static RegisterAgencyResponse GetRegisterAgencyResponse(Guid agencyId, string password)
        {
            RegisterAgencyResponse result = null;
            var requestUrl = string.Format("http://sftpservice.axxessweb.com?action=registerAgency&agencyId={0}&password={1}", agencyId, password);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    var json = streamReader.ReadToEnd();
                    textWriter.WriteLine(json);
                    result = json.FromJson<RegisterAgencyResponse>();
                }
            }
            else
            {
                textWriter.WriteLine("Failure Response Code: {0}: ", httpWebResponse.StatusCode);
            }
            return result;
        }

        private static PostReportResponse GetPostReportResponse(Guid agencyId, string fileName, string reportDate, string content)
        {
            PostReportResponse result = null;

            ASCIIEncoding encoding = new ASCIIEncoding();
            string postData = string.Format("action=postReport&agencyId={0}&filename={1}&reportDate={2}&data={3}", agencyId, fileName, reportDate, content);
            byte[] requestData = encoding.GetBytes(postData);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://sftpservice.axxessweb.com");
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = requestData.Length;

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
                requestStream.Close();
            }

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    var json = streamReader.ReadToEnd();
                    textWriter.WriteLine(json);
                    result = json.FromJson<PostReportResponse>();
                }
            }
            else
            {
                textWriter.WriteLine("Failure Response Code: {0}: ", httpWebResponse.StatusCode);
            }
            return result;
        }

        #endregion
    }
}
