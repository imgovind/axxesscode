﻿namespace Axxess.Api.Membership
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;
    using System.IO;

    public class AuthenticationService : BaseService, IAuthenticationService
    {
        #region AgencyCore Members

        public int Count()
        {
            return ActivityMonitor.Instance.Count;
        }

        public List<SingleUser> ToList()
        {
            return ActivityMonitor.Instance.AsList();
        }

        public void SignIn(SingleUser user)
        {
            ActivityMonitor.Instance.Add(user);
        }

        public void Log(SingleUser user)
        {
            ActivityMonitor.Instance.Log(user);
        }

        public SingleUser Get(Guid loginId)
        {
            return ActivityMonitor.Instance.Get(loginId);
        }

        public void SignOut(string emailAddress)
        {
            ActivityMonitor.Instance.Expire(emailAddress);
        }

        public bool Verify(SingleUser user)
        {
            var result = false;
            if (user != null)
            {
                var existingUser = ActivityMonitor.Instance.Get(user.LoginId);
                if (existingUser != null)
                { 
                    if (existingUser.IsAuthenticated)
                    {
                        if (existingUser.SessionId.IsEqual(user.SessionId))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        existingUser.FullName = user.FullName;
                        existingUser.SessionId = user.SessionId;
                        existingUser.AgencyName = user.AgencyName;
                        existingUser.EmailAddress = user.EmailAddress;
                        existingUser.ServerName = user.ServerName;
                        existingUser.LastSecureActivity = DateTime.Now;
                        existingUser.IsAuthenticated = user.IsAuthenticated;
                        result = true;
                    }
                }
                else
                {
                    ActivityMonitor.Instance.Add(user);
                    result = true;
                }
            }
            return result;
        }

        public bool Authorize(string sessionId)
        {
            var result = false;
            try
            {
                result = ActivityMonitor.Instance.IsAuthenticated(sessionId);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }
            return result;
        }

        public WebinarUser Webinar(string sessionId)
        {
            WebinarUser result = null;
            try
            {
                var user = ActivityMonitor.Instance.Get(u => u.SessionId == sessionId);
                if (user != null)
                {
                    var nameArray = user.FullName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    var lastName = string.Empty;
                    var firstName = string.Empty;
                    if (nameArray != null && nameArray.Length >= 2)
                    {
                        firstName = nameArray[0];
                        lastName = nameArray[1];
                    }

                    var webinarUser = new WebinarUser
                    {
                        LastName = lastName,
                        FirstName = firstName,
                        AgencyName = user.AgencyName,
                        EmailAddress = user.EmailAddress
                    };
                    result = webinarUser;
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }
            return result;
        }

        #endregion

        #region Support Members

        public List<SupportUser> ToSupportList()
        {
            return SupportMonitor.Instance.AsList();
        }

        public void SignInSupport(SupportUser user)
        {
            SupportMonitor.Instance.Add(user);
        }

        public void LogSupportUser(SupportUser user)
        {
            SupportMonitor.Instance.Log(user);
        }

        public SupportUser GetSupportUser(Guid loginId)
        {
            return SupportMonitor.Instance.Get(loginId);
        }

        public void SignOutSupport(string emailAddress)
        {
            SupportMonitor.Instance.Expire(emailAddress);
        }

        public bool VerifySupportuser(SupportUser user)
        {
            var result = false;
            if (user != null)
            {
                var existingUser = SupportMonitor.Instance.Get(user.LoginId);
                if (existingUser != null)
                {
                    result = true;
                    existingUser.SessionId = user.SessionId;
                    existingUser.IpAddress = user.IpAddress;
                    existingUser.ServerName = user.ServerName;
                    existingUser.DisplayName = user.DisplayName;
                    existingUser.EmailAddress = user.EmailAddress;
                    existingUser.LastSecureActivity = DateTime.Now;
                    existingUser.IsAuthenticated = user.IsAuthenticated;
                }
                else
                {
                    SupportMonitor.Instance.Add(user);
                    result = true;
                }
            }
            return result;
        }

        public LoginResult AuthorizeSupport(string sessionId)
        {
            var result = new LoginResult { EmailAddress = string.Empty };
            try
            {
                Windows.EventLog.WriteEntry(SupportMonitor.Instance.Count.ToString(), System.Diagnostics.EventLogEntryType.Information);

                var singleUser = SupportMonitor.Instance.Get(a => a.SessionId == sessionId);
                if (singleUser != null && singleUser.IsAuthenticated)
                {
                    result.IsAuthenticated = true;
                    result.EmailAddress = singleUser.EmailAddress;
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }
            return result;
        }

        #endregion
    }
}