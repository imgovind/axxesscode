﻿namespace Axxess.Accounts.Components
{
    using System;

    public interface IMembershipProvider
    {
        void LogOff(string identityName);
        bool VerifyPassword(string password, string passwordHash, string passwordSalt);
    }
}
