﻿namespace Axxess.Accounts.Components
{
    public interface IFormsAuthenticationService
    {
        string LoginUrl { get; }

        void SignOut();
        void RedirectToLogin();
        void SignIn(string userName, bool rememberMe);
    }
}