﻿namespace Axxess.Accounts.Components
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Framework.Extensibility;

    public sealed class BusinessFactory
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.Initialize();
            }

            internal static readonly BusinessFactory instance = new BusinessFactory();
        }

        #endregion

        #region Private Members

        private List<string> register;

        #endregion

        #region Public Instance

        public static BusinessFactory Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private BusinessFactory() { }

        private void Initialize()
        {
            if (register == null)
            {
                register = new List<string>();
            }
        }

        public T Get<T>(params object[] args) where T : class
        {
            T instance = null;
            var typeName = typeof(T).Name;
            if (register.Contains(typeName))
            {
                instance = Container.Resolve<T>(typeName);
            }
            else
            {
                var abstractType = typeof(T);
                var types = AppDomain.CurrentDomain.GetAssemblies().ToList()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => p.IsClass && !p.IsAbstract && abstractType.IsAssignableFrom(p));
                var concreteType = types.FirstOrDefault();
                if (concreteType == null)
                {
                    throw new InvalidOperationException(String.Format("No implementation of {0} was found", abstractType));
                }
                instance = Activator.CreateInstance(concreteType, args) as T;
                register.Add(typeName);
                Container.Inject<T>(typeName, instance);
            }

            return instance;
        }

        #endregion
    }
}
