﻿namespace Axxess.Accounts.Components
{
    using System;

    using Axxess.Framework.Extensions;
    using Axxess.Framework.Extensibility;
    using Axxess.Framework.Configuration;

    public static class ApplicationSettings
    {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static string RememberMeCookie { get { return configuration.AppSettings["RememberMeCookie"]; } }
        public static string AuthenticationType { get { return configuration.AppSettings["AuthenticationType"]; } }
        public static string IdentifyAgencyCookie { get { return configuration.AppSettings["IdentifyAgencyCookie"]; } }
        public static string AgencyHomeSiteUrlFormat { get { return configuration.AppSettings["AgencyHomeSiteUrlFormat"]; } }
        public static bool UsePersistentCookies { get { return configuration.AppSettings["UsePersistentCookies"].ToBoolean(); } }
        public static int RememberMeForTheseDays { get { return configuration.AppSettings["RememberMeForTheseDays"].ToInteger(); } }
        public static bool BrowserCompatibilityCheck { get { return configuration.AppSettings["BrowserCompatibilityCheck"].ToBoolean(); } }
        public static double FormsAuthenticationTimeoutInMinutes { get { return configuration.AppSettings["FormsAuthenticationTimeoutInMinutes"].ToDouble(); } }
        public static string[] DoNotEncryptPaths { get { return configuration.AppSettings["DoNotEncryptPaths"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries); } }
        public static string[] DoNotEncryptFiles { get { return configuration.AppSettings["DoNotEncryptFiles"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries); } }
    }
}
