﻿namespace Axxess.Accounts.Components
{
    using System;

    using Axxess.Accounts.Data;

    using Axxess.Framework.Extensions;
    using Axxess.Framework.Cryptography;

    public class MembershipProvider : IMembershipProvider
    {
        #region Private Members/Constructors

        public MembershipProvider() { }

        #endregion

        #region IMembershipProvider Implementation

        public bool VerifyPassword(string password, string passwordHash, string passwordSalt)
        {
            var result = false;
            if (password.IsNotNullOrEmpty() && passwordHash.IsNotNullOrEmpty() && passwordSalt.IsNotNullOrEmpty())
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(password, passwordHash, passwordSalt))
                {
                    result = true;
                }
            }
            return result;
        }

        public void LogOff(string identityName)
        {
        }

        #endregion
    }
}
