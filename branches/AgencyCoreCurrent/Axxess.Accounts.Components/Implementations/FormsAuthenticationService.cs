﻿namespace Axxess.Accounts.Components
{
    using System;
    using System.Web;
    using System.Web.Security;

    using Axxess.Framework;
    using Axxess.Framework.Extensions;
    using Axxess.Framework.Cryptography;

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        #region IFormsAuthenticationService Members

        public string LoginUrl
        {
            get
            {
                return FormsAuthentication.LoginUrl;
            }
        }

        public void SignIn(string name, bool rememberMe)
        {
            FormsAuthentication.Initialize();

            var ticket = new FormsAuthenticationTicket(1, 
                name, 
                DateTime.Now,
                DateTime.Now.AddMinutes(ApplicationSettings.FormsAuthenticationTimeoutInMinutes), 
                ApplicationSettings.UsePersistentCookies, 
                string.Empty
            );

            string encryptedTicket = FormsAuthentication.Encrypt(ticket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            authCookie.Domain = FormsAuthentication.CookieDomain;
            HttpContext.Current.Response.Cookies.Set(authCookie);

            if (rememberMe)
            {
                var cookie = HttpContext.Current.Request.Cookies[ApplicationSettings.RememberMeCookie];
                if (cookie != null && name.IsEqual(Crypto.Decrypt(cookie.Value)))
                {
                    cookie.Expires = DateTime.Now.AddDays(ApplicationSettings.RememberMeForTheseDays);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
                else
                {
                    HttpCookie newCookie = new HttpCookie(ApplicationSettings.RememberMeCookie);
                    newCookie.Expires = DateTime.Now.AddDays(ApplicationSettings.RememberMeForTheseDays);
                    newCookie.Value = Crypto.Encrypt(name);
                    HttpContext.Current.Response.Cookies.Add(newCookie);
                }
            }
            else
            {
                var cookie = HttpContext.Current.Request.Cookies[ApplicationSettings.RememberMeCookie];
                if (cookie != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
            }
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();

            HttpContext.Current.Session.Abandon();

            // clear authentication cookie
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                authCookie.Domain = FormsAuthentication.CookieDomain;
                authCookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Set(authCookie);
            }

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            var sessionCookie = HttpContext.Current.Request.Cookies["ASP.NET_SessionId"];
            if (sessionCookie != null)
            {
                sessionCookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Set(sessionCookie);
            }
        }

        #endregion
    }
}