﻿namespace Axxess.Business.Components
{
    using System;

    using Entities;

    using Axxess.Data;

    public static class Components
    {
        public static void Initialize()
        {
            EntityMap.Register();
            DataRegister.Initialize();
        }
    }
}