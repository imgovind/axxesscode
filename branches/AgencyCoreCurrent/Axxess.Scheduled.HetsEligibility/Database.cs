﻿namespace Axxess.Scheduled.AbilityEligibility
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    
    using SubSonic.Repository;

    public static class Database
    {
        #region Private Members

        private static readonly SimpleRepository database = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);

        private const string SELECT_AGENCIES = @"SELECT Id, Name, MedicareProviderNumber, NationalProviderNumber FROM `agencies` WHERE IsDeprecated = 0;";
        private const string SELECT_MEDICARE_PATIENTS = @"SELECT p.Id, p.AddressStateCode, p.PrimaryInsurance, p.SecondaryInsurance, p.TertiaryInsurance, p.MedicareNumber, p.PatientIdNumber, p.FirstName, p.LastName, p.MiddleInitial, p.DOB, p.Gender, p.LastEligibilityCheck FROM patients p WHERE (p.PrimaryInsurance = 1 OR p.PrimaryInsurance = 2 OR p.PrimaryInsurance = 3 OR p.PrimaryInsurance = 4) AND p.AgencyId = @agencyid AND p.IsDeprecated = 0 AND p.Status = 1;";

        #endregion

        #region Internal Methods

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                database.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                database.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<Agency> GetAgencies()
        {
            var list = new List<Agency>();

            using (var cmd = new FluentCommand<Agency>(SELECT_AGENCIES))
            {
                list = cmd
                    .SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Agency
                    {
                        Id = reader.GetGuid("Id"),
                        Name = reader.GetString("Name"),
                        MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber")
                    })
                    .AsList();
            }
            return list;
        }

        public static List<Patient> GetPatients(Guid agencyId)
        {
            var patientList = new List<Patient>();

            using (var cmd = new FluentCommand<Patient>(SELECT_MEDICARE_PATIENTS))
            {
                patientList = cmd
                    .SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new Patient
                    {
                        AgencyId = agencyId,
                        Id = reader.GetGuid("Id"),
                        Gender = reader.GetString("Gender"),
                        LastName = reader.GetString("LastName"),
                        DOB = reader.GetDateTime("DOB"),
                        FirstName = reader.GetString("FirstName"),
                        MiddleInitial = reader.GetStringNullable("MiddleInitial"),
                        PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                        SecondaryInsurance = reader.GetStringNullable("SecondaryInsurance"),
                        TertiaryInsurance = reader.GetStringNullable("TertiaryInsurance"),
                        MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        LastEligibilityCheck = reader.GetDateTime("LastEligibilityCheck"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode")
                    }).AsList();
            }

            return patientList;
        }

        public static PatientEpisode GetCurrentPatientEpisode(Guid agencyId, Guid patientId)
        {
            PatientEpisode result = null;
            var sql = @"SELECT *
                        FROM    
                                patientepisodes 
                        WHERE 
                                patientepisodes.AgencyId = @agencyid AND
                                patientepisodes.PatientId = @patientid AND
                                patientepisodes.IsActive = 1 AND 
                                patientepisodes.IsDischarged = 0 AND 
                                CURDATE() BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate LIMIT 0,1";

            using (var cmd = new FluentCommand<PatientEpisode>(sql))
            {
                result = cmd
                            .SetConnection("AgencyManagementConnectionString")
                            .AddGuid("agencyid", agencyId)
                            .AddGuid("patientid", patientId)
                            .AsSingle();
            }

            return result;
        }

        #endregion

    }
}
