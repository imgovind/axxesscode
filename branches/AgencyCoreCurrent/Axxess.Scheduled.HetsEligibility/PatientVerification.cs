﻿namespace Axxess.Scheduled.AbilityEligibility
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Web.Script.Serialization;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public class PatientVerification
    {
        private Patient patient;
        private ManualResetEvent manualEvent;
        private string medicareProviderNumber;

        public string ErrorMessage { get; set; }
        public static string EligibilityVerificationUrl = ConfigurationManager.AppSettings["PatientEligibilityUrl"];

        public PatientVerification(Patient patient, string medicareProviderNumber, ManualResetEvent manualEvent)
        {
            this.patient = patient;
            this.manualEvent = manualEvent;
            this.medicareProviderNumber = medicareProviderNumber;
        }

        public void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = (int)threadContext;
            Console.WriteLine("Starting {0} on thread {1}", this.patient.DisplayName, threadIndex);
            VerifyPatient();
            Console.WriteLine("Ended {0} on thread {1}", this.patient.DisplayName, threadIndex);

            this.manualEvent.Set();
        }

        #region Private Methods

        public void VerifyPatient()
        {
            try
            {
                if (patient.MedicareNumber.IsNotNullOrEmpty() && patient.LastName.IsNotNullOrEmpty() && patient.FirstName.IsNotNullOrEmpty() && patient.Gender.IsNotNullOrEmpty() && patient.DOB.IsValid())
                {
                    bool isHiqh = false;

                    var result = GetHetsResponse();
                    Console.WriteLine("HetsResponse:{0}", result);
                    if (result.IsNotNullOrEmpty())
                    {
                        var newId = Guid.NewGuid();
                        var episode = Database.GetCurrentPatientEpisode(patient.AgencyId, patient.Id);
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleEvents = episode.Schedule.ToObject<List<Axxess.AgencyManagement.Domain.ScheduleEvent>>();
                            var scheduledTask = new Axxess.AgencyManagement.Domain.ScheduleEvent
                            {
                                EventId = newId,
                                UserId = Guid.Empty,
                                UserName = "Axxess",
                                PatientId = patient.Id,
                                EpisodeId = episode.Id,
                                EndDate = episode.EndDate,
                                AgencyId = patient.AgencyId,
                                StartDate = episode.StartDate,
                                EventDate = DateTime.Now.ToShortDateString(),
                                VisitDate = DateTime.Now.ToShortDateString(),
                                Discipline = Disciplines.ReportsAndNotes.ToString(),
                                Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString(),
                                DisciplineTask = (int)DisciplineTasks.MedicareEligibilityReport,
                                IsBillable = false
                            };
                            scheduleEvents.Add(scheduledTask);
                            episode.Schedule = scheduleEvents.ToXml();

                            if (Database.Update<PatientEpisode>(episode))
                            {
                                Console.WriteLine("Schedule Updated.");
                            }
                        }

                        var eligibility = new MedicareEligibility
                        {
                            Id = newId,
                            IsHiqh = isHiqh,
                            IsDeprecated = false,
                            PatientId = patient.Id,
                            Created = DateTime.Now,
                            Result = result,
                            AgencyId = patient.AgencyId,
                            Status = (int)ScheduleStatus.ReportAndNotesCompleted,
                            EpisodeId = episode != null && !episode.Id.IsEmpty() ? episode.Id : Guid.Empty,
                        };

                        if (Database.Add(eligibility))
                        {
                            Console.WriteLine("Result Saved into the Database.");
                            Console.WriteLine("========================================================================");
                        }
                        else
                        {
                            this.ErrorMessage += string.Format("Patient {0} with Medicare Number {1} could not be saved. ", patient.DisplayName, patient.MedicareNumber);
                        }
                    }
                }
                else
                {
                    this.ErrorMessage += string.Format("Patient {0} with Medicare Number {1} Skipped. ", patient.DisplayName, patient.MedicareNumber);
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage += ex.ToString();
            }
        }

        private string GetHetsResponse()
        {
            var jsonResult = string.Empty;
            var jsonData = new
            {
                input_last_name = patient.LastName.IsNotNullOrEmpty() ? patient.LastName.Trim() : "",
                input_medicare_number = patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber.Trim() : "",
                input_first_name = patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.Trim().Substring(0, 1) : "",
                input_date_of_birth = patient.DOB.IsValid() && patient.DOB != DateTime.MinValue ? patient.DOB.ToString("MM/dd/yyyy") : "",
                input_gender_id = patient.Gender.IsNotNullOrEmpty() && patient.Gender.Trim().Length > 0 ? patient.Gender.Trim().Substring(0, 1) : ""
            };

            var javaScriptSerializer = new JavaScriptSerializer();
            var jsonRequest = javaScriptSerializer.Serialize(jsonData);

            ASCIIEncoding encoding = new ASCIIEncoding();
            string postData = ("request=" + jsonRequest);
            byte[] requestData = encoding.GetBytes(postData);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(EligibilityVerificationUrl);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = requestData.Length;

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
                requestStream.Close();
            }

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    jsonResult = streamReader.ReadToEnd();
                }
            }
            return jsonResult;
        }

        #endregion
    }
}
