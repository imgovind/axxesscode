﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<% using (Html.BeginForm("Edit", "Referral", FormMethod.Post, new { @id = "editReferralForm" }))%>
<%  { %>
<%=Html.Hidden("Id", Model.Id, new { @id="txtEdit_ReferralID"})%>
<div id="editReferralValidaton" class="marginBreak" style="display: none">
</div>
<div class="marginBreak">
    <b>Referred By</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="source">
                          Admission Source:</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "ReferralSource", Model.ReferralSource, new { @id = "txtEdit_Referral_ReferralSource", @class = "input_wrapper", @tabindex = "1" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneHome">
                            Referral Date:</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker().Name("ReferralDate").Value(Model.ReferralDate).HtmlAttributes(new { @id = "txtEdit_Referral_Date", @class = "date" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("ReferrerFirstName", Model.ReferrerFirstName, new { @id = "txtEdit_Referrer_FirstName", @class = "text names", @maxlength = "20", @tabindex = "1" })%>
                        </span>
                    </div>
                    <div class="row">
                        <label for="FirstName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("ReferrerLastName", Model.ReferrerLastName, new { @id = "txtEdit_Referrer_LastName", @class = "text names", @maxlength = "20", @tabindex = "1" })%>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Patient Demographics</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row" style="vertical-align: middle;">
                        <label for="FirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("FirstName", Model.FirstName, new { @id = "txtEdit_Referral_FirstName", @class = "text  names" })%>
                        </span>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("LastName", Model.LastName, new { @id = "txtEdit_Referral_LastName", @class = "text names" })%>
                        </span>
                    </div>
                    <div class="row ">
                        <label for="MedicareNo">
                            Medicare No:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("MedicareNo", Model.MedicareNumber, new { @id = "txtEdit_Referral_MedicareNo", @class = "text MedicareNo" })%>
                        </span>
                    </div>
                    <div class="row ">
                        <label for="MedicaidNo">
                            Medicaid No:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("MedicaidNo", Model.MedicaidNumber, new { @id = "txtEdit_Referral_MedicaidNo", @class = "text MedicaidNo" })%>
                        </span>
                    </div>
                    <div class="row ">
                        <label for="MedicaidNo">
                            SSN:&nbsp;&nbsp;&nbsp;</label>
                        <span class="input_wrapper">
                            <%=Html.TextBox("SSN", Model.SSN, new { @id = "txtEdit_Referral_SSN", @class = "text ssn" })%>
                        </span>
                    </div>
                    <div class="row">
                        <label for="DateOfBirth">
                            Date of Birth :</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker().Name("DateOfBirth").Value(Model.DOB).HtmlAttributes(new { @id = "txtEdit_Referral_DateOfBirth", @class = "required date" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Gender">
                            Gender :</label>
                        <div id="referralRadio" class="inputs">
                            <%=Html.RadioButton("Gender", "Female", Model.Gender == "Female" ? true : false, new { @id = "" })%>Female
                            <%=Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "" })%>Male
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="HomePhone">
                            Home Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "txtEdit_Referral_HomePhone1", @class = "autotext required digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "19" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "txtEdit_Referral_HomePhone2", @class = "autotext required digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "20" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "txtEdit_Referral_HomePhone3", @class = "autotext required digits", @style = "width: 51.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "21" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Email">
                            Email:</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%=Html.TextBox("Email", Model.EmailAddress, new { @id = "txtEdit_Referral_Email", @class = "text email" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "txtEdit_Referral_AddressLine1", @class = "text required" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "txtEdit_Referral_AddressLine2", @class = "text" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "txtEdit_Referral_AddressCity", @class = "text required" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State :</label>
                        <div class="inputs">
                            <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "txtEdit_Referral_AddressStateCode", @class = "AddressStateCode selectDropDown input_wrapper", @tabindex = "17" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            Zip Code :</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%=Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "txtEdit_Referral_AddressZipCode", @class = "text numeric required", @size = "5", @maxlength = "5" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AssignedTo">
                            Staff Assigned To:</label>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId != null && Model.UserId != Guid.Empty ? Model.UserId.ToString() : "0", new { @id = "txtEdit_Referral_AssignedTo", @class = "input_wrapper required selectDropDown", @tabindex = "" })%>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="margin">
                <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th colspan='6'>
                                Services Required
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %>
                        <tr>
                            <td>
                                <input type="checkbox" value="0" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("0")  ){ %>checked="checked"<% }%>'" />
                                SNV
                            </td>
                            <td>
                                <input type="checkbox" value="1" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("1")  ){ %>checked="checked"<% }%>'" />
                                HHA
                            </td>
                            <td>
                                <input type="checkbox" value="2" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("2")  ){ %>checked="checked"<% }%>'" />
                                PT
                            </td>
                            <td>
                                <input type="checkbox" value="3" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("3")  ){ %>checked="checked"<% }%>'" />
                                OT
                            </td>
                            <td>
                                <input type="checkbox" value="4" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("4")  ){ %>checked="checked"<% }%>'" />
                                SP
                            </td>
                            <td>
                                <input type="checkbox" value="5" name="ServicesRequiredCollection" '<% if(  servicesRequired!=null && servicesRequired.Contains("5")  ){ %>checked="checked"<% }%>'" />
                                MSW
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="margin">
                <table width="100%" class="agency-data-table" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th colspan='5'>
                                DME Needed
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %>
                        <tr>
                            <td>
                                <input type="checkbox" value="0" name="DMECollection" '<% if(  DME!=null && DME.Contains("0")  ){ %>checked="checked"<% }%>'" />
                                Bedside Commode
                            </td>
                            <td>
                                <input type="checkbox" value="1" name="DMECollection" '<% if(  DME!=null && DME.Contains("1")  ){ %>checked="checked"<% }%>'" />
                                Cane
                            </td>
                            <td>
                                <input type="checkbox" value="2" name="DMECollection" '<% if(  DME!=null && DME.Contains("2")  ){ %>checked="checked"<% }%>'" />
                                Elevated Toilet Seat
                            </td>
                            <td>
                                <input type="checkbox" value="3" name="DMECollection" '<% if(  DME!=null && DME.Contains("3")  ){ %>checked="checked"<% }%>'" />
                                Grab Bars
                            </td>
                            <td>
                                <input type="checkbox" value="4" name="DMECollection" '<% if(  DME!=null && DME.Contains("4")  ){ %>checked="checked"<% }%>'" />
                                Hospital Bed
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" value="5" name="DMECollection" '<% if(  DME!=null && DME.Contains("5")  ){ %>checked="checked"<% }%>'" />
                                Nebulizer
                            </td>
                            <td>
                                <input type="checkbox" value="6" name="DMECollection" '<% if(  DME!=null && DME.Contains("6")  ){ %>checked="checked"<% }%>'" />
                                Oxygen
                            </td>
                            <td>
                                <input type="checkbox" value="7" name="DMECollection" '<% if(  DME!=null && DME.Contains("7")  ){ %>checked="checked"<% }%>'" />
                                Tub/Shower Bench
                            </td>
                            <td>
                                <input type="checkbox" value="8" name="DMECollection" '<% if(  DME!=null && DME.Contains("8")  ){ %>checked="checked"<% }%>'" />
                                Walker
                            </td>
                            <td>
                                <input type="checkbox" value="9" name="DMECollection" '<% if(  DME!=null && DME.Contains("9")  ){ %>checked="checked"<% }%>'" />
                                Wheelchair
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <input type="checkbox" value="10" id="Edit_DME_Other" name="DMECollection" '<% if(  DME!=null && DME.Contains("10")  ){ %>checked="checked"<% }%>'" />
                                other &nbsp;&nbsp;&nbsp;
                                <%=Html.TextBox("OtherDME", Model.OtherDME, new { @id = "txtEdit_Referral_OtherDME", @class = "text" ,@style="display:none;"})%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Primary Physician</b>
    <div class="rowTable">
        <table border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        <label>
                            Select from the list below:</label>
                        <select style="width: 150px;" class="Physicians input_wrapper" tabindex="17" name="AgencyPhysicians"
                            id="txtEdit_Referral_PhysicianDropDown">
                            <option value="0" selected="selected">** Select Physician **</option>
                        </select>
                    </th>
                    <th>
                        <label>
                            Search by NPI Number:</label>
                        <input type="text" name="txtEdit_Referral_NpiNumber" id="txtEdit_Referral_NpiNumber"
                            maxlength="10" />
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <div class="rowBreak">
                            <div class="contentDivider">
                                <div class="patientfieldset">
                                    <div class="fix">
                                        <div class="row">
                                            <label for="PhysicianFirstName">
                                                First Name:</label>
                                            <div class="inputs">
                                                <span class="input_wrapper">
                                                    <%=Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @id = "txtEdit_Referral_PhysicianFirstName", @class = "text names" })%>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="PhysicianLastName">
                                                Last Name:</label>
                                            <div class="inputs">
                                                <span class="input_wrapper">
                                                    <%=Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @id = "txtEdit_Referral_PhysicianLastName" , @class = "text names"  })%>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="Relationship">
                                                NPI No:</label>
                                            <div class="inputs">
                                                <span class="input_wrapper">
                                                    <%=Html.TextBox("ReferralPhysicianNPI", Model.PhysicianNPI, new { @id = "txtEdit_Referral_PhysicianNPI", @class = "text", @tabindex = "8" })%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contentDivider">
                                <div class="patientfieldset">
                                    <div class="fix">
                                        <div class="row">
                                            <label for="PhysicianPhone">
                                                Phone:</label>
                                            <div class="inputs">
                                                <span class="input_wrappermultible">
                                                    <%=Html.TextBox("PhysicianPhoneArray", Model.PhysicianPhone != null && Model.PhysicianPhone != "" ? Model.PhysicianPhone.Substring(0, 3) : "", new { @id = "txtEdit_Referral_PhysicianPhone1", @class = "input_wrappermultible", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                                                </span>- <span class="input_wrappermultible">
                                                    <%=Html.TextBox("PhysicianPhoneArray", Model.PhysicianPhone != null && Model.PhysicianPhone != "" ? Model.PhysicianPhone.Substring(3, 3) : "", new { @id = "txtEdit_Referral_PhysicianPhone2", @class = "input_wrappermultible", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                                                </span>- <span class="input_wrappermultible">
                                                    <%=Html.TextBox("PhysicianPhoneArray", Model.PhysicianPhone != null && Model.PhysicianPhone != "" ? Model.PhysicianPhone.Substring(6, 4) : "", new { @id = "txtEdit_Referral_PhysicianPhone3", @class = "input_wrappermultible", @style = "width: 51.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "" })%>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="PhysicianFax">
                                                Fax:</label>
                                            <div class="inputs">
                                                <span class="input_wrappermultible">
                                                    <%=Html.TextBox("PhysicianFaxArray", Model.PhysicianFax != null && Model.PhysicianFax != "" ? Model.PhysicianFax.Substring(0, 3) : "", new { @id = "txtEdit_Referral_PhysicianFax1", @class = "autotext numeric phone_short", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                                                </span>- <span class="input_wrappermultible">
                                                    <%=Html.TextBox("PhysicianFaxArray", Model.PhysicianFax != null && Model.PhysicianFax != "" ? Model.PhysicianFax.Substring(3, 3) : "", new { @id = "txtEdit_Referral_PhysicianFax2", @class = "autotext numeric phone_short", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                                                </span>- <span class="input_wrappermultible">
                                                    <%=Html.TextBox("PhysicianFaxArray", Model.PhysicianFax != null && Model.PhysicianFax != "" ? Model.PhysicianFax.Substring(6, 4) : "", new { @id = "txtEdit_Referral_PhysicianFax3", @class = "autotext numeric phone_short", @style = "width: 51.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "" })%>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="PhysicianEmail">
                                                Email:</label>
                                            <div class="inputs">
                                                <span class="input_wrapper">
                                                    <%=Html.TextBox("PhysicianEmail", Model.PhysicianEmailAddress, new { @id = "txtEdit_Referral_PhysicianEmail", @class = "text email" })%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="buttons">
        <ul>
            <li>
                <input name="" type="submit" value="Add" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input name="" type="button" value="Reset" /></li>
        </ul>
    </div>
</div>
<%} %>
