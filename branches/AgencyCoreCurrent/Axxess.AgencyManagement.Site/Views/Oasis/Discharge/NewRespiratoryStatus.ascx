﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyRespiratoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1400) When is the patient dyspneic or noticeably Short of Breath?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M1400PatientDyspneic", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Patient is not short of breath<br />
            <%=Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - When walking more than 20 feet, climbing stairs<br />
            <%=Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - With moderate exertion (e.g., while dressing, using commode or bedpan, walking
            distances less than 20 feet)<br />
            <%=Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - With minimal exertion (e.g., while eating, talking, or performing other ADLs)
            or with agitation<br />
            <%=Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - At rest (during day or night)<br />
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1410) Respiratory Treatments utilized at home: (Mark all that apply.)
            </div>
        </div>
        <div class="padding">
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen" value=" "
                type="hidden" />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen" value="1"
                type="checkbox" '<% if( data.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen") && data["M1410HomeRespiratoryTreatmentsOxygen"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;1
            - Oxygen (intermittent or continuous)<br />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator" value=" "
                type="hidden" />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator" value="1"
                type="checkbox" '<% if( data.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator") && data["M1410HomeRespiratoryTreatmentsVentilator"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;2
            - Ventilator (continually or at night)<br />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous" value=" "
                type="hidden" />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous" value="1"
                type="checkbox" '<% if( data.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous") && data["M1410HomeRespiratoryTreatmentsContinuous"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;3
            - Continuous / Bi-level positive airway pressure<br />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone" value=" " type="hidden" />
            <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone" value="1" type="checkbox" '<% if( data.ContainsKey("M1410HomeRespiratoryTreatmentsNone") && data["M1410HomeRespiratoryTreatmentsNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;4
            - None of the above
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
