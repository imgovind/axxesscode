﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationBehaviourialForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="2">
                Neuro/Emotional/Behavioral Status
            </th>
        </tr>
        <tr>
            <th width="50%">
                Neurological
            </th>
            <th width="50%">
                Psychosocial
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>Oriented to: </li>
                </ul>
                <%string[] genericNeurologicalOriented = data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer != "" ? data["GenericNeurologicalOriented"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li>&nbsp; </li>
                    <li>
                        <input type="hidden" name="Recertification_GenericNeurologicalOriented" value=" " />
                        <input name="Recertification_GenericNeurologicalOriented" value="1" type="checkbox" '<% if( genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Person </li>
                </ul>
                <ul class="columns">
                    <li>&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericNeurologicalOriented" value="2" type="checkbox" '<% if( genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Place </li>
                </ul>
                <ul class="columns">
                    <li>&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericNeurologicalOriented" value="3" type="checkbox" '<% if( genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Time </li>
                </ul>
                <%string[] genericNeurologicalStatus = data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer != "" ? data["GenericNeurologicalStatus"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li>
                        <input type="hidden" name="Recertification_GenericNeurologicalStatus" value=" " />
                        <input name="Recertification_GenericNeurologicalStatus" value="1" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Disoriented </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericNeurologicalStatus" value="2" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Forgetful </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericNeurologicalStatus" value="3" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        PERRL </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericNeurologicalStatus" value="4" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Seizures </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericNeurologicalStatus" value="5" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Tremors </li>
                    <li class="span">Location(s)
                        <%=Html.TextBox("Recertification_GenericNeurologicalTremorsLocation", data.ContainsKey("GenericNeurologicalTremorsLocation") ? data["GenericNeurologicalTremorsLocation"].Answer : "", new { @id = "Recertification_GenericNeurologicalTremorsLocation", @size = "30", @maxlength = "30" })%>
                    </li>
                </ul>
            </td>
            <td>
                <%string[] genericPsychosocial = data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer != "" ? data["GenericPsychosocial"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li>
                        <input type="hidden" name="Recertification_GenericPsychosocial" value="" />
                        <input name="Recertification_GenericPsychosocial" value="1" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="2" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Poor Home Environment </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="3" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Poor Coping Skills </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="4" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Agitated </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="5" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Depressed Mood </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="6" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("6")  ){ %>checked="checked"<% }%>'" />
                        Impaired Decision Making </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="7" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("7")  ){ %>checked="checked"<% }%>'" />
                        Demonstrated/Expressed Anxiety </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="8" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("8")  ){ %>checked="checked"<% }%>'" />
                        Inappropriate Behavior </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericPsychosocial" value="9" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        Irritability </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul class="columns">
                    <li>Comments:<br />
                        <%=Html.TextArea("Recertification_GenericNeuroEmoBehaviorComments", data.ContainsKey("GenericNeuroEmoBehaviorComments") ? data["GenericNeuroEmoBehaviorComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericNeuroEmoBehaviorComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>

<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <%string[] behaviorInterventions = data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer != "" ? data["485BehaviorInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485BehaviorInterventions" value=" " />
                <input name="Recertification_485BehaviorInterventions" value="1" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <strong>*SN TO NOTIFY PHYSICIAN THIS PATIENT WAS SCREENED FOR DEPRESSION USING THE PHQ-2
                    SCALE AND MEETS CRITERIA FOR FURTHER EVALUATION FOR DEPRESSION</strong>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="2" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess for changes in neurological status every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="3" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient's communication skills every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="4" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructSeizurePrecautionPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructSeizurePrecautionPerson") && data["485InstructSeizurePrecautionPerson"].Answer != "" ? data["485InstructSeizurePrecautionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructSeizurePrecautionPerson", instructSeizurePrecautionPerson)%>
                on seizure precautions
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="5" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct caregiver on orientation techniques to use when patient becomes disoriented
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="6" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                MSW:
                <%=Html.Hidden("Recertification_485MSWProvideServiceNumberVisits", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_485MSWProvideServiceNumberVisits", "1", data.ContainsKey("485MSWProvideServiceNumberVisits") && data["485MSWProvideServiceNumberVisits"].Answer == "1" ? true : false, new { @id = "" })%>
                1-2 OR
                <%=Html.RadioButton("Recertification_485MSWProvideServiceNumberVisits", "0", data.ContainsKey("485MSWProvideServiceNumberVisits") && data["485MSWProvideServiceNumberVisits"].Answer == "0" ? true : false, new { @id = "" })%>
                <%=Html.TextBox("Recertification_485MSWProvideServiceVisitAmount", data.ContainsKey("485MSWProvideServiceVisitAmount") ? data["485MSWProvideServiceVisitAmount"].Answer : "", new { @id = "Recertification_485MSWProvideServiceVisitAmount", @size = "5", @maxlength = "5" })%>
                visits, every 60 days for provider services
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="7" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                MSW:
                <%=Html.Hidden("Recertification_485MSWLongTermPlanningVisits", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_485MSWLongTermPlanningVisits", "1", data.ContainsKey("485MSWLongTermPlanningVisits") && data["485MSWLongTermPlanningVisits"].Answer == "1" ? true : false, new { @id = "" })%>
                1-2 OR
                <%=Html.RadioButton("Recertification_485MSWLongTermPlanningVisits", "0", data.ContainsKey("485MSWLongTermPlanningVisits") && data["485MSWLongTermPlanningVisits"].Answer == "0" ? true : false, new { @id = "" })%>
                <%=Html.TextBox("Recertification_485MSWLongTermPlanningVisitAmount", data.ContainsKey("485MSWLongTermPlanningVisitAmount") ? data["485MSWLongTermPlanningVisitAmount"].Answer : "", new { @id = "Recertification_485MSWLongTermPlanningVisitAmount", @size = "5", @maxlength = "5" })%>
                visits, every 60 days for long term planning
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorInterventions" value="8" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                MSW:
                <%=Html.Hidden("Recertification_485MSWCommunityAssistanceVisits", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_485MSWCommunityAssistanceVisits", "1", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "1" ? true : false, new { @id = "" })%>
                1-2 OR
                <%=Html.RadioButton("Recertification_485MSWCommunityAssistanceVisits", "0", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "0" ? true : false, new { @id = "" })%>
                <%=Html.TextBox("Recertification_485MSWCommunityAssistanceVisitAmount", data.ContainsKey("485MSWCommunityAssistanceVisitAmount") ? data["485MSWCommunityAssistanceVisitAmount"].Answer : "", new { @id = "Recertification_485MSWCommunityAssistanceVisitAmount", @size = "5", @maxlength = "5" })%>
                visits, every 60 days for community resource assistance
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                Additional Orders: &nbsp;
                <%var behaviorOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485BehaviorOrderTemplates") && data["485BehaviorOrderTemplates"].Answer != "" ? data["485BehaviorOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485BehaviorOrderTemplates", behaviorOrderTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485BehaviorComments", data.ContainsKey("485BehaviorComments") ? data["485BehaviorComments"].Answer : "", 5, 70, new { @id = "Recertification_485BehaviorComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] behaviorGoals = data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer != "" ? data["485BehaviorGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485BehaviorGoals" value=" " />
                <input name="Recertification_485BehaviorGoals" value="1" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will remain free from increased confusion during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorGoals" value="2" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeSeizurePrecautionsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeSeizurePrecautionsPerson") && data["485VerbalizeSeizurePrecautionsPerson"].Answer != "" ? data["485VerbalizeSeizurePrecautionsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeSeizurePrecautionsPerson", verbalizeSeizurePrecautionsPerson)%>
                will verbalize understanding of seizure precautions
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorGoals" value="3" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Caregiver will verbalize understanding of proper orientation techniques to use when
                patient becomes disoriented
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485BehaviorGoals" value="4" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's community resource needs will be met with assistance of social worker
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                <%var behaviorGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485BehaviorGoalTemplates") && data["485BehaviorGoalTemplates"].Answer != "" ? data["485BehaviorGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485BehaviorGoalTemplates", behaviorGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485BehaviorGoalComments", data.ContainsKey("485BehaviorGoalComments") ? data["485BehaviorGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485BehaviorGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="4">
                Mental Status (locator #19)
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="Recertification_485MentalStatus" value=" " />
                <input name="Recertification_485MentalStatus" value="1" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("1")){ %>checked="checked"<% }%>'" />&nbsp;
                Oriented
            </td>
            <td>
                <input name="Recertification_485MentalStatus" value="2" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("2")){ %>checked="checked"<% }%>'" />&nbsp;
                Comatose
            </td>
            <td>
                <input name="Recertification_485MentalStatus" value="3" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("3")){ %>checked="checked"<% }%>'" />&nbsp;
                Forgetful
            </td>
            <td>
                <input name="Recertification_485MentalStatus" value="7" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("7")){ %>checked="checked"<% }%>'" />&nbsp;
                Agitated
            </td>
        </tr>
        <tr>
            <td>
                <input name="Recertification_485MentalStatus" value="4" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("4")){ %>checked="checked"<% }%>'" />&nbsp;
                Depressed
            </td>
            <td>
                <input name="Recertification_485MentalStatus" value="5" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("5")){ %>checked="checked"<% }%>'" />&nbsp;
                Disoriented
            </td>
            <td>
                <input name="Recertification_485MentalStatus" value="6" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("6")){ %>checked="checked"<% }%>'" />&nbsp;
                Lethargic
            </td>
            <td>
                <input name="Recertification_485MentalStatus" value="8" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("8")){ %>checked="checked"<% }%>'" />&nbsp;
                Other (specify):&nbsp;
                <%=Html.TextBox("Recertification_485MentalStatusOther", data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : "", new { @id = "Recertification_485MentalStatusOther", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <ul class="columns">
                    <li>Additional Orders(Specify):<br />
                        <%=Html.TextArea("Recertification_485MentalStatusComments", data.ContainsKey("485MentalStatusComments") ? data["485MentalStatusComments"].Answer : "", 5, 70, new { @id = "Recertification_485MentalStatusComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
