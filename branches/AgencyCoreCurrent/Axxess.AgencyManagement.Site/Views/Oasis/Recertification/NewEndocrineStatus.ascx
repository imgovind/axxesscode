﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationEndocrineForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Endocrine
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="Recertification_GenericEndocrineWithinNormalLimits" id="Recertification_GenericEndocrineWithinNormalLimits" />
                <input name="Recertification_GenericEndocrineWithinNormalLimits" value="1" type="checkbox" '<% if( data.ContainsKey("GenericEndocrineWithinNormalLimits") && data["GenericEndocrineWithinNormalLimits"].Answer == "1" ){ %>checked="checked"<% }%>'" />
                WNL (Within Normal Limits)
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is patient diabetic? </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericPatientDiabetic", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericPatientDiabetic", "1", data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "1" ? true : false, new { @id = "" })%>
                        &nbsp; Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericPatientDiabetic", "0", data.ContainsKey("GenericPatientDiabetic") && data["GenericPatientDiabetic"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Insulin dependent? </li>
                    <li class="spacer">
                        <%=Html.Hidden("Recertification_GenericInsulinDependent", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericInsulinDependent", "1", data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "1" ? true : false, new { @id = "" })%>
                        &nbsp; Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericInsulinDependent", "0", data.ContainsKey("GenericInsulinDependent") && data["GenericInsulinDependent"].Answer == "0" ? true : false, new { @id = "" })%>
                        &nbsp; No </li>
                    <li class="spacer">For how long? </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericInsulinDependencyDuration", data.ContainsKey("GenericInsulinDependencyDuration") ? data["GenericInsulinDependencyDuration"].Answer : "", new { @id = "Recertification_GenericInsulinDependencyDuration", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is patient independently able to draw up correct dose of insulin?
                    </li>
                    <li class="spacer">
                        <%=Html.Hidden("Recertification_GenericDrawUpInsulinDose", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericDrawUpInsulinDose", "1", data.ContainsKey("GenericDrawUpInsulinDose") && data["GenericDrawUpInsulinDose"].Answer == "1" ? true : false, new { @id = "" })%>
                        &nbsp; Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericDrawUpInsulinDose", "0", data.ContainsKey("GenericDrawUpInsulinDose") && data["GenericDrawUpInsulinDose"].Answer == "0" ? true : false, new { @id = "" })%>
                        &nbsp; No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is patient able to properly administer own insulin? </li>
                    <li class="spacer">
                        <%=Html.Hidden("Recertification_GenericAdministerOwnInsulin", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericAdministerOwnInsulin", "1", data.ContainsKey("GenericAdministerOwnInsulin") && data["GenericAdministerOwnInsulin"].Answer == "1" ? true : false, new { @id = "" })%>
                        &nbsp; Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericAdministerOwnInsulin", "0", data.ContainsKey("GenericAdministerOwnInsulin") && data["GenericAdministerOwnInsulin"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is patient taking oral hypoglycemic agent? </li>
                    <li class="spacer">
                        <%=Html.Hidden("Recertification_GenericOralHypoglycemicAgent", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericOralHypoglycemicAgent", "1", data.ContainsKey("GenericOralHypoglycemicAgent") && data["GenericOralHypoglycemicAgent"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;
                        Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericOralHypoglycemicAgent", "0", data.ContainsKey("GenericOralHypoglycemicAgent") && data["GenericOralHypoglycemicAgent"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is patient independent with glucometer use? </li>
                    <li class="spacer">
                        <%=Html.Hidden("Recertification_GenericGlucometerUseIndependent", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericGlucometerUseIndependent", "1", data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;&nbsp;
                        Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericGlucometerUseIndependent", "0", data.ContainsKey("GenericGlucometerUseIndependent") && data["GenericGlucometerUseIndependent"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is caregiver able to correctly draw up and administer insulin?
                    </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericCareGiverDrawUpInsulin", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericCareGiverDrawUpInsulin", "1", data.ContainsKey("GenericCareGiverDrawUpInsulin") && data["GenericCareGiverDrawUpInsulin"].Answer == "1" ? true : false, new { @id = "" })%>
                        &nbsp; Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericCareGiverDrawUpInsulin", "0", data.ContainsKey("GenericCareGiverDrawUpInsulin") && data["GenericCareGiverDrawUpInsulin"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No &nbsp;
                        <%=Html.RadioButton("Recertification_GenericCareGiverDrawUpInsulin", "2", data.ContainsKey("GenericCareGiverDrawUpInsulin") && data["GenericCareGiverDrawUpInsulin"].Answer == "2" ? true : false, new { @id = "" })%>&nbsp;
                        N/A, no caregiver</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Is caregiver independent with glucometer use? </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericCareGiverGlucometerUse", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericCareGiverGlucometerUse", "1", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;
                        Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericCareGiverGlucometerUse", "0", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No &nbsp;
                        <%=Html.RadioButton("Recertification_GenericCareGiverGlucometerUse", "2", data.ContainsKey("GenericCareGiverGlucometerUse") && data["GenericCareGiverGlucometerUse"].Answer == "2" ? true : false, new { @id = "" })%>&nbsp;
                        N/A, no caregiver</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Does patient or caregiver routinely perform inspection of the
                        patient's lower extremities? </li>
                    <li class="spacer">
                        <%=Html.Hidden("Recertification_GenericCareGiverInspectLowerExtremities", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericCareGiverInspectLowerExtremities", "1", data.ContainsKey("GenericCareGiverInspectLowerExtremities") && data["GenericCareGiverInspectLowerExtremities"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;
                        Yes&nbsp;
                        <%=Html.RadioButton("Recertification_GenericCareGiverInspectLowerExtremities", "0", data.ContainsKey("GenericCareGiverInspectLowerExtremities") && data["GenericCareGiverInspectLowerExtremities"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        No </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Does patient have any of the following? </li>
                </ul>
                <%string[] patientEdocrineProblem = data.ContainsKey("GenericPatientEdocrineProblem") && data["GenericPatientEdocrineProblem"].Answer != "" ? data["GenericPatientEdocrineProblem"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input type="hidden" name="Recertification_GenericPatientEdocrineProblem" value="" />
                        <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="1" '<% if(  patientEdocrineProblem!=null && patientEdocrineProblem.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Polyuria
                        <br />
                        <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="2" '<% if(  patientEdocrineProblem!=null && patientEdocrineProblem.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Polydipsia<br />
                        <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="3" '<% if(  patientEdocrineProblem!=null && patientEdocrineProblem.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Polyphagia
                        <br />
                        <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="4" '<% if(  patientEdocrineProblem!=null && patientEdocrineProblem.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Neuropathy<br />
                        <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="5" '<% if(  patientEdocrineProblem!=null && patientEdocrineProblem.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Radiculopathy
                        <br />
                        <input type="checkbox" name="Recertification_GenericPatientEdocrineProblem" value="6" '<% if(  patientEdocrineProblem!=null && patientEdocrineProblem.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Thyroid problems
                        <br />
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Blood Sugar&nbsp;
                        <%=Html.TextBox("Recertification_GenericBloodSugarLevelText", data.ContainsKey("GenericBloodSugarLevelText") ? data["GenericBloodSugarLevelText"].Answer : "", new { @id = "Recertification_GenericBloodSugarLevelText", @size = "10", @maxlength = "10" })%>
                    </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericBloodSugarLevel", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericBloodSugarLevel", "Random", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Random" ? true : false, new { @id = "" })%>
                        Random
                        <br />
                    </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericBloodSugarLevel", "Fasting", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "Fasting" ? true : false, new { @id = "" })%>
                        Fasting
                        <br />
                    </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericBloodSugarLevel", "2HoursPP", data.ContainsKey("GenericBloodSugarLevel") && data["GenericBloodSugarLevel"].Answer == "2HoursPP" ? true : false, new { @id = "" })%>
                        2 Hours PP
                        <br />
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Blood sugar checked by: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericBloodSugarCheckedBy", data.ContainsKey("GenericBloodSugarCheckedBy") ? data["GenericBloodSugarCheckedBy"].Answer : "", new { @id = "Recertification_GenericBloodSugarCheckedBy", @size = "15", @maxlength = "15" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Site: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericBloodSugarSite", data.ContainsKey("GenericBloodSugarSite") ? data["GenericBloodSugarSite"].Answer : "", new { @id = "Recertification_GenericBloodSugarSite", @size = "15", @maxlength = "15" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Comments:<br />
                        <%=Html.TextArea("Recertification_GenericEndocrineComments", data.ContainsKey("GenericEndocrineComments") ? data["GenericEndocrineComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericEndocrineComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <%string[] endocrineInterventions = data.ContainsKey("485EndocrineInterventions") && data["485EndocrineInterventions"].Answer != "" ? data["485EndocrineInterventions"].Answer.Split(',') : null; %>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485EndocrineInterventions" value=" " />
                <input name="Recertification_485EndocrineInterventions" value="1" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructDiabeticManagementPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructDiabeticManagementPerson") && data["485InstructDiabeticManagementPerson"].Answer != "" ? data["485InstructDiabeticManagementPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructDiabeticManagementPerson", instructDiabeticManagementPerson)%>
                on all aspects of diabetic management to include disease process, foot assessments,
                signs and symptoms of hypo/hyperglycemia, glucometer use and preparation and administration
                of diabetic medications ordered by physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="2" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructInspectFeetDailyPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructInspectFeetDailyPerson") && data["485InstructInspectFeetDailyPerson"].Answer != "" ? data["485InstructInspectFeetDailyPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructInspectFeetDailyPerson", instructInspectFeetDailyPerson)%>
                to inspect patient's feet daily and report any skin or nail problems to SN
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="3" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructWashFeetWarmPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructWashFeetWarmPerson") && data["485InstructWashFeetWarmPerson"].Answer != "" ? data["485InstructWashFeetWarmPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructWashFeetWarmPerson", instructWashFeetWarmPerson)%>
                to wash patient's feet in warm (not hot) water. Wash feet gently and pat dry thoroughly
                making sure to dry between toes
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="4" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructMoisturizerPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructMoisturizerPerson") && data["485InstructMoisturizerPerson"].Answer != "" ? data["485InstructMoisturizerPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructMoisturizerPerson", instructMoisturizerPerson)%>
                to use moisturizer daily but avoid getting between toes
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="5" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to wear clean, dry, properly-fitted socks and change them
                every day
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="6" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructNailCarePerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructNailCarePerson") && data["485InstructNailCarePerson"].Answer != "" ? data["485InstructNailCarePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructNailCarePerson", instructNailCarePerson)%>
                on appropriate nail care as follows: trim nails straight across and file rough edges
                with nail file
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="7" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("7")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructNeverWalkBareFootPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructNeverWalkBareFootPerson") && data["485InstructNeverWalkBareFootPerson"].Answer != "" ? data["485InstructNeverWalkBareFootPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructNeverWalkBareFootPerson", instructNeverWalkBareFootPerson)%>
                that patient should never walk barefoot
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="8" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("8")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructElevateFeetPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructElevateFeetPerson") && data["485InstructElevateFeetPerson"].Answer != "" ? data["485InstructElevateFeetPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructElevateFeetPerson", instructElevateFeetPerson)%>
                that patient should elevate feet when sitting
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="9" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("9")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructProtectFeetPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructProtectFeetPerson") && data["485InstructProtectFeetPerson"].Answer != "" ? data["485InstructProtectFeetPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructProtectFeetPerson", instructProtectFeetPerson)%>
                to protect patient's feet from extreme heat or cold
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="10" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("10")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructNeverCutCornsPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructNeverCutCornsPerson") && data["485InstructNeverCutCornsPerson"].Answer != "" ? data["485InstructNeverCutCornsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructNeverCutCornsPerson", instructNeverCutCornsPerson)%>
                never to try to cut off corns, calluses, or any other lesions from lower extremities
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="11" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("11")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to perform finger stick for fasting blood sugar/random blood sugar during visit
                if it has not been done or if patient reports signs and symptoms of hypo/hyperglycemia
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="12" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("12")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to give patient 4 oz of fruit juice or 1 tablespoon of sugar in H2O if blood
                sugar is
                <%=Html.TextBox("Recertification_485GiveJuiceIfBloodSugarLevel", data.ContainsKey("485GiveJuiceIfBloodSugarLevel") ? data["485GiveJuiceIfBloodSugarLevel"].Answer : "", new { @id = "Recertification_485GiveJuiceIfBloodSugarLevel", @size = "3", @maxlength = "3" })%>
                mg/dl or below, and recheck blood sugar in 15 to 20 minutes. If blood sugar remains
                <%=Html.TextBox("Recertification_485GiveJuiceIfBloodSugarLevelRemains", data.ContainsKey("485GiveJuiceIfBloodSugarLevelRemains") ? data["485GiveJuiceIfBloodSugarLevelRemains"].Answer : "", new { @id = "Recertification_485GiveJuiceIfBloodSugarLevelRemains", @size = "3", @maxlength = "3" })%>
                mg/dL or below, notify physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="13" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("13")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to prepare and administer insulin (freq)
                <%=Html.TextBox("Recertification_485PrepareAdministerInsulinType", data.ContainsKey("485PrepareAdministerInsulinType") ? data["485PrepareAdministerInsulinType"].Answer : "", new { @id = "Recertification_485PrepareAdministerInsulinType", @size = "15", @maxlength = "15" })%>
                as follows:
                <%=Html.TextBox("Recertification_485PrepareAdministerInsulinFrequency", data.ContainsKey("485PrepareAdministerInsulinFrequency") ? data["485PrepareAdministerInsulinFrequency"].Answer : "", new { @id = "Recertification_485PrepareAdministerInsulinFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="14" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("14")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess blood sugar via finger stick every visit prior to insulin administration
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="15" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("15")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to prefill insulin syringes (freq)
                <%=Html.TextBox("Recertification_485PrefillInsulinSyringesType", data.ContainsKey("485PrefillInsulinSyringesType") ? data["485PrefillInsulinSyringesType"].Answer : "", new { @id = "Recertification_485PrefillInsulinSyringesType", @size = "15", @maxlength = "15" })%>
                as follows:
                <%=Html.TextBox("Recertification_485PrefillInsulinSyringesTypeFrequency", data.ContainsKey("485PrefillInsulinSyringesTypeFrequency") ? data["485PrefillInsulinSyringesTypeFrequency"].Answer : "", new { @id = "Recertification_485PrefillInsulinSyringesTypeFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineInterventions" value="16" type="checkbox" '<% if(  endocrineInterventions!=null && endocrineInterventions.Contains("16")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to perform inspection of patient's lower extremities every visit and report any
                alteration in skin integrity to physician
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var endocrineInterventionTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485EndocrineInterventionTemplates") && data["485EndocrineInterventionTemplates"].Answer != "" ? data["485EndocrineInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485EndocrineInterventionTemplates", endocrineInterventionTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485EndocrineInterventionComments", data.ContainsKey("485EndocrineInterventionComments") ? data["485EndocrineInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485EndocrineInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] endocrineGoals = data.ContainsKey("485EndocrineGoals") && data["485EndocrineGoals"].Answer != "" ? data["485EndocrineGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485EndocrineGoals" value=" " />
                <input name="Recertification_485EndocrineGoals" value="1" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's fasting blood sugar will remain between
                <%=Html.TextBox("Recertification_485FastingBloodSugarBetween", data.ContainsKey("485FastingBloodSugarBetween") ? data["485FastingBloodSugarBetween"].Answer : "", new { @id = "Recertification_485FastingBloodSugarBetween", @size = "4", @maxlength = "4" })%>
                mg/dl and
                <%=Html.TextBox("Recertification_485FastingBloodSugarAnd", data.ContainsKey("485FastingBloodSugarAnd") ? data["485FastingBloodSugarAnd"].Answer : "", new { @id = "Recertification_485FastingBloodSugarAnd", @size = "4", @maxlength = "4" })%>
                mg/dl during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineGoals" value="2" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's random blood sugar will remain between
                <%=Html.TextBox("Recertification_485RandomBloodSugarBetween", data.ContainsKey("485RandomBloodSugarBetween") ? data["485RandomBloodSugarBetween"].Answer : "", new { @id = "Recertification_485RandomBloodSugarBetween", @size = "4", @maxlength = "4" })%>
                mg/dl and
                <%=Html.TextBox("Recertification_485RandomBloodSugarAnd", data.ContainsKey("485RandomBloodSugarAnd") ? data["485RandomBloodSugarAnd"].Answer : "", new { @id = "Recertification_485RandomBloodSugarAnd", @size = "4", @maxlength = "4" })%>
                mg/dl during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineGoals" value="3" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineGoals" value="4" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var glucometerUseIndependencePerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485GlucometerUseIndependencePerson") && data["485GlucometerUseIndependencePerson"].Answer != "" ? data["485GlucometerUseIndependencePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485GlucometerUseIndependencePerson", glucometerUseIndependencePerson)%>
                will be independent with glucometer use by:
                <%=Html.TextBox("Recertification_485GlucometerUseIndependenceDate", data.ContainsKey("485GlucometerUseIndependenceDate") ? data["485GlucometerUseIndependenceDate"].Answer : "", new { @id = "Recertification_485GlucometerUseIndependenceDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineGoals" value="5" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeSkinConditionUnderstandingPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485VerbalizeSkinConditionUnderstandingPerson") && data["485VerbalizeSkinConditionUnderstandingPerson"].Answer != "" ? data["485VerbalizeSkinConditionUnderstandingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeSkinConditionUnderstandingPerson", verbalizeSkinConditionUnderstandingPerson)%>
                will verbalize an understanding of skin conditions that must be reported to SN or
                physician immediately
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineGoals" value="6" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var independentInsulinAdministrationPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485IndependentInsulinAdministrationPerson") && data["485IndependentInsulinAdministrationPerson"].Answer != "" ? data["485IndependentInsulinAdministrationPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485IndependentInsulinAdministrationPerson", independentInsulinAdministrationPerson)%>
                will be independent with insulin administration by:
                <%=Html.TextBox("Recertification_485IndependentInsulinAdministrationDate", data.ContainsKey("485IndependentInsulinAdministrationDate") ? data["485IndependentInsulinAdministrationDate"].Answer : "", new { @id = "Recertification_485IndependentInsulinAdministrationDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EndocrineGoals" value="7" type="checkbox" '<% if(  endocrineGoals!=null && endocrineGoals.Contains("7")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeProperFootCarePerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485VerbalizeProperFootCarePerson") && data["485VerbalizeProperFootCarePerson"].Answer != "" ? data["485VerbalizeProperFootCarePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeProperFootCarePerson", verbalizeProperFootCarePerson)%>
                will verbalize understanding of proper diabetic foot care by:
                <%=Html.TextBox("Recertification_485VerbalizeProperFootCareDate", data.ContainsKey("485VerbalizeProperFootCareDate") ? data["485VerbalizeProperFootCareDate"].Answer : "", new { @id = "Recertification_485VerbalizeProperFootCareDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var endocrineGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485EndocrineGoalTemplates") && data["485EndocrineGoalTemplates"].Answer != "" ? data["485EndocrineGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485EndocrineGoalTemplates", endocrineGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485EndocrineGoalComments", data.ContainsKey("485EndocrineGoalComments") ? data["485EndocrineGoalComments"].Answer : "", 2, 70, new { @id = "Recertification_485EndocrineGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
