﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<div class="oasisAssWindowContainer">
    <div id="socTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
        <ul>
            <li><a href="#demographics_soc">Demographics</a></li>
            <li><a href="#patienthistory_soc">Patient History & Diagnoses</a></li>
            <li><a href="#riskassessment_soc">Risk Assessment</a></li>
            <li><a href="#prognosis_soc">Prognosis</a></li>
            <li><a href="#supportiveassistance_soc">Supportive Assistance</a></li>
            <li><a href="#sensorystatus_soc">Sensory Status</a></li>
            <li><a href="#pain_soc">Pain</a></li>
            <li><a href="#integumentarystatus_soc">Integumentary Status</a></li>
            <li><a href="#respiratorystatus_soc">Respiratory Status</a></li>
            <li><a href="#endocrine_soc">Endocrine</a></li>
            <li><a href="#cardiacstatus_soc">Cardiac Status</a></li>
            <li><a href="#eliminationstatus_soc">Elimination Status</a></li>
            <li><a href="#nutrition_soc">Nutrition</a></li>
            <li><a href="#behaviourialstatus_soc">Neuro/Behaviourial Status</a></li>
            <li><a href="#adl_soc">ADL/IADLs</a></li>
            <li><a href="#suppliesworksheet_soc">Supplies Worksheet</a></li>
            <li><a href="#medications_soc">Medications</a></li>
            <li><a href="#caremanagement_soc">Care Management</a></li>
            <li><a href="#therapyneed_soc">Therapy Need & Plan Of Care</a></li>
            <li><a href="#ordersdisciplinetreatment_soc">Orders for Discipline and Treatment</a></li>
        </ul>
        <div style="width: 179px;">
            <input id="socValidation" type="button" value="Validate" onclick="SOC.Validate('<%=Model.Id%>');" /></div>
        <div id="demographics_soc" class="general abs">
            <% Html.RenderPartial("~/Views/Oasis/StartOfCare/Demographics.ascx", Model); %>
        </div>
        <div id="patienthistory_soc" class="general abs">
        </div>
        <div id="riskassessment_soc" class="general abs">
        </div>
        <div id="prognosis_soc" class="general abs">
        </div>
        <div id="supportiveassistance_soc" class="general abs">
        </div>
        <div id="sensorystatus_soc" class="general abs">
        </div>
        <div id="pain_soc" class="general abs">
        </div>
        <div id="integumentarystatus_soc" class="general abs">
        </div>
        <div id="respiratorystatus_soc" class="general abs">
        </div>
        <div id="endocrine_soc" class="general abs">
        </div>
        <div id="cardiacstatus_soc" class="general abs">
        </div>
        <div id="eliminationstatus_soc" class="general abs">
        </div>
        <div id="nutrition_soc" class="general abs">
        </div>
        <div id="behaviourialstatus_soc" class="general abs">
        </div>
        <div id="adl_soc" class="general abs">
        </div>
        <div id="suppliesworksheet_soc" class="general abs">
        </div>
        <div id="medications_soc" class="general abs">
        </div>
        <div id="caremanagement_soc" class="general abs">
        </div>
        <div id="therapyneed_soc" class="general abs">
        </div>
        <div id="ordersdisciplinetreatment_soc" class="general abs">
        </div>
    </div>
</div>
