﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="newemployee" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">New User</span> <span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <!--[if !IE]>start forms<![endif]-->
            <% using (Html.BeginForm("New", "Employee", FormMethod.Post, new { @id = "newEmployeeForm" }))%>
            <%  { %>
            <div id="newEmployeeValidaton" class="marginBreak " style="display: none">
            </div>
            <div class="marginBreak">
                <b>User Information</b>
                <div class="rowBreak">
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="EmailAddress">
                                        E-mail:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("EmailAddress", "", new { @id = "txtAdd_Employee_EmailAddress", @class = "text required input_wrapper", @maxlength = "50", @tabindex = "1" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="Password">
                                        Password:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("Password", "", new { @id = "txtAdd_Employee_Password", @class = "text required input_wrapper", @maxlength = "20", @style="width: 105px;", @tabindex = "2" })%>
                                        <input type="button" id="btnAdd_Employee_GeneratePassword" value="*****" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="CustomAgencyEmployeeId">
                                        Agency Employee Id:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("CustomAgencyEmployeeId", "", new { @id = "txtAdd_Employee_CustomAgencyEmployeeId", @class = "text input_wrapper", @maxlength = "20", @tabindex = "4" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="FirstName">
                                        First Name:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("FirstName", "", new { @id = "txtAdd_Employee_FirstName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "3" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="LastName">
                                        Last Name:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("LastName", "", new { @id = "txtAdd_Employee_LastName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "4" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="Suffix">
                                        Suffix (optional):</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("Suffix", "", new { @id = "txtAdd_Employee_LastName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "4" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="marginBreak">
                <b>Role Within Agency</b>
                <div class="rowBreak">
                    <div class="row">
                        <ul class="columns">
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="1" />&nbsp;Alt. Admin</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="2" />&nbsp;Case Manager</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="3" />&nbsp;Skilled Nurse</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="4" />&nbsp;Home Health
                                Aide</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="5" />&nbsp;Clerk (non-clinical)</li>
                        </ul>
                        <ul class="columns">
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="6" />&nbsp;Physical Therapist</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="7" />&nbsp;Occupational
                                Therapist</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="8" />&nbsp;Speech Therapist</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="9" />&nbsp;Medicare Social
                                Worker</li>
                            <li class="spacer">
                                <input type="radio" name="AgencyRoleId" tabindex="13" value="10" />&nbsp;Physician</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="marginBreak">
                <b>Permissions</b>
                <div class="rowBreak">
                    <div class="row">
                    </div>
                </div>
            </div>
            <div class="marginBreak">
                <b>Access Restriction</b>
                <div class="rowBreak">
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="AllowWeekendAccess">
                                        Allow Weekend Access?</label>
                                    <input type="checkbox" name="AllowWeekendAccess" id="txtAdd_Employee_AllowWeekendAccess"
                                        tabindex="13" />
                                </div>
                                <div class="row">
                                    <label for="EarliestLoginTime">
                                        Earliest Login Time:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("EarliestLoginTime", "", new { @id = "txtAdd_Employee_EarliestLoginTime", @maxlength = "20", @class = "text input_wrapper", @tabindex = "21" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AutomaticLogoutTime">
                                        Automatic Logout Time:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AutomaticLogoutTime", "", new { @id = "txtAdd_Employee_AutomaticLogoutTime", @class = "text input_wrapper", @maxlength = "20", @tabindex = "22" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li>
                            <input type="submit" value="Save" /></li>
                        <li>
                            <input type="button" value="Cancel" onclick="Employee.Close($(this));" /></li>
                        <li>
                            <input type="reset" value="Reset" /></li>
                    </ul>
                </div>
            </div>
            <%} %>
            <!--[if !IE]>end forms<![endif]-->
        </div>
        <div class="abs window_bottom">
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
