﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editPhysicianContact" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Edit Physician Contact </span><span class="float_right"><a
                href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                </a><a href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <% using (Html.BeginForm("EditPhysicianContact", "Patient", FormMethod.Post, new { @id = "editPhysicianContactForm" }))%>
            <%  { %>
            <%=Html.Hidden("PatientId", "", new { @id = "txtEdit_PhysicianContactPatientID" })%>
            <%=Html.Hidden("Id","", new { @id ="txtEdit_PhysicianContactID" })%>
            <div id="editPhysicianContactValidaton" class="marginBreak" style="display: none">
            </div>
            <div class="marginBreak">
                <div class="rowBreak">
                    <!--[if !IE]>start section content top<![endif]-->
                    <div class="row">
                        <div class="contentDivider">
                            <div class="patientfieldset">
                                <div class="fix">
                                    <div class="row">
                                        <label for="FirstName">
                                            First Name:&nbsp;&nbsp;&nbsp;</label>
                                        <%=Html.TextBox("FirstName", "", new { @id = "txtEdit_PhysicianContact_FirstName", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "48" })%>
                                    </div>
                                    <div class="row">
                                        <label for="LastName">
                                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                                        <%=Html.TextBox("LastName", "", new { @id = "txtEdit_PhysicianContact_LastName", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "49" })%>
                                    </div>
                                    <div class="row">
                                        <label for="PhoneHome">
                                            Primary Phone:</label>
                                        <div class="inputs">
                                            <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                    name="PhysicianContactPrimaryPhoneArray" id="txtEdit_PhysicianContact_PrimaryPhoneArray1"
                                                    maxlength="3" size="3" tabindex="50" /></span> - <span class="input_wrappermultible">
                                                        <input type="text" class="autotext digits required" style="width: 49px; padding: 0px; margin: 0px;"
                                                            name="PhysicianContactPrimaryPhoneArray" id="txtEdit_PhysicianContact_PrimaryPhoneArray2"
                                                            maxlength="3" size="3" tabindex="51" /></span> - <span class="input_wrappermultible">
                                                                <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                                    name="PhysicianContactPrimaryPhoneArray" id="txtEdit_PhysicianContact_PrimaryPhoneArray3"
                                                                    maxlength="4" size="5" tabindex="52" /></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="PhoneMobile">
                                            Alt Phone:</label>
                                        <div class="inputs">
                                            <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                    name="PhysicianContactAltPhoneArray" id="txtEdit_PhysicianContact_AltPhoneArray1"
                                                    maxlength="3" size="3" tabindex="53" /></span> - <span class="input_wrappermultible">
                                                        <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                                            name="PhysicianContactAltPhoneArray" id="txtEdit_PhysicianContact_AltPhoneArray2"
                                                            maxlength="3" size="3" tabindex="54" /></span> - <span class="input_wrappermultible">
                                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                                    name="PhysicianContactAltPhoneArray" id="txtEdit_PhysicianContact_AltPhoneArray3"
                                                                    maxlength="4" size="5" tabindex="55" /></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="PhoneMobile">
                                            Fax:</label>
                                        <div class="inputs">
                                            <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                    name="PhysicianContactFaxArray" id="txtEdit_Patient_PhysicianContactFax1" maxlength="3"
                                                    size="3" tabindex="53" /></span> - <span class="input_wrappermultible">
                                                        <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                                            name="PhysicianContactFaxArray" id="txtEdit_Patient_PhysicianContactFax2" maxlength="3"
                                                            size="3" tabindex="54" /></span> - <span class="input_wrappermultible">
                                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                                    name="PhysicianContactFaxArray" id="txtEdit_Patient_PhysicianContactFax3" maxlength="4"
                                                                    size="5" tabindex="55" /></span>
                                        </div>
                                    </div>
                                    <div class="row" style="vertical-align: middle;">
                                        <label for="FirstName">
                                            Email:&nbsp;&nbsp;&nbsp;</label>
                                        <%=Html.TextBox("EmailAddress", "", new { @id = "txtEdit_PhysicianContact_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "56" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="contentDivider">
                            <div class="patientfieldset">
                                <div class="fix">
                                    <div class="row">
                                        <label for="Relationship">
                                            NPI No:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("NPI", "", new { @id = "txtEdit_PhysicianContact_NPINo", @class = "text input_wrapper digits", @maxlength = "10", @tabindex = "57" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="AddressLine1">
                                            Address Line 1:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("MailingAddressLine1", "", new { @id = "txtEdit_PhysicianContact_AddressLine1", @class = "text input_wrapper required", @tabindex = "58" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="AddressLine2">
                                            Address Line 2:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("MailingAddressLine2", "", new { @id = "txtEdit_PhysicianContact_AddressLine2", @class = "text input_wrapper", tabindex = "59" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="AddressCity">
                                            City:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("MailingAddressCity", "", new { @id = "txtEdit_PhysicianContact_AddressCity", @class = "text input_wrapper required", @tabindex = "60" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="AddressStateCode">
                                            State, Zip Code :</label>
                                        <div class="inputs">
                                            <select id="txtEdit_PhysicianContact_AddressStateCode" name="MailingAddressStateCode" class="AddressStateCode input_wrapper required"
                                                style="width: 119px;" tabindex="61">
                                                <option value="0" selected>** Select State **</option>
                                            </select>
                                            &nbsp;
                                            <%=Html.TextBox("MailingAddressZipCode", "", new { @id = "txtEdit_PhysicianContact_AddressZipCode", @class = "text digits isValidUSZip required", @tabindex = "62", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="SetPrimary">
                                            Set Primary:</label>
                                        <div class="inputs">
                                            <%=Html.CheckBox("Primary", new { @id = "txtEdit_PhysicianContact_SetPrimary"  })%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li>
                            <input name="" type="submit" value="Add" /></li>
                        <li>
                            <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
                        <li>
                            <input name="" type="reset" value="Reset" /></li>
                    </ul>
                </div>
            </div>
            <%} %>
        </div>
        <div class="abs window_bottom">
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
