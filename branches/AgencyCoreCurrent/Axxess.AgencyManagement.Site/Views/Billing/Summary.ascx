﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<fieldset style="height: 100%;">
    <div style="width: 48%; display: block; margin: 0px; padding: 0px;" class="window_aside">
        <div style="display: block; width: 100%;">
            <%=
                Html.Hidden("Id",Model.Id) %>
            <div style="vertical-align: middle;" class="row">
                <label for="FirstName">
                    Patient First Name:&nbsp;&nbsp;&nbsp;</label>
                <%=Html.Label(Model.FirstName != null ? Model.FirstName : "")%>
            </div>
            <div class="row">
                <label for="LastName">
                    Patient Last Name:&nbsp;&nbsp;&nbsp;</label>
                <%=Html.Label(Model.LastName != null ? Model.LastName : "")%>
            </div>
            <div class="row">
                <label for="MedicareNumber">
                    Medicare #:</label>
                <div class="inputs">
                    <%=Html.Label(Model.MedicareNumber != null ? Model.MedicareNumber : "")%>
                </div>
            </div>
            <div class="row">
                <label for="PatientIdNumber">
                    Patient Record #:</label>
                <div class="inputs">
                    <%=Html.Label(Model.PatientIdNumber != null ? Model.PatientIdNumber : "")%>
                </div>
            </div>
            <div class="row">
                <label for="Gender">
                    Gender (M0069):</label>
                <div class="inputs">
                    <%=Html.Label(Model.Gender!=null?Model.Gender:"")%>
                </div>
            </div>
            <div class="row">
                <label for="DOB">
                    Date of Birth (M0066):</label>
                <%=Html.Label(Model.DOB!=null?Model.DOB.ToShortDateString():"")%>
            </div>
            <div class="row">
                <label for="EpisodeStartDate">
                    Episode Start Date:</label>
                <div class="inputs">
                    <%=Html.Label(Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : "")%>
                </div>
            </div>
            <div class="row">
                <label for="StartOfCareDate">
                    Admission Date:</label>
                <div class="inputs">
                    <%=Html.Label(Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : "")%>
                </div>
            </div>
            <div class="row">
                <label for="AddressLine1">
                    Address Line 1:</label>
                <div class="inputs">
                    <%=Html.Label(Model.AddressLine1 != null ? Model.AddressLine1 : "")%>
                </div>
            </div>
            <div class="row">
                <label for="AddressLine2">
                    Address Line 2:</label>
                <div class="inputs">
                    <%=Html.Label(Model.AddressLine2 != null ? Model.AddressLine2 : "")%>
                </div>
            </div>
            <div class="row">
                <label for="AddressCity">
                    City:</label>
                <div class="inputs">
                    <%=Html.Label(Model.AddressCity != null ? Model.AddressCity : "")%>
                </div>
            </div>
            <div class="row">
                <label for="AddressStateCode">
                    State, Zip Code :</label>
                <div class="inputs">
                    <%=Html.Label((Model.AddressStateCode != null ? Model.AddressStateCode : "") +"," +(Model.AddressZipCode != null ? Model.AddressZipCode : ""))%>
                </div>
            </div>
        </div>
    </div>
    <div style="margin: 0px; width: 47%; float: left; padding-top: 10px; display: table;"
        class="window_main">
        <div style="display: block; width: 100%;">
            <div class="row">
                <label for="HippsCode">
                    HIPPS Code:</label>
                <div class="inputs">
                    <%=Html.Label(Model.HippsCode != null ? Model.HippsCode : "")%>
                </div>
            </div>
            <div class="row">
                <label for="ClaimKey">
                    Oasis Matching Key:</label>
                <div class="inputs">
                    <%=Html.Label(Model.ClaimKey != null ? Model.ClaimKey : "")%>
                </div>
            </div>
            <div class="row">
                <label for="FirstBillableVisitDate">
                    Date Of First Billable Visit:</label>
                <div class="inputs">
                    <%=Html.Label(Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : "")%>
                </div>
            </div>
            <div class="row">
                <label for="PhysicianLastName">
                    Physician Last Name, F.I.:</label>
                <div class="inputs">
                    <%=Html.Label(Model.PhysicianLastName != null ? Model.PhysicianLastName : "" + "-" + Model.PhysicianFirstName != null && Model.PhysicianFirstName != "" ? Model.PhysicianFirstName.Substring(0, 1) : "")%>
                </div>
            </div>
            <div class="row">
                <label for="PhysicianNPI">
                    Physician NPI #:</label>
                <div class="inputs">
                    <%=Html.Label(Model.PhysicianNPI != null ? Model.PhysicianNPI : "")%>
                </div>
            </div>
            <div class="row">
                <% 
                    var diganosis = XElement.Parse(Model.DiagonasisCode);
                    var val = (diganosis != null && diganosis.Element("code1") != null ? diganosis.Element("code1").Value : "");
                %>
                Diagonasis Codes:-
                <div style="width: 335px;">
                    <div class="row">
                        <div style="width: 33.33%; float: left;">
                            Primary
                            <%=Html.Label(diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Second
                            <%=Html.Label(diganosis != null && diganosis.Element("code2") != null ?Regex.Replace( diganosis.Element("code2").Value , @"[.]", ""): "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Third
                            <%=Html.Label(diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : "")%>
                        </div>
                    </div>
                    <div class="row">
                        <div style="width: 33.33%; float: left;">
                            Fourth
                            <%=Html.Label(diganosis != null && diganosis.Element("code4") != null ?Regex.Replace( diganosis.Element("code4").Value, @"[.]", "") : "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Fifth
                            <%=Html.Label(diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : "")%>
                        </div>
                        <div style="width: 33.33%; float: left;">
                            Sixth
                            <%=Html.Label(diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : "")%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                Remark:
                <div style="width: 335px;">
                    <%=Html.TextArea("FinalRemark", Model.Remark, new { @style = "width: 100%;" })%>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="claim">
            <thead>
                <tr>
                    <th>
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        HCPCS/HIIPS Code
                    </th>
                    <th>
                        Service Date
                    </th>
                    <th>
                        Service Unit
                    </th>
                    <th>
                        Total Charges
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="even">
                    <td>
                        0023
                    </td>
                    <td>
                        Home Health Services
                    </td>
                    <td>
                        <%=Model.HippsCode %>
                    </td>
                    <td>
                        <%=Model.EpisodeStartDate!=null?Model.EpisodeStartDate.ToShortDateString():"" %>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="odd">
                    <td>
                        0272
                    </td>
                    <td>
                        Service Supplies
                    </td>
                    <td>
                    </td>
                    <td>
                        <%=Model.EpisodeStartDate!=null?Model.EpisodeStartDate.ToShortDateString():"" %>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <% var visits = Model.VerifiedVisits.ToObject<List<ScheduleEvent>>().OrderBy(s => s.EventDate).ToList();
                   var count = 0;%>
                <% foreach (var visit in visits)
                   {%>
                <% if (count % 2 == 0)
                   {%>
                <tr class="even">
                    <%}
                   else
                   { %>
                    <tr class="odd">
                        <%} %>
                        <td>
                            <%if (visit.Discipline == "PT")
                              { %>
                            0421
                            <%}
                              else if (visit.Discipline == "OT")
                              { %>
                            0431
                            <%}
                              else if (visit.Discipline == "ST")
                              { %>
                            0440
                            <%}
                              else if (visit.Discipline == "Nursing")
                              { %>
                            0551
                            <%}
                              else if (visit.Discipline == "MSW")
                              { %>
                            0561
                            <%}
                              else if (visit.Discipline == "HHA")
                              { %>
                            0571
                            <%} %>
                        </td>
                        <td>
                            Visit
                        </td>
                        <td>
                            <%if (visit.Discipline == "PT")
                              { %>
                            G0151
                            <%}
                              else if (visit.Discipline == "OT")
                              { %>
                            G0152
                            <%}
                              else if (visit.Discipline == "ST")
                              { %>
                            G0153
                            <%}
                              else if (visit.Discipline == "Nursing")
                              { %>
                            G0154
                            <%}
                              else if (visit.Discipline == "MSW")
                              { %>
                            G0155
                            <%}
                              else if (visit.Discipline == "HHA")
                              { %>
                            G0156
                            <%} %>
                        </td>
                        <td>
                            <%=visit.EventDate%>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <% count++;
                   } %>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div style="text-align: center; vertical-align: middle; width: 100%;" class="buttons">
            <ul style="margin: 0px; width: 100%; text-align: center;">
                <li><span class="button orange_btn"><span><span>Back</span></span><input type="button"
                    name="" id=" " onclick="Billing.NavigateBack(2);" /></span></li>
                <li><span class="button send_form_btn"><span><span>Verify and Next</span></span><input
                    type="submit" name="" id="" onclick="Billing.Navigate(2,'#billingInfo');" /></span></li>
            </ul>
        </div>
    </div>
</fieldset>
