﻿namespace Axxess.Physician.App
{
    using System;
    using System.Web;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;

    using Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;

    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core;

    public static class Current
    {
        public static Func<DateTime> Time = () => DateTime.UtcNow;

        public static string AssemblyVersion
        {
            get
            {
                System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            }
        }

        public static AxxessPhysicianIdentity User
        {
            get
            {
                AxxessPhysicianIdentity identity = null;
                if (HttpContext.Current.User is WindowsIdentity)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessPhysicianPrincipal)
                {
                    AxxessPhysicianPrincipal principal = (AxxessPhysicianPrincipal)HttpContext.Current.User;
                    identity = (AxxessPhysicianIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DisplayName;
                }

                return string.Empty;
            }
        }

        //public static IDictionary<Guid, Guid> PhysicianIdentifiers
        //{
        //    get
        //    {
        //        if (User != null && User.Session != null)
        //        {
        //            return User.Session.AgencyPhysicianIdentifiers;
        //        }

        //        return new Dictionary<Guid, Guid>();
        //    }
        //}

        public static List<Guid> AgencyIds
        {
            get
            {
                if (User != null && User.Session != null && User.Session.Agencies!=null)
                {
                    return User.Session.Agencies.Keys.Where(s=>s.IsGuid()).Select(s=>s.ToGuid()).ToList();
                }

                return new List<Guid>();
            }
        }
        public static IDictionary<string,NameIdPair> Agencies
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.Agencies;
                }

                return new Dictionary<string, NameIdPair>();
            }
        }

        public static Guid DefaultAgencyId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DefaultAgencyId;
                }

                return Guid.Empty;
            }
        }

        public static string IpAddress
        {
            get
            {
                var request = HttpContext.Current.Request;
                return request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["REMOTE_ADDR"];
            }
        }

    }
}
