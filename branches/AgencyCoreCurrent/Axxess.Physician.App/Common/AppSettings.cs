﻿namespace Axxess.Physician.App {
    using System;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    public static class AppSettings {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();
        public static string PdfBasePath { get { return configuration.AppSettings["PdfBasePath"]; } }
        public static string XmlBasePath { get { return configuration.AppSettings["XmlBasePath"]; } }
        public static string PlanOfCare485Pdf { get { return configuration.AppSettings["PlanOfCare485Pdf"]; } }
        public static string PlanOfCare487Pdf { get { return configuration.AppSettings["PlanOfCare487Pdf"]; } }
        public static string PhysicianOrderPdf { get { return configuration.AppSettings["PhysicianOrderPdf"]; } }
        public static string PhysFaceToFacePdf { get { return configuration.AppSettings["PhysFaceToFacePdf"]; } }
        public static string PhysFaceToFaceXml { get { return configuration.AppSettings["PhysFaceToFaceXml"]; } }
        public static string MSWEvalPdf { get { return configuration.AppSettings["MSWEvalPdf"]; } }
        public static string MSWEvalXml { get { return configuration.AppSettings["MSWEvalXml"]; } }
        public static string OTEvalPdf { get { return configuration.AppSettings["OTEvalPdf"]; } }
        public static string OTEvalXml { get { return configuration.AppSettings["OTEvalXml"]; } }
        public static string OTEvalXml2 { get { return configuration.AppSettings["OTEvalXml2"]; } }
        public static string OTEvalXml3 { get { return configuration.AppSettings["OTEvalXml3"]; } }
        public static string OTEvalXml4 { get { return configuration.AppSettings["OTEvalXml4"]; } }
        public static string OTReassessmentXml { get { return configuration.AppSettings["OTReassessmentXml"]; } }
        public static string PTEvalPdf { get { return configuration.AppSettings["PTEvalPdf"]; } }
        public static string PTEvalXml { get { return configuration.AppSettings["PTEvalXml"]; } }
        public static string PTEvalXml2 { get { return configuration.AppSettings["PTEvalXml2"]; } }
        public static string PTEvalXml3 { get { return configuration.AppSettings["PTEvalXml3"]; } }
        public static string PTEvalXml4 { get { return configuration.AppSettings["PTEvalXml4"]; } }
        public static string PTEvalXml5 { get { return configuration.AppSettings["PTEvalXml5"]; } }
        public static string PTPOCXml { get { return configuration.AppSettings["PTPOCXml"]; } }
        public static string OTPOCXml { get { return configuration.AppSettings["OTPOCXml"]; } }
        public static string OTPOCXml2 { get { return configuration.AppSettings["OTPOCXml2"]; } }
        public static string STPOCXml { get { return configuration.AppSettings["STPOCXml"]; } }
        public static string PTReassessmentXml { get { return configuration.AppSettings["PTReassessmentXml"]; } }
        public static string STEvalPdf { get { return configuration.AppSettings["STEvalPdf"]; } }
        public static string POCPdf { get { return configuration.AppSettings["POCPdf"]; } }
        public static string STEvalXml { get { return configuration.AppSettings["STEvalXml"]; } }
        public static string STEvalXml3 { get { return configuration.AppSettings["STEvalXml3"]; } }
        public static string STDischargeXml2 { get { return configuration.AppSettings["STDischargeXml2"]; } }
        public static string SixtyDaySummaryPdf { get { return configuration.AppSettings["SixtyDaySummaryPdf"]; } }
        public static string SixtyDaySummaryXml { get { return configuration.AppSettings["SixtyDaySummaryXml"]; } }
        public static string PTDischargeXml { get { return configuration.AppSettings["PTDischargeXml"]; } }
        public static string PTDischargeXml2 { get { return configuration.AppSettings["PTDischargeXml2"]; } }
        public static string TherapyPdf { get { return configuration.AppSettings["TherapyPdf"]; } }
        public static string PsychAssessmentPdf { get { return configuration.AppSettings["PsychAssessmentPdf"]; } }
        public static string PsychAssessmentXml2 { get { return configuration.AppSettings["PsychAssessmentXml2"]; } }
        public static bool UseMinifiedJs { get { return configuration.AppSettings["UseMinifiedJs"].ToBoolean(); } }
    }
}
