﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement;

    public class PhysicianService : IPhysicianService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagmentDataProvider, IOasisCDataProvider oasisCDataProvider)
        {
            Check.Argument.IsNotNull(oasisCDataProvider, "oasisCDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.planofCareRepository = oasisCDataProvider.PlanofCareRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
            this.physicianRepository = agencyManagmentDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
        }
        
        #endregion

        #region IPhysicianService Members

        public List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var faceToFaceEncounter = new List<FaceToFaceEncounter>();
           var agencies = Current.Agencies;
           if (agencies.ContainsKey(agencyId.ToString()))
           {
               var agency = agencies[agencyId.ToString()];
               if (agency != null)
               {
                   var physicianId = agency.Id;
                   var agencyName = agency.Name;
                   faceToFaceEncounter = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(agencyId, physicianId, new int[] { (int)ScheduleStatus.OrderSavedByPhysician, (int)ScheduleStatus.OrderSentToPhysicianElectronically }, startDate, endDate);
                   faceToFaceEncounter.ForEach(ffe =>
                   {
                       ffe.AgencyDisplayName = agencyName;
                       ffe.PrintUrl = Url.Print(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true);
                   });
               }
           }
            return faceToFaceEncounter;
        }

        public List<CarePlanOversight> GetPhysicianCPO(Guid agencyId,DateTime startDate, DateTime endDate)
        {
            var cpos = physicianRepository.GetCPO(agencyId, Current.LoginId,startDate,endDate);
            var agencyName = AgencyEngine.Get(agencyId).Name;
            if (cpos != null)
            {
                cpos.OrderBy(c => c.LogDateFormatted)
                    .ForEach(cpo =>
                    {
                        cpo.AgencyName = agencyName;//agencyRepository.GetAgencyOnly(cpo.AgencyId).Name;
                        cpo.PatientName = patientRepository.GetPatientNameById(cpo.PatientId, cpo.AgencyId);
                    });
            }
            return cpos;
        }

        public bool SaveCPO(CarePlanOversight cpo)
        {
            if (patientRepository.UpdateCPO(cpo))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Order> GetOrders(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var agencies = Current.Agencies;
            if (agencies.ContainsKey(agencyId.ToString()))
            {
                var agency = agencies[agencyId.ToString()];
                if (agency != null)
                {
                    var agencyName = agency.Name;// AgencyEngine.Get(agencyId).Name;
                    var physicianId = agency.Id;
                    var evalOrders = patientRepository.GetEvalOrdersByPhysician(agencyId, physicianId, startDate, endDate, new int[] { (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.OrderSentToPhysicianElectronically });
                    evalOrders.ForEach(eval =>
                    {
                        var orderDate = GetNoteVisitDateFromSchedule(eval.Schedule, eval.Id);
                        if (orderDate >= startDate && orderDate <= endDate)
                        {
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                Type = GetOrderType(eval.NoteType),
                                Number = eval.OrderNumber,
                                Status = eval.Status,
                                AgencyName = agencyName,
                                PrintUrl = Url.Print(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, GetOrderType(eval.NoteType), false),
                                PatientName = eval.PatientName,
                                CreatedDate = orderDate.ToString("MM/dd/yyyy")
                            });
                        }
                    });

                    var physicianOrders = patientRepository.GetPhysicianOrdersByPhysicianAndDate(agencyId, physicianId, (int)ScheduleStatus.OrderSentToPhysicianElectronically, startDate, endDate);
                    physicianOrders.ForEach(po =>
                    {
                        orders.Add(new Order
                        {
                            Id = po.Id,
                            Status = po.Status,
                            Number = po.OrderNumber,
                            Type = OrderType.PhysicianOrder,
                            PrintUrl = Url.Print(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, false),
                            PatientName = po.PatientName, 
                            AgencyName = agencyName,
                            CreatedDate = po.OrderDateFormatted
                        });
                    });

                    var planofCareOrders = planofCareRepository.GetByPhysicianIdAndDate(agencyId, physicianId, (int)ScheduleStatus.OrderSentToPhysicianElectronically, startDate, endDate);
                    planofCareOrders.ForEach(poc =>
                    {
                        orders.Add(new Order
                        {
                            Id = poc.Id,
                            Type = OrderType.HCFA485,
                            Number = poc.OrderNumber,
                            Status = poc.Status,
                            PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, false),
                            AgencyName = agencyName,
                            PatientName = poc.PatientName,
                            CreatedDate = poc.OrderDateFormatted
                        });
                    });

                    var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianIdAndDate(agencyId, physicianId, (int)ScheduleStatus.OrderSentToPhysicianElectronically, startDate, endDate);
                    planofCareStandAloneOrders.ForEach(poc =>
                    {
                        orders.Add(new Order
                        {
                            Id = poc.Id,
                            Type = OrderType.HCFA485StandAlone,
                            Number = poc.OrderNumber,
                            Status = poc.Status,
                            AgencyName = agencyName,
                            PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, false),
                            PatientName = poc.PatientName,  
                            CreatedDate = poc.OrderDateFormatted
                        });
                    });



                    List<FaceToFaceEncounter> facetofaceOrder = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(agencyId, physicianId, new int[] { (int)ScheduleStatus.OrderSavedByPhysician, (int)ScheduleStatus.OrderSentToPhysicianElectronically }, startDate, endDate);
                    facetofaceOrder.ForEach(ftf =>
                    {
                        orders.Add(new Order
                        {
                            Id = ftf.Id,
                            Type = OrderType.FaceToFaceEncounter,
                            Number = ftf.OrderNumber,
                            Status = ftf.Status,
                            AgencyName = agencyName,
                            PrintUrl = Url.Print(ftf.Id, ftf.PatientId, ftf.EpisodeId, ftf.AgencyId, OrderType.FaceToFaceEncounter, false),
                            PatientName = ftf.PatientName,
                            CreatedDate = ftf.RequestDate.ToString("MM/dd/yyyy")
                        });
                    });

                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }
        private DateTime GetNoteVisitDate(string note)
        {
            if (note.IsNotNullOrEmpty())
            {
                XElement rootElement = XElement.Parse(note);
                IDictionary<string, NotesQuestion> dict = new Dictionary<string, NotesQuestion>();
                foreach (XElement el in rootElement.Elements())
                {
                    if (el.Attribute("Name") != null && el.Attribute("Answer") != null && el.Attribute("Type") != null)
                    {
                        dict.Add(el.Attribute("Name").Value, new NotesQuestion { Name = el.Attribute("Name").Value, Answer = el.Attribute("Answer").Value, Type = el.Attribute("Type").Value });
                    }
                }
                if (dict != null && dict.ContainsKey("VisitDate"))
                {
                    return dict["VisitDate"].Answer.ToDateTime();
                }
                else
                    return DateTime.MinValue;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private DateTime GetNoteVisitDateFromSchedule(string schedule, Guid eventId)
        {
            if (schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = schedule.ToObject<List<ScheduleEvent>>();
                if (scheduleEvents.IsNotNullOrEmpty())
                {
                    var scheduleEvent = scheduleEvents.FirstOrDefault(s => s.EventId == eventId);
                    if (scheduleEvent != null)
                    {
                        return scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate.ToDateTime() : (scheduleEvent.EventDate.IsValidDate()? scheduleEvent.EventDate.ToDateTime():DateTime.MinValue);
                    }
                }
            }
            return DateTime.MinValue;
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTEvaluation":
                        orderType = OrderType.PtEvaluation;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "PTReassessment":
                        orderType = OrderType.PTReassessment;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "OTReassessment":
                        orderType = OrderType.OTReassessment;
                        break;
                    case "OTDischarge":
                        orderType = OrderType.OTDischarge;
                        break;
                    case "STDischarge":
                        orderType = OrderType.STDischarge;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType = OrderType.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "MSWDischarge":
                        orderType = OrderType.MSWDischarge;
                        break;
                    case "PTPlanOfCare":
                        orderType = OrderType.PTPlanOfCare;
                        break;
                    case "OTPlanOfCare":
                        orderType = OrderType.OTPlanOfCare;
                        break;
                    case "STPlanOfCare":
                        orderType = OrderType.STPlanOfCare;
                        break;
                    case "SNPsychAssessment":
                        orderType = OrderType.SNPsychAssessment;
                        break;

                }
            }
            return orderType;
        }

        public string GetReturnComments(Guid agencyId, Guid eventId, Guid episodeId, Guid patientId)
        {
            string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, agencyId);
            List<ReturnComment> NewComments = patientRepository.GetReturnComments(agencyId, episodeId, eventId);
            foreach (ReturnComment comment in NewComments)
            {
                if (comment.IsDeprecated) continue;
                if (CommentString.IsNotNullOrEmpty()) CommentString += "<hr/>";
                if (comment.UserId == Current.User.Id) CommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                CommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", UserEngine.GetName(comment.UserId, agencyId), comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return CommentString;
        }

        public bool AddReturnComments(Guid eventId, Guid episodeId, string comment, Guid agencyId)
        {
            return patientRepository.AddReturnComment(new ReturnComment
            {
                AgencyId = agencyId,
                Comments = comment,
                Created = DateTime.Now,
                EpisodeId = episodeId,
                EventId = eventId,
                Modified = DateTime.Now,
                UserId = Current.User.Id
            });
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetOrdersCompleted(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
             var agencies = Current.Agencies;
             if (agencies.ContainsKey(agencyId.ToString()))
             {
                 var agency = agencies[agencyId.ToString()];
                 if (agency != null)
                 {
                     var agencyName = agency.Name;
                     var physicianId = agency.Id;

                     var evalOrders = patientRepository.GetEvalOrdersByPhysician(agencyId, physicianId, startDate, endDate, new int[] { (int)ScheduleStatus.EvalReturnedWPhysicianSignature, (int)ScheduleStatus.OrderReturnedWPhysicianSignature });
                     evalOrders.ForEach(eval =>
                     {
                         var orderDate = GetNoteVisitDateFromSchedule(eval.Schedule, eval.Id);
                         if (orderDate >= startDate && orderDate <= endDate)
                         {
                             orders.Add(new Order
                             {
                                 Id = eval.Id,
                                 Type = GetOrderType(eval.NoteType),
                                 Number = eval.OrderNumber,
                                 AgencyName = agencyName,
                                 PrintUrl = Url.View(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, GetOrderType(eval.NoteType), true),
                                 PatientName = eval.PatientName,
                                 CreatedDate = orderDate.ToString("MM/dd/yyyy")
                             });
                         }
                     });

                     var physicianOrders = patientRepository.GetPhysicianOrdersByPhysicianAndDate(agencyId, physicianId, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
                     physicianOrders.ForEach(po =>
                     {
                         orders.Add(new Order
                         {
                             Id = po.Id,
                             Number = po.OrderNumber,
                             Type = OrderType.PhysicianOrder,
                             PrintUrl = Url.View(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, true),
                             PatientName = po.PatientName,
                             AgencyName = agencyName,
                             CreatedDate = po.OrderDateFormatted
                         });
                     });

                     var planofCareOrders = planofCareRepository.GetByPhysicianIdAndDate(agencyId, physicianId, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
                     planofCareOrders.ForEach(poc =>
                     {
                         orders.Add(new Order
                         {
                             Id = poc.Id,
                             Type = OrderType.HCFA485,
                             Number = poc.OrderNumber,
                             PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, true),
                             AgencyName = agencyName,
                             PatientName = poc.PatientName,
                             CreatedDate = poc.OrderDateFormatted
                         });
                     });

                     var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianIdAndDate(agencyId, physicianId, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
                     planofCareStandAloneOrders.ForEach(poc =>
                     {
                         orders.Add(new Order
                         {
                             Id = poc.Id,
                             Type = OrderType.HCFA485StandAlone,
                             Number = poc.OrderNumber,
                             AgencyName = agencyName,
                             PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, true),
                             PatientName = poc.PatientName,
                             CreatedDate = poc.OrderDateFormatted
                         });
                     });

                     var facetofaceOrders = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(agencyId, physicianId, new int[] { (int)ScheduleStatus.OrderReturnedWPhysicianSignature }, startDate, endDate);
                     facetofaceOrders.ForEach(ffe =>
                     {
                         orders.Add(new Order
                         {
                             Id = ffe.Id,
                             Type = OrderType.FaceToFaceEncounter,
                             Number = ffe.OrderNumber,
                             PatientName = ffe.PatientName,
                             AgencyName = agencyName,
                             PrintUrl = Url.View(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true),
                             CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
                         });
                     });

                 }
             }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

       

        public bool UpdatePhysicianOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PhysicianOrder physicianOrder = null;

            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, orderId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, orderId);
                }

                physicianOrder = patientRepository.GetOrder(orderId, patientId, agencyId);
                if (physicianOrder != null)
                {
                    if (actionType == "Approve")
                    {
                        physicianOrder.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        physicianOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        physicianOrder.PhysicianSignatureDate = DateTime.Now;
                        physicianOrder.ReceivedDate = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        physicianOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(agencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            FaceToFaceEncounter facetoFace = null;

            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, orderId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, orderId);
                }

                facetoFace = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId);
                if (facetoFace != null)
                {
                    if (actionType == "Approve")
                    {
                        facetoFace.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        facetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        facetoFace.SignatureDate = DateTime.Now;
                        facetoFace.ReceivedDate = DateTime.Now;
                        if (patientRepository.UpdateFaceToFaceEncounter(facetoFace))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        facetoFace.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        facetoFace.Modified = DateTime.Now;
                        if (patientRepository.UpdateFaceToFaceEncounter(facetoFace))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(agencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter order)
        {
            var result = false;
            FaceToFaceEncounter existingFacetoFace = patientRepository.GetFaceToFaceEncounter(order.Id, order.PatientId, order.AgencyId);

            if (existingFacetoFace != null)
            {
                existingFacetoFace.Status = order.Status;
                existingFacetoFace.Modified = DateTime.Now;
                existingFacetoFace.Services = order.Services;
                existingFacetoFace.ServicesOther = order.ServicesOther;
                existingFacetoFace.Certification = order.Certification;
                existingFacetoFace.EncounterDate = order.EncounterDate;
                existingFacetoFace.MedicalReason = order.MedicalReason;
                existingFacetoFace.ClinicalFinding = order.ClinicalFinding;

                if (order.ServicesArray != null && order.ServicesArray.Count > 0)
                {
                    existingFacetoFace.Services = order.ServicesArray.ToArray().AddColons();
                }

                if (existingFacetoFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                {
                    existingFacetoFace.ReceivedDate = DateTime.Now;
                    existingFacetoFace.SignatureDate = DateTime.Now;
                    existingFacetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                }
                else
                {
                    existingFacetoFace.SignatureText = string.Empty;
                }

                if (patientRepository.UpdateFaceToFaceEncounter(existingFacetoFace))
                {
                    var scheduleEvent = patientRepository.GetSchedule(existingFacetoFace.AgencyId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                    if (scheduleEvent != null)
                    {
                        scheduleEvent.Status = existingFacetoFace.Status.ToString();
                        if (patientRepository.UpdateEpisode(existingFacetoFace.AgencyId, scheduleEvent))
                        {
                            var userEvent = userRepository.GetEvent(existingFacetoFace.AgencyId, scheduleEvent.UserId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                            if (userEvent != null)
                            {
                                userEvent.Status = scheduleEvent.Status;
                                userEvent.ReturnReason = scheduleEvent.ReturnReason;
                                userRepository.UpdateEvent(existingFacetoFace.AgencyId, userEvent);
                                result = true;
                            }
                            else
                            {
                                userRepository.AddUserEvent(existingFacetoFace.AgencyId, scheduleEvent.PatientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateEvalOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid evalId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PatientVisitNote evalOrder = null;

            if (!evalId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, evalId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, evalId);
                }
                bool poc = false;
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STPlanOfCare)
                {
                    poc = true;
                }
                evalOrder = patientRepository.GetVisitNote(agencyId, patientId, evalId);
                if (evalOrder != null)
                {
                    if (actionType == "Approve")
                    {
                        evalOrder.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                        evalOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        evalOrder.PhysicianSignatureDate = DateTime.Now;
                        evalOrder.ReceivedDate = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(evalOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                if (poc)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                }
                                else
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                                }
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    if (poc)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    }
                                    else
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                                    }
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        evalOrder.Status = ((int)ScheduleStatus.EvalReturnedByPhysician);
                        evalOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(evalOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                if (poc)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                }
                                else
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.EvalReturnedByPhysician).ToString();
                                }
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    if (poc)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    }
                                    else
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.EvalReturnedByPhysician).ToString();
                                    }
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(agencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
