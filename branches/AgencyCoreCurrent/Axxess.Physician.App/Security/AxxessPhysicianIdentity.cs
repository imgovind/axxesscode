﻿namespace Axxess.Physician.App.Security
{
    using System;
    using System.Security.Principal;

    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;

    using Axxess.AgencyManagement.Domain;

    [Serializable]
    public class AxxessPhysicianIdentity : IIdentity
    {
        #region Constructor

        public AxxessPhysicianIdentity()
        {
            this.IsAuthenticated = true;
            this.Session = new PhysicianSession();
            this.LastSecureActivity = DateTime.Now;
        }

        public AxxessPhysicianIdentity(Guid id, string name) : this()
        {
            this.Id = id;
            this.Name = name;
        }

        #endregion

        #region IIdentity Members

        public string Name { get; private set; }

        public bool IsAuthenticated { get; set; }

        public PhysicianSession Session { get; set; }

        public string AuthenticationType
        {
            get { return PhysicianAppSettings.AuthenticationType; }
        }

        #endregion

        #region Axxess Identity Members

        public Guid Id { get; private set; }

        public DateTime LastSecureActivity { get; set; }

        public bool IsImpersonated { get; set; }

        #endregion

    }
}
