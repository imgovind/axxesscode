﻿namespace Axxess.Physician.App.Security
{
    public interface IFormsAuthenticationService
    {
        void SignOut();
        string LoginUrl { get; }
        void SignIn(string userName, bool rememberMe);
    }
}
