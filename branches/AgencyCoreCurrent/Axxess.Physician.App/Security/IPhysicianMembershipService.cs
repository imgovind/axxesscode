﻿namespace Axxess.Physician.App.Security
{
    using System;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    
    using Security;

    public interface IPhysicianMembershipService
    {
        void LogOff(string userName);
        bool Activate(Account account);
        AxxessPhysicianPrincipal Get(string userName);
        ResetAttemptType Validate(string userName);
        LoginAttemptType Validate(string userName, string password);
        bool ResetPassword(string userName);
        bool UpdatePassword(Account account);
        bool Impersonate(Guid linkId);
    }
}
