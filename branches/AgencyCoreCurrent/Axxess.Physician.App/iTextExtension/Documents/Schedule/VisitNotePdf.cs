﻿namespace Axxess.Physician.App.iTextExtension
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.Physician.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.Physician.App.iTextExtension.XmlParsing;
    using Axxess.Physician.App.Enums;
    abstract class VisitNotePdf : AxxessPdf {
        private VisitNoteXml xml;
        protected String DocType;
        public VisitNotePdf(VisitNoteViewData data) : base() {
            PdfDoc type;
            switch (data.Type) {
                case "MSWAssessment": case "MSWDischarge": case "MSWEvaluationAssessment":
                    type = PdfDocs.MSWEval;
                    this.Init(data, type);
                    break;
                case "PTEvaluation": case "PTReEvaluation": case "PTMaintenance":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTEval2;
                    }
                    else if (data.Version == 3)
                    {
                        type = PdfDocs.PTEval3;
                    }
                    else if (data.Version == 4)
                    {
                        type = PdfDocs.PTEval4;
                    }
                    else if (data.Version == 5)
                    {
                        type = PdfDocs.PTEval5;
                    }
                    else
                    {
                        type = PdfDocs.PTEval;
                    }
                    this.Init(data, type);
                    break;
                case "OTEvaluation": case "OTReEvaluation": case "OTDischarge": case "OTMaintenance":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.OTEval2;
                    }
                    else if (data.Version == 3)
                    {
                        type = PdfDocs.OTEval3;
                    }
                    else if (data.Version == 4)
                    {
                        type = PdfDocs.OTEval4;
                    }
                    else
                    {
                        type = PdfDocs.OTEval;
                    }
                    this.Init(data, type);
                    break;
                case "STEvaluation": case "STReEvaluation": case "STMaintenance":
                    if (data.Version == 3)
                    {
                        type = PdfDocs.STEval3;
                    }
                    else
                    {
                        type = PdfDocs.STEval;
                    }
                    this.Init(data, type);
                    break;
                case "PTReassessment":
                    type = PdfDocs.PTReassessment;
                    this.Init(data, type);
                    break;
                case "OTReassessment":
                    type = PdfDocs.OTReassessment;
                    this.Init(data, type);
                    break;
                case "PTPlanOfCare":
                    type = PdfDocs.PTPOC;
                    this.Init(data, type);
                    break;
                case "OTPlanOfCare":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.OTPOC2;
                    }
                    else
                    {
                        type = PdfDocs.OTPOC;
                    }
                    this.Init(data, type);
                    break;
                case "STPlanOfCare":
                    type = PdfDocs.STPOC;
                    this.Init(data, type);
                    break;
                case "PTDischarge":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTDischarge2;
                    }
                    else
                    {
                        type = PdfDocs.PTDischarge;
                    }
                    this.Init(data, type);
                    break;
                case "STDischarge":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STDischarge2;
                    }
                    else
                    {
                        type = PdfDocs.STEval;
                    }
                    this.Init(data, type);
                    break;
            }
        }
        public VisitNotePdf(VisitNoteViewData data, PdfDoc type) : base() {
            this.Init(data, type);
        }
        private void Init(VisitNoteViewData data, PdfDoc type) {
            this.xml = new VisitNoteXml(data, type);
            this.SetType(type);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.Content(this.xml));
            this.SetMargins(this.Margins(data));
            this.SetFields(this.FieldMap(data));
        }
        protected virtual IElement[] Content(VisitNoteXml xml) {
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            return content;
        }
        protected virtual List<Dictionary<String, String>> FieldMap(VisitNoteViewData data) {
            return new List<Dictionary<string, string>>();
        }
        protected virtual float[] Margins(VisitNoteViewData data) {
            return new float[] { 0, 0, 0, 0 };
        }
    }
}