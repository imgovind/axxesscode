﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Linq.Expressions;
    using System.Collections.Generic;

    public interface IDatabase
    {
        string ConnectionString { get; }

        bool Execute<T>(string sql) where T : class, new();

        bool Insert<T>(T entity) where T : class, new();
        bool Update<T>(T entity) where T : class, new();
        bool Delete<T>(T entity) where T : class, new();

        T Get<T>(string sql) where T : class, new();
        IList<T> Select<T>(string sql) where T : class, new();

        T Get<T>(Expression<Func<T, bool>> expression) where T : class, new();
        IList<T> Select<T>(Expression<Func<T, bool>> expression) where T : class, new();
    }
}
