﻿namespace Axxess.Data
{
    using System;
    using System.Collections.Generic;

    public interface IReadOnlyRepository<TEntity> where TEntity : class, new()
    {
        TEntity Single(string sql);
        IList<TEntity> Find(string sql);
        string ConnectionName { get; set; }
    }
}
