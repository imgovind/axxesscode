﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Collections.Generic;

    using Axxess.Accounts.Entities;

    public interface IAccountRepository : IRepository
    {
        Login GetById(Guid id);
        Login GetByEmailAddress(string emailAddress);
        List<UserAccount> GetUserAccounts(Guid loginId);
        UserAccount GetUserAccount(Guid userId, Guid agencyId);
        bool UpdatePasswordAndHash(Guid id, string hash, string salt);
        bool UpdateSignatureAndHash(Guid id, string hash, string salt);
        bool UpdatePasswordAndSignature(Guid id, string hash, string salt);
    }
}
