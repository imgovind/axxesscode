﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Linq.Expressions;
    using System.Collections.Generic;

    public interface IRepository
    {
        bool Add<T>(T entity) where T : class, new();
        bool Update<T>(T entity) where T : class, new();
        void AddMany<T>(IEnumerable<T> entity) where T : class, new();

        T Get<T>(string sql) where T : class, new();
        T Get<T>(Expression<Func<T, bool>> expression) where T : class, new();

        IList<T> Find<T>(string sql) where T : class, new();
        IList<T> Find<T>(Expression<Func<T, bool>> expression) where T : class, new();
    }
}
