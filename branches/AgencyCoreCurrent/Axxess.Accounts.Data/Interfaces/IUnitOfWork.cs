﻿namespace Axxess.Data
{
    using System;
    using System.Collections.Generic;

    public interface IUnitOfWork : IDisposable
    {
        ITransaction BeginTransaction();
        void EndTransaction(ITransaction transaction);
    }
}
