﻿namespace Axxess.Data
{
    using System;
    public interface IUnitOfWorkFactory : IDisposable
    {
        void EndUnitOfWork(IUnitOfWork unitOfWork);
        IUnitOfWork BeginUnitOfWork(string connectionStringName);
    }
}
