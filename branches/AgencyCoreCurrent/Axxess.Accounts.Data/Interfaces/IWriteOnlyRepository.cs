﻿namespace Axxess.Data
{
    using System;
    using System.Collections.Generic;

    public interface IWriteOnlyRepository<TEntity> where TEntity : class, new()
    {
        bool Add(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(TEntity entity);
    }
}
