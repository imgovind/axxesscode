﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Accounts.Entities;
    using Axxess.Accounts.Entities.Enums;

    using Axxess.Framework.Extensions;
    using Axxess.Framework.Extensibility;

    public class AccountRepository : BaseRepository, IAccountRepository
    {
        #region Constructor

        public AccountRepository(string connectionName) : base(connectionName) { }

        #endregion

        #region IAccountRepository Implementation

        public Login GetById(Guid id)
        {
            return this.Database.Get<Login>(string.Format("SELECT * FROM logins WHERE ID = '{0}' LIMIT 0, 1;", id));
        }

        public Login GetByEmailAddress(string emailAddress)
        {
            return this.Database.Get<Login>(string.Format("SELECT * FROM logins WHERE EMAILADDRESS = '{0}' LIMIT 0, 1;", emailAddress));
        }

        public List<UserAccount> GetUserAccounts(Guid loginId)
        {
            var statement = new StringBuilder()
            .Append("SELECT agencysnapshots.Id as AgencyId, agencysnapshots.Name as AgencyName, agencysnapshots.ClusterId, ")
            .Append("agencysnapshots.IsSuspended, agencysnapshots.IsFrozen, agencysnapshots.FrozenDate, ")
            .Append("usersnapshots.LoginId as LoginId, usersnapshots.Id as UserId, usersnapshots.Created, usersnapshots.TitleType as Title, usersnapshots.Status ")
            .Append("FROM usersnapshots INNER JOIN agencysnapshots on usersnapshots.AgencyId = agencysnapshots.Id ")
            .AppendFormat("WHERE usersnapshots.LoginId = '{0}' AND usersnapshots.IsDeprecated = 0 AND usersnapshots.Status = 1 ", loginId)
            .Append("AND agencysnapshots.IsDeprecated = 0 ORDER BY agencysnapshots.Name ASC;")
            .ToString();
            return this.Database.Select<UserAccount>(statement).ToList();
        }

        public UserAccount GetUserAccount(Guid userId, Guid agencyId)
        {
            var statement = new StringBuilder()
            .Append("SELECT agencysnapshots.Id as AgencyId, agencysnapshots.Name as AgencyName, agencysnapshots.ClusterId, ")
            .Append("agencysnapshots.IsSuspended, agencysnapshots.IsFrozen, agencysnapshots.FrozenDate, ")
            .Append("usersnapshots.LoginId as LoginId, usersnapshots.Id as UserId, usersnapshots.Created, usersnapshots.TitleType as Title, usersnapshots.Status ")
            .Append("FROM usersnapshots INNER JOIN agencysnapshots on usersnapshots.AgencyId = agencysnapshots.Id ")
            .AppendFormat("WHERE usersnapshots.Id = '{0}' AND agencysnapshots.Id = '{1}' AND usersnapshots.IsDeprecated = 0 AND usersnapshots.Status = 1 ", userId, agencyId)
            .Append("AND agencysnapshots.IsDeprecated = 0 ORDER BY agencysnapshots.Name ASC;")
            .ToString();
            return this.Database.Get<UserAccount>(statement);
        }

        public bool UpdatePasswordAndHash(Guid id, string hash, string salt)
        {
            var statement = new StringBuilder()
                .AppendFormat("UPDATE logins SET PasswordHash = '{0}', PasswordSalt = '{1}' ", hash, salt)
                .AppendFormat("WHERE Id = '{0}';", id)
                .ToString();

            return this.Database.Execute<Login>(statement);
        }

        public bool UpdateSignatureAndHash(Guid id, string hash, string salt)
        {
            var statement = new StringBuilder()
                .AppendFormat("UPDATE logins SET SignatureHash = '{0}', SignatureSalt = '{1}' ", hash, salt)
                .AppendFormat("WHERE Id = '{0}';", id)
                .ToString();

            return this.Database.Execute<Login>(statement);
        }

        public bool UpdatePasswordAndSignature(Guid id, string hash, string salt)
        {
            var statement = new StringBuilder()
                .AppendFormat("UPDATE logins SET PasswordHash = '{0}', PasswordSalt = '{1}', ", hash, salt)
                .AppendFormat("SignatureHash = '{0}', SignatureSalt = '{1}' WHERE Id = '{2}';", hash, salt, id)
                .ToString();

            return this.Database.Execute<Login>(statement);
        }

        #endregion
    }
}
