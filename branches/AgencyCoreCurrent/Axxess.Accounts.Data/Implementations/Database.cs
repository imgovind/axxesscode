﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Collections.Generic;

    using Axxess.Framework.Validation;
    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Extensions;
    using Axxess.Framework.Configuration;
    using Axxess.Framework.Extensibility;

    using MySql.Data.MySqlClient;

    public class Database : IDatabase
    {
        public string ConnectionString { get; private set; }
        public Database(string connectionName)
        {
            Check.Argument.IsNotEmpty(connectionName, "connectionName");

            this.ConnectionString = Container.Resolve<IWebConfiguration>().ConnectionStrings(connectionName);
        }

        public bool Execute<T>(string sql) where T : class, new()
        {
            using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
            {
                return connection.Execute(sql);
            }
        }

        public bool Insert<T>(T entity) where T : class, new()
        {
            if (entity != null)
            {
                using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
                {
                    return connection.Insert(entity);
                }
            }
            return false;
        }

        public bool Update<T>(T entity) where T : class, new()
        {
            if (entity != null)
            {
                using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
                {
                    return connection.Update(entity);
                }
            }
            return false;
        }

        public bool Delete<T>(T entity) where T : class, new()
        {
            if (entity != null)
            {
                using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
                {
                    return connection.Delete(entity);
                }
            }
            return false;
        }

        public T Get<T>(string sql) where T : class, new()
        {
            using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
            {
                return connection.Select(sql).ToList().FirstOrDefault();
            }
        }

        public IList<T> Select<T>(string sql) where T : class, new()
        {
            using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
            {
                return connection.Select(sql).ToList();
            }
        }

        public T Get<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
            {
                QueryResult result = DynamicQuery.GetDynamicQuery(typeof(T).Name.ToLowerInvariant().Pluralize(), expression);
                return connection.Select(result.Statement, result.Parameters).ToList().FirstOrDefault();
            }
        }

        public IList<T> Select<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            using (var connection = new Connection<T>(new MySqlConnection(this.ConnectionString)))
            {
                QueryResult result = DynamicQuery.GetDynamicQuery(typeof(T).Name.ToLowerInvariant().Pluralize(), expression);
                return connection.Select(result.Statement, result.Parameters).ToList();
            }
        }
    }
}
