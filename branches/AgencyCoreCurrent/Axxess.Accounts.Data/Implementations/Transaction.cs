﻿namespace Axxess.Data
{
    using System;
    using System.Transactions;

    public class Transaction : ITransaction
    {
        public DatabaseUnitOfWork UnitOfWork { get; private set; }
        public TransactionScope TransactionScope { get; private set; }

        public Transaction(DatabaseUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.TransactionScope = new TransactionScope();
        }

        public void Commit()
        {
            this.TransactionScope.Complete();
        }

        public void Rollback()
        {
        }

        public void Dispose()
        {
            if (this.TransactionScope != null)
            {
                (this.TransactionScope as IDisposable).Dispose();
                this.TransactionScope = null;
                this.UnitOfWork = null;
            }
        }
    }
}
