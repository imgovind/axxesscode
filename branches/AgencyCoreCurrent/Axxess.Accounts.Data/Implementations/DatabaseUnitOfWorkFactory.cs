﻿namespace Axxess.Data
{
    using System;

    using MySql.Data.MySqlClient;

    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Configuration;
    using Axxess.Framework.Extensibility;

    public sealed class DatabaseUnitOfWorkFactory : IUnitOfWorkFactory
    {
        public DatabaseUnitOfWorkFactory() { }

        private string connectionStringName { get; set; }

        public IUnitOfWork BeginUnitOfWork(string connectionStringName)
        {
            this.connectionStringName = connectionStringName;
            return new DatabaseUnitOfWork(
                this.CreateConnection()
                );
        }

        private Connection CreateConnection()
        {
            return new Connection(
                new MySqlConnection(Container.Resolve<IWebConfiguration>().ConnectionStrings(this.connectionStringName))
                );
        }

        public void EndUnitOfWork(IUnitOfWork unitOfWork)
        {
            var databaseUnitOfWork = unitOfWork as DatabaseUnitOfWork;
            if (databaseUnitOfWork != null)
            {
                databaseUnitOfWork.Dispose();
                databaseUnitOfWork = null;
            }
        }

        public void Dispose()
        {
            this.connectionStringName = null;
        }
    }
}
