﻿namespace Axxess.Data
{
    using System;
    using System.Collections.Generic;

    using Axxess.Framework.Validation;

    public class WriteOnlyRepository<TEntity> : IWriteOnlyRepository<TEntity> where TEntity : class, new()
    {
        protected IUnitOfWork UnitOfWork { get; private set; }

        public WriteOnlyRepository(IUnitOfWork unitOfWork)
        {
            Check.Argument.IsNotNull(unitOfWork, "unitOfWork");

            this.UnitOfWork = unitOfWork;
        }

        public virtual bool Add(TEntity entity)
        {
            return this.UnitOfWork.Insert<TEntity>(entity);
        }

        public virtual bool Update(TEntity entity)
        {
            return this.UnitOfWork.Update<TEntity>(entity);
        }

        public virtual bool Delete(TEntity entity)
        {
            return this.UnitOfWork.Delete<TEntity>(entity);
        }
    }
}
