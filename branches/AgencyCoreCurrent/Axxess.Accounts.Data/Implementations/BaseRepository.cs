﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Linq.Expressions;
    using System.Collections.Generic;

    using Axxess.Framework.Validation;
    using Axxess.Framework.Extensions;

    public abstract class BaseRepository : IRepository
    {
        public IDatabase Database { get; private set; }

        public BaseRepository(string connectionName)
        {
            Check.Argument.IsNotEmpty(connectionName, "connectionName");

            this.Database = new Database(connectionName);
        }

        public virtual bool Add<T>(T entity) where T : class, new()
        {
            return this.Database.Insert<T>(entity);
        }

        public virtual bool Update<T>(T entity) where T : class, new()
        {
            return this.Database.Update<T>(entity);
        }

        public void AddMany<T>(IEnumerable<T> list) where T : class, new()
        {
            list.ForEach(item =>
            {
                this.Database.Insert<T>(item);
            });
        }

        public virtual T Get<T>(string sql) where T : class, new()
        {
            return this.Database.Get<T>(sql);
        }

        public virtual IList<T> Find<T>(string sql) where T : class, new()
        {
            return this.Database.Select<T>(sql);
        }

        public virtual T Get<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            return this.Database.Get<T>(expression);
        }

        public virtual IList<T> Find<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            return this.Database.Select<T>(expression);
        }
    }
}
