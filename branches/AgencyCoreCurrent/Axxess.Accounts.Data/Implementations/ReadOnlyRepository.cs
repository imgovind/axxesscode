﻿namespace Axxess.Data
{
    using System;
    using System.Collections.Generic;

    using Axxess.Framework.Validation;

    public class ReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity> where TEntity : class, new()
    {
        public string ConnectionName { get; set; }

        protected IUnitOfWork UnitOfWork { get; private set; }

        public ReadOnlyRepository(IUnitOfWork unitOfWork)
        {
            Check.Argument.IsNotNull(unitOfWork, "unitOfWork");

            this.UnitOfWork = unitOfWork;
        }

        public virtual TEntity Single(string sql)
        {
            return this.UnitOfWork.Get<TEntity>(sql);
        }

        public virtual IList<TEntity> Find(string sql)
        {
            return this.UnitOfWork.Find<TEntity>(sql);
        }
    }
}
