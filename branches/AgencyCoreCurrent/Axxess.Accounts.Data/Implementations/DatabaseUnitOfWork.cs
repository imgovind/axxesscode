﻿namespace Axxess.Data
{
    using System;
    using System.Linq;

    using Axxess.Framework.DataAccess;
    using System.Collections.Generic;

    public class DatabaseUnitOfWork : IUnitOfWork
    {
        public Connection Connection { get; protected set; }

        public DatabaseUnitOfWork(Connection connection)
        {
            this.Connection = connection;
        }

        public ITransaction BeginTransaction()
        {
            return new Transaction(this);
        }

        public void EndTransaction(ITransaction transaction)
        {
            if (transaction != null)
            {
                (transaction as IDisposable).Dispose();
                transaction = null;
            }
        }

        public bool Insert<TEntity>(TEntity entity) where TEntity : class, new()
        {
            return this.Connection.Insert<TEntity>(entity);
        }

        public bool Update<TEntity>(TEntity entity) where TEntity : class, new()
        {
            return this.Connection.Update<TEntity>(entity);
        }

        public bool Delete<TEntity>(TEntity entity) where TEntity : class, new()
        {
            return this.Connection.Delete<TEntity>(entity);
        }

        public TEntity Get<TEntity>(string sql) where TEntity : class, new()
        {
            var list = this.Connection.Select<TEntity>(sql).ToList();
            if (list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        public IList<TEntity> Find<TEntity>(string sql) where TEntity : class, new()
        {
            return this.Connection.Select<TEntity>(sql).ToList();
        }

        public void Dispose()
        {
            if (this.Connection != null)
            {
                this.Connection.Dispose();
                this.Connection = null;
            }
        }
    }
}
