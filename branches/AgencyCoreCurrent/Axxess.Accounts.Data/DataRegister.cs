﻿namespace Axxess.Accounts.Data
{
    using System;
    using System.Linq;

    using Axxess.Accounts.Entities;

    using Axxess.Framework.DataAccess;
    using Axxess.Framework.Extensions;
    using Axxess.Framework.Extensibility;

    public static class DataRegister
    {
        public static void Initialize()
        {
            var type = typeof(IDataModel);
            var types = AppDomain.CurrentDomain
                .GetAssemblies()
                .ToList()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && p.IsClass && !p.IsAbstract);
            
            types.ForEach(t =>
            {
                PropertyCache.Register(t);
            });
        }
    }
}