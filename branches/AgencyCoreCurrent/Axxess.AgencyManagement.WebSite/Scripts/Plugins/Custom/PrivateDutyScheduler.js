(function($) {
    $.extend($.fn, {
        PrivateDutyScheduler: function(command) {
            return this.each(function() {
                // Refresh Tasks
                if (command === "refresh") {
                    if ($(this).fullCalendar("getView").title != "month") $(this).fullCalendar("changeView", "month");
                    if ($(this).fullCalendar("month") != new Date().getMonth()) $(this).fullCalendar("today");
                    else $(this).fullCalendar("refetchEvents");
                    // New Scheduler
                } else {
                    // Find Aspect Ratio for available space
                    var ratio = $(this).closest(".ui-layout-center").width() / ($(this).closest(".ui-layout-center").height() - 75);
                    // Initialize the Plugin
                    $(this).fullCalendar({
                        aspectRatio: ratio,
                        editable: false,
                        events: function(start, end, callback) {
                            // Get Task Data
                            if (U.IsGuid(Schedule.PrivateDuty.Center.PatientId)) U.PostUrl("Schedule/PrivateDuty/Event/List", {
                                startDate: U.FormatDate(start),
                                endDate: U.FormatDate(end),
                                patientId: Schedule.PrivateDuty.Center.PatientId
                            }, function(result) {
                                // Parse Data and Pass it to Events JSON
                                var events = [];
                                $(result).find("events").each(function() {
                                    var s = parseInt($(this).attr("Status"));
                                    if (s == 425 || s == 330 || s == 220 || s == 225 || s == 135 || s == 455) back = "#3ea123", border = "#173b0d";
                                    else if (s == 400) back = "#b20e0e", border = "#4d0606";
                                    else back = "#3e799c", border = "#152a36";
                                    events.push({
                                        id: $(this).attr("Id"),
                                        title: "<strong>" + $(this).attr("DisciplineTaskName") + "</strong> &#8211; " + $(this).attr("UserName"),
                                        allDay: $(this).attr("AllDay"),
                                        start: $(this).attr("StartDate"),
                                        end: $(this).attr("EndDate"),
                                        backgroundColor: back,
                                        borderColor: border
                                    });
                                });
                                callback(events);
                            }, function() {
                                // If Error on Getting Data
                                U.Growl("There was an error in getting your events.  Please try again.", "error")
                            });
                        },
                        header: { left: "prev,next today", center: "title", right: "month,agendaWeek,agendaDay" },
                        loading: function(isLoading, view) {
                            // Toggle Loading Class
                            if (isLoading) $(view).addClass("loading");
                            else $(view).removeClass("loading");
                        },
                        titleFormat: {
                            // Overload Title Format to Allow Patient Name
                            month: "'<span class=\"fc-patient-name\"></span> &#8212; 'MMMM yyyy",
                            week: "'<span class=\"fc-patient-name\"></span> &#8212; 'MMM d[ yyyy]{'&#8211;'[ MMM]d, yyyy}",
                            day: "'<span class=\"fc-patient-name\"></span> &#8212; 'dddd, MMM d, yyyy"
                        },
                        viewDisplay: function(view) {
                            // Set Patient Name
                            $(view.element).closest(".pd-calendar").find(".fc-patient-name").text(Schedule.PrivateDuty.Center.PatientName);
                        },
                        windowResize: function(view) {
                            // Reset Aspect Ratio upon Resize
                            $(this).fullCalendar("option", "aspectRatio", $("#PDScheduleMainResult").width() / ($("#PDScheduleMainResult").height() - 75));
                        }
                    });
                    // Fix Styling
                    $(".fc-button-effect", this).remove();
                    $(".fc-button-content", this).each(function() {
                        $(this).text($(this).text().replace(/\b./g, function(m) { return m.toUpperCase(); }))
                    });
                    // Add new event button
                    $(".fc-header-left", this).prepend(
                        $("<div>").addClass("abs").Buttons([{ Text: "New Task", Click: Schedule.PrivateDuty.Event.New}])
                    );
                }
            })
        }
    })
})(jQuery);