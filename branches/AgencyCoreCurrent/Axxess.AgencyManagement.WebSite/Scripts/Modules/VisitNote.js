﻿var V = {
    Init: function() {
        Template.OnChangeInit();
    },
    HandleSubmit: function(page, form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            complete: function(result) {

                try {
                    result = $.parseJSON(result.responseText);
                } catch (e) { return; }

                // If the form was autosaved, we don't want to do Growl messages or refresh the UI.
                if (form.data('autosaved')) {
                    form.data({
                        autosaved: false,
                        changed: false
                    });
                    return;
                }

                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.Growl(result.errorMessage, "success");
                        if(result.warningMessage!=undefined && result.warningMessage!="")
                            U.Growl(result.warningMessage, "warning");
                        UserInterface.Refresh();
                    } else if (control.html() == "Complete") {
                        UserInterface.CloseAndRefresh(page);
                        U.Growl(result.errorMessage, "success");
                        if (result.warningMessage != undefined && result.warningMessage != "")
                            U.Growl(result.warningMessage, "warning");
                    } else if (control.html() == "Approve") {
                        UserInterface.CloseAndRefresh(page);
                        U.Growl(result.errorMessage, "success");
                        if (result.warningMessage != undefined && result.warningMessage != "")
                            U.Growl(result.warningMessage, "warning");
                    } else if (control.html() == "Return") {
                        UserInterface.CloseAndRefresh(page);
                        U.Growl(result.errorMessage, "success");
                        if (result.warningMessage != undefined && result.warningMessage != "")
                            U.Growl(result.warningMessage, "warning");
                    }
                } else U.Growl(result.errorMessage, "error");
            },
            error: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.Growl(result.errorMessage, "success");
                        UserInterface.Refresh();
                    } else if (control.html() == "Complete") {
                        UserInterface.CloseAndRefresh(page);
                        U.Growl(result.errorMessage, "success");
                    } else if (control.html() == "Approve") {
                        UserInterface.CloseAndRefresh(page);
                        U.Growl(result.errorMessage, "success");
                    } else if (control.html() == "Return") {
                        UserInterface.CloseAndRefresh(page);
                        U.Growl(result.errorMessage, "success");
                    }
                }
                else if (result.errorMessage) {
                    U.Growl(result.errorMessage, "error");
                }
                else {
                    U.Growl("The note contains potentially dangerous values. Ensure that special characters such as #,$,<,&...etc are not next to each other.", "error");
                }
            }
        };
        if (form.data('autosaved')) {
            options.data = { AuditSilence: true }
        }
        $(form).ajaxSubmit(options);
        return false;
    },
    HandleWoundSubmit: function(page, form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') U.Growl("Wound care saved successfully.", "success");
                else U.Growl($.trim(result.responseText), "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') U.Growl("Wound care saved successfully.", "success");
                else U.Growl($.trim(result.responseText), "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
}
var snVisit = {
    Init: function(episodeId, patientId, eventId) {
        V.Init();
    },
    Submit: function(control) {
        $('#SNVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snVisit", 'Schedule/SNVisit', function() { snVisit.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var sixtyDaySummary = {
    Submit: function(control) {
        $('#Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("sixtyDaySummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("sixtyDaySummary", 'Schedule/SixtyDaySummary', function() { V.Init(); Schedule.sixtyDaySummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var nutritionalAssessment = {
    Init: function() {
        Visit.Shared.Init("NutritionalAssessment", V.Init(), "Schedule/NutritionalAssessmentContent");
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("nutritionalAssessment", 'Schedule/NutritionalAssessment', function() { nutritionalAssessment.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var dischargeSummary = {
    Submit: function(control) {
        $('#DischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("dischargeSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("dischargeSummary", 'Schedule/DischargeSummary', function() { V.Init(); Schedule.dischargeSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var PTDischargeSummary = {
    Submit: function(control) {
        $('#PTDischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("PTDischargeSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("PTDischargeSummary", 'Schedule/PTDischargeSummary', function() { V.Init(); Schedule.ptDischargeSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var OTDischargeSummary = {
    Submit: function(control) {
        $('#OTDischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("OTDischargeSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("OTDischargeSummary", 'Schedule/OTDischargeSummary', function() { V.Init(); Schedule.otDischargeSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var STDischargeSummary = {
    Submit: function(control) {
        $('#STDischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("STDischargeSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("STDischargeSummary", 'Schedule/STDischargeSummary', function() { V.Init(); Schedule.stDischargeSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var lvnSupVisit = {
    Submit: function(control) {
        $('#LVNSVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("lvnsVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("lvnsVisit", 'Schedule/LVNSVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var transferSummary = {
    Submit: function(control) {
        $('#TransferSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("transferSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("transferSummary", 'Schedule/TransferSummary', function() { V.Init(); Schedule.transferSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var woundCare = {
    Submit: function(control) {
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleWoundSubmit("woundcare", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("woundcare", 'Schedule/WoundCare', function() { V.Init(); Schedule.WoundCareInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaCarePlan = {
    Init: function() {
    var Prefix = "#HHACarePlan_";
        U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
        U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
        U.ShowIfChecked($(Prefix + "IsDiet"), $(Prefix + "Diet"));
        U.ShowIfChecked($(Prefix + "Allergies"), $(Prefix + "AllergiesDescription"));
        U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
        U.ShowIfChecked($(Prefix + "ActivitiesPermitted12"), $(Prefix + "ActivitiesPermittedOther"));
        Visit.Shared.Init("HHACarePlan", hhaCarePlan.Init);
    },
    Submit: function(control) {
        $('#HHACarePlan_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhaCarePlan", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhaCarePlan", 'Schedule/HHACarePlan', function() { hhaCarePlan.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaSupVisit = {
    Submit: function(control) {
        $('#HHASVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhasVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhasVisit", 'Schedule/HHASVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var transportationLog = {
    Submit: function(control) {
        $('#DriverOrTransportationNote_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("transportationnote", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("transportationnote", 'Schedule/TransportationNote', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snDiabeticDailyVisit = {
    Submit: function(control) {
        $('#SNDiabeticDailyVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snDiabeticDailyVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snDiabeticDailyVisit", 'Schedule/SNDiabeticDailyVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPediatricVisit = {
    Submit: function(control) {
    $('#SNPediatricVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPediatricVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPediatricVisit", 'Schedule/SNPediatricVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPediatricAssessment = {
    Submit: function(control) {
        $('#SNPediatricAssessment_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPediatricAssessment", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPediatricAssessment", 'Schedule/SNPediatricAssessment', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPsychVisit = {
    Submit: function(control) {
        $('#SNPsychVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPsychVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPsychVisit", 'Schedule/SNPsychVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snPsychAssessment = {
    Submit: function(control) {
        $('#SNPsychAssessment_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snPsychAssessment", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snPsychAssessment", 'Schedule/SNPsychAssessment', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snLabs = {
    Submit: function(control) {
    $('#SNLabs_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snLabs", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
    Acore.Open("snLabs", 'Schedule/SNLabs', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var ISOC = {
    Submit: function(control) {
        $('#SNVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("ISOC", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
    Acore.Open("ISOC", 'Schedule/InitialSummaryOfCare', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
