var UploadType = {
    Delete: function(id) { 
        U.DeleteTemplate("UploadType", id, function(){},"Upload Type"); 
    },
    InitEdit: function() {
        U.InitEditTemplate("UploadType");
    },
    InitNew: function() {
        U.InitNewTemplate("UploadType");
    },
    RebindList: function() { U.RebindTGrid($("#List_UploadType")); }
}
