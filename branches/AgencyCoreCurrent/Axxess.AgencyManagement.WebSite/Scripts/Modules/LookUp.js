﻿var Lookup = {
    _Users: "",
    _Discipline: new Array(),
    Load: function(u, p, v, n, a) {
        U.PostUrl(u, v, function(data) {
            $(p).each(function() {
                var s = this;
                $(this).children('option').remove();
                if (n != undefined) s[0] = new Option("-- Select " + n + " --", "", false, false);
                if (a) s[1] = new Option("** Add New " + n + " **", "new", false, false);
                $.each(data, function(index, itemData) {
                    s[s.options.length] = new Option(
                            (n == "Admission Source" ? "(" + itemData.Code + ") - " + itemData.Description : "") +
                            (n == "Task" ? itemData.Task : "") +
                            (n == "Physician" || n == "User" ? itemData.DisplayName : "") +
                            (n == "Race" || n == "Referral Source" ? itemData.Description : "") +
                            (n == "Insurance" || n == "State" ? itemData.Name : ""),
                            (n == "State" || n == "Admission Source" ? itemData.Code : itemData.Id), false, false);
                    if (n == "Task") $("option:last", s).attr("data", itemData.Discipline).attr("IsBillable", itemData.IsBillable);
                });
            });
        });
    },
    LoadAdmissionSources: function() { Lookup.Load("/LookUp/AdmissionSources", "select.AdmissionSource", null, "Admission Source"); },
    LoadDiscipline: function(control, input) { Lookup.Load("/LookUp/DisciplineTasks", control + " select.DisciplineTask", "Discipline=" + input, "Task"); },
    LoadInsurance: function() { Lookup.Load("/Agency/GetInsurances", "select.Insurances", null, "Insurance", true); },
    LoadMultipleDisciplines: function(control) { Lookup.Load("/LookUp/MultipleDisciplineTasks", control + " select.MultipleDisciplineTask", null, "Task"); },
    LoadPhysicians: function() { Lookup.Load("/Agency/GetPhysicians", "select.Physicians", null, "Physician", true); },
    LoadRaces: function() { Lookup.Load("/LookUp/Races", "select.EthnicRaces", null, "Race"); },
    LoadReferralSources: function() { Lookup.Load("/LookUp/ReferralSources", "select.ReferralSources", null, "Referral Source"); },
    LoadStates: function() { Lookup.Load("/LookUp/States", "select.States", null, "State"); },
    LoadUsers: function(control) { Lookup.Load("/User/All", control + " select.Users", null, "User"); },
    SetDiscipline: function(name, discipline) { Lookup._Discipline[name] = discipline; },
    Language: function(selector) { $(selector).Autocomplete({ source: ["Spanish", "Chinese", "French", "German", "Filipino", "Vietnamese", "Italian", "English"] }) }
}