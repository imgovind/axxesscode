﻿var Template = {
    Delete: function(id) { U.DeleteTemplate("Template", id); },
    InitEdit: function() {
        U.InitEditTemplate("Template");
    },
    InitNew: function() {
        U.InitNewTemplate("Template");
    },
    RebindList: function() { U.RebindTGrid($('#List_Template')); },
    OnChangeInit: function() {
        $("select.Templates").change(function() {
            var selectList = this;
            var textarea = $(this).attr("template");
            if ($(this).val() == "empty") {
                $(textarea).val("");
                selectList.selectedIndex = 0;
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            } else {
                if ($(this).val().length > 0) {
                    U.PostUrl("/Template/Get", "id=" + $(this).val(), function(template) {
                        if (template != undefined) {
                            var existingText = $(textarea).val();
                            if (existingText == '') {
                                $(textarea).val(template.Text);
                                $(textarea).trigger('keyup');
                            } else {
                                $(textarea).val(existingText + '\n' + template.Text);
                                $(textarea).trigger('keyup');
                            }
                        }
                    });
                }
            }
        });
    }
}