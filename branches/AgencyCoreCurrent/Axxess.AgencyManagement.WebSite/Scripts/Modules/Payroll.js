var Payroll = {
    _sort: {},
    InitSearch: function() {
        var errorHtml = "<div class='jGrowl' style='text-align: center; position: relative; width: 760px; margin: .5em auto;'><div class='error'>A problem happend during your request, please try again</div></div>";
        function updatePayrollResults(html){
            Payroll.EndLoading();
            $("#payrollSearchResult").show();
            $("#payrollSearchResult").html(html);
        }

        $("#payrollMarkAsPaidButton li").hide();
        $("#searchPayrollForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'html',
                    beforeSubmit: function(values, form, options) {
                        Payroll.StartLoading();
                    },
                    success: function(result) {
                        $("#payrollSearchResultDetails").hide();
                        $("#payrollSearchResultDetail").hide();
                        $("#payrollMarkAsPaidButton li").hide();
                        updatePayrollResults(result);
                        $("#payroll_print").removeClass("hidden").bind("mouseup", Payroll.Print);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        updatePayrollResults(errorHtml);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    Print: function() {
        if ($("#payrollSearchResult").is(":visible")) $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryPdf", {
                payrollStartDate: $("#payrollStartDate").val(),
                payrollEndDate: $("#payrollEndDate").val(),
                payrollStatus: $("#payrollStatus").val()
            })
        });
        else if ($("#payrollSearchResultDetails ul").length > 1) $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryDetailsPdf", {
                payrollStartDate: $("#payrollStartDate").val(),
                payrollEndDate: $("#payrollEndDate").val(),
                payrollStatus: $("#payrollStatus").val()
            })
        });
        else $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryDetailPdf", {
                userId: $("#payrollUserId").text(),
                payrollStartDate: $("#payrollStartDate").val(),
                payrollEndDate: $("#payrollEndDate").val(),
                payrollStatus: $("#payrollStatus").val()
            })
        })
    },
    LoadDetail: function(userId) {
    $("#payrollSearchType").val(userId);
        Payroll.StartLoading();
        $("#payrollSearchResultDetail").Load("Payroll/Detail", {
            userId: userId,
            payrollStartDate: $("#payrollStartDate").val(),
            payrollEndDate: $("#payrollEndDate").val(),
            payrollStatus: $("#payrollStatus").val()
        }, function(r, t, x, e) {
            Payroll.EndLoading();
            $("#payrollSearchResultDetail,#payrollMarkAsPaidButton").show();
            $("#payrollSearchResult").hide();
            if (t == "error") $("#payrollSearchResultDetail").html(U.AjaxError());
            else if (t == "success" && r.indexOf("No Results found") == -1) {
                $("#payrollSearchResultDetails,#payrollMarkAsPaidButton li").hide();
                $("#markAsPaidButtonId").val("#payrollSearchResultDetail");
                if ($("#payrollStatus").val() == "All" || $("#payrollStatus").val() == "false") $("#payrollMarkAsPaidButton li:eq(0)").show();
                if ($("#payrollStatus").val() == "All" || $("#payrollStatus").val() == "true") $("#payrollMarkAsPaidButton li:eq(1)").show();
            } else $("#payrollMarkAsPaidButton li").hide();
        })
    },
    LoadDetails: function() {
        $("#payrollSearchType").val("");
        Payroll.StartLoading();
        $("#payrollSearchResultDetails").Load("Payroll/Details", {
            payrollStartDate: $("#payrollStartDate").val(),
            payrollEndDate: $("#payrollEndDate").val(),
            payrollStatus: $("#payrollStatus").val()
        }, function(r, t, x, e) {
            Payroll.EndLoading();
            $("#payrollSearchResultDetails,#payrollMarkAsPaidButton").show();
            $("#payrollSearchResult").hide();
            if (t == "error") $("#payrollSearchResultDetail").html(U.AjaxError());
            else if (t == "success" && r.indexOf("No Results found") == -1) {
                $("#payrollSearchResultDetail,#payrollMarkAsPaidButton li").hide();
                $("#markAsPaidButtonId").val("#payrollSearchResultDetails");
                if ($("#payrollStatus").val() == "All" || $("#payrollStatus").val() == "false") $("#payrollMarkAsPaidButton li:eq(0)").show();
                if ($("#payrollStatus").val() == "All" || $("#payrollStatus").val() == "true") $("#payrollMarkAsPaidButton li:eq(1)").show();
            } else $("#payrollMarkAsPaidButton li").hide();
        })
    },
    MarkAsPaid: function() {
        var control = $("#markAsPaidButtonId").val();
        if ($("input[name=visitSelected]:checked").length > 0) {
            U.PostUrl("Payroll/PayVisit", $("input[name=visitSelected]:checked", $(control)).serializeArray(), function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    if ($("#payrollSearchType").val().length > 0) Payroll.LoadDetail($("#payrollSearchType").val());
                    else Payroll.LoadDetails();
                } else U.Growl(result.errorMessage, "error");
            }, null);
        } else U.Growl("Select at least one visit to mark as paid.", "error");
    },
    MarkAsUnpaid: function() {
        var control = $("#markAsPaidButtonId").val();
        if ($("input[name=visitSelected]:checked").length > 0) {
            U.PostUrl("Payroll/UnpayVisit", $("input[name=visitSelected]:checked", $(control)).serializeArray(), function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    if ($("#payrollSearchType").val().length > 0) Payroll.LoadDetail($("#payrollSearchType").val());
                    else Payroll.LoadDetails();
                } else U.Growl(result.errorMessage, "error");
            }, null);
        } else U.Growl("Select at least one visit to set as unpaid.", "error");
    },
    Sort: function(section, sortBy, event) {
        $("#" + section + " ol").addClass("loading");
        var invert = true;
        if (Payroll._sort == sortBy)
            invert = false, Payroll._sort = "";
        else
            Payroll._sort = sortBy;

        setTimeout(function() {
            var items = new Array();
            var sortedItems = new Array();

            // var divSection = $(#section);

            //$(event.originalTarget).parent().parent().parent();


            if (sortBy == "payrollcount") {
                $("#" + section).find("ol span." + sortBy).each(function() {
                    items.push(parseInt($(this).text(), 10))
                });

                if (invert) {
                    sortedItems = items.sort(function sortfunc(a, b) { return a - b; });
                }
                else if (!invert) {
                    sortedItems = items.sort(function sortfunc(a, b) { return b - a; });
                }
                for (var i = sortedItems.length - 1; i >= 0; i--) {
                    $("#" + section).find("ol").prepend($("#" + section).find("ol ." + sortBy).filter(function() { return $(this).text() == sortedItems[i]; }).closest("li"));
                }
            }
            else {
                $("#" + section).find("ol span." + sortBy).each(function() { items.push($(this).text()) });
                sortedItems = items.sort();
                if (invert)
                    sortedItems.reverse();

                for (var i = sortedItems.length - 1; i >= 0; i--) {
                    $("#" + section).find("ol").prepend($("#" + section).find("ol ." + sortBy + ":contains('" + sortedItems[i] + "')").closest("li"));
                }
            }
            $("#" + section).find("ol li:nth-child(even)").removeClass("odd").addClass("even");
            $("#" + section).find("ol li:nth-child(odd)").removeClass("even").addClass("odd");
            $("#" + section).find("ol").removeClass("loading");
            if (invert) $("input[name=Sort]").val(sortBy + "-invert");
            else $("input[name=Sort]").val(sortBy);
        }, 100);
    },
    StartLoading: function() {
        $("#payrollSearchResult").hide();
        $("#payrollMarkAsPaidButton li").hide();        
        $("#payrollSearchResultDetails").empty();
        $("#payrollSearchResultDetail").empty();
        $("#payrollLoading").addClass("loading").css("z-index", "1");
    },
    EndLoading: function() {
        $("#payrollLoading").removeClass("loading").css("z-index", "-1");
    }
}
