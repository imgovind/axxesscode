﻿var AdjustmentCode = {
    Delete: function(id) { U.DeleteTemplate("AdjustmentCode", id); },
    InitEdit: function() {
        U.InitEditTemplate("AdjustmentCode");
    },
    InitNew: function() {
        U.InitNewTemplate("AdjustmentCode");
    },
    RebindList: function() { U.RebindTGrid($('#List_AdjustmentCode')); }
}