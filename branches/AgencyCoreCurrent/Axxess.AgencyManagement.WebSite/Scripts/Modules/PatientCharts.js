﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    Charts: {
        AcoreId: "patientcenter",
        _patientId: "",
        _dateFilter: "",
        _showFilter: "",
        _isRebind: false,
        _fromDate: "",
        _toDate: "",
        PatientToSelect: "",                // ID of Patient to be Selected
        Init: function(r, t, x, e) {
            var patientFilterTimer,
                currentFilterValue,
                selectionGrid = $("#PatientSelectionGrid .t-grid-content", e);
            $("#txtSearch_Patient_Selection", e).keyup(function() {
                var filterValue = $(this).val();
                patientFilterTimer = setTimeout(function() {
                    if (currentFilterValue != filterValue) {
                        currentFilterValue = filterValue;
                        if (filterValue) {
                            Patient.Charts.PatientSelector.TextFilter(filterValue);
                            if ($("tr.match:even", selectionGrid).length) $("tr.match", selectionGrid).first().click();
                            else Patient.Charts.SetPatientError();
                        } else {
                            $("tr", selectionGrid).removeClass("match t-alt").show();
                            $("tr:even", selectionGrid).addClass("t-alt");
                            $("tr:first", selectionGrid).click();
                        }
                    }
                }, 350);
            }).keydown(function() {
                clearTimeout(patientFilterTimer);
            });
            Patient.Charts._patientId = "";
            $(".top select", e).change(function() {
                Patient.Charts._patientId = "";
                Patient.Charts.PatientToSelect = "";
                U.FilterResults("Patient");
            });
            $(".t-grid-content", e).css({ height: "auto" });
            $(".layout", e).layout({
                west: {
                    paneSelector: ".ui-layout-west",
                    size: 200,
                    minSize: 160,
                    maxSize: 400,
                    livePaneResizing: true,
                    spacing_open: 3
                }
            });
        },
        LoadInfoAndActivity: function(patientId) {
            $("#patient-info").remove();
            var oldId = Patient.Charts._patientId,
                mainResult = $("#PatientMainResult").empty().addClass("loading");
            Patient.Charts._patientId = patientId;
            U.LoadTrackedUrl("Patient/Data", { patientId: patientId }, function(responseText, textStatus) {
                if (textStatus == "error") {
                    mainResult.html(U.AjaxError);
                    Patient.Charts._patientId = oldId;
                } else mainResult.html(responseText);
                mainResult.removeClass("loading");
            });
        },
        MoreInfo: function(e) {
            var $dialog = $("#patient-info");
            $dialog.dialog({
                width: 500,
                position: [$(e).offset().left, $(e).offset().top],
                modal: false,
                resizable: false,
                close: function() {
                    $dialog.dialog("destroy");
                    $dialog.hide();
                }
            }).show();
            $("#ui-dialog-title-patientInfo").html("Patient Info");
        },
        SetPatientError: function() {
            $.proxy(Patient.Charts.SetError, $("#PatientMainResult").removeClass("loading"), "No Patients Found to Meet Your Search Requirements.", [
                { Text: "Add New Patient", Click: function() { Acore.Open('newpatient'); } }
            ])();
        },
        SetError: function(message, buttons) {
            var error = $("<div/>").addClass("ajaxerror").append(
                $("<h1/>").text(message)).append(
                $("<div/>").Buttons(buttons));
            $(this).html(error);
        },
        PatientSelector: {
            Select: function(patientId, status) {
                var selectionFilter = $("#txtSearch_Patient_Selection"),
                    selectorGrid = $("#PatientSelectionGrid"),
                    selectorGridContent = $(".t-grid-content", selectorGrid);
                if (selectionFilter.val() != "") {
                    selectionFilter.val("");
                    $("tr", selectorGridContent).removeClass("match t-alt").show();
                    $("tr:even", selectorGridContent).addClass("t-alt");
                }
                if (patientId == Patient.Charts.PatientId) {
                    Patient.Charts.Activities.Rebind();
                    return;
                }
                Patient.Charts.PatientToSelect = patientId;
                var statusNumber = !isNaN(status) ? status : (status.toLowerCase() == "false" ? "1" : "2"),
                    window = $("#window_" + Patient.Charts.AcoreId),
                    statusDropDown = $("select.PatientStatusDropDown", window);
                if (statusNumber != statusDropDown.val()) {
                    statusDropDown.val(statusNumber);
                    U.RebindTGrid(selectorGrid, {
                        branchId: $("select.PatientBranchCode", window).val(),
                        statusId: statusNumber,
                        paymentSourceId: $("select.PatientPaymentDropDown", window).val()
                    });
                } else {
                    var row = $("tr td", selectorGridContent).filter(":contains('" + patientId + "')").parent();
                    $("tr.t-state-selected", selectorGridContent).removeClass("t-state-selected");
                    row.addClass("t-state-selected");
                    var scroll = $(row).position().top + selectorGridContent.scrollTop() - 24;
                    selectorGridContent.animate({ scrollTop: scroll }, "slow");
                    Patient.Charts.LoadInfoAndActivity(patientId);
                }
            },
            OnDataBound: function() {
                var selectedPatient = $("#txtSearch_Patient_Selection").val();
                Patient.Charts.PatientSelector.TextFilter(selectedPatient);
                var selectionGrid = $("#PatientSelectionGrid .t-grid-content"),
                    selectionGridRows = $("tr", selectionGrid);
                if (selectionGridRows.length) {
                    var patientId = Patient.Charts.PatientToSelect || Patient.Charts._patientId;
                    if (!patientId || patientId == U.GuidEmpty) {
                        if (selectedPatient.length) selectionGridRows = selectionGridRows.filter(".match");
                        selectionGridRows.eq(0).click();
                    } else $("td", selectionGrid).filter(":contains(" + patientId + ")").closest("tr").click();
                } else Patient.Charts.SetPatientError();
                if ($(".t-state-selected", selectionGrid).length) selectionGrid.scrollTop($(".t-state-selected", selectionGrid).position().top - 50);
            },
            OnRowSelect: function(e) {
                if (e.row.cells[2] != undefined) {
                    var patientId = e.row.cells[2].innerHTML;
                    if (patientId != Patient.Charts._patientId || patientId == Patient.Charts.PatientToSelect) {
                        if (!Patient.Charts._isRebind) {
                            Patient.Charts._dateFilter = "";
                            Patient.Charts._showFilter = "";
                            Patient.Charts._fromDate = "";
                            Patient.Charts._toDate = "";
                        }
                        var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
                        $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, "slow");
                        Patient.Charts._patientId = patientId;
                        Patient.Charts.PatientToSelect = "";
                        Patient.Charts.LoadInfoAndActivity(patientId);
                    }
                }
            },
            TextFilter: function(text) {
                var search = text.split(" ");
                $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match").hide();
                for (var i = 0; i < search.length; i++) {
                    $("td", "#PatientSelectionGrid .t-grid-content").each(function() {
                        if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) {
                            $(this).parent().addClass("match");
                        }
                    });
                }
                $("tr.match", "#PatientSelectionGrid .t-grid-content").removeClass("t-alt").show();
                $("tr.match:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
            }

        },
        Activities: {
            Array: [],
            OnLoad: function() {
                $(".t-grid-content", this).css({ height: "auto" });
                var grid = $.proxy(U.OnTGridClientLoad, this);
                //Overrides the default rebind behavior of the telerik grid with the 
                grid.rebind = Patient.Charts.Activities.Rebind;
                if (!Patient.Charts._isRebind) {
                    Patient.Charts._fromDate = $("#patient-activity-from-date").val();
                    Patient.Charts._toDate = $("#patient-activity-to-date").val();
                }
                $("#patient-activity-drop-down").val(Patient.Charts._showFilter);
                $("#patient-activity-from-date,#patient-activity-to-date").DatePicker();
                $("#patient-activity-date-drop-down").val(Patient.Charts._dateFilter);
                var pastDate = new Date();
                pastDate.setDate(pastDate.getDate() - 60);
                $("#patient-activity-from-date").val(Patient.Charts._fromDate != "" ? Patient.Charts._fromDate : U.FormatDate(pastDate));
                $("#patient-activity-to-date").val(Patient.Charts._toDate != "" ? Patient.Charts._toDate : U.FormatDate(new Date()));
                Patient.Charts.Activities.Rebind();
                Patient.Charts._isRebind = false;
            },
            Init: function() {
                $("select.patient-activity-drop-down").change(function() {
                    Patient.Charts._showFilter = $("select.patient-activity-drop-down").val();
                    var $grid = $('#PatientActivityGrid'),
                        grid = $grid.data('tGrid');
                    if (grid.Discipline == "All") {
                        var data = $.grep(Patient.Charts.Activities.Array, function(e) {
                            return Patient.Charts._showFilter == "All" || Patient.Charts._showFilter == e.Discipline;
                        });
                        if (!data.length) $(".t-grid-content tbody", $grid).empty().append($("<tr/>").addClass("t-no-data").append($("<td/>").attr("colspan", grid.columns.length).append($("<div/>").html("No Tasks could be found."))));
                        else grid.dataBind(data);
                    } else Patient.Charts.Activities.Rebind(null, $grid);
                });
                if ($("select.patient-activity-date-drop-down").val() == "DateRange") {
                    $("#date-range-text").hide();
                    $("div.custom-date-range").show();
                } else if ($("select.patient-activity-date-drop-down").val() == "All") {
                    $("#date-range-text").hide();
                    $("div.custom-date-range").hide();
                } else {
                    $("#date-range-text").show();
                    $("div.custom-date-range").hide();
                }
                $("select.patient-activity-date-drop-down").change(function() {
                    var value = $("select.patient-activity-date-drop-down").val();
                    if (!Patient.Charts._isRebind) Patient.Charts._dateFilter = value;
                    if (value == "DateRange") {
                        $("#date-range-text").hide();
                        $("div.custom-date-range").show();
                    } else if (value == "All") {
                        $("#date-range-text").hide();
                        $("div.custom-date-range").hide();
                        Patient.Charts.Activities.Rebind();
                    } else {
                        $("#date-range-text").show();
                        $("div.custom-date-range").hide();
                        Patient.Charts.Activities.Rebind();
                    }
                });
            },
            OnDataBound: function(e) {
                var grid = $(e.target).data("tGrid");
                if (!grid.hasCompletedInitialLoading) {
                    Patient.Charts.Activities.Array = grid.data;
                    grid.hasCompletedInitialLoading = true;
                }
            },
            OnRowDataBound: function(e) {
                GridProcessor.ProcessActivityRow(e.row, e.dataItem);
            },
            OnDataBinding: function(e) {
                var grid = $(e.target).data("tGrid");
                if (!grid.hasFirstClientBindingOccured) {
                    grid.hasFirstClientBindingOccured = true;
                    e.preventDefault();
                }
            },
            //Will rebind the patient charts activites if the current patient matches the patient passed in
            Refresh: function(patientId) {
                if (Patient.Charts._patientId == patientId && $('#PatientActivityGrid').length) Patient.Charts.Activities.Rebind();
            },
            Rebind: function(e, gridControl) {
                if (!e) e = $("#window_" + Patient.Charts.AcoreId + " .ui-layout-center");
                if (!gridControl) gridControl = $("#PatientActivityGrid", e);
                if (gridControl) {
                    var grid = gridControl.data("tGrid");
                    if (grid) {
                        var url = "Patient/Activity";
                        var filterBar = $(".bottom .above", e),
                            input = $("input,select", filterBar).serializeArray();
                        input.push({ name: "patientId", value: Patient.Charts._patientId });
                        grid.Discipline = input[0].value;
                        if (filterBar && input[2].name == "DateRangeId") {
                            var dateRangeId = input[2].value;
                            if (dateRangeId == "DateRange") {
                                $(".bottom .above #date-range-text").hide();
                                $(".bottom .above .custom-date-range").show();
                                input.push({ name: "RangeStartDate", value: $("input[name=RangeStartDate]", filterBar).val() });
                                input.push({ name: "RangeEndDate", value: $("input[name=RangeEndDate]", filterBar).val() });
                            } else if (dateRangeId == "All") {
                                $("#date-range-text,.custom-date-range", filterBar).hide();
                            } else {

                                $("#date-range-text", filterBar).show();
                                $(".custom-date-range", filterBar).hide();
                            }
                        }
                        U.PostTrackedUrl(url, input, function(result) {
                            for (var i = 0; i < result.ScheduleEvents.length; i++) {
                                result.ScheduleEvents[i] = $.extend({
                                    PatientId: result.PatientId,
                                    OasisProfileUrl: "",
                                    StatusComment: "",
                                    EpisodeNotes: "",
                                    PrintUrl: "",
                                    OnClick: "",
                                    Comments: "",
                                    IsVisitVerified: 0,
                                    HasAttachments: 0,
                                    IsMissed: 0,
                                    Orphaned: 0,
                                    IsComplete: 0,
                                    AP: ""
                                }, result.ScheduleEvents[i]);
                            }
                            Patient.Charts.Activities.Array = result.ScheduleEvents;
                            grid.dataBind(result.ScheduleEvents);
                            if (!result.ScheduleEvents.length) $(".t-grid-content tbody", gridControl).empty().append($("<tr/>").addClass("t-no-data").append($("<td/>").attr("colspan", grid.columns.length).append($("<div/>").html("No Tasks could be found."))));
                            if (filterBar) {
                                $("#date-range-text", filterBar).empty().append(result.Range);
                            }
                        }, function(result) {
                            console.log(result);
                        });
                    }
                }
            }
        }

    }
});