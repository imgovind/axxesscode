﻿var Visit = {
    Shared: {
        Init: function(Type, InitFunction, ContentUrl) {
            var Prefix = "#" + Type + "_";
            if (ContentUrl == undefined) ContentUrl = "Schedule/" + Type + "Content";
            Template.OnChangeInit();

            $("#" + Type + "_PreviousNotes").change(function() {
                $(Prefix + "Content").Load(ContentUrl, {
                    patientId: $("#" + Type + "_PatientId").val(),
                    noteId: $("#" + Type + "_PreviousNotes").val(),
                    type: Type
                }, InitFunction);
            })
        },
        Submit: function(Button, Completing, Type) {
            var $form = Button.closest("form");
            if (Completing) $(".complete-required", "#window_" + Type).addClass("required");
            else $(".complete-required", "#window_" + Type).removeClass("required");
            $("#" + Type + "_Button").val(Button.text());
            $form.validate();
            if ($form.valid()) {
                $form.ajaxSubmit({
                    dataType: "json",
                    data: { AuditSilence: $form.data('autosaved') },
                    beforeSubmit: Visit.Shared.BeforeSubmit,
                    success: function(Result) { Visit.Shared.AfterSubmit(Result, Button.text(), Type, $form) },
                    error: function(Result) { Visit.Shared.AfterSubmit(Result, Button.text(), Type, $form) }
                })
            } else U.ValidationError(Button);
        },
        BeforeSubmit: function() { },
        AfterSubmit: function(Result, Command, Type, $form) {

            // If the form was autosaved, we don't want to do Growl messages or refresh the UI.
            if ($form.data('autosaved')) {
                $form.data({
                    autosaved: false,
                    changed: false
                });
                return;
            }

            if (Result.isSuccessful) switch (Command) {
                case "Save":
                    U.Growl(Result.errorMessage, "success");
                    UserInterface.Refresh();
                    break;
                case "Complete":
                case "Approve":
                case "Return":
                    U.Growl(Result.errorMessage, "success");
                    UserInterface.CloseAndRefresh(Type);
                    break;
            } else {
                if (Result.errorMessage) {
                    U.Growl(Result.errorMessage, "error");
                }
                else {
                    U.Growl("The note contains potentially dangerous values. Ensure that special characters such as #,$,<,&...etc are not next to each other.", "error");
                }

            };
        },
        Print: function(EpisodeId, PatientId, EventId, QA, Type, Load) {
            if (Load == undefined) Load = function() {
                eval("Visit." + Type + ".Load('" + EpisodeId + "','" + PatientId + "','" + EventId + "')");
                UserInterface.CloseModal();
            };
            var Arguments = {
                Url: Type + "/View/" + EpisodeId + "/" + PatientId + "/" + EventId,
                PdfUrl: "Schedule/" + Type + "Pdf",
                PdfData: {
                    episodeId: EpisodeId,
                    patientId: PatientId,
                    eventId: EventId
                }
            };
            if (QA) $.extend(Arguments, {
                ReturnClick: function() { Schedule.ProcessNote("Return", EpisodeId, PatientId, EventId) },
                Buttons: [
                    {
                        Text: "Edit",
                        Click: function() {
                            if (typeof Load === "function") {
                                try {
                                    Load();
                                } catch (e) {
                                    U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                    console.log("Error in edit function on load for " + Type + ": " + e.toString());
                                    $.error(e)
                                }
                            }
                        }
                    }, {
                        Text: "Approve",
                        Click: function() {
                            Schedule.ProcessNote("Approve", EpisodeId, PatientId, EventId)
                        }
                    }
                ]
            });
            Acore.OpenPrintView(Arguments);
        }
    }
};