﻿if (typeof Message == "undefined") var Message = new Object();
$.extend(Message, {
    InboxAcoreId: "Inbox",
    InboxType: "inbox",
    DefaultMessage: "",
    Forward: false,
    Reply: false,
    ReplyAll: false,
    Inbox: function(id) {
        if (id) Message.DefaultMessage = id;
        Acore.Open("Inbox");
    },
    InitInbox: function(r, t, x, e) {
        $(".new-message", e).click(function() {
            Message.New();
            return false;
        });
        $(".new-folder", e).click(function() {
            Message.Folder.New();
            return false;
        });
        $(".layout", e).layout({
            west: {
                paneSelector: ".ui-layout-west",
                size: 300,
                minSize: 160,
                maxSize: 400,
                livePaneResizing: true,
                spacing_open: 3
            }
        });
        $(".folder", e).change(function() { U.RebindTGrid($("#list-messages"), { inboxType: $(this).val() }) });
        Message.Folder.LoadSelector();
        // $(".dropdown-button", e).Menu()
    },
    LoadMessage: function(e) {
        $("#message-view").Load("Message/View", {
            id: $("td > div", e.row).attr("id"),
            messageType: $("td > div", e.row).attr("type")
        }, function() {
            $("td > div", e.row).removeClass("read-false").addClass("read-true");
            e = $("#window_" + Message.InboxAcoreId + "_content");
            $(".message-body-container", e).css("top", $(".message-header-container", e).height() + 52);
            $(".reply,.reply-all,.forward", e).click(function() {
                Message.DefaultMessage = $(".t-state-selected > td > div", "#list-messages").attr("id");
                if ($(this).hasClass("reply")) Message.Reply = true;
                else if ($(this).hasClass("reply-all")) Message.ReplyAll = true;
                else if ($(this).hasClass("forward")) Message.Forward = true;
                Message.New();
                return false;
            });
            $(".delete", e).click(function() {
                Message.Delete($(".t-state-selected > td > div", "#list-messages").attr("id"), $(".t-state-selected > td > div", "#list-messages").attr("type"), Message.InboxType);
                return false;
            });
            $(".print", e).click(function() {
                U.GetAttachment("Message/Pdf", { id: $(".t-state-selected > td > div", "#list-messages").attr("id"), messageType: $(".t-state-selected > td > div", "#list-messages").attr("type") });
                return false;
            });
            // Message.Folder.LoadMoveMenu();
        })
    },
    OnMessageListReady: function(e) {
        if ($("#" + Message.DefaultMessage).length) $("#" + Message.DefaultMessage).closest("tr").click();
        else if ($("#list-messages .t-grid-content tr").length) $("#list-messages .t-grid-content tr").eq(0).click();
        else $("#message-view").removeClass("loading").html(
            U.MessageInfo("No Messages Found", "There are currently no messages in this mailbox."));
        Message.DefaultMessage = "";
    },
    RefreshInbox: function() {
        var e = $("#window_" + Message.InboxAcoreId + "_content");
        U.RebindTGrid($("#list-messages"), { inboxType: $(".folder", e).val() });
    }
});