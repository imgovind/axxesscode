﻿<html>
    <head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <style type="text/css" media="screen">
		    p {
			    margin: 0 0 10px 0;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
			    font-family: arial, sans-serif;
			    font-size: 13px;
		    }

		    body, div, td, th, textarea, input, h2, h3 {
			    font-family: arial, sans-serif;
			    font-size: 12px;
			    padding: 0;
 	            border: 0;
 	            outline: 0;
		    }
		    ol, ul {
 	            list-style: none;
 	            margin: 0;
            }
	    </style>
	</head>
	<body>
		<p>Dear <%=primarycontact%>,</p>
		<p>A change to your Axxess software company information was recently updated by <%=authorizeduser%>. If you did not authorize this change, please contact our support team at <a href="mailto:support@axxessconsult.com">support@axxessconsult.com</a>.</p>
		<p><strong>AgencyName: <%=agencyname%></strong></p>
		<p><strong>Contact Person Email: <%=contactemail%></strong></p>
		<p><strong>Contact Person Phone Number: <%=contactphone%></strong></p>
		<hr />
		<p>Agency name has been changed:</p>
		<p><strong>Previous Agency Name: <%=preagencyname%></strong></p>
		<p><strong>Current Agency Name: <%=curagencyname%></strong></p>
		<hr />
		<p>Thank you for being such a valued customer.</p>
		<p>Sincerely,</p>
		<p>The Axxess Team</p>
		
		<p style="font-family: arial, sans-serif; font-size: 11px;">This is an automated e-mail, please do not reply.</p>
		<p style="font-family: arial, sans-serif; font-size: 11px;">This communication is intended for the use of the recipient to which it is addressed, and may contain confidential, personal and/or privileged information. Please contact us immediately if you are not the intended recipient of this communication, and do not copy, distribute, or take action relying on it. Any communication received in error, or subsequent reply, should be deleted or destroyed.</p>
	</body>
</html>