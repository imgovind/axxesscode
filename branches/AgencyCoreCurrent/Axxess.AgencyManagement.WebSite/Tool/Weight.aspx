﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<script runat="server" type="text/C#">
    public float GetCpuUsage()
    {
        var counter = new System.Diagnostics.PerformanceCounter();
        counter.CategoryName = "Processor";
        counter.CounterName = "% Processor Time";
        counter.InstanceName = "_Total";
        counter.NextValue();
        System.Threading.Thread.Sleep(1000);
        return counter.NextValue();
    }

    public float GetRamUsage()
    {
        var counter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
        return counter.NextValue();
    }
</script>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Cache</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% if (!Current.IsIpAddressRestricted) { %>
        <% var cpuUsage = GetCpuUsage(); %>
        <%= string.Format("CPU Usage: {0} %", cpuUsage.ToString("0.00"))%>
        <br />
        <% var ramUsage = GetRamUsage(); %>
        <%= string.Format("RAM Usage: {0} MB", ramUsage.ToString("0.00"))%>
    <% } %>
</asp:Content>
    