﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Revenue>>" %>
<% string pagename = "BilledRevenue"; %>
<div class="wrapper">
    <fieldset>
        <legend>Revenue Report</legend>
        <div class="column" style="">
              <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
              <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%=Html.Insurances("InsuranceId", "0", new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div></div>
              <div class="row"><label class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-29).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
               <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','BilledRevenueContent',{{ BranchCode: $('#{0}_BranchCode').val(),InsuranceId: $('#{0}_InsuranceId').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
           <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportBilledRevenue", new { BranchCode = Guid.Empty, InsuranceId = 0, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/BilledRevenue", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadMedicareWithHMOInsuarnceDropDown('<%= pagename %>', 'All', true, function() { }); });
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
 </script>
