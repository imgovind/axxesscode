﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimInfoDetail>>" %>
<% string pagename = "BillingBatch"; %>
        <%= Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.BatchId).Title("Batch Id").Width(80);
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient");
               columns.Bound(p => p.Range).Sortable(false).Title("Episode").Width(150);
               columns.Bound(p => p.MedicareNumber).Sortable(false).Title("Medicare Number");
               columns.Bound(p => p.BillType).Sortable(false).Title("Bill Type");
               columns.Bound(p => p.ClaimAmount).Sortable(false).Format("${0:0.00}").Title("Claim Amount");
               columns.Bound(p => p.ProspectivePay).Sortable(false).Format("${0:0.00}").Title("Prospective Amount");
           })
          //.DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { ClaimType = "ALL", BatchDate = DateTime.Now }))
                              .Sortable(sorting =>
                                  sorting.SortMode(GridSortMode.SingleColumn)
                                      .OrderBy(order =>
                                      {
                                          var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                          var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                          if (sortName == "BatchId")
                                          {
                                              if (sortDirection == "ASC")
                                              {
                                                  order.Add(o => o.BatchId).Ascending();
                                              }
                                              else if (sortDirection == "DESC")
                                              {
                                                  order.Add(o => o.BatchId).Descending();
                                              }
                                          }
                                          if (sortName == "PatientIdNumber")
                                          {
                                              if (sortDirection == "ASC")
                                              {
                                                  order.Add(o => o.PatientIdNumber).Ascending();
                                              }
                                              else if (sortDirection == "DESC")
                                              {
                                                  order.Add(o => o.PatientIdNumber).Descending();
                                              }
                                          }
                                          else if (sortName == "DisplayName")
                                          {
                                              if (sortDirection == "ASC")
                                              {
                                                  order.Add(o => o.DisplayName).Ascending();
                                              }
                                              else if (sortDirection == "DESC")
                                              {
                                                  order.Add(o => o.DisplayName).Descending();
                                              }
                                          }
                                      })
                              )
                           .Scrollable()
                                   .Footer(false)%>
 
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','BillingBatchContent',{  ClaimType : \"" + $('#<%= pagename %>_ClaimType').val() + "\", BatchDate : \"" + $('#<%= pagename %>_BatchDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>
