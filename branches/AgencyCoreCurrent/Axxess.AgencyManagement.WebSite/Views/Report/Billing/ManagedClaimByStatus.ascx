﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimLean>>" %>
<% string pagename = "ManagedClaimsByStatus"; %>
<div class="wrapper">
    <div class="wide-column"><em>Note:The date range calculated based on the end date of the claim.</em></div>
    <fieldset>
        <legend>Managed Care Claims History</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Bill Status:</label><div class="float-right"><%= Html.ManagedBillStatus("Status", ((int)ManagedClaimStatus.ClaimCreated).ToString(), false, new { @id = pagename + "_Status" })%></div></div> 
            <div class="row"><label  class="float-left">Insurance:</label><div class="float-right"><%= Html.InsurancesFilter("PrimaryInsurance", "0", new { @id = pagename + "_Insurance", @class = "Insurance" })%></div></div> 
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
             <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"RebindReportGrid('{0}','{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"RebindExportReport('{0}');\">Export to Excel</a>",pagename)%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Billing/Content/ManagedClaimByStatus", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });
    
    $("#<%= pagename %>_Insurance").multiSelect({
        selectAll: false,
        noneSelected: '- Select Insurance -',
        oneOrMoreSelected: '*'
    });
    function RebindExportReport(pagename) {
        var insurance = [], checkbox = $("input[name=PrimaryInsurance]:checked");
        checkbox.each(function() { insurance.push($(this).val()); });
        U.GetAttachment('Report/ExportManagedClaimsByStatus', { 'BranchCode': $('#<%=pagename %>_BranchCode').val(), 'Status': $('#<%=pagename %>_Status').val(), 'InsuranceId': insurance.join(), 'StartDate': $('#<%=pagename %>_StartDate').val(), 'EndDate': $('#<%=pagename %>_EndDate').val() });
    }
    function RebindReportGrid(page, sortParams) {
        var insurance = [], checkbox = $("input[name=PrimaryInsurance]:checked");
        checkbox.each(function() { insurance.push($(this).val()); });

        if (insurance != "") Report.RebindReportGridContent(page, 'ManagedClaimsByStatusContent', { BranchCode: $("#" + page + "_BranchCode").val(), Status: $("#" + page + "_Status").val(), StartDate: $("#" + page + "_StartDate").val(), EndDate: $("#" + page + "_EndDate").val(), InsuranceId: insurance.join() }, sortParams); else U.Growl('Please select insurance', 'error');
    }
</script>
