﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listreports",
        "List of Reports",
        Current.AgencyName.Clean())%>
<div class="wrapper">
    <%= Html.Telerik().Grid<ReportLite>().Name("List_CompletedReports").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(r => r.Name).Title("Name").Sortable(false);
            columns.Bound(r => r.Format).Title("Format").Sortable(false);
            columns.Bound(r => r.Status).Title("Status").Sortable(false);
            columns.Bound(r => r.UserName).Title("Requested By").Sortable(false);
            columns.Bound(r => r.Created).Title("Started").Sortable(false);
            columns.Bound(r => r.Completed).Title("Completed").Sortable(false);
            columns.Bound(r => r.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Report.Delete('<#=Id#>');\">Delete</a>").Title("Action").Sortable(false).Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("CompletedList", "Report")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_CompletedReports .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $(".t-grid-content").css("height", "auto");
</script>