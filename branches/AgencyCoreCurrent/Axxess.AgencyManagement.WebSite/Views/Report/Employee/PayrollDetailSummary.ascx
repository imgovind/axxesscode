﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PayrollDetailSummaryItem>>" %>
<% string pagename = "PayrollDetailSummary"; %>
<div class="wrapper">
    <fieldset>
        <legend>Payroll Detail Summary</legend>
        <div class="column">
            <div class="row">
                <label  class="float-left">Branch:</label>
                <div class="float-right">
                    <%= Html.LookupSelectList( 
                        SelectListTypes.BranchesReport, 
                        "BranchCode", 
                        "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })
                    %>
                </div> 
            </div>
            <div class="row">
                <label  class="float-left">Date Range:</label>
                <div class="float-right">
                    <input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-15).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> 
                    To 
                    <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(15).ToShortDateString() %>" id="<%= pagename %>_EndDate" />
                </div>
            </div>
            
            <div class="row">
                <label class="float-left">Employee Name:</label>
            
                <div class="float-right">
                        <%= Html.LookupSelectList( 
                            SelectListTypes.Users, 
                            "UserCode", 
                            "", new { @id = pagename + "_UserCode", @class = "AddressBranchCode report_input required notzero" })
                        %>                
                </div>
            </div>

            <div class="row">
                <label class="float-left">Status:</label>
            
                <div class="float-right">
                    <select name="_PayrollStatus" id="<%= pagename %>_PayrollStatus">
                        <option value="true">Paid</option>
                        <option value="false" selected="selected">Unpaid</option>
                    </select>
                </div>
            </div>
            
        </div>
        <div class="column">
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons">
                <ul>
                    <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','EmployeePayrollDetailSummaryContent',{{ BranchCode: $('#{0}_BranchCode').val(), EmployeeId: $('#{0}_UserCode').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val(), PayrollStatus: $('#{0}_PayrollStatus').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li>
                </ul>
            </div>
            <div class="buttons">
                <ul>
                    <li>
                        <a href="javascript:void(0);" onclick="Report.ExportPayrollSummary();">Export To Excel</a>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
   <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Employee/Content/PayrollDetailSummary", Model); %>
    </div>
</div>
