﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PayrollDetailSummaryItem>>" %>
<% string pagename = "PayrollDetailSummaryContent"; %>
 <%= Html.Telerik().Grid(Model).Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.TaskName).Title("Task").ReadOnly();
               columns.Bound(s => s.Insurance).Title("Primary Payor").Width(200).ReadOnly();
               columns.Bound(s => s.TaskCount).Title("Total Visits").Width(100).ReadOnly();
               columns.Bound(s => s.TotalTaskTime).Title("Total Time").Width(150).ReadOnly();
               columns.Bound(s => s.PayRate).Title("Pay Rate").Width(100).ReadOnly();
               columns.Bound(s => s.TotalMileage).Title("Total Mileage").Width(100).ReadOnly();
               columns.Bound(s => s.MileageRage).Title("Mileage Rate").Width(100).ReadOnly();
               columns.Bound(s => s.TotalSurchargesFormat).Title("Surcharges").Width(100).ReadOnly();
               columns.Bound(s => s.TotalPaymentFormat).Title("Total Payment").Width(100).ReadOnly();
           }).Scrollable().Footer(false)
 %>
 
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','EmployeePayrollDetailSummaryContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
</script>

<label class="float-left">Total Hours:</label>   
<label class ="float-left>Total Mileage:</label>
