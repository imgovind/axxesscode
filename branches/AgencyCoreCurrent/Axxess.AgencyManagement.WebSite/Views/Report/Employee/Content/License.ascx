﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<License>>" %>
<% string pagename = "EmployeeLicenseListing"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(l => l.LicenseType).Title("License Name");
         columns.Bound(l => l.LicenseNumber).Title("License Number");
         columns.Bound(l => l.InitiationDate).Template(l => string.Format("{0}", l.InitiationDate>DateTime.MinValue?l.InitiationDate.ToString("MM/dd/yyyy"):string.Empty)).Title("Initiation Date");
         columns.Bound(l => l.ExpirationDate).Template(l => string.Format("{0}", l.ExpirationDate > DateTime.MinValue ? l.ExpirationDate.ToString("MM/dd/yyyy") : string.Empty)).Title("Expiration Date");
       })
              // .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new {userId = Guid.Empty}))
                                 .Sortable(sorting =>
                                                     sorting.SortMode(GridSortMode.SingleColumn)
                                                         .OrderBy(order =>
                                                         {
                                                             var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                             var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                             if (sortName == "LicenseType")
                                                             {
                                                                 if (sortDirection == "ASC")
                                                                 {
                                                                     order.Add(o => o.LicenseType).Ascending();
                                                                 }
                                                                 else if (sortDirection == "DESC")
                                                                 {
                                                                     order.Add(o => o.LicenseType).Descending();
                                                                 }
                                                             }
                                                             else if (sortName == "InitiationDate")
                                                             {
                                                                 if (sortDirection == "ASC")
                                                                 {
                                                                     order.Add(o => o.InitiationDate).Ascending();
                                                                 }
                                                                 else if (sortDirection == "DESC")
                                                                 {
                                                                     order.Add(o => o.InitiationDate).Descending();
                                                                 }
                                                             }
                                                             else if (sortName == "ExpirationDate")
                                                             {
                                                                 if (sortDirection == "ASC")
                                                                 {
                                                                     order.Add(o => o.ExpirationDate).Ascending();
                                                                 }
                                                                 else if (sortDirection == "DESC")
                                                                 {
                                                                     order.Add(o => o.ExpirationDate).Descending();
                                                                 }
                                                             }

                                                         })
                                                 )
                                       .Scrollable()
                                               .Footer(false)%>
  
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
        var link = $(this).attr("href");
        $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','EmployeeLicenseContent',{  UserId : \"" + $('#<%= pagename %>_UserId').val() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>

