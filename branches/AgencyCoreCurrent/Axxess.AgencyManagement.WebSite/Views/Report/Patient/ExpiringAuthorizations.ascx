﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Authorization>>" %>
<% string pagename = "ExpiringAuthorizations"; %>
<div class="wrapper">
    <fieldset>
        <legend>Expiring Authorizations</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Patient Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float-left">Authorization Status:</label><div class="float-right"><%= Html.GetAuthorizationStatus("AuthorizationStatus", Guid.Empty.ToString(), new { @id = pagename + "_AuthorizationStatus", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.AddDays(60).ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','ExpiringAuthorizationsContent',{{ BranchCode: $('#{0}_BranchCode').val(), StatusId: $('#{0}_StatusId').val(), AuthorizationStatus: $('#{0}_AuthorizationStatus').val(), StartDate: $('#{0}_StartDate').val(),  EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="U.GetAttachment('Report/ExportExpiringAuthorizations',{ 'BranchCode': $('#<%= pagename %>_BranchCode').val(), 'StatusId': $('#<%= pagename %>_StatusId').val(), 'AuthorizationStatus': $('#<%= pagename %>_AuthorizationStatus').val(), 'StartDate': $('#<%= pagename %>_StartDate').val(),  'EndDate': $('#<%= pagename %>_EndDate').val() });">Export to Excel</a></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
     <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Patient/Content/ExpiringAuthorizationsContent", Model); %>
    </div>
</div>
