﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientSixtyDaySummary"; %>
<div class="wrapper">
    <fieldset>
        <legend> Vital Signs Report </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid", @tabindex = "3" })%></div> </div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float-left">Patient:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, "PatientId", Guid.Empty.ToString(), Guid.Empty, 1, new { @id = pagename + "_PatientId", @class = "report_input valid", @tabindex = "1" })%></div> </div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientSixtyDaySummary();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientSixtyDaySummary", new { patientId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
        <%= Html.Telerik().Grid<VisitNoteViewData>().Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(v => v.UserDisplayName).Title("Employee Name").Sortable(false).ReadOnly();
               columns.Bound(v => v.VisitDate).Title("Visit Date").Sortable(false).ReadOnly();
               columns.Bound(v => v.SignatureDate).Title("Signature Date").Sortable(false).ReadOnly();
               columns.Bound(v => v.EpisodeRange).Title("Episode Date").Sortable(false).ReadOnly();
               columns.Bound(v => v.PhysicianDisplayName).Title("Physician Name").Sortable(false).ReadOnly();
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty})).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>GridContainer").css({ 'top': '160px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_StatusId').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); }); 
</script>
