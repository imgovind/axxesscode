﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientContactInfoViewData>" %>
<h3>Patient Address Listing</h3>
<fieldset>
    <div class="row">
        <label for="">
            Patient Name:</label><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientAddressBook_Name", "", new { @id = "PatientAddressBook_Name", @class = "Name report_input valid" })%>
    </div>
    <div class="row">
        <label for="">
            State:</label><%= Html.LookupSelectList(SelectListTypes.States, "PatientAddressBook_AddressStateCode", "", new { @id = "PatientAddressBook_AddressStateCode", @class = "AddressStateCode report_input valid" })%>
    </div>
    <div class="row">
        <label for="">
            Branch:</label><%= Html.LookupSelectList(SelectListTypes.Branches, "PatientAddressBook_AddressBranchCode", "", new { @id = "PatientAddressBook_AddressBranchCode", @class = "AddressBranchCode report_input valid" })%>
    </div>
    <input id="report_step" type="hidden" name="step" value="2" />
    <input id="report_action" type="hidden" name="action" value="PatientAddressBook" />
    <p>
        <input type="button" value="Generate Report" onclick="Report.populateReport($(this));" />
    </p>
</fieldset>
<div id="PatinetAddressBookResult" class="report-grid">
    <h3>Patient Address Book</h3>
    <% =Html.Telerik().Grid<AddressBookEntry>()
         .Name("PatientAddressBookGrid")        
         .Columns(columns =>
   {
       columns.Bound(e => e.Name);
       columns.Bound(e => e.AddressFirstRow).Title("Address");
       columns.Bound(e => e.AddressCity).Title("City");
       columns.Bound(e => e.AddressStateCode).Title("State");
       columns.Bound(e => e.AddressZipCode).Title("Zip Code");
       columns.Bound(e => e.PhoneHome).Title("Home Phone");
       columns.Bound(e => e.PhoneMobile).Title("Mobile Phone");
       columns.Bound(e => e.EmailAddress);
   })
   .DataBinding(dataBinding => dataBinding.Ajax().Select("PatientAddressBookResult", "Report", new { Name = Guid.Empty, AddressStateCode = " ", AddressBranchCode = Guid.Empty, ReportStep = "" }))
   .Sortable()
   .Selectable()
   .Scrollable()
   .Footer(false)
  
    %>
</div>
