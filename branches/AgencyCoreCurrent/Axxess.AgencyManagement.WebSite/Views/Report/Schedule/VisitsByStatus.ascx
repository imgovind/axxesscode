﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "ScheduleVisitsByStatus"; %>
<div class="wrapper main">
    <fieldset>
         <legend> Visits By Status</legend>
         <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
            <div class="row"><label  class="float-left">Status:</label>
            <div class="float-right">
                <select id="<%= pagename %>_Status" name="Status" multiple="multiple" >
                        <option value="400">Missed Visit</option>
                        <option value="401">Missed Visit (Pending QA Review)</option>
                        <option value="402">Missed Visit (Complete)</option>
                        <option value="403">Missed Visit (Returned For Review)</option>
                        <option value="405">Not Yet Started</option>
                        <option value="410">Not Yet Due</option>
                        <option value="415">Saved</option>
                        <option value="420">Submitted With Signature</option>
                        <option value="425">Completed</option>
                        <option value="225">Exported</option>
                        <option value="430">Returned For Review</option>
                        <option value="435">Reopened</option>
                        <option value="440">Awaiting Clinician Signature</option>
                        <option value="445">To Be Sent To Physician</option>
                        <option value="450">Sent To Physician (Fax, Mail, etc)</option>
                        <option value="455">Returned W/ Physician Signature</option>
                        <option value="460">Sent To Physician (Electronically)</option>
                        <option value="465">Returned for Clinician Signature</option>
                        <option value="470">Returned for Review by Physician</option>
                    </select>
            </div></div>
            <div class="row"><label  class="float-left">Patient:</label><div class="float-right"><%= Html.Patients("Patients", "", 1, "All", new { @id = pagename + "_Patient", @class = " report_input valid" }) %></div></div>
            <div class="row"><label  class="float-left">Assigned To:</label><div class="float-right"><%= Html.Users("Clinicians", "", "All", new { @id = pagename + "_Clinician", @class = " report_input valid" }) %></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"RebindReportGrid('{0}','{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleVisitsByStatus", new { BranchCode = Guid.Empty, PatientId = Guid.Empty, ClinicianId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now, Status = 405 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
     <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/VisitsByStatus", Model); %>
    </div>
</div>

<script type="text/javascript">
    $("#<%= pagename %>_Status").multiSelect({
        selectAll: false,
        noneSelected: '- Select Status -',
        oneOrMoreSelected: '*'
    });
    function RebindReportGrid(page,sortParams){
        var status = [], checkbox = $("input[name=Status]:checked");
        checkbox.each(function() { status.push($(this).val()); });
        if (status!="") Report.RebindReportGridContent(page, 'ScheduleVisitsByStatusContent', { BranchCode: $("#" + page + "_BranchCode").val(), PatientId: $("#" + page + "_Patient").val(), ClinicianId: $("#" + page + "_Clinician").val(), StartDate: $("#" + page + "_StartDate").val(), EndDate: $("#" + page + "_EndDate").val(), Status: status.join() }, sortParams); else U.Growl('Status is required', 'error');
    }
</script>
