﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "ScheduleDailyWork"; %>
<div class="wrapper">
    <fieldset>
        <legend>Daily Work Schedule</legend>
        <div class="column">
              <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label  class="float-left">Date:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="Date" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_Date" /></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
         <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','DailyWorkContent',{{ BranchCode: $('#{0}_BranchCode').val(), Date: $('#{0}_Date').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
         <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleDailyWork", new { BranchCode = Guid.Empty, Date = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Schedule/Content/DailyWork", Model); %>
    </div>
</div>
 
