﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<% string pagename = "ScheduleVisitsByStatus"; %>

        <% =Html.Telerik().Grid(Model).Name(pagename + "Grid")        
                 .Columns(columns =>
                 {
                 columns.Bound(m => m.PatientIdNumber).Title("MRN").Width(150);
                 columns.Bound(m => m.PatientName).Title("Patient");
                 columns.Bound(m => m.DisciplineTaskName).Title("Task");
                 columns.Bound(m => m.StatusName).Title("Status"); 
                 columns.Bound(m => m.EventDateSortable).Title("Schedule Date").Template(t => t.EventDateSortable).Width(110);
                 columns.Bound(p => p.UserName).Title("Assigned To");
               })
                           .Sortable(sorting =>
              sorting.SortMode(GridSortMode.SingleColumn)
                  .OrderBy(order =>
                  {
                      var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                      var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                      if (sortName == "PatientIdNumber")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientIdNumber).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientIdNumber).Descending();
                          }

                      }
                      else if (sortName == "StatusName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.StatusName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.StatusName).Descending();
                          }
                      }
                      else if (sortName == "PatientName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.PatientName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.PatientName).Descending();
                          }

                      }
                      else if (sortName == "UserName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.UserName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.UserName).Descending();
                          }
                      }
                      else if (sortName == "DisciplineTaskName")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.DisciplineTaskName).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.DisciplineTaskName).Descending();
                          }
                      }
                      else if (sortName == "EventDateSortable")
                      {
                          if (sortDirection == "ASC")
                          {
                              order.Add(o => o.EventDateSortable).Ascending();
                          }
                          else if (sortDirection == "DESC")
                          {
                              order.Add(o => o.EventDateSortable).Descending();
                          }
                      }
                  })
                  )
               .Scrollable().Footer(false)  %>
   
<script type="text/javascript">
    $("#<%= pagename %>Grid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
    var link = $(this).attr("href");
    var status = [], checkbox = $("input[name=Status]:checked");
    checkbox.each(function() { status.push($(this).val()); });
    $(this).attr("href", "javascript:void(0)").attr("onclick", "Report.RebindReportGridContent('<%= pagename %>','ScheduleVisitsByStatusContent',{  BranchCode : \"" + $('#<%= pagename %>_BranchCode').val() + "\", PatientId : \"" + $('#<%= pagename %>_Patient').val() + "\", ClinicianId : \"" + $('#<%= pagename %>_Clinician').val() + "\", StartDate : \"" + $('#<%= pagename %>_StartDate').val() + "\", EndDate : \"" + $('#<%= pagename %>_EndDate').val() + "\", Status : \"" + status.join() + "\" },'" + U.ParameterByName(link, '<%= pagename %>Grid-orderBy') + "');");
    });
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
