﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Visits By Primary Payment Source</legend>
        <div class="column">
            <div class="row">
                <label for="VisitsByPrimaryPaymentSource_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "VisitsByPrimaryPaymentSource_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="VisitsByPrimaryPaymentSource_Year" class="float-left">Sample Year:</label>
                <div class="float-right"><%= Html.SampleYear("Year", new { @id = "VisitsByPrimaryPaymentSource_Year" }) %></div>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/VisitsByPrimaryPaymentSourceReport', '#VisitsByPrimaryPaymentSource_BranchId', '#VisitsByPrimaryPaymentSource_Year');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
