﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Discharges By Reason</legend>
        <div class="column">
            <div class="row">
                <label for="DischargesByReason_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "DischargesByReason_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="DischargesByReason_Year" class="float-left">Sample Year:</label>
                <div class="float-right"><%= Html.SampleYear("Year", new { @id = "DischargesByReason_Year" })%></div>
            </div>  
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/DischargesByReasonReport', '#DischargesByReason_BranchId', '#DischargesByReason_Year');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
