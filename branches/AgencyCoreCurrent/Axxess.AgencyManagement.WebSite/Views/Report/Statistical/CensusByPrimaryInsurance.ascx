﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "CensusByPrimaryInsurance"; %>
<div class="wrapper">
    <fieldset>
        <legend>Census By Primary Insurance</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances("InsuranceId", "0", false, new { @id = pagename + "_InsuranceId", @class = "Insurances " })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
        </div>
         <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
         <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','CensusByPrimaryInsuranceContent',{{ BranchCode: $('#{0}_BranchCode').val(),StatusId: $('#{0}_StatusId').val(), InsuranceId: $('#{0}_InsuranceId').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportCensusByPrimaryInsurance", new { BranchCode = Guid.Empty, StatusId = 1, InsuranceId = 0 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">
       <% Html.RenderPartial("Statistical/Content/CensusByPrimaryInsurance", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>','-- Select Insurance --',true); });
 </script>