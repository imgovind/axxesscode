﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<VisitsByPayor>>" %>
<% string pagename = "VisitsByPayor"; %>
<div class="wrapper">
    <fieldset>
        <legend>Visits By Payor</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id =  pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="<%= pagename %>_StatusId" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option><option value="3">Pending</option></select></div></div>
            <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.InsuranceListByBranch("InsuranceId", "0", Guid.Empty.ToString(), false, string.Empty, new { @id = pagename + "_InsuranceId", @class = "report_input" })%></div></div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"RebindReportGrid('{0}','{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
        <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"RebindExportReport('{0}','{1}');\">Export to Excel</a>", pagename, sortParams)%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>GridContainer" class="report-grid">"
       <% Html.RenderPartial("Statistical/Content/VisitsByPayorContent", Model); %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>_BranchCode').change(function() { Insurance.loadInsuarnceDropDown('<%= pagename %>', 'Select an Insurance', false); });
    $("#<%= pagename %>_InsuranceId").multiSelect({
        selectAll: true,
        noneSelected: '-- Select Insurance --',
        oneOrMoreSelected: '*'
    });
    function RebindReportGrid(pagename, sortParams) {
        var insurance = [], checkbox = $("input[name=InsuranceId]:checked");
        checkbox.each(function() { insurance.push($(this).val()); });
        Report.RebindReportGridContent(pagename, 'StatisticalVisitsByPayorContent', { BranchCode: $("#" + pagename + "_BranchCode").val(), StatusId: $("#" + pagename + "_StatusId").val(), InsuranceId: insurance.join(), StartDate: $("#" + pagename + "_StartDate").val(), EndDate: $("#" + pagename + "_EndDate").val() }, sortParams);
    }
    function RebindExportReport(pagename, sortParams) {
        var insurance = [], checkbox = $("input[name=InsuranceId]:checked");
        checkbox.each(function() { insurance.push($(this).val()); });
        U.GetAttachment('Report/ExportStatisticalVisitsByPayor', { BranchCode: $("#" + pagename + "_BranchCode").val(), StatusId: $("#" + pagename + "_StatusId").val(), InsuranceId: insurance.join(), StartDate: $("#" + pagename + "_StartDate").val(), EndDate: $("#" + pagename + "_EndDate").val() }, sortParams);
    }
 </script>