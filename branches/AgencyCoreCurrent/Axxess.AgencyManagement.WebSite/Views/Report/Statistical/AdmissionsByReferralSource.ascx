﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Admissions By Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="AdmissionsByReferralSource_BranchId" class="float-left">Branch:</label>
                <div class="float-right"><%= Html.BranchOnlyList("BranchId", ViewData["BranchId"].ToString(), new { @id = "AdmissionsByReferralSource_BranchId" })%></div>
            </div>
            <div class="row">
                <label for="AdmissionsByReferralSource_Year" class="float-left">Sample Year:</label>
                <div class="float-right"><%= Html.SampleYear("Year", new { @id = "AdmissionsByReferralSource_Year" })%></div>
            </div>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Report.RequestReport('/Request/AdmissionsByReferralSourceReport', '#AdmissionsByReferralSource_BranchId', '#AdmissionsByReferralSource_Year');">Request Report</a></li>
            </ul>
        </div>
    </fieldset>
</div>
