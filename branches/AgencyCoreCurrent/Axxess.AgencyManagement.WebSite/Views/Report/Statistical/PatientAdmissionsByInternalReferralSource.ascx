﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "StatisticalPatientAdmissionsByInternalReferralSource"; %>
<div class="wrapper">
    <fieldset>
        <legend> Patient Admissions By Internal Referral Source </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", Guid.Empty.ToString(), new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label class="float-left">Admission Status:</label><div class="float-right"><select id= "<%= pagename %>_StatusId"  name="StatusId" class="PatientStatusDropDown"><option value="0" selected>All</option><option value="1" >Admitted</option><option value="2">Discharged</option><option value="3">Pending<option value="4">Non-Admit</option></select></div></div>
           
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="text" class="date-picker shortdate" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" /> To <input type="text" class="date-picker shortdate" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" /></div></div>
        </div>
        <div class="column">
            <% var sortParams= string.Format("{0}-{1}",ViewData["SortColumn"],ViewData["SortDirection"]);%>
            <div class="buttons"><ul><li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Report.RebindReportGridContent('{0}','StatisticalPatientAdmissionsByInternalReferralSourceContent',{{ BranchId: $('#{0}_BranchCode').val(),StatusId:$('#{0}_StatusId').val(), StartDate: $('#{0}_StartDate').val(), EndDate: $('#{0}_EndDate').val() }},'{1}');\">Generate Report</a>", pagename, sortParams)%></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalPatientAdmissionsByInternalReferralSource", new { BranchId = Guid.Empty,StatusId=0,StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
   <div id="<%= pagename %>GridContainer" class="report-grid">
         <% Html.RenderPartial("Statistical/Content/PatientAdmissionsByInternalReferralSource", Model); %>
    </div>
</div>
<script type="text/javascript">
    $("#<%= pagename %>GridContainer").css({ 'top': '190px' });
</script>
