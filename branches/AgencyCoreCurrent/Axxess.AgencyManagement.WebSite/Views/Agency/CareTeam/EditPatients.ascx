﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTeam>" %>
<span class="wintitle">Manage Care Team&#8217;s Patients | <%= Model.Name %></span>
<div class="wrapper main">
    <fieldset>
        <legend>Notice</legend>
        <div class="wide-column">
            <div class="row">
                <p>
                    Use this tool to grant access to this team&#8217;s users to view all visits/documents including
                    those not assigned to them for those patients listed on the left. &#160;This group&#8217;s users
                    will always have access to all visits/documents assigned to them even on patients listed on the
                    right.
                </p>
            </div>
        </div>
    </fieldset>
    <div class="team-access-patients" guid="<%= Model.Id %>"></div>
</div>