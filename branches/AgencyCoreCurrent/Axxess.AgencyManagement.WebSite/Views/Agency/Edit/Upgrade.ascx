﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <div class="bigtext align-center"><%= Current.AgencyName%></div>
    <div class="align-center"><label for="Edit_AgencyPackage_LocationId">Agency Branch: </label><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "Edit_AgencyPackage_LocationId", @class = "BranchLocation required" })%></div>
    <div id="Edit_AgencyPackage_Container">
    </div>
</div>