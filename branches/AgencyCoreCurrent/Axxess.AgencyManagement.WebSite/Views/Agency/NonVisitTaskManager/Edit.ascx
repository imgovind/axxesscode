﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<UserNonVisitTask>" %>
<span class="wintitle">Edit Task | <%= Current.AgencyName %></span>
<%  using (Html.BeginForm("Update", "NonVisitTaskManager", FormMethod.Post, new { @id = "editNonVisitTaskManagerForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_UserNonVisitTask_Id" }) %>
<div class="wrapper main">
    <fieldset>
        <div class="wide_column">
            <div class="row">
                <label class="float-left"><strong>User:</strong></label><div class="float-left"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", Model.UserId.ToString(), Guid.Empty, 1, new { @id = "UserId", @class = "valid" })%></div>    
            </div>
            <div class="row">
                <label class="float-left"><strong>Task Date:</strong></label><div class="float-left"><input type="text" class="date-picker shortdate" name="TaskDate" value="<%= Model.TaskDate.ToShortDateString() %>" id="TaskDate" /> </div>
            </div>
            <div class="row">
                <label class="float-left"><strong>Task:</strong></label><div class="float-left:"><%= Html.AllNonVisitTaskList("TaskId", Model.TaskId.ToString() , Current.AgencyId, "--Select Task--", new { @id = "TaskId_1", @class = "valid" })%></div>
            </div>
            <div class="row">
                <label class="float-left"><strong>Time In:</strong></label><input type="text" size="10" id="Task_TimeIn_5" name="TimeIn" class="time-picker fill tasktimein" value="<%= Model.TimeIn.ToShortTimeString() %>" />
            </div>
            <div class="row">
                <label class="float-left"><strong>Time Out:</strong></label><input type="text" size="10" id="Task_TimeOut_5" name="TimeOut" class="time-picker fill tasktimeout" value="<%= Model.TimeOut.ToShortTimeString() %>" />
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadNonVisitTaskManagerLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a Class = "save close">Save</a></li>
        <li><a Class = "close">Exit</a></li>
    </ul></div>
</div>
<% } %>
