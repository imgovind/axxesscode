﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"> Task Manager | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("UserNonVisitTasks", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<UserNonVisitTask>()
            .Name("List_NonVisitTaskManager")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(t => t.UserDisplayName).Title("User").Sortable(true).Width(170);
            columns.Bound(t => t.TaskTitle).Title("Task").Sortable(true).Width(130);
            columns.Bound(t => t.TaskDateDateString).Title("Task Date").Width(75);
            columns.Bound(t => t.TimeInDateString).Title("Begin Time").Width(60);
            columns.Bound(t => t.TimeOutDateString).Title("End Time").Width(60);
            columns.Bound(t => t.PaidDateDateString).Title("Paid Date").Width(75);
            columns.Bound(t => t.PaidStatusString).Title("Paid").Width(30);
            columns.Bound(t => t.Comments).Title("Comments").Width(200);
            columns.Bound(t => t.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"NonVisitTaskManager.Modal.Edit('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"NonVisitTaskManager.Delete('<#=Id#>');\" class=\"deleteTemplate\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Log.LoadNonVisitTaskManagerLog('<#=Id#>');\" >Activity Logs</a>").Title("Action").Sortable(false).Width(160).Visible(!Current.IsAgencyFrozen);
            
        })
            .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "NonVisitTaskManager"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_NonVisitTaskManager .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (!Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "Assign User Task", Click: UserInterface.ShowNewNonVisitTaskManager } ])
)<% } if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css({ height: "auto", top: 60 });
</script>
<% } %>