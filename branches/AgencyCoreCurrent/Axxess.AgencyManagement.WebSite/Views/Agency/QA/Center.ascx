﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<QaCenterViewData>" %>
<span class="wintitle">QA Center | <%= Current.AgencyName %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top" style="margin-top: 10px;">
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new {  @class = "PatientBranchCode" })%></div></div>
            <div class="row"><label>View:</label><div><select name="list" class="PatientStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><select name="list" class="PatientPaymentDropDown"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs</option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Qa_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom" style="top: 105px;"><% Html.RenderPartial("~/Views/Agency/QA/Patients.ascx"); %></div>
    </div>
    <div id="QaMainResult" class="ui-layout-center"><% if (Model == null || Model.Count == 0) { %><div class="abs center"><p>No Patients found that fit your search criteria.</p></div><% } %></div>
</div>