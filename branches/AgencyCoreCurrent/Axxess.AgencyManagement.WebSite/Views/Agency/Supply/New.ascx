﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Supply | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Add", "Supply", FormMethod.Post, new { @id = "newSupplyForm" })) { %>
<div class="wrapper main">
    <fieldset>
       <div class="wide-column">
         <div class="row"><label for="New_Supply_Description" class="strong">Description:</label><div class="float-right"><%= Html.TextBox("Description", "", new { @id = "New_Supply_Description", @class = "required", @maxlength = "350", @style = "width: 320px;" })%></div></div>
         <div class="row"><label for="New_Supply_HcpcsCode" class="strong">HCPCS:</label><div class="float-right"><%= Html.TextBox("Code", "", new { @id = "New_Supply_HcpcsCode", @class = "", @maxlength = "6", @style = "width: 70px;" })%></div></div>
         <div class="row"><label for="New_Supply_RevCode" class="strong">Revenue Code:</label><div class="float-right"><%= Html.TextBox("RevenueCode", "", new { @id = "New_Supply_RevCode", @class = "", @maxlength = "6", @style = "width: 70px;" })%></div></div>
         <div class="row"><label for="New_Supply_UnitCost" class="strong">Unit Cost:</label><div class="float-right"><%= Html.TextBox("UnitCost", "", new { @id = "New_Supply_UnitCost", @class = "", @maxlength = "6", @style = "width: 70px;" })%></div></div>
       </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newsupply');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
