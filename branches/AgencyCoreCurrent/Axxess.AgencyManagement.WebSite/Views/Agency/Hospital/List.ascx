﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Hospitals | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Hospitals", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyHospital>().Name("List_AgencyHospital").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.Name).Title("Hospital Name").Width(150).Sortable(true);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(true);
    columns.Bound(c => c.AddressFull).Title("Address").Sortable(false);
    columns.Bound(c => c.PhoneFormatted).Title("Phone").Width(120).Sortable(false);
    columns.Bound(c => c.FaxFormatted).Title("Fax Number").Width(120).Sortable(false);
    columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
    columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditHospital('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Hospital.Delete('<#=Id#>');\" class=\"deleteContact\">Delete</a>").Title("Action").Sortable(false).Width(100).Visible(!Current.IsAgencyFrozen);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Hospital")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_AgencyHospital .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageHospital) && !Current.IsAgencyFrozen) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Hospital", Click: UserInterface.ShowNewHospital } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Export to Excel", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
