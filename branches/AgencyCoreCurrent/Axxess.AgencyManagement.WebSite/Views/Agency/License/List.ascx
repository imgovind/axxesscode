﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LicenseItem>>" %>
<ul>
    <li>
        <span class="grid-fifth">First Name</span>
        <span class="grid-fifth">Last Name</span>
        <span class="grid-fifth">License Type</span>
        <span class="grid-tenth">Issue Date</span>
        <span class="grid-tenth">Expire Date</span>
        <span class="grid-tenth">Software User?</span>
<%  if (!Current.IsAgencyFrozen) { %>
        <span>Action</span>
<%  } %>
    </li>
</ul>
<%  if (Model != null && Model.Count > 0) { %>
<ol>
    <%  foreach (var license in Model) { %>
    <li>
        <span class="grid-fifth"><%= license.FirstName %></span>
        <span class="grid-fifth"><%= license.LastName %></span>
        <span class="grid-fifth"><%= license.AssetUrl %></span>
        <span class="grid-tenth"><%= license.IssueDateFormatted.IsEqual("01/01/0001") ? string.Empty : license.IssueDateFormatted %></span>
        <span class="grid-tenth"><%= license.ExpireDateFormatted.IsEqual("01/01/0001") ? string.Empty : license.ExpireDateFormatted %></span>
        <span class="grid-tenth"><%= license.IsUser %></span>
        <%  if (!Current.IsAgencyFrozen) { %>
        <span>
            <a class="link" onclick="LicenseManager.Edit('<%= license.Id %>', '<%= license.UserId %>')">Edit</a>
            |
            <a class="link" onclick="LicenseManager.Delete('<%= license.Id %>', '<%= license.UserId %>')">Delete</a>
        </span>
        <%  } %>
    </li>
    <%  } %>
</ol>
<%  } else { %>
<h1 class="blue">No Licenses Found</h1>
<%  } %>