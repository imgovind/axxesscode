<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
<%  using (Html.BeginForm("License/Add", "Agency", FormMethod.Post, new { @id = "NewLicenseItem_Form" })) { %>
    <fieldset>
        <legend>New License</legend>
        <div class="wide-column">
            <div class="row">
                <label for="NewLicenseItem_FirstName" class="fl strong">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", string.Empty, new { @id = "NewLicenseItem_FirstName", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewLicenseItem_LastName" class="fl strong">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", string.Empty, new { @id = "NewLicenseItem_LastName", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="NewLicenseItem_LicenseType" class="fl strong">License Type</label>
                <div class="fr"><%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", string.Empty, new { @id = "NewLicenseItem_Type" })%></div>
                <div class="clr"></div>
                <div class="fr license-type-other">
                    <label for="NewLicenseItem_TypeOther"><em>(Specify)</em></label>
                    <%= Html.TextBox("OtherLicenseType", string.Empty, new { @id = "NewLicenseItem_TypeOther" }) %>
                </div>
            </div>
            <div class="row">
                <label for="NewLicenseItem_IssueDate" class="fl strong">Initiation Date</label>
                <div class="fr"><input type="text" class="date-picker required" name="IssueDate" id="NewLicenseItem_IssueDate" /></div>
            </div>
            <div class="row">
                <label for="NewLicenseItem_ExpireDate" class="fl strong">Expiration Date</label>
                <div class="fr"><input type="text" class="date-picker" name="ExpireDate" id="NewLicenseItem_ExpireDate" /></div>
            </div>
            <div class="row">
                <label for="NewLicenseItem_Attachment" class="fl strong">File Attachment</label>
                <div class="fr"><input type="file" name="Attachment" id="NewLicenseItem_Attachment" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save close">Add</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
<%  } %>
</div>