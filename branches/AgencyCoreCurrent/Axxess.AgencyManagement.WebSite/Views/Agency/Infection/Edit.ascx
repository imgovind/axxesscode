﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Infection>" %>
<span class="wintitle">Edit Infection Log | <%= Current.AgencyName %></span>
<% using (Html.BeginForm("Update", "Infection", FormMethod.Post, new { @id = "editInfectionReportForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Infection_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Infection_PatientId" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Infection_UserId" })%>
<% string[] infectionTypes = Model.InfectionType != null && Model.InfectionType != "" ? Model.InfectionType.Split(';') : null; %>
<% if (Model != null) Model.SignatureDate = DateTime.Today; %>
<div class="wrapper main">
    <%  if (Model.StatusComment.IsNotNullOrEmpty()) { %>
    <fieldset class="return-alert">
        <div>
            <span class="img icon error float-left"></span>
            <p>This document has been returned by a member of your QA Team.  Please review the reasons for the return and make appropriate changes.</p>
            <div class="buttons">
                <ul>
                    <li class="red"><a href="javascript:void(0)" onclick="Acore.ReturnReason('<%= Model.Id %>','<%= Model.EpisodeId %>','<%= Model.PatientId %>');return false">Return Comments</a></li>
                </ul>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_PatientId" class="float-left">Patient:</label>
                <div class="float-right"><span class="bigtext"><%= Model.PatientName %></span></div>
            </div>
            <% if (Model.EpisodeId.IsEmpty()) { %>
            <div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_Infection_EpisodeId", @class = "requireddropdown" })%></div>
            </div>
            <% } else { %>
            <div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right"><span class="bigtext"><%=string.Format("{0}-{1}",Model.EpisodeStartDate,Model.EpisodeEndDate) %></span></div>
            </div>
            <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Infection_EpisodeId" })%>
            <% } %>
            <div class="row">
                <div>
                    <label for="PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                    <div class="float-right">
                        <span class="bigtext"><%= Model.Diagnosis.ContainsKey("PrimaryDiagnosis") ? Model.Diagnosis["PrimaryDiagnosis"] : "" %></span>
                        <%  if (Model.Diagnosis.ContainsKey("ICD9M") && Model.Diagnosis["ICD9M"].IsNotNullOrEmpty()) { %>
                         <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= Model.Diagnosis["ICD9M"] %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                        <%  } %>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <label for="SecondaryDiagnosis" class="float-left">Secondary Diagnosis:</label>
                    <div class="float-right">
                        <span class="bigtext"><%= Model.Diagnosis.ContainsKey("SecondaryDiagnosis") ? Model.Diagnosis["SecondaryDiagnosis"] : ""%></span>
                        <%  if (Model.Diagnosis.ContainsKey("ICD9M2") && Model.Diagnosis["ICD9M2"].IsNotNullOrEmpty()) { %>
                         <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= Model.Diagnosis["ICD9M2"] %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
                        <%  } %>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", Model.PhysicianId != Guid.Empty ? Model.PhysicianId.ToString() : string.Empty, new { @id = "Edit_Infection_PhysicianId", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right ancillary-button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
         </div><div class="column">
            <div class="row">
                <label for="Edit_Infection_InfectionDate" class="float-left">Date of Infection:</label>
                <div class="float-right"><input type="text" class="date-picker required" name="InfectionDate" value="<%= Model.InfectionDate.IsValid() ? Model.InfectionDate.ToShortDateString() : string.Empty %>" id="Edit_Infection_InfectionDate" /></div>
            </div><div class="row">
                <label for="Edit_Infection_TreatmentPrescribedYes" class="float-left">Treatment Prescribed?</label>
                <div class="float-right">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", Model.TreatmentPrescribed == "Yes" ? true : false, new { @id = "Edit_Infection_TreatmentPrescribedYes", @class = "radio" })%>
                    <label for="Edit_Infection_TreatmentPrescribedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", Model.TreatmentPrescribed == "No" ? true : false, new { @id = "Edit_Infection_TreatmentPrescribedNo", @class = "radio" })%>
                    <label for="Edit_Infection_TreatmentPrescribedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", Model.TreatmentPrescribed == "NA" ? true : false, new { @id = "Edit_Infection_TreatmentPrescribedNA", @class = "radio" })%>
                    <label for="Edit_Infection_TreatmentPrescribedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="Edit_Infection_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right">
                    <%= Html.RadioButton("MDNotified", "Yes", Model.MDNotified == "Yes" ? true : false, new { @id = "Edit_Infection_MDNotifiedYes", @class = "radio" })%>
                    <label for="Edit_Infection_MDNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", Model.MDNotified == "No" ? true : false, new { @id = "Edit_Infection_MDNotifiedNo", @class = "radio" })%>
                    <label for="Edit_Infection_MDNotifiedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", Model.MDNotified == "NA" ? true : false, new { @id = "Edit_Infection_MDNotifiedNA", @class = "radio" })%>
                    <label for="Edit_Infection_MDNotifiedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="Edit_Infection_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", Model.NewOrdersCreated == "Yes" ? true : false, new { @id = "Edit_Infection_NewOrdersYes", @class = "radio" })%>
                    <label for="Edit_Infection_NewOrdersYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", Model.NewOrdersCreated == "No" ? true : false, new { @id = "Edit_Infection_NewOrdersNo", @class = "radio" })%>
                    <label for="Edit_Infection_NewOrdersNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", Model.NewOrdersCreated == "NA" ? true : false, new { @id = "Edit_Infection_NewOrdersNA", @class = "radio" })%>
                    <label for="Edit_Infection_NewOrdersNA" class="inline-radio">N/A</label>
                </div>
            </div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Type of Infection</legend>
         <table class="form">
            <tbody>
                <tr>
                    <td>
                        <%= string.Format("<input id='Edit_Infection_InfectionType1' type='checkbox' value='Gastrointestinal' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Gastrointestinal") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType1" class="radio">Gastrointestinal</label>
                    </td><td>
                        <%= string.Format("<input id='Edit_Infection_InfectionType2' type='checkbox' value='Respiratory' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Respiratory") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType2" class="radio">Respiratory</label>
                    </td><td>
                        <%= string.Format("<input id='Edit_Infection_InfectionType3' type='checkbox' value='Skin' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Skin") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType3" class="radio">Skin</label>
                    </td><td>
                        <%= string.Format("<input id='Edit_Infection_InfectionType4' type='checkbox' value='Wound' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Wound") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType4" class="radio">Wound</label>
                    </td>
                </tr><tr>
                    <td>
                        <%= string.Format("<input id='Edit_Infection_InfectionType5' type='checkbox' value='Urinary' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Urinary") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType5" class="radio">Urinary</label>
                    </td><td>
                        <%= string.Format("<input id='Edit_Infection_InfectionType6' type='checkbox' value='Other' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Other") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType6" class="radio">Other (specify)</label>
                    </td><td colspan="2">
                        <%= Html.TextBox("InfectionTypeOther", Model.InfectionTypeOther, new { @id = "Edit_Infection_InfectionTypeOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <legend>Treatment</legend>
        <table class="form">
            <tbody>
                <tr class="line-seperated vert">
                    <td>
                        <label for="Treatment">Treatment / Antibiotic:</label>
                        <%= Html.Templates("TreatmentTemplates", new { @class = "Templates", @template = "#New_Infection_Treatment" })%>
                        <div><%= Html.TextArea("Treatment", Model.Treatment, new { @id = "New_Infection_Treatment", @style = "height: 180px;" })%></div>
                    </td>
                </tr>
            </tbody>
        </table><table class="form">
            <tbody>
                <tr class="line-seperated vert">
                    <td>
                        <label for="Orders">Narrative:</label>
                        <%= Html.Templates("NarrativeTemplates", new { @class = "Templates", @template = "#New_Infection_Narrative" })%>
                        <div><%= Html.TextArea("Orders", Model.Orders, new { @id = "New_Infection_Narrative", @style = "height: 180px;" })%></div>
                    </td>
                </tr>
            </tbody>
        </table><table class="form">
            <tbody>
                <tr class="line-seperated vert">
                    <td>
                        <label for="FollowUp">Follow Up:</label>
                        <%= Html.Templates("FollowUpTemplates", new { @class = "Templates", @template = "#New_Infection_FollowUp" })%> 
                        <div><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "New_Infection_FollowUp", @style = "height: 180px;" })%></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_ClinicianSignature" class="bigtext float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_Infection_ClinicianSignature" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="Edit_Infection_SignatureDate" class="bigtext float-left">Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="SignatureDate" id="Edit_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "Edit_Infection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#Edit_Infection_Status').val('515');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#Edit_Infection_Status').val('520');$(this).closest('form').submit();">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editinfectionreport');">Exit</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
   
    Template.OnChangeInit();
    
    
</script>
<% } %>