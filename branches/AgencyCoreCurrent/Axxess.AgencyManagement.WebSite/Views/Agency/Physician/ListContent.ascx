﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<AgencyPhysician>>" %>
<%= Html.Telerik()
        .Grid(Model)
        .Name("List_PhysicianGrid")
             .HtmlAttributes(new { @style = "top:50px;bottom:0px" }) 
        .Columns(columns => {
            columns.Bound(p => p.NPI).Title("NPI").Sortable(true).Width(3);
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(true).Width(4);
            columns.Bound(p => p.EmailAddress).Title("Email").Sortable(true).Width(4);
            columns.Bound(p => p.AddressFull).Title("Address").Sortable(false).Width(5);
            columns.Bound(p => p.PhoneWorkFormatted).Title("Phone Number").Sortable(false).Width(3);
            columns.Bound(p => p.PhoneAlternateFormatted).Title("Alternate Number").Sortable(false).Width(3);
            columns.Bound(p => p.FaxNumberFormatted).Title("Fax Number").Sortable(false).Width(3);
            columns.Bound(p => p.PhysicianAccess).Template(p=>p.PhysicianAccess ? "Yes" : "No").ClientTemplate("<#= PhysicianAccess ? 'Yes' : 'No' #>").Title("Physician Access").Sortable(false).Width(2);
            columns.Bound(p => p.IsPecosVerified).Template(p => p.IsPecosVerified ? "Yes" : "No").ClientTemplate("<#= IsPecosVerified ? 'Yes' : 'No' #>").Title("PECOS").Sortable(false).Width(2);
            columns.Bound(p => p.Id).Template(p=>string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPhysician('{0}');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Physician.Delete('{0}');\" >Delete</a>",p.Id)).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPhysician('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Physician.Delete('<#=Id#>');\" class=\"\">Delete</a>").Sortable(false).Title("Action").Width(2).Visible(!Current.IsAgencyFrozen);
        })
             .Sortable(sorting =>
                                                 sorting.SortMode(GridSortMode.SingleColumn)
                                                     .OrderBy(order =>
                                                     {
                                                         var sortName = ViewData["SortColumn"] != null ? ViewData["SortColumn"].ToString() : string.Empty;
                                                         var sortDirection = ViewData["SortDirection"] != null ? ViewData["SortDirection"].ToString() : string.Empty;
                                                         if (sortName == "NPI")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.NPI).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.NPI).Descending();
                                                             }
                                                         }
                                                         else if (sortName == "DisplayName")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.DisplayName).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.DisplayName).Descending();
                                                             }
                                                         }
                                                         else if (sortName == "EmailAddress")
                                                         {
                                                             if (sortDirection == "ASC")
                                                             {
                                                                 order.Add(o => o.EmailAddress).Ascending();
                                                             }
                                                             else if (sortDirection == "DESC")
                                                             {
                                                                 order.Add(o => o.EmailAddress).Descending();
                                                             }
                                                         }

                                                     }))
        .Scrollable(scrolling => scrolling.Enabled(true)).Footer(false) %>
        
        <script type="text/javascript">
            $("#List_PhysicianGrid .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
            $("#window_listphysicians_content").css({ 'bottom': '0px' });
            
            $("#List_PhysicianGrid div.t-grid-header div.t-grid-header-wrap table tbody tr th.t-header a.t-link").each(function() {
                var link = $(this).attr("href");
                $(this).attr("href", "javascript:void(0)").attr("onclick", "U.RebindDataGridContent('List_Physicians','Agency/PhysicianListContent',{  },'" + U.ParameterByName(link, 'List_PhysicianGrid-orderBy') + "');");
            });
        </script>