﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop();
// -- Build Menu -->
Acore.AddMenu({ Name: "Home", Id: "home", IconX: "121", IconY: "86" });
    Acore.AddMenu({ Name: "My Account", Id: "account", Parent: "home" });
Acore.AddMenu({ Name: "Create", Id: "create", IconX: "140", IconY: "108" });
    Acore.AddMenu({ Name: "New", Id: "createnew", Parent: "create" });
Acore.AddMenu({ Name: "Patients", Id: "patients", IconX: "183", IconY: "108" });
Acore.AddMenu({ Name: "Help", Id: "help", IconX: "186", IconY: "86" });
// -- Home Menu -->
Acore.AddWindow({ Name: "Edit Profile", Id: "editprofile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "account" });
Acore.AddWindow({ Name: "Reset Signature", Id: "forgotsignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "account", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "200px", Width: "500px" });
Acore.AddWindow({ Name: "Dashboard", MenuName: "My Dashboard", Id: "homepage", Url: "Home/Dashboard", OnLoad: Widgets.Init, Menu: "home" });
Acore.AddWindow({ Name: "My Messages", Id: "Inbox", Url: "Message/Inbox", OnLoad: Message.InitInbox, Menu: "home" });

// -- Create Menu (Some Duplicated to Admin Menu) -->
Acore.AddWindow({ Name: "Compose Message", MenuName: "Message", Id: "NewMessage", Url: "Message/New", OnLoad: Message.InitCompose, Menu: [ "createnew" ] });

// -- Patient Menu -->
Acore.AddWindow({ Name: "Patient Charts", Id: "patientcenter", Url: "Patient/Center", OnLoad: Patient.InitCenter, Menu: "patients" ,Inputs: { status: 1 } });
// -- Help Menu -->
Acore.AddMenuItem({ Name: "Training Manual", Href: '<%= string.Format("http://axxessweb.com/kb/?u={0}", SessionStore.SessionId) %>', Parent: "help" });
Acore.AddMenuItem({ Name: "Discussion Forum", Href: "/Forum", Parent: "help" });
Acore.AddMenuItem({ Name: "Facebook User Community", Href: "http://www.facebook.com/axxessusers", Parent: "help" });
Acore.AddMenuItem({ Name: "Support Page", Href: "http://axxessweb.com/support", Parent: "help" });
Acore.AddWindow({ Name: "After-Hours Support", Id: "afterhourssupport", Url: "Agency/AfterHoursSupport", Menu: "help", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "300px", Width: "600px" });
Acore.AddMenuItem({ Name: "Like us on Facebook", Href: "https://www.facebook.com/pages/Axxess-Healthcare-Consult/99677660749", Parent: "help" });
Acore.AddWindow({ Name: "Launch Join.Me", Id: "JoinMeMeeting", Url: "User/JoinMe", Menu: "support" });
Acore.AddWindow({ Name: "Recent Software Updates", Id: "RecentUpdates", Url: "Home/Updates", Menu: "help" });

// -- Windows not found in the menus -->
Acore.AddWindow({ Name: "Allergy Profile", Id: "allergyprofile", Url: "Patient/AllergyProfile" });
Acore.AddWindow({ Name: "Medicare Eligibility Reports", Id: "medicareeligibilitylist", Url: "Patient/MedicareEligibilityList" });
Acore.AddWindow({ Name: "Medication Profile SnapShot", Id: "medicationprofilesnapshot", Url: "Patient/MedicationProfileSnapShot", OnLoad: Patient.InitMedicationProfileSnapshot });
Acore.AddWindow({ Name: "Medication Profile", Id: "medicationprofile", Url: "Patient/MedicationHistoryView" });
Acore.AddWindow({ Name: "Signed Medication Profiles", Id: "medicationprofilesnapshothistory", Url: "Patient/MedicationHistorySnapshotView" });
Acore.AddWindow({ Name: "Authorization List", Id: "listauthorizations" });
Acore.AddWindow({ Name: "Communication Notes", Id: "patientcommunicationnoteslist" });
Acore.AddWindow({ Name: "Patient Orders History", Id: "patientordershistory" });
Acore.AddWindow({ Name: "Patient 60 Day Summary", Id: "patientsixtydaysummary" });
Acore.AddWindow({ Name: "Patient Vital Signs Charts", Id: "PatientVitalSigns", Url: "Patient/VitalSignsCharts", OnLoad: Patient.VitalSigns.Init });
Acore.AddWindow({ Name: "Patient Documents", Id: "patientdocuments" });
$("ul#mainmenu").Menu();
Acore.GetRemoteContent = <%= AppSettings.GetRemoteContent %>;
Acore.Open("homepage");
</script>

