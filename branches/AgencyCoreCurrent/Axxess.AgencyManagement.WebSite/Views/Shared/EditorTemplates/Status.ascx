﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var items = new List<SelectListItem>();
            var billStatus = Enum.GetValues(typeof(BillingStatus));
            if (billStatus != null && billStatus.Length > 0)
            {
                foreach (BillingStatus status in billStatus)
                {
                    items.Add(new SelectListItem
                    {
                        Text = status.GetDescription(),
                        Value = ((int)status).ToString(),
                    });
                }
            } %>
<%= Html.Telerik().DropDownList().Name("Status").BindTo(new SelectList(items, "Value", "Text")).HtmlAttributes(new { @class = "Status", id = ViewData.TemplateInfo.HtmlFieldPrefix + DateTime.Now.Millisecond.ToString() })%>
