﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<% var contentId = string.Format("Billing_CenterContent{2}Rap_{0}{1}",Model.BranchId,Model.Insurance,ViewData.ContainsKey("BillUIIdentifier") ? ViewData["BillUIIdentifier"].ToString() : string.Empty); %>
<div id="<%=contentId %>">
<div class="billing">
     <% var isDataExist = (Model != null && Model.Claims != null && Model.Claims.Count > 0); %>
     <input type="hidden" name="BranchId" value="<%=Model.BranchId %>" />
     <input type="hidden" name="PrimaryInsurance" value="<%=Model.Insurance %>" />
     <input type="hidden" name="Type" value="RAP" />
    <input type="hidden" name="RapSort" value="rap-name" />
    <input type="hidden" name="IsSortInvert" value="true" />
    <ul>
        <li class="align-center">
        <%=string.Format("{0} | {1}", Model.BranchName, Model.InsuranceName)%>
        <% if (isDataExist)
           { %>  [ <a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsXls', { 'branchId': '<%=Model.BranchId %>', 'insuranceid':'<%=Model.Insurance %>','parentSortType': 'branch', 'columnSortType': $('input[name=RapSort]').val(), 'claimType': 'RAP', });" >Export to Excel</a> ]
        [ <a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdf', { 'branchId': '<%=Model.BranchId %>', 'insuranceid':'<%=Model.Insurance %>','parentSortType': 'branch', 'columnSortType': $('input[name=RapSort]').val(), 'claimType': 'RAP', });" >Print</a> ]<%} %>
        </li>
        <li>
            <% if(!Current.IsAgencyFrozen) { %>
            <span class="rap-checkbox"></span>
            <% } %>
            <span class="rap-name pointer" onclick="Billing.Sort('<%=contentId %>','Rap', 'rap-name');">Patient Name</span>
            <span class="rap-id pointer" onclick="Billing.Sort('<%=contentId %>','Rap', 'rap-id');">Patient Id/MR Number</span>
            <span class="rap-episode pointer" onclick="Billing.Sort('<%=contentId %>','Rap', 'rap-episode');">Episode Period</span>
            <span class="icon-column">OASIS</span>
            <span class="icon-column">Billable Visit</span>
            <span class="icon-column">Verified</span>
        </li>
    </ul>
    <%
    if (isDataExist) {%>
     <ol>
      <%  int i = 1;
        foreach (var claim in Model.Claims)
        {
            var rap = (RapBill)claim;%>
            
        <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd " : "even ") + (rap.IsOasisComplete && rap.IsFirstBillableVisit ? "ready" : "notready")) %>
            <% if(!Current.IsAgencyFrozen) { %>
            <span class="rap-checkbox"><%= i %>. <%=  rap.IsOasisComplete &&  rap.IsFirstBillableVisit && rap.IsVerified ? "<input name='ClaimSelected' class='radio' type='checkbox' value='" + rap.Id + "' id='RapSelected" + rap.Id + "' />" : "" %></span>
            <% } %> 
            <%= string.Format("<a class='float-right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/RapPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\"><span class='img icon print'></span></a>", rap.EpisodeId, rap.PatientId) %>
            <% if(!Current.IsAgencyFrozen) { %>
            <a href='javascript:void(0);' onclick="<%= (rap.IsOasisComplete && rap.IsFirstBillableVisit) ? "UserInterface.ShowRap('" + rap.EpisodeId + "','" + rap.PatientId + "');" : "U.Growl('Error: Not Completed', 'error');" %>">
            <% } %> 
                <span class="rap-name"><%= rap.LastName %>, <%= rap.FirstName %></span>
                 </a>
                <span class="rap-id"><%= rap.PatientIdNumber%></span>
                <span class="rap-episode"><span class="very-hidden"><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("MM/dd/yyyy") : string.Empty %><%= rap.EpisodeEndDate != null ? "&#8211;" + rap.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty %></span>
                <span class="icon-column"><span class='img icon <%= rap.IsOasisComplete ? "success-small" : "error-small" %>'></span></span>
                <span class="icon-column"><span class='img icon <%= rap.IsFirstBillableVisit ? "success-small" : "error-small" %>'></span></span>
                <span class="icon-column"><span class='img icon <%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "success-small" : "error-small" %>'></span></span>
           
        </li><%
            i++;
        }%></ol>
       
   <% } %>
   </div>
    <%  if (isDataExist && Model.Insurance > 0 && Current.HasRight(Permissions.GenerateClaimFiles) && !Current.IsAgencyFrozen) { %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.loadGenerate('#<%=contentId %>','claimSummary<%= Model.ClaimType.ToString().ToLower()%>');">Generate Selected</a></li>
            <li><a href="javascript:void(0);" onclick="Billing.GenerateAllCompleted('#<%=contentId %>','claimSummary<%= Model.ClaimType.ToString().ToLower()%>');">Generate All Completed</a></li>
        </ul>
    </div>
    <%  } %>
    <script type="text/javascript">
        $("#<%=contentId %> ol li:first").addClass("first");
        $("#<%=contentId %> ol li:last").addClass("last");
    </script>
</div>
