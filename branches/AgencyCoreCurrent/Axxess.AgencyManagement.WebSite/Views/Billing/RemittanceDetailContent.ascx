﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
 <table class="remittance">
    <%  if (Model != null && Model.RemittanceData != null && Model.RemittanceData.Claim !=null && Model.RemittanceData.Claim.Count > 0)
        { %>
        <%  int zebra = 0, count = 0; %>
        <tr class="claiminfo"><th colspan="4">Claim(s)</th></tr>
        <%  foreach (var claim in Model.RemittanceData.Claim)
            { %>
            <%  if (claim.ProviderLevelAdjustment != null) { %>
                     <tr class="<%= zebra % 2 > 0 ? "even" : "odd" %>">
                         <td colspan="4">
                            <table class="claim">
                               <tbody>
                                  <tr class="top"><th colspan="4">Provider Level Adjustment</th></tr>
                                  <tr class="bottom">
                                    <td><label class="float-left">Provider Identifier:</label><label class="float-right"><%=claim.ProviderLevelAdjustment.ProviderIdentifier%></label></td>
                                    <td><label class="float-left">Fiscal Period Date:</label><label class="float-right"><%=claim.ProviderLevelAdjustment.FiscalPeriodDate.IsValidPHPDate() ? claim.ProviderLevelAdjustment.FiscalPeriodDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label></td>
                                    <td><label class="float-left">Adjustment Reason:</label><label class="float-right"><%= claim.ProviderLevelAdjustment.AdjustmentReasonDesc%></label></td>
                                    <td><label class="float-left">Adjustment Amount:</label><label class="float-right"><%=string.Format("${0:#,0.00}", claim.ProviderLevelAdjustment.ProviderAdjustmentAmount)%></label></td>
                                  </tr>
                               </tbody>
                             </table>
                         </td>
                     </tr>
                <%  zebra++; %>
            <%  } %>
            <%  if (claim.ClaimPaymentInformation != null && claim.ClaimPaymentInformation.Count > 0)
                {
                    var i = 0; %>
                <%  foreach (var claimPaymentInfo in claim.ClaimPaymentInformation) { %>
                      <tr class="<%= zebra % 2 > 0 ? "even" : "odd" %>">
                         <td colspan="4">
                            <table class="claim">
                               <tbody>
                                  <tr class="top">
                                    <td><label class="float-left">Patient Name:</label><label class="float-right"><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty%></label></td>
                                    <td><%  if (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty()) { %><label class="float-left"><%= claimPaymentInfo.Patient.IdQualifierName%>:</label><label class="float-right"><%= claimPaymentInfo.Patient.Id%></label><%  } %></td>
                                    <td><label class="float-left">Patient Control Number:</label><label class="float-right"><%= claimPaymentInfo.PatientControlNumber%></label></td>
                                    <td><label class="float-left">ICN Number:</label><label class="float-right"><%= claimPaymentInfo.PayerClaimControlNumber%></label></td>
                                  </tr>
                                  <tr>
                                    <td><label class="float-left">Start Date:</label><label class="float-right"><%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label></td>
                                    <td><label class="float-left">End Date:</label><label class="float-right"><%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label></td>
                                    <td><label class="float-left">Type Of Bill:</label><label class="float-right"><%= claimPaymentInfo.TypeOfBill%></label></td>
                                    <td><label class="float-left">Claim Status:</label><label class="float-right"><%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode)%></label></td>
                                  </tr>
                                  <tr>
                                    <td><label class="float-left">Claim Number:</label><label class="float-right"><%= count + 1 %></label></td>
                                    <td><label class="float-left">Reported Charge:</label><label class="float-right"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount)%></label></td>
                                    <td><label class="float-left">Remittance:</label><label class="float-right"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount)%></label></td>
                                    <td><label class="float-left">Line Adjustment Amount:</label><label class="float-right"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ServiceAdjustmentTotal)%></label></td>
                                  </tr>
                                  <tr class="bottom">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><label class="float-left">Paid Amount:</label><label class="float-right"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ClaimPaymentAmount) %></label></td>
                                  </tr>
                                  <%  if (!claimPaymentInfo.IsPosted && claimPaymentInfo.AssociatedClaims != null && claimPaymentInfo.AssociatedClaims.Count > 0){ %>
                                  <tr>
                                     <td colspan="3">
                                      <table class="episodes">
                                         <thead><tr><th width="2%"></th><th>Patient Id</th><th>Patient Name</th><th> Episode</th><th> Claim Date</th><th> Batch Id</th></tr></thead>
                                         <tbody><% foreach (var cl in claimPaymentInfo.AssociatedClaims){ %><tr><td><%=string.Format("<input id=\"Episodes_{3}\" type=\"checkbox\" value=\"{0}|{1}|{2}\" name=\"Episodes\" class=\"radio  float-left\" />", cl.EpisodeId,cl.BatchId,claimPaymentInfo.PayerClaimControlNumber ,i)%> </td><td><%= cl.PatientIdNumber%></td><td><%= cl.DisplayName%></td><td><%= cl.EpisodeRange%></td><td><%= cl.ClaimDateFormatted%></td><td><%= cl.BatchId%></td></tr><%  }%></tbody>
                                      </table>
                                    </td>
                                     <td>
                                        <div class="buttons"><ul><li><a href="javascript:void(0)">Post</a></li></ul></div>
                                    </td>
                                  </tr>
                                  <%  }%>
                               </tbody>
                            </table>
                         </td>
                      </tr>
                    <%  count++; %>
                    <%  zebra++; %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </table>
<script type="text/javascript">
    var $episode = $('div#RemittanceDetailContainer table.episodes tbody');
    $episode.each(function() {
        var $each = $(this);
        var $eachbox = $("input:checkbox", $each);
        $eachbox.each(function() { $(this).bind('click', function() { if ($(this).is(':checked')) {  $eachbox.removeAttr('checked'); $(this).attr('checked', true); } else {  $eachbox.removeAttr('checked'); } }); });
        $each.closest("table.claim").find("div.buttons ul li a", $each).each(function() { $(this).click(function() { Billing.PostRemittance('<%=Model.Id %>', $each); }); });
    });
</script>