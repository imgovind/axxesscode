﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimSnapShotViewData>" %>
<div class="form-wrapper">
<%  using (Html.BeginForm("UpdateSnapShotClaim", "Billing", FormMethod.Post, new { @id = "updateBatchClaimForm" })) { %>
    <%= Html.Hidden("Id", Model.Id)%>
    <%= Html.Hidden("BatchId", Model.BatchId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <fieldset>
        <legend>Update Information</legend>
        <div class="wide-column">
            <div class="row">
                <label for="PaymentAmountValue" class="float-left">Payment Amount:</label>
                <div class="float-right">$<%= Html.TextBox("PaymentAmountValue", Model.PaymentAmount > 0 ? Model.PaymentAmount.ToString() : "0.00", new { @class = "text input_wrapper", @maxlength = "" })%></div>
            </div>
            <div class="row">
                <label for="PaymentDateValue" class="float-left">Payment Date:</label>
                <div class="float-right"><input type="text" class="date-picker" name="PaymentDateValue" value="<%= Model.PaymentDate.Date <= DateTime.MinValue.Date ? DateTime.Now.ToShortDateString() : Model.PaymentDate.ToShortDateString() %>" /></div>
            </div>
            <div class="row">
                <label for="PaymentDateValue" class="float-left">Status:</label>
                <div class="float-right"> <%= Html.BillStatus("Status", Model.Status.ToString(), false, new {}) %> </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>